<?php
/**
 * 签约艺术家
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class member_artistControl extends MemberAccessControl {
    public function __construct()
    {
        parent::__construct();
    }

    //艺术家列表
    public function indexOp(){
        $this->artist_listOp();
    }

    //艺术家列表
    public function artist_listOp(){
        $param = $_GET;
        if(isset($param['order_type']) && in_array(strtolower($param['order_type']),array('desc','asc'))){
            $order = 'add_time '.strtolower($param['order_type']);
        }
        $condition = array();
        $condition['bind_member_id'] = $this->member_info['member_id'];
        $field = 'member.member_id,member.member_name,member.member_avatar,member.member_time,artist_vendue.artist_vendue_id';
        $member_model = Model('member');
        $member_list = $member_model->getArtistWithMemberList($condition, $field, 20, $order);
        $member_ids = $this->_dataSort($member_list);
        $this->_makeCommissionData($member_list, $member_ids);
        Tpl::output('artist_list', $member_list);
        Tpl::showpage('member_artist.list');
    }

    /**
     * 整理数据结构
     * @param $member_list
     * @return array
     */
    private function _dataSort(& $member_list){
        $member_ids = array();
        $tmp_list = array();
        foreach($member_list as $member){
            array_push($member_ids,$member['member_id']);
            $tmp_list[$member['member_id']] = $member;
        }
        $member_list = $tmp_list;
        return $member_ids;
    }

    /**
     * 处理艺术家佣金
     * @param $member_list
     * @param $member_ids
     */
    private function _makeCommissionData(& $member_list, $member_ids){
        if(!empty($member_list) && !empty($member_ids)){
            $condition = array();
            $condition['from_member_id'] = array('in',$member_ids);
            $condition['dis_member_id'] = $this->member_info['member_id'];
            $field = "from_member_id,sum('commission_amount') as commission_amount";
            $member_model = Model('member');
            $contri_list = (array)$member_model->table('member_commission')->field($field)->where($condition)->group('from_member_id')->select();
            foreach($contri_list as $val){
                $member_list[$val['from_member_id']]['commission_amount'] = $val['commission_amount'];
            }
        }
    }
}