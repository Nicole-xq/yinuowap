<?php
/**
 * 会员中心——账户概览
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');

class memberControl extends BaseMemberControl{

    /**
     * 我的商城
     */
    public function homeOp() {
        $model_consume = Model('consume');
        $consume_list = $model_consume->getConsumeList(array('member_id' => $_SESSION['member_id']), '*', 0, 10);
        Tpl::output('consume_list', $consume_list);
        Tpl::showpage('member_home');
    }

    /**
     * 我的账户
     * @return [type] [description]
     */
    public function accountOp() {
    	$menu_list = $this->_getMenuList();
    	Tpl::output('menu_list', $menu_list);
    	Tpl::showpage('member_account');
    }

    /**
     * 我的红包
     * @return array()
     */
    public function redpacketOp() {
        $menu_list = $this->_getMenuList();
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);

        $model_redpacket = Model('redpacket');
        //更新红包过期状态 
        $model_redpacket->updateRedpacketExpire($_SESSION['member_id']);
        //查询红包
        $where = array();
        $where['rpacket_owner_id'] = $_SESSION['member_id'];
        $where['rpacket_state'] = 1;
        $list = $model_redpacket->getRedpacketList($where, '*', 0, 6, 'rpacket_active_date desc');
        Tpl::output('list', $list);
        Tpl::output('redpacketstate_arr', $model_redpacket->getRedpacketState());
        Tpl::output('show_page',$model_redpacket->showpage(2)) ;

        Tpl::output('member_info', $member_info);
        Tpl::output('menu_list', $menu_list);
        Tpl::showpage('member_new_redpacket.list');
    }

    /**
     * 我的代金券
     * @return array()
     */
    public function voucherOp() {
        $menu_list = $this->_getMenuList();
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);

        $model = Model('voucher');
        $list = $model->getMemberVoucherList($_SESSION['member_id'], 1, 10, 'voucher_active_date desc');
        // 清空缓存
        dcache($_SESSION['member_id'], 'm_voucher');

        Tpl::output('list', $list);
        Tpl::output('show_page', $model->showpage(2));


        Tpl::output('member_info', $member_info);
        Tpl::output('menu_list', $menu_list);
        Tpl::showpage('member_new_voucher.list');
    }

    /**
     * 余额列表
     * @return [type] [description]
     */
    public function predeposit_listOp(){

        $model_pd = Model('predeposit');
        $condition = array();
        $condition['lg_member_id'] = $_SESSION['member_id'];
        $list = $model_pd->getPdLogList($condition,20,'*','lg_id desc');

        Tpl::output('show_page',$model_pd->showpage());
        Tpl::output('list',$list);
        //Tpl::showpage('predeposit.pd_log_list');
        Tpl::showpage('predeposit.new_pd_list','null_layout');
    }

    /**
     * 积分列表
     * @return [type] [description]
     */
    public function points_listOp(){
        Language::read('member_member_points,member_pointorder');
        $where = array();
        $where['pl_memberid'] = $_SESSION['member_id'];
        //查询积分日志列表
        $points_model = Model('points');
        $list_log = $points_model->getPointsLogList($where, '*', 0, 10);
        //信息输出
        Tpl::output('show_page',$points_model->showpage());
        Tpl::output('list_log',$list_log);
        Tpl::showpage('points.new_list','null_layout');
    }


    //银行卡管理
    public function bank_card_listOp(){
        $model_bankcard = Model('bankcard');
        $bankcard_list = $model_bankcard->getbankCardList(array('member_id'=>$_SESSION['member_id']));
        foreach($bankcard_list as $k=>$v){
            $count = strlen($v['bank_card']);
            $bankcard_list[$k]['bank_card'] = substr_replace($v['bank_card'],str_repeat('*',$count-4),0,-4);
        }
        $menu_list = $this->_getMenuList();
        Tpl::output('menu_list', $menu_list);
        Tpl::output('bankcard_list', $bankcard_list);
        Tpl::showpage('bank_card.list');
    }

    //添加银行卡
    public function add_bank_cardOp(){
        $menu_list = $this->_getMenuList();
        Tpl::output('menu_list', $menu_list);
        $model_article = Model('article');
        $yinuo_pay = $model_article->getArticleOne(10,1);
        Tpl::output('yinuo_pay', $yinuo_pay);
        Tpl::showpage('bank_card.add');
    }

    //保存银行卡
    public function save_cardOp(){
        $model_bankcard = Model('bankcard');
        $model_member = Model('member');
        $result = chksubmit(true,C('captcha_status_register'),'num');
        if ($result){
            if ($result === -11){
                showDialog('非法访问','','error');
            }elseif ($result === -12){
                showDialog('验证码错误','','error');
            }
        } else {
            showDialog('非法访问','','error');
        }
        $vcode = $_POST['vcode'];
        $condition = array();
        $condition['member_id'] = $_SESSION['member_id'];
        $condition['auth_code'] = intval($vcode);
        $member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
        if (!$member_common_info) {
            showDialog('手机验证码错误，请重新输入','','error');
        }
        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
            showDialog('手机验证码已过期，请重新获取验证码','','error');
        }

        $appcode = C('bank.appcode');//appcode
        $bankcard_info = $model_bankcard->getbankCardInfo(array('bank_card'=>$_POST['bank_card']));
        if(!empty($bankcard_info)){
            showDialog('您当前使用的银行卡已绑定，请点击银行卡查看',urlMember('member','bank_card_list'),'error');
        }


        $host = "http://jisubank4.market.alicloudapi.com";
        $path = "/bankcardverify4/verify";
        $method = "GET";

        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "bankcard=".$_POST['bank_card']."&idcard=".$_POST['id_number']."&mobile=".$_POST['phone']."&realname=".$_POST['true_name']."";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $result = curl_exec($curl);

        $jsonarr = json_decode($result, true);
        if($jsonarr['status'] != 0 || $jsonarr['result']['verifystatus'] != 0)
        {
            showDialog($jsonarr['msg'],'','error');
        }else{
            $result = $jsonarr['result'];
            $data = array();
		    $banlInfo = $this->_bankName($result['bankcard']);
		    if($banlInfo){
		        $data['card_type'] = $banlInfo['cardtype'];
		        $data['bank_name'] = $banlInfo['bankname'];
		        $data['bank_image'] = $banlInfo['bankimage'];
            }
            $data['member_name'] = $_SESSION['member_name'];
            $data['member_id'] = $_SESSION['member_id'];
            $data['true_name'] = $result['realname'];
            $data['bank_card'] = $result['bankcard'];
            $data['id_number'] = $result['idcard'];
            $data['member_phone'] = $result['mobile'];
            $result = $model_bankcard->addbankCard($data);
            if($result){
                showDialog('绑定成功',urlMember('member','bank_card_list'),'succ');
            }else{
                showDialog('绑定失败','','error');
            }

        }

    }

    /**
	 * 银行卡归属查询
	 * */
	private function _bankName($card){
	    if(!$card) return null;
	    $card = $card;
        $host = "http://aliyun.apistore.cn";
        $path = "/7";
        $method = "GET";
        $appcode = C('bank_info.appcode');
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "bankcard=" . $card;
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }

		$result = json_decode(curl_exec($curl), true);
        if($result['error_code'] == 0) return $result['result'];
        return null;
    }

    //保证金
    public function margin_log_listOp() {
        $model_margin_log = Model('margin_log');
        $condition = array();
        $condition['member_id'] = $_SESSION['member_id'];
        $list = $model_margin_log->getLogList($condition,20);
        Tpl::output('show_page',$model_margin_log->showpage());
        Tpl::output('list',$list);
        Tpl::showpage('margin.log_list','null_layout');
    }

    /**
     * 返佣记录
     */
    public function commission_log_listOp(){
        $condition = array();
        $condition['dis_member_id'] = $_SESSION['member_id'];//157
        $commission_model = Model('member_commission');
        $field = 'order_sn,from_member_name,goods_name,commission_amount,dis_commis_state';
        $list = $commission_model->getCommissionList($condition,$field,10);
        Tpl::output('show_page',$commission_model->showpage());
        Tpl::output('list',$list);
        Tpl::showPage('commission.log_list','null_layout');

    }

    //解绑银行卡
    public function remove_cardOp(){
        $bankcard_id = $_GET['bankcard_id'];
        $model_bankcard = Model('bankcard');
        $result = $model_bankcard->delbankCard(array('bankcard_id'=>$bankcard_id));
        if($result){
            showDialog('解绑成功',urlMember('member','bank_card_list'),'succ');
        }else{
            showDialog('解绑失败','','error');
        }
    }

    //银行卡是否为优选
    public function edit_defaultOp(){
        $bankcard_id = $_GET['bankcard_id'];
        $model_bankcard = Model('bankcard');
        $data = array();
        $data['is_default'] = 1;
        $cardid_list = $model_bankcard->getbankCardList(array('member_id'=>$_SESSION['member_id'],'bankcard_id'=>array('neq',$bankcard_id)),'','','bankcard_id');
        foreach($cardid_list as $k=>$v){
            $a[$k]= $v['bankcard_id'];
        }
        $model_bankcard->editbankCard($data,array('bankcard_id'=>$bankcard_id));
        $result = $model_bankcard->editbankCard(array('is_default'=>0),array('bankcard_id'=>array('in',$a)));
        if($result){
            showDialog('设置默认优选成功',urlMember('member','bank_card_list'),'succ');
        }else{
            showDialog('设置默认优选失败','','error');
        }
    }

    /**
     * 发送短信验证码
     * */
    public function send_modify_mobileOp()
    {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_GET["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号码'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            exit(json_encode(array('state'=>'false','msg'=>$error)));
        }

        $model_member = Model('member');

        //发送频率验证
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$_SESSION['member_id']));
        if (!empty($member_common_info['send_mb_time'])) {
            if (TIMESTAMP - $member_common_info['send_mb_time'] < 58) {
                exit(json_encode(array('state'=>'false','msg'=>'请60秒以后再次发送短信')));
            }
        }

        // 生成验证码
        $verify_code = rand(1000,9999);

        //组装发送内容
        $model_tpl = Model('mail_templates');
        $tpl_info = $model_tpl->getTplInfo(array('code'=>'authenticate'));
        $param = array();
        $param['site_name'] = C('site_name');
        $param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
        $param['verify_code'] = $verify_code;
        $message    = ncReplaceText($tpl_info['content'],$param);
        // 发送
        $sms = new Sms();
        $result = $sms->send($_GET["mobile"],$message);

        // 发送成功回调
        if ($result) {
            $data = array();
            $data['auth_code'] = $verify_code;
            $data['send_acode_time'] = TIMESTAMP;
            $data['send_mb_time'] = TIMESTAMP;
            $update = $model_member->editMemberCommon($data,array('member_id'=>$_SESSION['member_id']));
            if (!$update) {
                exit(json_encode(array('state'=>'false','msg'=>'系统发生错误，如有疑问请与管理员联系')));
            }
            exit(json_encode(array('state'=>'true','msg'=>'发送成功')));
        } else {
            exit(json_encode(array('state'=>'false','msg'=>'发送失败')));
        }
    }


    /*
     * 保证金账户提现到预存款
     * */
    public function margin_to_pdOp()
    {
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
        if (chksubmit()) {
            $num = ncPriceFormat(floatval($_POST['num']));
            if ($num <= 0) {
                showDialog('提现金额异常', 'reload', 'error');
            }
            if ($member_info['available_margin'] < $num) {
                return callback(false, '提现金额大于余额');
            }

            $logic_auction_order = Logic('auction_order');
            $result = $logic_auction_order->margin_to_pd($num, $_SESSION['member_id'], $_SESSION['member_name']);
            if (!$result['state']) {
                showDialog($result['msg'], 'reload', 'error');
            } else {
                showDialog($result['msg'], 'reload', 'succ');
            }
        }
        Tpl::output('member_info', $member_info);
        Tpl::showpage('margin_to_pd', 'null_layout');
    }

    /**
     * 左侧导航
     * 菜单数组中child的下标要和其链接的act对应。否则面包屑不能正常显示
     * @return array
     */
    private function _getMenuList() {
        $menu_list = array('account' => array('name' => '会员账户', 'child' => array('member_account' => array('name' => '我的账户', 'url' => urlMember('member', 'account')))));
//        if($this->member_info['member_type'] < 4){

            $menu_list['account']['child']['member_partner'] = array('name' => '我的合伙人', 'url' => urlMember('member_partner', 'index'));

            if (in_array($this->member_info['member_type'], array('2', '3'))) {
                $menu_list['account']['child']['member_artist'] = array('name' => '签约艺术家', 'url' => urlMember('member_artist', 'index'));
            }

            $menu_list['account']['child']['member_invitecode'] = array('name' => '邀请码', 'url' => urlMember('member_invitecode', 'index'));
//        }
        return $menu_list;
    }
}
