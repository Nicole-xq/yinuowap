<?php
/**
 * 邀请码
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class member_invitecodeControl extends MemberAccessControl {
    public function __construct() {
        parent::__construct();


    }

    public function indexOp(){
        $this->mine_codeOp();
    }

    /**
     * 我的邀请码
     */
    public function mine_codeOp(){
        $model_member = Model('member');
        $member_dcode = $model_member->getUpperMember(array('member_id'=>$this->member_info['member_id']));

        if(!$member_dcode['dis_code']){
            /** @var j_memberLogic $logic_member */
            $logic_member = Logic('j_member');
            if($logic_member->createCode($this->member_info)){
                $member_dcode = $model_member->getUpperMember(array('member_id'=>$this->member_info['member_id']));
            }
        }

        if($member_dcode['dis_code']){
            $code_url = WAP_SITE_URL."/tmpl/member/register.html?invite_code=".$member_dcode['dis_code'];
            Tpl::output('code_url',$code_url);
        }

        Tpl::output('dcode_info',$member_dcode);
        Tpl::showpage('member_invitecode.mine');
    }

    /**
     * 绑定邀请码
     */
    public function bind_codeOp(){
        $this->mine_codeOp();
        die();
        $model_member = Model('member');
        $msg = '';
        if(chksubmit()){
            if(Logic('j_member')->bind_by_code($this->member_info['member_id'], $_POST['invite_code'], $msg)){
                showDialog('绑定成功','index.php?act=member_invitecode&op=bind_code','succ');
            }else{
                $msg = empty($msg) ? '绑定失败' : $msg;
                showDialog($msg,'index.php?act=member_invitecode&op=bind_code','error');
            }
        }
        $member_dcode = $model_member->getUpperMember(array('member_id'=>$this->member_info['member_id']));
        Tpl::output('dcode_info',$member_dcode);
        Tpl::showpage('member_invitecode.bind');
    }

    /**
     * 校验短信验证码
     */
    public function check_pcodeOp(){
        $model_member = Model('member');
        $mem_common = $model_member->getMemberCommonInfo(array('member_id'=>$this->member_info['member_id']));
        if($mem_common['auth_code'] == trim($_REQUEST['phone_code'])){
            echo 'true';exit;
        }else{
            echo 'false';
        }
    }

    /**
     * 校验手机号
     */
    public function check_pnumOp(){
        $model_member = Model('member');
        $mem_common = $model_member->getMemberInfo(array('member_mobile'=>trim($_REQUEST['phone_num'])));
        if($mem_common['member_id'] == $this->member_info['member_id'] || empty($mem_common)){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    /**
     * 获取短信验证码
     */
    public function send_codeOp(){
        $sms = new Sms();
        $phone = trim($_POST['mobile']);
        $captcha = rand(1000, 9999);
        $log_msg = '【'.C('site_name').'】您于'.date("Y-m-d");
        $log_msg .= '申请邀请码，验证码：'.$captcha.'。';
        $result = $sms->send($phone,$log_msg);
        if($result){
            $model_member = Model('member');
            $data = array();
            $data['auth_code'] = $captcha;
            $data['send_acode_time'] = time();
            $data['send_acode_times'] = array('exp','send_acode_times+1');
            $model_member->editMemberCommon($data,array('member_id'=>$this->member_info['member_id']));
            echo 'true';
        }else{
            echo 'false';
        }
    }

    /**
     * 邀请码检测
     *
     * @param
     * @return
     */
    public function check_dcodeOp() {
        $model_member = Model('member');
        $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>strtolower(trim($_REQUEST['dcode']))));
        if(is_array($check_member_dcode) && count($check_member_dcode)>0) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /**
     * 获取邀请码
     *
     * @param $stri
     * @return string
     */
    private function _getInviteCode($stri){
        $model_member = Model('member');
        $invite_code = substr(hash('md5',$stri.time()),-8);
        $check_member_dcode = (array) $model_member->getUpperMember(array('dis_code'=>strtolower($invite_code)));
        if(!empty($check_member_dcode)) $this->_getInviteCode($stri);
        return $invite_code;
    }
}