<?php
/**
 * 微信登录
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');

class connect_wxControl extends BaseLoginControl{
    public function __construct(){
        parent::__construct();
        Language::read("home_login_register,home_login_index");
        Tpl::output('hidden_login', 1);
    }
    /**
     * 微信登录
     */
    public function indexOp(){
        if(empty($_GET['code'])) {
            Tpl::showpage('connect_wx.index','null_layout');
        } else {
            $this->get_infoOp();
        }
        
    }
    /**
     * 微信登录完善信息后提交数据
     */
    public function edit_infoOp(){
        if (chksubmit() && $_SESSION['wx_user_info']) {
            $_SESSION['wx_user_info']['nickname'] = $_POST["user"];
            $_SESSION['wx_user_info']['pwd'] = $_POST["password"];
            //校验是手机号是否存在
            $model_member = Model('member');
            $member_info = $model_member->getMemberInfo(['member_mobile'=> $_POST["mobile"]]);
            if (!empty($member_info)) {
                showDialog('手机号已被绑定',urlMember('login', 'index'),'error');
            }
            $_SESSION['wx_user_info']['member_mobile'] = $_POST["mobile"];
            $_SESSION['wx_user_info']['member_mobile_bind'] = 1;
            //验证码验证
            $logic_connect_api = Logic('connect_api');
            $state_data = $logic_connect_api->checkSmsCaptcha($_POST['mobile'], $_POST['sms_captcha'], $log_type = 1);
            if (!$state_data['state']){
                showDialog($state_data['msg'],urlMember('login', 'index'),'error');
            }
            $member = $logic_connect_api->wxRegister($_SESSION['wx_user_info'], $client = 'pc_www');
            $re = Logic('register')->getAward($member['member_id']);
            if(!empty($member)) {
                $model_member->createSession($member);//自动登录
                showDialog('登录成功',urlMember('member', 'home'),'succ');
            }
        }
    }
    /**
     * 回调获取信息
     */
    public function get_infoOp(){
        $code = $_GET['code'];
        if(!empty($code)) {
            $logic_connect_api = Logic('connect_api');
            $user_info = $logic_connect_api->getWxUserInfo($code);
            if(!empty($user_info['unionid'])) {
                $unionid = $user_info['unionid'];
                $model_member = Model('member');
                $member = $this->checkWxInfo($user_info);
                if(!empty($member) && empty($_SESSION['member_id'])) {//会员信息存在时自动登录
                    $model_member->createSession($member);
                    showDialog('登录成功',urlMember('member', 'home'),'succ');
                }
                if(!empty($_SESSION['member_id'])) {//已登录时绑定微信
                    $member_id = $_SESSION['member_id'];
                    if (!empty($member) && $member_id != $member['member_id']){
                        showDialog('此微信账号已绑定其他账号,请更换其他微信号','','error');//微信号已经绑定
                    }
                    $member = array();
                    $member['weixin_unionid'] = $unionid;
                    $member['weixin_open_id'] = $user_info['openid'];
                    $member['weixin_info'] = $user_info['weixin_info'];
                    $model_member->editMember(array('member_id'=> $member_id),$member);
                    showDialog('微信绑定成功',urlMember('member', 'home'),'succ');
                } else {//自动注册会员并登录
                    $this->register($user_info);
                    exit;
                }
            }
        }
        showDialog('微信登录失败',urlLogin('login', 'index'),'succ');
    }
    /**
     * 注册
     */
    public function register($user_info){
        Language::read("home_login_register,home_login_index");
        $unionid = $user_info['unionid'];
        $nickname = $user_info['nickname'];
        $_SESSION['wx_user_info'] = $user_info;
        if(!empty($unionid)) {
            $logic_connect_api = Logic('connect_api');
            $member = $logic_connect_api->wxRegister($user_info, 'www');
            if(!empty($member)) {
                Tpl::output('user_info',$user_info);
                Tpl::output('headimgurl',$user_info['headimgurl']);
                Tpl::output('password',$member['password']);
                Tpl::output('member_name',$member['member_name']);
                Tpl::showpage('connect_wx.register');
            }
        }
    }
    /**
     * 检测有无微信注册的用户并更新数据
     * @param $user_info API返回用户信息
     * @return array
     */
    public function checkWxInfo($user_info){
        $model_member = Model('member');
        //unionid 检测
        $member = $model_member->getMemberInfo(['weixin_unionid'=> $user_info['unionid']]);
        $member_id = $member['member_id'];
        // 更新微信数据
        if ($member_id){
            $update_data = [
                'weixin_info'=>$user_info['weixin_info'],
                'member_avatar'=>substr($user_info['headimgurl'], 0, -1).'132'
            ];
            $model_member->editMember(['member_id'=> $member_id],$update_data);
        }

        return $member;
    }

}