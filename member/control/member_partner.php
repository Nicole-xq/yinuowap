<?php
/**
 * 我的合伙人
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class member_partnerControl extends MemberAccessControl {
    public function __construct()
    {
        parent::__construct();
    }

    //合伙人列表
    public function indexOp(){
        $this->partner_listOp();
    }

    //合伙人列表
    public function partner_listOp(){
        $partner_list = $this->_getPartnerList($_GET);
        Tpl::output('partner_level','合作');
        Tpl::output('partner_list',$partner_list);
        Tpl::showpage('member_partner.list');
    }

    /**
     * 获取二级合作伙伴
     * @param $param
     */
    private function _getPartnerList($param){
        if(isset($param['order_type']) && in_array(strtolower($param['order_type']),array('desc','asc'))){
            $order = 'add_time '.strtolower($param['order_type']);
        }
        $condition = array();
        $condition['top_member'] = $this->member_info['member_id'];
        $field = 'member_distribute.*,member.member_name,member.member_avatar,member.member_time,member.member_id';
        $member_model = Model('member');
        $member_list = $member_model->getMemberDistributeWithMemberList($condition, $field, 20, $order);


        if(!empty($member_list)){
            $distribution_list = $this->_getMemberType();
            foreach($member_list as &$member){
                $member['member_type_name'] = $distribution_list[$member['member_type']];
            }
        }
        $member_ids = $this->_dataSort($member_list);
        $this->_checkChildInfo($member_list, $member_ids);
        return $member_list;
    }

    /**
     * 获取三级合作伙伴
     * @param $param
     */
    private function _getChildsPartnerList($param){
        if(isset($param['order_type']) && in_array(strtolower($param['order_type']),array('desc','asc'))){
            $order = 'add_time '.strtolower($param['order_type']);
        }
        $condition = array();
        $condition['top_member'] = $param['upper_id'];
        $field = 'member_distribute.*,member.member_name,member.member_avatar,member.member_time';
        $member_model = Model('member');
        $member_list = $member_model->getMemberDistributeWithMemberList($condition, $field, 20, $order);
        $member_ids = $this->_dataSort($member_list);
        $this->_checkChildInfo($member_list, $member_ids);
        return $member_list;
    }

    /**
     * 整理数据结构
     * @param $member_list
     * @return array
     */
    private function _dataSort(& $member_list){
        $member_ids = array();
        $tmp_list = array();
        foreach($member_list as $member){
            array_push($member_ids,$member['member_id']);
            $tmp_list[$member['member_id']] = $member;
        }
        $member_list = $tmp_list;
        return $member_ids;
    }

    /**
     * 补充合作伙伴佣金数据
     * @param $member_list
     * @param $member_ids
     */
    private function _checkChildInfo(& $member_list, $member_ids){
        if(!empty($member_list) && !empty($member_ids)){
            $condition = array();
            $condition['from_member_id'] = array('in',$member_ids);
            $condition['dis_member_id'] = $this->member_info['member_id'];
            $list = Model('member_commission')->get_members_commission($condition);

            foreach($list as $val){
                $member_list[$val['from_member_id']]['commission_amount'] = $val['commission_amount'];
            }
        }

    }

    /**
     * 会员类型
     */
    private function _getMemberType(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $arr = array();
        foreach($re as $v){
            $arr[$v['type_id']] = $v['name'];
        }
        return $arr;
    }


    /**
     * 获取下级订单返佣
     */
    public function partner_order_infoOp(){
        $form_member_id = $_REQUEST['partner_id'];

        if(empty($form_member_id)){
            goto END;
        }

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        $where['dis_member_id'] = $this->member_info['member_id'];
        $where['from_member_id'] = $form_member_id;
        $this->page = 100;
        $list = $model_commission->getByTypeCommission(1, $where, '*',$this->page, 'log_id desc');

        if(empty($list)){
            goto  END;
        }

        //获取订单信息
        $order_id = array();
        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $order_id[$value['order_id']] = $value['order_id'];
            $value['label_status'] = $model_commission->status_setting[$value['dis_commis_state']]['label'];
        }

        /** @var orderModel $model_order */
        $model_order = Model('order');
        Tpl::output('orders_list',$model_order->getCommissionOrderList($order_id));

        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(1, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        Tpl::output('sum_commission_amount',$sum_commission_amount);

        END:
        Tpl::output('commission_list',$list);

        Tpl::showpage('member_partner.order_list','null_layout');

    }



    /**
     * 获取下级会员保证金返佣
     */
    public function partner_commission_infoOp(){
        $form_member_id = $_REQUEST['partner_id'];

        if(empty($form_member_id)){
            goto END;
        }

        $list = Logic('j_commission_show')->get_form_margin_commission($this->member_info['member_id'], $form_member_id, 100);

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        $where['dis_member_id'] = $this->member_info['member_id'];
        $where['from_member_id'] = $form_member_id;
        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(2, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        Tpl::output('sum_commission_amount',$sum_commission_amount);


        END:

        Tpl::output('list',$list);
        Tpl::showpage('member_partner.commission_list','null_layout');

    }

    /**
     * 获取下级会员推荐返佣列表
     */
    public function partner_infoOp(){
        $form_member_id = $_REQUEST['partner_id'];

        if(empty($form_member_id)){
            goto END;
        }

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        $where['dis_member_id'] = $this->member_info['member_id'];
        $where['from_member_id'] = $form_member_id;
        $this->page = 100;
        $list = $model_commission->getByTypeCommission(3, $where, 'log_id,goods_name,commission_amount,add_time,dis_commis_rate,dis_commis_state',$this->page, 'log_id desc');

        if(empty($list)){
            goto  END;
        }

        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $value['label_status'] = $model_commission->status_setting[$value['dis_commis_state']]['label'];
        }

        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(3, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        Tpl::output('sum_commission_amount', $sum_commission_amount);


        END:
        Tpl::output('list', $list);

        Tpl::showpage('member_partner.info_list', 'null_layout');

    }

    public function partner_common_infoOp(){
        $form_member_id = $_GET['partner_id'];
        $model_member = Model('member');
        $partner_info = $model_member->getMemberInfo(array('member_id'=>$form_member_id),'member_name,member_id,member_avatar,member_type');
        $type_name_arr = $this->_getMemberType();
        $partner_info['member_type_name'] = $type_name_arr[$partner_info['member_type']];
        $re = $model_member->getUpperMember(array('member_id'=>$form_member_id));
        $partner_info['member_comment'] = $re['member_comment'];
        Tpl::output('partner',$partner_info);
        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        Tpl::output('sum_commission_amount',$sum_commission_amount);

        Tpl::showpage('member_partner.common_info');
    }

    public function update_member_commentOp(){
        $member_id = $_POST['member_id'];
        $comment = $_POST['comment'];
        $model_member = Model('member');
        $re = $model_member->editMemberDistribute(array('member_comment'=>$comment),array('member_id'=>$member_id));
        return $re;
//        print_R($re);exit;
//        echo 1;exit;
    }


}