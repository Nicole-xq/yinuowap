<?php
/**
 * 支付宝绑定
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');

class member_alipay_bindControl extends BaseMemberControl {
    public function __construct(){
        parent::__construct();
    }
    /**
     * 支付宝绑定
     */
    public function indexOp(){
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
        Tpl::output('member_info', $member_info);
        if (chksubmit()) {
            $update_array = array();
            $update_array['alipay_account'] = $_POST['alipay_account'];
            $update_array['alipay_name'] = $_POST['alipay_name'];
            $update_array['member_alipay_bind'] = 1;
            $result = $model_member->editMember(array('member_id'=>$_SESSION['member_id']),$update_array);
            $message = $result? '绑定成功' : '绑定失败';
            showDialog($message,'reload',$result ? 'succ' : 'error');

        }
        self::profile_menu('alipay_bind');
        Tpl::showpage('member_alipay');

    }

    public function alipay_editOp(){
        $member_id = $_GET['member_id'];
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($member_id);
        Tpl::output('member_info', $member_info);
        if (chksubmit()) {
            $update_array = array();
            $update_array['alipay_account'] = $_POST['alipay_account'];
            $update_array['alipay_name'] = $_POST['alipay_name'];
            $result = $model_member->editMember(array('member_id'=>$_POST['member_id']),$update_array);
            $message = $result? '绑定成功' : '绑定失败';
            if(!$result){
                showDialog($message,'reload','error');
            }else{
                showDialog($message,'index.php?act=member_alipay_bind&op=index','succ');
            }


        }
        self::profile_menu('alipay_bind');
        Tpl::showpage('member_alipay_edit');

    }

    /**
     * 用户中心右边，小导航
     *
     * @param string    $menu_type  导航类型
     * @param string    $menu_key   当前导航的menu_key
     * @param array     $array      附加菜单
     * @return
     */
    private function profile_menu($menu_key='',$array=array()) {
        $menu_array     = array();
        $menu_array = array(
            1=>array('menu_key'=>'alipay_bind', 'menu_name'=>'支付宝绑定',    'menu_url'=> urlMember('member_alipay_bind', 'index')),
        );
        if(!empty($array)) {
            $menu_array[] = $array;
        }
        Tpl::output('member_menu',$menu_array);
        Tpl::output('menu_key',$menu_key);
    }
}
