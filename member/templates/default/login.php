<?php defined('InShopNC') or exit('Access Invalid!');?>

<link href="<?php echo MEMBER_TEMPLATES_URL;?>/css/lm_registed.css" rel="stylesheet" type="text/css">

<div class="login-bg">
 <div class="nc-login-layout">
  <!--<div class="left-pic"><img src="<?php echo $output['lpic'];?>"  border="0"></div>-->
   <a style="width: 200px;height: 100px;position: absolute;left: 0;bottom: 0;cursor: pointer;" href="<?php echo urlLogin('login', 'register', array('ref_url' => $_GET['ref_url']));?>"></a>
  <div class="nc-login">
  <div class="arrow"></div>
    <div class="nc-login-mode">
      <ul class="tabs-nav">
        <li><a href="#default"><?php echo $lang['login_index_user_login'];?><i></i></a></li>
        <?php if (C('sms_login') == 1){?>
        <li><a href="#mobile">手机验证码登录<i></i></a></li>
        <?php } ?>
      </ul>
      <div id="tabs_container" class="tabs-container" >
        <div id="default" class="tabs-content" style="height: 260px;">
          <form id="login_form" class="nc-login-form" method="post" action="<?php echo urlLogin('login', 'login');?>">
            <?php Security::getToken();?>
            <input type="hidden" name="form_submit" value="ok" />
            <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt><img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/lm_username.png" alt=""></dt>
              <dd>
                <input type="text" class="text" autocomplete="off"  name="user_name" tipMsg="用户名/手机/邮箱" id="user_name"  >
              </dd>
            </dl>
            <dl>
              <dt><img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/lm_password.png" alt=""></dt>
              <dd>
                <input type="password" class="text" name="password" autocomplete="off" tipMsg="密码" id="password">
              </dd>
            </dl>
            <?php if(C('captcha_status_login') == '1') { ?>
            <div class="code-div mt15">
              <dl>
                <dt><?php echo $lang['login_index_checkcode'];?>：</dt>
                <dd>
                  <input type="text" name="captcha" autocomplete="off" class="text w100" tipMsg="<?php echo $lang['login_register_input_code'];?>" id="captcha" size="10" />                  
                </dd>
              </dl>
              <span><img src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>" name="codeimage" id="codeimage"> <a class="makecode" href="javascript:void(0)" onclick="javascript:document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();"><?php echo $lang['login_index_change_checkcode'];?></a></span></div>
            <?php } ?>
            <div class="handle-div">
            <span class="auto"><input type="checkbox" class="checkbox" name="auto_login" value="1">七天自动登录<em style="display: none;">请勿在公用电脑上使用</em></span>
            <a class="forget" href="<?php echo urlLogin('login', 'forget_password');?>"><?php echo $lang['login_index_forget_password'];?></a></div>
            <div class="submit-div">
              <input type="submit" class="submit" value="<?php echo $lang['login_index_login'];?>">
              <input type="hidden" value="<?php echo $_GET['ref_url']?>" name="ref_url">
            </div>
          </form>
        </div>
        <?php if (C('sms_login') == 1){?>
        <div id="mobile" class="tabs-content">
          <form id="post_form" method="post" class="nc-login-form" action="<?php echo urlLogin('connect_sms', 'login');?>">
            <?php Security::getToken();?>
            <input type="hidden" name="form_submit" value="ok" />
            <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
            <dl>
              <dt>手机号：</dt>
              <dd>
                <input name="phone" type="text" class="text" id="phone" tipMsg="可填写已注册的手机号接收短信" autocomplete="off" value="" >              
              </dd>
            </dl>
            <div class="code-div">
              <dl>
                <dt><?php echo $lang['login_register_code'];?>：</dt>
                <dd>
                  <input type="text" name="captcha" class="text w100" tipMsg="<?php echo $lang['login_register_input_code'];?>" id="image_captcha" size="10" />
                 
                </dd>
              </dl>
              <span><img src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>" title="<?php echo $lang['login_index_change_checkcode'];?>" name="codeimage" id="sms_codeimage"> <a class="makecode" href="javascript:void(0);" onclick="javascript:document.getElementById('sms_codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();"><?php echo $lang['login_password_change_code']; ?></a></span> </div>

              <div class="lm_code-div">
                    <dl>
                      <dt>验证码：</dt>
                      <dd>
                          <input type="text" name="sms_captcha" autocomplete="off" class="text" tipMsg="请输入验证码" id="sms_captcha" size="15"/>
                      </dd>
                    </dl>
                    <span><a href="javascript:void(0);" onclick="get_sms_captcha('2')">获取验证码</a></span>
              </div>

              <div class="submit-div">
                <input type="submit" id="submit" class="submit" value="<?php echo $lang['login_index_login'];?>">
                <input type="hidden" value="<?php echo $_GET['ref_url']?>" name="ref_url">
              </div>
          </form>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="nc-login-api" id="demo-form-site">

        <div class="lm_share_con">

            <?php if (C('qq_isuse') == 1 || C('sina_isuse') == 1 || C('weixin_isuse') == 1){?>
            <?php if (C('qq_isuse') == 1){?>
            <a href="<?php echo MEMBER_SITE_URL;?>/index.php?act=connect_qq" title="QQ账号登录" class="lm_qq"><i></i>QQ</a>
            <?php } ?>
            <?php if (C('sina_isuse') == 1){?>
            <a href="<?php echo MEMBER_SITE_URL;?>/index.php?act=connect_sina" title="新浪微博账号登录" class="lm_sina"><i></i>新浪微博</a>
            <?php } ?>
            <?php if (C('weixin_isuse') == 1){?>
            <a href="javascript:void(0);" onclick="ajax_form('weixin_form', '微信账号登录', '<?php echo urlLogin('connect_wx', 'index');?>', 360);" title="微信账号登录" class="lm_wx"><i></i>微信</a>
            <?php } ?>
            <?php } ?>
        </div>
        <a href="<?php echo urlLogin('login', 'register', array('ref_url' => $_GET['ref_url']));?>" class="lm_registed_href">注册新用户</a>


    </div>
  </div>
  <div class="clear"></div>
</div>
</div>
<script>
$(function(){
	//初始化Input的灰色提示信息  
	$('input[tipMsg]').inputTipText({pwd:'password'});
	//登录方式切换
	$('.nc-login-mode').tabulous({
		 effect: 'flip'//动画反转效果
	});	
	var div_form = '#default';
	$(".nc-login-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
    	}
	});
	$.validator.addMethod('name_length', function(value,element,param){
    name_length = value.replace(/[^\u0000-\u00ff]/g,"aa").length;
    if(name_length >= param[0] && name_length <= param[1]){
        return true;
    }else{
        return false;
    }
  }, '');

	$("#login_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        submitHandler:function(form){
            ajaxpost('login_form', '', '', 'onerror');
        },
        onkeyup: false,
		rules: {
            user_name : {
                required : true,
                //rangelength : [6,20],
                name_length : [4,20],
            },
            password : {
                required : true,
                minlength: 6,
                maxlength: 20
            }
			<?php if(C('captcha_status_login') == '1') { ?>
            ,captcha : {
                required : true,
                remote   : {
                    url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            }
			<?php } ?>
		},
		messages: {
            user_name : {
                required : '<?php echo $lang['login_register_input_username'];?>',
                name_length : '<?php echo $lang['login_register_username_range'];?>',
            },
            password  : {
                required : '<?php echo $lang['login_register_input_password'];?>',
                minlength: '<?php echo $lang['login_register_password_range'];?>',
                maxlength: '<?php echo $lang['login_register_password_range'];?>'
            }
			<?php if(C('captcha_status_login') == '1') { ?>
            ,captcha : {
                required : '<i class="icon-remove-circle" title="<?php echo $lang['login_index_input_checkcode'];?>"></i>',
				remote	 : '<i class="icon-remove-circle" title="<?php echo $lang['login_index_input_checkcode'];?>"></i>'
            }
			<?php } ?>
		}
	});

    // 勾选自动登录显示隐藏文字
    $('input[name="auto_login"]').click(function(){
        if ($(this).attr('checked')){
            $(this).attr('checked', true).next().show();
        } else {
            $(this).attr('checked', false).next().hide();
        }
    });
});
</script>
<?php if (C('sms_login') == 1){?>
<script type="text/javascript" src="<?php echo LOGIN_RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>
<script>
$(function(){
	$("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form){
    	    ajaxpost('post_form', '', '', 'onerror');
    	},
        onkeyup: false,
		rules: {
			phone: {
                required : true,
                mobile : true
            },
			captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	document.getElementById('sms_codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            },
			sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 4
            }
		},
		messages: {
			phone: {
                required : '请输入正确的手机号',
                mobile : '请输入正确的手机号'
            },
			captcha : {
                required : '<i class="icon-remove-circle" title="请输入正确的验证码"></i>',
                minlength: '<i class="icon-remove-circle" title="请输入正确的验证码"></i>',
				remote	 : '<i class="icon-remove-circle" title="请输入正确的验证码"></i>'
            },
			sms_captcha: {
                required : '请输入四位短信动态码',
                minlength: '请输入四位短信动态码'
            }
		}
	});
});
</script>
<?php } ?>