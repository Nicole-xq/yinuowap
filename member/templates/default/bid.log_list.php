<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#baozhengjin_list').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#baozhengjin_list'
    });
});
</script>
<table width="1025" border="0" cellspacing="0" cellpadding="0" class="account-record">
            <tr style="height: 36px">
              <td class="w10"></th>
              <td class="w150 tc">创建时间</td>
              <td class="w150 tc">拍品ID</td>
              <td class="w150 tc">出价价格</td>
              <td class="tc">是否匿名</td>
            </tr>
            <?php  if (count($output['list'])>0) { ?>
            <?php foreach($output['list'] as $v) { ?>
            <tr class="bd-line">
              <td></td>
              <td class="goods-time tl"><?php echo @date('Y-m-d H:i:s',$v['created_at']);?></td>
              <td class="tl"><?php echo $v['auction_id']; ?></td>
              <td class="tl"><?php echo $v['offer_num']; ?></td>
              <td class="tl"><?php echo $v['is_anonymous'] ? '匿名':'不匿名'; ?></td>
            </tr>
            <?php } ?>
            <?php } else {?>
            <tr>
              <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
            <?php } ?>
          </table>
          <?php  if (count($output['list'])>0): ?>
            <div class="pagination"> <?php echo $output['show_page']; ?></div></td>
          <?php endif; ?>
