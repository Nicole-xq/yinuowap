<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="account-center">
	<div class="people-list box">
		<div class="tabmenu" style="padding:0 2%;width: 96%;background:#fcfcfc;">
			<ul class="tab pngFix fl">
				<li class="active"><a href="javascript:void(0);">签约艺术家</a></li>
			</ul>			
			<ul class="jsddm fl">
				<li><a href="javascript:void(0);"><span class="show-select-text">时间排序</span>&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul class="check_option">
						<li data-type="desc"><a href="javascript:void(0);">最新时间</a></li>
						<li data-type="asc"><a href="javascript:void(0);">最长时间</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<?php if(is_array($output['artist_list']) && !empty($output['artist_list'])){?>
		<ul class="list">
			<?php foreach($output['artist_list'] as $artist){?>
			<li>
				<img src="<?php echo getMemberAvatar($artist['member_avatar']);?>" width="48" height="48">
				<div class="fl">
					<span class="all-block">伙伴名称：<?php echo $artist['member_name']?></span>
					<span class="all-block">注册时间：<?php echo date('Y-m-d',$artist['member_time']);?></span>
					<span class="all-block">佣金金额：￥<?php echo isset($artist['commission_amount'])?ncPriceFormat($artist['commission_amount']):ncPriceFormat(0);?></span>
				</div>
				<div class="clear"></div>
				<a href="<?php echo C('auction_site_url');?>/index.php?act=index&op=artist_detail&artist_id=<?php echo $artist['artist_vendue_id'];?>" class="fr" target="_blank">
					<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;查看详情
				</a>
			</li>
			<?php }?>	
		</ul>
		<?php }else{?>
			<div class="empty">
				您还没有签约艺术家
			</div>
		<?php }?>
		<div class="clear"></div>
	</div>
</div>

<script type="text/javascript">
	var timeout         = 500;
	var closetimer		= 0;
	var ddmenuitem      = 0;
	var orderType = "<?php echo $_GET['order_type']?>";
	if(orderType != ''){
		var obj = $('.check_option').find("li[data-type="+orderType+"]");
		$('.jsddm .show-select-text').html(obj.find('a').html());
	}
	function jsddm_open(){
		jsddm_canceltimer();
		jsddm_close();
		ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');
	}
	function jsddm_close(){	
		if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
	}
	function jsddm_timer(){	
		closetimer = window.setTimeout(jsddm_close, timeout);
	}
	function jsddm_canceltimer(){
		if(closetimer)
		{	
			window.clearTimeout(closetimer);
			closetimer = null;
		}
	}
	$(document).ready(function(){
		$('.jsddm > li').bind('mouseover', jsddm_open);
		$('.jsddm > li').bind('mouseout',  jsddm_timer);
	});
	document.onclick = jsddm_close;
	$('.check_option').on('click', 'li' ,function(){
		var orderType = $(this).attr('data-type');
		window.location.href = "index.php?act=member_artist&order_type="+orderType;
	});
</script>