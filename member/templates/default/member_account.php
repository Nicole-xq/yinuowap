<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<!--添加新的个人中心首页-->
<div class="account-center">
 <?php include template('member_account_common'); ?>
  <script type="text/javascript">
    $(document).ready(function(){
      var $tab_li = $('.new-tab ul li');
      $tab_li.hover(function(){
        $(this).addClass('selected').siblings().removeClass('selected');
        var index = $tab_li.index(this);
        $('div.tab_box > div').eq(index).show().siblings().hide();
      });
    });
    $(function(){$('.account-record tr:odd').css('background','#fff'); $('.account-record tr:even').css('background','#fafafa');});
  </script>
  <div class="new-tab">
      <ul class="tab_menu">
        <li class="selected">金额详情</li>
        <li>保证金详情</li>
        <li>诺币详情</li>
        <li>佣金详情</li>
      </ul>
      <div class="tab_box" >
        <div id="predeposit_list">
        </div>
        <div class="hide" id="baozhengjin_list">
        </div>
        <div class="hide" id="points_list">
        </div>
        <div class="hide" id="commission_list">
        </div>
      </div>
      <div class="clear"></div>
    </div>
</div>
  <script type="text/javascript">
    function get_predeposit(){
      $.ajax({
        type:"GET",
        url: MEMBER_SITE_URL+"/index.php?act=member&op=predeposit_list",
        dataType: "html",
        success: function(data){
          $('#predeposit_list').html(data);
        }
      });
     } 
     function get_points(){
      $.ajax({
        type:"GET",
        url: MEMBER_SITE_URL+"/index.php?act=member&op=points_list",
        dataType: "html",
        success: function(data){
          $('#points_list').html(data);
        }
      });
     }
     function get_bids(){
       $.ajax({
        type:"GET",
        url: MEMBER_SITE_URL+"/index.php?act=member&op=margin_log_list",
        dataType: "html",
        success: function(data){
          $('#baozhengjin_list').html(data);
        }
      });
     }

     function get_commission_list(){
         $.ajax({
            type:"GET",
            url: MEMBER_SITE_URL+"/index.php?act=member&op=commission_log_list",
            dataType: "html",
            success: function(data){
              $('#commission_list').html(data);
            }
          });
     }
    $(function(){
        get_predeposit();
        get_points();
        get_bids();
        get_commission_list();
      <?php if($_GET['list_location'] == 'predeposit'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:first-child').addClass('selected');
        $('#predeposit_list').show();
        $('#baozhengjin_list').hide();
        $('#points_list').hide();
        $('#commission_list').hide();
      <?php endif;?>
      <?php if($_GET['list_location'] == 'baozhengjin'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:nth-child(2)').addClass('selected');
        $('#predeposit_list').hide();
        $('#baozhengjin_list').show();
        $('#points_list').hide();
        $('#commission_list').hide();
      <?php endif;?>
      <?php if($_GET['list_location'] == 'points'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:nth-child(3)').addClass('selected');
        $('#predeposit_list').hide();
        $('#baozhengjin_list').hide();
        $('#points_list').show();
        $('#commission_list').hide();
      <?php endif;?>
      <?php if($_GET['list_location'] == 'commission'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:last-child').addClass('selected');
        $('#predeposit_list').hide();
        $('#baozhengjin_list').hide();
        $('#points_list').hide();
        $('#commission_list').show();
      <?php endif;?>
    });
  </script>