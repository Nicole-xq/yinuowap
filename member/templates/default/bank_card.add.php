<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .ncm-container .right-layout .account-center .invitation label{width:100%}
</style>
<div class="account-center">
    <?php include template('member_account_common'); ?>
    <form id="post_form" method="post" action="<?php echo urlMember('member','save_card')?>">
        <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
        <input type="hidden" name="form_submit" value="ok"/>
        <?php Security::getToken();?>
        <div class="box invitation">
            <div class="tabmenu" style="padding:0 2%;width: 96%;background:none">
                <ul class="tab pngFix" style="background:#fcfcfc;">
                    <li class="active"><a href="javascript:void(0);" style="border:none">我的银行卡&nbsp;<span style="color:#666">&gt;&nbsp;添加银行卡</span></a></li>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="bank-card">
                <label for="">真实姓名:&nbsp;&nbsp;<input type="text" name="true_name" placeholder="" autocomplete="off"  style="margin-top: -4px;width: 200px"><span class="fl"></span></label>
                <label for="">身份证号:&nbsp;&nbsp;<input type="text" name="id_number" id="id_number" placeholder="" autocomplete="off" style="margin-top: -4px;width: 200px"><span class="fl"></span></label>
                <label for="">银行卡号:&nbsp;&nbsp;<input type="text" name="bank_card" id="bank_card" placeholder="" autocomplete="off"  style="margin-top: -4px;width: 200px"><span class="fl"></span><span style="color:#999">输入卡号会智能识别银行和卡种</span></label>
                <label for="">手机号码:&nbsp;&nbsp;<input type="text" name="phone" id="phone" placeholder="" autocomplete="off" maxlength="11" style="margin-top: -4px;width: 200px"><span class="fl"></span><span style="color:#999">请填写您在银行预留的手机号，验证是否属于本人</span></label>


                <label for="">手机验证码:&nbsp;&nbsp;
<!--                    <input type="text" name="sms_seccode" placeholder="" autocomplete="off" maxlength="11" style="margin-top: -4px;width: 80px"><a href="javascript:void(0);" class="btn" style="display:inline-block;margin-left:10px" onclick="get_sms_captcha('4')"><i class="icon-mobile-phone"></i>发送短信验证</a><span class="fl"></span>-->
                    <input type="text" name="vcode" id="vcode" placeholder="" autocomplete="off" maxlength="6"style="width: 70px;margin-top: 12px">
                    <a href="javascript:;" id="send_auth_code" class="btn" style="display:inline-block;margin-left:10px;transform:translateY(11px);background:#f5f5f5;border: #cccccc solid 1px">
                        <span id="sending" style="display:none; color:#333;float:left;margin-top: 0">正在</span>
                                                        <span class="send_success_tips" style="display:none; color:#333;margin:0">
                                                            <strong id="show_times" class="red mr5"></strong>秒后再次
                                                        </span>
                        <span style="color:#333;margin:0">获取验证码</span>
                    </a>
                </label>
                <p class="send_success_tips hint mt10" style="display: none;">“安全验证码”已发出，请注意查收，请在
                    <strong>“30分种”</strong>内完成验证。
                </p>
                <div class="clear"></div>
                <input type="submit" class="btn" id="submitBtn" value="同意协议并确认" style="margin:20px auto 0; background:#000;width:140px">
                <a href="<?php echo empty($output['yinuo_pay']) ? '#' : urlMember('article', 'show', array('article_id' => $output['yinuo_pay']['article_id'])); ?>" style="text-align: center;display: block;width: 100%;margin: 10px 0 0">《艺诺快捷支付服务协议》</a>
            </div>
            <div class="clear"></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo LOGIN_RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>
<script type="text/javascript">

    $(function(){
        $("#submitBtn").click(function(){
            if($("#post_form").valid()){
//                check_captcha();
                $("#post_form").submit();
            }
        });

        //验证码秒数倒计时
        function StepTimes() {
            $num = parseInt($('#show_times').html());
            $num = $num - 1;
            $('#show_times').html($num);
            if ($num <= 0) {
                ALLOW_SEND = !ALLOW_SEND;
                $('.send_success_tips').hide();
            } else {
                setTimeout(StepTimes,1000);
            }
        }
        //发送短信验证码
        var ALLOW_SEND = true;

        $('#send_auth_code').on('click',function(){
            if ($('#phone').val() == '') return false;
            if (!ALLOW_SEND) return;
            ALLOW_SEND = !ALLOW_SEND;
            $('#sending').show();
            $.getJSON('index.php?act=member&op=send_modify_mobile',{mobile:$('#phone').val()},function(data){
                if (data.state == 'true') {
                    $('#sending').hide();
                    $('.send_success_tips').show();
                    $('#show_times').html(60);
                    setTimeout(StepTimes,1000);
                } else {
                    ALLOW_SEND = !ALLOW_SEND;
                    $('#sending').hide();
                    $('.send_success_tips').hide();
                    showDialog(data.msg,'error','','','','','','','','',2);
                }
            });
        });


//        $.validator.addMethod('checkBank', function(value,element){
//            var account = $("#bank_card").val();
//            var reg = /^\d{19}$/g; // 以19位数字开头，以19位数字结尾
//            if(!reg.test(account) )
//            {
//                return false;
//            }else {
//                return true;
//            }
//
//        }, '');

        $.validator.addMethod('checkID', function(value,element){
            var account = $("#id_number").val();
            var reg = /^(([0-9]{15})|([0-9]{18})|([0-9]{17}x))$/;
            if(!reg.test(account) )
            {
                return false;
            }else {
                return true;
            }

        }, '');

        $("#post_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parents('label').find('.fl').addClass('error');
                error_td.append(error);
            },
            onkeyup: false,
            rules: {
                true_name:{
                    required : true
                },
                id_number:{
                    required : true,
                    checkID: true
                },
                bank_card:{
                    required : true
                },
                phone: {
                    required : true,
                    mobile : true
                }
//                captcha : {
//                    required : true,
//                    minlength: 4,
//                    remote   : {
//                        url : 'index.php?act=seccode&op=check&nchash=<?php //echo getNchash();?>//',
//                        type: 'get',
//                        data:{
//                            captcha : function(){
//                                return $('#image_captcha').val();
//                            }
//                        },
//                        complete: function(data) {
//                            if(data.responseText == 'false') {
//                                document.getElementById('sms_codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php //echo getNchash();?>//&t=' + Math.random();
//                            }
//                        }
//                    }
//                },
//                sms_captcha: {
//                    required : function(element) {
//                        return $("#image_captcha").val().length == 4;
//                    },
//                    minlength: 6
//                }
            },
            messages: {
                true_name:{
                    required : '<i class="icon-exclamation-sign"></i>输入真实姓名'
                },
                id_number:{
                    required : '<i class="icon-exclamation-sign"></i>输入身份证号',
                    checkID: '<i class="icon-exclamation-sign"></i>身份证号错误'
                },
                bank_card:{
                    required : '<i class="icon-exclamation-sign"></i>输入银行卡号'
                },
                phone: {
                    required : '<i class="icon-exclamation-sign"></i>输入正确的手机号',
                    mobile : '<i class="icon-exclamation-sign"></i>输入正确的手机号'
                }
//                captcha : {
//                    required : '<i class="icon-remove-circle"></i>请输入验证码',
//                    minlength: '<i class="icon-remove-circle"></i>请输入验证码',
//                    remote	 : '<i class="icon-remove-circle" title=""></i>验证码不正确'
//                },
//                sms_captcha: {
//                    required : '<i class="icon-exclamation-sign"></i>请输入六位短信验证码',
//                    minlength: '<i class="icon-exclamation-sign"></i>请输入六位短信验证码'
//                }
            }
        });
    });
</script>