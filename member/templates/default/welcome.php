<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="registed_success">
    <div class="lm_container_ful">
        <div class="lm_info">
            注册成功！<?php echo str_cut($_SESSION['member_name'],20);?>欢迎加入艺诺网<br/> 祝您购物愉快
            <a href="<?php echo SHOP_SITE_URL;?>"><img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/lm_house.png" alt=""><span>去首页</span></a>
        </div>
        <div class="lm_image lm_clear">
            <div class="lm_one">
                <img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/lm_people.png" alt="">
                <span class="text">188元新人红包等你来领</span>
            </div>
            <div class="lm_line"></div>
            <div class="lm_two">
                <img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/lm_registed_ewm.png" alt="">
                <span class="text">扫码下载APP有惊喜哦</span>
            </div>
        </div>
    </div>
</div>