<?php defined('InShopNC') or exit('Access Invalid!');?>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>">
<title><?php echo $output['html_title'];?></title>
<meta name="keywords" content="<?php echo $output['seo_keywords']; ?>" />
<meta name="description" content="<?php echo $output['seo_description']; ?>" />
<meta name="author" content="ShopNC">
<meta name="copyright" content="ShopNC Inc. All Rights Reserved">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta content=always name=referrer>
    <meta name="renderer" content="webkit">
<?php echo html_entity_decode($output['setting_config']['qq_appcode'],ENT_QUOTES); ?><?php echo html_entity_decode($output['setting_config']['sina_appcode'],ENT_QUOTES); ?><?php echo html_entity_decode($output['setting_config']['share_qqzone_appcode'],ENT_QUOTES); ?><?php echo html_entity_decode($output['setting_config']['share_sinaweibo_appcode'],ENT_QUOTES); ?>
<style type="text/css">
body { _behavior: url(<?php echo LOGIN_TEMPLATES_URL;
?>/css/csshover.htc);
}
</style>
<link href="<?php echo LOGIN_TEMPLATES_URL;?>/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo LOGIN_TEMPLATES_URL;?>/css/home_header.css" rel="stylesheet" type="text/css">
<link href="<?php echo LOGIN_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<link href="<?php echo LOGIN_RESOURCE_SITE_URL;?>/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="<?php echo LOGIN_RESOURCE_SITE_URL;?>/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/html5shiv.js"></script>
      <script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/respond.min.js"></script>
<![endif]-->
<script>
var COOKIE_PRE = '<?php echo COOKIE_PRE;?>';var _CHARSET = '<?php echo strtolower(CHARSET);?>';var SITEURL = '<?php echo SHOP_SITE_URL;?>';var SHOP_SITE_URL = '<?php echo SHOP_SITE_URL;?>';var RESOURCE_SITE_URL = '<?php echo RESOURCE_SITE_URL;?>';var RESOURCE_SITE_URL = '<?php echo RESOURCE_SITE_URL;?>';var SHOP_TEMPLATES_URL = '<?php echo SHOP_TEMPLATES_URL;?>';
</script>
<script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/jquery.js"></script>
<script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/jquery-ui/jquery.ui.js"></script>
<script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/common.js"></script>
<script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/jquery.validation.min.js"></script>
<script src="<?php echo RESOURCE_SITE_URL_HTTPS;?>/js/dialog/dialog.js" id="dialog_js"></script>
<script src="<?php echo LOGIN_RESOURCE_SITE_URL?>/js/taglibs.js"></script>
<script src="<?php echo LOGIN_RESOURCE_SITE_URL?>/js/tabulous.js"></script>

<link href="<?php echo MEMBER_TEMPLATES_URL;?>/css/lm-style.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class="lm_header">
    <div class="lm_container_ful ">
        <a href="<?php echo SHOP_SITE_URL;?>"><img src="<?php echo UPLOAD_SITE_URL_HTTPS.DS.ATTACH_COMMON.DS.$output['setting_config']['site_logo']; ?>" class="pngFix"></a>
        <img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/lm/<?php echo $output['lm_logo']?>" alt=""></a>

        <?php if ($_SESSION['is_login'] === '1'):?>
        <div class="lm_header_right">
            <a href="<?php echo urlShop('member','home');?>">个人中心</a>
            | <a href="<?php echo urlLogin('login','logout');?>"><?php echo $lang['nc_logout'];?></a>
        </div>
        <?php elseif ($output['hidden_login'] != '1' && $_GET['op'] != 'index'):?>
        <div class="lm_header_right">
            已有账号
            <a href="<?php echo urlLogin('login', 'index', array('ref_url' => $_GET['ref_url']));?>">| [请登录]</a>
        </div>
        <?php endif;?>
    </div>
</div>
<!-- PublicHeadLayout End -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<?php require_once($tpl_file);?>
<?php require_once template('layout/footer_https');?>
</body>
</html>
