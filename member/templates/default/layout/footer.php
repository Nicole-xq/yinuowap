<!--start footer -->
<?php echo getChat($layout); ?>
<style>
    #faq {
        background: #f6f6f6;
        height: 210px;
        overflow: hidden
    }

    #faq li {
        width: 13%
    }

    #faq li:last-child {
        display: none
    }

    #footer {
        background: #f6f6f6;
        width: 100%
    }

    #faq .wrapper {
        border-bottom-color: #e6e6e6
    }
</style>

<div id="faq">
    <div class="wrapper">
        <?php if (is_array($output['article_list']) && !empty($output['article_list'])) { ?>
            <ul class="lm_clear">
                <?php foreach ($output['article_list'] as $k => $article_class) { ?>
                    <?php if (!empty($article_class)) { ?>
                        <li>
                            <dl class="s<?php echo '' . $k + 1; ?>">
                                <dt>
                                    <?php if (is_array($article_class['class'])) echo $article_class['class']['ac_name']; ?>
                                </dt>
                                <?php if (is_array($article_class['list']) && !empty($article_class['list'])) { ?>
                                    <?php foreach ($article_class['list'] as $article) { ?>
                                        <dd><i></i><a
                                                    href="<?php if ($article['article_url'] != '') echo $article['article_url']; else echo urlMember('article', 'show', array('article_id' => $article['article_id'])); ?>"
                                                    title="<?php echo $article['article_title']; ?>"> <?php echo $article['article_title']; ?> </a>
                                        </dd>
                                    <?php } ?>
                                <?php } ?>
                            </dl>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li>
                    <dl class="s7">
                        <dt>在线客服</dt>
                        <dd><i></i><a target="_blank" href="<?php echo urlMember('article', 'show',array('article_id'=> 21)); ?>" title="联系我们"> 联系我们 </a></dd>
                        <dd class="lm_tc"><i></i><a href="" title="微博"> 微博 </a>
                            <div class="lm_tc_con">
                                <img src="<?php echo RESOURCE_SITE_URL; ?>/image/weibo_154.jpg" alt="">
                                <span class="lm_text">扫一扫，关注微博</span>
                            </div>
                        </dd>
                        <dd class="lm_tc"><i></i><a href="#" title="微信"> 微信 </a>
                            <div class="lm_tc_con">
                                <img src="<?php echo RESOURCE_SITE_URL; ?>/image/weixin_154.jpg" alt="">
                                <span class="lm_text">扫一扫，关注微信</span>
                            </div>
                        </dd>
                        <dd>
                            <i></i><a href="#" title="电话：400-135-2688"> 电话：400-135-2688 </a>
                        </dd>
                        <dd>
                            <i></i><a href="#" title="工作时间：7*24小时"> 工作时间：7*24小时 </a>
                        </dd>
                    </dl>
                </li>
                <li>
                    <dl class="s8">
                        <dt>移动客户端</dt>
                        <dd>
                            <i></i><a href="" title="苹果IOS"> 苹果IOS </a>
                        </dd>
                        <dd>
                            <i></i><a href="" title="安卓"> 安卓 </a>
                        </dd>
                    </dl>
                </li>

            </ul>
        <?php } ?>
<!--        <div class="faq-other">-->
<!--            <div class="fl">-->
<!--                <h1>在线客服</h1>-->
<!--                <a href="#" name="connect" class="all-block"><span>联系我们</span></a>-->
<!--                <a href="">微博</a>-->
<!--                <a href="">微信</a>-->
<!--                <a href="">工作时间:7*24小时</a>-->
<!--                <h2><span class="fl">电话:400-135-2688</span></h2>-->
<!--            </div>-->
<!--            <div class="fr">-->
<!--                <h1 style="text-align: center">手机网站</h1>-->
<!--                <img src="--><?php //echo SHOP_TEMPLATES_URL; ?><!--/images/new-index/qrcode.png" alt="" class="all-block">-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>
<div id="footer" class="wrapper">
    <img src="<?php echo RESOURCE_SITE_URL; ?>/image/lm/index-footer.png" alt="" class="all-block"
         style="margin: 6px auto 0">
    <p style="text-align: center;padding: 12px 0 0">Copyright  2017 艺诺艺术有限公司 版权所有</p>
    <p style="text-align: center;padding: 4px 0 0"><?php echo $output['setting_config']['icp_number']; ?></p>
</div>
<?php if (C('debug') == 1) { ?>
    <div id="think_page_trace" class="trace">
        <fieldset id="querybox">
            <legend><?php echo $lang['nc_debug_trace_title']; ?></legend>
            <div> <?php print_r(\Shopnc\Tpl::showTrace()); ?> </div>
        </fieldset>
    </div>
<?php } ?>

<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.cookie.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
    $(function () {
        // Membership card
        $('[nctype="mcard"]').membershipCard({type: 'shop'});
    });
</script>
<!--end  cur_local -->
