<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#predeposit_list').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#predeposit_list'
    });
});
</script>
<table width="1025" border="0" cellspacing="0" cellpadding="0" class="account-record">
            <tr style="height: 36px">
              <td class="w10"></th>
              <td class="w150 tc">创建时间</td>
              <td class="w150 tc">收入(元)</td>
              <td class="w150 tc">支出(元)</td>
              <td class="w150 tc">冻结(元)</td>
              <td class="tc">交易方式</td>
            </tr>
            <?php  if (count($output['list'])>0) { ?>
            <?php foreach($output['list'] as $v) { ?>
            <tr class="bd-line">
              <td></td>
              <td class="goods-time tc"><?php echo @date('Y-m-d H:i:s',$v['lg_add_time']);?></td>
      <?php $availableFloat = (float) $v['lg_av_amount']; if ($availableFloat > 0) { ?>
              <td class="tc red">+<?php echo $v['lg_av_amount']; ?></td>
              <td class="tc green"></td>
      <?php } elseif ($availableFloat < 0) { ?>
              <td class="tc red"></td>
              <td class="tc green"><?php echo $v['lg_av_amount']; ?></td>
      <?php } else { ?>
              <td class="tc red"></td>
              <td class="tc green"></td>
      <?php } ?>
              <td class="tc blue"><?php echo floatval($v['lg_freeze_amount']) ? (floatval($v['lg_freeze_amount']) > 0 ? '+' : null ).$v['lg_freeze_amount'] : null;?></td>
              <td class="tc"><?php echo $v['lg_desc'];?></td>
            </tr>
            <?php } ?>
            <?php } else {?>
            <tr>
              <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
            <?php } ?>
          </table>
          <?php  if (count($output['list'])>0): ?>
            <div class="pagination"> <?php echo $output['show_page']; ?></div></td>
          <?php endif; ?>
