<?php defined('InShopNC') or exit('Access Invalid!');?>



<h3 class="qll_order_list_title">推荐返佣总收益：￥<?php echo ncPriceFormat($output['sum_commission_amount']); ?></h3>
<?php foreach($output['list'] as $row): ?>

<ul>
    <li class="qll_other_list_detail">
        <dl>
            <dt><?php echo $row['goods_name']; ?></dt>
            <dd>
                <span class="qll_order_detail_count">+<?php echo ncPriceFormat($row['commission_amount']); ?></span>
                <span><?php echo $row['label_date']; ?></span>
                <span><?php echo $row['label_status']; ?></span>
            </dd>
        </dl>
    </li>
</ul>
<?php endforeach; ?>
