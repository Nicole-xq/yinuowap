<div class="eject_con">
    <div id="warning-1" class="alert alert-error" style="display: none"></div>
    <form id="send_form" method="post" target="_parent" action="<?php echo urlMember('member', 'margin_to_pd') ?>">
        <input type="hidden" name="form_submit" value="ok" />
        <dl>
            <dt>提示：</dt>
            <dd>
                提现金额将转到预存款账户。
            </dd>
        </dl>
        <dl>
            <dt>可用金额：</dt>
            <dd>
                <p><?php echo $output['member_info']['available_margin']; ?></p>
            </dd>
        </dl>
        <dl>
            <dt><i class="required">*</i>提现金额：</dt>
            <dd>
                <input class="text w200" type="text" name="num" id="num" value="" />
            </dd>
        </dl>
        <div class="bottom">
            <label class="submit-border"><input type="submit" class="submit" value="确认" /></label>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#send_form').validate({
            errorLabelContainer: $('#warning-1'),
            invalidHandler: function(form, validator) {
                $('#warning').show();
            },
            submitHandler:function(form){
                ajaxpost('send_form', '', '', 'onerror')
            },
            rules : {
                num : {
                    required : true,
                    min: 1,
                    max: <?php echo $output['member_info']['available_margin'] ?>,
                    digits:true
                }
            },
            messages : {
                num : {
                    required : '<i class="fa fa-warning"></i>请填写提现金额',
                    min: '<i class="fa fa-warning"></i>最小金额为1元',
                    max: '<i class="fa fa-warning"></i>不能超过最大提现金额',
                    digits:'<i class="fa fa-warning"></i>请填写整数',

                }
            }
        });
    });
</script>
