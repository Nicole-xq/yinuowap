<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo MEMBER_TEMPLATES_URL;?>/css/lm_registed.css" rel="stylesheet" type="text/css">

<div class="nc-register-bg lm_registed qll_register">
  <div class="nc-register-box">
    <div class="nc-register-layout" style="overflow:inherit">
      <div class="left">
        <div class="nc-register-mode">
          <div id="tabs_container" class="tabs-container">
            <div id="default" class="tabs-content">
              <form id="register_form" method="post" class="nc-login-form" action="<?php echo urlLogin('connect_sms', 'register')?>">
                <?php Security::getToken();?>
                <dl>
                  <dt><?php echo $lang['login_register_username'];?>：</dt>
                  <dd>
                    <input type="text" id="user_name" name="user_name" class="text" tipMsg="<?php echo $lang['login_register_username_to_login'];?>"/>
                  </dd>
                </dl>
                <dl>
                  <dt><?php echo $lang['login_register_pwd'];?>：</dt>
                  <dd>
                    <input type="password" id="password" name="password" class="text" tipMsg="<?php echo $lang['login_register_password_to_login'];?>"/>
                  </dd>
                </dl>
                <dl>
                  <dt><?php echo $lang['login_register_ensure_password'];?>：</dt>
                  <dd>
                    <input type="password" id="password_confirm" name="password_confirm" class="text" tipMsg="<?php echo $lang['login_register_input_password_again'];?>"/>
                  </dd>
                </dl>
                <dl class="mt15 lm_email">
                  <dt><?php echo $lang['login_register_email'];?>：</dt>
                  <dd>
                    <input type="text" id="email" name="email" class="text" tipMsg="<?php echo $lang['login_register_input_valid_email'];?>"/>
                  </dd>
                </dl>
                <dl class="lm_phone">
                    <dt>手机号：</dt>
                    <dd>
                        <input type="text" class="text" tipMsg="请输入手机号码" autocomplete="off" value="" name="register_phone" id="phone">
                    </dd>
                </dl>
                <div class="lm_change">
                    完成验证后，你可以用该手机登录和找回密码
                    <div class="lm_spans">
                    <span>邮箱验证</span>
                    <span>手机验证</span>
                    </div>
                </div>

                <dl class="mt15">
                  <dt><?php echo '邀请码';?>：</dt>
                  <dd>
                    <input type="text" id="dis_code" name="dis_code" class="text" tipMsg="请输入推荐人的邀请码"/>
                  </dd>
                </dl>

                <?php if(C('captcha_status_register') == '1') { ?>
                <div class="mt15 code-div">
                  <dl>
                    <dt><?php echo $lang['login_register_code'];?>：</dt>
                    <dd>
                      <input type="text" id="image_captcha"  name="captcha" class="text w80" size="10" tipMsg="<?php echo $lang['login_register_input_code'];?>" />
                    </dd>
                  </dl>
                  <span><img src="index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>" name="codeimage" id="codeimage"/> <a class="makecode" href="javascript:void(0)" onclick="javascript:document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();"><?php echo $lang['login_password_change_code']; ?></a></span></div>
                <?php } ?>

                <div class="clear">
                </div>


                <div class="lm_code_div lm_phone">
                    <dl>
                        <dt>短信验证：</dt>
                        <dd>
                            <input type="text" name="register_captcha" autocomplete="off" tipMsg="请输入验证码" class="text w100" id="sms_captcha" size="15" />
                        </dd>
                    </dl>
                    <span><a href="javascript:void(0);" onclick="get_sms_captcha('1')">发送短信验证</a></span>
                </div>

                <dl class="clause-div mt15">
                  <dd>
                    <input name="agree" type="checkbox" class="checkbox" id="clause" value="1" checked="checked" />
                    <?php echo $lang['login_register_agreed'];?><a href="<?php echo urlShop('document', 'index',array('code'=>'agreement'));?>" target="_blank" class="agreement" title="<?php echo $lang['login_register_agreed'];?>"><?php echo $lang['login_register_agreement'];?></a></dd>
                </dl>


                <div class="submit-div">
                  <input type="submit" id="Submit" value="<?php echo $lang['login_register_regist_now'];?>" class="submit"/>
                </div>
                <input type="hidden" value="<?php echo urlLogin('login','welcome');?>" name="ref_url">
                <input name="nchash" type="hidden" value="<?php echo getNchash();?>" />
                <input type="hidden" name="form_submit" value="ok" />
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="right">
        <?php if (C('qq_isuse') == 1 || C('sina_isuse') == 1 || C('weixin_isuse') == 1){?>
        <div class="api-login">
          <h4>使用合作网站账号直接登录</h4>
          <?php if (C('qq_isuse') == 1){?>
          <a class="lm_qq" href="<?php echo MEMBER_SITE_URL;?>/index.php?act=connect_qq" title="QQ账号登录" class="qq"><i></i></a>
          <?php } ?>
          <?php if (C('sina_isuse') == 1){?>
          <a class="lm_sina" href="<?php echo MEMBER_SITE_URL;?>/index.php?act=connect_sina" title="<?php echo $lang['nc_otherlogintip_sina']; ?>" class="sina"><i></i></a>
          <?php } ?>
          <?php if (C('weixin_isuse') == 1){?>
          <a class="lm_wx" href="javascript:void(0);" onclick="ajax_form('weixin_form', '微信账号登录', '<?php echo urlLogin('connect_wx', 'index');?>', 360);" title="微信账号登录" class="wx"><i></i></a>
          <?php } ?>
        </div>
        <?php } ?>

      </div>
    </div>
  </div>
</div>
<script>
$(function(){
	jQuery.validator.addMethod("letters_name", function(value, element) {
		return this.optional(element) || (/^[A-Za-z0-9\u4e00-\u9fa5_-]+$/i.test(value) && !/^\d+$/.test(value));
	}, "Letters only please");
	//初始化Input的灰色提示信息
	$('input[tipMsg]').inputTipText({pwd:'password,password_confirm'});
	//注册方式切换
	$('.nc-register-mode').tabulous({
		 //动画缩放渐变效果effect: 'scale'
		 effect: 'slideLeft'//动画左侧滑入效果
		//动画下方滑入效果 effect: 'scaleUp'
		//动画反转效果 effect: 'flip'
	});
	var div_form = '#default';
	$(".nc-register-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
    	}
	});

    function email_check(){
        if($('.lm_registed').find('.lm_change').hasClass('active')){
            return true;
        }
        return false;
    }

    $.extend($.validator.defaults,{ignore:":hidden"});


  $.validator.addMethod('name_length', function(value,element,param){
    name_length = value.replace(/[^\u0000-\u00ff]/g,"aa").length;
    if(name_length >= param[0] && name_length <= param[1]){
        return true;
    }else{
        return false;
    }
  }, '');

	var _register_member = 0;
    //注册表单验证
    $("#register_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form){
    	    if (_register_member) return false;
    	    _register_member = 1;
    	    ajaxpost('register_form', '', '', 'onerror');
    	},
        onkeyup: false,
        rules : {
            user_name : {
                required : true,
                //rangelength : [6,20],
                name_length : [4,20],
                letters_name : true,
                remote   : {
                    url :'index.php?act=login&op=check_member&column=ok',
                    type:'get',
                    data:{
                        user_name : function(){
                            return $('#user_name').val();
                        }
                    }
                }
            },
            password : {
                required : true,
                minlength: 6,
				        maxlength: 20
            },
            password_confirm : {
                required : true,
                equalTo  : '#password'
            },
            sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 4
            },
            phone: {
                required: true,
                mobile: true
            },
            email : {
                required : function(){
                    if(!email_check()){
                        return false;
                    }
                },
                email : function(){
                    if(!email_check()){
                        return false;
                    }
                },
                remote   : {
                    url : 'index.php?act=login&op=check_email',
                    type: 'get',
                    data:{
                        email : function(){
                            return $('#email').val();
                        }
                    }
                }
            },
            dis_code : {
                remote : {
                    url : 'index.php?act=login&op=check_dcode',
                    type: 'get',
                    data: {
                      dcode : function (){
                        return $('#dis_code').val() == $('#dis_code').attr('tipMsg')?'':$('#dis_code').val();
                      }
                    }
                }
            },
			<?php if(C('captcha_status_register') == '1') { ?>
            captcha : {
                required : true,
                remote   : {
                    url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                    type: 'get',
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	document.getElementById('codeimage').src='index.php?act=seccode&op=makecode&type=50,120&nchash=<?php echo getNchash();?>&t=' + Math.random();
                        }
                    }
                }
            },
			<?php } ?>
            agree : {
                required : true
            }
        },
        messages : {
            phone: {
                required: "手机号不能为空",
                mobile: "请输入正确的手机号"
            },
            user_name : {
                required : '<?php echo $lang['login_register_input_username'];?>',
                name_length : '<?php echo $lang['login_register_username_range'];?>',
        				letters_name: '<?php echo $lang['login_register_username_lettersonly'];?>',
        				remote	 : '<?php echo $lang['login_register_username_exists'];?>'
            },
            password  : {
                required : '<?php echo $lang['login_register_input_password'];?>',
                minlength: '<?php echo $lang['login_register_password_range'];?>',
				        maxlength: '<?php echo $lang['login_register_password_range'];?>'
            },
            password_confirm : {
                required : '<?php echo $lang['login_register_input_password_again'];?>',
                equalTo  : '<?php echo $lang['login_register_password_not_same'];?>'
            },
            email : {
                required : '<?php echo $lang['login_register_input_email'];?>',
                email    : '<?php echo $lang['login_register_invalid_email'];?>',
                remote	 : '<?php echo $lang['login_register_email_exists'];?>'
            },
            dis_code : {
                remote   : '邀请码不存在'
            },
			<?php if(C('captcha_status_register') == '1') { ?>
            captcha : {
                required : '<i class="icon-remove-circle" title="<?php echo $lang['login_register_input_text_in_image'];?>"></i>',
				        remote	 : '<i class="icon-remove-circle" title="<?php echo $lang['login_register_code_wrong'];?>"></i>'
            },
			<?php } ?>
            agree : {
                required : '<?php echo $lang['login_register_must_agree'];?>'
            },
            sms_captcha: {
                required : '请输入四位短信动态码',
                minlength: '请输入四位短信动态码'
            }
        }
    });
});
</script>
<?php if (C('sms_register') == 1){?>
<script type="text/javascript" src="<?php echo LOGIN_RESOURCE_SITE_URL;?>/js/connect_sms.js" charset="utf-8"></script>
<script>
    $(function() {
        var $dom = $('.lm_registed');
        $dom.find('.lm_change').on('click', function() {
            console.log($dom.find('.lm_change').hasClass('active'))
            if ($dom.find('.lm_change').hasClass('active')) {
                $dom.find('.lm_change').removeClass('active');
                $dom.find('.lm_email').hide();
                $dom.find('.lm_phone').show();
                $('#register_form').attr('action', "<?php echo urlLogin('connect_sms', 'register');?>");
            } else {
                $dom.find('.lm_change').addClass('active');
                $dom.find('.lm_phone').hide();
                $dom.find('.lm_email').show();
                $('#register_form').attr('action', "<?php echo urlLogin('login', 'usersave');?>");
            }
        })
    })
</script>
<?php } ?>