<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .qll_order_list .qll_goods_detail{border-bottom: solid 1px #f2f2f2;}
    .qll_order_list h3{height: 50px;line-height: 50px;padding: 0 30px;border-bottom: 1px solid #f2f2f2}
    .qll_order_list .qll_order_detail dl{display: flex;justify-content: space-between;}
    .qll_order_list .qll_order_detail{padding: 10px 30px;border-bottom: dashed 1px #f2f2f2;height: 30px;line-height: 30px;}
    .qll_order_list .qll_order_detail span{display: inline-block;margin-left: 30px;}
    .qll_order_list .qll_order_detail .qll_order_detail_count{margin-right: 60px;}
    .qll_order_list .qll_goods_detail{padding: 10px 30px;}
    .qll_order_list .qll_goods_detail dl{display: flex;justify-content: space-between}
    .qll_order_list .qll_goods_detail span{display: block;padding: 10px 0;}
    .qll_order_list .qll_goods_detail .qll_goods_detail_left{text-align: right;}
    .qll_commission_detail{padding: 30px 30px;border-bottom: solid 1px #f2f2f2;}
    .qll_commission_detail dl{display: flex;justify-content: space-between;}
    .other_list .qll_other_list_detail dl{display: flex;justify-content: space-between;}
    .other_list .qll_other_list_detail span{display: inline-block;margin-left: 30px;}
    .other_list .qll_other_list_detail .qll_order_detail_count{margin-right: 60px;}
    .other_list .qll_other_list_detail{padding: 20px 30px;border-bottom: solid 1px #f2f2f2;height: 30px;line-height: 30px;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<!--添加新的个人中心首页-->
<div class="account-center">
    <script type="text/javascript">
        $(document).ready(function(){
            var $tab_li = $('.new-tab ul li');
            $tab_li.hover(function(){
                $(this).addClass('selected').siblings().removeClass('selected');
                var index = $tab_li.index(this);
                $('div.tab_box > div').eq(index).show().siblings().hide();
            });
        });
        $(function(){$('.account-record tr:odd').css('background','#fff'); $('.account-record tr:even').css('background','#fafafa');});
    </script>
    <li>
        <img src="<?php echo getMemberAvatar($output['partner']['member_avatar']);?>" width="48" height="48">
        <div class="fl">
            <span class="all-block">伙伴名称：<?php echo $output['partner']['member_name'];?></span>
<!--          <span class="all-block">备注：--><?php //echo $output['partner']['member_comment'];?><!--<i id="edit_comment"><img style="    width: 13px;display: inline-block;margin-left: 7px;vertical-align: middle;cursor: pointer;" src="--><?php //echo RESOURCE_SITE_URL;?><!--/image/lm/edit.png"></i></span>-->
            <span class="all-block">会员等级：<?php echo $output['partner']['member_type_name'];?></span>
            <span class="all-block">佣金金额：<?php echo $output['sum_commission_amount'];?></span>
        </div>
        <div class="clear"></div>
    </li>
    <div class="new-tab">
        <ul class="tab_menu">
            <li class="selected">交易返佣</li>
            <li>保证金收益返佣</li>
            <li>推荐返佣</li>
        </ul>
        <div class="tab_box" >
            <div class="qll_order_list" id="order_list" >

            </div>

            <div class="qll_commission_list qll_order_list hide" id="commission_list" >

            </div>

            <div class="other_list qll_order_list hide" id="other_list" >

            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
  //添加输入框
(function($) {
    $.lm_dialog_inp = function(options) {
        var settings = $.extend({}, {action:'', nchash:'', formhash:'' ,anchor:''}, options);
        var login_dialog_html = $('<div class="quick-login"></div>');
        var ref_url = document.location.href;

        login_dialog_html.append('<form class="bg" method="post" id="ajax_lm_inp" action=""></form>').find('form')
            .append('<dl><dt>用户名</dt><dd><input type="text" name="comment" id="comment" autocomplete="off" class="text"></dd><dd><input type="hidden" id="member_id" name="member_id" value="<?php echo $output['partner']['member_id'];?>"></dd></dl>')
            .append('<div class="enter"><input type="submit" name="Submit" value="登&#12288;&#12288;录" class="submit"></div><input type="hidden" name="ref_url" value="'+ref_url+'">');

        login_dialog_html.find('input[type="submit"]').click(function(){
            $.post(MEMBER_SITE_URL+"/index.php?act=member_partner&op=update_member_comment",{'comment':$('#comment').val(),'member_id':$('#member_id').val()},function(re){
//                if(re == 1){
//                    console.log(re);
                if(re){

                    window.location.reload();
                }
//                }else{
//                    console.log(re);
//                    alert('失败');
//                }
            });
//            $('#ajax_lm_inp').submit();
//            window.location.reload();
            return true;
        });
        html_form("lm_dialog_inp", "备注", login_dialog_html, 360);
    };
    $.fn.nc_login = function(options) {
        return this.each(function() {
            $(this).on('click',function(){
                $.lm_dialog_inp(options);
                return false;
            });
        });
    };

})(jQuery);
$(function(){
    $('#edit_comment').click(function(){

        $.lm_dialog_inp();
    });
});
    function get_order(){
        $.ajax({
            type:"GET",
            url: MEMBER_SITE_URL+"/index.php?act=member_partner&op=partner_order_info&partner_id="+<?php echo $output['partner']['member_id'];?>,
            dataType: "html",
            success: function(data){
                $('#order_list').html(data);
            }
        });
    }
    function get_commission(){
        $.ajax({
            type:"GET",
            url: MEMBER_SITE_URL+"/index.php?act=member_partner&op=partner_commission_info&partner_id="+<?php echo $output['partner']['member_id'];?>,
            dataType: "html",
            success: function(data){
                $('#commission_list').html(data);
            }
        });
    }
    function get_other() {
        $.ajax({
            type: "GET",
            url: MEMBER_SITE_URL + "/index.php?act=member_partner&op=partner_info&partner_id=" +<?php echo $output['partner']['member_id'];?>,
            dataType: "html",
            success: function (data) {
                $('#other_list').html(data);
            }
        });
    }

    $(function(){
        get_order();
        get_commission();
        get_other();
        <?php if($_GET['list_location'] == 'order_list'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:first-child').addClass('selected');
        $('#order_list').show();
        $('#commission_list').hide();
        $('#other_list').hide();
        <?php endif;?>
        <?php if($_GET['list_location'] == 'baozhengjin'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:nth-child(2)').addClass('selected');
        $('#order_list').hide();
        $('#commission_list').show();
        $('#other_list').hide();
        <?php endif;?>
        <?php if($_GET['list_location'] == 'commission'):?>
        $('.tab_menu li').removeClass('selected');
        $('.tab_menu li:last-child').addClass('selected');
        $('#order_list').hide();
        $('#commission_list').hide();
        $('#other_list').show();
        <?php endif;?>
    });
</script>