<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#baozhengjin_list').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#baozhengjin_list'
    });
});
</script>
<table width="1025" border="0" cellspacing="0" cellpadding="0" class="account-record">
            <tr style="height: 36px">
              <td class="w10"></th>
              <td class="w150 tc">创建时间</td>
              <td class="w150 tc">余额变更</td>
              <td class="w150 tc">冻结余额变更</td>
              <td class="tc">描述</td>
            </tr>
            <?php  if (count($output['list'])>0) { ?>
            <?php foreach($output['list'] as $v) { ?>
            <tr class="bd-line">
              <td></td>
              <td class="goods-time tc"><?php echo @date('Y-m-d H:i:s',$v['add_time']);?></td>
              <td class="tc"><?php echo $v['available_amount']; ?></td>
              <td class="tc"><?php echo $v['freeze_amount']; ?></td>
              <td class="tc"><?php echo $v['description']; ?></td>
            </tr>
            <?php } ?>
            <?php } else {?>
            <tr>
              <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
            <?php } ?>
          </table>
          <?php  if (count($output['list'])>0): ?>
            <div class="pagination"> <?php echo $output['show_page']; ?></div></td>
          <?php endif; ?>
