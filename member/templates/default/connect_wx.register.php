<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .lm_code_div{

    }
    .lm_code_div dl {
        width: 228px !important;
        float: left;
    }
    .lm_code_div span{
        width: 120px;
        height: 52px;
        float: left;
        margin: -1px 0 0 -1px;
        border: solid 1px #E6E6E6;
        position: relative;
        z-index: 1;
        height: 52px;
        border-left: none;
        text-align: center;
        background: #fff;
        font-size: 14px;
        color: red;
        margin: 0px 0 0 -1px;
        border-top:0;
    }
    .lm_code_div span a {
        background: #f2f2f2;
        display: block;
        height: 22px;
        width: 80%;
        margin: 15px auto;
        color: #777777;
        text-align: center;
        font-size: 14px;
        line-height: 22px;
    }
</style>
<div class="nc-login-layout">
  <div class="openid">
      <span class="avatar"><img src="<?php echo $output['headimgurl'];?>" /></span>
      <span>您使用微信账号<a href="#register_form"><?php echo $output['user_info']['nickname']; ?></a>已经授权成功，<br/>请补全用户名、密码、手机号，方便您再次登录。</span>
  </div>
  <div class="left-pic"><img src="<?php echo MEMBER_TEMPLATES_URL;?>/images/login_openid.jpg" /> </div>
  <div class="nc-login">
    <div class="arrow"></div>
    <div class="nc-wx-mode">
      <ul class="tabs-nav">
        <li><a href="#register">完善账号信息<i></i></a></li>
      </ul>
      <div id="tabs_container" class="tabs-container">
        <div id="register" class="tabs-content">
          <form name="register_form" id="register_form" class="nc-login-form" method="post" action="index.php?act=connect_wx&op=edit_info">
            <input type="hidden" value="ok" name="form_submit">
<!--            <dl>-->
<!--              <dt>--><?php //echo $lang['login_register_username']; ?><!--：</dt>-->
<!--              <dd>-->
<!--                <input type="text" value="" id="user" name="user" class="text" />-->
<!--              </dd>-->
<!--            </dl>-->
            <dl>
              <dt><?php echo $lang['login_register_pwd']; ?>：</dt>
              <dd>
                <input type="text" value="<?php echo $output['password'];?>" id="password" name="password" class="text" tipMsg="<?php echo $lang['login_register_password_to_login'];?>"/>
              </dd>
            </dl>
            <dl class="mt15">
              <dt>手机号：</dt>
              <dd>
                <input type="text" id="mobile" name="mobile" class="text" />
              </dd>
            </dl>

              <div class="lm_code_div lm_phone">
                  <dl>
                      <dt>短信验证：</dt>
                      <dd>
                          <input type="text" name="sms_captcha" autocomplete="off" placeholder="请输入验证码" tipMsg="" class="text w100" id="sms_captcha" size="15" />
                      </dd>
                  </dl>
                  <span><a href="javascript:void(0);" onclick="get_sms_captcha('1')">发送短信验证</a></span>
              </div>

            <div class="submit-div">
              <input type="submit" name="submit" value="<?php echo $lang['login_register_enter_now'];?>" class="submit"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    function get_sms_captcha(type){
        if($("#mobile").val().length == 11){
            var ajaxurl = 'index.php?act=connect_sms&op=get_captcha&wx_bind_mobile=1&type='+type;
            ajaxurl += '&phone='+$('#mobile').val();
            $.ajax({
                type: "GET",
                url: ajaxurl,
                async: false,
                success: function(rs){
                    if(rs == 'true') {
                        showError('短信验证码已发出');
                    } else {
                        showError(rs);
                    }
                }
            });
        }
    }
$(function(){
	//初始化Input的灰色提示信息  
	$('input[tipMsg]').inputTipText();
	//登录方式切换
	$('.nc-wx-mode').tabulous({
		 effect: 'flip'//动画反转效果
	});
    //注册表单验证
        $('#register_form').validate({
            errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        submitHandler:function(form){
            if (_register_member) return false;
            _register_member = 1;
            ajaxpost('register_form', '', '', 'onerror');
        },
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: 'index.php?act=login&op=check_email',
                        type: 'get',
                        data: {
                            email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                },
                mobile: {
                    required: true,
                    mobile: true
                },
                sms_captcha: {
                    required : true,
                    minlength: 4
                }
        },
        messages : {
            password  : {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_input_password'];?>',
                minlength: '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_password_range'];?>',
				maxlength: '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_password_range'];?>'
            },
            email : {
                required : '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_input_email'];?>',
                email    : '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_invalid_email'];?>',
				remote	 : '<i class="icon-exclamation-sign"></i><?php echo $lang['login_register_email_exists'];?>'
            },
            mobile: {
                required: "手机号不能为空",
                mobile: "请输入正确的手机号"
            },
            sms_captcha: {
                required : '请输入四位短信动态码',
                minlength: '请输入四位短信动态码'
            }
        }
    });
});
</script>