<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="wrap">
    <div class="tabmenu">
        <?php include template('layout/submenu');?>
    </div>
        <div class="ncm-default-form">
            <form method="post" id="alipay_form" action="<?php echo MEMBER_SITE_URL;?>/index.php?act=member_alipay_bind&op=alipay_edit">
                <input type="hidden" name="form_submit" value="ok" />
                <input type="hidden" name="member_id" id="member_id" value="<?php echo $output['member_info']['member_id'];?>" />
                <dl>
                    <dt><i class="required">*</i>绑定支付宝账户：</dt>
                    <dd>
                        <input type="text" class="text w180"  maxlength="40" value="<?php echo $output['member_info']['alipay_account'];?>" name="alipay_account" id="alipay_account" />
                        <span></span>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="required">*</i>绑定支付宝名称：</dt>
                    <dd>
                        <input type="text" class="text w180"  maxlength="40" value="<?php echo $output['member_info']['alipay_name'];?>" name="alipay_name" id="alipay_name" />
                        <span></span>
                    </dd>
                </dl>
                <dl class="bottom">
                    <dt>&nbsp;</dt>
                    <dd><label class="submit-border">
                            <input type="submit" class="submit" id="submitBtn" value="立即绑定" /></label>
                    </dd>
                </dl>
            </form>
        </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(){
            if($("#alipay_form").valid()){
                $("#alipay_form").submit();
            }
        });
        $('#alipay_form').validate({
            submitHandler:function(form){
                ajaxpost('alipay_form', '', '', 'onerror')
            },
            rules : {
                alipay_account : {
                    required   : true
                },
                alipay_name : {
                    required   : true
                }
            },
            messages : {
                alipay_account : {
                    required : '<i class="icon-exclamation-sign"></i>请填写支付宝账户'
                },
                alipay_name : {
                    required : '<i class="icon-exclamation-sign"></i>请填写支付宝名称'
                }
            }
        });
    });
</script>
