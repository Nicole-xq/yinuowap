<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#points_list').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#points_list'
    });
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="wrap">
  <table class="ncm-default-table">
    <thead>
      <tr style="height: 36px;text-align:center">
        <td class="w200"><?php echo $lang['points_addtime']; ?></td>
        <td class="w150"><?php echo $lang['points_pointsnum']; ?></td>
        <td class="w300"><?php echo $lang['points_stage']; ?></td>
        <td class="tl"><?php echo $lang['points_pointsdesc']; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php  if (count($output['list_log'])>0) { ?>
      <?php foreach($output['list_log'] as $val) { ?>
      <tr class="bd-line">
        <td class="goods-time"><?php echo @date('Y-m-d',$val['pl_addtime']);?></td>
        <td class="goods-price"><?php echo ($val['pl_points'] > 0 ? '+' : '').$val['pl_points']; ?></td>
        <td><?php 
	              	switch ($val['pl_stage']){
	              		case 'regist':
	              			echo $lang['points_stage_regist'];
	              			break;
	              		case 'login':
	              			echo $lang['points_stage_login'];
	              			break;
	              		case 'comments':
	              			echo $lang['points_stage_comments'];
	              			break;
	              		case 'order':
	              			echo $lang['points_stage_order'];
	              			break;
	              		case 'system':
	              			echo $lang['points_stage_system'];
	              			break;
	              		case 'pointorder':
	              			echo $lang['points_stage_pointorder'];
	              			break;
                    case 'nuobiorder':
                      echo '诺币消耗抵现';
                      break;
	              		case 'app':
	              			echo $lang['points_stage_app'];
	              			break;
                    case 'guess':
                      echo '趣猜赠送';
                      break;
	              	}
	              ?></td>
        <td class="tl"><?php echo $val['pl_desc'];?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record']; ?></span></div></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php  if (count($output['list_log'])>0) { ?>
      <tr>
        <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script> 
