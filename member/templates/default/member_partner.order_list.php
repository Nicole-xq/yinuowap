<?php defined('InShopNC') or exit('Access Invalid!');?>


<h3 class="qll_order_list_title">交易返佣总收益：￥<?php echo ncPriceFormat($output['sum_commission_amount']); ?></h3>
<?php foreach($output['commission_list'] as $row): ?>
<ul>
    <li class="qll_order_detail">
        <dl>
            <dt>商品名称</dt>
            <dd>
                <span class="qll_order_detail_count">+<?php echo ncPriceFormat($row['commission_amount']); ?></span>
                <span><?php echo $row['label_date']; ?></span>
                <span><?php echo $row['label_status']; ?></span>
            </dd>
        </dl>
    </li>
    <li class="qll_goods_detail">
        <dl>
            <dt>
                <span>订单编号：<?php echo $output['orders_list'][$row['order_id']]['order_sn']; ?></span>
                <?php foreach($output['orders_list'][$row['order_id']]['goods_info'] as $good_row): ?>
                <span><?php echo  $good_row['goods_name'];?> <?php echo  $good_row['goods_num'];?>件</span>
                <?php endforeach; ?>
            </dt>
            <dd>
                <span class="qll_goods_detail_left">总金额：<?php echo ncPriceFormat($output['orders_list'][$row['order_id']]['order_amount']); ?></span>
                <span class="qll_goods_detail_left">时间：<?php echo $output['orders_list'][$row['order_id']]['label_date']; ?></span>
                <span class="qll_goods_detail_left">订单状态：<?php echo $output['orders_list'][$row['order_id']]['label_status']; ?></span>
            </dd>
        </dl>
    </li>
</ul>
<?php endforeach; ?>
