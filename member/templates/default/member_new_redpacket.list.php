<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="account-center">
<?php include template('member_account_common'); ?>
<div class="box invitation">
		<div class="tabmenu" style="padding:0 2%;width: 96%">
			<ul class="tab pngFix" style="background:#fcfcfc;">
				<li class="active"><a href="javascript:void(0);">我的红包</a></li>
				<li class="normal"><a href="<?php echo urlMember('member_redpacket','rp_binding');?>">领取红包</a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<ul class="mine">
			<?php foreach ($output['list'] as $key => $value): ?>
			<li>
        <a href="<?=urlShop('show_store','index',array('store_id'=>C('store.zq_id')));?>">

          <div class="fl">
            <div><h1><?php echo $value['rpacket_title'];?></h1></div>
            <div class="clear"></div>
            <p><?php echo $value['rpacket_desc'];?></p>
            <p>有效期到:<?php echo date('Y.m.d',$value['rpacket_end_date']);?></p>
          </div>
          <div class="fr youhuiquan" style="background:#FD5856;">
            <div><b style="font-size: 24px"><?php echo $value['rpacket_price'];?></b>&nbsp;¥</div>
            <span>满<?php echo $value['rpacket_limit'];?>使用</span>
            <span>红包</span>
          </div>
        </a>
			</li>

		<?php endforeach;?>
		</ul>
		<div class="clear"></div>
		<div class="pagination"><?php echo $output['show_page'];?></div>
	</div>
	</div>