<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="account-center">
	<div class="box invitation">
		<div class="tabmenu" style="padding:0 2%;width: 96%">
			<ul class="tab pngFix" style="background:#fcfcfc;">
			  <li class="normal"><a href="index.php?act=member_invitecode&op=index">我的邀请码</a></li>
				<li class="active"><a href="javascript:void(0);">绑定邀请码</a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<div style="width:300px;padding:40px 140px;margin:40px auto 20px">
			<?php if(intval($output['dcode_info']['top_member']) > 0){?>
				<h2>您的上级会员是：<br /><?php echo strtoupper($output['dcode_info']['top_member_name']);?></h2>
			<?php }else{?>
			<form method="post" id="invitecode_form" action="<?php echo MEMBER_SITE_URL;?>/index.php?act=member_invitecode&op=bind_code">
				<input type="hidden" name="form_submit" value="ok" />
				<input type="hidden" name="act" value="member_invitecode" />
				<input type="hidden" name="op" value="bind_code" />
				<label>
					邀请码:&nbsp;&nbsp;
					<input type="text" name="invite_code" placeholder="" autocomplete="off" maxlength="22" style="margin-top:-4px;width:200px">
				</label>
				<span style="padding-left:50px">提示：一旦绑定将无法变更请认真检查。</span>
				<div class="clear"></div>
				<input type="submit" class="btn" value="确定" style="margin:20px auto 0;width:80px">
			</form>
			<?php }?>			
		</div>
		<div class="clear"></div>
	</div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		var _subState = 0;
		$('#invitecode_form').validate({
	    	submitHandler:function(form){
	    		if (_subState) showError('正在处理中，请勿重复点击！');
	    	    _subState = 1;
				ajaxpost('invitecode_form', '', '', 'onerror');
	    	},
	        rules : {
	            invite_code : {
	                required : true,
	                remote : {
	                		url : "index.php?act=member_invitecode&op=check_dcode",
	                		type : "get",
	                		data : {
	                			dcode : function (){
	                				return $('input[name="invite_code"]').val();
	                			}
	                		}
	                }

	            }
	        },
	        messages : {
	            invite_code : {
	                required : '邀请码不能为空',
	                remote : '邀请码不存在'
	            }
	        }
    	});
	});
</script>