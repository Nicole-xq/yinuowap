<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
  label.error {
    width: auto !important;
    display: inline-block !important;
    margin: 0 !important;
  }

  .ncm-container .right-layout .account-center .invitation h2 {
    width: 100%;
    text-align: center;
    font-size: 16px;
    margin: 110px 0 20px;
  }

  .ncm-container .right-layout .account-center .invitation h2 ~ img {
    display: block;
    margin: 0 auto 80px;
  }
  .ncm-container .right-layout{
    margin-left: -3px;
  }
#btn_target{
    margin: 0 auto;
    width: 105px;
    display: block;
  }

</style>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/qrcode.js"></script>
<div class="account-center">
	<div class="box invitation">
		<div class="tabmenu" style="padding:0 2%;width: 96%">
			<ul class="tab pngFix" style="background:#fcfcfc;">
				<li class="active"><a href="javascript:void(0);">我的邀请码</a></li>
				<!-- <li class="normal"><a href="index.php?act=member_invitecode&op=bind_code">绑定邀请码</a></li> -->
			</ul>
		</div>
		<div class="clear"></div>
		<?php if($output['dcode_info']['dis_code'] != ''){?>
			<h2>您的邀请码是：<br /><?php echo strtoupper($output['dcode_info']['dis_code']);?></h2>
      <div id="btn_target"><button id="get_target" >获取分享链接</button></div>
      <textarea id="contents" cols="1" rows="1" style="opacity: 0;width:1px;height: 1px;"><?php echo $output['code_url']; ?></textarea>
      <div id="dialog-confirm"  style="display: none">
        <p>
         <span class="ui-icon" style="float: left; margin: 0 7px 20px 0;"></span>
         链接已经被复制
        </p>
      </div>


		<?php }else{?>
			<?php if($_GET['get_code'] == 'ok'){?>
			<form method="post" id="invitecode_form" action="<?php echo MEMBER_SITE_URL;?>/index.php?act=member_invitecode">
				<input type="hidden" name="form_submit" value="ok" />
				<input type="hidden" name="act" value="member_invitecode" />
				<input type="hidden" name="op" value="get_code" />
				<div style="width:300px;padding:40px 140px;background:#fff;border:#ccc solid 1px;margin:40px auto 20px">
					<h3>获取邀请码</h3>
					<label for="">手机号:&nbsp;&nbsp;
						<input type="text" name="member_mobile" placeholder="" autocomplete="off" maxlength="11" style="margin-top:-4px;width:200px">
					</label>
					<label for="">
						<span class="fl">验证码:&nbsp;&nbsp;
							<input type="text" name="phone_code" placeholder="" autocomplete="off" maxlength="6" style="margin-top: -4px;width:50px">
						</span>
						<input type="button" id="get_code" class="btn fl" value="获取验证码" style="margin: -4px 0 0 20px">
					</label>
					<div class="clear"></div>
					<input type="submit" class="btn" value="确定" style="margin:20px auto 0;width:80px">				
				</div>
			</form>
			<?php }else{?>
			<h2>您现在还没有邀请码！<br /><a href="index.php?act=member_invitecode&op=mine_code&get_code=ok">马上获取</a>吧</h2>
			<?php }?>
		<?php }?>
		<div class="clear"></div>
	</div>
</div>
<?php if($output['code_url']){?>
<script type="text/javascript">
  function create_qrcode(text){
      var qr =  qrcode(15,'H');
      qr.addData(text);
      qr.make();
      return qr.createImgTag(2,1);
  }

  var qr_img = create_qrcode('<?php echo $output['code_url']; ?>');
  $('.account-center h2').after(qr_img);
  $("#dialog-confirm").dialog(
     {
         autoOpen: false
     }
   );
  $('#get_target').click(function () {
        document.getElementById('contents').select();
        document.execCommand("Copy");
        showDialog('已成功复制到剪贴板', 'succ', '系统提示', '', 5);
    });

</script>
<?php }?>





