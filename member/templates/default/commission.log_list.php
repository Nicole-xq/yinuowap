<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#commission_list').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#commission_list'
    });
});
</script>
<table width="1025" border="0" cellspacing="0" cellpadding="0" class="account-record">
            <tr style="height: 36px">
              <td class="w10"></th>
              <td class="w150 tc">订单编号</td>
<!--              <td class="w150 tc">返佣来源</td>-->
              <td class="w150 tc">来源会员</td>
              <td class="tc">商品名称</td>
              <td class="tc">返佣金额</td>
              <td class="tc">返佣状态</td>
            </tr>
            <?php  if (count($output['list'])>0) { ?>
            <?php foreach($output['list'] as $v) { ?>
            <tr class="bd-line">
              <td></td>
              <td class="tc"><?=$v['order_sn']; ?></td>
<!--              <td class="tc">--><?//= ?><!--</td>-->
              <td class="tc"><?=$v['from_member_name']; ?></td>
              <td class="tc"><?=$v['goods_name']; ?></td>
              <td class="tc"><?=$v['commission_amount']; ?></td>
              <td class="tc"><?=$v['dis_commis_state'] ==0?'未结算':'已结算,可提现' ?></td>
            </tr>
            <?php } ?>
            <?php } else {?>
            <tr>
              <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
            <?php } ?>
          </table>
          <?php  if (count($output['list'])>0): ?>
            <div class="pagination"> <?php echo $output['show_page']; ?></div></td>
          <?php endif; ?>
