  <div class="top-box">
    <div class="fl">
      <div class="box-01">
        <div class="price fl">
          <span class="all-block">账户金额</span>
          <?php $predeposit = explode('.',(string)$output['member_info']['available_predeposit']);?>
          <b class="all-block"><?php echo $predeposit[0],'.<span style="font-size:14px;font-weight:bold;color:#666">'.$predeposit[1].'</span>';?>&nbsp;<font style="font-size: 14px;font-weight:normal">元</font></b>
        </div>
        <a href="<?php echo urlMember('predeposit','recharge_add');?>" class="botton fl">充值</a>
        <a href="<?php echo urlMember('predeposit','pd_cash_list');?>" class="botton fl">提现</a>
        <div class="clear"></div>
        <a href="<?php echo urlMember('member','account',array('list_location'=>'predeposit'));?>" class="more fr" style="bottom:-2px;">查看</a>
      </div>
      <div class="box-02 fl">
        <div class="price fl">
          <span class="all-block">待收佣金</span>
          <?php $freeze_commis = explode('.',ncPriceFormat($output['member_info']['freeze_commis']));?>
          <b class="all-block"><?php echo $freeze_commis[0].'.<span style="font-size:14px;font-weight:bold;color:#666">'.$freeze_commis[1].'</span>';?>&nbsp;<font style="font-size: 14px">元</font></b>
			<span class="all-block">佣金总计:&nbsp;<?php echo ncPriceFormat($output['member_info']['available_commis']);?>&nbsp;<font style="font-size:14px;font-weight:normal">元</font></span>
        </div>
        <div class="clear"></div>
      </div>
      <div class="box-02 fl">
        <div class="price fl" style="width: 92%">
          <span class="all-block">
              保证金
              <a href="<?php echo urlMember('member','account',array('list_location'=>'baozhengjin'));?>" class="more fr" style="right:0;transform: translateY(65px);;font-size: 12px">查看</a>
               <a href="javascript:void(0)"
                  class="fr"
                  nc_type="dialog"
                  dialog_title="提现到预存款"
                  dialog_id="auction_send"
                  dialog_width="480"
                  uri="<?php echo urlMember('member', 'margin_to_pd');?>"
                  title="提现到预存款" style="transform:translateX(23px)">
                  提现
               </a>
          </span>
          <?php $avaiable_margin =  explode('.', $output['member_info']['available_margin'] + $output['member_info']['freeze_margin']);?>
          <b class="all-block"><?php echo $avaiable_margin[0].'.<span style="font-size:14px;font-weight:bold;color:#666">'.($avaiable_margin[1] ? $avaiable_margin[1] : '00').'</span>';?>&nbsp;<font style="font-size:14px;font-weight:normal">元</font></b>
          <span class="all-block">冻结:&nbsp;<?php echo $output['member_info']['freeze_margin'];?>&nbsp;<font style="font-size: 14px">元</font></span>
        </div>
        <div class="clear"></div>

      </div>
    </div>
    <div class="fl">
      <div class="box-03">
        <div class="price fl">
			<span class="all-block"><i class="nuobi"></i>&nbsp;<font style="font-size: 14px">诺币</font></span>
          <b class="all-block"><?php echo $output['member_info']['member_points']; ?>&nbsp;<font style="font-size:12px;font-weight:normal">诺币</b>
          <a href="<?php echo urlMember('member_points','index');?>">了解更多&nbsp;&gt;</a>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlShop('pointshop','index');?>" class="more fr" style="bottom:18px;">兑奖</a>
        <a href="<?php echo urlMember('member','account',array('list_location'=>'points'));?>" class="more fr" style="bottom:18px;right:20px">查看</a>
      </div>
      <div class="box-03">
        <div class="price fl">
          <span class="all-block"><i class="daijinquan"></i>&nbsp;我的代金券</span>
          <h1 style="margin-top: 16px;">您的可用优惠券共:&nbsp;<b><?php echo $output['member_info']['voucher_counts'];?>&nbsp;</b>张</h1>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlMember('member','voucher');?>" class="more fr" style="bottom:-10px;">查看</a>
      </div>
    </div>
    <div class="fl">
      <div class="box-03">
        <div class="price fl">
          <span class="all-block"><i class="hongbao"></i>&nbsp;我的红包</span>
          <h1 style="margin-top: 16px;font-size: 14px">您的可用红包共:&nbsp;<b><?php echo $output['member_info']['rpt_counts'];?>&nbsp;</b>个</h1>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlMember('member','redpacket');?>" class="more fr" style="bottom:-10px;">查看</a>
      </div>
      <div class="box-03" style="padding: 14px 14px 0 16px;width:168px;height: 110px">
        <div><span class="fl" style="font-size: 14px;">其他账户</span><a href="<?php echo urlMember('member','bank_card_list')?>" class="fr" style="color: #970000;font-size:12px">更多&nbsp;&gt;</a></div>
        <div class="clear"></div>
        <div style="padding: 7px 0;border-bottom:#f2f2f2 solid 1px;height: 14px"><span class="fl"><i class="fa fa-credit-card-alt" aria-hidden="true" style="color: #000"></i>&nbsp;银行卡</span><a href="<?php echo urlMember('member','bank_card_list')?>" class="fr" style="color: #970000">管理</a></div>
        <div class="clear"></div>
        <div style="padding: 7px 0;border-bottom:#f2f2f2 solid 1px;height: 14px"><span class="fl">支付宝绑定:</span>
          <?php if($output['member_info']['member_alipay_bind'] == 0){?>
          <a href="<?php echo urlMember('member_alipay_bind','index')?>" class="fr">未绑定</a>
        <?php } else{?>
            <a href="<?php echo urlMember('member_alipay_bind','index')?>" class="fr" style="color: #970000">已绑定</a>
        <?php }?>
        </div>
        <div class="clear"></div>
        <div style="padding: 7px 0;height: 14px"><span class="fl">微信绑定:</span><a href="#" class="fr" style="color: #970000">已绑定</a></div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="fl">
      <div class="box-04">
        <div class="price fl">
          <span class="all-block">商品</span>
          <h1 style="margin-top:6px;font-size: 13px">已经购买商品:&nbsp;<b><?php echo $output['member_info']['bought_goods'];?>&nbsp;</b>件</h1>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlShop('member_order');?>" class="more fr" style="bottom:0px;">查看详情</a>
      </div>
      <div class="box-04">
        <div class="price fl">
          <span class="all-block">拍品</span>
          <h1 style="margin-top:6px;font-size: 13px">已经拍中拍品:&nbsp;<b><?php echo $output['member_info']['margin_counts'];?>&nbsp;</b>件</h1>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlShop('member_auction','index');?>" class="more fr" style="bottom:0px;">查看详情</a>
      </div>
      <div class="box-04" style="height: 73px">
        <div class="price fl">
          <span class="all-block">趣猜</span>
          <h1 style="margin-top:6px;font-size: 13px">已经中奖趣猜:&nbsp;<b><?php echo $output['member_info']['guess_count'];?>&nbsp;</b>件</h1>
        </div>
        <div class="clear"></div>
        <a href="<?php echo urlShop('member_guess','list',array('limit_type' => 'choiced'));?>" class="more fr" style="bottom:0px;">查看详情</a>
      </div>
    </div>
    <div class="clear"></div>
  </div>