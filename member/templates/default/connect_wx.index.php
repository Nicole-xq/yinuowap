<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
 .lm_login_dialog{
   width:100%;
   padding: 20px 20px 0;
   box-sizing: border-box;
 }
 .lm_login_dialog .lm_login_dialog_btn{
   width: 100%;
   background-color: #eee;
   height: 38px;
   color: #000000;
   font-weight: normal;
   font-size: 14px;
   letter-spacing: 1px;
   border: none;
   margin:20px 0 10px;
 }
</style>
<div class="nc-login-content tc" id="login_container"></div>
<?php if (!$_SESSION['member_id']) { ?>
<div class="lm_login_dialog">
  <span>提示：如已注册过本站帐号的用户，请先登录后，去用户中心绑定微信。</span>
  <button class="lm_login_dialog_btn">登录</button>
</div>
<?php } ?>
<script>
$(function(){
    $.getScript("<?php echo C('https')?'https':'http';?>://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js", function(){
        var obj = new WxLogin({
            id:"login_container",
            appid: "<?php echo C('weixin_appid');?>",
            scope: "snsapi_login",
            redirect_uri: "<?php echo MEMBER_SITE_URL.'/index.php?act=connect_wx&op=get_info';?>",
            state: "",
            style: "",
            href: ""
        });
    });
//    if(LM_Request('act')!=='login'){
//        $('.lm_login_dialog').remove();
//    }
    $('.lm_login_dialog_btn').click(function(){
        $('#fwin_weixin_form').hide();
        $('#dialog_manage_screen_locker').hide();
        $("input[name='ref_url']").val("<?php echo urlLogin('member_bind', 'weixinbind');?>");
    })
});
</script>
