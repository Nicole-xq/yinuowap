<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .card-box{background:#fff !important}
    .card-box .card-box-name{background:#f7f7f7}
    .card-box .card-box-name .bank-name{left:10px;font-size:13px}
    .card-box .card-box-name .bank-num4{right:10px;font-size:13px}
    .card-box .card-box-express{background:#f2f2f2 !important}
    .card-box .express-status{cursor:pointer}
    .card-box .card-box-express .express-status span i{font-size:18px;transform:translateY(1px);color:#ccc}
    .card-box .card-box-express .express-status span font{color:#CCC;font-size:14px}
    .card-box .card-box-express .selected span i{color:#000}
    .card-box .card-box-express .selected span font{color:#000;}
    .card-box .card-box-express .express-status:hover span i{color:#000}
    .card-box .card-box-express .express-status:hover span font{color:#000;}
</style>

<div class="account-center">
    <?php include template('member_account_common'); ?>
    <script>
        $(function(){
            $('.remove_card').on('click',function(){
                if(confirm("请您确认是否解除此银行卡的相关绑定!")){
                    var bankcard_id = $(this).attr('data-id');
                    window.location.href='index.php?act=member&op=remove_card&bankcard_id='+bankcard_id;
                }
            });
            $('.youxuan').on('click',function(){
                if(confirm("是否设置此银行卡为默认优选")){
                    var bankcard_id = $(this).attr('data-id');
                    window.location.href='index.php?act=member&op=edit_default&bankcard_id='+bankcard_id;
                }
            });

        })
    </script>

    <div class="card-box-list">
        <?php if(!empty($output['bankcard_list'])){?>
            <?php foreach($output['bankcard_list'] as $k=>$v){?>
                <div class="card-box">
                    <div class="card-box-name">
                        <span class="bank-name">银行卡号</span><span class="bank-num4"><?php echo $v['bank_card']?></span>
                    </div>
                    <div class="card-box-express">
                        <?php if($v['is_default'] == 0){?>
                        <div class="express-status">
                            <span class="youxuan" data-id="<?php echo $v['bankcard_id']?>"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;<font>默认优先</font></span>
                        </div>
                    <?php }else{?>
                            <div class="express-status selected">
                                <span><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;<font>默认优先</font></span>
                            </div>
                    <?php }?>
                    </div>
                    <div class="card-detail">
                        <a href="javascript:void(0);" class="remove_card" data-id="<?php echo $v['bankcard_id']?>">解除绑定</a>
                    </div>
                </div>
            <?php }?>
        <?php }?>

        <div class="add-card">
            <a href="<?php echo urlMember('member','add_bank_card')?>" target="_blank"><i class="fa fa-plus" aria-hidden="true" style="display: block;font-size: 36px;margin: 0 0 8px"></i>添加银行卡</a>
        </div>
    </div>
    <div class="clear"></div>
</div>
</div>
