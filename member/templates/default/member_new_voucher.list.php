<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="account-center">
<?php include template('member_account_common'); ?>
<div class="box invitation">
		<div class="tabmenu" style="padding:0 2%;width: 96%">
			<ul class="tab pngFix" style="background:#fcfcfc;">
				<li class="active"><a href="javascript:void(0);">我的优惠券</a></li>
				<li class="normal"><a href="<?php echo urlMember('member_voucher','voucher_binding');?>">领取优惠券</a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<ul class="mine">
			<?php foreach ($output['list'] as $key => $value):?>
				<li>
					<div class="fl">
						<div><h1><?php echo $value['voucher_title'];?></h1></span></div>
						<div class="clear"></div>
						<p><?php echo $value['voucher_desc'];?></p>
						<p>有效期至:<?php echo date('Y.m.d',$value['voucher_end_date']);?></p>
					</div>
					<div class="fr youhuiquan">
						<div><b><?php echo $value['voucher_price'];?></b>&nbsp;¥</div>
						<span>满<?php echo $value['voucher_limit'];?>使用</span>
						<span>优惠券</span>
					</div>
				</li>
			<?php endforeach;?>
		</ul>
		<div class="clear"></div>
	</div>
	</div>