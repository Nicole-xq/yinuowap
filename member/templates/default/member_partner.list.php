<?php defined('InShopNC') or exit('Access Invalid!');?>
<link rel="stylesheet" href="http://at.alicdn.com/t/font_43vrpvhbklw61or.css">
<style>
    a:hover .qll_more{color: #C81623;}
    .qll_more{text-align: right;margin: 10px 10px 0 0;color: #999;}
</style>
<div class="account-center">
    <div class="people-list box">
        <div class="tabmenu" style="padding:0 2%;width: 96%;background:#fcfcfc;">
            <ul class="tab pngFix fl">
                <li class="active"><a href="javascript:void(0);"><?php echo $output['partner_level'];?>伙伴</a></li>
            </ul>
            <ul class="jsddm fl">
                <li><a href="javascript:void(0);"><span class="show-select-text">时间排序</span>&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    <ul class="check_option">
                        <li data-type="desc"><a href="javascript:void(0);">最新时间</a></li>
                        <li data-type="asc"><a href="javascript:void(0);">最长时间</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php if(is_array($output['partner_list']) && !empty($output['partner_list'])){?>
            <ul class="list qll_list">
                <?php foreach($output['partner_list'] as $partner){?>

                        <li class="qll_partner">
                            <img src="<?php echo getMemberAvatar($partner['member_avatar']);?>" width="48" height="48">
                            <div class="fl">
                                <span class="all-block">伙伴名称：<?php echo $partner['member_comment'] == ''?$partner['member_name']:$partner['member_comment'];?></span>
                                <span class="all-block">会员等级：<?= $partner['member_type_name'];?></span>
                                <span class="all-block">佣金金额：￥<?php echo isset($partner['commission_amount'])?ncPriceFormat($partner['commission_amount']):ncPriceFormat(0);?></span>
                            </div>
                            <div class="clear"></div>
                          <div class="lm_setting">
                            <a id="edit_comment" href="javascript:edit_comment(<?=$partner['member_id'];?>);" class="edit_comment fl">修改备注名</a>
                            <a href="index.php?act=member_partner&op=partner_common_info&partner_id=<?=$partner['member_id'];?>" class="qll_more fr">更多 <i class="iconfont icon-shuangjiantou-copy"></i></a>
                          </div>
                          <div class="clear"></div>
                        </li>
                <?php }?>
            </ul>
        <?php }else{?>
            <div class="empty">
                <?php if(isset($_GET['upper_id'])){?>
                    还没有合作伙伴
                <?php }else{?>
                    您还没有合作伙伴，快去<a href="index.php?act=member_invitecode&get_code=ok">获取邀请码</a>发展自己的合作伙伴吧！
                <?php }?>
            </div>
        <?php }?>
        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
  var m_id;
    //添加输入框
(function($) {
    $.lm_dialog_inp = function(options) {
        console.log(options);
        var settings = $.extend({}, {action:'', nchash:'', formhash:'' ,anchor:''}, options);
        var login_dialog_html = $('<div class="quick-login"></div>');
        var ref_url = document.location.href;
        login_dialog_html.append('<form class="bg" method="post" id="ajax_lm_inp" action=""></form>').find('form')
            .append('<dl><dt>用户名</dt><dd><input type="text" name="comment" id="comment" autocomplete="off" class="text"></dd><dd><input type="hidden" id="member_id" name="member_id" value="<?php echo $output['partner']['member_id'];?>"></dd></dl>')
            .append('<div class="enter"><input type="submit" name="Submit" value="登&#12288;&#12288;录" class="submit"></div><input type="hidden" name="ref_url" value="'+ref_url+'">');
        login_dialog_html.find('input[type="submit"]').click(function(){
            $.post(MEMBER_SITE_URL+"/index.php?act=member_partner&op=update_member_comment",{'comment':$('#comment').val(),'member_id':m_id},function(re){
//                if(re == 1){
//                    console.log(re);
                if(re){
                    window.location.reload();
                }
//                }else{
//                    console.log(re);
//                    alert('失败');
//                }
            });
//            $('#ajax_lm_inp').submit();
//            window.location.reload();
            return true;
        });
        html_form("lm_dialog_inp", "备注", login_dialog_html, 360);
    };
    $.fn.nc_login = function(options) {
        return this.each(function() {
            $(this).on('click',function(){
                $.lm_dialog_inp(options);
                return false;
            });
        });
    };
})(jQuery);
function edit_comment(id){
    m_id = id;
    $.lm_dialog_inp();
}
$(function(){
    $('#edit_comment').click(function(){
        $.lm_dialog_inp();
    });
});
    var timeout         = 500;
    var closetimer		= 0;
    var ddmenuitem      = 0;
    var orderType = "<?php echo $_GET['order_type']?>";
    if(orderType != ''){
        var obj = $('.check_option').find("li[data-type="+orderType+"]");
        $('.jsddm .show-select-text').html(obj.find('a').html());
    }
    function jsddm_open(){
        jsddm_canceltimer();
        jsddm_close();
        ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');
    }
    function jsddm_close(){
        if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
    }
    function jsddm_timer(){
        closetimer = window.setTimeout(jsddm_close, timeout);
    }
    function jsddm_canceltimer(){
        if(closetimer)
        {
            window.clearTimeout(closetimer);
            closetimer = null;
        }
    }
    $(document).ready(function(){
        $('.jsddm > li').bind('mouseover', jsddm_open);
        $('.jsddm > li').bind('mouseout',  jsddm_timer);
    });
    document.onclick = jsddm_close;

    $('.check_option').on('click', 'li' ,function(){
        var orderType = $(this).attr('data-type');
        window.location.href = "index.php?act=member_partner&op=partner_list&order_type="+orderType;
    });

    function get_member_order(id){

    }
</script>