<?php defined('InShopNC') or exit('Access Invalid!');?>

<h3 class="qll_order_list_title">保证金返佣总收益：￥<?php echo ncPriceFormat($output['sum_commission_amount']); ?></h3>
<?php foreach($output['list'] as $row): ?>
<ul>
    <li class="qll_order_detail">
        <dl>
            <dt>
              <span style="margin-left: 0"><?php echo $row['goods_name']; ?></span>
            </dt>
            <dd>

                <span class="qll_order_detail_count">+<?php echo ncPriceFormat($row['commission_amount']); ?></span>
                <span><?php echo $row['label_date']; ?></span>
                <span><?php echo $row['label_status']; ?></span>
            </dd>
        </dl>
    </li>
    <li class="qll_commission_detail">
        <dl>
            <dt>保证金总金额：<?php echo ncPriceFormat($row['goods_pay_amount']); ?></dt>
            <dd>年化收益率：<?php echo $row['dis_commis_rate']; ?>%</dd>
            <dd><?php echo $row['order_info']['label_payment_time']; ?></dd>
            <dd><?php echo $row['label_status']; ?></dd>
        </dl>
    </li>
</ul>
<?php endforeach; ?>