<?php
defined('InShopNC') or exit('Access Invalid!');
/**
 * 积分功能公用
 */
$lang['points_unavailable']	 		= '系统未开启诺币功能';
$lang['points_membername']			= '会员名称';
$lang['points_pointsnum']			= '诺币变更';
$lang['points_pointsdesc']			= '描述';
/**
 * 积分日志
 */
$lang['points_log_title']			= '诺币日志';
$lang['points_stage']				= '操作';
$lang['points_stage_regist']		= '注册';
$lang['points_stage_login']			= '登录';
$lang['points_stage_comments']		= '商品评论';
$lang['points_stage_order']			= '订单消费';
$lang['points_stage_system']		= '诺币管理';
$lang['points_stage_pointorder']		= '礼品兑换';
$lang['points_stage_app']		= '诺币兑换';
$lang['points_addtime']				= '添加时间';
$lang['points_addtime_to']			= '至';
$lang['points_log_pointscount']				= '诺币总数：';