<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="top-hot-search">
        <div class="hot-search swiper-container" style="width:100%;">
            <div class="swiper-wrapper">
                <style>
                    .top-hot-search .hot-search .swiper-wrapper .swiper-slide{margin-right:16px !important}
                </style>
                <?php  $i = 1;?>
                  <?php foreach ((array) $vv['item'] as $item) {?>
                    <div class="swiper-slide"><a nctype="btn_item" href="javascript:;"  data-type="<?php echo $item['type'];?>" data-data="<?php echo $item['data'];?>" <?php if($i==1):?>class="selected"<?php endif;?>><?php echo $item['sem'];?></a></div>
                <?php $i++;} ?>        
            </div>
        </div>
        <div class="clear"></div>
    </div>
