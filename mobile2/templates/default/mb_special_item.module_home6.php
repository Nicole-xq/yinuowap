<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<div class="content-box-01">
    <?php if(!empty($vv['title'])) {?>
        <div class="title-name">
            <h1><?php echo $vv['title']; ?></h1>
            <i class="all-block"></i>
        </div>
    <?php } ?>
        
        <div class="row no-gutter">
            <?php foreach ((array) $vv['item'] as $item) { ?>
                <div nctype="item_image" class="item">
                   <a nctype="btn_item" href="javascript:;" data-type="<?php echo $item['type']; ?>" data-data="<?php echo $item['data']; ?>" class="col-25"><img src="<?php echo $item['image']; ?>" alt=""></a>
                </div>
            <?php } ?>                 
        </div>
        <div class="clear"></div>
    </div>