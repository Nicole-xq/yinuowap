<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<div class="content-box-03">
	<div class="title-name">
		<h1> <?php echo $vv['title'];?></h1>
		<i class="all-block"></i>
	</div>
	<div class="row no-gutter">	
	 <?php foreach ((array) $vv['item'] as $item) { ?>
		<a nctype="btn_item" href="javascript:;" class="col-50" data-type="<?php echo $item['type']; ?>" data-data="<?php echo $item['data']; ?>">
			<img src="<?php echo $item['image'];?>" alt="">
			<div class="content-detail fl" style="padding: 8px 0px 8px 10px;">
				<img src="<?php echo $item['store_logo'];?>" alt="" style="width: 60%; height: auto;">
				<h1><?php echo $item['store_name'];?></h1>
				<?php echo $item['store_collect'];?>人关注
			</div>
		</a>
	<?php } ?>
	</div>
	<div class="clear"></div>
</div>