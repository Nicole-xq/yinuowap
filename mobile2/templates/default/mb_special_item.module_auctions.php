<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<div class="content-box-02">
         <?php if(!empty($vv['title'])) {?>
            <div class="title-name">
                <h1><?php echo $vv['title']; ?></h1>
                <i class="all-block"></i>
            </div>
        <?php } ?>
        <div class="row no-gutter">
             <div nctype="item_content" class="content">
                <?php foreach ((array) $vv['item'] as $item) { ?>
                    <a nctype="btn_item" href="javascript:;" data-type="auction" data-data="<?php echo $item['auction_id']; ?>" class="col-50">
                        <img src="<?php echo $item['auction_image'];?>" alt="">
                        <div class="content-detail">
                            <h1><?php echo $item['auction_name']; ?></h1>
                            <?php if($item['is_kaipai'] == 0):?><?php echo $item['auction_end_date']; ?>
                            <span class="theme-color">起拍价&nbsp;<b>&yen;<?php echo $item['auction_start_price']; ?></b></span>
                            <span>开拍时间&nbsp;<?php echo $item['auction_start_date']; ?></span>
                            <?php elseif($item['is_kaipai'] == 1):?>
                            <span class="theme-color">当前价&nbsp;<b>&yen;<?php echo $item['current_price']; ?></b></span>
                            <span class="count-time" count_down="<?php echo $item['auction_remain_date']; ?>">距&nbsp;结&nbsp;束&nbsp;&nbsp;<em time_id="d">01</em>天<em time_id="h">01</em>时<em time_id="m">01</em>分<em time_id="s">01</em>秒</span>
                            <?php else:?>
                            <span class="theme-color">成交价&nbsp;<b>&yen;<?php echo $item['current_price']; ?></b></span>
                            <span>结束时间&nbsp;<?php echo $item['auction_end_date']; ?></span>
                            <?php endif;?>
                        </div>
                        <div class="fixed-box" style="top:60%;border-left:none">
                            <?php if($item['is_kaipai'] == 1 || $item['is_kaipai'] ==2 ):?>
                            <span style="background:#262626;"><b class="all-block"><?php echo $item['bid_number']; ?></b>次出价</span>
                            <?php else: ?>
                            <span style="background:#262626;"><b class="all-block"><?php echo $item['auction_click']; ?></b>次围观</span>
                            <?php endif;?>
                        </div>
                        <?php if($item['is_kaipai'] == 1):?>
                            <div class="fixed-box" style="bottom:5%;">
                                <img src="<?php echo RESOURCE_SITE_URL; ?>/wap/images/index-offer.jpg" alt=""/>
                            </div>
                        <?php endif;?>
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
        <a href="./tmpl/auction/auction_list.html" class="index-more">查看更多&nbsp;&gt;</a>
        <div class="clear"></div>
    </div>
