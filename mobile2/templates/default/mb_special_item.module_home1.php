<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="index_block home1">
   <?php if(!empty($vv['title'])) {?>
    <div class="title-name">
        <h1><?php echo $vv['title']; ?></h1>
        <i class="all-block"></i>
    </div>
<?php } ?>
    <div class="content">
        <div nctype="item_image" class="item">
            <a nctype="btn_item" href="javascript:;" data-type="<?php echo $vv['type']; ?>" data-data="<?php echo $vv['data']; ?>">
                <img nctype="image" src="<?php echo $vv['image']; ?>" alt="">
            </a>
        </div>
    </div>
</div>
