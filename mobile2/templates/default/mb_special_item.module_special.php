<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<?php if(isset($vv['special'])):?>
<div class="content content-box-05">
    <div class="buttons-tab">
        <?php $i=0;?>
        <?php foreach($vv['special'] as $key => $value):?>
              <a href="javascript:void(0);" id="special_<?php echo $i;?>" onclick="get_special_list(<?php echo $i;?>)" 
              <?php if($i == 0):?>class="tab-link active button"<?php else:?>class="tab-link button"<?php endif;?>
              ><?php echo $value['title'];?></a>
        <?php $i++; endforeach;?>
    </div>
    <div class="content-block">
        <div class="tabs">
        <?php $j=0;?>
        <?php foreach ($vv['special'] as $key => $value) :?>
             <div id="tab<?php echo $j;?>" <?php if($j == 0): ?> class="tab active" <?php else:?> class="tab" <?php endif;?>>
                <div class="content-block">
                    <div class="content-box-04">
                        <div class="row no-gutter">
                            <?php foreach($value['item'] as $k => $v):?>
                                 <a nctype="btn_item" href="javascript:;"  data-type="auc_specail" data-data="<?php echo $v['special_id'];?>" class="col-100">
                                    <img src="<?php echo $v['special_image']?>" alt=""/>
                                    <div class="content-detail fl">
                                        <h1><?php echo $v['special_name'];?></h1>
                                        <span class="theme-color"><?php echo $v['delivery_mechanism'];?></span>
                                        <?php if($v['is_kaipai'] == 0):?>
                                        <span>开拍时间&nbsp;<?php echo $v['special_start_date'];?></span>
                                        <?php elseif($v['is_kaipai'] == 1):?>
                                        <span class="settime" endtime="<?php echo $v['special_remain_date'];?>">距&nbsp;结&nbsp;束&nbsp;&nbsp;<em time_id="d">01</em>天<em time_id="h">01</em>时<em time_id="m">01</em>分<em time_id="s">01</em>秒</span>
                                        <?php else:?>
                                        <span>结束时间&nbsp;<?php echo $v['special_end_date'];?></span>
                                        <?php endif;?>
                                    </div>
                                    <?php if($v['is_kaipai'] == 0):?>
                                    <div class="fr"><b><?php echo $v['special_click'];?></b>次围观</div>
                                    <?php else:?>
                                    <div class="fr"><b><?php echo $v['all_bid_number'];?></b>次出价</div>
                                    <?php endif;?>
                                </a>
                            <?php endforeach;?>
                            </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php $j++;?>
        <?php endforeach;?>
            </div>
        </div>
</div>
<?php endif; ?>