<?php
/**
 * 拍卖商家中心
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class store_vendueControl extends mobileHomeControl
{
	public function __construct()
	{
		parent::__construct();
	}

	public function store_vendue_infoOp(){
		if(!$_POST['store_id']){
			output_error('参数错误');
		}
		$store_id = $_POST['store_id'];

		$model_store = Model('store');
		$model_auctions = Model('auctions');
		$model_special = Model('auction_special');

		$store_info = $model_store->getStoreOnlineInfoByID($store_id);
		if(empty($store_info)){
			output_error('店铺不存在或未开启');
		}

		$store_vendue_info = $this->getStoreInfo($store_id);

		$special_list = $model_special->getSpecialOpenList(array('store_id' => $store_info['store_id']));
		foreach($special_list as $key => $special_info){
			$special_list[$key]['wap_image_path'] = getVendueLogo($special_info['wap_image']);
			$special_list[$key]['special_start_date'] = date('Y年m月d日 H:i',$special_info['special_start_time']);
			$special_list[$key]['special_end_date'] = date('Y年m月d日 H:i',$special_info['special_end_time']);
			if(time() >= $special_info['special_start_time']){
				if(time() >= $special_info['special_start_time'] && $special_info['special_end_time']> time()){
					$is_kaipai = 1;
				}elseif($special_info['special_end_time']<= time()){
					$is_kaipai = 2;
				}

			}elseif($special_info['special_start_time'] > time()){
				$is_kaipai = 0;
			}
			$special_list[$key]['special_remain_date'] = $special_info['special_end_time']-TIMESTAMP >0 ? $special_info['special_end_time']-TIMESTAMP :0;
			$special_list[$key]['is_kaipai'] = $is_kaipai;
			$special_goods_list = $model_special->getSpecialGoodsLists(array('special_id'=>$special_info['special_id'],'auction_sp_state'=>1));
			$all_bid_number = 0;
			$jian_num = 0;
			foreach($special_goods_list as $k=>$v){
				$all_bid_number += $v['bid_number'];
				if($v['is_liupai'] == 0 && $v['auction_end_true_t'] <=time()){
					$jian_num += 1;
				}
			}
			$special_list[$key]['all_bid_number'] = $all_bid_number;
			$special_list[$key]['jian_num'] = $jian_num;
		}

		$auctions_list = $model_auctions->getAuctionList(array('store_id' => $store_info['store_id'],'state'=>array('in',array(0,1))));
		foreach($auctions_list as $k => $auctions_info){
			$auctions_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'],360);
			if(time() >= $auctions_info['auction_start_time']){
				if($auctions_info['auction_start_time'] <= time() && ($auctions_info['auction_end_time'] >= time() || $auctions_info['auction_end_true_t'] >= time())){
					$is_kaipai = 1;
				}elseif($auctions_info['auction_end_true_t'] < time()){
					$is_kaipai = 2;
				}
			}else{
				$is_kaipai = 0;
			}
			$auctions_list[$k]['is_kaipai'] = $is_kaipai;
			$auctions_list[$k]['auction_start_date'] = date('m月d日 H:i',$auctions_info['auction_start_time']);
			$auctions_list[$k]['auction_end_date'] = date('m月d日 H:i',$auctions_info['auction_end_time']);
			$auctions_list[$k]['auction_remain_date'] = $auctions_info['auction_end_time'] - time() >0 ? $auctions_info['auction_end_time'] - time() : 0;
			$auctions_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00 ? $auctions_info['current_price'] : $auctions_info['auction_start_price'];
		}
		output_data(array('store_vendue_info' => $store_vendue_info , 'special_list' => $special_list, 'auctions_list' => $auctions_list));

	}

	//商家店铺首页信息
	public function store_indexOp(){
        if(!$_GET['store_id']){
            output_error('参数错误');
        }
        output_data($this->getStoreInfo($_GET['store_id']));
    }

    /**
     * 获取商家信息
     * @param int $store_id
     * @return array
     */
    private function getStoreInfo($store_id){
        $model_store = Model('store');
        $model_store_vendue = Model('store_vendue');
        $model_artist_vendue = Model('artist_vendue');

        $store_info = $model_store->getStoreOnlineInfoByID($store_id);
        if(empty($store_info)){
            output_error('店铺不存在或未开启');
        }

        $store_vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_id' => $store_info['store_id']));
        if(empty($store_vendue_info)){
            output_error('参数错误');
        }

        if($store_vendue_info['store_apply_state'] != 40){
            output_error('该店铺的拍卖功能尚未开启');
        }
        $artist_info = $model_artist_vendue->getArtistInfo(array('store_vendue_id'=>$store_vendue_info['store_vendue_id']));
        if(!empty($artist_info)){
            $store_vendue_info['store_vendue_intro'] = $artist_info['artist_resume'];
        }

        $store_vendue_info['store_name'] = $store_info['store_name'];
        $store_vendue_info['store_collect'] = $store_info['store_collect'];
        $store_vendue_info['store_avatar'] = $store_info['store_avatar']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_info['store_avatar']
            : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');

        $store_vendue_info['store_logo'] = getStoreLogo($store_info['store_label'],'store_logo');
        $store_vendue_info['member_id'] = $store_info['member_id'];
        $store_vendue_info['member_name'] = $store_info['member_name'];

        // 如果已登录 判断该店铺是否已被收藏
        if ($memberId = $this->getMemberIdIfExists()) {
            $c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($store_id, $memberId);
            $store_vendue_info['is_favorate'] = $c > 0;
        } else {
            $store_vendue_info['is_favorate'] = false;
        }
        return $store_vendue_info;
    }
}