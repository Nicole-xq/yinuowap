<?php
/**
 * 刷新app专题页
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class cmsControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    // 获取专题信息
    public function indexOp(){
        $cms_id = $_GET['cms_id'];
        $info = Model('cms_special')->getOne(array('special_id'=>$cms_id));
        $info['special_mobile_content'] = html_entity_decode($info['special_mobile_content']);
        $info['special_content'] = html_entity_decode($info['special_content']);
        $info['special_share_logo'] = '/data/upload/cms/special/'.$info['special_share_logo'];

        output_data(array('detail' => $info));
//        print_R($info);exit;
    }

}