<?php
/**
 * 会员退款
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
use Shopnc\Tpl;
class member_underlineControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

      /**
     * 上传凭证
     */
    public function upload_picOp() {
        $upload = new UploadFile();
        $dir = ATTACH_PATH.DS.'pay_voucher'.DS;
        $upload->set('default_dir',$dir);
        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
        $upload->set('max_size', 5*1024);
        $result = 0;
        if (!empty($_FILES['refund_pic']['name'])){
            $result = $upload->upfile('refund_pic');
        }
        if ($result){
            $file_name = $upload->file_name;
            $pic = UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/pay_voucher/'.$file_name;
            output_data(array('file_name' => $file_name,'pic' => $pic));
        } else {
            output_error('图片上传失败');
        }
    }

     /**
     * 提交线下付款凭证
     * @return [type] [description]
     */
    public function pay_underline_orderOp(){
        // 1.接收参数
        // 2.获取订单信息验证是否是新订单
        $logic_payment = Logic('payment');
        $result = $logic_payment->getRealOrderInfo($_POST['pay_sn']);
        if (!$result['state']) {
            output_error('参数错误');
        }
        if ($result['data']['buyer_id'] != $this->member_info['member_id']) {
        	output_error('参数错误');
        }
        if ($result['data']['api_pay_state'] != 0) {
            output_error('该订单已支付');
        }
        // 3.上传图片
        if(empty($_POST['pay_voucher'])) {
            output_error('请上传付款凭证');
        }
        $logic_order = Logic('order');
        // 4.修改订单状态
        $result = $logic_order->changeOrderStateInConfirm($result['data']['pay_sn'], $_POST['pay_voucher']);
        if ($result['state']) {
            output_data(1);
        } else {
            output_error('操作失败，请重试');
        }
    }








}
