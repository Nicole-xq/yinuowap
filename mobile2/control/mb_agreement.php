<?php
/**
 * 协议
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class mb_agreementControl extends mobileControl {
    public function indexOp() {
        $this->agreement_infoOp();
    }

    /**
     * 协议信息
     *
     * @param
     * @return array 数组格式的返回结果
     * */
    public function agreement_infoOp() {
        if (empty($_GET['type'])) {
            output_error('参数错误');
        }

        $condition = array('agreement_code'=>$_GET['type']);
        $model_agreement = Model('mb_agreement');
        $agreement_info = $model_agreement->getMbAgreementInfo($condition);

        if (empty($agreement_info)) {
            output_error('参数错误');
        }

        $data = array();
        $data['title'] = $agreement_info['agreement_title'];
        $data['content'] = html_entity_decode($agreement_info['agreement_content']);;
        output_data($data);
        die;
    }
}