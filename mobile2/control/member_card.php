<?php
/**
 * 绑定银行卡
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_cardControl extends mobileMemberControl {

	public function __construct(){
		parent::__construct();
	}


	public function indexOp(){
		$model_bankcard = Model('bankcard');
		$bankcard_list = $model_bankcard->getbankCardList(array('member_id'=>$this->member_info['member_id']),$this->page);
		foreach($bankcard_list as $k => $value){
			$bankcard_list[$k]['bank_card_show'] = '**** **** **** '.substr($value['bank_card'],-4);
            $bankcard_list[$k]['bank_card_name'] = $this->card_name($value['bank_name']);
		}
		$page_count = $model_bankcard->gettotalpage();
		output_data(array('bankcard_list' => $bankcard_list),mobile_page($page_count));
	}

	public function bankcard_step2Op(){
		$this->_send_bind_mobile_msg();

	}

    public function deleteOp()
    {
        $model_bankcard = Model('bankcard');
        $condition = [
            'member_id' => $this->member_info['member_id'],
            'bankcard_id' => $_REQUEST['bankcard_id'],
        ];
        $result = $model_bankcard->delbankCard($condition);
        if ($result) {
            output_data($result);
        } else {
            output_error('操作失败！');
        }
    }

    public function bankcard_defaultOp()
    {
        $bankcard_id = $_REQUEST['bankcard_id'];
        $model_bankcard = Model('bankcard');
        $data = array();
        $data['is_default'] = 1;
        $cardid_list = $model_bankcard->getbankCardList(array('member_id'=>$this->member_info['member_id'],'bankcard_id'=>array('neq',$bankcard_id)),'','','bankcard_id');
        $a = [];
        foreach($cardid_list as $k=>$v){
            $a[$k]= $v['bankcard_id'];
        }
        $model_bankcard->editbankCard($data,array('bankcard_id'=>$bankcard_id));
        $result = $model_bankcard->editbankCard(array('is_default'=>0),array('bankcard_id'=>array('in',$a)));
        if ($result) {
            output_data($result);
        } else {
            output_error('操作失败！');
        }
    }


	public function bankcard_step3Op(){
		$model_member = Model('member');
		$vcode = $_POST['captcha'];
		$condition = array();
		$condition['member_id'] = $this->member_info['member_id'];
		$condition['auth_code'] = intval($vcode);
		$member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
		if (!$member_common_info) {
			output_error('手机验证码错误，请重新输入');
		}
		if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
			output_error('手机验证码已过期，请重新获取验证码');
		}
        $bank_card = $_POST['bank_card'];
		$model_bankcard = Model('bankcard');
		$is_have_card = $model_bankcard->getbankCardInfo(array('bank_card' => $bank_card));
		if(!empty($is_have_card)){
			output_error('您当前使用的银行卡已绑定，请点击银行卡查看');
		}

        $condition = array(
            'id_number' => $_POST['id_number'],
            'member_id' => array('neq', $this->member_info['member_id']),
        );
        $is_have_id_number = $model_bankcard->getbankCardInfo($condition);
        if(!empty($is_have_id_number)){
            output_error('您的身份证号码已绑定其它帐号，详情请联系客服400-135-2688');
        }

		$member_id = $this->member_info['member_id'];
		$member_name = $this->member_info['member_name'];
        $true_name = $_POST['true_name'];
        $id_number = $_POST['id_number'];
        $bank_info = $model_bankcard->getbankCardInfo(array('member_id' => $member_id));
        if(!empty($bank_info['id_number'])){
            $true_name = $bank_info['true_name'];
            $id_number = $bank_info['id_number'];
        }
        $jsonarr = $this->_realName($id_number, $true_name);
		if($jsonarr['ret'] != 200 || $jsonarr['data']['desc'] != 0){
            $msg = $jsonarr['ret'] == '200' ? $jsonarr['data']['desc'] : $jsonarr['msg'];
		    file_put_contents(BASE_DATA_PATH . '/log/wt_bank_test.log',json_encode($jsonarr)."\r\n", FILE_APPEND);
			output_error($msg);
		}else{
		    $bankInfo = $this->_bankName($bank_card);
		    if($bankInfo){
		        $data['card_type'] = $bankInfo['cardtype'];
		        $data['bank_name'] = $bankInfo['bankname'];
		        $data['bank_image'] = $bankInfo['bankimage'];
            }
			$data['member_id'] = $member_id;
			$data['member_name'] = $member_name;
			$data['bank_card'] = $bank_card;
			$data['true_name'] = $true_name;
			$data['member_phone'] = $_POST['mobile'];
			$data['id_number'] = $id_number;
			$result = $model_bankcard->addbankCard($data);
			if($result){
				output_data($result);
			} else {
                output_error('添加失败，请联系管理员');
			}
		}


	}

	/**
	 * 银行卡实名认证校验
     *
     * @return array
	 * */
    private function _realName($number, $true_name)
    {
        $host = "https://api11.aliyun.venuscn.com";
        $path = "/cert/bank-card/4";
        $method = "GET";
        $appcode = C('bank.appcode');
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "bank=".$_POST['bank_card']."&mobile=".$_POST['mobile']."&number=".$number."&name=".$true_name."";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $result = json_decode(curl_exec($curl), true);
        return $result;
    }

	/**
	 * 银行卡归属查询
	 * */
	private function _bankName($card){
	    if(!$card) return null;
        $host = "http://aliyun.apistore.cn";
        $path = "/7";
        $method = "GET";
        $appcode = C('bank_info.appcode');
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "bankcard=" . $card;
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }

		$result = json_decode(curl_exec($curl), true);
        if($result['error_code'] == 0) return $result['result'];
        return null;
    }

	private function _send_bind_mobile_msg() {

		$model_member = Model('member');
		//发送频率验证
		$member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$this->member_info['member_id']));
		if (!empty($member_common_info['send_acode_time'])) {
			if (date('Ymd',$member_common_info['send_acode_time']) != date('Ymd',TIMESTAMP)) {
				$data = array();
				$data['send_acode_times'] = 0;
				$update = $model_member->editMemberCommon($data,array('member_id'=>$this->member_info['member_id']));
				if (!$update) {
					output_error('系统发生错误');
				}
			} else {
				if (TIMESTAMP - $member_common_info['send_acode_time'] < DEFAULT_CONNECT_SMS_TIME) {
					output_error('请60秒以后再发');
				} else {
					if ($member_common_info['send_acode_times'] >= 15) {
						output_error('今天短信已超15条，无法发送');
					}
				}
			}
		}

		try {
			$verify_code = rand(1000,9999);
			$tpl_info = Model('mail_templates')->getTplInfo(array('code'=>'authenticate'));
			$param = array();
			$param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
			$param['verify_code'] = $verify_code;
			$param['site_name'] = C('site_name');
			$message = ncReplaceText($tpl_info['content'],$param);
			$sms = new Sms();
			$result = $sms->send($_GET['phone'],$message);
			if ($result) {
				$update_data = array();
				$update_data['auth_code'] = $verify_code;
				$update_data['send_acode_time'] = TIMESTAMP;
				$update_data['send_acode_times'] = array('exp','send_acode_times+1');
				$update = $model_member->editMemberCommon($update_data,array('member_id'=>$this->member_info['member_id']));
				if (!$update) {
					output_error('系统发生错误');
				}
				output_data(array('sms_time'=>DEFAULT_CONNECT_SMS_TIME));
			} else {
				output_error('验证码发送失败');
			}
		} catch (Exception $e) {
			output_error($e->getMessage());
		}
	}


	/**
	 * 检测会员手机是否绑定
	 * 更改绑定手机 第一步 - 得到已经绑定的手机号
	 * 修改密码 第一步 - 得到已经绑定的手机号
	 * 修改支付密码 第一步 - 得到已经绑定的手机号
	 */
	public function get_mobile_infoOp() {
		$data = array();
		$data['state'] = $this->member_info['member_mobile_bind'] ? true : false;
		$data['mobile'] = $data['state'] ? encryptShow($this->member_info['member_mobile'],4,4) : $this->member_info['member_mobile'];
		output_data($data);
	}

    /**
     * 获取用户信息
     * */
	public function get_member_infoOp(){
        $data = array();
        $data['member_id'] = $this->member_info['member_id'];
        $data['mobile'] = $this->member_info['member_mobile'];
        $data['member_name'] = $this->member_info['member_truename'] ? $this->member_info['member_truename'] : $this->member_info['member_name'];

        $card_info = Model('bankcard')->getbankCardInfo(['member_id'=>$this->member_info['member_id']]);
        if(!empty($card_info)){
            $data['true_name'] = $card_info['true_name'];
            $data['id_number'] = idNumberHide($card_info['id_number']);
        }
		output_data($data);

	}

	/**
     * 验证用户是否已绑定
	 * */
	public function test_bindOp(){
        $id_number = $_POST['id_number'];
        if(!empty($id_number)){
            $result = Model('bankcard')->getbankCardInfo(['id_number'=>$id_number]);
            if($result){
                output_error('您的身份证号码已在其它帐号绑定，详情请联系客服400-135-2688');
            }
            output_data('');
        }
        output_error('您的身份证号码有误');
    }

	public function delete_cardOp(){
		$bankcard_id = $_POST['bankcard_id'];
		if(!$bankcard_id){
			output_error('非法参数');
		}
		$model_bankcard = Model('bankcard');
		$result = $model_bankcard->delbankCard(array('bankcard_id' => $bankcard_id));
		if($result){
			output_data('1');
		}
	}

	private function card_name($card_name){
        if(!$card_name){
            return $card_name;
        }
        switch ($card_name) {
            case '中国农业银行' :
                $data = 'abchina_bank';
                break;
            case '农业银行' :
                $data = 'abchina_bank';
                break;
            case '招商银行' :
                $data = 'cmbchina_bank';
                break;
            case '建设银行' :
                $data = 'ccb_bank';
                break;
            case '工商银行' :
                $data = 'icbc_bank';
                break;
            case '中国邮政储蓄银行' :
                $data = 'psbc_bank';
                break;
            case '邮政储蓄银行' :
                $data = 'psbc_bank';
                break;
            case '中国民生银行' :
                $data = 'cmbc_bank';
                break;
            case '民生银行' :
                $data = 'cmbc_bank';
                break;
            case '平安银行' :
                $data = 'pingan_bank';
                break;
            case '浦发银行' :
                $data = 'spdb_bank';
                break;
            default :
                $data = 'other_bank';
        }
        return $data;
    }

}