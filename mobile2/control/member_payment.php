<?php
/**
 * 支付
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_paymentControl extends mobileMemberControl {

    private $payment_code;
    private $payment_config;

    public function __construct() {
        parent::__construct();

        if($_GET['op'] != 'payment_list' && !$_POST['payment_code']) {
            $payment_code = 'alipay';

            if(in_array($_GET['op'], array('wx_app_pay', 'wx_app_pay3', 'wx_app_vr_pay', 'wx_app_vr_pay3'), true)) {
                $payment_code = 'wxpay';
            }
            else if (in_array($_GET['op'],array('alipay_native_pay','alipay_native_vr_pay'),true)) {
                $payment_code = 'alipay_native';
            }
            else if (isset($_GET['payment_code'])) {
                $payment_code = $_GET['payment_code'];
            }

            /** @var mb_paymentModel $model_mb_payment */
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $payment_code;
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                output_error('支付方式未开启');
            }

            $this->payment_code = $payment_code;
            $this->payment_config = $mb_payment_info['payment_config'];

        }
    }

    /**
     * 实物订单支付 新方法
     */
    public function pay_newOp() {
        @header("Content-type: text/html; charset=".CHARSET);
        /** @var mb_paymentModel $model_mb_payment */


        $pay_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{18}$/',$pay_sn)){
            exit('支付单号错误');
        }
        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi','lklpay'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];            
        } else {
            exit('支付方式提交错误');
        }


        

        $pay_info = $this->_get_real_order_info($pay_sn,$_GET);

        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }
        if (isset($_GET['inapp'])) {
            output_data(array('state' => 'success'));
            return;
        }
        //第三方API支付
        $this->_api_pay($pay_info['data']);


    }

    /**
     * 虚拟订单支付 新方法
     */
    public function vr_pay_newOp() {
        @header("Content-type: text/html; charset=".CHARSET);
        $order_sn = $_GET['pay_sn'];
        if(!preg_match('/^\d{18}$/',$order_sn)){
            exit('订单号错误');
        }
        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi','lklpay'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];
        } else {
            exit('支付方式提交错误');
        }

        $pay_info = $this->_get_vr_order_info($order_sn,$_GET);
        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }
        if (isset($_GET['inapp'])) {
            output_data(array('state' => 'success'));
            return;
        }

        //第三方API支付
        $this->_api_pay($pay_info['data']);
    
    }

    /**
     * wap充值新方法
     */
    public function pd_pay_newOp() {
        @header("Content-type: text/html; charset=".CHARSET);
        $pay_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{18}$/',$pay_sn)){
            exit('支付单号错误');
        }
        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi','lklpay'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $model_mb_payment = new mb_paymentModel();
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];
        } else {
            exit('支付方式提交错误');
        }
        $pay_info = $this->_get_pd_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }

        $pay_info['data']['api_pay_amount'] = $pay_info['data']['pdr_amount'];

        //第三方API支付
        $this->_api_pay($pay_info['data']);


    }

    /**
     * 站内余额支付(充值卡、预存款支付) 实物订单
     *
     */
    private function _pd_pay($order_list, $post) {
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if($post['points_pay'] == 1){
            $post['points_amount'] = $buyer_info['member_points']/100;
        }else{
            $post['points_amount'] =  0;
        }
        if (empty($post['password']) && $post['points_amount'] <= 0) {
            return $order_list;
        }

        if (($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($post['password'])) && $post['points_amount'] <=0) {
            return $order_list;
        }
    
        if ($buyer_info['available_rc_balance'] == 0) {
            $post['rcb_pay'] = null;
        }
        if ($buyer_info['available_predeposit'] == 0) {
            $post['pd_pay'] = null;
        }
        if (floatval($order_list[0]['rcb_amount']) > 0 || floatval($order_list[0]['pd_amount']) > 0) {
            return $order_list;
        }

        try {
            $model_member->beginTransaction();
            /** @var buy_1Logic $logic_buy_1 */
            $logic_buy_1 = Logic('buy_1');
            //使用充值卡支付
            if (!empty($post['rcb_pay'])) {
                $order_list = $logic_buy_1->rcbPay($order_list, $post, $buyer_info,$post['points_amount']);
            }
    
            //使用预存款支付
            if (!empty($post['pd_pay'])) {
                $order_list = $logic_buy_1->pdPay($order_list, $post, $buyer_info,$post['points_amount']);
            }

            if(empty($post['pd_pay']) && empty($post['rcb_pay']) && $post['points_amount'] >0){
                $order_list = $logic_buy_1->nbPay($order_list, $post, $buyer_info,$post['points_amount']);
            }
    
            //特殊订单站内支付处理
            $logic_buy_1->extendInPay($order_list);
    
            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            exit($e->getMessage());
        }

        return $order_list;
    }

    /**
     * 站内余额支付(充值卡、预存款支付) 虚拟订单
     *
     */
    private function _pd_vr_pay($order_info, $post) {
        if (empty($post['password'])) {
            return $order_info;
        }
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($post['password'])) {
            return $order_info;
        }
        if ($buyer_info['available_rc_balance'] == 0) {
            $post['rcb_pay'] = null;
        }
        if ($buyer_info['available_predeposit'] == 0) {
            $post['pd_pay'] = null;
        }
        if (floatval($order_info['rcb_amount']) > 0 || floatval($order_info['pd_amount']) > 0) {
            return $order_info;
        }

        try {
            $model_member->beginTransaction();
            $logic_buy = Logic('buy_virtual');
            //使用充值卡支付
            if (!empty($post['rcb_pay'])) {
                $order_info = $logic_buy->rcbPay($order_info, $post, $buyer_info);
            }
    
            //使用预存款支付
            if (!empty($post['pd_pay'])) {
                $order_info = $logic_buy->pdPay($order_info, $post, $buyer_info);
            }
    
            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            exit($e->getMessage());
        }
    
        return $order_info;
    }

    /**
     * 实物订单支付
     */
    public function payOp() {
        $pay_sn = $_GET['pay_sn'];

        $pay_info = $this->_get_real_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        //第三方API支付
        $this->_api_pay($pay_info['data']);
    }

    /**
     * 虚拟订单支付
     */
    public function vr_payOp() {
        $pay_sn = $_GET['pay_sn'];

        $pay_info = $this->_get_vr_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        //第三方API支付
        $this->_api_pay($pay_info['data']);
    }

    /**
     * 第三方在线支付接口
     *
     */
    private function _api_pay($order_pay_info) {
//        print_R($order_pay_info);exit;
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $param = $this->payment_config;

        $order_type = 'r';
        switch($order_pay_info['order_type']){
            case 'real_order':
                $order_type = 'r';
                break;
            case 'vr_order':
                $order_type = 'v';
                break;
            case 'pd_order':
                $order_type = 'p';
                $param['limit_pay'] = 'no_credit'; //微信wap充值禁用信用卡
                break;
        }
        if ($this->payment_code == 'wxpay_jsapi') {
            $param['orderSn'] = $order_pay_info['pay_sn'];
            if($order_pay_info['order_type'] == 'pd_order'){
                $param['orderFee'] = (int) (100 * $order_pay_info['pdr_amount']);
            }else{
                $param['orderFee'] = (int) (100 * $order_pay_info['api_pay_amount']);
            }
            $param['orderInfo'] = $order_pay_info['pay_sn'] . '订单';
            $param['orderAttach'] = $order_type;
            $api = new wxpay_jsapi();
            $api->setConfigs($param);
            try {
                echo $api->paymentHtml($this);
            } catch (Exception $ex) {
                if (C('debug')) {
                    header('Content-type: text/plain; charset=utf-8');
                    echo $ex, PHP_EOL;
                } else {
                    Tpl::output('msg', $ex->getMessage());
                    Tpl::showpage('payment_result');
                }
            }
            exit;
        }
        $param['order_sn'] = $order_pay_info['pay_sn'];
        $param['order_amount'] = $order_pay_info['api_pay_amount'];
        $param['order_type'] = $order_type;
        if($this->payment_code == 'lklpay'){
            $member_id = $order_type == 'p'?$order_pay_info['pdr_member_id']:$order_pay_info['buyer_id'];
            $lkl_order_sn = Logic('buy_1')->makePaySn($member_id);
            $lkl_order = Logic('order')->createLklOrder($order_pay_info['pay_sn'],$lkl_order_sn);
//            print_R($order_pay_info);exit;
            $param['subject'] = $order_pay_info['subject'];
            $param['body'] = $order_pay_info['pay_sn'];
            $param['lkl_order'] = $lkl_order_sn;
            $param['member_id'] = $member_id;
//            print_R($param);exit;
            $payment_api = new \Paymax\example\lklpay();
        }else{
            $payment_api = new $this->payment_code();
        }
        $return = $payment_api->submit($param);
        echo $return;
        exit;
    }

    /**
     * 获取订单支付信息
     */
    private function _get_real_order_info($pay_sn,$rcb_pd_pay = array(),$is_app = 0) {
        /** @var paymentLogic $logic_payment */
        $logic_payment = Logic('payment');

        //取订单信息
        $result = $logic_payment->getRealOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            return array('error' => $result['msg']);
        }

        //站内余额支付
        if ($rcb_pd_pay) {
            $result['data']['order_list'] = $this->_pd_pay($result['data']['order_list'],$rcb_pd_pay);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        $pay_order_id_list = array();
        if (!empty($result['data']['order_list'])) {
            foreach ($result['data']['order_list'] as $order_info) {
                if ($order_info['order_state'] == ORDER_STATE_NEW) {
                    $pay_amount += $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] -$order_info['points_amount'];
                    $pay_amount = floatval(ncPriceFormat($pay_amount));
                    $pay_order_id_list[] = $order_info['order_id'];
                }
            }
        }

        if ($pay_amount == 0) {
            if (is_array($rcb_pd_pay) && isset($rcb_pd_pay['inapp'])) {
                return array('error' => 'pay_amount_zero');
            } else {
                if($is_app == 0){
                    redirect(WAP_SITE_URL.'/tmpl/member/order_list.html');
                }
            }
        }

        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);
        /** @var orderModel $oder_model */
        $oder_model = Model('order');
        if($rcb_pd_pay['points_pay'] == 1){
            $update = $oder_model->editOrder(array('api_pay_time'=>TIMESTAMP,'is_points'=>1),array('order_id'=>array('in',$pay_order_id_list)));
        }else{
            $update = $oder_model->editOrder(array('api_pay_time'=>TIMESTAMP),array('order_id'=>array('in',$pay_order_id_list)));
        }

        if(!$update) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }

        //如果是开始支付尾款，则把支付单表重置了未支付状态，因为支付接口通知时需要判断这个状态
        if ($result['data']['if_buyer_repay']) {
            $update = $oder_model->editOrderPay(array('api_pay_state'=>0),array('pay_id'=>$result['data']['pay_id']));
            if (!$update) {
                return array('error' => '订单支付失败');
            }
            $result['data']['api_pay_state'] = 0;
        }

        return $result;
    }

    /**
     * 获取虚拟订单支付信息
     */
    private function _get_vr_order_info($pay_sn,$rcb_pd_pay = array()) {
        /** @var paymentLogic $logic_payment */
        $logic_payment = Logic('payment');

        //取得订单信息
        $result = $logic_payment->getVrOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            output_error($result['msg']);
        }

        //站内余额支付
        if ($rcb_pd_pay) {
            $result['data'] = $this->_pd_vr_pay($result['data'],$rcb_pd_pay);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        if ($result['data']['order_state'] == ORDER_STATE_NEW) {
            $pay_amount += $result['data']['order_amount'] - $result['data']['pd_amount'] - $result['data']['rcb_amount'];
        }

        if ($pay_amount == 0) {
            if (is_array($rcb_pd_pay) && isset($rcb_pd_pay['inapp'])) {
                return array('error' => 'pay_amount_zero');
            } else {
                redirect(WAP_SITE_URL.'/tmpl/member/vr_order_list.html');
            }
        }

        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);
        
        $update = Model('order')->editOrder(array('api_pay_time'=>TIMESTAMP),array('order_id'=>$result['data']['order_id']));
        if(!$update) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }       

        //计算本次需要在线支付的订单总金额
        $pay_amount = $result['data']['order_amount'] - $result['data']['pd_amount'] - $result['data']['rcb_amount'];
        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);

        return $result;
    }

    /**
     * 获取充值订单信息
     * @param $pay_sn
     * @param array $rcb_pd_pay
     * @return array
     */
    private function _get_pd_order_info($pay_sn) {
        $logic_payment = Logic('payment');

        //取得订单信息
        $result = $logic_payment->getPdOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            output_error($result['msg']);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        if ($result['data']['order_state'] == 0) {
            $pay_amount += $result['data']['pdr_amount'];
        }

        if ($pay_amount == 0) {
            redirect(WAP_SITE_URL.'/tmpl/member/predepositlog.html');
        }

        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);

        $data = array('pdr_api_pay_time'=>TIMESTAMP, 'pdr_payment_code'=>$_GET['payment_code'], 'pdr_payment_name'=>orderPaymentName($_GET['payment_code']));
        $update = Model('predeposit')->editPdRecharge($data,array('pdr_id'=>$result['data']['pdr_id']));
        if(!$update) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }

        //计算本次需要在线支付的订单总金额
        $pay_amount = $result['data']['order_amount'] - $result['data']['pd_amount'] - $result['data']['rcb_amount'];
        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);

        return $result;
    }

    private function _get_margin_order_info($pay_sn,$rcb_pd_pay = array(),$is_app = 0) {
        $logic_payment = Logic('payment');

        //取订单信息
        $result = $logic_payment->getMgOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            return array('error' => $result['msg']);
        }
        $order_info = $result['data'];

        //站内余额支付
        if ($rcb_pd_pay) {
//            if($rcb_pd_pay[''])
            $order_info = $this->_pd_pay_margin($order_info,$rcb_pd_pay);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        if ($order_info['order_type'] == 0) {
            $pay_amount = floatval(ncPriceFormat($order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount']));
        }
        if ($pay_amount == 0 && !empty($rcb_pd_pay)) {
            $model_margin_orders = Model('margin_orders');
            $margin_info = $model_margin_orders->getOrderInfo(array('margin_id'=>$order_info['margin_id']));
//            print_R($margin_info);
            $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);
//            dd();
            $day_num = Logic('auction')->getInterestDay(TIMESTAMP,$auction_info['auction_end_time']);//计息天数
            $condition = array(
                    'buyer_id'=>$order_info['buyer_id'],
                    'order_state'=>array('neq','4'),
                    'margin_id'=>array('neq',$order_info['margin_id'])
                );
                $tmp_re = Model('margin_orders')->getOrderInfo($condition);
            if(TIMESTAMP < strtotime($auction_info['interest_last_date']) && $order_info['order_state'] == 1 && ($auction_info['auction_type'] != 1 || empty($tmp_re))){
                $member_distribute_logic = Logic('j_member_distribute');
                //添加利息待结算记录
                //$member_distribute_logic->margin_refund($margin_info,$day_num,$auction_info['auction_bond_rate']);
                //添加佣金待结算记录
                $result_3 = $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);
                //添加区域代理佣金待结算记录
                $member_distribute_logic->agent_margin_refund($margin_info, $day_num,0);
            }
                output_data(0);

        }

        $order_info['api_pay_amount'] = ncPriceFormat($pay_amount);

        $result = Model('margin_orders')->editOrder(array('api_pay_time'=>TIMESTAMP,'payment_code'=>$rcb_pd_pay['payment_code']),array('margin_id'=>$order_info['margin_id']));

        if(!$result) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }
        return $order_info;
    }

    private function _get_auction_order_info($pay_sn,$rcb_pd_pay = array(),$is_app = 0) {
        $logic_payment = Logic('payment');

        //取订单信息
        $result = $logic_payment->getAtOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            return array('error' => $result['msg']);
        }
        $order_info = $result['data'];
        //站内余额支付
        if ($rcb_pd_pay) {
//            if($rcb_pd_pay[''])
            $order_info = $this->_pd_pay_margin($order_info,$rcb_pd_pay);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        if ($order_info['order_type'] == 0) {
            $pay_amount = floatval(ncPriceFormat($order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount']));
        }

//        if ($pay_amount == 0 && !empty($rcb_pd_pay)) {
//                output_data(0);
//        }

        $order_info['api_pay_amount'] = ncPriceFormat($pay_amount);

        $result = Model('margin_orders')->editOrder(array('api_pay_time'=>TIMESTAMP),array('margin_id'=>$order_info['margin_id']));

        if(!$result) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }
        return $order_info;
    }

    /**
     * 可用支付参数列表
     */
    public function payment_listOp() {
        $model_mb_payment = Model('mb_payment');

        $payment_list = $model_mb_payment->getMbPaymentOpenList();

        $payment_array = array();
        if(!empty($payment_list)) {
            foreach ($payment_list as $value) {
                $payment_array[] = $value['payment_code'];
            }
        }

        output_data(array('payment_list' => $payment_array));
    }

    /**
     * 微信APP订单支付
     */
    public function wx_app_payOp() {
        $pay_sn = $_POST['pay_sn'];

        $pay_info = $this->_get_real_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = $pay_info['data']['subject'];
        $param['amount'] = $pay_info['data']['api_pay_amount'] * 100;

        $data = $this->_get_wx_pay_info($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
    }

    /**
     * 微信APP虚拟订单支付
     */
    public function wx_app_vr_payOp() {
        $pay_sn = $_POST['pay_sn'];

        $pay_info = $this->_get_vr_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = $pay_info['data']['subject'];
        $param['amount'] = $pay_info['data']['api_pay_amount'];

        $data = $this->_get_wx_pay_info($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
   }

    /**
     * 获取支付参数
     */
    private function _get_wx_pay_info($pay_param) {
        $access_token = $this->_get_wx_access_token();
        if(empty($access_token)) {
            return array('error' => '支付失败code:1001');
        }

        $package = $this->_get_wx_package($pay_param);

        $noncestr = md5($package + TIMESTAMP);
        $timestamp = TIMESTAMP;
        $traceid = $this->member_info['member_id'];

        // 获取预支付app_signature
        $param = array();
        $param['appid'] = $this->payment_config['wxpay_appid'];
        $param['noncestr'] = $noncestr;
        $param['package'] = $package;
        $param['timestamp'] = $timestamp;
        $param['traceid'] = $traceid;
        $app_signature = $this->_get_wx_signature($param);

        // 获取预支付编号
        $param['sign_method'] = 'sha1';
        $param['app_signature'] = $app_signature;
        $post_data = json_encode($param);
        $prepay_result = http_postdata('https://api.weixin.qq.com/pay/genprepay?access_token=' . $access_token, $post_data);
        $prepay_result = json_decode($prepay_result, true);
        if($prepay_result['errcode']) {
            return array('error' => '支付失败code:1002');
        }
        $prepayid = $prepay_result['prepayid'];

        // 生成正式支付参数
        $data = array();
        $data['appid'] = $this->payment_config['wxpay_appid'];
        $data['noncestr'] = $noncestr;
        $data['package'] = 'Sign=WXPay';
        $data['partnerid'] = $this->payment_config['wxpay_partnerid'];
        $data['prepayid'] = $prepayid;
        $data['timestamp'] = $timestamp;
        $sign = $this->_get_wx_signature($data);
        $data['sign'] = $sign;
        return $data;
    }

    /**
     * 获取微信access_token
     */
    private function _get_wx_access_token() {
        // 尝试读取缓存的access_token
        $access_token = rkcache('wx_access_token');
        if($access_token) {
            $access_token = unserialize($access_token);
            // 如果access_token未过期直接返回缓存的access_token
            if($access_token['time'] > TIMESTAMP) {
                return $access_token['token'];
            }
        }

        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s';
        $url = sprintf($url, $this->payment_config['wxpay_appid'], $this->payment_config['wxpay_appsecret']);
        $re = http_get($url);
        $result = json_decode($re, true);
        if($result['errcode']) {
            return '';
        }

        // 缓存获取的access_token
        $access_token = array();
        $access_token['token'] = $result['access_token'];
        $access_token['time'] = TIMESTAMP + $result['expires_in'];
        wkcache('wx_access_token', serialize($access_token));

        return $result['access_token'];
    }

    /**
     * 获取package
     */
    private function _get_wx_package($param) {
        $array = array();
        $array['bank_type'] = 'WX';
        $array['body'] = $param['subject'];
        $array['fee_type'] = 1;
        $array['input_charset'] = 'UTF-8';
        $array['notify_url'] = MOBILE_SITE_URL . '/api/payment/wxpay/notify_url.php';
        $array['out_trade_no'] = $param['pay_sn'];
        $array['partner'] = $this->payment_config['wxpay_partnerid'];
        $array['total_fee'] = $param['amount'];
        $array['spbill_create_ip'] = get_server_ip();

        ksort($array);

        $string = '';
        $string_encode = '';
        foreach ($array as $key => $val) {
            $string .= $key . '=' . $val . '&';
            $string_encode .= $key . '=' . urlencode($val). '&';
        }

        $stringSignTemp = $string . 'key=' . $this->payment_config['wxpay_partnerkey'];
        $signValue = md5($stringSignTemp);
        $signValue = strtoupper($signValue);

        $wx_package = $string_encode . 'sign=' . $signValue;
        return $wx_package;
    }

    /**
     * 获取微信支付签名
     */
    private function _get_wx_signature($param) {
        $param['appkey'] = $this->payment_config['wxpay_appkey'];

        $string = '';

        ksort($param);
        foreach ($param as $key => $value) {
            $string .= $key . '=' . $value . '&';
        }
        $string = rtrim($string, '&');

        $sign = sha1($string);

        return $sign;
    }

    /**
     * 微信APP订单支付
     */
    public function wx_app_pay3Op() {
        $app_type = $_POST['app_type'];
        $pay_sn = $_POST['pay_sn'];
        if (isset($_GET['opex']) && $_GET['opex'] == 'pd_pay_new') {
            $pay_info = $this->_get_pd_order_info($pay_sn);
            $pay_info['data']['api_pay_amount'] = $pay_info['data']['pdr_amount'];
            $no_credit = true; //微信APP充值禁用信用卡
        }elseif(isset($_GET['margin']) && $_GET['margin'] == 1){
            $_POST['payment_code'] = 'wxpay';
            $tmp_info = $this->_get_margin_order_info($pay_sn,$_POST);
            $pay_info['data'] = $tmp_info;
            $no_credit = true; //微信APP支付保证金禁用信用卡
        } else {
            $pay_info = $this->_get_real_order_info($pay_sn);
        }
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = $pay_info['data']['subject'];
        $param['amount'] = $pay_info['data']['api_pay_amount'] * 100;
        $param['no_credit'] = $no_credit ? true : false;

        $data = $this->_get_wx_pay_info3($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
    }

    /**
     * 微信APP虚拟订单支付
     */
    public function wx_app_vr_pay3Op() {
        $pay_sn = $_POST['pay_sn'];

        $pay_info = $this->_get_vr_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = $pay_info['data']['subject'];
        $param['amount'] = $pay_info['data']['api_pay_amount'] * 100;

        $data = $this->_get_wx_pay_info3($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
   }

    /**
     * 获取支付参数
     */
    private function _get_wx_pay_info3($pay_param) {
        $noncestr = md5(rand());

        $param = array();
        $param['appid'] = $this->payment_config['wxpay_appid'];
        $param['mch_id'] = $this->payment_config['wxpay_partnerid'];
        $param['nonce_str'] = $noncestr;
        $param['body'] = $pay_param['subject'];
        $param['out_trade_no'] = $pay_param['pay_sn'];
        $param['total_fee'] = $pay_param['amount'];
        $param['spbill_create_ip'] = get_server_ip();
        $param['notify_url'] = MOBILE_SITE_URL . '/api/payment/wxpay3/notify_url.php';
        $param['trade_type'] = 'APP';
        $pay_param['no_credit'] ? $param['limit_pay'] = 'no_credit' : '';
        $sign = $this->_get_wx_pay_sign3($param);
        $param['sign'] = $sign;
        $post_data = '<xml>';
        foreach ($param as $key => $value) {
            $post_data .= '<' . $key .'>' . $value . '</' . $key . '>';
        }
        $post_data .= '</xml>';

        $prepay_result = http_postdata('https://api.mch.weixin.qq.com/pay/unifiedorder', $post_data);
        $prepay_result = simplexml_load_string($prepay_result, 'SimpleXMLElement', LIBXML_NOCDATA);
        if($prepay_result->return_code != 'SUCCESS') {
            return array('error' => '支付失败code:1002');
        }

        // 生成正式支付参数
        $data = array();
        $data['appid'] = $this->payment_config['wxpay_appid'];
        $data['noncestr'] = $noncestr;
        //微信修改接口参数，否则IOS报解析失败
        //$data['package'] = 'prepay_id=' . $prepay_result->prepay_id;
        $data['package'] = 'Sign=WXPay';
        $data['partnerid'] = $this->payment_config['wxpay_partnerid'];
        $data['prepayid'] = (string)$prepay_result->prepay_id;
        $data['timestamp'] = TIMESTAMP;
        $sign = $this->_get_wx_pay_sign3($data);
        $data['sign'] = $sign;
        return $data;
    }

    private function _get_wx_pay_sign3($param) {
        ksort($param);
        foreach ($param as $key => $val) {
            $string .= $key . '=' . $val . '&';
        }
        $string .= 'key=' . $this->payment_config['wxpay_partnerkey'];
        return strtoupper(md5($string));
    }

    /**
     * 取得支付宝移动支付 订单信息 实物订单
     */
    public function alipay_native_payOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        if (isset($_GET['opex']) && $_GET['opex'] == 'pd_pay_new') {
            $pay_info = $this->_get_pd_order_info($pay_sn);
            $pay_info['data']['api_pay_amount'] = $pay_info['data']['pdr_amount'];
        } else {
            $pay_info = $this->_get_real_order_info($pay_sn,$_POST);
        }
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }
        
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $pay_info['data']['order_type'] = 'r';
        $payment_api = new $this->payment_code();
        $payment_api->init($this->payment_config,$pay_info['data']);
        $prestr = 'partner="'.$payment_api->param['partner']
        .'"&seller_id="'.$payment_api->param['seller_id']
        .'"&out_trade_no="'.$payment_api->param['out_trade_no']
        .'"&subject="'.$payment_api->param['subject'].'"&body="r"&total_fee="'
        .$payment_api->param['total_fee'].'"&notify_url="'
        .$payment_api->param['notify_url'].'"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1"';
        $mysign = $payment_api->mySign($prestr);
        output_data(array('signStr'=>$prestr.'&sign_type="RSA"&sign="'.urlencode($mysign).'"'));
    }

    public function alipay_native_pay_newOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        if (isset($_GET['opex']) && $_GET['opex'] == 'pd_pay_new') {
            $pay_info = $this->_get_pd_order_info($pay_sn);
            $pay_info['data']['api_pay_amount'] = $pay_info['data']['pdr_amount'];
            $disable_pay_channels = 'creditCard,creditCardExpress,creditCardCartoon';//充值不能用信用卡支付
        } elseif (isset($_GET['margin']) && $_GET['margin'] == 1){
            $_POST['payment_code'] = 'alipay';
            $tmp_info = $this->_get_margin_order_info($pay_sn,$_POST);
            $pay_info['data'] = $tmp_info;
            $disable_pay_channels = 'creditCard,creditCardExpress,creditCardCartoon';//提交保证金不能用信用卡支付
        } elseif (isset($_GET['auction']) && $_GET['auction'] == 1){
            $tmp_info = $this->_get_auction_order_info($pay_sn,$_POST);
            $pay_info['data'] = $tmp_info;

        } else {
            $pay_info = $this->_get_real_order_info($pay_sn,$_POST,1);
        }
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'AopClient.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $aop = new AopClient;
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        $aop->appId = 2017063007608325;
        $aop->rsaPrivateKey = 'MIICXAIBAAKBgQCuVM6xnG1mmjBh8k5weX7vP4NUwZCvGHO16P2HsETBChXvQwvPXe3aQYitNL7kU0S9X7VBV+3LxPF6jqJ2NyPE6JlTLafEeQddL3vznXlGv4ijgxiG78E9mOO6kleLxiEQtihQQhQHu3dRZrhPV0VUtjdU/f9FOszwkwhu2R5odQIDAQABAoGAFDSoMFXdKEM+0FtxVAABAmHuKui0iFbhMnhqyktX7LfqiuzOdZ6BbwahfNHcPkKYeQqay5QRb8jH5Fib4+/IKV11WCvC5kGM1bjqO8QpByd+RAulMnmEX7M/l/lb3KDJ6bEeZgM7F95gu2owNJXTqbyUJ0mBgawJ+SbMr3qSqhECQQDdkomUjfM0yZ5J0kYNYPLjG3e4aUTnIWh9TE47ae9awC9MoVCwU8O47yqQHqao0DoMw+qq/QvR0Zo3T4AJ1Ej3AkEAyWsqFUpHv1nx7ig9UnYKxxQ2pZIRaVIB4G9G1LWZTAR0HaCkHV/oj4VCpSN5S6KMkPyneJfjfEVxBjE9SviK8wJABho8GdBTC3gmGOhmr4WlCuY9xOF5WVhNNW49lVtUkU5LvzOOMl0MPfKwXGnLs0iQ4Lsgonb3tV6tfap930dufwJBALNsLxTAEqG2cfkBB39Jf9hPfU6Ii9ISJ3HSLnqVOnWpEfbCfu9b3ELdJr0MmKRzrFwLdPPL+e1dvo0Rl9QNC1kCQDF8LVsaC3Tjs+xtGh9a3k1lkXiVmLYflgt0350W/hhy7I62SpBhBQgPJk2ajAM9r5ELvXwnfamw546VtqLdSis=';
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA";
        $aop->alipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB';
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        require(BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'request'.DS.'AlipayTradeAppPayRequest.php');
        $request = new AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $tmp = array(
            'body'=>'r',
            'timeout_express'=>'30m',
            'subject'=>$pay_info['data']['subject'],
            'out_trade_no'=>$pay_info['data']['pay_sn'],
            'total_amount'=>$pay_info['data']['api_pay_amount'],
            'product_code'=>'QUICK_MSECURITY_PAY'
        );
        $disable_pay_channels ? $tmp['disable_pay_channels'] = $disable_pay_channels : '';
        $bizcontent = json_encode($tmp);
//        $bizcontent = "{\"body\":\"r\","
//                        . "\"subject\": \"{$pay_info['subject']}\","
//                        . "\"out_trade_no\": \"{$pay_info['pay_sn']}\","
//                        . "\"timeout_express\": \"30m\","
//                        . "\"total_amount\": \"{$pay_info['api_pay_amount']}\","
//                        . "\"product_code\":\"QUICK_MSECURITY_PAY\""
//                        . "}";
        $request->setNotifyUrl(MOBILE_SITE_URL.'/api/payment/alipay_aopclient/notify_url.php');
        $request->setBizContent($bizcontent);
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
//        echo htmlspecialchars($response);//就是orderString 可以直接给客户端请求，无需再做处理。
        output_data(array('signStr'=>$response,'code'=>$pay_info['data']['api_pay_amount'] > 0?0:1000));
    }

    /**
     * 取得支付宝移动支付 订单信息 虚拟订单
     */
    public function alipay_native_vr_payOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        $pay_info = $this->_get_vr_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $pay_info['data']['order_type'] = 'v';
        $payment_api = new $this->payment_code();
        $payment_api->init($this->payment_config,$pay_info['data']);
        $prestr = 'partner="'.$payment_api->param['partner']
        .'"&seller_id="'.$payment_api->param['seller_id']
        .'"&out_trade_no="'.$payment_api->param['out_trade_no']
        .'"&subject="'.$payment_api->param['subject'].'"&body="v"&total_fee="'
        .$payment_api->param['total_fee'].'"&notify_url="'
        .$payment_api->param['notify_url'].'"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1"';
        $mysign = $payment_api->mySign($prestr);
        output_data(array('signStr'=>$prestr.'&sign_type="RSA"&sign="'.urlencode($mysign).'"'));

    }

    /**
     * 站内余额支付(充值卡、预存款支付) 保证金订单
     * @param $order_info array 订单信息
     * @param $post array POST数据
     * @param $type int 0 保证金 1 尾款
     * @return array order_info
     */
    private function _pd_pay_margin($order_info, $post, $type = 0) {
        if (empty($post['password'])) {
            return $order_info;
        }
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($post['password'])) {
            return $order_info;
        }

        if ($buyer_info['available_rc_balance'] == 0) {
            $post['rcb_pay'] = null;
        }
        if ($buyer_info['available_predeposit'] == 0) {
            $post['pd_pay'] = null;
        }
        if (floatval($order_info['rcb_amount']) > 0 || floatval($order_info['pd_amount']) > 0) {
            return $order_info;
        }
        try {
            $model_member->beginTransaction();
            $logic_auction_pay = Logic('auction_pay');
            //使用充值卡支付
            if (!empty($post['rcb_pay'])) {
                $order_info = $logic_auction_pay->rcbPay($order_info, $post, $buyer_info, $type);
            }

            //使用预存款支付
            if (!empty($post['pd_pay'])) {
                $order_info = $logic_auction_pay->pdPay($order_info, $post, $buyer_info, $type);
            }

            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            exit($e->getMessage());
        }
        $this->_sendAuctionsMarginMessage($order_info);

        return $order_info;
    }

    /**
     * 发送客户系统通知
     * @param $order_info
     */
    private function _sendAuctionsMarginMessage($order_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $order_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        /** @var margin_ordersModel $model_margin_order */
        $model_margin_order = Model('margin_orders');
        //已交保证金金额
        $getMarginSum=$model_margin_order->getMarginSum(
            [
                'auction_id' => $order_info['auction_id'],
                'buyer_id' => $order_info['buyer_id'],
                'order_state' => 1
            ],
            'margin_amount'
        );
        $margin_count = $getMarginSum ?: 0;
        $sendParam = [
            'code'     =>'aucution_margin_pay',
            'member_id'=>intval($order_info['buyer_id']),
            'number'   =>['mobile'=>intval($order_info['buyer_phone'])],
            'param'    =>[
                'wx_first'=>'保证金支付成功',
                'wx_remark'=>'请及时关注拍卖动态',
                'auction_name'=>$order_info['auction_name'],
                'margin_amount'=>$order_info['margin_amount'],
                'money'=>$order_info['margin_amount'],
                'margin_count' => $margin_count,
                'url' => $url,
                'wx_url' => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $sendParam);
    }

}
