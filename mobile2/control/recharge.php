<?php
/**
 * 拍卖
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class rechargeControl extends mobileMemberControl
{
	public function __construct()
	{
		parent::__construct();
	}

	public function indexOp(){

	}

    /**
     * 创建充值订单
     */
	public function add_rechargeOp(){
	    $pdr_amount = abs(floatval($_POST['amount']));
        if ($pdr_amount <= 0) {
            //充值金额非法
            output_error('非法输入');
        }
        if ($pdr_amount < 10) {
            //最低充值10元
            output_error('最低充值金额≥10元');
        }
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        $model_pdr = Model('predeposit');
        $data = array();
        $data['pdr_sn'] = $pay_sn = $model_pdr->makeSn();
        $data['pdr_member_id'] = $this->member_info['member_id'];
        $data['pdr_member_name'] = $this->member_info['member_name'];
        $data['pdr_amount'] = $pdr_amount;
        $data['pdr_add_time'] = TIMESTAMP;
        $insert = $model_pdr->addPdRecharge($data);
//        output_data($insert);
        if(!$insert){
            output_error('错误!');
        }
        output_data($data);
    }

    /**
     * 选择充值订单渠道
     */
    public function choose_channelOp(){
        $logic_payment = Logic('payment');
        $order = $logic_payment->getPdOrderInfo($_POST['pay_sn'],$this->member_info['member_id']);
//        print_R($result);exit;
        $payment_list = Model('mb_payment')->getMbPaymentOpenList();
        foreach ($payment_list as $k => $value) {
             if ($value['payment_code'] == 'wxpay') {
                 unset($payment_list[$k]);
                 continue;
             }
             if($order['data']['pdr_amount'] < LKLPAY){
                if ($value['payment_code'] == 'lklpay') {
                     unset($payment_list[$k]);
                     continue;
                 }
             }else{
                if ($value['payment_code'] == 'alipay') {
                     unset($payment_list[$k]);
                     continue;
                 }
             }
//            unset($payment_list[$k]['payment_id']);
//            unset($payment_list[$k]['payment_config']);
//            unset($payment_list[$k]['payment_state']);
//            unset($payment_list[$k]['payment_state_text']);
        }
//        print_R($payment_list);exit;
        $pay_info = array(
            'member_available_pd'=>0,
            'member_available_rcb'=>0,
            'pay_amount'=>$order['data']['pdr_amount'],
            'payment_list'=>$payment_list?array_values($payment_list):array()
        );
        output_data(array('pay_info'=>$pay_info));

    }

}