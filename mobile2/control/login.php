<?php
/**
 * 前台登录 退出操作
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class loginControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 登录
     */
    public function indexOp(){
        if(empty($_POST['username']) || empty($_POST['password']) || !in_array($_POST['client'], $this->client_type_array)) {
            output_error('登录失败');
        }

        /** @var memberModel $model_member */
        $model_member = Model('member');
        $login_info = array();
        $login_info['user_name'] = $_POST['username'];
        $login_info['password'] = $_POST['password'];
        $member_info = $model_member->login($login_info);
        if(isset($member_info['error'])) {
            output_error($member_info['error']);
        } else {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if($token) {
                $this->bindWeiXin($member_info['member_id'],cookie('weixin_info'));
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token, 'usertype'=>$member_info['member_type']));
            } else {
                output_error('登录失败');
            }
        }
    }

    /**
     * 登录生成token
     */
    private function _get_token($member_id, $member_name, $client) {
        $model_mb_user_token = Model('mb_user_token');

        //重新登录后以前的令牌失效
        //暂时停用
        //$condition = array();
        //$condition['member_id'] = $member_id;
        //$condition['client_type'] = $client;
        //$model_mb_user_token->delMbUserToken($condition);

        //生成新的token
        $mb_user_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_user_token_info['member_id'] = $member_id;
        $mb_user_token_info['member_name'] = $member_name;
        $mb_user_token_info['token'] = $token;
        $mb_user_token_info['login_time'] = TIMESTAMP;
        $mb_user_token_info['client_type'] = $client;

        /** @var mb_user_tokenModel $model_mb_user_token */
        $result = $model_mb_user_token->addMbUserToken($mb_user_token_info);

        if($result) {
            return $token;
        } else {
            return null;
        }

    }

    /**
     * 注册
     */
    public function registerOp(){
        $model_member   = Model('member');
//        if(trim($_POST['dis_code']) != ''){
//            $return = $this->check_dcode(trim($_POST['dis_code']));
//            if($return == false){
//                output_error('邀请码不存在');
//            }
//        }

        $register_info = array();
        $register_info['username'] = $_POST['username'];
        $register_info['password'] = $_POST['password'];
        $register_info['password_confirm'] = $_POST['password_confirm'];
//        $register_info['email'] = $_POST['email'];
        $register_info['dis_code'] = trim($_POST['dis_code']);
        $register_info['registered_source'] = isset($_POST['client']) ? $_POST['client'] : '';
        $register_info['source_staff_id'] = isset($_POST['source_staff_id']) ? $_POST['source_staff_id'] : 0;
        $member_info = $model_member->register($register_info);
        if(!isset($member_info['error'])) {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if($token) {
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token));
            } else {
                output_error('注册失败');
            }
        } else {
            output_error($member_info['error']);
        }

    }

    /**
     * 获取手机短信验证码
     */
    public function get_sms_captchaOp(){

        $http_referer = $_SERVER['HTTP_REFERER'] ?: $_SERVER['HTTP_ORIGIN'];
        $sign = $_GET['sign'];
        $ynys_check = md5($_GET['phone'].'ynys') == $sign?true:false;
        if (
            (empty($http_referer)
                || !strpos($http_referer, 'yinuovip')
                || !strpos($http_referer, 'servicewechat.com')
                || !strpos($http_referer, '127.0.0.1'))
            && ($sign == '' || ($sign != '' && !$ynys_check))
        ) {
            exit();
        }

        $phone = $_GET['phone'];
        $log_type = $_GET['type'];//短信类型:1为注册,2为登录,3为找回密码
        $state_data = array(
            'state' => false,
            'msg' => '验证码或手机号码不正确'
        );

        if (strlen($phone) == 11){
            $logic_connect_api = Logic('connect_api');
            $state_data = $logic_connect_api->sendCaptcha($phone, $log_type);
        }
        $this->connect_output_data($state_data);
    }
    /**
     * 验证手机验证码
     */
    public function check_sms_captchaOp(){
        $phone = $_GET['phone'];
        $log_type = $_GET['type'];
        $state_data = $this->checkSmsCaptcha($phone, $log_type);
        $this->connect_output_data($state_data, 1);
    }

    /**
     * 手机注册
     */
    public function sms_registerOp(){
        $phone = $_POST['phone'];
        $password = $_POST['password'];
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array(
                "input" => $phone,
                "require" => "true",
                "message" => "请填写用户名"
            ),

            array(
                "input" => $password,
                "require" => "true",
                "message" => "密码不能为空"
            ),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            $state['state'] = false;
            $state['msg'] = $error;
            $this->connect_output_data($state);
        }

        $dis_code = '';

        if(!empty($_COOKIE['invite_code']) && $_COOKIE['invite_code'] != 'null'){
            $invite_code = trim($_COOKIE['invite_code']);
            $state['state'] = false;
            $model_member = Model('member');
            $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>$invite_code));
            if(empty($check_member_dcode) /*|| in_array($check_member_dcode['member_type'],array(4))*/){
                $state['msg'] = '分享链接错误，无法注册绑定上级，请检查后再注册。';
                $invite_code = '';
                //$this->connect_output_data($state);
            }
            $dis_code = $invite_code;
        }
        $regArr = [
            'source'=>$_POST['source'],
            'captcha'=>$_POST['captcha'],
            'password'=>$password,
            'client'=>$_POST['client'],
            'member_areaid'=>$_POST['member_areaid'],
            'member_cityid'=>$_POST['member_cityid'],
            'member_provinceid'=>$_POST['member_provinceid'],
            'member_areainfo'=>$_POST['member_areainfo']
        ];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsRegister($phone, $dis_code, $regArr);

        //绑定微信
        $this->bindWeiXin($state_data['userid'],cookie('weixin_info'));
        if ($_POST['ex_id'] && $invite_code){
            Logic('register')->exhibition_reg($state_data['userid'],$check_member_dcode['member_id'],$_POST['ex_id']);
        }

        $this->connect_output_data($state_data);
    }

    /**
     * 绑定微信
     * @param int $member_id
     * @param array $weixin_info
     * @return bool
     */
    private function bindWeiXin($member_id, $weixin_info){
        $user_info = unserialize($weixin_info);
        if (empty($user_info['unionid'])){
            return false;
        }
        $model_member = Model('member');
        $condition = ['member_id'=>$member_id];
        $member_info = $model_member->getMemberInfo($condition, $field = 'weixin_unionid');
        if (!$member_info['weixin_unionid'] || ($member_info['weixin_unionid'] == trim($user_info['unionid']))){
            $update_data = [
                'weixin_unionid'=>trim($user_info['unionid']),
                'member_sex' => $user_info['sex'],
                'member_avatar' => $user_info['headimgurl'],
                'weixin_open_id' => trim($user_info['openid']),
                'weixin_info' => serialize([
                    'nickname'=>$user_info['nickname'],
                    'openid'=>$user_info['openid'],
                ])
            ];
            if(empty($member_info['member_truename'])){
                $update_data['member_truename'] = $user_info['nickname'];
            }
            return $model_member->editMember(['member_id'=>$member_id],$update_data);
        }
        return false;
    }

    /**
     * 格式化输出数据
     */
    public function connect_output_data($state_data, $type = 0){
        if($state_data['state']){
            unset($state_data['state']);
            unset($state_data['msg']);
            if ($type == 1){
                $state_data = 1;
            }
            output_data($state_data);
        } else {
            output_error($state_data['msg']);
        }
    }


    /**
     * 邀请码检测
     *
     * @param
     * @return
     */
    private function check_dcode($dcode) {
        $model_member = Model('member');
        $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>$dcode));
        if(is_array($check_member_dcode) && count($check_member_dcode)>0 || $dcode == '') {
            return true;
        } else {
            return false;
        }
    }
}
