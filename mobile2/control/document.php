<?php
/**
 * 前台品牌分类
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class documentControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }

    public function agreementOp() {
        $doc = Model('document')->getOneByCode('agreement');
        output_data($doc);
    }

    /*
     * 获取文章详细
     * */
    public function get_articleOp()
    {
        $ac_id = intval($_GET['ac_id']);
        $article_position = intval($_GET['article_position']);
        $model_article = Model('article');
        $article_info = $model_article->getArticleOne($ac_id, $article_position);
        output_data($article_info);
    }
}
