<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/12/14 0014
 * Time: 17:13
 */


use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class testControl extends mobileHomeControl
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexOp()
    {

        echo MD5_KEY;die;
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $condition['special_end_time'] = 0;
        $condition['special_state'] = 20;
        $condition['store_id']=0;
        $condition['is_open']=1;
        $special_list = $model_special->getSpecialList($condition, '*', 'special_id desc', 0);

        output_data(count($special_list));
        //提醒前一个用户竞价被超越
        /** @var member_msg_tplModel $member_msg_tpl */
        $auction_info = MOdel('auctions')->where(['auction_id' => 4408])->find();
        $member_msg_tpl = Model('member_msg_tpl');
        $sendParam = [
            'member_id' => 105081,
            'mobile' => 17612157796,
            'current_price' => 12
        ];
        $res = $member_msg_tpl->sendBeyondPriceMessage($sendParam, $auction_info);
        output_data($res);

        output_data('done');
        $model_auctions = Model('auctions');
        $condition['is_liupai'] = 0;
        $condition['state'] = 0;
        $condition['auction_end_true_t'] = array('between','0,' . TIMESTAMP);
        $list = $model_auctions->getAuctionList($condition);
        print_r($list);

        die;
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $condition['special_end_time'] = 0;
        $condition['special_state'] = 20;
        $condition['store_id']=0;
        $condition['is_open']=1;
        $special_list = $model_special->getSpecialList($condition);
        if (!empty($special_list)) {
            foreach ($special_list as $key => $item) {
                $map['special_id']=$item['special_id'];
                $map['state']=0;
                $num=$model_auctions->where($map)->count();
                $arr[$map['special_id']] = $num;
                if (!$num) {
                    $update = array('special_end_time' => TIMESTAMP);
                    // 更新成结束状态
                    $model_special->editSpecial($update, array('special_id'=>$item['special_id']));
                    echo "专场ID".$item['special_id']."关闭<br/>";
                }
            }
        }
        output_data($arr);
//        $auction_id = 4336;
//        $margin_id = 2113;
//        /** @var auctionLogic $auction_logic */
//        $auction_logic = Logic('auction');
//        /** @var auctionsModel $auction_model */
//        $auction_model = Model('auctions');
//        /** @var margin_ordersModel $margin_orders_model */
//        $margin_orders_model = Model('margin_orders');
//        $auction_info = $auction_model->getAuctionsInfoByID($auction_id);
//        $margin_info = $margin_orders_model->getOrderInfo(array('margin_id'=>$margin_id));
//        $day_num = $auction_logic->getInterestDay(TIMESTAMP, $auction_info['auction_start_time']);//计息天数
//        if (TIMESTAMP < strtotime($auction_info['interest_last_date'])
//            && $margin_info['order_state'] == 1 && $auction_info['auction_type'] != 1)
//        {
//            /** @var j_member_distributeLogic $member_distribute_logic */
//            $member_distribute_logic = Logic('j_member_distribute');
//            //添加利息待结算记录
//            //添加佣金待结算记录
//            $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num, 0);
//
//            //添加区域代理佣金待结算记录
//            $member_distribute_logic->agent_margin_refund($margin_info, $day_num, 0);
//        }
//        echo 'done';
//        die;
    }


    /**
     * 实物订单支付 新方法
     */
    public function pay_newOp() {
        @header("Content-type: text/html; charset=".CHARSET);
        /** @var mb_paymentModel $model_mb_payment */
        $pay_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{18}$/',$pay_sn)){
            exit('支付单号错误');
        }
        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi','lklpay'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];
        } else {
            exit('支付方式提交错误');
        }

        $pay_info = $this->_get_real_order_info($pay_sn,$_GET);

        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }
        if (isset($_GET['inapp'])) {
            output_data(array('state' => 'success'));
            return;
        }
        output_data('ok');
        //第三方API支付
        $this->_api_pay($pay_info['data']);


    }


    /**
     * 第三方在线支付接口
     *
     */
    private function _api_pay($order_pay_info)
    {
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';
        if (!is_file($inc_file)) {
            exit('支付接口不存在');
        }
        require($inc_file);
        $param = $this->payment_config;

        $order_type = 'r';
        switch ($order_pay_info['order_type']) {
            case 'real_order':
                $order_type = 'r';
                break;
            case 'vr_order':
                $order_type = 'v';
                break;
            case 'pd_order':
                $order_type = 'p';
                $param['limit_pay'] = 'no_credit'; //微信wap充值禁用信用卡
                break;
        }
        if ($this->payment_code == 'wxpay_jsapi') {
            $param['orderSn'] = $order_pay_info['pay_sn'];
            if ($order_pay_info['order_type'] == 'pd_order') {
                $param['orderFee'] = (int)(ncPriceFormat(100 * $order_pay_info['pdr_amount']));
            } else {
                $param['orderFee'] = (int)(ncPriceFormat(100 * $order_pay_info['api_pay_amount']));
            }
            $param['orderInfo'] = $order_pay_info['pay_sn'] . '订单';
            $param['orderAttach'] = $order_type;
            $api = new wxpay_jsapi();
            $api->setConfigs($param);
            try {
                echo $api->paymentHtml($this);
            } catch (Exception $ex) {
                if (C('debug') or 1) {
                    header('Content-type: text/plain; charset=utf-8');
                    echo $ex, PHP_EOL;
                } else {
                    Tpl::output('msg', $ex->getMessage());
                    Tpl::showpage('payment_result');
                }
            }
            exit;
        }
    }


    /**
     * 获取订单支付信息
     */
    private function _get_real_order_info($pay_sn,$rcb_pd_pay = array(),$is_app = 0) {
        /** @var paymentLogic $logic_payment */
        $logic_payment = Logic('payment');

        //取订单信息
        $result = $logic_payment->getRealOrderInfo($pay_sn, $this->member_info['member_id']);
        if(!$result['state']) {
            return array('error' => $result['msg']);
        }

        //站内余额支付
        if ($rcb_pd_pay) {
            $result['data']['order_list'] = $this->_pd_pay($result['data']['order_list'],$rcb_pd_pay);
        }
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        $pay_order_id_list = array();
        if (!empty($result['data']['order_list'])) {
            foreach ($result['data']['order_list'] as $order_info) {
                if ($order_info['order_state'] == ORDER_STATE_NEW) {
                    $pay_amount += $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] -$order_info['points_amount'];
                    $pay_amount = floatval(ncPriceFormat($pay_amount));
                    $pay_order_id_list[] = $order_info['order_id'];
                }
            }
        }

        if ($pay_amount == 0) {
            if (is_array($rcb_pd_pay) && isset($rcb_pd_pay['inapp'])) {
                return array('error' => 'pay_amount_zero');
            } else {
                if($is_app == 0){
                    redirect(WAP_SITE_URL.'/tmpl/member/order_list.html');
                }
            }
        }

        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);
        /** @var orderModel $oder_model */
        $oder_model = Model('order');
        if($rcb_pd_pay['points_pay'] == 1){
            $update = $oder_model->editOrder(array('api_pay_time'=>TIMESTAMP,'is_points'=>1),array('order_id'=>array('in',$pay_order_id_list)));
        }else{
            $update = $oder_model->editOrder(array('api_pay_time'=>TIMESTAMP),array('order_id'=>array('in',$pay_order_id_list)));
        }

        if(!$update) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }

        //如果是开始支付尾款，则把支付单表重置了未支付状态，因为支付接口通知时需要判断这个状态
        if ($result['data']['if_buyer_repay']) {
            $update = $oder_model->editOrderPay(array('api_pay_state'=>0),array('pay_id'=>$result['data']['pay_id']));
            if (!$update) {
                return array('error' => '订单支付失败');
            }
            $result['data']['api_pay_state'] = 0;
        }

        return $result;
    }

    public function jsapiOp()
    {
        //        print_R($order_pay_info);exit;
        $order_pay_info = [
            'order_type' => 'p'
        ];
        $this->payment_code = 'wxpay_jsapi';
        $model_mb_payment = Model('mb_payment');
        $condition = array();
        $condition['payment_code'] = $this->payment_code;
        $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
        if(!$mb_payment_info) {
            exit('支付方式未开启');
        }
        $this->payment_config = $mb_payment_info['payment_config'];
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';
        if (!is_file($inc_file)) {
            exit('支付接口不存在');
        }
        require($inc_file);
        $param = $this->payment_config;

        $order_type = 'r';
        switch ($order_pay_info['order_type']) {
            case 'real_order':
                $order_type = 'r';
                break;
            case 'vr_order':
                $order_type = 'v';
                break;
            case 'pd_order':
                $order_type = 'p';
                $param['limit_pay'] = 'no_credit'; //微信wap充值禁用信用卡
                break;
        }
        if ($this->payment_code == 'wxpay_jsapi') {
            $param['orderSn'] = $order_pay_info['pay_sn'];
            if ($order_pay_info['order_type'] == 'pd_order') {
                $param['orderFee'] = (int)(ncPriceFormat(100 * $order_pay_info['pdr_amount']));
            } else {
                $param['orderFee'] = (int)(ncPriceFormat(100 * $order_pay_info['api_pay_amount']));
            }
            $param['orderInfo'] = $order_pay_info['pay_sn'] . '订单';
            $param['orderAttach'] = $order_type;
            $api = new wxpay_jsapi();
            $api->setConfigs($param);
            try {
                echo $api->paymentHtml($this);
            } catch (Exception $ex) {
                if (C('debug') or 1) {
                    header('Content-type: text/plain; charset=utf-8');
                    echo $ex, PHP_EOL;
                } else {
                    Tpl::output('msg', $ex->getMessage());
                    Tpl::showpage('payment_result');
                }
            }
            exit;
        }
    }
}