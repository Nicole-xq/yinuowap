<?php
/**
 * 第三方账号登录
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class connectControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }

    /**
     * 登录开关状态
     */
    public function get_stateOp() {
        $logic_connect_api = Logic('connect_api');
        $state_array = $logic_connect_api->getStateInfo();
        
        $key = $_GET['t'];
        if(trim($key) != '' && array_key_exists($key,$state_array)){
            output_data($state_array[$key]);
        } else {
            output_data($state_array);
        }
    }
    /**
     * WAP页面微信登录回调
     */
    public function indexOp(){
        /** @var connect_apiLogic $logic_connect_api */
        $logic_connect_api = Logic('connect_api');
        $invite_code = $_GET['invite_code'];
        $ex_id = $_GET['ex_id'];
        $param_str = '&redirect_url='.$_REQUEST['redirect_url'];
        $param_str .= ($invite_code && $invite_code != 'null') ? '&invite_code='.$invite_code : '';
        $param_str .= ($ex_id && $ex_id != 'null') ? '&ex_id='.$ex_id : '';

        if(!empty($_GET['code'])) {
            $code = $_GET['code'];
            $client = 'wap';
            $user_info = $logic_connect_api->getWxUserInfo($code,'wap');
            if(!empty($user_info['unionid'])){
                redirect(self::checkWxMember($user_info,$param_str));
            } else {
                output_error('微信登录失败');
            }
        } else {
            $_url = $logic_connect_api->getWxOAuth2Url($param_str);
            @header("location: ".$_url);
        }
    }
    /**
     * 检测微信会员
     * @param array $user_info
     * @param array $param_str
     * @return string
     */
    private function checkWxMember($user_info,$param_str){
        $model_member = Model('member');
        $member = $model_member->getMemberInfo(array('weixin_unionid'=> $user_info['unionid']));
        $state_data = array();
        if(!empty($member)) {//会员信息存在时自动登录
            $token = Logic('connect_api')->getUserToken($member, $client = 'wap');
            $member['weixin_open_id'] ? '' : $model_member->editMember(['member_id'=>$member['member_id']],['weixin_open_id'=>$user_info['openid']]);
            $state_data['key'] = $token;
            $state_data['username'] = $member['member_name'];
            $state_data['userid'] = $member['member_id'];
            $state_data['usertype'] = $member['member_type'];

            $url_params = array(
                'username' => $state_data['username'],
                'key' => $state_data['key'],
                'usertype' => $state_data['usertype'],
                'redirect_url' => $_REQUEST['redirect_url'],
            );
            return WAP_SITE_URL.'/tmpl/member/member.html?'.http_build_query($url_params);
        } else {//自动注册会员并登录
            setNcCookie('weixin_info',$user_info['weixin_info']);
            return WAP_SITE_URL.'/tmpl/member/register.html?unionid='.$user_info['unionid'].$param_str;
        }
    }

    /**
     * app授权完毕后登陆
     */
    public function appAuthorizeLoginOp(){
        $weixin_info = [
            'weixin_info'=>$_POST,
            'unionid'=>$_POST['unionid'],
        ];
        $ref_url = self::checkWxMember($weixin_info,[]);
        output_data($ref_url);
    }

    /**
     * WAP页面微信完善信息注册
     */
    public function wxRegisterOp(){
        $reg_info = $_POST['user_info'];
        if (empty($reg_info)){
            output_data('请填写完整信息');
        }
        //验证码
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->checkSmsCaptcha($reg_info['member_mobile'], $reg_info['captcha'], $log_type = 1);
        if (!$state_data['state']){
            output_error($state_data['msg']);
        }
        //用户名判断
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfo(['member_name'=> $reg_info['nickname']],'member_id');
        if ($member_info){
            output_error('用户名已存在');
        }
        //注册
        $info_data = $logic_connect_api->wxRegister($reg_info, $client = 'wap');
        $member = $info_data['member'];
        if($info_data['token']) {
            $state_data['key'] = $info_data['token'];
            $state_data['username'] = $member['member_name'];
            $state_data['userid'] = $member['member_id'];
            $state_data['usertype'] = $member['member_type'];
            output_data($state_data);
        } else {
            output_error('微信绑定会员登录失败');
        }
    }
    /**
     * QQ互联获取应用唯一标识
     */
    public function get_qq_appidOp(){
        output_data(C('app_qq_akey'));
    }
    /**
     * 请求QQ互联授权
     */
    public function get_qq_oauth2Op(){
        $logic_connect_api = Logic('connect_api');
        $qq_url = $logic_connect_api->getQqOAuth2Url('api');
        @header("location: ".$qq_url);
    }
    /**
     * QQ互联获取回调信息
     */
    public function get_qq_infoOp(){
        $code = $_GET['code'];
        $token = $_GET['token'];
        $client = $_GET['client'];
        $logic_connect_api = Logic('connect_api');
        $user_info = $logic_connect_api->getQqUserInfo($code,$client,$token);
        if(!empty($user_info['openid'])){
            $qqopenid = $user_info['openid'];
            $model_member = Model('member');
            $member = $model_member->getMemberInfo(array('member_qqopenid'=> $qqopenid));
            $state_data = array();
            $token = 0;
            if(!empty($member)) {//会员信息存在时自动登录
                $token = $logic_connect_api->getUserToken($member, $client);
            } else {//自动注册会员并登录
                $info_data = $logic_connect_api->qqRegister($user_info, $client);
                $token = $info_data['token'];
                $member = $info_data['member'];
                $state_data['password'] = $member['member_passwd'];
            }
            if($token) {
                $state_data['key'] = $token;
                $state_data['username'] = $member['member_name'];
                $state_data['userid'] = $member['member_id'];
                $state_data['usertype'] = $member['member_type'];
                if($client == 'wap'){
                    redirect(WAP_SITE_URL.'/tmpl/member/member.html?username='.$state_data['username'].'&key='.$state_data['key'].'&usertype='.$state_data['usertype']);
                }
                output_data($state_data);
            } else {
                output_error('会员登录失败');
            }
        } else {
            output_error('QQ互联登录失败');
        }
    }
    /**
     * 新浪微博获取应用唯一标识
     */
    public function get_sina_appidOp(){
        output_data(C('app_sina_akey'));
    }
    /**
     * 请求新浪微博授权
     */
    public function get_sina_oauth2Op(){
        $logic_connect_api = Logic('connect_api');
        $sina_url = $logic_connect_api->getSinaOAuth2Url('api');
        @header("location: ".$sina_url);
    }
    /**
     * 新浪微博获取回调信息
     */
    public function get_sina_infoOp(){
        $code = $_GET['code'];
        $client = $_GET['client'];
        $sina_token['access_token'] = $_GET['accessToken'];
        $sina_token['uid'] = $_GET['userID'];
        $logic_connect_api = Logic('connect_api');
        $user_info = $logic_connect_api->getSinaUserInfo($code,$client,$sina_token);
        if(!empty($user_info['id'])){
            $sinaopenid = $user_info['id'];
            $model_member = Model('member');
            $member = $model_member->getMemberInfo(array('member_sinaopenid'=> $sinaopenid));
            $state_data = array();
            $token = 0;
            if(!empty($member)) {//会员信息存在时自动登录
                $token = $logic_connect_api->getUserToken($member, $client);
            } else {//自动注册会员并登录
                $info_data = $logic_connect_api->sinaRegister($user_info, $client);
                $token = $info_data['token'];
                $member = $info_data['member'];
                $state_data['password'] = $member['member_passwd'];
            }
            if($token) {
                $state_data['key'] = $token;
                $state_data['username'] = $member['member_name'];
                $state_data['userid'] = $member['member_id'];
                $state_data['usertype'] = $member['member_type'];
                if($client == 'wap'){
                    redirect(WAP_SITE_URL.'/tmpl/member/member.html?username='.$state_data['username'].'&key='.$state_data['key'].'&usertype='.$state_data['usertype']);
                }
                output_data($state_data);
            } else {
                output_error('会员登录失败');
            }
        } else {
            output_error('新浪微博登录失败');
        }
    }
    /**
     * 微信获取应用唯一标识
     */
    public function get_wx_appidOp(){
        output_data(C('app_weixin_appid'));
    }
    /**
     * 微信获取回调信息
     */
    public function get_wx_infoOp(){
        $code = $_GET['code'];
        $access_token = $_GET['access_token'];
        $openid = $_GET['openid'];
        $client = $_GET['client'];
        $logic_connect_api = Logic('connect_api');
        if(!empty($code)) {
            $user_info = $logic_connect_api->getWxUserInfo($code,'api');
        } else {
            $user_info = $logic_connect_api->getWxUserInfoUmeng($access_token, $openid);
        }
        if(!empty($user_info['unionid'])){
            $unionid = $user_info['unionid'];
            $model_member = Model('member');
            $member = $model_member->getMemberInfo(array('weixin_unionid'=> $unionid));
            $state_data = array();
            $token = 0;
            if(!empty($member)) {//会员信息存在时自动登录
                $token = $logic_connect_api->getUserToken($member, $client);
            } else {//自动注册会员并登录
                $info_data = $logic_connect_api->wxRegister($user_info, $client);
                $token = $info_data['token'];
                $member = $info_data['member'];
                $state_data['password'] = $member['member_passwd'];
            }
            if($token) {
                $state_data['key'] = $token;
                $state_data['username'] = $member['member_name'];
                $state_data['userid'] = $member['member_id'];
                $state_data['usertype'] = $member['member_type'];
                output_data($state_data);
            } else {
                output_error('会员登录失败');
            }
        } else {
            output_error('微信登录失败');
        }
    }
    /**
     * 获取手机短信验证码
     */
    public function get_sms_captchaOp(){
        $sec_key = $_GET['sec_key'];
        $sec_val = $_GET['sec_val'];
        $r = false;
        if($sec_key == ''&& $sec_val == ''){
            $r = true;
        }
        $phone = $_GET['phone'];
        $log_type = $_GET['type'];//短信类型:1为注册,2为登录,3为找回密码
        $state_data = array(
            'state' => false,
            'msg' => '验证码或手机号码不正确'
            );
        
        $result = Model('apiseccode')->checkApiSeccode($sec_key,$sec_val);
        if ($r || ($result && strlen($phone) == 11)){
            $logic_connect_api = Logic('connect_api');
            $state_data = $logic_connect_api->sendCaptcha($phone, $log_type);
        }
        $this->connect_output_data($state_data);
    }
    /**
     * 验证手机验证码
     */
    public function check_sms_captchaOp(){
        $phone = $_GET['phone'];
        $captcha = $_GET['captcha'];
        $log_type = $_GET['type'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->checkSmsCaptcha($phone, $captcha, $log_type);
        $this->connect_output_data($state_data, 1);
    }
    /**
     * 手机注册
     */
    public function sms_registerOp(){
        $phone = $_POST['phone'];
        $dis_code = trim($_POST['dis_code']);
        $regArr = [
            'source'=>0,
            'captcha'=>$_POST['captcha'],
            'password'=>$_POST['password'],
            'client'=>$_POST['client']
        ];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsRegister($phone, $dis_code, $regArr);
        $this->connect_output_data($state_data);
    }
    /**
     * 手机找回密码
     */
    public function find_passwordOp(){
        $phone = $_POST['phone'];
        $captcha = $_POST['captcha'];
        $password = $_POST['password'];
        $client = $_POST['client'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsPassword($phone, $captcha, $password, $client);
        $this->connect_output_data($state_data);
    }
    /**
     * 格式化输出数据
     */
    public function connect_output_data($state_data, $type = 0){
        if($state_data['state']){
            unset($state_data['state']);
            unset($state_data['msg']);
            if ($type == 1){
                $state_data = 1;
            }
            output_data($state_data);
        } else {
            output_error($state_data['msg']);
        }
    }
}
