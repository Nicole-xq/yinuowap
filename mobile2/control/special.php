<?php
/**
 * 专场详情
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class specialControl extends mobileHomeControl
{
	public function __construct()
	{
		parent::__construct();
	}

	public function special_detailsOp(){
        if(!$_POST['special_id']){
            output_error('参数错误');
        }
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        /** @var auction_specialModel $model_special */
        /** @var auctionsModel $model_auctions */
        /** @var auctionsModel $model_auctions */
        /** @var favoritesModel $favorites */
        /** @var auctionLogic $auction_logic */
        $special_id = $_POST['special_id'];
		$model_special = Model('auction_special');
		$model_auctions = Model('auctions');
		$pageSize = $model_auctions->page;
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
		$fields = 'special_id, store_id, special_name, special_image, special_click, share_content, is_pd_pay as is_nb,
		 special_rate, special_rate_time, special_preview_start, special_start_time, special_end_time';
        $condition = array('special_id' => $special_id);
		$special_info = $model_special->getSpecialInfo($condition, $fields);
		if (empty($special_info)) {
            output_error('该专场不存在');
        }
		if($special_info['special_state'] == 10 || $special_info['special_state'] == 30){
			output_error('未审核通过的专场');
		}
        $special_info['special_image'] = getVendueLogo($special_info['special_image']);
        $special_info['special_start_date'] = date('m月d日 H:i', $special_info['special_start_time']);


        $auction_logic = Logic('auction');
        //$special_info['day_num'] = $auction_logic->getInterestDay($special_info['special_rate_time'],$special_info['special_end_time']);//计息天数
        //$special_info['special_remain_time_end'] = $special_info['special_end_time'] - TIMESTAMP >0 ? $special_info['special_end_time'] - TIMESTAMP : 0;
        //$special_info['special_remain_time_start'] = $special_info['special_start_time'] - TIMESTAMP >0 ? $special_info['special_start_time'] - TIMESTAMP : 0;
        //$special_info['special_remain_time_rate'] = $special_info['special_rate_time'] - TIMESTAMP >0 ? $special_info['special_rate_time'] - TIMESTAMP : 0;
//        if(time() >= $special_info['special_start_time'] && $special_info['special_end_time']> time()) {
//            $special_info['is_kaipai'] = 1;
//        }elseif($special_info['special_end_time']<= time()){
//            $special_info['is_kaipai'] = 2;
//        }elseif($special_info['special_start_time'] > time()){
//            $special_info['is_kaipai'] = 0;
//        }
//        if($special_info['special_rate_time'] > time()){
//            $special_info['is_kaipai'] = 3;
//        }
        // 如果已登录 判断该店铺是否已被关注
        $member_id = $this->getMemberIdIfExists();
//        if ($memberId) {
//            $favorites = Model('favorites');
//            $c = (int) $favorites->getStoreFavoritesCountByStoreId($special_info['store_id'], $memberId);
//            $special_info['is_favorate'] = $c > 0;
//        } else {
//            $special_info['is_favorate'] = false;
//        }
        $special_info['is_mind'] = 0;
        if ($member_id) {
            $relation_model = Model('special_notice');
            $where = [
                'special_id' => $special_id,
                'member_id' => $member_id,
            ];
            $remind = $relation_model->where($where)->find();
            if ($remind) {
                $special_info['is_mind'] = 1;
            }
        }

        $fields = 'auction_id, auction_name, auction_image, auction_start_price, current_price,
         auction_preview_start, auction_start_time, auction_end_time, state';
		$auction_list = $model_auctions->getAuctionList($condition, $fields, '' , '', $limit);
        $count = $model_auctions->where(array('special_id' => $special_id))->count();
//        $all_bid_number = 0;
//        $piece_num = 0;
//        $num_of_applicants = 0;//报名人数
//        $all_price = 0;
//        $special_click = 0;
//        $collect = 0;
        /** @var auction_configModel $auction_config_model */
        $auction_config_model = Model('auction_config');
        $auction_config = $auction_config_model->getInfo(1);
		if(!empty($auction_list)){
			foreach($auction_list as $key => &$value){
				$auction_id = $value['auction_id'];
                $value['current_price'] = $value['current_price'] != 0.00 ? $value['current_price'] : $value['auction_start_price'];
                $value['auction_image'] = cthumb($value['auction_image'],360);

                $value['duration_time'] = isset($auction_config['duration_time']) ? $auction_config['duration_time'] : 60;
                $value['add_time'] = isset($auction_config['add_time']) ? $auction_config['add_time'] : 2;


//                $all_bid_number += $value['bid_number'];
//                $num_of_applicants += $value['num_of_applicants'];
//                $collect += $value['auction_collect'];
                //$special_click += $value['auction_click'];
                if($value['is_liupai'] == 0 && $value['auction_end_true_t'] <=time()){
                    //$piece_num += 1;
                    //$all_price += $value['current_price'];
                }
                //$cur_goods_info = Model('goods')->getGoodsInfo(['goods_id'=>$value['goods_id']], 'goods_marketprice');
                //$value['goods_marketprice'] = $cur_goods_info['goods_marketprice'];
                //收藏  & 提醒
//                if ($memberId) {
//                    $c = (int)Model('favorites')->getGoodsFavoritesCountByAuctionId($auction_id, $memberId);
//                    $value['is_favorate'] = $c > 0;
//                    $relation_info = Model('auction_member_relation')->getRelationInfo(['member_id'=>$memberId,'auction_id'=>$auction_id],'is_remind,relation_id');
//                    $value['is_remind'] = $relation_info['is_remind'] ? true : false ;
//                    $value['relation_id'] = $relation_info['relation_id'];
//                }
			}
			unset($value);
		}
        //浏览量+1
        $click = 1;
        $model_special->editSpecial(
            ['special_click' => array('exp', 'special_click + '.$click)],
            $condition
        );
        $special_info['special_auctions_list'] = $auction_list;
        $page = api_page($count, $page, $pageSize);
        $auction_list = array_merge($page, ['special_auctions_list' => $auction_list]);
        $special_info['special_auctions_list'] = $auction_list;

        output_data($special_info);
	}

    /**
     * 专场列表
     */
	public function get_special_listOp(){
        $page = $_GET['page'];
        $is_kai_pai = $_GET['is_kai_pai'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $condition = '';
        $current_time = time();
        $model_special = Model('auction_special');
        switch ($is_kai_pai)
        {
            case '1':
                //拍卖中专场列表
                $condition .= "special_start_time < {$current_time} and `special_end_time` = 0 and special_rate = 0 and ";
                $order = 'special_id asc';
            break;
            case '2':
                //收益专场列表
                $condition .= "special_rate_time > {$current_time} and `special_start_time` > {$current_time} and special_preview_start <= {$current_time} and special_rate > 0 and ";
                $order = 'special_rate_time, special_id asc';
            break;
            case '3':
                //预展中专场列表
                $condition .= "special_start_time > {$current_time} and special_preview_start <= {$current_time} and special_rate = 0 and ";
                $order = 'special_start_time, special_id asc';
                break;
            case '4':
                //拍乐赚专场
                //拍乐赚专场设成一页60个
                $model_special->page = 60;
                $condition .= "store_id = 0 and special_rate = 0 and special_preview_start <= {$current_time} and ";
                $order = 'special_start_time asc';
                break;
            default:
                //预展中专场列表
                $condition .= "special_start_time < {$current_time} and special_preview_start <= {$current_time} and special_rate = 0 and ";
                $order = 'special_start_time, special_id asc';
                break;
        }

        $begin = ($page-1) * $model_special->page;
        $limit = $begin .','. $model_special->page;
        $condition .= 'is_open = 1 and special_end_time = 0 and special_type <>' . \App\Models\ShopncAuctionSpecial::SPECIAL_TYPE_NEWBIE;
        if (empty($order)) {
            $order = 'special_start_time asc';
        }
        $fields = 'special_id, store_id, special_name, wap_image, participants, special_click, special_preview_start, 
        special_start_time, special_end_time, special_rate, special_rate_time, is_pd_pay as is_nb';
        $special_list = $model_special->specialOpenListFetch($condition, $fields, $order, $limit);
        $count = $model_special->where($condition)->count();
        foreach($special_list as $key => &$special_info) {
            $special_info['wap_image'] = getVendueLogo($special_info['wap_image']);
            $special_info['special_start_date'] = date('m月d日 H:i', $special_info['special_start_time']);
            $special_info['special_end_date'] = date('m月d日 H:i', $special_info['special_end_time']);
        }

        output_data(['list'=>$special_list], api_page($count, $page, $model_special->page));
    }

}