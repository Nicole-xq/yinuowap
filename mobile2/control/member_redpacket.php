<?php
/**
 * 我的红包
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_redpacketControl extends mobileMemberControl {
    private $redpacket_state_arr;

    public function __construct() {
        parent::__construct();
        //判断系统是否开启红包功能
        if (C('redpacket_allow') != 1){
            output_error('系统未开启红包功能');
        }
        $model_redpacket = Model('redpacket');
        $this->redpacket_state_arr = $model_redpacket->getRedpacketState();
    }
    /*
     * 我的红包列表
     */
    public function redpacket_listOp(){
        /** @var redpacketModel $model_redpacket */
        $model_redpacket = Model('redpacket');
        //更新红包过期状态
        $model_redpacket->updateRedpacketExpire($this->member_info['member_id']);
        //查询红包
        $where = ['rpacket_owner_id'=>$this->member_info['member_id']];
        intval($_GET['rpacket_state']) > 0 ? $where['rpacket_state'] = intval($_GET['rpacket_state']) : '';

        $redpacket_list = $model_redpacket->getRedpacketList($where, '*', 0, $this->page, 'rpacket_state asc,rpacket_id desc');
        $page_count = $model_redpacket->gettotalpage();
        foreach ($redpacket_list as &$value){
            $value['rpacket_limit'] =  intval($value['rpacket_limit']);
            $rpt_template_info = Model('redpacket')->getRptTemplateInfo(['rpacket_t_id'=>$value['rpacket_t_id']],'rpacket_t_store_id,rpacket_t_wap_link');
                 $value['rpacket_t_wap_link'] = $rpt_template_info['rpacket_t_wap_link'];
        }
        unset($value);

        //统计个数和总金额
        $count_info['rpt_num'] = $model_redpacket->own_available_rpt($this->member_info['member_id'], intval($_GET['rpacket_state']));
        $count_info['rpt_total_price'] = $model_redpacket->own_rpt_total_price($this->member_info['member_id'], intval($_GET['rpacket_state']));

        output_data(array('redpacket_list' => $redpacket_list,'count_info'=>$count_info), mobile_page($page_count));
    }

    /*
     * 我的红包统计
     */
    public function redpacket_countOp(){
        $model_redpacket = Model('redpacket');
        $redpacket_detail['rpt_num'] = $model_redpacket->own_available_rpt($this->member_info['member_id']);
        $redpacket_detail['rpt_total_price'] = $model_redpacket->own_rpt_total_price($this->member_info['member_id']);
        output_data($redpacket_detail);
    }
    /**
     * 卡密领取红包
     */
    public function rp_pwexOp(){
        $param = $_POST;
        $pwd_code = trim($param["pwd_code"]);
        if (!$pwd_code) {
            output_error('请输入红包卡密');
        }
        if (!Model('apiseccode')->checkApiSeccode($param["codekey"],$param['captcha'])) {
            output_error('验证码错误');
        }
        //查询红包
        $model_redpacket = Model('redpacket');
        $redpacket_info = $model_redpacket->getRedpacketInfo(array('rpacket_pwd'=>md5($pwd_code)));
        if(!$redpacket_info){
            output_error('红包卡密错误');
        }
        if($redpacket_info['rpacket_owner_id'] > 0){
            output_error('该红包卡密已被使用');
        }
        $where = array();
        $where['rpacket_id'] = $redpacket_info['rpacket_id'];
        $update_arr = array();
        $update_arr['rpacket_owner_id'] = $this->member_info['member_id'];
        $update_arr['rpacket_owner_name'] = $this->member_info['member_name'];
        $update_arr['rpacket_active_date'] = time();
        $result = $model_redpacket->editRedpacket($where, $update_arr, $this->member_info['member_id']);
        if($result){
            //更新红包模板
            $update_arr = array();
            $update_arr['rpacket_t_giveout'] = array('exp','rpacket_t_giveout+1');
            $model_redpacket->editRptTemplate(array('rpacket_t_id'=>$redpacket_info['rpacket_t_id']),$update_arr);
            output_data('1');
        } else {
            output_error('红包领取失败');
        }
    }
}
