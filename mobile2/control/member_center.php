<?php
/**
 * 个人资料
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_centerControl extends mobileMemberControl{
    public function __construct(){
        parent::__construct();
    }

    /**
     * 个人信息
     * */
    public function get_informationOp(){
        $sex_array = $this->get_sex();
        list($type_arr, $logo_arr) = $this->_getMemberType();
        $data = array(
            'member_type' => $type_arr[$this->member_info['member_type']],
            'logo_img' => $logo_arr[$this->member_info['member_type']],
            'member_truename' => $this->member_info['member_truename'],
            'member_real_name' => $this->member_info['member_real_name'],
            'member_using_mobile' => $this->member_info['member_using_mobile'],
            'member_sex' => $sex_array[$this->member_info['member_sex']],
            'member_profile' => $this->member_info['member_profile'],
            'member_is_wx' => $this->member_info['weixin_open_id']? 1 : 0,
        );
        $member_avatar = $this->member_info['member_avatar'];
        if (substr($member_avatar, 0, 4) == 'http') {
            $data['member_avatar'] = $member_avatar;
        } else {
            $data['member_avatar'] = getMemberAvatarForID($this->member_info['member_id'], $member_avatar).'?'.TIMESTAMP;
        }
        output_data($data);
    }

    /**
     * 编辑信息
     * */
    public function save_informationOp(){
        $member_truename = $_POST['member_truename'];
        if (mb_strlen($member_truename, 'UTF8') > 30) {
            output_error('昵称不能超过30个字');
        }

        $member_profile = $_POST['member_profile'];
        if (mb_strlen($member_profile, 'UTF8') > 300) {
            output_error('简介不能超过300个字');
        }

        $param = array('member_truename', 'member_sex', 'member_profile', 'member_real_name', 'member_using_mobile');
        $update = array();
        if (!empty($_POST['edit_name']) && in_array($_POST['edit_name'], $param)) {
            if ($_POST['edit_name'] == 'member_sex' && !in_array($_POST['edit_value'], [1, 2, 3])) {
                output_error('参数错误');
            }

            if ($_POST['edit_name'] == 'member_truename' && mb_strlen($_POST['edit_value'], 'UTF8') > 30) {
                output_error('昵称不能超过30个字');
            }

            if ($_POST['edit_name'] == 'member_real_name' && mb_strlen($_POST['edit_value'], 'UTF8') > 30) {
                output_error('名字不能超过30个字');
            }

            $preg = "/^(13\d|14[57]|15[^4\D]|17[0135-8]|18\d)\d{8}$/";
            if ($_POST['edit_name'] == 'member_using_mobile' && !preg_match($preg,$_POST['edit_value'])) {
                output_error('请出入正确手机号');
            }

            $update[$_POST['edit_name']] = $_POST['edit_value'];
        }

        $old_member_avatar = $this->member_info['member_avatar'];

        //上次头像
        if ($_FILES['member_avatar']['name']) {
            $upload = new UploadFile();
            $upload->set('max_size', 1024);
            $upload->set('default_dir', '/shop/avatar');
            $upload->set('file_name', 'avatar_' . $this->member_info['member_id'] . '.jpg');
            $result = $upload->upfile('member_avatar');
            if ($result) {
                $update['member_avatar'] = $upload->file_name;
            } else {
                output_error($upload->error);
            }
        }

        $condition = array('member_id' => $this->member_info['member_id']);
        $res = Model('member')->editMember($condition, $update);
        if ($res) {
            output_data(1);
        } else {
            output_error('保存失败！');
        }
    }

    /**
     * 性别
     * @return multitype:string
     */
    private function get_sex() {
        $array = array();
        $array[1] = '男';
        $array[2] = '女';
        $array[3] = '保密';
        return $array;
    }

    /**
     * 会员类型
     */
    private function _getMemberType(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $type = array();
        $logo = array();
        foreach ($re as $v) {
            $type[$v['type_id']] = $v['name'];
            $logo[$v['type_id']] = $v['logo_img'];
        }
        return array($type, $logo);
    }

    /**
     * 微信解绑
     * */
    public function wx_unbindOp(){
        $data = ['error'=>1,'massage'=>'解绑失败'];
        if (!$this->member_info['weixin_open_id'] && !$this->member_info['weixin_unionid'] && !$this->member_info['weixin_info']) {
            $data = ['error' => 0, 'massage' => '解绑成功'];
        } elseif (($this->member_info['member_name'] || $this->member_info['member_mobile']) && $this->member_info['member_passwd']){
            /** @var memberModel $member_model */
            $member_model = Model('member');
            $update = array();
            $update['weixin_open_id'] = null;
            $update['weixin_unionid'] = null;
            $update['weixin_info'] = null;
            if($this->member_info['member_avatar'] && substr($this->member_info['member_avatar'],0,4) == 'http'){
                $update['member_avatar'] = '';
            }
            $result = $member_model->editMember(['member_id'=>$this->member_info['member_id']],$update);
            if($result){
                $data = ['error'=>0,'massage'=>'解绑成功'];
            }else{
                $data = ['error'=>1,'massage'=>'解绑失败，请联系客服'];
            }
        }
        output_data($data);
    }
}