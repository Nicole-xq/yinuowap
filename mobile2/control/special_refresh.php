<?php
if (empty($_SERVER['argv'][1])) exit('Access Invalid!');
/**
 * 刷新app专题页
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class special_refreshControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    // 刷新app端专题页静态页面
    public function indexOp(){
        $special_list = Model('mb_special')->where(array('special_id' => array('gt',1)))->field('special_id')->select();
        foreach ($special_list as $key => $value) {
            $this->_get_special_data($value['special_id']);
        }
        echo "success at ".date('Y-m-d H:i:s');
    }

    // 获取专场数据信息
    private function _get_special_data($special_id) {
        $model_mb_special = Model('mb_special');
        $info = $model_mb_special->getMbSpecialInfoByID($special_id);
        $list = $model_mb_special->getMbSpecialItemUsableListByID($special_id);
        $model_auctions = Model('auctions');
        $model_special = Model('auction_special');
        $model_store = Model('store');
        $model_guess = Model('p_guess');
        $b= 0;
        foreach ($list as $key => $value) {
            $a = array();
            if ($value['store_top']) {
                foreach ($value['store_top']['item'] as $k => $v) {
                    $store_info = $model_store->getStoreInfoByID($v['store_id']);
                    if($member_id = $this->getMemberIdIfExists()) {
                        $c = (int) Model('favorites')->getStoreFavoritesCountByMemberId($member_id, $v['store_id']);
                        $value['store_top']['item'][$k]['if_fav'] = $c > 0;
                    }
                    $value['store_top']['item'][$k]['store_collect'] = $store_info['store_collect'];
                }
                $list[$key]['store_top'] = $value['store_top'];
            }

            if ($value['qucai']) {
                $list[$key]['qucai'] = $value['qucai'];
                foreach($list[$key]['qucai']['item'] as $k=>$v){
                    $qucai_info = $model_guess->getOpenGuessInfo(array('gs_id'=>$v['gs_id']));
                    $list[$key]['qucai']['item'][$k]['joined_times'] = $qucai_info['joined_times'];
                }
            }

            if ($value['auctions']) {
                foreach($value['auctions']['item'] as $k => $v) {
                    $auction_info = $model_auctions->getAuctionsInfoByID($v['auction_id']);
                    if($auction_info['auction_start_time'] <= TIMESTAMP){
                        if($auction_info['auction_start_time'] <= time() && ($auction_info['auction_end_time'] >= time() || $auction_info['auction_end_true_t'] >= time())){
                            $v['is_kaipai'] = 1;
                        }elseif($auction_info['auction_end_true_t'] < time()){
                            $v['is_kaipai'] = 2;
                        }
                    }else{
                        $v['is_kaipai'] = 0;
                    }
                    $v['auction_remin_date'] = $auction_info['auction_end_time'] - TIMESTAMP >0 ?$auction_info['auction_end_time'] - TIMESTAMP : 0;
                    $v['current_price'] = $auction_info['current_price'] != 0.00 ?$auction_info['current_price']:$auction_info['auction_start_price'];
                    $v['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                    $v['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);
                    $v['auction_start_time'] = $auction_info['auction_start_time'];
                    $v['auction_end_time'] = $auction_info['auction_end_time'];
                    $v['bid_number'] = $auction_info['bid_number'];
                    $v['auction_click'] = $auction_info['auction_click'];

                    $value['auctions']['item'][$k] = $v;
                }
                $list[$key]['auctions'] = $value['auctions'];
            }

            if($value['special']){
                if($b == 0){
                    $k = $key;
                }
                unset($key);
                if(!in_array($value['special'],$a)){
                    $a[] = $value['special'];
                    $list[$k]['special']['special'][] = $value['special'];
                }
                else{
                    $list[$k]['special']['special'][] = array_merge(array($a),array($value['special']));
                }
                $b += 1;
                if(!empty($list[$k]['special']['special'])){
                    foreach($list[$k]['special']['special'] as $kk=>$vv){
                        foreach($vv['item'] as $k1=>$v1){
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remin_date'] = $v1['special_end_time'] - TIMESTAMP >0 ?$v1['special_end_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_start_date'] = date('m月d日 H:i',$v1['special_start_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_date'] = date('m月d日 H:i',$v1['special_end_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_time'] = $v1['special_end_time'];
                            if(time() >= $v1['special_start_time'] && $v1['special_end_time']> time()) {
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 1;
                            }elseif($v1['special_end_time']<= time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 2;
                            }elseif($v1['special_start_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 0;
                            }
                            $auction_list = $model_special->getSpecialGoodsLists(array('auction_sp_state'=>1));
                            $all_bid_number = 0;
                            $jian_num = 0;
                            if(!empty($auction_list)){
                                foreach($auction_list as $id=>$auction_info){
                                    $all_bid_number += $auction_info['bid_number'];
                                    if($auction_info['is_liupai'] == 0 && $auction_info['auction_end_true_t'] <=time()){
                                        $jian_num += 1;
                                    }
                                }
                            }

                            $list[$k]['special']['special'][$kk]['item'][$k1]['all_bid_number'] = $all_bid_number;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['jian_num'] = $jian_num;
                        }
                    }
                }
            }
        }
        $list_sort = array();
        $sem = array();
        $adv = array();
        foreach ($list as $key => $value) {
            if ($value['sem']){
                $sem = $value;
            } elseif($value['adv_list']){
                $adv = $value;
            } else {
                $list_sort[] = $value;
            }
        }
        if (!empty($adv)){
            array_unshift($list_sort, $adv);
        }
        if (!empty($sem)){
            array_unshift($list_sort, $sem);
        }
        $data = array_merge($info, array('list' => $list_sort));
        $this->_output_special($data, $special_id);
    }

    /**
     * 生成静态页
     */
    private function _output_special($data, $special_id = 0) {
        $model_special = Model('mb_special');
        $html_path = $model_special->getMbSpecialHtmlPath($special_id);
        ob_start();
        Tpl::output('list', $data);
        Tpl::showpage('mb_special');
        file_put_contents($html_path, ob_get_clean());
    }
}
