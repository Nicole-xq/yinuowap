<?php
/**
 * 前台品牌分类
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class otherControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }


    public function get_numberOp() {
        $num = file_get_contents(BASE_DATA_PATH . '/log/number.txt');
        output_data($num);
    }

}
