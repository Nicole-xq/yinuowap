<?php
/**
 * 用户趣猜
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
use Shopnc\Tpl;
class member_guessControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }


    /**
     * 添加收藏
     */
    public function favorites_addOp() {
        $gs_id = intval($_POST['gs_id']);
        if ($gs_id <= 0){
            output_error('参数错误');
        }

        $favorites_model = Model('favorites');

        //判断是否已经收藏
        $favorites_info = $favorites_model->getOneFavorites(array('fav_id'=>$gs_id,'fav_type'=>'guess','member_id'=>$this->member_info['member_id']));
        if(!empty($favorites_info)) {
            output_error('您已经收藏了该商品');
        }

        //判断商品是否为当前会员所有
        $guess_model = Model('p_guess');
        $guess_info = $guess_model->getGuessInfo(array('gs_id' => $gs_id));
        $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$this->member_info['member_id']));
        if ($guess_info['store_id'] == $seller_info['store_id']) {
            output_error('您不能收藏自己发布的商品');
        }

        //添加收藏
        $insert_arr = array();
        $insert_arr['member_id'] = $this->member_info['member_id'];
        $insert_arr['member_name'] = $this->member_info['member_name'];
        $insert_arr['fav_id'] = $gs_id;
        $insert_arr['fav_type'] = 'guess';
        $insert_arr['fav_time'] = TIMESTAMP;
        $result = $favorites_model->addFavorites($insert_arr);

        if ($result){
            //增加收藏数量
            $guess_model->editGuess(array('gs_collect' => array('exp', 'gs_collect + 1')), array('gs_id' => $fav_id));
            output_data('1');
        }else{
            output_error('收藏失败');
        }
    }

    /**
     * 删除收藏
     */
    public function favorites_delOp() {
        $fav_id = intval($_POST['fav_id']);
        if ($fav_id <= 0){
            output_error('参数错误');
        }

        $model_favorites = Model('favorites');
        $model_guess = Model('p_guess');

        $condition = array();
        $condition['fav_type'] = 'guess';
        $condition['fav_id'] = $fav_id;
        $condition['member_id'] = $this->member_info['member_id'];
        //判断收藏是否存在
        $favorites_info = $model_favorites->getOneFavorites($condition);
        if(empty($favorites_info)) {
            output_error('收藏删除失败');
        }

        $model_favorites->delFavorites($condition);

        $model_guess->editGuess(array('gs_collect' => array('exp', 'gs_collect - 1')), array('gs_id' => $fav_id));

        output_data('1');
    }


     /**
     * 添加趣猜报价
     */
    public function add_guess_offerOp()
    {
        $gs_id = $_POST['gs_id'];
        $guess_offer = $_POST['guess_offer'];
        if (empty($this->member_info['member_id'])){
            output_error('请登录');
        }
        if ($gs_id < 0) {
            output_error('参数错误');
        }
        if ($guess_offer <= 0) {
            output_error('请正确填写出价金额');
        }
        $guess_info = Model('p_guess')->getGuessInfo(array('gs_id' => $gs_id));
        if (empty($guess_info)) {
            output_error('参数错误');
        }
        if ($guess_info['store_id'] == $this->member_info['store_id']) {
            output_error('你不能对自己的趣猜出价');
        }
        if ($guess_info['start_time'] > time()) {
             output_error('活动尚未开始');
        }
        if ($guess_offer < $guess_info['gs_min_price'] || $guess_offer > $guess_info['gs_max_price']) {
            output_error('出价超出了出价范围');
        }
        if ($guess_info['end_time'] < time()) {
             output_error('活动已经结束');
        }
        $insert['gs_id'] =$gs_id;
        $insert['gs_name'] = $guess_info['gs_name'];
        $insert['member_id'] = $this->member_info['member_id'];
        $insert['member_name'] =$this->member_info['member_name'];
        $insert['gs_offer_price'] = $guess_offer;
        $insert['end_time'] = $guess_info['end_time'];
        $insert['partake_time'] = time();
        $model_guess_offer = Model('guess_offer');
        $guess_offer_data = $model_guess_offer->get_guess_offer(array('member_id' => $this->member_info['member_id'], 'gs_id' => $gs_id));
        if ($guess_offer_data) {
           output_error('您已经参与过该趣猜');
        }
        // 添加趣猜
        $result = $model_guess_offer->add_guess_offer($insert);
        if($result) {
            // 更新趣猜报名人数
            $model_p_guess = Model('p_guess');
            $model_p_guess->update_join_num(array('gs_id' => $gs_id));
            output_data(array(array('price' => $guess_offer)));
        } else {
            output_data('添加失败');
        }
    }


    /**
     * 获取趣猜基本信息
     * @return [type] [description]
     */
    public function getGuessInfoOp(){
        $gs_id = $_POST['gs_id'];
        if ($gs_id < 1) {
            output_error('参数有误请重试');
        }
        // 获取趣猜信息
        $model_p_guess = Model('p_guess');
        $guess_info = $model_p_guess->getOpenGuessInfo(array('gs_id'=>$gs_id));

        if (empty($guess_info)) {
            output_error('参数有误请重试');
        }
        unset($guess_info['gs_right_price']);
        unset($guess_info['gs_discount_price']);
        unset($guess_info['gs_estimate_price']);
        unset($guess_info['points']);
        output_data(array('guess_info' => $guess_info));
    }           

    /**
     * 是否收藏趣猜
     */
    public function if_fav_guessOp(){
        $gs_id = $_POST['gs_id'];
        if ($gs_id < 1) {
            output_error('参数有误请重试');
        }
        $model_favorites = Model('favorites'); 
        $favorites_status = $model_favorites->getOneFavorites(array('fav_type' => 'guess', 'member_id' => $this->member_info['member_id'], 'fav_id' => $gs_id));
        $model_guess_member_relation = Model('guess_member_relation');
        $relation = $model_guess_member_relation->getRelationInfoByMemberGuessID($this->member_info['member_id'], $gs_id);
        output_data(array('fav_info' => $favorites_status,'rel_info' => $relation));
    }


    /** 
     * 获取趣猜列表信息
     * @return [type] [description]
     */
    public function mb_gs_listOp(){
        // 1.实例化数据模型
        $model_guess_offer = Model('guess_offer');
        // 2.加好限制条件
        $condition = array();
        $condition['member_id'] = $this->member_info['member_id'];
        switch ($_POST['state_type']) {
            case 'finished':
                $condition['finished_time'] = array('lt',time());
                break;
            case 'choiced':
                $condition['gs_state'] = array('in',array(2,3));
                break;
            case 'guessing':
                $condition['gs_state'] = 0;
                break;
            default:
                break;
        }
        // 3.获取数据和分页数据
        $guess_list = $model_guess_offer->get_guess_offer_list($condition, $this->page, $limit = null, $order = 'partake_time desc');

        /** @var Model $p_guess_goods_model */
        $p_guess_goods_model = Model('p_guess_goods');

        $guess_list_data = array();
        if (!empty($guess_list)) {
                foreach ($guess_list as $key => $value) {
                    $guess_data = $p_guess_goods_model->where(array('gs_id' => $value['gs_id']))->find();
                    $value['goods_id'] = $guess_data['goods_id'];
                    $guess_list_data[$key] = $value;
                    $guess_list_data[$key]['show_img'] = $this->get_gs_img($value['gs_id']);
                    $guess_list_data[$key]['partake_time'] = date("Y-m-d H:i:s",$guess_list_data[$key]['partake_time']);
                    $guess_list_data[$key]['if_can_buy'] =  (($guess_list_data[$key]['end_time'] + C('guess_add_cart_limit_time')) >= time()) ? true : false;

                }
        }
        $page_count = $model_guess_offer->gettotalpage();
        output_data(array('guess_list' => $guess_list_data), mobile_page($page_count));
    }


    /**
     * 发送短信验证码
     */
    public function send_modify_mobileOp()
    {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号码'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            exit(json_encode(array('state'=>'false','msg'=>$error)));
        }
        $gs_mb_relation_info = Model('guess_member_relation')->getRelationInfo(array('gs_id' => $_POST['gs_id'],'member_mobile' => $_POST['mobile']));
        if ($gs_mb_relation_info) {
            exit(json_encode(array('state'=>'false','msg'=>'该手机已设置了趣猜提醒')));
        }
        $model_member = Model('member');
        //发送频率验证
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$this->member_info['member_id']));
        if (!empty($member_common_info['send_mb_time'])) {
            if (TIMESTAMP - $member_common_info['send_mb_time'] < 58) {
                exit(json_encode(array('state'=>'false','msg'=>'请60秒以后再次发送短信')));
            }
        }

        // 生成验证码
        $verify_code = rand(1000,9999);

        //组装发送内容
        $model_tpl = Model('mail_templates');
        $tpl_info = $model_tpl->getTplInfo(array('code'=>'guess_mobile'));
        $param = array();
        $param['site_name'] = C('site_name');
        $param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
        $param['verify_code'] = $verify_code;
        $message    = ncReplaceText($tpl_info['content'],$param);
        // 发送
        $sms = new Sms();
        $result = $sms->send($_POST["mobile"],$message);

        // 发送成功回调
        if ($result) {
            $data = array();
            $data['auth_code'] = $verify_code;
            $data['send_acode_time'] = TIMESTAMP;
            $data['send_mb_time'] = TIMESTAMP;
            $update = $model_member->editMemberCommon($data,array('member_id'=>$this->member_info['member_id']));
            if (!$update) {
                exit(json_encode(array('state'=>'false','msg'=>'系统发生错误，如有疑问请与管理员联系')));
            }
            exit(json_encode(array('state'=>'true','msg'=>'发送成功')));
        } else {
            exit(json_encode(array('state'=>'false','msg'=>'发送失败')));
        }
    }


    /**
     * 设置结束前提醒
     * */
    public function guess_remindOp()
    {
        $mobile = $_POST['mobile'];
        $vcode = $_POST['vcode'];
        $is_remind = 1;//$_POST['is_remind'];
        // $is_remind_app = $_POST['is_remind_app'];
        $gs_id = $_POST['gs_id'];
        if (empty($this->member_info['member_id'])){
            json_api('error', '请登录');
        }
        if (empty($this->member_info['member_mobile'])){
            json_api('error1', '请绑定手机');
        }
        $model_member = Model('member');
        $model_guess = Model('p_guess');
//        $obj_validate = new Validate();
//        $obj_validate->validateparam = array(
//            array("input"=>$mobile, "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号'),
//        );
//        $error = $obj_validate->validate();
//        if ($error != ''){
//            json_api('error', $error);
//        }
//        //如果是获取验证码用户进行手机验证
//        $condition = array();
//        $condition['member_id'] = $this->member_info['member_id'];
//        $condition['auth_code'] = intval($vcode);
//        $member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
//        if (!$member_common_info) {
//            json_api('error', '手机验证码错误，请重新输入');
//        }
//        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
//            json_api('error','手机验证码已过期，请重新获取验证码');
//        }

        //建立订阅关系
        $model_guess_member_relation = Model('guess_member_relation');
        //查询是否已经存在
        $relation = $model_guess_member_relation->getRelationInfoByMemberGuessID($this->member_info['member_id'], $gs_id);
        $insert['gs_id'] = $gs_id;
        $insert['member_id'] = $this->member_info['member_id'];
        $insert['is_remind'] = $is_remind;
        $insert['member_mobile'] = $this->member_info['member_mobile'];
        // $insert['is_remind_app'] = $is_remind_app;
        $insert['remind_time'] = time();
        $insert['member_name'] = $_SESSION['member_name'];
        if (empty($relation)) {
            $res = $model_guess_member_relation->addRelation($insert);
        } else {
            $res = $model_guess_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }

        // 设置提醒人数 +1
        if (!$res) {
            json_api('error','订阅失败');
        } else {
            $model_guess->editGuess(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('gs_id' => $gs_id));
            json_api('ok', '订阅提醒成功');
        }
    }

    /*
     * 取消提醒
     *
     * */
    public function cancel_guess_remindOp() 
    {
        $gs_id = intval($_GET['gs_id']);
        if (empty($gs_id)) {
             json_api('error','无效的参数');
        }
        $model_guess_member_relation = Model('guess_member_relation');
        $model_guess = Model('p_guess');
        $result_1 = $model_guess_member_relation->delRelationInfo(array('gs_id' => $gs_id,'member_id' => $this->member_info['member_id']));

        // 设置提醒人数 -1
        $result_2 = $model_guess->editGuess(array('set_reminders_num' => array('exp', 'set_reminders_num-1')), array('gs_id' => $gs_id));
        if ($result_1&&$result_2) {
             json_api('ok','取消订阅成功');
        } else {
             json_api('error','操作失败，请重试');
        }
    }

     /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240);
    }

    /**
     * 精品推荐
     */
    public function more_guessOp(){
        $model_guess = Model('p_guess');
        $guess_array = $model_guess->getGuessOpenList(array(),'gs_id,gs_max_price,gs_min_price','end_time asc',6);
        $guess_data = array();
        foreach ($guess_array as $key => $value) {
            $value['guess_image'] = $this->get_gs_img($value['gs_id']);
            $value['gs_max_price'] = floor($value['gs_max_price']);
            $value['gs_min_price'] = floor($value['gs_min_price']);
            $guess_data[$key] = $value;
        }

        output_data(array('guess_list' => $guess_data));       
    } 
}