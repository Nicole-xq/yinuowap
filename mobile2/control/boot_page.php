<?php
/**
 * 引导页调取数据管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class boot_pageControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }

    /**
     * 引导页调取
     */
    public function getBootPageOp() {
        $boot_info = Model('mb_boot_page')->getBootPageInfo(['boot_state'=>2], 'id,title,boot_image,wap_link,share_state,share_desc');
        if (!empty($boot_info)) {
            $boot_info['boot_image'] = UPLOAD_SITE_URL.'/'.ATTACH_MOBILE.'/boot'.'/'.$boot_info['boot_image'];
            $boot_info['thumbs_up_flag'] = cookie('thumbs_up_flag') == 1 ? true : false;
        }
        output_data($boot_info);
    }

    /**
     * 记录数据——点击、点赞、分享
     */
    public function recordNumOp(){
        $id = intval($_GET['id']);
        $type = intval($_GET['type']);//1：点击；2：点赞；3：分享
        if (empty($id) || empty($type)){
            output_error('无效参数请求');
        }
        $update = [];
        if ($type == 1){
            $update = [ 'click_num'=>['exp','click_num + 1']];
        }elseif ($type == 2){
            setNcCookie('thumbs_up_flag', '1', 86400);//60*60*24 一天之后可以再点赞
            $update = [ 'thumbs_up_num'=>['exp','thumbs_up_num + 1']];
        }elseif ($type == 3){
            $update = [ 'share_num'=>['exp','share_num + 1']];
        }
        $result = Model('mb_boot_page')->updateBootData(['id'=>$id],$update);
        if ($result) {
            output_data('success');
        }else{
            output_error('无效操作');
        }
    }

}
