<?php
/**
 * 拍卖商品支付
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/20 0020
 * Time: 10:42
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_buy_auctionControl extends mobileMemberControl {

    protected $logic_auction_buy;

    public function __construct() {
        parent::__construct();
        $this->logic_auction_buy = Logic('auction_buy');
    }

    // 订单 分类 支付

    public function payOp()
    {
        $type = $_REQUEST['type'];
        $order_sn = $_REQUEST['pay_sn'];
        $rpacket_id = isset($_REQUEST['rpacket_id'])?$_REQUEST['rpacket_id']:0;
        $pay_message =  isset($_REQUEST['pay_message'])?$_REQUEST['pay_message']:'';
        switch ($type) {
            case 'margin':
                $result = $this->pay_margin($order_sn);
                break;
            case 'auction':
                $result = $this->pay_auction($order_sn, $rpacket_id, $pay_message);
                break;
        }
    }

    // 保证金支付，支付方式选择
    protected function pay_margin($order_sn)
    {
        //验证订单号
        if (!preg_match('/^\d{16}$/',$order_sn)){
            showMessage("无效的订单号",'index.php?act=auctions','html','error');
        }

        $model_margin_orders = Model('margin_orders');
        //查询订单信息
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));

        if (empty($order_info)) {
            output_error('未找到需要支付的订单');
        }

        // 排除非自己订单
        if ($order_info['buyer_id'] != $this->member_info['member_id']) {
            output_error('未找到需要支付的订单');
        }
        
        // 排除线下支付，保证金账户支付
        if ($order_info['order_type'] != 1) {
            /** @var auction_orderLogic $logic_auction_order */
            $logic_auction_order = Logic('auction_order');
            // 保证金账户支付
            $result = $logic_auction_order->MarginPay($this->member_info['member_id'], $order_info['order_sn']);
            // 更新订单内容
            $order_info = $result['data']['order_info'];
        }

        // 获取下单支付页面的信息
        $result = $this->getOrderPay($order_info, $this->member_info['member_id']);

        unset($order_info);
        list($over_pay, $order_info, $pay) = $result;

        $data = array(
            'order_info'=>$order_info,
            'pay_info' => $pay,
            'over_pay' => $over_pay,
        );
        output_data($data);
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //诺币支付金额(之前支付中止，余额被锁定)
        $pay['payd_points_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        // 是否已经被保证金余额支付完成
        $over_pay = 0;
        //显示支付金额和支付方式

        // 保证金总数
        $pay['pay_amount_online'] = $order_info['margin_amount'] - $order_info['account_margin_amount'];
        // 充值卡支付金额
        $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
        // 预存款支付金额
        $pay['payd_pd_amount'] = $order_info['pd_amount'];
        // 诺币支付
        $pay['payd_points_amount'] = $order_info['points_amount'];
        // 保证金账户支付
        $pay['account_margin_amount'] = $order_info['account_margin_amount'];

        // 未支付金额
        $pay['payd_diff_amount'] = $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'] - $order_info['account_margin_amount'];
        $order_info['payment_type'] = '在线支付';
        $pay['order_type'] = 0;

        //如果保证金金额为0，转到支付订单逻辑
        if ($pay['payd_diff_amount'] == 0) {
            $over_pay = 1;
        }

        //输出订单描述
        if (!empty($pay['payd_diff_amount'])) {
            $pay['order_remind'] = '下单成功，请尽快完成支付。';
        } else {
            $pay['order_remind'] = '保证金支付成功';
        }

        if ($pay['pay_amount_online'] >= 0) {
            //显示支付接口列表
            $payment_list = Model('mb_payment')->getMbPaymentOpenList();
            if(!empty($payment_list)) {
                foreach ($payment_list as $k => $value) {
                     if ($value['payment_code'] == 'wxpay') {
                         unset($payment_list[$k]);
                         continue;
                     }
                     if($pay['pay_amount_online'] < LKLPAY){
                        if ($value['payment_code'] == 'lklpay') {
                             unset($payment_list[$k]);
                             continue;
                         }
                     }else{
                        if ($value['payment_code'] == 'alipay') {
                             unset($payment_list[$k]);
                             continue;
                         }
                     }
                    unset($payment_list[$k]['payment_id']);
                    unset($payment_list[$k]['payment_config']);
                    unset($payment_list[$k]['payment_state']);
                    unset($payment_list[$k]['payment_state_text']);
                }
            }
        }

        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['member_points'] = $this->member_info['member_points'];
        $pay['points_money'] = ncPriceFormat($this->member_info['member_points']/100);
        $pay['pay_amount'] = ncPriceFormat($pay['payd_diff_amount']);
        $pay['hide_pd'] = $order_info['api_pay_time'] >0?1:0;
        //是否显示站内余额操作(如果以前没有使用站内余额支付过)
        $pay['payed_amount'] = ncPriceFormat($pay['payd_rcb_amount']+$pay['payd_pd_amount']+$pay['payd_points_amount']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        // 返回需要的数组
        return array(
            $over_pay,
            $order_info,
            $pay
        );
    }

    // 拍卖订单支付
    protected function pay_auction($order_sn, $rpacket_id = 0, $pay_message='')
    {
        $auction_order_sn = $order_sn;
        $member_id = $this->member_info['member_id'];
        //验证订单号
        if (!preg_match('/^\d{16}$/',$auction_order_sn)){
            output_error("无效的订单号");
        }
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition = array(
            'order_sn' => $auction_order_sn
        );
        $order_info = $model_order->getOrderInfo($condition);
        if ($order_info['order_state'] == ORDER_STATE_PAY) {
            \App\Exceptions\ApiResponseException::throwFailMsg("订单已支付");
        }
        if($member_id != $order_info['buyer_id']){
            \App\Exceptions\ApiResponseException::throwFailMsg("无权操作");
        }
        $result = $this->getAuctionOrderPay($order_info, $member_id);

        unset($order_info);

        list($order_info, $pay) = $result;

        /**
         * 使用结算红包的 拍卖订单 -----------------------------------------------------------------------
         */
        if($rpacket_id > 0 && $order_info['auction_id'] > 0){
            $auctionInfo = Model("auctions")->getAuctionsInfoByID($order_info['auction_id'], "auction_type");
            //新手专区代码 1.新手专区可以使用结算红包
            if($auctionInfo['auction_type'] != \App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE){
                \App\Exceptions\ApiResponseException::throwFailMsg("当前拍品类型不能使用红包,请返回重新选择");
            }

            if(Model('margin_orders')->where([
                "red_packet_id"=>['gt', 0],
                "auction_id" => $order_info['auction_id'],
                "buyer_id" => $member_id,
                "order_state" => ['in', [\App\Models\ShopncMarginOrder::ORDER_STATE_PAID,\App\Models\ShopncMarginOrder::ORDER_STATE_OFFLINE_PAY, \App\Models\ShopncMarginOrder::ORDER_STATE_PARTIAL_PAY]]
            ])->count()>0){
                \App\Exceptions\ApiResponseException::throwFailMsg("保证金中已使用红包,不能再次使用红包,请返回重新选择");
            }

            $redpacketInfo = Model("redpacket")->where(["rpacket_id"=>$rpacket_id])->find();
            //不是我的红包
            if($redpacketInfo['rpacket_owner_id'] != $member_id){
                \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
            }
            //红包检查 不通过的: 1.状态是过期和已使用 2.类型不是结算红包 3.使用范围不是新手专区的
            if(
                !$redpacketInfo ||
                in_array($redpacketInfo["rpacket_state"], [\App\Models\ShopncRedpacket::RPACKET_STATE_EXPIRE, \App\Models\ShopncRedpacket::RPACKET_STATE_USED])||
                $redpacketInfo["rpacket_t_special_type"] != \App\Models\ShopncRedpacketTemplate::RPACKET_T_SPECIAL_TYPE_ORDER ||
                $redpacketInfo["rpacket_t_type"] != \App\Models\ShopncRedpacketTemplate::RPACKET_T_TYPE_NEWBIE
            ){
                \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
            }


            if($redpacketInfo["rpacket_state"]==\App\Models\ShopncRedpacket::RPACKET_STATE_USED){
                if($redpacketInfo['binding_id']!=$order_info['order_id']){
                    \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
                }
            }elseif($redpacketInfo["rpacket_state"]==\App\Models\ShopncRedpacket::RPACKET_STATE_UNUSED){
                if($order_info["rpt_amount"] > 0){
                    \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
                }
                //正常的红包第一次
                $nowTime = time();
                //时间
                if($nowTime < $redpacketInfo['rpacket_start_date'] || $nowTime > $redpacketInfo['rpacket_end_date']){
                    \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
                }

                //金额
                if($redpacketInfo["rpacket_limit"] > $order_info['order_amount']){
                    \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
                }

                Model()->beginTransaction();
                try{
                    //锁定并使用红包

                    $upOrderCou = Model("order")->where(
                        [
                            "order_id"=>$order_info["order_id"],
                            "rpt_amount"=>0,
                        ]
                    )->updateRetCount(
                        [
                            "rpt_amount"=>$redpacketInfo["rpacket_price"],
                            "order_amount"=>["exp", "order_amount-".$redpacketInfo["rpacket_price"]]
                        ]
                    );
                    if($upOrderCou < 1){
                        \App\Exceptions\ApiResponseException::throwFailMsg("操作频繁,请返回重试");
                    }

                    $upRedCou = Model("redpacket")
                        ->where(["rpacket_id"=>$rpacket_id, "rpacket_state"=>\App\Models\ShopncRedpacket::RPACKET_STATE_UNUSED])
                        ->updateRetCount(["rpacket_state"=>\App\Models\ShopncRedpacket::RPACKET_STATE_USED, "binding_id"=>$order_info['order_id']]);
                    if($upRedCou < 1){
                        \App\Exceptions\ApiResponseException::throwFailMsg("操作频繁,请返回重试");
                    }
                    Model()->commit();
                }catch (\Exception $e){
                    Model()->rollback();
                    if($e instanceof \App\Exceptions\ApiResponseException){
                        throw $e;
                    }
                    yLog()->error("redpacket_lock_error", [\App\Lib\Helpers\LogHelper::getEInfo($e), func_get_args()]);
                    \App\Exceptions\ApiResponseException::throwFailMsg("红包使用错误,请返回重试");
                }
                //更新订单信息
                $order_info = $model_order->getOrderInfo($condition);
                $result = $this->getAuctionOrderPay($order_info, $member_id);
                unset($order_info);
                list($order_info, $pay) = $result;
            }else{
                \App\Exceptions\ApiResponseException::throwFailMsg("指定的红包不能使用,请返回重新选择");
            }
        }
        /**
         * 使用结算红包的 拍卖订单 end -----------------------------------------------------------------------
         */
        //更新订单留言
        Model()->table('order_common')->where(['order_id'=>$order_info['order_id']])->update(["order_message"=>$pay_message]);
        $data = array(
            'order_info' => $order_info,
            'pay_info' => $pay
        );
        output_data($data);
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getAuctionOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        $if_underline = false;
        //显示支付金额和支付方式
        if ($order_info['payment_code'] != 'underline') {
            // 需要支付尾款
            if($order_info['order_amount']  < $order_info['margin_amount']){//订单金额小于保证金金额
                $pay['pay_amount_online'] = 0;
            }else{
                $pay['pay_amount_online'] = bcsub($order_info['order_amount'], $order_info['margin_amount'], 2);
            }

            // 充值卡支付金额
            $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
            // 预存款支付金额
            $pay['payd_pd_amount'] = $order_info['pd_amount'];
            // 诺币支付
            $pay['payd_points_amount'] = $order_info['points_amount'];
            // 未支付金额
            $pay['payd_diff_amount'] = bcsub($pay['pay_amount_online'], bcadd(bcadd($order_info['rcb_amount'], $order_info['pd_amount'], 2), $pay['payd_points_amount'], 2), 2);
            if($pay['payd_diff_amount'] < 0){
                $pay['payd_diff_amount'] = 0;
            }
            $order_info['payment_type'] = '在线支付';
            $pay['order_type'] = 0;
        } else {
            if ($if_underline != true) {
                $if_underline = true;
            }
            $pay['pay_amount_underline'] = bcsub($order_info['order_amount'] , $order_info['margin_amount'], 2);
            $order_info['payment_type'] = '线下支付';
            $pay['order_type'] = 1;
        }

        //如果订单未支付金额为0，转到支付订单逻辑
        if (bcsub($order_info['order_amount'], bcadd(bcadd(bcadd($order_info['margin_amount'] , $order_info['rcb_amount'], 2), $order_info['pd_amount'], 2), $pay['payd_points_amount'], 2), 2) == 0) {
            $logic_auction_pay = Logic('auction_pay');
            //正好使用保证金支付
            $logic_auction_pay->marginPayOrder($order_info, $order_info['margin_amount']);
//            if($logic_auction_pay->marginPayOrder($order_info, 0)){
//                output_data(array('over_pay'=>1));exit;
////                redirect('index.php');
//            }
//            redirect('index.php?act=auction_payment&op=auction_order&order_sn='.$order_info['order_sn']);
        }

        //是否显示站内余额操作(如果以前没有使用站内余额支付过且非货到付款)
        $pay['if_show_pdrcb_select'] = ($pay['pay_amount_underline'] == 0 && $pay['payd_rcb_amount'] == 0 && $pay['payd_pd_amount'] == 0 && $pay['payd_points_amount']);

        //输出订单描述
        if (empty($pay['pay_amount_underline']) && !empty($pay['pay_amount_online'])) {
            $pay['order_remind'] = '请您在48小时内完成支付，逾期订单将视为违约。 ';
        } else {
            $pay['order_remind'] = '拍卖订单支付成功';
        }

        if ($pay['pay_amount_online'] >= 0) {
            //显示支付接口列表
            /** @var mb_paymentModel $mb_payment */
            $mb_payment = Model('mb_payment');
            $payment_list = $mb_payment->getMbPaymentOpenList();

            if(!empty($payment_list)) {
                foreach ($payment_list as $k => $value) {
                     if ($value['payment_code'] == 'wxpay') {
                         unset($payment_list[$k]);
                         continue;
                     }
                    unset($payment_list[$k]['payment_id']);
                    unset($payment_list[$k]['payment_config']);
                    unset($payment_list[$k]['payment_state']);
                    unset($payment_list[$k]['payment_state_text']);
                }
            }
        }

        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['member_points'] = $this->member_info['member_points'];
        $pay['points_money'] = ncPriceFormat($this->member_info['member_points']/100);
        $pay['pay_amount'] = ncPriceFormat($pay['payd_diff_amount']);
        //是否显示站内余额操作(如果以前没有使用站内余额支付过)
        $pay['payed_amount'] = ncPriceFormat($pay['payd_rcb_amount']+$pay['payd_pd_amount']+$pay['payd_points_amount']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        $pay['hide_pd'] = $order_info['api_pay_time'] >0?1:0;
        // 返回需要的数组
        return array(
            $order_info,
            $pay,
        );
    }


    /*
     * 显示保证金页面
     * */
    public function show_margin_orderOp()
    {
        $auction_id = intval($_GET['auction_id']);
        //获取界面 会员 店铺 拍品 收货地址 支付方式信息
        /** @var auction_buyLogic $this->logic_auction_buy */
        $result = $this->logic_auction_buy->show_margin_order($auction_id, $this->member_info['member_id']);
        $auction = Model('auctions')->getAuctionsInfo(array('auction_id'=>$auction_id));
        $e = false;
        if( ($auction['auction_end_time'] < time() && empty($auction['auction_end_time_t'])) || (!empty($auction['auction_end_time_t']) && $auction['auction_end_time_t'] < time())){
            $e = true;
        }
        if (intval($_GET['address_id']) > 0) {
            $result['address_info'] = Model('address')->getDefaultAddressInfo(array('address_id'=>intval($_GET['address_id']),'member_id'=>$this->member_info['member_id']));
        }
        /** @var margin_ordersModel $model_margin_order */
        $model_margin_order = Model('margin_orders');
        $condition = array(
            'buyer_id'=>$this->member_info['member_id'],
            'order_state'=>array('in','0,2,3'),
            'auction_id'=>$_GET['auction_id']);
        $margin_order = $model_margin_order->getOrderInfo($condition);

        $data = array(
            'e'=>$e,
            'address_info' => $result['address_info'],
            'is_auction_end_true_t' => $result['is_auction_end_true_t'],
            'auction_info' => $this->logic_auction_buy->auction_info,
            'margin_order' =>empty($margin_order)?'':$margin_order
        );
        output_data($data);
    }

    /*
     * 显示保证金页面
     * */
    public function show_margin_order2Op()
    {
        $auction_id = intval($_GET['auction_id']);
        //获取界面 会员 店铺 拍品 收货地址 支付方式信息
        /** @var auction_buyLogic $this->logic_auction_buy */
        $result = $this->logic_auction_buy->show_margin_order($auction_id, $this->member_info['member_id']);
        $auction = Model('auctions')->getAuctionsInfo(array('auction_id'=>$auction_id));
        $e = false;
        if( ($auction['auction_end_time'] < time() && empty($auction['auction_end_time_t'])) || (!empty($auction['auction_end_time_t']) && $auction['auction_end_time_t'] < time())){
            $e = true;
        }
        if (intval($_GET['address_id']) > 0) {
            $result['address_info'] = Model('address')->getDefaultAddressInfo(array('address_id'=>intval($_GET['address_id']),'member_id'=>$this->member_info['member_id']));
        }
        /** @var margin_ordersModel $model_margin_order */
        $model_margin_order = Model('margin_orders');
        $condition = array(
            'order_sn'=>$_GET['order_sn'],
            'buyer_id'=>$this->member_info['member_id'],
            'order_state'=>array('in','0,2,3'),
            'auction_id'=>$_GET['auction_id']);
        $margin_order = $model_margin_order->getOrderInfo($condition);

        $data = array(
            'e'=>$e,
            'address_info' => $result['address_info'],
            'is_auction_end_true_t' => $result['is_auction_end_true_t'],
            'auction_info' => $this->logic_auction_buy->auction_info,
            'margin_order' =>empty($margin_order)?'':$margin_order
        );
        output_data($data);
    }
    /*
     * 生成保证金订单
     * */
    public function create_margin_orderOp()
    {
        $auction_id = $_POST['auction_id'];
        $pay_name = $_POST['pay_name'];
        $is_margin = $_POST['is_margin'];
        $margin_amount = $_POST['margin_amount'];
        $redPacketId = $_REQUEST['redPacketId'];
        $_POST['redPacketId'] = $_REQUEST['redPacketId'];
        $address_id  = isset($_POST['address_id']) ? $_POST['address_id'] : '';         //非必选参数
        $is_anonymous = isset($_POST['is_anonymous']) ? $_POST['is_anonymous'] : 1;     //非必选参数
        if (empty($auction_id) || empty($pay_name) || empty($is_margin) || empty($margin_amount)) {
            output_error('缺少生成保证金订单参数 !');
        }
        //判断红包代金券是否可用
        if (!empty($redPacketId)) {
            /** @var redPacketLogic $redPacketLogic */
            $redPacketLogic = Logic('redPacket');
            $result = $redPacketLogic->checkMarginEnabled($auction_id, $redPacketId);
            if ($result['state'] === false) {
                output_error($result['data']);
            }
            $redPacketInfo = $result['data'];
            if ($redPacketInfo['rpacket_limit'] > $margin_amount) {
                output_error('不满足红包使用条件');
            }
        }
        if ($margin_amount > 50000 || $margin_amount <=0) {
            output_error('保证金支付金额不能超出五万,亦不为空 !');
        }
        if ($is_margin != 1) {
            output_error('支付保证金异常 !');
        }
        $member_id = $this->member_info['member_id'];

        //创建订单，记录线上或者线下支付方式，检查是否存在会员和拍品关系，不存在则创建。
        /** @var auction_buyLogic  $logic_auction_buy*/
        $logic_auction_buy = $this->logic_auction_buy;
        $result = $logic_auction_buy->new_create_margin_order(
            $auction_id, $member_id, $pay_name, $address_id, $is_anonymous, 2, $_POST
        );

        if($result['state'] != 200) {
            output_error($result['msg']);
        }
        output_data(array('pay_sn' => $result['data']['order_sn'],'payment_code'=>$result['data']['payment_code']));
    }
    
    /**
     *
     * 获取拍卖订单信息
     * @deprecated 请使用laravel接口
     */
    public function show_auction_orderOp()
    {
        /*$auction_order_id = $_GET['auction_order_id'];
        $auction_id = $_GET['auction_id'];
        if (empty($auction_order_id) && empty($auction_id)) {
            output_error('缺少必要参数');
        }
        $member_id = $this->member_info['member_id'];
        if (empty($auction_order_id)) {
            $auction_model = Model('auctions');
            $auction_info = $auction_model->getAuctionsInfoByID($auction_id);
            $map['auction_id']=$auction_id;
            $bid_log=Model("bid_log")->where($map)->order("offer_num DESC")->find();
            //查出谁最后出的最高价
            if ($bid_log['member_id'] != 0 && $bid_log['member_id']==$member_id) {
                $this->_auctions_order($auction_info,$bid_log);
                $model_order = Model('order');
                $order_chekc_info=$model_order->table("orders")->where(array('buyer_id' => $member_id,'auction_id'=>$auction_id,'order_state'=>array("neq",0)))->find();
                if ($order_chekc_info){
                    $auction_order_id=$order_chekc_info['order_id'];
                }else{
                    output_error('创建订单错误!请稍后再试!'.$member_id."+++".$bid_log['member_id']);
                }
            } else {
                output_error('非法请求');
            }
        }

        // 获取支付尾款的必要信息
        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->new_show_auction_order($auction_order_id, $member_id);
        if (!$result['state']) {
            output_error($result['msg']);
        }
        $data = array(
            'vat_deny' => $result['data']['vat_deny'],
            'vat_hash' => $result['data']['vat_hash'],
            'inv_info' => $result['data']['inv_info'],
            'store_info' => $result['data']['store_info'],
            'auction_order_info' => $result['data']['auction_order_info'],
            'margin_order_info' => $result['data']['margin_order_info'],
            'auction_info' => $result['data']['auction_info'],
            'underline' => true,
            'margin_amount' => $result['data']['margin_amount'],
            'refuse_amount' => $result['data']['refuse_amount'],
        );

        output_data($data);*/
    }

    /*
     * 更新订单支付方式和发票
     * */
    public function update_auction_orderOp()
    {
        /** @var auction_buyLogic $logic_auction_buy */
        $logic_auction_buy = Logic('auction_buy');
        // 查询发票信息和相关记录进行更新
        $member_id = $this->member_info['member_id'];
        $result = $logic_auction_buy->update_auction_order($_POST, $member_id);

        if ($result['state']) {
            //转向到商城支付页面
            output_data(array('pay_sn' => $result['data']['order_sn'], 'payment_code' => $result['data']['payment_code']));
        } else {
            output_error($result);
        }
    }

    public function check_auction_typeOp(){
        $model_margin_order = Model('margin_orders');
        $member_id = $this->member_info['member_id'];
        $condition = array(
            'buyer_id'=>$member_id,
            'order_state'=>array('neq','4'),
            'auction_id'=>array('neq',$_POST['auction_id'])
        );
        $re = $model_margin_order->getOrderInfo($condition);
        $res = empty($re)?0:1;
        output_data($res);
    }

    public function check_margin_orderOp(){
        $order_sn = $_GET['order_sn'];
        $info = Model('margin_orders')->getOrderInfo(array('order_sn'=>$order_sn));
        if(!empty($info)&&$info['order_state'] == 1){
            output_data(1);
        }else{
            output_data(0);
        }
    }
}
