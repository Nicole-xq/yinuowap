<?php
/**
 * 我的收藏
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_favoritesControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 收藏列表
     */
    public function favorites_listOp() {
        $model_favorites = Model('favorites');
        $model_auctions = Model('auctions');
        $model_guess = Model('p_guess');
        $favorites_list = $model_favorites->getWapFavoritesList(array('member_id'=>$this->member_info['member_id']), '*', $this->page);
        $page_count = $model_favorites->gettotalpage();
        $favorites_id = '';
        $auctions_ids = '';
        foreach ($favorites_list as $value){
            if ($value['fav_type'] == 'goods') {
                // 商品ID 字符串
                $favorites_id .= $value['fav_id'] . ',';
            } elseif ($value['fav_type'] == 'auction') {
                $auctions_ids .= $value['fav_id'] . ',';
            } elseif ($value['fav_type'] == 'guess') {
                $guess_ids .= $value['fav_id'] . ',';
            }
        }
        $favorites_id = rtrim($favorites_id, ',');
        $auctions_ids = rtrim($auctions_ids, ',');
        $guess_ids = rtrim($guess_ids, ',');
        // 商品
        $model_goods = Model('goods');
        $field = 'goods_id,goods_name,goods_promotion_price,goods_image,store_id';
        $goods_list = $model_goods->getGoodsList(array(
            'goods_id' => array('in', $favorites_id),
            // 默认不显示预订商品
            'is_book' => 0,
        ), $field);

        foreach ($goods_list as $key=>$value) {
            $goods_list[$key]['fav_id'] = $value['goods_id'];
            $goods_list[$key]['goods_image_url'] = cthumb($value['goods_image'], 240, $value['store_id']);
            $goods_list[$key]['goods_price'] = $value['goods_promotion_price'];
            $goods_list[$key]['type'] = 'goods';
            unset($goods_list[$key]['goods_promotion_price']);
        }

        // 拍品
        $field = 'auction_id,store_id,auction_image,auction_start_price,current_price,auction_name';
        $auction_list = $model_auctions->getAuctionList(
            array(
                'auction_id' => array('in', $auctions_ids)
            ),$field
        );
        foreach ($auction_list as $key => $value) {
            $auction_list[$key]['fav_id'] = $value['auction_id'];
            $auction_list[$key]['goods_image_url'] = cthumb($value['auction_image'], 240, $value['store_id']);
            $auction_list[$key]['current_price'] = $value['current_price'] != 0 ? $value['current_price'] : $value['auction_start_price'];
            $auction_list[$key]['type'] = 'auction';
        }
        // 趣猜
        $field = 'gs_id,store_id,gs_min_price,gs_max_price,gs_name';
        $guess_list = $model_guess->getGuessList1(
            array(
                'gs_id' => array('in', $guess_ids)
            ),$field);
        foreach ($guess_list as $key => $value) {
            $guess_list[$key]['fav_id'] = $value['gs_id'];
            $guess_list[$key]['goods_image_url'] = $this->get_gs_img($value['gs_id']);
            $guess_list[$key]['gs_max_price'] = $value['gs_max_price'];
            $guess_list[$key]['gs_min_price'] = $value['gs_min_price'];
            $guess_list[$key]['type'] = 'guess';
        }
        // 拼接
        $goods_list = array_merge($goods_list, $auction_list);
        $goods_list = array_merge($goods_list,$guess_list);
        output_data(array('favorites_list' => $goods_list), mobile_page($page_count));
    }

    /**
     * 添加收藏
     */
    public function favorites_addOp() {
        $favorites_model = Model('favorites');
        $auction_model = Model('auctions');
        $goods_model = Model('goods');
        if($_POST['fav_type'] == 'goods'){
            $goods_id = intval($_POST['goods_id']);
            if ($goods_id <= 0){
                output_error('参数错误');
            }

            //判断是否已经收藏
            $favorites_info = $favorites_model->getOneFavorites(array('fav_id'=>$goods_id,'fav_type'=>'goods','member_id'=>$this->member_info['member_id']));
            if(!empty($favorites_info)) {
                output_error('您已经收藏了该商品');
            }

            //判断商品是否为当前会员所有
            $goods_info = $goods_model->getGoodsInfoByID($goods_id);
            $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$this->member_info['member_id']));
            if ($goods_info['store_id'] == $seller_info['store_id']) {
                output_error('您不能收藏自己发布的商品');
            }


        }elseif($_POST['fav_type'] == 'auction'){
            $auction_id = $_POST['auction_id'];
            if($auction_id <= 0){
                output_error('参数错误');
            }

            //判断是否已经收藏
            $favorites_info = $favorites_model->getOneFavorites(array('fav_id'=>$auction_id,'fav_type'=>'auction','member_id'=>$this->member_info['member_id']));
            if(!empty($favorites_info)) {
                output_error('您已经收藏了该商品');
            }

            //判断商品是否为当前会员所有
            $auction_info = $auction_model->getAuctionsInfoByID($auction_id);
            $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$this->member_info['member_id']));
            if ($auction_info['store_id'] == $seller_info['store_id']) {
                output_error('您不能收藏自己发布的商品');
            }
        }

        //添加收藏
        $insert_arr = array();
        $insert_arr['member_id'] = $this->member_info['member_id'];
        $insert_arr['member_name'] = $this->member_info['member_name'];
        $insert_arr['fav_id'] = ($_POST['fav_type'] == 'goods') ? $goods_id : $auction_id;
        $insert_arr['fav_type'] = ($_POST['fav_type'] == 'goods') ? 'goods' : 'auction';
        $insert_arr['fav_time'] = TIMESTAMP;
        $result = $favorites_model->addFavorites($insert_arr);

        if ($result){
            if($_POST['fav_type'] == 'goods') {
                //增加收藏数量
                $goods_model->editGoodsById(array('goods_collect' => array('exp', 'goods_collect + 1')), $goods_id);
                output_data('1');
            }elseif($_POST['fav_type'] == 'auction'){
                $auction_model->editAuctionsById(array('auction_collect' => array('exp', 'auction_collect + 1')), $auction_id);
                output_data('1');
            }
        }else{
            output_error('收藏失败');
        }
    }

    /**
     * 删除收藏
     */
    public function favorites_delOp() {
        $fav_id = intval($_POST['fav_id']);
        $fav_type = $_POST['fav_type'] ?$_POST['fav_type']: 'goods';
        if ($fav_id <= 0){
            output_error('参数错误');
        }

        $model_favorites = Model('favorites');
        if ($fav_type == 'goods') {
            $model_goods = Model('goods');
        } elseif($fav_type == 'auction') {
            $auctions_model = Model('auctions');
        }else{
            $guess_model = Model('p_guess');
        }

        $condition = array();
        $condition['fav_type'] = $fav_type;
        $condition['fav_id'] = $fav_id;
        $condition['member_id'] = $this->member_info['member_id'];
//        print_R($condition);
        //判断收藏是否存在
        $favorites_info = $model_favorites->getOneFavorites($condition);
//        print_R($favorites_info);exit;
        if(empty($favorites_info)) {
            output_error('收藏删除失败');
        }
        $model_favorites->delFavorites($condition);
        if ($fav_type == 'goods') {
            $model_goods->editGoodsById(array('goods_collect' => array('exp', 'goods_collect - 1')), $fav_id);
        } elseif($fav_type == 'auction') {
            $auctions_model->editAuctionsById(array('auction_collect' => array('exp', 'auction_collect - 1')), array($fav_id));
        }else{
            $guess_model->editGuess(array('gs_id'=>$fav_id),array('gs_collect'=>array('exp','gs_collect - 1')));
        }

        output_data('1');
    }
    /**
     * 收藏详情
     */
    public function favorites_infoOp() {
        $param = $_POST;
        $fav_id = $param['fav_id']?intval($param['fav_id']):0;
        if (!$fav_id) {
            output_data(array());
        }
        $model_favorites = Model('favorites');
        $where = array();
        $where['member_id'] = $this->member_info['member_id'];
        $where['fav_id'] = $fav_id;
        $where['fav_type'] = 'goods';
        $favorites_info = $model_favorites->getOneFavorites($where);
        if (!$favorites_info) {
            output_data(array());
        }
        $field = 'goods_id,goods_name,goods_price,goods_image,store_id';
        $goods_info = Model('goods')->getGoodsInfo(array(
            'goods_id' => $favorites_info['fav_id'],
            'is_book' => 0,// 默认不显示预订商品
        ), $field);
        $favorites_info = array_merge($favorites_info,$goods_info);
        $favorites_info['goods_image_url'] = cthumb($favorites_info['goods_image'], 240, $favorites_info['store_id']);
        output_data(array('favorites_info' => $favorites_info));
    }

     /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240);
    }   
}
