<?php
/**
 * 拍卖
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctionControl extends mobileHomeControl
{

    const PAGE_SIZE = 2;

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * 商场新需求拍卖首页
     */
    public function indexOp()
    {
        $sourceType = isset($_REQUEST['sourceType']) ? $_REQUEST['sourceType'] : '';
        /** @var mb_classModel $model_mb_class */
        $model_mb_class = Model('mb_class');
        $SpecialUsableList = $model_mb_class->getNewSpecialUsableListByID();
        //推荐专场列表 调取数量5个   正在拍优先级大于预展中
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        Model('auctions');
        $current_time = time();
        $start_condition = "is_rec = '1' and is_open = '1' and special_start_time < {$current_time}
         and special_type <> ".\App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE."
         and special_end_time = 0";
        $limit = 5;
        $fields = 'special_id, wap_image, special_name, special_click, special_state, special_rate,
         special_preview_start, special_start_time, special_end_time, special_rate_time, is_pd_pay as is_nb';
        $special_list = $model_special
            ->getSpecialList(
                $start_condition,
                $fields,
                'special_id asc',
                0,
                $limit
            );
        $special_count = count($special_list);
        if ($special_count != 5) {
            $limit = $limit - $special_count;
            $preview_condition = "is_rec = '1' and is_open = '1' and special_preview_start < {$current_time}
             and special_type <> ".\App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE."
             and special_start_time > {$current_time}
             and special_end_time = 0";
            $preview_list = $model_special
                ->getSpecialList(
                    $preview_condition,
                    'special_id, wap_image, special_name, special_click, special_state, special_rate,
                     special_preview_start, special_start_time, special_end_time, special_rate_time,
                      is_pd_pay as is_nb',
                    'special_start_time asc',
                    0,
                    $limit
                );
            $special_list = array_merge($special_list, $preview_list);
        }
        if (is_array($special_list) && count($special_list) > 0) {
            $special_first = $special_second = [];
            $current_time = time();
            foreach ($special_list as &$special) {
                $special['wap_image'] = getVendueLogo($special['wap_image']);
                $special['special_end_time'] = date('Y-m-d H:i:s', $special['special_end_time']);
                $special['special_start_time'] = date('Y-m-d H:i:s', $special['special_start_time']);
                $special['special_rate_time'] = date('Y-m-d H:i:s', $special['special_rate_time']);
                $special['special_preview_start'] = date('Y-m-d H:i:s', $special['special_preview_start']);
                if (isset($special['special_start_time'])
                    && strtotime($special['special_start_time']) < $current_time) {
                    $special_first[] = $special;
                } else {
                    $special_second[] = $special;
                }
            }
            $special_first = $this->sortByKey($special_first, 'special_id');
            $special_second = $this->sortByKey($special_second, 'special_start_time');
            $new_special_list = array_merge($special_first, $special_second);
        } else {
            $new_special_list = [];
        }

        //拍乐赚 专场选取三个专场未结束 ID升序
        $paiLe_condition = "is_open = '1' and special_end_time = 0 and store_id = 0 and special_rate = 0 and special_type = '0'";
        $limit = 3;
        $paiLe_list = $model_special
            ->getSpecialList(
                $paiLe_condition,
                $fields,
                'special_id asc',
                0,
                $limit
            );
        foreach ($paiLe_list as &$paiLe) {
            $paiLe['wap_image'] = getVendueLogo($paiLe['wap_image']);
            $paiLe['special_end_time'] = date('Y-m-d H:i:s', $paiLe['special_end_time']);
            $paiLe['special_start_time'] = date('Y-m-d H:i:s', $paiLe['special_start_time']);
            $paiLe['special_rate_time'] = date('Y-m-d H:i:s', $paiLe['special_rate_time']);
            $paiLe['special_preview_start'] = date('Y-m-d H:i:s', $paiLe['special_preview_start']);
        }
        //收益专场选取三个 未结束 ID升序
        $current_time = time();
        $earnings_condition = "special_rate_time > {$current_time} and special_rate > 0 and is_open = '1' and special_type = '0'
         and special_end_time = 0";
        $limit = 3;
        $earnings_list = $model_special
            ->getSpecialList(
                $earnings_condition,
                $fields,
                'special_id asc',
                0,
                $limit
            );
        foreach ($earnings_list as &$earnings) {
            $earnings['wap_image'] = getVendueLogo($earnings['wap_image']);
            $earnings['special_end_time'] = date('Y-m-d H:i:s', $earnings['special_end_time']);
            $earnings['special_start_time'] = date('Y-m-d H:i:s', $earnings['special_start_time']);
            $earnings['special_rate_time'] = date('Y-m-d H:i:s', $earnings['special_rate_time']);
            $earnings['special_preview_start'] = date('Y-m-d H:i:s', $earnings['special_preview_start']);
        }
        $newSpecialUsable = [
            'adv_list' => [],
            'sem' => [],
        ];
        foreach ($SpecialUsableList as $val) {
            if ($val['adv_list']) {
                //接口返回轮播图 直接显示地址--仅仅做兼容处理
                //增加判断 小程序的轮播图分开处理
                if ($sourceType == 'mini') {
                    $newSpecialUsable['adv_list'] = $this->mini_img_url_dispose($val['adv_list']);
                } else {
                    $newSpecialUsable['adv_list'] = $this->img_url_dispose($val['adv_list']);
                }
            }
            if ($val['sem']) {
                //接口返回轮播图 直接显示地址--仅仅做兼容处理
                $newSpecialUsable['sem'] = $this->img_url_dispose($val['sem']);
            }
            unset($val);
        }
        $return['special_list'] = $new_special_list;
        $return['paiLe_list'] = $paiLe_list;
        $return['earnings_list'] = $earnings_list;
        foreach ($newSpecialUsable as $key => $value) {
                $return[$key] = $value;
        }
        output_data($return);
    }

    public function hotAuctionOp()
    {
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        //推荐热拍
        $model_auctions = Model('auctions');
        $pageSize = $model_auctions->page;
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        $condition = "recommended = 1 and state = 0 and auction_bond_rate = 0 and auction_type <> ".\App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE;
        $page_count = $model_auctions->where($condition)->count();
        $fields = '*';
        $model_auctions = new auctionsModel();
        $condition = "recommended = 1 and state = 0 and auction_bond_rate = 0 and auction_type <> ".\App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE;;
        $special_goods = $model_auctions
            ->getNbAuctionList(
                $condition,
                $fields,
                $order = 'auction_id asc',
                $limit
            );
        $return = [];
        //TODO  拍卖详情需要调整字段
        if (!empty($special_goods) && is_array($special_goods)) {
            $special_ids = array_column($special_goods, 'special_id');
            $special_ids = "'" . implode(",", $special_ids) . "'";
            $nb_condition['special_id'] = array('in', $special_ids);
            /** @var auction_specialModel $special */
            $special = Model('auction_special');
            $nb_list = $special->field('special_id, is_pd_pay')->where($nb_condition)->select();
            foreach ($special_goods as $goods) {
                $auction_info['auction_id'] = $goods['auction_id'];
                $auction_info['goods_common_id'] = $goods['goods_common_id'];
                $auction_info['store_id'] = $goods['store_id'];
                $auction_info['is_nb'] = isset($nb_list['is_nb']) && $nb_list['is_nb'] == 1 ? 1 : 0;
                $auction_info['auction_name'] = $goods['auction_name'];
                $auction_info['auction_start_price'] = $goods['auction_start_price'];
                $auction_info['auction_image'] = $goods['auction_image'];
                $auction_info['auctions_artist_name'] = $goods['auctions_artist_name'];
                $auction_info['current_price'] = $goods['current_price'];
                $auction_info['auction_start_time'] = $goods['auction_start_time'];
                $auction_info['auction_end_time'] = $goods['auction_end_time'];
                $auction_info['auction_preview_start'] = $goods['auction_preview_start'];
                $auction_info['auction_start_time'] = date('Y-m-d H:i:s', $goods['auction_start_time']);
                $auction_info['auction_end_time'] = date('Y-m-d H:i:s', $goods['auction_end_time']);
                $auction_info['auction_preview_start'] = date('Y-m-d H:i:s', $goods['auction_preview_start']);
                $auction_info['auction_image'] = cthumb($goods['auction_image'], 240, $goods['store_id']);
                $auction_info['state'] = $goods['state'];
                $return[] = $auction_info;
            }
        }
        output_data($return, api_page($page_count, $page, $pageSize));
    }

    public function auction_listOp()
    {
        /** @var auctionsModel $model_auctions */
        /** @var favoritesModel $favorites */
        //分类筛选
        $auctions_class_lv1 = $_GET['auctions_class_lv1'];
        $auctions_class_lv2 = $_GET['auctions_class_lv2'];
        if (!empty($auctions_class_lv2)) {
            if (!empty($auctions_class_lv2)) {
                $auctions_class_lv2 = implode(",", $auctions_class_lv2);
                $condition['auctions_class_lv2'] = ['in', $auctions_class_lv2];
            }
        } else {
            if (!empty($auctions_class_lv1)) {
                $condition['auctions_class_lv1'] = $auctions_class_lv1;
            }
        }
        if (isset($gc_id) && is_array($gc_id)) {
            $gc_id = array_filter($gc_id);
            if (!empty($gc_id)) {
                $gc_id = implode(",", $gc_id);
                $condition['gc_id'] = ['in', $gc_id];
            }
        }
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $model_auctions = Model('auctions');
        $begin = ($page - 1) * $model_auctions->page;
        $limit = $begin . ',' . $model_auctions->page;
        //这一行添加搜索条件字段

        $condition['is_liupai'] = 0;
        $condition['state'] = 0;
        $condition['auction_end_time'] = ['gt', time()];
        $condition['special_id'] = array('neq', 0);
        intval($_GET['store_id']) > 0 ? $condition['store_id'] = intval($_GET['store_id']) : '';
        //排序方式
        //推荐排序 排前边
        $order = 'recommended desc';
        $rank = $_GET['rank'];
        if (isset($order) && in_array($rank, ['asc', 'desc'])) {
            $order .= ', current_price ' . $rank;
        } else {
            $order .= ', auction_start_time asc';
        }
        //拍卖状态显示
        $condition['auction_bond_rate'] = 0;
        $countField = '*';
        $group = '';
        $fields =  'current_price,auction_id';
        if ($_GET['is_kai_pai'] == 1) {          //   正在拍
            $condition['auction_start_time'] = ['elt', time()];
            $condition['auction_end_time'] = ['gt', time()];
        } elseif ($_GET['is_kai_pai'] == 3) {     //   预展
            $condition['auction_preview_start'] = ['elt', time()];
            $condition['auction_start_time'] = ['gt', time()];
        } elseif ($_GET['is_kai_pai'] == 4) {
            //已结束的拍品
            $condition = [];
            $condition['auction_id'] = array('gt', 4231);
            $condition['auction_end_time'] = [['elt', time()], ['neq', 0]];
            //参考价的60%
            $condition['__RAW'] = "current_price >= auction_reference_price * 0.6";
            $order = ' auction_id desc';
            $group = "auction_name";
            $countField = "distinct auction_name";
            $fields = 'SUBSTRING_INDEX(GROUP_CONCAT(auction_id ORDER BY current_price desc),\',\',1) auction_id, max(current_price) as current_price';
        }
        if (!empty($_GET['keyword'])) {
            $condition['auction_name'] = array('like', '%' . $_GET['keyword'] . '%');
        }
        $condition["auction_type"] = ["neq", \App\Models\ShopncAuction::AUCTION_TYPE_NEWBIE];
            $fields .= ', goods_common_id, store_id, auction_name, auction_image, recommended, auction_start_price,auction_start_time, auction_end_time, auction_preview_start, state, special_id, auction_end_true_t';
        $count = $model_auctions->getAuctionsCount($condition, $countField);

        $auctions_list = $model_auctions
            ->getAuctionList(
                $condition,
                $fields,
                $group,
                $order,
                $limit,
                0,
                $count
            );
        if (empty($auctions_list)) {
            output_data(array('auction_list' => []), api_page($count, $page, $model_auctions->page));
        }
        $auctions_ids_arr = [];
        /** @var auction_configModel $auction_config_model */
        $auction_config_model = Model('auction_config');
        $auction_config = $auction_config_model->getInfo(1);
        $special_ids = array_column($auctions_list, 'special_id');
        $special_ids = "'" . implode(",", $special_ids) . "'";
        $nb_condition['special_id'] = array('in', $special_ids);
        /** @var auction_specialModel $special */
        $special = Model('auction_special');
        $nb_list = $special->field('special_id, is_pd_pay')->where($nb_condition)->select();
        $new_nb_list = [];
        foreach ($nb_list as $value) {
            $new_nb_list[$value['special_id']] = $value['is_pd_pay'];
        }
        $nb_list = $new_nb_list;
        foreach ($auctions_list as $k => $auctions_info) {
            $auctions_ids_arr[] = $auctions_info['auction_id'];
            if (time() >= $auctions_info['auction_start_time']) {
                if ($auctions_info['auction_end_true_t'] >= time()) {
                    $is_kaipai = 1; //拍卖中
                } elseif ($auctions_info['auction_end_true_t'] < time()) {
                    $is_kaipai = 2; //已结束
                    //过滤测试拍品展示
                    if(isNcProduction()){
                        $val = '补差价';
                        preg_match('/' . $val . '/', $auctions_info['auction_name'], $matches);//匹配值
                        if (!empty($matches[0])) {
                            unset($auctions_list[$k]);
                            continue;
                        }
                        $val = '测试';
                        preg_match('/' . $val . '/', $auctions_info['auction_name'], $matches);//匹配值
                        if (!empty($matches[0])) {
                            unset($auctions_list[$k]);
                            continue;
                        }
                    }
                }
                $auctions_list[$k]['auction_remain_date']
                    = $auctions_info['auction_end_true_t'] - time() > 0
                    ? $auctions_info['auction_end_true_t'] - time() : 0;
            } else {
                $auctions_list[$k]['auction_remain_date']
                    = $auctions_info['auction_start_time'] - time() > 0
                    ? $auctions_info['auction_start_time'] - time() : 0;

                $is_kaipai = 3;
            }
            $auctions_list[$k]['duration_time'] = isset($auction_config['duration_time'])
                ? $auction_config['duration_time'] : 60;
            $auctions_list[$k]['add_time'] = isset($auction_config['add_time'])
                ? $auction_config['add_time'] : 2;
            $auctions_list[$k]['is_kaipai'] = $is_kaipai;
            $auctions_list[$k]['is_nb'] = isset($nb_list[$auctions_info['special_id']])
            && $nb_list[$auctions_info['special_id']] == 1
                ? 1 : 0;
            $auctions_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'], 360, 0);
            $auctions_list[$k]['auction_start_date'] = date('m月d日 H:i', $auctions_info['auction_start_time']);
            $auctions_list[$k]['auction_end_date'] = date('m月d日 H:i', $auctions_info['auction_end_time']);
            $auctions_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00
                ? number_format($auctions_info['current_price'], 0)
                : number_format($auctions_info['auction_start_price'], 0);
        }

        $member_id = (int)$this->getMemberIdIfExists();
        if ($member_id > 0) {
            $condition = [
                'fav_id' => ['in', $auctions_ids_arr],
                'member_id' => $member_id
            ];
            $favorites = Model('favorites');
            $auctions_favorite_list = $favorites->getAuctionsFavoritesList($condition, 'log_id , fav_id');
            $auctions_favorite_list = array_under_reset($auctions_favorite_list, 'fav_id');
            foreach ($auctions_list as &$item) {
                $item['is_favorate'] = $auctions_favorite_list[$item['auction_id']] ? true : false;
            }
            unset($item);
        }
        $auctions_list = array_values($auctions_list);
        output_data(array('auction_list' => $auctions_list), api_page($count, $page, $model_auctions->page));
    }

    private function img_url_dispose($pending_data)
    {
        $config = Shopnc\Core::getConfigs();
        foreach ($pending_data as $key => &$adv_list) {
            switch ($adv_list['type']) {
                //商品关键字
                case 'keyword':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=goods&op=goods_list&keyword=' . $adv_list['data'];
                    break;
                //趣猜编号
                case 'mini':
                    unset($pending_data[$key]);
                    break;
                //拍品关键字
                case 'auction':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=auction&op=auction_list&page=10&curpage=1&key=&keyword='
                        . $adv_list['data'];
                    break;
                //专题编号
                case 'special':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=auction&op=auction_list&special_id='
                        . $adv_list['data'] . '&page=10&curpage=1';
                    break;
                //商品编号
                case 'goods':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=goods&op=goods_detail&goods_id='
                        . $adv_list['data'];
                    break;
                //拍品编号
                case 'pai':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=auction&op=auction_detail&auction_id='
                        . $adv_list['data'];
                    break;
                //链接
//                            case 'url':
//                                //这里不做处理
//                                break;
                //店铺编号
                case 'store':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=store&op=store_info&key=&store_id='
                        . $adv_list['data'] . '&page=20&curpage=1&keyword=&goods_commend=1';
                    break;
                //趣猜编号
                case 'guess':
                    $adv_list['url'] = $config['mobile_site_url']
                        . '/index.php?act=guess&op=guess_detail&gs_id=' . $adv_list['data'];
                    break;
                default:
                    $adv_list['url'] = $adv_list['data'];
                    break;
            }
            unset($adv_list['type'], $adv_list['data']);
        }
        return $pending_data;
    }


    private function mini_img_url_dispose($pending_data)
    {
        foreach ($pending_data as $key => &$adv_list) {
            if ($adv_list['type'] != 'mini') {
                unset($pending_data[$key]);
                continue;
            }
            unset($adv_list['type'], $adv_list['data']);
        }
        return $pending_data;
    }

    /**
     * 拍卖列表排序方式
     * TODO 待删除
     */
    private function _auction_list_order($key, $order)
    {
        $result = 'auction_id desc';
        if (!empty($key)) {

            $sequence = 'desc';
            if ($order == 1) {
                $sequence = 'asc';
            }

            switch ($key) {
                //最新
                case '1' :
                    $result = 'auction_start_time' . ' ' . $sequence;
                    break;
                //浏览量
                case '2' :
                    $result = 'bid_number' . ' ' . $sequence;
                    break;
                //价格
                case '3' :
                    $result = 'current_price' . ' ' . $sequence;
                    break;
                //最
                case '4' :
                    $result = 'auction_end_time' . ' ' . $sequence;
                    break;
            }
        }
        return $result;
    }


    /*
     * 拍卖专场 数组按照 数组某一key的value排序
     */
    public function sortByKey($array, $keys, $sort = 'asc')
    {
        $newArr = $valArr = array();
        foreach ($array as $key => $value) {
            $valArr[$key] = $value[$keys];
        }
        ($sort == 'asc') ? asort($valArr) : arsort($valArr);//先利用keys对数组排序，目的是把目标数组的key排好序
        reset($valArr); //指针指向数组第一个值
        foreach ($valArr as $key => $value) {
            $newArr[$key] = $array[$key];
        }
        return $newArr;
    }

    /*
     * 拍卖详情
     */
    public function auction_detailOp()
    {
        // is_kaipai 是否开拍 1是（正在进行） 0否 2是（已结束）

        if (intval($_GET['auction_id']) <= 0) {
            output_error('非法参数');
        }
        $auction_id = intval($_GET['auction_id']);

        /** @var auctionsModel $model_auction */
        /** @var auctionLogic $logic_auction */
        /** @var goodsModel $model_goods */
        /** @var goodsLogic $logic_goods */
        $model_auction = Model('auctions');
        $logic_auction = Logic('auction');
        $model_goods = Model('goods');
        $logic_goods = Logic('goods');

        // ---------new
        $fields = 'auction_id, special_id, auction_name, auction_image, other_images, auction_start_price,
         current_price, auction_increase_range, auctions_artist_name, create_age, auctions_long,
         auctions_width, auctions_height, auctions_spec, auctions_class_lv1, auctions_class_lv2,
        auction_bond_rate, interest_last_date, auction_click, num_of_applicants, auctions_summary,
        auction_preview_start, auction_start_time, auction_end_true_t, auction_end_time, special_id, bid_number,
        auction_reference_price, auction_reserve_price';
        $auction_detail = $model_auction->getAuctionsInfo(['auction_id' => $auction_id], $fields);
        //output_data($auction_detail);
        if (empty($auction_detail)) {
            output_error('拍品不存在或未审核');
        }
        //判断是否诺币专场
        /** @var auction_specialModel $special */
        $special = Model('auction_special');
        $nb_condition = ['special_id' => $auction_detail['special_id']];
        $special_info = $special
            ->getSpecialInfo($nb_condition, $field = "special_id,special_name,is_pd_pay");
        $auction_detail['is_nb'] = isset($special_info['is_pd_pay']) && $special_info['is_pd_pay'] == 1 ? 1 : 0;
        $auction_detail['is_fetch'] = 0;
        $auction_detail['order_id'] = 0;
        $auction_detail['is_pay'] = 0;
        $auction_detail['is_mind'] = 0;
        $auction_detail['margin_money'] = 0;
        //介绍拍乐赚
        $auction_detail['recommended'] = 0;
        //最后一次出价时间
        $auction_detail['last_offer_time'] = 0;
        $auction_detail['auction_end_true_t'] = $auction_detail['auction_end_time'];
        $auction_detail['commission_amount'] = 0;
        $auctions_class_lv1_info = Model('auctions_class')->getOneById($auction_detail['auctions_class_lv1']);
        $auctions_class_lv1_name = $auctions_class_lv1_info['auctions_class_name'];
        $auctions_class_lv2_info = Model('auctions_class')->getOneById($auction_detail['auctions_class_lv2']);
        $auctions_class_lv2_name = $auctions_class_lv2_info['auctions_class_name'];
        $auction_detail['auctions_class_lv1_name'] = $auctions_class_lv1_name;
        $auction_detail['auctions_class_lv2_name'] = $auctions_class_lv2_name;
        /** @var auction_configModel $auction_config_model */
        $auction_config_model = Model('auction_config');
        $auction_config = $auction_config_model->getInfo(1);
        $auction_detail['add_time'] = isset($auction_config['add_time'])
            ? $auction_config['add_time'] : 2;
        $auction_detail['auction_image'] = cthumb($auction_detail['auction_image'], 360);
        $auction_detail['auctions_summary'] = html_entity_decode(
            htmlspecialchars_decode($auction_detail['auctions_summary'])
        );
        $auction_body = '<div class="default">' . $auction_detail['auctions_summary'] . '</div>';
        $auction_info['auctions_summary'] = $auction_body;
        if (!empty($auction_detail['auctions_spec'])) {
            $auctions_spec_info = json_decode($auction_detail['auctions_spec'], true);
        }
        $new_spec = [];
        if (!empty($auctions_spec_info) && is_array($auctions_spec_info)) {
            /** @var $auctions_spec_model $auctions_spec_model */
            /** @var $auctions_spec_class_model $auctions_spec_model */
            $auctions_spec_model = Model('auctions_spec');
            $auctions_spec_class_model = Model('auctions_spec_class');
            foreach ($auctions_spec_info as $key => $value) {
                $spec_info = $auctions_spec_model->getOneById($key);
                if (empty($spec_info)) {
                    continue;
                }
                $class_info = $auctions_spec_class_model->getOneById($value);
                if (empty($class_info)) {
                    continue;
                }
                $key = $spec_info['auctions_spec_name'];
                $value = $class_info['auctions_spec_class_name'];
                $new_spec1['name'] = $key;
                $new_spec1['value'] = $value;
                $new_spec[] = $new_spec1;
            }
        } else {
            $new_spec = [];
        }
        $auction_detail['auctions_spec'] = $new_spec;
        //专场推荐
        /** @var auction_specialModel $model_special */
        $auction_detail['special_name'] = $special_info['special_name'];
        //获取作品分类名
        /** @var auctions_spec_classModel $category_model */
        $auction_detail['interest_last_date'] = strtotime($auction_detail['interest_last_date']);
        $other_images = [];
        if (isset($auction_detail['other_images']) && !empty(isset($auction_detail['other_images']))) {
            $auction_detail['other_images'] = json_decode($auction_detail['other_images'], true);
            foreach ($auction_detail['other_images'] as $key => $value) {
                $other_images[] = cthumb($value,360);
            }
        }
        $auction_detail['other_images'] = $other_images;
        array_unshift($auction_detail['other_images'], $auction_detail['auction_image']);
        $auction_detail['bid_num'] = $auction_detail['bid_number'] > 0 ? $auction_detail['bid_number'] : '';
        unset($auction_detail['bid_number']);
        //是否提提醒
        $auction_detail['is_mind'] = 0;
        //返佣金额
        $auction_detail['commission_amount'] = 0;
        //保证金金额
        $auction_detail['margin_money'] = 0;

        /**
         * 用户相关信息 ------------------------------
         */

        $member_id = (int)$this->getMemberIdIfExists();
        if (!empty($member_id)) {
            //是否提示介绍拍乐赚
            /** @var memberModel $model_member */
            $model_member = Model('member');
            $member_info = $model_member->getMemberInfoByID($member_id);
            if ($member_info['recommended'] == 0) {
                $auction_detail['recommended'] = 1;
            }
            //当前用户是否拍中
            if ($auction_detail['auction_end_true_t'] <= time()) {
                $order = Model('orders');
                $fetchWhere = [
                    'buyer_id' => $member_id,
                    'auction_id' => $auction_id
                ];
                $order_fetch = $order->where($fetchWhere)->find();
                if (empty($order_fetch)) {
                    //显示当前最高出价人, 方便片品结束生成订单显示
                    $bidWhere = ['auction_id' => $auction_id];
                    $auction_detail['auction_reserve_price'] = $auction_detail['auction_reserve_price'] ?: 0;
                    $bidWhere['offer_num']=array('egt',$auction_detail['auction_reserve_price']);
                    $bidder_member_info = Model('bid_log')->where($bidWhere)
                        ->field('member_id')
                        ->order('offer_num desc')->find();
                    $bidder_member_id = isset($bidder_member_info['member_id'])
                        ? $bidder_member_info['member_id'] : 0;
                    if ($bidder_member_id == $member_id) {
                        $auction_detail['is_fetch'] = 1;
                    }
                } else {
                    $auction_detail['is_fetch'] = 1;
                    $auction_detail['order_id'] = $order_fetch['order_id'];
                    if ($order_fetch['order_state'] == 10 || $order_fetch['order_state'] == 0) {
                        $auction_detail['is_pay'] = 0;
                    } else {
                        $auction_detail['is_pay'] = 1;
                    }
                }
            }
            //出价提醒
            $relation_model = Model('auction_member_relation');
            $where = [
                'auction_id' => $auction_id,
                'member_id' => $member_id,
            ];
            $remind = $relation_model->where($where)->find();
            if ($remind) {
                if ($remind['is_remind'] != 0) {
                    $auction_detail['is_mind'] = 1;
                }
            }

            //佣金保证金查询
            $margin_orders = Model('margin_orders');
            $condition = [];
            //$auction_start_time = $auction_detail['auction_start_time'];
            $condition['auction_id'] = $auction_id;
            $condition['buyer_id'] = $member_id;
            $condition['order_state'] = 1; //订单已支付状态
            //$condition['updated_at'] = array('lt', $auction_start_time);
            $margin_amount_sum = $margin_orders->where($condition)->sum('margin_amount');
            if($margin_amount_sum > 0){
                $auction_detail['margin_money'] = $margin_amount_sum;
            }
            $member_commission_model = Model('member_commission');
            $condition = [];
            $condition['goods_id'] = $auction_id;
            $condition['dis_member_id'] = $member_id;
            $condition['dis_commis_state'] = 1; //佣金已结算
            $condition['commission_type'] = 7; //竞拍返佣
            //TODO 竞拍返佣总金额计算
            $commission_amount = $member_commission_model->where($condition)->sum('commission_amount');
            if($commission_amount > 0){
                $auction_detail['commission_amount'] = $commission_amount;
            }
            //用户最后出价时间
/*            $condition = [
                'member_id' => $member_id,
                'auction_id' => $auction_id
            ];
            $bid_info =  Model('bid_log')->where($condition)->order('created_at desc')->field('created_at')->find();
            if ($bid_info) {
                $auction_detail['last_offer_time'] = $bid_info['created_at'] ?: 0;
            }*/
        }
        if (($auction_detail['auction_end_true_t'] <= 0 || $auction_detail['auction_end_true_t'] > time()) && !empty($_REQUEST['isFirst'])) {
            //浏览量+1
            $click = 1;
            $model_auction->editAuctionsById(
                ['auction_click' => array('exp', 'auction_click + ' . $click)],
                array($auction_id)
            );
            $special->editSpecial(
                ['special_click' => array('exp', 'special_click + ' . $click)],
                $nb_condition
            );
        }
        unset($auction_detail['auction_reserve_price']);
        output_data($auction_detail);






        //$auction_detail = $model_auction->getAuctionDetail($auction_id);
        //$auction_info = $auction_detail['auction_info'];
        $auction_info = $auction_detail;
        if (empty($auction_info)) {
            output_error('拍品不存在或未审核');
        }
        if (time() >= $auction_info['auction_start_time']) {
            if ($auction_info['auction_start_time'] <= time()
                && (($auction_info['auction_end_time'] >= time()
                        && empty($auction_info['auction_end_true_t']))
                    || (!empty($auction_info['auction_end_true_t'])
                        && $auction_info['auction_end_true_t'] >= time()))) {
                $is_kaipai = 1;
            } elseif ($auction_info['auction_end_true_t'] < time()) {
                $is_kaipai = 2;
            }
        } else {
            $is_kaipai = 0;
        }
        $auction_info['auction_image_path'] = cthumb($auction_info['auction_image'], 360);
        $goods_info = $logic_goods
            ->getGoodsCommonInfo($auction_info['goods_id'], $auction_info['auction_image_path']);
        output_data($goods_info);
        $auction_info['is_kaipai'] = $is_kaipai;
        $auction_info['auction_video_path'] = goodsVideoPath($auction_info['auction_video'], $auction_info['store_id']);
        $auction_info = array_merge($auction_info, $goods_info);
        $auction_info['current_price'] = $auction_info['current_price'] != 0.00
            ? $auction_info['current_price'] : $auction_info['auction_start_price'];
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfo(array('store_id' => $auction_info['store_id']));
        $auction_info['store_avatar'] = $store_info['store_avatar']
            ? UPLOAD_SITE_URL . '/' . ATTACH_STORE . '/' . $store_info['store_avatar']
            : UPLOAD_SITE_URL . '/' . ATTACH_COMMON . DS . C('default_store_avatar');
        $auction_info['delayed_time'] = $store_info['delayed_time'];
        $auction_info['auction_start_date'] = date('m月d日 H:i', $auction_info['auction_start_time']);
        $auction_info['auction_remain_start_time'] = $auction_info['auction_start_time'] - time() > 0
            ? $auction_info['auction_start_time'] - time() : 0;
        $auction_info['auction_end_date'] = date('m月d日 H:i', $auction_info['auction_end_time']);
        $auction_info['auction_remain_date'] = $auction_info['auction_end_time'] - time() > 0
            ? $auction_info['auction_end_time'] - time() : 0;
        $auction_info['store_member_id'] = $store_info['member_id'];
        if ($auction_info['auction_preview_start'] <= time()
            && ($auction_info['auction_end_true_t'] >= time()
                || (empty($auction_info['auction_end_true_t'])
                    && $auction_info['auction_end_time'] >= time()))) {
            $auction_info['submit_bond_flag'] = true;
        }
        // 出价记录
        $result = $logic_auction->getBidList($auction_id, 5);
        $bid_log_list = $result['bid_log_list'];
        foreach ($bid_log_list as $key => &$value) {
            //$value['member_name'] = substr($value['member_name'],0,1).'***'.substr($value['member_name'],-1,1);
            $value['member_name'] = mb_substr($value['member_name'], 0, 3, 'utf-8') . '***'
                . mb_substr($value['member_name'], -1, 3, 'utf-8');
            $bid_log_list[$key]['created_at'] = date('Y-m-d H:i:s', $value['created_at']);
        }
        unset($value);
        //拍品推荐
        $where = array();
//		$where['recommended'] = 1;
        $where['store_id'] = $auction_info['store_id'];
        $where['auction_id'] = array('neq', $auction_info['auction_id']);
        $where['special_id'] = $auction_info['special_id'];
//		$where['state'] = array('in',array(0,1));
        $where['state'] = 0;
        $auction_recommended_list_tmp = $model_auction->getAuctionList($where);
        $auction_recommended_list = array();
        $tmp = array();
        if (!empty($auction_recommended_list_tmp)) {
            if (count($auction_recommended_list_tmp) <= 4) {
                $auction_recommended_list = $auction_recommended_list_tmp;
            } else {
                while (count($tmp) < 4) {
                    $num = rand(0, count($auction_recommended_list_tmp) - 1);
                    if (!in_array($num, $tmp)) {
                        $tmp[] = $num;
                    }
                }
                foreach ($tmp as $v) {
                    $auction_recommended_list[] = $auction_recommended_list_tmp[$v];
                }
            }
        }
        if (!empty($auction_recommended_list)) {

            foreach ($auction_recommended_list as $k => $auctions_info) {
                $auction_recommended_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'], 360);
                if (time() >= $auctions_info['auction_start_time']) {
                    if ($auctions_info['auction_start_time'] <= time()
                        && ($auctions_info['auction_end_time'] >= time()
                            || $auctions_info['auction_end_true_t'] >= time())) {
                        $is_kaipai = 1;
                    } elseif ($auctions_info['auction_end_true_t'] < time()) {
                        $is_kaipai = 2;
                    }
                } else {
                    $is_kaipai = 0;
                }
                $auction_recommended_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00
                    ? $auctions_info['current_price'] : $auctions_info['auction_start_price'];
                $auction_recommended_list[$k]['is_kaipai'] = $is_kaipai;
            }
        }

        //专场推荐
        $model_special = Model('auction_special');
        $special_list = $model_special
            ->getSpecialOpenList(array('store_id' => $store_info['store_id'], 'is_rec' => 1), '*', 'special_id asc', 3);
        foreach ($special_list as $key => $special_info) {
            $special_list[$key]['adv_image_path'] = getVendueLogo($special_info['wap_image']);
            $special_list[$key]['special_start_date'] = date('Y年m月d日 H:i', $special_info['special_start_time']);
            $special_list[$key]['special_end_date'] = date('Y年m月d日 H:i', $special_info['special_end_time']);
            $goods_list = $model_special
                ->getSpecialGoodsLists(
                    [
                        'special_id' => $special_info['special_id'],
                        'auction_sp_state' => 1],
                    'bid_number'
                );
            $all_bid_number = 0;
            foreach ($goods_list as $k => $v) {
                $all_bid_number += $v['bid_number'];
            }
            $special_list[$key]['all_bid_number'] = $all_bid_number;
            $special_list[$key]['special_remain_date'] = $special_info['special_end_time'] - time() > 0
                ? $special_info['special_end_time'] - time() : 0;
            if (time() >= $special_info['special_start_time']) {
                if ($special_info['special_state'] == 12) {
                    $is_kaipai = 1;
                } elseif ($special_info['special_state'] == 13) {
                    $is_kaipai = 2;
                }
            } else {
                $is_kaipai = 0;
            }
            $special_list[$key]['is_kaipai'] = $is_kaipai;
        }
        $special_info = $model_special
            ->getSpecialInfo(['special_id' => $auction_info['special_id']], $field = "special_id,special_name");
        $auction_info['special_name'] = $special_info['special_name'];
        // 整理拍品属性
        $goods_detail = $model_goods->getGoodsDetail($auction_info['goods_id']);
        foreach ($goods_detail['goods_info']['goods_attr'] as $val) {
            $auction_info['auction_attr'][] = array_values($val);
        }
        $auction_info['interest_last_time'] = strtotime($auction_info['interest_last_date']);
        //计息时间到期标识
        $auction_info['interest_flag'] = strtotime($auction_info['interest_last_date']) > time() ? true : false;
        $auction_info['interest_time_str'] = strtotime($auction_info['interest_last_date']) - time() > 0
            ? strtotime($auction_info['interest_last_date']) - time() : 0;
        //利息标识
        $auction_info['bond_flag'] = $auction_info['auction_bond_rate'] > 0 ? true : false;
        $auction_info['auction_start_time_str'] = $auction_info['auction_start_time'] - time() > 0
            ? $auction_info['auction_start_time'] - time() : 0;
        $auction_info['auction_end_time_str'] = empty($auction_info['auction_end_true_t'])
            ? ($auction_info['auction_end_time'] - time() > 0
                ? $auction_info['auction_end_time'] - time() : 0)
            : ($auction_info['auction_end_true_t'] - time() > 0
                ? $auction_info['auction_end_true_t'] - time() : 0);
        //计息天数
        $auction_info['bond_day'] = Logic('auction')
            ->getInterestDay(strtotime($auction_info['interest_last_date']), $auction_info['auction_end_time']);
        $spec_arr = array();
        if (!empty($goods_attr)) {
            foreach ($goods_attr as $v) {
                $spec_arr[] = array(
                    'name' => $v['name'],
                    'val' => $v[0]
                );
            }
        }
        $auction_info['goods_attr'] = $spec_arr;
        if (!empty($auction_info['auction_custom'])) {
            $auction_info['auction_custom'] = array_merge($auction_info['auction_custom']);
        }

        // 整理拍品详情
        $auction_info['auction_mobile_body'] = $this->auction_body($auction_info);
        // 如果已登录 判断该商品是否已被收藏&&添加浏览记录，检查会员和拍品的关系
        if ($this->getMemberIdIfExists() != 0) {
            $member_id = $this->getMemberIdIfExists();
            $c = (int)Model('favorites')->getGoodsFavoritesCountByAuctionId($auction_id, $member_id);
            $auction_info['is_favorate'] = $c > 0;
            /** @var auction_member_relationModel $model_relation */
            $model_relation = Model('auction_member_relation');
            $relation_info = $model_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
        } else {
            $relation_info = array();
        }
        // 读取服务保障
        $article = Model('article')->getArticleOne(9, 15);

        output_data(array(
                'auction_info' => $auction_info,
                'bid_log_list' => $bid_log_list,
                'recommend_list' => array('auction' => $auction_recommended_list, 'special' => $special_list),
                'relation_info' => $relation_info,
                'article' => $article,
            )
        );
    }


    public function updateRecommendedOp()
    {
        $member_id = (int)$this->getMemberIdIfExists();
        if (empty($member_id)) {
            output_error('请登录 !');
        }
        $update = ['recommended' => 1];
        //是否提示介绍拍乐赚
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $model_member->where(['member_id' => $member_id])->update($update);
        output_data(0);
    }

    /**
     * 输出专题
     */
    private function _output_special($data, $type = 'json', $special_id = 0)
    {
        $model_class = Model('mb_class');
        if ($_GET['type'] == 'html') {
            $html_path = $model_class->getMbSpecialHtmlPath($special_id);
            if (!is_file($html_path)) {
                ob_start();
                Tpl::output('list', $data);
                Tpl::showpage('mb_auction');
                file_put_contents($html_path, ob_get_clean());
            }
            header('Location: ' . $model_class->getMbSpecialHtmlUrl($special_id));
            die;
        } else {
            output_data($data);
        }
    }

    /**
     * 热门搜索列表
     */
    public function search_hot_infoOp()
    {
//        //查询数据显示  table => setting
//        //$model_setting = new auction_settingModel();
//        //$search_info = $model_setting->getRowSetting('auction_rec_search');
//        //dkcache('setting'); - -  清理搜索缓存
//        if (C('auction_rec_search') != '') {
//            $rec_search_list = @unserialize(C('auction_rec_search'));
//        }
        $setting_model = Model('setting');
        $rec_search_info = $setting_model->where(['name' => 'auction_rec_search'])->find();
        if (!empty($rec_search_info) && isset($rec_search_info['value'])) {
            $rec_search_list = @unserialize($rec_search_info['value']);
            $rec_search_list = is_array($rec_search_list) ? $rec_search_list : [];
        } else {
            $rec_search_list = [];
        }
        output_data(['hot_info' => $rec_search_list] ? $rec_search_list : []);
    }

    public function search_auction_listOp()
    {
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');

        $begin = ($page - 1) * $model_auctions->page;
        $limit = $begin . ',' . $model_auctions->page;
        //这一行添加搜索条件字段

        $condition['is_liupai'] = 0;
        $condition['auction_end_true_t'] = array('gt', time());
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $auctions_class_model = Model('auctions_class');
            $class_info = $auctions_class_model->where([
                'auctions_class_name' => array('like', '%' . $keyword . '%')
            ])->find();
            if (!empty($class_info)) {
                $keyword = $class_info['auctions_class_id'];
                if (empty($class_info['auctions_class_fid'])) {
                    $condition['auctions_class_lv1'] = array('like', '%' . $keyword . '%');
                } else {
                    $condition['auctions_class_lv2'] = array('like', '%' . $keyword . '%');
                }
            } else {
                $condition['auction_name|auctions_artist_name'] = array('like', '%' . $keyword . '%');
            }
        }
        intval($_GET['store_id']) > 0 ? $condition['store_id'] = intval($_GET['store_id']) : '';
        //排序方式
        //$order = $this->_auction_list_order(4, $_GET['order']);
        $order = 'auction_id asc';
        if ($_GET['is_kai_pai'] == 1) {          //   正在拍
            $condition['auction_start_time'] = ['elt', time()];
            $condition['auction_end_time'] = ['gt', time()];
        } elseif ($_GET['is_kai_pai'] == 3) {     //   预展
            $condition['auction_preview_start'] = ['elt', time()];
            $condition['auction_start_time'] = ['gt', time()];
        }
        $fields = 'auction_id, goods_common_id, store_id, auction_name, auction_image, auction_start_price, 
        auction_start_time, auction_end_time, auction_preview_start';
        $auctions_list = $model_auctions
            ->getAuctionList(
                $condition,
                $fields,
                '',
                $order,
                $limit
            //$this->page,
            //$count
            );
        //output_data($auctions_list);
        $auctions_ids_arr = [];
        foreach ($auctions_list as $k => $auctions_info) {
            $auctions_ids_arr[] = $auctions_info['auction_id'];
            if (time() >= $auctions_info['auction_start_time']) {
                $is_kaipai = 1; //拍卖中
                $auctions_list[$k]['auction_remain_date']
                    = $auctions_info['auction_end_true_t'] - time() > 0
                    ? $auctions_info['auction_end_true_t'] - time() : 0;
            } else {
                $auctions_list[$k]['auction_remain_date']
                    = $auctions_info['auction_start_time'] - time() > 0
                    ? $auctions_info['auction_start_time'] - time() : 0;

                $is_kaipai = 3;
            }
            $auctions_list[$k]['is_kaipai'] = $is_kaipai;
            $auctions_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'], 360);
            $auctions_list[$k]['auction_start_date'] = date('m月d日 H:i', $auctions_info['auction_start_time']);
            $auctions_list[$k]['auction_end_date'] = date('m月d日 H:i', $auctions_info['auction_end_time']);
            $auctions_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00
                ? number_format($auctions_info['current_price'], 0)
                : number_format($auctions_info['auction_start_price'], 0);
        }
        $page_count = $model_auctions->where($condition)->count();
        $member_id = (int)$this->getMemberIdIfExists();
        if ($member_id > 0) {
            $condition = [
                'fav_id' => ['in', $auctions_ids_arr],
                'member_id' => $member_id
            ];
            $auctions_favorite_list = Model('favorites')
                ->getAuctionsFavoritesList($condition, 'log_id , fav_id');
            $auctions_favorite_list = array_under_reset($auctions_favorite_list, 'fav_id');
            foreach ($auctions_list as &$item) {
                $item['is_favorate'] = $auctions_favorite_list[$item['auction_id']] ? true : false;
            }
            unset($item);
        }
        output_data(array('auction_list' => $auctions_list), api_page($page_count, $page, $model_auctions->page));
    }

    /*
     * 出价记录详细
     * */
    public function get_bid_listOp()
    {
        $auction_id = intval($_GET['auction_id']);
        if ($auction_id <= 0) {
            output_error('拍品ID异常');
        }
        $last_bid_id = isset($_GET['last_bid_id']) ? $_GET['last_bid_id'] : 0;
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $pageSize = (isset($_GET['pSize']) && $_GET['pSize'] > 0 && $_GET['pSize'] < 1000) ?
            intval($_GET['pSize']) :
            (empty(C('page_size')) ? $this->page = 15 : $this->page = C('page_size'));
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        /** @var auctionLogic $logic_auction */
        $logic_auction = Logic('auction');
        $bid_log_list = $logic_auction->getBidList($auction_id, $limit, '', $last_bid_id);
        $bid_log_list_b = $bid_log_list['bid_log_list'];

        foreach ($bid_log_list_b as $key => &$value) {
            if ($value[''] == 0) {
                $bid_log_list_b[$key]['member_avatar'] = !empty($value['member_avatar'])
                    ? $value['member_avatar']
                    : getRobotAvatarByPhone($value['member_name']);
            } else {
                $bid_log_list_b[$key]['member_avatar'] = !empty($value['member_avatar'])
                    ? $value['member_avatar']
                    : getMemberAvatarForID($value['member_id']);
            }
            $bid_log_list_b[$key]['commission_amount'] = ncPriceFormat($value['commission_amount']);
            $bid_log_list_b[$key]['created_at'] = date('Y-m-d H:i:s', $value['created_at']);

        }
        unset($value);
        $return = array(
            'bid_log_list' => $bid_log_list_b,
//            'count_rows' => $bid_log_list['count_rows'],
        );
        output_data($return);
    }

    /**
     * 拍品竞价出价接口
     * @Date: 2018/12/26 0026
     * @author: Mr.Liu
     * @throws \App\Exceptions\ResponseException
     */
    public function member_offerOp()
    {
        $auction_id = intval($_REQUEST['auction_id']);
        $offer_num = intval($_REQUEST['bid_num']);
        $member_id = $this->getMemberIdIfExists();

        /** @var memberModel $model_member */
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($member_id);
        if (empty($member_info)) {
            output_error('不存在的会员');
        }
        if($auction_id< 1){
            output_error('错误参数');
        }
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');

        $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
        //基本校验
        if ($auction_info['auction_type'] == auctionsModel::AUCTION_TYPE_PICKER) {
            $this->checkOfferNum($auction_info, $offer_num, $member_id, false);
        } else {
            $this->checkOfferNum($auction_info, $offer_num, $member_id, true);
        }

        /**
         * 通过了基本校验 锁住 ---------------
         */
        $uSleepCount = 0;
        $lockKey = \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_AUCTION_MEMBER_OFFER.$auction_id;
        do{
            $myRandomValue = \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->lock($lockKey, 10);
            if($myRandomValue === false){
                //0.01 - 0.5 s
                $uSleepMicroseconds = mt_rand(10000, 500000);
                usleep($uSleepMicroseconds);
            }
            $uSleepCount ++;
        }while($myRandomValue === false && $uSleepCount < 25);

        if($myRandomValue === false){
            \App\Exceptions\ApiResponseException::throwFailMsg("当前出价人数过多,请稍后重试!");
        }
        try{
            //更新数据
            $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
            //获得锁了 再次进行一次基本校验
            $checkInfo = $this->checkOfferNum($auction_info, $offer_num, $member_id, false);
            $is_nb = $checkInfo['is_nb'];
            $current_time = time();

            // 出价写入关系表
            /** @var auction_member_relationModel $model_auction_member_relation */
            $model_auction_member_relation = Model('auction_member_relation');
            $update = array(
                'is_offer' => 1,
                'offer_num' => $offer_num
            );
            $condition = array(
                'auction_id' => $auction_id,
                'member_id' => $member_id
            );
            $result = $model_auction_member_relation->setRelationInfo($update, $condition);
            if (!$result) {
                \App\Exceptions\ApiResponseException::throwFailMsg('更新关系表失败');
            }

            $relation_info = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);

            // 写入出价日志
            /** @var bid_logModel $model_bid_log */
            $model_bid_log = Model('bid_log');

            //提醒前一个用户竞价被超越
            /** @var member_msg_tplModel $member_msg_tpl */
            $member_msg_tpl = Model('member_msg_tpl');
            //显示当前最高出价人, 方便片品结束生成订单显示
            /** @var bid_logModel $bidder_member_model */
            $bidder_member_model = Model('bid_log');
            $bidder_member_info = $bidder_member_model->where(['auction_id' => $auction_id])
                ->order('offer_num desc')->find();
            $bidder_member_id = isset($bidder_member_info['member_id'])
                ? $bidder_member_info['member_id'] : 0;
            if (!empty($bidder_member_id) && $bidder_member_id != $member_id) {
                $sendParam = [
                    'member_id' => $bidder_member_id,
                    'mobile' => $this->member_info['member_mobile'],
                    'current_price' => $offer_num
                ];
                $member_msg_tpl->sendBeyondPriceMessage($sendParam, $auction_info);
            }
            //拍卖延时时间
            /** @var auction_configModel $auction_config_model */
            $auction_config_model = Model('auction_config');
            $auction_config_info = $auction_config_model->where(['id' => 1])->find();
            $add_time = isset($auction_config_info['add_time']) ? $auction_config_info['add_time'] * 60 : 10 * 60;
            // 更新拍品最新价格
            $update = array(
                'current_price' => $offer_num,
                'bid_number' => array('exp', 'bid_number+1'),
                'current_price_time' => $current_time,
            );
            if ($auction_info['auction_end_time'] < ($current_time + $add_time)) {
                $update['auction_end_true_t'] = $update['auction_end_time'] = $current_time + $add_time;
            }
            $result = $model_auctions->editAuctions($update, array('auction_id' => $auction_id));
            if (!$result) {
                \App\Exceptions\ApiResponseException::throwFailMsg('更新拍品价格失败');
            }

            //TODO  待确定 之前的竞拍出价是否添加返佣
            /** @var member_commissionModel $member_commission */
            $member_commission = Model('member_commission');
            $commission_num = $offer_num - $auction_info['current_price'];
            //个人竞价返佣金额
            $person_num = 0.00;
            //诺币不进行返佣处理
            if ((!$is_nb || empty($is_nb)) && $auction_info['auction_type'] != auctionsModel::AUCTION_TYPE_PICKER) {
            $returnDta = $member_commission->addWaitForCommission($member_id, $commission_num, $auction_info);
            $person_num = isset($returnDta['commission_num']) ? $result['commission_num'] : 0.00;
            } else {
                //生成一条保证金金额为0的保证金已支付订单--保证金退还的时候不写入余额操作记录
                /** @var auction_buyLogic  $logic_auction_buy*/
                $logic_auction_buy = Logic('auction_buy');
                $result = $logic_auction_buy->new_create_picker_margin_order($auction_info, $member_info);
                if (!$result) {
                    output_error('捡漏拍品保证金订单生成失败 !');
                }
            }
            $bidType = $auction_info['auction_type'] == 2
                ? \App\Models\ShopncBidLog::BID_TYPE_NEWBIE
                : \App\Models\ShopncBidLog::BID_TYPE_AUCTION;
            if(substr($member_info['member_avatar'], 0, 4) == 'http') {
                $avatar = $member_info['member_avatar'];
            } else {
                $avatar = getMemberAvatarForID($member_info['member_id']);
            }
            $param = array(
                'member_id' => $member_id,
                'member_avatar' => $avatar,
                'auction_id' => $auction_id,
                'created_at' => $current_time,
                'offer_num' => $offer_num,
                'member_name' => $member_info['member_name'],
                'is_anonymous' => $relation_info['is_anonymous'],
                'commission_amount' => $person_num,
                'bid_type' => $bidType
            );
            $result = $model_bid_log->addBid($param);
            if (!$result) {
                \App\Exceptions\ApiResponseException::throwFailMsg('写入出价日志失败');
            }
        }catch (\Exception $e){
            //解锁
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
            if (!($e instanceof \App\Exceptions\ApiResponseException)){
                yLog()->error("auction_member_offer_error_exception", [\Yinuo\Lib\Helpers\LogHelper::getEInfo($e), $auction_id, $offer_num, $member_id]);
            }
            throw $e;
        }
        //解锁
        \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
        //代理出价不要了
        /** @var auction_agentLogic $auction_agent */
        //$auction_agent =Logic('auction_agent');
        //$auction_agent->updateCurrentPrice($auction_id);
        output_data($returnDta);
    }

    /**
     * 检查出价金额,并且返回is_nb
     * @param $auction_info
     * @param $offer_num
     * @param $member_id
     * @param bool $is_check_margin_amount 是否检查保证金
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    private function checkOfferNum($auction_info, $offer_num, $member_id, $is_check_margin_amount = true){
        if (empty($auction_info)) {
            \App\Exceptions\ApiResponseException::throwFailMsg('拍品不存在');
        }
        if ($auction_info['auction_start_time'] > time()) {
            \App\Exceptions\ApiResponseException::throwFailMsg('拍卖还未开始!');
        }
        $special = Model('auction_special');
        $nb_condition = ['special_id' => $auction_info['special_id']];
        $nb_list = $special->where($nb_condition)->find();
        //判断是否诺币专场
        $is_nb = isset($nb_list['is_pd_pay']) && $nb_list['is_pd_pay'] == 1 ? 1 : 0;
        if (!$is_nb || empty($is_nb)) {
            // 验证出价
            if ($offer_num < ($auction_info['auction_increase_range'] + $auction_info['current_price'])) {
                //LM 17.5.19
                \App\Exceptions\ApiResponseException::throwFailMsg('出价过低，当前最低出价:'. ($auction_info['auction_increase_range'] + $auction_info['current_price']) . ';刷新价格后重试.');
            }
            if($is_check_margin_amount){
            //已交保证金金额
                $getMarginSum= Model('margin_orders')->getMarginSum(
                    array('auction_id' => $auction_info['auction_id'], 'buyer_id' => $member_id, 'order_state' => 1),'margin_amount'
                );
                if (empty($getMarginSum)) {
                    \App\Exceptions\ApiResponseException::throwFailMsg('未支付保证金');
                }
                $check_num=$getMarginSum*2;
                if ($check_num < $offer_num) {
                    \App\Exceptions\ApiResponseException::throwFailMsg('当保证金不足竞价50%，请先缴纳保证金。');
                }
            }
        }
        if ($offer_num <= $auction_info['current_price']) {
            \App\Exceptions\ApiResponseException::throwFailMsg('拍卖当前价格为' . $auction_info['current_price']);
        }
        $current_time = time();
        // 验证拍卖是否已经结束
        if (empty($auction_info['auction_end_true_t'])) {
            if ($current_time > $auction_info['auction_end_time']) {
                \App\Exceptions\ApiResponseException::throwFailMsg('拍卖已结束');
            }
        } else {
            if ($current_time > $auction_info['auction_end_true_t']) {
                \App\Exceptions\ApiResponseException::throwFailMsg('拍卖已结束');
            }
        }
        // 验证出价
        if ($offer_num < $auction_info['auction_increase_range'] + $auction_info['current_price']) {
            //LM 17.5.19
            \App\Exceptions\ApiResponseException::throwFailMsg('拍卖当前价格' . $auction_info['current_price'] . ', 加附价为'
                . $auction_info['auction_increase_range']);
        }
        return ["is_nb" => $is_nb];
    }

    // 拍品详情
    protected function auction_body($auction_info)
    {
        if ($auction_info['auction_mobile_body'] == '') {
            $auction_info['auction_mobile_body'] = $auction_info['auction_body'];
        }
        return $auction_info['auction_mobile_body'];
    }

    /*
     * 设置代理价，获取当前价格信息
     * */
    public function get_dailiOp()
    {
        $auction_id = $_GET['auction_id'];
        $auction_info = Model('auctions')->getAuctionsInfoByID($auction_id);
        $member_id = $this->getMemberIdIfExists();
        // 验证拍卖是否已经结束
        if (empty($auction_info['auction_end_true_t'])) {
            if (time() > $auction_info['auction_end_time']) {
                output_error('拍卖已结束');
            }
        } else {
            if (time() > $auction_info['auction_end_true_t']) {
                output_error('拍卖已结束');
            }
        }
        // 关系表
        $model_relation = Model('auction_member_relation');
        $relation_info = $model_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);

        $data = array(
            'lowest_price' => $relation_info['agent_num'] > $auction_info['current_price'] + $auction_info['auction_increase_range'] * 2
                ? $relation_info['agent_num']
                : $auction_info['current_price'] + $auction_info['auction_increase_range'] * 2,
            'is_agent' => $relation_info['is_agent'],
            'agent_num' => $relation_info['agent_num'],
            'auction_id' => $auction_id,
        );
        output_data($data);
    }

    /*
     * 保存代理价
     * */
    public function save_dailiOp()
    {
        $member_id = $this->getMemberIdIfExists();
        $result = Logic('auction_agent')->setAgentPrice($_POST, $member_id);
        if ($result['state'] == 'succ') {
            output_data(0);
        } else {
            output_error($result['msg']);
        }
    }

    /*
     * 设置拍品提醒
     * */
    public function set_remindOp()
    {
        $auction_id = intval($_POST['auction_id']);
        $member_id = $this->getMemberIdIfExists();

        if (empty($auction_id) || empty($member_id)) {
            output_error('缺少参数');
        }
        $is_remind = 1;     //设置提醒is_remind  0 未设置
        $is_remind_app = 1;     //设置提醒is_remind_app  0 未设置

        /** @var memberModel $model_member */
        /** @var auctionsModel $model_auctions */
        $model_member = Model('member');
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_id));
        $member_info = $model_member->getMemberInfoByID($member_id);
        //建立订阅关系
        /** @var auction_member_relationModel $model_auction_member_relation */
        $model_auction_member_relation = Model('auction_member_relation');
        //查询是否已经存在
        $relation = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
        $insert['auction_id'] = $auction_id;
        $insert['member_id'] = $member_id;
        $insert['is_remind'] = $is_remind;
        $insert['member_mobile'] = $member_info['member_mobile'];
        $insert['is_remind_app'] = $is_remind_app;
        $insert['member_name'] = $member_info['member_name'];
        $insert['remind_time'] = $auction_info['auction_end_time'] - AUCTION_OVER_REMIND;
        $insert['auction_name'] = $auction_info['auction_name'];
        if (empty($relation)) {
            $model_auction_member_relation->addRelation($insert);
        } else {
            $model_auction_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }
        // 设置提醒人数 +1
        $model_auctions->editAuctions(
            array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('auction_id' => $auction_id)
        );
        /** @var member_msg_tplModel $member_msg_tpl */
        $member_msg_tpl = Model('member_msg_tpl');
        $sendParam = [
            'code'     =>'aucution_margin_pay',
            'member_id'=>$member_id,
            'mobile'   =>$member_info['member_mobile']
        ];
        $member_msg_tpl->sendSetRemindMessage($sendParam, $auction_info);
        output_data(0);
    }


    /*
    * 设置专场提醒
    * */
    public function set_special_remindOp()
    {
        $special_id = intval($_POST['special_id']);
        $member_id = $this->getMemberIdIfExists();

        if (empty($special_id) || empty($member_id)) {
            output_error('缺少参数');
        }
        $model_member = Model('member');
        /** @var memberModel $model_member */
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        $condition = ['special_id' => $special_id];
        $special_info = $model_special->getSpecialInfo($condition, $field = "*");
        if (empty($special_info) && $special_info['is_open'] == 0) {
            output_error('该专场已关闭');
        }
        $member_info = $model_member->getMemberInfoByID($member_id);
        //建立订阅关系
        /** @var special_noticeModel $model_special_notice */
        $model_special_notice = Model('special_notice');
        //查询是否已经存在
        /** @var auction_member_relationModel $model_auction_member_relation */
        $where = ['special_id' => $special_id, 'member_id' => $member_id];
        $relation = $model_special_notice->where($where)->find();
        if (empty($relation)) {
            $insert['special_id'] = $special_id;
            $insert['special_name'] = $special_info['special_name'];
            $insert['member_id'] = $member_id;
            $insert['sn_remind_time'] = $special_info['special_start_time'] - (60 * 10);
            $insert['sn_email'] = $member_info['member_email'];
            $insert['sn_mobile'] = $member_info['member_mobile'];
            $insert['sn_type'] = 1;
            $insert['is_send'] = 0;
            $model_special_notice->addSpecialNotice($insert);
            /** @var member_msg_tplModel $member_msg_tpl */
            $member_msg_tpl = Model('member_msg_tpl');
            $sendParam = [
                'code'     =>'follow_special',
                'member_id'=>$member_id,
                'mobile'   =>$member_info['member_mobile']
            ];
            $member_msg_tpl->sendSetSpecialRemindMessage($sendParam, $special_info);
            output_data(0);
        } else {
            if ($relation['is_send'] == 1) {
                $update['is_send'] = 0;
                $model_special_notice->editSpecialNotice($update, $where);
                output_data(0);
            }
            output_error('您已设置提醒!');
        }
        // 设置提醒人数 +1
        //$model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('auction_id' => $auction_id));
    }

    /*
    * 取消专场提醒
    * */
    public function cancel_special_remindOp()
    {
        $special_id = intval($_POST['special_id']);
        $member_id = $this->getMemberIdIfExists();
        if (empty($special_id) || empty($member_id)) {
            output_error('缺少参数');
        }
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        $condition = ['special_id' => $special_id];
        $special_info = $model_special->getSpecialInfo($condition, $field = "*");
        if (empty($special_info) && $special_info['is_open'] == 0) {
            output_error('该专场已关闭');
        }
        //建立订阅关系
        /** @var special_noticeModel $model_special_notice */
        $model_special_notice = Model('special_notice');
        //查询是否已经存在
        /** @var auction_member_relationModel $model_auction_member_relation */
        $where = ['special_id' => $special_id, 'member_id' => $member_id];
        $relation = $model_special_notice->where($where)->find();
        if (empty($relation)) {
            output_error('你还没有设置提醒 !');
        } else {
            $model_special_notice->where($where)->delete();
        }
        /** @var member_msg_tplModel $member_msg_tpl */
        $member_msg_tpl = Model('member_msg_tpl');
        $sendParam = [
            'code'     =>'cannel_follow_speciak',
            'member_id'=>$member_id,
            'mobile'   =>$this->member_info['member_mobile']
        ];
        $member_msg_tpl->sendCancelSpecialRemindMessage($sendParam, $special_info);
        output_data(0);
        // 设置提醒人数 +1
        //$model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('auction_id' => $auction_id));
    }


    /*
     * 拍品取消提醒
     * */
    public function cancel_remindOp()
    {
        $auction_id = intval($_POST['auction_id']);
        $member_id = $this->getMemberIdIfExists();
        if (empty($auction_id) || empty($member_id)) {
            output_error('缺少参数');
        }
        $is_remind = 0;         //设置提醒is_remind  0 未设置
        $is_remind_app = 0;     //设置提醒is_remind_app  0 未设置

        /** @var memberModel $model_member */
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        //建立订阅关系
        /** @var auction_member_relationModel $model_auction_member_relation */
        $model_auction_member_relation = Model('auction_member_relation');
        //查询是否已经存在
        $relation = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
        $insert['is_remind'] = $is_remind;
        $insert['is_remind_app'] = $is_remind_app;
        if (empty($relation)) {
            output_error('您还没有设置该拍品的提醒 !');
        } else {
            $model_auction_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }
        // 设置提醒人数 +1
        $result_2 = $model_auctions->editAuctions(
            array('set_reminders_num' => array('exp', 'set_reminders_num-1')),
            array('auction_id' => $auction_id)
        );
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_id));
        /** @var member_msg_tplModel $member_msg_tpl */
        $member_msg_tpl = Model('member_msg_tpl');
        $sendParam = [
            'member_id'=>$member_id,
            'mobile'   =>$this->member_info['member_mobile']
        ];
        $member_msg_tpl->sendCancelRemindMessage($sendParam, $auction_info);
        output_data(0);
    }

    public function check_mobileOp()
    {
        $member_id = $this->getMemberIdIfExists();
        $member_info = Model('member')->getMemberInfoByID($member_id);
//        print_R($member_info);exit;
        if ($member_info['member_mobile'] != '') {
            output_data('1');
        } else {
            output_data('0');
        }
    }

    /**
     * 获去拍品图片
     * @return array
     */
    public function get_auction_imgOp()
    {
        $auction_id = intval($_GET['auction_id']);

        $auction_info = Model('auctions')->getAuctionsInfo(['auction_id' => $auction_id], 'goods_id,auction_image,store_id');
        $model_goods = Model('goods');
        $goods_info = $model_goods->getGoodsInfo(['goods_id' => $auction_info['goods_id']], 'goods_commonid,color_id,store_id');
        $image_more = $model_goods->getGoodsImageByKey($goods_info['goods_commonid'] . '|' . $goods_info['color_id']);


        $image_list = array();
        foreach ($image_more as $val) {
            $image_list[] = array('_small' => cthumb($val['goods_image'], 60, $goods_info['store_id']), '_mid' => cthumb($val['goods_image'], 360, $goods_info['store_id']), '_big' => cthumb($val['goods_image'], 1280, $goods_info['store_id']));
        }
        if (empty($image_more)) {
            $image_list[] = array('_small' => cthumb($auction_info['auction_image'], 60), '_mid' => cthumb($auction_info['auction_image'], 360), '_big' => cthumb($auction_info['auction_image'], 1280));
        }
        output_data(['img_list' => $image_list]);
    }

    /*
     * 拍品详情竞价日志列表
     */
    public function bidLogFetchOp()
    {
        if (intval($_GET['auction_id']) <= 0) {
            output_error('非法参数');
        }
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $pageSize = empty(C('page_size')) ? $this->page = 15 : $this->page = C('page_size');
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        $auction_id = intval($_GET['auction_id']);
        /** @var auctionLogic $logic_auction */
        $logic_auction = Logic('auction');
        $log_list = $logic_auction->bidListFetch($auction_id,'*',  $limit);
        output_data($log_list['bid_log_list'], api_page($log_list['count_rows'], $page, $pageSize));
    }



    /*
     * 获取返佣列表信息 最近的
     * 拍卖详情 已经不使用这个接口
    */
    public function offerCommissionFetchOp()
    {
        if (intval($_GET['auction_id']) <= 0) {
            output_error('非法参数');
        }
        $page = $_GET['page'];
        $limit_sec = $_GET['limit_sec'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        if (!isset($limit_sec) || empty($limit_sec) || !is_numeric($limit_sec)) {
            $limit_sec = 3;
        }
        $end_time = time()-$limit_sec;
        $pageSize = empty(C('page_size')) ? $this->page = 15 : $this->page = C('page_size');
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        $auction_id = intval($_GET['auction_id']);
        /** @var auctionLogic $logic_auction */
        $logic_auction = Logic('auction');
        $condition = [
            'auction_id' => $auction_id
        ];
        $condition['created_at'] = ['gt', $end_time];
        $log_list = $logic_auction->offerCommissionFetch($condition,'*',  $limit);
        output_data($log_list['bid_log_list'], api_page($log_list['count_rows'], $page, $pageSize));
    }


    /*
     * 拍品分类列表查询
     */
    public function categoryFetchOp()
    {
        /** @var auctions_classModel $category_model */
        $category_model = Model('auctions_class');
        $fields = '`auctions_class_id` as gc_id, `auctions_class_name` as gc_name, `auctions_class_fid` as pid';
        $category_list = $category_model->field($fields)->limit(1000)->select();
        $category_list = self::recursionTree($category_list, 'gc_id', 'pid');
        array_unshift($category_list, ['gc_id' => '0', 'gc_name' => '全部', 'pid' => '0', 'child' => []]);
        output_data($category_list);
    }

    /**
     * 分支树显示无限分类
     * @param $arr  需要处理的数组
     * @param $key  id名称
     * @param $pkey 父类id名称
     * @param $pid  父id
     * @return $list 返回的数组
     */
    public static function recursionTree($arr = [], $key = 'id', $pkey = 'pid', $pid = 0)
    {
        $list = array();
        foreach($arr as $val){
            if($val[$pkey] == $pid){
                $tmp = self::recursionTree($arr,$key,$pkey,$val[$key]);
                if($tmp){
                    $val['child'] = $tmp;
                } else {
                    $val['child'] = [];
                }
                $list[] = $val;
            }
        }
        return $list;
    }

    public function timeFetchOp()
    {
        output_data(time());
    }
}