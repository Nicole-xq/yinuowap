<?php
/**
 * 我的订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_auction_orderControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 订单列表--我的拍卖
     */
    public function order_listOp() {
        /** @var auction_orderLogic $logic_auction_order */
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $type = $_GET['state_type'];
        $member_id = $this->member_info['member_id'];
        $logic_auction_order = Logic('auction_order');
        $pageSize = empty(C('page_size')) ? $this->page = 15 : $this->page = C('page_size');
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        if ($member_id == 0) {
            output_error('会员不存在');
        }
        $margin_state = array();
        if($_GET['p_type'] == 'auction') {
            $margin_state = array(0,1,2,3,5);
        }
        // 获取订单列表
        switch ($type) {
            case 'list_all'://所有保证金订单
                $result = $logic_auction_order->auctionListFetch($member_id, '', '','', [], $limit);
                break;
            case 'list_ing'://参拍
                $result = $logic_auction_order->auctionListFetch($member_id, '', '','',array(1,3), $limit);
                break;
            case 'list_have'://已拍下
                $result = $logic_auction_order->auctionListFetch($member_id, 0, 1,null,$margin_state, $limit, 'list_have');
                break;
            case 'list_die'://未拍中
                $result = $logic_auction_order->auctionListFetch($member_id, 1, ['neq',1],null,$margin_state, $limit);
                break;
            case 'list_default'://违约订单default_state
                $result = $logic_auction_order->auctionListFetch($member_id, 1, ['neq',1],null,$margin_state, $limit);
                break;
        }
        $page_count = $result['page_count'];
        if (!isset($result['margin_order_list'])) {
            $result['margin_order_list'] = [];
        }
        output_data($result['margin_order_list'], api_page($page_count, $page,$pageSize));
    }


    public function marginListFetchOp()
    {
        /** @var auction_orderLogic $logic_auction_order */
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $refund_state = '';
        if (!empty($_REQUEST['refund_state'])) {
            $refund_state = $_REQUEST['refund_state'];
        }
        $member_id = $this->member_info['member_id'];
        $logic_auction_order = Logic('auction_order');
        $pageSize = empty(C('page_size')) ? $this->page = 15 : $this->page = C('page_size');
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        if ($member_id == 0) {
            output_error('会员不存在');
        }
        $result = $logic_auction_order->marginListFetch($member_id, $refund_state, '','', [], $limit);
        output_data($result['margin_order_list'], api_page($result['page_count'], $page,$pageSize));
    }


    public function marginDetailOp()
    {
        /** @var auction_orderLogic $logic_auction_order */
        $auction_id = $_GET['auction_id'];
        if (!intval($auction_id)) {
            output_error('参数有误');
        }
        $member_id = $this->member_info['member_id'];
        if ($member_id == 0) {
            output_error('会员不存在');
        }
        /** @var auctionsModel $model_auctions */
        /** @var margin_ordersModel $model_margin_orders */
        /** @var auction_member_relationModel $model_auction_member_relation */
        /** @var auction_specialModel $special_model */
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $special_model = Model('auction_special');
        $condition['buyer_id'] = $member_id;
        $condition['auction_id'] = intval($auction_id);
        // 查询保证金列表
        $fields = 'order_sn, auction_id, auction_name, margin_amount, order_state, refund_state,
         default_state, rufund_money, margin_amount, payment_code, created_at, api_pay_amount as wx_amount, pd_amount';
        $margin_order_list = $model_margin_orders->field($fields)->where($condition)->select();
        if (empty($margin_order_list)) {
            output_error('保证金订单不存在');
        }
        $return['order_sn'] = $margin_order_list[0]['order_sn'];
        $return['auction_name'] = $margin_order_list[0]['auction_name'];
        //TODO  保证金总额  修改为成功缴纳金额
        //$return['margin_amount'] = $model_margin_orders->where($condition)->sum('margin_amount');
        $return['margin_amount'] = $model_margin_orders->marginCountFetch($member_id, $auction_id);
        // 追加其余信息
        $fields = 'auction_id, auction_image, auction_preview_start, auction_start_time, auction_end_time, state,
         current_price, special_id';
        $auction_info = $model_auctions->getAuctionsInfoByID($auction_id, $fields);
        if (empty($auction_info)) {
            output_error('该拍品已不存在 !');
        }
        $special_info = $special_model->getSpecialInfo(['special_id' => $auction_info['special_id']], 'special_name');
        $return['special_name'] = isset($special_info['special_name'])
            ? $special_info['special_name']
            : '专场名称';
        $return['auction_image'] = cthumb($auction_info['auction_image'], 360);
        //获取拍品状态   默认显示     //total_status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款  5 : 已取消
        $return['total_status'] = 1;
        if (empty($return['margin_amount'])) {
            $return['total_status'] = 5;
        } else {
            if ($auction_info['state'] == 1) {
                //判断该用户是否拍中该拍品显示亏款和抵扣状态
                $return['total_status'] = 2;
                //当前用户是否拍中该拍品
                $order = Model('orders');
                $order_fetch = $order->where(['auction_id' => $auction_info['auction_id']])->find();
                if ($order_fetch && $order_fetch['buyer_id'] == $member_id) {
                    //该用户拍中拍品 显示已抵扣
                    $return['total_status'] = 3;
                    if ($order_fetch['order_state'] == 0) {
                        $return['total_status'] = 4;
                    } elseif ($order_fetch['order_state'] == 10) {
                        $return['total_status'] = 1;
                    }
                } else {
                    $return['total_status'] = 2;
                }
            }
        }
        foreach ($margin_order_list as &$margin_order_info) {
            //获取拍品状态   默认显示     //total_status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款  5 : 已取消
            //status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款  5 : 待支付 6 : 已取消  7 : 抵扣抵扣部分 8: 违约部分退款
            //$margin_order_info['status'] = 1;
            if ($margin_order_info['order_state'] == 1) {
                if ($return['total_status'] == 1) {
                    $margin_order_info['status'] = 1;
                } elseif ($return['total_status'] == 2) {
                    $margin_order_info['status'] = 2;
                } elseif ($return['total_status'] == 3) {
                    //抵扣
                    if ($margin_order_info['rufund_money'] == 0 && $margin_order_info['refund_state'] != 1) {
                        $margin_order_info['status'] = 3;
                        $margin_order_info['refund_money'] = 0.00;
                    } else {
                        //根据退换金额判断是退款 或者部分抵扣
                        if ($margin_order_info['rufund_money'] == 0 && $margin_order_info['refund_state'] == 1) {
                            $margin_order_info['status'] = 2;
                            $margin_order_info['refund_money'] = $margin_order_info['margin_amount'];
                        } else {
                            //部分抵扣  计算抵扣金额
                            $margin_order_info['status'] = 7;
                            $margin_order_info['refund_money'] = $margin_order_info['rufund_money'];
                        }
                    }
                } elseif ($return['total_status'] == 4) {
                    //根据退换金额判断是违约扣款 或者违约部分扣款
                    if ($margin_order_info['rufund_money'] == 0  && $margin_order_info['refund_state'] != 1) {
                        $margin_order_info['status'] = 4;
                        $margin_order_info['refund_money'] = $margin_order_info['margin_amount'];
                    } else {
                        //部分抵扣  计算抵扣金额
                        if ($margin_order_info['rufund_money'] == 0 && $margin_order_info['refund_state'] == 1) {
                            $margin_order_info['status'] = 2;
                            $margin_order_info['refund_money'] = $margin_order_info['margin_amount'];
                        } else {
                            //部分抵扣  计算抵扣金额
                            $margin_order_info['status'] = 8;
                            $margin_order_info['refund_money'] = $margin_order_info['rufund_money'];
                        }
                    }
                }
            } elseif ($margin_order_info['order_state'] == 0) {
                $margin_order_info['status'] = 5;
            } elseif ($margin_order_info['order_state'] == 4) {
                $margin_order_info['status'] = 6;
            } elseif ($margin_order_info['order_state'] == 3) {
                $margin_order_info['status'] = 5;
            }

            if ($margin_order_info['payment_code'] == 'wxpay_jsapi' || $margin_order_info['payment_code'] == 'wxpay') {
                $margin_order_info['payment_code'] = 'weiXin';
            } else {
                $margin_order_info['payment_code'] = 'balance';
            }
            $margin_order_info['created_at'] = date('Y.m.d H:i:s', $margin_order_info['created_at']);
            unset(
                $margin_order_info['auction_name'],
                $margin_order_info['rufund_money'],
                $margin_order_info['default_state'],
                $margin_order_info['refund_state'],
                $margin_order_info['order_state']
            );
        }
        $return['margin_info'] = $margin_order_list;
        output_data($return);
    }
    /*
    * 取消保证金订单
    * */
    public function order_cancelOp()
    {
        $margin_id = $_POST['order_id'];
        $model_margin_orders = Model('margin_orders');
        $logic_auction_order = Logic('auction_order');
        //获取订单详细
        $condition = array();
        $condition['margin_id'] = $margin_id;
        $order_info = $model_margin_orders->getOrderInfo($condition);
        // 取消订单
        $result =  $logic_auction_order->changeOrderStateCancel($order_info,'admin', $this->admin_info['name'],'',true);
//        $order_id = $_POST['order_id'];
//
//        $model_auction_order = Model('auction_order');
//        $order_info = $model_auction_order->getInfo(array('auction_order_id' => $order_id));
//
//        $logic_auction_order = Logic('auction_order');
//        // 会员中心修改尾款订单状态
//        $result = $logic_auction_order->MemberChangeOrderState($order_info, $_POST, 0);
//        if (TIMESTAMP - ORDER_CANCEL_TIMES < $order_info['api_pay_time'] && $order_info['payment_code'] != "underline") {
//            $_hour = ceil(($order_info['api_pay_time']+ORDER_CANCEL_TIMES-TIMESTAMP)/3600);
//            return output_error('该订单曾尝试使用第三方支付平台支付，须在'.$_hour.'小时以后才可申请退款');
//        }
        if (!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 订单确认收货
     */
    public function order_receiveOp()
    {
        $order_id = $_POST['order_id'];

        /** @var orderModel $model_order */
        $model_order = Model('order');
        $order_info = $model_order->getOrderInfo(array('order_id' => $order_id));
        /** @var auction_orderLogic $logic_auction_order */
        $logic_auction_order = Logic('auction_order');
        // 会员中心修改尾款订单状态
        $result = $logic_auction_order->MemberChangeOrderState($order_info, $_POST, 1);
        if (!$result['state']) {
            output_error($result['msg']);
        }

        // 添加会员积分，经验，会员返佣
        $result = $logic_auction_order->changeOrderStateReceive(
            $order_info, 'buyer', $_SESSION['member_name'], '签收了货物'
        );
        $condition = ['order_id' => $order_id];
        $order_common_info = $model_order->getOrderCommonInfo($condition);
        if (empty($order_common_info['ship_time'])) {
            $order_info['ship_time'] = 0;
        }
        if (!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 物流跟踪
     */
    public function search_deliverOp(){
        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['order_id']);
        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            output_error($result['msg']);
        }
        $express = rkcache('express',true);
        $e_code = $express[$result['data']['order_info']['express_id']]['e_code'];

        $deliver_info = $this->_get_express($e_code, $result['data']['order_info']['invoice_no']);

        output_data(array('express_name' => $result['data']['order_info']['express_info']['e_name'], 'shipping_code' => $result['data']['order_info']['invoice_no'], 'deliver_info' => $deliver_info));
    }

    /**
     * 取得当前的物流最新信息
     */
    public function get_current_deliverOp(){
        $auction_order_id   = intval($_GET['order_id']);
        $margin_id = intval($_GET['margin_id']);
        if ($auction_order_id <= 0 || $margin_id <= 0) {
            output_error('订单不存在');
        }

        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        $order_info = $result['data']['order_info'];

        if (empty($order_info) || !in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS))) {
            output_error('订单不存在');
        }
    
        $express = rkcache('express',true);
        $e_code = $express[$order_info['express_id']]['e_code'];
        $e_name = $express[$order_info['express_id']]['e_name'];

        $content = Model('express')->get_express($e_code, $order_info['invoice_no']);
        if (empty($content)) {
            output_error('物流信息查询失败');
        } else {
            foreach ($content as $k=>$v) {
                if ($v['time'] == '') continue;
                output_data(array('deliver_info'=>$content[0]));
            }
            output_error('物流信息查询失败');
        }
    }

    /**
     * 从第三方取快递信息
     *
     */
    public function _get_express($e_code, $shipping_code){

        $content = Model('express')->get_express($e_code, $shipping_code);
        if (empty($content)) {
            output_error('物流信息查询失败');
        }
        $output = array();
        foreach ($content as $k=>$v) {
            if ($v['time'] == '') continue;
            $output[]= $v['time'].'&nbsp;&nbsp;'.$v['context'];
        }

        return $output;
    }

    /*
     * 订单详情
     * */
    public function order_infoOp()
    {
        $auction_order_id = intval($_GET['auction_order_id']);
        if (empty($auction_order_id)) {
            output_error('参数有误');
        }
        // 获取订单详情
        /** @var auction_orderLogic $login_auction_order */
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->memberAuctionOrderInfo($auction_order_id);
        output_data($result);
    }

    /*
    * 根据订单ID 获取最新的pay_sn
    */
    public function orderSnFetchOp()
    {
        $order_id = intval($_GET['order_id']);
        if (empty($order_id)) {
            output_error('参数有误');
        }
        // 获取订单详情
        $order_model = Model('order');
        $where = [
            'order_id' => $order_id
        ];
        $order_info = $order_model->table('orders')->where($where)->find();
        if (empty($order_info)) {
            output_error('该订单不存在 !');
        }
        output_data(['pay_sn' => $order_info['order_sn']]);
    }
}
