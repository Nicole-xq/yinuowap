<?php
/**
 * cms首页
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class indexControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     */
    public function indexOp() {
        $model_mb_special = Model('mb_special');
        $model_auctions = Model('auctions');
        $model_store = Model('store');
        $model_guess = Model('p_guess');
        $model_mb_special->app_index = true;
        $data = $model_mb_special->getMbSpecialIndex();
        $b= 0;
        $a= array();
        foreach ($data as $key => $value) {
            if ($value['cla']) {
                foreach ($value['cla']['item'] as $k => $v) {
                    $e_images = explode(',', $v['e_images']);
                    $e_data = explode(',', $v['e_data']);
                    $e_type = explode(',', $v['e_type']);
                    $v['elem'] = array();
                    foreach($e_images as $ke => $va){
                        $v['elem'][$ke]['e_images'] = $va;
                        $v['elem'][$ke]['e_data'] = $e_data[$ke];
                        $v['elem'][$ke]['e_type'] = $e_type[$ke];
                    }
                    unset($v['e_links']);
                    unset($v['e_images']);
                    unset($v['data']);
                    unset($v['image']);
                    $value['cla']['item'][$k] = $v;
                }
                $list[$key]['cla'] = $value['cla'];
            }
            if ($value['qucai']) {
                $list[$key]['qucai'] = $value['qucai'];
                foreach($list[$key]['qucai']['item'] as $k=>$v){
                    $qucai_info = $model_guess->getOpenGuessInfo(array('gs_id'=>$v['gs_id']));
                    $list[$key]['qucai']['item'][$k]['joined_times'] = $qucai_info['joined_times'];
                }
            }
            if ($value['adv_list']) {
                $list[$key]['adv_list'] = $value['adv_list'];
            }
            $a = array();
            if ($value['store_top']) {
                foreach ($value['store_top']['item'] as $k => $v) {
                    $store_info = $model_store->getStoreInfoByID($v['store_id']);
                    $value['store_top']['item'][$k]['if_fav'] = false;
                    if($member_id = $this->getMemberIdIfExists()) {
                        $c = (int) Model('favorites')->getStoreFavoritesCountByMemberId($member_id, $v['store_id']);
                        $value['store_top']['item'][$k]['if_fav'] = $c > 0;   
                    }
                    $value['store_top']['item'][$k]['store_logo'] = getStoreLogo( $store_info['store_label'],'store_logo'); 
                    $value['store_top']['item'][$k]['store_collect'] = $store_info['store_collect'];    
                }
                $list[$key]['store_top'] = $value['store_top'];
            }
            if ($value['goods']) {
                $list[$key]['goods'] = $value['goods'];
            }
            if ($value['home1']) {
                $list[$key]['home1'] = $value['home1'];
            }
            if ($value['home2']) {
                $list[$key]['home2'] = $value['home2'];
            }
            if ($value['home3']) {
                $list[$key]['home3'] = $value['home3'];
            }
            if ($value['home4']) {
                $list[$key]['home4'] = $value['home4'];
            }
            if ($value['sem']) {
                $list[$key]['sem'] = $value['sem'];
            }
            if ($value['auctions']) {
                 foreach($value['auctions']['item'] as $k => $v) {
                    $auction_info = $model_auctions->getAuctionsInfoByID($v['auction_id']);
                    if($auction_info['auction_start_time'] <= TIMESTAMP){
                        if($auction_info['auction_start_time'] <= time() && ($auction_info['auction_end_time'] >= time() || $auction_info['auction_end_true_t'] >= time())){
                            $v['is_kaipai'] = 1;
                        }elseif($auction_info['auction_end_true_t'] < time()){
                            $v['is_kaipai'] = 2;
                        }
                    }else{
                        $v['is_kaipai'] = 0;
                    }
                    $v['auction_remin_date'] = $auction_info['auction_end_time'] - TIMESTAMP >0 ?$auction_info['auction_end_time'] - TIMESTAMP : 0;
                    $v['current_price'] = $auction_info['current_price'] != 0.00 ?$auction_info['current_price']:$auction_info['auction_start_price'];
                    $v['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                    $v['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);
                    $v['auction_start_time'] = $auction_info['auction_start_time'];
                    $v['auction_end_time'] = $auction_info['auction_end_time'];
                    $v['bid_number'] = $auction_info['bid_number'];
                    $v['auction_click'] = $auction_info['auction_click'];
                    $v['is_liupai'] = $auction_info['is_liupai'];
                    
                    $value['auctions']['item'][$k] = $v;
                }
                $list[$key]['auctions'] = $value['auctions'];
            }

            if($value['special']){
                if($b == 0){
                    $k = $key;
                }
                unset($key);
                if(!in_array($value['special'],$a)){
                    $a[] = $value['special'];
                    $list[$k]['special']['special'][] = $value['special'];
                }
                else{
                    $list[$k]['special']['special'][] = array_merge(array($a),array($value['special']));
                }
                $b += 1;
                if(!empty($list[$k]['special']['special'])){
                    foreach($list[$k]['special']['special'] as $kk=>$vv){
                        foreach($vv['item'] as $k1=>$v1){
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remin_date'] = $v1['special_end_time'] - TIMESTAMP >0 ?$v1['special_end_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_start_date'] = date('m月d日 H:i',$v1['special_start_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_date'] = date('m月d日 H:i',$v1['special_end_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_time'] = $v1['special_end_time'];
                            if(time() >= $v1['special_start_time'] && $v1['special_end_time']> time()) {
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 1;
                            }elseif($v1['special_end_time']<= time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 2;

                            }elseif($v1['special_start_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 0;
                            }
                            $auction_list = $model_auctions->getAuctionList(['special_id'=>$v1['special_id']]);
                            $all_bid_number = 0;
                            $jian_num = 0;
                            if(!empty($auction_list)){
                                foreach($auction_list as $id=>$auction_info){
                                    $all_bid_number += $auction_info['bid_number'];
                                    if($auction_info['is_liupai'] == 0 && $auction_info['auction_end_true_t'] <=time()){
                                        $jian_num += 1;
                                    }
                                }
                            }

                            $list[$k]['special']['special'][$kk]['item'][$k1]['all_bid_number'] = $all_bid_number;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['jian_num'] = $jian_num;
                        }
                    }
                }
            }
            $data = $list;
        }
        //手机端“我的视频” wap端不显示
        $model_video_category = Model('video_category');
        $cate_list = $model_video_category->getVideoCategoryList(array('is_recommend' => 1));
        if(!empty($cate_list) && is_array($cate_list)) {
            if (C('video_isuse') == 1) {
                $model_setting = Model('setting');
                $list_setting = $model_setting->getListSetting();
                $video_data = array();
                $video_list['video_isuse'] = C('video_isuse');
                $video_list['wap_display'] = 0;
                $video_list['video_modules_name'] = $list_setting['video_modules_name'];
                $video_list['video_modules_logo'] = UPLOAD_SITE_URL . DS . ATTACH_COMMON . DS . $list_setting['video_modules_logo'];
                //视频分类 显示推荐的分类
                foreach ($cate_list as $k => $v) {
                    $cate_list[$k]['cate_image'] = UPLOAD_SITE_URL . DS . ATTACH_MOBILE . DS . 'video_category' . DS . $v['cate_image'];
                }
                $video_list['item'] = $cate_list;
                $video_data['video_list'] = $video_list;
                array_push($data, $video_data);
            }
        }
        $end_data = array();
        foreach ($data as $value) {
            $end_data[] = $value;
        }
        $this->_output_special($end_data, $_GET['type']);
    }

    /**
     * 专题
     */
    public function specialOp() {
        $model_mb_special = Model('mb_special');
        $info = $model_mb_special->getMbSpecialInfoByID($_GET['special_id']);
        $list = $model_mb_special->getMbSpecialItemUsableListByID($_GET['special_id']);
        $model_auctions = Model('auctions');
        $model_store = Model('store');
        $model_guess = Model('p_guess');
        $b= 0;
        foreach ($list as $key => $value) {
            $a = array();
            if ($value['store_top']) {
                foreach ($value['store_top']['item'] as $k => $v) {
                    $store_info = $model_store->getStoreInfoByID($v['store_id']);
                    if($member_id = $this->getMemberIdIfExists()) {
                        $c = (int) Model('favorites')->getStoreFavoritesCountByMemberId($member_id, $v['store_id']);
                        $value['store_top']['item'][$k]['if_fav'] = $c > 0;
                    }
                    $value['store_top']['item'][$k]['store_logo'] = getStoreLogo( $store_info['store_label'],'store_logo'); 
                    $value['store_top']['item'][$k]['store_collect'] = $store_info['store_collect'];
                }
                $list[$key]['store_top'] = $value['store_top'];
            }

            if ($value['qucai']) {
                $list[$key]['qucai'] = $value['qucai'];
                foreach($list[$key]['qucai']['item'] as $k=>$v){
                    $qucai_info = $model_guess->getOpenGuessInfo(array('gs_id'=>$v['gs_id']));
                    $list[$key]['qucai']['item'][$k]['joined_times'] = $qucai_info['joined_times'];
                }
            }

            if ($value['auctions']) {
                foreach($value['auctions']['item'] as $k => $v) {
                    $auction_info = $model_auctions->getAuctionsInfoByID($v['auction_id']);
                    if($auction_info['auction_start_time'] <= TIMESTAMP){
                        if($auction_info['auction_start_time'] <= time() && ($auction_info['auction_end_time'] >= time() || $auction_info['auction_end_true_t'] >= time())){
                            $v['is_kaipai'] = 1;
                        }elseif($auction_info['auction_end_true_t'] < time()){
                            $v['is_kaipai'] = 2;
                        }
                    }else{
                        $v['is_kaipai'] = 0;
                    }
                    $v['auction_remin_date'] = $auction_info['auction_end_time'] - TIMESTAMP >0 ?$auction_info['auction_end_time'] - TIMESTAMP : 0;
                    $v['current_price'] = $auction_info['current_price'] != 0.00 ?$auction_info['current_price']:$auction_info['auction_start_price'];
                    $v['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                    $v['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);
                    $v['auction_start_time'] = $auction_info['auction_start_time'];
                    $v['auction_end_time'] = $auction_info['auction_end_time'];
                    $v['bid_number'] = $auction_info['bid_number'];
                    $v['auction_click'] = $auction_info['auction_click'];
                    $v['is_liupai'] = $auction_info['is_liupai'];

                    $value['auctions']['item'][$k] = $v;
                }
                $list[$key]['auctions'] = $value['auctions'];
            }

            if($value['special']){
                if($b == 0){
                    $k = $key;
                }
                unset($key);
                if(!in_array($value['special'],$a)){
                    $a[] = $value['special'];
                    $list[$k]['special']['special'][] = $value['special'];
                }
                else{
                    $list[$k]['special']['special'][] = array_merge(array($a),array($value['special']));
                }
                $b += 1;
                if(!empty($list[$k]['special']['special'])){
                    foreach($list[$k]['special']['special'] as $kk=>$vv){
                        foreach($vv['item'] as $k1=>$v1){
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remin_date'] = $v1['special_end_time'] - TIMESTAMP >0 ?$v1['special_end_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_start_date'] = date('m月d日 H:i',$v1['special_start_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_date'] = date('m月d日 H:i',$v1['special_end_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_time'] = $v1['special_end_time'];
                            if(time() >= $v1['special_start_time'] && $v1['special_end_time']> time()) {
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 1;
                            }elseif($v1['special_end_time']<= time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 2;
                            }elseif($v1['special_start_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 0;
                            }
                            $auction_list = $model_auctions->getAuctionList(['special_id'=>$v1['special_id']]);
                            $all_bid_number = 0;
                            $jian_num = 0;
                            if(!empty($auction_list)){
                                foreach($auction_list as $id=>$auction_info){
                                    $all_bid_number += $auction_info['bid_number'];
                                    if($auction_info['is_liupai'] == 0 && $auction_info['auction_end_true_t'] <=time()){
                                        $jian_num += 1;
                                    }
                                }
                            }

                            $list[$k]['special']['special'][$kk]['item'][$k1]['all_bid_number'] = $all_bid_number;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['jian_num'] = $jian_num;
                        }
                    }
                }
            }
        }
        $list_sort = array();
        $sem = array();
        $adv = array();
        foreach ($list as $key => $value) {
            if ($value['sem']){
                $sem = $value;
            } elseif($value['adv_list']){
                $adv = $value;
            } else {
                $list_sort[] = $value;
            }
        }
        if (!empty($adv)){
            array_unshift($list_sort, $adv);
        }
        if (!empty($sem)){
            array_unshift($list_sort, $sem);
        }
        $data = array_merge($info, array('list' => $list_sort));
        $this->_output_special($data, $_GET['type'], $_GET['special_id']);
    }
 
    //app首页
    public function app_indexOp(){
        $model_mb_special = Model('mb_special');
        $model_auctions = Model('auctions');
        $model_store = Model('store');
        $model_guess = Model('p_guess');
        $model_mb_special->app_index = true;
        $data = $model_mb_special->getMbSpecialAppIndex();
        $b= 0;
        foreach ($data as $key => $value) {
            if ($value['home5']) {
                $list[$key]['home5'] = $value['home5'];
            }
            if ($value['hot_good']) {
                $list[$key]['hot_good'] = $value['hot_good'];
            }
            if ($value['qucai']) {
                $list[$key]['qucai'] = $value['qucai'];
                foreach($list[$key]['qucai']['item'] as $k=>$v){
                    $qucai_info = $model_guess->getOpenGuessInfo(array('gs_id'=>$v['gs_id']));
                    $list[$key]['qucai']['item'][$k]['joined_times'] = $qucai_info['joined_times'];
                }
            }
            if ($value['cla']) {
                $list[$key]['cla'] = $value['cla'];
                foreach($list[$key]['cla']['item'] as $k=>$v){
                    $list[$key]['cla']['item'][$k] =[
                        'cla'=>$v['cla'],
                        'img_url'=>$v['e_images'],
                        'href'=>$v['e_data'],
                    ];
                }
            }
            if ($value['adv_list']) {
                $list[$key]['adv_list'] = $value['adv_list'];
            }
            $a = array();
            if ($value['store_top']) {
                foreach ($value['store_top']['item'] as $k => $v) {
                    $store_info = $model_store->getStoreInfoByID($v['store_id']);
                    if($member_id = $this->getMemberIdIfExists()) {
                        $c = (int) Model('favorites')->getStoreFavoritesCountByMemberId($member_id, $v['store_id']);
                        $value['store_top']['item'][$k]['if_fav'] = $c > 0;
                    }
                    $value['store_top']['item'][$k]['store_collect'] = $store_info['store_collect'];
                    $value['store_top']['item'][$k]['store_logo'] = getStoreLogo( $store_info['store_label'],'store_logo'); 
                }
                $list[$key]['store_top'] = $value['store_top'];
            }
            if ($value['goods']) {  /*app首页设置商品板块 */
                $list[$key]['goods'] = $value['goods'];
            }
            if ($value['enjoy_zone']) {  /*app首页设置商品板块 */
                $goods_list = Model('goods')->getGoodsList(['store_id' => C('store.zq_id'),'goods_state'=>1], 'goods_id,goods_name,goods_image,goods_promotion_price,goods_price,goods_marketprice,sales_model',null,'goods_id DESC',20);
                foreach ($goods_list as &$item) {
                    $item['goods_image'] = cthumb($item['goods_image'], 360);
                    if($item['sales_model'] != 2){
                        $item['goods_promotion_price'] = intval($item['goods_promotion_price']);
                        $item['goods_price'] = intval($item['goods_price']);
                        $item['goods_marketprice'] = intval($item['goods_marketprice']);
                    } else {
                        $item['goods_promotion_price'] = '可议价';
                        $item['goods_marketprice'] = '可议价';
                        $item['goods_price'] = '可议价';
                    }
                }
                unset($item);
                $value['enjoy_zone']['good_list'] = $goods_list;
                $list[$key]['enjoy_zone'] = $value['enjoy_zone'];
            }

            if ($value['auctions']) {
                foreach($value['auctions']['item'] as $k => $v) {
                    $auction_info = $model_auctions->getAuctionsInfoByID($v['auction_id']);
                    if($auction_info['auction_start_time'] <= TIMESTAMP){
                        if($auction_info['auction_start_time'] <= time() && ($auction_info['auction_end_time'] >= time() || $auction_info['auction_end_true_t'] >= time())){
                            $v['is_kaipai'] = 1;
                        }elseif($auction_info['auction_end_true_t'] < time()){
                            $v['is_kaipai'] = 2;
                        }
                    }else{
                        $v['is_kaipai'] = 0;
                    }
                    $v['auction_remin_date'] = $auction_info['auction_end_time'] - TIMESTAMP >0 ?$auction_info['auction_end_time'] - TIMESTAMP : 0;
                    $v['current_price'] = $auction_info['current_price'] != 0.00 ?$auction_info['current_price']:$auction_info['auction_start_price'];
                    $v['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                    $v['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);
                    $v['auction_start_time'] = $auction_info['auction_start_time'];
                    $v['auction_end_time'] = $auction_info['auction_end_time'];
                    $v['bid_number'] = $auction_info['bid_number'];
                    $v['auction_click'] = $auction_info['auction_click'];
                    $v['is_liupai'] = $auction_info['is_liupai'];

                    $value['auctions']['item'][$k] = $v;
                }
                $list[$key]['auctions'] = $value['auctions'];
            }

            if($value['special']){
                if($b == 0){
                    $k = $key;
                }
                unset($key);
                if(!in_array($value['special'],$a)){
                    $a[] = $value['special'];
                    $list[$k]['special']['special'][] = $value['special'];
                }
                else{
                    $list[$k]['special']['special'][] = array_merge(array($a),array($value['special']));
                }
                $b += 1;
                if(!empty($list[$k]['special']['special'])){
                    foreach($list[$k]['special']['special'] as $kk=>$vv){
                        foreach($vv['item'] as $k1=>$v1){
                            if($v1['special_end_time'] - TIMESTAMP <= 0){
                                unset($list[$k]['special']['special'][$kk]['item'][$k1]);
                                continue;
                            }
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_end'] = $v1['special_end_time'] - TIMESTAMP >0 ?$v1['special_end_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_start'] = $v1['special_start_time'] - TIMESTAMP >0 ?$v1['special_start_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_rate'] = $v1['special_rate_time'] - TIMESTAMP >0 ?$v1['special_rate_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_start_date'] = date('m月d日 H:i',$v1['special_start_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_date'] = date('m月d日 H:i',$v1['special_end_time']);
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_end_time'] = $v1['special_end_time'];
                            if(time() >= $v1['special_start_time'] && $v1['special_end_time']> time()) {
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 1;
                            }elseif($v1['special_end_time']<= time() || $v1['special_preview_start'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 2;
                                unset($list[$k]['special']['special'][$kk]['item'][$k1]);
                                continue;
                            }elseif($v1['special_start_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 0;
                            }
                            if($v1['special_rate_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 3;
                            }
                            $list[$k]['special']['special'][$kk]['item'][$k1]['day_num'] = Logic('auction')->getInterestDay($v1['special_rate_time'],$v1['special_end_time']);//计息天数
                            $v1['store_label'] = Model("store")->getStoreDetail(['store_id'=>$v1['store_id']],'store_label','store_label');
                            $list[$k]['special']['special'][$kk]['item'][$k1]['store_logo'] = getStoreLogo($v1['store_label'],'store_logo');
                            $auction_list = $model_auctions->getAuctionList(['special_id'=>$v1['special_id']]);
                            $all_bid_number = 0;
                            $jian_num = 0;
                            $all_price = 0;
                            $special_click = 0;
                            if(!empty($auction_list)){
                                foreach($auction_list as $id=>$auction_info){
                                    $all_bid_number += $auction_info['bid_number'];
                                    $special_click += $auction_info['auction_click'];
                                    if($auction_info['is_liupai'] == 0 && $auction_info['auction_end_true_t'] <=time()){
                                        $jian_num += 1;
                                        $all_price += $auction_info['current_price'];
                                    }
                                }
                            }

                            $list[$k]['special']['special'][$kk]['item'][$k1]['all_bid_number'] = $all_bid_number;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_click'] = $special_click;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['jian_num'] = $jian_num;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['all_price'] = $all_price;
                        }
                    }
                }
            }
            $data = $list;
        }

        $this->_output_special($data, $_GET['type']);
    }

    /**
     * 输出专题
     */
    private function _output_special($data, $type = 'json', $special_id = 0) {
        $model_special = Model('mb_special');
        if($_GET['type'] == 'html') {
            $html_path = $model_special->getMbSpecialHtmlPath($special_id);
            if(!is_file($html_path)) {
                ob_start();
                Tpl::output('list', $data);
                Tpl::showpage('mb_special');
                file_put_contents($html_path, ob_get_clean());
            }
            header('Location: ' . $model_special->getMbSpecialHtmlUrl($special_id));
            die;
        } else {
            $data[] = ['yinuo_store_id'=>C('store.zq_id')];
            output_data($data);
        }
    }

    /**
     * android客户端版本号
     */
    public function apk_versionOp() {
        $version = C('mobile_apk_version');
        $url = C('mobile_apk');
        if(empty($version)) {
           $version = '';
        }
        if(empty($url)) {
            $url = '';
        }

        output_data(array('version' => $version, 'url' => $url));
    }

    /**
     * 默认搜索词列表
     */
    public function search_key_listOp() {
        $list = @explode(',',C('hot_search'));
        if (!$list || !is_array($list)) { 
            $list = array();
        }
        if ($_COOKIE['hisSearch'] != '') {
            $his_search_list = explode('~', $_COOKIE['hisSearch']);
        }
        if (!$his_search_list || !is_array($his_search_list)) {
            $his_search_list = array();
        }
        output_data(array('list'=>$list,'his_list'=>$his_search_list));
    }

    /**
     * 热门搜索列表
     */
    public function search_hot_infoOp() {
        if (C('rec_search') != '') {
            $rec_search_list = @unserialize(C('rec_search'));
        }
        $rec_search_list = is_array($rec_search_list) ? $rec_search_list : array();
        $result = $rec_search_list[array_rand($rec_search_list)];
        output_data(array('hot_info'=>$result ? $result : array()));
    }

    /**
     * 高级搜索
     */
    public function search_advOp() {
        $area_list = Model('area')->getAreaList(array('area_deep'=>1),'area_id,area_name');
        if (C('contract_allow') == 1) {
            $contract_list = Model('contract')->getContractItemByCache();
            $_tmp = array();$i = 0;
            foreach ($contract_list as $k => $v) {
                $_tmp[$i]['id'] = $v['cti_id'];
                $_tmp[$i]['name'] = $v['cti_name'];
                $i++;
            }
        }
        output_data(array('area_list'=>$area_list ? $area_list : array(),'contract_list'=>$_tmp ? $_tmp : array()));
    }

    public function auction_listOp(){
        $model_auctions = Model('auctions');
        if (!empty($_GET['keyword'])) {
            $condition['auction_name'] = array('like', '%' . $_GET['keyword'] . '%');
        }
        //排序方式
        $order = $this->_auction_list_order($_GET['key'], $_GET['order']);
        $condition['state'] = array('in',array(0,1));
        $count = $model_auctions->getAuctionsCount($condition);
        $auctions_list = $model_auctions->getAuctionList($condition,'*','',$order,0,$this->page,$count);
        foreach($auctions_list as $k => $auctions_info){
            if(time() >= $auctions_info['auction_start_time']){
                if($auctions_info['auction_start_time'] <= time() && ($auctions_info['auction_end_time'] >= time() || $auctions_info['auction_end_true_t'] >= time())){
                    $is_kaipai = 1;
                }elseif($auctions_info['auction_end_true_t'] < time()){
                    $is_kaipai = 2;
                }
            }else{
                $is_kaipai = 0;
            }
            $auctions_list[$k]['is_kaipai'] = $is_kaipai;
            $auctions_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'],360);
            $auctions_list[$k]['auction_start_date'] = date('m月d日 H:i',$auctions_info['auction_start_time']);
            $auctions_list[$k]['auction_end_date'] = date('m月d日 H:i',$auctions_info['auction_end_time']);
            $auctions_list[$k]['auction_remain_date'] = $auctions_info['auction_end_time'] - time() > 0 ? $auctions_info['auction_end_time'] - time() : 0;
            $auctions_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00 ? $auctions_info['current_price'] : $auctions_info['auction_start_price'];
        }
        $page_count = $model_auctions->gettotalpage();
        output_data(array('auction_list' => $auctions_list ),mobile_page($page_count));
    }

    /**
     * 拍卖列表排序方式
     */
    private function _auction_list_order($key, $order) {
        $result = 'auction_id desc';
        if (!empty($key)) {

            $sequence = 'desc';
            if($order == 1) {
                $sequence = 'asc';
            }

            switch ($key) {
                //最新
                case '1' :
                    $result = 'auction_start_time' . ' ' . $sequence;
                    break;
                //浏览量
                case '2' :
                    $result = 'bid_number' . ' ' . $sequence;
                    break;
                //价格
                case '3' :
                    $result = 'current_price' . ' ' . $sequence;
                    break;
            }
        }
        return $result;
    }

    // 刷新app端专题页静态页面
    public function _refresh_app_special_html(){
        $special_list = Model('mb_special')->where(array('special_id' => array('gt',1)))->field('special_id')->select();
        foreach ($special_list as $key => $value) {
            $this->_get_special_data($value['special_id']);
        }
    }

        
}
