<?php
/**
 * 会员合作伙伴
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class member_partnerControl extends mobileMemberControl {
    public function __construct(){
        parent::__construct();
    }
    /**
     * 合伙人主页
     */
    public function indexOp(){
        $data = array();
        $data['partner_count'] = $this->member_info['distribute_lv_1'] + $this->member_info['distribute_lv_2'];
        $model_member_commission = Model('member_commission');
        $amount_list = $model_member_commission->getCommissionAmountByType(array('dis_member_id'=>$this->member_info['member_id'],'commission_type'=>array('neq',6),'dis_commis_state'=>1));
//        print_R($amount_list);exit;
        $amount = 0;
        $commission_list = array(
            1=>array(
                'type_name'=>'消费金',
                'amount'=>0
            ),
            2=>array(
                'type_name'=>'推荐会员',
                'amount'=>0
            ),
            3=>array(
                'type_name'=>'保证金收益',
                'amount'=>0
            ),
            4=>array(
                'type_name'=>'推荐艺术家',
                'amount'=>0
            ),
        );
        if(!empty($amount_list)){
            foreach($amount_list as $v){
                $amount += $v['commission_amount'];
                switch($v['commission_type']){
                    case 1:
                        $commission_list[1]['amount'] += $v['commission_amount'];
                        break;
                    case 2:
                        $commission_list[1]['amount'] += $v['commission_amount'];
                        break;
                    case 3:
                        $commission_list[3]['amount'] += $v['commission_amount'];
                        break;
                    case 4:
                        $commission_list[2]['amount'] += $v['commission_amount'];
                        break;
                    case 5:
                        $commission_list[4]['amount'] += $v['commission_amount'];
                        break;
                }
            }
        }
        $goods_amount = $model_member_commission->getCommissionInfo(array('dis_member_id'=>$this->member_info['member_id'],'dis_commis_state'=>1,'commission_type'=>array('in',array(1,2))),'sum(goods_pay_amount) goods_amount');
//        dd();
//        print_R($tmp);exit;
        $data['goods_amount'] = $goods_amount['goods_amount'] > 0?$goods_amount['goods_amount']:'0.00';
        $data['amount'] = $amount;
        $data['commission_list'] = $commission_list;
        output_data($data);
    }

    //合伙人列表
    public function partner_listOp(){
        if($_POST['is_childrens'] == 'ok'){
            $this->_getChildsPartnerList($_POST);
        }else{
            $this->_getPartnerList($_POST['is_artist']);
        }
    }

    /**
     * 获取二级合作伙伴
     * @param $param
     */
    private function _getPartnerList($is_artist){
        $lv = $_REQUEST['lv'];
        $member_id = $this->member_info['member_id'];

        if($_REQUEST['form_member_id']){
            $member_id = $_REQUEST['form_member_id'];
            $lv = 1;
        }
        if (empty($member_id)) {
            \App\Exceptions\ApiResponseException::throwFailMsg('memberId不能为空');
        }

        switch($lv){
            case 1:
                $condition = "top_member = {$member_id}";
                break;
            case 2:
                $condition = "top_member_2 = {$this->member_info['member_id']}";
                break;
            case 3:
                $condition = "(top_member = {$this->member_info['member_id']} OR top_member_2 = {$this->member_info['member_id']})  AND member_distribute.member_type > 0";
                break;
            default:
                $condition = "(top_member = {$this->member_info['member_id']} OR top_member_2 = {$this->member_info['member_id']})";
                break;
        }

        $field = 'member_distribute.*,member.member_name,member.member_avatar,member.member_time,member.member_type,member_comment,member.member_mobile,member.weixin_info';
        $member_model = Model('member');
        $total = $member_model->getCountDistribute("top_member = {$this->member_info['member_id']} or top_member_2 = {$this->member_info['member_id']}");
        //$this->page  下边的方法限制显示5条--8-1关闭
        $member_list = $member_model->getMemberDistributeWithMemberList($condition, $field);
        $page_count = $member_model->gettotalpage();

        $this->_dataSort($member_list);
        $list = array_values($member_list);

        if(!empty($lv)){
            output_data(array('member_list' => $list), mobile_page($page_count));
        }

        $lv_total_1 = $member_model->getCountDistribute("top_member = {$this->member_info['member_id']}");
        $lv_total_2 = $member_model->getCountDistribute("top_member_2 = {$this->member_info['member_id']}");
        $lv_total_3 = $member_model->getCountDistribute("(top_member = {$this->member_info['member_id']} OR top_member_2 = {$this->member_info['member_id']}) AND member_type > 0");

        //$this->_checkChildInfo($member_list, $member_ids);
        //下级人数注释掉
        /*if($lv == 1 && !empty($list)){
            foreach($list as &$m_val){
                $m_val['total'] = $member_model->getCountDistribute("top_member = {$m_val['member_id']} or top_member_2 = {$m_val['member_id']}");
            }
        }*/
        $new_data_list = [];
        $cur_year = date("Y",time());
        foreach ($member_list as $value){
            $month_key = date('m',$value['add_time']).'月';
            $year_key = date('Y',$value['add_time']);
            $month_key = ($cur_year == $year_key) ? $month_key : $year_key.'年'.$month_key;
            $value['add_time'] = date('Y.m.d',$value['add_time']);
            $new_data_list[$month_key][] = $value;
        }
        output_data(array('member_list' => $new_data_list,'partner_level'=>2,'user_name'=>$this->member_info['member_name'],'total'=>$total,'lv_total_1'=> $lv_total_1,'lv_total_2'=>$lv_total_2,'lv_total_3'=>$lv_total_3), mobile_page($page_count));
    }

    /**
     * 获取三级合作伙伴
     * @param $param
     */
    private function _getChildsPartnerList($param){
        $condition = array();
        if(intval($param['upper_id']) <= 0){
            output_error('参数错误');
        }
        $condition['top_member'] = $param['upper_id'];
        $field = 'member_distribute.member_id,member.member_name,member.member_avatar,member.member_time,member.member_type,member_comment,member.member_mobile,member.weixin_info';
        $member_model = Model('member');
        $member_list = $member_model->getMemberDistributeWithMemberList($condition, $field, $this->page);
        $member_ids = $this->_dataSort($member_list);
        $this->_checkChildInfo($member_list, $member_ids);
        $page_count = $member_model->gettotalpage();
        $total = $member_model->getCountDistribute("top_member = {$param['upper_id']}");
        $list = array_values($member_list);
        $member_info = $member_model->getMemberInfoByID($param['upper_id']);

        $amount = Model('member_commission')->get_special_commission_amount($param['upper_id']);
        output_data(array('member_list' => $list,'partner_level'=>3,'user_name'=>$member_info['member_name'],'total'=>$total,'commission_amount'=>$amount), mobile_page($page_count));
    }

    /**
     * 整理数据结构
     * @param $member_list
     * @return array
     */
    private function _dataSort(& $member_list){
        $member_ids = array();
        $tmp_list = array();
        $member_type_list= Logic('j_member')->getMemberTypeList();
        foreach($member_list as $key => $member){
            array_push($member_ids,$member['member_id']);
            $weixin_info = unserialize($member['weixin_info']);
            $tmp_list[$member['member_id']]['member_id'] = $member['member_id'];
            $tmp_list[$member['member_id']]['member_name'] = empty($weixin_info['nickname']) ? tellHide($member['member_name']) : $weixin_info['nickname'];
            if(substr($member['member_avatar'], 0, 4) == 'http'){
                $tmp_list[$member['member_id']]['member_avatar'] = $member['member_avatar'];
            } else {
                $tmp_list[$member['member_id']]['member_avatar'] = getMemberAvatarForID($member['member_id']);
            }
            if ($this->member_info['member_id'] == $member['top_member']) {
                $tmp_list[$member['member_id']]['comment'] = tellHide($member['member_comment']);
            } elseif ($this->member_info['member_id'] ==  $member['top_member_2']) {
                $tmp_list[$member['member_id']]['comment'] = tellHide($member['member_comment_2']);
            }
            $tmp_list[$member['member_id']]['member_time'] = date('Y-m-d', $member['member_time']);
            $tmp_list[$member['member_id']]['add_time'] = $member['add_time'];
            $tmp_list[$member['member_id']]['commission_amount'] = ncPriceFormat(0);
            $tmp_list[$member['member_id']]['member_type_display'] = $member_type_list[$member['member_type']]['name'];
            $tmp_list[$member['member_id']]['member_type_img'] = getMemberVendueForID($member['member_type']);
            $tmp_list[$member['member_id']]['member_mobile'] = $member['member_mobile'] ? tellHide($member['member_mobile']) : '';
            unset($member_list[$key]);
        }
        $member_list = $tmp_list;
        return $member_ids;
    }

    /**
     * 补充合作伙伴佣金数据
     * @param $member_list
     * @param $member_ids
     */
    private function _checkChildInfo(& $member_list, $member_ids){
        if(!empty($member_list) && !empty($member_ids)){
            $condition = array();
            $condition['from_member_id'] = array('in',$member_ids);
            $condition['dis_member_id'] = $this->member_info['member_id'];
            $list = Model('member_commission')->get_members_commission($condition);
            foreach($list as $val){
                $member_list[$val['from_member_id']]['commission_amount'] = ncPriceFormat($val['commission_amount']);
            }
        }
    }

    /**
     * 编辑合伙人备注
     *
     * @param string $member_id 合伙人ID
     * @param string $comment || $comment2 一级或二级备注
     * return string success || fail 返回成功或失败
     * */
    public function update_member_commentOp(){
        $model_member = Model('member');
        $member_id = $_POST['member_id'];

        $condition = array('member_id'=>$member_id);
        $member_info = $model_member->getUpperMember($condition);

        if($member_info && isset($_POST['comment'])){
            if ($member_info['top_member'] == $this->member_info['member_id']) {
                $updata = array('member_comment'=>$_POST['comment']);
            } elseif ($member_info['top_member_2'] == $this->member_info['member_id']) {
                $updata = array('member_comment_2'=>$_POST['comment']);
            }
            $model_member = Model('member');
            $re = $model_member->editMemberDistribute($updata, $condition);
            if($re == 1){
                output_data('success');
            }else{
                output_data('fail');
            }
        }
        output_data('fail');
    }

    /*
    * 我的升级
    */
    public function partner_countOp(){
        $partner_special = [
            'member_type_name' =>$this->member_info['member_type_name'],
            'available_commis' =>$this->member_info['available_commis'],
            'member_type' =>$this->member_info['member_type'],
            'td_amount' =>$this->member_info['td_amount'],
            'store_id'=>C('store.zq_id')
        ];
        $dis_type_model = Model('distribution_config');
        $condition = [
            'member_distribute_detail.level'=>1
        ];
        $field = 'member_distribute_type.type_id,member_distribute_type.name,member_distribute_type.price,member_distribute_detail.goods_num';
        $dis_list = $dis_type_model->getDisList($condition, $field, $group = 'member_distribute_type.type_id');
        foreach ($dis_list as $key => &$value){
            $value['price'] = ($value['price'] > $partner_special['td_amount']) ? ($value['price'] - $partner_special['td_amount']) : 0.00;
            $partner_special['dis_list'.$key] = $value;
        }
        unset($value);
        output_data($partner_special);
    }

    /**
     * 合伙人信息详情
     *
     * @param   int $member_id 会员ID
     * @return  array[
     *      dis_id            主键ID
     *      member_id         合伙人ID
     *      member_avatar     合伙人头像
     *      member_name       合伙人名字
     *      member_type       合伙人等级logo
     *      member_mobile     合伙人手机号码
     *      comment           合伙人备注
     *      add_time          邀请时间
     *      dis_code          邀请码
     *      top_member_name   上级用户名
     *      top_member_avatar 上级用户头像
     *      top_member        上级ID
     *      top_member_2      上上级ID
     *      member_comment    一级备注
     *      member_comment_2  二级备注
     *      top               分销等级
     * ]
     * */
    public function partner_infoOp(){
        if(!($_POST['member_id'] > 0)){
            output_data('参数错误');
        }
        /** @var memberModel $member_model */
        $member_model = Model('member');
        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $member_id = $_POST['member_id'];
        $condition = [
            'member_id' => $member_id,
            'top_member|top_member_2' => $this->member_info['member_id'],
            '_op' => 'and'
        ];
        $partner_info = $member_model->getUpperMember($condition);
        if($partner_info){
            $member_partner_info = $member_model->getMemberInfoByID($member_id);
            $top_member_info = $member_model->getMemberInfoByID($partner_info['top_member']);

            if ($this->member_info['member_id'] == $partner_info['top_member']) {
                $partner_info['top'] = 1;
                $partner_info['comment'] = tellHide($partner_info['member_comment']);
            } elseif ($this->member_info['member_id'] ==  $partner_info['top_member_2']) {
                $partner_info['top'] = 2;
                $partner_info['comment'] = tellHide($partner_info['member_comment_2']);
            }
            $partner_info['member_type'] = getMemberVendueForID($member_partner_info['member_type']);
            $partner_info['top_member_type_logo'] = getMemberVendueForID($top_member_info['member_type']);
            $partner_info['member_avatar'] = getMemberAvatarForID($member_partner_info['member_id'],$member_partner_info['member_avatar']);
            $partner_info['top_member_avatar'] = getMemberAvatarForID($top_member_info['member_id'],$top_member_info['member_avatar']);
            $partner_info['top_member_name'] = $top_member_info['member_name'];
            $partner_info['member_mobile'] = tellHide($member_partner_info['member_mobile']);
            $partner_info['add_time'] = date('Y.m.d',$partner_info['add_time']);
            $partner_info['top_member_name'] = tellHide($partner_info['top_member_name']);
            $partner_info['member_name'] = tellHide($member_partner_info['member_name']);
        }
        $partner_count = $member_model->getCountDistribute(['top_member'=>$_REQUEST['member_id']]);
        $commission_sum = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $member_id,array('top_lv'=>1));

        output_data(['partner_info'=>$partner_info, 'partner_count'=>$partner_count, 'commission_sum'=>$commission_sum]);die;
    }

    /*
    * 合伙人搜索
    */
    public function search_partner_listOp(){
        if (empty($_GET['keyword'])) {
            output_data('暂无匹配数据');
        }

        $condition = "
        (
            top_member = {$this->member_info['member_id']} 
            AND 
            (
                member.member_name LIKE '%".$_GET['keyword']."%' 
                OR 
                member.member_mobile LIKE '%".$_GET['keyword']."%'
                OR 
                member_distribute.member_comment LIKE '%".$_GET['keyword']."%'
            )
        )
        OR
        (
            top_member_2 = {$this->member_info['member_id']}
             AND 
            (
                member.member_name LIKE '%".$_GET['keyword']."%'
                OR 
                member.member_mobile LIKE '%".$_GET['keyword']."%'
                OR 
                member_distribute.member_comment_2 LIKE '%".$_GET['keyword']."%'
            )
        )";

        $field = 'member.member_id,member.member_name';
        $member_list = Model('member')->getMemberDistributeWithMemberList($condition, $field, 10);

        output_data($member_list);
    }

}