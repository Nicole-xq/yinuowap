<?php
/**
 * 趣猜
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class guessControl extends mobileHomeControl{
    public function __construct() {
        parent::__construct();
    }

    /**
     * 趣猜列表
     * @return [type] [description]
     */
    public function guess_listOp() {
        $condition = array();
        $gs_model = Model('p_guess');
        if($_GET['close']){
            $condition['gs_state'] = 2;
            $guess_list = $gs_model->getGuessList1($condition,$this->page,'end_time desc',5);

            /** @var guess_offerModel $model_guess_offer */
            $model_guess_offer = Model('guess_offer');
            $condition['gs_state'] = array('in',array(2,3));

            foreach ($guess_list as $key => $value) {
                $condition['gs_id'] = $value['gs_id'];
                $guess_user = $model_guess_offer->where($condition)->find();
                $value['guess_user'] = $guess_user['member_name'] ? hideStar($guess_user['member_name']) : '本场无人中奖';
                $value['is_close'] = 1;
                $value['gs_offer_price'] = $guess_user['gs_offer_price'];
                $value['gs_max_price'] = (int)$value['gs_max_price'];
                $value['gs_min_price'] = (int)$value['gs_min_price'];
                $value['gs_img'] = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$value['gs_img'];
                $guess_list_deal[$key] = $value;
            }
            $page_count = 1;
        }else{
            $guess_list = $gs_model->getGuessOpenListForMobile($condition,$this->page,'',10);
            $guess_list_deal = array();
            foreach ($guess_list as $key => $value) {
                $value['gs_img'] = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$value['gs_img'];
                $value['gs_max_price'] = (int)$value['gs_max_price'];
                $value['gs_min_price'] = (int)$value['gs_min_price'];
                $guess_list_deal[$key] = $value;
            }
            $page_count = $gs_model->gettotalpage();
        }


        output_data(array('guess_list' => $guess_list_deal), mobile_page($page_count));
    }


    /**
     * 趣猜详情页
     * @return [type] [description]
     */
    public function guess_detailOp()
    {
        $gs_id = $_GET['gs_id'];
        if (empty($gs_id)) {
            output_error('参数有误请重试');
        }
        $time = 0;
        $model_p_guess = Model('p_guess');
        $model_guess_offer = Model('guess_offer');
        $guess_info = $model_p_guess->getOpenGuessInfo(array('gs_id'=>$gs_id));
        if ($guess_info['gs_state'] != 2) {
            unset($guess_info['gs_right_price']);
            unset($guess_info['gs_discount_price']);
            unset($guess_info['gs_estimate_price']);
            unset($guess_info['gs_estimate_price']);
            unset($guess_info['points']);
        } else {
            $guess_info['discount'] = round(($guess_info['gs_discount_price']/$guess_info['gs_cost_price'])*10,2);;
            $winer = $model_guess_offer->get_guess_offer(array('gs_id' => $gs_id,'gs_state' => array('in',array(2,3))));
            if (empty($winer)) {
                $guess_info['if_has_actor'] = false;
            } else {
                $if_create_order = Model('order')->getOrderCount(['buyer_id' => $winer['member_id'],'gs_id' =>$gs_id,'delete_state' => 0]);
                $winer['gs_state'] = empty($if_create_order) ? 2 : 3;
                $guess_info['if_has_actor'] = true;
                $guess_info['winer'] = $winer;
                $guess_info['winer']['member_name'] = hideStar($guess_info['winer']['member_name']);
                if (($winer['end_time'] + C('guess_add_cart_limit_time')) > time()){
                    $guess_info['if_out_time'] = false;
                } else {
                    $guess_info['if_out_time'] = true;
                }
            }
        }
        //当前用户有无趣猜出价
        $guess_offer_data = $model_guess_offer->get_guess_offer(array('member_id' => $this->getMemberIdIfExists(), 'gs_id' => $gs_id),$field = 'id');
        if ($guess_offer_data) {
            $guess_info['guess_offer_price'] = true;
        }

        $store_info = Model('store')->getStoreInfoByID($guess_info['store_id']);
        $guess_info['store_member_id'] = $store_info['member_id'];
        if (empty($guess_info)) {
            output_error('趣猜不存在或者已经关闭');
        }
        $guess_info['guess_start_flag'] = $guess_info['start_time'] > time() ? true : false;
        $guess_info['guess_start_remain_time'] = $guess_info['start_time'] > time() ? $guess_info['start_time'] - time() : 0;
        $guess_info['guess_end_flag'] = ($guess_info['start_time'] < time() && $guess_info['end_time'] > time()) ? true : false;
        $guess_info['guess_end_remain_time'] = ($guess_info['start_time'] < time() && $guess_info['end_time'] > time()) ? $guess_info['end_time'] - time() : 0;
        $data = array();
        $data['guess']['guess_info'] = $guess_info;

        // 商品详情
        //获取 goods ids
        $gs_goods_list = $model_p_guess->get_guess_goods_list(array('gs_id' => $gs_id),array('goods_id,goods_image'));
        $goods_id_list = array();
        $goods_img_list = array();
        foreach ($gs_goods_list as $key => $value) {
//            $re = Model('goods')->getGoodsInfo(array('goods_id'=>$value['goods_id']));
//            $data['guess']['guess_info']['detail'] = $re['mobile_body'] == ''?$re['goods_body']:$re['mobile_body'];//趣猜组合弃用,不考虑多商品情况
            $goods_id_list[$key] = $value['goods_id'];
            $data['guess']['guess_info']['goods_id'] = $value['goods_id'];
            $goods_img_list[$key] = cthumb( $value['goods_image'], 360);
        }
        $data['banner']['list'] = $goods_img_list;
        
        // 获取 goods_list
        $model_goods = Model('goods');
        $goods_list =  $model_goods->getGoodsList(array('goods_id' => array('in',$goods_id_list)),'goods_commonid,spec_name,goods_spec');
        $goods_commonid_list = array();
        foreach ($goods_list as $key => $value) {
            $goods_commonid_list[]= $value['goods_commonid'];
            $spec_name = array_values(unserialize($value['spec_name']));
            $goods_spec = array_values(unserialize($value['goods_spec']));
            $spec_name_value = [];
            foreach ($spec_name as $item =>$val){
                $spec_name_value[] = $val.':'.$goods_spec[$item];
            }
        }
        $data['guess']['guess_info']['spec_name_value'] = $spec_name_value;
        $goods_commonid_list = array_unique($goods_commonid_list);
        $goods_common_list = $model_goods->getGoodsCommonList(array('goods_commonid' => array('in',$goods_commonid_list)),'mobile_body,goods_body,goods_attr');
        foreach ($goods_common_list as $key => $value) {
            $mobile_body_array[] = unserialize($value['mobile_body']);
            $goods_attr = array_values(unserialize($value['goods_attr']));
            $goods_body_array[] = $value['goods_body'];
            $goods_goods_jingle = $value['goods_jingle'];
            foreach ($goods_attr as $item =>$val){
                $goods_attr[$item] = array_values($val);
            }
        }
        if(!empty($mobile_body_array[0])){
            $str = '';
            foreach($mobile_body_array[0] as $v){
                if($v['type'] == 'image'){
                    $str .= '<img src="'.$v['value'].'">';
                }else{
                    $str .= $v['value'];
                }
            }
        }
        $data['guess']['guess_info']['detail'] = $str == ''?$goods_body_array[0]:$str;
        $data['guess']['guess_info']['goods_jingle'] = $goods_goods_jingle;
        $data['guess']['guess_info']['goods_attr'] = $goods_attr;
        // 是否收藏过该趣猜
        $store_online_info = Model('store')->getStoreOnlineInfoByID($guess_info['store_id']);
      
        // 店铺头像
        $store_info['store_avatar'] = $store_online_info['store_avatar']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_online_info['store_avatar']
            : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
        $data['guess']['guess_info']['store_avatar'] = $store_info['store_avatar'];
        $model_favorites = Model('favorites'); 
        $favorites_status = $model_favorites->getOneFavorites(array('fav_type' => 'guess', 'member_id' => $this->member_info['member_id'], 'fav_id' => $gs_id));
        $data['guess']['guess_info']['favorites_status'] = $favorites_status;
      
        // 最高竞猜价和最低竞猜价
        $guess_offer_min = $model_guess_offer->get_guess_offer_min(array('gs_id' => $gs_id));
        $data['guess']['guess_info']['guess_offer_min'] = $guess_offer_min['gs_offer_price'] ? $guess_offer_min['gs_offer_price'] : 0; 
        $guess_offer_max = $model_guess_offer->get_guess_offer_max(array('gs_id' => $gs_id));
        $data['guess']['guess_info']['guess_offer_max'] =  $guess_offer_max['gs_offer_price'] ? $guess_offer_max['gs_offer_price'] : 0;
        
        // 趣猜参与人员列表
        $data['guess']['guess_info']['guesslog'] = $this->guesslog($gs_id,0,5);

        // 更多趣猜
        $more_guess_array = $model_p_guess->getGuessOpenList(array('end_time' => array('gt',time()),'gs_id' => array('neq',$gs_id)),'*',$order = 'end_time asc',4);
        $more_guess = array();
        foreach ($more_guess_array as $key => $value) {
            $value['gs_time_flag'] = $value['start_time']>time() ? 1 : 2;
            $value['gs_remain_time'] = $value['start_time']>time() ? $value['start_time'] - time() : $value['end_time'] - time();
            $value['gs_img'] = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$value['gs_img']; 
            $value['discount'] = round(($value['gs_discount_price']/$value['gs_cost_price'])*10,2);;
            unset($value['gs_right_price']);
            unset($value['gs_discount_price']);
            unset($value['gs_estimate_price']);
            unset($value['gs_estimate_price']);
            $more_guess[$key] = $value;
        }
        $data['guess']['more_guess'] = $more_guess;

        //拍品推荐
        $model_auction = Model('auctions');
        $where = array();
        // $where['recommended'] = 1;
        $auction_recommended_list = $model_auction->getAuctionList($where,'*','','','',4);
        foreach($auction_recommended_list as $k => $auctions_info){
            $auction_recommended_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'],360);
            if(time() >= $auctions_info['auction_start_time']){
                if($auctions_info['auction_start_time'] <= time() && ($auctions_info['auction_end_time'] >= time() || $auctions_info['auction_end_true_t'] >= time())){
                    $v['is_kaipai'] = 1;
                }elseif($auctions_info['auction_end_true_t'] < time()){
                    $v['is_kaipai'] = 2;
                }
            }else{
                $is_kaipai = 0;
            }
            $auction_recommended_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00 ? $auctions_info['current_price'] : $auctions_info['auction_start_price'];
            $auction_recommended_list[$k]['is_kaipai'] = $is_kaipai;
        }

        $data['guess']['recommended_list'] = $auction_recommended_list;
        
        //增加增加围观次数
        $model_p_guess->addViews(array('gs_id' => $gs_id));
        $guess_offer_data = $model_guess_offer->get_guess_offer(array('member_id' => $this->getMemberIdIfExists(), 'gs_id' => $gs_id));
        $is_guess = 0;
        if ($guess_offer_data) {
           $is_guess = 1;
        }
        // 是否收藏和提醒
        $favorites_status = Model('favorites')->getOneFavorites(array('fav_type' => 'guess', 'member_id' => $this->getMemberIdIfExists(), 'fav_id' => $gs_id));
        $data['guess']['guess_info']['is_favorite_flag'] = empty($favorites_status) ? false : true;
        $relation = Model('guess_member_relation')->getRelationInfoByMemberGuessID($this->getMemberIdIfExists(), $gs_id);
        $data['guess']['guess_info']['is_remind_flag'] = empty($relation) ? false : true;
        output_data(array('guess' => $data['guess'],'banner' => $data['banner'],'u_id'=>$this->getMemberIdIfExists(),'is_guess'=>$is_guess));
    }


     /** 
     * 趣猜记录
     * @return [type] [description]
     */
    public function guesslog($gs_id,$page = null,$limit = null) {
        $model_guess_offer = Model('guess_offer');
        $result = $model_guess_offer->get_guess_offer_list(array('gs_id'=>$gs_id),$page,$limit);
        $data = array();
        foreach ($result as $key => $value) {
            $value['member_name'] = hideStar($value['member_name']); 
            $value['partake_time'] = date('Y-m-d H:i:s', $value['partake_time']);
            $data[$key] = $value;
        }
        return $data;
    }

    public function guess_logOp(){
        $gs_id = $_GET['gs_id'];
        $curpage = $_GET['cur_page'];
        $data = $this->guesslog($gs_id,$this->page);
        $count = count($this->guesslog($gs_id));
        $pageCount = ceil($count/$this->page);
        output_data(array('list'=>$data,'total'=>$pageCount,'cur_page'=>$curpage,'per_count'=>$this->page,'guess_count'=>$count));
    }

    /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240);
    }
}
