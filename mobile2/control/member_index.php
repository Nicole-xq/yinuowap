<?php
/**
 * 我的商城
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_indexControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 我的商城
     */
    public function indexOp() {
        $member_info = array();
        $memberId =  $this->member_info['member_id'];
        $member_info['user_id'] = $memberId;
        $member_info['user_name'] = $this->member_info['member_name'];
        $member_info['type_logo'] = getMemberVendueForID($this->member_info['member_type']);
        $member_info['user_name'] = $this->member_info['member_truename'] ? $this->member_info['member_truename'] : $this->member_info['member_name'];
        if(substr($this->member_info['member_avatar'], 0, 4) == 'http'){
            $member_info['avatar'] = $this->member_info['member_avatar'] ;
        } else {
            $member_info['avatar'] = getMemberAvatarForID($this->member_info['member_id']).'?'.TIMESTAMP;
        }
        $member = Model('member')->getMemberInfoByID($this->member_info['member_id']);
        $member_gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
        $member_info['level'] = $member_gradeinfo['level'];
        $member_info['level_name'] = $member_gradeinfo['level_name'];
        $member_info['favorites_store'] = Model('favorites')->getStoreFavoritesCountByMemberId($this->member_info['member_id']);
        $member_info['favorites_goods'] = Model('favorites')->getFavoritesCount($this->member_info['member_id']);
//        print_R($member_info);exit;
        // 交易提醒
        $model_order = Model('order');
        $member_info['order_nopay_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'NewCount');
        $member_info['order_noreceipt_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'SendCount');
        $member_info['order_notakes_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'TakesCount');
        $member_info['order_noeval_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'EvalCount');
        $member_info['member_type_name'] = $this->member_info['member_type_name'];
        $member_info['count_price'] = $member['available_predeposit'] + $member['freeze_margin'];//总资产
        $member_info['freeze_margin'] = $member['freeze_margin'];//保证金
        $member_info['available_predeposit'] = $member['available_predeposit'];//余额
        $member_info['member_points'] = $member['member_points'];//余额
        //$member_info['favourite_count'] = Model('favorites')->getFavoritesCount($this->member_info['member_id']);
        //$member_info['store_favourite_count'] = Model('favorites')->getStoreFavoritesCount($this->member_info['member_id']);
        // 售前退款
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_state'] = array('lt', 3);
        $member_info['return'] = Model('refund_return')->getRefundReturnCount($condition);
        $member_info['is_distri'] = $this->member_info['distri_state'] == 2 ? 1 : 0;
        $model_dis_goods = Model('dis_goods');
        $dis_goods_count = $model_dis_goods->getDistriGoodsCount(array('member_id' => $this->member_info['member_id']));//查看该会员分销商品数量
        if(C('live.open')){
            if($dis_goods_count > 0){
                $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
                $member_info['is_movie_msg'] = '';
            }else{
                $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
                $member_info['is_movie_msg'] = '您还没有选择分销商品，请先选择。';
            }
        }else{
            $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
            $member_info['is_movie_msg'] = '';
        }
        $member_info['count_partner'] = $this->member_info['distribute_lv_1'] + $this->member_info['distribute_lv_2'];
        $available_info = Model('member')->getMemberInfoByID($this->member_info['member_id'],'available_commis,freeze_commis');
        $member_info['available_commis'] = $available_info['available_commis'];

        //红包个数
        $member_info['rpt_num'] = Model('redpacket')->own_available_rpt($this->member_info['member_id']);

        $top_member = Model()->table('member_distribute')->field('top_member_name,dis_code')->where(array('member_id'=>$this->member_info['member_id']))->find();
        $service_hotline = Model()->table('setting')->where(array('name' => 'service_hotline'))->find();

        //获取 订单下的 代付款订单和待后货订单数量
        $newOrderCondition = [
            'buyer_id' => $memberId,
            'order_state' => ORDER_STATE_NEW
        ];
        $sendOrderCondition = [
            'buyer_id' => $memberId,
            'order_state' => ORDER_STATE_SEND
        ];
        //待付款订单数量
        $newOrderCount = Model('')->table('orders')->where($newOrderCondition)->count();
        //待收货订单数量
        $sendOrderCount = Model('')->table('orders')->where($sendOrderCondition)->count();
        $order = [
            'newOrderCount' => $newOrderCount,
            'sendOrderCount' => $sendOrderCount
        ];

        output_data(array(
            'member_info' => $member_info,
            'top_member'=>$top_member,
            'service_hotline' => $service_hotline,
            'order' => $order
        ));
    }
    
    /**
     * 我的资产
     */
    public function my_assetOp() {
        $param = $_GET;
        $fields_arr = array('point','predepoit','available_rc_balance','redpacket','voucher','available_commis');
        $fields_str = trim($param['fields']);
        if ($fields_str) {
            $fields_arr = explode(',',$fields_str);
        }
        $member_info = array();
        if (in_array('point',$fields_arr)) {
            $member_info['point'] = $this->member_info['member_points'];
        }
        if (in_array('predepoit',$fields_arr)) {
            $member_info['predepoit'] = $this->member_info['available_predeposit'];
        }
        if (in_array('available_rc_balance',$fields_arr)) {
            $member_info['available_rc_balance'] = $this->member_info['available_rc_balance'];
        }
        if (in_array('redpacket',$fields_arr)) {
            $member_info['redpacket'] = Model('redpacket')->getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        }
        if (in_array('voucher',$fields_arr)) {
            $member_info['voucher'] = Model('voucher')->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        }
        if (in_array('available_commis',$fields_arr)) {
            $member = Model('member')->getMemberInfoByID($this->member_info['member_id'],'available_commis,freeze_commis');
            $member_info['available_commis'] = $member['available_commis'];
        }
        output_data($member_info);
    }
    
    /**
     * 我的会员等级
     */
    public function member_gradeOp() {
        $gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
        $_info = array();
        $_info['level'] = $gradeinfo['level'];
        $_info['level_name'] = $gradeinfo['level_name'];
        output_data($_info);
    }

    /**
     * 首页店铺的收藏状态
     * @return [type] [description]
     */
    public function fav_store_listOp() {
        $store_ids = $_GET['store_ids'];
        if (empty($store_ids)) {
            output_error('参数错误');
        }
        $store_ids = explode(',',$store_ids);
        $store_ids = array_unique($store_ids);
        $model_fav = Model('favorites');
        $store_id_list = $model_fav->getStoreFavoritesList(array('fav_id' => array('in',$store_ids),'member_id' => $this->member_info['member_id']));
        $my_fav_stores = array();
        foreach ($store_id_list as $key => $value) {
            $my_fav_stores[] = $value['fav_id'];
        }
        if (empty($my_fav_stores)) {
            output_error('没有匹配到的店铺');
        }
        output_data($my_fav_stores);
    }


    //账户信息
    public function member_infoOp(){
        $model_member = Model('member');
        $model_pd = Model('predeposit');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        $recharge_amount = 0.00;
        $recharge_list = $model_pd->getPdRechargeList(
            array('pdr_member_id'=>$this->member_info['member_id'],'pdr_payment_state'=>1)
        );
        //output_data($recharge_list);
        if(!empty($recharge_list)){
            foreach($recharge_list as $k1=>$v1){
                $recharge_amount += $v1['pdr_amount'];
            }
        }
        $member_info['all_recharge'] = ncPriceFormat($recharge_amount);
        if($member_info['available_commis'] == ''){
            $member_info['available_commis'] = 0.00;
        }
        $member_info['all_commis'] =  $member_info['available_commis'];
        output_data($member_info);
    }

    /**
     * 申请佣金提现
     */
    public function commis_cash_addOp(){

            $pdc_amount = abs(floatval($_POST['money']));
            $model_pd = Model('predeposit');
            $model_member = Model('member');
            $member_info = $model_member->table('member')->where(array('member_id'=> $this->member_info['member_id']))->master(true)->lock(true)->find();;//锁定当前会员记录
            //验证支付密码
            if (md5($_POST['password']) != $member_info['member_paypwd']) {
                output_error('支付密码错误');
            }
            $cash_amount = 0.00;
            $cash_list = $model_pd->getPdCashList(array('pdc_member_id'=>$this->member_info['member_id'],'is_commis'=>1));
            if(!empty($cash_list)){
                foreach($cash_list as $k=>$v){
                    $cash_amount += $v['pdc_amount'];
                }
            }

            if($member_info['available_predeposit'] > ($member_info['available_commis']-$cash_amount)){
                $member_info['effect_commis'] = $member_info['available_commis']-$cash_amount;
            }else{
                $member_info['effect_commis'] = $member_info['available_predeposit'];
            }

            //验证金额是否足够
            if (floatval($member_info['effect_commis']) < $pdc_amount){
                output_error('佣金金额不足');
            }
            try {
                $model_pd->beginTransaction();
                $pdc_sn = $model_pd->makeSn();
                $data = array();
                $data['pdc_sn'] = $pdc_sn;
                $data['pdc_member_id'] = $this->member_info['member_id'];
                $data['pdc_member_name'] = $this->member_info['member_name'];
                $data['pdc_amount'] = $pdc_amount;
                $data['pdc_bank_name'] = $_POST['bank_name'];
                $data['pdc_bank_no'] = $_POST['card_number'];
                $data['pdc_bank_user'] = $_POST['bank_user'];
                $data['pdc_add_time'] = TIMESTAMP;
                $data['pdc_payment_state'] = 0;
                $data['is_commis'] = 1;
                $insert = $model_pd->addPdCash($data);
                if (!$insert) {
                    output_error('提现信息添加失败');
                }
                //冻结可用预存款
                $data = array();
                $data['member_id'] = $member_info['member_id'];
                $data['member_name'] = $member_info['member_name'];
                $data['amount'] = $pdc_amount;
                $data['order_sn'] = $pdc_sn;
                $model_pd->changePd('cash_apply_commis',$data);
                $model_pd->commit();
                output_data('1');
            } catch (Exception $e) {
                $model_pd->rollback();
                output_error($e->getMessage());
            }

    }

    /**
     * 申请余额提现
     */
    public function pd_cash_addOp(){

        $pdc_amount = abs(floatval($_POST['money']));
        //验证提现金额不小于100
        if ($pdc_amount < 100) {
            output_error('最低提现金额≥100元');
        }
        $model_pd = Model('predeposit');
        $model_member = Model('member');
        $member_info = $model_member->table('member')->where(array('member_id'=> $this->member_info['member_id']))->master(true)->lock(true)->find();;//锁定当前会员记录
        //验证支付密码
        if (md5($_POST['password']) != $member_info['member_paypwd']) {
            output_error('支付密码错误');
        }

        //验证金额是否足够
        if (floatval($member_info['available_predeposit']) < $pdc_amount){
            output_error('预存款金额不足');
        }

        $bank_card = $_POST['card_number'];
        $bankcard = Model('bankcard')->getbankCardInfo(['bank_card' => $bank_card]);
        if (empty($bankcard)) {
            output_error('没有找到此银行卡');
        }
        try {
            $model_pd->beginTransaction();
            $pdc_sn = $model_pd->makeSn();
            $data = array();
            $data['pdc_sn'] = $pdc_sn;
            $data['pdc_member_id'] = $this->member_info['member_id'];
            $data['pdc_member_name'] = $this->member_info['member_name'];
            $data['pdc_amount'] = $pdc_amount;
            $data['pdc_bank_name'] = $bankcard['bank_name'];
            $data['pdc_bank_no'] = $bank_card;
            $data['pdc_bank_user'] = $bankcard['true_name'];
            $data['pdc_add_time'] = TIMESTAMP;
            $data['pdc_payment_state'] = 0;
            $insert = $model_pd->addPdCash($data);
            if (!$insert) {
                output_error('提现信息添加失败');
            }
            //冻结可用预存款
            $data = array();
            $data['member_id'] = $member_info['member_id'];
            $data['member_name'] = $member_info['member_name'];
            $data['amount'] = $pdc_amount;
            $data['order_sn'] = $pdc_sn;
            $model_pd->changePd('cash_apply',$data);
            $model_pd->commit();
            output_data('1');
        } catch (Exception $e) {
            $model_pd->rollback();
            output_error($e->getMessage());
        }

    }

    /*
     * 获取保证金收益
     * */
    public function member_margin_infoOp()
    {
        $model_member = Model('member');
        $model_predeposit = Model('predeposit');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        $all_margin = $model_predeposit->get_cumulative_margin($this->member_info['member_id']);
        $marginModel = Model('margin_orders');
        $condition = [];
        $condition = [
            'buyer_id' => $this->member_info['member_id'],
            'order_state' => 1,
            'default_state' => 0,
            //'lock_state' => 0,
            'api_refund_state' => 0,
            'refund_state' => 0,
        ];
        $condition['__RAW'] = 'margin_amount <> bag_amount';
        $condition['__RAW'] = 'auction_id <> ' . C('newBieAuctionId');
        $freeMarginAmount = $marginModel->getMarginSum(
            $condition,
            'margin_amount');
        $data = array(
            //'available_margin' => number_format($member_info['available_margin']+$member_info['freeze_margin'], 2),
            'available_margin' => number_format($freeMarginAmount, 2),
            'switch_margin' => number_format($member_info['available_margin'], 2),
            //'freeze_margin' => $member_info['freeze_margin'],
            'freeze_margin' => number_format($freeMarginAmount, 2),
            'all_recharge' => number_format($all_margin, 2)
        );
        output_data($data);
    }

    /*
     * 获取会员保证金列表信息
     * */
    public function member_margin_listOp()
    {
//        //查询红包
//        $where = [
//            'dis_member_id'=>$this->member_info['member_id'],
//            'commission_type'=>6,
//            ];
//        if (isset($_GET['commission_time'])){
//            $month_day = date('t', strtotime($_GET['commission_time']));
//            $month_first = strtotime($_GET['commission_time'].'-01');
//            $month_last =  strtotime($_GET['commission_time'].'-'.$month_day.' 23:59:59');
//            $where['add_time'] = ['between',$month_first.','.$month_last];
//        }
//        if (strval($_GET['dis_commis_state']) !='' && in_array(strval($_GET['dis_commis_state']),[0,1])){
//            $where['dis_commis_state'] = intval($_GET['dis_commis_state']);
//        }
//        $field = "log_id,goods_name,add_time,commission_amount,dis_member_id,commission_time,order_sn,dis_commis_state";
//        $model_margin = Model('member_commission');
//        //$model_margin = new member_commissionModel();
//        $margin_list = $model_margin->getCommissionList($where, $field, $this->page, 'add_time desc');
//
//        $page_count = $model_margin->gettotalpage();
//        $new_data_list = [];
//        $cur_year = date("Y",time());
//        foreach ($margin_list as $value){
//            $month_key = date('m',$value['add_time']).'月';
//            $year_key = date('Y',$value['add_time']);
//            $month_key = ($cur_year == $year_key) ? $month_key : $year_key.'年'.$month_key;
//            $value['commission_time'] = date('Y.m.d',$value['add_time']);
//            $auction_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$value['order_sn']], 'auction_name');;
//            $value['auction_name'] = $auction_info['auction_name'];
//            $new_data_list[$month_key][] = $value;
//        }
//        output_data(array('margin_list' => $new_data_list), mobile_page($page_count));




        //-----------new
        /** @var margin_ordersModel $margin_model */
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $where = [
            'buyer_id'=>$this->member_info['member_id'],
        ];
        $fields = 'order_sn, auction_id, auction_image, auction_name, order_state, margin_amount';
        $margin_model = Model('margin_orders');
        $pageSize = $margin_model->page;
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        $margin_list = $margin_model->get_margin_list($where, $fields, $limit);
        $page_count = $margin_model->where($where)->count();
        //查询拍品状态
        if (!empty($margin_list)) {
            //$auction_ids = array_column($margin_list, 'auction_id');
            $fields = 'auction_id, goods_common_id, store_id, auction_name, auction_image, auction_start_time,
                 auction_end_time, auction_preview_start';
            foreach ($margin_list as &$value) {
                $condition = "auction_id = {$value['auction_id']}";
                $model_auctions = Model('auctions');
                $auction = $model_auctions
                    ->getAuctionList(
                        $condition,
                        $fields,
                        $group = '',
                        $order = 'auction_end_time asc',
                        $limit = 0
                    );
                if (empty($auction)) {
                    continue;
                }
                //output_data($auction);
                $value['auction_name'] = $auction[0]['auction_name'];
                $value['auction_image'] = $auction[0]['auction_image'];
                $value['auction_start_time'] = $auction[0]['auction_start_time'];
                $value['auction_end_time'] = $auction[0]['auction_end_time'];
                $value['auction_preview_start'] = $auction[0]['auction_preview_start'];
            }
        }
        output_data($margin_list, api_page($page_count, $page, $pageSize));
    }

    /*
 * 获取会员保证金列表信息
 * */
    public function member_margin_detailOp()
    {
        if (!intval($_GET['log_id'])){
            output_error('参数有误');
        }

        $field = "log_id,goods_name,goods_pay_amount,commission_amount,order_id,order_sn,dis_commis_state,commission_time";
        $where = ['log_id'=>intval($_GET['log_id'])];
        $member_commission = Model('member_commission');
        $commission_info = $member_commission->getCommissionInfo($where, $field);

        $commission_info['redemption_amount'] = ncPriceFormat($commission_info['goods_pay_amount'] + $commission_info['commission_amount']);

        //保证金订单
        $field = 'payment_code,created_at,payment_time,auction_id';
        $margin_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$commission_info['order_sn']], $field);
        $margin_info['add_time'] = date('Y.m.d H:i:s',$margin_info['created_at']);
        $margin_info['bond_start_data'] = date('Y.m.d',strtotime('+1 day',$margin_info['created_at']));
        $margin_info['payment_name'] = orderPaymentName($margin_info['payment_code']);

        //拍卖的相关信息
        $field = 'auction_end_time,auction_bond_rate,auction_name';
        $auction_info = Model('auctions')->getAuctionsInfo(['auction_id'=>$margin_info['auction_id']], $field);
        $auction_info['auction_bond_rate'] = $auction_info['auction_bond_rate']."%";
        $auction_info['bond_end_data'] = date('Y.m.d',$auction_info['auction_end_time']);
        $auction_info['pay_date_number'] = Logic('auction')->getInterestDay($margin_info['payment_time'],$auction_info['auction_end_time']);
        $auction_info['auction_end_time'] = date('Y.m.d', $auction_info['auction_end_time']);

        output_data(array_merge($commission_info,$margin_info,$auction_info));
    }
    /*
     * 保证金提现
     * */
    public function margin_cash_addOp()
    {
        $num = ncPriceFormat(floatval($_POST['money']));

        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($num <= 0) {
            output_error('提现金额异常');
        }
        if ($member_info['available_margin'] < $num) {
            output_error('提现金额大于余额');
        }

        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->margin_to_pd($num, $this->member_info['member_id'], $this->member_info['member_name']);
        if (!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 获取注册红包信息
     */
    public function get_register_awardOp(){
        $model_redpacket = Model('redpacket');
//        //更新红包过期状态
//        $model_redpacket->updateRedpacketExpire($_SESSION['member_id']);
        //查询红包
        $where = array();
        $where['rpacket_owner_id'] = $this->member_info['member_id'];
        $where['rpacket_state'] = 1;
        $list = $model_redpacket->getRedpacketList($where, '*', 0, 10, 'rpacket_active_date desc');
        output_data($list);
    }

    /**
     * 获取红包通知信息
     */
    public function get_noticeOp(){
        $list = Logic('notice')->get_redpacket_notice($this->member_info['member_id']);
        Model('redpacket_notice')->update_notice_status(array('member_id'=>$this->member_info['member_id']));
        output_data($list);
    }

}
