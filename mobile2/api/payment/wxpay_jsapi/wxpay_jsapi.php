<?php
/**
 * 微信支付接口类
 * JSAPI 适用于微信内置浏览器访问WAP时支付
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

/**
 * @todo
 */
class wxpay_jsapi
{
    const DEBUG = 0;

    protected $config;

    public function __construct()
    {
        $this->config = (object) array(
            'appId' => '',
            'appSecret' => '',
            'partnerId' => '',
            'apiKey' => '',

            'notifyUrl' => MOBILE_SITE_URL . '/api/payment/wxpay_jsapi/notify_url.php',
            //'finishedUrl' => WAP_SITE_URL . '/tmpl/member/pd_payment_result.html?_=2&attach=_attach_',
            //'undoneUrl' => WAP_SITE_URL . '/tmpl/member/pd_payment_result_failed.html?_=2&attach=_attach_',
            'finishedUrl' => AUCTION_URL.'/auctionCommodityDetails?type=success&auction_id=',
            'undoneUrl' => AUCTION_URL.'/auctionCommodityDetails?type=fail&auction_id=',

            'orderSn' => date('YmdHis'),
            'orderInfo' => 'Test wxpay js api',
            'orderFee' => 1,
            'orderAttach' => '_',
            'trade_type'=>'JSAPI',
            'scene_info'=>'{"h5_info":{"type":"Wap","wap_url":"http://test.yinuovip.me","wap_name":"支付"}}'
        );
    }

    public function setConfig($name, $value)
    {
        $this->config->$name = $value;
    }

    public function setConfigs(array $params)
    {
        foreach ($params as $name => $value) {
            $this->config->$name = $value;
        }
    }

    public function notify()
    {
        try {
            $data = $this->onNotify();
            $resultXml = $this->arrayToXml(array(
                'return_code' => 'SUCCESS',
            ));

            if (self::DEBUG) {
                file_put_contents(__DIR__ . '/log.txt', var_export($data, true), FILE_APPEND | LOCK_EX);
            }

        } catch (Exception $ex) {

            $data = null;
            $resultXml = $this->arrayToXml(array(
                'return_code' => 'FAIL',
                'return_msg' => $ex->getMessage(),
            ));

            if (self::DEBUG) {
                file_put_contents(__DIR__ . '/log_err.txt', $ex . PHP_EOL, FILE_APPEND | LOCK_EX);
            }

        }

        return array(
            $data,
            $resultXml,
        );
    }

    public function onNotify()
    {
        $d = $this->xmlToArray(file_get_contents('php://input'));

        if (empty($d)) {
            throw new Exception(__METHOD__);
        }

        if ($d['return_code'] != 'SUCCESS') {
            throw new Exception($d['return_msg']);
        }

        if ($d['result_code'] != 'SUCCESS') {
            throw new Exception("[{$d['err_code']}]{$d['err_code_des']}");
        }

        if (!$this->verify($d)) {
            throw new Exception("Invalid signature");
        }

        return $d;
    }

    public function verify(array $d)
    {
        if (empty($d['sign'])) {
            return false;
        }

        $sign = $d['sign'];
        unset($d['sign']);

        return $sign == $this->sign($d);
    }

    protected $control;

    public function getMpPaymentInfo($control = null)
    {
        $this->control = $control;
        $prepayId = $this->getPrepayId();

        $params = array();
        $params['appId'] = $this->config->appId;
        $params['timeStamp'] = '' . time();
        $params['nonceStr'] = md5(uniqid(mt_rand(), true));
        $params['package'] = 'prepay_id=' . $prepayId;
        $params['signType'] = 'MD5';
        $sign = $this->sign($params);
        $params['paySign'] = $sign;
        return json_encode($params);
    }

    /**
     * @param object $control !这个必须有getOpenId方法!
     * @return string
     * @throws Exception
     */
    public function paymentHtml($control = null)
    {
        $this->control = $control;
        $prepayId = $this->getPrepayId();
        
        $params = array();
        $params['appId'] = $this->config->appId;
        $params['timeStamp'] = '' . time();
        $params['nonceStr'] = md5(uniqid(mt_rand(), true));
        $params['package'] = 'prepay_id=' . $prepayId;
        $params['signType'] = 'MD5';
//        $params['limit_pay'] = $this->config->limit_pay;

        $sign = $this->sign($params);
        $params['paySign'] = $sign;
//        print_R($params);exit;
        // @todo timestamp
        $jsonParams = json_encode($params);

        return <<<EOB
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=utf-8" />
<title>微信安全支付</title>
</head>
<body>
正在加载…
<script type="text/javascript">
function jsApiCall() {
    WeixinJSBridge.invoke(
        'getBrandWCPayRequest',
        {$jsonParams},
        function(res) {
            var h;
            if (res && res.err_msg == "get_brand_wcpay_request:ok") {
                // success;
                h = '{$this->config->finishedUrl}';
            } else {
                // fail;
                //alert(res && res.err_msg);
                h = '{$this->config->undoneUrl}';
            }
            location.href = h.replace('_attach_', '{$this->config->orderAttach}');
        }
    );
}
window.onload = function() {
    if (typeof WeixinJSBridge == "undefined") {
        if (document.addEventListener) {
            document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
        } else if (document.attachEvent) {
            document.attachEvent('WeixinJSBridgeReady', jsApiCall);
            document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
        }
    } else {
        jsApiCall();
    }
}
</script>
</body>
</html>
EOB;
    }

    protected function getOpenId()
    {
        if ($c = $this->control) {
            $openId = $c->getOpenId();
            if ($openId) {
                return $openId;
            }

            // through multiple requests
            $openId = $this->getOpenIdThroughMultipleRequests();
            $c->setOpenId($openId);
            return $openId;
        }
        return $this->getOpenIdThroughMultipleRequests();
    }

    public function getPrepayId()
    {
        // ...
        $openId = $this->getOpenId();

        $data = array();
        $data['appid'] = $this->config->appId;
        $data['mch_id'] = $this->config->partnerId;
        $data['nonce_str'] = md5(uniqid(mt_rand(), true));
        $data['body'] = $this->config->orderInfo;
        $data['attach'] = $this->config->orderAttach;
        $data['out_trade_no'] = $this->config->orderSn;
        $data['total_fee'] = $this->config->orderFee;
        $data['spbill_create_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['notify_url'] = $this->config->notifyUrl;
        $data['trade_type'] = $this->config->trade_type;
        $data['limit_pay'] = $this->config->limit_pay;
        $data['openid'] = $openId;

        $sign = $this->sign($data);
        $data['sign'] = $sign;
        

        $result = $this->postXml('https://api.mch.weixin.qq.com/pay/unifiedorder', $data);
//        print_R($result);exit;
        if ($result['return_code'] != 'SUCCESS') {
            throw new Exception($result['return_msg']);
        }

        if ($result['result_code'] != 'SUCCESS') {
            throw new Exception("[{$result['err_code']}]{$result['err_code_des']}");
        }

        return $result['prepay_id'];
    }
    public function getMwebUrl($control = null){
        $this->control = $control;
        $money= $this->config->orderFee;                     //充值金额 微信支付单位为分
        $userip = $this->get_client_ip();     //获得用户设备 IP
        $appid  = wx9da3fc70e829b0d3;                  //应用 APPID
        $mch_id = 1484146532;                 //微信支付商户号
        $key    = $this->config->apiKey;                 //微信商户 API 密钥
        $out_trade_no = $this->config->orderSn;//平台内部订单号
        $nonce_str = $this->createNoncestr();//随机字符串
        $body = $this->config->orderInfo;//内容
        $total_fee = $this->config->orderFee; //金额
        $spbill_create_ip = $userip; //IP
        $notify_url = $this->config->notifyUrl; //回调地址
        $attach = $this->config->orderAttach;
        $trade_type = 'MWEB';//交易类型 具体看 API 里面有详细介绍
        $scene_info =$this->config->scene_info;//场景信息 必要参数
        $signA ="appid=$appid&attach=$attach&body=$body&mch_id=$mch_id&nonce_str=$nonce_str&notify_url=$notify_url&out_trade_no=$out_trade_no&scene_info=$scene_info&spbill_create_ip=$spbill_create_ip&total_fee=$total_fee&trade_type=$trade_type";
        $strSignTmp = $signA."&key=$key"; //拼接字符串  注意顺序微信有个测试网址 顺序按照他的来 直接点下面的校正测试 包括下面 XML  是否正确
        $sign = strtoupper(MD5($strSignTmp)); // MD5 后转换成大写
        $post_data = "<xml>
                            <appid>$appid</appid>
                            <mch_id>$mch_id</mch_id>
                            <body>$body</body>
                            <out_trade_no>$out_trade_no</out_trade_no>
                            <total_fee>$total_fee</total_fee>
                            <spbill_create_ip>$spbill_create_ip</spbill_create_ip>
                            <notify_url>$notify_url</notify_url>
                            <trade_type>$trade_type</trade_type>
                            <scene_info>$scene_info</scene_info>
                            <attach>$attach</attach>
                            <nonce_str>$nonce_str</nonce_str>
                            <sign>$sign</sign>
                    </xml>";//拼接成 XML 格式
        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";//微信传参地址
        $dataxml = $this->postXmlCurl($post_data,$url); //后台 POST 微信传参地址  同时取得微信返回的参数 
        $objectxml = (array)simplexml_load_string($dataxml, 'SimpleXMLElement', LIBXML_NOCDATA); //将微信返回的 XML 转换成数组
        return $objectxml;
    }
    public function postXmlCurl($xml,$url,$second = 30){
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        //设置 header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post 提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行 curl
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        }else{
            $error = curl_errno($ch);
            curl_close($ch);
            echo "curl 出错，错误码:$error"."<br>";
        }
    }
    public function createNoncestr( $length = 32 ){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }
    public function get_client_ip($type = 0) {
        $type       =  $type ? 1 : 0;
        $ip         =   'unknown';
        if ($ip !== 'unknown') return $ip[$type];
        if($_SERVER['HTTP_X_REAL_IP']){//nginx 代理模式下，获取客户端真实 IP
            $ip=$_SERVER['HTTP_X_REAL_IP'];
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {//客户端的 ip
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {//浏览当前页面的用户计算机的网关
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip     =   $_SERVER['REMOTE_ADDR'];//浏览当前页面的用户计算机的 ip 地址
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        // IP 地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }
    public function get_client_ip2(){
        $cip="unknown";
        if ($_SERVER['REMOTE_ADDR']) {
            $cip=$_SERVER['REMOTE_ADDR'];
        }elseif(getenv("ROMOTE_ADDR")){
            $cip=getenv("ROMOTE_ADDR");
        }
        return $cip;
    }
    public function getOpenIdThroughMultipleRequests()
    {
        if (empty($_GET['code'])) {
            if (isset($_GET['state']) && $_GET['state'] == 'redirected') {
                throw new Exception('Auth failed');
            }
            $url = sprintf(
                'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=redirected#wechat_redirect',
                $this->config->appId,
                urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])
            );
            header('Location: ' . $url);
            exit;
        }
		$rurl = sprintf(
            'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code',
            $this->config->appId,
            $this->config->appSecret,
            $_GET['code']
        );
        //$d = json_decode(file_get_contents($rurl), true);
		$res = $this->wxcurl_get($rurl);
		$d = json_decode($res, true);
        if (empty($d)) {
            throw new Exception(__METHOD__);
        }

        if (empty($d['errcode']) && isset($d['openid'])) {
            return $d['openid'];
        }

        throw new Exception(var_export($d, true));
    }

	public function wxcurl_get($url){
		$chs = curl_init();
        curl_setopt($chs, CURLOPT_TIMEOUT, 30);
        curl_setopt($chs, CURLOPT_URL, $url);
        curl_setopt($chs, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($chs, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($chs, CURLOPT_HEADER, FALSE);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($chs);
		if (!$response) {
            throw new Exception('CURL Error: ' . curl_errno($chs));
        }
        curl_close($chs);
		return $response;
	}

    public function sign(array $data)
    {
        ksort($data);

        $a = array();
        foreach ($data as $k => $v) {
            if ((string) $v === '') {
                continue;
            }
            $a[] = "{$k}={$v}";
        }

        $a = implode('&', $a);
        $a .= '&key=' . $this->config->apiKey;

        return strtoupper(md5($a));
    }

    public function postXml($url, array $data)
    {
        // pack xml
        $xml = $this->arrayToXml($data);

        // curl post
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $response = curl_exec($ch);
        if (!$response) {
            throw new Exception('CURL Error: ' . curl_errno($ch));
        }
        curl_close($ch);

        // unpack xml
        return $this->xmlToArray($response);
    }

    public function arrayToXml(array $data)
    {
        $xml = "<xml>";
        foreach ($data as $k => $v) {
            if (is_numeric($v)) {
                $xml .= "<{$k}>{$v}</{$k}>";
            } else {
                $xml .= "<{$k}><![CDATA[{$v}]]></{$k}>";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    public function xmlToArray($xml)
    {
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

}
