$(function(){
    //   首页导航条计算
    var ling = $(document).scrollTop();
    function home_navShow(){
        if(!is_home){
            return false;
        }
        $('.lm_home_nav .lm_all_category').addClass('lm_active');
        if(ling>190){
            $('.header-wrap').addClass('lm_fixed_nav');
            $('.site-logo').find('img').attr('src','/data/resource/image/lm/lm_fixed_logo.png');
            var html=$('.lm_home_nav .lm_content_ful>ul').html();
            $('.lm_fixed_nav_ul').html(html);
        }else{
            $('.header-wrap').removeClass('lm_fixed_nav');
            $('.site-logo').find('img').attr('src','/data/upload/shop/common/logo.png');
        }
    }
    home_navShow();
    $(window).scroll(function () {
        ling = $(document).scrollTop();
        home_navShow(ling);
    });
    if(typeof tb_line!="undefined"){
        $('.lm_home_nav').addClass('other_nav_box');
    }

    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    if (window.ActiveXObject) {
        Sys.ie = ua.match(/msie ([\d.]+)/)[1]
        if (Sys.ie <= 8) {
            alert('您目前的IE版本为' + Sys.ie + '版本太低，请您升级！');
            location.href = "http://windows.microsoft.com/zh-CN/internet-explorer/downloads/ie";
        }
    }

});