/**
 * Created by lm on 2017/5/6.
 */
//jquery.ping.js
(function ($) {
    "use strict";
    $.fn.pin = function (options) {
        var scrollY = 0, elements = [], disabled = false, $window = $(window);
        options = options || {};

        var recalculateLimits = function () {
            for (var i = 0, len = elements.length; i < len; i++) {
                var $this = elements[i];

                if (options.minWidth && $window.width() <= options.minWidth) {
                    if ($this.parent().is(".pin-wrapper")) {
                        $this.unwrap();
                    }
                    $this.css({width: "", left: "", top: "", position: ""});
                    if (options.activeClass) {
                        $this.removeClass(options.activeClass);
                    }
                    disabled = true;
                    continue;
                } else {
                    disabled = false;
                }

                var $container = options.containerSelector ? $this.closest(options.containerSelector) : $(document.body);
                var offset = $this.offset();
                var containerOffset = $container.offset();
                var parentOffset = $this.offsetParent().offset();

                if (!$this.parent().is(".pin-wrapper")) {
                    $this.wrap("<div class='pin-wrapper'>");
                }

                var pad = $.extend({
                    top: 0,
                    bottom: 0
                }, options.padding || {});

                $this.data("pin", {
                    pad: pad,
                    from: (options.containerSelector ? containerOffset.top : offset.top) - pad.top,
                    to: containerOffset.top + $container.height() - $this.outerHeight() - pad.bottom,
                    end: containerOffset.top + $container.height(),
                    parentTop: parentOffset.top
                });

                $this.css({width: $this.outerWidth()});
                $this.parent().css("height", $this.outerHeight());
            }
        };

        var onScroll = function () {
            if (disabled) {
                return;
            }

            scrollY = $window.scrollTop();

            var elmts = [];
            for (var i = 0, len = elements.length; i < len; i++) {
                var $this = $(elements[i]),
                    data = $this.data("pin");

                if (!data) { // Removed element
                    continue;
                }

                elmts.push($this);

                var from = data.from - data.pad.bottom,
                    to = data.to - data.pad.top;

                if (from + $this.outerHeight() > data.end) {
                    $this.css('position', '');
                    continue;
                }

                if (from < scrollY && to > scrollY) {
                    !($this.css("position") == "fixed") && $this.css({
                        left: $this.offset().left,
                        top: data.pad.top
                    }).css("position", "fixed");
                    if (options.activeClass) {
                        $this.addClass(options.activeClass);
                    }
                } else if (scrollY >= to) {
                    $this.css({
                        left: "",
                        top: to - data.parentTop + data.pad.top
                    }).css("position", "absolute");
                    if (options.activeClass) {
                        $this.addClass(options.activeClass);
                    }
                } else {
                    $this.css({position: "", top: "", left: ""});
                    if (options.activeClass) {
                        $this.removeClass(options.activeClass);
                    }
                }
            }
            elements = elmts;
        };

        var update = function () {
            recalculateLimits();
            onScroll();
        };

        this.each(function () {
            var $this = $(this),
                data = $(this).data('pin') || {};

            if (data && data.update) {
                return;
            }
            elements.push($this);
            $("img", this).one("load", recalculateLimits);
            data.update = update;
            $(this).data('pin', data);
        });

        $window.scroll(onScroll);
        $window.resize(function () {
            recalculateLimits();
        });
        recalculateLimits();

        $window.load(update);

        return this;
    };
})(jQuery);

//首页第二块区域商城拍卖所用的js
$(function ($) {
    $('.lm_home_mod2_left').hide();
    // $(".lm_home_mod2_left").pin({
    //     containerSelector: "#lm_pin_container",
    //     padding: {top: 95, bottom: 40},
    //     minWidth: 1330
    // });
    if(!$('.lm_product_list').length){
        return false;
    }
    var lmHomeLeft=$('.lm_product_list').offset().left;
    var lmHomeLeft_de=lmHomeLeft-96;
    if(lmHomeLeft_de<10){
        $('.lm_home_mod2_left').hide();
    }
    $('.lm_home_mod2_left').css({
        "position":"fixed",
        "left":lmHomeLeft_de+"px",
        "top":"95px"
    });

    $(window).scroll(function () {
        var ling = $(document).scrollTop();
        var s1 = $('.lm_home_mod2_one').offset().top -140,
            s2 = $('.lm_home_mod2_two').offset().top - 140,
            s3 = $('.lm_home_mod2_three').offset().top - 160,
            other = $('.lm_home_mod3').offset().top-200;
        if (s2 > ling && ling > s1) {
            $('.lm_home_mod2_left li').removeClass('active').eq(0).addClass('active');
            $('.lm_home_mod2_left').show();
        }
        else if (s2 < ling && ling < s3) {
            $('.lm_home_mod2_left li').removeClass('active').eq(1).addClass('active');
            $('.lm_home_mod2_left').show();
        }
        else if (s3 < ling && ling < other) {
            $('.lm_home_mod2_left li').removeClass('active').eq(2).addClass('active');
            $('.lm_home_mod2_left').show();
        }else{
            $('.lm_home_mod2_left').hide();
        }
    });
    $('.lm_home_mod2_left li').on('click', function () {
        var index = $(this).index();
        if (index === 0) {
            $('html,body').animate({
                scrollTop: $('.lm_home_mod2_one').offset().top-70
            });
        } else if (index === 1) {
            $('html,body').animate({
                scrollTop: $('.lm_home_mod2_two').offset().top-70
            });
        } else {
            $('html,body').animate({
                scrollTop: $('.lm_home_mod2_three').offset().top-70
            });
        }
    })

});

//首页底部轮滑切换
$(function ($) {
    $.fn.indexSilder = function (opts) {
        var $dom = $(opts),
            $ul = $dom.find('ul'),
            $li = $ul.find('li'),
            $right = $dom.parent('.lm_pub_lag').find('.lm_next'),
            liWidth = $li.outerWidth(),
            marR = 7,
            num = 0,
            count = $li.size(),
            zj = 5,
            val,
            windowWidth = $(window).width();
        $(window).on('load resize', function () {
            windowWidth = $(this).width();
        });

        if (!$dom.length || count <= 5) {
            return false;
        }



        // watch();
        function main(val) {
            if (num > count - 4) {
                num = 0;
                val = 0;
            }
            if (num < 0) {
                num = zj;
                val = -(liWidth + marR) * num;
            }
            $ul.css({
                'margin-left': val
            });
            moveis(num)
        }

        // main();

        function next() {
            val = (liWidth + marR) * (num + 1);
            num++;
            main(-val);
        }

        $right.on('click', function () {
            next();
        });


        function moveis(num) {
            $li.hover(function () {
                if ($(this).index() === num + 4) {
                    $dom.width(720);
                }
            }, function () {
                if ($(this).index() === num + 4) {
                    $dom.width(602);
                }
            });
        }

        moveis(num);


        //首页名家专栏模块的tab切换
        $('.lm_home_pub .lm_home_left li').eq(0).addClass('active');
        $('.lm_home_pub .lm_home_center_tab').eq(0).show();
        $('.lm_home_pub .lm_home_left li').on('click', function () {
            num = 0;
            main(num);
            var $dom = $('.lm_home_pub .lm_home_center'),
                $title = $('.lm_home_pub .lm_home_left li'),
                $tabcon = $dom.find('.lm_home_center_tab'),
                index = $(this).index();

            $title.removeClass('active');
            $tabcon.hide()
            $(this).addClass('active');
            $tabcon.eq(index).show();
        });
    };
    jQuery.indexSilder_ready = function () {
        var locDate = Date.parse(new Date());
        $('.lm_pub_lag').each(function (i, e) {
            $(this).find('.pub_lag_con').addClass('rankCon_tab_con' + locDate + i);
            $(document).indexSilder('.rankCon_tab_con' + locDate + i);
        });
    };
    $.indexSilder_ready();
    // $(document).indexSilder('.lm_pub_lag');

//    首页多图切换计算

        $.fn.indexSilder1 = function (opts) {
            var $dom = $(opts),
                $ul = $dom.find('.lm_home_double_wrapper'),
                $li = $ul.find('ul'),
                $right = $dom.parent('.lm_home_double').find('.lm_next'),
                ulWidth = $li.outerWidth(),
                num = 0,
                count = $li.size(),
                val,
                windowWidth = $(window).width();
            $(window).on('load resize', function () {
                windowWidth = $(this).width();
            });

            if (!$dom.length || count < 2) {
                return false;
            }



            // watch();
            function main(val) {
                if (num > count - 1) {
                    num = 0;
                    val = 0;
                }
                if (num < 0) {
                    num = count;
                    val = -ulWidth * num;
                }
                $ul.css({
                    'margin-left': val
                });
                $li.css({'opacity':'0'});
                $ul.find('ul').eq(num).css({'opacity':'1'});

            }

            // main();

            function next() {
                val = ulWidth * (num + 1);
                num++;
                main(-val);
            }



            $right.on('click', function () {
                next();
            });
        };
        jQuery.indexSilder_ready1 = function () {
            var locDate = Date.parse(new Date());
            $('.lm_home_double').each(function (i, e) {
                $(this).find('.lm_home_double_con').addClass('rankCon_tab_con1' + locDate + i);
                $(document).indexSilder1('.rankCon_tab_con1' + locDate + i);
            });
        };
        $.indexSilder_ready1();
        // $(document).indexSilder1('.lm_pub_lag');

//    首页倒计时插件
//     $('.lm_countdown').each(function(i,e){
//         var date=parseInt($(this).attr('data-date')),
//             three,two,one,
//             $data=$(this).find('.lm_date');
//             // $three=$(this).find('.lm_three'),
//             // $two=$(this).find('.lm_two'),
//             // $one=$(this).find('.lm_one');
//          //day
//         //一天等于86400s
//         if(date>86400){
//             //时间戳如果是s的时候不是s则多除1000
//             var displayTime;
//             if (date == -1) {
//                 clearInterval(displayTime);
//                 return;
//             }
//             function showTime(){
//                 date-= 1;
//                 three=Math.floor(date/60/60/24);
//                 two=Math.floor(date/60/60%24);
//                 one=Math.floor(date/60%60);
//                 var html='<div class="lm_three lm_date_info">'+three+'</div>\
//                     <span>天</span>\
//                     <div class="lm_two   lm_date_info">'+two+'</div>\
//                     <span>时</span>\
//                     <div class="lm_one   lm_date_info">'+one+'</div>\
//                     <span>分</span>'
//                 $data.html(html);
//                 // $three.text(three);
//                 // $two.text(two);
//                 // $one.text(one);
//             }
//             displayTime = setInterval(function(){
//                 showTime();
//             }, 1000);
//             // $three.next('span').text('天');
//             // $two.next('span').text('时');
//             // $one.next('span').text('分');
//         }else if(date<86400){
//             var displayTime;
//             displayTime = setInterval(function(){
//                 showTime();
//             }, 1000);
//
//             function showTime(){
//                 if (date <= 0) {
//                     clearInterval(displayTime);
//                     return;
//                 }
//                 date-= 1;
//                 three=Math.floor(date/60/60%24);
//                 two=Math.floor(date/60%60);
//                 one=Math.floor(date%60);
//                 // $three.text(three);
//                 // $two.text(two);
//                 // $one.text(one);
//                 var html='<div class="lm_three lm_date_info">'+three+'</div>\
//                     <span>时</span>\
//                     <div class="lm_two   lm_date_info">'+two+'</div>\
//                     <span>分</span>\
//                     <div class="lm_one   lm_date_info">'+one+'</div>\
//                     <span>秒</span>'
//                 $data.html(html);
//             }
//
//             // $three.next('span').text('时');
//             // $two.next('span').text('分');
//             // $one.next('span').text('秒');
//
//         }
//     });


    function lmCuteDown() {
        $('.lm_countdown').each(function (i, e) {
            var date = parseInt($(this).attr('data-date')),
                three, two, one, sec,
                $data = $(this).find('.lm_date');
            function showTime(type) {
                if (date <= 0) {
                    clearInterval(displayTime);
                    return;
                }
                date -= 1;
                if (arguments.length === 2 && arguments[0].length === 3) {
                    if (arguments[1] === 1) {
                        three = Math.floor(date / 60 / 60 / 24);
                        two = Math.floor(date / 60 / 60 % 24);
                        one = Math.floor(date / 60 % 60);
                    } else {
                        three = Math.floor(date / 60 / 60 % 24);
                        two = Math.floor(date / 60 % 60);
                        one = Math.floor(date % 60);
                    }
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>' + arguments[0][0] + '</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>' + arguments[0][1] + '</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>' + arguments[0][2] + '</span>';
                } else {
                    three = Math.floor(date / 60 / 60 / 24);
                    two = Math.floor(date / 60 / 60 % 24);
                    one = Math.floor(date / 60 % 60);
                    sec = Math.floor(date % 60);
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    if (sec < 10) {
                        sec = '0' + sec
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>天</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>时</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>分</span>\
                <div class="lm_zeo   lm_date_info">' + sec + '</div><span>秒</span>';
                }
                $data.html(html);
                // console.log(three);
                // console.log(two);
                // console.log(one);
                if(three == 0 && two == 0 && one == 0){
                    // console.log(2222);
                    location.reload();
                }
            }

            if ($(this).attr('data-type') != 3) {
                if (date > 86400) {
                    //时间戳如果是s的时候不是s则多除1000
                    var displayTime;
                    if (date == -1) {
                        clearInterval(displayTime);
                        return;
                    }
                    displayTime = setInterval(function () {
                        showTime(['天', '时', '分'], 1);
                    }, 1000);
                } else if (date < 86400) {
                    var displayTime;
                    displayTime = setInterval(function () {
                        showTime(['时', '分', '秒'], 2);
                    }, 1000);
                }
            }else{
                var displayTime;
                displayTime = setInterval(function () {
                    showTime(['天', '时', '分', '秒'], 1);
                }, 1000);
            }
        });
    }

    lmCuteDown();
});

