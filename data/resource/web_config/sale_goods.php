<?php defined('InShopNC') or exit('Access Invalid!');?>
  <?php $hot_sale = current($output['code_sale_list']['code_info']);?>
  <?php if(!empty($hot_sale)): ?>
  <ul>
<?php foreach ($hot_sale['goods_list'] as $k => $v): ?>
    <li>
      <a href="<?php echo urlShop('goods','index',array('goods_id'=>$v['goods_id'])); ?>" title="<?php echo $v['goods_name']; ?>">
        <img src="<?php echo strpos($v['goods_pic'],'http')===0 ? $v['goods_pic']:UPLOAD_SITE_URL."/".$v['goods_pic'];?>"  alt="<?php echo $v['goods_name']; ?>"/>
        <div class="title"><?php echo $v['goods_name']; ?></div>
        <div class="info"><span class="fl">当前价&nbsp;<b>￥<?php echo ncPriceFormatForList($v['goods_price']); ?></b></span><span class="fr"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;<b>20</b>&nbsp;次</span></div>
    </a>
    </li>
<?php endforeach;?>
  </ul>
<?php endif; ?>
