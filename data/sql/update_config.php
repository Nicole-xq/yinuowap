<?php

//特定专区店铺(根据环境自己定义)    2017-06-15
$config['store']['zq_id'] = 85;

//系统配置拍卖延时周期(5分钟)
$config['auction_delayed_time'] = 300;

//系统配置拍卖结束前4分钟内出价需要延迟
$config['auction_limit_time'] = 240;//4*60

//系统配置趣猜中奖后72小时可以购买
$config['guess_add_cart_limit_time'] = 259200;//3600*24*3

//尊享专区店铺ID
$config['yinuo_store_id'] = 85;

//尊享专区店铺折扣比例
$config['yinuo_store_discount'] = 0.3;

//银行卡归属行查询  2017-10-18
$config['bank_info']['appcode']='3af1d58913a141978d3191884a5063f9';

?>