#修改店铺分类价格优惠字段
ALTER TABLE `shopnc_store_class`
ADD COLUMN `sc_sale`  mediumint(8) NOT NULL COMMENT '保证金优惠' AFTER `sc_bail`,
ADD COLUMN `sc_service_price`  mediumint(8) NOT NULL COMMENT '平台服务费'  AFTER `sc_sale`,
ADD COLUMN `sc_service_price_sale`  mediumint(8) NOT NULL COMMENT '平台服务费优惠'  AFTER `sc_service_price`;


#修改入驻加入
ALTER TABLE `shopnc_store_joinin`
ADD COLUMN `paying_memo`  varchar(100) COMMENT '付款金额备注' AFTER `paying_amount`;



#修改资质证明照片上传可以多张
ALTER TABLE `shopnc_store_joinin`
MODIFY COLUMN `other_qualifications_elc`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '其他资质' AFTER `member_certificate_elc`;


#修改商户编号生成记录表
CREATE TABLE `shopnc_store_card_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `f_num` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '首字母类型',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '入驻类型 0商家 1艺术家',
  `date` varchar(10) NOT NULL COMMENT '日期',
  `l_num` int(3) NOT NULL DEFAULT '1' COMMENT '最后序号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`f_num`,`type`,`date`) USING BTREE COMMENT '日期不重复索引'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='商户编号记录表';

#修改商户编号生成记录表
ALTER TABLE `shopnc_store_joinin`
ADD COLUMN `record_number`  varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '入驻标号' AFTER `member_name`,
ADD UNIQUE INDEX `record_number_unique` (`record_number`) USING BTREE COMMENT '编号唯一';

#修改商户编号生成记录表
ALTER TABLE `shopnc_store`
ADD COLUMN `record_number`  varchar(13) NULL COMMENT '入驻标号' AFTER `store_name`,
ADD UNIQUE INDEX `record_number_unique` (`record_number`) USING HASH COMMENT '编号唯一';



#金融板块修改
ALTER TABLE `shopnc_member`
ADD COLUMN `td_amount`  decimal(12,2) NULL DEFAULT 0 COMMENT '购买特定专区商品金额' AFTER `freeze_commis`;

UPDATE `shopnc_member` set freeze_commis = 0.00 where freeze_commis is NULL;
ALTER TABLE `shopnc_member`
MODIFY COLUMN `freeze_commis`  decimal(12,2) NOT NULL DEFAULT 0 COMMENT '待返佣金' AFTER `available_commis`;


CREATE TABLE `shopnc_member_distribute_type` (
  `id` int(8) NOT NULL COMMENT '主键id',
  `type_id` int(2) NOT NULL COMMENT '分销类型id',
  `name` varchar(20) NOT NULL COMMENT '名字',
  `price` decimal(10,2) DEFAULT NULL COMMENT '特定专区升级金额',
  `wdl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '微代理分销百分比',
  `zl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '战略合伙人百分比',
  `sy_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '事业合伙人百分比',
  `artist_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '艺术家百分比',
  `goods_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '购买艺术品百分比',
  `bzj_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '拍卖保证金百分比',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` int(10) DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shopnc_member_distribute_type
-- ----------------------------
INSERT INTO `shopnc_member_distribute_type` VALUES ('1', '1', '微代理', '5000.00', '50.00', '10.00', '10.00', '1.00', '2.00', '0.20', null, null);
INSERT INTO `shopnc_member_distribute_type` VALUES ('2', '2', '战略合伙人', '100000.00', '50.00', '0.00', '0.00', '10.00', '0.00', '0.20', null, null);
INSERT INTO `shopnc_member_distribute_type` VALUES ('3', '3', '事业合伙人', '1000000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.20', null, null);
INSERT INTO `shopnc_member_distribute_type` VALUES ('4', '4', '艺术家', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.20', null, null);
INSERT INTO `shopnc_member_distribute_type` VALUES ('5', '0', '会员', '0.00', '50.00', '10.00', '10.00', '0.00', '2.00', '0.20', null, null);


### 2017-05-24
#会员表代收佣金字段更新
ALTER TABLE `shopnc_member`
MODIFY COLUMN `available_commis`  decimal(12,2) NULL DEFAULT 0 COMMENT '已返佣金' AFTER `bind_time`;

UPDATE `shopnc_member` set available_commis = 0 where available_commis is NULL;

DROP TABLE IF EXISTS `shopnc_artist`;
CREATE TABLE `shopnc_artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL COMMENT '用户id',
  `artist_name` varchar(255) NOT NULL COMMENT '艺术家名称',
  `artist_job_title` varchar(255) NOT NULL COMMENT '艺术家职称',
  `artist_resume` text NOT NULL COMMENT '艺术家简历',
  `artist_represent` text COMMENT '艺术家年代表',
  `artist_awards` text COMMENT '艺术家获奖情况',
  `artist_works` text COMMENT '艺术家获奖作品',
  `artist_classify` int(10) unsigned DEFAULT NULL COMMENT '艺术家分类',
  `artist_grade` int(10) unsigned DEFAULT NULL COMMENT '艺术家等级',
  `is_rec` int(10) unsigned DEFAULT '0' COMMENT '是否推荐',
  `artist_sort` int(10) unsigned DEFAULT '0' COMMENT '排序 值越小越靠前',
  `artist_image` varchar(255) DEFAULT NULL COMMENT '艺术家主图',
  `artist_click` int(10) unsigned DEFAULT '0' COMMENT '点击量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='艺术家表';


ALTER TABLE `shopnc_member`
ADD COLUMN `is_artist`  tinyint(1) NULL DEFAULT 0 COMMENT '是否是艺术家 0:否   1:是' AFTER `freeze_commis`;

update shopnc_member set member_type = 0,is_artist = 1 where member_type = 4;


###2017-05-27
#更新专场表,添加专场统一保证金利率
ALTER TABLE `shopnc_auction_special`
ADD COLUMN `special_rate`  decimal(10,2) NULL DEFAULT 0.00 COMMENT '专场统一利率' AFTER `is_rec`,
ADD COLUMN `special_rate_time`  INT (11) DEFAULT NULL COMMENT '计息时间(超过此时间,将不再获得保证金收益)' AFTER `special_rate`;

###2017-06-1
#更新用户表,添加粉丝ID
ALTER TABLE `shopnc_member`
ADD COLUMN `weixin_open_id`  varchar(50) NOT NULL COMMENT '微信OP_ID'  AFTER `weixin_info`;

###2017-06-1
#返佣状态定义
ALTER TABLE `shopnc_member_commission`
MODIFY COLUMN `commission_type`  tinyint(1) NULL DEFAULT 1 COMMENT '返佣来源类型 1普通购物  2拍卖  3 保证金返佣 4 会员升级 5 开店铺' AFTER `goods_commission`;

###2017-06-2
#返佣金额默认值设定
ALTER TABLE `shopnc_member_commission`
MODIFY COLUMN `commission_amount`  decimal(12,2) NULL DEFAULT 0 COMMENT '返佣金额' AFTER `from_member_id`;

###2017-06-12 author:HJH
#更新红包表,添加红包类型,适用店铺
ALTER TABLE `shopnc_redpacket_template`
ADD COLUMN  `rpacket_t_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '红包类型(1-全平台,2-指定店铺)',
ADD COLUMN  `rpacket_t_store_id` int(10) NOT NULL COMMENT '店铺ID',
ADD COLUMN  `rpacket_t_wap_link` varchar(100) DEFAULT '' COMMENT 'WAP链接',
ADD COLUMN  `rpacket_t_pc_link` varchar(100) DEFAULT '' COMMENT 'PC链接';

###2017-06-13
#注册红包相关sql
#添加领取注册红包状态
ALTER TABLE `shopnc_member`
ADD COLUMN `register_award`  tinyint(1) NULL DEFAULT 0 COMMENT '是否领取过注册奖励' AFTER `td_amount`;
#新建注册送红包表
DROP TABLE if EXISTS shopnc_register_activity;
CREATE TABLE `shopnc_register_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int (11) DEFAULT 0 COMMENT '赠送类型,0:红包',
  `code` int (11) DEFAULT NULL COMMENT '奖励内容id(对应奖励类型的id)',
  `start_time` int(11) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '活动结束时间',
  `status` varchar(255) DEFAULT '1' COMMENT '活动状态 0:关闭 1:开启',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='注册活动表';


###2017-06-14
#记录保证金计息天数
ALTER TABLE `shopnc_margin_orders`
ADD COLUMN `pay_date_number`  int(4) NULL DEFAULT 0 COMMENT '保证金计息天数' AFTER `payment_time`;

#注释
ALTER TABLE `shopnc_member_distribute_type`
COMMENT='分销配置表';
ALTER TABLE `shopnc_auctions_images`
COMMENT='拍卖商品图片表';

###2017-06-16
#分销下级添加备注字段
ALTER TABLE `shopnc_member_distribute`
ADD COLUMN `member_comment`  varchar(255) NULL COMMENT '用户备注(上级用户对下级用户的备注)' AFTER `member_name`;

###2017-06-21
#分配推荐表
CREATE TABLE `shopnc_store_recommend_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL COMMENT '分类id',
  `category_name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `img` varchar(255) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='特殊店铺(尊享专区)推荐分类配置表';

###2017-06-22
#红包通知表
CREATE TABLE `shopnc_redpacket_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL COMMENT '用户id',
  `redpacket_id` int(11) DEFAULT NULL COMMENT '用户红包id',
  `status` int(11) DEFAULT '0' COMMENT '状态 0:未读,1:已读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='红包通知';

###2017-06-22
#推荐分类配置表新增字段(状态)
ALTER TABLE `shopnc_store_recommend_category`
ADD COLUMN `status`  int(11) NULL DEFAULT 0 COMMENT '状态  0:有效 1:无效' AFTER `img`;

###2017-07-1
#用户表头像字段扩容
ALTER TABLE `shopnc_member`
MODIFY COLUMN `member_avatar` varchar(200) DEFAULT NULL COMMENT '会员头像';


###2017-07-11
#分销返佣比例明细表
CREATE TABLE `shopnc_member_distribute_detail` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `dis_id` int(8) NOT NULL COMMENT '表：distribute_type主键ID',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '层级1：一级；2：二级',
  `type_id` int(2) NOT NULL COMMENT '分销类型id',
  `wdl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '微代理分销百分比',
  `zl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '战略合伙人百分比',
  `sy_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '事业合伙人百分比',
  `artist_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '艺术家百分比',
  `goods_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '购买艺术品百分比',
  `bzj_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '拍卖保证金百分比',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`dis_id`,`level`,`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销等级配置明细表';

###2017-07-11
#分销等级配置表删除相关配置信息——移植到明细中去
ALTER TABLE shopnc_member_distribute_type
DROP COLUMN wdl_num,
DROP COLUMN zl_num,
DROP COLUMN sy_num,
DROP COLUMN artist_num,
DROP COLUMN goods_num,
DROP COLUMN bzj_num;
###2017-07-13
#三级分佣,新增字段
ALTER TABLE `shopnc_member_distribute`
ADD COLUMN `top_member_2`  int(11) NULL DEFAULT 0 COMMENT '上级的上级会员编号' AFTER `top_member`;

###2017-07-14
#三级分销,新增字段
ALTER TABLE `shopnc_member_commission`
ADD COLUMN `top_lv`  int(11) NULL DEFAULT 1 COMMENT '上级级别(1:上级,2:上级的上级..对应三级分销)' AFTER `order_sn`;
##老数据top_member_2处理
UPDATE shopnc_member_distribute AS t
 INNER JOIN shopnc_member_distribute AS s ON t.top_member = s.member_id
 SET t.top_member_2 = s.top_member;


###2017-07-15
#用户来源记录,新增字段
ALTER TABLE `shopnc_member`
ADD COLUMN `source`  int(11) NULL DEFAULT 0 COMMENT '用户来源  1,张雄艺术' AFTER `register_award`;

###2017-07-25
#商品卖点链接
ALTER TABLE `shopnc_goods_common`
ADD COLUMN `goods_jingle_pc_link` varchar(100) DEFAULT '' COMMENT '商品广告词PC链接',
ADD COLUMN `goods_jingle_wap_link` varchar(100) DEFAULT '' COMMENT '商品广告词WAP链接';

###2017-07-25
#商品卖点链接
ALTER TABLE `shopnc_goods`
ADD COLUMN `goods_jingle_pc_link` varchar(100) DEFAULT '' COMMENT '商品广告词PC链接',
ADD COLUMN `goods_jingle_wap_link` varchar(100) DEFAULT '' COMMENT '商品广告词WAP链接';

###2017-08-01
#趣猜短信变更
UPDATE shopnc_member_msg_tpl SET `mmt_short_content` = '【{$site_name}】您参与的趣猜活动已中奖，请在 {$end_time} 前下单，过期将不可领取，点击会员中心领奖http://t.cn/R9cou2E' WHERE mmt_code = 'guess_notice';

###2017-08-28
#机器人表增加拍品id字段
ALTER TABLE `shopnc_robot`
ADD COLUMN `auction_id`  int(11) NULL DEFAULT 0 COMMENT '当前绑定的拍品id' AFTER `description`;

###2017-09-20
#引导页设置表
CREATE TABLE `shopnc_mb_boot_page` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(50) NOT NULL COMMENT '引导标题',
  `boot_image` varchar(100) NOT NULL DEFAULT '' COMMENT '引导图片',
  `wap_link` varchar(100) DEFAULT '' COMMENT '跳转链接',
  `click_num` int(10) unsigned DEFAULT '0' COMMENT '点击数',
  `thumbs_up_num` int(10) unsigned DEFAULT '0' COMMENT '点赞数',
  `share_num` int(10) unsigned DEFAULT '0' COMMENT '分享数',
  `share_state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '分享状态: 1关闭2开启',
  `boot_state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '引导状态: 1关闭2开启',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WAP引导页设置表';
###2017-09-14
#商品公共内容表增添sort字段
ALTER TABLE `shopnc_goods`
ADD COLUMN `sort`  smallint(5) NULL DEFAULT 10000 COMMENT '商品排序' AFTER `is_add_auctions`;

###2017-09-14
#商品表增添sort字段
ALTER TABLE `shopnc_goods_common`
ADD COLUMN `sort`  smallint(5) NULL DEFAULT 10000 COMMENT '商品排序' AFTER `goods_jingle_wap_link`;

###2017-09-12
#专题-新增字段(手机端排版)
ALTER TABLE `shopnc_cms_special`
ADD COLUMN `special_mobile_content`  text NULL COMMENT '专题内容(手机排版)' AFTER `special_content`;
ALTER TABLE `shopnc_cms_special`
ADD COLUMN `special_share_logo`  varchar(255) NULL COMMENT '微信分享logo' AFTER `special_mobile_content`;
ALTER TABLE `shopnc_cms_special`
ADD COLUMN `special_description`  text NULL COMMENT '描述(分享用)' AFTER `special_title`;
ALTER TABLE `shopnc_cms_special`
ADD COLUMN `special_add_time` int(10) DEFAULT NULL COMMENT '专题添加时间'  AFTER `special_type`;

###2017-10-12
#银行卡绑定表增添bank_image字段
ALTER TABLE `shopnc_bankcard`
ADD COLUMN `bank_image`  varchar(255) NULL DEFAULT NULL COMMENT '银行logo' AFTER `bank_name`;

###2017-09-25
#消息通知-新增内容
INSERT INTO shopnc_member_msg_tpl(`mmt_code`,`mmt_name`,`mmt_message_switch`,`mmt_message_content`,`mmt_short_switch`,`mmt_short_content`,`mmt_mail_switch`,`mmt_mail_subject`,`mmt_mail_content`)
value('partner_notice','新增合伙人提醒',1,'您有新的合伙人加入啦！',0,'【{$site_name}】您有新的合伙人加入啦！',0,'{$site_name}提醒：您有新的合伙人加入啦！','<p>{$site_name}提醒：</p><p>您有新的合伙人加入啦！</p><p style="text-align:right;">{$site_name}</p><p style="text-align:right;">{$mail_send_time}</p>');

###2017-09-29
#分销等级配置表-增加logo字段
ALTER TABLE `shopnc_member_distribute_type` ADD COLUMN `logo_img` varchar(255) DEFAULT NULL COMMENT 'logo' AFTER `name`;

###2017-10-10
#佣金到账通知消息模板
INSERT INTO `db_yinuo`.`shopnc_member_msg_tpl` (`mmt_code`, `mmt_name`, `mmt_message_switch`, `mmt_message_subject`, `mmt_message_content`, `mmt_short_switch`, `mmt_short_content`, `mmt_mail_switch`, `mmt_mail_subject`, `mmt_mail_content`) VALUES ('fanyong_notice', '佣金到账通知', '1', '', '您有一笔新收益,请及时查看', '1', '【{$site_name}】您有一笔佣金返点，请登录艺诺网查看。如有问题，请联系客服：400-135-2688', '0', '{$site_name}发范德萨发生大幅是', '<p>发烦死哒烦死哒');

###2017-10-12
#拍卖专场表增添sort字段
ALTER TABLE `shopnc_auction_special`
ADD COLUMN `participants`  INT(10) NULL DEFAULT '0' COMMENT '专场参与量' AFTER `special_click`;

###2017-10-12
#拍卖商品表增添participants字段
ALTER TABLE `shopnc_auctions`
ADD COLUMN `real_participants`  INT(10) NULL DEFAULT '0' COMMENT '真实参与人数' AFTER `num_of_applicants`;

###2017-10-10
#用户消息模板添加站内信
ALTER TABLE `shopnc_member_msg_tpl`
ADD COLUMN `mmt_message_subject` varchar(255) NOT NULL COMMENT '站内信标题';
###2017-10-16
#全局推送消息模板
CREATE TABLE `shopnc_global_msg_tpl` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `tpl_title` varchar(50) NOT NULL COMMENT '模板标题',
  `mmt_message_switch` tinyint(3) NOT NULL DEFAULT '0' COMMENT '站内信接收开关',
  `mmt_message_subject` varchar(50) NOT NULL COMMENT '消息标题',
  `mmt_message_content` varchar(100) DEFAULT '' COMMENT '消息内容',
  `mmt_short_switch` tinyint(3) NOT NULL DEFAULT '0' COMMENT '短信接收开关',
  `mmt_short_content` varchar(100) DEFAULT '' COMMENT '短信内容',
  `send_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '发送类型: 1系统+短信2系统3短信',
  `send_way` tinyint(2) NOT NULL DEFAULT '0' COMMENT '发送方式: 1立即2定时',
  `send_state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '发送状态: 1未发送2已发送',
  `send_time` int(11) DEFAULT NULL COMMENT '发送时间',
  `add_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局推送消息表';

###2017-10-18
#全局推送消息日志
CREATE TABLE `shopnc_global_msg_log` (
  `id` INT(8) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `global_id` INT(11) NOT NULL DEFAULT '0' COMMENT '全局发送ID',
  `send_member_id_str` TEXT(500) DEFAULT '' COMMENT '发送会员ids',
  `add_time` INT(11) DEFAULT NULL COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局推送消息表';
#合伙人数量缓存
ALTER TABLE `shopnc_member`
ADD COLUMN `distribute_lv_1`  int(11) NULL DEFAULT 0 COMMENT '一级合伙人' AFTER `source`,
ADD COLUMN `distribute_lv_2`  int(11) NULL DEFAULT 0 COMMENT '二级合伙人' AFTER `distribute_lv_1`;


###2017-10-10
#充值、提现、收益、通知消息模板
INSERT INTO `shopnc_member_msg_tpl` VALUES ('recharge_notice', '充值通知', '1', '您有一笔充值消息，请及时关注', '您于{$times}，成功充值：{$money}元。感谢您对艺诺网的信任！如有任何疑问，欢迎咨询我们的售后团队。', '0', '', '0', ' ', ' '),
  ('withdrawals_notice', '提现通知', '1', '您有一笔提现消息，请及时关注', '您于{$times}，成功提现：{$money}元。预计会在2-3个工作日到账。请关注短信通知。感谢您对艺诺网的信任！如有任何问题，欢迎咨询我们的售后团队。', '1', '【{$site_name}】您有一笔提现{$money}元金额，提现成功。如有问题，请联系客服：400-135-2688', '0', ' ', ' '),
  ('profit_notice', '收益提醒', '1', '拍卖结束。您有一笔资金消息，请及时关注', '您参与的拍卖：{$good_name}，订单号：{$order_sn}。拍卖保证金已转入您的余额中，保证金天数：{$num}天。收益：{$money}，已到账。感谢您的参与！艺诺网--艺术品金融理财保障平台。', '1', '【{$site_name}】您有一笔收益：{$money}元，已到账。可通过余额账号查看，感谢您的信任。艺诺网是国内最好的艺术品交易平台。', '0', ' ', ' ');


###2017-10-20
#拍卖专场表增添sort字段
ALTER TABLE `shopnc_member_msg_tpl`
ADD COLUMN `mmt_wx_switch`  tinyint(3) NOT NULL DEFAULT '0' COMMENT '微信消息模板开关' AFTER `mmt_mail_content`,
ADD COLUMN `mmt_wx_subject`  VARCHAR(255) NULL DEFAULT NULL COMMENT '微信消息模板标题' AFTER `mmt_wx_switch`;
###2017-10-16
#保证金表增加出价标记
ALTER TABLE `shopnc_margin_orders`
ADD COLUMN `is_bid`  int(11) NULL DEFAULT 0 COMMENT '是否出过价(0,未出价 1,已出价)' AFTER `api_pay_state`;

###2017-10-24
#站内信logo
ALTER TABLE db_yinuo.shopnc_member_msg_tpl
  ADD mmt_message_logo VARCHAR(100) DEFAULT '' COMMENT '站内信logo'
  AFTER mmt_message_content;

ALTER TABLE db_yinuo.shopnc_message
  ADD `mmt_code` varchar(50) DEFAULT '' COMMENT '用户消息模板编号';


###2017-10-28
#推荐店铺
CREATE TABLE `shopnc_store_recommendation` (
	`id` INT (8) NOT NULL AUTO_INCREMENT COMMENT '推荐id',
	`store_id` INT (11) DEFAULT NULL COMMENT '店铺ID',
	`store_add_time` INT (10) unsigned DEFAULT NULL COMMENT '店铺添加时间',
	`store_state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '推荐状态 1开启 0关闭',
	PRIMARY KEY (`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT = '推荐店铺';
#添加首页默认店铺
INSERT INTO `shopnc_store_recommendation` VALUES ('1', '84', UNIX_TIMESTAMP(NOW()),'1');
###2017-10-26
#保证金表增加利息待返记录id
ALTER TABLE `shopnc_margin_orders`
ADD COLUMN `lg_id`  int(11) NULL DEFAULT 0 COMMENT '预存款变更id' AFTER `is_bid`;

ALTER TABLE `shopnc_member_commission`
MODIFY COLUMN `commission_type`  tinyint(1) NULL DEFAULT 1 COMMENT '返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息' AFTER `goods_commission`;

###2017-10-30
#个人简介
ALTER TABLE `shopnc_member`
MODIFY COLUMN `member_truename` VARCHAR (30) DEFAULT NULL COMMENT '会员昵称',
MODIFY COLUMN `member_sex` tinyint(1) DEFAULT NULL COMMENT '会员性别 1为男 2为女',
ADD COLUMN `member_profile` VARCHAR (300) DEFAULT NULL COMMENT '会员简介' AFTER `member_sex`;

###20171108
#上上级用户备注
ALTER TABLE `shopnc_member_distribute`
ADD COLUMN `member_comment_2` varchar(255) DEFAULT NULL COMMENT '用户备注(上上级用户对2级用户的备注)' AFTER `member_comment`;


###2017-11-20
#协议表 新增协议
INSERT INTO `shopnc_mb_agreement` VALUES (NULL, 'caution_money', '保证金协议合同', NULL, UNIX_TIMESTAMP(NOW())), (NULL, 'bankcard', '银行卡限额说明', NULL, UNIX_TIMESTAMP(NOW()));


###2017-11-20
#拍卖商品虚拟数据配置表
DROP TABLE IF EXISTS `shopnc_auction_virtual_data_config`;
CREATE TABLE `shopnc_auction_virtual_data_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `type` int(11) DEFAULT '0' COMMENT '类型(1,点击数  2,收藏数  3,参拍数)',
  `min_value` decimal(11,2) DEFAULT '0.00' COMMENT '下限',
  `max_value` decimal(11,2) DEFAULT '0.00' COMMENT '上限',
  `rate_type` int(11) DEFAULT '0' COMMENT '是否有收益 0,无收益 1,有收益',
  `udate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `cdate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='拍品虚拟数据配置表';

-- ----------------------------
-- Records of shopnc_auction_virtual_data_config
-- ----------------------------
INSERT INTO `shopnc_auction_virtual_data_config` VALUES ('1', '收益拍品点击区间', '1', '5000.00', '10000.00', '1', '2017-11-22 11:04:56', null);
INSERT INTO `shopnc_auction_virtual_data_config` VALUES ('2', '普通拍品点击区间', '1', '3000.00', '7000.00', '0', '2017-11-22 11:22:00', null);
INSERT INTO `shopnc_auction_virtual_data_config` VALUES ('3', '拍品收藏区间', '2', '5.00', '8.00', '0', '2017-11-21 10:30:17', null);
INSERT INTO `shopnc_auction_virtual_data_config` VALUES ('4', '收益拍品参拍人数', '3', '3.00', '6.00', '1', '2017-11-22 11:05:33', null);
INSERT INTO `shopnc_auction_virtual_data_config` VALUES ('5', '普通拍品参拍区间', '3', '3.00', '5.00', '0', null, null);


###2017-11-23
#商品表增添artist_sort字段
ALTER TABLE `shopnc_goods`
ADD COLUMN `artist_sort`  smallint(5) NULL DEFAULT 10000 COMMENT '艺术家首页排序';
#商品公共内容表增添artist_sort字段
ALTER TABLE `shopnc_goods_common`
ADD COLUMN `artist_sort`  smallint(5) NULL DEFAULT 10000 COMMENT '艺术家首页排序';
###20171121
#区域代理表
CREATE TABLE `shopnc_area_agent` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '代理id',
  `agent_name` varchar(50) NOT NULL COMMENT '代理名称',
  `agent_address` varchar(50) NOT NULL COMMENT '代理街道地址',
  `agent_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '代理类型：1自然人2法人',
  `agent_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '代理状态：1为正常2禁用',
  `linkman_name` varchar(50) NOT NULL COMMENT '联系人名称',
  `linkman_mobile` varchar(11) NOT NULL COMMENT '联系人手机号',
  `commission_lv` tinyint(1) NOT NULL DEFAULT '1' COMMENT '返佣等级',
  `area_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '区域类型：1省2市3县',
  `reg_time` int(10) NOT NULL COMMENT '注册时间',
  `edit_time` int(10) NOT NULL COMMENT '更新时间',
  `area_id` int(11) DEFAULT NULL COMMENT '地区ID',
  `county_id` int(11) DEFAULT NULL COMMENT '县ID',
  `city_id` int(11) DEFAULT NULL COMMENT '市ID',
  `province_id` int(11) DEFAULT NULL COMMENT '省ID',
  `area_info` varchar(255) DEFAULT NULL COMMENT '地区内容',
  `bank_householder` varchar(10) DEFAULT NULL COMMENT '银行卡户主',
  `bank_number` varchar(50) DEFAULT NULL COMMENT '银行卡账号',
  `bank_accounts_address` varchar(50) DEFAULT NULL COMMENT '银行卡支行',
  `identity_card_number` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `business_license_image` varchar(100) DEFAULT NULL COMMENT '营业执照图片',
  PRIMARY KEY (`agent_id`),
  UNIQUE KEY `agent_name` (`agent_name`)
) ENGINE=InnoDB  CHARSET=utf8 COMMENT='区域代理表';

###20171123
#区域代理表
CREATE TABLE `shopnc_area_agent_commission_set` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `commission_set_name` varchar(50) NOT NULL COMMENT '返佣配置名称',
  `commission_lv` tinyint(1) NOT NULL DEFAULT '1' COMMENT '返佣等级',
  `agent_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '区域类别：1省级2市级3县级',
  `wdl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '升级微代理返佣比例',
  `zl_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '升级战略合伙人返佣比例',
  `sy_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '升级事业合伙人返佣比例',
  `artist_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '艺术家入驻返佣比例',
  `gg_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '广告返佣比例',
  `goods_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '销售返佣比例',
  `bzj_num` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT '保证金返佣比例',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `edit_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `commission_agent_unique` (`commission_lv`,`agent_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域代理返佣比例配置表';


###20171130
#区域代理账户表
CREATE TABLE `shopnc_area_agent_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账户id',
  `agent_account` varchar(50) NOT NULL COMMENT '区域代理账号',
  `agent_pwd` varchar(50) NOT NULL COMMENT '密码',
  `agent_id` int(10) NOT NULL COMMENT '代理ID',
  `last_login_time` int(10) NOT NULL COMMENT '最后登录时间',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `edit_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `agent_id_account` (`agent_account`,`agent_id`)
) ENGINE=InnoDB CHARSET=utf8 COMMENT='区域代理账户表';

###2017-12-01
#添加活动信息
INSERT INTO `db_yinuo`.`shopnc_mb_agreement` (`agreement_code`, `agreement_title`, `agreement_content`, `last_modify_time`) VALUES ('underline_pay_prize', '线下支付奖励', '  ', '1512208297');

###20171208
#区域代理考核表
CREATE TABLE `shopnc_area_agent_assessment_set` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `assessment_set_name` varchar(50) NOT NULL COMMENT '考核配置名称',
  `commission_lv` tinyint(1) default '1' not null comment '返佣等级',
  `agent_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '区域类别：1省级2市级3县级',
  `last_bzj_content` TEXT NOT NULL COMMENT '上月保证金考核',
  `last_goods_content` TEXT NOT NULL COMMENT '上月订单考核',
  `last_artist_content` TEXT NOT NULL COMMENT '上月艺术家考核',
  `curr_bzj_content` TEXT NOT NULL COMMENT '当月保证金考核',
  `curr_goods_content` TEXT NOT NULL COMMENT '当月订单考核',
  `curr_artist_content` TEXT NOT NULL COMMENT '当月艺术家考核',
  `next_bzj_content` TEXT NOT NULL COMMENT '下月保证金考核',
  `next_goods_content` TEXT NOT NULL COMMENT '下月订单考核',
  `next_artist_content` TEXT NOT NULL COMMENT '下月艺术家考核',
  `edit_time` int(10) NOT NULL COMMENT '编辑时间',
  `create_time` int(10) not null comment '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `commission_agent_unique` (`commission_lv`,`agent_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域代理考核表';

###2017-11-20
#预存款变更日志表 添加lg_available_amount字段
 ALTER TABLE `shopnc_pd_log`
ADD COLUMN `lg_available_amount`  decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '剩余可用预存款总额' AFTER `lg_freeze_amount`,
ADD COLUMN `lg_freeze_predeposit`  decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '冻结预存款总额' AFTER `lg_freeze_amount`;
#删除冗余(提现成功)消息通知
 DELETE FROM `shopnc_member_msg_tpl` WHERE (`mmt_code`='withdrawals_notice');

###2017-11-23
#用户消息模板表增添 <收货成功提醒>
INSERT INTO `db_yinuo`.`shopnc_member_msg_tpl`  VALUES ('receipt_success_notice', '收货成功提醒', '1', '收货成功提醒', '您已完成一笔交易，感谢您对艺诺网的信任！', '', '0', '0', '0', '0', '0', '0', '0');

###2017-12-21
#处理拍卖报错,添加拍卖分类字段
ALTER TABLE `shopnc_auctions`
ADD COLUMN `gc_id`  int(4) NULL COMMENT '拍卖分类' AFTER `state`;


###2017-12-25
ALTER TABLE `shopnc_margin_orders`
MODIFY COLUMN `order_state`  tinyint(1) NULL DEFAULT 0 COMMENT '订单状态：0：未支付，1：已支付，2：提交线下支付凭证，3：线上支付部分支付，4：取消支付,5:支付失败' AFTER `api_pay_amount`;

CREATE TABLE `shopnc_payment_error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trade_no` varchar(255) DEFAULT NULL COMMENT '支付订单号',
  `order_sn` varchar(255) DEFAULT NULL COMMENT '内部订单号',
  `api_price` decimal(10,2) DEFAULT NULL COMMENT '应付金额',
  `api_price_t` decimal(10,2) DEFAULT NULL COMMENT '实付金额',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方支付异常订单log';



###2017-12-15
#添加新手拍品标记
ALTER TABLE `shopnc_auctions`
ADD COLUMN `auction_type`  int(11) NULL DEFAULT 0 COMMENT '拍品类型: 0,普通拍品,1,新手拍品' AFTER `auction_name`;
#添加新手拍品标记
ALTER TABLE `shopnc_auction_special`
ADD COLUMN `special_type`  int(11) NULL DEFAULT 0 COMMENT '拍品类型: 0,普通拍品,1,新手拍品' AFTER `special_name`;

###2017-12-26
#添加拍卖结束竞拍(成功|失败)消息模版
INSERT INTO `test_yinuo`.`shopnc_member_msg_tpl` (`mmt_code`, `mmt_name`, `mmt_message_switch`, `mmt_message_subject`, `mmt_message_content`, `mmt_message_logo`, `mmt_short_switch`, `mmt_short_content`, `mmt_mail_switch`, `mmt_mail_subject`, `mmt_mail_content`, `mmt_wx_switch`, `mmt_wx_subject`)
VALUES ('auction_end_notice', '竞拍结束通知', '1', '提示：{$auction_name} 竞拍结束', '您有一笔{$desc}的拍卖，请查看。', '', '1', '【{$site_name}】您参与的竞拍：{$auction_name}{$desc}。感谢您的参与！更多珍品精品，请关注艺诺网。', '0', ' ', ' ', '0', '0');
###2017-12-19
#店铺所在地区
ALTER TABLE `shopnc_store`
MODIFY COLUMN `province_id` INT DEFAULT '0' NOT NULL COMMENT '省份ID',
ADD COLUMN `city_id` INT DEFAULT '0' NOT NULL COMMENT '城市ID',
ADD COLUMN `area_id` INT DEFAULT '0' NOT NULL COMMENT '地区ID';

###2017-12-05
#订单表增添notice_state字段
ALTER TABLE `shopnc_orders` ADD COLUMN `notice_state`  tinyint(4) NULL DEFAULT 0 COMMENT '消息提醒：0未提醒，1已经提醒' AFTER `auction_id`;
#保证金订单表增添notice_state字段
ALTER TABLE `shopnc_margin_orders` ADD COLUMN `notice_state`  tinyint(4) NULL DEFAULT 0 COMMENT '消息提醒：0未提醒，1已经提醒';
#消息表增添 待支付消息模版
INSERT INTO `shopnc_member_msg_tpl` (`mmt_code`, `mmt_name`, `mmt_message_switch`, `mmt_message_subject`, `mmt_message_content`, `mmt_message_logo`, `mmt_short_switch`, `mmt_short_content`, `mmt_mail_switch`, `mmt_mail_subject`, `mmt_mail_content`, `mmt_wx_switch`, `mmt_wx_subject`) VALUES ('waiting_payment_notice', '待支付提醒', '1', '待支付提醒', '您有一笔订单待支付，请及时关注！', '', '1', '【{$site_name}】您有一笔订单将在{$delete_time}后自动取消，请及时关注。如有问题，请联系客服：400-135-2688', '0', '1111', ' 1111', '0', NULL);

###2017-12-28
#预存款充值表增添pdr_api_pay_time字段
ALTER TABLE `shopnc_pd_recharge`
ADD COLUMN `pdr_api_pay_time`  int(11) NULL COMMENT '在线支付动作时间,只要向第三方支付平台提交就会更新' AFTER `pdr_payment_name`;

###2017-12-28
#修改表payment_code字段储存长度
ALTER TABLE `shopnc_margin_orders`
MODIFY COLUMN `payment_code`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付方式名称代码' AFTER `updated_at`


###2018-01-10
#保证金订单扣除状态
ALTER TABLE `shopnc_margin_orders`
MODIFY COLUMN `refund_state`  tinyint(1) NULL DEFAULT 0 COMMENT '退款状态：0：未退款，1：已退款,2:已扣除' AFTER `order_state`;

ALTER TABLE `shopnc_member`
ADD COLUMN `default_amount`  decimal(20,2) NULL DEFAULT 0.00 COMMENT '违约金金额' AFTER `available_margin`;


###2018-01-04
#会员等级-新增数据
INSERT INTO `shopnc_member_distribute_type` VALUES('6','11','内部-微合伙人','0.00',NOW(),NOW()),('7','12','内部-战略合伙人','0.00', NOW(),NOW()),('8','13','内部-事业合伙人','0.00',NOW(),NOW()),('9','14','内部-会员','0.00',NOW(),NOW());
#添加会员角色
ALTER TABLE `shopnc_member` ADD COLUMN `member_role`  tinyint(4) NULL DEFAULT 0 COMMENT '会员角色：0 注册会员，1 内部会员，2 导入会员' AFTER `member_type`;
#添加角色配置
INSERT INTO `shopnc_setting` (`name`, `value`) VALUES ('member_role', 'a:3:{i:0;a:2:{s:16:\"member_role_code\";i:0;s:16:\"member_role_name\";s:12:\"注册会员\";}i:1;a:2:{s:16:\"member_role_code\";i:1;s:16:\"member_role_name\";s:12:\"内部员工\";}i:2;a:2:{s:16:\"member_role_code\";i:2;s:16:\"member_role_name\";s:12:\"导入会员\";}}');

###2018-01-08
#新增 shopnc_customize 定制表
CREATE TABLE `shopnc_customize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(50) NOT NULL COMMENT '姓名',
  `cus_mobile` varchar(11) NOT NULL COMMENT '手机号',
  `content` text COMMENT '定制内容',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `add_time` varchar(10) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定制咨询内容表';


INSERT INTO `shopnc_mb_agreement`
(`agreement_code`, `agreement_title`,`agreement_content`,`last_modify_time`)
VALUES ('custom_made_explanation', '定制说明',  null, null);

###2018-01-15
#会员拍品关系表更新
ALTER TABLE `shopnc_auction_member_relation`
MODIFY COLUMN `is_bond`  tinyint(1) NULL DEFAULT 0 COMMENT '是否提交保证金 0:未创建保证金订单,1：已支付,2:待支付' AFTER `is_remind`;


###2018-01-18
#增添协议
INSERT INTO `shopnc_mb_agreement` (`agreement_code`, `agreement_title`, `agreement_content`, `last_modify_time`)
VALUES ('bond_explanation', '保证金收益页面说明', NULL, NULL),('predeposit_explanation', '账户余额明细页面说明', NULL, NULL);

###2018-01-22
#shopnc_member_common表添加auth_code_check_date字段
ALTER TABLE `shopnc_member_common` ADD COLUMN `auth_code_check_date`  int(11) NOT NULL DEFAULT 0 COMMENT '提交错误验证码时间';

###2018-01-19
#保证金订单表添加校验字段
ALTER TABLE `shopnc_margin_orders` ADD COLUMN `financial_verification`  tinyint(1) NULL DEFAULT 0 COMMENT '财务校验：1已校验、0未校验 ';
#预存款充值表添加校验字段
ALTER TABLE `shopnc_pd_recharge` ADD COLUMN `financial_verification`  tinyint(1) NULL DEFAULT 0 COMMENT '财务校验：1已校验、0未校验 ';

###2018-02-05
#预存款充值表添加校验字段
ALTER TABLE `shopnc_orders` ADD COLUMN `api_pay_amount`  DECIMAL(20, 2) UNSIGNED DEFAULT 0 COMMENT '第三方支付金额' AFTER `api_pay_time`;
#编辑已经支付成功的第三方支付金额
UPDATE shopnc_orders LEFT JOIN shopnc_order_pay ON shopnc_orders.pay_sn = shopnc_order_pay.pay_sn
SET shopnc_orders.api_pay_amount = (
  shopnc_orders.goods_amount - shopnc_orders.rcb_amount - shopnc_orders.pd_amount - shopnc_orders.rpt_amount - shopnc_orders.points_amount - shopnc_orders.margin_amount
)
WHERE
  shopnc_orders.order_state <> 10
		AND
	shopnc_order_pay.api_pay_state = 1
		AND
	(shopnc_orders.goods_amount > (shopnc_orders.rcb_amount + shopnc_orders.pd_amount + shopnc_orders.rpt_amount + shopnc_orders.points_amount + shopnc_orders.margin_amount))

###2018-02-10
#新增 变更邀请人记录表
CREATE TABLE `shopnc_top_member_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `member_name` varchar(255) NOT NULL COMMENT '用户名',
  `old_top_member_id` int(11) DEFAULT NULL COMMENT '原上级用户ID',
  `old_top_member_name` varchar(255) DEFAULT NULL COMMENT '原上级用户名',
  `change_top_member_id` int(11) NOT NULL COMMENT '更改的上级用户ID',
  `change_top_member_name` varchar(255) NOT NULL COMMENT '更改的上级用户名',
  `admin_id` int(11) NOT NULL COMMENT '操作的管理员ID',
  `admin_name` varchar(255) NOT NULL COMMENT '操作的管理员名称',
  `msg` varchar(255) DEFAULT NULL COMMENT '变更备注信息',
  `add_time` int(11) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='变更邀请人记录表';

###2018-02-24
#预存款变更日志表 添加lg_available_amount字段
ALTER TABLE `shopnc_member`
ADD COLUMN `member_using_mobile`  varchar(11) DEFAULT NULL COMMENT '正在使用的手机' AFTER `member_sex`,
ADD COLUMN `member_real_name`  varchar(50) DEFAULT NULL COMMENT '真实姓名' AFTER `member_sex`;



#消息模版新增app推送模版
ALTER TABLE `shopnc_member_msg_tpl`
  ADD COLUMN `mmt_app_switch` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'app消息模板开关',
  ADD COLUMN `mmt_app_subject` varchar(255) DEFAULT NULL COMMENT 'app消息模板标题',
  ADD COLUMN `mmt_app_link` varchar(50) NOT NULL COMMENT '消息链接',
  ADD COLUMN `mmt_app_content` text NOT NULL COMMENT 'app描述';

###2018-03-06
#新增 app公告列表
CREATE TABLE `shopnc_app_broadcast` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `subtitle` varchar(255) DEFAULT NULL COMMENT '副标题',
  `content` text NOT NULL COMMENT '描述',
  `link` varchar(50) NOT NULL COMMENT '消息链接',
  `state` tinyint(1) default '0' null comment '发送状态 0未发送 1已发送',
  `admin_name` varchar(255) NOT NULL COMMENT '操作的管理员',
  `add_time` int(11) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='APP公告发送表';

###2018-03-15
#新增 会销报名截止时间
ALTER TABLE `shopnc_exhibition_content`
    MODIFY COLUMN  `entry_end_time` int(10) NOT NULL DEFAULT '0' COMMENT '报名截止时间';





###2018-03-27
#尊享品牌排序
ALTER TABLE `shopnc_brand`
  ADD COLUMN `brand_enjoy_zone_sort` tinyint(3) unsigned default '0' null comment '尊享品牌排序';

# 投票活动报名表 设置默认票数0
ALTER TABLE `shopnc_vote_activity_entry`
MODIFY COLUMN `vote_num`  int(11) DEFAULT '0' COMMENT '票数' AFTER `image_all`;

###2018-03-27
#合伙人保证金分级返佣配置
ALTER TABLE `shopnc_auction_special`
ADD COLUMN `partner_lv_1_wdl_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级为微代理时,额外返佣比率' AFTER `special_rate_time`,
ADD COLUMN `partner_lv_1_zl_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级为战略合伙人时,额外返佣比率' AFTER `partner_lv_1_wdl_rate`,
ADD COLUMN `partner_lv_1_sy_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级为事业合伙人时,额外返佣比率' AFTER `partner_lv_1_zl_rate`,
ADD COLUMN `partner_lv_2_wdl_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级的上级为微代理时,额外返佣比率' AFTER `partner_lv_1_sy_rate`,
ADD COLUMN `partner_lv_2_zl_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级的上级为战略合伙人时,额外返佣比率' AFTER `partner_lv_2_wdl_rate`,
ADD COLUMN `partner_lv_2_sy_rate`  decimal(10,2) NULL DEFAULT 0 COMMENT '直属上级的上级为事业合伙人时,额外返佣比率' AFTER `partner_lv_2_zl_rate`;

DROP TABLE IF EXISTS `shopnc_special_rate_config`;
CREATE TABLE `shopnc_special_rate_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '配置标题',
  `partner_lv_type` varchar(255) DEFAULT NULL COMMENT '合伙人类型(wdl:微代理  zl:战略合伙人 sy:事业合伙人)',
  `partner_lv` varchar(255) DEFAULT NULL COMMENT '分销级别(1:直属上级 2:上级的上级)',
  `rate` decimal(10,2) DEFAULT '0.00' COMMENT '比率',
  `max_rate` decimal(10,2) DEFAULT '0.00' COMMENT '最大比率',
  `udate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `cdate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='拍品保证金分级返佣配置';

-- ----------------------------
-- Records of shopnc_special_rate_config
-- ----------------------------
INSERT INTO `shopnc_special_rate_config` VALUES ('1', '微代理一级佣金奖励配置', 'wdl', '1', '1.00', '5.00', '2018-03-28 15:46:25', '2018-03-28 15:46:25');
INSERT INTO `shopnc_special_rate_config` VALUES ('2', '战略合伙人一级佣金奖励配置', 'zl', '1', '5.00', '10.00', '2018-03-28 14:17:00', '2018-03-28 14:17:00');
INSERT INTO `shopnc_special_rate_config` VALUES ('3', '事业合伙人一级佣金奖励配置', 'sy', '1', '6.00', '15.00', '2018-03-28 14:17:09', '2018-03-28 14:17:09');
INSERT INTO `shopnc_special_rate_config` VALUES ('4', '微代理二级佣金奖励配置', 'wdl', '2', '1.00', '3.00', '2018-03-28 14:17:16', '2018-03-28 14:17:16');
INSERT INTO `shopnc_special_rate_config` VALUES ('5', '战略合伙人二级佣金奖励配置', 'zl', '2', '2.00', '4.00', '2018-03-28 14:17:25', '2018-03-28 14:17:25');
INSERT INTO `shopnc_special_rate_config` VALUES ('6', '事业合伙人二级佣金奖励配置', 'sy', '2', '3.00', '5.00', '2018-03-28 14:17:35', '2018-03-28 14:17:35');

###2018-04-11
#新增 行为记录表
CREATE TABLE `shopnc_extension` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `member_mobile` varchar(12) DEFAULT NULL COMMENT '联系电话',
  `extension_code` varchar(255) DEFAULT NULL COMMENT '推广码',
  `extension_name` varchar(255) DEFAULT NULL COMMENT '渠道名称',
  `extension_type` int(11) DEFAULT '0' COMMENT '渠道分类：0校园 1机构 2个人',
  `extension_class` int(11) DEFAULT '0' COMMENT '渠道种类：0线上 1线下',
  `area_info` varchar(255) DEFAULT NULL COMMENT '地区',
  `total_follow` int(11) DEFAULT '0' COMMENT '总关注量',
  `total_register` int(11) DEFAULT '0' COMMENT '总注册量',
  `month_follow` int(11) DEFAULT '0' COMMENT '本月关注量',
  `month_cancel` int(11) DEFAULT '0' COMMENT '本月取关量',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '添加时间',
  `modify_time` int(11) unsigned DEFAULT NULL COMMENT '修改时间',
  `qrcode_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广表';

#新增 推广日志表
CREATE TABLE `shopnc_extension_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `extension_code` varchar(50) NOT NULL COMMENT '渠道ID',
  `subscribe_type` int(11) DEFAULT '0' COMMENT '关注状态：0未关注，1关注',
  `subscribe_time` int(11) unsigned DEFAULT NULL COMMENT '关注时间',
  `unsubscribe_time` int(11) unsigned DEFAULT NULL COMMENT '取消关注时间',
  `first_unsubscribe_time` int(11) unsigned DEFAULT NULL COMMENT '第一次取消时间',
  `is_register` int(11) DEFAULT '0' COMMENT '是否注册：0未注册，1注册',
  `open_id` varchar(500) DEFAULT NULL COMMENT '微信openID',
  `add_time` int(11) unsigned DEFAULT NULL,
  `modify_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广日志表';

#新增 行为记录表
CREATE TABLE `shopnc_extension_log_act` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '行为表id',
  `open_id` varchar(255) DEFAULT NULL COMMENT '微信open_id',
  `extension_code` varchar(255) DEFAULT NULL COMMENT '推广code',
  `record` varchar(255) DEFAULT NULL COMMENT '记录行为',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行为记录表';

###2018-04-10
#新增 新增推广落地页表
CREATE TABLE `shopnc_landing_page` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `page_name` varchar(255) NOT NULL COMMENT '名称',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `banner_image` varchar(255) DEFAULT NULL COMMENT '顶部图片',
  `footer_image` varchar(255) DEFAULT NULL COMMENT '底部图片',
  `show_goods_num` tinyint(1) default '0' null comment '显示商品数',
  `state` tinyint(1) default '0' null comment '发布状态 0未发布 1已发布',
  `content` text NOT NULL COMMENT '描述',
  `admin_name` varchar(255) NOT NULL COMMENT '操作的管理员',
  `modify_time` INT(11) NOT NULL COMMENT '修改时间',
  `add_time` int(11) NOT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='推广落地页';

###2018-04-10
#新增 新增推广落地页关联商品表
CREATE TABLE `shopnc_landing_relation_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `goods_id` int(10) NOT NULL COMMENT '商品ID',
  `landing_page_id` int(10) NOT NULL COMMENT '落地页ID',
  `introduce` text NOT NULL COMMENT '介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='推广落地页关联商品';


###2018-07-02
#修改用户表微信'微信openID'字段，NOT NULL 改为 NULL
ALTER TABLE `shopnc_member`
MODIFY COLUMN `weixin_open_id`  varchar(50) NULL COMMENT '微信openID' AFTER `weixin_info`;

###2018-07-02
#修改用户表微信'微信openID'字段，NOT NULL 改为 NULL
ALTER TABLE `shopnc_member`
ADD COLUMN `member_key` varchar(255) DEFAULT NULL COMMENT '用户唯一标识key' AFTER `member_id`;


###2018-07-02
#用户token表新增字段
ALTER TABLE `shopnc_mb_user_token`
ADD COLUMN `user_id` INT (10) unsigned DEFAULT NULL COMMENT '用户ID' AFTER `member_name`,
ADD COLUMN `user_name` varchar(255) DEFAULT NULL COMMENT '用户名' AFTER `user_id`;
