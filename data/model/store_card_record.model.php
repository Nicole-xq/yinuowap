<?php
/**
 * 商户编号记录表
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');


class store_card_recordModel extends Model{

    public function __construct(){
        parent::__construct('store_card_record');
    }


    public function getNumber($f_num, $type, $date = false) {
        $condition = array(
            'f_num' => $f_num,
            'type' => $type,
            'date' => $date ? $date : date('Ymd')
        );

        $number = 1;
        $data = $this->where($condition)->find();

        if($data){
            if(!$this->where(array('id' => $data['id']))->setInc('l_num', 1)){
                return false;
            }
            $number = $data['l_num'] + 1;
        }else{
            if(!$this->insert($condition)){
                return false;
            }
        }

        return $number;
    }

}
