<?php
/**
 * 拍卖拍品模型
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/22 0022
 * Time: 11:56
 * @author ADKi
 */

use Shopnc\Lib\StdArray;

defined('InShopNC') or exit('Access Invalid!');

class auctionsModel extends Model
{
    const VERIFY1 = 1;      // 审核通过
    const VERIFY2 = 2;      // 审核失败
    const VERIFY3 = 3;      // 拍卖预展
    const VERIFY4 = 4;      // 拍卖开始
    const VERIFY5 = 5;      // 拍卖结束
    const VERIFY0 = 0;      // 等待审核


    //是否流拍，0：未流拍，1：流拍
    const IS_LIUPAI_NOT = 0;
    const IS_LIUPAI_YES = 1;

    //拍卖状态: 0未结束 1拍卖结束
    const STATE_UNFINISHED = 0;
    const STATE_FINISHED = 1;

    //拍卖状态细化: 1.拍卖结束 2.预展中 3.拍卖中
    const STATUS_FINISHED = 1;
    const STATUS_IN_PREVIEW = 2;
    const STATUS_IN_AUCTION = 3;


    //拍品类型: 0,普通拍品,1,新手拍品 2.新手专区 3.捡漏拍品
    const AUCTION_TYPE_COMMON = 0;
    const AUCTION_TYPE_NEW = 1;
    const AUCTION_TYPE_NEWBIE = 2;
    const AUCTION_TYPE_PICKER = 3;

    public static $auctionTypeText = [
        self::AUCTION_TYPE_COMMON => "普通拍品",
        self::AUCTION_TYPE_NEW => "新手拍品",
        self::AUCTION_TYPE_NEWBIE => "新手专区",
    ];



    public function __construct(){
        parent::__construct('auctions');
    }

    /**
     * 新增拍品数据
     *
     * @param array $insert 数据
     * @return  int 插入ID
     */
    public function addAuction($insert) {
        return $this->insert($insert);
    }

    /**
     * 拍品列表
     *
     * @param array $condition 条件
     * @param string $field 字段
     * @param string $group 分组
     * @param string $order 排序
     * @param int $limit 限制
     * @param int $page 分页
     * @return array 二维数组
     */
    public function getAuctionList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $condition = $this->_getRecursiveClass($condition);
        return $this->field($field)->where($condition)
            ->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }


    /**
     * 最新商城关联专场--拍品列表
     *
     * @param array $condition 条件
     * @param string $field 字段
     * @param string $group 分组
     * @param string $order 排序
     * @param int $limit 限制
     * @param int $page 分页
     * @return array 二维数组
     */
    public function getNbAuctionList($condition, $fields = '*', $order = '', $limit = 0)
    {
        return $this->field($fields)->where($condition)->order($order)->limit($limit)->select();
    }

    /**
     * 拍品+专场 列表
     *
     * @param array $condition 条件
     * @param string $field 字段
     * @param string $group 分组
     * @param string $order 排序
     * @param int $limit 限制
     * @param int $page 分页
     * @param int $on 链接条件
     * @return array 二维数组
     */
    public function getAuctionSpecialList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0, $on = 'auctions.special_id=auction_special.special_id')
    {
        $condition = $this->_getRecursiveClass($condition);
        return Model()->table('auctions,auction_special')->field($field)->join('left')->on($on)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }

    /**
     * 获得拍品子分类的ID
     * @param array $condition
     * @return array
     */
    private function _getRecursiveClass($condition){
        if (isset($condition['gc_id']) && !is_array($condition['gc_id'])) {
            /** @var goods_classModel $goods_class_model */
            $goods_class_model = Model('goods_class');
            $gc_list = $goods_class_model->getGoodsClassForCacheModel();
            if (!empty($gc_list[$condition['gc_id']])) {
                $gc_id[] = $condition['gc_id'];
                $gcchild_id = empty($gc_list[$condition['gc_id']]['child']) ? array() : explode(',', $gc_list[$condition['gc_id']]['child']);
                $gcchildchild_id = empty($gc_list[$condition['gc_id']]['childchild']) ? array() : explode(',', $gc_list[$condition['gc_id']]['childchild']);
                $gc_id = array_merge($gc_id, $gcchild_id, $gcchildchild_id);
                $condition['gc_id'] = array('in', $gc_id);
            }
        }
        return $condition;
    }

    /**
     * 更新单个拍品数据
     *
     * @param array $update 更新数据
     * @param array $condition 条件
     * @return boolean
     */
    public function editAuctions($update, $condition, $updateXS = false) {
        $auctions_list = $this->getAuctionList($condition, 'auction_id');
        if (empty($auctions_list)) {
            return true;
        }

        return $this->editAuctionsById($update, $auctions_list[0], $updateXS);
    }

    /**
     * 批量更新拍品数据
     * @param array $update
     * @param int|array $auctions_id_array
     * @return boolean|unknown
     */
    public function editAuctionsById($update, $auctions_id_array, $updateXS = false) {
        if (empty($auctions_id_array)) {
            return true;
        }

        $condition['auction_id'] = array('in', $auctions_id_array);
        $update['auction_edit_time'] = TIMESTAMP;

        //TODO 锁定该拍品详细数据
        //LOCK TABLES tbl_name WRITE;  UNLOCK TABLES;
        $result = Model()->table('auctions')->where($condition)->lock(true)->update($update);
        if ($result) {
            foreach ((array)$auctions_id_array as $value) {
                $this->_dAuctionsCache($value);
            }
            if (C('fullindexer.open') && $updateXS) {
                QueueClient::push('updateXS', $auctions_id_array);
            }
        }
        return $result;
    }

    /**
     * 带店铺通知的拍品修改
     * @param array $update
     * @param array $condition
     * @param int $store_id
     * @param bool $updateXS
     * */
    public function editAuctionsVerifyFail($update, $condition, $store_id, $updateXS = false) {
        $result = $this->editAuctions($update, $condition, $updateXS = false);
        if ($result) {
            $param['auction_id'] = $condition['auction_id'];
            $param['remark'] = $update['auction_verifyremark'];
            $this->_sendStoreMsg('auctions_verify', $store_id, $param);
        }
    }

    /**
     * 发送店铺消息
     * @param string $code
     * @param int $store_id
     * @param array $param
     */
    private function _sendStoreMsg($code, $store_id, $param) {
        QueueClient::push('sendStoreMsg', array('code' => $code, 'store_id' => $store_id, 'param' => $param));
    }

    /**
     * 新增多条拍品图片
     *
     * @param unknown $insert
     */
    public function addAuctionsImagesAll($insert) {
        $result = Model()->table('auctions_images')->insertAll($insert);

        if ($result) {
            $auction_id_array = array();
            foreach ($insert as $val) {
                $this->_dAuctionsImageCache($val['auction_id']);
                $auction_id_array[] = $val['auction_id'];
            }
            if (C('cache_open') && !empty($auction_id_array)) {
                $auction_id_array = array_unique($auction_id_array);
                $auction_id_list = $this->getAuctionList(array('auction_id' => array('in', $auction_id_array)), 'auction_id');
                foreach ($auction_id_list as $val) {
                    $this->_dAuctionsCache($val['auction_id']);
                }
            }
        }
        return $result;
    }

    /**
     * 通过的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsOfflineList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']   = self::VERIFY1;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 审核失败的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsLockUpList($condition, $field = '*', $page = 10, $order = "auction_id desc", $limit = '') {
        $condition['state']   = self::VERIFY2;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 等待审核的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsWaitVerifyList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']  = self::VERIFY0;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 预展拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionPreviewList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']  = self::VERIFY3;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 正在拍卖的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsTheAuctionList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']  = self::VERIFY4;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 拍卖流拍的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsFailBidList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']  = self::VERIFY5;
        $condition['is_liupai']  = 1;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 拍卖成功的拍品列表 卖家中心使用
     *
     * @param array $condition 条件
     * @param array $field 字段
     * @param string $page 分页
     * @param string $order 排序
     * @return array
     */
    public function getAuctionsSuccessList($condition, $field = '*', $page = 10, $order = "auction_id desc") {
        $condition['state']  = self::VERIFY5;
        $condition['is_liupai']  = 0;
        return $this->getAuctionList($condition, $field, '', $order, '', $page, '');
    }

    /**
     * 获取单条拍品信息
     *
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAuctionsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 取得拍品详细信息
     * @param string $fields
     * @return array
     */
    public function getAuctionsInfoByID($auction_id, $fields = '*') {
            $auction_info = $this->getAuctionsInfo(array('auction_id'=>$auction_id), $fields);
        return $auction_info;
    }

    /**
     * 拍品删除及相关信息
     *
     * @param   array $condition 列表条件
     * @return boolean
     */
    public function delAuctionsAll($condition) {
        // 删除拍品表数据
        $this->delAuctions($condition);
        // 删除拍品图片表数据
        unset($condition['auction_lock']);
        $this->delAuctionsImages($condition);
        return true;
    }

    /**
     * 删除拍品信息
     *
     * @param array $condition
     * @return boolean
     */
    public function delAuctions($condition) {
        $auction_list = $this->getAuctionList($condition, 'auction_id,store_id');
        if (!empty($auction_list)) {
            $auction_id_array = array();
            // 删除拍品二维码
            foreach ($auction_list as $val) {
                $auction_id_array[] = $val['auction_id'];
                @unlink(BASE_UPLOAD_PATH.DS.ATTACH_STORE.DS.$val['store_id'].DS.$val['auction_id'].'.png');
                // 删除拍品缓存
                $this->_dAuctionsCache($val['auction_id']);
            }

            if (C('fullindexer.open')) {
                QueueClient::push('updateXS', $auction_id_array);
            }
            //删除拍品浏览记录
            //Model('goods_browse')->delGoodsbrowse(array('goods_id' => array('in', $goodsid_array)));
            // 删除买家收藏表数据
            //Model('favorites')->delFavorites(array('fav_id' => array('in', $goodsid_array), 'fav_type' => 'goods'));
        }
        return Model()->table('auctions')->where($condition)->delete();
    }

    /**
     * 删除拍品图片表信息
     *
     * @param array $condition
     * @return boolean
     */
    public function delAuctionsImages($condition) {
        $image_list = $this->getAuctionsImageList($condition, 'auction_id,store_id');
        if (empty($image_list)) {
            return true;
        }
        $result = Model()->table('auctions_images')->where($condition)->delete();
        if ($result) {
            foreach ($image_list as $val) {
                $this->_dAuctionsImageCache($val['auction_id']);
            }
        }
        return $result;
    }

    /**
     * 拍品图片列表
     *
     * @param array $condition
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getAuctionsImageList($condition, $field = '*', $order = 'is_default desc,auction_image_sort asc') {
        return Model()->table('auctions_images')->field($field)->where($condition)->order($order)->select();
    }

    /**
     * 等待审核的拍品数量
     *
     * @param array $condition
     * @return int
     */
    public function getAuctionsWaitVerifyCount($condition) {
        $condition['state']  = self::VERIFY0;
        return $this->getAuctionsCount($condition);
    }

    /**
     * 等待审核的拍品数量
     *
     * @param array $condition
     * @return int
     */
    public function getAuctionsLockUpCount($condition) {
        $condition['state']  = self::VERIFY2;
        return $this->getAuctionsCount($condition);
    }

    /**
     * 获得拍品数量
     *
     * @param array $condition
     * @param string $field
     * @return int
     */
    public function getAuctionsCount($condition, $field = '*') {
        return Model()->table('auctions')->where($condition)->count($field);
    }

    /**
     * 获取单条拍品详细信息
     *
     * @param int $auction_id
     * @return array
     */
    public function getAuctionDetail($auction_id) {
        if($auction_id <= 0) {
            return null;
        }
        $auction_info = $this->getAuctionsInfo(array('auction_id'=>$auction_id));
        if ($auction_info['auction_body'] == '') unset($auction_info['auction_body']);
        if ($auction_info['auction_mobile_body'] == '') unset($auction_info['auction_mobile_body']);

        $auction_info['auction_attr'] = unserialize($auction_info['auction_attr']);
        $auction_info['auction_custom'] = unserialize($auction_info['auction_custom']);

        // 手机拍品描述
        if ($auction_info['auction_mobile_body'] != '') {
            $mobile_body_array = unserialize($auction_info['auction_mobile_body']);
            $mobile_body = '';
            if (is_array($mobile_body_array)) {
                foreach ($mobile_body_array as $val) {
                    switch ($val['type']) {
                        case 'text':
                            $mobile_body .= '<div>' . $val['value'] . '</div>';
                            break;
                        case 'image':
                            $mobile_body .= '<img src="' . $val['value'] . '">';
                            break;
                    }
                }
            }
            $auction_info['auction_mobile_body'] = $mobile_body;
        }

        //视频
        if($auction_info['auction_video']){
            $video_path = goodsVideoPath($auction_info['auction_video'], $auction_info['store_id']);
        }

        // 拍品多图
        $image_more = $this->getAuctionImageByKey($auction_info['auction_id']);

        // 拍品正在拍卖区间时长
        if (empty($auction_info['auction_end_true_t'])) {
            $auction_info['auctioning_time'] = $auction_info['auction_end_time'] - time();
        } else {
            $auction_info['auctioning_time'] = $auction_info['auction_end_true_t'] - time();
        }
        if ($auction_info['auctioning_time'] < 0) {
            $auction_info['auctioning_time'] = 0;
        }
        //拍品开始倒计时时间
        $auction_info['auction_remaining_start_time'] = $auction_info['auction_start_time'] - time();
        $image_list = array();
        $auction_image = array();
        $auction_image_mobile = array();
        if (!empty($image_more)) {
            array_splice($image_more, 5);
            foreach ($image_more as $val) {
                $image_list[] = array( '_small' => cthumb($val['auction_image'] , 60 , $auction_info['store_id']) , '_mid' => cthumb($val['auction_image'] , 360 , $auction_info['store_id']) , '_big' => cthumb($val['auction_image'] , 1280 , $auction_info['store_id']) );
                $auction_image[] = "{ title : '', levelA : '".cthumb($val['auction_image'], 60, $auction_info['store_id'])."', levelB : '".cthumb($val['auction_image'], 360, $auction_info['store_id'])."', levelC : '".cthumb($val['auction_image'], 360, $auction_info['store_id'])."', levelD : '".cthumb($val['auction_image'], 1280, $auction_info['store_id'])."'}";
                $auction_image_mobile[] = cthumb($val['auction_image'], 360, $auction_info['store_id']);
            }
        } else {
            $image_list[] = array( '_small' => cthumb($auction_info['auction_image'],60) , '_mid' => cthumb($auction_info['auction_image'] , 360) ,'_big' => cthumb($auction_info['auction_image'] , 1280) );
            $auction_image[] = "{ title : '', levelA : '".cthumb($auction_info['auction_image'], 60)."', levelB : '".cthumb($auction_info['auction_image'], 360)."', levelC : '".cthumb($auction_info['auction_image'], 360)."', levelD : '".cthumb($auction_info['auction_image'], 1280)."'}";
            $auction_image_mobile[] = cthumb($auction_info['auction_image'], 360);
        }

        $auction_body = '<div class="default">' . $auction_info['auction_body'] . '</div>';
        $auction_info['auction_body'] = $auction_body;
        // 拍品受关注次数加(30-50)
        //2017-11-20 取消随机增加点击数.
        $click = 1;//rand(30, 50);
        $auction_info['auction_click'] = intval($auction_info['auction_click']) + $click;
//        $collect = rand(2,9);
//        $this->editAuctionsById(array('auction_click' => array('exp', 'auction_click + '.$click),'auction_collect'=>array('exp','auction_collect + '.$collect)), array($auction_id));
        $this->editAuctionsById(array('auction_click' => array('exp', 'auction_click + '.$click)), array($auction_id));
        $result = array();
        $result['auction_info'] = $auction_info;

        $result['image_list'] = $image_list;
        $result['auction_image'] = $auction_image;
        $result['video_path'] = $video_path;
        $result['auction_image_mobile'] = $auction_image_mobile;
        return $result;
    }

    /**
     * 获得拍品图片数组
     * @param int $key
     * @return array
     */
    public function getAuctionImageByKey($key) {
        $image_list = $this->_rAuctionImageCache($key);
        if (empty($image_list)) {
            $auction_id = $key;
            $image_more = $this->getAuctionsImageList(array('auction_id' => $auction_id, 'auction_image'));
            $image_list['image'] = serialize($image_more);
            $this->_wAuctionImageCache($key, $image_list);
        }
        $image_more = unserialize($image_list['image']);
        return $image_more;
    }

    /**
     * 获取首页热拍拍品列表
     * @param  integer $n [description]
     * @return [type]     [description]
     */
    public function getHotAuctionGoodsList($n = 4) {
        $condition = array();
        $condition['state'] = self::VERIFY4;
        $condition['recommended'] = 1;
        return $this->field('*')->where($condition)->order('auction_end_time asc')->limit($n)->select();
    }

    /**
     * 读取拍品缓存
     * @param int $auction_id
     * @param string $fields
     * @return array
     */
    private function _rAuctionsCache($auction_id, $fields) {
        return rcache($auction_id, 'auctions', $fields);
    }

    /**
     * 写入拍品缓存
     * @param int $auction_id
     * @param array $auction_info
     * @return boolean
     */
    private function _wAuctionsCache($auction_id, $auction_info) {
        return wcache($auction_id, $auction_info, 'auctions');
    }

    /**
     * 删除拍品缓存
     * @param int $auction_id
     * @return boolean
     */
    private function _dAuctionsCache($auction_id) {
        dcache($auction_id, 'product');
        return dcache($auction_id, 'auctions');
    }

    /**
     * 读取拍品图片缓存
     * @param int $key ($goods_commonid .'|'. $color_id)
     * @param string $fields
     * @return array
     */
    private function _rAuctionImageCache($key) {
        return rcache($key, 'auctions_image');
    }

    /**
     * 写入拍品图片缓存
     * @param int $key ($goods_commonid .'|'. $color_id)
     * @param array $image_list
     * @return boolean
     */
    private function _wAuctionImageCache($key, $image_list) {
        return wcache($key, $image_list, 'auctions_image');
    }

    /**
     * 删除拍品图片缓存
     * @param int $key ($goods_commonid .'|'. $color_id)
     * @return boolean
     */
    private function _dAuctionsImageCache($key) {
        return dcache($key, 'auctions_image');
    }

    public function getArtworkCategoryByAuctionId($auctionId)
    {
        $auctionInfo = Model('')->table('auctions')->field('goods_id')->where(['auction_id' => $auctionId])->find();
        if (!isset($auctionInfo['goods_id']) || empty($auctionInfo['goods_id'])) {
            return 14;
        } else {
            $goods_id = $auctionInfo['goods_id'];
        }
        $goodsInfo = Model('')->table('goods')->field('artwork_category_id')->where(['goods_id' => $goods_id])->find();
        if (!isset($goodsInfo['artwork_category_id']) || empty($goodsInfo['artwork_category_id'])) {
            return 14;
        } else {
            return $goodsInfo['artwork_category_id'];
        }
    }

}