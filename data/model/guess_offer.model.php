<?php
/**
 * 趣猜报价
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class guess_offerModel extends Model {
    
    public function __construct() {
        parent::__construct('contract_guess');
    }

     /**
     * 添加趣猜出价
     * @param unknown $insert
     * @return boolean
     */
    public function add_guess_offer($insert) {
        return $this->insert($insert);
    }

     /**
     * 获取趣猜出价
     * @param array $condition
     * @param string $field
     * @return boolean
     */
    public function get_guess_offer($condition, $field = '*') {
        return $this->where($condition)->field($field)->find();
    }

    /**
     * 查询评价列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @return array
     */
    public function get_guess_offer_list($condition, $page = null, $limit = null, $order = 'gs_state desc,partake_time desc', $field = '*') {
        return $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
    }

    /** 
     * 趣猜最高出价
     * @param  [type] $condition [description]
     * @return [type]            [description]
     */
    public function get_guess_offer_max($condition) {
        return $this->where($condition)->field('gs_offer_price')->order('gs_offer_price desc')->find();
    }

    /**
     * 趣猜最小出价
     * @param  [type] $condition [description]
     * @return [type]            [description]
     */
    public function get_guess_offer_min($condition) {
        return $this->where($condition)->field('gs_offer_price')->order('gs_offer_price asc')->find();
    }

    /**
     * 获取竞猜次数
     * @return [type] [description]
     */
    public function get_guess_times($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 抽取中奖人
     * @param array $condition
     * @param float $right_price
     * @param array $guess_info
     * @return array [type] [description]
     */
    public function get_winer($condition,$right_price = 0,$guess_info) {
        $contract_guess_list = $this->where($condition)->select();
        if (empty($contract_guess_list)){
            Model('p_guess')->editGuess(array('gs_state' => 2),array('gs_id' => $condition['gs_id']));
            return false;
        }
        // 比较大小
        $count = count($contract_guess_list);
        for ($i=0; $i < $count; $i++) { 
            $arr2[] = abs($right_price - $contract_guess_list[$i]['gs_offer_price']);
        }
        $min = min($arr2);
        $result_arr = array();
        for ($i=0; $i < $count; $i++) { 
            if ($min == $arr2[$i]) {
                $result_arr[] = $contract_guess_list[$i];
            }
        }
        // 获取赢家信息
        if (count($result_arr) != 1) {
            $result_time_sort = array();
            foreach ($result_arr as $key => $value) {
                $result_time_sort[$value['partake_time']] = $value;
            }
            ksort($result_time_sort);
            $winer = current($result_time_sort);
        } else {
            $winer = current($result_arr);
        }
        try {
           $this->beginTransaction();
            // 更新参与者信息
            $normal_up = $this->where(array('gs_id' => $condition['gs_id'],'member_id' => array('neq',$winer['member_id'])))->update(array('gs_state' => 1));
            if (!$normal_up) {
                throw new Exception('更新信息失败');
            }
            // 更新赢家信息
            $winer_up = $this->where(array('gs_id' => $condition['gs_id'],'member_id' => $winer['member_id']))->update(array('gs_state' => 2,'finished_time' => time()));
            if (!$winer_up) {
                 throw new Exception('更新信息失败');
            }
             // 发送买家消息
            $param = array();
            $param['code'] = 'guess_notice';
            $param['member_id'] = $winer['member_id'];
            $param['param'] = array(
                'end_time' => date('Y-m-d H:i:s',time() + C('guess_add_cart_limit_time')*3),
                'gs_name' => $guess_info['gs_name']? $guess_info['gs_name']: ' ',
                'guess_url'=> str_replace('http://','',urlShop('show_guess', 'guess_detail', array('gs_id' => $condition['gs_id'])))
            );
            QueueClient::push('sendMemberMsg', $param);

            // 在中奖时获取趣猜赠送的诺币数
            if (!empty($guess_info['points'])) {
                Model('points')->savePointsLog('guess', array('pl_memberid' => $winer['member_id'], 'pl_membername' => $winer['member_name'] ), true, $guess_info['points']);
            }
            // 更新趣猜状态为已结束
            Model('p_guess')->editGuess(array('gs_state' => 2),array('gs_id' => $condition['gs_id']));
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            return callback(false,'操作失败');            
        }
    }

    /**
     *  更新趣猜时间
     * @param  [type] $condition [description]
     * @param  [type] $update    [description]
     * @return [type]            [description]
     */
    public function update_guess_offer($condition, $update) {
        return $this->where($condition)->update($update);
    }

    //SELECT count(*) as times,max(finished_time)as last_time,member_id FROM shopnc_contract_guess WHERE gs_state = 3 GROUP BY member_id  ORDER BY times desc limit 2;
    /**
     * 获取龙虎榜
     * @return [type] [description]
     */
    public function get_winer_list($limit = 6) {
        return $this->field('count(*) as times,max(finished_time)as last_time,member_id,member_name')->where(array(gs_state => 3))->group('member_id')->order('times desc,last_time desc')->limit($limit)->select();
    }

    // 获取中奖的趣猜数
    public function get_win_guess_count($member_id) {
        return $this->where(array('member_id' => $member_id,'gs_state' => 3))->count();
    }
}
