<?php
/**
 * 趣猜活动
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Lib\StdArray;

defined('InShopNC') or exit('Access Invalid!');

class p_act_guessModel extends Model
{
    const GUS_STATE_PENDING = 10;
    const GUS_STATE_NoRMAL = 20;
    const GUS_STATE_FAIL = 30;
    const GUS_STATE_CANCEL = 31;
    const GUS_STATE_FINISHED = 32;

    private $gusActStates = array(
        0 => '全部',
        self::GUS_STATE_PENDING => '审核中',
        self::GUS_STATE_NoRMAL => '正常',
        self::GUS_STATE_FAIL => '审核失败',
        self::GUS_STATE_CANCEL => '管理员关闭',
        self::GUS_STATE_FINISHED => '已结束',
    );

    public function __construct()
    {
        parent::__construct('p_act_guess');
    }

    /** 
     * 趣猜活动列表
     * @param  array  $where 查询条件
     * @param  int  $page  页数
     * @param  string  $order 排序
     * @return array 趣猜列表    
     */
    public function getGusActList($where, $page = null, $order = 'id desc',$limit=0)
    {
        return (array) $this->where($where)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 获取活动状态列表
     * @return [type] [description]
     */
    public function getGusActStates()
    {
        return $this->gusActStates;
    }

    /**
     * 通过ID获取趣猜活动基本信息
     */
    public function getGusActInfoByID($actId, $storeId = 0)
    {
        $where = array(
            'id' => (int) $actId,
        );

        if ($storeId > 0) {
            $where['store_id'] = (int) $storeId;
        }

        $record = (array) $this->where($where)->find();

        return $record;
    }

     /**
     * 更新
     * @param array $update
     * @param array $condition
     * @return bool
     *
     */
    public function editGuess($update, $condition) {
        $result = $this->where($condition)->update($update);
        return $result;
    }

    /** 
     * 获取推荐趣猜精品列表
     * @return [type] [description]
     */
    public function getRecGuessList($limit=4) {
        return $this->where(array('tend' => array('gt',time()),'tstart' => array('lt',time()),'recommended' => 1))->order('recommended desc,id desc')->limit($limit)->select();
    }

  /**
     * 添加趣猜活动
     */
    public function addGuess($data)
    {
        return $this->insert($data);
    }    

    /** 
     * 删除趣猜活动
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function delGuessAct($data)
    {
        return $this->where($data)->delete();
    }
     /*
     * 审核成功
     * @param int $groupbuy_id
     * @return bool
     *
     */
    public function reviewPassGuess($id) {
        $condition = array();
        $condition['id'] = $id;

        $update = array();
        $update['state'] = self::GUS_STATE_NoRMAL;

        return $this->editGuess($update, $condition);
    }

    /*
     * 审核失败
     * @param int $groupbuy_id
     * @return bool
     *
     */
    public function reviewFailGuess($id) {
        // // 商品解锁
        // $groupbuy_info = $this->getGroupbuyInfoByID($groupbuy_id);

        $condition = array();
        $condition['id'] = $id;

        $update = array();
        $update['state'] = self::GUS_STATE_FAIL;

        $return =  $this->editGuess($update, $condition);
       
        return $return;
    }
}
