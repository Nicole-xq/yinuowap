<?php
/**
 * 用户消息模板模型
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class member_msg_tplModel extends Model{
    public function __construct() {
        parent::__construct('member_msg_tpl');
    }

    /**
     * 用户消息模板列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getMemberMsgTplList($condition, $field = '*', $page = 100, $order = 'mmt_code asc') {
        return $this->field($field)->where($condition)->order($order)->page($page)->select();
    }

    /**
     * 用户消息模板详细信息
     * @param array $condition
     * @param string $field
     */
    public function getMemberMsgTplInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 编辑用户消息模板
     * @param array $condition
     * @param unknown $update
     */
    public function editMemberMsgTpl($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 拍品设置提醒
     * @param $condition
     * @param $order_info
     */
    public function sendSetRemindMessage($condition,$auction_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $auction_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'follow_auction',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'auction_name'=>$auction_info['auction_name'],
                'url'  => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }


    /**
     * 发送客户系统通知
     * 拍品取消
     * @param $condition
     * @param $order_info
     */
    public function sendCancelRemindMessage($condition,$auction_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $auction_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'cannel_follow_auction',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'auction_name'=>$auction_info['auction_name'],
                'url'  => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }


    /**
     * 发送客户系统通知
     * 设置专场提醒
     * @param $condition
     * @param $order_info
     */
    public function sendSetSpecialRemindMessage($condition,$special_info) {
        $url = AUCTION_NEW_URL . '/profitDetails?special_id=' . $special_info['special_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'follow_special',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'special_name'=>$special_info['special_name'],
                'url'  => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }

    /**
     * 专场取消关注
     * @param $condition
     * @param $order_info
     */
    public function sendCancelSpecialRemindMessage($condition,$special_info) {
        $url = AUCTION_NEW_URL . '/profitDetails?special_id=' . $special_info['special_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'cannel_follow_speciak',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'special_name'=>$special_info['special_name'],
                'url'  => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }

    /**
     * 竞价被超越提醒
     * 同时发送微信公众号提醒
     * @param $condition
     * @param $order_info
     */
    public function sendBeyondPriceMessage ($condition,$auction_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $auction_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'beyond_price',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'auction_name'=>$auction_info['auction_name'],
                'url'  => $url,
                'wx_url'  => $url,
                'current_price' => $condition['current_price']
            ]
        ];
        $this->sendWXTplMsg($param);
        QueueClient::push('sendMemberMsg', $param);
    }


    /**
     * 竞价被超越提醒
     * 同时发送微信公众号提醒
     * @param $condition
     * @param $order_info
     */
    public function sendAucutionMarginPayMessage ($condition,$auction_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $auction_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        $param = [
            'code'     =>'beyond_price',
            'member_id'=>intval($condition['member_id']),
            'number'   =>['mobile'=>intval($condition['mobile'])],
            'param'    =>[
                'auction_name'=>$auction_info['auction_name'],
                'url'  => $url,
                'wx_url'  => $url,
                'current_price' => $auction_info['current_price']
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }



    /*
     * 发送微信消息
     */
    public function sendWXTplMsg($param)
    {
        /** @var wx_template_messageLogic $wx_template_message */
        $inc_file = BASE_PATH.DS.'..'.DS.'mobile'.DS.'api'.DS.'payment'.DS.'wxpay_jsapi'.DS.'wx_jssdk.php';
        require_once($inc_file);
        $postData = $this->getWxTemplateData($param);
        $condition = ['member_id' => $param['member_id']];
        $field = 'member_id,member_name,weixin_unionid,weixin_info,weixin_open_id';
        $memberInfo = Model('member')->getMemberInfo($condition, $field);
        $wx_openid = $memberInfo['weixin_open_id'];
        $template_id = C('wx_message_templates')[$param['code']];
        $url = isset($param['param']['url']) ? $param['param']['url'] : '';
        $post_data = [
            "touser"=>$wx_openid,
            "template_id"=>$template_id,
            'url' => $url,
            "data"=>$postData
        ];
        $Wx_info = C('wx_payment_info');
        if (empty($postData) || empty($wx_openid) || empty($template_id)) {
            return false;
        }
        $sdkObj = new JSSDK($Wx_info['app_id'], $Wx_info['script']);
        $sdkObj->sendMessage($post_data);
    }


    /**
     * 微信模板基础配置信息
     */
    function getWxTemplateData($param){
        $SendData = [];
        switch ($param['code']){
            case 'order_payment_success' :  //订单支付成功
                $SendData = [
                    "first" => [
                        "value"=>'您好，恭喜您已购买成功。',//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "Remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "orderProductName"=>[
                        "value"=>$param['param']['goods_name'],
                        "color"=>"#173177"
                    ],
                    "orderMoneySum"=>[
                        "value"=>$param['param']['order_amount'],
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'order_deliver_success' :  //订单已发货
                $SendData = [
                    "first" => [
                        "value"=>'您购买的艺术品已发货，敬请关注。',
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['goods_name'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$param['param']['shipping_time'],
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'auction_end_notice' :  //竞拍结束通知
                $SendData = [
                    "first" => [
                        "value"=>'竞拍结束通知',
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['auction_name'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$param['param']['auction_bond'],
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>$param['param']['end_time'],
                        "color"=>"#173177"
                    ],
                ];
                break;
            //Mr.Liu新增微信模板消息
            case 'aucution_margin_pay' :  //保证金缴纳通知
                $SendData = [
                    "first" => [
                        "value"=>"您的{$param['param']['auction_name']}保证金缴纳成功",//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>"￥{$param['param']['margin_amount']}",//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>date('Y-m-d', time()),
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"此拍品已累计缴纳￥{$param['param']['margin_count']}",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'release_margin' :  //释放保证金通知
                $SendData = [
                    "first" => [
                        "value"=>'您好，您的保证金已退回',//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['auction_name'],//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"￥{$param['param']['money']}",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"好遗憾！您参加的拍卖未中标，保证金将退回原路，预计1-3个工作日到，请查收。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'margin_timeout_remind' :  //违约提前提醒
                $SendData = [
                    "first" => [
                        "value"=>"您好，参加的{$param['param']['auction_name']}订单尾款支付开始。",//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['order_sn'],//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"{$param['param']['remind_time']}",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"超时违约，会扣除保证金，请及时付款。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'default_margin' :  //违约保证金
                $SendData = [
                    "first" => [
                        "value"=>"尊敬的客户，您参加的{$param['param']['auction_name']}因拍中，超时未支付，",//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>date('Y-m-d H:00:00', time()),//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"很抱歉，因您违反相关约定",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"感谢您的参与。",
                        "color"=>"#173177"
                    ]
                ];
                if ($param['param']['type'] == 1) {
                    $SendData['keyword2'] = [
                        "value"=>"很抱歉，因您违反相关约定，现违约扣除保证金￥{$param['param']['money']}，
                            剩余保证金￥{$param['param']['return_amount']}已原路退回。",
                        "color"=>"#173177"
                    ];
                } else {
                    $SendData['keyword2'] = [
                        "value"=>"很抱歉，因您违反相关约定，现违约扣除保证金￥{$param['param']['money']}",
                        "color"=>"#173177"
                    ];
                }
                break;
            case 'auction_auction' :  //中拍消息
                $SendData = [
                    "first" => [
                        "value"=>'恭喜您在所参与的竞拍中胜出',//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['auction_name'],//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$param['param']['money'],
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>date('Y-m-d H:i', time()),
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"请及时支付尾款结算，过期违约将扣除保证金。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'lost_auction' :  //未中拍消息
                $SendData = [
                    "first" => [
                        "value"=>'很抱歉，您竞拍的商品竞拍失败',//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$param['param']['auction_name'],//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>'竞拍失败',
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>"已有人超过您的叫价价格",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"很抱歉，您竞拍的{$param['param']['auction_name']}商品竞拍失败，保证金将退回原路，预计1-3个工作日到，请查收。。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case 'beyond_price' :  //竞价超越消息
                $SendData = [
                    "first" => [
                        "value"=>'您好，您的竞价已被超过。',//$param['param']['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>"正在竞拍 {$param['param']['auction_name']}",//$param['param']['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$param['param']['current_price'],
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"立即前往加价",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case '':
                break;

            //...

            default :
                return $SendData;
                break;
        }
        return $SendData;
    }
}
