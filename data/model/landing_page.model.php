<?php
/**
 * 手机落地页模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class landing_pageModel extends Model{

    public function __construct() {
        parent::__construct('landing_page');
    }

    /**
     * 读取落地页列表
     * @param array $condition
     * @param string $page
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getLandingPageList($condition, $page = '', $order='id desc', $field='*', $limit = '') {
        return  $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 获取设置的落地页
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLandingPageInfo($condition, $field='*', $order = null) {
        $list = $this->field($field)->where($condition)->order($order)->find();
        return $list;
    }

    /**
     * 增加落地页记录
     * @param $addData
     * @return int
     */
    public function addData($addData){
        return $this->insert($addData);
    }

    /**
     * 更新
     * @param array $condition
     * @param array $update
     * @return boolean|unknown
     */
    public function updateLandingData($condition,$update){
        if (empty($condition)) {
            return true;
        }
        return $this->where($condition)->update($update);
    }

}
