<?php
/**
 * 根据url获取资讯信息
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @url       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class inform_info_by_urlModel extends Model {

    /**
     * 根据链接返回资讯信息
     */
    public function get_inform_info_by_url($url) {
        $url_host_name = self::get_url_domain($url);
        $store_host_name = self::get_url_domain(CMS_SITE_URL);
        switch ($url_host_name) {
//            case 'tmall.com':
//            case 'taobao.com':
//                if(C('taobao_api_isuse')) {
//                    return self::get_taobao_inform_info_by_url($url);
//                } else {
//                    return FALSE;
//                }
//                break;
            case $store_host_name:
                return self::get_store_inform_info_by_url($url);
            default:
                return FALSE;
                break;
        }
    }

    /**
     * 判断链接合法性
     */
    public function check_personal_buy_link($url) {
        $link_host_name = self::get_url_domain($url);
        $store_host_name = self::get_url_domain(SHOP_SITE_URL);
        switch ($link_host_name) {
            case 'tmall.com':
            case 'taobao.com':
                if(C('taobao_api_isuse')) {
                    return TRUE;
                } else {
                    return FALSE;
                }
                break;
            case $store_host_name:
                return TRUE;
            default:
                return FALSE;
                break;
        }
    }

    /**
     * 获取主域名
     */
    private function get_url_domain($url) {
        $url_parse_array = parse_url($url);
        $host = $url_parse_array['host'];
        $host_names = explode(".", $host);
        $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
        return $bottom_host_name;
    }



    private function get_store_inform_info_by_url($url) {
        $array = parse_url($url);
        $article_id = 0;
        if(isset($array['query'])){
            // 未开启伪静态
            parse_str($array['query'],$arr);
            $article_id = $arr['article_id'];
        }else{
            // 开启伪静态
            $data = explode('/', $array['path']);
            $path = end($data);
            $article_id = preg_replace('/item-(\d+)\.html/i', '$1', $path);
        }
        if(intval($article_id) > 0) {
            $model = Model('cms_article');
            $article_info = $model->getOne(array('article_id'=>intval($article_id)));
            if(!empty($article_info)) {
                $result = array();
                $result['result'] = 'true';
                $result['id'] = intval($article_id);
                $result['url'] = $url;
                $result['publisherid'] = $article_info['article_publisher_id'];
                $result['title'] = $article_info['article_title'];
                $result['img'] = $article_info['article_image'];
                $result['image'] = getCMSArticleImageUrl('',$article_info['article_image']);
                $result['abstract'] = $article_info['article_abstract'];
                $result['time'] = $article_info['article_publish_time'];
                return $result;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }

    }




}
