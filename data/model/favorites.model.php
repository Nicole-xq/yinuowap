<?php
/**
 * 买家收藏
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class favoritesModel extends Model{

    /**
     * 收藏列表
     *
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getFavoritesList($condition, $field = '*', $page = 0 , $order = 'log_id desc', $limit = 0) {
        return Model()->table('favorites')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

    /**
     * 收藏商品列表
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getGoodsFavoritesList($condition, $field = '*', $page = 0, $order = 'log_id desc') {
        $condition['fav_type'] = 'goods';
        return $this->getFavoritesList($condition, $field, $page, $order);
    }

    /**
     * 收藏拍品列表
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getAuctionsFavoritesList($condition, $field = '*', $page = 0, $order = 'log_id desc') {
        $condition['fav_type'] = 'auction';
        return $this->getFavoritesList($condition, $field, $page, $order);
    }

   /**
     * 收藏趣猜列表
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getGuessFavoritesList($condition, $field = '*', $page = 0, $order = 'log_id desc') {
        $condition['fav_type'] = 'guess';
        return $this->getFavoritesList($condition, $field, $page, $order);
    }

    /**
     * 接口商品收藏 含 商品 拍品 趣猜
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getWapFavoritesList($condition, $field = '*', $page = 0, $order = 'log_id desc') {
        $condition['fav_type'] = array('in', array('goods', 'auction', 'guess'));
        return $this->getFavoritesList($condition, $field, $page, $order);
    }

    /**
     * 获取收藏总数
     * @param $member_id
     * @return Model
     */
    public function getFavoritesCount($member_id){
        $condition['fav_type'] = array('in', array('goods', 'auction', 'guess'));
        $condition['member_id'] = $member_id;
        return Model()->table('favorites')->where($condition)->count();
    }
    /**
     * 收藏店铺列表
     * @param array $condition
     * @param treing $field
     * @param int $page
     * @param string $order
     * @return array
     */
    public function getStoreFavoritesList($condition, $field = '*', $page = 0, $order = 'log_id desc', $limit = 0) {
        $condition['fav_type'] = 'store';
        return $this->getFavoritesList($condition, $field, $page, $order, $limit);
    }

    /**
     * 获取用户收藏的店铺总数
     * @return Model
     */
    public function getStoreFavoritesCount($member_id){
        $condition['fav_type'] = 'store';
        $condition['member_id'] = $member_id;
        return Model()->table('favorites')->where($condition)-count();
    }

    /**
     * 取单个收藏的内容
     *
     * @param array $condition 查询条件
     * @return array 数组类型的返回结果
     */
    public function getOneFavorites($condition) {
        return Model()->table('favorites')->where($condition)->find();
    }

    /**
     * 获取店铺收藏数
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getStoreFavoritesCountByStoreId($storeId, $memberId = 0)
    {
        $where = array(
            'fav_type' => 'store',
            'fav_id' => $storeId,
        );

        if ($memberId > 0) {
            $where['member_id'] = (int) $memberId;
        }

        return (int) Model()->table('favorites')->where($where)->count();
    }

    /**
     * 获取商品收藏数
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getGoodsFavoritesCountByGoodsId($goodsId, $memberId = 0)
    {
        $where = array(
            'fav_type' => 'goods',
            'fav_id' => $goodsId,
        );

        if ($memberId > 0) {
            $where['member_id'] = (int) $memberId;
        }

        return (int) Model()->table('favorites')->where($where)->count();
    }

    /**
     * 获取商品收藏数
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getGoodsFavoritesCountByAuctionId($goodsId, $memberId = 0)
    {
        $where = array(
            'fav_type' => 'auction',
            'fav_id' => $goodsId,
        );

        if ($memberId > 0) {
            $where['member_id'] = (int) $memberId;
        }

        return (int) Model()->table('favorites')->where($where)->count();
    }
    
    /**
     * 获取店铺收藏数
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getStoreFavoritesCountByMemberId($member_id,$store_id = 0) {
        $where = array(
            'fav_type' => 'store',
            'member_id' => (int) $member_id
        );
        if (!empty($store_id)) {
            $where['fav_id'] = (int) $store_id;
        }
        return (int) Model()->table('favorites')->where($where)->count();
    }
    
    /**
     * 获取商品收藏数
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getGoodsFavoritesCountByMemberId($member_id)
    {
        $where = array(
            'fav_type' => 'goods',
            'member_id' => (int) $member_id
        );
        return (int) Model()->table('favorites')->where($where)->count();
    }

    /**
     * 新增收藏
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addFavorites($param) {
        if (empty($param)) {
            return false;
        }
        if ($param['fav_type'] == 'store') {
            $store_id = intval($param['fav_id']);
            $model_store = Model('store');
            $store = $model_store->getStoreInfoByID($store_id);
            $param['store_name'] = $store['store_name'];
            $param['store_id'] = $store['store_id'];
            $param['sc_id'] = $store['sc_id'];
        }
        if ($param['fav_type'] == 'goods') {
            $goods_id = intval($param['fav_id']);
            $model_goods = Model('goods');
            $fields = 'goods_id,store_id,goods_name,goods_image,goods_price,goods_promotion_price';
            $goods = $model_goods->getGoodsInfoByID($goods_id,$fields);
            $param['goods_name'] = $goods['goods_name'];
            $param['goods_image'] = $goods['goods_image'];
            $param['log_price'] = $goods['goods_promotion_price'];//商品收藏时价格
            $param['log_msg'] = $goods['goods_promotion_price'];//收藏备注，默认为收藏时价格，可修改
            $param['gc_id'] = $goods['gc_id'];

            $store_id = intval($goods['store_id']);
            $model_store = Model('store');
            $store = $model_store->getStoreInfoByID($store_id);
            $param['store_name'] = $store['store_name'];
            $param['store_id'] = $store['store_id'];
            $param['sc_id'] = $store['sc_id'];
        }
        if ($param['fav_type'] == 'auction') {
            $auction_id = intval($param['fav_id']);
            $auctions_model = Model('auctions');
            $auction_info = $auctions_model->getAuctionsInfoByID($auction_id);
            $param['goods_name'] = $auction_info['auction_name'];
            $param['goods_image'] = $auction_info['auction_image'];
            $param['log_price'] = $auction_info['auction_start_price'];//商品收藏时价格
            $param['log_msg'] = $auction_info['auction_start_price'];//收藏备注，默认为收藏时价格，可修改
            $param['gc_id'] = $auction_info['gc_id'];

            $store_id = intval($auction_info['store_id']);
            $model_store = Model('store');
            $store = $model_store->getStoreInfoByID($store_id);
            $param['store_name'] = $store['store_name'];
            $param['store_id'] = $store['store_id'];
            $param['sc_id'] = $store['sc_id'];
        }
        if ($param['fav_type'] == 'guess') {
            $gs_id = intval($param['fav_id']);
            $gs_model = Model('p_guess');
            $gs_info = $gs_model->getGuessInfo(array('gs_id' => $gs_id));
            $param['goods_name'] = $gs_info['gs_name'];
            $param['goods_image'] = $this->get_gs_img($gs_id);

            $store_id = intval($gs_info['store_id']);
            $model_store = Model('store');
            $store = $model_store->getStoreInfoByID($store_id);
            $param['store_name'] = $store['store_name'];
            $param['store_id'] = $store['store_id'];
            $param['sc_id'] = $store['sc_id'];
        }
        return Model()->table('favorites')->insert($param);
    }

    /**
     * 修改记录
     *
     * @param
     * @return bool
     */
    public function editFavorites($condition, $data) {
        if (empty($condition)) {
            return false;
        }
        if (is_array($data)) {
            $result = Model()->table('favorites')->where($condition)->update($data);
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 删除
     *
     * @param array $condition 查询条件
     * @return bool 布尔类型的返回结果
     */
    public function delFavorites($condition) {
        if (empty($condition)) {
            return false;
        }
        return Model()->table('favorites')->where($condition)->delete();
    }


      /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240, $_SESSION['store_id']);
    }   
}
