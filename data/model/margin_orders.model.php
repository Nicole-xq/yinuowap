<?php
/**
 * 保证金订单编号
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/20 0020
 * Time: 17:23
 * @author ADKi
 */


defined('InShopNC') or exit('Access Invalid!');

class margin_ordersModel extends Model
{
    const REFUND_STATE_REFUNDED = 3;
    //订单状态：0：未支付，1：已支付，2：提交线下支付凭证，3：线上支付部分支付，4：取消支付,5:支付失败
    const ORDER_STATE_DEFAULT = 0;
    const ORDER_STATE_PREPAID = 1;
    const ORDER_STATE_OFFLINE = 2;
    const ORDER_STATE_ONLINE = 3;
    const ORDER_STATE_CANCEL = 4;
    const ORDER_STATE_FAIL = 5;

    public function __construct(){
        parent::__construct('margin_orders');
    }

    /**
     * 新增
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addOrder($param){
        return Model('')->table('margin_orders')->insert($param);
    }

    /*
     * 获取保证金订单列表
     */
    public function get_margin_list ($condition, $field = '*', $limit = 0, $order = 'margin_id desc')
    {
        return Model()->table('margin_orders')
            ->field($field)->where($condition)
            ->order($order)->limit($limit)
            ->select();
    }

    /**
     * 获取订单详细内容
     * @param $condition array 条件数组
     * @param $fields string 字段名
     * @param $order string 排序
     * @return array
     * */
    public function getOrderInfo($condition = array(), $fields = '*', $order = '')
    {
        return Model('')->table('margin_orders')->where($condition)->field($fields)->order($order)->find();
    }

    /**
     * 编辑订单
     * @param $data array 更新的数据
     * @param $condition array 条件
     * */
    public function editOrder($data, $condition)
    {
        return Model('')->table('margin_orders')->where($condition)->update($data);
    }
    /**
     * 编辑订单 返回更新行数
     * @param $data array 更新的数据
     * @param $condition array 条件
     * @return int
     * */
    public function editOrderRetCount($data, $condition)
    {
        return Model('')->table('margin_orders')->where($condition)->updateRetCount($data);
    }

    /**
     * 获取订单列表
     * */
    public function getOrderList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $order_list = Model('')->table('margin_orders')->field($field)->where($condition)
            ->group($group)->order($order)->limit($limit)->page($page, $count)->select();
        //$model_store = Model('store');
        foreach ($order_list as $key => &$value) {
            // 追加店铺详细信息
            if ($value['store_id']>0) {
                $order_list[$key]['extend_store'] = Model("store")->getStoreInfoByID($value['store_id']);
                // 处理商品显示信息
                $order_list[$key]['image_60_url'] = cthumb($value['auction_image'], 60, $value['store_id']);
                $order_list[$key]['image_240_url'] = cthumb($value['auction_image'], 240, $value['store_id']);
                $order_list[$key]['auction_url'] = urlAuction('auctions','index',array('id'=>$value['auction_id']));
            }
            
            // 店铺支付方式名称
            if (isset($value['payment_code'])) {
                $order_list[$key]['payment_name'] = orderPaymentName($value['payment_code']);
            }
            // 支付状态显示
            if (isset($value['order_state'])) {
                $order_list[$key]['state_desc'] = orderMarginState($value);
                unset($value['order_state']);
            }
        }
        return $order_list;
    }
    public function getOrderList2($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $order_list = Model('')->table('margin_orders')->field($field)->where($condition)
            ->group($group)->order($order)->limit($limit)->page($page, $count)->select();        
        return $order_list;
    }
    /**
     * 获取订单列表
     * */
    public function orderListFetch($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $order_list = Model('')->table('margin_orders')->field($field)->where($condition)->group($group)
            ->order($order)->limit($limit)->page($page, $count)->select();
        foreach ($order_list as $key => &$value) {
            // 店铺支付方式名称
            if (isset($value['payment_code'])) {
                $order_list[$key]['payment_name'] = orderPaymentName($value['payment_code']);
            }
            // 支付状态显示
            if (isset($value['order_state'])) {
                $order_list[$key]['state_desc'] = orderMarginState($value);
                unset($value['order_state']);
            }
        }
        return $order_list;
    }

    /**
     * 获取用户参拍数量
     * @return [type] [description]
     */
    public function member_margin_count($member_id){
        $condition = array('buyer_id' => $member_id, 'refund_state' => 0, 'lock_state' => 1);
        return Model('')->table('margin_orders')->where($condition)->count();
    }

    /**
     * 获取用户参拍数量
     * @return [type] [description]
     */
    public function getOrderCount($condition)
    {
        return Model('')->table('margin_orders')->where($condition)->count();
    }


    /**
     * 计算保证金金额统计
     * @param array $condition
     * @param string $fields
     * @return int
     */
    public function getMarginSum($condition = array(),$fields = 'api_pay_amount') {
        return Model('')->table('margin_orders')->where($condition)->sum($fields);
    }


    /**
     * 返回是否允许某些操作
     * @param $operate string 操作代码
     * @param $order_info array 订单详情
     * @return bool
     * */
    public function getOrderOperateState($operate, $order_info)
    {

        if (!is_array($order_info) || empty($order_info)) {
            return false;
        }

        if (isset($order_info['if_'.$operate])) {
            return $order_info['if_'.$operate];
        }
        // 判断是否显示按钮，返回 true 是显示
        switch ($operate) {

            //买家取消订单
            case 'buyer_cancel':
                $state = ($order_info['order_state'] == 0) || ($order_info['refund_state'] = 0);
                break;

            //商家取消订单
            case 'store_cancel':
                $state = ($order_info['order_state'] == 0) || ($order_info['refund_state'] = 0);
                break;

            //平台取消订单
            case 'system_cancel':
                $state = (!in_array($order_info['order_state'],array(1,4,5))) || ($order_info['refund_state'] = 0);
                break;

            //平台收款
            case 'system_receive_pay':
                $state = ($order_info['order_state'] == 3 || $order_info['order_state'] == 0);
                $state = $state && $order_info['payment_code'] == 'online' && $order_info['api_pay_time'];
                break;

            //平台确认线下订单
            case 'system_underline_confirm':
                $state = ($order_info['order_type'] == 1);
                $state = $state && ($order_info['order_state'] == 2);
                break;

            //显示支付
            case 'payment':
                $state = $order_info['order_state'] == 0 && $order_info['payment_code'] == 'online';
                $state  =$state || ($order_info['order_state'] == 3 && $order_info['payment_code'] == 'online');
                break;

            //锁定
            case 'lock':
                $state = intval($order_info['lock_state']) ? true : false;
                break;
        }
        return $state;
    }

    /**
     * 取得订单列表(未被删除)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getNormalOrderList($condition, $pagesize = '', $field = '*', $order = 'order_id desc', $limit = '', $extend = array()){
        return $this->getMemberOrderList($condition, $pagesize, $field, $order, $limit, $extend);
    }

    /**
     * 取得订单列表(所有)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getMemberOrderList($condition, $pagesize = '', $field = '*', $order = 'order_id desc', $limit = '', $extend = array(), $master = false){
        $list = Model()->table('margin_orders')->field($field)->where($condition)->page($pagesize)->order($order)->limit($limit)->master($master)->select();
        if (empty($list)) return array();
        $order_list = array();
        foreach ($list as $order) {
            if (isset($order['order_state'])) {
                $order['state_desc'] = orderMarginState($order);
            }
            if (isset($order['payment_code'])) {
                $order['payment_name'] = orderPaymentName($order['payment_code']);
            }
            if (!empty($extend)) $order_list[$order['auction_id']] = $order;
        }
        if (empty($order_list)) $order_list = $list;

        //追加返回商品信息
        if (in_array('auctions',$extend)) {
            //取商品列表
            $order_goods_list = $this->getOrderAuctionsList(array('auction_id'=>array('in',array_keys($order_list))));
            if (!empty($order_goods_list)) {
                foreach ($order_goods_list as $value) {
                    $order_list[$value['auction_id']]['extend_order_auctions'][] = $value;
                }
            } else {
                $order_list[$value['auction_id']]['extend_order_auctions'] = array();
            }
        }

        return $order_list;
    }

    /**
     * 取得订单商品表列表
     * @param unknown $condition
     * @param string $fields
     * @param string $limit
     * @param string $page
     * @param string $order
     * @param string $group
     * @param string $key
     */
    public function getOrderAuctionsList($condition = array(), $fields = '*', $limit = null, $page = null, $order = 'auction_id desc', $group = null, $key = null) {
        return Model()->table('auctions')->field($fields)->where($condition)->limit($limit)->order($order)->group($group)->key($key)->page($page)->select();
    }

    public function getList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $order_list = Model('')->table('margin_orders')->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
        return $order_list;
    }

    /**
     * 获取用户成功提交保证金的总金额
     * @param $member_id
     * @param $auction_id
     * @Date: 2018/12/26 0026
     * @author: Mr.Liu
     */
    public function marginCountFetch($member_id, $auction_id)
    {
        $condition = ['buyer_id' => $member_id, 'auction_id' => $auction_id, 'order_state' => 1];
        return Model('')->table('margin_orders')->where($condition)->sum('margin_amount') ?: 0;
    }
}
