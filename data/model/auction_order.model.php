<?php
/**
 * 
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class auction_orderModel extends Model{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 增加
     *
     * @param
     * @return int
     */
    public function add($array) {
        $id = Model()->table('auction_order')->insert($array);
        return $id;
    }

    /**
     * 获取列表
     */
    public function getList($condition = array(),$page = 0, $field = '*', $order = 'auction_order_id desc', $limit = 0, $group = ''){
        return Model()->table('auction_order')->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page)->select();
    }

    /**
     * 取得订单数量
     * @param unknown $condition
     */
    public function getOrderCount($condition) {
        return Model()->table('auction_order')->where($condition)->count();
    }

    /**
     * 获取详情
     */
    public function getInfo($condition, $field = '*', $order = 'auction_order_id desc'){
        $result = Model()->table('auction_order')->field($field)->where($condition)->order($order)->find();
        // 添加订单状态
        // 支付方式名称
        if (isset($result['payment_code'])) {
            $result['payment_name'] = orderPaymentName($result['payment_code']);
        }
        // 支付状态显示
        if (isset($result['order_state'])) {
            $result['state_desc'] = orderState($result);
        }
        return $result;
    }

    /*
     * 编辑订单信息
     * */
    public function setOrder($condition, $update)
    {
        $update = Model()->table('auction_order')->where($condition)->update($update);
        return $update;
    }


    /**
     * 取得订单列表(未被删除)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getNormalOrderList($condition, $pagesize = '', $field = '*', $order = 'auction_order_id desc', $limit = '', $extend = array()){
        return $this->getOrderList($condition, $pagesize, $field, $order, $limit, $extend);
    }

    /**
     * 取得订单列表(所有)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getOrderList($condition, $pagesize = '', $field = '*', $order = 'auction_order_id desc', $limit = '', $extend = array(), $master = false){
        $list = Model()->table('auction_order')->field($field)->where($condition)->page($pagesize)->order($order)->limit($limit)->master($master)->select();
        if (empty($list)) return array();
        $order_list = array();
        foreach ($list as $order) {
            if (isset($order['order_state'])) {
                $order['state_desc'] = orderState($order);
            }
            if (isset($order['payment_code'])) {
                $order['payment_name'] = orderPaymentName($order['payment_code']);
            }
            if (!empty($extend)) $order_list[$order['auction_id']] = $order;
        }
        if (empty($order_list)) $order_list = $list;

        //追加返回商品信息
        if (in_array('auctions',$extend)) {
            //取商品列表
            $order_goods_list = $this->getOrderAuctionsList(array('auction_id'=>array('in',array_keys($order_list))));
            if (!empty($order_goods_list)) {
                foreach ($order_goods_list as $value) {
                    $order_list[$value['auction_id']]['extend_order_auction'][] = $value;
                }
            } else {
                $order_list[$value['auction_id']]['extend_order_auction'] = array();
            }
        }

        return $order_list;
    }

    /**
     * 取得订单商品表列表
     * @param unknown $condition
     * @param string $fields
     * @param string $limit
     * @param string $page
     * @param string $order
     * @param string $group
     * @param string $key
     */
    public function getOrderAuctionsList($condition = array(), $fields = '*', $limit = null, $page = null, $order = 'auction_id desc', $group = null, $key = null) {
        return Model()->table('auctions')->field($fields)->where($condition)->limit($limit)->order($order)->group($group)->key($key)->page($page)->select();
    }
}