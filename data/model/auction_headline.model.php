<?php
/**
 * 拍卖头条模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class auction_headlineModel extends Model {


    public function __construct() {
        parent::__construct('auction_headline');
    }


    /**
     * 查询拍卖头条列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getAu_headList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 拍卖头条数量
     * @param array $condition
     * @return int
     */
    public function getAu_headCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条拍卖头条信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getAu_headInfo($condition) {
        $au_head_info = $this->where($condition)->find();
        return $au_head_info;
    }

    /**
     * 添加拍卖头条
     * @param unknown $insert
     * @return boolean
     */
    public function addAu_head($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新拍卖头条
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editAu_head($update, $condition) {
        return $this->where($condition)->update($update);
    }
    /**
     * 删除
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function del($id){
        if (intval($id) > 0){
            $where = " au_head_id = '". intval($id) ."'";
            $result = $this->delete1('auction_headline',$where);
            return $result;
        }else {
            return false;
        }
    }


    //多条编辑
    public function all_edit($update, $condition){
        $au_head_list = $this->getAu_headList($condition, 0,'','au_head_id');
        if (empty($au_head_list)) {
            return false;
        }
        $commonid_array = array();
        foreach ($au_head_list as $val) {
            $commonid_array[] = $val['au_head_id'];
        }
        if (empty($commonid_array)) {
            return true;
        }
        $condition['au_head_id'] = array('in', $commonid_array);
        $result = Model()->table('auction_headline')->where($condition)->update($update);
        return $result;
    }



}
