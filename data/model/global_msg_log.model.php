<?php
/**
 * 全局消息发送日志
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class global_msg_logModel extends Model{
    public function __construct() {
        parent::__construct('global_msg_log');
    }

    /**
     * 全局发送消息列表
     * @param array $condition
     * @param string $field
     * @param string $group
     * @param integer $page
     * @param string $order
     * @return  array
     */
    public function getGlobalMsgLogList($condition, $field = '*', $group = '', $order = 'id ASC', $page = 0) {
        return $this->field($field)->where($condition)->group($group)->order($order)->page($page)->select();
    }

    /**
     * 全局发送消息详情
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getMemberMsgLogInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 增加全局发送消息日志
     * @param $global_array
     * @return int
     */
    public function addGlobalMsgLog($global_array) {
        return $this->insert($global_array);
    }

}
