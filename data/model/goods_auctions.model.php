<?php
/**
 * 拍卖池模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

defined('InShopNC') or exit('Access Invalid!');

class goods_auctionsModel extends Model
{
    //拍卖池拍品状态信息
    private $auctionState = [
        "-1" => '拍品状态',
        "0"  => '未审核',
        "1"  => '审核中',
        "2"  => '审核通过',
        "3"  => '审核失败',
    ];

    //拍卖池拍品是否关联专场状态
    private $linkSpecialState = [
        '-1' => '当前关联专场',
        '0'  => '未关联',
        '1'  => '已关联',
    ];

    public function __construct() {
        parent::__construct('goods_auctions');
    }

 /**
 * 拍品列表
 *
 * @param  array   $condition  检索条件
 * @param  string  $field
 * @param  integer  $page
 * @param  string  $count
 * @param  string  $order
 * @return array  返回二位数组
 */
    public function getGoodsAuctionList($condition, $field = '*', $page = 10, $count = '', $order = "id desc") {
        return  $this->field($field)->where($condition)->order($order)->limit(false)->page($page, $count)->select();
    }

    /**
     * 拍品明细
     *
     * @param  array   $condition  检索条件
     * @param  string  $field
     * @param  string  $get_field
     * @return array  返回二位数组
     */
    public function getGoodsAuctionInfo($condition, $field = '*', $get_field = '') {
        $result = $this->field($field)->where($condition)->find();
        return  $get_field ? $result[$get_field] : $result;
    }

    /**
     * 更新信息
     * @param array $data
     * @param array $condition
     * @return boolean
     */
    public function updateGoodsAuctions($data = array(), $condition = array()) {
        return $this->where($condition)->update($data);
    }

     /**
     * 删除拍卖池数据
     * @param array $condition
     * @return boolean
     */
    public function delGoodsAuctions($condition) {
        return $this->where($condition)->delete();
    }

    /**
     * 新增
     * @param array $insert
     * @return boolean
     */
    public function addGoodsAuctions($insert) {
        return $this->insert($insert);
    }

    /**
     * 获取拍品状态
     * @return array
     */
    public function getAuctionStatus() {
        return $this->auctionState;
    }

    /**
     * 获取拍品关联专场状态
     * @return array
     */
    public function getLinkSpecialStatus() {
        return $this->linkSpecialState;
    }


}
