<?php
/**
 * 区域代理考核配置模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class agent_assessmentModel extends Model {

    public function __construct(){
        parent::__construct('area_agent_assessment_set');
    }

    /**
     * 获取区域代理考核配置列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getAgentAssessmentList($condition = [], $field = '*', $page = null, $order = 'id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 添加区域代理考核配置记录
     * @param array $data
     * @return mixed
     */
    public function addAgentAssessment($data){
        return $this->insert($data);
    }

    /**
     * 修改区域代理考核配置记录数据
     * @param $conditions
     * @param $data
     * @return bool
     */
    public function updateAgentAssessment($conditions,$data){
        return $this->where($conditions)->update($data);
    }

    /**
     * 获取区域代理考核配置详情
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAgentAssessmentInfo($condition = array(), $field = '*'){
        return $this->field($field)->where($condition)->find();
    }

}