<?php
/**
 * 艺术家拍卖模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class artist_vendueModel extends Model {


    public function __construct() {
        parent::__construct('artist_vendue');
    }


    /**
     * 查询商家拍卖列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getArtist_vendueList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = Model()->table('artist_vendue')->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }

    public function getArtist_StoreInfo($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'artist_vendue.store_vendue_id = store_vendue.store_vendue_id';
        $result = Model()->table('artist_vendue,store_vendue')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->find();
        return $result;
    }

    public function getArtist_StoreList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'artist_vendue.store_vendue_id = store_vendue.store_vendue_id';
        $result = Model()->table('artist_vendue,store_vendue')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    public function getArtist_vendueJoinList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'artist_vendue.artist_classify = artist_classify.artist_classify_id,artist_vendue.artist_grade=artist_grade.artist_grade_id';
        $result = Model()->table('artist_vendue,artist_classify,artist_grade')->field($field)->join('inner,inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    public function getArtist_vendueJoinInfo($condition, $order = '', $field = '*') {
         $on = 'artist_vendue.artist_classify = artist_classify.artist_classify_id,artist_vendue.artist_grade=artist_grade.artist_grade_id';
         $result = Model()->table('artist_vendue,artist_classify,artist_grade')->field($field)->join('inner,inner')->on($on)->where($condition)->order($order)->find();
        return $result;
    }
    public function getArtist_vendueClassifyInfo($condition, $order = '', $field = '*') {
    $on = 'artist_vendue.artist_classify = artist_classify.artist_classify_id';
    $result = Model()->table('artist_vendue,artist_classify')->field($field)->join('inner')->on($on)->where($condition)->order($order)->find();
    return $result;
}
    public function getArtist_vendueGradeInfo($condition, $order = '', $field = '*') {
        $on = 'artist_vendue.artist_grade=artist_grade.artist_grade_id';
        $result = Model()->table('artist_vendue,artist_grade')->field($field)->join('inner')->on($on)->where($condition)->order($order)->find();
        return $result;
    }
    public function getArtistInfo($condition, $order = '', $field = '*') {
        $result = Model()->table('artist_vendue')->field($field)->where($condition)->order($order)->find();
        return $result;
    }





    /**
     * 商家拍卖数量
     * @param array $condition
     * @return int
     */
    public function getArtist_vendueCount($condition) {
        return Model()->table('artist_vendue')->where($condition)->count();
    }


    /**
     * 查询店铺信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getArtist_vendueInfo($condition) {
        $artist_vendue_info = Model()->table('artist_vendue')->where($condition)->find();
        return $artist_vendue_info;
    }

    /**
     * 添加商家拍卖
     * @param unknown $insert
     * @return boolean
     */
    public function addArtist_vendue($insert) {
        return Model()->table('artist_vendue')->insert($insert);
    }

    /**
     * 更新商家拍卖
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editArtist_vendue($update, $condition) {
        return Model()->table('artist_vendue')->where($condition)->update($update);
    }

    /**
     * 艺术家列表(临时使用,排除店铺关闭的艺术家)
     * @param $condition
     * @param null $page
     * @param string $order
     * @param string $field
     * @param string $limit
     * @return mixed
     * @todo 整理规则,预计此方法临时使用
     */
    public function getArtist_StoreWebList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'artist_vendue.store_vendue_id = store_vendue.store_vendue_id,store_vendue.store_id = store.store_id';
        $result = Model()->table('artist_vendue,store_vendue,store')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


}
