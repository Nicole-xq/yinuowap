<?php
/**
 * 保证金使用日志
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/13 0013
 * Time: 15:35
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');

class margin_logModel extends Model
{
    public function __construct()
    {
        parent::__construct('margin_log');
    }

    /**
     * 添加日志
     * @param $insert array
     * @return bool
     * */
    public function addLog($insert)
    {
        $insert['add_time'] = time();
        return Model()->table('margin_log')->insert($insert);
    }

    /**
     * 获取使用日志列表
     *
     * @param array $condition 条件数组
     * @param int $pageSize 分页长度
     *
     * @return array 使用日志列表
     */
    public function getLogList($condition, $pageSize = 20, $limit = null, $sort = 'log_id desc')
    {
        if ($condition) {
            $this->where($condition);
        }

        if ($sort) {
            $this->order($sort);
        }

        if ($limit) {
            $this->limit($limit);
        } else {
            $this->page($pageSize);
        }

        return $this->select();
    }
}