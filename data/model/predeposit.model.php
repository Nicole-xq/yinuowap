<?php
/**
 * 预存款
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class predepositModel extends Model {
    /**
     * 生成充值编号
     * @return string
     */
    public function makeSn() {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $_SESSION['member_id'] % 1000);
    }

    public function addRechargeCard($sn, array $session)
    {
        $memberId = (int) $session['member_id'];
        $memberName = $session['member_name'];

        if ($memberId < 1 || !$memberName) {
            throw new Exception("当前登录状态为未登录，不能使用充值卡");
        }

        $rechargecard_model = Model('rechargecard');

        $card = $rechargecard_model->getRechargeCardBySN($sn);

        if (empty($card) || $card['state'] != 0 || $card['member_id'] != 0) {
            throw new Exception("充值卡不存在或已被使用");
        }

        $card['member_id'] = $memberId;
        $card['member_name'] = $memberName;

        try {
            $this->beginTransaction();

            $rechargecard_model->setRechargeCardUsedById($card['id'], $memberId, $memberName);

            $card['amount'] = $card['denomination'];
            $this->changeRcb('recharge', $card);

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * 取得充值列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdRechargeList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return Model()->table('pd_recharge')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加充值记录
     * @param array $data
     */
    public function addPdRecharge($data) {
        return Model()->table('pd_recharge')->insert($data);
    }

    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdRecharge($data,$condition = array()) {
        return Model()->table('pd_recharge')->where($condition)->update($data);
    }

    /**
     * 取得单条充值信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdRechargeInfo($condition = array(), $fields = '*',$lock = false) {
        return Model()->table('pd_recharge')->where($condition)->field($fields)->lock($lock)->find();
    }

    /**
     * 计算充值金额
     * @param array $condition
     * @param string $fields
     * @return int
     */
    public function getPdRechargeSum($condition = array(),$fields = 'pdr_amount') {
        return Model()->table('pd_recharge')->where($condition)->sum($fields);
    }
    /**
     * 取充值信息总数
     * @param unknown $condition
     */
    public function getPdRechargeCount($condition = array()) {
        return Model()->table('pd_recharge')->where($condition)->count();
    }

    /**
     * 计算提现金额
     * @param array $condition
     * @param string $fields
     * @return int
     */
    public function getPdCashSum($condition = array(),$fields = 'pdc_amount') {
        return Model()->table('pd_cash')->where($condition)->sum($fields);
    }

    /**
     * 取提现单信息总数
     * @param unknown $condition
     */
    public function getPdCashCount($condition = array()) {
        return Model()->table('pd_cash')->where($condition)->count();
    }

    /**
     * 取日志总数
     * @param unknown $condition
     */
    public function getPdLogCount($condition = array()) {
        return Model()->table('pd_log')->where($condition)->count();
    }

    /**
     * 计算金额
     * @param array $condition
     * @param string $fields
     * @return int
     */
    public function getPdLogSum($condition = array(),$fields = 'lg_av_amount') {
        return Model()->table('pd_log')->where($condition)->sum($fields);
    }

    /**
     * 取得预存款变更日志列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdLogList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return Model()->table('pd_log')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 获取得预存款变更日志单条数据
     * @param array $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     * @param string $limit
     * @return mixed
     * @Date: 2019/5/7 0007
     * @author: Mr.Liu
     */
    public function getPdLogDetail($condition = array(), $fields = '*') {
        return Model()->table('pd_log')->where($condition)->field($fields)->find();
    }

    /**
     * 变更充值卡余额
     *
     * @param string $type
     * @param array  $data
     *
     * @return mixed
     * @throws Exception
     */
    public function changeRcb($type, $data = array())
    {
        $amount = (float) $data['amount'];
        if ($amount < .01) {
            throw new Exception('参数错误');
        }

        $available = $freeze = 0;
        $desc = null;

        switch ($type) {
        case 'order_pay':
            $available = -$amount;
            $desc = '下单，使用充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_freeze':
            $available = -$amount;
            $freeze = $amount;
            $desc = '下单，冻结充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_cancel':
            $available = $amount;
            $freeze = -$amount;
            $desc = '取消订单，解冻充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_comb_pay':
            $freeze = -$amount;
            $desc = '下单，扣除被冻结的充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'recharge':
            $available = $amount;
            $desc = '平台充值卡充值，充值卡号: ' . $data['sn'];
            break;

        case 'refund':
            $available = $amount;
            $desc = '确认退款，订单号: ' . $data['order_sn'];
            break;

        case 'vr_refund':
            $available = $amount;
            $desc = '虚拟兑码退款成功，订单号: ' . $data['order_sn'];
            break;

        case 'order_book_cancel':
            $available = $amount;
            $desc = '取消预定订单，退还充值卡余额，订单号: ' . $data['order_sn'];
            break;

        default:
            throw new Exception('参数错误');
        }

        $update = array();
        if ($available) {
            $update['available_rc_balance'] = array('exp', "available_rc_balance + ({$available})");
        }
        if ($freeze) {
            $update['freeze_rc_balance'] = array('exp', "freeze_rc_balance + ({$freeze})");
        }

        if (!$update) {
            throw new Exception('参数错误');
        }

        // 更新会员
        $updateSuccess = Model('member')->editMember(array(
            'member_id' => $data['member_id'],
            "available_rc_balance"=>["egt", "0"], "freeze_rc_balance" => ["egt", "0"]
        ), $update, true);

        if ($updateSuccess < 1) {
            throw new Exception('操作失败');
        }

        // 添加日志
        $log = array(
            'member_id' => $data['member_id'],
            'member_name' => $data['member_name'],
            'type' => $type,
            'add_time' => TIMESTAMP,
            'available_amount' => $available,
            'freeze_amount' => $freeze,
            'description' => $desc,
        );

        $insertSuccess = Model()->table('rcb_log')->insert($log);
        if (!$insertSuccess) {
            throw new Exception('操作失败');
        }

        $msg = array(
            'code' => 'recharge_card_balance_change',
            'member_id' => $data['member_id'],
            'param' => array(
                'time' => date('Y-m-d H:i:s', TIMESTAMP),
                'url' => urlMember('predeposit', 'rcb_log_list'),
                'available_amount' => ncPriceFormat($available),
                'freeze_amount' => ncPriceFormat($freeze),
                'description' => $desc,
            ),
        );

        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
                'consume_amount'=>$amount,'consume_time'=>time(),'consume_remark'=>$desc));
        // 发送买家消息
        QueueClient::push('sendMemberMsg', $msg);

        return $insertSuccess;
    }

    /**
     * 变更预存款 !外部事务!
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array(), $order_info= []) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();

        $data_log['lg_member_id'] = $data['member_id'];
        $data_log['lg_member_name'] = $data['member_name'];
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
        switch ($change_type){
            case 'order_pay':
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '下单，支付预存款，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'auction_order_pay':
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '下单，支付预存款，保证金订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'order_freeze':
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = $data['amount'];
                $data_log['lg_desc'] = '下单，冻结预存款，订单号: '.$data['order_sn'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit+'.$data['amount']);
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'order_cancel':
                $data_log['lg_av_amount'] = $data['amount'];
                //保证金订单取消,是混合支付时才解冻
                if(!empty($order_info['margin_id']) && $order_info['order_state'] == 3){
                    $data_log['lg_freeze_amount'] = -$data['amount'];
                }
                $data_log['lg_desc'] = '取消订单，解冻预存款，订单号: '.$data['order_sn'];
                //保证金订单取消,是混合支付时才解冻
                if(!empty($order_info['margin_id']) && $order_info['order_state'] == 3) {
                    $data_pd['freeze_predeposit'] = array('exp', 'freeze_predeposit-' . $data['amount']);
                }
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                //保证金订单取消,是混合支付时才解冻
                if(!empty($order_info['margin_id']) && $order_info['order_state'] == 3) {
                    $data_msg['freeze_amount'] = -$data['amount'];
                }
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'order_cancel2':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_freeze_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '退款，订单号: '.$data['order_sn'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = -$data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'order_comb_pay':
                $data_log['lg_freeze_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '下单，支付被冻结的预存款，订单号: '.$data['order_sn'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = 0;
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'recharge':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '充值，充值单号: '.$data['pdr_sn'];
                $data_log['lg_admin_name'] = $data['admin_name'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;

            case 'refund':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '确认退款，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;

            case 'auction_refund':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '拍卖订单确认退款，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'vr_refund':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '虚拟兑码退款成功，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'auction_refund72':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '商家违约，拍卖退款成功，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'auction_3':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '拍卖保证金利息，保证金订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'cash_apply':
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = $data['amount'];
                $data_log['lg_desc'] = '申请提现，冻结预存款，提现单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'cash_pay':
                $data_log['lg_freeze_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '提现成功，提现单号: '.$data['order_sn'];
                $data_log['lg_admin_name'] = $data['admin_name'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = 0;
                $data_msg['freeze_amount'] = -$data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'cash_del':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_freeze_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '取消提现申请，解冻预存款，提现单号: '.$data['order_sn'];
                $data_log['lg_admin_name'] = $data['admin_name'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = -$data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'order_book_cancel':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '取消预定订单，退还预存款，订单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
            case 'margin_to_pd':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = '保证金释放到预存款,保证金订单号:'.$data['order_sn'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_pd['freeze_margin'] = array('exp','freeze_margin-'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = '保证金释放到预存款';
                break;
            case 'member_distribute_upgrade_lv' :
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = $data['msg'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']); //可用余额
                $data_pd['available_commis'] = array('exp','available_commis+'.$data['amount']);        //已返佣金

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = '下级分销升级';
                break;
            case 'member_distribute_buy_good' :
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = $data['msg'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);//可用余额
                $data_pd['available_commis'] = array('exp','available_commis+'.$data['amount']);        //已返佣金
                $data_pd['freeze_commis'] = array('exp','freeze_commis-'.$data['amount']);        //代返佣金

                $data_msg['av_amount'] = $data['amount'];

                if($data['commission_type'] == 1){
                    $data_msg['desc'] = '商品分销返佣';
                }else{
                    $data_msg['desc'] = '拍卖商品分销返佣';
                }
                break;
            case 'member_distribute_create_artist':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = $data['msg'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']); //可用余额
                $data_pd['available_commis'] = array('exp','available_commis+'.$data['amount']);        //已返佣金

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = '下级成为艺术家';
                break;
            case 'member_margin_commission' : //下级保证金返佣
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = $data['msg'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']); //可用余额
                $data_pd['available_commis'] = array('exp','available_commis+'.$data['amount']);        //已返佣金

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = '下级保证金返佣';
                break;
            case 'auction_offer_commission' : //拍卖出价返佣
                $data_log['lg_av_amount'] = $data['amunt'];
                $data_log['lg_desc'] = $data['msg'];

                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']); //可用余额
                $data_pd['available_commis'] = array('exp','available_commis+'.$data['amount']);        //已返佣金

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['desc'] = '下级保证金返佣';
                break;
            default:
                throw new Exception('参数错误');
                break;
        }
//        if($status == 1){
//            file_put_contents(BASE_DATA_PATH . '/log/note.txt', print_r($data_pd, true));
        /** @var memberModel $member_model */
        $member_model = Model('member');
        //这里保证 余额 和冻结金额不溢出
            $update = $member_model->editMember(array('member_id'=>$data['member_id'], "available_predeposit"=>["egt", "0"], "freeze_predeposit" => ["egt", "0"]), $data_pd, true);

            if ($update < 1) {
                throw new Exception('操作失败1');
            }
//        }
        $update = $member_model->field('member_id,available_predeposit,freeze_predeposit')->where(array('member_id'=>$data['member_id']))->find();
        $data_log['lg_available_amount'] = !empty($update)?$update['available_predeposit']:'0.00';
        $data_log['lg_freeze_predeposit'] = !empty($update)?$update['freeze_predeposit']:'0.00';
        //output_data($data_log);
        if ($change_type == 'auction_order_pay' && !empty($order_info)) {
            //margin_list 插入记录 list记录表  结算编号
            $auction_id = isset($order_info['auction_id']) ?: 0;
            if ($auction_id) {
                /** @var margin_listModel $margin_list_model */
                $margin_list_model = Model('margin_list');
                $condition = [
                    'buyer_id' => $data['member_id'],
                    'auction_id' => $order_info['auction_id']
                ];
                if (!empty($condition)) {
                    $margin_list_data = $margin_list_model->where($condition)->find();
                    if ($margin_list_data) {
                        //$update = ['auction_click' => array('exp', 'auction_click + '.$click)];
                        $update = [
                            'all_money' => ['exp', 'all_money + ' . $data['amount']],
                            'pay_num' => ['exp', 'pay_num + ' . 1],
                            'last_pay_time' => TIMESTAMP,
                            'last_payment_code' => $order_info['payment_code']
                        ];
                        $margin_list_model->where($condition)->update($update);
                    } else {
                        $list_insert = [
                            'list_sn' => $order_info['order_sn'],
                            'buyer_id' => $data['member_id'],
                            'buyer_name' => $data['member_name'],
                            'all_money' => $data['amount'],
                            'pay_num' => 1,
                            'last_payment_code' => $order_info['payment_code'],
                            'last_pay_time' => TIMESTAMP,
                            'auction_id' => $order_info['auction_id'],
                            'statue' => 1,
                            'create_time' => TIMESTAMP,
                        ];
                        $margin_list_model->insert($list_insert);
                    }
                }
            }
        }
        $insert = Model()->table('pd_log')->insert($data_log);
        //Model('margin_orders')->editOrder(array('lg_id'=>$insert),array('margin_id'=>$data['margin_id']));
        if (!$insert) {
            throw new Exception('操作失败2');
        }
            // 支付成功发送买家消息
            $param = array();
            $param['code'] = 'predeposit_change';
            $param['member_id'] = $data['member_id'];
            $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
            $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
            $param['param'] = $data_msg;
            QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
                'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
            QueueClient::push('sendMemberMsg', $param);
        return $insert;
    }


    public function insertPdLog($commissionData)
    {
        $pdModel = Model('predeposit');
        $insertData = [];
        $insertData['lg_member_id'] = $commissionData['dis_member_id'];
        $insertData['lg_member_name'] = $commissionData['dis_member_name'];
        $insertData['lg_add_time'] = TIMESTAMP;
        $insertData['lg_type'] = 'offer_comm';
        $insertData['lg_av_amount'] = $commissionData['commission_amount'];
        $insertData['lg_available_amount'] = $commissionData['lg_available_amount'];
        $insertData['lg_desc'] = $pdModel->commissionType[$commissionData['commission_type']] . $commissionData['commission_amount'];
        $insertData['lg_desc'] .= '返佣来源人 :' . $commissionData['from_member_name'];
        return Model()->table('pd_log')->insert($insertData);
    }

    /**
     * 删除充值记录
     * @param unknown $condition
     */
    public function delPdRecharge($condition) {
        return Model()->table('pd_recharge')->where($condition)->delete();
    }

    /**
     * 获取累加保证金收益
     * @param $member_id int 会员ID
     * @return int 累计金额
     * */
    public function get_cumulative_margin($member_id)
    {
        $log_list = Model()->table('pd_log')->where(array('lg_type' => 'auction_3', 'lg_member_id' => $member_id))->select();
        $all_margin = 0;
        foreach ($log_list as $log_info) {
            $all_margin += $log_info['lg_av_amount'];
        }
        return $all_margin;
    }

    /**
     * 获取保证金收益列表
     * @
     * */
    public function get_cumulative_margin_list($member_id)
    {
        $log_list = Model()->table('pd_log')->where(array('lg_type' => 'auction_3', 'lg_member_id' => $member_id))->order('lg_add_time desc')->select();
        return $log_list;
    }
    
    /**
     * 取得提现列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdCashList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return Model()->table('pd_cash')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加提现记录
     * @param array $data
     */
    public function addPdCash($data) {
        return Model()->table('pd_cash')->insert($data);
    }

    /**
     * 编辑提现记录
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdCash($data,$condition = array()) {
        return Model()->table('pd_cash')->where($condition)->update($data);
    }

    /**
     * 取得单条提现信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdCashInfo($condition = array(), $fields = '*') {
        return Model()->table('pd_cash')->where($condition)->field($fields)->find();
    }

    /**
     * 删除提现记录
     * @param unknown $condition
     */
    public function delPdCash($condition) {
        return Model()->table('pd_cash')->where($condition)->delete();
    }
}
