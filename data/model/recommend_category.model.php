<?php
/**
 * 地区模型
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release  v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class recommend_categoryModel extends Model {

    public function __construct() {
        parent::__construct('store_recommend_category');
    }

    public function get_list($order = ''){
        return $this->where(array('status'=>0))->order($order)->select();
    }
    public function get_info($id){
        return $this->where(array('id'=>$id))->find();
    }
    public function update_info($id,$param){
        return $this->where(array('id'=>$id))->update($param);
    }
    public function add_info($param){
        return $this->insert($param);
    }

}
