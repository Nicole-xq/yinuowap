<?php
/**
 * 区域代理订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class agent_statementModel extends Model {

    public function __construct(){
        parent::__construct('agent_statement');
    }

    public function addStatement($insert){
        $this->insert($insert);
    }

    public function getList($condition = [], $field = '*', $page = null, $order = 'id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    public function getInfo($condition){
        return $this->where($condition)->find();
    }

    public function updateInfo($condition,$data){
        return $this->where($condition)->update($data);
    }


}