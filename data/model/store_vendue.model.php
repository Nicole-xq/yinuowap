<?php
/**
 * 商家拍卖模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class store_vendueModel extends Model {


    public function __construct() {
        parent::__construct('store_vendue');
    }


    /**
     * 查询商家拍卖列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getStore_vendueList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }
    public function getStore_vendueSList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'store_vendue.store_id = store.store_id';
        $result = Model()->table('store_vendue,store')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 商家拍卖数量
     * @param array $condition
     * @return int
     */
    public function getStore_vendueCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询店铺信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getStore_vendueInfo($condition) {
        $store_vendue_info = $this->where($condition)->find();
        return $store_vendue_info;
    }

    public function getStore_vendueSInfo($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $on = 'store_vendue.store_id = store.store_id';
        $result = Model()->table('store_vendue,store')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page)->find();
        return $result;
    }

    /**
     * 添加商家拍卖
     * @param unknown $insert
     * @return boolean
     */
    public function addStore_vendue($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新商家拍卖
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editStore_vendue($update, $condition) {
        return Model()->table('store_vendue')->where($condition)->update($update);
    }


}
