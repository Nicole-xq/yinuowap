<?php
/**
 * 拍品分类
 *@author 汪继君 <675429469@qq.com>
 *
 */
defined('InShopNC') or exit('Access Invalid!');
class auctions_specModel extends Model {
	/**
     * 分类列表
     *
     * @param array $condition 查询条件
     * @param obj $page 分页对象
     * @return array 二维数组
     */
    public function getList($condition,$page=''){
        $param  = array();
        $param['table'] = 'auctions_spec';
        $param['where'] = $this->getCondition($condition);
        $param['order'] = $condition['order'] ? $condition['order'] : 'auctions_spec_id';
        return $this->select1($param,$page);
    }
    /**
     * 添加分类
     *
     * @param array $input
     * @return bool
     */
    public function add($input){
        return $this->insert1('auctions_spec',$input);
    }
    /**
     * 更新分类
     *
     * @param array $input
     * @param int $id
     * @return bool
     */
    public function updates($input,$id){
        return $this->update1('auctions_spec',$input," auctions_spec_id='$id' ");
    }
    /**
     * 删除分类
     *
     * @param string $id
     * @return bool
     */
    public function del($id){
        return $this->delete1('auctions_spec','auctions_spec_id in('.$id.')');
    }
    /**
     * 根据id查询一条分类
     *
     * @param int $id 分类id
     * @return array 一维数组
     */
    public function getOneById($id){
        return $this->getRow1(array('table'=>'auctions_spec','field'=>'auctions_spec_id','value'=>$id));
    }
    /**
     * 构造查询条件
     *
     * @param array $condition 条件数组
     * @return string
     */
    public function getCondition($condition){
        $conditionStr   = '';
        if($condition['auctions_spec_id'] != ''){
            $conditionStr   .= " and auctions_spec.auctions_spec_id='{$condition['auctions_spec_id']}' ";
        }
        if ($condition['like_auctions_spec_name']) {
        	$conditionStr .= " and auctions_spec_name like '%".$condition['like_auctions_spec_name']."%'";
        }
        return $conditionStr;
    }    
}