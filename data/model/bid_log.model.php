<?php
/**
 * 拍品出价日志表
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/9 0009
 * Time: 18:59
 * @author DukeAnn
 */

defined('InShopNC') or exit('Access Invalid!');

class bid_logModel extends Model
{
    const BID_TYPE_AUCTION = 1;        //拍卖竞价
    const BID_TYPE_NEWBIE = 2;         //新手专区竞价
    const BID_TYPE_EXP = 3;         //新手体验竞价

    public function __construct()
    {
        parent::__construct('bid_log');
    }

    /**
     * 写入日志
     * @param $param array 插入的数组
     * @return array
     * */
    public function addBid($param)
    {
        return $this->insert($param);
    }

    /**
     * 日志列表
     * */
    public function getBidList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        return $this->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }

    /**
     * 出价总和
     * */
    public function getBidCount($condition)
    {
        return $this->where($condition)->count();
    }
    /**
     * 分类列表
     *
     * @param array $condition 查询条件
     * @param obj $page 分页对象
     * @return array 二维数组
     */
    public function getList($condition,$page=''){
        $param  = array();
        $param['table'] = 'bid_log';
        $param['where'] = $this->getCondition($condition);
        $param['order'] = $condition['order'] ? $condition['order'] : 'bid_id';
        return $this->select1($param,$page);
    }
    /**
     * 构造查询条件
     *
     * @param array $condition 条件数组
     * @return string
     */
    public function getCondition($condition){
        $conditionStr   = " and member_id>0";
        return $conditionStr;
    }
}

