<?php
/**
 * Created by Test, 2017/05/19 15:41.
 * @author serpent.
 *
 * Copyright (c) 2017 serpent All rights reserved.
 */
defined('InShopNC') or exit('Access Invalid!');
class special_rate_configModel extends Model
{

    public function __construct()
    {
        parent::__construct('special_rate_config');
    }

    public function updateInfo($data,$condition){
        return $this->where($condition)->update($data);
    }

    public function getInfo($condition){
        return $this->where($condition)->find();
    }

    public function getList($condition = array(), $field = '*', $page = null, $order = 'id desc', $limit = '') {
       return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

}