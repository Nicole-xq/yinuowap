<?php
/**
 * 积分及积分日志管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class pointsModel extends Model {
    private $stage_arr;
    public function __construct(){
        parent::__construct();
        $this->stage_arr = array('regist'=>'注册','login'=>'登录','comments'=>'商品评论','order'=>'订单消费','system'=>'诺币管理','pointorder'=>'礼品兑换','app'=>'诺币兑换','signin'=>'签到');
    }
    /**
     * 操作积分
     * @author ShopNC Develop Team
     * @param  string $stage 操作阶段 regist(注册),login(登录),comments(评论),order(下单),system(系统),other(其他),pointorder(积分礼品兑换),app(同步积分兑换)
     * @param  array $insertarr 该数组可能包含信息 array('pl_memberid'=>'会员编号','pl_membername'=>'会员名称','pl_adminid'=>'管理员编号','pl_adminname'=>'管理员名称','pl_points'=>'积分','pl_desc'=>'描述','orderprice'=>'订单金额','order_sn'=>'订单编号','order_id'=>'订单序号','point_ordersn'=>'积分兑换订单编号','signin'=>'签到');
     * @param  bool $if_repeat 是否可以重复记录的信息,true可以重复记录，false不可以重复记录，默认为true
     * @return bool
     */
    function savePointsLog($stage,$insertarr,$if_repeat = true,$guess_points = 0){
        if (!$insertarr['pl_memberid']){
            return false;
        }
        //记录原因文字
        switch ($stage){
            case 'regist':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '注册会员';
                }
                $insertarr['pl_points'] = intval(C('points_reg'));
                break;
            case 'login':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '会员登录';
                }
                $insertarr['pl_points'] = intval(C('points_login'));
                break;
            case 'comments':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '评论商品';
                }
                $insertarr['pl_points'] = intval(C('points_comments'));
                break;
            case 'order':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '订单'.$insertarr['order_sn'].'购物消费';
                }
                $insertarr['pl_points'] = 0;
                if ($insertarr['orderprice']){
                    $insertarr['pl_points'] = @intval($insertarr['orderprice']/C('points_orderrate'));
                    if ($insertarr['pl_points'] > intval(C('points_ordermax'))){
                        $insertarr['pl_points'] = intval(C('points_ordermax'));
                    }
                }
                //订单添加赠送积分列
                $obj_order = Model('order');
                $data = array();
                $data['order_pointscount'] = array('exp','order_pointscount+'.$insertarr['pl_points']);
                $obj_order->editOrderCommon($data,array('order_id'=>$insertarr['order_id']));
                break;
            case 'guess':
                $insertarr['pl_desc'] = '趣猜中奖赠送';
                $insertarr['pl_points'] = $guess_points;
                //订单添加赠送积分列
                /*$obj_order = Model('order');
                $data = array();
                $data['order_pointscount'] = array('exp','order_pointscount+'.$insertarr['pl_points']);
                $obj_order->editOrderCommon($data,array('order_id'=>$insertarr['order_id']));*/
                break;
            case 'system':
                break;
            case 'pointorder':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '兑换礼品信息'.$insertarr['point_ordersn'].'消耗诺币';
                }
                break;
            case 'app':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = Language::get('points_pointorderdesc_app');
                }
                break;
            case 'signin':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '签到领诺币';
                }
                break;
            case 'nuobiorder':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '诺币抵现'.$insertarr['order_sn'].'消耗诺币';
                }
                break;
            case 'freeze':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '诺币冻结'.$insertarr['order_sn'].'冻结诺币';
                }
                break;
            case 'pay_points':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '下单，支付被冻结的诺币'.$insertarr['order_sn'].'支付冻结诺币';
                }
                break;
            case 'cancel':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '取消订单，解冻诺币'.$insertarr['order_sn'].'解冻诺币';
                }
                break;
            case 'refund':
                if (!$insertarr['pl_desc']){
                    $insertarr['pl_desc'] = '订单退款退货，返还诺币'.$insertarr['order_sn'].'返还诺币';
                }
                break;
            case 'other':
                break;
        }
        $save_sign = true;
        if ($if_repeat == false){
            //检测是否有相关信息存在，如果没有，入库
            $condition['pl_memberid'] = $insertarr['pl_memberid'];
            $condition['pl_stage'] = $stage;
            $log_array = self::getPointsInfo($condition);
            if (!empty($log_array)){
                $save_sign = false;
            }
        }
        if ($save_sign == false){
            return true;
        }
        //新增日志
        $value_array = array();
        $value_array['pl_memberid'] = $insertarr['pl_memberid'];
        $value_array['pl_membername'] = $insertarr['pl_membername'];
        if ($insertarr['pl_adminid']){
            $value_array['pl_adminid'] = $insertarr['pl_adminid'];
        }
        if ($insertarr['pl_adminname']){
            $value_array['pl_adminname'] = $insertarr['pl_adminname'];
        }
        $value_array['pl_points'] = $insertarr['pl_points'];
        $value_array['pl_addtime'] = time();
        $value_array['pl_desc'] = $insertarr['pl_desc'];
        $value_array['pl_stage'] = $stage;
        $result = false;
        if($value_array['pl_points'] != '0'){
            $result = self::addPointsLog($value_array);
        }

        if ($result) {
            //更新member内容
            $obj_member = Model('member');
            $upmember_array = array();
            if ($stage == 'nuobiorder') {
                $upmember_array['member_points'] = array('exp', 'member_points' . $insertarr['pl_points']);
            } elseif($stage == 'freeze'){
                $upmember_array['freeze_points'] = array('exp', 'freeze_points+' . $insertarr['pl_points']);
                $upmember_array['member_points'] = array('exp', 'member_points-' . $insertarr['pl_points']);
            }elseif($stage == 'pay_points'){
                $upmember_array['freeze_points'] = array('exp','freeze_points-'.$insertarr['pl_points']);
            }elseif($stage == 'cancel'){
                $upmember_array['freeze_points'] = array('exp','freeze_points-'.$insertarr['pl_points']);
                $upmember_array['member_points'] = array('exp', 'member_points+' . $insertarr['pl_points']);
            }else {
                $upmember_array['member_points'] = array('exp', 'member_points+' . $insertarr['pl_points']);
            }
            $count = $obj_member->editMember(array('member_id'=>$insertarr['pl_memberid'], "member_points"=>["egt", "0"], "freeze_points" => ["egt", "0"]), $upmember_array, true);
            if($count < 1){
                \App\Exceptions\ApiResponseException::throwFailMsg("更新失败,操作频繁,您可以联系客服!");
            }
            return true;
        }else {
            return false;
        }
    }
    /**
     * 添加积分日志信息
     *
     * @param array $param 添加信息数组
     */
    public function addPointsLog($param) {
        if(empty($param)) {
            return false;
        }
        return Model()->table('points_log')->insert($param);
    }
    /**
     * 积分日志列表
     *
     * @param array $condition 条件数组
     * @param array $page   分页
     * @param array $field   查询字段
     * @param array $page   分页
     */
    public function getPointsLogList($where, $field = '*', $limit = 0, $page = 0, $order = '', $group = ''){
        $order = $order ? $order : 'pl_id desc';
        $list = array();
        if (is_array($page)){
            if ($page[1] > 0){
                $list = Model()->table('points_log')->field($field)->where($where)->page($page[0],$page[1])->limit($limit)->order($order)->group($group)->select();
            } else {
                $list = Model()->table('points_log')->field($field)->where($where)->page($page[0])->limit($limit)->order($order)->group($group)->select();
            }
        } else {
            $list = Model()->table('points_log')->field($field)->where($where)->page($page)->limit($limit)->order($order)->group($group)->select();
        }
        if ($list && is_array($list)){
            foreach ($list as $k=>$v){
                $v['stagetext'] = $this->stage_arr[$v['pl_stage']];
                $v['addtimetext'] = @date('Y-m-d',$v['pl_addtime']);
                $list[$k] = $v;
            }
        }
        return $list;
    }
    /**
     * 积分日志详细信息
     *
     * @param array $condition 条件数组
     * @param array $field   查询字段
     */
    public function getPointsInfo($where = array(), $field = '*', $order = '',$group = ''){
        $info = Model()->table('points_log')->where($where)->field($field)->order($order)->group($group)->find();
        if (!$info){
            return array();
        }
        if($info['pl_stage']){
            $info['stagetext'] = $this->stage_arr[$info['pl_stage']];
        }
        if ($info['pl_addtime']) {
            $info['addtimetext'] = @date('Y-m-d',$info['pl_addtime']);
        }
        return $info;
    }
}
