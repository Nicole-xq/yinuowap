<?php
/**
 * 红包通知
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class redpacket_noticeModel extends Model {

    public function __construct($table = null)
    {
        parent::__construct('redpacket_notice');
    }

    public function add_notice($param){
        return $this->insert($param);
    }

    public function get_notice($member_id){
        return $this->where(array('member_id'=>$member_id,'status'=>0))->select();
    }

    public function update_notice_status($condition){
        return $this->where($condition)->update(array('status'=>1));
    }
}
