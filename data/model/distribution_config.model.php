<?php
/**
 * Created by Test, 2017/05/19 15:41.
 * @author serpent.
 *
 * Copyright (c) 2017 serpent All rights reserved.
 */
defined('InShopNC') or exit('Access Invalid!');
class distribution_configModel extends Model
{

    public function __construct()
    {
        parent::__construct('member_distribute_type');
    }

    /**
     * 获取分销等级配置列表
     * @return array
     */
    public function getConfigList($condition = array()){
        return $this->where($condition)->select();
    }

    /**
     * 获取分销等级详情配置
     * @param $id 分销等级id
     * @return null
     */
    public function getConfigById($id){
        return Model()->table('member_distribute_type')->where(array('id'=>$id))->find();
    }

    /**
     * 根据id更新分销等级配置
     * @param $id
     * @param $param 更新内容
     */
    public function updateConfigById($id,$param){
        return $re = Model()->table('member_distribute_type')->where(array('id'=>$id))->update($param);
    }

    public function getConfigByTypeId($type_id){
        return Model()->table('member_distribute_type')->where(array('type_id'=>$type_id))->find();
    }

    /**
     *
     * @param array $condition
     * @param string $field
     * @param string $group
     * @return array
     */
    public function getDisList($condition = array(), $field = '*', $group) {
        return Model()->table('member_distribute_type,member_distribute_detail')
                    ->join('inner')
                    ->on('member_distribute_type.id=member_distribute_detail.dis_id')
                    ->field($field)
                    ->where($condition)
                    ->group($group)
                    ->select();
    }
}