<?php
/**
 * 会员模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class memberModel extends Model {

    public function __construct(){
        parent::__construct('member');
    }

    /**
     * 会员详细信息（查库）
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getMemberInfo($condition, $field = '*', $master = false) {
        return Model()->table('member')->field($field)->where($condition)->master($master)->find();
    }

    /**
     * 取得会员详细信息（优先查询缓存）
     * 如果未找到，则缓存所有字段
     * @param int $member_id
     * @param string $fields
     * @param bool $isRealTime 是否是实时数据
     * @return array
     */
    public function getMemberInfoByID($member_id, $fields = '*', $isRealTime = false) {
        if(!$isRealTime){
            $member_info = rcache($member_id, 'member', $fields);
        }else{
            $member_info = null;
        }
        if (empty($member_info)) {
            $member_info = $this->getMemberInfo(array('member_id'=>$member_id),'*',true);
            /** @var guess_offerModel $guess_offer */
            $guess_offer = Model('guess_offer');
            $member_info['guess_count'] = $guess_offer->get_win_guess_count($member_id);
            /** @var goodsModel $goods */
            $goods = Model('goods');
            $member_info['bought_goods'] = $goods->getBuyedGoodsCount($member_id);
            /** @var redpacketModel $redpacket */
            $redpacket = Model('redpacket');
            $member_info['rpt_counts'] = $redpacket->own_available_rpt($member_id);
            /** @var voucherModel $voucher */
            $voucher = Model('voucher');
            $member_info['voucher_counts'] = $voucher->member_voucher_count($member_id);
            /** @var margin_ordersModel $margin_orders */
            $margin_orders = Model('margin_orders');
            $member_info['margin_counts'] = $margin_orders->member_margin_count($member_id);
            wcache($member_id, $member_info, 'member');
        }
        return $member_info;
    }

    /**
     * 会员列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getMemberList($condition = array(), $field = '*', $page = null, $order = 'member_id desc', $limit = '') {
       return Model()->table('member')->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 会员数量
     * @param array $condition
     * @return int
     */
    public function getMemberCount($condition) {
        return Model()->table('member')->where($condition)->count();
    }

    /**
     * 计算相关金额
     * @param array $condition
     * @param string $fields
     * @return int
     */
    public function getApredepositSum($condition = array(),$fields = 'available_predeposit') {
        return Model()->table('member')->where($condition)->sum($fields);
    }

    /**
     * 编辑会员
     * @param array $condition
     * @param array $data
     * @param bool $retCount 是否返回个数
     * @return bool|int
     */
    public function editMember($condition, $data, $retCount = false) {
        $this->beginTransaction();
        try {
            if($retCount){
                $update = Model()->table('member')->where($condition)->updateRetCount($data);
            }else{
                $update = Model()->table('member')->where($condition)->update($data);
            }
            if (!$update) {
                throw new Exception('用户信息更新失败');
            }
            $param = array();
            !empty($data['member_mobile'])     and $param['mobile']     = $data['member_mobile'];
            !empty($data['member_email'])      and $param['email']      = $data['member_email'];
            !empty($data['member_passwd'])     and $param['password']   = $data['member_passwd'];
            !empty($data['member_avatar'])     and $param['avatar']     = getMemberAvatar($data['member_avatar']);
            !empty($data['member_provinceid']) and $param['provinceid'] = $data['member_provinceid'];
            !empty($data['member_cityid'])     and $param['cityid']     = $data['member_cityid'];
            !empty($data['member_areaid'])     and $param['areaid']     = $data['member_areaid'];
            !empty($data['member_areainfo'])   and $param['areainfo']   = $data['member_areainfo'];
            !empty($data['member_truename'])   and $param['nickname']   = $data['member_truename'];
            isset($data['member_state'])       and $param['state']      = $data['member_state'];

            if (!empty($param)) {
                $member_info = $this->getMemberInfo($condition);

                $param['key'] = $member_info['member_key'];
                $result = $this->updateUCenter($param);

                if (!(isset($result->status_code) && $result->status_code == 200)) {
                    throw new Exception("用户中心信息更新失败！");
                }
            }

            $this->commit();
            dcache($condition['member_id'], 'member');
        } catch (Exception $e) {
            $this->rollback();
            yLog()->error("editMember_error", [\Yinuo\Lib\Helpers\LogHelper::getEInfo($e), func_get_args()]);
            file_put_contents(BASE_DATA_PATH.'/log/editMember.log',"\r\n-----------------------------\r\n".$e->getMessage(),FILE_APPEND);
            $update = $retCount ? 0 : false;
        }
        return $update;
    }

    /**
     * 登录时创建会话SESSION
     *
     * @param array $member_info 会员信息
     */
    public function createSession($member_info = array(),$reg = false) {
        if (empty($member_info) || !is_array($member_info)) return ;

        $_SESSION['is_login']   = '1';
        $_SESSION['member_id']  = $member_info['member_id'];
        $_SESSION['member_name']= $member_info['member_name'];
        $_SESSION['member_email']= $member_info['member_email'];
        $_SESSION['member_type'] = $member_info['member_type'];
        $_SESSION['is_buy']     = isset($member_info['is_buy']) ? $member_info['is_buy'] : 1;
        $_SESSION['avatar']     = $member_info['member_avatar'];
        $_SESSION['is_distributer'] = ($member_info['distri_state'] == 2) ? 1 : 0;

        // 头衔COOKIE
        $this->set_avatar_cookie();

        $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$_SESSION['member_id']));
        $_SESSION['store_id'] = $seller_info['store_id'];

        if (trim($member_info['member_qqopenid'])){
            $_SESSION['openid']     = $member_info['member_qqopenid'];
        }
        if (trim($member_info['member_sinaopenid'])){
            $_SESSION['slast_key']['uid'] = $member_info['member_sinaopenid'];
        }

        if (!$reg) {
            //添加会员积分
            $this->addPoint($member_info);
            //添加会员经验值
            $this->addExppoint($member_info);
            $update_info    = array(
                'member_login_num'=> ($member_info['member_login_num']+1),
                'member_login_time'=> TIMESTAMP,
                'member_old_login_time'=> $member_info['member_login_time'],
                'member_login_ip'=> getIp(),
                'member_old_login_ip'=> $member_info['member_login_ip']
            );
            $this->editMember(array('member_id'=>$member_info['member_id']),$update_info);
        }
        
        setNcCookie('cart_goods_num','',-3600);
        // cookie中的cart存入数据库
        Model('cart')->mergecart($member_info,$_SESSION['store_id']);
        
        // 自动登录
        if ($member_info['auto_login'] == 1) {
            $this->auto_login();
        }
    }
    
    /**
     * 7天内自动登录
     */
    public function auto_login() {
        // 自动登录标记 保存7天
        setNcCookie('auto_login', encrypt($_SESSION['member_id'], MD5_KEY), 7*24*60*60);
    }
    
    public function set_avatar_cookie() {
        setNcCookie('member_avatar', $_SESSION['avatar'], 365*24*60*60);
    }
    
    /**
     *
     */
    public function login($login_info) {
        if (process::islock('login')) {
            return array('error' => '您的操作过于频繁，请稍后再试');
        }
        process::addprocess('login');
        $user_name = $login_info['user_name'];
        $password = $login_info['password'];
        $password_md5 = md5($password);
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array(
                "input" => $user_name,
                "require" => "true",
                "message" => "请填写用户名"
            ),
            array(
                "input" => $user_name,
                "validator" => "username",
                "message" => "请填写字母、数字、中文、_"
            ),
            array(
                "input" => $user_name,
                "max" => "20",
                "min" => "4",
                "validator" => "string_length",
                "message" => "用户名长度要在4~20个字符"
            ),
            array(
                "input" => $password,
                "require" => "true",
                "message" => "密码不能为空"
            )
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            return array('error' => $error);
        }
        $member_info = $this->getMemberInfo(['member_name' => $user_name]);
        if(empty($member_info) && preg_match('/^0?(13|15|17|18|14)[0-9]{9}$/i', $user_name)) {//根据会员名没找到时查手机号
            $member_info = $this->getMemberInfo(['member_mobile' => $user_name]);
        }
        if(empty($member_info) && (strpos($user_name, '@') > 0)) {//按邮箱和密码查询会员
            $member_info = $this->getMemberInfo(['member_email' => $user_name]);
        }
        // 没有用户，或者密码错误，则调取ucenter
        if (empty($member_info) || $member_info['member_passwd'] != $password_md5) {
            $result = $this->loginUCenter(['username' => $user_name]);
            if(isset($result->status_code) && $result->status_code == 200){
                $member = $result->data;
                // 有用户，若与ucenter用户密码不同，则更新用户表密码
                if (!empty($member_info) && $member->password != $member_info['member_passwd']) {
                    $re = Model()->table('member')->where(['member_id' => $member_info['member_id']])->update(['member_passwd' => $member->password]);
                }

                // 没有用户，则添加用户
                if (empty($member_info)) {
                    $re = $this->ucenterToShopnc($member);
                    if (empty($re)) {
                        return array('error' => '登录失败');
                    }
                    $member_info = $this->getMemberInfo(['member_id' => $re]);
                }
                // 验证码ucenter用户密码
                if ($member->password != $password_md5) {
                    return array('error' => '登录失败');
                }
                // 验证码ucenter用户状态
                if ($member->state == 0) {
                    return array('error' => '登录失败');
                }
            } else {
                return array('error' => '登录失败');
            }
        }
        if (!empty($member_info)) {
            if(!$member_info['member_state']){
                return array('error' => '账号被停用');
            }
            if($login_info['unionid'] && $member_info['weixin_unionid'] && ($member_info['weixin_unionid'] != $login_info['unionid'])){
                return array('error' => '未登录绑定');
            }
            process::clear('login');

            //添加会员积分
            $this->addPoint($member_info);
            //添加会员经验值
            $this->addExppoint($member_info);

            $update_info    = array(
                'member_login_num'=> ($member_info['member_login_num']+1),
                'member_login_time'=> TIMESTAMP,
                'member_old_login_time'=> $member_info['member_login_time'],
                'member_login_ip'=> getIp(),
                'member_old_login_ip'=> $member_info['member_login_ip']
            );
            $login_info['unionid'] ? $update_info['weixin_unionid'] =  $login_info['unionid'] : '';
            $this->editMember(array('member_id'=>$member_info['member_id']),$update_info);
            
            return $member_info;
        } else {
            return array('error' => '登录失败');
        }
    }

    public function ucenterToShopnc($member)
    {
        $insert = array(
            'member_key' => $member->key,
            'member_name' => $member->name,
            'member_mobile' => $member->mobile,
            'member_avatar' => $member->avatar,
            'member_email' => empty($member->email) ? '' : $member->email,
            'member_truename' => $member->nickname,
            'member_passwd' => $member->password,
            'member_mobile_bind' => 1,
            'member_areaid' => $member->areaid,
            'member_cityid' => $member->cityid,
            'member_provinceid' => $member->provinceid,
            'member_areainfo' => $member->areainfo,
            'member_sex' => $member->sex,
            'weixin_unionid' => $member->weixin_unionid,
            'weixin_open_id' => $member->weixin_open_id,
            'weixin_info' => $member->weixin_info,
        );
        if (isset($member->registered_source) && !empty($member->registered_source)) {
            $insert['registered_source'] = $member->registered_source;
        }
        if (isset($member->source_staff_id) && !empty($member->source_staff_id)) {
            $insert['source_staff_id'] = $member->source_staff_id;
        }
        return $this->addMember($insert);
    }

    /**
     * 注册
     */
    public function register($register_info) {
        //重复注册验证
        if (process::islock('reg')){
            return array('error' => '您的操作过于频繁，请稍后再试');
        }
        // 注册验证
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array(
                "input" => $register_info["username"],
                "require" => "true",
                "message" => "请填写用户名"
            ),
            array(
                "input" => $register_info["username"],
                "validator" => "username",
                "message" => "请填写字母、数字、中文、_"
            ),
            array(
                "input" => $register_info["username"],
                "max" => "20",
                "min" => "4",
                "validator" => "string_length",
                "message" => "用户名长度要在4~20个字符"
            ),
            array(
                "input" => $register_info["password"],
                "require" => "true",
                "message" => "密码不能为空"
            ),
            array(
                "input" => $register_info["password_confirm"],
                "require" => "true",
                "validator" => "Compare",
                "operator" => "==",
                "to" => $register_info["password"],
                "message" => "密码与确认密码不相同"
            ),
            array(
                "input" => $register_info["email"],
                "require" => "true",
                "validator" => "email",
                "message" => "电子邮件格式不正确"
            ),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            return array('error' => $error);
        }

        if(is_numeric($register_info["username"])) {
            return array('error' => '用户名不能为纯数字');
        }

        // 验证用户名是否重复
        $check_member_name  = $this->getMemberInfo(array('member_name'=>$register_info['username']));
        if(is_array($check_member_name) and count($check_member_name) > 0) {
            return array('error' => '用户名已存在');
        }

        // 验证邮箱是否重复
        $check_member_email = $this->getMemberInfo(array('member_email'=>$register_info['email']));
        if(is_array($check_member_email) and count($check_member_email)>0) {
            return array('error' => '邮箱已存在');
        }

        $curl_param = array();
        $curl_param['username'] = $register_info['username'];
        $result = $this->loginUCenter($curl_param);
        if (isset($result->status_code) && $result->status_code == 200) {
            return array('error' => '用户名已存在');
        }

        // 会员添加
        $member_info    = array();
        $member_info['member_name']     = $register_info['username'];
        $member_info['member_passwd']   = $register_info['password'];
        $member_info['member_email']    = $register_info['email'];
        //添加运营商的员工ID
        $member_info['source_staff_id'] = $register_info['source_staff_id'];
        $member_info['registered_source'] = $register_info['registered_source'];

        if(!empty($register_info['dis_code'])){
            //处理邀请码
            $member_dcode = $this->getUpperMember(array('dis_code'=>$register_info['dis_code']));
            if(!empty($member_dcode)){
                $member_info['upper_member_id'] = $member_dcode['member_id'];
            }
        }

        $insert_id  = $this->addMember($member_info);
        if($insert_id) {
            $member_info['member_id'] = $insert_id;
            $member_info['is_buy'] = 1;
            process::addprocess('reg');
            return $member_info;
        } else {
            return array('error' => '注册失败');
        }
    }

    /**
     * 注册商城会员
     *
     * @param   array $param 会员信息
     * @return  array 数组格式的返回结果
     */
    public function addMember($param) {
        if(empty($param)) {
            return false;
        }
        try {
            $password = empty($param['member_key']) ? md5(trim($param['member_passwd'])) : $param['member_passwd'];
            $this->beginTransaction();
            $member_info    = array();
            $member_info['member_key']          = $param['member_key'];
            $member_info['member_name']         = $param['member_name'];
            $member_info['member_passwd']       = $password;
            $member_info['member_email']        = $param['member_email'];
            $member_info['member_time']         = TIMESTAMP;
            $member_info['member_login_time']   = TIMESTAMP;
            $member_info['member_old_login_time'] = TIMESTAMP;
            $member_info['member_login_ip']     = getIp();
            $member_info['member_old_login_ip'] = $member_info['member_login_ip'];

            $member_info['member_truename']     = $param['member_truename'];
            $member_info['member_qq']           = $param['member_qq'];
            $member_info['member_sex']          = $param['member_sex'];
            $member_info['member_avatar']       = $param['member_avatar'];
            $member_info['member_qqopenid']     = $param['member_qqopenid'];
            $member_info['member_qqinfo']       = $param['member_qqinfo'];
            $member_info['member_sinaopenid']   = $param['member_sinaopenid'];
            $member_info['member_sinainfo'] = $param['member_sinainfo'];
            $member_info['member_type'] = 0;
            $member_info['source'] = $param['source'];

            $member_info['member_areaid'] = $param['member_areaid'];
            $member_info['member_cityid'] = $param['member_cityid'];
            $member_info['member_provinceid'] = $param['member_provinceid'];
            $member_info['member_areainfo'] = $param['member_areainfo'];

            if($param['member_type']){
                $member_info['member_type'] = $param['member_type'];
            }
            if ($param['member_mobile_bind']) {
                $member_info['member_mobile'] = $param['member_mobile'];
                $member_info['member_mobile_bind'] = $param['member_mobile_bind'];
            }
            if ($param['weixin_unionid']) {
                $member_info['weixin_unionid'] = $param['weixin_unionid'];
                $member_info['weixin_info'] = $param['weixin_info'];
            }
            if (isset($param['registered_source']) && !empty($param['registered_source'])) {
                $member_info['registered_source'] = $param['registered_source'];
            }
            if (isset($param['source_staff_id']) && !empty($param['source_staff_id'])) {
                $member_info['source_staff_id'] = $param['source_staff_id'];
            }
            $insert_id  = Model()->table('member')->insert($member_info);
            $member_info['member_id'] = $insert_id;
            $member_info['top_member'] = $param['upper_member_id'];
            if (!$insert_id) {
                throw new Exception("更新用户信息失败");
            }
            $insert = $this->addMemberCommon(array('member_id'=>$insert_id));
            if (!$insert) {
                throw new Exception("更新用户信息失败");
            }
            if (empty($param['member_key'])) {
                $curl_param = array();
                $curl_param['username'] = $param['member_name'];
                $curl_param['password'] = $param['member_passwd'];
                $curl_param['areaid'] = $param['member_areaid'];
                $curl_param['cityid'] = $param['member_cityid'];
                $curl_param['provinceid'] = $param['member_provinceid'];
                $curl_param['areainfo'] = $param['member_areainfo'];
                $result = $this->registerUCenter($curl_param);
                if (isset($result->status_code)) {
                    if($result->status_code != 200){
                        throw new Exception("更新用户信息失败");
                    }
                    $key = $result->data->key;
                    $re = Model()->table('member')->where(['member_id' => $insert_id])->update(['member_key' => $key]);
                } else {
                    throw new Exception();
                }
            }

            // 添加默认相册
            $insert = array();
            $insert['ac_name']      = '买家秀';
            $insert['member_id']    = $insert_id;
            $insert['ac_des']       = '买家秀默认相册';
            $insert['ac_sort']      = 1;
            $insert['is_default']   = 1;
            $insert['upload_time']  = TIMESTAMP;
            $rs = Model()->table('sns_albumclass')->insert($insert);
            //添加会员积分
            if (C('points_isuse')){
                Model('points')->savePointsLog('regist',array('pl_memberid'=>$insert_id,'pl_membername'=>$param['member_name']),false);
            }
            $this->invitationRegister($member_info);
//            //添加关系
//            $member_relation = array();
//            $member_relation['member_id'] = $insert_id;
//            $member_relation['member_name'] = $param['member_name'];
//            $member_relation['add_time'] = TIMESTAMP;
//            $member_relation['dis_code'] = '';
//            $member_relation['member_mobile'] = '';
//            $member_relation['top_member'] = 0;
//            if($param['upper_member_id'] > 0){
//                $member_relation['top_member'] = $param['upper_member_id'];
//                $top_member = $this->getUpperMember(array('member_id'=>$param['upper_member_id']),'top_member,member_name');
//                $member_relation['top_member_name'] = $top_member['member_name'];
//                $member_relation['top_member_2'] = $top_member['top_member'];
//                if($member_relation['top_member'] > 0){
//                    //更新上级的一级合伙人人数
//                    $condition = array(
//                        'member_id'=>$member_relation['top_member']
//                    );
//                    $update = array(
//                        'distribute_lv_1'=>array('exp','distribute_lv_1+1')
//                    );
//                    $re = Model()->table('member')->where($condition)->update($update);
//                }
//                if($member_relation['top_member_2'] > 0){
//                    //更新上级的上级二级合伙人人数
//                    $condition = array(
//                        'member_id'=>$member_relation['top_member_2']
//                    );
//                    $update = array(
//                        'distribute_lv_2'=>array('exp','distribute_lv_2+1')
//                    );
//                    $re = Model()->table('member')->where($condition)->update($update);
//                }
//                $param_1 = array();
//                $param_1['code'] = 'partner_notice';
//                $param_1['member_id'] = $param['upper_member_id'];
//                $param_1['param'] = array();
//                $param_1['number'] = array('mobile' => '', 'email' => '');
//                QueueClient::push('sendMemberMsg', $param_1);
//            }
//            $member_relation['member_type'] = $member_info['member_type'];
//            Model()->table('member_distribute')->insert($member_relation);

            $this->commit();
            return $insert_id;
        } catch (Exception $e) {
            yLog()->error("addMember error", [\Yinuo\Lib\Helpers\LogHelper::getEInfo($e), func_get_args()]);
            $this->rollback();
            return false;
        }
    }


    public function new_addMember($param) {
        if(empty($param)) {
            return false;
        }

        if(empty($param['time'])){
            $param['time'] = TIMESTAMP;
        }


        try {
            $this->beginTransaction();
            $member_info    = array();
            $member_info['member_id']           = $param['member_id'];
            $member_info['member_name']         = $param['member_name'];
            $member_info['member_passwd']       = md5(trim($param['member_passwd']));
            $member_info['member_email']        = $param['member_email'];
            $member_info['member_time']         = $param['time'];
            $member_info['member_login_time']   = $param['time'];
            $member_info['member_old_login_time'] = $param['time'];
            $member_info['member_login_ip']     = $param['ip'];
            $member_info['member_old_login_ip'] = $member_info['member_login_ip'];

            $member_info['member_truename']     = $param['member_truename'];
            $member_info['member_qq']           = $param['member_qq'];
            $member_info['member_sex']          = $param['member_sex'];
            $member_info['member_avatar']       = $param['member_avatar'];
            $member_info['member_qqopenid']     = $param['member_qqopenid'];
            $member_info['member_qqinfo']       = $param['member_qqinfo'];
            $member_info['member_sinaopenid']   = $param['member_sinaopenid'];
            $member_info['member_sinainfo'] = $param['member_sinainfo'];
            $member_info['member_type'] = 0;
            $member_info['source'] = $param['source'];


            if($param['member_type']){
                $member_info['member_type'] = $param['member_type'];
            }
            if ($param['member_mobile_bind']) {
                $member_info['member_mobile'] = $param['member_mobile'];
                $member_info['member_mobile_bind'] = $param['member_mobile_bind'];
            }
            if ($param['weixin_unionid']) {
                $member_info['weixin_unionid'] = $param['weixin_unionid'];
                $member_info['weixin_info'] = $param['weixin_info'];
            }
            $insert_id  = Model()->table('member')->insert($member_info);
            if (!$insert_id) {
                throw new Exception();
            }
            $insert = $this->addMemberCommon(array('member_id'=>$insert_id));
            if (!$insert) {
                throw new Exception();
            }

            // 添加默认相册
            $insert = array();
            $insert['ac_name']      = '买家秀';
            $insert['member_id']    = $insert_id;
            $insert['ac_des']       = '买家秀默认相册';
            $insert['ac_sort']      = 1;
            $insert['is_default']   = 1;
            $insert['upload_time']  = $param['time'];
            $rs = Model()->table('sns_albumclass')->insert($insert);
            //添加会员积分
            if (C('points_isuse')){
                Model('points')->savePointsLog('regist',array('pl_memberid'=>$insert_id,'pl_membername'=>$param['member_name']),false);
            }

            //添加关系
            $member_relation = array();
            $member_relation['member_id'] = $insert_id;
            $member_relation['member_name'] = $param['member_name'];
            $member_relation['add_time'] = $param['time'];
            $member_relation['dis_code'] = '';
            $member_relation['member_mobile'] = '';
            $member_relation['top_member'] = 0;
            if($param['upper_member_id'] > 0){
                $member_relation['top_member'] = $param['upper_member_id'];
                $top_member = $this->getUpperMember(array('member_id'=>$param['upper_member_id']),'top_member,member_name');
                $member_relation['top_member_name'] = $top_member['member_name'];
                $member_relation['top_member_2'] = $top_member['top_member'];
            }
            $member_relation['member_type'] = $member_info['member_type'];
            Model()->table('member_distribute')->insert($member_relation);
            $this->commit();
            return $insert_id;
        } catch (Exception $e) {
            $this->rollback();
            return false;
        }
    }


    /*
     * 注册邀请码绑定关系
     */
    public function invitationRegister($memberInfo)
    {
        $where = ['member_id' => $memberInfo['member_id']];
        $exit = Model()->table('member_distribute')->where($where)->find();
        if ($exit && !empty($exit['top_member'])) {
            return true;
        }
        //添加关系
        $member_relation = array();
        $member_relation['member_id'] = $memberInfo['member_id'];
        $member_relation['member_name'] = $memberInfo['member_name'];
        $member_relation['add_time'] = TIMESTAMP;
        $member_relation['dis_code'] = '';
        $member_relation['member_mobile'] = '';
        $member_relation['top_member'] = 0;
        if($memberInfo['upper_member_id'] > 0 || $memberInfo['top_member'] > 0){
            $member_relation['top_member'] = $memberInfo['top_member'];
            $top_member = $this->getUpperMember(array('member_id'=>$memberInfo['top_member']),'top_member,member_name');
            $member_relation['top_member_name'] = $top_member['member_name'];
            $member_relation['top_member_2'] = $top_member['top_member'];
            if($member_relation['top_member'] > 0){
                //更新上级的一级合伙人人数
                $condition = array(
                    'member_id'=>$member_relation['top_member']
                );
                $update = array(
                    'distribute_lv_1'=>array('exp','distribute_lv_1+1')
                );
                $re = Model()->table('member')->where($condition)->update($update);
            }
            if($member_relation['top_member_2'] > 0){
                //更新上级的上级二级合伙人人数
                $condition = array(
                    'member_id'=>$member_relation['top_member_2']
                );
                $update = array(
                    'distribute_lv_2'=>array('exp','distribute_lv_2+1')
                );
                $re = Model()->table('member')->where($condition)->update($update);
            }
            $param_1 = array();
            $param_1['code'] = 'partner_notice';
            $param_1['member_id'] = $memberInfo['top_member'];
            $param_1['param'] = array();
            $param_1['number'] = array('mobile' => '', 'email' => '');
            QueueClient::push('sendMemberMsg', $param_1);
        }
        $member_relation['member_type'] = $memberInfo['member_type'];
        if ($exit) {
            $result = Model()->table('member_distribute')->where($where)->update($member_relation);
        } else {
            $result = Model()->table('member_distribute')->insert($member_relation);
        }
        if ($result === false) {
            return false;
        }
        return true;
    }

    /**
     * 获取上级用户
     * @param array $condition
     * @param string $field
     * @param bool $master
     */
    public function getUpperMember($condition, $field = '*', $master = false){
        return Model()->table('member_distribute')->field($field)->where($condition)->master($master)->find();
    }

    /**
     * 编辑分销会员信息
     * @param $data
     * @param $condition
     */
    public function editMemberDistribute($data, $condition){
        return Model()->table('member_distribute')->where($condition)->update($data);
    }

    /**
     * 获取分销会员列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     * @return mixed
     */
    public function getMemberDistributeList($condition = array(), $field = '*', $page = null, $order = 'dis_id desc', $limit = ''){
        return Model()->table('member_distribute')->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 获取分销会员及会员信息列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getMemberDistributeWithMemberList($condition, $field = '*', $page = null, $order = 'dis_id desc', $limit = ''){
        return Model()->table('member_distribute,member')->field($field)->join('left')->on('member_distribute.member_id=member.member_id')->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 获取下线总数
     * @param $condition
     * @return int
     */
    public function getCountDistribute($condition){
        return $this->getCount1('member_distribute',$condition);
    }

    /**
     * 获取艺术家及会员信息列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     * @return mixed
     */
    public function getArtistWithMemberList($condition = array(), $field = '*', $page = null, $order = 'member_id desc', $limit = ''){
        return Model()->table('member,artist_vendue')->field($field)->join('left')->on('member.member_id=artist_vendue.member_id')->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 会员登录检查
     *
     */
    public function checkloginMember() {
        if($_SESSION['is_login'] == '1') {
            @header("Location: index.php");
            exit();
        }
    }

    /**
     * 检查会员是否允许举报商品
     *
     */
    public function isMemberAllowInform($member_id) {
        $condition = array();
        $condition['member_id'] = $member_id;
        $member_info = $this->getMemberInfo($condition,'inform_allow');
        if(intval($member_info['inform_allow']) === 1) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 取单条信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getMemberCommonInfo($condition = array(), $fields = '*') {
        return Model()->table('member_common')->where($condition)->field($fields)->find();
    }

    /**
     * 插入扩展表信息
     * @param unknown $data
     * @return Ambigous <mixed, boolean, number, unknown, resource>
     */
    public function addMemberCommon($data) {
        return Model()->table('member_common')->insert($data);
    }

    /**
     * 编辑会员扩展表
     * @param unknown $data
     * @param unknown $condition
     * @return Ambigous <mixed, boolean, number, unknown, resource>
     */
    public function editMemberCommon($data,$condition) {
        return Model()->table('member_common')->where($condition)->update($data);
    }

    /**
     * 添加会员积分
     * @param unknown $member_info
     */
    public function addPoint($member_info) {
        if (!C('points_isuse') || empty($member_info)) return;

        //一天内只有第一次登录赠送积分
        if(trim(@date('Y-m-d',$member_info['member_login_time'])) == trim(date('Y-m-d'))) return;

        //加入队列
        $queue_content = array();
        $queue_content['member_id'] = $member_info['member_id'];
        $queue_content['member_name'] = $member_info['member_name'];
        QueueClient::push('addPoint',$queue_content);
    }

    /**
     * 添加会员经验值
     * @param unknown $member_info
     */
    public function addExppoint($member_info) {
        if (empty($member_info)) return;

        //一天内只有第一次登录赠送经验值
        if(trim(@date('Y-m-d',$member_info['member_login_time'])) == trim(date('Y-m-d'))) return;

        //加入队列
        $queue_content = array();
        $queue_content['member_id'] = $member_info['member_id'];
        $queue_content['member_name'] = $member_info['member_name'];
        QueueClient::push('addExppoint',$queue_content);
    }

    /**
     * 取得会员安全级别
     * @param unknown $member_info
     */
    public function getMemberSecurityLevel($member_info = array()) {
        $tmp_level = 0;
        if ($member_info['member_email_bind'] == '1') {
            $tmp_level += 1;
        }
        if ($member_info['member_mobile_bind'] == '1') {
            $tmp_level += 1;
        }
        if ($member_info['member_paypwd'] != '') {
            $tmp_level += 1;
        }
        return $tmp_level;
    }

    /**
     * 获得会员等级
     * @param bool $show_progress 是否计算其当前等级进度
     * @param int $exppoints  会员经验值
     * @param array $cur_level 会员当前等级
     */
    public function getMemberGradeArr($show_progress = false,$exppoints = 0,$cur_level = ''){
        $member_grade = C('member_grade')?unserialize(C('member_grade')):array();
        //处理会员等级进度
        if ($member_grade && $show_progress){
            $is_max = false;
            if ($cur_level === ''){
                $cur_gradearr = $this->getOneMemberGrade($exppoints, false, $member_grade);
                $cur_level = $cur_gradearr['level'];
            }
            foreach ($member_grade as $k=>$v){
                if ($cur_level == $v['level']){
                    $v['is_cur'] = true;
                }
                $member_grade[$k] = $v;
            }
        }
        return $member_grade;
    }

    /**
     * 获得某一会员等级
     * @param int $exppoints
     * @param bool $show_progress 是否计算其当前等级进度
     * @param array $member_grade 会员等级
     */
    public function getOneMemberGrade($exppoints,$show_progress = false,$member_grade = array()){
        if (!$member_grade){
            $member_grade = C('member_grade')?unserialize(C('member_grade')):array();
        }
        if (empty($member_grade)){//如果会员等级设置为空
            $grade_arr['level'] = -1;
            $grade_arr['level_name'] = '暂无等级';
            return $grade_arr;
        }

        $exppoints = intval($exppoints);

        $grade_arr = array();
        if ($member_grade){
            foreach ($member_grade as $k=>$v){
                if($exppoints >= $v['exppoints']){
                    $grade_arr = $v;
                }
            }
        }
        //计算提升进度
        if ($show_progress == true){
            if (intval($grade_arr['level']) >= (count($member_grade) - 1)){//如果已达到顶级会员
                $grade_arr['downgrade'] = $grade_arr['level'] - 1;//下一级会员等级
                $grade_arr['downgrade_name'] = $member_grade[$grade_arr['downgrade']]['level_name'];
                $grade_arr['downgrade_exppoints'] = $member_grade[$grade_arr['downgrade']]['exppoints'];
                $grade_arr['upgrade'] = $grade_arr['level'];//上一级会员等级
                $grade_arr['upgrade_name'] = $member_grade[$grade_arr['upgrade']]['level_name'];
                $grade_arr['upgrade_exppoints'] = $member_grade[$grade_arr['upgrade']]['exppoints'];
                $grade_arr['less_exppoints'] = 0;
                $grade_arr['exppoints_rate'] = 100;
            } else {
                $grade_arr['downgrade'] = $grade_arr['level'];//下一级会员等级
                $grade_arr['downgrade_name'] = $member_grade[$grade_arr['downgrade']]['level_name'];
                $grade_arr['downgrade_exppoints'] = $member_grade[$grade_arr['downgrade']]['exppoints'];
                $grade_arr['upgrade'] = $member_grade[$grade_arr['level']+1]['level'];//上一级会员等级
                $grade_arr['upgrade_name'] = $member_grade[$grade_arr['upgrade']]['level_name'];
                $grade_arr['upgrade_exppoints'] = $member_grade[$grade_arr['upgrade']]['exppoints'];
                $grade_arr['less_exppoints'] = $grade_arr['upgrade_exppoints'] - $exppoints;
                $grade_arr['exppoints_rate'] = round(($exppoints - $member_grade[$grade_arr['level']]['exppoints'])/($grade_arr['upgrade_exppoints'] - $member_grade[$grade_arr['level']]['exppoints'])*100,2);
            }
        }
        return $grade_arr;
    }

    /**
     * 调用ucenter注册接口
     *
     * @param
     * @return mixed
     */
    public function registerUCenter($param){
        $data = $this->_curl('register', $param);
        return $data;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function loginUCenter($param){
        $data = $this->_curl('login', $param);
        return $data;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function passwordUCenter($param){
        $data = $this->_curl('password', $param);
        return $data;
    }

    /**
     * 调用ucenter更新
     *
     * @param
     * @return mixed
     */
    public function updateUCenter($param){
        $data = $this->_curl('update', $param);
        return $data;
    }

    private function _curl($url_path, $curlPost){
        $url = C('ucenter');
        if (empty($url)) {
            return false;
        }
        $postUrl = $url . '/' . $url_path;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, TRUE);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);

        if ($data) {
            $data = json_decode($data);
        }
        return $data;
    }
}
