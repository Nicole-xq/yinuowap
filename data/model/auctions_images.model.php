<?php
/**
 * 拍品图片表
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/30 0030
 * Time: 15:08
 * @author ADKi
 */

use Shopnc\Lib\StdArray;

defined('InShopNC') or exit('Access Invalid!');

class auctions_imagesModel extends Model
{
    public function __construct(){
        parent::__construct('auctions_images');
    }

    /**
     * 更新拍卖商品图片
     * @param $condition array
     * @param $update array
     * @return bool
     * */
    public function updateImage($condition, $update)
    {
        return $this->where($condition)->update($update);
    }

    /**
     * 拍品图片列表
     *
     * @param array $condition
     * @param array $order
     * @param string $field
     * @return array
     */
    public function getAuctionImageList($condition, $field = '*', $order = '') {
        return $this->field($field)->where($condition)->order($order)->select();
    }
}
