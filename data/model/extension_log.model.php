<?php
/**
 * 推广渠道模型
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class extension_logModel extends Model
{

    public function __construct()
    {
        parent::__construct('extension_log');
    }

    /**
     * 推广日志列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getList($condition = array(), $field = '*', $page = null, $order = 'log_id desc', $limit = '') {
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 推广日志信息
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getOne($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /*
     *  判断是否存在
     *  @param array $condition
     *
     */
    public function isExist($condition) {
        $result = $this->getOne($condition);
        if(empty($result)) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function save($param){
        return $this->insert($param);
    }

    /*
     * 更新
     * @param array $update
     * @param array $condition
     * @return bool
     */
    public function modify($update, $condition){
        return $this->where($condition)->update($update);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function drop($condition){
        return $this->where($condition)->delete();
    }
}