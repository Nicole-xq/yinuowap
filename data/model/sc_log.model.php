<?php
/**
 * 专场围观记录
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class sc_logModel extends Model{

    public function __construct() {
        parent::__construct('sc_log');
    }

    /**
     * 增加专场围观记录
     *
     * @param
     * @return int
     */
    public function addSc($log_array) {
        $log_array['add_time'] = TIMESTAMP;
        $log_id = $this->insert($log_array);
        return $log_id;
    }

    /**
     * 查询单条记录
     *
     * @param
     * @return array
     */
    public function getScInfo($condition) {
        if (empty($condition)) {
            return false;
        }
        $result = $this->where($condition)->order('sc_log_id asc')->find();
        return $result;
    }

    /**
     * 查询记录
     *
     * @param
     * @return array
     */
    public function getScList($condition = array(), $page = '', $limit = '', $order = 'sc_log_id asc') {
        $result = $this->where($condition)->page($page)->limit($limit)->order($order)->select();
        return $result;
    }

    /**
     * 取得记录数量
     *
     * @param
     * @return int
     */
    public function getScCount($condition) {
        return $this->where($condition)->count();
    }
}
