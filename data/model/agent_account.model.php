<?php
/**
 * 区域代理账户模型
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class agent_accountModel extends Model {

    public function __construct(){
        parent::__construct('area_agent_account');
    }

    /**
     * 获取区域代理账户列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getAgentAccountList($condition = [], $field = '*', $page = null, $order = 'id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 添加区域代理账户记录
     * @param array $data
     * @return mixed
     */
    public function addAgentAccount($data){
        return $this->insert($data);
    }

    /**
     * 修改区域代理账户记录数据
     * @param $conditions
     * @param $data
     * @return bool
     */
    public function updateAgentAccount($conditions,$data){
        return $this->where($conditions)->update($data);
    }

    /**
     * 获取区域代理账户详情
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAgentAccountInfo($condition = array(), $field = '*'){
        return $this->field($field)->where($condition)->find();
    }



}