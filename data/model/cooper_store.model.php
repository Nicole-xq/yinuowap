<?php
/**
 * 合作商家模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class cooper_storeModel extends Model {


    public function __construct() {
        parent::__construct('cooper_store');
    }


    /**
     * 查询合作商家列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getcooper_storeList($condition, $field = '*',$page = null, $order = 'cooper_store_sort asc, cooper_store_id desc', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 合作商家数量
     * @param array $condition
     * @return int
     */
    public function getcooper_storeCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条合作商家信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getcooper_storeInfo($condition) {
        $cooper_store_info = $this->where($condition)->find();
        return $cooper_store_info;
    }

    /**
     * 添加合作商家
     * @param unknown $insert
     * @return boolean
     */
    public function addcooper_store($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新合作商家
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editcooper_store($update, $condition) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除合作商家
     * @param unknown $condition
     * @return boolean
     */
    public function delcooper_store($condition) {
        $cooper_store_array = $this->getcooper_storeList($condition, 'cooper_store_id,cooper_store_pic');
        $cooper_storeid_array = array();
        foreach ($cooper_store_array as $value) {
            $cooper_storeid_array[] = $value['cooper_store_id'];
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_PAINTING.DS.$value['cooper_store_pic']);
        }
        return $this->where(array('cooper_store_id' => array('in', $cooper_storeid_array)))->delete();
    }


}
