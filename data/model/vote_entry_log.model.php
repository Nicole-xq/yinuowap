<?php
/**
 * 艺术之星投票
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class vote_entry_logModel extends Model{
    public function __construct(){
        parent::__construct('vote_entry_log');
    }

    /**
     * 读取投票log列表
     * @param array $condition
     *
     */
    public function getList($condition, $page=null, $field='*', $order='', $limit=''){
        $list = $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
        return $list;
    }

    /**
     * 读取投票log单条记录
     * @param array $condition
     *
     */
    public function getInfo($condition, $field='*', $order=''){
        $list = $this->field($field)->where($condition)->order($order)->find();
        return $list;
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function save($param){
        return $this->insert($param);
    }

    /*
     * 更新
     * @param array $update
     * @param array $condition
     * @return bool
     */
    public function modify($update, $condition){
        return $this->where($condition)->update($update);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function drop($condition){
        return $this->where($condition)->delete();
    }

}
