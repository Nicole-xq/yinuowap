<?php
/**
 * 商家资讯模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class store_informModel extends Model {


    public function __construct() {
        parent::__construct('store_inform');
    }


    /**
     * 查询商家资讯列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getStore_informList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 商家资讯数量
     * @param array $condition
     * @return int
     */
    public function getStore_informCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询商家资讯单条信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getStore_informInfo($condition) {
        $store_vendue_info = $this->where($condition)->find();
        return $store_vendue_info;
    }

    /**
     * 添加商家资讯
     * @param unknown $insert
     * @return boolean
     */
    public function addStore_inform($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新商家资讯
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editStore_inform($update, $condition) {
        return $this->where($condition)->update($update);
    }


}
