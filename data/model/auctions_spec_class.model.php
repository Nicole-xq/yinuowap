<?php
/**
 * 拍品分类
 *@author 汪继君 <675429469@qq.com>
 *
 */
defined('InShopNC') or exit('Access Invalid!');
class auctions_spec_classModel extends Model {

    /**
     * 添加分类
     *
     * @param array $input
     * @return bool
     */
    public function add($input){
        return $this->insert1('auctions_spec_class',$input);
    }
    /**
     * 更新分类
     *
     * @param array $input
     * @param int $id
     * @return bool
     */
    public function updates($input,$id){
        return $this->update1('auctions_spec_class',$input," auctions_spec_class_id='$id' ");
    }
    /**
     * 删除分类
     *
     * @param string $id
     * @return bool
     */
    public function del($id){
        return $this->delete1('auctions_spec_class','auctions_spec_class_id in('.$id.')');
    }
    /**
     * 根据id查询一条分类
     *
     * @param int $id 分类id
     * @return array 一维数组
     */
    public function getOneById($id){
        return $this->getRow1(array('table'=>'auctions_spec_class','field'=>'auctions_spec_class_id','value'=>$id));
    }
   
}