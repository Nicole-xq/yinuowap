<?php
/**
 * 书画模块名称模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class paint_moduleModel extends Model {


    public function __construct() {
        parent::__construct('paint_module');
    }


    /**
     * 查询模块名称列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getpaint_moduleList($condition, $field = '*',$page = null, $order = 'paint_module_sort asc, paint_module_id desc', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 模块名称数量
     * @param array $condition
     * @return int
     */
    public function getpaint_moduleCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条模块名称信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getpaint_moduleInfo($condition) {
        $paint_module_info = $this->where($condition)->find();
        return $paint_module_info;
    }

    /**
     * 添加模块名称
     * @param unknown $insert
     * @return boolean
     */
    public function addpaint_module($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新模块名称
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editpaint_module($update, $condition) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除模块名称
     * @param unknown $condition
     * @return boolean
     */
    public function delpaint_module($condition) {
        $paint_module_array = $this->getpaint_moduleList($condition, 'paint_module_id');
        $paint_moduleid_array = array();
        foreach ($paint_module_array as $value) {
            $paint_moduleid_array[] = $value['paint_module_id'];
        }
        return $this->where(array('paint_module_id' => array('in', $paint_moduleid_array)))->delete();
    }


}
