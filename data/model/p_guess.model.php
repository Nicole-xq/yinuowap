<?php
/**
 * 商品趣猜
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class p_guessModel extends Model {
    const STATE0 = 0;       // 关闭
    const STATE1 = 1;       // 开启
    const STATE2 = 2;       // 结束

    const GUESS_STATE_REVIEW = 10;
    const GUESS_STATE_NORMAL = 20;
    const GUESS_STATE_REVIEW_FAIL = 30;
    const GUESS_STATE_CANCEL = 31;
    const GUESS_STATE_CLOSE = 32;

    private $guess_state_array = array(
        0 => '全部',
        self::GUESS_STATE_REVIEW => '审核中',
        self::GUESS_STATE_NORMAL => '正常',
        self::GUESS_STATE_CLOSE => '已结束',
        self::GUESS_STATE_REVIEW_FAIL => '审核失败',
        self::GUESS_STATE_CANCEL => '管理员关闭',
    );

    public function __construct() {
        parent::__construct();
    }

    /**
     * 趣猜数量
     *
     * @param array $condition
     * @return array
     */
    public function getGuessCount($condition) {
        return Model()->table('p_guess')->where($condition)->count();
    }

    /**
     * 趣猜列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $page
     * @param int $limit
     * @param int $count
     * @return array
     */
    public function getGuessList($condition, $field = '*', $order = 'gs_id desc', $page = 10, $limit = 0, $count = 0) {
        return Model()->table('p_guess')->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }

    /**
     * 开启的活动列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $limit
     * @return array
     */
    public function getGuessOpenList($condition, $field = '*', $order = 'end_time asc', $limit = 0) {
        $condition['gs_state'] = self::STATE1;
        $condition['verify_state'] = 1;
        return $this->getGuessList1($condition, $field, $order, $limit);
    }


     /**
     * 开启的活动列表(手机)
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $limit
     * @return array
     */
    public function getGuessOpenListForMobile($condition, $page = 0, $field = '*', $limit = 0, $order = 'end_time desc,gs_id desc') {
        $condition['gs_state'] = self::STATE1;
        $condition['end_time'] = array('gt',time());
        $condition['verify_state'] = 1;
        return Model()->table('p_guess')->where($condition)->order($order)->limit($limit)->page($page)->select();
    }

    /*
     * 活动列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getGuessList1($condition, $field = '*', $order = 'gs_id desc',$limit = 0) {
        return Model()->table('p_guess')->field($field)->where($condition)->order($order)->limit($limit)->select();
    }

    public function getOpenGuessInfo($condition) {
        $condition['gs_state'] = array('in',array(self::STATE1,self::STATE2));
        return $this->getGuessInfo($condition);
    }

    /**
     * 获得获得详细信息
     */
    public function getGuessInfo($condition,$order = 'gs_id desc') {
        return Model()->table('p_guess')->where($condition)->order($order)->find();
    }

    /**
     * 保存活动
     *
     * @param array $insert
     * @param string $replace
     * @return boolean
     */
    public function addGuess($insert, $replace = false) {
        return Model()->table('p_guess')->pk(array('gs_id'))->insert($insert, $replace);
    }

    /**
     * 更新活动
     *
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editGuess($update, $condition) {
        return Model()->table('p_guess')->where($condition)->update($update);
    }

    /**
     * 更新活动关闭
     *
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editGuessCloseByGoodsIds($condition) {
        $guessgoods_list = $this->getGuessGoodsList($condition, 'gs_id');
        if (!empty($guessgoods_list)) {
            $blid_array = array();
            foreach ($guessgoods_list as $val) {
                $blid_array[] = $val['gs_id'];
            }
            $update = array('gs_state' => self::STATE0);
            return Model()->table('p_guess')->where(array('gs_id' => array('in', $blid_array)))->update($update);
        }
        return true;
    }

    /**
     * 删除套餐活动
     * @param array $gsids
     * @param int $store_id
     * @return boolean
     */
    public function delGuess($gsids, $store_id) {
        $gsid_array = explode(',', $gsids);
        foreach ($gsid_array as $val) {
            if (!is_numeric($val)) {
                return false;
            }
        }
        $where = array();
        $where['gs_id'] = array('in', $gsid_array);
        $where['store_id'] = $store_id;
        $gs_list = $this->getGuessList($where, 'gs_id');
        $gs_list = array_under_reset($gs_list, 'gs_id');
        $gsid_array = array_keys($gs_list);

        $where = array();
        $where['gs_id'] = array('in', $gsid_array);
        $rs = Model()->table('p_guess')->where($where)->delete();
        if ($rs) {
            return $this->delGuessGoods($where);
        } else {
            return false;
        }
    }

    /**
     * 趣猜商品列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $group
     * @return array
     */
    public function getGuessGoodsList($condition, $field = '*', $order = 'gs_goods_id asc', $group = '') {
        return Model()->table('p_guess_goods')->field($field)->where($condition)->group($group)->order($order)->select();
    }

    /**
     * 保存趣猜商品
     *
     * @param unknown $insert
     * @return boolean
     */
    public function addGuessGoodsAll($insert) {
        $result = Model()->table('p_guess_goods')->insertAll($insert);
        return $result;
    }

    /**
     * 删除套餐商品
     *
     * @param array $condition
     * @return boolean
     */
    public function delGuessGoods($condition) {
        $list = $this->getGuessGoodsList($condition, 'goods_id');
        if (empty($list)) {
            return true;
        }
        $result = Model()->table('p_guess_goods')->where($condition)->delete();
        return $result;
    }

    /**
     * 趣猜状态数组
     */
    public function getGuessStateArray() {
        return $this->guess_state_array;
    }

    /**
     * 添加围观数
     * @param [type] $condition [description]
     */
    public function addViews($condition) {
        return $this->editGuess(array('views' => array('exp', 'views+1')), $condition);
    }

    /** 
     * 更新参加人数
     * @return [type] [description]
     */
    public function update_join_num($condition) {
        $model_guess_offer = Model('guess_offer');
        $joined_times = $model_guess_offer->get_guess_times($condition);
        return $this->editGuess(array('joined_times' => $joined_times), $condition);
    }

    /** 
     * 获取趣猜商品列表
     * @param  [type] $condition [description]
     * @param  [type] $field     [description]
     * @return [type]            [description]
     */
    public function get_guess_goods_list($condition = array(),$field = "*") {
        return Model()->table('p_guess_goods')->where($condition)->field($field)->select();
    }

    /** 根据趣猜专场id获取趣猜参与数量
     * @param $id
     * @return int
     */
    public function get_guess_number_by_id($id){
        $count_info = Model()->table('p_guess')->where(array('gs_act_id' => $id))->sum('joined_times');
        return empty($count_info) ? 0 : $count_info;
    }

}
