<?php
/**
 * 区域代理模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class area_agentModel extends Model {

    private $agent_type = [
        1 => '自然人',
        2 => '企业法人'
    ];

    private $agent_state = [
        1 => '正常',
        2 => '禁用'
    ];
    private $area_type = [
        1  => '省级代理',
        2  => '市级代理',
        3  => '县级代理'
    ];

    public function __construct(){
        parent::__construct('area_agent');
    }

    /**
     * 获取区域代理列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getAreaAgentList($condition = [], $field = '*', $page = null, $order = 'agent_id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 添加区域代理记录
     * @param array $data
     * @return mixed
     */
    public function addAreaAgent($data){
        return $this->insert($data);
    }

    /**
     * 修改区域代理记录数据
     * @param $conditions
     * @param $data
     * @return bool
     */
    public function updateAreaAgent($conditions,$data){
        return $this->where($conditions)->update($data);
    }

    /**
     * 获取区域代理详情
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAreaAgentInfo($condition = array(), $field = '*'){
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 获取代理类型
     * @return array
     */
    public function getAgentType() {
        return $this->agent_type;
    }

    /**
     * 获取代理状态
     * @return array
     */
    public function getAgentState() {
        return $this->agent_state;
    }

    /**
     * 获取区域代理类型
     * @return array
     */
    public function getAreaType() {
        return $this->area_type;
    }

}