<?php
/**
 * Created by Test, 2017/07/11
 * @author serpent.
 *
 * Copyright (c) 2017 serpent All rights reserved.
 */
defined('InShopNC') or exit('Access Invalid!');
class distribution_detail_configModel extends Model
{

    public function __construct()
    {
        parent::__construct('member_distribute_detail');
    }

    /**
     * 获取某条明细
     * @param array $condition
     * @param string $field
     * @param bool $master
     * @return array
     */
    public function getDisDetailInfo($condition, $field = '*', $master = false){
        return Model()->table('member_distribute_detail')->field($field)->where($condition)->master($master)->find();
    }

    /**
     * 更新
     * @param array $condition
     * @param array $data
     * @return  bool
     */
    public function updateDisDetail($condition,$data){
        return Model()->table('member_distribute_detail')->where($condition)->update($data);
    }

    /**
     * 新增
     * @param array $data
     * @return  bool
     */
    public function addDisDetail($data){
        return Model()->table('member_distribute_detail')->insert($data);
    }

    public function getList($condition){
        return $this->where($condition)->select();
    }

}