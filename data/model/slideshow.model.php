<?php

/**
 * Created by Test, 2018/10/16 15:39.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */
defined('InShopNC') or exit('Access Invalid!');
class slideshowModel extends Model
{
    public function __construct(){
        parent::__construct('slideshow');
    }

    public function getList($condition, $page = null, $order = '', $field = '*', $limit = ''){
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }

    public function addInfo($param){
        return $this->insert($param);
    }

    public function updateInfo($param,$condition){
        $re = $this->where($condition)->update($param);
        return $re;
    }

    public function getInfo($condition){
        return $this->where($condition)->find();
    }
}