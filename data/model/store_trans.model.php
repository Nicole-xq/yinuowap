<?php
/**
 * 店铺成交模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class store_transModel extends Model {


    public function __construct() {
        parent::__construct('store_trans');
    }


    /**
     * 查询店铺成交列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getStore_transList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 店铺成交数量
     * @param array $condition
     * @return int
     */
    public function getStore_transCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询店铺成交信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getStore_transInfo($condition) {
        $store_vendue_info = $this->where($condition)->find();
        return $store_vendue_info;
    }

    /**
     * 添加店铺成交
     * @param unknown $insert
     * @return boolean
     */
    public function addStore_trans($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新店铺成交
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editStore_trans($update, $condition) {
        return $this->where($condition)->update($update);
    }


}
