<?php
/**
 * 艺术家分类模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class artist_classifyModel extends Model {


    public function __construct() {
        parent::__construct('artist_classify');
    }


    /**
     * 查询艺术家分类列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getArtist_classifyList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 艺术家分类数量
     * @param array $condition
     * @return int
     */
    public function getArtist_classifyCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条艺术家分类信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getArtist_classifyInfo($condition) {
        $artist_classify_info = $this->where($condition)->find();
        return $artist_classify_info;
    }

    /**
     * 添加艺术家分类
     * @param unknown $insert
     * @return boolean
     */
    public function addArtist_classify($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新艺术家分类
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editArtist_classify($update, $condition) {
        return $this->where($condition)->update($update);
    }


}
