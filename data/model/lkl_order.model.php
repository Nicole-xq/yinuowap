<?php
/**
 * 拉卡拉订单
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');


class lkl_orderModel extends Model {

    public function __construct($table = null)
    {
        parent::__construct('lkl_order');
    }

    public function createOrder($param){
        $this->insert($param);
    }

    public function updateInfo($order_sn,$param){
        $this->where(array('lkl_order_sn'=>$order_sn))->update($param);
    }

}
