<?php
/**
 * 区域代理返佣配置模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class agent_commissionModel extends Model {


    private $agent_type = [
        1  => '省级代理',
        2  => '市级代理',
        3  => '县级代理'
    ];

    public function __construct(){
        parent::__construct('area_agent_commission_set');
    }

    /**
     * 获取区域代理返佣配置列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getAgentCommissionList($condition = [], $field = '*', $page = null, $order = 'id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 添加区域代理返佣配置记录
     * @param array $data
     * @return mixed
     */
    public function addAgentCommission($data){
        return $this->insert($data);
    }

    /**
     * 修改区域代理返佣配置记录数据
     * @param $conditions
     * @param $data
     * @return bool
     */
    public function updateAgentCommission($conditions,$data){
        return $this->where($conditions)->update($data);
    }

    /**
     * 获取区域代理返佣配置详情
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAgentCommissionInfo($condition = array(), $field = '*'){
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 获取区域代理返佣类型
     * @return array
     */
    public function getAgentType(){
        return $this->agent_type;
    }

}