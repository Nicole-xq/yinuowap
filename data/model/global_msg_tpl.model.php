<?php
/**
 * 全局消息模板模型
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class global_msg_tplModel extends Model{
    public function __construct() {
        parent::__construct('global_msg_tpl');
    }

    private $send_state = [
         1=>'未发送',
         2=>'已发送'
        ];

    private $send_type = [
        1=>'系统+短信',
        2=>'系统',
        3=>'短信'
    ];

    /**
     * 全局消息模板列表
     * @param array $condition
     * @param string $field
     * @param string $group
     * @param integer $page
     * @param string $order
     * @return  array
     */
    public function getGlobalMsgTplList($condition, $field = '*', $group = '', $order = 'id ASC', $page = 0) {
        return $this->field($field)->where($condition)->group($group)->order($order)->page($page)->select();
    }

    /**
     * 全局消息模板详细信息
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getMemberMsgTplInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 编辑全局消息模板
     * @param array $condition
     * @param unknown $update
     * @return array
     */
    public function editMemberMsgTpl($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 增加全局消息模板
     * @param $global_array
     * @return int
     */
    public function addGlobalMsgTpl($global_array) {
        return $this->insert($global_array);
    }

    /**
     * 获取发送状态
     * @return array
     */
    public function getSendState(){
        return $this->send_state;
    }

    /**
     * 获取发送类型
     * @return array
     */
    public function getSendType(){
        return $this->send_type;
    }
}
