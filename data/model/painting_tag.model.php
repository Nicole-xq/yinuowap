<?php
/**
 * 书画标签模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class painting_tagModel extends Model {


    public function __construct() {
        parent::__construct('painting_tag');
    }


    /**
     * 查询标签列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getpainting_tagList($condition, $field = '*',$page = null, $order = 'paint_tag_sort asc, paint_tag_id desc', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 标签数量
     * @param array $condition
     * @return int
     */
    public function getpainting_tagCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条标签信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getpainting_tagInfo($condition) {
        $painting_tag_info = $this->where($condition)->find();
        return $painting_tag_info;
    }

    /**
     * 添加标签
     * @param unknown $insert
     * @return boolean
     */
    public function addpainting_tag($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新标签
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editpainting_tag($update, $condition) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除标签
     * @param unknown $condition
     * @return boolean
     */
    public function delpainting_tag($condition) {
        $paint_tag_array = $this->getpainting_tagList($condition, 'paint_tag_id');
        $paint_tagid_array = array();
        foreach ($paint_tag_array as $value) {
            $paint_tagid_array[] = $value['paint_tag_id'];
        }
        return $this->where(array('paint_tag_id' => array('in', $paint_tagid_array)))->delete();
    }


}
