<?php
/**
 * 协议设定
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class mb_agreementModel extends Model {
    public function __construct() {
        parent::__construct('mb_agreement');
    }

    /**
     * 读取单行信息
     *
     * @param $condition array 条件数组
     * @return array 数组格式的返回结果
     */
    public function getMbAgreementInfo($condition = array()) {
        $agreement_info = $this->where($condition)->find();
        return $agreement_info;
    }

    /**
     * 读取多行
     *
     * @param $condition array 条件数组
     * @return array 数组格式的返回结果
     */
    public function getMbAgreementList($condition = array()){
        $agreement_list = $this->where($condition)->select();
        return $agreement_list;
    }

    /**
     * 编辑信息
     *
     * @param $condition array 条件数组
     * @param $data array 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editMbAgreement($data, $condition){
        return $this->where($condition)->update($data);
    }
}
