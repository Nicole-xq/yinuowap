<?php
/**
 * 定制模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class customizeModel extends Model {
    public function __construct() {
        parent::__construct('customize');
    }

    /**
     * 新增定制内容
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrder($data) {
        return $this->insert($data);
    }



    /**
     * 定制咨询内容列表
     * @param array $condition
     * @param string $fields
     * @param integer $pagesize
     * @param string $order
     * @param integer $limit
     * @param string $group
     * @return array
     */
    public function getCodeList($condition = array(), $fields = '*', $pagesize = 0,$order = 'id desc',$limit = 0, $group = '') {
        return $this->field($fields)->where($condition)->page($pagesize)->order($order)->limit($limit)->group($group)->select();
    }


}
