<?php
/**
 * 手机落地页关联商品模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class landing_relation_goodsModel extends Model{

    public function __construct() {
        parent::__construct('landing_relation_goods');
    }

    /**
     * 读取落地页商品列表
     * @param array $condition
     * @param string $page
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getLandingGoodsList($condition, $field='*', $page = '', $order='id desc') {
        return  Model()->table('landing_relation_goods,goods')
                     ->join('left')
                     ->field($field)
                     ->on('landing_relation_goods.goods_id=goods.goods_id')
                     ->where($condition)
                     ->page($page)
                     ->order($order)
                     ->select();
    }

    /**
     * 增加落地页商品记录
     * @param $addData
     * @return bool
     */
    public function addData($addData){
        return $this->insertAll($addData);
    }

    /**
     * 更新
     * @param array $condition
     * @param array $update
     * @return boolean|unknown
     */
    public function updateLandingGoodsData($condition,$update){
        if (empty($condition)) {
            return true;
        }
        return $this->where($condition)->update($update);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function delLandingGoods($condition){
        return $this->where($condition)->delete();
    }

}
