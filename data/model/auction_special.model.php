<?php
/**
 * 拍品专场
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class auction_specialModel extends Model {
    const STATE0 = 0;       // 未审核
    const STATE1 = 10;       // 审核中
    const STATE2 = 20;       // 审核成功
    const STATE3 = 30;       //审核失败
    const STATE4 = 11;       //预展开始
    const STATE5 = 12;       //专场开始
    const STATE6 = 13;       //专场结束

    public function __construct() {
        parent::__construct('auction_special');
    }

    /**
     *拍品专场数量
     *
     * @param array $condition
     * @return array
     */
    public function getSpecialCount($condition) {
        return Model()->table('auction_special')->where($condition)->count();
    }

    /**
     * 专场列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $page
     * @param int $limit
     * @param int $count
     * @return array
     */
    public function getSpecialList($condition, $field = '*', $order = 'special_id desc', $page = 10, $limit = 0, $count = 0) {
        return Model()->table('auction_special')->field($field)->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }

    /**
     * 开启的专场列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $limit
     * @return array
     */
    public function getSpecialOpenList($condition, $field = '*', $order = 'special_id desc', $limit = 0,$page = 10) {
        if(is_array($condition)){
            $condition['special_state'] = array('in',array(self::STATE2,self::STATE4,self::STATE5,self::STATE6));
            $condition['is_open'] = 1;
        }else{
            $str = self::STATE2.','.self::STATE4.','.self::STATE5.','.self::STATE6;
            if($condition != ''){
                $condition .= ' and ';
            }
            $condition .= 'is_open = 1 and special_state in('.$str.')';
        }

        return $this->getSpecialList($condition, $field, $order, 0, $limit);
    }

    /**
     * new开启的专场列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $limit
     * @return array
     */
    public function specialOpenListFetch($condition, $field = '*', $order = 'special_id desc', $limit = 0,$page = 10) {
        return $this->getSpecialList($condition, $field, $order, 0, $limit);
    }
    /**
     * 获得详细信息
     * @param $condition
     * @param $field
     */
    public function getSpecialInfo($condition,$field = "*") {
        return Model()->table('auction_special')->field($field)->where($condition)->find();
    }

    /**
     * 保存专场
     *
     * @param array $insert
     * @param string $replace
     * @return boolean
     */
    public function addSpecial($insert, $replace = false) {
        return Model()->table('auction_special')->pk(array('special_id'))->insert($insert, $replace);
    }

    /**
     * 更新专场
     *
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editSpecial($update, $condition) {
        return Model()->table('auction_special')->where($condition)->update($update);
    }


    /**
     * 删除专场
     * @param array $blids
     * @param int $store_id
     * @return boolean
     */
    public function delSpecial($blids, $store_id) {
        $blid_array = explode(',', $blids);
        foreach ($blid_array as $val) {
            if (!is_numeric($val)) {
                return false;
            }
        }
        $where = array();
        $where['special_id'] = array('in', $blid_array);
        $where['store_id'] = $store_id;
        $bl_list = $this->getSpecialList($where, 'special_id');
        $bl_list = array_under_reset($bl_list, 'special_id');
        $blid_array = array_keys($bl_list);

        $where = array();
        $where['special_id'] = array('in', $blid_array);
        $rs = Model()->table('auction_special')->where($where)->delete();
        if ($rs) {
            return $this->delSpecialGoods($where);
        } else {
            return false;
        }
    }

    /**
     * 删除专场（平台后台使用）
     * @param array $condition
     * @return boolean
     */
    public function delSpecialForAdmin($condition) {
        $rs = Model()->table('auction_special')->where($condition)->delete();
        if ($rs) {
            return $this->delSpecialGoods($condition);
        } else {
            return false;
        }
    }



    /**
     * 专场拍品列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $group
     * @return array
     */
    public function getSpecialGoodsList($condition, $field = '*', $order = 'special_auction_id asc', $group = '',$limit = 0, $page = 0, $count = 0) {
        return Model()->table('au_special_goods')->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }

    //专场拍品表与拍品池表相连接
    public function getSpecialGoodsLists($condition, $field = '*', $order = 'special_auction_id asc', $group = '',$limit = 0, $page = 0, $count = 0) {
        $on = 'au_special_goods.goods_auction_id = goods_auctions.id';
        return Model()->table('au_special_goods,goods_auctions')->field($field)->join('inner')->on($on)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }

    //拍品数量
    public function getSpecialGoodsCount($condition) {
        return Model()->table('au_special_goods')->where($condition)->count();
    }

    /**
     * 保存专场拍品
     *
     * @param unknown $insert
     * @return boolean
     */
    public function addSpecialGoodsAll($insert) {
        $result = Model()->table('au_special_goods')->insertAll($insert);
        if ($result) {
            foreach ((array)$insert as $v) {
                if (isset($v['auction_id'])) $this->_dSpecialCache($v['auction_id']);
            }
        }
        return $result;
    }

    public function addSpecialGoods($insert) {
        $result = Model()->table('au_special_goods')->insert($insert);
        return $result;
    }

    /**
     * 更新专场拍品
     *
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editSpecialGoods($update, $condition) {
        return Model()->table('au_special_goods')->where($condition)->update($update);
    }

    /**
     * 更新
     *
     * @param array $input 更新内容
     * @param string $id 活动内容id
     * @return bool
     */
    public function updatesGoods($input,$id){
        return $this->update1('au_special_goods',$input,'special_auction_id in('.$id.')');
    }

    /**
     * 删除专场拍品
     *
     * @param array $condition
     * @return boolean
     */
    public function delSpecialGoods($condition) {
        $list = $this->getSpecialGoodsList($condition, 'goods_auction_id');
        if (empty($list)) {
            return true;
        }
        $result = Model()->table('au_special_goods')->where($condition)->delete();
        if ($result) {
            foreach ($list as $v) {
                $this->_dSpecialCache($v['goods_auction_id']);
            }
        }
        return $result;
    }

    /**
     * 根据条件批量删除
     *
     * @param array $condition 条件数组
     * @return bool
     */
    public function delGoodsList($condition){
        return $this->delete1('au_special_goods',$this->getCondition($condition));
    }


    /**
     * 构造查询条件
     *
     * @param array $condition 查询条件数组
     * @return string
     */
    private function getCondition($condition){
        $conditionStr   = '';
        if($condition['special_id']>0){
            $conditionStr   .= " and au_special_goods.special_id = '{$condition['special_id']}'";
        }
        if (isset($condition['special_auction_id_in'])){
            if ($condition['special_auction_id_in'] == ''){
                $conditionStr   .= " and special_auction_id in ('')";
            }else{
                $conditionStr   .= " and special_auction_id in ({$condition['special_auction_id_in']})";
            }
        }
        if(isset($condition['auction_sp_state_in'])){
            if ($condition['auction_sp_state_in'] == ''){
                $conditionStr   .= " and auction_sp_state in ('')";
            }else{
                $conditionStr   .= " and auction_sp_state in ({$condition['auction_sp_state_in']})";
            }
        }
        if ((string) $condition['auction_sp_state'] !== ''){
            $conditionStr   .= " and au_special_goods.auction_sp_state='".$condition['auction_sp_state']."'";
        }

        if(intval($condition['auction_id'])>0){
            $conditionStr   .= " and au_special_goods.auction_id='".intval($condition['auction_id'])."'";
        }
        if($condition['auction_name'] != ''){
            $conditionStr   .= " and au_special_goods.auction_name like '%{$condition['auction_name']}%'";
        }
        if(intval($condition['store_id'])>0){
            $conditionStr   .= " and au_special_goods.store_id='".intval($condition['store_id'])."'";
        }
        if($condition['store_name'] != ''){
            $conditionStr   .= " and au_special_goods.store_name like '%{$condition['store_name']}%'";
        }

        return $conditionStr;
    }



    /**
     * 删除拍品专场缓存
     * @param int $goods_id
     * @return boolean
     */
    private function _dSpecialCache($auction_id) {
        return dcache($auction_id, 'auction_special');
    }

//    /**
//     * 根据拍品id获取所在专场信息
//     * @param $condition
//     * @param string $field
//     * @param string $order
//     * @param string $group
//     * @param int $limit
//     * @param int $page
//     * @param int $count
//     * @return mixed
//     */
//    public function getSpecialInfoByAuctionId($condition, $field = '*') {
//        $on = 'au_special_goods.special_id = auction_special.special_id';
//        return Model()->table('au_special_goods,auction_special')->field($field)->join('inner')->on($on)->where($condition)->select();
//    }
}
