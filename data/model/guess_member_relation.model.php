<?php

/**
 * 趣猜关系表
 */
use Shopnc\Lib\StdArray;

defined('InShopNC') or exit('Access Invalid!');

class guess_member_relationModel extends Model
{
    public function __construct(){
        parent::__construct('guess_member_relation');
    }

    /**
     * 获取关系信息
     *
     * @param array $condition 条件
     * @param string $field 字段
     * @return array 二维数组
     */
    public function getRelationInfo($condition, $field = '*')
    {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 通过会员ID和趣猜ID获取
     *
     * @param int $member_id 会员ID
     * @param int $gs_id 趣猜ID
     * @return array
     * */
    public function getRelationInfoByMemberGuessID($member_id, $gs_id)
    {
        return $this->getRelationInfo(array('member_id' => $member_id, 'gs_id' => $gs_id));
    }

    /**
     * 创建会员和趣猜关系
     * @param array $insert 数据
     * @return  int 插入ID
     */
    public function addRelation($insert) {
        return $this->insert($insert);
    }


    /**
     * 编辑会员和趣猜关系
     * @param $update array
     * @param $condition array
     * @return int
     * */
    public function setRelationInfo($update, $condition)
    {
        $result = $this->where($condition)->update($update);
        return $result;
    }

    public function getRelationList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        return $this->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
    }

    public function delRelationInfo($condition)
    {
        return $this->where($condition)->delete();
    }

}