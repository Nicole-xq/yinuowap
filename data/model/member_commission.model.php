<?php
/**
 * 会员返佣模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class member_commissionModel extends Model {
    const TYPE_CHILD_ID = 0;        //童生ID
    const TYPE_XIU_ID = 5;          //秀才ID
    const TYPE_JU_ID = 1;           //举人ID

    const TYPE_CHILD_NUM = 2;        //童生升级次数--实际是3三次当前未加入日志
    const TYPE_XIU_NUM = 9;          //秀才升级次数--实际是10三次当前未加入日志

    public $leverUpDate = '';
    var $type_setting = array(
        1 => array(
            'label' => '普通购物'
        ),2 => array(
            'label' => '拍卖'
        ),3 => array(
            'label' => '保证金返佣'
        ),4 => array(
            'label' => '会员升级'
        ),5 => array(
            'label' => '开店铺'
        )
    );

    var $status_setting = array(
        0 => array(
            'label' => '未结算'
        ),1 => array(
            'label' => '已结算'
        )
    );
    // 返佣类型
    private $commission_type = [
        "1"  => '商品订单',
        "2"  => '拍卖',
        "3"  => '保证金返佣',
        "4"  => '合伙人升级',
        "5"  => '艺术家入驻',
        "6"  => '保证金利息',
        "7"  => '竞拍返佣',
        "8"  => '成交返佣'
    ];

    public function __construct(){
        parent::__construct('member_commission');
        $this->leverUpDate = C('leverUpDate') ? strtotime(C('leverUpDate')) : strtotime('2019-4-18');
    }

    /**
     * 获取返佣列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     */
    public function getCommissionList($condition = array(), $field = '*', $page = null, $order = 'log_id desc', $limit = ''){
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 获取返佣记录数量
     * @param $condition
     * @return mixed
     */
    public function getCommissionCount($condition){
        return $this->where($condition)->count();
    }

    /**
     * 计算返佣金额
     * @param $condition
     * @param $field
     * @return mixed
     */
    public function getCommissionSum($condition,$field = 'commission_amount'){
        return $this->where($condition)->sum($field);
    }

    /**
     * 添加单条返佣记录
     * @param array $data
     * @return mixed
     */
    public function addCommission($data = array()){
        return $this->insert($data);
    }

    /**
     * 修改返佣记录数据
     * @param $conditions
     * @param $data
     * @return bool
     */
    public function updateCommission($conditions,$data){
        return $this->where($conditions)->update($data);
    }

    /*
     * 竞价返佣数据保存佣金奖励表结算
     */
    public function addWaitForCommission($member_id, $commission_num, $auction_info)
    {
        //TODO
        /** @var memberModel $model_member */
        $model_member = Model('member');
        //保存余额变更日志
        $pdModel = Model('predeposit');
        $member_info = $model_member->getMemberInfoByID($member_id);
        //TODO 待优化 这里应该直接查询数据表
        $type_name = [
            0 => 'hy',      //会员
            1 => 'wdl',        //微合伙人
            2 => 'zl',        //战略合伙人
            3 => 'sy',        //事业合伙人
            4 => 'hy',      //艺术家
            5 => 'dz',      //大众合伙人
        ];

        $member_rate_config = Model('member_rate_config');
        //给自己返佣比率

        $msg = '';
        $mine_where = ['type' => $type_name[$member_info['member_type']]];
        $mine_bid_rate_info = $member_rate_config->where($mine_where)->find();
        $mine_bid_rate = $mine_bid_rate_info['bid_rate'];
        //获取上级用户id 和 上上级用户id
        $special_rate_config = Model('special_rate_config');
        /** @var member_distributeModel $member_distribute */
        $member_distribute = Model('member_distribute');
        $distribute_info = $member_distribute->where(['member_id' => $member_id])->find();
        if (!empty($distribute_info)) {
            $top_member = !empty($distribute_info['top_member']) ? $distribute_info['top_member'] : 0;
            $top_member_2 = !empty($distribute_info['top_member_2']) ? $distribute_info['top_member_2'] : 0;
        }
        $commission_insert = [
            'order_id' => 0,               //订单编号
            'order_goods_id' => $auction_info['auction_id'],         //订单商品编号
            'goods_id' => $auction_info['auction_id'],               //商品编号
            'goods_name' => $auction_info['auction_name'],             //商品名称
            'goods_image' => $auction_info['auction_image'],            //订单商品图片
            'goods_pay_amount' => $commission_num,       //商品支付金额
            'from_member_id' => $member_id,         //佣金来源用户ID
            'add_time' => TIMESTAMP,               //添加时间
            'dis_commis_state' => 1,       //佣金结算状态 0未结 1已结
            'commission_type' => 7,        //返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息7竞拍返佣8成交返佣9保证金利息返佣
            'commission_time' => TIMESTAMP,        //返佣结算时间
            'from_member_name' => $member_info['member_truename'],       //返佣来源用户名
            'order_sn' => 0,               //订单编号
            //下边的待重写
            'dis_commis_rate' => '0.00',        //返佣比例
            'commission_amount' => '0.00',      //返佣金额
            'dis_member_id' => $member_id,          //返佣佣金收益人
            'dis_member_name' => $member_info['member_truename']        //返佣收益人用户名
        ];
        //TODO  要重新做
        //output_data($commission_insert);
        //插入奖金记录表信息
        /** @var member_commissionModel $member_commission */
        $member_commission = Model('member_commission');


        if (!empty($mine_bid_rate) || $mine_bid_rate != 0) {
            $rate_num = $commission_num * $mine_bid_rate / 100;
            $person_num = $rate_num = ncPriceFormat($rate_num);
            $mine_bid_rate = ncPriceFormat($mine_bid_rate);
            $mine_update = array('available_predeposit' => array('exp', 'available_predeposit+' . $rate_num));
            $model_member->where(['member_id' => $member_id])->update($mine_update);
            $commission_insert['dis_commis_rate'] = $mine_bid_rate;
            $commission_insert['dis_member_id'] = $member_id;
            $commission_insert['opreate_commission'] = bcmul($commission_num,0.05, 2);       //商品支付金额
            $commission_insert['commission_amount'] = $rate_num;
            $commission_insert['dis_member_name'] = $member_info['member_truename'];
            $commission_insert['source_staff_id'] = $member_info['source_staff_id'];       //用户来源ID
            $result = $member_commission->insert($commission_insert);
            if (!$result) {
                \App\Exceptions\ApiResponseException::throwFailMsg('个人返佣失败 !');
            }
            //做日志使用
            $priceInfo = Model('')->table('member')
                ->field('available_predeposit')
                ->where(['member_id' => $member_id])->find();
            if (isset($priceInfo['available_predeposit'])) {
                $commission_insert['lg_available_amount'] = $priceInfo['available_predeposit'];
                $pdModel->insertPdLog($commission_insert);
            }
        }

        if (isset($top_member) && $top_member) {
            $top_member_info = $model_member->getMemberInfoByID($top_member);
            $top_member_bid_rate_info = $special_rate_config
                ->where(['partner_lv_type' => $type_name[$top_member_info['member_type']], 'partner_lv' => 1])->find();
            $top_member_bid_rate = $top_member_bid_rate_info['rate'];
            $rate_num = $commission_num * $top_member_bid_rate / 100;
            $rate_num = ncPriceFormat($rate_num);
            if (!empty($rate_num) && $rate_num != 0) {
                $top_member_update = array(
                    'available_predeposit' => array('exp', 'available_predeposit+' . $rate_num)
                );
                $top_member_bid_rate = ncPriceFormat($top_member_bid_rate);
                $model_member->where(['member_id' => $top_member])->update($top_member_update);
                //上级用户返佣记录
                $commission_insert['dis_commis_rate'] = $top_member_bid_rate;
                $commission_insert['dis_member_id'] = $top_member;
                $commission_insert['commission_amount'] = $rate_num;
                $commission_insert['dis_member_name'] = $top_member_info['member_truename'];
                $commission_insert['source_staff_id'] = $top_member_info['source_staff_id'];       //用户来源ID
                $member_commission->insert($commission_insert);

                //做日志使用
                $priceInfo = Model('')->table('member')
                    ->field('available_predeposit')
                    ->where(['member_id' => $top_member])->find();
                if (isset($priceInfo['available_predeposit'])) {
                    $commission_insert['lg_available_amount'] = $priceInfo['available_predeposit'];
                    $pdModel->insertPdLog($commission_insert);
                }
            }
        }
        $model_setting = Model('setting');
        $setting = $model_setting->getRowSetting("level_two_rebate");
        if ($setting && $setting['value']==1) {
            if (isset($top_member_2) && $top_member_2) {
                $top_member_2_info = $model_member->getMemberInfoByID($top_member_2);
                $top_member_2_bid_rate_info = $special_rate_config
                    ->where(
                        ['partner_lv_type' => $type_name[$top_member_2_info['member_type']], 'partner_lv' => 2]
                    )->find();
                $top_member_2_bid_rate = $top_member_2_bid_rate_info['rate'];
                $rate_num = $commission_num * $top_member_2_bid_rate / 100;
                $rate_num = ncPriceFormat($rate_num);
                if (!empty($rate_num) && $rate_num != 0) {
                    $top_member_2_update = array(
                        'available_predeposit' => array('exp', 'available_predeposit+' . $rate_num)
                    );
                    $top_member_2_bid_rate = ncPriceFormat($top_member_2_bid_rate);
                    $model_member->where(['member_id' => $top_member_2])->update($top_member_2_update);
                    //上上级用户返佣记录
                    $commission_insert['dis_commis_rate'] = $top_member_2_bid_rate;
                    $commission_insert['dis_member_id'] = $top_member_2;
                    $commission_insert['commission_amount'] = $rate_num;
                    $commission_insert['dis_member_name'] = $top_member_2_info['member_truename'];
                    $commission_insert['source_staff_id'] = $top_member_2_info['source_staff_id'];       //用户来源ID
                    $member_commission->insert($commission_insert);

                    //做日志使用
                    $priceInfo = Model('')->table('member')
                        ->field('available_predeposit')
                        ->where(['member_id' => $top_member_2])->find();
                    if (isset($priceInfo['available_predeposit'])) {
                        $commission_insert['lg_available_amount'] = $priceInfo['available_predeposit'];
                        $pdModel->insertPdLog($commission_insert);
                    }
                }
            }
        }
        try {
            $msg = $this->memberLevelUp($member_id);
        } catch (\Exception $e) {
            yLog()->error($e->getMessage());
        }
        $person_num = is_numeric($person_num) ? $person_num : 0;
        return [
            'commission_num' => number_format($person_num, 2),
            'msg' => $msg
        ];
    }


    //TODO //TODO

    /**
     * 新手竞价 若用户等级小于举人则进行升级
     * @param $memberId
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function memberLevelUp($memberId)
    {
        //判断用户缴纳保证金次数
        $memberModel = Model('member');
        $memberInfo = $memberModel->getMemberInfo(array('member_id' => $memberId));
        if (in_array($memberInfo['member_type'], [self::TYPE_CHILD_ID])) {
//            $upWhere = [
//                'member_id' => $memberId,
//                'bid_type' => \App\Models\ShopncBidLog::BID_TYPE_AUCTION
//            ];
            //目前新老用户出价都可以进行升级操作
            $upWhere = [];
            $upWhere['member_id'] = $memberId;
            $upWhere['created_at'] = ['gt', $this->leverUpDate];
            $bidModel = Model('bid_log');
            $upWhere['bid_type'] = ['neq', bid_logModel::BID_TYPE_EXP];
            $marginNum = $bidModel->where($upWhere)->count();
            $msg = '';
            if ($marginNum) {
                if ($memberInfo['member_type'] == self::TYPE_CHILD_ID) {
                    if ($marginNum == self::TYPE_CHILD_NUM) {
                        //用户进行升级操作
                        $memberModel->beginTransaction();
                        $update_data = array(
                            'member_type' => self::TYPE_XIU_ID
                        );
                        if (!$memberModel->editMember(array('member_id' => $memberId), $update_data)) {
                            yLog()->error($memberId . '新手专区出价三次,升级秀才失败!');
                            throw new  \Exception ('数据库更新出错!');
                        }
                        if (!$memberModel->editMemberDistribute(array('member_type' => self::TYPE_XIU_ID), array('member_id' => $memberId))) {
                            yLog()->error($memberId . '新手专区出价三次,升级秀才返佣比例修改失败!');
                            throw new  Exception ('数据库更新出错!');
                        }
                        $memberModel->commit();
                        //$msg = '新手专区出价三次,升级秀才返佣比例修改失败!';
                        $msg = 'xiucai';
                    }
                } elseif ($memberInfo['member_type'] == self::TYPE_XIU_ID) {
                    if ($marginNum == 9) {
                        //用户进行升级操作
                        $memberModel->beginTransaction();
                        $update_data = array(
                            'member_type' => self::TYPE_JU_ID
                        );
                        if (!$memberModel->editMember(array('member_id' => $memberId), $update_data)) {
                            yLog()->error($memberId . '新手专区出价十次,升级举人失败!');
                            throw new  \Exception ('数据库更新出错!');
                        }
                        if (!$memberModel->editMemberDistribute(array('member_type' => self::TYPE_JU_ID), array('member_id' => $memberId))) {
                            yLog()->error($memberId . '新手专区出价十次,升级举人返佣比例修改失败!');
                            throw new  Exception ('数据库更新出错!');
                        }
                        $memberModel->commit();
                        $msg = 'juren';
                    } else {
                        return $msg;
                    }
                }
            }
        }
        //TODO 升级成功发送通知消息
        return $msg;
    }


    /*
     * 注意仅仅针对拍卖 普通商品返佣不可调用
     * 拍品成交返佣数据保存佣金奖励表结算
     */
    //public function transactionForCommission($member_id, $offer_num, $margin_info)
    public function transactionForCommission($order_info)
    {
        //TODO
        //判断用户该拍品是否已经进行成交返佣操作
        $condition = [
            'from_member_id' => $order_info['buyer_id'],
            'order_goods_id' => $order_info['auction_id'],
            'commission_type' => 8
        ];
        $member_commission = Model('member_commission');
        $exist_commission = $member_commission->where($condition)->find();
        if ($exist_commission) {
            return false;
        }
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $member_id = $order_info['buyer_id'];
        $member_info = $model_member->getMemberInfoByID($member_id);
        //TODO 待优化 这里应该直接查询数据表
        $type_name = [
            0 => 'hy',      //会员
            1 => 'wdl',        //微合伙人
            2 => 'zl',        //战略合伙人
            3 => 'sy',        //事业合伙人
            4 => 'hy',      //艺术家
            5 => 'dz',      //大众合伙人
        ];

        $member_rate_config = Model('member_rate_config');
        //给自己返佣比率

        $mine_where = ['type' => $type_name[$member_info['member_type']]];
        $mine_bid_rate_info = $member_rate_config->where($mine_where)->find();
        $mine_bid_rate = $mine_bid_rate_info['deal_rate'];
        $auction_model = Model('auctions');
        $auction_info = $auction_model->where(['auction_id' => $order_info['auction_id']])->find();
        //获取上级用户id 和 上上级用户id
        $commission_insert = [
            'order_id' => $order_info['order_id'],               //订单编号
            'order_goods_id' => $order_info['auction_id'],         //订单商品编号
            'goods_id' => $order_info['auction_id'],               //商品编号
            'goods_name' => $order_info['auction_name'],             //商品名称
            'goods_image' => isset($auction_info['auction_image']) ? $auction_info['auction_image'] : '', //订单商品图片
            'goods_pay_amount' => '',       //商品支付金额
            'from_member_id' => $member_id,         //佣金来源用户ID
            'add_time' => TIMESTAMP,               //添加时间
            'dis_commis_state' => 0,       //佣金结算状态 0未结 1已结
            'commission_type' => 8,        //返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息7竞拍返佣8成交返佣9保证金利息返佣
            'commission_time' => TIMESTAMP,        //返佣结算时间
            'from_member_name' => $member_info['member_truename'],       //返佣来源用户名
            'source_staff_id' => $member_info['source_staff_id'],       //返佣账户的来源员工ID
            'order_sn' => $order_info['order_sn'],               //订单编号
            //下边待重写
            'top_lv' => '',                 //上级级别(1:上级,2:上级的上级..对应三级分销)
            'dis_member_id' => '',          //返佣佣金收益人
            'commission_amount' => '',      //返佣金额
            'dis_commis_rate' => '',        //返佣比例
            'dis_member_name' => '',        //返佣收益人用户名
        ];
        $offer_num = $order_info['order_amount'];
        //插入奖金记录表信息
        if (!empty($mine_bid_rate) && $mine_bid_rate != 0) {
            $rate_num = $offer_num * $mine_bid_rate / 100;
            $rate_num = ncPriceFormat($rate_num);
            $commission_insert['top_lv'] = 0;
            $commission_insert['dis_member_id'] = $member_id;
            $commission_insert['commission_amount'] = $rate_num;
            $commission_insert['dis_commis_rate'] = $mine_bid_rate;
            $commission_insert['dis_member_name'] = $member_info['member_truename'];
            $commission_insert['source_staff_id'] = $member_info['source_staff_id'];       //用户来源ID
            $member_commission->insert($commission_insert);
        }
        //判断是否为拍品订单, 拍品订单返佣按照配置来进行返佣
        $special_rate_config = Model('special_rate_config');
        $distribute_info = Model()->table('member_distribute')->where(['member_id' => $member_id])->find();
        if (!empty($distribute_info)) {
            $top_member = !empty($distribute_info['top_member']) ? $distribute_info['top_member'] : 0;
            $top_member_info = $model_member->getMemberInfoByID($top_member);
            $top_member_2 = !empty($distribute_info['top_member_2']) ? $distribute_info['top_member_2'] : 0;
            $top_member_2_info = $model_member->getMemberInfoByID($top_member_2);
            $top_member_bid_rate_info = $special_rate_config
                ->where([
                    'partner_lv_type' => $type_name[$top_member_info['member_type']],
                    'partner_lv' => 1
                ])->find();
            $top_member_2_bid_rate_info = $special_rate_config
                ->where(
                    ['partner_lv_type' => $type_name[$top_member_2_info['member_type']], 'partner_lv' => 2]
                )->find();
            $top_member_bid_rate = $top_member_bid_rate_info['deal_rate'];
            $top_member_2_bid_rate = $top_member_2_bid_rate_info['deal_rate'];
        }
        if (isset($top_member) && $top_member && !empty($top_member_bid_rate) && $top_member_bid_rate != 0) {
            $rate_num = $offer_num * $top_member_bid_rate / 100;
            $rate_num = ncPriceFormat($rate_num);
            //上级用户返佣记录
            $commission_insert['top_lv'] = 1;
            $commission_insert['dis_member_id'] = $top_member;
            $commission_insert['commission_amount'] = $rate_num;
            $commission_insert['dis_commis_rate'] = $top_member_bid_rate;
            $commission_insert['dis_member_name'] = $top_member_info['member_truename'];
            $commission_insert['source_staff_id'] = $top_member_info['source_staff_id'];       //用户来源ID
            $member_commission->insert($commission_insert);
        }
        if (isset($top_member_2) && $top_member_2 && !empty($top_member_2_bid_rate) && $top_member_2_bid_rate != 0) {
            $rate_num = $offer_num * $top_member_2_bid_rate / 100;
            $rate_num = ncPriceFormat($rate_num);
            //上上级用户返佣记录
            $commission_insert['top_lv'] = 2;
            $commission_insert['dis_member_id'] = $top_member_2;
            $commission_insert['commission_amount'] = $rate_num;
            $commission_insert['dis_commis_rate'] = $top_member_2_bid_rate;
            $commission_insert['dis_member_name'] = $top_member_2_info['member_truename'];
            $commission_insert['source_staff_id'] = $top_member_2_info['source_staff_id'];       //用户来源ID
            $member_commission->insert($commission_insert);
        }
        $commission_amount = Model('')->table('member_commission')->where(['order_goods_id' => $order_info['auction_id']])->sum('commission_amount');
        //$total_commission_amount = bcadd($order_info['pd_amount'], $bid_comssion, 2);
        //成交返佣在支付完成之后生成
        //奖励金结算金额=（订单金额-商品成本价-返佣总金额（竞价返佣+成交返佣））*60%
        $operate_commission = bcmul(bcsub($auction_info['current_price'], bcadd($auction_info['auction_cost'], $commission_amount, 2), 2), 0.6, 2);
        Model('')->table('orders')->where(['order_id' => $order_info['order_id']])->update(['commission_amount' => $operate_commission]);
    }


    /**获取购买商品订单返佣列表
     * @param array $condition
     * @param string $field
     * @param null $page
     * @param string $order
     * @param string $limit
     * @return mixed
     */
    public function getByTypeCommission($type, $condition = array(), $field = '*', $page = null, $order = 'log_id desc', $limit = ''){
        $condition = array_merge($condition, $this->get_type_filter($type));
        return $this->getCommissionList($condition, $field, $page, $order, $limit);
    }


    /**获取订单的佣金总额
     * @param $member_id            会员id
     * @param $form_member_id       来源会员id
     */
    public function getMemberCommissionCount($type = '', $member_id = null, $form_member_id = null, $where = null){
        $filter = array();

        $filter = array_merge($filter, $this->get_type_filter($type));
        $filter['dis_commis_state'] = 1;
        if(!empty($member_id)){
            $filter['dis_member_id'] = $member_id;
        }

        if(!empty($form_member_id)){
            $filter['from_member_id'] = $form_member_id;
        }

        if($where){
            $filter = array_merge($filter,$where);
        }
        //$filter['dis_commis_state'] = 1;
        return $this->where($filter)->sum('commission_amount');
    }


    public function get_type_filter($type){
        $filter = array('commission_type'=>array('neq',6));
        switch($type){
            case 1:     //交易返佣
                $filter['commission_type'] = array('in', '1,2');
                break;
            case 2:     //保证金返佣
                $filter['commission_type'] = 3;
                break;
            case 3:     //推荐返佣
               $filter['commission_type'] = array('in', '4,5');
                break;
            case 7:     //竞拍返佣
                $filter['commission_type'] = 7;
                break;
        }
        return $filter;
    }

    /**批量获取下级会员返佣总金额
     * @param $condition 筛选条件
     * @return array
     */
    public function get_members_commission($condition){
        $field = "from_member_id,sum(commission_amount) as commission_amount";
        return $this->field($field)->where($condition)->group('from_member_id')->select();
    }

    /**
     * 获取用户下级返给用户上级的金额
     * @param $member_id
     */
    public function get_special_commission_amount($member_id){
        $sql = 'select sum(commission_amount) amount from shopnc_member_commission WHERE top_lv = 2 AND from_member_id in (SELECT member_id from shopnc_member_distribute where top_member = '.$member_id.');';
        $re = $this->aquery($sql);
//        dd();
        return $re[0]['amount'];
    }

    /**
     * 获取各种返佣类型总额金额
     * @param array $condition
     */
    public function getCommissionAmountByType($condition = array()){
        $field = "commission_type,sum(commission_amount) as commission_amount";
        return $this->field($field)->where($condition)->group('commission_type')->select();
    }

    /**
     * 获取返佣类型
     * @return array
     */
    public function getCommissionType() {
        return $this->commission_type;
    }

    /**
     * 获取返佣信息
     * @param $condition
     * @return array
     */
    public function getCommission($condition)
    {
        return $this->where($condition)->select();
    }
    /**
     * 获取各种返佣类型总额金额
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getCommissionInfo($condition = array(), $field = '*'){
        return $this->field($field)->where($condition)->find();
    }

}