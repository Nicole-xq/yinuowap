<?php
/**
 * 书画推荐专场
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class special_recModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     *推荐专场数量
     *
     * @param array $condition
     * @return array
     */
    public function getSpecialRecCount($condition) {
        return Model()->table('special_rec')->where($condition)->count();
    }

    /**
     * 推荐专场列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $page
     * @param int $limit
     * @param int $count
     * @return array
     */
    public function getSpecialRecList($condition, $field = '*', $order = 'special_rec_id desc', $page = 10, $limit = 0, $count = 0) {
        return Model()->table('special_rec')->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }

    //推荐专场表与专场表相连接
    public function getSpecialRecLists($condition, $field = '*', $order = 'special_rec_id desc,rec_sort asc', $limit = 0, $page = 0, $count = 0) {
        $on = 'special_rec.special_id = auction_special.special_id';
        return Model()->table('special_rec,auction_special')->field($field)->join('inner')->on($on)->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }



    /**
     * 获得详细信息
     */
    public function getSpecialRecInfo($condition) {
        return Model()->table('special_rec')->where($condition)->find();
    }

    /**
     * 保存推荐专场
     *
     * @param array $insert
     * @param string $replace
     * @return boolean
     */
    public function addSpecialRec($insert, $replace = false) {
        return Model()->table('special_rec')->pk(array('special_rec_id'))->insert($insert, $replace);
    }

    /**
     * 更新推荐专场
     *
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editSpecialRec($update, $condition) {
        return Model()->table('special_rec')->where($condition)->update($update);
    }


    /**
     * 删除推荐专场
     * @param array $blids
     * @param int $store_id
     * @return boolean
     */
    public function delSpecialRec($id) {
        $sid= explode(',',$id);
        $rec_array = $this->getSpecialRecList(array('special_id'=>array('in',$sid)), 'special_id,special_rec_pic');
        foreach ($rec_array as $value) {
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_PAINTING.DS.$value['special_rec_pic']);
        }
        return $this->delete1('special_rec','special_id in('.$id.')');
    }








}
