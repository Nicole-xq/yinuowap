<?php
/**
 * 区域代理订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class agent_orderModel extends Model {

   public function __construct(){
        parent::__construct('agent_order');
   }

   public function addData($param){
        return $this->insert($param);
   }

   public function updateInfo($condition,$param){
       return $this->where($condition)->update($param);
   }

   public function getList($condition = [], $field = '*', $page = null, $order = 'id desc', $limit = '',$group = ''){
       return $this->field($field)->where($condition)->group($group)->page($page)->order($order)->limit($limit)->select();
   }

   public function getRowCount($where, $field = '*', $order = '', $group = '') {
        return $this->field($field)->where($where)->group($group)->order($order)->find();
   }

   public function getAgentListBySQL($condition,$field,$page,$page_rows = 15){
       $sql = "SELECT {$field},
                sum( case when commission_type = '1' then order_price else 0 end) as 'sum_order_price_1',
                sum( case when commission_type = '2' then order_price else 0 end) as 'sum_order_price_2',
                sum( case when commission_type = '3' then order_price else 0 end) as 'sum_order_price_3',
                sum( case when commission_type = '4' then order_price else 0 end) as 'sum_order_price_4',
                sum( case when commission_type = '5' then order_price else 0 end) as 'sum_order_price_5',
                
                sum( case when commission_type = '1' then commission_amount else 0 end) as 'sum_commission_amount_1',
                sum( case when commission_type = '2' then commission_amount else 0 end) as 'sum_commission_amount_2',
                sum( case when commission_type = '3' then commission_amount else 0 end) as 'sum_commission_amount_3',
                sum( case when commission_type = '4' then commission_amount else 0 end) as 'sum_commission_amount_4',
                sum( case when commission_type = '5' then commission_amount else 0 end) as 'sum_commission_amount_5'
                FROM ".$this->getTableName()."
                WHERE {$condition}
                GROUP BY agent_id
                ORDER BY id DESC 
                LIMIT {$page},{$page_rows}
       ";
       return $this->query($sql);
   }

   public function getCountNum($condition){
       $sql = " SELECT COUNT(*) as nums
                FROM (
                    SELECT COUNT(id) as count_num
                    FROM " . $this->getTableName() . "
                    WHERE {$condition}
                    GROUP BY agent_id
                ) temp_tab ";
       return $this->query($sql);
   }


}