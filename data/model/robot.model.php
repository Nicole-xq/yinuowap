<?php
/**
 * 
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class robotModel extends Model{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 增加
     *
     * @param
     * @return int
     */
    public function add($array) {
        $id = Model()->table('robot')->insert($array);
        return $id;
    }

    /**
     * 获取列表
     */
    public function getList($condition = array(),$page = 0, $field = '*', $order = 'robot_id desc', $limit = 0, $group = ''){
        return Model()->table('robot')->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page)->select();
    }

    /**
     * 获取详情
     */
    public function getInfo($condition, $field = '*', $order = 'robot_id desc'){
        return Model()->table('robot')->field($field)->where($condition)->order($order)->find();
    }

    public function updateInfo($condition,$data,$limit = null){
        return Model()->table('robot')->where($condition)->limit($limit)->update($data);
    }
}