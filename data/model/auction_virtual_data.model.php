<?php


defined('InShopNC') or exit('Access Invalid!');

class auction_virtual_dataModel extends Model
{

    public function __construct(){
        parent::__construct('auction_virtual_data_config');
    }

    /**
     * 获取配置信息
     * @param $condition
     * @return array
     */
    public function getConfig($condition){
        return $this->where($condition)->find();
    }


}