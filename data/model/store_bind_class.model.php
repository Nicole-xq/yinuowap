<?php
/**
 * 店铺分类分佣比例
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class store_bind_classModel extends Model{

    public function __construct(){
        parent::__construct('store_bind_class');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getStoreBindClassList($condition,$page='',$order='',$field='*', $limit = ''){
        $this->format_params($condition);
        $result = Model()->table('store_bind_class')->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
        return $result;
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getStoreBindClassInfo($condition){
        $this->format_params($condition);
        $result = $this->where($condition)->find();
        return $result;
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function addStoreBindClass($param){
        return $this->insert($param);
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function addStoreBindClassAll($param){
        return $this->insertAll($param);
    }

    /*
     * 更新
     * @param array $update
     * @param array $condition
     * @return bool
     */
    public function editStoreBindClass($update, $condition){
        return $this->where($condition)->update($update);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function delStoreBindClass($condition){
        return $this->where($condition)->delete();
    }

    /**
     * 总数量
     * @param unknown $condition
     */
    public function getStoreBindClassCount($condition = array()) {
        return $this->where($condition)->count();
    }

    /**
     * 取得店铺下商品分类佣金比例
     * @param array $goods_list
     * @return array 店铺ID=>array(分类ID=>佣金比例)
     */
    public function getStoreGcidCommisRateList($goods_list) {
        if (empty($goods_list) || !is_array($goods_list)) return array();

        // 获取绑定所有类目的自营店
        $own_shop_ids = Model('store')->getOwnShopIds(true);

        //定义返回数组
        $store_gc_id_commis_rate = array();

        //取得每个店铺下有哪些商品分类
        $store_gc_id_list = array();

        if(!$this->goodsClassInfo){
            /** @var goods_classModel $gc 商品分类 */
            $gc = Model('goods_class');
            $this->goodsClassInfo = $gc->getGoodsClassForCacheModel();
        }

        foreach ($goods_list as $goods) {
            if (!intval($goods['gc_id'])) continue;
                if (in_array($goods['store_id'], $own_shop_ids)) {
                    //平台店铺佣金为0
                    $store_gc_id_commis_rate[$goods['store_id']][$goods['gc_id']] = 0;
                } else {
                    $store_id = $goods['store_id'];
                    //获取商品分类所属的二级分类 以便计算佣金
                    if(!$this->cacheStoreClassInfo[$goods['gc_id']]['parent_class']){
                        $parent_class = $goods['gc_id'];
                        $info = $this->goodsClassInfo[$goods['gc_id']];
                        for($i = $info['depth']; $i > 2; $i--){
                            $parent_class = $this->goodsClassInfo[$parent_class]['gc_parent_id'];
                        }
                        $this->cacheStoreClassInfo[$goods['gc_id']]['parent_class'] = $parent_class;
                    }

                    if(!$this->cacheStoreClassInfo[$store_id][$goods['gc_id']]['commis_rate']){
                        $condition['store_id'] = $store_id;
                        //新老数据都按照二级类目计算佣金
                        $condition['class_2'] = array('in',$parent_class);
                        $bind_info = $this->where($condition)->find();
                        $this->cacheStoreClassInfo[$store_id][$goods['gc_id']]['commis_rate'] = $bind_info['commis_rate'];
                    }

                    $store_gc_id_commis_rate[$store_id][$goods['gc_id']] = $this->cacheStoreClassInfo[$store_id][$goods['gc_id']]['commis_rate'];
                }
        }

        return $store_gc_id_commis_rate;
    }


    /** 兼容老版本 3级类目筛选
     * @param $condition 搜索参数
     * @return mixed
     */
    public function format_params(&$condition){
        if($condition['class_1|class_2|class_3']){
            $data = Model('goods_class')->getGoodsClassLineForTag($condition['class_1|class_2|class_3']);

            if($data['gc_id_3'] == $condition['class_1|class_2|class_3']){
                $class_id = $condition['class_1|class_2|class_3'];
                unset($condition['class_1|class_2|class_3']);
                $condition['class_2'] = $data['gc_id_2'];
            }

        }

        return $condition;
    }
}
