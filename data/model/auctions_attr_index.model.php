<?php
/**
 * 拍卖商品与属性对应
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class auctions_attr_indexModel extends Model {
    public function __construct() {
        parent::__construct('auctions_attr_index');
    }

    /**
     * 对应列表
     *
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAuAttrIndexList($condition, $field = '*') {
        return $this->where($condition)->field($field)->select();
    }
}
