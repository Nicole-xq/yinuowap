<?php
/**
 * 保证金返佣峰值配置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class auction_special_rate_checkModel extends Model {


    public function __construct() {
        parent::__construct('special_rate_config');
    }


    /**
     * 专场列表
     *
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param int $page
     * @param int $limit
     * @param int $count
     * @return array
     */
    public function getList($condition, $field = '*', $order = 'id desc', $page = 10, $limit = 0, $count = 0) {
        return $this->field($field)->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }

    /**
     * 获取单条信息详情
     * @param $id
     * @return null
     */
    public function getInfo($id){
        return $this->where(array('id'=>$id))->find();
    }

    /**
     * 更新数据
     * @param $condition
     * @param $param
     * @return bool
     */
    public function updateInfo($condition,$param){
        return $this->where($condition)->update($param);
    }


}
