<?php
/**
 * Created by Test, 2017/05/19 15:41.
 * @author serpent.
 *
 * Copyright (c) 2017 serpent All rights reserved.
 */
defined('InShopNC') or exit('Access Invalid!');
class underline_partner_orderModel extends Model
{

    public function __construct()
    {
        parent::__construct('underline_partner_order');
    }

    /**
     * 创建线下升级合伙人订单
     * @param $param
     * @return mixed
     */
    public function create_order($param){
        return $this->insert($param);
    }

    public function updateInfo($data,$condition){
        return $this->where($condition)->update($data);
    }

    public function getOrderInfo($condition){
        return $this->where($condition)->find();
    }

    public function getOrderList($condition = array(), $field = '*', $page = null, $order = 'id desc', $limit = '') {
       return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

}