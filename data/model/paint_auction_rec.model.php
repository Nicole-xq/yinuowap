<?php
/**
 * 书画推荐
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class paint_auction_recModel extends Model{

    public function __construct(){
        parent::__construct('paint_auction_rec');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getPaintRecList($condition, $page = '', $order = '', $field = '*', $gourpby = '', $key = '',$count = null) {
        return $this->field($field)->where($condition)->page($page,$count)->order($order)->group($gourpby)->key($key)->select();
    }

    public function getPaintRecTagList($condition, $page = '', $order = '', $field = '*', $gourpby = '', $key = '',$count = null) {
        $on = 'paint_auction_rec.module_title_id = paint_module.paint_module_id';
        $result = Model()->table('paint_auction_rec,paint_module')->field($field)->join('inner')->on($on)->where($condition)->page($page,$count)->order($order)->group($gourpby)->key($key)->select();
        return $result;
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getPaintRecInfo($condition) {
        return $this->where($condition)->find();
    }

    /*
     * 增加
     * @param array $data
     * @return bool
     */
    public function addPaintRec($data){
        return $this->insertAll($data);
    }

    public function editPaintRec($data,$condition) {
        return $this->where($condition)->update($data);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function delPaintRec($condition){
        return $this->where($condition)->delete();
    }

    public function getPaintRecCount($condition = array(), $field = '') {
        return $this->where($condition)->count($field);
    }

}
