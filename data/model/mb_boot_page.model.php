<?php
/**
 * 手机引导页模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class mb_boot_pageModel extends Model{

    public function __construct() {
        parent::__construct('mb_boot_page');
    }

    /**
     * 读取引导页列表
     * @param array $condition
     * @param string $page
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getBootPageList($condition, $page = '', $order='id desc', $field='*') {
        return  Model()->table('mb_boot_page')->field($field)->where($condition)->page($page)->order($order)->select();
    }

    /**
     * 获取设置的引导页
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getBootPageInfo($condition, $field='*') {
        $list = Model()->table('mb_boot_page')->field($field)->where($condition)->find();
        return $list;
    }

    /**
     * 增加引导页记录
     * @param $addData
     * @return int
     */
    public function addData($addData){
        return Model()->table('mb_boot_page')->insert($addData);
    }

    /**
     * 更新
     * @param array $condition
     * @param array $update
     * @return boolean|unknown
     */
    public function updateBootData($condition,$update){
        if (empty($condition)) {
            return true;
        }
        return Model()->table('mb_boot_page')->where($condition)->update($update);
    }

}
