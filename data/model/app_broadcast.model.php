<?php
/**
 * APP广播模型
 */
defined('InShopNC') or exit('Access Invalid!');
class app_broadcastModel extends Model{
    public function __construct() {
        parent::__construct('app_broadcast');
    }

    /**
     * 公告列表
     * @param array $condition
     * @param string $field
     * @param int $page
     * @param string $order
     * @return
     */
    public function getBroadcastList($condition, $field = '*', $page = 100, $order = 'id DESC') {
        return $this->field($field)->where($condition)->order($order)->page($page)->select();
    }

    /**
     * 公告详细信息
     * @param array $condition
     * @param string $field
     * @return
     */
    public function getBroadcastInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 增加公告信息
     * @param array $add_array
     * @return int
     */
    public function addBroadcast($add_array) {
        return $this->insert($add_array);
    }

    /**
     * 编辑公告消息内容
     * @param array $condition
     * @param array $update
     * @return integer
     */
    public function editBroadcast($condition, $update) {
        return $this->where($condition)->update($update);
    }
}
