<?php
/**
 * 拍卖违约log
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/20 0020
 * Time: 17:23
 * @author ADKi
 */


defined('InShopNC') or exit('Access Invalid!');

class auction_default_logModel extends Model
{
    public function __construct(){
        parent::__construct('auction_default_log');
    }

    public function addDefaultLog($param){
        return Model()->table('auction_default_log')->insert($param);
    }
}
