<?php
/**
 * 关注商家模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class store_attentionModel extends Model {


    public function __construct() {
        parent::__construct('store_attention');
    }


    /**
     * 查询关注列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getAttentionList($condition, $field = '*',$page = null, $order = '', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }


    /**
     * 关注数量
     * @param array $condition
     * @return int
     */
    public function getAttentionCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询单条关注信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getAttentionInfo($condition) {
        $attention_info = $this->where($condition)->find();
        return $attention_info;
    }

    /**
     * 添加关注
     * @param unknown $insert
     * @return boolean
     */
    public function addAttention($insert) {
        return $this->insert($insert);
    }


    /**
     * 删除关注
     * @param unknown $condition
     * @return boolean
     */
    public function delAttention($condition) {
        return $this->where($condition)->delete();
    }


}
