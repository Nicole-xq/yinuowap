<?php
/**
 * 手机端令牌模型
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

defined('InShopNC') or exit('Access Invalid!');

class mb_user_tokenModel extends Model{
    public function __construct(){
        parent::__construct('mb_user_token');
    }

    /**
     * 查询
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getMbUserTokenInfo($condition) {
        $token = $this->where($condition)->find();
        $token = $this->_checkMemberId($token);
        return $token;
    }

    /**
     * 检测member_id
     *
     * @param
     * @return mixed
     */
    private function _checkMemberId($token)
    {
        if (!empty($token) && (empty($token['member_id']) || empty($token['member_name'])) && !empty($token['user_name'])) {
            $condition = ['member_name' => $token['user_name']];
            /** @var memberModel $member_model */
            $member_model = Model('member');
            $member = $member_model->getMemberInfo($condition);
            if ($member) { // 如果有用户，返回token
                $update = array(
                    'member_id' => $member['member_id'],
                    'member_name' => $member['member_name'],
                );
                $this->upMbUserToken(['token_id'=>$token['token_id']], $update);

                $token['member_id'] = $member['member_id'];
                $token['member_name'] = $member['member_name'];
            } else { // 如果有用户，同步用户信息
                $result = $member_model->loginUCenter(['username' => $token['user_name']]);
                if(isset($result->status_code) && $result->status_code == 200){
                    $member = $result->data;
                    $re = $member_model->ucenterToShopnc($member);
                    if (empty($re)) {
                        return false;
                    }
                    $token['member_id'] = $re;
                    $token['member_name'] = $token['user_name'];
                } else {
                    return false;
                }
            }
        }
        if (empty($token['member_id'])){
            return false;
        }
        return $token;
    }

    public function getMbUserTokenInfoByToken($token) {
        if(empty($token)) {
            return null;
        }
        $redisKey = \Yinuo\Entity\Constant\RedisKeyConstant::TOKEN_INFO.$token;
        //缓存用户token信息
        $tokenInfo = \Yinuo\Lib\HelperFactory::instance()->Redis->getRedis()->get($redisKey);
        if($tokenInfo){
            return json_decode($tokenInfo, true);
        }
        $tokenInfo = $this->getMbUserTokenInfo(array('token' => $token));
        if($tokenInfo){
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedis()->setex($redisKey, \Yinuo\Entity\Constant\RedisKeyConstant::TOKEN_INFO_EXPIRES, json_encode($tokenInfo));
        }
        return $tokenInfo;
    }

    public function upMbUserToken($condition, $update){
        return $this->where($condition)->update($update);
    }

    public function updateMemberOpenId($token, $openId)
    {
        $condition = array('token' => $token);
        $update = array('openid' => $openId);
        return $this->upMbUserToken($condition, $update);
    }

    /**
     * 新增
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addMbUserToken($param){
        return $this->insert($param);
    }

    /**
     * 删除
     *
     * @param int $condition 条件
     * @return bool 布尔类型的返回结果
     */
    public function delMbUserToken($condition){
        return $this->where($condition)->delete();
    }
}
