<?php
/**
 * 银行卡绑定模型管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class bankcardModel extends Model {


    public function __construct() {
        parent::__construct('bankcard');
    }


    /**
     * 查询银行卡绑定列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @param string $limit 取多少条
     * @return array
     */
    public function getbankCardList($condition, $page = null, $order = '', $field = '*', $limit = '') {
        $result = $this->field($field)->where($condition)->order($order)->limit($limit)->page($page)->select();
        return $result;
    }



    /**
     * 银行卡绑定数量
     * @param array $condition
     * @return int
     */
    public function getbankCardCount($condition) {
        return $this->where($condition)->count();
    }


    /**
     * 查询银行卡绑定信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getbankCardInfo($condition) {
        $bankcard_info = $this->where($condition)->find();
        return $bankcard_info;
    }


    /**
     * 添加银行卡绑定
     * @param unknown $insert
     * @return boolean
     */
    public function addbankCard($insert) {
        return $this->insert($insert);
    }

    /**
     * 更新银行卡绑定
     * @param array $update
     * @param array $condition
     * @return boolean
     */
    public function editbankCard($update, $condition) {
        return $this->where($condition)->update($update);
    }

    //删除银行卡
    public function delbankCard($condition){
        return $this->where($condition)->delete();
    }


}
