<?php
/**
 * 保证金使用日志
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/13 0013
 * Time: 15:35
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');

class margin_listModel extends Model
{
    public function __construct()
    {
        parent::__construct('margin_list');
    }

    

    public function getOrderList($condition, $field = '*', $group = '',$order = '', $limit = 0, $page = 0, $count = 0)
    {
        $order_list = $this->field($field)->where($condition)->group($group)->order($order)->limit($limit)->page($page, $count)->select();
        return$order_list;
    }
}