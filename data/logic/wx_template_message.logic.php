<?php
/**
 * 微信消息模板管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class wx_template_messageLogic {

    private $appId;
    private $appSecret;
    private $templateId;
    private $memberInfo;
    private $sendMessageData;
    private $templateIdConfig;

    /**
     * 发送微信消息模板信息
     * @param $templateId
     * @param $memberInfo
     * @param $sendMessageData
     * @return array
     */
    public function sendWxMessage($templateId,$memberInfo,$sendMessageData) {
        $this->templateId = $templateId;
        $this->memberInfo = $memberInfo;
        $this->sendMessageData = $sendMessageData;
        $this->templateIdConfig = C('wx_message_templates');
        self::_initializationDta();
        $inc_file = BASE_PATH.DS.'..'.DS.'mobile'.DS.'api'.DS.'payment'.DS.'wxpay_jsapi'.DS.'wx_jssdk.php';
        if (empty($this->appId) || empty($this->appSecret) || !is_file($inc_file) || empty($this->memberInfo['weixin_open_id'])){
            $send_result_data = [201];
        }else{
            require_once($inc_file);
            $post_data = self::getSendData();
            $sdkObj = new JSSDK($this->appId, $this->appSecret);
            $send_result_data = $sdkObj->sendMessage($post_data);
        }
        return $send_result_data;
    }

    /**
     * 初始化参数配置
     */
    private function _initializationDta(){
        self::setWxSendParam();
        self::getMemberSendParam();
    }


    /**
     * 构造微信消息模板发送的信息
     */
    private function getSendData(){
        $sendData = $this->setWxTemplateData();
        $post_data = [
            "touser"=>$this->memberInfo['weixin_open_id'],
            "template_id"=>$this->templateId,
            "url"=>$this->sendMessageData['wx_url'],
            "data"=>$sendData
        ];
        return $post_data;
    }

    /**
     * 微信模板基础配置信息
     */
    function setWxTemplateData(){
        $addSendData = [];
        switch ($this->templateId){
            case $this->templateIdConfig['order_payment_success'] :  //订单支付成功
                $addSendData = [
                    "first" => [
                        "value"=>'您好，恭喜您已购买成功。',//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "Remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "orderProductName"=>[
                        "value"=>$this->sendMessageData['goods_name'],
                        "color"=>"#173177"
                    ],
                    "orderMoneySum"=>[
                        "value"=>$this->sendMessageData['order_amount'],
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['order_deliver_success'] :  //订单已发货
                $addSendData = [
                    "first" => [
                        "value"=>'您购买的艺术品已发货，敬请关注。',
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['goods_name'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$this->sendMessageData['shipping_time'],
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['auction_end_notice'] :  //竞拍结束通知
                $addSendData = [
                    "first" => [
                        "value"=>'竞拍结束通知',
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>'如有问题请致电400-135-2688或直接在微信留言，小艺将在第一时间为您服务!',
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['auction_name'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$this->sendMessageData['auction_bond'],
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>$this->sendMessageData['end_time'],
                        "color"=>"#173177"
                    ],
                ];
                break;
                //Mr.Liu新增微信模板消息
            case $this->templateIdConfig['aucution_margin_pay'] :  //保证金缴纳通知
                $addSendData = [
                    "first" => [
                        "value"=>"您的{$this->sendMessageData['auction_name']}保证金缴纳成功",//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>"￥{$this->sendMessageData['margin_amount']}",//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>date('Y-m-d', time()),
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"此拍品已累计缴纳￥{$this->sendMessageData['margin_count']}",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['release_margin'] :  //释放保证金通知
                $addSendData = [
                    "first" => [
                        "value"=>'您好，您的保证金已退回',//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['auction_name'],//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"￥{$this->sendMessageData['money']}",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"好遗憾！您参加的拍卖未中标，保证金将退回原路，预计1-3个工作日到，请查收。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['margin_timeout_remind'] :  //违约提前提醒
                $addSendData = [
                    "first" => [
                        "value"=>"您好，参加的{$this->sendMessageData['auction_name']}订单尾款支付开始。",//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['order_sn'],//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"{$this->sendMessageData['remind_time']}",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"超时违约，会扣除保证金，请及时付款。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['default_margin'] :  //违约保证金
                $addSendData = [
                    "first" => [
                        "value"=>"尊敬的客户，您参加的{$this->sendMessageData['auction_name']}因拍中，超时未支付，",//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>date('Y-m-d H:00:00', time()),//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>"很抱歉，因您违反相关约定",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"感谢您的参与。",
                        "color"=>"#173177"
                    ]
                ];
                if ($this->sendMessageData['type'] == 1) {
                    $addSendData['keyword2'] = [
                            "value"=>"很抱歉，因您违反相关约定，现违约扣除保证金￥{$this->sendMessageData['money']}，
                            剩余保证金￥{$this->sendMessageData['return_amount']}已原路退回。",
                            "color"=>"#173177"
                        ];
                } else {
                    $addSendData['keyword2'] = [
                        "value"=>"很抱歉，因您违反相关约定，现违约扣除保证金￥{$this->sendMessageData['money']}",
                        "color"=>"#173177"
                    ];
                }
                break;
            case $this->templateIdConfig['auction_auction'] :  //中拍消息
                $addSendData = [
                    "first" => [
                        "value"=>'恭喜您在所参与的竞拍中胜出',//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['auction_name'],//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$this->sendMessageData['money'],
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>date('Y-m-d H:i', time()),
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"请及时支付尾款结算，过期违约将扣除保证金。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['lost_auction'] :  //未中拍消息
                $addSendData = [
                    "first" => [
                        "value"=>'很抱歉，您竞拍的商品竞拍失败',//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$this->sendMessageData['auction_name'],//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>'竞拍失败',
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>"已有人超过您的叫价价格",
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"很抱歉，您竞拍的{$this->sendMessageData['auction_name']}商品竞拍失败，保证金将退回原路，预计1-3个工作日到，请查收。。",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case $this->templateIdConfig['beyond_price'] :  //竞价超越消息
                $addSendData = [
                    "first" => [
                        "value"=>'您好，您的竞价已被超过。',//$this->sendMessageData['wx_first'],
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>"正在竞拍 {$this->sendMessageData['auction_name']}",//$this->sendMessageData['wx_remark'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>$this->sendMessageData['current_price'],
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"立即前往加价
                        详情 > {$this->sendMessageData['url']}",
                        "color"=>"#173177"
                    ]
                ];
                break;
            case '':
                break;

                //...

            default :
                return $addSendData;
                break;
        }
        return $addSendData;
    }

    /**
     * 获取公众号基础配置信息
     */
    private function setWxSendParam(){
        $model_mb_payment = Model('mb_payment');
        $mb_payment_info = $model_mb_payment->getMbPaymentInfo(array('payment_id' => 3));
        $this->appId = $mb_payment_info['payment_config']['appId'];
        $this->appSecret = $mb_payment_info['payment_config']['appSecret'];
    }

    /**
     * 获取会员微信基础配置信息
     */
    private function getMemberSendParam(){
        if (empty($this->memberInfo['weixin_open_id'])){
            $condition = [
                'member_id'=>$this->memberInfo['member_id']
            ];
            $field = 'member_id,member_name,weixin_unionid,weixin_info,weixin_open_id';
            $this->memberInfo = Model('member')->getMemberInfo($condition, $field);
        }
    }


}
