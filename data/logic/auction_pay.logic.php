<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/25 0025
 * Time: 16:27
 * @author ADKi
 */

defined('InShopNC') or exit('Access Invalid!');

class auction_payLogic
{
    /**
     * 诺币支付
     * !外围事务!
     * @param $order_sn int 订单号
     * @param $member_id int 会员ID
     * @param $member_name string 会员名
     * @param $nuobi_money double 诺币
     * @param $type int 0 保证金支付 1 尾款支付
     * @return array
     * */
    public function NuobiPay($order_sn, $member_id, $member_name, $type = 0)
    {
        // 查询会员信息
        $member_info = Model('member')->getMemberInfo(array('member_id' => $member_id));
        $nuobi_money = $member_info['member_points']/100;
        /** @var auction_orderLogic $logic_auction_order */
        $logic_auction_order = Logic('auction_order');
        // 保证金使用诺币支付
        if ($type == 0) {
            $model_margin_orders = Model('margin_orders');
            $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
            // 为 0 直接返回
            if ($nuobi_money <= 0) {
                return callback(true, '', $order_info);
            }
            $order_amount = $order_info['margin_amount'] - $order_info['account_margin_amount'];

            if ($order_amount <= 0) {
                return callback(true, '', $order_info);
            }
            $update_array = array();
            $update_array['is_points'] = 1;
            if ($order_amount <= $nuobi_money) {
                // 全部支付 扣除诺币
                $update_array['points_amount'] = ncPriceFormat($order_amount);
                $update_array['order_state'] = 1;
                $update_array['payment_code'] = 'predeposit';
                $nuobi_points = $order_amount * (-100);

                // 记录日志，更新会员表诺币
                Model('points')->savePointsLog(
                    'nuobiorder',
                    array(
                        'pl_memberid'=>$member_id,
                        'pl_membername'=>$member_name,
                        'pl_points'=>$nuobi_points,
                        'order_sn'=>$order_info['order_sn'],
                        'order_id'=>$order_info['order_id']
                    ),
                    true
                );
                // 修改会员与拍品关系，增加除了保证金账户支付金额部分以外的保证金冻结金额
                $result = Logic('auction_order')->auction_member_bond($order_info);
                if(!$result){
                    return callback(false,'更新会员与拍品关系失败');
                }
            } else {
                // 部分支付，冻结诺币
                $update_array['points_amount'] = ncPriceFormat($nuobi_money);
                $update_array['order_state'] = 3;
                $nuobi_points = $nuobi_money * 100;
                // 记录日志，更新会员表诺币
                Model('points')->savePointsLog(
                    'freeze',
                    array(
                        'pl_memberid'=>$member_id,
                        'pl_membername'=>$member_name,
                        'pl_points'=>$nuobi_points,
                        'order_sn'=>$order_info['order_sn'],
                        'order_id'=>$order_info['order_id']
                    ),
                    true
                );
            }
            // 更新订单状态 乐观锁
            $result = $model_margin_orders->editOrderRetCount($update_array,array('order_sn' => $order_sn, 'order_state'=>['neq', 1]));
            if($result<1){
                return callback(false,'操作过于频繁,请稍后重试');
            }
            // 更新订单内容
            $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
        } elseif ($type == 1) {
            // 尾款使用诺币支付
            $model_order = Model('order');
            $order_info = $model_order->getOrderInfo(array('order_sn' => $order_sn));
            // 为 0 直接返回
            if ($nuobi_money <= 0) {
                return callback(true, '', $order_info);
            }
            $order_amount = $order_info['order_amount'] - $order_info['margin_amount'];
            if ($order_amount <= 0) {
                return callback(true, '', $order_info);
            }
            if ($order_amount <= $nuobi_money) {

                // 完全诺币支付
                $update_array['points_amount'] = ncPriceFormat($order_amount);
                $update_array['payment_code'] = 'predeposit';
                $update_array['order_state'] = 20;
                $nuobi_points = $order_amount * (-100);

                // 记录日志，更新会员表诺币
                /** @var pointsModel $points */
                $points =Model('points');
                $points->savePointsLog(
                    'nuobiorder',
                    array(
                        'pl_memberid'=>$member_id,
                        'pl_membername'=>$member_name,
                        'pl_points'=>$nuobi_points,
                        'order_sn'=>$order_info['auction_order_sn'],
                        'order_id'=>$order_info['auction_order_id']
                    ),
                    true
                );
                // 更新订单状态
                /** @var orderModel $model_order */
                // 减掉会员保证金冻结部分，修改订单状态，记录保证金日志
                $logic_auction_order->AuctionOrderMarginLog($member_id, $member_name, 4, 0, $order_info['order_id']);
                $result = $model_order->editOrder($update_array,array('order_sn' => $order_sn));

            } else {
                $update_array['points_amount'] = ncPriceFormat($nuobi_money);
                $nuobi_points = $nuobi_money * 100;
                // 记录日志，更新会员表诺币
                Model('points')->savePointsLog(
                    'freeze',
                    array(
                        'pl_memberid'=>$member_id,
                        'pl_membername'=>$member_name,
                        'pl_points'=>$nuobi_points,
                        'order_sn'=>$order_info['order_sn'],
                        'order_id'=>$order_info['order_id']
                    ),
                    true
                );
                // 更新订单状态
                $result = $model_order->editOrder($update_array,array('order_sn' => $order_sn));
            }
            //更新订单内容
            $order_info = $model_order->getOrderInfo(array('order_sn' => $order_sn));
        }
        if(!$result){
            return callback(false,'更新订单失败');
        }
        return callback(true, '', $order_info);
    }

    /**
     * 保证金支付完成更新关系表和保证金订单表
     * @param $order_sn int 订单号
     * @return array 新订单信息
     * */
    public function completeMarginOrder($order_sn)
    {
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
        // 修改会员与拍品关系，增加除了保证金账户支付金额部分以外的保证金冻结金额
        /** @var auction_orderLogic $auction_order_logic */
        $auction_order_logic = Logic('auction_order');
        $result = $auction_order_logic->auction_member_bond($order_info);
        if(!$result){
            return callback(false,'更新会员与拍品关系失败');
        }
        $update_array['order_state'] = 1;
        $update_array['payment_code'] = 'predeposit';
        // 更新订单状态
        $model_margin_orders->editOrder($update_array,array('order_sn' => $order_sn));
        $margin_info = $model_margin_orders->getOrderInfo(array('margin_id'=>$order_info['margin_id']));
        /** @var auctionsModel $auctions_model */
        $auctions_model = Model('auctions');
        $auction_info = $auctions_model->getAuctionsInfoByID($margin_info['auction_id']);
        /** @var auctionLogic $auction_logic */
        $auction_logic = Logic('auction');
        $day_num = $auction_logic->getInterestDay(TIMESTAMP,$auction_info['auction_start_time']);//计息天数
        /** @var j_member_distributeLogic $j_member_distribute_logic */
        $j_member_distribute_logic = Logic('j_member_distribute');
        $j_member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);

        //添加区域代理佣金待结算记录
        $j_member_distribute_logic->agent_margin_refund($margin_info, $day_num,0);
        // 更新订单内容
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
        return $order_info;
    }
    
    /**
     * 充值卡支付
     * 如果充值卡足够就单独支付了该订单，如果不足就暂时冻结，等API支付成功了再彻底扣除
     * @param $type int 0 保证金 1 尾款
     * @return array
     */
    public function rcbPay($order_info, $input, $buyer_info, $type = 0) {
        // 如果已支付直接返回
        if ($order_info['order_state'] == 1) return $order_info;
        // 充值卡余额
        $available_rcb_amount = floatval($buyer_info['available_rc_balance']);
        if ($available_rcb_amount <= 0) return $order_info;
        $model_pd = Model('predeposit');

        // 要支付的金额
        if ($type == 0) {
            /** @var margin_ordersModel $model_margin_orders */
            $model_margin_orders = Model('margin_orders');
            $order_amount = floatval($order_info['margin_amount']-$order_info['points_amount']-$order_info['account_margin_amount']);
        } elseif($type == 1) {
            $model_auction_order = Model('auction_order');
            $order_amount = floatval($order_info['order_amount']-$order_info['margin_amount']-$order_info['points_amount']);
        }
        $data_pd = array();
        $data_pd['member_id'] = $buyer_info['member_id'];
        $data_pd['member_name'] = $buyer_info['member_name'];
        $data_pd['amount'] = $order_amount;
        if ($type == 0) {
            $data_pd['order_sn'] = $order_info['order_sn'];
        } elseif ($type == 1) {
            $data_pd['order_sn'] = $order_info['auction_order_sn'];
        }
        if ($order_amount <= 0) {
            return callback(true, '', $order_info);
        }
        if ($available_rcb_amount >= $order_amount) {

            // 预存款立即支付，订单支付完成
            $model_pd->changeRcb('order_pay',$data_pd);
            $available_rcb_amount -= $order_amount;

            //下单，支付被冻结的积分
            $points_amount = floatval($order_info['points_amount']);
            if ($points_amount > 0) {
                $nuobi_points = $points_amount * 100;
                if ($type == 0) {
                    $order_sn_log = $order_info['order_sn'];
                    $order_id_log = $order_info['order_id'];
                } else {
                    $order_sn_log = $order_info['auction_order_sn'];
                    $order_id_log = $order_info['auction_order_id'];
                }
                /** @var pointsModel $points_model */
                $points_model = Model('points');
                $points_model->savePointsLog(
                    'pay_points',
                    array(
                        'pl_memberid' => $buyer_info['member_id'],
                        'pl_membername' => $buyer_info['member_name'],
                        'pl_points' => $nuobi_points,
                        'order_sn' => $order_sn_log,
                        'order_id' => $order_id_log
                    ),
                    true
                );
            }

            // 订单状态 置为已支付
            $data_order = array();
            if ($type == 0) {
                $order_info['order_state'] = $data_order['order_state'] = 1;
                $order_info['payment_time'] = $data_order['payment_time'] = TIMESTAMP;
            }

            $order_info['payment_code'] = $data_order['payment_code'] = 'predeposit';
            $order_info['rcb_amount'] = $data_order['rcb_amount'] = $order_amount;
            /** @var auction_orderLogic $logic_auction_order */
            $logic_auction_order = Logic('auction_order');
            if ($type == 0) {
                $model_margin_orders->editOrder($data_order,array('margin_id'=>$order_info['margin_id']));
                // 更新会员和拍品关系 增加除了保证金账户支付金额部分以外的保证金冻结金额
                $result = $logic_auction_order->auction_member_bond($order_info);
                if (!$result) {
                    throw new Exception('更新会员与拍品关系失败');
                }
            } elseif ($type == 1) {
                // 更新支付方式和支付金额
                $model_auction_order->setOrder(array('auction_order_id'=>$order_info['auction_order_id']), $data_order);
                // 更新订单已完成和写入日志，扣除会员保证金
                $logic_auction_order->AuctionOrderMarginLog($order_info['buyer_id'], $order_info['buyer_name'], 4, 0, $order_info['auction_order_sn']);
            }
        } else {
            //暂冻结预存款,后面还需要 API彻底完成支付
            $data_pd['amount'] = $available_rcb_amount;
            $model_pd->changeRcb('order_freeze',$data_pd);
            //预存款支付金额保存到订单
            $data_order = array();
            $order_info['rcb_amount'] = $data_order['rcb_amount'] = $available_rcb_amount;
            // 线上支付部分支付
            if ($type == 0) {
                $data_order['order_state'] = 3;
                $result = $model_margin_orders->editOrder($data_order,array('margin_id'=>$order_info['margin_id']));
            } elseif ($type == 1) {
                $result = $model_auction_order->setOrder(array('auction_order_id'=>$order_info['auction_order_id']), $data_order);
            }
            if (!$result) {
                throw new Exception('订单更新失败');
            }
        }

        return $order_info;
    }

    /**
     * 预存款支付 !外围事务 $isThrow = true!
     * 如果预存款足够就单独支付了该订单，如果不足就暂时冻结，等API支付成功了再彻底扣除
     * @param $order_info
     * @param $input
     * @param $buyer_info
     * @param $type int 0 保证金 1 尾款
     * @param bool $isThrow 操作失败是否抛出异常
     * @return array|mixed|PHPUnit_Framework_Constraint_Callback
     * @throws Exception
     */
    public function pdPay($order_info, $input, $buyer_info, $type = 0, $isThrow = false) {
        if ($order_info['order_state'] == 1) {
            if($isThrow){
                throw new Exception("请勿重复支付");
            }
            return $order_info;
        }

        $available_pd_amount = floatval($buyer_info['available_predeposit']);
        if ($available_pd_amount <= 0){
            if($isThrow){
                throw new Exception("账户余额不足");
            }
            return $order_info;
        }

        /** @var predepositModel $model_pd */
        $model_pd = Model('predeposit');
        $data_pd = array();
        $data_pd['member_id'] = $buyer_info['member_id'];
        $data_pd['member_name'] = $buyer_info['member_name'];
        // 要支付的金额

        if ($type == 0) {
            /** @var margin_ordersModel $model_margin_orders */
            $model_margin_orders = Model('margin_orders');
            $order_amount = floatval($order_info['margin_amount'])-floatval($order_info['rcb_amount'])-floatval($order_info['points_amount']) - floatval($order_info['account_margin_amount']) - floatval($order_info['bag_amount']);
            $data_pd['order_sn'] = $order_info['order_sn'];
        } elseif($type == 1) {
            $order_amount = floatval($order_info['order_amount'])-floatval($order_info['rcb_amount'])-floatval($order_info['points_amount']) - floatval($order_info['margin_amount']) - floatval($order_info['bag_amount']);
            $data_pd['order_sn'] = $order_info['auction_order_sn'];
        }

        $data_pd['amount'] = $order_amount;
        //无需支付
        if ($order_amount <= 0) {
            return callback(true, '', $order_info);
        }

        if ($available_pd_amount >= $order_amount) {

            //预存款立即支付，订单支付完成
            $model_pd->changePd('auction_order_pay',$data_pd, $order_info);
            $available_pd_amount -= $order_amount;

            //下单，支付被冻结的积分
            $points_amount = floatval($order_info['points_amount']);
            if ($points_amount > 0) {
                $nuobi_points = $points_amount * 100;
                if ($type == 0) {
                    $order_sn_log = $order_info['order_sn'];
                    $order_id_log = $order_info['order_id'];
                } else {
                    $order_sn_log = $order_info['auction_order_sn'];
                    $order_id_log = $order_info['auction_order_id'];
                }/** @var pointsModel $points */
                $points = Model('points');
                $points->savePointsLog(
                    'pay_points',
                    array(
                        'pl_memberid' => $buyer_info['member_id'],
                        'pl_membername' => $buyer_info['member_name'],
                        'pl_points' => $nuobi_points,
                        'order_sn' => $order_sn_log,
                        'order_id' => $order_id_log
                    ),
                    true
                );
            }

            //下单，支付被冻结的充值卡
            $pd_amount = floatval($order_info['rcb_amount']);
            if ($pd_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $buyer_info['member_id'];
                $data_pd['member_name'] = $buyer_info['member_name'];
                $data_pd['amount'] = $pd_amount;
                if ($type == 0) {
                    $data_pd['order_sn'] = $order_info['order_sn'];
                } elseif($type == 1) {
                    $data_pd['order_sn'] = $order_info['auction_order_sn'];
                }
                $model_pd->changeRcb('order_comb_pay',$data_pd);
            }

            // 订单状态 置为已支付
            $data_order = array();

            $order_info['payment_code'] = $data_order['payment_code'] = 'predeposit';
            $order_info['pd_amount'] = $data_order['pd_amount'] = $order_amount;
            /** @var auction_orderLogic $logic_auction_order */
            $logic_auction_order = Logic('auction_order');
            if ($type == 0) {
                $order_info['payment_time'] = $data_order['payment_time'] = TIMESTAMP;
                $order_info['order_state'] = $data_order['order_state'] = 1;
                $upCou = $model_margin_orders->editOrderRetCount($data_order,array('margin_id'=>$order_info['margin_id'], 'order_state'=>['neq', 1]));
                if($upCou < 1){
                    throw new Exception("失败,操作频繁");
                }
                // 增加除了保证金账户支付金额部分以外的保证金冻结金额
                $result = $logic_auction_order->auction_member_bond($order_info);
                if (!$result) {
                    throw new Exception('更新会员与拍品关系失败');
                }
            } elseif($type == 1) {
                //todo 这里没有做乐观锁考虑pdPay整体加锁 锁住 当期订单支付只能同时发生一次不能点两次
                $order_info['order_state'] = $data_order['order_state'] = 20;
                $re =  Model('order')->editOrder($data_order,array('order_id'=>$order_info['order_id']));
//                dd();
                //上面已经付过钱了,这里不需要再付了
                $logic_auction_order->AuctionOrderMarginLog($order_info['buyer_id'], $order_info['buyer_name'], 4, 0, $order_info['auction_order_sn']);
            }
        } else {

            //暂冻结预存款,后面还需要 API彻底完成支付
            $data_pd['amount'] = $available_pd_amount;
            $model_pd->changePd('order_freeze',$data_pd);
            //预存款支付金额保存到订单
            $data_order = array();
            $order_info['pd_amount'] = $data_order['pd_amount'] = $available_pd_amount;
            // 线上支付部分支付
            if ($type == 0) {
                $data_order['order_state'] = 3;
                $result = $model_margin_orders->editOrderRetCount($data_order,array('margin_id'=>$order_info['margin_id'], 'order_state'=>['neq', 1]));
                if($result < 1){
                    throw new Exception("失败,操作频繁");
                }
            } elseif ($type == 1) {
                $result = Model("order")->editOrder($data_order,array('order_id'=>$order_info['order_id']));
            }
            if (!$result) {
                throw new Exception('订单更新失败');
            }
        }
        return $order_info;
    }

    /** 保证金全额支付
     * @param $order_info
     * @param $pay_money
     * @return bool
     */
    public function marginPayOrder($order_info, $pay_money){
        if($pay_money > $order_info['margin_amount']){
            return false;
        }
        /** @var auction_orderLogic $logic_auction_order */
        $logic_auction_order = Logic('auction_order');
        $logic_auction_order->AuctionOrderMarginLog($order_info['buyer_id'], $order_info['buyer_name'], 10, $pay_money, $order_info['order_id']);

        // 订单状态 置为已支付
        $data_order = array();
        $order_info['payment_code'] = $data_order['payment_code'] = 'predeposit';
        $order_info['pd_amount'] = $data_order['pd_amount'] = 0;
        $order_info['order_state'] = $data_order['order_state'] = 20;
        $data_order['payment_time'] = time();
        /** @var orderModel $model_auction_order */
        $model_auction_order = Model('order');
        try{
            $model_auction_order->beginTransaction();
            $re1 = $model_auction_order->editOrder($data_order,array('order_id'=>$order_info['order_id']));

            if (isset($order_info['auction_id'])) {
                /** @var member_commissionModel $member_commission */
                $member_commission = Model('member_commission');
                $auctionModel = Model('auctions');
                $auction_info = $auctionModel->where(['auction_id' => $order_info['auction_id']])->find();
                if ($auction_info && $auction_info['auction_type'] != $auctionModel::AUCTION_TYPE_NEWBIE) {
                    $member_commission->transactionForCommission($order_info);
                }
            }
//            $model_member = Model('member');
//            $model_member->editMember(
//                array('member_id' => $order_info['buyer_id']),
//                array(
//                    'available_predeposit' => array('exp','available_predeposit+'.($order_info['margin_amount']-$pay_money)),
//                    'freeze_margin' => array('exp','freeze_margin-'.$order_info['margin_amount'])
//                )
//            );
//            $margin_info = Model('margin_orders')->getOrderInfo(array('auction_order_id'=>$order_info['order_id'],'buyer_id'=>$order_info['buyer_id']));
//            $re2 = Logic('auction_order')->MarginLog($order_info['buyer_id'], $order_info['buyer_name'], 2, $order_info['margin_amount']-$pay_money, $margin_info['order_sn']);
            $re2 = true;
            if($re1&&$re2){
                $model_auction_order->commit();
            }else{
                throw new Exception('false');
            }
        }catch(Exception $e){
                $model_auction_order->rollback();
        }
//        print_R($order_info);exit;
        return $re1&&$re2;
    }

    /** 保证金全额支付（新方法）
     * @author 汪继君
     * @param $order_info
     * @param $pay_money
     * @return bool
     */
    public function marginPayOrderNew($order_info, $pay_money){
        $re2 = false;
        /** @var auction_orderLogic $logic_auction_order */
        $logic_auction_order = Logic('auction_order');
        //$logic_auction_order->AuctionOrderMarginLog($order_info['buyer_id'], $order_info['buyer_name'], 10, $pay_money, $order_info['order_id']);

        // 订单状态 置为已支付
        $data_order = array();
        //$order_info['payment_code'] = $data_order['payment_code'] = 'predeposit';
        //$order_info['pd_amount'] = $data_order['pd_amount'] = 0;
        $order_info['order_state'] = $data_order['order_state'] = 20;
        $data_order['payment_time']=time();
        /** @var auction_orderModel $model_auction_order */
        $model_auction_order = Model('order');
        try{
            $model_auction_order->beginTransaction();
            //($data_order,array('order_id'=>$order_info['order_id']));
            $re1=Model("")->table('orders')->where(array('order_id'=>$order_info['order_id']))->update($data_order);
            //退多余的保证金
            $map['auction_id']=$order_info['auction_id'];
            $map['order_state']=1;
            $map['refund_state']=0;
            $map['default_state']=0;
            $map['lock_state']=1;
            $map['buyer_id']=$order_info['buyer_id'];
            $margin_order_list=Model("margin_orders")->where($map)->order("api_pay_amount DESC,margin_amount DESC")->select();
            if (!empty($margin_order_list)) {
                foreach ($margin_order_list as $key => $value) {
                    $return_amount = $value['margin_amount']-$pay_money;
                    $pay_money=$pay_money-$value['margin_amount'];
                    if ($pay_money<0) {
                        $update_margin_order=array();
                        //部分退款
                        $update_margin_order['rufund_money']=$return_amount;//涉及部分退款的金额                         
                        $update_margin_order['updated_at']=time();
                        //需要部分退款
                        $update_margin_order['refund_state']=2;
                        $update_margin_order['default_state']=2;
                        //$update_margin_order['update_margin_order'];
                        $re2=Model("margin_orders")->where(array('margin_id'=>$value['margin_id']))->update($update_margin_order);
                        //echo $re2."<br/>";
                        $logic_auction_order->MarginLog($order_info['buyer_id'], $order_info['buyer_name'], 2, $return_amount, $order_info['order_sn']);
                        //退出循环
                        break;
                    }else{
                        $update_margin_order=array();
                        //全额抵扣
                        $update_margin_order['refund_state']=2;
                        $update_margin_order['updated_at']=time();
                        $re2=Model("margin_orders")->where(array('margin_id'=>$value['margin_id']))->update($update_margin_order);

                        $logic_auction_order->MarginLog($order_info['buyer_id'], $order_info['buyer_name'], 2, $value['margin_amount'], $order_info['order_sn']);
                    }
                }
                //更新其他的
                $dat['default_state']=2;                
                $re3=Model("margin_orders")->where($map)->update($dat);
                if($re1&&$re2){  
                    $model_auction_order->commit();                  
                    return true;                    
                }else{
                    $model_auction_order->rollback();
                    return false;
                }
            }else{
                $model_auction_order->commit();
                return true;  
            }
        }catch(Exception $e){
                $model_auction_order->rollback();
        }
    }
    /** 保证金全额支付（新方法）
     * @author 汪继君
     * @param $order_info
     * @return bool
     */
    public function marginPayOrderAllNew($order_info){
        $rec = false;
        /** @var auction_orderLogic $logic_auction_order */
        $logic_auction_order = Logic('auction_order');
        //$logic_auction_order->AuctionOrderMarginLog($order_info['buyer_id'], $order_info['buyer_name'], 10, $pay_money, $order_info['order_id']);

        // 订单状态 置为已支付
        $data_order = array();
        //$order_info['payment_code'] = $data_order['payment_code'] = 'predeposit';
        //$order_info['pd_amount'] = $data_order['pd_amount'] = 0;
        $data_order['payment_time']=time();
        /** @var auction_orderModel $model_auction_order */
        $model_auction_order = Model('order');
        try{
            $model_auction_order->beginTransaction();
            Model("")->table('orders')->where(array('order_id'=>$order_info['order_id']))->update($data_order);
            //全部扣除保证金
            $map['auction_id']=$order_info['auction_id'];
            $map['order_state']=1;
            $map['refund_state']=0;
            $map['default_state']=0;
            $map['lock_state']=1;
            $map['buyer_id']=$order_info['buyer_id'];
            $margin_order_list=Model("margin_orders")->where($map)->order("api_pay_amount DESC,margin_amount DESC")->select();
            //print_r($map);exit();
            if (!empty($margin_order_list)) {
                foreach ($margin_order_list as $key => $value) {
                    //全额抵扣                      
                    $update_margin_order['refund_state']=2;
                    $update_margin_order['updated_at']=time();
                    $rec=Model("margin_orders")->where(array('margin_id'=>$value['margin_id']))->update($update_margin_order);
                    $logic_auction_order->MarginLog($order_info['buyer_id'], $order_info['buyer_name'], 2, $value['margin_amount'], $order_info['order_sn']);
                }
                if($rec){
                    $model_auction_order->commit();
                    return true;
                }else{
                    $model_auction_order->rollback();
                    return false;
                }
            }else{
                //已经有抵扣不在重复抵扣
                $map['refund_state']=2;
                $margin_order_list2=Model("margin_orders")->where($map)->order("api_pay_amount DESC,margin_amount DESC")->select();
                if ($margin_order_list2) {
                    $model_auction_order->commit();
                    return true;
                }else{
                    $model_auction_order->rollback();
                    return false;
                }
            }
        }catch(Exception $e){
                $model_auction_order->rollback();
        }
    }
}