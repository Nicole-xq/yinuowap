<?php
/**
 * 会员评价
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class j_memberLogic {


    /**
     * @var memberModel
     */
    var $model;

    public function __construct()
    {
        $this->model = Model('member');
    }

    /**生成会员的邀请码
     * @param $member_info  会员信息
     * @return bool|string  是否成功
     */
    public function createCode($member_info){
        if(empty($member_info)){
            return false;
        }

        $res = false;
        $i = 1;
        do{
            $str_code = $member_info['member_id'] . '_' . trim($member_info['member_mobile']);
            $invite_code = substr(hash('md5',$str_code.time()),-8);
            $check_member_dcode = (array) $this->model->getUpperMember(array('dis_code'=>strtolower($invite_code)));

            if(empty($check_member_dcode)){
                $update_arr = array();
                $update_arr['dis_code'] = $invite_code;
                $update_arr['member_mobile'] = trim($_POST['member_mobile']);
                $res = $this->model->editMemberDistribute($update_arr ,array('member_id' => $member_info['member_id']));
            }

           // echo  $i;
        }while(!$res && $i++ < 5);

        if($res){
            return $invite_code;
        }else{
            return false;
        }
    }


    /**绑定上级分销
     * @param int $user_id  会员id
     * @param string $code     邀请码
     * @param string $msg   错误消息
     * @return bool     是否成功
     */
    public function bind_by_code($user_id, $code, &$msg = ''){
        $rs = false;
        $check_member_dcode = $this->model->getUpperMember(array('dis_code' => strtolower(trim($code))));
        if($user_id == $check_member_dcode['member_id']){
            $msg = '绑定失败,不能自己绑定自己!';
            goto END;
        }

        $data = array('top_member'=>$check_member_dcode['member_id'],'top_member_name'=>$check_member_dcode['member_name']);
        $condition = array('member_id' => $user_id);

        $res = $this->model->editMemberDistribute($data, $condition);
        if($res){
            $rs = true;
        }else{
            $msg = '绑定失败';
        }

        END:
        return $rs;
    }

    /**获取所有会员类型
     *
     * @return array
     */
    function getMemberTypeList(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $arr = [];
        foreach($re as $v){
            $arr[$v['type_id']] = $v;
        }
        return $arr;
    }

    /**
     * 邀请码获取
     * @param int $member_id
     * @return string
     */
    public function getMemberInviteCode($member_id){
        if (empty($member_id)){
            return '';
        }
        $member_model = Model('member');
        $member_info = $member_model->getMemberInfoByID($member_id);
        $d_member = $member_model->getUpperMember(array('member_id'=>$member_id));
        if(!empty($d_member)){
            if(empty($d_member['dis_code'])){
                if(self::createCode($member_info)){
                    $d_member = $member_model->getUpperMember(array('member_id'=>$member_id));
                }
            }
            return $d_member['dis_code'];
        }else{
            return $d_member['dis_code'];
        }
    }

}
