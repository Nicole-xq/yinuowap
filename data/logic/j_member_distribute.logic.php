<?php
/**
 * 佣金处理
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class j_member_distributeLogic
{
    public $distribute_str = array(
        0 => '会员',
        1 => '微合伙人',
        2 => '战略合伙人',
        3 => '事业合伙人',
        4 => '艺术家'
    );

    /**
     * @var memberModel
     */
    var $member_model;

    /**
     * @var predepositModel
     */
    var $predeposit_model;

    /**
     * @var Model
     */
    var $distribute_type_model;

    /**
     * @var member_commissionModel
     */
    var $commission_model;

    var $agent_order_model;


    var $province_id;
    var $city_id;
    var $area_id;

    /**
     * @var array
     */
    var $upgrade_setting = array(
        '1' => 'wdl',
        '2' => 'zl',
        '3' => 'sy',
        '5' => 'dz',
        '11'=> 'wdl',
        '12'=> 'zl',
        '13'=> 'sy'
    );

    public function __construct()
    {
        $this->predeposit_model = Model('predeposit');
        $this->member_model = Model('member');
        $this->distribute_type_model = Model('member_distribute_type');
        $this->commission_model = Model('member_commission');
        $this->distribute_detail_model = Model('member_distribute_detail');
        $this->agent_order_model = Model('agent_order');
    }

    /**根据会员类型获取分销佣金设置信息
     * @param int $type         会员类型
     * @param array $member  会员id(预留)
     * @return bool|array   分销佣金信息
     */
    public function get_type_distribute( $type, $member = false){
        if(!isset($type)){
            return false;
        }
        return $this->distribute_type_model->where(array('type_id' => $type))->find();

    }

    /**根据会员id获取上级分销会员信息
     * @param  int $member_id 会员id
     * @return mixed    上级分销信息
     */
    public function get_top_member($member_id){
        return $this->member_model->getUpperMember(array('member_id' => $member_id));
    }


    /** 获取特定专区店铺id
     * @return int
     */
    public function get_td_store_id(){
        return C('store.zq_id');
    }

    /** 处理购买特定专区店铺产品
     * @param $goods
     */
    public function td_goods_process($goods){
        $number = 0;
        $store_id = $this->get_td_store_id();
        $buy_id = 0;
        foreach($goods as $row){
            $buy_id = $row['buyer_id'];
            if($row['store_id'] == $store_id){
                $number += $row['goods_pay_amount'];
            }
        }

        if($number > 0){
            $update = array(
                'td_amount' => array('exp','td_amount+'.$number)
            );
            return $this->member_model->editMember(array('member_id' => $buy_id), $update);
        }

        return true;
    }


    /** 开通店铺处理
     * @param $joinin_detail
     * @return bool
     */
    public function openStore($joinin_detail){

        $member_relation = $this->get_top_member($joinin_detail['member_id']);

        if(empty($member_relation['top_member'])){
            goto END;
        }
        $distribute_info = $this->get_distribution_info($member_relation['member_id']);
//        $distribute_info = $this->get_member_distribute($member_relation['top_member'], 'artist');

        $money = $joinin_detail['paying_amount'] - $joinin_detail['sc_bail'];
        if(!empty($distribute_info) && $money > 0){//如果设置的艺术家入驻返利金额大于0  执行返利
            $this->member_model->beginTransaction();
            foreach($distribute_info as $v){
                $insert_data = array();
                $add_money = $money * ($v['artist_num'] * 1.0 / 100);
                $insert_data['order_id'] = '';
                $insert_data['order_sn'] = '';
                $insert_data['order_goods_id'] = '';
                $insert_data['goods_id'] = '';
                $insert_data['goods_name'] = '艺术家入驻('.$joinin_detail['store_name'].')';
                $insert_data['goods_image'] = '';
                $insert_data['goods_pay_amount'] = $money;  //付款金额
                $insert_data['from_member_id'] = $joinin_detail['member_id']; //佣金来源用户id
                $insert_data['from_member_name'] = $joinin_detail['member_name']; //佣金来源用户名字
                $insert_data['commission_amount'] = $add_money; //佣金金额
                $insert_data['dis_commis_rate'] = $distribute_info;    //返佣比例
                $insert_data['dis_member_id'] = $v['member_id'];   //返佣受益人id
                $insert_data['dis_member_name'] = $v['member_name'];   //返佣受益人名字
                $insert_data['add_time'] = time();
                $insert_data['commission_time'] = $insert_data['add_time'];
                $insert_data['top_lv'] = $v['level'];
                $insert_data['dis_commis_state'] = 1;   //佣金状态 0未结算 1结算
                $insert_data['commission_type'] = 5;    //返佣来源类型
                $res = $this->commission_model->insert($insert_data);
                if($res){
                    $data_pd = array();
                    $data_pd['member_id'] = $insert_data['dis_member_id'];
                    $data_pd['member_name'] = $insert_data['dis_member_name'];
                    $data_pd['amount'] = $insert_data['commission_amount'];
                    $data_pd['msg'] = $joinin_detail['store_name'].'成为艺术家返佣收益';
                    $member = Model('member')->getMemberInfo(array('member_id'=>$v['member_id']));
                    $param = array();
                    $param['code'] = 'fanyong_notice';
                    $param['member_id'] = $member['member_id'];
                    $param['number']['mobile'] = $member['member_mobile'];
                    $param['param'] = array();
                    QueueClient::push('sendMemberMsg', $param);
                    //记录预存款日志
                    if(!$this->predeposit_model->changePd('member_distribute_create_artist',$data_pd)){
                        $this->member_model->rollback();
                        throw new  Exception ( '数据库更新出错!' );
                    }

                }else{
                    $this->member_model->rollback();
                }
            }

            $this->member_model->commit();
        }

        END:
        return true;


    }

    /**
     * 区域代理添加开店佣金记录
     * @param $joinin_detail
     * @return bool
     * @throws Exception
     */
    public function open_store_for_agent($joinin_detail){
        $agent_distribute_info = $this->get_agent_info($joinin_detail['member_id']);

        $money = $joinin_detail['paying_amount'] - $joinin_detail['sc_bail'];
        if(!empty($agent_distribute_info) && $money > 0){//如果设置的艺术家入驻返利金额大于0  执行返利
            $this->agent_order_model->beginTransaction();
            //三级分销
            foreach($agent_distribute_info as $val){
                $insert_data = array();
                $add_money = $money * ($val['artist_num'] * 1.0 / 100);
                $insert_data['agent_id'] = $val['agent_id'];
                $insert_data['commission_type'] = 5;
                $insert_data['description'] = '艺术家入驻('.$joinin_detail['store_name'].')';
                $insert_data['from_member_id'] = $joinin_detail['member_id'];
                $insert_data['agent_type'] = $val['agent_type'];
                $insert_data['agent_lv'] = $val['agent_lv'];
                $insert_data['rate_num'] = $val['artist_num'];
                $insert_data['commission_status'] = 1;
                $insert_data['commission_amount'] = $add_money;
                $insert_data['province_id'] = $this->province_id;
                $insert_data['city_id'] = $this->city_id;
                $insert_data['area_id'] = $this->area_id;
                $insert_data['cdate'] = date('Y-m-d H:i:s');
                $res = $this->agent_order_model->insert($insert_data);
                if(!$res){
                    $this->agent_order_model->rollback();
                }
            }

            $this->agent_order_model->commit();
        }

        END:
        return true;


    }


    /** 保证金利息结算
     * @param  array  $margin_order_info    保证金订单信息
     * @param  int    $day_number           保证金投资天数
     * @param  double $auction_bond_rate    保证金年化利率百分比
     * @return bool 执行结果
     */
    public function pay_margin_refund($margin_order_info, $day_number, $auction_bond_rate){
        //$auction_bond_rate = 5;

        do{//保证金计息

            if ($auction_bond_rate <= 0 ) { //保证金小于0不执行
                break;
            }

//            if ($day_number <= 3) { //投保时间小于等于3天 不计算利息
//                break;
//            }

            $year_amount = $margin_order_info['margin_amount'] * $auction_bond_rate / 100 ; //保证金金额*年化收益/100    年化收益

            $_amount = ncPriceFormat($day_number * $year_amount/365);  //收益金额

            if ($_amount <= 0) {
                break;
            }

            $log_array = array();
            $log_array['member_id'] = $margin_order_info['buyer_id'];
            $log_array['member_name'] = $margin_order_info['buyer_name'];
            $log_array['order_sn'] = $margin_order_info['order_sn'];
            $log_array['amount'] = $_amount;
            $log_array['margin_id'] = $margin_order_info['margin_id'];
            $data = array(
                'dis_commis_state'=>1,
                'commission_amount'=>$_amount,
                'commission_time'=>time()
            );
            /********此段代码仅用做处理历史数据使用********/
            $tmp = Model('member_commission')->getCommission(array('commission_type'=>6,'dis_commis_state'=>0,'order_id'=>$margin_order_info['margin_id'],'dis_member_id'=>$margin_order_info['buyer_id']));
            if(!$tmp){
                $this->margin_refund($margin_order_info,$day_number,$auction_bond_rate);
            }
            /***************end******************/
            $re = Model('member_commission')->updateCommission(array('commission_type'=>6,'dis_commis_state'=>0,'order_id'=>$margin_order_info['margin_id'],'dis_member_id'=>$margin_order_info['buyer_id']),$data);
            if(!$re){
                return false;
            }
            $res = $this->predeposit_model->changePd('auction_3', $log_array);//增加买家可用预存款金额
            if($res){
                $member_info = Model('member')->getMemberInfo(array('member_id' => $margin_order_info['buyer_id']),'member_mobile,member_email');
                $param = array();
                $param['code'] = 'profit_notice';
                $param['member_id'] = $margin_order_info['buyer_id'];
                $param['param'] = array(
                    'money' => $_amount,
                    'good_name' => $margin_order_info['auction_name'],
                    'order_sn' => $margin_order_info['order_sn'],
                    'num' => $day_number,
                );
                $param['number'] = array('mobile' => $member_info['member_mobile'], 'email' => $member_info['member_email']);
                QueueClient::push('sendMemberMsg', $param);
            }
            return $res;
        }while(0);

        return true;
    }

    /**
     * 添加保证金利息待返记录
     * @param $margin_order_info
     * @param $day_number
     */
    public function margin_refund($margin_order_info,$day_number,$auction_bond_rate){
        $condition = [
            'from_member_id' => $margin_order_info['buyer_id'],
            'order_goods_id' => $margin_order_info['auction_id'],
            'commission_type' => 6
        ];
        $exist_commission = $this->commission_model->where($condition)->find();
        if ($exist_commission) {
            return false;
        }
        $margin_amount = $margin_order_info['margin_amount'];
        $insert_data = array();
        if($day_number <= 0){
            return false;
        }
        /** @var auctionsModel $auction_model */
        $auction_model = Model('auctions');
        $auction_info = $auction_model->getAuctionsInfoByID($margin_order_info['auction_id']);
        $auction_bond_rate = isset($auction_info['auction_bond_rate']) ? $auction_info['auction_bond_rate'] : 0;
        $add_money = ncPriceFormat( $margin_amount * ($auction_bond_rate/100) * ($day_number / 365)); //获取年化收益
        if($add_money <= 0){
            return false;
        }
        $pdModel = Model('predeposit');
        $insert_data['order_id'] = $margin_order_info['margin_id'];
        $insert_data['order_sn'] = $margin_order_info['order_sn'];
        $insert_data['order_goods_id'] = $margin_order_info['auction_id'];
        $insert_data['goods_id'] = $margin_order_info['auction_id'];
        $insert_data['goods_name'] = '"'.$margin_order_info['auction_name'].'"'.'保证金收益'.$day_number.'天';
        $insert_data['goods_image'] = '';
        $insert_data['goods_pay_amount'] = $margin_amount;  //保证金金额
        $insert_data['from_member_id'] = $margin_order_info['buyer_id']; //佣金来源用户id
        $insert_data['from_member_name'] = $margin_order_info['buyer_name']; //佣金来源用户名字
        $insert_data['commission_amount'] = $add_money; //佣金金额
        $insert_data['dis_commis_rate'] = $auction_bond_rate;    //返佣比例
        $insert_data['dis_member_id'] = $margin_order_info['buyer_id'];   //返佣受益人id
        $insert_data['dis_member_name'] = $margin_order_info['buyer_name'];   //返佣受益人名字
        $insert_data['add_time'] = time();
        $insert_data['commission_time'] = 0;
        $insert_data['dis_commis_state'] = 0;   //佣金状态 0未结算 1结算
        $insert_data['commission_type'] = 6;    //返佣来源类型  保证金收益
        $insert_data['top_lv'] = 0;//上级的级别,上级或上上级(对应三级分销)
        $tmp = $this->commission_model->getCommissionInfo(array('order_id'=>$margin_order_info['margin_id'],'dis_member_id'=>$margin_order_info['buyer_id']));
        if(empty($tmp)){
            $res = $this->commission_model->insert($insert_data);
        }
        $type_name = [
            0 => 'hy',      //会员
            1 => 'wdl',        //微合伙人
            2 => 'zl',        //战略合伙人
            3 => 'sy',        //事业合伙人
            4 => 'hy',      //艺术家
            5 => 'dz',      //大众合伙人
            11 => 'wdl',        //内部--微合伙人
            12 => 'zl',        //内部--战略合伙人
            13 => 'sy',        //内部--事业合伙人
            10 => 'hy',      //内部--会员
        ];
        //获取上级用户id和上上级用户id
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $special_rate_config = Model('special_rate_config');
        $distribute_info = Model('member_distribute')
            ->where(['member_id' => $margin_order_info['buyer_id']])->find();
        if (!empty($distribute_info)) {
            $top_member = !empty($distribute_info['top_member']) ? $distribute_info['top_member'] : 0;
            $top_member_info = $model_member->getMemberInfoByID($top_member);
            $top_member_2 = !empty($distribute_info['top_member_2']) ? $distribute_info['top_member_2'] : 0;
            $top_member_2_info = $model_member->getMemberInfoByID($top_member_2);
            $top_member_bid_rate_info = $special_rate_config
                ->where(['partner_lv_type' => $type_name[$top_member_info['member_type']], 'partner_lv' => 1])->find();
            $top_member_2_bid_rate_info = $special_rate_config
                ->where(
                    ['partner_lv_type' => $type_name[$top_member_2_info['member_type']], 'partner_lv' => 2]
                )->find();
            $top_member_bid_rate = $top_member_bid_rate_info['rate'];
            $top_member_2_bid_rate = $top_member_2_bid_rate_info['rate'];
        }
        $condition1 = [
            'from_member_id' => $margin_order_info['buyer_id'],
            'dis_member_id' => $top_member,
            'order_goods_id' => $margin_order_info['auction_id'],
            'commission_type' => 9
        ];
        $exist_commission1 = $this->commission_model->where($condition1)->find();
        if (isset($top_member) && $top_member && !$exist_commission1) {
            $rate_num = $add_money * $top_member_bid_rate / 100;
            $rate_num = ncPriceFormat($rate_num);
            $top_member_update = array('available_predeposit' => array('exp', 'available_predeposit+' . $rate_num));
            $model_member->where(['member_id' => $top_member])->update($top_member_update);
            //上级用户返佣记录
            $insert_data['top_lv'] = 1;
            $insert_data['commission_type'] = 9;
            $insert_data['dis_member_id'] = $top_member;
            $insert_data['commission_amount'] = $rate_num;
            $insert_data['dis_commis_rate'] = $top_member_bid_rate;
            $insert_data['dis_member_name'] = $top_member_info['member_truename'];
            $this->commission_model->insert($insert_data);
            //做日志使用
            $insert_data['lg_available_amount'] = !empty($top_member_update)?$top_member_update['available_predeposit']:'0.00';
            $pdModel->insertPdLog($insert_data);
        }
        //是否开启二级返佣
        $model_setting = Model('setting');
        $setting = $model_setting->getRowSetting("level_two_rebate");
        if ($setting && $setting['value']==1) {
            $condition2 = [
                'from_member_id' => $margin_order_info['buyer_id'],
                'dis_member_id' => $top_member_2,
                'order_goods_id' => $margin_order_info['auction_id'],
                'commission_type' => 9
            ];
            $exist_commission2 = $this->commission_model->where($condition2)->find();
            if (isset($top_member_2) && $top_member_2 && !$exist_commission2) {
                $rate_num = $add_money * $top_member_2_bid_rate / 100;
                $rate_num = ncPriceFormat($rate_num);
                $top_member_2_update = array('available_predeposit' => array('exp', 'available_predeposit+' . $rate_num));
                $model_member->where(['member_id' => $top_member_2])->update($top_member_2_update);
                //上上级用户返佣记录
                $insert_data['top_lv'] = 2;
                $insert_data['commission_type'] = 9;
                $insert_data['dis_member_id'] = $top_member_2;
                $insert_data['commission_amount'] = $rate_num;
                $insert_data['dis_commis_rate'] = $top_member_2_bid_rate;
                $insert_data['dis_member_name'] = $top_member_2_info['member_truename'];
                $this->commission_model->insert($insert_data);
                $insert_data['lg_available_amount'] = !empty($top_member_2_update)?$top_member_2_update['available_predeposit']:'0.00';
                $pdModel->insertPdLog($insert_data);
            }
        }
    }


    /**
     * 添加保证金利息待返记录
     * @param $margin_order_info
     * @param $day_number
     */
    public function pay_top_margin_refund($margin_order_info,$day_number,$status = 0){
        //判断用户该拍品是否已经缴纳保障金并进行返佣操作
        $condition = [
            'from_member_id' => $margin_order_info['buyer_id'],
            'order_goods_id' => $margin_order_info['auction_id'],
            'commission_type' => 6
        ];
        $exist_commission = $this->commission_model->where($condition)->find();
        if ($exist_commission) {
            return false;
        }
        $margin_amount = $margin_order_info['margin_amount'];
        $insert_data = array();
        if($day_number <= 0){
            return false;
        }
        /** @var auctionsModel $auction_model */
        $auction_model = Model('auctions');
        $auction_info = $auction_model->getAuctionsInfoByID($margin_order_info['auction_id']);
        $auction_bond_rate = isset($auction_info['auction_bond_rate']) ? $auction_info['auction_bond_rate'] : 0;
        $add_money = ncPriceFormat( $margin_amount * ($auction_bond_rate/100) * ($day_number / 365)); //获取年化收益
        if($add_money <= 0){
            return false;
        }
        $insert_data['order_id'] = $margin_order_info['margin_id'];
        $insert_data['order_sn'] = $margin_order_info['order_sn'];
        $insert_data['order_goods_id'] = $margin_order_info['auction_id'];
        $insert_data['goods_id'] = $margin_order_info['auction_id'];
        $insert_data['goods_name'] = '"'.$margin_order_info['auction_name'].'"'.'保证金收益'.$day_number.'天';
        $insert_data['goods_image'] = '';
        $insert_data['goods_pay_amount'] = $margin_amount;  //保证金金额
        $insert_data['from_member_id'] = $margin_order_info['buyer_id']; //佣金来源用户id
        $insert_data['from_member_name'] = $margin_order_info['buyer_name']; //佣金来源用户名字
        $insert_data['commission_amount'] = $add_money; //佣金金额
        $insert_data['dis_commis_rate'] = $auction_bond_rate;    //返佣比例
        $insert_data['dis_member_id'] = $margin_order_info['buyer_id'];   //返佣受益人id
        $insert_data['dis_member_name'] = $margin_order_info['buyer_name'];   //返佣受益人名字
        $insert_data['add_time'] = time();
        $insert_data['commission_time'] = 0;
        $insert_data['dis_commis_state'] = 0;   //佣金状态 0未结算 1结算
        $insert_data['commission_type'] = 6;    //返佣来源类型  保证金收益
        $insert_data['top_lv'] = 0;//上级的级别,上级或上上级(对应三级分销)
        $this->commission_model->insert($insert_data);
        //获取上级用户id和上上级用户id
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $member_update = array('freeze_commis' => array('exp', 'freeze_commis+' . $add_money));
        $model_member->where(['member_id' => $margin_order_info['buyer_id']])->update($member_update);
        $type_name = [
            0 => 'hy',      //会员
            1 => 'wdl',        //微合伙人
            2 => 'zl',        //战略合伙人
            3 => 'sy',        //事业合伙人
            4 => 'hy',      //艺术家
            5 => 'dz',      //大众合伙人
            11 => 'wdl',        //内部--微合伙人
            12 => 'zl',        //内部--战略合伙人
            13 => 'sy',        //内部--事业合伙人
            10 => 'hy',      //内部--会员
        ];
        $special_rate_config = Model('special_rate_config');
        $distribute_info = Model('member_distribute')
            ->where(['member_id' => $margin_order_info['buyer_id']])->find();
        if (!empty($distribute_info)) {
            $top_member = !empty($distribute_info['top_member']) ? $distribute_info['top_member'] : 0;
            $top_member_info = $model_member->getMemberInfoByID($top_member);
            $top_member_2 = !empty($distribute_info['top_member_2']) ? $distribute_info['top_member_2'] : 0;
            $top_member_2_info = $model_member->getMemberInfoByID($top_member_2);
            $top_member_bid_rate_info = $special_rate_config
                ->where(['partner_lv_type' => $type_name[$top_member_info['member_type']], 'partner_lv' => 1])->find();
            $top_member_2_bid_rate_info = $special_rate_config
                ->where(
                    ['partner_lv_type' => $type_name[$top_member_2_info['member_type']], 'partner_lv' => 2]
                )->find();
            $top_member_bid_rate = $top_member_bid_rate_info['max_rate'];
            $top_member_2_bid_rate = $top_member_2_bid_rate_info['max_rate'];
        }
        $condition1 = [
            'from_member_id' => $margin_order_info['buyer_id'],
            'dis_member_id' => $top_member,
            'order_goods_id' => $margin_order_info['auction_id'],
            'commission_type' => 9
        ];
        $exist_commission1 = $this->commission_model->where($condition1)->find();
        if (isset($top_member) && $top_member && !$exist_commission1) {
            $rate_num = ncPriceFormat( $margin_amount * ($top_member_bid_rate/100) * ($day_number / 365)); //获取年化收益
            $top_member_update = array('freeze_commis' => array('exp', 'freeze_commis+' . $rate_num));
            $model_member->where(['member_id' => $top_member])->update($top_member_update);
            //上级用户返佣记录
            $insert_data['top_lv'] = 1;
            $insert_data['commission_type'] = 9;
            $insert_data['dis_member_id'] = $top_member;
            $insert_data['commission_amount'] = $rate_num;
            $insert_data['dis_commis_rate'] = $top_member_bid_rate;
            $insert_data['dis_member_name'] = $top_member_info['member_truename'];
            $this->commission_model->insert($insert_data);
        }
        //是否开启二级返佣
        $model_setting = Model('setting');
        $setting = $model_setting->getRowSetting("level_two_rebate");
        if ($setting && $setting['value']==1) {
            $condition2 = [
                'from_member_id' => $margin_order_info['buyer_id'],
                'dis_member_id' => $top_member_2,
                'order_goods_id' => $margin_order_info['auction_id'],
                'commission_type' => 9
            ];
            $exist_commission2 = $this->commission_model->where($condition2)->find();
            if (isset($top_member_2) && $top_member_2 && !$exist_commission2) {
                $rate_num = ncPriceFormat( $margin_amount * ($top_member_2_bid_rate/100) * ($day_number / 365)); //获取年化收益
                $top_member_2_update = array('freeze_commis' => array('exp', 'freeze_commis+' . $rate_num));
                $model_member->where(['member_id' => $top_member_2])->update($top_member_2_update);
                //上上级用户返佣记录
                $insert_data['top_lv'] = 2;
                $insert_data['commission_type'] = 9;
                $insert_data['dis_member_id'] = $top_member_2;
                $insert_data['commission_amount'] = $rate_num;
                $insert_data['dis_commis_rate'] = $top_member_2_bid_rate;
                $insert_data['dis_member_name'] = $top_member_2_info['member_truename'];
                $this->commission_model->insert($insert_data);
            }
        }
        
    }

    /** 保证金返佣处理
     * @param $margin_order_info    保证金订单信息
     * @param $day_number           收益天数
     * @param $state                是否结算
     * @return bool                 是否成功
     */
    public function pay_top_margin_refund_bak($margin_order_info, $day_number,$state = 1){
        $margin_amount = $margin_order_info['margin_amount']; //保证金金额
        if($day_number <= 0 || $margin_amount <= 0){
            goto  END;
        }
        /** @var auctionsModel $auction_model */
        $auction_model = Model('auctions');
        $auction_info = $auction_model->getAuctionsInfoByID($margin_order_info['auction_id']);
        /** @var auction_specialModel $auction_special_model */
        $auction_special_model = Model('auction_special');
        $special_info = $auction_special_model->getSpecialInfo(array('special_id'=>$auction_info['special_id']));
        $margin_order_info['partner_lv_1_wdl_rate'] = $special_info['partner_lv_1_wdl_rate'];
        $margin_order_info['partner_lv_1_zl_rate'] = $special_info['partner_lv_1_zl_rate'];
        $margin_order_info['partner_lv_1_sy_rate'] = $special_info['partner_lv_1_sy_rate'];
        $margin_order_info['partner_lv_2_wdl_rate'] = $special_info['partner_lv_2_wdl_rate'];
        $margin_order_info['partner_lv_2_zl_rate'] = $special_info['partner_lv_2_zl_rate'];
        $margin_order_info['partner_lv_2_sy_rate'] = $special_info['partner_lv_2_sy_rate'];
        //获取保证金收益百分比
        $distribute_info = $this->get_distribution_info($margin_order_info['buyer_id']);
//        file_put_contents(BASE_DATA_PATH.'/log/wt_test.log',"------margin_order_info--------\r\n",FILE_APPEND);
//        file_put_contents(BASE_DATA_PATH.'/log/wt_test.log',json_encode($margin_order_info)."\r\n",FILE_APPEND);
//        file_put_contents(BASE_DATA_PATH.'/log/wt_test.log',"------------------\r\n",FILE_APPEND);
//        file_put_contents(BASE_DATA_PATH.'/log/wt_test.log',json_encode($distribute_info)."\r\n",FILE_APPEND);
        if(empty($distribute_info)){
            goto END;
        }
        try{
            $this->commission_model->beginTransaction();
            foreach($distribute_info as $val){
                $tmp_key = "partner_lv_{$val['level']}_{$this->upgrade_setting[$val['member_type']]}_rate";
                $rate = $val['bzj_num'];
                $rate += isset($margin_order_info[$tmp_key])?$margin_order_info[$tmp_key]:0;
                $insert_data = array();
                $add_money = ncPriceFormat( $margin_amount * ($rate * 1.0 / 100) * ($day_number / 365)); //获取年化收益
                $insert_data['order_id'] = $margin_order_info['margin_id'];
                $insert_data['order_sn'] = $margin_order_info['order_sn'];
                $insert_data['order_goods_id'] = $margin_order_info['auction_id'];
                $insert_data['goods_id'] = $margin_order_info['auction_id'];
                $insert_data['goods_name'] = '"'.$margin_order_info['auction_name'].'"'.'保证金返佣收益'.$day_number.'天';
                $insert_data['goods_image'] = '';
                $insert_data['goods_pay_amount'] = $margin_amount;  //保证金金额
                $insert_data['from_member_id'] = $margin_order_info['buyer_id']; //佣金来源用户id
                $insert_data['from_member_name'] = $margin_order_info['buyer_name']; //佣金来源用户名字
                $insert_data['commission_amount'] = $add_money; //佣金金额
                $insert_data['dis_commis_rate'] = $rate;    //返佣比例
                $insert_data['dis_member_id'] = $val['member_id'];   //返佣受益人id
                $insert_data['dis_member_name'] = $val['member_name'];   //返佣受益人名字
                $insert_data['add_time'] = time();
                $insert_data['commission_time'] = 0;
                $insert_data['dis_commis_state'] = $state;   //佣金状态 0未结算 1结算
                $insert_data['commission_type'] = 3;    //返佣来源类型  保证金收益
                $insert_data['top_lv'] = $val['level'];//上级的级别,上级或上上级(对应三级分销)
                $tmp = $this->commission_model->getCommission(array('commission_type'=>3,'dis_member_id'=>$val['member_id'],'order_id'=>$margin_order_info['margin_id']));
                if($state == 1){
                    /********此段代码仅用做处理历史数据使用********/
                if(empty($tmp)){
                    $this->commission_model->insert($insert_data);
                }
                /********end********/
                    $data = array(
                        'dis_commis_state'=>1,
                        'commission_amount'=>$add_money,
                        'commission_time'=>time()
                    );
                    $res = $this->commission_model->updateCommission(array('commission_type'=>3,'dis_member_id'=>$val['member_id'],'order_id'=>$margin_order_info['margin_id'],'dis_commis_state'=>0),$data);
                }else{
                    if(empty($tmp)){
                       $res = $this->commission_model->insert($insert_data);
                    }
                }
                if(!$res){
                    throw new Exception('error');
                }
                if($state == 1){
                    $data_pd = array();
                    $data_pd['member_id'] = $insert_data['dis_member_id'];
                    $data_pd['member_name'] = $insert_data['dis_member_name'];
                    $data_pd['amount'] = $insert_data['commission_amount'];
                    $data_pd['msg'] = $insert_data['goods_name'];
                    $member = Model('member')->getMemberInfo(array('member_id'=>$val['member_id']));
                    $param = array();
                    $param['code'] = 'fanyong_notice';
                    $param['member_id'] = $member['member_id'];
                    $param['number']['mobile'] = $member['member_mobile'];
                    $param['param'] = array();
                    QueueClient::push('sendMemberMsg', $param);
                    //记录预存款日志
                    if(!$this->predeposit_model->changePd('member_margin_commission',$data_pd)){
                        throw new Exception('error:预存款日志错误');
                    }
                }
            }
        }catch(Exception $e){
            $this->commission_model->rollback();
            return false;
        }
        $this->commission_model->commit();
        END:
        return true;
    }

    /**
     * 区域代理保证金佣金记录
     * @param $margin_order_info
     * @param $day_number
     * @param int $state
     * @return bool
     */
    public function agent_margin_refund($margin_order_info, $day_number,$state = 1){
        $margin_amount = $margin_order_info['margin_amount']; //保证金金额
        if($day_number <= 0 || $margin_amount <= 0){
            goto  END;
        }

        //获取保证金收益百分比
        $distribute_info = $this->get_agent_info($margin_order_info['buyer_id']);
        if(empty($distribute_info)){
            goto END;
        }
        try{
            $this->agent_order_model->beginTransaction();
            foreach($distribute_info as $val){
                $insert_data = array();
                $add_money = ncPriceFormat( $margin_amount * ($val['bzj_num'] * 1.0 / 100) * ($day_number / 365)); //获取年化收益
                $insert_data['agent_id'] = $val['agent_id'];
                $insert_data['order_id'] = $margin_order_info['margin_id'];
                $insert_data['order_no'] = $margin_order_info['order_sn'];
                $insert_data['order_price'] = $margin_amount;  //保证金金额
                $insert_data['description'] = '"'.$margin_order_info['auction_name'].'"'.'保证金返佣收益'.$day_number.'天';
                $insert_data['from_member_id'] = $margin_order_info['buyer_id'];
                $insert_data['commission_type'] = 3;
                $insert_data['agent_type'] = $val['agent_type'];
                $insert_data['agent_lv'] = $val['agent_lv'];
                $insert_data['rate_num'] = $val['bzj_num'];
                $insert_data['commission_status'] = $state;
                $insert_data['commission_amount'] = $add_money;
                $insert_data['province_id'] = $this->province_id;
                $insert_data['city_id'] = $this->city_id;
                $insert_data['area_id'] = $this->area_id;
                $insert_data['cdate'] = date('Y-m-d H:i:s');

                if($state == 1){
                    $data = array(
                        'commission_status'=>1,
                        'finish_time'=>date('Y-m-d H:i:s')
                    );
                    $res = $this->agent_order_model->updateInfo(array('order_id'=>$margin_order_info['margin_id'],'agent_id'=>$val['agent_id']),$data);
                }else{
                    $res = $this->agent_order_model->addData($insert_data);
                }
//                dd();
                if(!$res){
                    throw new Exception('error');
                }
            }
        }catch(Exception $e){
            $this->agent_order_model->rollback();
            return false;
        }
        $this->agent_order_model->commit();
        END:
        return true;
    }


    /** 会员等级升级
     * @param  int  $member_id  会员id
     * @param  int $lv_id   分销等级
     * @param  string $msg  错误信息
     * @return bool
     */
    public function upgrade_lv($member_id, $lv_id, &$msg = null){
        $rs = false;
        if(empty($member_id) || empty($lv_id)){
            goto END;
        }

        $member_info = $this->member_model->getMemberInfo(array('member_id' => $member_id), 'member_name,member_id,td_amount,member_type');

        if(empty($member_info)){
            goto END;
        }

        $type_info = $this->distribute_type_model->where(array('type_id' => $lv_id))->find();

        if(empty($type_info)){
            goto END;
        }

        if($member_info['td_amount'] < $type_info['price'] || $member_info['member_type'] == $type_info['type_id']){
            goto  END;
        }

        $this->member_model->beginTransaction();
        try{
            $update_data = array(
                'member_type' => $type_info['type_id'],
                'td_amount' => array('exp','td_amount-'.$type_info['price'])
            );

            if(!$this->member_model->editMember(array('member_id' => $member_id, 'td_amount' => array('egt', $type_info['price'])), $update_data)){
                throw new  Exception ( '数据库更新出错!' );
            }

            if(!$this->member_model->editMemberDistribute(array('member_type' => $type_info['type_id']), array('member_id' => $member_id))){
                throw new  Exception ( '数据库更新出错!' );
            }

            $msg = $member_info['member_name'].'(会员id:'.$member_info['member_id'].')升级'.$type_info['name'];
            $rs = true;
             //区域代理佣金记录
            $agent_distribute_info = $this->get_agent_info($member_id);
            if(!empty($agent_distribute_info)){
                foreach($agent_distribute_info as $val){
                    $insert_data = array();
                    $add_money =ncPriceFormat( $type_info['price'] * ($val['up_num'] * 1.0 / 100)); //获取年化收益
                    $insert_data['agent_id'] = $val['agent_id'];
                    $insert_data['commission_type'] = 4;
                    $insert_data['agent_type'] = $val['agent_type'];
                    $insert_data['description'] = '升级'.$type_info['name'].'收益';
                    $insert_data['from_member_id'] = $member_info['member_id'];
                    $insert_data['agent_lv'] = $val['agent_lv'];
                    $insert_data['rate_num'] = $val['up_num'];
                    $insert_data['commission_status'] = 1;
                    $insert_data['commission_amount'] = $add_money;
                    $insert_data['province_id'] = $this->province_id;
                    $insert_data['city_id'] = $this->city_id;
                    $insert_data['area_id'] = $this->area_id;
                    $insert_data['cdate'] = date('Y-m-d H:i:s');
                    $res = $this->agent_order_model->insert($insert_data);
                    if(!$res){
                        throw new  Exception ( '数据库更新出错!' );
                    }
                }
            }
            //获取分销信息
            $member_relation = $this->get_top_member($member_id);
            if(empty($member_relation['top_member'])){
                goto END;
            }

            $distribute_info = $this->get_distribution_info($member_id);//获取分销返佣信息
            if(empty($distribute_info)){
                goto END;
            }
            foreach($distribute_info as $val){
                $insert_data = array();
                $add_money = ncPriceFormat( $type_info['price'] * ($val['up_num'] * 1.0 / 100)); //获取年化收益
                $insert_data['order_id'] = '';
                $insert_data['order_sn'] = '';
                $insert_data['order_goods_id'] = '';
                $insert_data['goods_id'] = '';
                $insert_data['goods_name'] = '升级'.$type_info['name'].'收益';
                $insert_data['goods_image'] = '';
                $insert_data['goods_pay_amount'] = $type_info['price'];  //升级所需消费金额
                $insert_data['from_member_id'] = $member_info['member_id']; //佣金来源用户id
                $insert_data['from_member_name'] = $member_info['member_name']; //佣金来源用户名字
                $insert_data['commission_amount'] = $add_money; //佣金金额
                $insert_data['dis_commis_rate'] = $val['up_num'];    //返佣比例
                $insert_data['dis_member_id'] = $val['member_id'];   //返佣受益人id
                $insert_data['dis_member_name'] = $val['member_name'];   //返佣受益人名字
                $insert_data['add_time'] = time();
                $insert_data['commission_time'] = $insert_data['add_time'];
                $insert_data['dis_commis_state'] = 1;   //佣金状态 0未结算 1结算
                $insert_data['commission_type'] = 4;    //返佣来源类型  会员分销升级
                $insert_data['top_lv'] = $val['level'];//上级级别,上级或上上级(对应三级分销)
                $res = $this->commission_model->insert($insert_data);

                if(!$res){
                    throw new  Exception ( '数据库更新出错!' );
                }

                $data_pd = array();
                $data_pd['member_id'] = $insert_data['dis_member_id'];
                $data_pd['member_name'] = $insert_data['dis_member_name'];
                $data_pd['amount'] = $insert_data['commission_amount'];
                $data_pd['msg'] = $insert_data['from_member_name'].'升级'.$type_info['name'].'返佣收益';

                //记录预存款日志
                if(!$this->predeposit_model->changePd('member_distribute_upgrade_lv',$data_pd)){
                    throw new  Exception ( '数据库更新出错!' );
                }
                $member = Model('member')->getMemberInfo(array('member_id'=>$val['member_id']));
                $param = array();
                $param['code'] = 'fanyong_notice';
                $param['member_id'] = $member['member_id'];
                $param['number']['mobile'] = $member['member_mobile'];
                $param['param'] = array();
                QueueClient::push('sendMemberMsg', $param);
            }



            END:
            $this->member_model->commit();
        }catch(Exception $e){
            $rs = false;
            $this->member_model->rollback();
            throw_exception($e->getMessage());
        }

        return $rs;
    }

/******************************************
 *       ↓↓↓↓三级分销↓↓↓↓↓↓
 ******************************************
 */

    /**
     * 分销信息,及返佣比例信息
     * @param $member_id
     */
    public function get_distribution_info($member_id){
        $info = $this->member_model->getUpperMember(array('member_id' => $member_id));
        $member_info = $this->member_model->getMemberInfoByID($member_id);
        $config = $this->get_distribute_config();
        $re = array();
        if($info['top_member'] != 0){
            //存在上一级用户
            $tmp_member = $this->member_model->getMemberInfoByID($info['top_member']);
            $tmp = array(
                'member_id'=>$tmp_member['member_id'],
                'member_name' => $tmp_member['member_name'],
                'level'=>1,
                'member_type'=>$tmp_member['member_type']
            );
            $re[] = $tmp;
        }
        //是否开启二级返佣
        $model_setting = Model('setting');
        $setting = $model_setting->getRowSetting("level_two_rebate");
        if($info['top_member_2'] != 0 && $setting && $setting['value']==1){
            //存在上级的上级(三级分销)
            $tmp_member = $this->member_model->getMemberInfoByID($info['top_member_2']);
            $tmp = array(
                'member_id'=>$tmp_member['member_id'],
                'member_name' => $tmp_member['member_name'],
                'level'=>2,
                'member_type'=>$tmp_member['member_type']
            );
            $re[] = $tmp;
        }
//        file_put_contents(BASE_DATA_PATH . '/log/test.log','--------------'.$this->u.'------------------'."\r\n", FILE_APPEND);
        if(!empty($re)){//存在上级
            foreach($re as &$top_member){
                $top_member['goods_num'] = 0;
                $top_member['bzj_num'] = 0;
                $top_member['artist_num'] = 0;
                $top_member['up_num'] = 0;
                if(!empty($config)){//存在配置
                    foreach($config as $config_val){
                        if($config_val['level'] == $top_member['level'] && $config_val['type_id'] == $top_member['member_type']){
                            $top_member['goods_num'] = $config_val['goods_num'];
                            $top_member['bzj_num'] = $config_val['bzj_num'];
                            $top_member['artist_num'] = $config_val['artist_num'];
                            $top_member['up_num'] = $config_val[$this->upgrade_setting[$member_info['member_type']].'_num'];
                        }
                    }
                }
            }
        }
        return $re;

    }

    /**
     * 获取用户所在区域的区域代理返佣信息
     * @param $member_id
     * @return array
     */
    public function get_agent_info($member_id){
        $re = array();
        $member_info = $this->member_model->getMemberInfoByID($member_id);
        /** @var agent_commissionModel $agent_commission */
        $agent_commission = Model('agent_commission');
        $area_agent = Model('area_agent');
        if(!$member_info['member_provinceid']){
            return $re;
        }
        $this->province_id = $member_info['member_provinceid'];
        $this->city_id = $member_info['member_cityid'];
        $this->area_id = $member_info['member_areaid'];
        $area_str = $member_info['member_provinceid'].','.$member_info['member_cityid'].','.$member_info['member_areaid'];
        $agent_list = $area_agent->getAreaAgentList(array('area_id'=>array('in',$area_str),'agent_state'=>1));
        if(!empty($agent_list)){
            foreach($agent_list as $agent){
                $config = $agent_commission->getAgentCommissionInfo(array('commission_lv'=>$agent['commission_lv'],'agent_type'=>$agent['area_type']));
                $re[] = array(
                    'agent_id'=>$agent['agent_id'],
                    'agent_type'=>$agent['area_type'],
                    'agent_lv'=>$config['commission_lv'],
                    'goods_num'=>$config['goods_num'],
                    'bzj_num'=>$config['bzj_num'],
                    'artist_num'=>$config['artist_num'],
                    'gg_num'=>$config['gg_num'],
                    'up_num'=>$config[$this->upgrade_setting[$member_info['member_type']].'_num']
                );
            }
        }
        return $re;

    }

    /**
     * 获取上级返佣配置信息
     * @return array
     */
    private function get_distribute_config(){
        return $this->distribute_detail_model->select();
    }

    /**
     * 区域代理商品返佣记录
     * @param $data
     * @param $order
     * @param $rate
     */
    public function agent_goods_commission($config,$order,$type){
        if($type == 1){
            $insert_data = array();
            foreach($order as $key => $val){
                $data = array();
                $add_money = $val['goods_commission'] * ($config['goods_num'] * 1.0 / 100);
                $data['agent_id'] = $config['agent_id'];
                $data['commission_type'] = 1;
                $data['order_id'] = $val['order_id'];
                $data['order_price'] = $val['goods_pay_amount'];
                $data['description'] = $val['goods_name'];
                $data['from_member_id'] = $val['from_id'];
                $data['order_no'] = $val['order_sn'];
                $data['agent_type'] = $config['agent_type'];
                $data['agent_lv'] = $config['agent_lv'];
                $data['rate_num'] = $config['goods_num'];
                $data['commission_status'] = $type;
                $data['commission_amount'] = $add_money;
                $data['province_id'] = $this->province_id;
                $data['city_id'] = $this->city_id;
                $data['area_id'] = $this->area_id;
                $data['cdate'] = date('Y-m-d H:i:s');
                $insert_data[] = $data;
            }
            $this->agent_order_model->insertAll($insert_data);
        }else{
            $add_money = $order['goods_commission'] * ($config['goods_num'] * 1.0 / 100);
            $insert_data['agent_id'] = $config['agent_id'];
            $insert_data['commission_type'] = 1;
            $insert_data['order_id'] = $order['order_id'];
            $insert_data['order_price'] = $order['goods_pay_amount'];
            $insert_data['description'] = $order['goods_name'];
            $insert_data['from_member_id'] = $order['from_id'];
            $insert_data['order_no'] = $order['order_sn'];
            $insert_data['agent_type'] = $config['agent_type'];
            $insert_data['agent_lv'] = $config['agent_lv'];
            $insert_data['rate_num'] = $config['goods_num'];
            $insert_data['commission_status'] = $type;
            $insert_data['commission_amount'] = $add_money;
            $insert_data['province_id'] = $this->province_id;
            $insert_data['city_id'] = $this->city_id;
            $insert_data['area_id'] = $this->area_id;
            $insert_data['cdate'] = date('Y-m-d H:i:s');
            $this->agent_order_model->insert($insert_data);
        }
    }

    public function getLowerLevelMemberAvatarByMemberId($memberId)
    {
        $condition['top_member|top_member_2'] = $memberId;
        $memberIds = Model('')->table('member_distribute')
            ->field('member_id')->where($condition)->limit(5)->select();
        if (empty($memberIds)) {
            return [];
        }
        $memberIds = array_column($memberIds, 'member_id');
        return array_map(function ($value) {
            return getMemberAvatarForID($value) . '?' . TIMESTAMP;
        }, $memberIds);
    }


}
