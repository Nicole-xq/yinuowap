<?php
/**
 * 微信消息模板管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class wx_qrcode_createLogic
{

    private $app_id;
    private $app_secret;
    private $qr_scene;
    private $post_data;
    private $action_type;
    private $expire_seconds;
    private $qrcode_action_name = ['QR_STR_SCENE', 'QR_SCENE', 'QR_LIMIT_STR_SCENE', 'QR_LIMIT_SCENE'];

    /**
     * 生成分享图片
     * @param $member_id
     * @return string
     * */
    public function getQrcodeImg($member_id)
    {
        /** @var memberModel $model_member */
        $model_member = Model('member');

        $info = $model_member->getUpperMember(['member_id' => $member_id], 'dis_code,wx_qrcode_ticket');
        $qrcode = "dis_code_{$info['dis_code']}.jpg";
        $image_path = BASE_DATA_PATH . "/upload/mobile/wx_qrcode/" . $qrcode;

        if (is_file($image_path)) {
            return $qrcode;
        }

        if (empty($info['wx_qrcode_ticket'])) {
            $ticket_info = $this->getWxQrcode($info['dis_code'],2);
            $ticket_info = json_decode($ticket_info[1], true);

            if (!empty($ticket_info['ticket'])) {
                $save_data = array(
                    'wx_qrcode_ticket' => $ticket_info['ticket'],
                    'wx_qrcode_url' => $ticket_info['url'],
                    'qr_expire_seconds' => 0,
                );
                $re = $model_member->editMemberDistribute($save_data, ['member_id' => $member_id]);
                if (!empty($re)) {
                    $info['wx_qrcode_ticket'] = $ticket_info['ticket'];
                }
            }
            if (empty($info['wx_qrcode_ticket'])) {
                return '';
            }
        }

        // 模版图片处理
        $filename = BASE_DATA_PATH . '/upload/mobile/wx_qrcode/img_tamplate.jpg';
        list($width, $height) = getimagesize($filename);
        $new_width = 750;
        $new_height = 1050;
        $template_image = imagecreatefromjpeg($filename);
        $tmp = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($tmp, $template_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        // 二维码图片处理
        $qrcode_url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={$info['wx_qrcode_ticket']}";
        list($width, $height) = getimagesize($qrcode_url);
        $new_width = 272;
        $new_height = 272;
        $qrcode_image = imagecreatefromjpeg($qrcode_url);
        $tmp2 = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($tmp2, $qrcode_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        // 合并保存
        imagecopymerge($tmp, $tmp2, 239, 463, 0, 0, $new_width, $new_height, 100);
        imagejpeg($tmp, $image_path);

        return $qrcode;
    }

    /**
     * 获取二维码ticket
     * @param $qr_scene [二维码参数]
     * @param $action_type [1临时，2永久，默认临时]
     * @param $expire_seconds [过期时间,默认30天]
     * @return array
     */
    public function getWxQrcode($qr_scene, $action_type = 0, $expire_seconds = 2592000)
    {
        $this->qr_scene = $qr_scene;
        $this->action_type = $action_type;
        $this->expire_seconds = $expire_seconds;
        self::_initializationData();
        $inc_file = BASE_ROOT_PATH .'/mobile/api/payment/wxpay_jsapi/wx_jssdk.php';
        
        if (empty($this->app_id) || empty($this->app_secret) || empty($this->qr_scene) || !is_file($inc_file)) {
            $send_result_data = [201];
        } else {
            require_once($inc_file);
            $sdkObj = new JSSDK($this->app_id, $this->app_secret);
            $send_result_data = $sdkObj->createQrcode($this->post_data);
        }
        return $send_result_data;
    }

    /**
     * 初始化参数配置
     */
    private function _initializationData()
    {
        self::setWxTokenParam();
        self::setWxTicketParam();
    }

    /**
     * 构造ticket发送参数
     */
    private function setWxTicketParam()
    {
        $type = in_array($this->action_type, array('1', '3')) ? 'scene_id' : 'scene_str';
        $this->post_data = array(
            'action_name' => $this->qrcode_action_name[$this->action_type],
            'expire_seconds' => $this->expire_seconds,
            'action_info' => array(
                'scene' => array(
                    $type => $this->qr_scene,
                )
            )
        );
    }

    /**
     * 获取公众号基础配置信息
     */
    private function setWxTokenParam()
    {
        /** @var mb_paymentModel $model_mb_payment */
        $model_mb_payment = Model('mb_payment');
        $mb_payment_info = $model_mb_payment->getMbPaymentInfo(array('payment_id' => 3));
        $this->app_id = $mb_payment_info['payment_config']['appId'];
        $this->app_secret = $mb_payment_info['payment_config']['appSecret'];
    }

}
