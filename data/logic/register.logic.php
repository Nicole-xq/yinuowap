<?php
/**
 * 注册奖励
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class registerLogic {
    protected $log;
    /**
     * 用户获取注册奖励
     * @param $member_id
     */
    public function getAward($member_id){
        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($member_id);
        $this->log['member_id'] = $member_id;
        $this->log['member'] = $member_info;
        if($member_info['register_award'] == 0){
            $model_register_activity = Model('register_activity');
            $list = $model_register_activity->get_award_list();
            $num = 0;
            if(!empty($list)){
                foreach($list as $val){
                    $re = $this->_get_redpacket_info($val['code'],$member_info);
                    if($re){
                        $num += 1;
                    }
                }
            }
            $model_member->editMember(array('member_id'=>$member_id),array('register_award'=>1));
            $re = $model_member->getUpperMember(array('member_id'=>$member_id));
            $this->log['top_member'] = $re;
            $top_member_id = $re['top_member'];
            $top_member_name = $re['top_member_name'];
            if($top_member_id != 0){
                $this->top_member_award($top_member_id,$top_member_name);
            }
            return $num;
            //未领取过注册奖励
        }else{
            //已经领取过注册奖励,不可再领取
            return 0;
        }
//        print_R($member_info);exit;
    }

    private function _get_redpacket_info($id,$member_info){
        $model_redpacket = Model('redpacket');
        //验证是否可领取红包
        $data = $model_redpacket->getCanChangeTemplateInfo($id,$member_info['member_id']);
        if ($data['state'] == false){
            $this->log['member_redpacket'] = 'no redpacket';
            return false;
//            showDialog($data['msg'], '', 'error');
        }
        try {
            $model_redpacket->beginTransaction();
            //添加红包信息
            $data = $model_redpacket->exchangeRedpacket($data['info'], $member_info['member_id'], $member_info['member_name']);
            if ($data['state'] == false) {
                $this->log['member_redpacket'] = $data['msg'];
                throw new Exception($data['msg']);
            }
            Model('redpacket_notice')->add_notice(array('member_id'=>$member_info['member_id'],'redpacket_id'=>$data['info']));
            $model_redpacket->commit();
            $this->log['member_redpacket'] = 'success';
            return true;
//            showDialog('红包领取成功', MEMBER_SITE_URL.'/index.php?act=member_redpacket&op=index', 'succ');
        } catch (Exception $e) {
            $model_redpacket->rollback();
            $this->log['member_redpacket'] = $e->getMessage();
            return false;
//            showDialog($e->getMessage(), '', 'error');
        }
    }

    public function top_member_award($member_id,$member_name){
        $award = Model('setting')->getRowSetting('top_member_award');
        $model_redpacket = Model('redpacket');
        $data = $model_redpacket->getCanChangeTemplateInfo($award['value'],$member_id);
        if ($data['state'] == false){
            $this->log['top_member_award'] = 'no award';
            return false;
//            showDialog($data['msg'], '', 'error');
        }
        $model_redpacket = Model('redpacket');
        $model_redpacket->beginTransaction();
        //添加红包信息
        $data = $model_redpacket->exchangeRedpacket($data['info'], $member_id, $member_name);
        if ($data['state'] == false) {
            $this->log['top_member_award'] = 'fail';
            $model_redpacket->rollback();
            return false;
//            throw new Exception($data['msg']);
        }
        Model('redpacket_notice')->add_notice(array('member_id'=>$member_id,'redpacket_id'=>$data['info']));
        $this->log['top_member_award'] = 'success';
        $model_redpacket->commit();
    }


    /**
     * 会销注册
     * @param integer $reg_member_id
     * @param integer $top_member_id
     * @param integer $ex_id
     * @return boolean
     */
    public function exhibition_reg($reg_member_id,$top_member_id,$ex_id){
        if (empty($reg_member_id) || empty($top_member_id) || empty($ex_id)){
            return false;
        }
        $model_member = Model('member');

        $reg_member_info = $model_member->getMemberInfo(['member_id'=>$reg_member_id], 'member_id,member_name,member_mobile');
        $top_member_info = $model_member->getMemberInfo(['member_id'=>$top_member_id], 'member_id,member_real_name,member_using_mobile');

        $add_data = [
            'ex_id'=>intval($ex_id),
            'member_id'=>$reg_member_info['member_id'],
            'member_real_name'=>'',
            'member_using_mobile'=>$reg_member_info['member_mobile'],
            'member_top'=>$top_member_info['member_id'],
            'member_top_name'=>$top_member_info['member_real_name'],
            'member_top_mobile'=>$top_member_info['member_using_mobile'],
            'add_time'=>TIMESTAMP,
            'modify_time'=>TIMESTAMP,
        ];
        return Model('exhibition_entry')->insert($add_data);
    }
}
