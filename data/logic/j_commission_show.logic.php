<?php
/**
 * 佣金显示
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class j_commission_showLogic
{

    /**
     * @var member_commissionModel
     */
    var $model;


    public function __construct(){
        $this->model = Model('member_commission');
    }



    /**获取下级会员保证金分销信息
     * @param $member_id        会员id
     * @param $form_member_id   下级分销会员id
     * @param int $page         显示页数
     * @return array
     */
    public function get_form_margin_commission($member_id, $form_member_id, $page = 100){

        $where = array();
        $where['dis_member_id'] = $member_id;
        if($form_member_id){
            $where['from_member_id'] = $form_member_id;
        }
        $list = $this->model->getByTypeCommission(2, $where, 'order_id,log_id,commission_amount,add_time,dis_commis_rate,dis_commis_state,goods_pay_amount',$page, 'log_id desc');

        if(empty($list)){
            goto  END;
        }

        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');

        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $value['label_dis_commis_rate'] = $value['dis_commis_rate'] .'%';

            //保证金订单内容
            $order_info = $model_margin_orders->getOrderInfo(array('margin_id' => $value['order_id']), 'pay_date_number,payment_time');

            $order_info['pay_date_number'] = $order_info['pay_date_number'];
            $order_info['label_payment_time'] = date('Y.m.d', $order_info['payment_time']);
            $value['order_info'] = $order_info;

            $value['label_status'] = $this->model->status_setting[$value['dis_commis_state']]['label'];
        }

        END:
        return $list;
    }


}
