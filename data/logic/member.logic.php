<?php
defined('InShopNC') or exit('Access Invalid!');
class memberLogic {
    /**
     * 包装 兼容代码以后考虑移植到laravel里 -----------------------------------------------------------------------------------------------------
     */

    /**
     * 通过token获取用户信息
     * @param $token
     * @param bool $isSimple 是否只需要member表信息
     * @return null|array
     */
    public function getMemberInfoByToken($token, $isSimple = false){
        $userTokenInfo = $this->getUserTokenInfoByToken($token);
        if(!$userTokenInfo){
            return null;
        }
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $memberInfo = $model_member->getMemberInfoByID($userTokenInfo['member_id']);
        if(empty($memberInfo)) {
            return null;
        } else if (!$isSimple) {
            $member_gradeinfo = $model_member->getOneMemberGrade(intval($memberInfo['member_exppoints']));
            $memberInfo['level'] = $member_gradeinfo['level'];
            $memberInfo['client_type'] = $userTokenInfo['client_type'];
            $memberInfo['openid'] = $userTokenInfo['openid'];
            $memberInfo['token'] = $userTokenInfo['token'];
//            $this->member_info['distribute_lv_1'] = $mb_user_token_info['distribute_lv_1'];
//            $this->member_info['distribute_lv_2'] = $mb_user_token_info['distribute_lv_2'];
            /** @var distribution_configModel $model_distribution_config */
            $model_distribution_config = Model('distribution_config');
            $distribution_info = $model_distribution_config->getConfigByTypeId($memberInfo['member_type']);

//            print_R($this->member_info);exit;
            $memberInfo['member_type_name'] = $distribution_info['name'];

            //读取卖家信息
            $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$memberInfo['member_id']));
            $memberInfo['store_id'] = $seller_info['store_id'];
        }
        return $memberInfo;
    }

    /**
     * 通过mb_user_token表获得基础用户信息
     * @param $token
     * @return array|null
     *      int $token_id 令牌编号
     *      int $member_id 用户编号
     *      string $member_name 用户名
     *      int|null $user_id 用户ID
     *      string|null $user_name 用户名
     *      string $token 登录令牌
     *      int $login_time 登录时间
     *      string $client_type 客户端类型 android wap
     *      string|null $openid 微信支付jsapi的openid缓存
     */
    public function getUserTokenInfoByToken($token){
        $model_mb_user_token = Model('mb_user_token');
        $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($token);
        return $mb_user_token_info?$mb_user_token_info:null;
    }

    /**
     * 根据用户ID判断用户是否为新手
     * @param $memberId
     * @return bool
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function isNewBie($memberId)
    {
        Model("bid_log");
        $checkBidLogCondition = [
            'member_id' => $memberId,
            //'bid_type' => bid_logModel::BID_TYPE_NEWBIE,
        ];
        $result = $this->checkBidLog($checkBidLogCondition);
        //第一次出价后（包含体验）7天内是新手
        if ($result && $result['created_at'] < time() - 7*24*3600) {
            return false;
        }

        //全场拍中1单即不为新手
        if (Model('order')->where(["buyer_id"=>$memberId, "order_type"=>4, "auction_id"=>["neq", getMainConfig("newBieAuctionId")]])->count() > 0){
            return false;
        }

        //出价6次之后不做为新手
        if (Model('bid_log')->where(["member_id"=>$memberId, "auction_id"=>["neq", getMainConfig("newBieAuctionId")]])->count() >= \App\Models\ShopncBidLog::NEWBIE_COUNT){
            return false;
        }


        return true;
    }

    /**
     * 解密程序登录信息
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($appid, $sessionKey, $encryptedData, $iv, &$data)
    {
        if (strlen($sessionKey) != 24) {
            return 41001;
        }
        $aesKey = base64_decode($sessionKey);


        if (strlen($iv) != 24) {
            return 41002;
        }
        $aesIV = base64_decode($iv);

        $aesCipher = base64_decode($encryptedData);

        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj = json_decode($result);
        if (empty($dataObj)) {
            return 41003;
        }
        if ($dataObj->watermark->appid != $appid) {
            return 41003;
        }
        $data = $result;
        return 0;
    }




    public function checkBidLog($checkBidLog)
    {
        return Model('')->table('bid_log')->where($checkBidLog)->order('created_at asc')->find();
    }


    /**
     * 包装 兼容代码以后考虑移植到laravel里 end -----------------------------------------------------------------------------------------------------
     */
}
