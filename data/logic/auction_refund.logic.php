<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/29 0029
 * Time: 15:26
 * @author DukeAnn
 */

defined('InShopNC') or exit('Access Invalid!');

class auction_refundLogic{
    // 支付方式代码
    protected $payment_code;
    // 支付方式配置信息
    protected $payment_config;
    // 订单详情
    protected $order_info;
    // 退款结果
    protected $result;

    /**
     * 拍卖订单退款，站内支付方式退款，如果三方支付的没有原路返回，退回预存款
     * @param $order_info array 订单信息
     * @param $admin_message string 商家退款备注
     * @return mixed
     * */
    public function auction_refund($order_info, $admin_message)
    {
        $this->payment_code = $order_info['payment_code'];
        $this->order_info = $order_info;
        // 退回诺币
        if ($this->order_info['points_amount'] > 0 && $this->order_info['points_amount_refund'] == 0) {
            $this->_refund_nuobi();
        }
        // 退回保证金
        if ($this->order_info['margin_amount'] > 0 && $this->order_info['margin_amount_refund'] == 0) {
            $this->_refund_margin();
        }
        // 退回预存款
        if (($this->order_info['pd_amount'] > 0 || $this->order_info['api_pay_amount'] > 0 || $this->order_info['payment_code'] == 'underline') && $this->order_info['pd_amount_refund'] == 0) {
            $this->_refund_predeposit();
        }
        // 退回充值卡，三方支付和线下支付
        if ($this->order_info['rcb_amount'] > 0 && $this->order_info['rcb_amount_refund'] == 0) {
            $this->_refund_rcb();
        }

        // 修改订单状态
        $model_auction_order = Model('auction_order');
        $order_info = $model_auction_order->getInfo(array('auction_order_id' => $this->order_info['auction_order_id']));
        // 检查是否全部退完
        $state = $order_info['order_amount'] - $order_info['api_refund_amount'] - $order_info['rcb_amount_refund'] - $order_info['points_amount_refund'] - $order_info['pd_amount_refund'] - $order_info['margin_amount_refund'];
        // 全部退款完成修改订单状态
        if ($state == 0) {
            $model_auction_order->setOrder(
                array('auction_order_id'=> $this->order_info['auction_order_id']),
                array(
                    'refund_state'=> 2,
                    'refund_amount' => $order_info['order_amount'],
                    'close_time' => time(),
                    'close_reason' => '平台完成退款',
                    'finnshed_time' => time(),
                    'order_state' => 0,
                    'admin_message' => $admin_message,
                )
            );
            $msg = '退款完成';
            return callback(true, $msg);
        } else {
            return callback(true, '退款失败');
        }
    }

    /*
     * 退回诺币
     * */
    protected function _refund_nuobi()
    {
        $nuobi_points = $this->order_info['points_amount'] *100;
        Model('points')->savePointsLog(
            'refund',
            array(
                'pl_memberid'=>$this->order_info['buyer_id'],
                'pl_membername'=>$this->order_info['buyer_name'],
                'pl_points'=>$nuobi_points,
                'order_sn'=>$this->order_info['auction_order_sn'],
                'order_id'=>$this->order_info['auction_order_id']
            ),
            true
        );
        $model_auction_order = Model('auction_order');
        $model_auction_order->setOrder(
            array('auction_order_id'=> $this->order_info['auction_order_id']),
            array(
                'points_amount_refund'=> $this->order_info['points_amount'],
            )
        );

    }

    /*
     * 退回保证金
     * */
    protected function _refund_margin()
    {
        $logic_auction_order = Logic('auction_order');
        $data = array(
            'member_id' => $this->order_info['buyer_id'],
            'order_sn' => $this->order_info['auction_order_sn'],
            'amount' => $this->order_info['margin_amount'],
        );
        // 退保证金并记录日志
        $logic_auction_order->MarginRefund($data, 'return');

        $model_auction_order = Model('auction_order');
        $model_auction_order->setOrder(
            array('auction_order_id'=> $this->order_info['auction_order_id']),
            array(
                'margin_amount_refund'=> $this->order_info['margin_amount'],
            )
        );
    }

    /*
     * 退回预存款，检查三方支付是否原路返回，如果没有退回预存款
     * */
    protected function _refund_predeposit()
    {
        $model_predeposit = Model('predeposit');
        $log_array = array();
        $log_array['member_id'] = $this->order_info['buyer_id'];
        $log_array['member_name'] = $this->order_info['buyer_name'];
        $log_array['order_sn'] = $this->order_info['auction_order_sn'];
        if ($this->order_info['api_refund_amount'] == 0) {
            $log_array['amount'] = $this->order_info['pd_amount'] + $this->order_info['api_pay_amount'];//退预存款金额
        } elseif($this->order_info['payment_code'] == 'underline') {
            $log_array['amount'] = $this->order_info['order_amount'] - $this->order_info['margin_amount'];
        } else {
            $log_array['amount'] = $this->order_info['pd_amount'];
        }


        $model_predeposit->changePd('refund', $log_array);//增加买家可用预存款金额

        $model_auction_order = Model('auction_order');
        $model_auction_order->setOrder(
            array('auction_order_id'=> $this->order_info['auction_order_id']),
            array(
                'pd_amount_refund'=> $log_array['amount'],
            )
        );
    }

    /*
     * 退回充值卡
     * */
    protected function _refund_rcb()
    {
        $model_predeposit = Model('predeposit');
        $log_array = array();
        $log_array['member_id'] = $this->order_info['buyer_id'];
        $log_array['member_name'] = $this->order_info['buyer_name'];
        $log_array['order_sn'] = $this->order_info['auction_order_sn'];
        $log_array['amount'] = $this->order_info['rcb_amount'];

        $model_predeposit->changeRcb('refund', $log_array);//增加买家可用充值卡金额

        $model_auction_order = Model('auction_order');
        $model_auction_order->setOrder(
            array('auction_order_id'=> $this->order_info['auction_order_id']),
            array(
                'rcb_amount_refund'=> $this->order_info['rcb_amount'],
            )
        );
    }
}
