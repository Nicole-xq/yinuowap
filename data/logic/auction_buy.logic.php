<?php
/**
 * 拍品购买
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/20 0020
 * Time: 10:57
 * @author ADKi
 */

defined('InShopNC') or exit('Access Invalid!');

class auction_buyLogic
{
    // 拍品信息
    public $auction_info;

    // 店铺信息
    public $store_info;

    // 会员信息
    public $member_info;

    /**
     *
     * */
    public function show_margin_order($auction_id, $member_id)
    {
        $this->set_basis_info($auction_id, $member_id);
        //输出用户默认收货地址
        /** @var addressModel $address */
        $address = Model('address');
        $result['address_info'] = $address->getDefaultAddressInfo(array('member_id'=>$member_id));
        //如果所购商品只支持线上支付 true，支付方式不允许修改
        $result['deny_edit_payment'] = false;

        //不提供增值税发票时抛出true(模板使用)
        $vat_deny = true;
        $result['vat_deny'] = $vat_deny;
        $result['vat_hash'] = $this->buyEncrypt($result['vat_deny'] ? 'deny_vat' : 'allow_vat', $member_id);
        //保证金无发票
        $inv_info['content'] = '无发票';
        $result['inv_info'] = $inv_info;

        //移动端
        if (APP_ID == 'mobile') {
            $buyer_info = Model('member')->getMemberInfoByID($member_id);
            //预存款可用金额
            if (floatval($buyer_info['available_predeposit']) > 0) {
                $result['available_predeposit'] = $buyer_info['available_predeposit'];
            }
            //可用充值卡余额
            if (floatval($buyer_info['available_rc_balance']) > 0) {
                $result['available_rc_balance'] = $buyer_info['available_rc_balance'];
            }
            //支付密码
            $result['member_paypwd'] = $buyer_info['member_paypwd'] ? true : false;
        }

        //检查是否已经结束拍卖
        $result['is_auction_end_true_t'] = $this->auctionEndTime($this->auction_info->auction_end_true_t);
        return $result;
    }

    /**
     * 生成保证金订单
     * @param $auction_id int 拍品ID
     * @param $member_id int 会员ID
     * @param $payment_code string 支付方式代码
     * @param $address_id int 地址ID
     * @param $is_anonymous int 是否匿名
     * @param $order_from int 1PC，2移动
     * @param $other_arr array 其他信息
     * @return array
     * */
    public function create_margin_order($auction_id, $member_id, $payment_code, $address_id, $is_anonymous, $order_from,$other_arr)
    {
        /** @var margin_ordersModel $model_margin_orders */
        /** @var addressModel $model_address */
        /** @var auction_member_relationModel $model_auction_member_relation */
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $model_address = Model('address');
        $model_auction_member_relation = Model('auction_member_relation');

        $this->set_basis_info($auction_id, $member_id);
        if ($other_arr['margin_amount'] && $other_arr['margin_amount']<$this->auction_info['auction_bond']){
            return callback(false, '保证金低于拍卖保证金');
        }

        //联系地址
        if ($address_id){// member_mobile
            $address_info = $model_address->getAddressInfo(array('address_id' => $address_id));
        }
        $order_sn = $this->make_order_sn();

        //生成订单初始信息
        $param = array(
            'order_sn' => $order_sn,
            'store_id' => $this->store_info['store_id'],
            'store_name' => $this->store_info['store_name'],
            'buyer_id' => $this->member_info['member_id'],
            'buyer_name' => $this->member_info['member_name'],
            'buyer_phone' => $this->member_info['member_mobile'],
            'created_at' => time(),
            'updated_at' => time(),
            'margin_amount' => $other_arr['margin_amount']
                ?  $other_arr['margin_amount']
                : $this->auction_info['auction_bond'],
            'order_state' => 0,
            'order_type' => $payment_code == 'online' ? 0 : 1,
            'address_id' => $address_id,
            'payment_code' => $payment_code,
            'auction_id' => $this->auction_info['auction_id'],
            'auction_name' => $this->auction_info['auction_name'],
            'auction_image' => $this->auction_info['auction_image'],
            'order_from' => $order_from,
        );
        if (!empty($address_info)){
            $param['buyer_phone'] = empty($address_info['mob_phone'])
                ? $address_info['tel_phone']
                : $address_info['mob_phone'];
            $param['area_info'] = $address_info['area_info'];
            $param['address'] =  $address_info['address'];
            $param['reciver_name'] = $address_info['true_name'];
        }

        try {

            $model_margin_orders->beginTransaction();
            if(empty($this->member_info['member_mobile'])){
                throw new Exception('请先绑定手机!');
            }

            $order = $model_margin_orders->where(array('buyer_id'=> $member_id,'created_at'=> array('egt',TIMESTAMP-3)))->find();//防止误操作,单个会员3秒内只能提交一个订单
            if (is_array($order) && !empty($order)) {
                throw new Exception('请勿多次提交订单');
            }
            //锁定当前会员记录
            $_info = Model('member')->table('member')->where(array('member_id'=> $member_id))
                ->master(true)->lock(true)->find();

            // 订单查重
            $condition = array('buyer_id'=> $member_id,'order_state'=>array('neq',4),
                'auction_id' => $this->auction_info['auction_id']);
            $order_info = $model_margin_orders->getOrderInfo($condition);

            if (empty($order_info)) {
                // 不存在生成订单
                $margin_id = $model_margin_orders->addOrder($param);
                if (!$margin_id) {
                    throw new Exception('生成订单失败');
                }
            } else {
                throw new Exception('已经生成过保证金订单');
//                if($order_info['order_state'] == 1){
//                    throw new Exception('已经生成过保证金订单');
//                }
//
//                // 存在 更新订单信息
//                $order_sn = $order_info['order_sn'];
//                unset($param['order_sn']);
//                unset($param['created_at']);
//                $margin_id = $model_margin_orders->editOrder($param, array('order_sn' => $order_sn));
//                if (!$margin_id) {
//                    throw new Exception('更新订单失败');
//                }
            }
            // 检查是否存在拍品会员关系
            $relation_info = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
            if ($is_anonymous != 1) {
                $is_anonymous = 0;
            }
            if (empty($relation_info)) {
                // 为空创建关系
                $Insert = array(
                    'auction_id' => $auction_id,
                    'member_id' => $member_id,
                    'is_bond'=>2,
                    'is_anonymous' => $is_anonymous,
                    'member_name' => $this->member_info['member_name'],
                    'member_mobile' => $this->member_info['member_mobile'],
                );
                $result = $model_auction_member_relation->addRelation($Insert);
                if (empty($result)) {
                    throw new Exception('创建关系失败');
                }
            } else {
                // 不为空更新关系
                $update = array(
                    'is_anonymous' => $is_anonymous,
                    'member_name' => $this->member_info['member_name'],
                    'member_mobile' => $this->member_info['member_mobile'],
                    'is_bond'=>2
                );
                $condition = array(
                    'auction_id' => $auction_id,
                    'member_id' => $member_id,
                );
                $model_auction_member_relation->setRelationInfo($update, $condition);
            }

            $model_margin_orders->commit();
            return callback(true,'',array('order_sn' => $order_sn, 'payment_code' => $payment_code));
        } catch (Exception $e) {
            $model_margin_orders->rollback();
            return callback(false, $e->getMessage());
        }
    }

    /**
     * @param $memberId
     * @param $redPacketId
     * @param $auctionId
     * @return bool
     * @Date: 2019/4/22 0022
     * @author: Mr.Liu
     */
    public function createNewbieMargin($memberId, $redPacketId, $auctionId, $orderId = 0)
    {
        $condition = [
            'buyer_id' => $memberId,
            'auction_id' => $auctionId,
        ];
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $exist = Model('')->table('margin_orders')->where($condition)->find();
        if ($exist) {
            return true;
        }
        $order_sn = $this->make_order_sn();
        $this->set_basis_info($auctionId, $memberId);
        //生成订单初始信息
        $param = array(
            'order_sn' => $order_sn,
            'store_id' => $this->store_info['store_id'],
            'store_name' => $this->store_info['store_name'],
            'buyer_id' => $this->member_info['member_id'],
            'buyer_name' => $this->member_info['member_name'],
            'buyer_phone' => $this->member_info['member_mobile'],
            'created_at' => time(),
            'updated_at' => time(),
            'margin_amount' => 10,
            'order_state' => 1,
            'order_type' => 0,
            'address_id' => 0,
            'payment_code' => 'online',
            'auction_id' => $this->auction_info['auction_id'],
            'auction_name' => $this->auction_info['auction_name'],
            'auction_image' => $this->auction_info['auction_image'],
            'auction_order_id' => $orderId,
            'order_from' => 2,
            'red_packet_id' => $redPacketId,
            'bag_amount' => 10,
            'auction_id' => $auctionId,
        );
        $margin_id = $model_margin_orders->addOrder($param);
        if (!$margin_id) {
            return false;
        }
        return true;
    }

    /**
     * 最新生成保证金订单
     * @param $auction_id int 拍品ID
     * @param $member_id int 会员ID
     * @param $payment_code string 支付方式代码
     * @param $address_id int 地址ID
     * @param $is_anonymous int 是否匿名
     * @param $order_from int 1PC，2移动
     * @param $other_arr array 其他信息
     * @return array
     * */
    public function new_create_margin_order (
        $auction_id, $member_id, $payment_code, $address_id, $is_anonymous, $order_from,$other_arr
    )
    {
        /** @var margin_ordersModel $model_margin_orders */
        /** @var addressModel $model_address */
        /** @var auction_member_relationModel $model_auction_member_relation */
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $model_address = Model('address');
        $model_auction_member_relation = Model('auction_member_relation');

        $this->set_basis_info($auction_id, $member_id);

        //判断新手拍品, 老用户不能参与
        if ($this->auction_info['auction_type'] == auctionsModel::AUCTION_TYPE_NEWBIE) {
            $checkNiebie = Logic('member')->isNewBie($member_id);
            if (!$checkNiebie) {
                \App\Exceptions\ApiResponseException::throwFailMsg('老用户不能参与新手拍卖');
            }
        }

        //联系地址
        if ($address_id) {// member_mobile
            $address_info = $model_address->getAddressInfo(array('address_id' => $address_id));
        }
        $order_sn = $this->make_order_sn();
        //红包ID 红包面额保存
        $redPacketId = $other_arr['redPacketId'] ?: 0;
        $redPacketPrice = 0;
        if (!empty($redPacketId)) {
            $packetWhere = ['rpacket_id' => $redPacketId];
            $redPacketInfo = Model('redpacket')->getRedpacketInfo($packetWhere);
            $redPacketPrice = $redPacketInfo['rpacket_price'] ?: 0;
        }

        //生成订单初始信息
        $param = array(
            'order_sn' => $order_sn,
            'store_id' => $this->store_info['store_id'],
            'store_name' => $this->store_info['store_name'],
            'buyer_id' => $this->member_info['member_id'],
            'buyer_name' => $this->member_info['member_name'],
            'buyer_phone' => $this->member_info['member_mobile'],
            'created_at' => time(),
            'updated_at' => time(),
            'margin_amount' => $other_arr['margin_amount']
                ?  $other_arr['margin_amount']
                : $this->auction_info['auction_bond'],
            'red_packet_id' => $redPacketId,
            'bag_amount' => $redPacketPrice,
            'order_state' => 0,
            'order_type' => $payment_code == 'online' ? 0 : 1,
            'address_id' => $address_id,
            'payment_code' => $payment_code,
            'auction_id' => $this->auction_info['auction_id'],
            'auction_name' => $this->auction_info['auction_name'],
            'auction_image' => $this->auction_info['auction_image'],
            'order_from' => $order_from,
        );
        if (!empty($address_info)){
            $param['buyer_phone'] = empty($address_info['mob_phone'])
                ? $address_info['tel_phone']
                : $address_info['mob_phone'];
            $param['area_info'] = $address_info['area_info'];
            $param['address'] =  $address_info['address'];
            $param['reciver_name'] = $address_info['true_name'];
        }

        try {

            $model_margin_orders->beginTransaction();
            if(empty($this->member_info['member_mobile'])){
                throw new Exception('请先绑定手机!');
            }

            $order = $model_margin_orders->where(array('buyer_id'=> $member_id,'created_at'=> array('egt',TIMESTAMP-3)))->find();//防止误操作,单个会员3秒内只能提交一个订单
            if (is_array($order) && !empty($order)) {
                throw new Exception('请勿多次提交订单');
            }
            //锁定当前会员记录
            $_info = Model('member')->table('member')->where(array('member_id'=> $member_id))
                ->master(true)->lock(true)->find();

            // 订单查询, 开拍前不能多次缴纳保证金
            $condition['buyer_id'] = $member_id;
            $condition['order_state'] = 1;
            $condition['auction_id'] = $auction_id;
            $condition['finnshed_time'] = ['lt', $this->auction_info['auction_start_time']];
            $exit_first = $model_margin_orders->where($condition)->count();
            if ($this->auction_info['auction_start_time'] > time()) {
                if ($exit_first) {
                    return callback(false, '拍卖开始前不能多次提交保证金');
                }
            }

            $margin_id = $model_margin_orders->addOrder($param);
            if (!$margin_id) {
                throw new Exception('生成订单失败');
            }
            // 检查是否存在拍品会员关系
            $relation_info = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
            if ($is_anonymous != 1) {
                $is_anonymous = 0;
            }
            if (empty($relation_info)) {
                // 为空创建关系
                $Insert = array(
                    'auction_id' => $auction_id,
                    'member_id' => $member_id,
                    'is_bond'=>2,
                    'is_anonymous' => $is_anonymous,
                    'member_name' => $this->member_info['member_name'],
                    'member_mobile' => $this->member_info['member_mobile'],
                );
                $result = $model_auction_member_relation->addRelation($Insert);
                if (empty($result)) {
                    throw new Exception('创建关系失败');
                }
            } else {
                // 不为空更新关系
                $update = array(
                    'is_anonymous' => $is_anonymous,
                    'member_name' => $this->member_info['member_name'],
                    'member_mobile' => $this->member_info['member_mobile'],
                    'is_bond'=>2
                );
                $condition = array(
                    'auction_id' => $auction_id,
                    'member_id' => $member_id,
                );
                $model_auction_member_relation->setRelationInfo($update, $condition);
            }

            $model_margin_orders->commit();
            return callback(true,'',array('order_sn' => $order_sn, 'payment_code' => $payment_code));
        } catch (Exception $e) {
            $model_margin_orders->rollback();
            return callback(false, $e->getMessage());
        }
    }

    public function new_create_picker_margin_order ($auction_info, $member_info) {
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $where = array('buyer_id'=> $member_info['member_id'],'auction_id'=> $auction_info['auction_id']);
        $order = $model_margin_orders->where($where)->find();//防止误操作,单个会员3秒内只能提交一个订单
        if (is_array($order) && !empty($order)) {
            return true;
        }
        $order_sn = $this->make_order_sn();
        //生成订单初始信息
        $param = array(
            'order_sn' => $order_sn,
            'store_id' => $auction_info['store_id'],
            'store_name' => $auction_info['store_name'],
            'buyer_id' => $member_info['member_id'],
            'buyer_name' => $member_info['member_name'],
            'buyer_phone' => $member_info['member_mobile'],
            'created_at' => time(),
            'updated_at' => time(),
            'margin_amount' => 0,
            'order_state' => 1,
            'order_type' => 0,
            'address_id' => 0,
            'payment_code' => 'online',
            'auction_id' => $auction_info['auction_id'],
            'auction_name' => $auction_info['auction_name'],
            'auction_image' => $auction_info['auction_image'],
            'order_from' => 2,
        );
        $margin_id = $model_margin_orders->addOrder($param);
        if (!$margin_id) {
            return false;
        }
        return true;
    }

    /**
     * 下单时更新拍品订单信息
     * @param $post int POST数据
     * @param $member_id int 会员ID
     * @return mixed
     * */
    public function update_auction_order($post, $member_id)
    {
        /** @var orderModel $model_order */
        $model_order = Model('order');
        try{
            $model_order->beginTransaction();
            $auction_order_id = $post['auction_order_id'];
            $pay_name = $post['pay_name'];
            // 查新发票信息
            // 是否开发票
            $input_if_vat = $this->buyDecrypt($post['vat_hash'], $member_id);
            if (!in_array($input_if_vat,array('allow_vat','deny_vat'))) {
                throw new Exception('订单保存出现异常[增值税发票出现错误]，请重试');
//                return callback(false,'订单保存出现异常[增值税发票出现错误]，请重试');
            }

            $input_if_vat = ($input_if_vat == 'allow_vat') ? true : false;

            //验证发票信息
            if (!empty($post['invoice_id'])) {
                $input_invoice_id = intval($post['invoice_id']);
                if ($input_invoice_id > 0) {
                    $input_invoice_info = Model('invoice')->getinvInfo(array('inv_id'=>$input_invoice_id));
                    if ($input_invoice_info['member_id'] != $member_id) {
                        throw new Exception('请正确填写发票信息');
//                        return callback(false, '请正确填写发票信息');
                    }
                }
            }
            // 获取发票信息
            $logic_buy_1 = Logic('buy_1');
            $invoice_info = $logic_buy_1->createInvoiceData($input_invoice_info);

            $input_address_id = intval($post['address_id']);
            if ($input_address_id < 0) {
                throw new Exception('请选择收货地址');
            } else {
                if ($input_address_id) {
                    /** @var addressModel $address_model */
                    $address_model = Model('address');
                    $input_address_info = $address_model->getAddressInfo(array('address_id'=>$input_address_id));

                    if ($input_address_info['member_id'] != $member_id) {
                        throw new Exception('请选择收货地址');
                    } else {
                        $input_address_info = Model('address')->getAddressInfo(array('address_id'=>$input_address_id));

                        if ($input_address_info['member_id'] != $member_id) {
                            throw new Exception('请选择收货地址');
                        }
                    }
                }
            }
            if ($input_address_id) {
                //收货地址城市编号
                $input_city_id = intval($input_address_info['city_id']);
                //收货人信息
                /** @var buy_1Logic $buy_1_logic */
                $buy_1_logic = Logic('buy_1');
                list($reciver_info,$reciver_name,$reciver_phone) = $buy_1_logic->getReciverAddr($input_address_info);

                $order_common = array(
                    'store_id'=>$post['store_id'],
                    'invoice_info'=>$invoice_info,
                    'reciver_info' => $reciver_info,
                    'reciver_name' => $reciver_name,
                    'reciver_city_id' => $input_city_id,
                    'daddress_id' => $input_address_id
                );
                $order_common_info = $model_order->getOrderCommonInfo(array('order_id'=>$auction_order_id));
                if(!empty($order_common_info)){
                    $re = $model_order->editOrderCommon($order_common,array('order_id'=>$auction_order_id));
                    if (!$re) {
                        throw new Exception('订单保存失败[未生成订单扩展数据]');
                    }
                }else{
                    $order_common['order_id'] = $auction_order_id;
                    $insert = $model_order->addOrderCommon($order_common);
                    if (!$insert) {
                        throw new Exception('订单保存失败[未生成订单扩展数据]');
                    }
                }
            }
            // 更新订单信息
            $condition = array(
                'order_id' => $auction_order_id
            );
            $order_info=Model("")->table("orders")->where($condition)->find();
            if ($order_info['margin_amount']>0) {
                $margin_amount=$order_info['margin_amount'];
            }else{
                $margin_order_model=Model("margin_orders");
                $margin_order_map['order_state']=1;
                $margin_order_map['refund_state']=0;
                $margin_order_map['lock_state']=1;
                $margin_order_map['buyer_id']=$order_info['buyer_id'];
                $margin_order_map['auction_order_id']=$auction_order_id;
                $margin_amount = $margin_order_model->where($margin_order_map)->sum("margin_amount");
            }
            if ($margin_amount>$order_info['order_amount']) {
                $refund_amount=$margin_amount-$order_info['order_amount'];
            }else{
                $refund_amount=0;                
            }

            $update = array(
                'payment_code' => $pay_name,
                'margin_amount' => $margin_amount,
                'refund_amount' => $refund_amount,
            );
            $result = $model_order->editOrder($update,$condition);
            if(!$result){
                throw new Exception('失败');
            }
            $model_order->commit();
            $auction_order_info = $model_order->getOrderInfo($condition);
            return callback(true, '', $auction_order_info);
        }catch(Exception $e){
            $model_order->rollback();
            return callback(false, $e->getMessage());
        }

//        if ($result) {
//            $auction_order_info = $model_order->getOrderInfo($condition);
//            return callback(true, '', $auction_order_info);
//        } else {
//            return callback(false, '订单更新失败！');
//        }
    }
    
    /**
     * 拉取要生成订单的基本对象信息
     *
     * @param $auction_id int 拍品ID
     * @param $member_id int 会员ID
     * */
    protected function set_basis_info($auction_id, $member_id)
    {
        $model_auctions = Model('auctions');
        $model_store = Model('store');
        $model_member = Model('member');
        // 获取拍品详细信息
        $this->auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_id));
        // 获取店铺详细信息
        $this->store_info = $model_store->getStoreInfo(array('store_id' => $this->auction_info['store_id']));
        if (empty($this->store_info)) {
            $this->store_info['store_id'] = 0;
            $this->store_info['store_name'] = $this->auction_info['store_name'] ?: '';
        }
        // 获取会员详细信息
        $this->member_info = $model_member->getMemberInfo(array('member_id' => $member_id));
    }

    /**
     * 生成订单号算法
     * */
    protected function make_order_sn()
    {
        $order_sn = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        return $order_sn;
    }

    /**
     * 加密
     * @param array/string $string
     * @param int $member_id
     * @return mixed arrray/string
     */
    public function buyEncrypt($string, $member_id)
    {
        $buy_key = sha1(md5($member_id.'&'.MD5_KEY));
        if (is_array($string)) {
            $string = serialize($string);
        } else {
            $string = strval($string);
        }
        return encrypt(base64_encode($string), $buy_key);
    }

    /**
     * 解密
     * @param string $string
     * @param int $member_id
     * @param int $ttl
     * @return string
     */
    public function buyDecrypt($string, $member_id, $ttl = 0)
    {
        $buy_key = sha1(md5($member_id.'&'.MD5_KEY));
        if (empty($string)) return;
        $string = base64_decode(decrypt(strval($string), $buy_key, $ttl));
        return ($tmp = @unserialize($string)) !== false ? $tmp : $string;
    }

    /**
     * 检查是否结束拍卖
     * @param $auction_end_true_t int
     * @return bool
     * */
    protected function auctionEndTime($auction_end_true_t)
    {
        if (empty($auction_end_true_t) || $auction_end_true_t > TIMESTAMP) {
            return false;
        }
        return true;
    }

    /**
     *
     * */
}
