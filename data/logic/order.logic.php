<?php
/**
 * 实物订单行为
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class orderLogic
{

    /**
     * 取消订单
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @param string $msg 操作备注
     * @param boolean $if_update_account 是否变更账户金额
     * @param array $cancel_condition 订单更新条件,目前只传入订单状态，防止并发下状态已经改变
     * @return array
     */
    public function changeOrderStateCancel($order_info, $role, $user = '', $msg = '', $if_update_account = true, $cancel_condition = array())
    {
        try {
            $model_order = Model('order');
            $model_order->beginTransaction();
            $order_id = $order_info['order_id'];
            $_info = $model_order->table('orders')->where(array('order_id' => $order_id))->master(true)->lock(true)->find();
            if ($_info['order_state'] == ORDER_STATE_CANCEL) {
                throw new Exception('参数错误');
            }

            //库存销量变更
            $goods_list = $model_order->getOrderGoodsList(array('order_id' => $order_id));
            $data = array();
            $goods_sale = array();
            foreach ($goods_list as $goods) {
                $data[$goods['goods_id']] = $goods['goods_num'];
                $goods_sale[$goods['goods_id']] = $goods['goods_commonid'];
                //添加作品售卖状态更改
                if (isset($goods['artwork_id']) && !empty($goods['artwork_id'])) {
                    //application()->artistService->syncArtworkStatus($goods['artwork_id'], 1, "");
                    $postData = [
                        'artwork_id' => $goods['artwork_id'],
                        'state' => 1
                    ];
                    $this->syncArtworkStatus($postData);
                }
            }
            $result = Logic('queue')->cancelOrderUpdateStorage($data, $goods_sale);
            if (!$result['state']) {
                throw new Exception('还原库存失败');
            }
            if ($order_info['chain_id']) {
                $result = Logic('queue')->cancelOrderUpdateChainStorage($data, $order_info['chain_id']);
                if (!$result['state']) {
                    throw new Exception('还原门店库存失败');
                }
            }

            if ($if_update_account) {
                $model_pd = Model('predeposit');
                //解冻充值卡
                $rcb_amount = floatval($order_info['rcb_amount']);
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changeRcb('order_cancel', $data_pd);
                }

                //解冻预存款
                $pd_amount = floatval($order_info['pd_amount']);
                if ($pd_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $pd_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changePd('order_cancel', $data_pd);
                }

                //解冻积分
                $points_amount = floatval($order_info['points_amount']);
                if ($points_amount > 0) {
                    $nuobi_points = $points_amount * 100;
                    Model('points')->savePointsLog('cancel', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                }

                //退换红包
                if ($order_info['rpt_amount']){
                    $where = [
                        'rpacket_order_id'=>$order_info['order_id'],
                        'rpacket_owner_id'=>$order_info['buyer_id']
                    ];
                    $data = ['rpacket_state'=>1,'rpacket_order_id'=> 0];
                    Model('redpacket')->editRedpacket($where, $data, $goods_list['buyer_id']);
                }

            }

            //更新订单信息
            $update_order = array('order_state' => ORDER_STATE_CANCEL);
            $cancel_condition['order_id'] = $order_id;
            $cancel_condition['order_state'] = array('neq', ORDER_STATE_CANCEL);
            $update = $model_order->editOrder($update_order, $cancel_condition);
            if (!$update) {
                throw new Exception('保存失败');
            }

            //添加订单日志
            $data = array();
            $data['order_id'] = $order_id;
            $data['log_role'] = $role;
            $data['log_msg'] = '取消了订单';
            $data['log_user'] = $user;
            if ($msg) {
                $data['log_msg'] .= ' ( ' . $msg . ' )';
            }
            $data['log_orderstate'] = ORDER_STATE_CANCEL;
            $model_order->addOrderLog($data);

            Model('voucher')->returnVoucher($order_info['order_id']);
            $model_order->commit();

            return callback(true, '操作成功');

        } catch (Exception $e) {
            $model_order->rollback();
            return callback(false, '操作失败');
        }
    }

    public function syncArtworkStatus($postData)
    {
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, YSJ_URL.'/api/artwork/editState');//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data, true);
        }
        return $data;
    }

    /**
     * 收货
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system,chain 分别代表买家、商家、管理员、系统、门店
     * @param string $user 操作人
     * @param string $msg 操作备注
     * @return array
     */
    public function changeOrderStateReceive($order_info, $role, $user = '', $msg = '')
    {
        try {

            $order_id = $order_info['order_id'];
            $model_order = Model('order');

            //更新订单状态
            $update_order = array();
            $update_order['finnshed_time'] = TIMESTAMP;
            $update_order['order_state'] = ORDER_STATE_SUCCESS;
            $update = $model_order->editOrder($update_order, array('order_id' => $order_id));
            if (!$update) {
                throw new Exception('保存失败');
            }

            //添加订单日志
            $data = array();
            $data['order_id'] = $order_id;
            $data['log_role'] = $role;
            $data['log_msg'] = $msg;
            $data['log_user'] = $user;
            $data['log_orderstate'] = ORDER_STATE_SUCCESS;
            $model_order->addOrderLog($data);

            if ($order_info['buyer_id'] > 0 && $order_info['order_amount'] > 0) {
                //添加会员积分
                if (C('points_isuse') == 1) {
                    Model('points')->savePointsLog('order', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'orderprice' => $order_info['order_amount'], 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                }
                // 获取趣猜赠送的诺币数-在中奖时给到用户
                /*if (!empty($order_info['gs_id'])) {
                    $guess_info = Model('p_guess')->getOpenGuessInfo(array('gs_id' => $order_info['gs_id']));
                    if (!empty($guess_info['points'])) {
                        Model('points')->savePointsLog('guess', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'orderprice' => $order_info['order_amount'], 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true, $guess_info['points']);
                    }
                }*/
                //添加会员经验值
                Model('exppoints')->saveExppointsLog('order', array('exp_memberid' => $order_info['buyer_id'], 'exp_membername' => $order_info['buyer_name'], 'orderprice' => $order_info['order_amount'], 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);

                //处理会员类型(完成订单将注册会员变为普通会员)
                //Model()->table('member')->where(array('member_type' => 0, 'member_id' => $order_info['buyer_id']))->update(array('member_type' => 1));
                //Model()->table('member_distribute')->where(array('member_type' => 0, 'member_id' => $order_info['buyer_id']))->update(array('member_type' => 1));

                //会员返佣处理
                $this->_commissionHandle($order_info);
            }

            // 发送用户消息
            $param = array();
            $param['code'] = 'receipt_success_notice';
            $param['member_id'] = $order_info['buyer_id'];
            $param['number'] = array('mobile' => $order_info['buyer_phone'], 'email' => $order_info['buyer_email']);
            $param['param'] = array();
            QueueClient::push('sendMemberMsg', $param);

            return callback(true, '操作成功');
        } catch (Exception $e) {
            return callback(false, '操作失败');
        }
    }

    /**
     * 更改运费
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @param float $price 运费
     * @return array
     */
    public function changeOrderShipPrice($order_info, $role, $user = '', $price)
    {
        try {

            $order_id = $order_info['order_id'];
            $model_order = Model('order');

            $data = array();
            $data['shipping_fee'] = abs(floatval($price));
            $data['order_amount'] = array('exp', 'goods_amount+' . $data['shipping_fee']);
            $update = $model_order->editOrder($data, array('order_id' => $order_id));
            if (!$update) {
                throw new Exception('保存失败');
            }
            //记录订单日志
            $data = array();
            $data['order_id'] = $order_id;
            $data['log_role'] = $role;
            $data['log_user'] = $user;
            $data['log_msg'] = '修改了运费' . '( ' . $price . ' )';;
            $data['log_orderstate'] = $order_info['payment_code'] == 'offline' ? ORDER_STATE_PAY : ORDER_STATE_NEW;
            $model_order->addOrderLog($data);
            return callback(true, '操作成功');
        } catch (Exception $e) {
            return callback(false, '操作失败');
        }
    }

    /**
     * 更改商品价格
     * @return [type] [description]
     */
    public function changeOrderGoodPrice($order_info, $role, $user = '', $post_param)
    {
        try {

            $order_id = $order_info['order_id'];
            $model_order = Model('order');
            $model_order->beginTransaction();
            $data = array();
            // 1.更改订单商品价格
            $change_price = ($post_param['new_goods_price'] - $post_param['old_goods_price']) * $post_param['goods_num'];
            $data['goods_amount'] = array('exp', 'goods_amount+' . $change_price);
            $data['order_amount'] = array('exp', 'order_amount+' . $change_price);
            $update = $model_order->editOrder($data, array('order_id' => $order_id));
            if (!$update) {
                throw new Exception('保存失败');
            }
            $data = array();
            $update = false;
            $data['goods_price'] = $post_param['new_goods_price'];
            $data['goods_pay_price'] = $post_param['goods_pay_price'] + ($post_param['new_goods_price'] - $post_param['old_goods_price']) * $post_param['goods_num'];
            $condition['order_id'] = $order_id;
            $condition['goods_id'] = $post_param['goods_id'];
            // 2.更改订单商品表价格
            $update = $model_order->editOrderGoods($data, $condition);
            if (!$update) {
                throw new Exception('保存失败');
            }

            //记录订单日志
            $data = array();
            $data['order_id'] = $order_id;
            $data['log_role'] = $role;
            $data['log_user'] = $user;
            $data['log_msg'] = '将订单' . $order_id . '的价格从(' . $post_param['old_goods_price'] . ')改为(' . $post_param['new_goods_price'] . ')';
            $data['log_orderstate'] = $order_info['payment_code'] == 'offline' ? ORDER_STATE_PAY : ORDER_STATE_NEW;
            $model_order->addOrderLog($data);
            $model_order->commit();
            return callback(true, '操作成功');
        } catch (Exception $e) {
            $model_order->rollback();
            return callback(false, $e->getMessage());
        }
    }

    /**
     * 回收站操作（放入回收站、还原、永久删除）
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $state_type 操作类型
     * @return array
     */
    public function changeOrderStateRecycle($order_info, $role, $state_type)
    {
        $order_id = $order_info['order_id'];
        $model_order = Model('order');
        //更新订单删除状态
        $state = str_replace(array('delete', 'drop', 'restore'), array(ORDER_DEL_STATE_DELETE, ORDER_DEL_STATE_DROP, ORDER_DEL_STATE_DEFAULT), $state_type);
        $update = $model_order->editOrder(array('delete_state' => $state), array('order_id' => $order_id));
        if (!$update) {
            return callback(false, '操作失败');
        } else {
            return callback(true, '操作成功');
        }
    }

    /**
     * 发货
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @return array
     */
    public function changeOrderSend($order_info, $role, $user = '', $post = array())
    {
        $order_id = $order_info['order_id'];
        $model_order = Model('order');
        try {
            $model_order->beginTransaction();
            $data = array();
            if (!empty($post['reciver_name'])) {
                $data['reciver_name'] = $post['reciver_name'];
            }
            if (!empty($post['reciver_info'])) {
                $data['reciver_info'] = $post['reciver_info'];
            }
            $data['deliver_explain'] = $post['deliver_explain'];
            $data['daddress_id'] = intval($post['daddress_id']);
            $data['shipping_express_id'] = intval($post['shipping_express_id']);
            $data['shipping_time'] = $shipping_time = TIMESTAMP;

            $condition = array();
            $condition['order_id'] = $order_id;
            $condition['store_id'] = $order_info['store_id'];
            $update = $model_order->editOrderCommon($data, $condition);
            if (!$update) {
                throw new Exception('操作失败');
            }

            $data = array();
            $data['shipping_code'] = $post['shipping_code'];
            $data['order_state'] = ORDER_STATE_SEND;
            $data['delay_time'] = TIMESTAMP;
            $update = $model_order->editOrder($data, $condition);
            if (!$update) {
                throw new Exception('操作失败');
            }
            $model_order->commit();
        } catch (Exception $e) {
            $model_order->rollback();
            return callback(false, $e->getMessage());
        }

        //更新表发货信息
        if ($post['shipping_express_id'] && $order_info['extend_order_common']['reciver_info']['dlyp']) {
            $data = array();
            $data['shipping_code'] = $post['shipping_code'];
            $data['order_sn'] = $order_info['order_sn'];
            $express_info = Model('express')->getExpressInfo(intval($post['shipping_express_id']));
            $data['express_code'] = $express_info['e_code'];
            $data['express_name'] = $express_info['e_name'];
            Model('delivery_order')->editDeliveryOrder($data, array('order_id' => $order_info['order_id']));
        }

        //添加订单日志
        $data = array();
        $data['order_id'] = $order_id;
        $data['log_role'] = 'seller';
        $data['log_user'] = $user;
        $data['log_msg'] = '发出货物(编辑信息)';
        $data['log_orderstate'] = ORDER_STATE_SEND;
        $model_order->addOrderLog($data);

        $goods_name = $order_info['extend_order_goods'][0]['goods_name'];
        if(strlen($order_info['extend_order_goods']) > 1){
            $goods_name .= '...';
        }
        // 发送买家消息
        $param = array();
        $param['code'] = 'order_deliver_success';
        $param['member_id'] = $order_info['buyer_id'];
        $param['param'] = array(
            'order_sn' => $order_info['order_sn'],
            'goods_name' => $goods_name,
            'shipping_time' => date('Y年m月d日 H:i', $shipping_time),
            'order_url' => urlShop('member_order', 'show_order', array('order_id' => $order_id))
        );
        QueueClient::push('sendMemberMsg', $param);

        return callback(true, '操作成功');
    }

    /**
     * 收到货款
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @return array
     */
    public function changeOrderReceivePay($order_list, $role, $user = '', $post = array())
    {
        $model_order = Model('order');
        $model_gs_offer = Model('guess_offer');
        $model_gs = Model('p_guess');
        try {
            $model_order->beginTransaction();
            $pay_info = $model_order->getOrderPayInfo(array('pay_sn' => $order_list[0]['pay_sn']));
            if ($pay_info) {
                if ($pay_info['api_pay_state'] == 1) {
                    return callback(true, '操作成功');
                }
                $pay_info = $model_order->getOrderPayInfo(array('pay_id' => $pay_info['pay_id']), true, true);
                if ($pay_info['api_pay_state'] == 1) {
                    return callback(true, '操作成功');
                }
            }
            $model_pd = Model('predeposit');
            foreach ($order_list as $order_info) {
                $order_id = $order_info['order_id'];
                if (!in_array($order_info['order_state'], array(ORDER_STATE_NEW, ORDER_STATE_UPLOAD_EVIDENCE)))
                    continue;
                //下单，支付被冻结的充值卡
                $rcb_amount = floatval($order_info['rcb_amount']);
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changeRcb('order_comb_pay', $data_pd);
                }
                // 趣猜订单更新相关信息
                if (!empty($order_info['gs_id'])) {
                    //1.修改趣猜中奖状态信息
                    $gs_state = $model_gs->editGuess(array('be_win' => 1), array('gs_id' => $order_info['gs_id']));
                    //2.修改用户中奖状态信息
                    $gs_offer_state = $model_gs_offer->update_guess_offer(array('gs_id' => $order_info['gs_id'], 'member_id' => $order_info['buyer_id']), array('gs_state' => 3));
                }

                //下单，支付被冻结的预存款
                $pd_amount = floatval($order_info['pd_amount']);
                if ($pd_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $pd_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changePd('order_comb_pay', $data_pd);
                }
                //下单，支付被冻结的积分
                $points_amount = floatval($order_info['points_amount']);
                if ($points_amount > 0) {
                    $nuobi_points = $points_amount * 100;
                    Model('points')->savePointsLog('pay_points', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                }

                //更新订单相关扩展信息
                $result = $this->_changeOrderReceivePayExtend($order_info, $post);
                if (!$result['state']) {
                    throw new Exception($result['msg']);
                }
                // 


                //添加订单日志
                $data = array();
                $data['order_id'] = $order_id;
                $data['log_role'] = $role;
                $data['log_user'] = $user;
                $data['log_msg'] = '收到货款(外部交易号:' . $post['trade_no'] . ')';
                $data['log_orderstate'] = ORDER_STATE_PAY;
                $insert = $model_order->addOrderLog($data);
                if (!$insert) {
                    throw new Exception('操作失败');
                }

                //更新订单状态
                $update_order = array();
                $update_order['order_state'] = ORDER_STATE_PAY;
                $update_order['payment_time'] = ($post['payment_time'] ? strtotime($post['payment_time']) : TIMESTAMP);
                $update_order['payment_code'] = $post['payment_code'];
                if ($post['trade_no'] != '') {
                    $update_order['trade_no'] = $post['trade_no'];
                }
                if($post['api_pay_amount'] > 0){
                    $update_order['api_pay_amount'] = $post['api_pay_amount'];
                }
                $condition = array();
                $condition['order_id'] = $order_info['order_id'];
                $condition['order_state'] = array('in', array(ORDER_STATE_NEW, ORDER_STATE_UPLOAD_EVIDENCE));
                $update = $model_order->editOrder($update_order, $condition);
                if (!$update) {
                    throw new Exception('操作失败');
                }
            }

            //更新支付单状态
            $data = array();
            $data['api_pay_state'] = 1;
            $update = $model_order->editOrderPay($data, array('pay_sn' => $order_info['pay_sn']));
            if (!$update) {
                throw new Exception('更新支付单状态失败');
            }
            $model_order->commit();
        } catch (Exception $e) {
            $model_order->rollback();
            return callback(false, $e->getMessage());
        }

        foreach ($order_list as $order_info) {

            $order_id = $order_info['order_id'];
            $condition = array('order_id' => $order_id);
            $goods_info = $model_order->getOrderInfo($condition,['order_goods']);
            $goods_name = '';
            foreach ($goods_info['extend_order_goods'] as $key=>$value) {
                $goods_name .= $value['goods_name'] . ' ';
            }
            if(mb_strlen($goods_name,'UTF8') > 10){
                $goods_name = mb_substr($goods_name,0,10,'UTF8') . '...';
            }
            //支付成功发送买家消息
            $param = array();
            $param['code'] = 'order_payment_success';
            $param['member_id'] = $order_info['buyer_id'];
            $param['param'] = array(
                'goods_name' => $goods_name,
                'order_amount' => $order_info['order_amount'],
                'order_sn' => $order_info['order_sn'],
                'order_url' => urlShop('member_order', 'show_order', array('order_id' => $order_info['order_id']))
            );
            QueueClient::push('sendMemberMsg', $param);

            //非预定订单下单或预定订单全部付款完成
            if ($order_info['order_type'] != 2 || $order_info['if_send_store_msg_pay_success']) {
                //支付成功发送店铺消息
                $param = array();
                $param['code'] = 'new_order';
                $param['store_id'] = $order_info['store_id'];
                $param['param'] = array('order_sn' => $order_info['order_sn']);
                QueueClient::push('sendStoreMsg', $param);
                //门店自提发送提货码
                if ($order_info['order_type'] == 3) {
                    $_code = rand(100000, 999999);
                    $result = $model_order->editOrder(array('chain_code' => $_code), array('order_id' => $order_info['order_id']));
                    if (!$result) {
                        throw new Exception('订单更新失败');
                    }
                    $param = array();
                    $param['chain_code'] = $_code;
                    $param['order_sn'] = $order_info['order_sn'];
                    $param['buyer_phone'] = $order_info['buyer_phone'];
                    QueueClient::push('sendChainCode', $param);
                }
            }
        }

        return callback(true, '操作成功');
    }


    /**
     * 上传支付凭证平台待确认
     */
    public function changeOrderStateInConfirm($pay_sn, $pay_voucher)
    {
        $model_order = Model('order');
        //更新订单删除状态
        $update = $model_order->editOrder(array('order_state' => ORDER_STATE_UPLOAD_EVIDENCE, 'payment_code' => 'underline', 'pay_voucher' => $pay_voucher), array('pay_sn' => $pay_sn));
        if (!$update) {
            return callback(false, '操作失败');
        } else {
            return callback(true, '操作成功');
        }
    }

    /**
     * 更新订单相关扩展信息
     * @param unknown $order_info
     * @return unknown
     */
    private function _changeOrderReceivePayExtend($order_info, $post)
    {
        //预定订单收款
        if ($order_info['order_type'] == 2) {
            $result = Logic('order_book')->changeBookOrderReceivePay($order_info, $post);
        }
        if ($order_info['order_type'] == 4) {//拼团订单
            $model_pintuan = Model('p_pintuan');
            $model_pintuan->payOrder($order_info);
        }
        return callback(true);
    }

    /**
     * 买家订单详细
     */
    public function getMemberOrderInfo($order_id, $member_id = 'unset')
    {
        $order_id = intval($order_id);
        if($member_id !== 'unset'){
            $member_id = intval($member_id);
        }
        if ($order_id <= 0) {
            return callback(false, '订单不存在');
        }

        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        if($member_id !== 'unset') {
            $condition['buyer_id'] = $member_id;
        }
        $order_info = $model_order->getOrderInfo($condition, array('order_goods', 'order_common', 'store'));
        if (empty($order_info) || $order_info['delete_state'] == ORDER_DEL_STATE_DROP) {
            return callback(false, '订单不存在');
        }

        $model_refund_return = Model('refund_return');
        $order_list = array();
        $order_list[$order_id] = $order_info;
        $order_list = $model_refund_return->getGoodsRefundList($order_list, 1);//订单商品的退款退货显示
        $order_info = $order_list[$order_id];
        $refund_all = $order_info['refund_list'][0];
        if (!empty($refund_all) && $refund_all['seller_state'] < 3) {//订单全部退款商家审核状态:1为待审核,2为同意,3为不同意
        } else {
            $refund_all = array();
        }

        //显示锁定中
        $order_info['if_lock'] = $model_order->getOrderOperateState('lock', $order_info);

        //显示取消订单
        $order_info['if_buyer_cancel'] = $model_order->getOrderOperateState('buyer_cancel', $order_info);

        //显示退款取消订单
        $order_info['if_refund_cancel'] = $model_order->getOrderOperateState('refund_cancel', $order_info);

        //显示投诉
        $order_info['if_complain'] = $model_order->getOrderOperateState('complain', $order_info);

        //显示收货
        $order_info['if_receive'] = $model_order->getOrderOperateState('receive', $order_info);

        //显示物流跟踪
        $order_info['if_deliver'] = $model_order->getOrderOperateState('deliver', $order_info);

        //显示评价
        $order_info['if_evaluation'] = $model_order->getOrderOperateState('evaluation', $order_info);

        //显示分享
        $order_info['if_share'] = $model_order->getOrderOperateState('share', $order_info);

        //显示系统自动取消订单日期
        if ($order_info['order_state'] == ORDER_STATE_NEW) {
            $order_info['order_cancel_day'] = $order_info['add_time'] + ORDER_AUTO_CANCEL_TIME * 3600;
        }

        //显示快递信息
        if ($order_info['shipping_code'] != '') {
            $express = rkcache('express', true);
            $order_info['express_info']['e_code'] = $express[$order_info['extend_order_common']['shipping_express_id']]['e_code'];
            $order_info['express_info']['e_name'] = $express[$order_info['extend_order_common']['shipping_express_id']]['e_name'];
            $order_info['express_info']['e_url'] = $express[$order_info['extend_order_common']['shipping_express_id']]['e_url'];
        }

        //显示系统自动收获时间
        if ($order_info['order_state'] == ORDER_STATE_SEND) {
            $order_info['order_confirm_day'] = $order_info['delay_time'] + ORDER_AUTO_RECEIVE_DAY * 24 * 3600;
        }

        //如果订单已取消，取得取消原因、时间，操作人
        if ($order_info['order_state'] == ORDER_STATE_CANCEL) {
            $order_info['close_info'] = $model_order->getOrderLogInfo(array('order_id' => $order_info['order_id']), 'log_id desc');
        }
        //查询消费者保障服务
        if (C('contract_allow') == 1) {
            $contract_item = Model('contract')->getContractItemByCache();
        }
        foreach ($order_info['extend_order_goods'] as $value) {
            $value['image_60_url'] = cthumb($value['goods_image'], 60, $value['store_id']);
            $value['image_240_url'] = cthumb($value['goods_image'], 240, $value['store_id']);
            $value['goods_type_cn'] = orderGoodsType($value['goods_type']);
            $value['goods_url'] = urlShop('goods', 'index', array('goods_id' => $value['goods_id']));
            $value['refund'] = $value['refund'] ? 1 : 0;
            //处理消费者保障服务
            if (trim($value['goods_contractid']) && $contract_item) {
                $goods_contractid_arr = explode(',', $value['goods_contractid']);
                foreach ((array)$goods_contractid_arr as $gcti_v) {
                    $value['contractlist'][] = $contract_item[$gcti_v];
                }
            }
            if ($value['goods_type'] == 5) {
                $order_info['zengpin_list'][] = $value;
            } else {
                $order_info['goods_list'][] = $value;
            }
        }

        if (empty($order_info['zengpin_list'])) {
            $order_info['zengpin_list'] = array();
            $order_info['goods_count'] = count($order_info['goods_list']);
        } else {
            $order_info['goods_count'] = count($order_info['goods_list']) + 1;
        }

        //取得其它订单类型的信息
        $model_order->getOrderExtendInfo($order_info);

        //卖家发货信息
        if (!empty($order_info['extend_order_common']['daddress_id'])) {
            $daddress_info = Model('daddress')->getAddressInfo(array('address_id' => $order_info['extend_order_common']['daddress_id']));
        } else {
            $daddress_info = array();
        }
        return callback(true, '', array('order_info' => $order_info, 'refund_all' => $refund_all, 'daddress_info' => $daddress_info));
    }

    /**
     * 会员返佣处理
     * @param array $order_info
     */
    public function _commissionHandle($order_info = array())
    {
        if(!empty($order_info)){
            //获取订单商品信息
            $order_goods_commis = $this->_getOrderCommis($order_info);
            //获取商品返佣比例
            //$goods_model = Model('goods');
            //$goods_list = $goods_model->getGoodsList(array('goods_id' => array('in', array_keys($order_goods_commis))), 'goods_id,commis_level_1,commis_level_2');

            /** @var j_member_distributeLogic $member_distribute_logic */
            $member_distribute_logic = Logic('j_member_distribute');

            //处理购买特定专区店铺产品
            if($order_info['goods_amount'] == $order_info['order_amount'] && $order_info['points_amount'] == 0 && $order_info['rpt_amount'] == 0){
                $member_distribute_logic->td_goods_process($order_goods_commis);
            }

            //获取分销上级
            $member_relation_info = $member_distribute_logic->get_top_member($order_info['buyer_id']);

            $distribute_list = $member_distribute_logic->get_distribution_info($member_relation_info['member_id']);
            if(!empty($distribute_list)){
                foreach($distribute_list as $v){
                    if($v['goods_num'] > 0){
                        $member_relation = array(
                            'from_id' => $member_relation_info['member_id'],
                            'from_name' => $member_relation_info['member_name'],
                            'to_id' => $v['member_id'],
                            'to_name' => $v['member_name'],
                            'top_lv' => $v['level']
                        );
                        $this->_CommisHandle($member_relation,$order_goods_commis,$v['goods_num']);
                    }
                }
            }

            //区域代理佣金记录
            $agent_distribute_info = $member_distribute_logic->get_agent_info($order_info['buyer_id']);
            if(!empty($agent_distribute_info)){
                foreach($agent_distribute_info as $v){
                    if($v['goods_num'] > 0){
                        $member_distribute_logic->agent_goods_commission($v,$order_goods_commis,1);
                    }
                }
            }

        }
    }

    /**
     * 卖家类型处理
     * @param int $buyer_id
     * @param int $store_id
     */
    private function _sellerHandle($buyer_id = 0,$store_id = 0)
    {
        $return = array();
        $return['is_artist'] = false;
        $return['is_bind'] = false;
        $return['top_member'] = array();
        if ($store_id > 0 && $buyer_id > 0) {
            $store_model = Model('store');
            $member_model = Model('member');

            $store_info = $store_model->getStoreInfoByID($store_id); //卖家店铺信息
            $s_member_info = $member_model->getMemberInfoByID($store_info['member_id']); //卖家会员信息

            $b_member_info = $member_model->getUpperMember(array('member_id' => $buyer_id)); //买家分销信息
            //通用返佣用户关系处理
            if($b_member_info['top_member'] > 0){
                $return['top_member'][0]['from_id'] = $buyer_id;
                $return['top_member'][0]['from_name'] = $b_member_info['member_name'];
                $return['top_member'][0]['to_id'] = $b_member_info['top_member'];
                $return['top_member'][0]['to_name'] = $b_member_info['top_member_name'];
                //获取买家上级会员信息
                $supper_member_info = $member_model->getUpperMember(array('member_id'=>$b_member_info['top_member']));
                if($supper_member_info['top_member'] > 0){
                    $top_member_info = $member_model->getUpperMember(array('member_id'=>$supper_member_info['top_member']));
                    if(in_array($top_member_info['member_type'],array('2','3'))){
                        $return['top_member'][1]['from_id'] = $buyer_id;
                        $return['top_member'][1]['from_name'] = $b_member_info['member_name'];
                        $return['top_member'][1]['to_id'] = $top_member_info['member_id'];
                        $return['top_member'][1]['to_name'] = $top_member_info['member_name'];
                    }
                }
            }
            //绑定特级会员艺术家返佣用户关系处理
            if ($store_info['store_type'] == 1) {
                $return['is_artist'] = true;
                if (intval($s_member_info['bind_member_id']) > 0) { //已绑定特级会员
                    $return['is_bind'] = true;
                    $return['top_member'][0]['from_id'] = $s_member_info['member_id'];
                    $return['top_member'][0]['from_name'] = $s_member_info['member_name'];
                    $return['top_member'][0]['to_id'] = $s_member_info['bind_member_id'];
                    $return['top_member'][0]['to_name'] = $s_member_info['bind_member_name'];
                    if(isset($return['top_member'][1])) unset($return['top_member'][1]);
                }
            }
        }
        return $return;
    }

    /**
     * 处理艺术家商家返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    private function _artistSellerHandle($member_relation = array(),$order_goods_commis = array(),$goods_list = array()){
        //整理并保存返佣信息
        $insert_data = array();
        foreach($order_goods_commis as $key => $commis){
            if($goods_list[$key]['commis_level_1'] <= 0){
                continue;
            }
            $insert_data[$key]['order_id'] = $commis['order_id'];
            $insert_data[$key]['order_sn'] = $commis['order_sn'];
            $insert_data[$key]['order_goods_id'] = $commis['order_goods_id'];
            $insert_data[$key]['goods_id'] = $commis['goods_id'];
            $insert_data[$key]['goods_name'] = $commis['goods_name'];
            $insert_data[$key]['goods_image'] = $commis['goods_image'];
            $insert_data[$key]['goods_pay_amount'] = $commis['goods_pay_amount'];
            $insert_data[$key]['from_member_id'] = $member_relation[0]['from_id'];
            $insert_data[$key]['from_member_name'] = $member_relation[0]['from_name'];
            $insert_data[$key]['commission_amount'] = $commis['goods_commission'] * $goods_list[$key]['commis_level_1'];
            $insert_data[$key]['dis_commis_rate'] = $goods_list[$key]['commis_level_1'] * 100;
            $insert_data[$key]['dis_member_id'] = $member_relation[0]['to_id'];
            $insert_data[$key]['dis_member_name'] = $member_relation[0]['to_name'];
            $insert_data[$key]['add_time'] = time();
            $insert_data[$key]['dis_commis_state'] = 0;
            $insert_data[$key]['commission_type'] = 1;
        }
        if(!empty($insert_data)){
            $res = Model()->table('member_commission')->insertAll($insert_data);
            if($res){
                foreach($insert_data as $member){
                    Model()->table('member')->where(array('member_id'=>$member['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$member['commission_amount'])));
                }
            }
        }
    }

    /**
     * 处理普通返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    private function _memberCommisHandle($member_relation = array(),$order_goods_commis = array(),$goods_list = array()){
        //整理并保存返佣信息
        $insert_data = array();
        $i = 0;
        foreach((array)$member_relation as $key => $relation){
            foreach($order_goods_commis as $k => $commis){
                if($key == 0 && $goods_list[$k]['commis_level_1'] <= 0 || $key == 1 && $goods_list[$k]['commis_level_2'] <= 0){
                    continue;
                }
                $tmp_data = array();
                $tmp_data['order_id'] = $commis['order_id'];
                $tmp_data['order_sn'] = $commis['order_sn'];
                $tmp_data['order_goods_id'] = $commis['order_goods_id'];
                $tmp_data['goods_id'] = $commis['goods_id'];
                $tmp_data['goods_name'] = $commis['goods_name'];
                $tmp_data['goods_image'] = $commis['goods_image'];
                $tmp_data['goods_pay_amount'] = $commis['goods_pay_amount'];
                $tmp_data['from_member_id'] = $relation['from_id'];
                $tmp_data['from_member_name'] = $relation['from_name'];
                $tmp_data['commission_amount'] = $commis['goods_commission'] * $goods_list[$k]['commis_level_1'];
                $tmp_data['dis_commis_rate'] = $goods_list[$k]['commis_level_1'] * 100;
                if($key == 1){
                    $tmp_data['commission_amount'] = $commis['goods_commission'] * $goods_list[$k]['commis_level_2'];
                    $tmp_data['dis_commis_rate'] = $goods_list[$k]['commis_level_2'] * 100;
                }
                $tmp_data['dis_member_id'] = $relation['to_id'];
                $tmp_data['dis_member_name'] = $relation['to_name'];
                $tmp_data['add_time'] = time();
                $tmp_data['dis_commis_state'] = 0;
                $tmp_data['commission_type'] = 1;
                $insert_data[$i] = $tmp_data;
                $i++;
            }
        }
        if(!empty($insert_data)){
            $res = Model()->table('member_commission')->insertAll($insert_data);
            if($res){
                foreach($insert_data as $member){
                    Model()->table('member')->where(array('member_id'=>$member['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$member['commission_amount'])));
                }
            }
        }
    }


    /**
     * 获取订单商品佣金详情
     * @param array $order_id
     */
    private function _getOrderCommis($order_info = array())
    {
        $commis_amount = array();
        if (!empty($order_info)) {
            $order_id = $order_info['order_id'];
            $order_model = Model('order');
            $order_detail = (array)$order_model->getOrderGoodsList(array('order_id' => $order_id));
            foreach ($order_detail as $goods) {
                $commis_amount[$goods['goods_id']]['order_id'] = $order_id;
                $commis_amount[$goods['goods_id']]['order_sn'] = $order_info['order_sn'];
                $commis_amount[$goods['goods_id']]['order_goods_id'] = $goods['rec_id'];
                $commis_amount[$goods['goods_id']]['goods_id'] = $goods['goods_id'];
                $commis_amount[$goods['goods_id']]['goods_name'] = $goods['goods_name'];
                $commis_amount[$goods['goods_id']]['goods_image'] = $goods['goods_image'];
                $commis_amount[$goods['goods_id']]['goods_pay_amount'] = $goods['goods_pay_price'];
                //$commis_amount[$goods['goods_id']]['goods_commission'] = $goods['goods_pay_price'] * ($goods['commis_rate']*1.0/100);
                $commis_amount[$goods['goods_id']]['goods_commission'] = $goods['goods_pay_price'];
                $commis_amount[$goods['goods_id']]['store_id'] = $goods['store_id'];
                $commis_amount[$goods['goods_id']]['buyer_id'] = $goods['buyer_id'];
            }
        }

        return $commis_amount;
    }




    /**
     * 处理艺术家商家返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    public function _CommisHandle($member_relation,$order_goods_commis = array(),$top_member_number = 0){

        if($top_member_number <= 0){
            return true;
        }
        //整理并保存返佣信息
        $insert_data = array();

        foreach($order_goods_commis as $key => $commis){
            $data = array();
            $data['order_id'] = $commis['order_id'];
            $data['order_sn'] = $commis['order_sn'];
            $data['order_goods_id'] = $commis['order_goods_id'];
            $data['goods_id'] = $commis['goods_id'];
            $data['goods_name'] = $commis['goods_name'];
            $data['goods_image'] = $commis['goods_image'];
            $data['goods_pay_amount'] = $commis['goods_pay_amount'];
            $data['from_member_id'] = $member_relation['from_id'];
            $data['from_member_name'] = $member_relation['from_name'];
            $data['commission_amount'] = $commis['goods_commission'] * ($top_member_number * 1.0 / 100);
            $data['dis_commis_rate'] = $top_member_number;
            $data['dis_member_id'] = $member_relation['to_id'];
            $data['dis_member_name'] = $member_relation['to_name'];
            $data['add_time'] = time();
            $data['dis_commis_state'] = 0;
            $data['commission_type'] = 1;
            $data['top_lv'] = $member_relation['top_lv'];
            $insert_data[] = $data;
//            file_put_contents(BASE_DATA_PATH . '/log/test.log',json_encode($insert_data)."\r\n", FILE_APPEND);
        }
        if(!empty($insert_data)){
            $res = Model()->table('member_commission')->insertAll($insert_data);
            if($res){
                foreach($insert_data as $member){
                    Model()->table('member')->where(array('member_id'=>$member['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$member['commission_amount'])));
                }
                $param = array();
                $param['code'] = 'fanyong_notice';
                $param['member_id'] = $member['dis_member_id'];
                $param['number']['mobile'] = $member['member_mobile'];
                $param['param'] = array();
                //QueueClient::push('sendMemberMsg', $param);

            }
        }


    }

    public function getMemberOrderList($member_id){
        $model_order = Model('order');

        //搜索
        $condition = array();
        $condition['buyer_id'] = $member_id;
        if (preg_match('/^\d{10,20}$/',$_GET['keyword'])) {
            $condition['order_sn'] = $_GET['keyword'];
        } elseif ($_GET['keyword'] != '') {
            $condition['order_id'] = array('in',$this->_getOrderIdByKeyword($_GET['keyword']));
        }
        if (preg_match('/^\d{10,20}$/',$_GET['pay_sn'])) {
            $condition['pay_sn'] = $_GET['pay_sn'];
        }
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']): null;
        if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
        if ($_GET['state_type'] != '') {
            $condition['order_state'] = str_replace(
                    array('state_new','state_pay','state_send','state_success','state_noeval','state_cancel'),
                    array(ORDER_STATE_NEW,ORDER_STATE_PAY,ORDER_STATE_SEND,ORDER_STATE_SUCCESS,ORDER_STATE_SUCCESS,ORDER_STATE_CANCEL), $_GET['state_type']);
        }
        if ($_GET['state_type'] == 'state_new') {
            $condition['chain_code'] = 0;
        }
        if ($_GET['state_type'] == 'state_noeval') {
            $condition['evaluation_state'] = 0;
            $condition['order_state'] = ORDER_STATE_SUCCESS;
        }
        if ($_GET['state_type'] == 'state_notakes') {
            $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
            $condition['chain_code'] = array('gt',0);
        }

        //回收站
        if ($_GET['recycle']) {
            $condition['delete_state'] = 1;
        } else {
            $condition['delete_state'] = 0;
        }
        $order_list = $model_order->getOrderList($condition, 20, '*', 'order_id desc','', array('order_common','order_goods','store'));
        $model_refund_return = Model('refund_return');
        $order_list = $model_refund_return->getGoodsRefundList($order_list,1);//订单商品的退款退货显示
        foreach($order_list as &$v){
            $v['goods_name_str'] = '';
            $v['goods_num'] = 0;
            foreach($v['extend_order_goods'] as $goods_info){
                if($v['goods_name_str'] != ''){
                    $v['goods_name_str'] .= ',';
                }
                $v['goods_name_str'] .= $goods_info['goods_name'];
                $v['goods_num'] += $goods_info['goods_num'];
            }
        }
        return $order_list;
    }

    /**
     * 创建拉卡拉关联订单
     * @param $pay_sn
     * @param $lkl_order_sn
     */
    public function createLklOrder($pay_sn,$lkl_order_sn){
        $param = array(
            'pay_sn'=>$pay_sn,
            'lkl_order_sn'=>$lkl_order_sn,
            'cdate'=>date('Y-m-d H:i:s')
        );
        Model('lkl_order')->createOrder($param);
    }


}
