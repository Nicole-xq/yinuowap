<?php
/**
 * 拍卖订单操作
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/24 0024
 * Time: 17:11
 * @author ADKi
 */

defined('InShopNC') or exit('Access Invalid!');

class auction_orderLogic
{
    protected $order_info;
    /**
     * 支付和支付回调时验证订单信息
     * @param $order_sn int 订单号
     * @return array
     * */
    public function VerifyMarginOrderPay($order_sn)
    {
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));

        if (empty($order_info)) {
            return callback(false, '该订单不存在');
        }

        if ($order_info['order_type'] == 1) {
            return callback(false, '该订单是线下支付订单');
        }

        if ($order_info['order_state'] == 1) {
            return callback(false, '该订单已支付');
        }
        /** @var auctionsModel $auctions */
        $auctions = Model('auctions');
        $auction_info = $auctions->getAuctionsInfo(array('auction_id'=>$order_info['auction_id']));
        if($auction_info['auction_end_time'] < time()){
            return callback(false, '拍卖已结束');
        }

        return callback(true, '', $order_info);
    }

    /**
     * 支付和支付回调时验证订单信息
     * @param $order_sn int 订单号
     * @return array
     * */
    public function VerifyAuctionOrderPay($order_sn)
    {
        $model_auction_order = Model('order');
        $order_info = $model_auction_order->getOrderInfo(array('order_sn' => $order_sn));
        if (empty($order_info)) {
            callback(false, '该订单不存在');
        }
        if ($order_info['order_state'] != 10) {
            callback(false, '订单状态异常');
        }
        return callback(true,'',$order_info);
    }

    /**
     * 上传支付凭证平台待确认
     * @param $type int 0 保证金 1 拍卖尾款
     */
    public function changeOrderStateInConfirm($pay_sn, $pay_voucher, $type = 0)
    {
        if ($type == 0) {
            $model_margin_orders = Model('margin_orders');
            //更新订单待审核状态
            $update_arr = array('order_state' => 2, 'payment_code' => 'underline', 'pay_voucher' => $pay_voucher, 'order_type' => 1,);
            $update = $model_margin_orders->editOrder($update_arr, array('order_sn' => $pay_sn));
        } elseif($type == 1) {
            $model_order = Model('order');
            $update_arr = array('order_state' => 50, 'payment_code' => 'underline', 'pay_voucher' => $pay_voucher,);
            $update = $model_order->editOrder($update_arr,array('order_sn' => $pay_sn));
        }
        if (!$update) {
            return callback(false, '操作失败');
        } else {
            return callback(true, '操作成功');
        }
    }

    /**
     * 取消保证金订单
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @param string $msg 操作备注
     * @param boolean $if_update_account 是否变更账户金额
     * @param array $cancel_condition 订单更新条件,目前只传入订单状态，防止并发下状态已经改变
     * @return array
     */
    public function changeOrderStateCancel($order_info, $role, $user = '', $msg = '', $if_update_account = true, $cancel_condition = array())
    {
        try {
            $model_margin_orders = Model('margin_orders');
            $model_margin_orders->beginTransaction();
            $order_sn = $order_info['order_sn'];
            $_info = $model_margin_orders->where(array('order_sn' => $order_sn))->master(true)->lock(true)->find();

            if ($_info['order_state'] == 4) {
                throw new Exception('参数错误');
            }

            if ($if_update_account) {
                $model_pd = Model('predeposit');
                //解冻充值卡
                $rcb_amount = floatval($order_info['rcb_amount']);
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changeRcb('order_cancel', $data_pd);
                }

                //解冻预存款
                $pd_amount = floatval($order_info['pd_amount']);
                if ($pd_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $order_info['buyer_id'];
                    $data_pd['member_name'] = $order_info['buyer_name'];
                    $data_pd['amount'] = $pd_amount;
                    $data_pd['order_sn'] = $order_info['order_sn'];
                    $model_pd->changePd('order_cancel', $data_pd);
                }

                //解冻积分
                $points_amount = floatval($order_info['points_amount']);
                if ($points_amount > 0) {
                    $nuobi_points = $points_amount * 100;
                    Model('points')->savePointsLog('cancel', array('pl_memberid' => $_SESSION['member_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                }

                //解冻保证金
                $account_margin_amount = floatval($order_info['account_margin_amount']);
                if ($account_margin_amount) {
                    $data_ma = array();
                    $data_ma['member_id'] = $order_info['buyer_id'];
                    $data_ma['member_name'] = $order_info['buyer_name'];
                    $data_ma['amount'] = $account_margin_amount;
                    $data_ma['order_sn'] = $order_info['order_sn'];
                    $this->MarginRefund($data_ma, 'refund');
                }
            }

            //更新订单信息
            $update_order = array('order_state' => 4);
            $cancel_condition['order_sn'] = $order_sn;
            $update = $model_margin_orders->editOrder($update_order, $cancel_condition);
            if (!$update) {
                throw new Exception('保存失败');
            }
            Model('auction_member_relation')->setRelationInfo(array('is_bond'=>0),array('member_id'=>$order_info['buyer_id'],'auction_id'=>$order_info['auction_id']));

            $model_margin_orders->commit();

            return callback(true, '操作成功');

        } catch (Exception $e) {
            $model_margin_orders->rollback();
            return callback(false, '操作失败');
        }
    }

    /**
     * 收到保证金货款
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @return array
     */
    public function changeOrderReceivePay($order_info, $role, $user = '', $post = array())
    {
        $model_margin_orders = Model('margin_orders');
        try {
            $model_margin_orders->beginTransaction();

            $model_pd = Model('predeposit');

            $margin_id = $order_info['margin_id'];
            if (!in_array($order_info['order_state'], array(0, 2, 3))) {
                throw new Exception('订单状态不允许收款');
            }

            //下单，支付被冻结的充值卡
            $rcb_amount = floatval($order_info['rcb_amount']);
            if ($rcb_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $order_info['buyer_id'];
                $data_pd['member_name'] = $order_info['buyer_name'];
                $data_pd['amount'] = $rcb_amount;
                $data_pd['order_sn'] = $order_info['order_sn'];
                $model_pd->changeRcb('order_comb_pay', $data_pd);
            }

            //下单，支付被冻结的预存款
            $pd_amount = floatval($order_info['pd_amount']);
            if ($pd_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $order_info['buyer_id'];
                $data_pd['member_name'] = $order_info['buyer_name'];
                $data_pd['amount'] = $pd_amount;
                $data_pd['order_sn'] = $order_info['order_sn'];
                $model_pd->changePd('order_comb_pay', $data_pd);
            }
            //下单，支付被冻结的积分
            $points_amount = floatval($order_info['points_amount']);
            if ($points_amount > 0) {
                $nuobi_points = $points_amount * 100;
                Model('points')->savePointsLog(
                    'pay_points',
                    array(
                        'pl_memberid' => $_SESSION['member_id'],
                        'pl_membername' => $order_info['buyer_name'],
                        'pl_points' => $nuobi_points,
                        'order_sn' => $order_info['order_sn'],
                        'order_id' => $order_info['order_id']
                    ),
                    true
                );
            }

            //添加订单日志
            $data = array();
            $data['order_id'] = $margin_id;
            $data['log_role'] = $role;
            $data['log_user'] = $user;
            $data['log_msg'] = '收到保证金货款(外部交易号:' . $post['trade_no'] . ')，保证金订单号：'.$order_info['order_sn'];
            $data['log_orderstate'] = ORDER_STATE_PAY;
            $insert = Model('order')->addOrderLog($data);
            if (!$insert) {
                throw new Exception('操作失败');
            }

            //更新订单状态
            $update_order = array();
            $update_order['order_state'] = 1;
            $update_order['payment_time'] = ($post['payment_time'] ? strtotime($post['payment_time']) : TIMESTAMP);
            $update_order['payment_code'] = $post['payment_code'];
            $update_order['api_pay_amount'] = bcsub($order_info['margin_amount'],
                bcadd(bcadd(bcadd(bcadd($order_info['pd_amount'], $order_info['rcb_amount'], 2), $order_info['points_amount'], 2) , $order_info['account_margin_amount'], 2), $order_info['bag_amount'], 2), 2);
            if ($post['trade_no'] != '') {
                $update_order['trade_no'] = $post['trade_no'];
            }
            $update_order['api_pay_state'] = 1;
            $condition = array();
            $condition['margin_id'] = $order_info['margin_id'];
            $condition['order_state'] = array('in', array(0, 2, 3));
            $update = $model_margin_orders->editOrder($update_order, $condition);
            if (!$update) {
                throw new Exception('操作失败');
            }else{
                $margin_info = Model('margin_orders')->getOrderInfo(array('margin_id'=>$order_info['margin_id']));
                $condition = array(
                    'buyer_id'=>$order_info['buyer_id'],
                    'order_state'=>array('neq','4'),
                    'margin_id'=>array('neq',$order_info['margin_id'])
                );
                /** @var margin_ordersModel $margin_orders */
                $margin_orders = Model('margin_orders');
                $tmp_re = $margin_orders->getOrderInfo($condition);
                $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);
                if(($auction_info['auction_type'] != 1 || empty($tmp_re))
                    && TIMESTAMP < strtotime($auction_info['interest_last_date'])) {
                    //已经交过保证金的用户，新手拍品不享受收益
                    /** @var auctionLogic $auction_logic */
                    $auction_logic = Logic('auction');
                    $day_num = $auction_logic->getInterestDay($margin_info['payment_time'],$auction_info['auction_start_time']);//计息天数

                    /** @var j_member_distributeLogic $member_distribute_logic */
                    $member_distribute_logic = Logic('j_member_distribute');
                    //添加佣金待结算记录
                    $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);

                    //添加区域代理佣金待结算记录
                    $member_distribute_logic->agent_margin_refund($margin_info, $day_num,0);
                }
                $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $order_info['auction_id'];
                $url = filterUrl($url);
                $url = sinaShortenUrl($url);
                /** @var margin_ordersModel $model_margin_order */
                $model_margin_order = Model('margin_orders');
                //已交保证金金额
                $getMarginSum=$model_margin_order->getMarginSum(
                    [
                        'auction_id' => $order_info['auction_id'],
                        'buyer_id' => $order_info['buyer_id'],
                        'order_state' => 1
                    ],
                    'margin_amount'
                );
                $margin_count = $getMarginSum ?: 0;
                $param = [
                    'code'      =>'aucution_margin_pay',
                    'member_id' =>$order_info['buyer_id'],
                    'number'    =>['mobile'=>$order_info['buyer_phone']],
                    'param'     =>[
                        'wx_first'=>'保证金支付成功',
                        'wx_remark'=>'请及时关注拍卖动态',
                        'auction_name'=>$order_info['auction_name'],
                        'margin_amount'=>$order_info['margin_amount'],
                        'money'=>$order_info['margin_amount'],
                        'margin_count' => $margin_count,
                        'url' => $url,
                        'wx_url' => $url
                    ],
                ];
                QueueClient::push('sendMemberMsg', $param);
            }

            // 更新会员和拍品关系，报名人数 +1 增加除了保证金账户支付金额部分以外的保证金冻结金额
            $this->auction_member_bond($order_info);

            $model_margin_orders->commit();
        } catch (Exception $e) {
            $model_margin_orders->rollback();
            return callback(false, $e->getMessage());
        }

        return callback(true, '操作成功');
    }

    /**
     * 支付保证金，拍品和会员状态调整，设置已支付保证金，报名，拍品数据报名人数+1,添加冻结保证金
     * 增加除了保证金账户支付金额部分以外的保证金冻结金额
     * @param $order_info int 订单详情
     * @return bool
     * */
    public function auction_member_bond($order_info)
    {
        $model_auction_member_relation = Model('auction_member_relation');
        $model_auctions = Model('auctions');

        //增加除了保证金账户支付金额部分以外的保证金冻结金额
        $account_margin_amount = floatval($order_info['margin_amount'] - $order_info['account_margin_amount']);
        if ($account_margin_amount > 0) {
            $data_ma = array();
            $data_ma['member_id'] = $order_info['buyer_id'];
            $data_ma['member_name'] = $order_info['buyer_name'];
            $data_ma['amount'] = $account_margin_amount;
            $data_ma['order_sn'] = $order_info['order_sn'];
            // 保证金冻结
            $this->MarginRefund($data_ma, 'pay');
        }

        $update = array('is_bond' => 1, 'bond_num' => $order_info['margin_amount'],);
        $condition = array('member_id' => $order_info['buyer_id'], 'auction_id' => $order_info['auction_id'],);
        $result_1 = $model_auction_member_relation->setRelationInfo($update, $condition);

        unset($condition);
        // 报名人数 +1
        $condition = array('num_of_applicants' => array('exp', 'num_of_applicants+1'),'real_participants' => array('exp', 'real_participants + 1'),);
        $result_2 = $model_auctions->editAuctionsById($condition, array($order_info['auction_id']));
        $special_id = $model_auctions->getAuctionsInfoByID($order_info['auction_id'],'special_id');
        $result_3 = Model('auction_special')->editSpecial(array('participants'=>array('exp','participants+1')),$special_id);

        return $result_1 && $result_2 && $result_3;
    }

    /*
     * 收到拍卖订单尾款
     * */
    public function changeAuctionOrderReceivePay($order_info, $role, $user = '', $post = array())
    {
        $model_auction_order = Model('order');
        try {
            $model_auction_order->beginTransaction();

            $model_pd = Model('predeposit');

            $auction_order_id = $order_info['order_id'];
            if (!in_array($order_info['order_state'], array(10, 50))) {
                throw new Exception('订单状态不允许收款');
            }

            //下单，支付被冻结的充值卡
            $rcb_amount = floatval($order_info['rcb_amount']);
            if ($rcb_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $order_info['buyer_id'];
                $data_pd['member_name'] = $order_info['buyer_name'];
                $data_pd['amount'] = $rcb_amount;
                $data_pd['order_sn'] = $order_info['auction_order_sn'];
                $model_pd->changeRcb('order_comb_pay', $data_pd);
            }

            //下单，支付被冻结的预存款
            $pd_amount = floatval($order_info['pd_amount']);
            if ($pd_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $order_info['buyer_id'];
                $data_pd['member_name'] = $order_info['buyer_name'];
                $data_pd['amount'] = $pd_amount;
                $data_pd['order_sn'] = $order_info['auction_order_sn'];
                $model_pd->changePd('order_comb_pay', $data_pd);
            }
            //下单，支付被冻结的积分
            $points_amount = floatval($order_info['points_amount']);
            if ($points_amount > 0) {
                $nuobi_points = $points_amount * 100;
                Model('points')->savePointsLog(
                    'pay_points',
                    array(
                        'pl_memberid' => $_SESSION['member_id'],
                        'pl_membername' => $order_info['buyer_name'],
                        'pl_points' => $nuobi_points,
                        'order_sn' => $order_info['auction_order_sn'],
                        'order_id' => $order_info['order_id']
                    ),
                    true
                );
            }

            //添加订单日志
            $data = array();
            $data['order_id'] = $auction_order_id;
            $data['log_role'] = $role;
            $data['log_user'] = $user;
            $data['log_msg'] = '收到拍卖货款(外部交易号:' . $post['trade_no'] . ')，拍卖订单号：'.$order_info['auction_order_sn'];
            $data['log_orderstate'] = ORDER_STATE_PAY;
            $insert = Model('order')->addOrderLog($data);
            if (!$insert) {
                throw new Exception('操作失败');
            }
            //更新订单状态
            $update_order = array();
            $update_order['order_state'] = 20;
            $update_order['payment_time'] = ($post['payment_time'] ? strtotime($post['payment_time']) : TIMESTAMP);
            $update_order['payment_code'] = $post['payment_code'];
            $update_order['api_pay_amount'] = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];
            if ($post['trade_no'] != '') {
                $update_order['trade_no'] = $post['trade_no'];
            }
//            $update_order['api_pay_state'] = 1;
            $condition = array();
            $condition['order_id'] = $order_info['order_id'];
            $condition['order_state'] = array('in', array(10, 50));
            $update = $model_auction_order->editOrder($update_order,$condition);
            if (!$update) {
                throw new Exception('操作失败');
            }

            // 减掉会员账户冻结保证金
            /** @var memberModel $model_member */
            $model_member = Model('member');
            $update = array(
                'freeze_margin' => array('exp', 'freeze_margin-'.$order_info['margin_amount'])
            );
            $model_member->editMember(array('member_id' => $order_info['buyer_id']),$update);

            // 记录日志
            $this->MarginLog(
                $order_info['buyer_id'],
                $order_info['buyer_name'],
                4,
                $order_info['margin_amount'],
                $order_info['order_sn']
            );

            $model_auction_order->commit();
        } catch (Exception $e) {
            $model_auction_order->rollback();
            return callback(false, $e->getMessage());
        }
        $order_id = $order_info['order_id'];
        $condition = array('order_id' => $order_id);
        $goods_info = Model('order')->getOrderInfo($condition,['order_goods']);
        $goods_name = '';
        foreach ($goods_info['extend_order_goods'] as $key=>$value) {
            $goods_name .= $value['goods_name'] . ' ';
        }
        if(mb_strlen($goods_name,'UTF8') > 10){
            $goods_name = mb_substr($goods_name,0,10,'UTF8') . '...';
        }
        //支付成功发送买家消息
        $param = array();
        $param['code'] = 'order_payment_success';
        $param['member_id'] = $order_info['buyer_id'];
        $param['param'] = array(
            'goods_name' => $goods_name,
            'order_amount' =>$order_info['order_amount'],
            'order_sn' => $order_info['order_sn'],
            'order_url' => urlShop('member_auction', 'have_auction')
        );
        QueueClient::push('sendMemberMsg', $param);

        return callback(true, '操作成功');
    }

    /**
     * 获取买家参与拍卖商品列表
     * @param $member_id int 会员ID
     * @param $refund_state int 会员是否已经退保证金，是否过期的标识 ：1 已退
     * @param $lock_state int 是否中拍
     * @param $keyword array 是否中拍
     * @param $param array 其他参数
     * @return  array
     * */
    public function getMemberAuctionList($member_id, $refund_state, $lock_state, $keyword = null,$margin_state = array(),$limit = 0)
    {
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $model_auction_member_relation = Model('auction_member_relation');
        $condition = array('buyer_id' => $member_id);
        $refund_state !='' ? $condition['refund_state'] = $refund_state : '';
        $lock_state !='' ? $condition['lock_state'] = $lock_state : '';
        if(!empty($margin_state)){
            $condition['order_state'] = array('in',implode(',',$margin_state));
        }
        if (!empty($keyword)) {
            $condition['order_sn'] = $keyword;
        }
        if (!empty($param)) {
            $condition = array_merge($condition,$param);
        }
        // 查询保证金列表
        $margin_order_list = $model_margin_orders->getOrderList($condition, '*', '', 'updated_at desc', $limit, 10);
        //分页
        $showPage = $model_margin_orders->showpage();

        // 追加其余信息
        foreach ($margin_order_list as $key => $value) {
            $auction_info = $model_auctions->getAuctionsInfoByID($value['auction_id']);
            if ($auction_info['current_price'] == 0 || empty($auction_info['current_price'])) {
                $auction_info['current_price'] = $auction_info['auction_start_price'];
            }
            // 添加商品信息
            $margin_order_list[$key]['auction_info'] = $auction_info;
            // 添加关系信息
            $margin_order_list[$key]['relation'] = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $value['auction_id']);
            // 追加成交拍品的信息
            if ((is_array($lock_state) || $lock_state == 1) && $value['auction_order_id'] != 0) {
                $model_auction_order = Model('order');
                $condition = array('order_id' => $value['auction_order_id']);
                $auction_order = $model_auction_order->getOrderInfo($condition);
                // 计算是否支付完成
                $pay_amount = round($auction_order['order_amount'] - $auction_order['rcb_amount'] - $auction_order['pd_amount'] - $auction_order['points_amount'] - $auction_order['api_pay_amount'], 2);

                if($pay_amount < $auction_order['margin_amount']){//保证金金额大于拍卖订单金额
                    $pay_amount = 0;
                }else{
                    $pay_amount = $pay_amount - $auction_order['margin_amount'];
                }

                $margin_order_list[$key]['auction_order'] = $auction_order;
                $margin_order_list[$key]['pay_amount'] = $pay_amount;

                // 追加店铺信息
                $model_store = Model('store');
                $margin_order_list[$key]['extend_store'] = $model_store->getStoreInfoByID($value['store_id']);

                // 追加按钮显示
                // 查看物流
                $margin_order_list[$key]['if_deliver'] = $this->getOrderOperateState('deliver', $auction_order);
                // 收货
                $margin_order_list[$key]['if_receive'] = $this->getOrderOperateState('receive', $auction_order);
                // 锁定，退款中
                $margin_order_list[$key]['if_lock'] = $this->getOrderOperateState('lock', $auction_order);
                // 退款
                $margin_order_list[$key]['if_refund_cancel'] = $this->getOrderOperateState('refund_cancel', $auction_order);

            }
        }

        return array('showPage' => $showPage, 'margin_order_list' => $margin_order_list, 'totalpage' => $model_margin_orders->gettotalpage());
    }



    /**
     * 获取买家参与拍卖商品列表
     * @param $member_id int 会员ID
     * @param $refund_state int 会员是否已经退保证金，是否过期的标识 ：1 已退
     * @param $lock_state int 是否中拍
     * @param $keyword array 是否中拍
     * @param $param array 其他参数
     * @return  array
     * */
    public function auctionListFetchBak($member_id, $refund_state, $lock_state, $keyword = null,$margin_state = array(),$limit = 0)
    {
        /** @var auctionsModel $model_auctions */
        /** @var margin_ordersModel $model_margin_orders */
        /** @var auction_member_relationModel $model_auction_member_relation */
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $model_auction_member_relation = Model('auction_member_relation');
        $condition = array('buyer_id' => $member_id);
        $refund_state !='' ? $condition['refund_state'] = $refund_state : '';
        $lock_state !='' ? $condition['lock_state'] = $lock_state : '';
        if(!empty($margin_state)){
            $condition['order_state'] = array('in',implode(',',$margin_state));
        }
        if (!empty($keyword)) {
            $condition['order_sn'] = $keyword;
        }
        if (!empty($param)) {
            $condition = array_merge($condition,$param);
        }
        // 查询保证金列表
        $fields = 'margin_id, order_sn, auction_id, auction_image, auction_name, margin_amount, order_state, default_state';
        $margin_order_list = $model_margin_orders->orderListFetch($condition, $fields, '', 'updated_at desc', $limit, 10);
        $totalpage = $model_margin_orders->where($condition)->count();
        // 追加其余信息
        foreach ($margin_order_list as $key => &$value) {
            $value['auction_image'] = $value['auction_image'] ? cthumb($value['auction_image'], 360) : '';
            $auction_info = $model_auctions->getAuctionsInfoByID($value['auction_id']);
            $value['current_price'] = 0;
            if ($auction_info['current_price'] == 0 || empty($auction_info['current_price'])) {
                $value['current_price'] = $auction_info['auction_start_price'];
            }
            //status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款  5 : 待支付 6 : 已取消  7 : 抵扣部分
            if ($value['order_state'] == 1) {
                //已支付,  不违约
                if ($value['default_state'] == 0) {
                    if ($value['refund_state'] == 0) {
                        //未退款
                        $value['status'] = 1;
                    } elseif ($value['refund_state'] == 1) {
                        //已退款
                        $value['status'] = 2;
                    } else {
                        //抵扣
                        if ($value['rufund_money'] == 0) {
                            $value['status'] = 3;
                            $value['refund_money'] = $value['margin_amount'];
                        } else {
                            //根据退换金额判断是退款 或者部分抵扣
                            if ($value['rufund_money'] == $value['margin_amount']) {
                                $value['status'] = 2;
                            } else {
                                //部分抵扣  计算抵扣金额
                                $value['status'] = 7;
                                $value['refund_money'] = $value['margin_amount']
                                    - $value['rufund_money'];
                            }
                        }
                    }
                } else {
                    //已支付违约  违约扣款
                    $value['status'] = 4;
                }
            } elseif ($value['order_state'] == 0) {
                $value['status'] = 5;
            } elseif ($value['order_state'] == 4) {
                $value['status'] = 6;
            }
        }

        return array('margin_order_list' => $margin_order_list, 'totalpage' => $totalpage);
    }

    /**
     * 获取买家参与拍卖商品列表
     * @param $member_id int 会员ID
     * @param $refund_state int 会员是否已经退保证金，是否过期的标识 ：1 已退
     * @param $lock_state int 是否中拍
     * @param $keyword array 是否中拍
     * @param $param array 其他参数
     * @return  array
     * */
    public function auctionListFetch($member_id, $refund_state, $lock_state, $keyword = null,$margin_state = array(),$limit = 0, $type = null)
    {
        /** @var auctionsModel $model_auctions */
        /** @var margin_ordersModel $model_margin_orders */
        /** @var auction_member_relationModel $model_auction_member_relation */

        if($limit === 0){
            $page = $_GET['page'];
            if (!isset($page) || empty($page) || !is_numeric($page)) {
                $page = 1;
            }
            $pageSize = $model_auctions->page;
            $begin = ($page - 1) * $pageSize;
            $limit = $begin . ',' . $pageSize;
        }
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $condition = array('buyer_id' => $member_id);
        //这里 开始计算缴纳保证金总金额
        $condition['order_state'] = 1;
        $refund_state != '' ? $condition['refund_state'] = $refund_state : '';
        $lock_state != '' ? $condition['lock_state'] = $lock_state : '';
        if($type === 'list_have'){
            $condition['auction_order_id'] = ['gt', 0];
        }
        //参拍中--拍卖未结束
        if($type === 'list_ing'){
            $condition['auction_end_true_t'] = ['gt', time()];
        }
        if (!empty($margin_state)) {
            $condition['order_state'] = array('in', implode(',', $margin_state));
        }
        if (!empty($keyword)) {
            $condition['order_sn'] = $keyword;
        }
        if (!empty($param)) {
            $condition = array_merge($condition, $param);
        }
        $condition['auction_id'] = ['neq', C('newBieAuctionId')];
        // 查询保证金列表
        $fields = 'margin_id, order_sn, auction_id, auction_image, auction_name, sum(margin_amount) as margin_amount';
        $margin_order_list = $model_margin_orders->orderListFetch($condition, $fields, 'auction_id', 'updated_at desc', $limit);
        $fields = 'count(distinct(`auction_id`)) as cnt';
        $page_count = $model_margin_orders->field($fields)->where($condition)->find();
        if ($page_count) {
            $page_count = $page_count['cnt'] ?: 0;
        } else {
            $page_count = 0;
        }
        // 追加其余信息
        foreach ($margin_order_list as $key => $value) {
            $fields = 'auction_id, auction_type, special_id, state, auction_preview_start, auction_start_time, auction_end_time, auction_end_true_t, auction_start_price, current_price';
            $auction_info = $model_auctions->getAuctionsInfoByID($value['auction_id'], $fields);
            if (empty($auction_info)) {
                unset($margin_order_list[$key]);
                continue;
            }
            $margin_order_list[$key]['auction_type'] = $auction_info['auction_type'];
            $margin_order_list[$key]['auction_image'] = cthumb($value['auction_image'], 360);
            //预展中
            $nb_condition = [
                'special_id' => $auction_info['special_id']
            ];
            /** @var auction_specialModel $special */
            $special = Model('auction_special');
            $nb_list = $special->field('special_id, is_pd_pay')->where($nb_condition)->find();
            $margin_order_list[$key]['is_nb'] = isset($nb_list['is_pd_pay']) && $nb_list['is_pd_pay'] == 1 ? 1 : 0;
            $margin_order_list[$key]['is_kai_pai'] = 1;
            $margin_order_list[$key]['status'] = 1;
            $margin_order_list[$key]['order_id'] = 0;
            $margin_order_list[$key]['order_state'] = 0;     //订单状态  默认不展示
            //order_state 订单状态  0 : 默认不展示 1: 已取消 2: 未付款 3 : 待发货 4 : 已发货 5 已收货
            //is_kai_pai 状态判断   1: 竞拍中  2 : 已拍中  3 : 未拍中
            //status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款
            if ($auction_info['state'] == 1) {
                $margin_order_list[$key]['is_kai_pai'] = 3;
                //判断该用户是否拍中该拍品显示亏款和抵扣状态
                $margin_order_list[$key]['status'] = 2;
                //当前用户是否拍中该拍品
                $order = Model('orders');
                $order_fetch = $order->where(['auction_id' => $auction_info['auction_id']])->find();
                if ($order_fetch && $order_fetch['buyer_id'] == $member_id) {
                    $margin_order_list[$key]['is_kai_pai'] = 2;
                    //订单创建时间
                    $margin_order_list[$key]['created_at'] = $order_fetch['add_time'];
                    $margin_order_list[$key]['order_sn'] = $order_fetch['order_sn'];
                    //该用户拍中拍品 显示已抵扣
                    $margin_order_list[$key]['status'] = 3;
                    $margin_order_list[$key]['order_id'] = $order_fetch['order_id'];
                    if ($order_fetch['order_state'] == 0) {
                        $margin_order_list[$key]['status'] = 4;
                    } elseif ($order_fetch['order_state'] == 10) {
                        $margin_order_list[$key]['status'] = 1;
                    }
                    switch ($order_fetch['order_state'])
                    {
                        case '0':
                            $margin_order_list[$key]['order_state'] = 1;
                            break;
                        case '10':
                            $margin_order_list[$key]['order_state'] = 2;
                            break;
                        case '20':
                            $margin_order_list[$key]['order_state'] = 3;
                            break;
                        case '30':
                            $margin_order_list[$key]['order_state'] = 4;
                            break;
                        case '40':
                            $margin_order_list[$key]['order_state'] = 5;
                            break;
                        default:
                            $margin_order_list[$key]['order_state'] = 0;
                            break;
                    }
                }
            } elseif ($margin_order_list[$key]['order_state'] == 0 || $margin_order_list[$key]['order_state'] == 4) {
                $margin_order_list[$key]['is_kai_pai'] = 1;
                $margin_order_list[$key]['status'] = 1;
            }
            $margin_order_list[$key]['current_price'] = $auction_info['current_price'];
            if ($auction_info['current_price'] == 0 || empty($auction_info['current_price'])) {
                $margin_order_list[$key]['current_price'] = $auction_info['auction_start_price'];
            }
        }
        return ['margin_order_list' => $margin_order_list, 'page_count' => $page_count];
    }
    /**
     * 获取买家参与拍卖商品列表
     * @param $member_id int 会员ID
     * @param $refund_state int 会员是否已经退保证金，是否过期的标识 ：1 已退
     * @param $lock_state int 是否中拍
     * @param $keyword array 是否中拍
     * @param $param array 其他参数
     * @return  array
     * */
    public function marginListFetch($member_id, $refund_state, $lock_state, $keyword = null,$margin_state = array(),$limit = 0)
    {
        /** @var auctionsModel $model_auctions */
        /** @var margin_ordersModel $model_margin_orders */
        /** @var auction_member_relationModel $model_auction_member_relation */
        $page = $_GET['page'];
        if (!isset($page) || empty($page) || !is_numeric($page)) {
            $page = 1;
        }
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $pageSize = $model_auctions->page;
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        $condition = array('buyer_id' => $member_id);
        //这里 开始计算缴纳保证金总金额
        //$condition['order_state'] = 1;
        $refund_state !='' ? $condition['refund_state'] = $refund_state : '';
        $lock_state !='' ? $condition['lock_state'] = $lock_state : '';
        if(!empty($margin_state)){
            $condition['order_state'] = array('in',implode(',',$margin_state));
        }
        if (!empty($keyword)) {
            $condition['order_sn'] = $keyword;
        }
        if (!empty($param)) {
            $condition = array_merge($condition,$param);
        }
        // 查询保证金列表
        $fields = 'order_sn, auction_id, auction_image, auction_name, margin_amount, bag_amount';
        $margin_order_list = $model_margin_orders->orderListFetch($condition, $fields, 'auction_id', 'updated_at desc', $limit);
        $fields = 'count(distinct(`auction_id`)) as cnt';
        $page_count = $model_margin_orders->field($fields)->where($condition)->find();
        if ($page_count) {
            $page_count = $page_count['cnt'] ?: 0;
        } else {
            $page_count = 0;
        }
        // 追加其余信息
        foreach ($margin_order_list as $key => $value) {
            $fields = 'auction_id, state, auction_preview_start, auction_start_time, auction_end_time, auction_end_true_t, auction_start_price, current_price';
            $auction_info = $model_auctions->getAuctionsInfoByID($value['auction_id'], $fields);
            if (empty($auction_info) || $value['margin_amount'] == $value['bag_amount']  || $auction_info['auction_id'] == C('newBieAuctionId')) {
                unset($margin_order_list[$key]);
                continue;
            }
            $condition['order_state'] = 1;
            $condition['auction_id'] = $value['auction_id'];
            $margin_amount = $model_margin_orders->where($condition)->sum('margin_amount');
            $margin_order_list[$key]['margin_amount'] = $margin_amount ?: 0;
            $margin_order_list[$key]['auction_image'] = cthumb($value['auction_image'], 360);
            //预展中
            $margin_order_list[$key]['is_kai_pai'] = 3;
            $margin_order_list[$key]['status'] = 1;
            $margin_order_list[$key]['is_kai_pai_time'] = date('Y.m.d H:i', $auction_info['auction_start_time']);
            //is_kai_pai 状态判断   1: 拍卖中  2 : 收益中  3 : 预展中  4 : 已结束
            //status 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款  5:已取消
            $current_time = time();
            $orderId = 0;
            if ($auction_info['state'] == 1 || $auction_info['auction_end_time'] <= $current_time) {
                $margin_order_list[$key]['is_kai_pai'] = 4;
                //当前用户是否拍中该拍品
                $order = Model('orders');
                $order_fetch = $order->where(['auction_id' => $auction_info['auction_id']])->find();
                if ($margin_order_list[$key]['order_state'] == 4) {
                    //订单已取消
                    $margin_order_list[$key]['status'] = 5;
                } else {
                    //判断该用户是否拍中该拍品显示亏款和抵扣状态
                    $margin_order_list[$key]['status'] = 2;
                    //'buyer_id' => $member_id,
                    if ($order_fetch && $order_fetch['buyer_id'] == $member_id) {
                        //该用户拍中拍品 显示已抵扣
                        $margin_order_list[$key]['status'] = 3;
                        if ($order_fetch['order_state'] == 0) {
                            $margin_order_list[$key]['status'] = 4;
                        } elseif ($order_fetch['order_state'] == 10) {
                            $margin_order_list[$key]['status'] = 1;
                        }
                    }
                }
                $orderId = $order_fetch['order_id'];
                $margin_order_list[$key]['is_kai_pai_time'] = $auction_info['auction_end_true_t']
                    ?: $auction_info['auction_end_time'];
            } else {
                if ($auction_info['auction_preview_start'] < $current_time && $auction_info['auction_start_time'] > $current_time) {
                    //预展中
                    $margin_order_list[$key]['is_kai_pai'] = 3;
                    if ($margin_order_list[$key]['order_state'] == 4) {
                        //订单已取消
                        $margin_order_list[$key]['status'] = 5;
                    } else {
                        $margin_order_list[$key]['status'] = 1;
                    }
                    $margin_order_list[$key]['is_kai_pai_time'] = date('Y.m.d H:i', $auction_info['auction_start_time']);
                } elseif ($auction_info['auction_start_time'] < $current_time) {
                    //拍卖中
                    $margin_order_list[$key]['is_kai_pai'] = 1;
                    if ($margin_order_list[$key]['order_state'] == 4) {
                        //订单已取消
                        $margin_order_list[$key]['status'] = 5;
                    } else {
                        $margin_order_list[$key]['status'] = 1;
                    }
                    $margin_order_list[$key]['is_kai_pai_time'] = date('Y.m.d H:i', $auction_info['auction_end_true_t']);
                } elseif ($auction_info['auction_preview_start'] > $current_time) {
                    //已结束
                    $margin_order_list[$key]['is_kai_pai'] = 3;
                    if ($margin_order_list[$key]['order_state'] == 4) {
                        //订单已取消
                        $margin_order_list[$key]['status'] = 5;
                    } else {
                        $margin_order_list[$key]['status'] = 1;
                    }
                    $margin_order_list[$key]['is_kai_pai_time'] = date('Y.m.d H:i', $auction_info['auction_end_true_t']);
                }
            }
            $margin_order_list[$key]['order_id'] = $orderId;
            $margin_order_list[$key]['current_price'] = $auction_info['current_price'];
            if ($auction_info['current_price'] == 0 || empty($auction_info['current_price'])) {
                $margin_order_list[$key]['current_price'] = $auction_info['auction_start_price'];
            }
        }
        $margin_order_list = array_values($margin_order_list);
        return ['margin_order_list' => $margin_order_list, 'page_count' => $page_count];
    }

    /**
     * 保证金解冻，返还，退还
     * @param $data array
     * @param $type string 操作类型 pay 支付冻结部分， refund 冻结部分返还
     * */
    public function MarginRefund($data, $type)
    {
        $member_id = $data['member_id'];
        $order_sn = $data['order_sn'];

        $model_member = Model('member');
        $model_margin_orders = Model('margin_orders');

        $member_info = $model_member->getMemberInfoByID($member_id);

        switch ($type) {
//            冻结保证金
            case 'pay':
                $update_member = array(
                    'freeze_margin' => $member_info['freeze_margin'] + $data['amount'],
                );
                $update_order = array();
                $type = 3;
                break;
//                返还保证金
            case 'refund':
                $update_member = array('available_margin' => $member_info['available_margin'] + $data['amount'], 'freeze_margin' => $member_info['freeze_margin'] - $data['amount']);
                $update_order = array('account_margin_amount' => 0);
                $type = 2;
                break;
            case 'return'://退回保证金
                $update_member = array(
                    'available_margin' => $member_info['available_margin'] + $data['amount'],
                );
                $update_order = array();
                $type = 7;
        }
        // 更新会员保证金账户和冻结账户
        $model_member->editMember(array('member_id' => $member_id), $update_member);
        // 更新订单保证金支付金额
        $model_margin_orders->editOrder($update_order, array('order_sn' => $order_sn));
        // 记录保证金操作日志

        $this->MarginLog($member_id, $member_info['member_name'], $type, $data['amount'], $order_sn);
    }

    /**
     * 支付使用保证金支付
     * @param $member_id int 会员ID
     * @param $order_sn int 订单号
     * @return bool
     * */
    public function MarginPay($member_id, $order_sn)
    {
//        $model_member = Model('member');
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');

//        $member_info = $model_member->getMemberInfoByID($member_id);
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
//        // 保证金金额大于0使用保证金余额支付保证金
//        if ($member_info['available_margin'] > 0) {
//            // 如果余额大于所需保证金直接全部付款
//            if ($member_info['available_margin'] >= $order_info['margin_amount']) {
//                $available_margin = $member_info['available_margin'] - $order_info['margin_amount'];
//                $account_margin_amount = $order_info['margin_amount'];
//            } else {
//                $available_margin = 0;
//                $account_margin_amount = $member_info['available_margin'];
//            }
//
//            // 更新会员保证金账户和冻结账户
//            $model_member->editMember(array('member_id' => $member_id), array('available_margin' => $available_margin, 'freeze_margin' => $account_margin_amount + $member_info['freeze_margin']));
//            // 更新订单保证金支付金额
//            $model_margin_orders->editOrder(array('account_margin_amount' => $account_margin_amount), array('order_sn' => $order_sn));
//            // 记录保证金操作日志
//            $this->MarginLog($member_id, $member_info['member_name'], 1, $account_margin_amount, $order_sn);
//            $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));
//        }
        return callback(true, '', array('order_info' => $order_info));

    }

    /**
     * ！外围事务！
     * 尾款支付订购单成功,记录保证金日志,减掉会员账户金额
     * @param $member_id
     * @param $member_name
     * @param $type
     * @param $amount double 剩余支付金额(todo 只有一种情况进入这里需要扣钱, 保证金金额大于支付金额,支付订单时,这里需要返回用户的钱,但是这里没有记入日志) 其他情况传0,!已经并需要在上层付过款!这块返钱的操作有时间移走
     * @param $order_id
     * @throws \App\Exceptions\ResponseException
     */
    public function AuctionOrderMarginLog($member_id, $member_name, $type, $amount, $order_id)
    {
        // 更新订单状态
        /** @var margin_ordersModel $model_margin_order */
        $model_margin_order = Model('margin_orders');
        $filter = array('auction_order_id'=>$order_id,'buyer_id'=>$member_id, 'order_state'=>1);
        $this->order_info = $order_info = $model_margin_order->getOrderInfo($filter);
        $order_sn = $order_info['order_sn'];

        // 减掉会员账户冻结保证金
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $model_member->beginTransaction();
        try {
            //这块定位的有严重问题 ,检查发现其他地方已经退款多余金额,这里不需要在退钱!,会死的!
/*            if ($order_info['margin_amount'] >= $amount && $type == 10) {
                //todo 只有一种情况进入这里,保证金金额大于支付金额,支付订单时,这里需要返回用户的钱,但是这里没有记入日志,这块返钱的操作有时间移走
                //保证金金额大于订单价格
                $return_amount = $order_info['margin_amount'] - $amount;
                if ($return_amount > 0) {
                    $update = array(
                        'available_predeposit' => array('exp', 'available_predeposit+' . $return_amount)
                    );
                    // 记录日志
                    $this->MarginLog($member_id, $member_name, 2, $return_amount, $order_sn);
                    $condition = array('member_id' => $member_id);
                    $condition['available_predeposit'] = ['egt', 0];
                    $effectRow = $model_member->editMember($condition, $update, true);
                    if ($effectRow < 1) {
                        \App\Exceptions\ApiResponseException::throwFailMsg('请勿频繁操作!');
                    }
                }
            }*/
            // 记录日志
            $this->MarginLog($member_id, $member_name, 4, $amount, $order_sn);
            $model_member->commit();
        } catch (\Exception $e) {
            $model_member->rollback();
            if(!$e instanceof \App\Exceptions\ApiResponseException) {
                yLog()->error("AuctionOrderMarginLog_Exception", [\Yinuo\Lib\Helpers\LogHelper::getEInfo($e), func_get_args()]);
            }
            \App\Exceptions\ApiResponseException::throwFailMsg($e->getMessage());
        }
    }


    /**
     * 记录保证金日志
     * @param $member_id int
     * @param $member_name string
     * @param $type int 操作类型，1:保证金余额支付保证金，2:返还保证金，3:支付拍卖商品成功向冻结账户添加金额
     * @param $amount double 变动金额
     * @param $order_sn int 订单号
     * @return bool
     * */
    public function MarginLog($member_id, $member_name, $type, $amount, $order_sn = null)
    {
        $model_margin_log = Model('margin_log');
        $insert = array('member_id' => $member_id, 'member_name' => $member_name,);
        switch ($type) {
            case 1:
                $insert['available_amount'] = -$amount;
                $insert['freeze_amount'] = $amount;
                $insert['description'] = '支付保证金，订单号：' . $order_sn;
                break;
            case 2:
                $insert['available_amount'] = $amount;
                $insert['freeze_amount'] = -$amount;
                $insert['description'] = '返还保证金，订单号：' . $order_sn;
                $url = AUCTION_OLD_URL."/wap/tmpl/member/margin_details_wap2.html?auction_id=".$this->order_info['auction_id'];
                $url = filterUrl($url);
                $short = sinaShortenUrl($url);
                $param = array();
                $param['code'] = 'release_margin';
                $param['member_id'] = $this->order_info['buyer_id'];
                $param['number']['mobile'] = $this->order_info['buyer_phone'];
                $param['param'] = array(
                    'auction_name' => $this->order_info['auction_name'],
                    'money' => $this->order_info['margin_amount'],
                    'url' => $short,
                    'wx_url' => $short
                );
                /** @var member_msg_tplModel $member_msg_tpl */
                $member_msg_tpl =Model('member_msg_tpl');
                $member_msg_tpl->sendWXTplMsg($param);
                break;
            case 3:
                $insert['freeze_amount'] = $amount;
                $insert['description'] = '冻结保证金，订单号：' . $order_sn;
                break;
            case 4:
                $insert['freeze_amount'] = -$amount;
                $insert['description'] = '冻结保证金支付拍品尾款，订单号：' . $order_sn;
                break;
            case 5:
                $insert['freeze_amount'] = -$amount;
                $insert['description'] = '买家违约，平台没收保证金，订单号：' . $order_sn;
                break;
            case 6:
                $insert['freeze_amount'] = -$amount;
                $insert['description'] = '提现到预存款';
                break;
            case 7:
                $insert['available_amount'] = $amount;
                $insert['description'] = '拍卖订单退款保证金，订单号：'.$order_sn;
                break;
        }
        $result = $model_margin_log->addLog($insert);
        return $result;
    }

    /**
     * 支付拍品尾款获取订单信息
     * @param $auction_order_id int 尾款订单ID
     * @param $margin_id int 保证金订单ID
     * @return  mixed
     * */
    public function show_auction_order($auction_order_id, $margin_id, $member_id)
    {
        // 尾款订单信息
        /** @var orderModel $model_order */
        /** @var margin_ordersModel $auction_order_info */
        $model_order = Model('order');
        $condition = array('order_id' => $auction_order_id);
        $auction_order_info = $model_order->getOrderInfo($condition);
        // 保证金订单信息
        $model_margin_orders = Model('margin_orders');
        $condition = array('margin_id' => $margin_id);
        $margin_order_info = $model_margin_orders->getOrderInfo($condition);
        // 计算是否支付完成
        $pay_amount = $auction_order_info['order_amount'] - $auction_order_info['rcb_amount'] - $auction_order_info['pd_amount'] - $auction_order_info['points_amount'] - $auction_order_info['api_pay_amount'];
        if($margin_order_info['margin_amount'] > $pay_amount){//保证金金额大于订单金额
            $pay_amount = 0;
        }else{
            $pay_amount = $pay_amount - $margin_order_info['margin_amount'];
        }
        $auction_order_info['pay_amount'] = round($pay_amount, 2);
        $auction_order_info['address_info'] = Model('address')->getDefaultAddressInfo(array('member_id'=>$member_id));
        //地址信息
        if (intval($_GET['address_id']) > 0) {
            $auction_order_info['address_info'] = Model('address')->getDefaultAddressInfo(array('address_id'=>intval($_GET['address_id']),'member_id'=>$member_id));
        }
//        if ($auction_order_info['address_info']) {
//            $data_area = logic('buy')->changeAddr($auction_order_info['freight_list'], $auction_order_info['address_info']['city_id'], $auction_order_info['address_info']['area_id'], $this->member_info['member_id']);
//            if(!empty($data_area) && $data_area['state'] == 'success' ) {
//                if (is_array($data_area['content'])) {
//                    foreach ($data_area['content'] as $store_id => $value) {
//                        $data_area['content'][$store_id] = ncPriceFormat($value);
//                    }
//                }
//            } else {
//                output_error('地区请求失败');
//            }
//        }

//        print_R($margin_order_info);
//        print_R($auction_order_info);exit;
        // 验证匹配订单--新手体验订单不进行保证金验证
        if ($margin_order_info['auction_id'] != $auction_order_info['auction_id'] && $margin_order_info['auction_id'] != C('newBieAuctionId')) {
            return callback(false, '订单不匹配');
        }

        // 拍品信息
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $margin_order_info['auction_id']));
        $auction_info['image_60_url'] = cthumb($auction_info['auction_image'], 60, $auction_info['store_id']);
        $auction_info['image_240_url'] = cthumb($auction_info['auction_image'], 240, $auction_info['store_id']);
        $auction_info['auction_url'] = urlAuction('auctions', 'index', array('id' => $auction_info['auction_id']));
        // 店铺信息
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfoByID($margin_order_info['store_id']);

        // 默认支持发票
        $vat_deny = false;
        $vat_hash = $this->buyEncrypt($vat_deny ? 'deny_vat' : 'allow_vat', $member_id);
        //输出默认使用的发票信息
        $inv_info = Model('invoice')->getDefaultInvInfo(array('member_id' => $member_id));
        if ($inv_info['inv_state'] == '2' && !$vat_deny) {
            $inv_info['content'] = '增值税发票 ' . $inv_info['inv_company'] . ' ' . $inv_info['inv_code'] . ' ' . $inv_info['inv_reg_addr'];
        } elseif ($inv_info['inv_state'] == '2' && $vat_deny) {
            $inv_info = array();
            $inv_info['content'] = '不需要发票';
        } elseif (!empty($inv_info)) {
            $inv_info['content'] = '普通发票 ' . $inv_info['inv_title'] . ' ' . $inv_info['inv_content'];
        } else {
            $inv_info = array();
            $inv_info['content'] = '不需要发票';
        }

        return callback(true, '',
            array(
                'auction_order_info' => $auction_order_info,
                'margin_order_info' => $margin_order_info,
                'store_info' => $store_info,
                'vat_deny' => $vat_deny,
                'vat_hash' => $vat_hash,
                'auction_info' => $auction_info,
                'inv_info' => $inv_info,
                )
        );

    }


    /**
     * 最新支付拍品尾款获取订单信息
     * @author 刘帅
     * @param $auction_order_id int 尾款订单ID
     * @param $member_id
     * @param $address_id
     * @return  mixed
     */
    public function new_show_auction_order($auction_order_id, $member_id, $address_id = 0)
    {
        // 尾款订单信息
        /** @var orderModel $model_order */
        /** @var margin_ordersModel $model_margin_orders */
        $model_order = Model('order');
        $condition = array('order_id' => $auction_order_id);
        $auction_order_info = $model_order->getOrderInfo($condition);
        // 保证金订单信息
        $model_margin_orders = Model('margin_orders');
        $margin_amount = $model_margin_orders->marginCountFetch($member_id, $auction_order_info['auction_id']);
        $auction_order_info['margin_amount'] = $margin_amount;
        // 计算是否支付完成
        $pay_amount = $auction_order_info['order_amount'] - $auction_order_info['rcb_amount'] - $auction_order_info['pd_amount'] - $auction_order_info['points_amount'] - $auction_order_info['api_pay_amount'];
        if ($margin_amount > $pay_amount) {
            //保证金金额大于订单金额
            $refuse_amount = $margin_amount - $pay_amount;
            $pay_amount = 0;
        } else {
            $pay_amount = $pay_amount - $margin_amount;
            $refuse_amount = 0;
        }
        $auction_order_info['pay_amount'] = round($pay_amount, 2);
        /** @var addressModel $address */
        $address = Model('address');

        //地址信息
        if (intval($address_id) > 0) {
            $auction_order_info['address_info'] = $address->getDefaultAddressInfo(array('address_id'=>intval($address_id),'member_id'=>$member_id));
        }else{
            $auction_order_info['address_info'] = $address->getDefaultAddressInfo(array('member_id'=>$member_id));
        }

        // 验证匹配订单
        if ($member_id != $auction_order_info['buyer_id']) {
            return callback(false, '订单不匹配');
        }

        // 拍品信息
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_order_info['auction_id']));
        $auction_info['auction_image'] = cthumb($auction_info['auction_image'], 240, $auction_info['store_id']);
        $auction_info['image_60_url'] = cthumb($auction_info['auction_image'], 60, $auction_info['store_id']);
        $auction_info['image_240_url'] = cthumb($auction_info['auction_image'], 240, $auction_info['store_id']);
        $auction_info['auction_url'] = urlAuction('auctions', 'index', array('id' => $auction_info['auction_id']));
        if ($auction_info['refund_amount']>0) {
            $auction_order_info['wait_amount']=0.00;
        }else{
            $auction_order_info['wait_amount']=$auction_info['order_amount']-$auction_info['margin_amount'];
        }
        
        // 店铺信息
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfoByID($auction_order_info['store_id']);

        // 默认支持发票
        $vat_deny = false;
        $vat_hash = $this->buyEncrypt($vat_deny ? 'deny_vat' : 'allow_vat', $member_id);
        //输出默认使用的发票信息
        $inv_info = Model('invoice')->getDefaultInvInfo(array('member_id' => $member_id));
        if ($inv_info['inv_state'] == '2' && !$vat_deny) {
            $inv_info['content'] = '增值税发票 ' . $inv_info['inv_company'] . ' ' . $inv_info['inv_code'] . ' ' . $inv_info['inv_reg_addr'];
        } elseif ($inv_info['inv_state'] == '2' && $vat_deny) {
            $inv_info = array();
            $inv_info['content'] = '不需要发票';
        } elseif (!empty($inv_info)) {
            $inv_info['content'] = '普通发票 ' . $inv_info['inv_title'] . ' ' . $inv_info['inv_content'];
        } else {
            $inv_info = array();
            $inv_info['content'] = '不需要发票';
        }
        return callback(true, '',
            array(
                'auction_order_info' => $auction_order_info,
                'store_info' => $store_info,
                'vat_deny' => $vat_deny,
                'vat_hash' => $vat_hash,
                'auction_info' => $auction_info,
                'inv_info' => $inv_info,
                'refuse_amount' => $refuse_amount,
                'margin_amount' => $margin_amount
            )
        );

    }

    /**
     * 生成拍品订单
     * @throws \App\Exceptions\ResponseException
     */
    public function auctions_order($auction,$bid_log) {
        $model_margin_orders = Model('margin_orders');
        $model_order = Model('order');
        //锁定拍中人该拍品的保证金

        $check = $model_margin_orders->table('margin_orders')->where(array('buyer_id' => $bid_log['member_id'],'auction_id'=>$auction['auction_id'],'order_state'=> 1))->update(array('lock_state'=> 1));
        $margin_order=$model_margin_orders->table('margin_orders')->where(array('buyer_id' => $bid_log['member_id'],'auction_id'=>$auction['auction_id'],'order_state'=> 1))->order("margin_id DESC")->find();
        $order_chekc_info=$model_order->where(array('buyer_id' => $bid_log['member_id'],'auction_id'=>$auction['auction_id'],'order_state'=>array("neq",0)))->find();
        $member = Model("member")->getMemberInfo(array('member_id'=>$bid_log['member_id']));
        //查出已经生成的传order_id
        if ($order_chekc_info){
            return $order_chekc_info['order_id'];
        }
        if($check){
            //echo "string";
            $auction_id = $auction['auction_id'];
            $auction_info = $auction;
            $pay_sn = Logic('buy_1')->makePaySn($bid_log['member_id']);
            $order_pay = array();
            $order_pay['pay_sn'] = $pay_sn;
            $order_pay['buyer_id'] = $bid_log['member_id'];
            /** @var orderModel $model_order */
            $order_pay_id = $model_order->addOrderPay($order_pay);
            if(!$order_pay_id){
                \App\Exceptions\ApiResponseException::throwFailMsg('订单保存失败[未生成支付单]');
            }
            $order_info=Model("")->table("orders")->where(array('buyer_id'=>$bid_log['member_id'],'auction_id'=>$auction_id,'order_type'=>4))->find();
            if (empty($order_info)) {
                $order = array();
                $map1['buyer_id']= $bid_log['member_id'];
                $map1['auction_id'] = $auction_id;
                $map1['order_type'] = 4;
                $new_order =Model("orders")->where($map1)->find();
                if (empty($new_order)) {
                    $order['order_sn'] = Logic('buy_1')->makeOrderSn($order_pay_id);
                    $order['pay_sn'] = $pay_sn;
                    $order['store_id'] = $auction_info['store_id'];
                    $order['store_name'] = $auction_info['store_name'];
                    $order['buyer_id'] = $bid_log['member_id'];
                    $order['buyer_name'] = $bid_log['member_name'];
                    $order['add_time'] = time();
                    $order['payment_code'] = 'online';
                    $order['order_state'] = 10;
                    $order['order_amount'] = $auction_info['current_price'];
                    $order['goods_amount'] = $auction_info['current_price'];
                    if (empty($margin_order['order_from'])) {
                        $margin_order['order_from']=0;
                    }
                    $order['order_from'] = $margin_order['order_from'];

                    $order['order_type'] = 4;
                    $order['auction_id'] = $auction_id;
                    //$order['margin_amount'] = $bid_log['offer_num'];
                    //print_r($order);
                    $order_id = Model("")->table("orders")->insert($order);
                    if ($order_id){
                        $re = $model_margin_orders->table('margin_orders')->where(array('auction_id' => $auction_id,'buyer_id'=>$bid_log['member_id']))->update(array('auction_order_id'=> $order_id));
                        if(!$re){
                            return false;
                        }else{

                            $url = AUCTION_OLD_URL."/wap/tmpl/member/auction_order_detail_wap2.html?auction_order_id=".$order_id;
                            $url = filterUrl($url);
                            $short = sinaShortenUrl($url);
                            $param = array();
                            $param['code'] = 'auction_auction';
                            $param['member_id'] = $member['member_id'];
                            $param['number']['mobile'] = $member['member_mobile'];
                            $param['param'] = array(
                                'auction_name' => $auction_info['auction_name'],
                                'money' => $auction_info['current_price'],
                                'url' => $short,
                                'wx_url' => $short
                            );
                            QueueClient::push('sendMemberMsg', $param);
                            return $order_id;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /*
     * 拍品订单详细页信息
     * */
    public function getMemberAuctionOrderInfo($auction_order_id, $margin_id)
    {
        // 验证订单
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $model_margin_orders = Model('margin_orders');


        // 查询尾款订单和保证金订单
        $order_info = $model_order->getOrderInfo(array('order_id' => $auction_order_id), array('order_goods', 'order_common', 'store'));
        if (empty($order_info) && $order_info['margin_id'] == $margin_id) {
            callback(false, '订单不存在');
        }

        // 查看物流
        $order_info['if_deliver'] = $this->getOrderOperateState('deliver', $order_info);
        // 收货
        $order_info['if_receive'] = $this->getOrderOperateState('receive', $order_info);
        // 锁定，退款中
        $order_info['if_lock'] = $this->getOrderOperateState('lock', $order_info);
        // 退款
        $order_info['if_refund_cancel'] = $this->getOrderOperateState('refund_cancel', $order_info);

        // 评价 false
        $order_info['if_evaluation'] = $this->getOrderOperateState('refund_cancel', $order_info);

        // 商家审核退款
        $order_info['if_store_cancel'] = $this->getOrderOperateState('store_cancel', $order_info);

        // 商家发货
        $order_info['if_store_send'] = $this->getOrderOperateState('store_send', $order_info);

        // 保证金订单表追加详情
        $order_info['margin_info'] = $model_margin_orders->getOrderInfo(array('margin_id' => $margin_id));



        //显示商家未发货系统自动取消订单日期
        if ($order_info['order_state'] == ORDER_STATE_NEW) {
            $order_info['order_cancel_day'] = $order_info['payment_time'] + 48 * 3600;
        }
        $order_info['invoice_info'] = @unserialize($order_info['invoice_info']);
        //显示快递信息
        if ($order_info['invoice_no'] != '') {
            $express = rkcache('express', true);
            $order_info['express_info']['e_code'] = $express[$order_info['express_id']]['e_code'];
            $order_info['express_info']['e_name'] = $express[$order_info['express_id']]['e_name'];
            $order_info['express_info']['e_url'] = $express[$order_info['express_id']]['e_url'];
        }

        // 添加拍品信息
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $order_info['auction_info'] = $model_auctions->getAuctionsInfo(array('auction_id' => $order_info['auction_id']));
        $order_info['extend_order_goods'] = array(
            'image_60_url' => cthumb($order_info['auction_info']['auction_image'], 60, $order_info['store_id']),
            'image_240_url' => cthumb($order_info['auction_info']['auction_image'], 240, $order_info['store_id']),
            'auction_url' => urlAuction('auctions', 'index', array('id' => $order_info['auction_id']))
        );

        // 添加店铺信息
        $order_info['extend_store'] = Model('store')->getStoreInfoByID($order_info['store_id']);
        return callback(true, '', array(
            'order_info' => $order_info,
            )
        );
    }

    /*
    * 拍品订单详细页信息
    * */
    public function memberAuctionOrderInfo($auction_order_id)
    {
        // 验证订单
        /** @var orderModel $model_order */
        /** @var margin_ordersModel $model_margin_orders */
        $model_order = Model('order');
        // 查询尾款订单和保证金订单
        $orders_fields = 'auction_id, order_id, order_sn, store_id, add_time, payment_time, buyer_id, buyer_name,
         buyer_phone, goods_amount, order_amount, shipping_code, shipping_fee, order_state, rpt_amount';
        $order_info = $model_order->table('orders')->field($orders_fields)
            ->where(array('order_id' => $auction_order_id))->find();
        // 添加拍品信息
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        //拍品数量都是为 1
        $fields = 'auction_id, auction_name, auction_image, current_price';
        $auction_info = $model_auctions->field($fields)->where(['auction_id' => $order_info['auction_id']])->find();
        $order_info['auction_image'] = cthumb($auction_info['auction_image'], 60, $order_info['store_id']);
        $order_info['auction_name'] = $auction_info['auction_name'];
        $order_info['add_time'] = date('Y-m-d H:i:s', $order_info['add_time']);
        $order_info['payment_time'] = date('Y-m-d H:i:s', $order_info['payment_time']);
        $order_common_model = Model('order_common');
        $order_common_fields = 'reciver_name, order_message, reciver_info, shipping_express_id, shipping_time';
        $order_common = $order_common_model->field($order_common_fields)->where(['order_id' => $auction_order_id])->find();
        if (!empty($order_common)) {
            $shipping_express_model = Model('express');
            $shipping_express_info = $shipping_express_model->field('e_name')
                ->where(['id' => $order_common['shipping_express_id']])->find();
            if (!empty($shipping_express_info['e_name'])) {
                $order_info['shipping_express_name'] = $shipping_express_info['e_name'];
            } else {
                $order_info['shipping_express_name'] = '';
            }
            $order_info['payment_name'] = orderPaymentName($order_info['payment_code']);//支付方式
            $order_info['order_message'] = $order_common['order_message'] ?: '';
            $order_info['shipping_time'] = $order_common['shipping_time']
                ? date('Y-m-d H:i:s', $order_common['shipping_time']) : 0;
            $order_info['shipping_express_id'] = $order_common['shipping_express_id'];
            $express = Model('express');
            $express_info = $express->where(['id' => $order_common['shipping_express_id']])->find();
            $order_info['shipping_express_name'] = $express_info['e_name'];
            $reciver_info = unserialize($order_common['reciver_info']);
            $order_info['address'] = isset($reciver_info['address']) ? $reciver_info['address'] : '';
            $order_info['reciver_name'] = isset($order_common['reciver_name']) ? $order_common['reciver_name'] : '';
            $order_info['reciver_phone'] = isset($reciver_info['phone'])
                ? substr_replace($reciver_info['phone'], '****', 3, 4)
                : '';
        } else {
            $order_info['order_message'] = $order_common['order_message'];      //买家留言
            $order_info['shipping_time'] = '';                                  //发货时间
            $order_info['reciver_name'] = '';                                   //收货人姓名
            $order_info['address'] = '';                                        //收货人地址
            $order_info['shipping_express_id'] = $order_common['shipping_express_id'];//物流公司ID
            $order_info['shipping_express_name'] = '';
        }
        //到此为止吧
        return $order_info;
        output_data($order_info);
        if (!empty($order_info)) {
            if (isset($order_info['order_state'])) {
                $order_info['state_desc'] = orderState($order_info);
            }
            if (isset($order_info['payment_code'])) {
                $order_info['payment_name'] = orderPaymentName($order_info['payment_code']);
            }

            /** @var  $order_common */
            $order_common = Model('order_common');
            //追加返回订单扩展表信息
            if (in_array('order_common',$extend)) {
                $order_info['extend_order_common'] = $model_order->table('order_common')->where(array('order_id'=>$order_info['order_id']))->find();
                $order_info['extend_order_common']['reciver_info'] = unserialize($order_info['extend_order_common']['reciver_info']);
                $order_info['extend_order_common']['invoice_info'] = unserialize($order_info['extend_order_common']['invoice_info']);
            }

            //追加返回店铺信息
            if (in_array('store',$extend)) {
                $order_info['extend_store'] = Model('store')->getStoreInfo(array('store_id'=>$order_info['store_id']));
            }

            //返回买家信息
            if (in_array('member',$extend)) {
                /** @var memberModel $member */
                $member = Model('member');
                $order_info['extend_member'] = $member->getMemberInfoByID($order_info['buyer_id']);
            }

            //追加返回商品信息
            if (in_array('order_goods',$extend)) {
                //取商品列表
                $order_goods_list =  $order_common->field($fields)->where(array('order_id'=>$order_info['order_id']))->select();
                $order_info['extend_order_goods'] = $order_goods_list;
            }
        } else {
            $order_info = [];
        }

        output_data($order_info);
        if (empty($order_info) && $order_info['margin_id'] == $margin_id) {
            callback(false, '订单不存在');
        }

        // 查看物流
        $order_info['if_deliver'] = $this->getOrderOperateState('deliver', $order_info);
        // 收货
        $order_info['if_receive'] = $this->getOrderOperateState('receive', $order_info);
        // 锁定，退款中
        $order_info['if_lock'] = $this->getOrderOperateState('lock', $order_info);
        // 退款
        $order_info['if_refund_cancel'] = $this->getOrderOperateState('refund_cancel', $order_info);

        // 评价 false
        $order_info['if_evaluation'] = $this->getOrderOperateState('refund_cancel', $order_info);

        // 商家审核退款
        $order_info['if_store_cancel'] = $this->getOrderOperateState('store_cancel', $order_info);

        // 商家发货
        $order_info['if_store_send'] = $this->getOrderOperateState('store_send', $order_info);

        // 保证金订单表追加详情
        $order_info['margin_info'] = $model_margin_orders->getOrderInfo(array('margin_id' => $margin_id));



        //显示商家未发货系统自动取消订单日期
        if ($order_info['order_state'] == ORDER_STATE_NEW) {
            $order_info['order_cancel_day'] = $order_info['payment_time'] + 48 * 3600;
        }
        $order_info['invoice_info'] = @unserialize($order_info['invoice_info']);
        //显示快递信息
        if ($order_info['invoice_no'] != '') {
            $express = rkcache('express', true);
            $order_info['express_info']['e_code'] = $express[$order_info['express_id']]['e_code'];
            $order_info['express_info']['e_name'] = $express[$order_info['express_id']]['e_name'];
            $order_info['express_info']['e_url'] = $express[$order_info['express_id']]['e_url'];
        }



        // 添加店铺信息
        $order_info['extend_store'] = Model('store')->getStoreInfoByID($order_info['store_id']);
        return callback(true, '', array(
                'order_info' => $order_info,
            )
        );
    }

    /**
     * 商家中心订单列表
     * @param $store_id int
     * @param $order_sn int 订单号
     * @param $buyer_name string 买家名称
     * @param $state_type string 订单状态
     * @param $fields string 查询字段
     * @param $pagesize int
     * @return array
     * */
    public function getStoreAuctionOrderList($store_id, $order_sn, $buyer_name, $state_type, $fields = '*',$pagesize = 20)
    {
        $model_auction_order = Model('auction_order');
        $model_auctions = Model('auctions');
        $model_member = Model('member');
        $model_margin_orders = Model('margin_orders');
        $condition = array();
        $condition['store_id'] = $store_id;
        if (preg_match('/^\d{10,20}$/',$order_sn)) {
            $condition['order_sn'] = $order_sn;
        }
        if ($buyer_name != '') {
            $condition['buyer_name'] = $buyer_name;
        }
        $allow_state_array = array('state_new','state_pay','state_send','state_success','state_cancel');
        if (in_array($state_type, $allow_state_array)) {
            $condition['order_state'] = str_replace(
                $allow_state_array,
                array(ORDER_STATE_NEW,ORDER_STATE_PAY,ORDER_STATE_SEND,ORDER_STATE_SUCCESS,ORDER_STATE_CANCEL),
                $state_type
            );
        }
        $order_list = $model_auction_order->getList($condition, $pagesize, $fields);
        $showPage = $model_auction_order->showpage();
        //页面中显示那些操作
        foreach ($order_list as $key => $order_info) {

            $order_info['add_time_text'] = date('Y-m-d H:i:s',$order_info['add_time']);

            //显示发货
            $order_info['if_store_send'] = $this->getOrderOperateState('store_send',$order_info);

            // 显示订单退款
            $order_info['if_store_cancel'] = $this->getOrderOperateState('store_cancel',$order_info);

            // 显示平台正在审核退款
            $order_info['if_system_refund'] = $this->getOrderOperateState('system_refund',$order_info);

            //显示物流跟踪
            $order_info['if_deliver'] = $this->getOrderOperateState('deliver',$order_info);

            //显示等待审核支付凭证
            $order_info['if_system_underline_confirm'] = $this->getOrderOperateState('system_underline_confirm', $order_info);
            $order_info['auction_info'] = $model_auctions->getAuctionsInfo(array('auction_id' => $order_info['auction_id']));
            $order_info['extend_member'] = $model_member->getMemberInfoByID($order_info['buyer_id']);
            $order_info['extend_order_goods'] = array(
                'image_60_url' => cthumb($order_info['auction_image'], 60, $order_info['store_id']),
                'image_240_url' => cthumb($order_info['auction_image'], 240, $order_info['store_id']),
                'auction_url' => urlAuction('auctions', 'index', array('id' => $order_info['auction_id']))
            );
            $margin_order_info = $model_margin_orders->getOrderInfo(array('margin_id' => $order_info['margin_id']));
            $order_info['extend_order_common'] = array(
                'reciver_name' => $margin_order_info['reciver_name'],
                'phone' => $margin_order_info['buyer_phone'],
                'address' => $margin_order_info['area_info'],
            );

            if (isset($order_info['order_state'])) {
                $order_info['state_desc'] = orderState($order_info);
            }
            if (isset($order_info['payment_code'])) {
                $order_info['payment_name'] = orderPaymentName($order_info['payment_code']);
            }

            $order_list[$key] = $order_info;
        }
        return callback(true, '', array('order_list' => $order_list, 'showPage' => $showPage));
    }

    /**
     * 商家发货
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system 分别代表买家、商家、管理员、系统
     * @param string $user 操作人
     * @param array $post
     * @throws
     * @return mixed
     * */
    public function changeOrderSend($order_info, $role, $user = '', $post = array())
    {
        $model_auction_order = Model('auction_order');
        // 更细订单表发货信息,退款申请直接取消
        try {
            $model_auction_order->beginTransaction();
            $update = array(
                'invoice_no' => $post['invoice_no'],
                'express_id' => $post['express_id'],
                'ship_time' => time(),
                'order_state' => 30,
                'refund_state' => 0,
            );
            $model_auction_order->setOrder(array('auction_order_id' => $order_info['auction_order_id']), $update);
            $model_auction_order->commit();
        } catch(Exception $e) {
            $model_auction_order->rollback();
            return callback(false, $e->getMessage());
        }

        //添加订单日志
        $data = array();
        $data['order_id'] = $order_info['auction_order_id'];
        $data['log_role'] = $role;
        $data['log_user'] = $user;
        $data['log_msg'] = '拍品发货，拍品订单号：'.$order_info['auction_order_sn'];
        $data['log_orderstate'] = ORDER_STATE_SEND;
        Model('order')->addOrderLog($data);

        // 发送买家消息
        $param = array();
        $param['code'] = 'order_deliver_success';
        $param['member_id'] = $order_info['buyer_id'];
        $param['param'] = array('order_sn' => $order_info['auction_order_sn'], 'order_url' => urlShop('member_auction', 'have_auction'));
        QueueClient::push('sendMemberMsg', $param);

        return callback(true, '操作成功');
    }

    /*
     * 会员中心修改尾款订单状态
     * @param array $order_id 拍品订单
     * @param array $post POST数据
     * @param int $type 0 申请退款 1 确认收货
     * */
    public function MemberChangeOrderState($order_info, $post = array(), $type)
    {
        if (empty($order_info)) {
            return callback(false, '订单异常');
        }
        /** @var orderModel $model_order */
        $model_order = Model('order');
        switch ($type) {
            case 0:
                $update = array(
                    'refund_state' => 1,
                    'batch_no' => date('YmdHis').$order_info['auction_order_id'],
                    'buyer_refund_time' => time(),
                );
                $if_allow = $this->getOrderOperateState('refund_cancel', $order_info);
                break;
            case 1:
                $update = array(
                    'order_state' => ORDER_STATE_SUCCESS,
                    'finnshed_time' => TIMESTAMP,
                );
                $if_allow = $this->getOrderOperateState('receive', $order_info);
        }

        if (!$if_allow) {
            return callback(false, '不允许的操作');
        }
        // 修改订单状态
        $model_order->editOrder($update,array('order_id' => $order_info['order_id']));

        return callback(true, '成功');

    }

    /**
     * 买家收货
     * @param array $order_info
     * @param string $role 操作角色 buyer、seller、admin、system,chain 分别代表买家、商家、管理员、系统、门店
     * @param string $user 操作人
     * @param string $msg 操作备注
     * @return array
     */
    public function changeOrderStateReceive($order_info, $role, $user = '', $msg = '')
    {
        try {

            $order_id = $order_info['order_id'];
            $model_order = Model('order');

            //添加订单日志
            $data = array();
            $data['order_id'] = $order_id;
            $data['log_role'] = $role;
            $data['log_msg'] = $msg;
            $data['log_user'] = $user;
            $data['log_orderstate'] = ORDER_STATE_SUCCESS;
            $model_order->addOrderLog($data);

            if ($order_info['buyer_id'] > 0 && $order_info['order_amount'] > 0) {
                //添加会员积分
                if (C('points_isuse') == 1) {
                    Model('points')->savePointsLog('order', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'orderprice' => $order_info['order_amount'], 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                }

                //添加会员经验值
                Model('exppoints')->saveExppointsLog('order', array('exp_memberid' => $order_info['buyer_id'], 'exp_membername' => $order_info['buyer_name'], 'orderprice' => $order_info['order_amount'], 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);

                //处理会员类型(完成订单将注册会员变为普通会员)
                //Model()->table('member')->where(array('member_type' => 0, 'member_id' => $order_info['buyer_id']))->update(array('member_type' => 1));
                //Model()->table('member_distribute')->where(array('member_type' => 0, 'member_id' => $order_info['buyer_id']))->update(array('member_type' => 1));

                //会员返佣处理
                $this->_commissionHandle($order_info);
            }
            return callback(true, '操作成功');
        } catch (Exception $e) {
            return callback(false, '操作失败');
        }
    }


    /**
     * 获取订单目前状态允许哪些操作
     * @param $operate string 订单状态代码
     * @param $order_info array 订单数组
     * @return bool
     * */
    public function getOrderOperateState($operate, $order_info)
    {
        if (!is_array($order_info) || empty($order_info)) {
            return false;
        }

        if (isset($order_info['if_'.$operate])) {
            return $order_info['if_'.$operate];
        }

        switch ($operate) {
            //申请退款
            case 'refund_cancel':
                $state = ($order_info['order_state'] == ORDER_STATE_PAY && $order_info['refund_state'] == 0);
                break;

            //平台取消订单
            case 'system_cancel':
                $state = ($order_info['order_state'] == ORDER_STATE_NEW);
                break;

            //平台收款
            case 'system_receive_pay':
                $state = $order_info['order_state'] == ORDER_STATE_NEW && $order_info['refund_state'] == 0;
                $state = $state && $order_info['payment_code'] == 'online' && $order_info['api_pay_time'];
                break;

            //平台确认线下订单
            case 'system_underline_confirm':
                $state = $order_info['order_state'] == ORDER_STATE_UPLOAD_EVIDENCE && $order_info['refund_state'] == 0;
                $state = $state && $order_info['payment_code'] == 'underline';
                break;

            // 线上支付付款
            case 'payment':
                $state = $order_info['order_state'] == ORDER_STATE_NEW && $order_info['payment_code'] == 'online';
                break;

            //发货
            case 'store_send':
                $state = $order_info['order_state'] == ORDER_STATE_PAY && ($order_info['refund_state'] == 1 || $order_info['refund_state'] == 0);
                break;

            //收货
            case 'receive':
                $state = $order_info['order_state'] == ORDER_STATE_SEND;
                break;

            // 商家审核退款申请
            case 'store_cancel':
                $state = $order_info['order_state'] == ORDER_STATE_PAY && $order_info['refund_state'] == 1;
                break;

            //快递跟踪
            case 'deliver':
                $state = !empty($order_info['invoice_no']) && in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS));
                break;

            // 锁定，退款
            case 'lock':
                $state = $order_info['refund_state'] == 1 || $order_info['refund_state'] == 3;
                break;

            // 评价
            case 'evaluation':
                $state = false;
                break;

            // 平台确认退款
            case 'system_refund':
                $state = $order_info['order_state'] == ORDER_STATE_PAY && $order_info['refund_state'] == 3;
                break;
        }
        return $state;
    }

    /**
     * 加密
     * @param array /string $string
     * @param int $member_id
     * @return mixed arrray/string
     */
    public function buyEncrypt($string, $member_id)
    {
        $buy_key = sha1(md5($member_id . '&' . MD5_KEY));
        if (is_array($string)) {
            $string = serialize($string);
        } else {
            $string = strval($string);
        }
        return encrypt(base64_encode($string), $buy_key);
    }


    /**
     * 会员返佣处理
     * @param array $order_info
     */
    private function _commissionHandle($order_info = array())
    {
        //排除拍卖订单
        if (!empty($order_info) && $order_info['order_type'] != 4) {
            //获取订单商品信息
            $order_goods_commis = $this->_getOrderCommis($order_info['order_id']);
            //获取商品返佣比例
//            $goods_model = Model('auctions');
//            $goods = $goods_model->getAuctionsInfoByID($order_goods_commis['goods_id'], 'auction_id,commis_level_1,commis_level_2');
//            $goods['commis_level_1'] = $goods['commis_level_1'] * 1.0 / 100;
//            $goods['commis_level_2'] = $goods['commis_level_2'] * 1.0 / 100;

            /** @var j_member_distributeLogic $member_distribute_logic */
            $member_distribute_logic = Logic('j_member_distribute');

            //处理购买特定专区店铺产品
            //$member_distribute_logic->td_goods_process(array($order_goods_commis));

            //获取分销上级
            $member_relation_info = $member_distribute_logic->get_top_member($order_info['buyer_id']);

            $distribute_list = $member_distribute_logic->get_distribution_info($member_relation_info['member_id']);
            if(!empty($distribute_list)){
                foreach($distribute_list as $v){
                    if($v['goods_num'] > 0){
                        $member_relation = array(
                            'from_id' => $member_relation_info['member_id'],
                            'from_name' => $member_relation_info['member_name'],
                            'to_id' => $v['member_id'],
                            'to_name' => $v['member_name']
                        );
                        $this->_CommisHandle($member_relation,$order_goods_commis,$v['goods_num']);
                    }
                }
            }

            //区域代理佣金记录
            $agent_distribute_info = $member_distribute_logic->get_agent_info($order_info['buyer_id']);
            if(!empty($agent_distribute_info)){
                foreach($agent_distribute_info as $v){
                    if($v['goods_num'] > 0){
//                        $member_relation = array(
//                            'from_id' => $member_relation_info['member_id'],
//                            'from_name' => $member_relation_info['member_name'],
//                            'to_id' => $v['member_id'],
//                            'to_name' => $v['member_name']
//                        );
                        $member_distribute_logic->agent_goods_commission($v,$order_goods_commis,2);
                    }
                }
            }
//
//            $top_member_id = $member_relation_info['top_member'];
//
//            $top_member_number = 0;
//
//            if($top_member_id){//获取商品分销金额
//                $distribute_list = $member_distribute_logic->get_member_distribute($top_member_id, 'good');
//            }
//
//            if($top_member_number != 0){//处理分销
//
//                $member_relation = array(
//                    'from_id' => $member_relation_info['member_id'],
//                    'from_name' => $member_relation_info['member_name'],
//                    'to_id' => $member_relation_info['top_member'],
//                    'to_name' => $member_relation_info['top_member_name']
//                );
//                $this->_CommisHandle($member_relation,$order_goods_commis,$top_member_number);
//            }


            /*
            //处理商户信息
            //list($is_artist, $is_bind, $member_relation) = $this->_sellerHandle($order_info['buyer_id'], $order_info['store_id']);
            $seller_res = $this->_sellerHandle($order_info['buyer_id'],$order_info['store_id']);
            $is_artist = $seller_res['is_artist'];
            $is_bind = $seller_res['is_bind'];
            $member_relation = $seller_res['top_member'];

            //处理返佣
            if ($is_artist && $is_bind) {
                //绑定特技会员艺术家返佣
                $this->_artistSellerHandle($member_relation, $order_goods_commis, $goods);
            } else {
                //普通返佣处理
                $this->_memberCommisHandle($member_relation, $order_goods_commis, $goods);
            }

            */
        }
    }

    /**
     * 卖家类型处理
     * @param int $buyer_id
     * @param int $store_id
     */
    private function _sellerHandle($buyer_id = 0, $store_id = 0)
    {
        $return = array();
        $return['is_artist'] = false;
        $return['is_bind'] = false;
        $return['top_member'] = array();
        if ($store_id > 0 && $buyer_id > 0) {
            $store_model = Model('store');
            $member_model = Model('member');

            $store_info = $store_model->getStoreInfoByID($store_id); //卖家店铺信息
            $s_member_info = $member_model->getMemberInfoByID($store_info['member_id']); //卖家会员信息

            $b_member_info = $member_model->getUpperMember(array('member_id' => $buyer_id)); //买家分销信息
            //通用返佣用户关系处理
            if ($b_member_info['top_member'] > 0) {
                $return['top_member'][0]['from_id'] = $buyer_id;
                $return['top_member'][0]['from_name'] = $b_member_info['member_name'];
                $return['top_member'][0]['to_id'] = $b_member_info['top_member'];
                $return['top_member'][0]['to_name'] = $b_member_info['top_member_name'];
                //获取买家上级会员信息
                $supper_member_info = $member_model->getUpperMember(array('member_id' => $b_member_info['top_member']));
                if ($supper_member_info['top_member'] > 0) {
                    $top_member_info = $member_model->getUpperMember(array('member_id'=>$supper_member_info['top_member']));
                    if(in_array($top_member_info['member_type'],array('2','3'))){
                        $return['top_member'][1]['from_id'] = $buyer_id;
                        $return['top_member'][1]['from_name'] = $b_member_info['member_name'];
                        $return['top_member'][1]['to_id'] = $top_member_info['member_id'];
                        $return['top_member'][1]['to_name'] = $top_member_info['member_name'];
                    }
                }
            }
            //绑定特级会员艺术家返佣用户关系处理
            if ($store_info['store_type'] == 1) {
                $return['is_artist'] = true;
                if (intval($s_member_info['bind_member_id']) > 0) { //已绑定特级会员
                    $return['is_bind'] = true;
                    $return['top_member'][0]['from_id'] = $s_member_info['member_id'];
                    $return['top_member'][0]['from_name'] = $s_member_info['member_name'];
                    $return['top_member'][0]['to_id'] = $s_member_info['bind_member_id'];
                    $return['top_member'][0]['to_name'] = $s_member_info['bind_member_name'];
                    if (isset($return['top_member'][1]))
                        unset($return['top_member'][1]);
                }
            }
        }
        return $return;
    }

    /**
     * 处理艺术家商家返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    private function _artistSellerHandle($member_relation = array(), $order_goods_commis = array(), $goods = array())
    {
        //整理并保存返佣信息
        if($goods['commis_level_1'] > 0){
            $insert_data = $order_goods_commis;
            $insert_data['from_member_id'] = $member_relation[0]['from_id'];
            $insert_data['from_member_name'] = $member_relation[0]['from_name'];
            $insert_data['commission_amount'] = $order_goods_commis['goods_commission'] * $goods['commis_level_1'];
            $insert_data['dis_commis_rate'] = $goods['commis_level_1'] * 100;
            $insert_data['dis_member_id'] = $member_relation[0]['to_id'];
            $insert_data['dis_member_name'] = $member_relation[0]['to_name'];
            $insert_data['add_time'] = time();
            $insert_data['dis_commis_state'] = 0;
            $insert_data['commission_type'] = 2;
            $res = Model()->table('member_commission')->insert($insert_data);
            if($res){
                Model()->table('member')->where(array('member_id'=>$insert_data['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$insert_data['commission_amount'])));
            }
        }
    }

    /**
     * 处理普通返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    private function _memberCommisHandle($member_relation = array(), $order_goods_commis = array(), $goods = array())
    {
        //整理并保存返佣信息
        $insert_data = array();
        $i = 0;
        foreach ($member_relation as $key => $relation) {
            if($key == 0 && $goods['commis_level_1'] <= 0 || $key == 1 && $goods['commis_level_2'] <= 0){
                continue;
            }
            $tmp_data = $order_goods_commis;
            $tmp_data['from_member_id'] = $relation['from_id'];
            $tmp_data['from_member_name'] = $relation['from_name'];
            $tmp_data['commission_amount'] = $order_goods_commis['goods_commission'] * $goods['commis_level_1'];
            $tmp_data['dis_commis_rate'] = $goods['commis_level_1'] * 100;
            if ($key == 1) {
                $tmp_data['commission_amount'] = $order_goods_commis['goods_commission'] * $goods['commis_level_2'];
                $tmp_data['dis_commis_rate'] = $goods['commis_level_2'] * 100;
            }
            $tmp_data['dis_member_id'] = $relation['to_id'];
            $tmp_data['dis_member_name'] = $relation['to_name'];
            $tmp_data['add_time'] = time();
            $tmp_data['dis_commis_state'] = 0;
            $tmp_data['commission_type'] = 2;
            $insert_data[$i] = $tmp_data;
            $i++;
        }
        if(!empty($insert_data)){
            $res = Model()->table('member_commission')->insertAll($insert_data);
            if($res){
                foreach($insert_data as $member){
                    Model()->table('member')->where(array('member_id'=>$member['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$member['commission_amount'])));
                }
            }
        }
    }


    /**
     * 获取订单商品佣金详情
     * @param int $order_id
     */
    private function _getOrderCommis($order_id = 0)
    {
        $commis_amount = array();
        if ($order_id > 0) {
            $order_model = Model('order');
            $order_detail = (array)$order_model->getOrderInfo(array('order_id' => $order_id));
            $commis_amount['order_id'] = $order_id;
            $commis_amount['order_sn'] = $order_detail['order_sn'];
            $commis_amount['order_goods_id'] = $order_detail['auction_id'];
            $commis_amount['goods_id'] = $order_detail['auction_id'];
            $commis_amount['goods_name'] = $order_detail['auction_name'];
            $commis_amount['goods_image'] = $order_detail['auction_image'];
            $commis_amount['goods_pay_amount'] = $order_detail['order_amount'];
            //$commis_amount['goods_commission'] = $order_detail['order_amount'] * ($order_detail['commis_rate'] * 1.0 / 100);
            $commis_amount['goods_commission'] = $order_detail['order_amount'];
            $commis_amount['store_id'] = $order_detail['store_id'];
            $commis_amount['buyer_id'] = $order_detail['buyer_id'];
        }
        return $commis_amount;
    }

    /**
     * 保证现金存入预存款
     * @param $num int 转入金额
     * @param $member_id int 会员ID
     * @param $member_name string 会员名称
     * @return mixed
     * */
    public function margin_to_pd($num, $member_id, $member_name)
    {
        // 转账
        $model_member = Model('member');

        $data = array(
            'available_margin' => array('exp', 'available_margin-'.$num)
        );
        $model_member->editMember(array('member_id' => $member_id), $data);

        // 记录保证金日志
        $this->MarginLog($member_id, $member_name, 6, $num);
        // 记录预存款日志
        $model_pd = Model('predeposit');
        $data_pd['member_id'] = $member_id;
        $data_pd['member_name'] = $member_name;
        $data_pd['amount'] = $num;
        $model_pd->changePd('margin_to_pd',$data_pd);

        return callback(true, '成功');
    }


    /**
     * 处理艺术家商家返佣
     * @param array $member_relation
     * @param array $order_goods_commis
     * @param array $goods_list
     */
    private function _CommisHandle($member_relation = array(), $order_goods_commis = array(), $top_member_number = 0)
    {
        if($top_member_number <= 0){
            return true;
        }

        $insert_data = $order_goods_commis;
        $insert_data['from_member_id'] = $member_relation['from_id'];
        $insert_data['from_member_name'] = $member_relation['from_name'];
        $insert_data['commission_amount'] = $order_goods_commis['goods_commission'] * ($top_member_number * 1.0 / 100);
        $insert_data['dis_commis_rate'] = $top_member_number;
        $insert_data['dis_member_id'] = $member_relation['to_id'];
        $insert_data['dis_member_name'] = $member_relation['to_name'];
        $insert_data['add_time'] = time();
        $insert_data['dis_commis_state'] = 0;
        $insert_data['commission_type'] = 2;
        $res = Model()->table('member_commission')->insert($insert_data);
        if($res){
            Model()->table('member')->where(array('member_id'=>$insert_data['dis_member_id']))->update(array('freeze_commis'=>array('exp','freeze_commis+'.$insert_data['commission_amount'])));
        }
    }

}
