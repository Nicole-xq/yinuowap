<?php
/**
 * 微信消息模板管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class app_push_messageLogic {

    protected $android     = '';
    protected $ios         = '';
    protected $app_class   = false;

    public function __construct(){
        $app_push_set = C('app_push_set');
        if (empty($app_push_set)){
            showMessage('请先配置发送参数');
        }
        $this->android = $app_push_set['android'];
        $this->ios = $app_push_set['ios'];
        if (!$this->app_class){
            require_once(BASE_ROOT_PATH.DS.'ynadmin'.DS.'api'.DS.'umeng'.DS.'Index.php');
            $this->app_class = true;
        }
    }

    /**
     * 发送app消息模板信息
     * @param $templateInfo
     * @param $memberInfo
     * @param $sendMessageData
     * @return array
     */
    public function sendAppMessage($templateInfo,$memberInfo,$sendMessageData) {
        $message_android = [
            'ticker'=>$templateInfo['mmt_app_subject'],
            'title'=>$templateInfo['mmt_app_subject'],
            'text'=>$templateInfo['mmt_app_content'],
            'after_open'=>'go_app',
            'production_mode'=>true,
            'alias_type'=>'YiNuoApp',
            'alias'=>$memberInfo['member_id'],
            'extra'=>[
                'url'=>$templateInfo['mmt_app_link'],
            ],
        ];
        $this->androidCustomizedcast($message_android);
        $message_ios = [
            'alert'=>[
                'title'=>$templateInfo['mmt_app_subject'],
                'subtitle'=>$templateInfo['mmt_app_subject'],
                'body'=>$templateInfo['mmt_app_content']
            ],
            'badge'=> 0,
            'sound'=>'chime',
            'production_mode'=>true,
            'alias_type'=>'YiNuoApp',
            'alias'=>$memberInfo['member_id'],
            'extra'=>[
                'url'=>$templateInfo['mmt_app_link'],
            ],
        ];
        $this->iosCustomizedcast($message_ios);
    }

    /**
     * 发送app广播
     * @param $sendMessageData
     * @return array
     */
    public function sendAppBroadCast($sendMessageData) {


        $message_android = [
            'ticker'=>$sendMessageData['subtitle'],
            'title'=>$sendMessageData['title'],
            'text'=>$sendMessageData['content'],
            'after_open'=>'go_app',
            'production_mode'=>true,
            'extra'=>[
                'url'=>$sendMessageData['link'],
            ],
        ];
        $this->androidBroadcast($message_android);
        $message_ios = [
            'alert'=>[
                'title'=>$sendMessageData['title'],
                'subtitle'=>$sendMessageData['subtitle'],
                'body'=>$sendMessageData['content']
            ],
            'badge'=> 0,
            'sound'=>'chime',
            'production_mode'=>true,
            'extra'=>[
                'url'=>$sendMessageData['link'],
            ],
        ];
        $this->iOSBroadcast($message_ios);
    }

    /**
     * 单播-Android
     * @param array $message_body[
     * 'device_tokens'=>'表示指定的单个设备'
     * 'ticker'=>'通知栏提示文字'
     * 'title'=>'通知标题'
     * 'text'=>'通知文字描述'
     * 'after_open'=>'默认go_app打开应用'
     * 'production_mode'=>'可选 正式/测试模式（true/false）'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function androidUnicast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        if (!isset($message_body['after_open'])){
            $message_body['after_open'] = 'go_app';
        }
        $push = new Index($this->android['app_key'], $this->android['app_master_secret']);
        return $push->sendAndroidUnicast($message_body);
    }

    /**
     * 组播-Android
     * @param array $message_body[
     * 'ticker'=>'通知栏提示文字'
     * 'title'=>'通知标题'
     * 'text'=>'通知文字描述'
     * 'where'=>'筛选条件'
     * 'after_open'=>'默认go_app打开应用'
     * 'production_mode'=>'可选 正式/测试模式（true/false）'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function androidGroupcast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        if (!isset($message_body['after_open'])){
            $message_body['after_open'] = 'go_app';
        }
        $push = new Index($this->android['app_key'], $this->android['app_master_secret']);
        return $push->sendAndroidGroupcast($message_body);
    }


    /**
     * 自定义广播-Android
     * @param array $message_body[
     * 'ticker'=>'通知栏提示文字'
     * 'title'=>'通知标题'
     * 'text'=>'通知文字描述'
     * 'alias_type'=>'别名'
     * 'alias'=>'别名值'
     * 'after_open'=>'默认go_app打开应用'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function androidCustomizedcast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        if (!isset($message_body['after_open'])){
            $message_body['after_open'] = 'go_app';
        }
        $push = new Index($this->android['app_key'], $this->android['app_master_secret']);
        return $push->sendAndroidCustomizedcast($message_body);
    }
    /**
     * 广播-Android
     * @param array $message_body[
     * 'device_tokens'=>'表示指定的单个设备'
     * 'ticker'=>'通知栏提示文字'
     * 'title'=>'通知标题'
     * 'text'=>'通知文字描述'
     * 'after_open'=>'默认go_app打开应用'
     * 'production_mode'=>'可选 正式/测试模式（true/false）'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function androidBroadcast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        if (!isset($message_body['after_open'])){
            $message_body['after_open'] = 'go_app';
        }
        $push = new Index($this->android['app_key'], $this->android['app_master_secret']);
        return $push->sendAndroidBroadcast($message_body);
    }

    /**
     * 单播-IOS
     * @param array $message_body[
     * 'device_tokens'=>'表示指定的单个设备'
     * 'alert'=>'通知栏提示文字'
     * 'badge'=>'可选填默认0'
     * 'sound'=>'默认chime声音'
     * 'production_mode'=>'可选 正式/测试模式（true/false）'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function iOSUnicast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        $push = new Index($this->ios['app_key'], $this->ios['app_master_secret']);
        return $push->sendIOSUnicast($message_body);
    }

    /**
     * 广播-IOS
     * @param array $message_body[
     * 'device_tokens'=>'表示指定的单个设备'
     * 'alert'=>'通知栏提示文字'
     * 'badge'=>'可选填默认0'
     * 'sound'=>'默认chime声音'
     * 'production_mode'=>'可选 正式/测试模式（true/false）'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function iOSBroadcast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        $push = new Index($this->ios['app_key'], $this->ios['app_master_secret']);
        return $push->sendIOSBroadcast($message_body);
    }

    /**
     * 自定义广播-Ios
     * @param array $message_body[
     * 'ticker'=>'通知栏提示文字'
     * 'title'=>'通知标题'
     * 'text'=>'通知文字描述'
     * 'alias_type'=>'别名'
     * 'alias'=>'别名值'
     * 'after_open'=>'默认go_app打开应用'
     * 'extra'=>['key'=>'value',  ...]
     * ]
     * @return array
     */
    public function iosCustomizedcast($message_body){
        if (empty($message_body)){
            return callback(false, '推送数据不为空');
        }
        if (!isset($message_body['production_mode'])){
            $message_body['production_mode'] = true;
        }
        if (!isset($message_body['after_open'])){
            $message_body['after_open'] = 'go_app';
        }
        $push = new Index($this->ios['app_key'], $this->ios['app_master_secret']);
        return $push->sendIOSCustomizedcast($message_body);
    }

}
