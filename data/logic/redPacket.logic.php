<?php
/**
 * 拍卖拍品
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/23 0023
 * Time: 14:15
 * @author ADKi
 */

defined('InShopNC') or exit('Access Invalid!');

class redPacketLogic
{
    /**
     * 判断该红包是否可用于当前拍品支付保证金
     * @param $auctionId
     * @param $redPacketId
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function checkMarginEnabled($auctionId, $redPacketId)
    {
        $return = [
            'state' => true,
            'data' => ''
        ];
        $auctionInfo = Model('auctions')->getAuctionsInfoByID($auctionId);
        if (empty($auctionInfo)) {
            $return['state'] = false;
            $return['data'] = '拍品不存在';
            return $return;
        }
        $packetWhere = ['rpacket_id' => $redPacketId];
        $redPacketInfo = Model('redpacket')->getRedpacketInfo($packetWhere);
        //检测红包装填信息,通过返回红包模板信息
        $result = $this->checkRedPacket($redPacketInfo);
        if ($result['state'] === false) {
            return $result;
        }
        Model('auctions');
        Model('redpacket');
        if ($redPacketInfo['rpacket_t_type'] == redpacketModel::REDPACKET_BEWBIE) {
            if ($auctionInfo['auction_type'] != auctionsModel::AUCTION_TYPE_NEWBIE) {
                $return['state'] = false;
                $return['data'] = '红包仅限新手专区可用';
                return $return;
            }
        }
        Model('margin_orders');
        $existWhere = [
            'auction_id' => $auctionId,
            'buyer_id' => $redPacketInfo['rpacket_owner_id'],
            'order_state' => margin_ordersModel::ORDER_STATE_PREPAID
        ];
        $existWhere['red_packet_id'] = ['neq', 0];
        $packetExist = Model('margin_orders')->where($existWhere)->count();
        if ($packetExist >= 1) {
            $return['state'] = false;
            $return['data'] = '同一个拍品只能使用一个红包';
            return $return;
        }
        $tplInfo = $result['data'];
        if ($auctionInfo['auction_type'] != auctionsModel::AUCTION_TYPE_NEWBIE) {
            $return['state'] = false;
            $return['data'] = '非新手专区暂不支持红包';
            return $return;
        }
        if ($tplInfo['rpacket_t_special_type'] != redpacketModel::MARGIN_RED_PACKET) {
            $return['state'] = false;
            $return['data'] = '该代金券只能用于保证金';
            return $return;
        }
        $return['data'] = $redPacketInfo;
        return $return;
    }

    /**
     * 红包代金券使用
     * @param $redPacketId
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function redPacketUsed($redPacketId)
    {
        if (empty($redPacketId)) {
            return false;
        }
        $redPacketModel = Model('redpacket');
        $update = ['rpacket_state' => $redPacketModel::PACKET_STATE_USED];
        return $redPacketModel->where(['rpacket_id' => $redPacketId])->update($update);
    }

    /**
     * 检测红包装填信息,通过返回红包模板信息
     * @param $redPacketInfo
     * @return array
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function checkRedPacket($redPacketInfo)
    {
        $return = [
            'state' => true,
            'data' => ''
        ];
        if (empty($redPacketInfo)) {
            $return['state'] = false;
            $return['data'] = '代金券信息不存在';
            return $return;
        }
        if ($redPacketInfo['rpacket_state'] == redpacketModel::PACKET_STATE_USED) {
            $return['state'] = false;
            $return['data'] = '代金券已使用';
            return $return;
        }
        if ($redPacketInfo['rpacket_end_date'] < time()) {
            $return['state'] = false;
            $return['data'] = '代金券已过期';
            return $return;
        }
        $tplWhere = ['rpacket_t_id' => $redPacketInfo['rpacket_t_id']];
        $tmlInfo = Model('redpacket')->getRptTemplateInfo($tplWhere);
        if (empty($tmlInfo)) {
            $return['state'] = false;
            $return['data'] = '代金券扩展信息不存在';
            return $return;
        }
        $return['data'] = $tmlInfo;
        return $return;
    }
}