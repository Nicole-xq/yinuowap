<?php
/**
 * 通知
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class noticeLogic {

    public function get_redpacket_notice($member_id){
        $list = Model('redpacket_notice')->get_notice($member_id);
//        print_R($list);exit;
        $re = array();
        if(!empty($list)){
            foreach($list as $v){
                $info = Model('redpacket')->getRedpacketInfo(array('rpacket_id'=>$v['redpacket_id']),'rpacket_title,rpacket_desc,rpacket_start_date,rpacket_end_date,rpacket_price,rpacket_limit,rpacket_active_date');
                $info['rpacket_end_date_text'] = date('Y-m-d H:i:s',$info['rpacket_end_date']);
                $re[] = $info;
            }
        }
        return $re;
    }
}
