<?php
/**
 * 支付行为
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class paymentLogic {

    /**
     * 取得实物订单所需支付金额等信息
     * @param int $pay_sn
     * @param int $member_id
     * @return array
     */
    public function getRealOrderInfo($pay_sn, $member_id = null) {

        //验证订单信息
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition = array();
        $condition['pay_sn'] = $pay_sn;
        if (!empty($member_id)) {
            $condition['buyer_id'] = $member_id;
        }
        $order_pay_info = $model_order->getOrderPayInfo($condition,true);
        if(empty($order_pay_info)){
            return callback(false,'该支付单不存在');
        }

        $order_pay_info['subject'] = '实物订单_'.$order_pay_info['pay_sn'];
        $order_pay_info['order_type'] = 'real_order';

        $condition = array();
        $condition['pay_sn'] = $pay_sn;

        //同步异步通知时,预定支付尾款时需要用到已经支付状态
        $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
        $order_list = $model_order->getOrderList($condition,'','*','','',array(),true);

        //取订单其它扩展信息
        $result = $this->getOrderExtendList($order_list);
        if (!$result['state']) {
            return $result;
        }

        $order_pay_info['order_list'] = $order_list;
        $order_pay_info['if_buyer_repay'] = $result['data']['if_buyer_repay'];

        return callback(true,'',$order_pay_info);
    }

    /**
     * 取得订单其它扩展信息
     * @param unknown $order_list
     * @param string $role 操作角色 目前只用admin时需要传入
     */
    public function getOrderExtendList(& $order_list,$role = '') {

        //预定订单
        if ($order_list[0]['order_type'] == 2) {
            //原值需要记录一下[后面会被清空]，最后订单表需要记录再次支付的总额，因为退款会用到 2015/07/09
            $order_list[0]['original_pd_amount'] = $order_list[0]['pd_amount'];
            $order_list[0]['original_rcb_amount'] = $order_list[0]['rcb_amount'];

            $order_info = $order_list[0];
            $result = Logic('order_book')->getOrderBookInfo($order_info);
            if (!$result['data']['if_buyer_pay'] && $role != 'admin') {
                return callback(false,'未找到需要支付的订单');
            }
            $order_list[0] = $result['data'];
            $order_list[0]['order_amount'] = $order_list[0]['pay_amount'];
            
            //如果是支付尾款，则把订单状态更改为未支付状态，方便执行统一支付程序
            if ($result['data']['if_buyer_repay']) {
                $order_list[0]['order_state'] = ORDER_STATE_NEW;
            }

            //当以下情况时不需要清除数据pd_amount,rcb_amount：
            //如果第2次支付尾款，并且已经锁定了站内款
            //当以下情形时清除站内余额数据pd_amount,rcb_amount：
            //如果第1次支付，两个均为空，如果第1.5次支付，不会POST扣款标识不会重复扣站内款，不需要该值，所以可以清空
            //如果第2次支付尾款，如果第一次选择站内支付，也需要清空原来的支付定金的金额
            if (!$order_list[0]['if_buyer_pay_lock']) {
                $order_list[0]['pd_amount'] = $order_list[0]['rcb_amount'] = 0;
            }
        }
        return callback(true);
    }

    /**
     * 取得虚拟订单所需支付金额等信息
     * @param int $order_sn
     * @param int $member_id
     * @return array
     */
    public function getVrOrderInfo($order_sn, $member_id = null) {

        //验证订单信息
        $model_order = Model('vr_order');
        $condition = array();
        $condition['order_sn'] = $order_sn;
        if (!empty($member_id)) {
            $condition['buyer_id'] = $member_id;
        }
        //同步异步通知时需要用到已经支付状态
        $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
        $order_info = $model_order->getOrderInfo($condition);
        if(empty($order_info)){
            return callback(false,'该订单不存在');
        }

        $order_info['subject'] = '虚拟订单_'.$order_sn;
        $order_info['order_type'] = 'vr_order';
        $order_info['pay_sn'] = $order_sn;

        return callback(true,'',$order_info);
    }

    /**
     * 取得充值单所需支付金额等信息
     * @param int $pdr_sn
     * @param int $member_id
     * @return array
     */
    public function getPdOrderInfo($pdr_sn, $member_id = null) {

        $model_pd = Model('predeposit');
        $model_pd = new predepositModel();
        $condition = array();
        $condition['pdr_sn'] = $pdr_sn;
        if (!empty($member_id)) {
            $condition['pdr_member_id'] = $member_id;
        }
        $order_info = $model_pd->getPdRechargeInfo($condition);
        if(empty($order_info)){
            return callback(false,'该订单不存在');
        }

        $order_info['subject'] = '预存款充值_'.$order_info['pdr_sn'];
        $order_info['order_type'] = 'pd_order';
        $order_info['pay_sn'] = $order_info['pdr_sn'];
        $order_info['api_pay_amount'] = $order_info['pdr_amount'];
        return callback(true,'',$order_info);
    }

    /**
     * 取得保证金订单所需支付金额等信息
     * @param $order_sn int 订单编号
     * @param $member_id int 会员ID
     * @return mixed
     * */
    public function getMgOrderInfo($order_sn, $member_id = null)
    {
        /** @var margin_ordersModel $model_margin_orders */
        $model_margin_orders = Model('margin_orders');

        $condition = array();
        $condition['order_sn'] = $order_sn;
        if (!empty($member_id)) {
            $condition['buyer_id'] = $member_id;
        }

        //同步异步通知时需要用到已经支付状态
        $condition['order_state'] = array('in',array(3, 0, 1));
        $order_info = $model_margin_orders->getOrderInfo($condition);

        if(empty($order_info)){
            return callback(false,'该订单不存在');
        }

        $order_info['subject'] = '保证金订单_'.$order_sn;
        $order_info['is_margin'] = true;
        $order_info['pay_sn'] = $order_sn;

        return callback(true,'',$order_info);
    }
    
    /**
     * 取得拍卖尾款订单所需支付金额等信息
     * @param $order_sn int 订单编号
     * @param $member_id int 会员ID
     * @return mixed
     * */
    public function getAtOrderInfo($order_sn, $member_id = null)
    {
        $model_order = Model('order');
        $condition = array();
        $condition['order_sn'] = $order_sn;
        if (!empty($member_id)) {
            $condition['buyer_id'] = $member_id;
        }

        //同步异步通知时需要用到已经支付状态
        $condition['order_state'] = array('in',array(10, 20));
        $order_info = $model_order->getOrderInfo($condition);

        if(empty($order_info)){
            return callback(false,'该订单不存在');
        }

        $order_info['subject'] = '拍卖订单_'.$order_sn;
        $order_info['is_margin'] = false;
        $order_info['pay_sn'] = $order_sn;

        return callback(true,'',$order_info);
    }

    /**
     * 取得所使用支付方式信息
     * @param unknown $payment_code
     */
    public function getPaymentInfo($payment_code) {
        if (in_array($payment_code,array('offline','predeposit')) || empty($payment_code)) {
            return callback(false,'系统不支持选定的支付方式');
        }
        $model_payment = Model('payment');
        $condition = array();
        $condition['payment_code'] = $payment_code;

        $payment_info = $model_payment->getPaymentOpenInfo($condition);

        if(empty($payment_info)) {
            return callback(false,'系统不支持选定的支付方式');
        }

        if ($payment_code != 'underline') {
            $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$payment_info['payment_code'].DS.$payment_info['payment_code'].'.php';
            if(!file_exists($inc_file)){
                return callback(false,'系统不支持选定的支付方式');
            }
            require_once($inc_file);
            $payment_info['payment_config'] = unserialize($payment_info['payment_config']);
        }
        return callback(true,'',$payment_info);
    }

    /**
     * 支付成功后修改实物订单状态
     */
    public function updateRealOrder($out_trade_no, $payment_code, $order_list, $trade_no, $pay_amount = 0) {
        $post['payment_code'] = $payment_code;
        $post['trade_no'] = $trade_no;
        $post['api_pay_amount'] = $pay_amount;
        /** @var orderLogic $order_logic */
        $order_logic = Logic('order');
        return $order_logic->changeOrderReceivePay($order_list, 'system', '系统', $post);
    }

    /**
     * 支付成功后修改虚拟订单状态
     */
    public function updateVrOrder($out_trade_no, $payment_code, $order_info, $trade_no) {
        $post['payment_code'] = $payment_code;
        $post['trade_no'] = $trade_no;
        return Logic('vr_order')->changeOrderStatePay($order_info, 'system', $post);
    }

    /**
     * 支付成功后修改保证金订单状态
     * @param $payment_code string 支付方式代码
     * @param $order_info array 订单信息
     * @param $trade_no int 第三方支付订单号
     * @return mixed
     * */
    public function updateMgOrder($payment_code, $order_info, $trade_no)
    {
        $post['payment_code'] = $payment_code;
        $post['trade_no'] = $trade_no;
        /** @var auction_orderLogic $auction_order_logic */
        $auction_order_logic = Logic('auction_order');
        return $auction_order_logic->changeOrderReceivePay($order_info, 'system', '', $post);
    }

    /**
     * 支付成功后修改拍卖订单状态
     * @param $payment_code string 支付方式代码
     * @param $order_info array 订单信息
     * @param $trade_no int 第三方支付订单号
     * @return mixed
     * */
    public function updateAtOrder($payment_code, $order_info, $trade_no)
    {
        $post['payment_code'] = $payment_code;
        $post['trade_no'] = $trade_no;
        /** @var auction_orderLogic $auction_order */
        $auction_order = Logic('auction_order');
        return $auction_order->changeAuctionOrderReceivePay($order_info, 'system', '', $post);
    }

    /**
     * 支付成功后修改充值订单状态
     * @param unknown $out_trade_no
     * @param unknown $trade_no
     * @param unknown $payment_info
     * @throws Exception
     * @return multitype:unknown
     */
    public function updatePdOrder($out_trade_no,$trade_no,$payment_info,$recharge_info) {
		$model_pd = Model('predeposit');
        $condition = array();
        $condition['pdr_sn'] = $recharge_info['pdr_sn'];
        $condition['pdr_payment_state'] = 0;
        $pd_order_info = $model_pd->getPdRechargeInfo($condition);
        if (empty($pd_order_info)) {
            return callback(true);
        }
        try {
            $model_pd->beginTransaction();
            $pd_order_info = $model_pd->getPdRechargeInfo(array('pdr_id'=>$pd_order_info['pdr_id']),'*',true);
            if ($pd_order_info['pdr_payment_state'] == 1) {
                return callback(true);
            }
            $update = array();
            $update['pdr_payment_state'] = 1;
            $update['pdr_payment_time'] = TIMESTAMP;
            $update['pdr_payment_code'] = $payment_info['payment_code'];
            $update['pdr_payment_name'] = $payment_info['payment_name'];
            $update['pdr_trade_sn'] = $trade_no;

            //更改充值状态
            $state = $model_pd->editPdRecharge($update,$condition);
            if (!$state) {
                throw new Exception('更新充值状态失败');
            }
            //变更会员预存款
            $data = array();
            $data['member_id'] = $recharge_info['pdr_member_id'];
            $data['member_name'] = $recharge_info['pdr_member_name'];
            $data['amount'] = $recharge_info['pdr_amount'];
            $data['pdr_sn'] = $recharge_info['pdr_sn'];
            $model_pd->changePd('recharge',$data);
            //发送充值成功信息
//            $user_info = Model('member')->getMemberInfo(array('member_id'=>$pd_order_info['pdr_member_id']),'member_mobile,member_email');
            $param = array();
            $param['code'] = 'recharge_notice';
            $param['member_id'] = $pd_order_info['pdr_member_id'];
            $param['param'] = array('times' => date('Y.m.d H点i分',TIMESTAMP), 'money' => $pd_order_info['pdr_amount']);
//            $param['number'] = array('mobile' => $user_info['member_mobile'], 'email' => $user_info['member_email']);
            $param['number'] = array('mobile' => '', 'email' => '');
            QueueClient::push('sendMemberMsg', $param);
            $model_pd->commit();
            return callback(true);

        } catch (Exception $e) {
            $model_pd->rollback();
            return callback(false,$e->getMessage());
        }
    }

    /**
     * 包装 兼容代码以后考虑移植到laravel里 -----------------------------------------------------------------------------------------------------
     */

    /**
     * 支付并返回后续支付信息
     * @param $memberId
     * @param $paySn
     * @param array $payDetail
     *  points_pay 诺币
        rcb_pay 充值卡
        pd_pay 余额
        password
        inapp
     * @param int $isApp
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function toPayGetResultOrderPayInfo($memberId, $paySn, $payDetail = array(),$isApp = 0) {
        yLog()->debug("toPayGetResultOrderPayInfo_param", func_get_args());
        /** @var paymentLogic $logic_payment */
        //取订单信息
        $result = $this->getRealOrderInfo($paySn, $memberId);
        if(!$result['state']) {
            \App\Exceptions\ApiResponseException::throwFailMsg($result['msg']);
        }

        //站内余额支付
        if ($payDetail) {
            $result['data']['order_list'] = $this->_pdPay($memberId, $result['data']['order_list'],$payDetail);
        }

        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        $pay_order_id_list = array();
        if (!empty($result['data']['order_list'])) {
            foreach ($result['data']['order_list'] as $order_info) {
                if ($order_info['order_state'] == ORDER_STATE_NEW) {
                    $pay_amount += $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] -$order_info['points_amount'];
                    $pay_amount = floatval(ncPriceFormat($pay_amount));
                    $pay_order_id_list[] = $order_info['order_id'];
                }
            }
        }

        if ($pay_amount == 0) {
            if (is_array($payDetail) && isset($payDetail['inapp'])) {
                \App\Exceptions\ApiResponseException::throwFailMsg('pay_amount_zero');
            } else {
                if($isApp == 0){
                    redirect(WAP_SITE_URL.'/tmpl/member/order_list.html');
                }
            }
        }

        $result['data']['api_pay_amount'] = ncPriceFormat($pay_amount);
        if($payDetail['points_pay'] == 1){
            $update = Model('order')->editOrder(array('api_pay_time'=>TIMESTAMP,'is_points'=>1),array('order_id'=>array('in',$pay_order_id_list)));
        }else{
            $update = Model('order')->editOrder(array('api_pay_time'=>TIMESTAMP),array('order_id'=>array('in',$pay_order_id_list)));
        }

        if(!$update) {
            \App\Exceptions\ApiResponseException::throwFailMsg('更新订单信息发生错误，请重新支付');
        }

        //如果是开始支付尾款，则把支付单表重置了未支付状态，因为支付接口通知时需要判断这个状态
        if ($result['data']['if_buyer_repay']) {
            $update = Model('order')->editOrderPay(array('api_pay_state'=>0),array('pay_id'=>$result['data']['pay_id']));
            if (!$update) {
                \App\Exceptions\ApiResponseException::throwFailMsg('订单支付失败');
            }
            $result['data']['api_pay_state'] = 0;
        }
        return $result;
    }

    /**
     * 支付宝支付
     * @param $subject
     * @param $paySn
     * @param $apiPayAmount
     * @param null $disablePayChannels
     * @return array
     */
    public function alipay($subject, $paySn, $apiPayAmount, $disablePayChannels = null){
        yLog()->debug("alipay_param",  [func_get_args()]);
        if (!isNcProduction()){
            //测试机支付金额0.01
            $apiPayAmount = 0.01;
        }
        $payApiBaseDir = BASE_ROOT_PATH . DS . 'mobile';
        $inc_file = $payApiBaseDir . DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'AopClient.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require_once($inc_file);
        $aop = new AopClient;
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        $aop->appId = 2017063007608325;
        $aop->rsaPrivateKey = 'MIICXAIBAAKBgQCuVM6xnG1mmjBh8k5weX7vP4NUwZCvGHO16P2HsETBChXvQwvPXe3aQYitNL7kU0S9X7VBV+3LxPF6jqJ2NyPE6JlTLafEeQddL3vznXlGv4ijgxiG78E9mOO6kleLxiEQtihQQhQHu3dRZrhPV0VUtjdU/f9FOszwkwhu2R5odQIDAQABAoGAFDSoMFXdKEM+0FtxVAABAmHuKui0iFbhMnhqyktX7LfqiuzOdZ6BbwahfNHcPkKYeQqay5QRb8jH5Fib4+/IKV11WCvC5kGM1bjqO8QpByd+RAulMnmEX7M/l/lb3KDJ6bEeZgM7F95gu2owNJXTqbyUJ0mBgawJ+SbMr3qSqhECQQDdkomUjfM0yZ5J0kYNYPLjG3e4aUTnIWh9TE47ae9awC9MoVCwU8O47yqQHqao0DoMw+qq/QvR0Zo3T4AJ1Ej3AkEAyWsqFUpHv1nx7ig9UnYKxxQ2pZIRaVIB4G9G1LWZTAR0HaCkHV/oj4VCpSN5S6KMkPyneJfjfEVxBjE9SviK8wJABho8GdBTC3gmGOhmr4WlCuY9xOF5WVhNNW49lVtUkU5LvzOOMl0MPfKwXGnLs0iQ4Lsgonb3tV6tfap930dufwJBALNsLxTAEqG2cfkBB39Jf9hPfU6Ii9ISJ3HSLnqVOnWpEfbCfu9b3ELdJr0MmKRzrFwLdPPL+e1dvo0Rl9QNC1kCQDF8LVsaC3Tjs+xtGh9a3k1lkXiVmLYflgt0350W/hhy7I62SpBhBQgPJk2ajAM9r5ELvXwnfamw546VtqLdSis=';
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA";
        $aop->alipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB';
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        require_once($payApiBaseDir .DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'request'.DS.'AlipayTradeAppPayRequest.php');
        $request = new AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $tmp = array(
            'body'=>'r',
            'timeout_express'=>'30m',
            'subject'=>$subject,
            'out_trade_no'=>$paySn,
            'total_amount'=>$apiPayAmount,
            'product_code'=>'QUICK_MSECURITY_PAY'
        );
        if($disablePayChannels){
            $tmp['disable_pay_channels'] = $disablePayChannels;
        }
        $bizcontent = json_encode($tmp);
//        $bizcontent = "{\"body\":\"r\","
//                        . "\"subject\": \"{$pay_info['subject']}\","
//                        . "\"out_trade_no\": \"{$pay_info['pay_sn']}\","
//                        . "\"timeout_express\": \"30m\","
//                        . "\"total_amount\": \"{$pay_info['api_pay_amount']}\","
//                        . "\"product_code\":\"QUICK_MSECURITY_PAY\""
//                        . "}";
        $request->setNotifyUrl(MOBILE_SITE_URL.'/api/payment/alipay_aopclient/notify_url.php');
        $request->setBizContent($bizcontent);
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
//        echo htmlspecialchars($response);//就是orderString 可以直接给客户端请求，无需再做处理。
        return (array('signStr'=>$response,'code'=>$apiPayAmount > 0?0:1000));
    }

    /**
     * 获取微信支付参数
     * @param $payParam
     *       amount 单位是分
     * @param string $wxPayType
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function getWxPayInfo($payParam, $wxPayType = 'wxpay') {
        yLog()->debug("getWxPayInfo_param",  [func_get_args()]);
        if(!isNcProduction()){
            $payParam['amount'] = 1;
        }
        $noncestr = md5(rand());
        $mbPaymentInfo = Model('mb_payment')->getMbPaymentOpenInfo(['payment_code'=>$wxPayType]);
        $paymentConfig = $mbPaymentInfo['payment_config'];
        $param = array();
        $param['appid'] = $paymentConfig['wxpay_appid'];
        $param['mch_id'] = $paymentConfig['wxpay_partnerid'];
        $param['nonce_str'] = $noncestr;
        $param['body'] = $payParam['subject'];
        $param['out_trade_no'] = $payParam['pay_sn'];
        $param['total_fee'] = $payParam['amount'];
        $param['spbill_create_ip'] = get_server_ip();
        $param['notify_url'] = MOBILE_SITE_URL . '/api/payment/wxpay3/notify_url.php';
        $param['trade_type'] = 'APP';
        $payParam['no_credit'] ? $param['limit_pay'] = 'no_credit' : '';
        $sign = $this->_getWxPaySign($param, $paymentConfig);
        $param['sign'] = $sign;
        $post_data = '<xml>';
        foreach ($param as $key => $value) {
            $post_data .= '<' . $key .'>' . $value . '</' . $key . '>';
        }
        $post_data .= '</xml>';
        $prepay_result = http_postdata('https://api.mch.weixin.qq.com/pay/unifiedorder', $post_data);
        $prepay_result = simplexml_load_string($prepay_result, 'SimpleXMLElement', LIBXML_NOCDATA);
        if($prepay_result->return_code != 'SUCCESS') {
            yLog()->error("getWxPayInfoError",[@json_decode(@json_encode($prepay_result), true), func_get_args() ]);
            \App\Exceptions\ApiResponseException::throwFailMsg('支付失败code:1002');
        }

        // 生成正式支付参数
        $data = array();
        $data['appid'] = $paymentConfig['wxpay_appid'];
        $data['noncestr'] = $noncestr;
        //微信修改接口参数，否则IOS报解析失败
        //$data['package'] = 'prepay_id=' . $prepay_result->prepay_id;
        $data['package'] = 'Sign=WXPay';
        $data['partnerid'] = $paymentConfig['wxpay_partnerid'];
        $data['prepayid'] = (string)$prepay_result->prepay_id;
        $data['timestamp'] = TIMESTAMP;
        $sign = $this->_getWxPaySign($data, $paymentConfig);
        $data['sign'] = $sign;
        return $data;
    }

    private function _getWxPaySign($param, $paymentConfig) {
        ksort($param);
        $string = '';
        foreach ($param as $key => $val) {
            $string .= $key . '=' . $val . '&';
        }
        $string .= 'key=' . $paymentConfig['wxpay_partnerkey'];
        return strtoupper(md5($string));
    }

    /**
     * 站内余额支付(充值卡、预存款支付) 实物订单
     * @param $memberId
     * @param $order_list
     * @param array $payDetail
     *  points_pay
        rcb_pay
        pd_pay
        password
        inapp
     * @return mixed
     * @throws \App\Exceptions\ResponseException
     */
    private function _pdPay($memberId, $order_list, $payDetail) {
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($memberId);
        //如果使用 诺币 充值卡 余额支付 校验密码
        if(!empty($payDetail["points_pay"]) || !empty($payDetail["rcb_pay"]) || !empty($payDetail["pd_pay"])){
            if (empty($buyer_info['member_paypwd'])){
                \App\Exceptions\ApiResponseException::throwFailMsg("未设置支付密码,请先设置支付密码");
            }
            if($buyer_info['member_paypwd'] != md5($payDetail['password'])){
                \App\Exceptions\ApiResponseException::throwFailMsg("支付密码错误");
            }
        }
        if($payDetail['points_pay'] == 1){
            $payDetail['points_amount'] = $buyer_info['member_points']/100;
        }else{
            $payDetail['points_amount'] =  0;
        }

        if (empty($payDetail['password']) && $payDetail['points_amount'] <= 0) {
            return $order_list;
        }

        if (($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($payDetail['password'])) && $payDetail['points_amount'] <=0) {
            return $order_list;
        }

        if ($buyer_info['available_rc_balance'] == 0) {
            $payDetail['rcb_pay'] = null;
        }
        if ($buyer_info['available_predeposit'] == 0) {
            $payDetail['pd_pay'] = null;
        }
        if (floatval($order_list[0]['rcb_amount']) > 0 || floatval($order_list[0]['pd_amount']) > 0) {
            return $order_list;
        }

        try {
            $model_member->beginTransaction();
            /** @var buy_1Logic $logic_buy_1 */
            $logic_buy_1 = Logic('buy_1');
            //使用充值卡支付
            if (!empty($payDetail['rcb_pay'])) {
                $order_list = $logic_buy_1->rcbPay($order_list, $payDetail, $buyer_info,$payDetail['points_amount']);
            }

            //使用预存款支付
            if (!empty($payDetail['pd_pay'])) {
                $order_list = $logic_buy_1->pdPay($order_list, $payDetail, $buyer_info,$payDetail['points_amount']);
            }

            if(empty($payDetail['pd_pay']) && empty($payDetail['rcb_pay']) && $payDetail['points_amount'] >0){
                $order_list = $logic_buy_1->nbPay($order_list, $payDetail, $buyer_info,$payDetail['points_amount']);
            }

            //特殊订单站内支付处理
            $logic_buy_1->extendInPay($order_list);

            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            !isNcProduction() && yLog()->debug(__CLASS__.__FUNCTION__." error", [func_get_args(), showExceptionTrace($e, false, true, false)]);
            \App\Exceptions\ApiResponseException::throwFailMsg($e->getMessage());
        }
        return $order_list;
    }
    /**
     * 包装 兼容代码以后考虑移植到laravel里 end -----------------------------------------------------------------------------------------------------
     */
}
