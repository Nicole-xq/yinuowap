<?php
/**
 * 出价
 */

defined('InShopNC') or exit('Access Invalid!');

class auction_agentLogic
{
    // 更新出价前的最高出价人
    protected $old_member_id;

    /**
     * 代理出价
     */
    public function setAgentPrice($data, $member_id) {
        $auction_id = intval($data['auction_id']);
        $agent_price = floatval($data['agent_price']);
        $state = 'error';
        $msg = '设置失败';
        $auction_info = $this->getCurrentInfo($auction_id);
        $current_price = $auction_info['current_price'];
        if ($agent_price >= ($auction_info['auction_increase_range']*2 + $current_price)) {
            $model_relation = Model('auction_member_relation');
            $relation_info = $model_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
            if ($relation_info['is_bond'] == 1 && ($agent_price > $relation_info['agent_num'])) {
                $update = array(
                    'is_agent' => 1,
                    'agent_num' => $agent_price
                );
                $condition = array(
                    'auction_id' => $auction_id,
                    'member_id' => $member_id
                );
                $result = $model_relation->setRelationInfo($update, $condition);
                if ($result) {
                    $state = 'succ';
                    $msg = '设置成功';
                    $this->updateCurrentPrice($auction_id);
                }
            }
        }
        $state_data = array(
            'state' => $state,
            'msg' => $msg
            );
        return $state_data;
    }
    
    /**
     * 出价
     */
    public function updateCurrentPrice($auction_id){
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $auction_info = $this->getCurrentInfo($auction_id);
        //拍卖延迟时长  数据库存储单位为min
        $auction_config_model = Model('auction_config');
        $auction_limit_info = $auction_config_model->where(['id' => 1])->find();
        $auction_limit_time = isset($auction_limit_info['add_time'])
            ? $auction_limit_info['add_time'] * 60 : 10 * 60;
        // 拍品真实结束时间(按照系统默认30分钟一次延时)
        if ($auction_info['auction_end_time']-$auction_limit_time < time()) {
            $model_auctions->editAuctions(
                array('auction_end_true_t' => time() + $auction_limit_time),
                array('auction_end_time' => time() + $auction_limit_time),
                array('auction_id' => $auction_id)
            );
        }

        try {//代理出价处理
            $model_auctions->beginTransaction();

            //当前拍卖的最高出价
            $current_price = $auction_info['current_price'];

            if(!$current_price){
                goto END;
            }

            //测试代码
            //$current_price = 6500;

            // 加价幅度
            $increase_range = $auction_info['auction_increase_range'];

            $model_bid_log = Model('bid_log');
            $model_relation = Model('auction_member_relation');
            $condition = array();
            $condition['auction_id'] = $auction_id;
            $condition['is_agent'] = 1;
            // 大于等于 一个加价幅度
            $condition['agent_num'] = array('egt',$current_price + $increase_range);
            // 查询设置代理价的用户
            $list = $model_relation->table('auction_member_relation')->where($condition)->order('agent_num asc')->select();

            if(empty($list)){
                goto END;
            }

            // 获取到当前价最高的出价人
            $member_relation = $model_relation->table('auction_member_relation')->where(array('auction_id'=> $auction_id,'is_offer'=> 1))->order('offer_num desc')->find();
            $current_member_id = $member_relation['member_id'];

            //更新当前最高价格
            $current_price = $member_relation['offer_num'];

            $update_data = array();
            $number = 0;
            $update_price = 0;
            $run = true;
            $last_member_id = 0;

            $current_price = $current_price + $increase_range;

            do{
                if(empty($list)){
                    break;
                }

                if($current_member_id !== null &&count($list) == 1 && $current_member_id == $last_member_id){
                    break;
                }

                //移除代理出价小于当前价格的账号
                foreach($list as $key => $value){

                    //用户设置的代理出价小于当前价格 移除数组
                    if($value['agent_num'] < $current_price){
                        unset($list[$key]);
                        continue;
                    }

                    $last_member_id = $value['member_id'];

                    //用户是当前最高出价用户 继续下一个用户
                    if($value['member_id'] == $current_member_id){
                        continue;
                    }

                    //更新出价次数 和 更新后最高价格
                    $number++;
                    $update_price = $current_price;
                    $current_member_id = $value['member_id'];

                    // 如果是机器人出价，随机取一个机器人
                    if ($value['member_id'] == 0) {
                        goto END;
//                        $robot = Model('robot')->getInfo(array('robot_id'=> array('gt',0)), '*', 'rand()');
//                        //$current_price = $update_price = $value['agent_num'];
//                        $value['member_name'] = $robot['robot_name'];
                    }

                    // 添加出价日志
                    $param = array(
                        'member_id' => $value['member_id'],
                        'auction_id' => $auction_id,
                        'created_at' => time(),
                        'offer_num' => $current_price,
                        'member_name' => $value['member_name'],
                        'is_anonymous' => $value['is_anonymous']
                    );

                    if(!$model_bid_log->addBid($param)){
                        throw new Exception('数据库更新失败!');
                    }

                    $update_data[$value['member_id']] = array(
                        'offer_num' => $current_price,
                        'member_id' => $value['member_id']
                    );

                    $current_price = $current_price + $increase_range;
                }

            }while($run == true);

            //如果没有执行加价 返回结果
            if($number == 0){
                goto  END;
            }

            //更新牌品数据
            $update = array(
                'current_price' => $update_price,
                'bid_number' => array('exp', 'bid_number+'.$number),
                'current_price_time' => time()
            );

            if(!$model_auctions->editAuctions($update, array('auction_id' => $auction_id))){
                throw new Exception('数据库更新失败!');
            }

            //更新拍卖关联关系表
            $filter = array('auction_id' => $auction_id);
            $update_relation = array('is_offer' => 1);
            foreach($update_data as $value){
                $update_relation['offer_num'] = $value['offer_num'];
                $filter['member_id'] = $value['member_id'];
                if(!$model_relation->setRelationInfo($update_relation, $filter)){
                    throw new Exception('数据库更新失败!');
                }
            }

            $model_auctions->commit();
        }catch (Exception $e){
            $model_auctions->rollback();
        }

        END:
        return true;

    }
    
    /**
     * 拍品详细信息
     * @param $auction_id int 拍品ID
     * @return array
     */
    public function getCurrentInfo($auction_id){
        $condition_sql = " auction_id = '".$auction_id."' and (auction_end_time > '".TIMESTAMP."' or auction_end_true_t > '".TIMESTAMP."')";
        $auction_info = Model('auctions')->table('auctions')->where($condition_sql)->master(true)->lock(true)->find();
        if ($auction_info['auction_start_price']>0) {
            if ($auction_info['current_price']<$auction_info['auction_start_price']) $auction_info['current_price'] = $auction_info['auction_start_price'];
            $store_info = Model('store')->getStoreInfoByID($auction_info['store_id']);
            $auction_info['delayed_time'] = intval($store_info['delayed_time'])*60;
        }
        return $auction_info;
    }

    /*
     * 拉取要发送超价通知的会员ID
     * */
    protected function getSendMessage($auction_id, $auction_name)
    {
        /** @var auction_member_relationModel $model_auction_member_relation */
        $model_auction_member_relation = Model('auction_member_relation');
        $condition = [
            'auction_id' => $auction_id,
            'is_offer' => 1,
        ];
        $relation_list = $model_auction_member_relation->getRelationList($condition, "*", '', 'offer_num desc');
        // 过滤掉自己出价超出自己和无人出价
        if (count($relation_list) <= 1) {
            return callback(false,'无需要发送通知的会员');
        }
        $member_id_arr = array();
        $is_old_member = false;
        foreach ($relation_list as $key => $item) {
            if ($key == 0) {
                // 判断最高出价人是否是之前最高出价的人
                if ($item['member_id'] == $this->old_member_id) {
                    $is_old_member = true;
                }
            }else {
                $member_id_arr[$item['member_id']] = $item['member_id'];
            }
        }
        if ($is_old_member) {
            unset($member_id_arr[$this->old_member_id]);
        }
        // 发送通知
        if (!empty($member_id_arr)) {
            foreach ($member_id_arr as $member_id) {
                $param['code'] = 'beyond_price';
                $param['member_id'] = $member_id;
                $url = urlAuction('auctions', 'index', array('id' => $auction_id));
                $param['param'] = array(
                    'auction_url' => $url,
                    'url' => $url,
                    'wx_url' => $url,
                    'auction_name' => $auction_name,
                    'current_price' => $condition['current_price']
                );
                QueueClient::push('sendMemberMsg', $param);
                // 将发送过短信的出价状态设置成 0
                $condition = array('member_id' => $member_id, 'auction_id' => $auction_id);
                $model_auction_member_relation->setRelationInfo(
                    array('is_offer' => 0), $condition
                );
            }
        }
    }
}