<?php
/**
 * 拍卖拍品
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/23 0023
 * Time: 14:15
 * @author ADKi
 */

defined('InShopNC') or exit('Access Invalid!');

class auctionLogic
{
    /**
     * 保存拍品
     * */
    public function saveAuctions($param, $store_id, $store_name, $store_state, $seller_id, $seller_name, $bind_all_gc) {
        $model_auctions = Model('auctions');
        $model_type = Model('type');
        // 验证参数
        $error = $this->_validAuctionParam($param);
        if ($error != '') {
            return callback(false, $error);
        }

        $gc_id = intval($param['cate_id']);
        // 验证拍品分类是否存在且拍品分类是否为最后一级
        $data = Model('goods_class')->getGoodsClassForCacheModel();
        if (!isset($data[$gc_id]) || isset($data[$gc_id]['child']) || isset($data[$gc_id]['childchild'])) {
            return callback(false, '您选择的分类不存在，或没有选择到最后一级，请重新选择分类。');
        }

        // 三方店铺验证是否绑定了该分类
        if (!checkPlatformStoreBindingAllGoodsClass($store_id, $bind_all_gc)) {
            $where = array();
            $where['class_1|class_2|class_3'] = $gc_id;
            $where['store_id'] = $store_id;
            $rs = Model('store_bind_class')->getStoreBindClassInfo($where);
            if (empty($rs)) {
                return callback(false, '您的店铺没有绑定该分类，请重新选择分类。');
            }
        }

        // 根据参数初始化通用拍品数据
        $common_array = $this->_initCommonAuctionByParam($param, $store_id, $store_name, $store_state);
        // 保存拍品数据
        $auction_id = $model_auctions->addAuction($common_array);
        if (!$auction_id) {
            return callback(false, '拍品添加失败');
        }
        //保存属性类型关联
        $model_type->addAuctionsType($auction_id, array('cate_id' => $param['cate_id'], 'type_id' => $param['type_id'], 'attr' => $param['attr']));
        // 生成二维码
        if (!empty($auction_id)) {
            QueueClient::push('createAuctionQRCode', array('store_id' => $store_id, 'auction_id' => $auction_id));
        }

        // 记录日志
        $this->_recordLog('添加拍品，ID:'.$auction_id, $seller_id, $seller_name, $store_id);

        return callback(true, '', $auction_id);
    }

    /**
     * 更新拍品数据
     * */
    public function updateAuctions($param, $store_id, $store_name, $store_state, $seller_id, $seller_name, $bind_all_gc) {
        $model_auctions = Model('auctions');
        $model_type = Model('type');
        $model_auctions_images = Model('auctions_images');
        $auction_id = intval($param['auction_id']);
        $model_type->delAuctionsAttr(array('auction_id' => $auction_id));
        if ($auction_id <= 0) {
            return callback(false, '拍品编辑失败');
        }
        // 验证参数
        $error = $this->_validAuctionParam($param);
        if ($error != '') {
            return callback(false, $error);
        }

        $gc_id = intval($param['cate_id']);
        // 验证拍品分类是否存在且拍品分类是否为最后一级
        $data = Model('goods_class')->getGoodsClassForCacheModel();
        if (!isset($data[$gc_id]) || isset($data[$gc_id]['child']) || isset($data[$gc_id]['childchild'])) {
            return callback(false, '您选择的分类不存在，或没有选择到最后一级，请重新选择分类。');
        }

        // 三方店铺验证是否绑定了该分类
        if (!checkPlatformStoreBindingAllGoodsClass($store_id, $bind_all_gc)) {
            $where = array();
            $where['class_1|class_2|class_3'] = $gc_id;
            $where['store_id'] = $store_id;
            $rs = Model('store_bind_class')->getStoreBindClassInfo($where);
            if (empty($rs)) {
                return callback(false, '您的店铺没有绑定该分类，请重新选择分类。');
            }
        }

        // 根据参数初始化通用拍品数据
        $common_array = $this->_initCommonAuctionByParam($param, $store_id, $store_name, $store_state);

        // 接口不标记字段
        if (APP_ID == 'mobile') {
            unset($common_array['auction_mobile_body']);
        }
        // 更新拍品数据
        $result = $model_auctions->editAuctionsById($common_array, array($auction_id));

        //保存属性类型关联
        $model_type->addAuctionsType($auction_id, array('cate_id' => $param['cate_id'], 'type_id' => $param['type_id'], 'attr' => $param['attr']));

        // 更新拍品主图
        $model_auctions_images->updateImage(array('auction_id' => $auction_id, 'is_default' => 1), array('auction_image' => $common_array['auction_image']));
        
        // 生成二维码
        if (!empty($result)) {
            QueueClient::push('createAuctionQRCode', array('store_id' => $store_id, 'auction_id' => $auction_id));
        } else {
            return callback(false, '拍品编辑失败');
        }

        // 记录日志
        $this->_recordLog('编辑拍品，ID:'.$auction_id, $seller_id, $seller_name, $store_id);

        return callback(true, '', $auction_id);
    }

    /**
     * 验证参数
     */
    private function _validAuctionParam($param) {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array (
                "input" => $param["auction_name"],
                "require" => "true",
                "message" => "请填写拍品名称"
            ),
            array (
                "input" => $param["auction_increase_range"],
                "require" => "true",
                "validator" => "Double",
                "message" => "请填写加价幅度"
            ),
            array (
                "input" => $param["auction_reserve_price"],
                "require" => "true",
                "validator" => "Double",
                "message" => "请填写保留价"
            ),
            array (
                "input" => $param["auction_bond"],
                "require" => "true",
                "validator" => "Double",
                "message" => "请填写保证金"
            ),
            array (
                "input" => $param["auction_start_price"],
                "require" => "true",
                "validator" => "Double",
                "message" => "请填写起拍价"
            ),

        );

        return $obj_validate->validate();
    }

    /**
     * 根据参数初始化通拍品数据
     */
    private function _initCommonAuctionByParam($param, $store_id, $store_name, $store_state) {
        // 分类信息
        $goods_class = Model('goods_class')->getGoodsClassLineForTag(intval($param['cate_id']));
        // 追加拍品店铺ID
        $model_store_vendue = Model('store_vendue');
        $store_vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_id' => $store_id));
        $common_array = array();
        $common_array['auction_name']         = $param['auction_name'];
        $common_array['gc_id']              = intval($param['cate_id']);
        $common_array['gc_id_1']            = intval($goods_class['gc_id_1']);
        $common_array['gc_id_2']            = intval($goods_class['gc_id_2']);
        $common_array['gc_id_3']            = intval($goods_class['gc_id_3']);
        $common_array['gc_name']            = $param['cate_name'];
        $common_array['auction_image']        = $param['image_path'];
        $common_array['auction_start_price']        = ncPriceFormat($param['auction_start_price']);
        $common_array['auction_increase_range']        = ncPriceFormat($param['auction_increase_range']);
        $common_array['auction_bond']        = ncPriceFormat($param['auction_bond']);
        $common_array['auction_reserve_price']        = ncPriceFormat($param['auction_reserve_price']);
        $common_array['auction_number']       = $param['auction_number'];
        $common_array['delivery_mechanism']       = $param['delivery_mechanism'];
        $common_array['auction_attr']         = serialize($param['attr']);
        $common_array['auction_custom']       = serialize($param['custom']);
        $common_array['auction_body']         = $param['auction_body'];
        $common_array['auction_mobile_body']        = $this->_getMobileBody($param['auction_mobile_body']);
        $common_array['state']        = 0;            // 店铺关闭时，拍品下架
        $common_array['auction_add_time']      = TIMESTAMP;
        $common_array['auction_preview_start']     = strtotime($param['auction_preview_start']);
        $common_array['auction_preview_end']     = strtotime($param['auction_start_time']);
        $common_array['auction_start_time']     = strtotime($param['auction_start_time']);
        $common_array['auction_end_time']     = strtotime($param['auction_end_time']);
        $common_array['store_id']           = $store_id;
        $common_array['store_name']         = $store_name;
        $common_array['areaid_1']           = intval($param['province_id']);
        $common_array['areaid_2']           = intval($param['city_id']);
        $common_array['is_own_shop']        = in_array($store_id, Model('store')->getOwnShopIds()) ? 1 : 0;
        // 选择拍品视频后，添加拍品视频链接
        $common_array['auction_video'] = $param['auction_video'];
        $common_array['store_vendue_id'] = $store_vendue_info['store_vendue_id'];

        $common_array['commis_level_1'] = C('auction_commis_1');
        $common_array['commis_level_2'] = C('auction_commis_2');


        // 默认起拍价就是当前价
        $common_array['current_price'] = ncPriceFormat($param['auction_start_price']);
        return $common_array;
    }

    /**
     * 序列化保存手机端拍品描述数据
     */
    private function _getMobileBody($mobile_body) {
        if ($mobile_body != '') {
            $mobile_body = str_replace('&quot;', '"', $mobile_body);
            $mobile_body = json_decode($mobile_body, true);
            if (!empty($mobile_body)) {
                return serialize($mobile_body);
            }
        }
        return '';
    }

    /**
     * 编辑拍品图
     */
    public function editSaveImage($img, $auction_id, $store_id, $seller_id, $seller_name)
    {
        if ($auction_id <= 0 || empty($img)) {
            return callback(false, '参数错误');
        }
        $model_auctions = Model('auctions');
        // 删除原有图片信息
        $model_auctions->delAuctionsImages(array('auction_id' => $auction_id, 'store_id' => $store_id));
        // 保存
        $insert_array = array();
        $k = 0;
        foreach ($img as $key => $value) {
            if ($value['name'] == '') {
                continue;
            }
            // 拍品默认主图
            $update_array = array();        // 更新拍品主图
            $update_where = array();
            $update_array['auction_image']    = $value['name'];
            if ($k == 0 || $value['default'] == 1) {
                $update_array['state']    = 0;
                $update_array['auction_image']    = $value['name'];
                $update_where['auction_id'] = $auction_id;
                $update_where['store_id']       = $store_id;
                // 更新拍品主图
                if(!$model_auctions->editAuctions($update_array, $update_where)) {
                    return callback(false, '拍品图片编辑失败');
                }
            }
            $k++;

            $tmp_insert = array();
            $tmp_insert['auction_id']   = $auction_id;
            $tmp_insert['store_id']         = $store_id;
            $tmp_insert['auction_image']      = $value['name'];
            $tmp_insert['auction_image_sort'] = ($value['default'] == 1) ? 0 : $value['sort'];
            $tmp_insert['is_default']       = $value['default'];
            $insert_array[] = $tmp_insert;

        }
        $rs = $model_auctions->addAuctionsImagesAll($insert_array);
        if ($rs) {
            $this->_recordLog('拍品图片编辑，ID:'.$auction_id, $seller_id, $seller_name, $store_id);
            return callback(true);
        } else {
            return callback(false, '拍品图片编辑失败');
        }
    }

    /**
     * 记录日志
     *
     * @param string $content 日志内容
     * @param int $state 1成功 0失败
     */
    private function _recordLog($content = '', $seller_id, $seller_name, $store_id, $state = 1) {
        $log = array();
        $log['log_content'] = $content;
        $log['log_time'] = TIMESTAMP;
        $log['log_seller_id'] = $seller_id;
        $log['log_seller_name'] = $seller_name;
        $log['log_store_id'] = $store_id;
        $log['log_seller_ip'] = getIp();
        $log['log_url'] = 'auctionsLogic';
        $log['log_state'] = $state;
        $model_seller_log = Model('seller_log');
        $model_seller_log->addSellerLog($log);
    }

    /**
     * 删除拍品
     * @param $auction_id_array array
     * @param $store_id int
     * @param $seller_id int
     * @param $seller_name string
     * @return mixed
     * */
    public function auctionDrop($auction_id_array, $store_id, $seller_id, $seller_name) {
        $return = Model('auctions')->delAuctionsAll(array('auction_id' => array('in', $auction_id_array), 'store_id' => $store_id, 'auction_lock' => 0));
        if ($return) {
            // 添加操作日志
            $this->_recordLog('删除拍品，ID：'.implode(',', $auction_id_array), $seller_id, $seller_name, $store_id);
            return callback(true);
        } else {
            return callback(false, '拍品删除失败');
        }

    }

    /**
     * 拉取拍品出价日志信息
     * @param $auction_id int 拍品ID
     * @param string $limit int 获取个数
     * @param string $page int 每页显示个数
     * @param $last_bid_id int 末尾id 只有大于0才生效
     * @return array
     */
    public function getBidList($auction_id, $limit = '', $page = '', $last_bid_id = 0)
    {
        $model_bid_log = Model('bid_log');
        $condition = array('auction_id' => $auction_id);
        if($last_bid_id > 0){
            $condition['bid_id'] = ['gt', $last_bid_id];
        }
        $bid_log_list = $model_bid_log->getBidList($condition, '*', '', 'bid_id desc', $limit, $page);
        // 处理匿名
        foreach ($bid_log_list as $key => $value) {
            if ($value['is_anonymous'] == 1 || $value['member_id'] == 0) {
                $bid_log_list[$key]['member_name'] = mb_substr($value['member_name'], 0, 1, 'utf-8')."***".mb_substr($value['member_name'], -4, 4, 'utf-8');
            }
        }
//        $showPage = $model_bid_log->showpage();
//        $count_rows = $model_bid_log->getBidCount(array('auction_id' => $auction_id));
        return array(
            'bid_log_list' => $bid_log_list,
//            'showPage' => $showPage,
//            'count_rows' => $count_rows,
        );
    }

    /**
     * 拉取拍品出价日志信息
     * @param $auction_id int 拍品ID
     * @param $limit int 获取个数
     * @param $page int 每页显示个数
     * @return array
     * */
    public function bidListFetch($auction_id, $fields = '*', $limit = '')
    {
        /** @var bid_logModel $model_bid_log */
        $model_bid_log = Model('bid_log');
        $bid_log_list = $model_bid_log->getBidList(
            ['auction_id' => $auction_id],
            $fields,
            '',
            'offer_num desc',
            $limit
        );
        // 处理匿名
        foreach ($bid_log_list as $key => $value) {
            if ($value['is_anonymous'] == 1 || $value['member_id'] == 0) {
                $bid_log_list[$key]['member_name'] = mb_substr($value['member_name'], 0, 1, 'utf-8')
                    . "***" . mb_substr($value['member_name'], -4, 4, 'utf-8');
            }
            $bid_log_list[$key]['member_avatar'] = getUCenterAvatar($value['member_id']);
            $bid_log_list[$key]['commission_amount'] = ncPriceFormat($value['commission_amount']);
            $bid_log_list[$key]['created_at'] = date('Y.m.d H:i', $bid_log_list[$key]['created_at']);
        }
        $count_rows = $model_bid_log->getBidCount(array('auction_id' => $auction_id));
        return array(
            'bid_log_list' => $bid_log_list,
            'count_rows' => $count_rows,
        );
    }


    /**
     * 拉取拍品出价日志信息
     * @param $auction_id int 拍品ID
     * @param $limit int 获取个数
     * @param $page int 每页显示个数
     * @return array
     * */
    public function offerCommissionFetch($condition, $fields = '*', $limit = '')
    {
        /** @var bid_logModel $model_bid_log */
        $model_bid_log = Model('bid_log');
        $bid_log_list = $model_bid_log->getBidList(
            $condition,
            $fields,
            '',
            'created_at asc',
            $limit
        );
        //把已查询的数据进行已读操作
        //$model_bid_log->where($condition)->update(['is_show' => 0]);
        // 处理匿名
        foreach ($bid_log_list as $key => $value) {
            if ($value['is_anonymous'] == 1 || $value['member_id'] == 0) {
                $bid_log_list[$key]['member_name'] = mb_substr($value['member_name'], 0, 3, 'utf-8')
                    . "***" . mb_substr($value['member_name'], -3, 3, 'utf-8');
            }
            //$bid_log_list[$key]['member_avatar'] = getUCenterAvatar($value['member_id']);
            $bid_log_list[$key]['commission_amount'] = ncPriceFormat($value['commission_amount']);
            $bid_log_list[$key]['created_at'] = date('Y.m.d H:i', $bid_log_list[$key]['created_at']);
        }
        if (empty($bid_log_list)) {
            return array(
                'bid_log_list' => [],
                'count_rows' => 0,
            );
        }
        $count_rows = $model_bid_log->getBidCount($condition);
        return array(
            'bid_log_list' => $bid_log_list,
            'count_rows' => $count_rows,
        );
    }




    /**
     * 获取发布流程文章
     * */
    public function getAuctionArticle()
    {
        $model_article = Model('article');
        $article_list = array();

        for ($i = 6; $i<15; $i++) {
            $article_list[$i] = $model_article->getArticleOne(9, $i);
        }

        $data = array(
            'article_list' => $article_list
        );
        return callback(true, '', $data);
    }

    /**
     * 计算计息天数
     * @param $start_time
     * @param $end_time
     * @return float|int
     */
    public function getInterestDay($start_time,$end_time){
//        if($end_time <=$start_time || $start_time == 0){
//            file_put_contents(BASE_DATA_PATH.'/log/wt_test.log',$end_time."////{$start_time}\r\n",FILE_APPEND);
//            return 0;
//        }
        //$end_time = strtotime($end_time);
        $t = $end_time-$start_time;  //拍卖结束当天时间-支付下一天零点时间
        $day_num = ceil($t/(3600*24)) - 1;//舍去法利息天数
        return $day_num > 0 ? $day_num : 0;
    }

    public function getSellerIdByGoodsId($goodId)
    {
        $memberId = 0;
        $goodsInfo = Model('')->table('goods')->where(['goods_id' => $goodId])->find();
        if (!empty($goodsInfo) && isset($goodsInfo['store_id'])) {
            $storeInfo = Model('')->table('store')->where(['store_id' => $goodsInfo['store_id']])->find();
            if (!empty($storeInfo) && isset($storeInfo['member_id'])) {
                $memberId = $storeInfo['member_id'];
            }
        }
        return $memberId;
    }
}
