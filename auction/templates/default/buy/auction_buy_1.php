<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/22 0022
 * Time: 14:56
 * @author ADKi
 */
defined('InShopNC') or exit('Access Invalid!');
?>
<script type="text/javascript">
    //是否允许表单提交
    var SUBMIT_FORM = true;

    var chain_store_id = '<?php echo $output['chain_store_id'];?>';

    $(function(){

        $('select[nctype="voucher"]').on('change',function(){
            if ($(this).val() == '') {
                $('#eachStoreVoucher_'+items[1]).html('-0.00');
            } else {
                var items = $(this).val().split('|');
                $('#eachStoreVoucher_'+items[1]).html('-'+number_format(items[2],2));
            }
            calcOrder();
        });

        $('#rpt_panel').remove();
    });
    function disableOtherEdit(showText){
        $('a[nc_type="buy_edit"]').each(function(){
            if ($(this).css('display') != 'none'){
                $(this).after('<font color="#B0B0B0">' + showText + '</font>');
                $(this).hide();
            }
        });
        disableSubmitOrder();
    }
    function ableOtherEdit(){
        $('a[nc_type="buy_edit"]').show().next('font').remove();
        ableSubmitOrder();
    }
    function ableSubmitOrder(){
        $('#submitOrder').on('click',function(){submitNext()}).addClass('ok');
    }
    function disableSubmitOrder(){
        $('#submitOrder').unbind('click').removeClass('ok');
    }

</script>
<form method="post" id="order_form" name="order_form" action="index.php">

    <div class="ncc-main">
        <div class="ncc-title">
            <h3><?php echo $lang['cart_index_ensure_info'];?></h3>
            <h5>请仔细核对填写收货、发票等信息，以确保物流快递及时准确投递。</h5>
        </div>
        <?php include template('buy/buy_payment');?>
        <!--发票信息-->
        <?php include template('buy/buy_invoice');?>

        <?php include template('buy/buy_auction_order');?>

        <input value="auction_buy" type="hidden" name="act">
        <input value="update_auction_order" type="hidden" name="op">

        <!-- offline/online -->
        <input value="online" name="pay_name" id="pay_name" type="hidden">

        <!-- 是否保存增值税发票判断标志 -->
        <input value="<?php echo $output['vat_hash'];?>" name="vat_hash" type="hidden">

        <!-- 默认使用的发票 -->
        <input value="<?php echo $output['inv_info']['inv_id'];?>" name="invoice_id" id="invoice_id" type="hidden">
        <input value="<?php echo getReferer();?>" name="ref_url" type="hidden">

        <!-- 订单ID -->
        <input value="<?php echo $output['auction_order_info']['order_id'] ?>" name="auction_order_id" type="hidden">
    </div>
</form>
