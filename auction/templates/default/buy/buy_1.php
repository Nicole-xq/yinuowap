<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/22 0022
 * Time: 14:56
 * @author ADKi
 */
defined('InShopNC') or exit('Access Invalid!');
?>
<script type="text/javascript">
    //是否允许表单提交
    var SUBMIT_FORM = true;
    //记录无货的店铺ID数组
    var no_send_tpl_ids = [];

    var chain_store_id = '<?php echo $output['chain_store_id'];?>';
    //哪 些商品ID不可以门店自提
    var no_chain_goods_ids = [];


    $(function(){
        <?php if ($output['address_info']['chain_id']) { ?>
        showProductChain(<?php echo $output['address_info']['city_id'] ? $output['address_info']['city_id'] : $output['address_info']['area_id']?>);
        <?php } ?>
        $('select[nctype="voucher"]').on('change',function(){
            if ($(this).val() == '') {
                $('#eachStoreVoucher_'+items[1]).html('-0.00');
            } else {
                var items = $(this).val().split('|');
                $('#eachStoreVoucher_'+items[1]).html('-'+number_format(items[2],2));
            }
            calcOrder();
        });

        $('#rpt_panel').remove();
    });
    function disableOtherEdit(showText){
        $('a[nc_type="buy_edit"]').each(function(){
            if ($(this).css('display') != 'none'){
                $(this).after('<font color="#B0B0B0">' + showText + '</font>');
                $(this).hide();
            }
        });
        disableSubmitOrder();
    }
    function ableOtherEdit(){
        $('a[nc_type="buy_edit"]').show().next('font').remove();
        ableSubmitOrder();
    }
    function ableSubmitOrder(){
        $('#submitOrder').on('click',function(){submitNext()}).addClass('ok');
    }
    function disableSubmitOrder(){
        $('#submitOrder').unbind('click').removeClass('ok');
    }

</script>
<form method="post" id="order_form" name="order_form" action="index.php">

    <div class="ncc-main">
        <div class="ncc-title">
            <h3><?php echo $lang['cart_index_ensure_info'];?></h3>
            <h5>请仔细核对填写收货、发票等信息，以确保物流快递及时准确投递。</h5>
        </div>
        <?php /*include template('buy/buy_address');*/?>
        <?php include template('buy/buy_payment');?>
        <?php if (!$output['is_margin_pay']) { ?>
        <?php include template('buy/buy_invoice');?>
        <?php } ?>

        <?php if ($output['is_margin_pay']) { ?>
            <?php include template('buy/buy_margin_auction');?>
        <?php } else { ?>
            <?php include template('buy/buy_goods_list');?>
        <?php } ?>

        <input value="auction_buy" type="hidden" name="act">
        <input value="create_margin_order" type="hidden" name="op">

        <!-- offline/online -->
        <input value="online" name="pay_name" id="pay_name" type="hidden">

        <!-- 是否保存增值税发票判断标志 -->
        <input value="<?php echo $output['vat_hash'];?>" name="vat_hash" type="hidden">

        <!-- 收货地址ID -->
        <input value="<?php echo $output['address_info']['address_id'];?>" name="address_id" id="address_id" type="hidden">

        <!-- 城市ID(运费) -->
        <input value="" name="buy_city_id" id="buy_city_id" type="hidden">

        <!-- 默认使用的发票 -->
        <input value="<?php echo $output['inv_info']['inv_id'];?>" name="invoice_id" id="invoice_id" type="hidden">
        <input value="<?php echo getReferer();?>" name="ref_url" type="hidden">

        <!-- 是否匿名拍卖 -->
        <input value="<?php echo $output['is_anonymous'] ?>" name="is_anonymous" type="hidden">
    </div>
</form>
