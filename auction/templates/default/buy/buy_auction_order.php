<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .ncc-table-style tbody tr.item_disabled td { background: none repeat scroll 0 0 #F9F9F9; height: 30px; padding: 10px 0; text-align: center; }
    .qll_cash_deposit{text-align: right;text-indent: 0.8em;font-family: fantasy;}
    .qll_cash_deposit span{text-indent: 0.8em;font-family: fantasy;display:inline-block;text-align: left;width: 400px;}
    .qll_cash_deposit span b{font-size: 12px;color: #DB4453;font-weight: normal;}
</style>
<div class="ncc-receipt-info">
    <div class="ncc-receipt-info-title">
        <h3>拍品清单
            <input value="1" type="hidden" name="is_margin">
        </h3>
    </div>
    <table class="ncc-table-style">
        <thead>
        <tr>
            <th class="w10"></th>
            <th></th>
            <th><?php echo $lang['cart_index_store_goods'];?></th>
            <th class="w150">保证金(<?php echo $lang['currency_zh'];?>)</th>
            <th class="w150">剩余尾款(<?php echo $lang['currency_zh'];?>)</th>
            <th class="w100"><?php echo $lang['cart_index_amount'];?></th>
            <th class="w150"><?php echo $lang['cart_index_sum'].'('.$lang['currency_zh'].')';?></th>
        </tr>
        </thead>
        <!-- <?php /*$store_id = key($output['store_cart_list']);*/?>
    <?php /*$cart_list = current($output['store_cart_list']);*/?>
    --><?php /*$cart_info = $cart_list[0];*/?>
        <tbody>
        <tr>
            <th colspan="20"><!-- S 店铺名称 -->

                <div class="ncc-store-name">店铺：
                    <a href="<?php echo urlShop('show_store','index',array('store_id'=>$output['store_info']['store_id']));?>">
                        <?php echo $output['store_info']['store_name']; ?>
                    </a>
                    <span member_id="<?php echo $output['store_info']['member_id'];?>"></span>
                </div>
                <!-- E 店铺名称 -->
        </tr>

        <tr id="cart_item" class="shop-list">
            <td class="td-border-left">
<!--                --><?php //if (!$output['is_auction_end_true_t']) {?>
                    <input type="hidden" value="<?php echo $output['auction_info']['auction_id']?>" store_id="<?php echo $output['store_info']['store_id']?>" name="auction_id">
<!--                --><?php //} ?>
            </td>
            <td class="w100">
                <a href="<?php echo urlAuction('auctions','index',array('id'=>$output['auction_info']['auction_id']));?>" target="_blank" class="ncc-goods-thumb">
                    <img src="<?php echo cthumb($output['auction_info']['auction_image']);?>" alt="<?php echo $output['auction_info']['auction_name']; ?>" />
                </a>
            </td>
            <td class="tl">
                <dl class="ncc-goods-info">
                    <dt>
                        <a href="<?php echo urlAuction('auctions','index',array('id'=>$output['auction_info']['auction_id']));?>" target="_blank"><?php echo $output['auction_info']['auction_name']; ?></a>
                    </dt>
                </dl>
            </td>
            <td>
                <em class="goods-price">&yen;<?php echo $output['margin_order_info']['margin_amount'];?></em>
            </td>
            <td>
                <em class="goods-price">&yen;<?php echo $output['auction_order_info']['pay_amount'];?></em>
            </td>
            <td>1</td>
            <td class="td-border-right">
                <em class="goods-price">&yen;<?php echo $output['auction_order_info']['pay_amount'];?></em>
            </td>
        </tr>
        <tr>
            <td colspan="20"><div class="ncc-msg">

                </div>
                <div class="ncc-form-default"> </div>
                <div class="qll_cash_deposit"><span>您剩余的保证金<b>￥<?php echo $output['margin_order_info']['margin_amount']-$output['auction_order_info']['order_amount']; ?></b>&nbsp会保留在您的保证金帐号中</span></div>
                <div class="ncc-store-account">
                    <dl nctype="book_pay_content_part">
                        <dt>定时通知：</dt>
                        <dd class="all"><i>*</i>
                            <a href="<?php echo urlAuction('auctions','index',array('id'=>$output['auction_info']['auction_id']));?>" target="_blank">
                                请在拍品详细页订阅拍品结束通知
                            </a>
                        </dd>
                    </dl>
                </div>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="20">
                <div class="ncc-all-account">本次需支付：&yen;<em id="orderBookTotal"><?php echo $output['auction_order_info']['pay_amount'] ;?></em>
                    <?php echo $lang['currency_zh'];?>
                </div>
                <a href="javascript:void(0)" id='submitOrder' class="ncc-next-submit ok">
                    <?php echo $lang['cart_index_submit_order'];?>
                </a>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
<script>

    function submitNext(){
        if (!SUBMIT_FORM) return;

        if ($('input[name="auction_id"]').size() == 0) {
            showDialog('所购拍品无效', 'error','','','','','','','','',2);
            return;
        }
        if ($('#address_id').val() == ''){
            showDialog('<?php echo $lang['cart_step1_please_set_address'];?>', 'error','','','','','','','','',2);
            return;
        }

        SUBMIT_FORM = false;

        $('#order_form').submit();
    }
    $(function(){
        $('input[name="book_pay_type"]').on('change',function(){
            if ($(this).val() == 'full') {
                $('dl[nctype="book_pay_content_part"]').hide();
                $('dl[nctype="book_pay_content_full"]').show();
            } else {
                $('dl[nctype="book_pay_content_part"]').show();
                $('dl[nctype="book_pay_content_full"]').hide();
            }
            calcOrder();
        });
        $(document).keydown(function(e) {
            if (e.keyCode == 13) {
                submitNext();
                return false;
            }
        });
        $('#submitOrder').on('click',function(){submitNext()});
    });
</script>
