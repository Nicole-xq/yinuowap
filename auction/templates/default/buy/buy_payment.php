<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="ncc-receipt-info" id="paymentCon">
  <div class="ncc-receipt-info-title">
    <h3>支付方式</h3>
    <?php if (!$output['deny_edit_payment']) {?>
    <a href="javascript:void(0)" nc_type="buy_edit" id="edit_payment">[修改]</a>
    <?php }?>
  </div>
  <div class="ncc-candidate-items">
    <ul>
      <li>在线支付</li>
    </ul>
  </div>
  <div id="payment_list" class="ncc-candidate-items" style="display:none">
    <ul>
      <li>
        <input type="radio" value="online" name="payment_type" id="payment_type_online">
        <label for="payment_type_online">在线支付</label>
      </li>
        <li>
            <input type="radio" value="underline" name="payment_type" id="payment_type_underline">
            <label for="payment_type_underline">线下支付</label>
        </li>
    </ul>
    <div class="hr16"> <a href="javascript:void(0);" class="ncbtn ncbtn-grapefruit" id="hide_payment_list">保存支付方式</a></div>
  </div>
</div>

<script type="text/javascript">
$(function(){

var hybrid = <?php echo $output['ifshow_offpay'] === true && count($output['pay_goods_list']['online']) > 0 ? '1' : '0'; ?>;

var failInPage = false;
// 重置支付方式
$('#payment_type_online').attr('checked',true);
var payment_type = $('input[name="payment_type"]:checked').val();
$('#pay_name').val(payment_type);

// 重新调整在线支付/到付的商品展示
var setCodGoodsShow = function() {
    var j = $('#allow_offpay_batch').val();
    var arr = {};
    if (j) {
        $.each(j.split(';'), function(k, v) {
            vv = v.split(':');
            arr[vv[0]] = vv[1] == '1' ? true : false;
        });
    }

    $.each(arr, function(k, v) {
        //console.log(''+k+':'+v);
        if (v) {
            $("[data-cod-type='online'] [data-cod-store='"+k+"']").appendTo("[data-cod-type='offline']");
            $("[data-cod-type='online'] [data-cod-store='"+k+"']").remove();

            $("[data-cod2-type='online'] [data-cod2-store='"+k+"']").appendTo("[data-cod2-type='offline']");
            $("[data-cod2-type='online'] [data-cod2-store='"+k+"']").remove();
        } else {
            $("[data-cod-type='offline'] [data-cod-store='"+k+"']").appendTo("[data-cod-type='online']");
            $("[data-cod-type='offline'] [data-cod-store='"+k+"']").remove();

            $("[data-cod2-type='offline'] [data-cod2-store='"+k+"']").appendTo("[data-cod2-type='online']");
            $("[data-cod2-type='offline'] [data-cod2-store='"+k+"']").remove();
        }
    });

    var off = $("[data-cod2-type='offline'] [data-cod2-store]").length;
    var on = $("[data-cod2-type='online'] [data-cod2-store]").length;

    $("[data-hideshow='offline']")[off ? 'show' : 'hide']();
    $("[data-hideshow='online']")[on ? 'show' : 'hide']();

    $("span[data-cod-nums='offline']").html(off);
    $("span[data-cod-nums='online']").html(on);

    failInPage = ! off;
    hybrid = off && on;

};

	//点击修改支付方式
    $('#edit_payment').on('click',function(){
        $('#edit_payment').parent().next().remove();
        $(this).hide();
        $('#paymentCon').addClass('current_box');
        $('#payment_list').show();
        $('#payment_type_online').attr('checked',true);
        $('#payment_type_underline').parent().show();

        disableOtherEdit('如需要修改，请先保存支付方式');
    });
    //保存支付方式
    $('#hide_payment_list').on('click',function(){
        payment_type = $('input[name="payment_type"]:checked').val();
        if ($('input[name="payment_type"]:checked').size() == 0) return;

        setCodGoodsShow();

        $('#payment_list').hide();
        $('#edit_payment').show();
		$('.current_box').removeClass('current_box');
        var content = (payment_type == 'online' ? '在线支付' : '线下支付');

        $('#pay_name').val(payment_type);

        $('#edit_payment').parent().after('<div class="ncc-candidate-items"><ul><li>'+content+'</li></ul></div>');

        ableOtherEdit();
    });
    $('#show_goods_list').hover(function(){showPayGoodsList(this)},function(){$('#ncc-payment-showgoods-list').fadeOut()});
    function showPayGoodsList(item){
		var pos = $(item).position();
		var pos_x = pos.left+0;
		var pos_y = pos.top+25;
		$("#ncc-payment-showgoods-list").css({'left' : pos_x, 'top' : pos_y,'position' : 'absolute','display' : 'block'});
        $('#ncc-payment-showgoods-list').addClass('ncc-payment-showgoods-list').fadeIn();
    }
    $('input[name="payment_type"]').on('change',function(){
        if ($(this).val() == 'online'){
            $('#show_goods_list').hide();
        } else {

        }
    });

    $('body').on('click','#close_confirm_button',function(){
        DialogManager.close('confirm_pay_type');
    });
})
</script>