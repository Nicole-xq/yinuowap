<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<div class="nch-breadcrumb-layout">
  <?php if(!empty($output['nav_link_list']) && is_array($output['nav_link_list'])){?>
  <div class="nch-breadcrumb nch-module-filter wrapper"><i class="icon-home"></i>
    <?php foreach($output['nav_link_list'] as $nav_link){?>
    <?php if(!empty($nav_link['link'])){?>
    <span><a href="<?php echo $nav_link['link'];?>"><?php echo $nav_link['title'];?></a></span><span class="arrow">></span>
    <?php }else{?>
    <span><?php echo $nav_link['title'];?></span>
    <?php }?>
    <?php }?>
    <span class="arrow">></span>
    <?php if($_GET['cate_id']){?>
    <?php if(isset($output['checked_attr']) && is_array($output['checked_attr'])){?>
      <?php foreach ($output['checked_attr'] as $val){?>
        <span class="selected" nctype="span_filter"><?php echo $val['attr_name'].':<em>'.$val['attr_value_name'].'</em>'?><i data-uri="<?php echo removeParam(array('a_id' => $val['attr_value_id']));?>">X</i></span>
      <?php }?>
    <?php }?>
    <?php }?>
  <?php if (isset($output['count']))  { ?>
      <span>&nbsp;&nbsp;共&nbsp;<b><?php echo $output['count']?></b>&nbsp;件宝贝</span>
  <?php } ?>
  </div>

  <?php }?>
</div>
