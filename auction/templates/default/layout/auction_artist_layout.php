<?php defined('InShopNC') or exit('Access Invalid!');?>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo $output['html_title']; ?></title>
    <meta name="keywords" content="<?php echo $output['seo_keywords']; ?>"/>
    <meta name="description" content="<?php echo $output['seo_description']; ?>"/>
    <meta name="author" content="ShopNC">
    <meta name="copyright" content="ShopNC Inc. All Rights Reserved">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta content=always name=referrer>
    <meta name="renderer" content="webkit">

    <!--[if lt ie 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="<?php echo VENDUE_TEMPLATES_URL; ?>/css/base.css" rel="stylesheet" type="text/css">
    <link href="<?php echo VENDUE_TEMPLATES_URL; ?>/css/jl/store_joinin_new.css" rel="stylesheet" type="text/css">
    <link href="<?php echo VENDUE_TEMPLATES_URL; ?>/css/jl/lm-style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo SHOP_RESOURCE_SITE_URL;?>/font-new/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

  <script>
        var SITEURL = '<?php echo SHOP_SITE_URL;?>';
    </script>
    <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/jquery.js"></script>
    <script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script>
    <script type="text/javascript" src="<?php echo VENDUE_TEMPLATES_URL; ?>/js/jquery.pagination1.js"></script>
    <script type="text/javascript" src="<?php echo VENDUE_TEMPLATES_URL; ?>/js/art.js"></script>
    <script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script>

</head>

<body class="lm_art">
<div id="append_parent"></div>
<?php require_once($tpl_file); ?>
</body>
</html>
