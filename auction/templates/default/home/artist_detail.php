<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>

<body>


<div class="auction-master">
    <div class="middle-box master-detail">
        <div class="fl">
            <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['artist_info']['artist_image']?>" alt=""/>
            <h1><?php echo $output['artist_info']['artist_name']?></h1>
            <span class="all-block"><?php echo $output['artist_info']['artist_job_title']?></span>
            <a class="all-block" href="<?php echo urlVendue('index','store_au_detail',array('store_vendue_id'=>$output['artist_info']['store_vendue_id']))?>"><?php echo $output['artist_info']['store_name']?>&nbsp;></a>
            <a href="#artist-profile" class="pin">艺术家介绍</a>
            <a href="#awards" class="pin">获奖情况</a>
            <a href="#works" class="pin">代表作品</a>
        </div>
        <div class="fr">
            <div id="artist-profile">
                <h1>艺术家简介</h1>
                <span>丨&nbsp;个人简介&nbsp;丨</span>
                <p><?php echo $output['artist_info']['artist_resume']?></p>
                <span>丨&nbsp;艺术家年表&nbsp;丨</span>
                <p>
                    <?php foreach((array)$output['artist_info']['artist_represent'] as $k=>$v){?>
                        <?php if($v['repres_time'] != ''){?><?php echo $v['repres_time'];?>年<?php }?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $v['repres_intro'];?><br />
                    <?php }?>

                </p>
            </div>
            <div id="awards">
                <h1>获奖情况</h1>
                <p>
                    <?php foreach((array)$output['artist_info']['artist_awards'] as $k=>$v){?>
                        <?php if($v['awards_time'] != ''){?><?php echo $v['awards_time'];?>年<?php }?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $v['awards_intro'];?><br />
                    <?php }?>
                </p>
            </div>
            <div id="works">
                <h1>代表作品</h1>
                <ul class="works-list">
                    <?php foreach((array)$output['artist_info']['artist_works'] as $k=>$v){?>
                    <li>
                        <a>
                            <img class="all-block fl" src="<?php echo getVendueLogo($v['work_pic']);?>" alt=""/>
                            <div class="fr">
                                <h2><?php echo $v['work_name'];?></h2>
                                <p class="limited-words limited-words-4"><?php echo $v['work_intro'];?></p>
                                <span class="all-block"><?php echo $v['work_time'];?></span>
                            </div>
                        </a>
                    </li>
                    <?php }?>

                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
</body>

