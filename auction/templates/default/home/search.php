<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo VENDUE_RESOURCE_SITE_URL.'/js/search_goods.js';?>"></script>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/shuhua.css" rel="stylesheet" type="text/css">
<script>
    var tb_line=true;
</script>

<div class="painting">
    <div class="middle-box">
        <div>
            <?php $dl=1; $num_dl = 1;  //dl标记?>
            <?php if((!empty($output['brand_array']) && is_array($output['brand_array'])) || (!empty($output['attr_array']) && is_array($output['attr_array']))){?>
            <div class="screening attr">
                <div class="title fl">筛选</div>
                <div class="search-by by-category fl">
                    <?php if(!empty($output['attr_array']) && is_array($output['attr_array'])){?>
                        <?php $j = 0;foreach ($output['attr_array'] as $key=>$val){$j++;?>
                            <?php if(!isset($output['checked_attr'][$key]) && !empty($val['value']) && is_array($val['value'])){?>
                    <dl>
                        <dt class="fl"><span><?php echo $val['name'].$lang['nc_colon'];?></span></dt>
                        <dd class="fl show-con">
                            <?php $i = 0;foreach ($val['value'] as $k=>$v){$i++;?>
                                <a <?php if ($i>6){?>style="display:none" nc_type="none"<?php }?> href="<?php $a_id = (($_GET['a_id'] != '' && $_GET['a_id'] != 0)?$_GET['a_id'].'_'.$k:$k); echo replaceParam(array('a_id' => $a_id));?>"><?php echo $v['attr_value_name'];?></a>
                            <?php }?>
                        </dd>
                        <?php if (count($val['value']) > 6){?>
                            <dd class="all fl show-more"><span nc_type="show"><?php echo $lang['goods_class_index_more'];?><i class="fa fa-angle-down" aria-hidden="true"></i></span></dd>
                        <?php }?>
                    </dl>
                                <?php $num_dl++;}?>
                            <?php $dl++;} ?>

                    <?php }?>

                </div>
            </div>

<!--                --><?php //if($num_dl > 4){?>
<!--                    <div id="more_select_nav"><a href="javascript:void(0);" class="down"><span>更多选项&nbsp;</span><i class="icon-angle-down"></i></i></a></div>-->
<!--                --><?php //}?>
            <?php }?>

            <div class="screening">
                <div class="title fl">状态</div>
                <div class="search-by by-category fl">
                    <dl>
                        <dt class="fl"><a href="<?php echo replaceParam(array('state'=>'0'));?>" id="state_0" data-id="0" <?php if($_GET['state'] == 0 || !$_GET['state']){?>class="selected" <?php }?>>全部</a></dt>
                        <dd class="fl show-con">
                            <a href="<?php echo replaceParam(array('state'=>'4'));?>" id="state_4" data-id="4" <?php if($_GET['state'] == 4){?>class="selected" <?php }?>>正在进行</a>
                            <a href="<?php echo replaceParam(array('state'=>'3'));?>" id="state_3" data-id="3" <?php if($_GET['state'] == 3){?>class="selected" <?php }?>>即将开始</a>
                            <a href="<?php echo replaceParam(array('state'=>'5'));?>" id="state_5" data-id="5" <?php if($_GET['state'] == 5){?>class="selected" <?php }?>>已结束</a>
                        </dd>
                    </dl>
                </div>
            </div>

        </div>
    </div>
    <div class="clear"></div>
    <nav class="sort-bar middle-box" id="main-nav">
        <div class="nch-sortbar-array">
            <ul>
                <li <?php if(!$_GET['key']){?>class="selected"<?php }?>><a href="<?php echo dropParam(array('order', 'key'));?>"  title="<?php echo $lang['goods_class_index_default_sort'];?>"><?php echo $lang['goods_class_index_default'];?></a></li>
                <li <?php if($_GET['key'] == '1'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '1') ? replaceParam(array('key' => '1', 'order' => '1')):replaceParam(array('key' => '1', 'order' => '2')); ?>" <?php if($_GET['key'] == '1'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '1')?'点击出价次数从低到高':'点击出价次数从高到低' ?>">出价次数<i></i></a></li>
                <li <?php if($_GET['key'] == '2'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '2') ? replaceParam(array('key' => '2', 'order' => '1')):replaceParam(array('key' => '2', 'order' => '2')); ?>" <?php if($_GET['key'] == '2'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php  echo ($_GET['order'] == '2' && $_GET['key'] == '2')?'点击围观从低到高':'点击围观从高到低' ?>">围观<i></i></a></li>
                <li <?php if($_GET['key'] == '3'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3') ? replaceParam(array('key' => '3', 'order' => '1')):replaceParam(array('key' => '3', 'order' => '2')); ?>" <?php if($_GET['key'] == '3'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3')?'点击价格从低到高':'点击价格从高到低' ?>">价格<i></i></a></li>
            </ul>
        </div>
    </nav>
    <div class="clear"></div>
    <?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){?>
    <ul class="big-list">
        <?php foreach($output['goods_list'] as $value){?>
        <li>
            <a href="index.php?act=auctions&op=index&id=<?php echo $value['auction_id']?>" target="_blank" title="">
                <img src="<?php echo cthumb($value['auction_image'], 240,$value['store_id']);?>" alt=""/>
                <div class="new-tab-list fr">
                    <div class="new-tab-list-title fl">
                        <h1><?php echo $value['auction_name'];?></h1>
                        <span style="display:block;margin-top:6px"><?php if($value['auction_start_time'] > time()){?>起拍价
                                &nbsp;&nbsp;<b><?php echo ncPriceFormatForList($value['auction_start_price']);?></b>
                            <?php } elseif($value['auction_start_time'] <= time() && ( $value['auction_end_time'] >= time() || $value['auction_end_true_t'] >= time())){?>当前价
                                &nbsp;&nbsp;<b><?php if($value['current_price'] != 0.00){echo ncPriceFormatForList($value['current_price']);}else{echo ncPriceFormatForList($value['auction_start_price']);}?></b>
                            <?php } elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>成交总金额
                            &nbsp;&nbsp;<b style="color:#666"><?php echo ncPriceFormatForList($value['current_price']);?></b><?php }?></span>
                    </div>

                        <?php if($value['auction_start_time'] > time()){?>
                        <div class="offer fr green-bg">
                            <b><?php echo $value['auction_click'];?></b>
                            <b>次围观</b>
                        </div>
                        <?php }elseif($value['auction_start_time'] <= time() && ( ($value['auction_end_time'] >= time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] >= time()))){?>
                        <div class="offer fr black-bg">
                            <b><?php echo $value['bid_number'];?></b>
                            <b>次出价</b>
                        </div>
                        <?php }elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>
                            <div class="offer fr gray-bg">
                                <b><?php echo $value['bid_number'];?></b>
                                <b>次出价</b>
                            </div>
                            <?php if($value['is_liupai'] == 1){?>
                                <i class="liupai"></i>
                            <?php }else{?>
                                <i class="yichengjiao"></i>
                            <?php }?>
                    <?php }?>

                    <div class="clear"></div>
                    <dd class="time">
                        <?php if($value['auction_start_time'] <= time() && ( ($value['auction_end_time'] >= time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] >= time()))){?>
                        <span class="sell">距结束&nbsp;</span>
                            <span class="time-remain" count_down="<?php if(empty($value['auction_end_true_t'])){echo $value['auction_end_time']-TIMESTAMP; }else{echo $value['auction_end_true_t']-TIMESTAMP;}?>">
    <em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>
                                <?php }elseif($value['auction_start_time'] > time()){?>
                                <span class="time-remain"><font style="font-size:18px;color:#1b591b"><?php echo date('m月d日 H:i',$value['auction_start_time'])?>&nbsp;开拍</font></span>
                                <?php }elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>
                            <span class="time-remain"><font class="all-block" style="font-size:14px;transform:translateY(-14px)">结束时间：&nbsp;<?php if(empty($value['auction_end_true_t'])){echo date('m月d日 H:i',$value['auction_end_time']);}else{echo date('m月d日 H:i',$value['auction_end_true_t']);}?></font></span>
                        <?php }?>
                    </dd>
                </div>
            </a>
        </li>
        <?php }?>
    </ul>
    <?php }else{?>
        <div id="no_results" class="no-results"><i></i><?php echo $lang['index_no_record'];?></div>
    <?php }?>
    <div class="clear"></div>
    <div class="tc mt20 mb20">
        <?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){?>
        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
        <?php }?>
    </div>
</div>

<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/waypoints.js"></script>
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/search_category_menu.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fly/jquery.fly.min.js" charset="utf-8"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fly/requestAnimationFrame.js" charset="utf-8"></script>
<![endif]-->
<script type="text/javascript">

    $(function(){
        $(".show-more").click(function(event){
            event.preventDefault();
            var ticon = $(this).find("i");
            tspan = $(this).find("span");
            if($(this).hasClass("screening-more")){
                $(this).siblings(".show-con").css("height","30px");
                ticon.removeClass("fa-angle-up");
                ticon.addClass("fa-angle-down");
                tspan.html('更多<i class="fa fa-angle-down" aria-hidden="true"></i>');
                $(this).removeClass("screening-more")
            }else{
                $(this).siblings(".show-con").css("height","auto");
                ticon.removeClass("fa-angle-down");
                ticon.addClass("fa-angle-up");
                tspan.html('收起<i class="fa fa-angle-up" aria-hidden="true"></i>');
                $(this).addClass("screening-more")
            }
        });

        $('.fl>a').click(function(){
            var data_id = $(this).attr('data-id');
            $('#state_' + data_id).addClass('selected').siblings().removeClass('selected');
//            window.location.href='index.php?act=search&op=index&state='+data_id;
        });

        // 单行显示更多
        $('span[nc_type="show"]').click(function(){
            s = $(this).parents('dd').prev().find('a[nc_type="none"]');
            if(s.css('display') == 'none'){
                s.show();
                $(this).html('<i class="icon-angle-up"></i><?php echo $lang['goods_class_index_retract'];?>');
            }else{
                s.hide();
                $(this).html('<i class="icon-angle-down"></i><?php echo $lang['goods_class_index_more'];?>');
            }
        });


        //获取更多
        $('#more_select_nav a').click(function(){
            var attr = $(this).attr('class');
            if(attr == 'down'){
                $(this).attr('class','up');
                $(this).find('i').removeClass('icon-angle-down').addClass('icon-angle-up');
                $(this).find('span').html('精简选项&nbsp;');
                $('.nch-module-filter .hide_dl').show();
            }else{
                $(this).attr('class','down');
                $(this).find('i').removeClass('icon-angle-up').addClass('icon-angle-down');
                $(this).find('span').html('更多选项&nbsp;');
                $('.nch-module-filter .hide_dl').hide();
            }
        });
        <?php if(count($output['checked_attr']) >= count($output['attr_array'])){?>
        $(".attr").css('display','none');
        <?php }else{?>
        $(".attr").css('display','inline-block');
        <?php }?>


    })

</script> 
