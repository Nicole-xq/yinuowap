<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<script>
    var tb_line=true;
</script>

<body>

<div class="auction-master">
    <?php if(!empty($output['artist_list'])){?>
    <?php foreach($output['artist_list'] as $k=>$v){?>
    <div class="middle-box">
        <div class="title"><span><?php echo $v['artist_classify_name']?></span></div>
        <div class="master">
            <div>
                <?php if(!empty($v['artist_list'])){?>
                <?php foreach($v['artist_list'] as $key=>$val){?>
                <a href="index.php?act=index&op=artist_detail&artist_id=<?php echo $val['artist_vendue_id']?>"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$val['artist_image']?>" alt=""/><span class="all-block"><?php echo $val['artist_name']?></span></a>
                <?php }?>
                <?php }else{?>
                    <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无艺术家数据</span></div>
                <?php }?>

            </div>

            <div class="clear"></div>
        </div>
    </div>
    <?php }?>
    <?php } else{?>
        <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无艺术家数据</span></div>
    <?php }?>
</div>
</body>

