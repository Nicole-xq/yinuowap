<?php defined('InShopNC') or exit('Access Invalid!');?>

<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<script src="<?php echo VENDUE_RESOURCE_SITE_URL.'/js/search_goods.js';?>"></script>
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<style>
    .no-pai{width:100%;text-align:center; height:50px;line-height:50px; font-size: 16px; font-weight: bold;}
    .painting .nch-sortbar-array li.selected span {
        color: #fff;
    }
    .painting .nch-sortbar-array .sorting li:hover span{
        color: #000000;
    }
</style>

<body>
<div class="painting">
    <?php include template('home/store_top.detail');?>
    <div class="clear"></div>
    <div class="merchants-link">
        <a href="index.php?act=index&op=store_au_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>" class="selected">TA&nbsp;的拍品</a>
        <a href="index.php?act=index&op=store_special&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的专场</a>
        <a href="index.php?act=index&op=store_inform_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的资讯</a>
    </div>
    <nav class="sort-bar middle-box" id="main-nav">
        <div class="nch-sortbar-array">
            <ul>
                <li <?php if(!$_GET['key'] && (!$_GET['state'] && $_GET['state'] == 0)){?>class="selected"<?php }?>><a href="<?php echo dropParam(array('order', 'key','state'));?>"  title="默认">默认</a></li>
                <li <?php if($_GET['key'] == '3' && (!$_GET['state'] && $_GET['state'] == 0)){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3') ? replaceParam(array('key' => '3', 'order' => '1','state'=>0)):replaceParam(array('key' => '3', 'order' => '2','state'=>0)); ?>" <?php if($_GET['key'] == '3'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3')?'点击价格从低到高':'点击价格从高到低' ?>">价格<i></i></a></li>
            </ul>
            <ol class="sorting">
                <li class="no-hover <?php if($_GET['state'] && $_GET['state'] != 0){?>selected<?php }?>"><span onmouseover="mopen('showlist')" onmouseout="mclosetime()">
                        <?php if($_GET['state'] == 4){?>正在进行<?php }elseif($_GET['state'] == 3){?>即将开始<?php }elseif($_GET['state'] == 5){?>已经结束<?php }else{?>排序状态<?php }?>
                        &nbsp;<i class="fa fa-angle-down" style="background: none;" aria-hidden="true"></i></span>
                    <div id="showlist" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
                        <a href="<?php echo replaceParam(array('state'=>'0'));?>" id="state_0" data-id="0" vendue-id="<?php echo $output['store_au_info']['store_vendue_id']?>" <?php if($_GET['state'] == 0 || !$_GET['state']){?>class="selected" <?php }?>>全部</a>
                        <a href="<?php echo replaceParam(array('state'=>'4'));?>" id="state_4" data-id="4" vendue-id="<?php echo $output['store_au_info']['store_vendue_id']?>" <?php if($_GET['state'] == 4){?>class="selected" <?php }?>>正在进行</a>
                        <a href="<?php echo replaceParam(array('state'=>'3'));?>" id="state_3" data-id="3" vendue-id="<?php echo $output['store_au_info']['store_vendue_id']?>" <?php if($_GET['state'] == 3){?>class="selected" <?php }?>>即将开始</a>
                        <a href="<?php echo replaceParam(array('state'=>'5'));?>" id="state_5" data-id="5" vendue-id="<?php echo $output['store_au_info']['store_vendue_id']?>" <?php if($_GET['state'] == 5){?>class="selected" <?php }?>>已经结束</a>
                    </div>
                </li>
            </ol>
            <!--下拉菜单js-->
            <script type="text/javascript">var timeout=200;var closetimer=0;var ddmenuitem=0;function mopen(id){mcancelclosetime();if(ddmenuitem)ddmenuitem.style.visibility='hidden';ddmenuitem=document.getElementById(id);ddmenuitem.style.visibility='visible'}function mclose(){if(ddmenuitem)ddmenuitem.style.visibility='hidden'}function mclosetime(){closetimer=window.setTimeout(mclose,timeout)}function mcancelclosetime(){if(closetimer){window.clearTimeout(closetimer);closetimer=null}}document.onclick=mclose;</script>
        </div>
    </nav>
    <div class="clear"></div>
    <ul class="big-list">
        <?php if(!empty($output['auction_list'])){?>
            <?php foreach($output['auction_list'] as $key=>$value){?>
        <li>
            <a href="<?php echo urlVendue('auctions','index',array('id'=>$value['auction_id']))?>" target="_blank" title="">
                <img src="<?php echo cthumb($value['auction_image'], 240,$value['store_id']);?>" alt=""/>
                <div class="new-tab-list fr">
                    <div class="new-tab-list-title fl">
                        <h1><?php echo $value['auction_name'];?></h1>
                        <span style="display:block;margin-top:6px"><?php if($value['auction_start_time'] > time()){?>起拍价&nbsp;&nbsp;<b>&yen;<?php echo $value['auction_start_price'];?></b>
                            <?php } elseif($value['auction_start_time'] <= time() && ( ($value['auction_end_time'] >= time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] >= time()))){?>当前价
                                &nbsp;&nbsp;<b>&yen;<?php if($value['current_price'] != 0.00){echo $value['current_price'];}else{echo $value['auction_start_price'];}?></b>
                            <?php } elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>成交总金额
                            &nbsp;&nbsp;<b style="color:#666">&yen;<?php echo $value['current_price'];?></b><?php }?></span>
                    </div>
                    <?php if($value['auction_start_time'] > time()){?>
                    <div class="offer fr red-bg">
                        <b><?php echo $value['auction_click'];?></b>
                        <b>次围观</b>
                    </div>
            <?php }elseif($value['auction_start_time'] <= time() && ( ($value['auction_end_time'] >= time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] >= time()))){?>
                        <div class="offer fr black-bg">
                            <b><?php echo $value['bid_number'];?></b>
                            <b>次出价</b>
                        </div>
                    <?php }elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>
                        <div class="offer fr gray-bg">
                            <b><?php echo $value['bid_number'];?></b>
                            <b>次出价</b>
                        </div>
                    <?php if($value['is_liupai'] == 1){?>
                            <i class="liupai"></i>
                        <?php }else{?>
                            <i class="yichengjiao"></i>
                        <?php }?>
                    <?php }?>
                    <div class="clear"></div>
                    <dd class="time">
                        <?php if($value['auction_start_time'] <= time() && ( ($value['auction_end_time'] >= time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] >= time()))){?>
                        <span class="sell">距结束&nbsp;</span>
						<span class="time-remain" count_down="<?php echo $value['auction_end_time']-TIMESTAMP; ?>">
                            <em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>
                            <?php }elseif($value['auction_start_time'] > time()){?>
                            <span class="time-remain"><font style="font-size:18px;color:#800000"><?php echo date('m月d日 H:i',$value['auction_start_time'])?>&nbsp;开拍</font></span>
                    <?php }elseif(($value['auction_end_time'] < time() && empty($value['auction_end_true_t'])) || (!empty($value['auction_end_true_t']) && $value['auction_end_true_t'] < time())){?>
                            <span class="time-remain"><font class="all-block" style="font-size:14px;transform:translateY(-14px)">结束时间：&nbsp;<?php if(empty($value['auction_end_true_t'])){echo date('m月d日 H:i',$value['auction_end_time']);}else{echo date('m月d日 H:i',$value['auction_end_true_t']);}?></font></span>
                        <?php }?>

                    </dd>
                </div>
            </a>
        </li>
                <?php }?>
        <?php }else{?>
            <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无拍品数据</span></div>
        <?php }?>

    </ul>
    <div class="clear"></div>
    <div class="tc mt20 mb20">
        <?php if(!empty($output['auction_list'])){?>
        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
        <?php }?>
    </div>
</div>
</body>
<script type="text/javascript">

    $(function(){
//        $('.sorting li a').click(function(){
//            var data_id = $(this).attr('data-id');
//            var store_vendue_id = $(this).attr('vendue-id');
//            if(data_id != 0){
//                var param = $(this).attr('data-param');
//                $('#austate').html(param);
//            }
//            $('#state_' + data_id).addClass('selected').siblings().removeClass('selected');
//            window.location.href='index.php?act=index&op=store_au_detail&store_vendue_id=' + store_vendue_id +'&state='+data_id;
//        });

    })

</script>



