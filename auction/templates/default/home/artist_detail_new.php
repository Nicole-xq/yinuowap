<section class="lm_art_left">
    <a href="/" class="lm_art_logo">
        <img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_logo.png" alt="">
    </a>
    <div class="lm_art_content">
        <div class="lm_art_tx">
            <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$artist_info['artist_image']?>" alt="">
        </div>
        <div class="lm_art_thumbs">
            <a href=" javascript:collect_store1('<?php echo $output['store_info']['store_id']; ?>','count','store_collect');" >
<!--                <img src="--><?php //echo VENDUE_TEMPLATES_URL; ?><!--/images/jl/lm_heart.png" alt="">-->
                <img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_heart.png" alt="">
            </a>
            <span class="text"><?php echo $artist_info['attention_num']; ?></span>
        </div>
        <div class="lm_art_info">
            <h2 class="lm_art_name"><?php echo $artist_info['artist_name']; ?></h2>
            <img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_address.png" alt="">
            <div class="lm_art_address"><?php echo $output['artist_info']['city_name'].' '.$output['artist_info']['area_name']; ?></div>
        </div>
        <div class="lm_art_setting">
            <a target="_blank" href="<?php echo urlShop('show_store','index',array('store_id'=>$output['store_info']['store_id'])); ?>">商城在售作品</a>
            <!-- <a target="_blank" href="<?php echo urlAuction('index','store_au_detail',array('store_vendue_id'=>$output['artist_info']['store_vendue_id'])); ?>">正在拍卖作品</a> -->
            <!-- <a href="#"><i><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_message.png" alt=""></i>发消息</a> -->
        </div>
<!--      <div class="bdsharebuttonbox lm_bdsharebuttonbox"><a href="#" class="fa fa-share-alt" data-cmd="more" style="height:20px;line-height:21px; padding-left:0;margin:0px;background:none">&nbsp;分享</a></div>-->
<!--      <script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>-->
    </div>
    <div class="lm_art_ottom">
        <!--
        <div class="lm_share">
            <a href="javascript:void(0);" onclick="ajax_form('weixin_form', '微信账号登录', 'http://192.168.1.200/member/index.php?act=connect_wx&op=index', 360);" title="微信账号登录" class="lm_sina"><i></i></a>
            <a href="javascript:void(0);" onclick="ajax_form('weixin_form', '微信账号登录', 'http://192.168.1.200/member/index.php?act=connect_wx&op=index', 360);" title="微信账号登录" class="lm_qq"><i></i></a>
            <a href="javascript:void(0);" onclick="ajax_form('weixin_form', '微信账号登录', 'http://192.168.1.200/member/index.php?act=connect_wx&op=index', 360);" title="微信账号登录" class="lm_wx"><i></i></a>
        </div>
        -->
    </div>
</section>
<!-- left end -->
<aside class='lm_art_center'>
    <div class="lm_art_center_con">
        <div class="lm_art_top">
            <div class="lm_art_top_text">
                <?php echo $output['artist_info']['artist_resume']; ?>
            </div>

        </div>


    </div>
    <span class="lm_art_prev"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_prev.png" alt="收起" title="收起"></span>
    <span class="lm_art_next"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_next.png" alt="展开" title="展开"></span>
</aside>
<!-- center 结束 -->
<article class="lm_art_right">
    <div class="lm_art_right_con">
        <div class="lm_switch">
            <input id="lm_switch" type="checkbox" class="switch" />
            <label for="lm_switch"></label>
        </div>
        <div class="lm_work_double">
            <ul class="lm_art_list"></ul>
            <div class="lm_fenye">
                <div class="M-box"></div>
            </div>
        </div>
        <!-- 多图结束 -->
        <div class="lm_work_sign">
            <span class="lm_art_prev"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_art_prev.png" alt=""></span>
            <div class="lm_art_work_con">
            </div>
            <span class="lm_art_next"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/lm_art_next.png" alt=""></span>
        </div>
        <!-- 单图结束 -->
    </div>
</article>

<script>
    api_url = '<?php echo urlShop('show_store','ajax_page',array('store_id'=>$output['store_info']['store_id'])); ?>'
</script>
