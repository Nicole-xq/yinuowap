<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<style>
	.painting-bg{position:absolute;top:0;display:block;width:100%;height:480px;}
	.painting-bg img{width:1920px;height:480px;position: relative;left: 50%;margin-left: -960px}
</style>
<body>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/shuhua.css" rel="stylesheet" type="text/css">
<div class="painting">
    <div class="shuhua-top">
        <div class="middle-box" style="position:relative;z-index:999;top:20px">
            <div class="box-01"><img src="templates/default/images/painting/top-box-01.jpg" alt=""/></div>
            <div class="box-02">
                <?php if(!empty($output['paint_tag_list'])){?>
                    <?php foreach($output['paint_tag_list'] as $k=>$v){?>
                        <a href="<?php echo urlVendue('search','index',array('cate_id'=>$v['class_id']))?>"><?php echo $v['paint_tag_name']?></a>
                    <?php }?>
                <?php }?>
            </div>
        </div>
        <a href="<?php echo $output['list']['url'];?>" class="painting-bg"><img src="<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_PAINTING.DS.$output['list']['pic']);?>" alt=""/></a>
    </div>
    <div class="clear"></div>
    <div id="menu">
        <ul>
            <li><a href="#item1" class="current">合作机构</a></li>
            <li><a href="#item2">专场拍卖</a></li>
            <?php if(!empty($output['paint_module_list'])){?>
            <?php foreach($output['paint_module_list'] as $k=>$v){?>
            <li><a href="#item_<?php echo $v['paint_module_id']?>"><?php echo $v['paint_module_name']?></a></li>
            <?php }?>
            <?php }?>
<!--            <li><a href="#item4">西画雕塑</a></li>-->
        </ul>
    </div>
    <div id="content">
<!--        <div id="item1" class="item middle-box">-->
<!--            <h2>合作机构</h2>-->
<!--            <ul>-->
<!--                --><?php //if(!empty($output['co_store_list'])){?>
<!--                    --><?php //foreach($output['co_store_list'] as $k=>$v){?>
<!--                        <li><a href="--><?php //echo $v['cooper_store_url']?><!--"><img src="--><?php //echo co_storeImage($v['cooper_store_pic'])?><!--" alt=""/></a></li>-->
<!--                    --><?php //}?>
<!--                --><?php //}?>
<!---->
<!--            </ul>-->
<!--            <div class="clear"></div>-->
<!--        </div>-->
        <!-- 若干个item-->
        <div id="item2" class="item middle-box">
            <h2>专场拍卖&nbsp;&nbsp;<span>发现艺术等于在挖掘黄金</span></h2>
            <ul>
                <?php if(!empty($output['special_rec_list'])){?>
                    <?php foreach($output['special_rec_list'] as $k=>$v){?>
                <li>
                    <div class="title middle-box"><span><?php echo $v['special_name']?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-word"><?php echo date('m.d',$v['special_start_time'])?>-<?php echo date('m.d',$v['special_end_time'])?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:16px;">共&nbsp;<b><?php echo $v['auction_count']?></b>&nbsp;个拍品</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:16px;">&nbsp;<b class="red-word" style="font-size:18px"><?php echo $v['person_num']?></b>&nbsp;人参拍</span></div>
                    <div class="middle-box">
                        <a class="lm_painting_index_lm" href="<?php echo urlVendue('special_details','index',array('special_id'=>$v['special_id']))?>"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PAINTING.'/'.$v['special_rec_pic']; ?>" alt="" width="520px" height="221px"/>
                          <span class="lm_finance_ad"><span>保证金年收益率<strong><?php echo $v['special_rate']?></strong>%</span></span></a>
                        <?php foreach($v['auction_list'] as $kk=>$vv){?>
                        <a href="<?php echo urlVendue('auctions','index',array('id'=>$vv['auction_id']))?>" class="little"><img src="<?php echo cthumb($vv['auction_image'],240,$vv['store_id'])?>" alt=""/><span>&yen;<?php if($vv['current_price'] != 0.00){echo $vv['current_price'];} else{echo $vv['auction_start_price'];}?></span></a>
                        <?php }?>
                    </div>
                    <div class="clear"></div>
                </li>
                        <?php }?>
                <?php }else{?>
                    <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂无推荐的专场信息</span></div>
                <?php }?>

            </ul>
            <div class="clear"></div>
        </div>
        <?php if(!empty($output['paint_module_list'])){?>
            <?php foreach($output['paint_module_list'] as $k=>$v){?>
        <div id="item_<?php echo $v['paint_module_id']?>" class="item item-all">
            <div class="middle-box">
                <h2>丨<?php echo $v['paint_module_name']?>丨</h2>
                <div>
                    <?php if(!empty($v['tag_rec'])){?>
                        <?php foreach($v['tag_rec'] as $kk=>$vv){?>
                            <a href="javascript:void(0);" param-id="<?php echo $v['paint_module_id']?>" data-name="<?php echo $vv['tag_name']?>" name="tag_<?php echo $vv['tag_name']?>_<?php echo $v['paint_module_id']?>" class="paint_tag"><?php echo $vv['tag_name']?></a>
                        <?php }?>
                    <?php }?>
                </div>
            </div>
            <ul class="big-list" id="paint_<?php echo $v['paint_module_id']?>"  paint_module_id="<?php echo $v['paint_module_id']?>">
                <?php if(!empty($v['tag_rec'][0]['paint_rec'])){?>
                    <?php foreach($v['tag_rec'][0]['paint_rec'] as $key=>$val){?>
                <li>
                    <a href="<?php echo urlVendue('auctions','index',array('id'=>$val['auction_id']))?>" target="_blank" title="">
                        <img src="<?php echo cthumb($val['auction_image'], 240,$val['store_id']);?>" alt=""/>
                        <div class="new-tab-list fr">
                            <div class="new-tab-list-title fl">
                                <h1><?php echo $val['auction_name']?></h1>
                        <?php if($val['auction_start_time'] <= time() && ( ($val['auction_end_time'] >= time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] >= time()))){?>
                                <span style="display:block;margin-top:6px">当前价&nbsp;&nbsp;<b>&yen;<?php if($val['current_price'] != 0.00){echo $val['current_price'];}else{echo $val['auction_start_price'];}?></b></span>
                        <?php }elseif(($val['auction_end_time'] < time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] < time())){?>
                            <span style="display:block;margin-top:6px">成交总金额&nbsp;&nbsp;<b style="color:#666">&yen;<?php echo $val['current_price'];?></b></span>
                        <?php }elseif($val['auction_start_time'] > time()){?>
                            <span style="display:block;margin-top:6px">起拍价&nbsp;&nbsp;<b>&yen;<?php echo $val['auction_start_price'];?></b></span>
                        <?php }?>
                            </div>
                            <?php if($val['auction_start_time'] <= time() && ( ($val['auction_end_time'] >= time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] >= time()))){?>
                            <div class="offer fr black-bg">
                                <b><?php echo $val['bid_number']?></b>
                                <b>次出价</b>
                            </div>
                            <?php }elseif(($val['auction_end_time'] < time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] < time())){?>
                                <div class="offer fr gray-bg">
                                    <b><?php echo $val['bid_number']?></b>
                                    <b>次出价</b>
                                </div>
                                <?php if($val['is_liupai'] == 1){?>
                                    <i class="liupai"></i>
                                <?php }else{?>
                                    <i class="yichengjiao"></i>
                                <?php }?>
                            <?php }elseif($val['auction_start_time'] > time()){?>
                                <div class="offer fr green-bg">
                                    <b><?php echo $val['auction_click']?></b>
                                    <b>次围观</b>
                                </div>
                            <?php }?>
                            <div class="clear"></div>
                            <dd class="time">
                                <?php if($val['auction_start_time'] <= time() && ( ($val['auction_end_time'] >= time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] >= time()))){?>
                                <span class="sell">距结束&nbsp;</span>
								    <span class="time-remain" count_down="<?php if(empty($val['auction_end_true_t'])){echo $val['auction_end_time']-TIMESTAMP; }else{echo $val['auction_end_true_t']-TIMESTAMP;}?>">
                                        <em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>
                                <?php }elseif(($val['auction_end_time'] < time() && empty($val['auction_end_true_t'])) || (!empty($val['auction_end_true_t']) && $val['auction_end_true_t'] < time())){?>
                                    <span><font class="all-block" style="font-size:14px;transform:translateY(-14px)">结束时间&nbsp;<?php if(empty($val['auction_end_true_t'])){echo date('m月d日 H:i',$val['auction_end_time']);}else{echo date('m月d日 H:i',$val['auction_end_true_t']);}?></font></span>
                                <?php }elseif($val['auction_start_time'] > time()){?>
                                    <span><font style="font-size:18px;color:#800000"><?php echo date('m月d日 H:i',$val['auction_start_time'])?>&nbsp;开拍</font></span>
                            <?php }?>

                            </dd>
                        </div>
                    </a>
                </li>
                        <?php }?>
                    <?php }?>

            </ul>
            <div class="clear"></div>
        </div>
                <?php }?>
            <div id="item1" class="item middle-box">
            <h2>合作机构</h2>
            <ul>
                <?php if(!empty($output['co_store_list'])){?>
                    <?php foreach($output['co_store_list'] as $k=>$v){?>
                        <li><a href="<?php echo $v['cooper_store_url']?>"><img src="<?php echo co_storeImage($v['cooper_store_pic'])?>" alt=""/></a></li>
                    <?php }?>
                <?php }?>

            </ul>
            <div class="clear"></div>
        </div>
        <?php }?>

    </div>
    <div class="clear"></div>

</div>



<!--筛选折叠js-->
<script>

    $(function(){
        $(".show-more").click(function(event){
            event.preventDefault();
            var ticon = $(this).find("i");
            tspan = $(this).find("span");
            if($(this).hasClass("screening-more")){
                $(this).siblings(".show-con").css("height","30px");
                ticon.removeClass("icon-angle-up");
                ticon.addClass("icon-angle-down");
                tspan.html("更多");
                $(this).removeClass("screening-more")
            }else{
                $(this).siblings(".show-con").css("height","auto");
                ticon.removeClass("icon-angle-down");
                ticon.addClass("icon-angle-up");
                tspan.html("收起");
                $(this).addClass("screening-more")
            }
        });

    })
</script>
<!--楼层定位js-->
<script>
    $(function () {
        $(window).scroll(function () {
            var scrollTop = $(document).scrollTop();
            var documentHeight = $(document).height();
            var windowHeight = $(window).height();
            var contentItems = $("#content").find(".item");
            var currentItem = "";

            if (scrollTop+windowHeight==documentHeight) {
                currentItem= "#" + contentItems.last().attr("id");
            }else{
                contentItems.each(function () {
                    var contentItem = $(this);
                    var offsetTop = contentItem.offset().top;
                    if (scrollTop > offsetTop - 10) {//此处的200视具体情况自行设定，因为如果不减去一个数值，在刚好滚动到一个div的边缘时，菜单的选中状态会出错，比如，页面刚好滚动到第一个div的底部的时候，页面已经显示出第二个div，而菜单中还是第一个选项处于选中状态
                        currentItem = "#" + contentItem.attr("id");
                    }
                });
            }
            if (currentItem != $("#menu").find(".current").attr("href")) {
                $("#menu").find(".current").removeClass("current");
                $("#menu").find("[href=" + currentItem + "]").addClass("current");
            }
        });





        $('.paint_tag').click(function(){
            var data_name = $(this).attr('data-name');
            var module_id = $(this).attr('param-id');
            $('a[name=tag_'+data_name + '_'+ module_id+ ']').addClass('current').siblings().removeClass('current');
            $.ajax({
                url: "<?php echo C('auction_site_url')?>/index.php?act=painting&op=paint_ajax",
                data: {tag_name: data_name, paint_module_id: module_id},
                type: "post",
                dataType:'json',
                success: function (result) {
                    var html = '';
                    if (result != '') {
                        for(var i=0;i<result.length;i++){
                        html += '<li><a href="index.php?act=auctions&index&id='+ result[i].auction_id +'" target="_blank" title=""><img src="'+ UPLOAD_SITE_URL+'/shop/store/goods' + '/' + result[i].store_id + '/'+ result[i].auction_image+'" alt=""/>';
                        html += '<div class="new-tab-list fr"><div class="new-tab-list-title fl">';
                        if(result[i].is_kaipai == 1){
                            html += '<h1>'+result[i].auction_name+ '</h1><span style="display:block;margin-top:6px">当前价&nbsp;&nbsp;<b>&yen;'+result[i].current_price+'</b></span> </div>';
                        }else if(result[i].is_kaipai == 0){
                            html += '<h1>'+result[i].auction_name+ '</h1><span style="display:block;margin-top:6px">起拍价&nbsp;&nbsp;<b>&yen;'+result[i].auction_start_price+'</b></span> </div>';
                        }else if(result[i].is_kaipai == 2){
                            html += '<h1>'+result[i].auction_name+ '</h1><span style="display:block;margin-top:6px">成交总金额&nbsp;&nbsp;<b style="color:#666">&yen;'+result[i].current_price+'</b></span> </div>';
                        }
                        if(result[i].is_kaipai == 1){
                            html += ' <div class="offer fr black-bg"><b>'+ result[i].bid_number+ '</b><b>次出价</b></div> <div class="clear"></div>';
                        }else if(result[i].is_kaipai == 2){
                            html += ' <div class="offer fr gray-bg"><b>'+ result[i].bid_number+ '</b><b>次出价</b></div>' ;
                            if(result[i].is_liupai == 1){
                                html += '<i class="liupai"></i>';
                            }else{
                                html += '<i class="yichengjiao"></i>';
                            }
                            html +=   '<div class="clear"></div>';
                        }else if(result[i].is_kaipai == 0){
                            html += ' <div class="offer fr green-bg"><b>'+ result[i].auction_click+ '</b><b>次围观</b></div> <div class="clear"></div>';
                        }

                        html += '<dd class="time">';
                        if(result[i].is_kaipai == 1) {
                            html += '<span class="sell">距结束&nbsp;</span><span class="time-remain" count_down="' + result[i].time_cha + '">';
                            html += '<em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>';
                        }else if(result[i].is_kaipai == 2){
                            html += '<span class="sell"><font class="all-block" style="font-size:14px;transform:translateY(-14px)">结束时间&nbsp;'+ result[i].auction_end_date +'</font></span>';
                        }else if(result[i].is_kaipai == 0){
                            html += '<span class="sell"><font style="font-size:18px;color:#800000">'+ result[i].auction_start_date +'&nbsp;开拍</font></span>';
                            }
                        html += '</dd></div></a></li>';
                     }
                    }else{
                        html += '<div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无拍品数据</span></div>';
                    }
                    $('.big-list[paint_module_id="'+ module_id+ '"]').html(html);
                }
            })


        });

    });
</script>
</body>

