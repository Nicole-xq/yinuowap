<?php defined('InShopNC') or exit('Access Invalid!');?>

<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">

<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/shuhua.css" rel="stylesheet" type="text/css">
<script>
    var tb_line=true;
</script>

<style>
	h4.selected a,h4.selected a:hover{color:#fff}
	.painting .merchants-info .box-01 h4 a{display:block;padding-top:2px;height:22px;}
</style>
    <div class="middle-box">
        <div class="nch-breadcrumb wrapper">
            <span><a href="index.php?act=index">艺诺拍卖</a></span><span class="arrow">&gt;</span>
            <span><a href="index.php?act=index&op=store_au_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>"><?php echo $output['store_au_info']['store_name']?></a></span>
        </div>
    </div>
    <div class="clear"></div>
    <div class="middle-box">
        <div class="merchants-info">
            <div class="box-01 fl">
                <img src="<?php echo getStoreLogo($output['store_au_info']['store_avatar']);?>" alt=""/>
                <span><b><?php echo $output['attention_count']?></b>&nbsp;人关注</span>
                <?php if(empty($output['fav_info'])){?>
                    <h4><a id="guanzhu" href="javascript:collect_store1('<?php echo $output['store_au_info']['store_id']; ?>','count','store_collect');"
                               data-id="<?php echo $_SESSION['member_id']?>" store-id="<?php echo $output['store_au_info']['store_id']?>"
                               vendue-id="<?php echo $output['store_au_info']['store_vendue_id']?>" style="color: #FFF;"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;关注</a></h4>
                <?php }else{?>
                    <h4 class="selected">
                        <a href="javascript:void(0)" onclick="ajax_get_confirm('您确定要取消关注吗？', '<?php echo urlVendue('index','delfavorites',array('type'=>'store','fav_id'=>$output['fav_info']['fav_id']))?>');"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;已关注</a></h4>
                <?php }?>
            </div>
            <div class="box-02 fl">
                <h1><?php echo $output['store_au_info']['store_name']?></h1>
                <dl>
                    <dt>联&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;系：</dt>
                    <dd class="red-word"><span c_name="客服" member_id="<?php echo $output['store_au_info']['store_vendue_contact'];?>"></span>联系客服
<!--                        <a class="" title="在线联系" href="JavaScript:store_chat(--><?php //echo $output['store_au_info']['store_vendue_contact']?><!--,'客服');">联系客服</a>-->
                    </dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>所&nbsp;在&nbsp;&nbsp;地：</dt>
                    <dd><?php echo $output['store_au_info']['area_name']?></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>入驻时间：</dt>
                    <dd><?php echo date('Y年m月d日',$output['store_au_info']['store_time'])?></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>机构类型：</dt>
                    <dd><?php echo $output['store_au_info']['oz_type']?></dd>
                    <div class="clear"></div>
                </dl>
                <dl>
                    <dt>主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;营：</dt>
                    <dd><?php echo $output['store_au_info']['store_zy']?></dd>
                    <div class="clear"></div>
                </dl>
                <span class="all-block" title="已交保证金用于保障买家权益"><i class="fa fa-shield" aria-hidden="true"></i>&nbsp;已交保证金用于保障买家权益</span>
            </div>
            <div class="box-03 fr">
                <ul>
                    <?php if(!empty($output['store_trans_info'])){?>
<!--                        <li><b>--><?php //echo $output['store_trans_info']['turn_over']?><!--</b><span style="background: url(templates/default/images/painting/merchants-info-01.jpg) left no-repeat">累计成交额</span></li>-->
                        <li><b><?php echo $output['store_trans_info']['trade_goods_num']?></b><span style="background: url(templates/default/images/painting/merchants-info-02.jpg) left no-repeat">累计成交数</span></li>
                        <li><b><?php echo $output['store_trans_info']['special_num']?></b><span style="background: url(templates/default/images/painting/merchants-info-03.jpg) left no-repeat">累计专场数</span></li>
                        <li><b><?php echo $output['store_trans_info']['auction_num']?></b><span style="background: url(templates/default/images/painting/merchants-info-04.jpg) left no-repeat">累计拍品数</span></li>
                    <?php }else{?>
<!--                        <li><b>0.00</b><span style="background: url(templates/default/images/painting/merchants-info-01.jpg) left no-repeat">累计成交额</span></li>-->
                        <li><b>0</b><span style="background: url(templates/default/images/painting/merchants-info-02.jpg) left no-repeat">累计成交数</span></li>
                        <li><b>0</b><span style="background: url(templates/default/images/painting/merchants-info-03.jpg) left no-repeat">累计专场数</span></li>
                        <li><b>0</b><span style="background: url(templates/default/images/painting/merchants-info-04.jpg) left no-repeat">累计拍品数</span></li>
                    <?php }?>
                </ul>
                <div class="clear"></div>
                <p>
                    <?php if($output['store_au_info']['store_type'] == 0){?>
                    <?php echo $output['store_au_info']['store_vendue_intro']?>
                    <?php }else{?>
                        <?php echo $output['store_au_info']['artist_intro']?>
                    <?php }?>
                </p>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function collect_store1(fav_id,jstype,jsobj){
        $.get(SHOP_SITE_URL+'/index.php?act=index&op=login', function(result){
            if(result=='0'){
                login_dialog();
            }else{
                var url =  '<?php echo C('auction_site_url')?>'+'/index.php?act=index&op=favoritestore';
                $.getJSON(url, {'fid':fav_id}, function(data){
                    if (data.done){
                        showDialog(data.msg, 'succ','',function(){
                            window.location.href = data.url;
                        },'','','','','','',2);
                        if(jstype == 'count'){
                            $('[nctype="'+jsobj+'"]').each(function(){
                                $(this).html(parseInt($(this).text())+1);
                            });
                        }
                        if(jstype == 'succ'){
                            $('[nctype="'+jsobj+'"]').each(function(){
                                $(this).html("关注成功");
                            });
                        }
                        if(jstype == 'store'){
                            $('[nc_store="'+fav_id+'"]').each(function(){
                                $(this).before('<span class="goods-favorite" title="该店铺已关注"><i class="have">&nbsp;</i></span>');
                                $(this).remove();
                            });
                        }
                    }
                    else
                    {
                        showDialog(data.msg, 'notice');
                    }
                });
            }
        });
    }
</script>




