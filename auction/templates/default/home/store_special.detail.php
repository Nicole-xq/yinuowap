<?php defined('InShopNC') or exit('Access Invalid!');?>

<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<script src="<?php echo VENDUE_RESOURCE_SITE_URL.'/js/search_goods.js';?>"></script>
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<style>
    .no-pai{width:100%;text-align:center; height:50px;line-height:50px; font-size: 16px; font-weight: bold;}
</style>

<body>
<div class="painting">
    <?php include template('home/store_top.detail');?>
    <div class="clear"></div>
    <div class="merchants-link">
        <a href="index.php?act=index&op=store_au_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的拍品</a>
        <a href="index.php?act=index&op=store_special&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>" class="selected">TA&nbsp;的专场</a>
        <a href="index.php?act=index&op=store_inform_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的资讯</a>
    </div>

    <div class="clear"></div>
    <ul class="tab_box">
        <?php if(!empty($output['special_list'])){?>
            <?php foreach($output['special_list'] as $k=>$v){?>
        <li>
            <a href="index.php?act=special_details&op=inde&special_id=<?php echo $v['special_id']?>" target="_blank" title="">
                <img src="<?php echo getVendueLogo($v['special_image'])?>"  alt=""/>
                <div class="new-tab-list fr">
                    <div class="new-tab-list-title fl">
                        <h1><?php echo $v['special_name']?></h1>
                    </div>
                    <?php if($v['special_end_time'] <= time() ){?>
                        <div class="offer fr gray-bg">
                        <b><?php echo $v['jian_num']?></b>
                        <b>件成交</b>
                        </div>
                    <?php }elseif($v['special_start_time'] <= time() && $v['special_end_time'] > time()){?>
                        <div class="offer fr black-bg">
                        <b><?php echo $v['all_bid_number']?></b>
                        <b>次出价</b>
                        </div>
                    <?php }elseif($v['special_start_time'] > time()){?>
                            <div class="offer fr green-bg">
                        <b><?php echo $v['special_click']?></b>
                        <b>次围观</b>
                            </div>
                    <?php }?>
                    <div class="clear"></div>
                    <div class="others">
                        <?php if($v['special_end_time'] <= time()){?>
                        <span class="all-block">成交总金额&nbsp;&nbsp;&nbsp;<b>¥<?php echo ncPriceFormat($v['all_amount'])?></b></span>
                        <span class="all-block">结束时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo date('m月d日 H:i',$v['special_end_time'])?></b></span>
                    <?php }elseif($v['special_start_time'] <= time() && $v['special_end_time'] > time()){?>
                            <span class="all-block"><b><?php echo $v['special_count']?></b>&nbsp;&nbsp;&nbsp;件拍品</span>
                            <span class="all-block">结束时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo date('m月d日 H:i',$v['special_end_time'])?></b></span>
                        <?php }elseif($v['special_start_time'] > time()){?>
                            <span class="all-block"><b><?php echo $v['special_count']?></b>&nbsp;&nbsp;&nbsp;件拍品</span>
                            <span class="all-block">开拍时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo date('m月d日 H:i',$v['special_start_time'])?></b></span>
                        <?php }?>
                    </div>
                    <div class="clear"></div>
                </div>
            </a>
        </li>
                <?php }?>
        <?php }else{?>
            <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无专场数据</span></div>
        <?php }?>

    </ul>
    <div class="clear"></div>
    <div class="tc mt20 mb20">
        <?php if(!empty($output['special_list'])){?>
        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
        <?php }?>
    </div>
</div>
</body>




