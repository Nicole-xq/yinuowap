<?php defined('InShopNC') or exit('Access Invalid!');?>

<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>

<body>
<div class="painting">
    <?php include template('home/store_top.detail');?>
    <div class="clear"></div>
    <div class="merchants-link">
        <a href="index.php?act=index&op=store_au_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的拍品</a>
        <a href="index.php?act=index&op=store_special&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>">TA&nbsp;的专场</a>
        <a href="index.php?act=index&op=store_inform_detail&store_vendue_id=<?php echo $output['store_au_info']['store_vendue_id']?>" class="selected">TA&nbsp;的资讯</a>
    </div>
    <div class="clear"></div>
    <div class="merchants-news middle-box">
        <ul class="merchants-news-left fl">
            <?php if(!empty($output['store_inform_info']['inform_list'])){?>
                <?php foreach($output['store_inform_info']['inform_list'] as $k=>$v){?>
            <li>
                <img src="<?php echo $v['image']?>" alt=""/>
                <div>
                    <h1><a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$v['id']))?>" title=""><?php echo $v['title']?></a></h1>
                    <p><?php echo date('Y-m-d',$v['time'])?>&nbsp;&nbsp;
                        <a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$v['id']))?>"><i class="fa fa-share-square-o" aria-hidden="true"></i>&nbsp;分享</a></p>
                    <p><?php echo $v['abstract']?><a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$v['id']))?>">【详情】</a></p>
                </div>
            </li>
                    <?php }?>
            <?php }else{?>
                <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无资讯数据</span></div>
            <?php }?>

        </ul>
        <div id="vertical-slide" class="fr">
            <h1><span>精品推荐</span></h1>
            <div class="clear"></div>
            <div class="slide-pic" id="slidePic">
                <div class="vertical-slide-pic">
                    <ul>
                        <?php if(!empty($output['hot_auctions'])){?>
                        <?php foreach($output['hot_auctions'] as $k=>$v){?>
                            <li>
                                <a href="<?php echo urlVendue('auctions','index',array('id'=>$v['auction_id']))?>"><img src="<?php echo cthumb($v['auction_image'], 240,$v['store_id']);?>" alt=""/><span class="price">&yen;<?php echo $v['current_price']?></span></a>
                            </li>
                            <?php }?>
                        <?php }?>

                    </ul>
                </div>
                <a class="gray" id="prev" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                <a id="next" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="tc mt20 mb20">
        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
    </div>
</div>
</body>


<!--商品轮播js-->
<script type="text/javascript">
    jQuery(function() {
        if (!$('#slidePic')[0]) return;
        var i = 0,
            p = $('#slidePic ul'),
            pList = $('#slidePic ul li'),
            len = pList.length;
        var elePrev = $('#prev'),
            eleNext = $('#next');
        var w = 224,
            num = 5;
        p.css('width', w * len);
        if (len <= num) eleNext.addClass('gray');
        function prev() {
            if (elePrev.hasClass('gray')) {
                return
            }
            p.animate({
                marginTop: -(--i) * w
            }, 500);
            if (i < len - num) {
                eleNext.removeClass('gray')
            }
            if (i == 0) {
                elePrev.addClass('gray')
            }
        }
        function next() {
            if (eleNext.hasClass('gray')) {
                return
            }
            p.animate({
                marginTop: -(++i) * w
            }, 500);
            if (i != 0) {
                elePrev.removeClass('gray')
            }
            if (i == len - num) {
                eleNext.addClass('gray')
            }
        }
        elePrev.bind('click', prev);
        eleNext.bind('click', next);
    });
</script>




