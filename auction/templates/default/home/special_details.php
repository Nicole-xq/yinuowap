<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/shuhua.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<script src="<?php echo VENDUE_RESOURCE_SITE_URL.'/js/search_goods.js';?>"></script>
<style>
  .lm_bzjsy{
    color:#fff;
    font-size: 12px;
  }
</style>
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<div class="painting lm_painting_finance">
	<div class="top-bg" style="background: url(<?php echo $output['special_info']['adv_image_path']?>) center no-repeat">
		<div class="middle-box">
			<div class="info">
				<?php if($output['special_info']['special_start_time'] <= time() && $output['special_info']['special_end_time'] > time()){?>
				<div class="info-box-01">
					<div class="fl">
						<h1>距结束</h1>
						<dd class="time">
							<span class="time-remain" count_down="<?php echo $output['special_info']['special_end_time']-TIMESTAMP; ?>">
						  <em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>
						</dd>
					</div>
					<?php if(!empty($output['special_notice_info'])){?>
						<a class="fr" href="javascript:void(0);" id="cancel_remind_btn" data-id="2">
							<img src="<?php echo VENDUE_TEMPLATES_URL;?>/images/painting/clock.png" width="34" height="34" alt=""/>
							<span class="all-block">已设置提醒</span>
						</a>
					<?php }else{?>
						<a class="fr cd-popup-trigger1" href="javascript:void(0);" id="remind_btn">
							<img src="<?php echo VENDUE_TEMPLATES_URL;?>/images/painting/clock.png" width="34" height="34" alt=""/>
							<span class="all-block" id="remind_msg">结束前提醒</span>
						</a>
					<?php }?>
					<div class="clear"></div>
				</div>
				<?php }elseif($output['special_info']['special_start_time'] > time()){?>
					<div class="info-box-01" style="background: #1b591b !important;">
						<div class="fl">
							<h1>开拍时间</h1>
							<dd class="time" style="font-size:20px"><?php echo date('m月d日 H:i',$output['special_info']['special_start_time'])?>
							</dd>
						</div>
						<?php if(!empty($output['special_notice_info'])){?>
						<a class="fr" href="javascript:void(0);" id="cancel_remind_btn" data-id="1">
							<img src="<?php echo VENDUE_TEMPLATES_URL;?>/images/painting/clock.png" width="34" height="34" alt=""/>
							<span class="all-block">已设置提醒</span>
						</a>
						<?php }else{?>
							<a class="fr cd-popup-trigger1" href="javascript:void(0);" id="remind_btn1">
								<img src="<?php echo VENDUE_TEMPLATES_URL;?>/images/painting/clock.png" width="34" height="34" alt=""/>
								<span class="all-block" id="remind_msg1">开始前提醒</span>
							</a>
						<?php }?>
						<div class="clear"></div>
					</div>
				<?php }elseif($output['special_info']['special_end_time'] <= time()){?>
					<div class="info-box-02">
						<h2>本场已结束</h2>
					</div>
				<?php }?>
				<div class="info-box-03">
					<?php if($output['special_info']['special_end_time'] > time()){?>
					<span class="fl">拍品:&nbsp;<b><?php echo $output['special_info']['auction_count']?>件</b></span>
					<span class="fr">围观:&nbsp;<b><?php echo $output['special_info']['special_click']?>次</b></span>
					<div class="clear"></div>
					<span>出价:&nbsp;<b><?php echo $output['special_info']['all_bid_number']?>次</b></span>
					<?php }else{?>
						<h2>成交总金额<br /><br /><b class="red-word">&yen;<?php echo ncPriceFormat($output['special_info']['all_price'])?></b></h2>
					<?php }?>
					<?php if($output['special_info']['delivery_mechanism'] != ''){?>
						<span>送拍机构:&nbsp;<a href="<?php echo urlVendue('index','store_au_detail',array('store_vendue_id'=>$output['special_info']['store_vendue_id']))?>" class="red-word"><?php echo $output['special_info']['delivery_mechanism']?>&nbsp;</a></span>
					<?php }?>
				</div>
        <?php if ($output['special_info']['special_rate']>0){?>
        <span class="lm_finance_ad" style="color:#fff;font-size:12px;">
            <span>保证金年收益率<strong>
            <?php echo $output['special_info']['special_rate']?></strong>%
            <a class="lm_bzjsy" href="<?=C('cms_site_url')?>/index.php?act=special&op=special_detail&special_id=43" target="_blank">什么是保证金送收益?</a>
          </span>
        </span>
        <?php } ?>
			</div>
		</div>
    </div>
	<nav class="sort-bar middle-box" id="main-nav">
		<div class="nch-sortbar-array">
			<ul>
				<li <?php if(!$_GET['key']){?>class="selected"<?php }?>><a href="<?php echo dropParam(array('order', 'key'));?>"  title="默认">默认</a></li>
<!--				--><?php //if($output['special_info']['special_start_time'] <= time()){?>
					<li <?php if($_GET['key'] == '1'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '1') ? replaceParam(array('key' => '1', 'order' => '1')):replaceParam(array('key' => '1', 'order' => '2')); ?>" <?php if($_GET['key'] == '1'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '1')?'点击出价次数从低到高':'点击出价次数从高到低' ?>">出价次数<i></i></a></li>
					<li <?php if($_GET['key'] == '2'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '2') ? replaceParam(array('key' => '2', 'order' => '1')):replaceParam(array('key' => '2', 'order' => '2')); ?>" <?php if($_GET['key'] == '2'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php  echo ($_GET['order'] == '2' && $_GET['key'] == '2')?'点击围观从低到高':'点击围观从高到低' ?>">围观<i></i></a></li>
<!--				--><?php //}?>
				<li <?php if($_GET['key'] == '3'){?>class="selected"<?php }?>><a href="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3') ? replaceParam(array('key' => '3', 'order' => '1')):replaceParam(array('key' => '3', 'order' => '2')); ?>" <?php if($_GET['key'] == '3'){?>class="<?php echo $_GET['order'] == 1 ? 'desc' : 'asc';?>"<?php }?> title="<?php echo ($_GET['order'] == '2' && $_GET['key'] == '3')?'点击价格从低到高':'点击价格从高到低' ?>">价格<i></i></a></li>

			</ul>
		</div>
	</nav>
	<ul class="big-list">
        <?php if(!empty($output['special_goods_list']) && is_array($output['special_goods_list'])){ ?>
        <?php foreach($output['special_goods_list'] as $key => $value){ ?>
					<?php if($value['auction_start_time'] <= time() && $value['auction_end_time'] > time()){?>
		<li class="lb-item" nc_auction_id="<?php echo $value['auction_id']?>" auction_remain_date="<?php echo $value['auction_remain_date']?>">
			<a href="index.php?act=auctions&op=index&id=<?php echo $value['auction_id']?>" target="_blank" title="">
				<img src="<?php echo $value['auction_image_path']?>" alt=""/>
				<div class="new-tab-list fr">
					<div class="new-tab-list-title fl">
						<h1><?php echo $value['auction_name']?></h1>
						<span style="display:block;margin-top:6px">当前价&nbsp;&nbsp;<b>&yen;<?php if($value['current_price'] != 0.00 ){echo $value['current_price'];}else{echo $value['auction_start_price'];}?></b></span>
					</div>
					<div class="offer fr black-bg">
						<b><?php echo $value['bid_number']?></b>
						<b>次出价</b>
					</div>
					<div class="clear"></div>
					<dd class="time">
						距结束
						<span class="time-remain" count_down="<?php echo $value['auction_end_time']-TIMESTAMP; ?>">
						  <em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>
					</dd>
				</div>
			</a>
		</li>
				<?php }elseif($value['auction_start_time'] > time()){?>
					<li class="lb-item" nc_auction_id="<?php echo $value['auction_id']?>" auction_remain_date="<?php echo $value['auction_remain_date']?>">
						<a href="index.php?act=auctions&op=index&id=<?php echo $value['auction_id']?>" target="_blank" title="">
							<img src="<?php echo $value['auction_image_path']?>" alt=""/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $value['auction_name']?></h1>
									<span style="display:block;margin-top:6px">起拍价&nbsp;&nbsp;<b>&yen;<?php echo $value['auction_start_price']?></b></span>
								</div>
								<div class="offer fr green-bg">
									<b><?php echo $value['auction_click']?></b>
									<b>次围观</b>
								</div>
								<div class="clear"></div>
								<dd class="time">
									<font style="font-size:18px;color:#1b591b"><?php echo date('m月d日 H:i',$value['auction_start_time'])?>&nbsp;开拍</font>
								</dd>
							</div>
						</a>
					</li>
					<?php }elseif($value['auction_end_time'] <= time()){?>
					<li class="lb-item" nc_auction_id="<?php echo $value['auction_id']?>" auction_remain_date="<?php echo $value['auction_remain_date']?>">
						<a href="index.php?act=auctions&op=index&id=<?php echo $value['auction_id']?>" target="_blank" title="">
							<img src="<?php echo $value['auction_image_path']?>" alt=""/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $value['auction_name']?></h1>
									<span style="display:block;margin-top:6px">成交总金额&nbsp;&nbsp;<b style="color:#666">&yen;<?php echo $value['current_price']?></b></span>
								</div>
								<div class="offer fr gray-bg">
									<b><?php echo $value['bid_number']?></b>
									<b>次出价</b>
								</div>
								<?php if($value['is_liupai'] == 1){?>
									<i class="liupai"></i>
						<?php }else{?>
									<i class="yichengjiao"></i>
						<?php }?>
								<div class="clear"></div>
								<dd class="time">
									<font class="all-block" style="font-size:14px;transform:translateY(-14px)">结束时间&nbsp;<?php echo date('m月d日 H:i',$value['auction_end_time'])?></font>
								</dd>

							</div>
						</a>
					</li>
					<?php }?>
        <?php }?>
        <?php }else{ ?>
            <div id="no_results" class="no-results"><i></i>没有找到符合条件的商品</div>
        <?php }?>
	</ul>
	<div class="clear"></div>
	<h1 style="font-size: 24px; color: #353530;margin-top: 30px" class="middle-box">热门推荐</h1>
	<ul class="tab_box">
		<?php if(!empty($output['rec_special_list'])){?>
				<?php foreach($output['rec_special_list'] as $key=>$val){?>
		<li>
			<a href="<?php echo urlVendue('special_details','index',array('special_id'=>$val['special_id']))?>" target="_blank" title="">
				<img src="<?php echo getVendueLogo($val['special_image'])?>"  alt=""/>
				<div class="new-tab-list fr">
					<div class="new-tab-list-title fl">
						<h1><?php echo $val['special_name']?></h1>
					</div>
					<?php if($val['special_start_time'] <= time() && $val['special_end_time'] > time()){?>
					<div class="offer fr black-bg">
							<b><?php echo $val['all_bid_number']?></b>
							<b>次出价</b>
					</div>
						<?php }elseif($val['special_end_time'] <= time()){?>
						<div class="offer fr gray-bg">
							<b><?php echo $val['jian_num']?></b>
							<b>件成交</b>
						</div>
					<?php }elseif($val['special_start_time'] > time()){?>
						<div class="offer fr green-bg">
							<b><?php echo $val['special_click']?></b>
							<b>次围观</b>
						</div>
					<?php }?>
					<div class="clear"></div>
					<div class="others">
						<span class="all-block">送拍机构&nbsp;&nbsp;&nbsp;<b><?php echo $val['delivery_mechanism']?></b></span>
						<?php if($val['special_start_time'] > time()){?>
						<span class="all-block">开始时间&nbsp;&nbsp;&nbsp;<b><?php echo date('m月d日 H:i',$val['special_start_time'])?></b></span>
						<?php }elseif($val['special_start_time'] <= time()){?>
							<span class="all-block">结束时间&nbsp;&nbsp;&nbsp;<b><?php echo date('m月d日 H:i',$val['special_end_time'])?></b></span>
							<?php }?>
					</div>
					<div class="clear"></div>
				</div>
			</a>
		</li>
					<?php }?>
		<?php }?>

	</ul>
</div>

<?php if ($_SESSION['is_login'] == '1') {?>
	<!--服务提醒-->
	<div class="cd-popup1">
		<div class="cd-popup-container1">
			<div class="cd-popup-top"><h1>提醒设置</h1></div>
			<div class="cd-buttons">
				<div class="box-01">
					<div class="fl"><h1>提醒时间</h1><img class="all-block" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/clock-black.png" width="34" height="34" alt=""/></div>
                                <?php if($output['special_info']['special_start_time'] <= time() && $output['special_info']['special_end_time'] > time()){?>
					<span class="all-block fr">结束前30分钟
                                    <b class="gold-word">
										<?php echo format_date($output['special_info']['special_end_time']).'&nbsp;'.date('H:i', $output['special_info']['special_end_time']-1800) ?>
									</b>
                                </span>
					<?php }elseif($output['special_info']['special_start_time'] > time()){?>
									<span class="all-block fr">开拍前30分钟
                                    <b class="gold-word">
										<?php echo format_date($output['special_info']['special_start_time']).'&nbsp;'.date('H:i', $output['special_info']['special_start_time']-1800) ?>
									</b>
                                </span>
					<?php }?>
                                <span class="all-block fr">开拍时
                                    <b class="gold-word"><?php echo date('m-d',$output['special_info']['special_start_time']).'&nbsp;'.date('H:i', $output['special_info']['special_start_time']) ?></b>
                                </span>
				</div>
				<div class="clear"></div>
				<div class="box-02">
                                <span>拍卖会将根据您设置的手机(
									<?php
									if ($output['member_info']['member_mobile_bind'] == 1) {
										echo $output['member_info']['member_mobile'];
									} else {
										echo '<span style="color: red">'.'未绑定手机，请点击更改设置'.'</span>';
									} ?>
									)、手机艺诺提醒您。</span>
					<a href="javascript:void(0);" class="all-block" id="change_setting"> 更改设置&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
				</div>
				<div class="clear"></div>
				<div class="box-02" style="display: none" id="contact_setting">
					<div class="fl" id="contact_set" <?php if ($output['member_info']['member_mobile_bind'] == 0) { echo "style='display: none'"; } ?>>
						<span>提醒方式&nbsp;&nbsp;<b>(您可以在我的拍卖-设置中找到哦)</b></span>
						<div class="all-block">
							<label for="">
								手机短信：<?php echo $output['member_info']['member_mobile']; ?>
							</label>&nbsp;&nbsp;
							<a href="javascript:void(0);" id="chang_contact">修改</a>
						</div>
					</div>
					<div class="fl" id="change_set" <?php if ($output['member_info']['member_mobile_bind'] == 1) { echo "style='display: none'"; } ?>>
						<span>提醒方式&nbsp;&nbsp;<b>(您可以在我的拍卖-设置中找到哦)</b></span>
						<form id="mobile_form">
							<input type="hidden" value="<?php echo $output['special_info']['special_id']; ?>" id="special_id">
							<div class="all-block">
								<label for="">手机短信:（之前有过设置，也将通过该号码进行提醒）</label>
								<div class="phone-number">
									<label for="">手机号码:&nbsp;&nbsp;
										<input type="text" name="phone" id="phone" placeholder="" autocomplete="off" maxlength="11" value="<?php echo $output['member_info']['member_mobile']; ?>">
									</label>
									<label for="">手机短信:&nbsp;&nbsp;
										<input type="text" name="vcode" id="vcode" placeholder="" autocomplete="off" maxlength="6"style="width: 70px;">
										<input type="hidden" name="is_vcode" id="is_vcode" value="<?php echo $output['member_info']['member_mobile_bind'] == 1 ? '0' : '1'?>">
										<a href="javascript:;" id="send_auth_code">
											<span id="sending" style="display:none; color: #fff">正在</span>
                                                        <span class="send_success_tips" style="display: none; color: #fff">
                                                            <strong id="show_times" class="red mr5"></strong>秒后再次
                                                        </span>
											获取验证码
										</a>
									</label>
									<p class="send_success_tips hint mt10" style="display: none;">“安全验证码”已发出，请注意查收，请在
										<strong>“30分种”</strong>内完成验证。
									</p>
								</div>
							</div>
							<input type="hidden" name="notice_type" id="notice_type" value="<?php if($output['special_info']['special_start_time'] <= time() && $output['special_info']['special_end_time'] > time()){echo 2;}else{echo 1;}?>"/>
							<input type="hidden" name="remind_time" id="remind_time" value="<?php if($output['special_info']['special_start_time'] <= time() && $output['special_info']['special_end_time'] > time()){echo $output['special_info']['special_end_time'] - 1800;}else{echo $output['special_info']['special_start_time'] - 1800;}?>"/>
						</form>
					</div>

				</div>
				<div class="clear"></div>
				<div class="botton">
					<input type="submit" value="确 定" id="mobile_submit" class="fl">
					<input type="submit" value="取 消" class="cd-popup-close fr">
				</div>
				<div class="clear"></div>
			</div>
			<a href="#0" class="cd-popup-close">X</a>
		</div>
	</div>

<?php } ?>

<script>
    $(function() {
        $('.lb-item').each(function () {
            var _auction_id = $(this).attr('nc_auction_id');
            var _remain_date = $(this).attr('auction_remain_date');
            timer(_remain_date, _auction_id);
        });

		//打开设置提醒窗口
		$('.cd-popup-trigger1').on('click', function(event){
			event.preventDefault();
			<?php if ($_SESSION['is_login'] !== '1'){?>
			login_dialog();
			<?php }else{ ?>
			$('.cd-popup1').addClass('is-visible1');
			<?php } ?>
		});
		//关闭窗口
		$('.cd-popup1').on('click', function(event){
			if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup1') ) {
				event.preventDefault();
				$(this).removeClass('is-visible1');
			}
		});

		$('#change_setting').on('click', function () {
			$('#contact_setting').css('display', 'block');
		});
		$('#chang_contact').on('click', function () {
			$('#change_set').css('display', 'block');
			$('#contact_set').css('display', 'none');
		});

		//验证码秒数倒计时
		function StepTimes() {
			$num = parseInt($('#show_times').html());
			$num = $num - 1;
			$('#show_times').html($num);
			if ($num <= 0) {
				ALLOW_SEND = !ALLOW_SEND;
				$('.send_success_tips').hide();
			} else {
				setTimeout(StepTimes,1000);
			}
		}
		//发送短信验证码
		var ALLOW_SEND = true;

		$('#send_auth_code').on('click',function(){
			if ($('#phone').val() == '') return false;
			if (!ALLOW_SEND) return;
			ALLOW_SEND = !ALLOW_SEND;
			$('#sending').show();
			$('#is_vcode').val(1);
			$.getJSON('index.php?act=special_details&op=send_modify_mobile',{mobile:$('#phone').val()},function(data){
				if (data.state == 'true') {
					$('#sending').hide();
					$('.send_success_tips').show();
					$('#show_times').html(60);
					setTimeout(StepTimes,1000);
				} else {
					ALLOW_SEND = !ALLOW_SEND;
					$('#sending').hide();
					$('.send_success_tips').hide();
					showDialog(data.msg,'error','','','','','','','','',2);
				}
			});
		});

		// 提交订阅提醒表单
		$('#mobile_submit').on('click', function () {
			var mobile = $('#phone').val();
			var is_vcode = $('#is_vcode').val();
			var vcode = $('#vcode').val();
			var notice_type = $('#notice_type').val();
			var special_id = $('#special_id').val();
			var remind_time = $('#remind_time').val();
			$.ajax({
				url: 'index.php?act=special_details&op=special_remind',
				type: 'post',
				data: {mobile: mobile, vcode: vcode,  special_id: special_id, is_vcode: is_vcode,notice_type:notice_type,remind_time:remind_time},
				dataType: 'json',
				success: function (data) {
					if (data.code == 'ok') {
						showDialog(data.message, 'succ', '','','','','','','',2);
						$('.cd-popup1').removeClass('is-visible1');
						<?php if($output['special_info']['special_start_time'] <= time() && $output['special_info']['special_end_time'] > time()){?>
						$('#remind_msg').html('已设置提醒');
						$('#remind_btn').removeClass('cd-popup-trigger1');
						<?php }elseif($output['special_info']['special_start_time'] > time()){?>
						$('#remind_msg1').html('已设置提醒');
						$('#remind_btn1').removeClass('cd-popup-trigger1');
						<?php }?>

						setTimeout("location.reload()",2000);
					} else {
						showDialog(data.message, 'error','','','','','','','',2);
					}
				}
			});
		});


		$('#cancel_remind_btn').on('click', function () {
			var special_id = <?php echo $output['special_info']['special_id']?>;
			var sn_type = $(this).attr('data-id');
			ajax_get_confirm(
					'您确定要取消提醒吗？',
					AUCTION_SITE_URL+'/index.php?act=special_details&op=cancel_special_remind&special_id='+ special_id + '&sn_type='+ sn_type
			);
		})


    });
    function timer(_remain_date,_auction_id) {
        window.setInterval(function () {
            var t = _remain_date;
            var day = 0, hour = 0, minute = 0, second = 0; //时间默认值
            if (t >= 0) {
                day=Math.floor(t/1000/60/60/24);
                hour=Math.floor(t/1000/60/60%24);
                minute=Math.floor(t/1000/60%60);
                second=Math.floor(t/1000%60);
            }
            if (minute <= 9) minute = '0' + minute;
            if (second <= 9) second = '0' + second;
            $('#day_show_'+_auction_id).html(day);
            $('#hour_show_'+_auction_id).html(hour);
            $('#minute_show_'+_auction_id).html(minute);
            $('#second_show_'+_auction_id).html(second);
        }, 1000);
    }
</script>