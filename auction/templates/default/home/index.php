<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_login.css" rel="stylesheet" type="text/css">
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/qll_profit.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>

<style type="text/css">
    .home-focus-layout .right-sidebar{filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#B2000000', endColorstr='#B2000000');background:rgba(0,0,0,0.7);width: 120px; border:none; margin-left: 480px;}
    .home-focus-layout .right-sidebar .top-account img,.home-focus-layout .right-sidebar .activity img{margin: 10px auto; width:68px; height:68px; overflow: hidden; border-radius: 100px;}
    .home-focus-layout .right-sidebar .top-account p{text-align:center; font-size: 14px; color: #fff}
    .home-focus-layout .right-sidebar .top-account a,.home-focus-layout .right-sidebar .activity a{color:#fff; text-align: center;display:block}
    .home-focus-layout .right-sidebar .login{padding:0 14px; background: #800000;width:54px;border-radius: 100px;margin: 10px auto !important}
    .home-focus-layout .right-sidebar .activity h1{filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#B2434343', endColorstr='#B2434343');background:rgba(67,67,67,0.7);text-align: center; color: #fff;padding: 4px 0;margin: 8px 0 6px;}
    .home-focus-layout .right-sidebar .activity a{margin:2px 0}
    .full-screen-slides li a {
        display: block;
        width: 100% !important;
        height: 480px !important;
        text-indent:0px !important;
        margin-left: 0;
        position: inherit;
        z-index: 2;
        left: 0;
    }
    .full-screen-slides li a img{
        width:100% !important;
        height:480px !important;
    }
	.public-nav-layout .all-category .category{height:479px;}
</style>
<div class="home-focus-layout">
    <?php if (!empty($output['list'])) { ?>
        <div class="ncg-slides-banner">
            <ul id="fullScreenSlides" class="full-screen-slides">
                <?php foreach($output['list'] as $p) { ?>
                    <li><a href="<?php echo $p['url'];?>" target="_blank"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_AUCTION.'/'.$p['pic'];?>"></a></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    </div>
<div class="clear"></div>

<body>



<div class="date" style="display: none;">
    <div class="middle-box">
        <div class="date-list">
            <h1 class="fl" style="margin: 0 44px; line-height:34px;font-size: 16px">日期</h1>
            <div class="fl select-month">
                <select class="month">
                    <option value="01" <?php if($output['month'] == '01'){?>selected="selected" <?php }?>>一月</option>
                    <option value="02" <?php if($output['month'] == '02'){?>selected="selected" <?php }?>>二月</option>
                    <option value="03" <?php if($output['month'] == '03'){?>selected="selected" <?php }?>>三月</option>
                    <option value="04" <?php if($output['month'] == '04'){?>selected="selected" <?php }?>>四月</option>
                    <option value="05" <?php if($output['month'] == '05'){?>selected="selected" <?php }?>>五月</option>
                    <option value="06" <?php if($output['month'] == '06'){?>selected="selected" <?php }?>>六月</option>
                    <option value="07" <?php if($output['month'] == '07'){?>selected="selected" <?php }?>>七月</option>
                    <option value="08" <?php if($output['month'] == '08'){?>selected="selected" <?php }?>>八月</option>
                    <option value="09" <?php if($output['month'] == '09'){?>selected="selected" <?php }?>>九月</option>
                    <option value="10" <?php if($output['month'] == '10'){?>selected="selected" <?php }?>>十月</option>
                    <option value="11" <?php if($output['month'] == '11'){?>selected="selected" <?php }?>>十一月</option>
                    <option value="12" <?php if($output['month'] == '12'){?>selected="selected" <?php }?>>十二月</option>
                </select>
            </div>
            <div class="date-list-box fl">
                <div class="date-list-ul">
                    <ul id="product-list-color" style="width:10000px;">
                        <?php foreach($output['month_list'] as $key=>$val){?>
                            <li class="js-mytooltip type-inline-block <?php if($val['time'] == $output['time']){?>today<?php }?>" data-mytooltip-custom-class="align-center" data-mytooltip-direction="bottom" data-mytooltip-template="今日总场次：<?php echo $val['special_all_count']?>场次&nbsp;&nbsp;<font style='color:#800000'>围观：<?php echo $val['special_all_click']?>场</font>"><?php echo $val['time']?></li>
                        <?php }?>
                    </ul>
                </div>
                <div class="button-next">
                    <a href="javascript:void(0);" class="date-list-left prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <a href="javascript:void(0);" class="date-list-right next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".date-list-box").slide({
                    titCell: "",
                    mainCell: ".date-list-ul ul",
                    autoPage: true,
                    effect: "leftLoop",
                    autoPlay: false,
                    vis: 11,
                    pnLoop:false});
            });
        </script>
    </div>
</div>

<div class="clear"></div>
<!--日期选择结束-->
<div class="wrapper auction-index">
    <!--首页新添加开始-->
    <div class="artist-profile middle-box">
        <div class="artist-profile-word fl">
            <h1>艺术家们都来啦！</h1>
            <h2>你还在犹豫什么？</h2>
          <a href="/shop/index.php?act=show_joinin&op=index" title="" target="_blank">立即入驻艺诺网&nbsp;&gt;&gt;</a>
        </div>
        <ul class="artist-list fl">
            <?php if(!empty($output['artist_list'])){?>
            <?php foreach($output['artist_list'] as $k=>$v){?>
            <li>
                <a href="index.php?act=index&op=artist_detail&artist_id=<?php echo $v['artist_vendue_id']?>" title="" target="_blank">
					<span class="123">
						<p><?php echo $v['artist_name']?></p>
						<p><?php echo $v['artist_job_title']?></p>
						<i class="icon-double-angle-down"></i>
					</span>
                    <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$v['artist_image']?>"  alt=""/>
                </a>
            </li>
                <?php }?>
           <?php }?>
        </ul>
        <a href="index.php?act=index&op=artist_list" title="" target="_blank" class="artist-more">查看更多艺术家&nbsp;&gt;&gt;</a>
        <div class="clear"></div>
    </div>
    <section class="lm_home_pub ">
        <div class="lm_home_top mtb15">
            <h2 class="lm_home_titile qll_home_titile">
                <img src="<?php echo SHOP_TEMPLATES_URL; ?>/images/qll/profit_title_1.png" alt="">
            </h2>
        </div>
        <div class="lm_home_con">
            <ul class="qll_profit lm_clear">
                <?php if(!empty($output['store_list'])){ ?>
                    <?php foreach ($output['store_list'] as $key=>$val) {?>
                        <li>
                            <div class="qll_profit_img">
                                <a href="<?php echo urlVendue('special_details','index',array('special_id'=>$val['special_id']))?>">
                                    <img src="<?=$val['wap_image_path']?>" alt="">
                                </a>
                                <span class="qll_finance_ad">
                        <span>
                            保证金年收益率<strong><?=$val['special_rate']?></strong>%
                            计息期<?=$val['day_num']?>天
                        </span>
                    </span>
                            </div>
                            <div class="profit_activity_info">
                                <div class="profit_activity_info_top lm_clear">
                                    <h2><?=$val['special_name']?></h2>
                                    <div class="profit_activity_details">
                                        <span class="qll_address"><?=$val['store_name']?></span>
                                        <span class="qll_count">拍品数 <?=$val['auction_num']?>件</span>
                                        <span class="vote_count"><i><img src="<?php echo SHOP_TEMPLATES_URL;?>/lm/images/lm_eye.png" alt=""></i><?=$val['special_click']?>人次</span>
                                    </div>
                                </div>
                                <div class="profit_activity_info_bottom lm_countdown" data-date="<?=$val['special_remain_rate_time']?>">
                                    <span class="qll_timestate">距赠送收益截止</span>
                                    <?php// date('Y-m-d H:i:s',$val['special_rate_time'])?>
                                    <?php// $val['special_rate_time'] ?>
                                    <div class="qll_date lm_date">
                                        <div class="lm_three lm_date_info">09</div><span>天</span>
                                        <div class="lm_two   lm_date_info">11</div><span>时</span>
                                        <div class="lm_one   lm_date_info">42</div><span>分</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php }}?>
            </ul>
        </div>
    </section>

    <div class="middle-box">
        <div id="new-tab" class="fl">
            <div class="auction-headlines">
                <h1 class="fl">拍卖头条</h1>
                <div class="fl">
                    <?php if(!empty($output['au_head_list'])){?>
                    <span class="all-block fl">推荐</span>
                    <div id="scroll-news">
                        <?php foreach($output['au_head_list'] as $k=>$v){?>
                        <a href="<?php echo $v['au_head_url']?>"><?php echo $v['au_head_title']?></a>
                        <?php }?>
                    </div>
                    <?php }?>
                    <script language="javascript">var box=document.getElementById("scroll-news"),can=true;box.innerHTML+=box.innerHTML;box.onmouseover=function(){can=false};box.onmouseout=function(){can=true};new function(){var stop=box.scrollTop%24==0&&!can;if(!stop)box.scrollTop==parseInt(box.scrollHeight/2)?box.scrollTop=0:box.scrollTop++;setTimeout(arguments.callee,box.scrollTop%24?10:1500)};</script>
                </div>
            </div>
            <div class="tab_box">
                <div>
                    <?php if(!empty($output['special_list'])){?>
                        <?php foreach($output['special_list'] as $k=>$v){?>
                    <a href="index.php?act=special_details&op=index&special_id=<?php echo $v['special_id']?>" target="_blank" title="">
                        <div class="lm_finance_pc_aut">
                          <img src="<?php echo getVendueLogo($v['special_image'])?>"  alt=""/>
                          <?php if ($v['special_rate'] > 0){?>
                          <span class="lm_finance_ad"><span>保证金年收益率<strong><?php echo $v['special_rate']?></strong>%</span></span>
                          <?php } ?>
                        </div>
                        <div class="new-tab-list fr">
                            <div class="new-tab-list-title fl">
                                <h1><?php echo $v['special_name']?></h1>
                                <?php if($v['delivery_mechanism'] != ''){?>
                                <span>送拍机构&nbsp;&nbsp;<b><?php echo $v['delivery_mechanism']?></b></span>
                                <?php }?>
                            </div>

                            <?php if($v['special_start_time'] <= time() && $v['special_end_time'] > time()){?>
                            <div class="offer fr black-bg">
                                <b><?php echo $v['all_bid_number']?></b>
                                <b>次出价</b>
                            </div>
                                <?php }elseif($v['special_end_time'] <= time()){?>
                                <div class="offer fr gray-bg">
                                    <b><?php echo $v['jian_num']?></b>
                                    <b>件成交</b>
                                </div>
                                <?php }elseif($v['special_start_time'] > time()){?>
                            <div class="offer fr green-bg">
                                <b><?php echo $v['special_click']?></b>
                                <b>次围观</b>
                            </div>
                                <?php }?>

                            <div class="clear"></div>
                            <?php if($v['special_start_time'] <= time() && $v['special_end_time'] > time()){?>
                            <dd class="time">
                                <span class="sell">距&nbsp;结&nbsp;束&nbsp;</span>
									<span class="time-remain" count_down="<?php echo $v['special_end_time']-TIMESTAMP; ?>">
									<em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒</span>
                            </dd>
                                <?php }elseif($v['special_end_time'] <= time()){?>
                                <dd class="time">
                                    <span class="sell">结束时间&nbsp;<?php echo date('m月d日 H:i',$v['special_end_time'])?></span>
                                </dd>
                                <?php }elseif($v['special_start_time'] > time()){?>
                                <dd class="time">
                                    <span class="sell">开拍时间&nbsp;<?php echo date('m月d日 H:i',$v['special_start_time'])?></span>
                                </dd>
                                <?php }?>
                        </div>
                    </a>
                            <?php }?>
                    <?php }else{?>
                        <div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂时无专场数据</span></div>
                    <?php }?>

                    <input id="special_count" type="hidden" value="">
                    <?php if($output['special_count'] != 0){?>
                        <a href="javascript:;" id="more_button" class="tab-more" onclick="special_more()">未来七天共&nbsp;<b id="count"><?php echo $output['special_count']?></b>&nbsp;场</a>
                    <?php }?>
                </div>
            </div>
        </div>
        <div class="hot-list fr">
            <?php if(!empty($output['right_list'])){?>
            <a href="<?php echo $output['right_list']['url']?>" class="lucky-bag">
                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_AUCTION.'/'.$output['right_list']['pic'];?>"  alt=""/>
            </a>
            <?php }?>
            <div class="hot-goods fr">
                <img src="templates/default/images/new-index/hot-goods-title.jpg"  alt=""/>
                <ul>
                    <?php if(!empty($output['hot_auctions'])){?>
                        <?php foreach($output['hot_auctions'] as $k=>$v){?>
                    <li>
                        <a href="<?php echo urlVendue('auctions','index',array('id'=>$v['auction_id']))?>" title="">
                            <img src="<?php echo cthumb($v['auction_image'], 240,$v['store_id']);?>"  alt=""/>
                            <div class="title"><?php echo $v['auction_name']?></div>
                            <div class="info"><span class="fl">当前价&nbsp;<b><?php if($v['current_price'] != 0.00){echo ncPriceFormatForList($v['current_price']);}else{echo ncPriceFormatForList($v['auction_start_price']);}?></b></span><span class="fr"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;<b><?php echo $v['bid_number']?></b>&nbsp;次</span></div>
                        </a>
                    </li>
                            <?php }?>
                    <?php }?>

                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <!--首页新添加结束-->
</div>
</body>
<script type="text/javascript">
  //初始化加载个数
    $("#special_count").val(<?php echo $output['special_num']?>);
    var flag = true;
    function special_more() {
        //记录加载起始位置
        var special_start = parseInt($('#special_count').val() - <?php echo $output['special_num']?>);
        var html = '';
        $.getJSON("<?php echo C('auction_site_url')?>/index.php?act=index&op=special_ajax",
            {start:special_start},
            function (data) {
                var speciallist = data.special_list;
                var num = data.num;
                if(speciallist != ''){
                    $.each(speciallist,function (i) {
                        html += '<a href="index.php?act=special_details&op=index&special_id='+speciallist[i].special_id+'" target="_blank" title=""> <img src="'+ UPLOAD_SITE_URL+'/shop/vendue' + '/' + speciallist[i].special_image+'" alt=""/> <div class="new-tab-list fr">';
                        if(speciallist[i].delivery_mechanism != ''){
                            html += '<div class="new-tab-list-title fl"> <h1>'+speciallist[i].special_name+'</h1> <span>送拍机构&nbsp;&nbsp;<b>'+speciallist[i].delivery_mechanism +'</b></span> </div>';
                        }
                        html += '<div class="offer fr green-bg"> <b>'+speciallist[i].special_click+'</b> <b>次围观</b> </div> <div class="clear"></div>';
                        html +='<dd class="time"> <span class="sell">开拍时间&nbsp;'+ speciallist[i].special_start_date +'</span></dd>';
                        html+= '</div> </a>';

                    });
                    $('#special_count').before(html);
                    if(data.special_num == 0){
                        $("#more_button").remove();
                    }
                }else {
                    $("#more_button").remove();
                }
                $('#special_count').val(parseInt(num) + <?php echo $output['special_num']?>);

            });
    }


</script>

<script type="text/javascript">
    var data_year = <?php echo date('Y',TIMESTAMP)?>;
    var data_month = '<?php echo date('m-d',TIMESTAMP)?>';
    $(function(){
        $('.month').change(function(){
            var data_param = $(this).val();
            var month = data_year + '-'+ data_param;

            $.ajax({
                url: "<?php echo C('auction_site_url')?>/index.php?act=index&op=getMonthDays&type=html",
                data: {data_param:month},
                type: "post",
                dataType:'json',
                success: function (result) {
                    var html = '';
                    if (result != '') {
                        $('#product-list-color').remove();
                        $('.tempWrap').remove();
                        html += '<ul id="product-list-color">';
                        for(var i=0;i<result.length;i++){
                            if(result[i].time != data_month){
                                html += '<li class="js-mytooltip type-inline-block" ' +
                                    'data-mytooltip-custom-class="align-center" ' +
                                    'data-mytooltip-direction="bottom" ' +
                                    'data-mytooltip-template="今日总场次：'+result[i].special_all_count+'场次&nbsp;&nbsp;<font style=\'color:#800000\'>围观：'+result[i].special_all_click+'场</font>">'+ result[i].time + '</li>';
                            }else{
                                html += '<li class="js-mytooltip type-inline-block today" data-mytooltip-custom-class="align-center" data-mytooltip-direction="bottom" data-mytooltip-template="今日总场次：'+result[i].special_all_count +'场次&nbsp;&nbsp;<font style=\'color:#800000\'>围观：'+result[i].special_all_click+'场</font>">'+ result[i].time + '</li>';
                            }

                        }
                        html += '</ul>';
                    }
                    $('.date-list-ul').html(html);
                    $(".date-list-box").slide({
                        titCell: "",
                        mainCell: ".date-list-ul ul",
                        autoPage: true,
                        effect: "leftLoop",
                        autoPlay: false,
                        scroll: 1,
                        vis: 11,
                        pnLoop:false});
                    updatemytooltip();
                }
            })

        });
    });

//    var $ =  jQuery.noConflict();
</script>
<!--日期选择开始-->
<script src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/jquery.mytooltip.js"></script>
<script src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/tooltip-script.js"></script>
<script type="text/javascript" src="<?php echo VENDUE_RESOURCE_SITE_URL;?>/js/jquery.SuperSlide.js"></script>


<script>
    function lmCuteDown() {
        // console.log($('.cutedown').length);
        $('.lm_countdown').each(function (i, e) {
            var date = parseInt($(this).attr('data-date')),
                three, two, one, sec,
                $data = $(this).find('.lm_date');
            function showTime(type) {
                if (date <= 0) {
                    clearInterval(displayTime);
                    return;
                }
                date -= 1;
                if (arguments.length === 2 && arguments[0].length === 3) {
                    if (arguments[1] === 1) {
                        three = Math.floor(date / 60 / 60 / 24);
                        two = Math.floor(date / 60 / 60 % 24);
                        one = Math.floor(date / 60 % 60);
                    } else {
                        three = Math.floor(date / 60 / 60 % 24);
                        two = Math.floor(date / 60 % 60);
                        one = Math.floor(date % 60);
                    }
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>' + arguments[0][0] + '</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>' + arguments[0][1] + '</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>' + arguments[0][2] + '</span>';
                } else {
                    three = Math.floor(date / 60 / 60 / 24);
                    two = Math.floor(date / 60 / 60 % 24);
                    one = Math.floor(date / 60 % 60);
                    sec = Math.floor(date % 60);
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    if (sec < 10) {
                        sec = '0' + sec
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>天</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>时</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>分</span>\
                <div class="lm_zeo   lm_date_info">' + sec + '</div><span>秒</span>';
                }
                $data.html(html);
                // console.log(three);
                // console.log(two);
                // console.log(one);
                if(three == 0 && two == 0 && one == 0){
                    // console.log(2222);
                    location.reload();
                }
            }

            if ($(this).attr('data-type') != 3) {
                if (date > 86400) {
                    //时间戳如果是s的时候不是s则多除1000
                    var displayTime;
                    if (date == -1) {
                        clearInterval(displayTime);
                        return;
                    }
                    displayTime = setInterval(function () {
                        showTime(['天', '时', '分'], 1);
                    }, 1000);
                } else if (date < 86400) {
                    var displayTime;
                    displayTime = setInterval(function () {
                        showTime(['时', '分', '秒'], 2);
                    }, 1000);
                }
            }else{
                var displayTime;
                displayTime = setInterval(function () {
                    showTime(['天', '时', '分', '秒'], 1);
                }, 1000);
            }
        });
    }

    lmCuteDown();
</script>