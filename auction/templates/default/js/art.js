// $(function($) {
//     var now_page = 1,
//         windW = $(window).width(),
//         sdFlag = true,
//         firstReq = true,
//         total_pages = 0;
//     // sdflag true 多图 else 单图
//
//     var array = [];
//     // 当是多图多时候
//     function lm_reqList(current_page, per_count) {
//         $.ajax({
//             url: api_url,
//             type: 'Get',
//             dataType: 'json',
//             data: {
//                 groups: 'works',
//                 per_count: per_count,
//                 page: current_page
//             },
//             success: function(data) {
//                 // Math.ceil(id/4) ===第n页
//                 // id/4
//                 var $dom = sdFlag ? $('.lm_art_right .lm_art_list') : $('.lm_art_right .lm_art_work_con');
//                 $dom.html('');
//                 var html = '';
//                 now_page = data.meta.pagination.current_page;
//                 var total = data.meta.pagination.total;
//                 total_pages = data.meta.pagination.total_pages;
//                 $.each(data.data, function(index, el) {
//
//                     // 多图时
//                     if (sdFlag) {
//                         html +=
//                             '<li id="' + el.id + '">\
//                            <a href="#" class="lm_top_imgs">\
//                              <img src="' + el.cover + '" alt="">\
//                              <div class="lm_infos">\
//                                <span><i><img src="resource/imgs/lm_eye.png"></i>' + el.vote_count + '</span>\
//                                <span><i><img src="resource/imgs/lm_heart.png"></i>' + el.view_count + '</span>\
//                              </div>\
//                            </a>\
//                            <div class="lm_art_info">\
//                                <div class="lm_art_info_top lm_clear">\
//                                    <h2 class="lm_art_title"><p>' + el.title + '</p><span>￥' + el.price + '</span></h2>\
//                                    <a class="lm_art_shop" href="#">' + el.state + '</a>\
//                                </div>\
//                                <span class="lm_art_infos">创作时间：' + el.created_at + '   材料：' + el.type + '   尺寸：' + el.size + '</span>\
//                            </div>\
//                         </li>';
//                     } else {
//                         html =
//                             '<div class="lm_imgWrap" id="' + el.id + '">\
//                             <img src="' + el.cover + '" alt="">\
//                         </div>\
//                         <div class="lm_art_work_info">\
//                            <h2 class="title">' + el.title + '</h2>\
//                            <span class="time">创作时间：' + el.created_at + '</span>\
//                            <span class="type">材料：' + el.type + '</span>\
//                            <span class="size">尺寸：' + el.size + '</span>\
//                            <span class="price">￥' + el.price + '</span>\
//                            <div class="lm_infos">\
//                               <span><i><img src="images/lm_eye.png"></i>' + el.vote_count + '</span>\
//                               <span><i><img src="images/lm_heart.png"></i>' + el.view_count + '</span>\
//                            </div>\
//                            <div class="state">' + el.state + '</div>\
//                            <div class="lm_share">\
//                               <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_sina"><i></i></a>\
//                               <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_qq"><i></i></a>\
//                               <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_wx"><i></i></a>\
//                            </div>\
//                          </div>'
//                     }
//                 });
//                 // 请求完成初始化分页组件
//                 // 分页组件  首次加载执行。
//                 if (firstReq && sdFlag) {
//                     $('.M-box').pagination({
//                         pageCount: data.meta.pagination.total_pages,
//                         coping: true,
//                         homePage: '首页',
//                         endPage: '末页',
//                         prevContent: '上页',
//                         nextContent: '下页',
//                         count: 1,
//                         callback: function(api) {
//                             var current = api.getCurrent();
//                             lm_reqList(current, 4);
//                             now_page = current;
//                             $('.active').trigger('click');
//                         }
//
//                     });
//                 }
//                 firstReq = false;
//                 $dom.append(html);
//                 readLoad();
//             }
//         });
//     }
//     lm_reqList(now_page, 4);
//     $('.lm_art_center').addClass('lm_art_hide');
//     // 点击收缩展开的时候
//     $('.lm_art_center>span').on('click', function(e) {
//         if ($(this).hasClass('lm_art_prev')) {
//             $('.lm_art_center,.lm_art_right').addClass('active');
//             $('.lm_art_center').removeClass('lm_art_hide');
//         } else if ($(this).hasClass('lm_art_next')) {
//             $('.lm_art_center,.lm_art_right').removeClass('active');
//             $('.lm_art_center').addClass('lm_art_hide');
//         }
//     });
//     // 点击开关切换多图单图显示时
//     $('.lm_switch input.switch').on('click', function() {
//         var $double = $('.lm_work_double'),
//             $sign = $('.lm_work_sign');
//         if ($(this).prop('checked')) {
//             sdFlag = false;
//             now_page = $double.find('li').eq(0).attr('id');
//             lm_reqList(now_page, 1);
//             $sign.show();
//             $double.hide();
//         } else {
//             sdFlag = true;
//             now_page = Math.ceil($sign.find('.lm_imgWrap').attr('id') / 4)
//             lm_reqList(now_page, 4);
//             $sign.hide();
//             $double.show();
//             $('.M-box .fy_' + now_page + '').trigger('click')
//         }
//     });
//     // 单图列表
//     // 多图列表时点击切换页面
//     $('.lm_art_right .lm_work_sign>span').on('click', function() {
//         if ($(this).hasClass('lm_art_prev')) {
//             // 上一页
//             if (now_page === 1) {
//                 now_page = total_pages;
//             } else {
//                 now_page = now_page - 1;
//             }
//             lm_reqList(now_page, 1);
//         } else {
//             // 下一页
//             if (now_page === total_pages) {
//                 now_page = 1;
//             } else {
//                 ++now_page
//             }
//             lm_reqList(now_page, 1);
//         }
//     })
//
//     // 点击标题或者图片后请求单图页面
//     $('.lm_art_right .lm_work_double').on('click', '.lm_top_imgs,.lm_art_title', function() {
//         var $double = $('.lm_work_double'),
//             $sign = $('.lm_work_sign');
//         var id = $(this).parents('li').attr('id');
//         $('.lm_switch input.switch').trigger('click');
//         sdFlag = false;
//         $sign.show();
//         $double.hide();
//         lm_reqList(id, 1);
//         return false;
//     });
//
//     //页面右侧list动态控制
//     function readLoad() {
//         var defaultWinw = 1920,
//             defaultWinh = 960,
//             defaultw = 340,
//             defaulth = 275,
//             nowWinw = $(window).width(),
//             nowWinh = $(window).height(),
//             noww = (nowWinw * defaultw) / defaultWinw,
//             nowh = (nowWinh * defaulth) / defaultWinh;
//         // $('.lm_art_right .lm_art_list li').width(noww+' !important')
//
//         $('.lm_art_right .lm_art_list li').css({"width":"100px !important"});
//         $('.lm_art_right .lm_art_list li .lm_top_imgs').height(nowh);
//     }
//
//     // 中间个人信息高度计算
//     var windowH = $(window).height(),
//         $all = $('.lm_all'),
//         $part = $('.lm_part'),
//         $domtop = $('.lm_art_top'),
//         $domtoptext = $('.lm_art_top_text'),
//         textHeightResult = 0,
//         defaultH=600;
//     if(windowH<700){
//       defaultH=500;
//     }
//     $domtop.find('p').each(function(i, e) {
//         textHeightResult += $(this).height();
//     })
//     $domtop.height(windowH - defaultH + 'px');
//     if (textHeightResult > windowH - defaultH - 70) {
//         // 如果内容超出
//         $domtoptext.height(windowH - defaultH - 70 + 'px');
//         $all.show().siblings('span').hide();
//     } else {
//         $('.lm_art_top .setting span').hide();
//     }
//     $all.on('click', function() {
//         $part.show().siblings('span').hide();
//         $domtop.height('auto')
//         $domtoptext.height('auto')
//     })
//     $part.on('click', function() {
//         $all.show().siblings('span').hide();
//         $domtop.height(windowH - defaultH + 'px');
//         $domtoptext.height(windowH - defaultH - 70 + 'px');
//     })
// });
$(function($) {
    var arry = [],
        sdFlag = true,
        rangeFlag = false,
        firstReq = true,
        now_page=1,
        per_count = 4;

    function lm_reqList(current_page, idindex) {
        // 判断当请求是多图sdFlag=true或者单图请求超出范围时rangeFlag=false
        $.mask_pub.show();

        if($('.lm_art_center .lm_art_prev').is(':hidden')){
            $('.lm_mask_pub').width($(window).width()-250);
        }else{
            $('.lm_mask_pub').width($(window).width()-725);
        }
        if (sdFlag || !rangeFlag) {
            $.ajax({
                url: api_url,
                type: 'Get',
                dataType: 'json',
                data: {
                    groups: 'works',
                    per_count: per_count,
                    page: current_page
                },
                success: function(data) {
                    //如果没有数据的话
                    if(!data.data.length){
                      $('.lm_switch').hide();
                      $('.lm_art_right_con').html('<div class="lm_null"><div class="" style="">暂无作品</div></div>');
                    }

                    if (sdFlag) {
                        // 如果是多图
                        var $dom = $('.lm_art_right .lm_art_list'),
                            html = '';
                        now_page = data.meta.pagination.current_page;
                        total_pages = data.meta.pagination.total_pages;
                        $dom.html('');
                        $.each(data.data, function(index, el) {
                            var timeflag=el.created_at===''?'hidden':'',
                                typeflag=el.type===''?'hidden':'',
                                sizeflag=el.size===''?'hidden':'',
                                sales=el.auction_flag==='1'?'':el.sales==='2'?'':'￥';
                            html += '<li id="' + el.id + '">\
                               <a href="'+el.href+'" target="_blank" class="lm_top_imgs">\
                                 <img src="' + el.cover + '" alt="">\
                                 <div class="lm_infos">\
                                   <span><i><img src="resource/imgs/lm_eye.png"></i>' + el.vote_count + '</span>\
                                   <span><i><img src="resource/imgs/lm_heart.png"></i>' + el.view_count + '</span>\
                                 </div>\
                               </a>\
                               <div class="lm_art_info">\
                                  <div class="lm_art_info_top lm_clear">\
                                    <h2 class="lm_art_title"><a href="'+el.href+'" target="_blank"><p>' + el.title + '</p></a><span>'+ sales + el.price + '</span></h2>\
                                    <a class="lm_art_shop" href="#">' + el.state + '</a>\
                                 </div>\
                                 <div class="lm_art_infos">\
                                 <span class="time '+timeflag+'">创作时间：' + el.created_at + '</span>\
                                 <span class="type '+typeflag+'">材料：' + el.type + ' </span>\
                                 <span class="size '+sizeflag+'">尺寸：' + el.size + '</span>\
                                 </div>\
                              </div>\
                            </li>';
                        });
                        // 请求完成初始化分页组件
                        // 分页组件  首次加载执行。
                        if (firstReq && sdFlag) {
                            $('.M-box').pagination({
                                pageCount: data.meta.pagination.total_pages,
                                coping: true,
                                homePage: '首页',
                                endPage: '末页',
                                prevContent: '上页',
                                nextContent: '下页',
                                count: 1,
                                callback: function(api) {
                                    var current = api.getCurrent();
                                    lm_reqList(current);
                                    now_page = current;
                                    $('.active').trigger('click');
                                }
                            });
                        }
                        firstReq = false;
                        $dom.append(html);
                        readLoad();
                    } else {
                        var val = !idindex ? 0 : idindex;
                        singleShow(data, val);
                    }
                }
            })
                .done(function() {
                    $.mask_pub.hide();
                })
                .fail(function(erro) {
                    $.mask_pub.hide();
                })
        }
    }
    lm_reqList(1);

    function singleShow(data, idindex) {
        var $dom = $('.lm_art_right .lm_art_work_con');
        var html = '';
        var count = 3,
            zcresult = data.data[idindex];
        var total_pages = data.meta.pagination.total_pages,
            current_page = data.meta.pagination.current_page,
            nowpage = idindex;

        //两种情况，一种switch则为0，否则则为点击的index
        function _init(result) {
            $dom.html('');
            var sales=result.auction_flag==='1'?'':result.sales==='2'?'':'￥';
            html =
                '<div class="lm_imgWrap" id="' + result.id + '">\
                            <a href="'+result.href+'" target="_blank">\
                           <img src="' + result.cover + '" alt="">\
                            </a>\
                        </div>\
                        <div class="lm_art_work_info">\
                           <h2 class="title"><a href="'+result.href+'" target="_blank">' + result.title + '</a></h2>\
                           <span class="time">创作时间：' + result.created_at + '</span>\
                           <span class="type">材料：' + result.type + '</span>\
                           <span class="size">尺寸：' + result.size + '</span>\
                           <span class="price">'+ sales + result.price + '</span>\
                           <div class="lm_infos">\
                              <span><i><img src="resource/imgs/lm_eye.png"></i>' + result.vote_count + '</span>\
                              <span><i><img src="resource/imgs/lm_heart.png"></i>' + result.view_count + '</span>\
                           </div>\
                           <div class="state">' + result.state + '</div>\
                           <div class="lm_share">\
                              <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_sina"><i></i></a>\
                              <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_qq"><i></i></a>\
                              <a href="javascript:void(0);" onclick="ajax_form("weixin_form", "微信账号登录", "http://192.168.1.200/member/index.php?act=connect_wx&op=index", 360);" title="微信账号登录" class="lm_wx"><i></i></a>\
                           </div>\
                         </div>';
            $dom.append(html);
            if(!result.created_at){$dom.find('.time').hide()}
            if(!result.type){$dom.find('.type').hide()}
            if(!result.size){$dom.find('.size').hide()}
        }
        _init(zcresult);
        // 单图点击切换页面
        $('.lm_art_right .lm_work_sign .lm_art_prev').off().on('click', function(e) {
            if (nowpage === 0) {
                var nownum = current_page == 1 ? total_pages : current_page - 1;
                lm_reqList(nownum, 0);
                now_page=nownum;
            } else {
                nowpage = nowpage - 1;
                var zcarry = data.data[nowpage];
                _init(zcarry)
            }
            return false;
        });
        $('.lm_art_right .lm_work_sign .lm_art_next').off().on('click', function(e) {
            if (nowpage === count) {
                var nownum = current_page == total_pages ? 1 : parseInt(current_page) + 1;
                lm_reqList(nownum, 0);
                now_page=nownum;
            } else {
                nowpage = nowpage + 1;
                var zcarry = data.data[nowpage];
                _init(zcarry)
            }
            return false;
        });
    }

    // 点击开关切换多图单图显示时
    $('.lm_switch input.switch').on('click', function() {
        var $double = $('.lm_work_double'),
            $sign = $('.lm_work_sign');
        if ($(this).prop('checked')) {
            sdFlag = false;
            // now_page = $double.find('li').eq(0).attr('id');

            lm_reqList(now_page);
            $sign.show();
            $double.hide();
        } else {
            sdFlag = true;
            // now_page = Math.ceil($sign.find('.lm_imgWrap').attr('id') / 4)
            lm_reqList(now_page);
            $sign.hide();
            $double.show();
            $('.M-box .fy_' + now_page + '').trigger('click')
        }
    });

    $('.lm_art_center').addClass('lm_art_hide');
    // 点击收缩展开的时候
    $('.lm_art_center>span').on('click', function(e) {
        if ($(this).hasClass('lm_art_prev')) {
            $('.lm_art_center,.lm_art_right').addClass('active');
            $('.lm_art_center').removeClass('lm_art_hide');

        } else if ($(this).hasClass('lm_art_next')) {
            $('.lm_art_center,.lm_art_right').removeClass('active');
            $('.lm_art_center').addClass('lm_art_hide');
        }
    });

    // 点击标题或者图片后请求单图页面
    // $('.lm_art_right .lm_work_double').on('click', '.lm_top_imgs,.lm_art_title', function() {
    //     var $double = $('.lm_work_double'),
    //         $sign = $('.lm_work_sign');
    //     var id = $(this).parents('li').attr('id');
    //     $('.lm_switch input.switch').trigger('click');
    //     sdFlag = false;
    //     $sign.show();
    //     $double.hide();
    //     lm_reqList(now_page, $(this).parents('li').index());
    //     return false;
    // });

    //页面右侧list动态控制
    function readLoad() {
        var defaultWinw = 1920,
            defaultWinh = 960,
            defaultw = 340,
            defaulth = 275,
            nowWinw = $(window).width(),
            nowWinh = $(window).height(),
            noww = (nowWinw * defaultw) / defaultWinw,
            nowh = (nowWinh * defaulth) / defaultWinh;
        // $('.lm_art_right .lm_art_list li').width(noww+' !important')
        // var marginR=($('.lm_art_right_con').width()-(noww*2))/4;
        // $('.lm_art_right .lm_art_list li').css({'margin-right':""+marginR+"px",'margin-left':""+marginR+"px"});
        // $('.lm_art_right .lm_art_list li').width(noww);
        // $('.lm_art_right .lm_art_list li .lm_top_imgs').height(nowh);
    }

    // 中间个人信息高度计算
    var windowH = $(window).height(),
        $all = $('.lm_all'),
        $part = $('.lm_part'),
        $domtop = $('.lm_art_top'),
        $domtoptext = $('.lm_art_top_text'),
        textHeightResult = 0,
        defaultH=600;
    if(windowH<700){
        defaultH=500;
    }
    if(!$('.lm_art_bottom').length){
        defaultH=0;
    }
    textHeightResult=$('.lm_art_top_text').height();


    // $domtop.height(windowH - defaultH + 'px');
    if (textHeightResult > windowH - defaultH - 70) {
        // 如果内容超出
        // $domtoptext.height(windowH - defaultH - 78 + 'px');
        $all.show().siblings('span').hide();
    } else {
        $('.lm_art_top .setting span').hide();
    }
    $all.on('click', function() {
        $part.show().siblings('span').hide();
        $domtop.height('auto')
        $domtoptext.height('auto')
    });
    $part.on('click', function() {
        $all.show().siblings('span').hide();
        // $domtop.height(windowH - defaultH + 'px');
        // $domtoptext.height(windowH - defaultH - 78 + 'px');
    })
});



//全局遮罩进度
/*
 $.mask_pub.show();
 $.mask_pub.hide();
 */
$(function($) {
    $.mask_pub = {
        $html: $('<div class="lm_mask_pub"></div>'),
        show: function() {
            if ($('.lm_mask_pub').length) {
                $('.lm_mask_pub').remove()
            }
            $('html').append(this.$html);
            this.$html.html('<div class="lm_loader"><div class="lm_line-scale"><img src="resource/imgs/lm_loader1.gif" alt="" /></div></div>');
        },
        hide: function() {
            this.$html.remove();
        }
    };
}(jQuery));
