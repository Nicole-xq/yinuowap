<style>
    .focus_on {
        background: #800000 !important;
        color: #fff !important;
    }
</style>

<div class="box-01">
    <div class="title shop-name">
        <h4 class="fl"><?php echo $output['store_info']['store_name']?></h4><?php if(!empty($output['store_info']['store_qq'])){?>
            <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $output['store_info']['store_qq'];?>&site=qq&menu=yes" title="QQ: <?php echo $output['store_info']['store_qq'];?>"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo $output['store_info']['store_qq'];?>:52" style=" vertical-align: middle;"/></a>
        <?php }?>
        <?php if(!empty($output['store_info']['store_ww'])){?>
            <a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&amp;uid=<?php echo $output['store_info']['store_ww'];?>&site=cntaobao&s=1&charset=<?php echo CHARSET;?>" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=<?php echo $output['store_info']['store_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" alt="<?php echo $lang['nc_message_me'];?>" style=" vertical-align: middle;"/></a>
        <?php }?>
    </div>
    <div class="clear"></div>
    <div class="shop-info">
        <h1 class="fl"><?php echo $output['store_info']['store_credit_average'];?></h1>
        <p class="fl">
            <span>商品评价：<?php echo $output['store_info']['store_credit']['store_desccredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
            <span>服务态度：<?php echo $output['store_info']['store_credit']['store_servicecredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
            <span>物流速度：<?php echo $output['store_info']['store_credit']['store_deliverycredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
        </p>
    </div>
    <div class="clear"></div>
    <div class="shop-other">
        <a href="<?php echo urlShop('show_store', 'index', array('store_id' => $output['auction_info']['store_id']));?>">
            <i class="fa fa-home" aria-hidden="true"></i>&nbsp;进店逛逛</a>

        <a href="javascript:collect_store('<?php echo $output['store_info']['store_id'];?>','count','store_collect')" id="if_store_fav" class="<?php echo empty($output['if_store_favorites']) ? '' : 'focus_on'; ?>"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;<span id="store_fav_desc"><?php echo empty($output['if_store_favorites']) ? '关注店铺' : '已关注'; ?></span></a>
    </div>
</div>
<div class="clear"></div>