
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/home_goods.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/index-min.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/zoom.css" rel="stylesheet" type="text/css">
<link href="<?php echo VENDUE_TEMPLATES_URL;?>/css/fudai.css" rel="stylesheet" type="text/css">

<style type="text/css">
  .ncs-goods-picture .levelB,
  .ncs-goods-picture .levelC { cursor: url(<?php echo VENDUE_TEMPLATES_URL;?>/images/shop/zoom.cur), pointer;
  }
  .ncs-goods-picture .levelD { cursor: url(<?php echo VENDUE_TEMPLATES_URL;?>/images/shop/hand.cur), move\9;
  }
  .nch-sidebar-all-viewed { display: block; height: 20px; text-align: center; padding: 9px 0; }

  .player{position: absolute; overflow: hidden; opacity: 1; height: 320px; width: 324px; left: 16px; top: 20px; z-index:100}
  .btns .ncbtn,.ncs-cosult-askbtn .ncbtn{ color:#fff}
  .ncs-handle{ padding-top: 18px}
  #imageMenu ul li{margin-right: 8px;}
  .auction-middle-box{min-height:170px;height:auto !important}
  .jingpaibtn{background:none;border:none;cursor:pointer;margin:0 0 0 30px;width:124px;text-align: left}
  .value-box{float: left;margin:0 0 10px 10px;font-size: 0;}
  .title_lm{float: left;margin-top: 7px;}
  .qll_inp{display: inline-block;height: 30px;text-align: center; -moz-appearance:textfield; }
  input[type=number]::-webkit-inner-spin-button,
  input[type=number]::-webkit-outer-spin-button {-webkit-appearance: none;margin: 0;}
  .qll_count span{display: block;height: 17px;line-height:17px;text-align: center;width: 22px; }
  .qll_count img{width: 22px;height: 17px;}
  .qll_count{display: inline-block;vertical-align: middle;}
  .lm_bzjsy{
    position:absolute;
    top:55px;
    right:10px;
    font-size:14px;
    color:#880000 !important;
  }
  .lm_add-countDown{
width: 100% !important;
  }
  .lm_add-countDown .lm_countdown{
    background: #cccccc;
    border:#000 solid 1px;
    font-size: 14px;
    padding:5px;
    width: auto !important;
    float: left;
  }
  .lm_add-countDown .lm_timestate{
    display: inline-block;
  }
  .lm_add-countDown .lm_date .lm_date_info{
    display: inline-block;
    background: #fa193a;
    color:#fff;
    width:27px;
    height: 27px;
    line-height: 27px;
    font-size: 12px;
    text-align: center;
    margin-right: 3px;
  }
  .lm_add-countDown .lm_date{
    display: inline-block;
    width: auto !important;
    background-color: #ebe6d7;
    padding: 3px;
  }
  .lm_add-countDown .lm_date span{
    color:#000;
    font-size: 12px;
  }
  .lm_add-countDown .lm_add_date{
    float: right;
    width: auto !important;
    margin: 7px 0 0 0;
    font-size: 14px;
    color:#000;
  }
</style>
<script>
    var tb_line=true;
</script>
<script src="<?php echo VENDUE_RESOURCE_SITE_URL ?>/js/jquery.qrcode.js"></script>
<script src="<?php echo VENDUE_RESOURCE_SITE_URL ?>/js/qrcode.js"></script>
<script src="<?php echo VENDUE_RESOURCE_SITE_URL ?>/js/CountDown.js"></script>

<div class="wrapper pr">
  <input type="hidden" id="lockcompare" value="unlock" />
  <div class="ncs-detail ownshop">
    <!-- S 商品图片 -->
    <div class="preview fl">
      <div id="vertical" class="bigImg">
          <?php if (!empty($output['auction_video'])) {?>
            <video loop controls autoplay class="content-video" src="<?php echo $output['auction_video'] ?>" width="360" height="360"></video>
          <?php } ?>
        <img src="<?php echo cthumb($auction_info['auction_image'],'360',$auction_info['store_id']) ?>" width="360" height="360" nc-data-index="0" alt="" id="midimg" class="content-img" <?php if(!empty($output['auction_video'])){ ?> style="display:none;"  <?php }else{ ?> style="display:block;" <?php } ?> />

        <div style="display:none;" id="winSelector"></div>
      </div>
      <div class="smallImg">
        <div id="imageMenu">
          <ul>
              <?php if (!empty($output['auction_video'])) {?>
                <li id="onlickImg" class="list-video-img">
                  <i class="icon-play video-play"></i>
                  <video src="<?php echo $output['auction_video'] ?>" width="50" height="50" alt=""/></video>
                </li>
              <?php } ?>
              <?php if (!empty($output['auction_info']['image_list'])) {?>
                  <?php foreach($output['auction_info']['image_list'] as $key => $li) { ?>
                  <li class="list-img <?php if(empty($output['auction_video']) && $key == 0){ ?> list-img-hover <?php } ?>" >
                    <img id="nc_small" nc-data-index="<?php echo $key; ?>" src="<?php echo $li['_small'] ?>" width="68" height="68" alt=""/>
                  </li>
                  <?php } ?>
              <?php } ?>
          </ul>
        </div>
      </div><!--smallImg end-->
      <div id="bigView" style="display:none;"><img width="1280" height="1280" alt="" src="" /></div>
    </div>
    <div id="goodcover"></div>

    <!--    <div id="code">-->
    <!--      <img src="" alt="">-->
    <!--    </div>-->
    <input type="hidden" value='<?php echo $output['image_json']; ?>' id="image_json">
    <!-- S 商品基本信息 -->

    <!--新增拍卖部分开始-->
    <!--新增拍卖部分开始-->
    <div class="ncs-goods-summary">
      <div class="auction-middle-box">
        <div class="title">
          <div class="fl">
            <h1><?php echo $output['auction_info']['auction_name'] ?></h1>
                <!-- 通过审核 -->
                <!--正在进行-->
                  <?php if ($output['auction_info']['auction_start_time'] <= time() && ((empty($output['auction_info']['auction_end_true_t']) && $output['auction_info']['auction_end_time'] >= time()) || $output['auction_info']['auction_end_true_t'] >= time())) {?>
                  <dd class="time">
                    <span class="now fl">正在进行</span>
                    <span class="time-remain fl">
                             <span id="saleCountDown" class="time-remain"></span>

                            <div class="clear"></div>

                            <script>
                                $("#saleCountDown").ncCountDown({
                                    time: <?php echo $output['auction_info']['auctioning_time'] ?>,// 时间
                                    unitDay:"天",
                                    unitHour:"小时",
                                    unitMinute:"分钟",
                                    unitSecond:"秒",
                                    end: function() {
                                        //倒计时结束回调
                                        location.reload();
                                    }
                                });
                            </script>
                  </dd>
                  <?php } elseif ($output['auction_info']['auction_preview_start'] <= time() && $output['auction_info']['auction_preview_end'] >= time()) { ?>
                  <!--即将开拍-->
                  <dd class="time">
                    <span class="now fl" style="background: #1b591b">即将开拍</span>
                    <span class="time-remain fl" style="padding: 5px 0 0; font-size: 18px; color: #1b591b;display: none;">
                                        <?php echo date('Y-m-d H:i', $output['auction_info']['auction_start_time']) ?>
                      - <?php echo date('m-d H:i', $output['auction_info']['auction_end_time']) ?>
                                    </span>
                    <span id="saleCountDown_2" class="time-remain"></span>
                    <div class="clear"></div>
                    <script>
                        $("#saleCountDown_2").ncCountDown({
                            time: <?php echo $output['auction_info']['auction_remaining_start_time'] ?>,// 时间
                            unitDay:"天",
                            unitHour:"小时",
                            unitMinute:"分钟",
                            unitSecond:"秒",
                            end: function() {
                                //倒计时结束回调
                                location.reload();
                            }
                        });
                    </script>

                  </dd>
                  <?php } elseif ($output['auction_info']['auction_start_time'] >= time()) { ?>
                  <!--尚未开始-->
                  <dd class="time">
                    <span class="now fl" style="background: #666;<?php if($auction_info['interest_last_time'] != 0){?>display: none;<?php }?>">尚未开始</span>
                    <span class="time-remain fl" style="padding: 5px 0 0; font-size: 18px; color: #666"></span>
                    <div class="lm_add-countDown">
                      <span style="font-size:14px;color:#000;">距收益截止：</span>
                      <span id="saleCountDown_3" class="time-remain"  ></span>
<!--                      <div class="lm_add_date">拍卖时间：--><?php //echo date('m月d日 H:i',$output['auction_info']['auction_start_time'])?><!--</div>-->
                    </div>

                                  <div class="clear"></div>
                                  <script>
                                      $("#saleCountDown_3").ncCountDown({
                                          time: <?php echo $auction_info['interest_last_time'] ?>,// 时间
//                                          time: <?php //echo $output['auction_info']['auction_remaining_preview_start_time'] ?>//,// 时间
                                          unitDay:"天",
                                          unitHour:"小时",
                                          unitMinute:"分钟",
                                          unitSecond:"秒",
                                          end: function() {
                                              //倒计时结束回调
                                              location.reload();
                                          }
                                      });
                                  </script>
                                </dd>
                            <?php } else { ?>
                                <!--已结束-->
                                <dd class="time">
                                    <span class="now fl" style="background: #666">已结束</span>
                                    <span class="time-remain fl" style="padding: 5px 0 0; font-size: 18px; color: #666">
						                <?php
                            if (!empty($output['auction_info']['auction_end_true_t'])) {
                                echo date('Y-m-d H:i', $output['auction_info']['auction_end_true_t']);
                            }
                            ?>
                                    </span>
                    <div class="clear"></div>
                  </dd>
                  <?php } ?>
          </div>
            <?php if ($output['auction_info']['state'] == 3 || $output['auction_info']['state'] == 4) { ?>
                <?php
                if (!empty($output['relation_info'])) {
                    if ($output['relation_info']['is_remind'] == 1 || $output['relation_info']['is_remind_app'] == 1) { ?>
                      <a href="javascript:void(0);" class="fr" id="cancel_remind_btn">
                        <div class="clock-01"></div>
                        <span class="all-block">取消订阅</span>
                      </a>
                    <?php } else { ?>
                      <a href="javascript:void(0);" class="fr cd-popup-trigger1" id="remind_btn">
                        <div class="clock-01"></div>
                        <span class="all-block" id="remind_msg">结束前提醒</span>
                      </a>
                    <?php } } else { ?>
                  <a href="javascript:void(0);" class="fr cd-popup-trigger1" id="remind_btn">
                    <div class="clock-01"></div>
                    <span class="all-block" id="remind_msg">结束前提醒</span>
                  </a>
                <?php } ?>
            <?php } ?>
          <div class="clear"></div>
        </div>
              <?php if ($output['auction_info']['auction_preview_start'] <= time() && ($output['auction_info']['auction_end_true_t'] >= time() || (empty($output['auction_info']['auction_end_true_t']) && $output['auction_info']['auction_end_time'] >= time()))) {?>
                  <?php if (empty($output['relation_info']) || $output['relation_info']['is_bond'] == 0) { ?>
                <div class="auction-middle-box auction-middle-box_lm">
                  <div class="normal">
                    <h1><span>当前价</span><b>&yen;<?php echo $output['auction_info']['current_price'] ?></b><span>(<?php echo $output['auction_info']['bid_number'] ?>次出价)</span></h1>
                    <!--                                    <h1><span>保证金</span><b class="red-word">&yen;--><?php //echo $output['auction_info']['auction_bond'] ?><!--</b></h1>-->
                    <div class="record_lm_pc lm_clear">
                      <h2 class="title_lm">保证金</h2>
                      <div class="value-box">
                                        <span>
                                           <input class="qll_inp buy-num" data-min="<?php echo $output['auction_info']['auction_bond'] ?>" type="number" pattern="[0-9]*" class="buy-num" id="buynum" value="<?php echo $output['auction_info']['auction_bond'] ?>">
                                        </span>
                        <div class="qll_count">
                          <span class="add"><a href="javascript:void(0);"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/qll_add.png" alt=""/></a></span>
                          <span class="minus"><a href="javascript:void(0);"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/qll_minus.png" alt=""/></a></span>
                        </div>
                      </div>
                    </div>

                    <a href="javascript:void(0);" class="all-block cd-popup-trigger0"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/btn-01.png" alt=""/></a>
                    <?php if ($output['auction_info']['auction_bond_rate'] >0) { ?>
                    <div class="qll_finace_ad_1">
                      <span class="qll_finace_ad_new"><?php echo $output['auction_info']['auction_bond_rate']?>%</span>
                    </div>
                    <a class="lm_bzjsy" href="<?=C('cms_site_url')?>/index.php?act=special&op=special_detail&special_id=43" target="_blank">什么是保证金送收益?</a>
                    <?php } ?>
                  </div>
                  <div class="clear"></div>
                </div>
                  <?php } elseif ($output['relation_info']['is_bond'] == 1 && $output['auction_info']['auction_start_time'] < time()) {?>
                <div class="auction-middle-box">
                  <div class="reset normal">
                    <h1>
                      <span>当前价</span>
                      <b id="now_price_lm">&yen;<?php echo $output['auction_info']['current_price'] ?></b>
                      <span>(<i id="now_price_lm_num"><?php echo $output['auction_info']['bid_number'] ?></i>次出价)</span>
                    </h1>
                    <span class="fl" style="line-height: 40px;margin-right: 18px">
                                            出&nbsp;&nbsp;&nbsp;价
                                        </span>
                    <div class="value-box">
                      <input type="number"  class="spinnerExample qll_inp buy-num fl" id="offer_num"
                             value="<?php echo $output['auction_info']['current_price'] ?>" data-min="<?php echo $output['auction_info']['current_price'] ?>"/>
                      <div class="qll_count">
                        <span class="add"><a href="javascript:void(0);"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/qll_add.png" alt=""/></a></span>
                        <span class="minus"><a href="javascript:void(0);"><img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/jl/qll_minus.png" alt=""/></a></span>
                      </div>
                    </div>
                    <script type="text/javascript">
                        // 初始化插件
                        var offer_num_value  = <?php echo $output['auction_info']['current_price'] + $output['auction_info']['auction_increase_range'] ?>;
                        var step = <?php echo $output['auction_info']['auction_increase_range'] ?>;
                        $('.spinnerExample').spinner({
                            value: offer_num_value,
                            step: step,
                            min: offer_num_value
                        });
                    </script>
                    <div class="clear"></div>
                    <!-- 出价 -->
                    <a href="javascript:void(0);" class="all-block fl" style="margin-right: 14px;" id="member_offer">
                      <img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/btn-03.png" alt=""/>
                    </a>
                    <!-- 设置代理价 -->
                    <a href="javascript:void(0);" class="all-block fl cd-popup-trigger3">
                      <img src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/btn-04.png" alt=""/>
                    </a>
                  </div>
                  <div class="clear"></div>
                    <?php if ($output['relation_info']['is_agent'] == 1) { ?>
                      <h1 style="margin:16px 0;font-size:14px"><?php if ($output['relation_info']['agent_num'] >= $output['auction_info']['current_price']) { ?>
                          <span class="red-word">当前价格未超过您设置的代理出价</span>
                          <?php } ?>
                        <span style="margin: 6px 0 8px 18px">您当前设置的代理价为:<b>&yen;<?php echo $output['relation_info']['agent_num'] ?></b></span>
                      </h1>
                    <?php } ?>
                </div>
                  <?php } else { ?>
                <div class="auction-middle-box">
                  <div class="deal">
                    <h1><img style="margin-top: -10px" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/gavel.png" alt=""/>&nbsp;&nbsp;<span>等待开拍</span></h1>
                    <span class="all-block">起步价&nbsp;&nbsp;&nbsp;<b>&yen;<?php echo $output['auction_info']['current_price'] ?></b></span>
                  </div>
                </div>
                  <?php } ?>
              <?php } ?>
              <?php if (($output['auction_info']['auction_end_true_t'] < time() && !empty($output['auction_info']['auction_end_true_t'])) || ($output['auction_info']['auction_end_time'] <= time() && empty($output['auction_info']['auction_end_true_t']))) {?>
              <div class="auction-middle-box">
                  <?php if ($output['auction_info']['is_liupai'] == 1 && $output['auction_info']['auction_start_time'] <= time()) {?>
                    <div class="gone">
                      <h1 style="padding-left: 54px">
                        <img style="margin-top: -10px" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/gavel.png" alt=""/>&nbsp;&nbsp;<span>拍卖已流拍</span>
                      </h1>
                    </div>
                  <?php } else { ?>
                    <div class="deal">
                      <h1><img style="margin-top: -10px" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/gavel.png" alt=""/>&nbsp;&nbsp;<span>拍品已成交</span></h1>
                      <span class="all-block">成交金额&nbsp;&nbsp;&nbsp;<b>&yen;<?php echo $output['auction_info']['current_price'] ?></b></span>
                    </div>
                  <?php } ?>
                <div class="clear"></div>
              </div>
              <?php } ?>
              <?php if ($output['auction_info']['auction_preview_start'] > time()) { ?>
              <div class="auction-middle-box">
                <div class="deal">
                  <h1>
                    <img style="margin-top: -10px" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/gavel.png" alt=""/>&nbsp;&nbsp;<span>未开始拍卖</span></h1>
                  <span class="all-block">起拍价&nbsp;&nbsp;&nbsp;<b>&yen;<?php echo $output['auction_info']['auction_start_price'] ?></b></span>
                </div>
              </div>
              <?php } ?>

        <!--弹窗部分-->
        <!--出价-->
        <div class="cd-popup">
          <div class="cd-popup-container">
            <div class="cd-popup-top" style="height: 70px"><h1 style="font-size: 20px; text-align: center;padding-top: 20px">系统将暂时锁定<b id="bzj_lm"><?php echo $output['auction_info']['auction_bond'] ?></b>元在您的支付账户</h1></div>
            <div class="cd-buttons" style="background: #dbdbdb">
              <div class="box-02" style="padding-top: 0">
                <p style="text-align: left;font-size: 16px;color: #000">
                  1  若竞拍不成功，保证金自动释放；<br />
                  2  若竞拍成功后，保证金转货款，您需要支付剩余货款。
                </p>
                <div style="margin-top: 8px">
                  <form>
                    <div class="all-block">
                      <input type="checkbox" tabindex="3" style="outline:#6d6d6d none 0;margin-top: -2px" id="is_margin_submit">&nbsp;&nbsp;
                      <label for="" style="line-height:20px">
                        我同意
                        <a href="<?php echo empty($output['auction_article_list']['11']) ? '#' : urlMember('article', 'show', array('article_id' => $output['auction_article_list']['11']['article_id'])); ?>" target="_blank" class="red-word">
                          竞拍协议
                        </a>
                        并到支付界面支付，并且理解了
                        <a href="<?php echo empty($output['auction_article_list']['12']) ? '#' : urlMember('article', 'show', array('article_id' => $output['auction_article_list']['12']['article_id'])); ?>" target="_blank" class="red-word">
                          《保证金详细规则》
                        </a>
                        因拍品性质特殊，不支持7天无理由退货，请谨慎参拍。
                      </label>
                    </div>
                  </form>
                </div>
              </div>
              <div class="clear"></div>
              <div style="text-align: left;font-size: 14px; margin-top: 10px;">
                <a href="<?php echo empty($output['auction_article_list']['13']) ? '#' : urlMember('article', 'show', array('article_id' => $output['auction_article_list']['13']['article_id'])); ?>" target="_blank" style="color:#000">
                  如何大额支付？
                </a>
                <a href="<?php echo empty($output['auction_article_list']['14']) ? '#' : urlMember('article', 'show', array('article_id' => $output['auction_article_list']['14']['article_id'])); ?>" target="_blank" style="color:#000">
                  支付限额怎么办？
                </a>
              </div>
              <div class="botton">
                <input type="submit" value="确 定" id="margin_submit" class="fl">
                <input type="submit" value="取 消" class="cd-popup-close fr">
              </div>
              <div class="clear"></div>
            </div>
            <a href="#0" class="cd-popup-close">X</a>
          </div>
        </div>
          <?php if ($_SESSION['is_login'] == '1') {?>
            <!--服务提醒-->
            <div class="cd-popup1">
              <div class="cd-popup-container1">
                <div class="cd-popup-top"><h1>提醒设置</h1></div>
                <div class="cd-buttons">
                  <div class="box-01">
                    <div class="fl">
                      <h1>提醒时间</h1>
                      <img class="all-block" src="<?php echo VENDUE_TEMPLATES_URL; ?>/images/paimai/clock-black.png" width="34" height="34" alt=""/>
                    </div>
                    <span class="all-block fr">结束前60分钟
                                    <b class="gold-word">
                                        <?php echo format_date($output['auction_info']['auction_end_time']-1800).'&nbsp;'.date('H:i', $output['auction_info']['auction_end_time']-1800) ?>
                                    </b>
                                </span>
                    <span class="all-block fr">开拍时间
                                    <b class="gold-word">
                                        <?php echo format_date($output['auction_info']['auction_start_time']) ?>
                                    </b>
                                </span>
                  </div>
                  <div class="clear"></div>
                  <div class="box-02">
                                <span>拍卖会将根据您设置的手机(
                                    <?php
                                    if ($output['member_info']['member_mobile_bind'] == 1) {
                                        echo $output['member_info']['member_mobile'];
                                    } else {
                                        echo '<span style="color: red">'.'未绑定手机，请点击更改设置'.'</span>';
                                    } ?>
                                  )、站内信和邮件提醒您。</span>
                    <a href="javascript:void(0);" class="all-block" id="change_setting"> 更改设置&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  </div>
                  <div class="clear"></div>
                  <div class="box-02" style="display: none" id="contact_setting">
                    <div class="fl" id="contact_set" <?php if ($output['member_info']['member_mobile_bind'] == 0) { echo "style='display: none'"; } ?>>
                      <span>提醒方式</span>
                      <div class="all-block">
                        <label for="">
                          手机短信：<?php echo $output['member_info']['member_mobile']; ?>
                        </label>&nbsp;&nbsp;
                        <a href="javascript:void(0);" id="chang_contact">修改</a>
                      </div>
                    </div>
                    <div class="fl" id="change_set" <?php if ($output['member_info']['member_mobile_bind'] == 1) { echo "style='display: none'"; } ?>>
                      <span>提醒方式</span>
                      <form id="mobile_form">
                        <input type="hidden" value="<?php echo $output['auction_info']['auction_id']; ?>" id="auction_id">
                        <div class="all-block">
                          <label for="">手机短信:（之前有过设置，也将通过该号码进行提醒）</label>
                          <div class="phone-number">
                            <label for="">手机号码:&nbsp;&nbsp;
                              <input type="text" name="phone" id="phone" placeholder="" autocomplete="off" maxlength="11" value="<?php echo $output['member_info']['member_mobile']; ?>">
                            </label>
                            <label for="">手机短信:&nbsp;&nbsp;
                              <input type="text" name="vcode" id="vcode" placeholder="" autocomplete="off" maxlength="6"style="width: 70px;">
                              <input type="hidden" name="is_vcode" id="is_vcode" value="<?php echo $output['member_info']['member_mobile_bind'] == 1 ? '0' : '1'?>">
                              <a href="javascript:;" id="send_auth_code">
                                <span id="sending" style="display:none; color: #fff">正在</span>
                                <span class="send_success_tips" style="display: none; color: #fff">
                                                            <strong id="show_times" class="red mr5"></strong>秒后再次
                                                        </span>
                                获取验证码
                              </a>
                            </label>
                            <p class="send_success_tips hint mt10" style="display: none;">“安全验证码”已发出，请注意查收，请在
                              <strong>“30分种”</strong>内完成验证。
                            </p>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="qrcode">
                      <img src="<?php echo auctionsQRCode($output['auction_info']);?>" width="96" height="96" alt=""/>
                      <span class="all-block" style="font-size: 12px;">手机出价快人一步</span>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <div class="botton">
                    <input type="submit" value="确 定" id="mobile_submit" class="fl">
                    <input type="submit" value="取 消" class="cd-popup-close fr">
                  </div>
                  <div class="clear"></div>
                </div>
                <a href="#0" class="cd-popup-close">X</a>
              </div>
            </div>

            <!--设置代理出价-->
            <div class="cd-popup3">
              <div class="cd-popup-container3">
                <div class="cd-popup-top"><h1>设置代理出价</h1></div>
                <div class="cd-buttons">
                  <div class="box-02" style="padding-top: 0">
                      <?php if ($output['relation_info']['is_agent']) { ?>
                        <h3>您当前设置的代理出价为&nbsp;&nbsp;
                          <b class="red-word">
                            ¥ <?php echo $output['relation_info']['agent_num'] ?>
                          </b>
                        </h3>
                      <?php } else { ?>
                        <h3>尚未设置代理价
                        </h3>
                      <?php } ?>
                  </div>
                  <div class="clear"></div>
                  <div class="box-02">
                    <h3>请输入您的代理出价金额&nbsp;&nbsp;
                      <span>(不低于
                                        <b class="red-word" id="lm_daili_price">¥ <?php echo ($output['relation_info']['agent_num']>$output['auction_info']['current_price'])?$output['relation_info']['agent_num']:($output['auction_info']['auction_increase_range'] * 2 + $output['auction_info']['current_price']) ?></b>)
                                    </span>
                    </h3>
                    <label for=""><input type="text" id="agent_price" placeholder="" autocomplete="off" style="width: 400px;margin-top: 8px"></label>
                  </div>
                  <div class="clear"></div>
                  <div class="box-02" style="padding: 12px 0">
                    <h1 style="font-size: 16px;">提示:</h1>
                    <p>
                      1.代理价一旦设置不能下调或取消<br />
                      2.确认后系统将以最小加价幅度代您出价；<br />
                      3.若拍卖结束，当前价未达保留价，且您的代理出价高于保留价，系统将代您出价至保留价促成成交；<br />
                      4.代理出价需不低于当前价两个加价幅度。
                    </p>
                  </div>
                  <div class="botton">
                    <input type="submit" id="agent_submit" value="确 定" class="fl"><input type="submit" value="取 消" class="cd-popup-close fr">
                  </div>
                  <div class="clear"></div>
                </div>
                <a href="#0" class="cd-popup-close">X</a>
              </div>
            </div>
          <?php } ?>
        <script type="text/javascript">

          /*弹框JS内容*/
          jQuery(document).ready(function($){
              //打开窗口
              $('.cd-popup-trigger0').on('click', function(event){
                  event.preventDefault();
                  <?php if ($_SESSION['store_id'] == $output['auction_info']['store_id']) { ?>
                  alert('不能拍自己店铺的商品');return;
                  <?php } ?>
                  <?php if ($_SESSION['is_login'] !== '1'){?>
                  login_dialog();
                  <?php } ?>
                  console.log(parseInt($('#buynum').val())<parseInt($('#buynum').attr('data-min')),parseInt($('#buynum').val(),parseInt($('#buynum').attr('data-min'))));
                  if(parseInt($('#buynum').val())<parseInt($('#buynum').attr('data-min'))||!parseInt($('#buynum').val())){
                      $('#buynum').val(parseInt($('#buynum').attr('data-min')));
                      return false;
                  }else if(parseInt($('#buynum').val())>1000000000){
                      $('#buynum').val(1000000000);
                      return false;
                  }
                  else{
                      $('.cd-popup').addClass('is-visible');
                      $('#bzj_lm').text(parseInt($('#buynum').val()));
                  }



                  //$(".dialog-addquxiao").hide()
              });
              //关闭窗口
              $('.cd-popup').on('click', function(event){
                  if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
                      event.preventDefault();
                      $(this).removeClass('is-visible');
                  }
              });
              //ESC关闭
              $(document).keyup(function(event){
                  if(event.which=='27'){
                      $('.cd-popup').removeClass('is-visible');
                  }
              });

              //打开设置提醒窗口
              $('.cd-popup-trigger1').on('click', function(event){
                  event.preventDefault();
                  <?php if ($_SESSION['is_login'] !== '1'){?>
                  login_dialog();
                  <?php }else{ ?>
                  $('.cd-popup1').addClass('is-visible1');
                  <?php } ?>
              });
              //关闭窗口
              $('.cd-popup1').on('click', function(event){
                  if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup1') ) {
                      event.preventDefault();
                      $(this).removeClass('is-visible1');
                  }
              });
              //ESC关闭
              $(document).keyup(function(event){
                  if(event.which=='27'){
                      $('.cd-popup1').removeClass('is-visible1');
                  }
              });

              //打开窗口
              $('.cd-popup-trigger2').on('click', function(event){
                  event.preventDefault();
                  <?php if ($_SESSION['is_login'] !== '1'){?>
                  login_dialog();
                  <?php }else{ ?>
                  $('.cd-popup3').addClass('is-visible3');
                  <?php } ?>
              });

              //打开窗口
              $('.cd-popup-trigger3').on('click', function(event){
                  event.preventDefault();
                  $('.cd-popup3').addClass('is-visible3');
              });
              //关闭窗口
              $('.cd-popup3').on('click', function(event){
                  if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup3') ) {
                      event.preventDefault();
                      $(this).removeClass('is-visible3');
                  }
              });
              //ESC关闭
              $(document).keyup(function(event){
                  if(event.which=='27'){
                      $('.cd-popup3').removeClass('is-visible3');
                  }
              });
              $('#change_setting').on('click', function () {
                  $('#contact_setting').css('display', 'block');
              });
              $('#chang_contact').on('click', function () {
                  $('#change_set').css('display', 'block');
                  $('#contact_set').css('display', 'none');
              });

              $('#member_offer').on('click', function () {
                  var auction_id = $('#auction_id').val();
                  var offer_num = $('#offer_num').val();
                  var qll_value=parseInt(offer_num);
                  if(qll_value>1000000000){
                      $('#offer_num').val(1000000000);
                      return false;
                  }

                  $.ajax({
                      url: 'index.php?act=auctions&op=member_offer',
                      type: 'post',
                      data: {auction_id: auction_id, offer_num: offer_num},
                      dataType: 'json',
                      success: function (data) {
                          if (data.code == 1) {
                              showDialog(data.message, 'error');
                          } else {
                              showDialog(data.message, 'succ');
                              setTimeout('location.reload()', 2000);
                          }

                      }
                  });
              });

              $('#agent_submit').on('click', function () {
                  var auction_id = $('#auction_id').val();
                  var agent_price = $('#agent_price').val();
                  $.ajax({
                      url: 'index.php?act=auctions&op=agent_submit',
                      type: 'post',
                      data: {auction_id: auction_id, agent_price: agent_price},
                      dataType: 'json',
                      success: function (data) {
                          showDialog(data.msg, data.state);
                          setTimeout('location.reload()', 2000);
                      }
                  });
              });
          });
        </script>
        <div class="info">
          <div>
            <span><b><?php echo $output['auction_info']['num_of_applicants'] ?></b>&nbsp;人报名</span>
            <span><b><?php echo $output['auction_info']['set_reminders_num'] ?></b>&nbsp;人设置提醒</span>
            <span><b><?php echo $output['auction_info']['auction_click'] ?></b>&nbsp;次围观</span>
          </div>
          <ul>
            <li>起步价:&nbsp;&nbsp;&yen;<?php echo $output['auction_info']['auction_start_price'] ?></li>
            <li>加价幅度:&nbsp;&nbsp;&yen;<span id="lm_daili_jiajia"><?php echo $output['auction_info']['auction_increase_range'] ?></span></li>
            <li>保证金:&nbsp;&nbsp;&yen;<?php echo $output['auction_info']['auction_bond'] ?></li>
            <li>保留价:&nbsp;&nbsp;<?php if($output['auction_info']['auction_reserve_price']>0) {?> 有 <?php }else{ ?> 无 <?php } ?> </li>
            <li>特色服务：

                <?php if (!empty($output['store_pcontract'])) { ?>
                    <?php foreach ($output['store_pcontract'] as $value) { ?>
                    <span class="gold-word"><img src="<?php echo $value['cti_icon_url_60'] ?>" width="17" height="17">&nbsp;<?php echo $value['cti_name'] ?></span>&nbsp;&nbsp;
                    <?php } ?>
                <?php } else { ?>
                  <span class="gold-word">无</span>
                <?php } ?>
            </li>
            <li>送拍机构:&nbsp;&nbsp;<?php echo $output['auction_info']['delivery_mechanism'] ?></li>
          </ul>
          <div class="clear"></div>
          <div class="share">
            <a href="javascript:collect_auction('<?php echo $output['auction_info']['auction_id'];?>','count','guess_collect');" id="if_fav"<?php if($output['is_favorites']):?>class="selected"<?php endif;?>><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<font id="fav_desc"><?php if($output['is_favorites']):?>已关注<?php else:?>关注<?php endif;?></font></a>
            <div class="bdsharebuttonbox" style="display:inline-block;transform: translateY(2px);">
              <a href="#" class="share" style="background-image: none" data-cmd="more">
                <i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;分享
              </a>
            </div>
            <script>
/*                window._bd_share_config={
                    "common":{
                        "bdSnsKey":{},
                        "bdText":"",
                        "bdMini":"2",
                        "bdMiniList":false,
                        "bdPic":"",
                        "bdStyle":"1",
                        "bdSize":"16"
                    },"share":{}
                };
                with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];*/
            </script>
          </div>
        </div>
      </div>
    </div>
    <!--新增拍卖部分结束-->
    <div style="position: absolute; z-index: 2; top: -1px; right: -1px;">
      <!--S 出价记录 -->
      <div class="ncs-lal">
        <div class="title">出价记录(<?php echo $output['auction_info']['bid_number'] ?>)</div>
        <div class="content">
          <table width="260" border="0" cellspacing="0" cellpadding="0" class="auction-record" id="auction_content_lm">
            <tr>
              <td>状态</td>
              <td>竞拍人</td>
              <td>价格</td>
            </tr>
            <!--                        --><?php //if (!empty($output['bid_log_list_r'])) {?>
            <!--                            --><?php //foreach ($output['bid_log_list_r'] as $key => $value) { ?>
            <!--                                <tr>-->
            <!--                                    <td>-->
            <!--                                        --><?php //if ($key == 0) { ?>
            <!--                                            <span style="background: #800000">领先</span>-->
            <!--                                        --><?php //} else {?>
            <!--                                            <span style="background: #898989">出局</span>-->
            <!--                                        --><?php //} ?>
            <!--                                    </td>-->
            <!--                                    <td>--><?php //echo $value['member_name'] ?><!--</td>-->
            <!--                                    <td>&yen;--><?php //echo $value['offer_num'] ?><!--</td>-->
            <!--                                </tr>-->
            <!--                            --><?php //} ?>
            <!--                        --><?php //} else { ?>
            <!--                            <tr>-->
            <!--                                <td>无出价记录</td>-->
            <!--                            </tr>-->
            <!--                        --><?php //} ?>

          </table>
          <div class="clear"></div>
          <div class="auction-qrcode">
            <h1>扫描二维码到手机</h1>
            <div id="code">
              <img src="<?php echo auctionsQRCode($output['auction_info']);?>"  title="用商城手机客户端扫描二维码直达商品详情内容" style="display: block; margin: 0 auto;">
            </div>
          </div>
        </div>
      </div>
      <!--E 出价记录-->
    </div>
    <div class="clear"></div>
  </div>

  <div id="content" class="ncs-goods-layout expanded" >
    <!--竞拍流程-->
    <a class="bidding-process" href="javascript:void(0);">
      <h1 class="fl">竞拍流程</h1>
      <span class="fl all-block">

                <?php foreach($output['auction_article_list'] as $key => $article) { ?>
                    <?php if (!empty($article) && $key< 11) { ?>
                    <button class="jingpaibtn" onclick=window.open('<?php echo urlMember('article', 'show', array('article_id' => $article['article_id'])); ?>')>
                            <?php echo $article['article_title'] ?>
                    </button>
                    <?php } ?>
                <?php } ?>
            </span>
    </a>
    <div class="clear"></div>
    <div class="ncs-goods-main" id="main-nav-holder">
      <div class="tabbar pngFix" id="main-nav">
        <div class="ncs-goods-title-nav">
          <ul id="categorymenu">
            <li class="current"><a id="tabAuctionIntro" href="#content">拍品描述</a></li>
            <li><a id="tabBidRecord" href="#content">出价记录</a></li>
            <li><a id="tabServiceGuarantee" href="#content">服务保障</a></li>
            <li><a id="tabBondNotice" href="#content">保证金须知</a></li>
          </ul>
          <div class="switch-bar"><a href="javascript:void(0)" id="fold">&nbsp;</a></div>
        </div>
      </div>
      <div class="ncs-intro">
        <div class="content bd"  id="ncAuctionIntro">
          <ul class="nc-goods-sort">
              <?php if(is_array($output['auction_info']['auction_attr']) && !empty($output['auction_info']['auction_attr'])){ ?>
                  <?php foreach ($output['auction_info']['auction_attr'] as $val){
                      $val= array_values($val);
                      echo '<li>'.$val[0].$lang['nc_colon'].$val[1].'</li>';
                  }?>
              <?php } ?>
              <?php if (is_array($output['auction_info']['auction_custom'])) {
                  foreach ($output['auction_info']['auction_custom'] as $val) {
                      if ($val['value']!= '') {
                          echo '<li>'.$val['name'].$lang['nc_colon'].$val['value'].'</li>';
                      }
                  }
              } ?>
          </ul>
          <!--S 商品详情 -->
          <div class="ncs-goods-info-content">
              <?php echo $output['auction_info']['goods_body']; ?>
          </div>
          <!--E 商品详情 -->
        </div>
      </div>
      <!--出价记录-->
      <div class="ncs-intro guess bd" id="nc_bid_list" style="display: none">

      </div>
      <!--服务保障-->
      <div class="ncs-intro content bd" id="ncBondNotice" style="display: none">

      </div>
      <!--保证金须知-->
      <div class="ncs-intro content bd" id="ncServiceGuarantee" style="display: none">

      </div>
    </div>
    <!--拍卖详细左边部分-->
    <div class="auction-detail-left ncs-sidebar">
        <?php include template('store/info');?>
      <div class="clear"></div>
      <div id="vertical-slide" style="display: none;"><!--测试让暂时隐藏掉-->
        <h1><span>更多拍品</span></h1>
        <div class="clear"></div>
        <div class="slide-pic" id="slidePic">
          <div class="vertical-slide-pic">
            <ul>
                <?php if (!empty($output['more_auctions'])) { ?>
                    <?php foreach ($output['more_auctions'] as $auction) { ?>
                    <li>
                      <a href="<?php echo url('auctions', 'index', array('id' => $auction['auction_id']), false, APP_SITE_URL); ?>">
                        <img src="<?php echo cthumb($auction['auction_image']) ?>" alt="<?php echo $auction['auction_name'] ?>"/>
                        <div class="title">
                          <span class="title-name"><?php echo $auction['auction_name'] ?></span>
                          <div class="info">
                                                    <span class="fl">当前价&nbsp;<b class="red-word">￥
                                                            <?php
                                                            if ($auction['current_price']>0) {
                                                                echo $auction['current_price'];
                                                            } else {
                                                                echo $auction['auction_start_price'];
                                                            } ?>
                                                        </b></span>
                            <span class="fr">
                                                        <i class="fa fa-gavel red-word" aria-hidden="true"></i>&nbsp;
                                                        <b class="red-word"><?php echo $auction['bid_number'] ?></b>&nbsp;次
                                                    </span>
                            <div></div>
                          </div>
                        </div>
                      </a>
                    </li>
                    <?php } ?>
                <?php } ?>
            </ul>
          </div>
          <a class="gray" id="prev" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
          <a id="next" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
        </div>
        <div class="clear"></div>
      </div>
      <!--商品轮播js-->
      <script type="text/javascript">
          jQuery(function() {
              if (!$('#slidePic')[0]) return;
              var i = 0,
                  p = $('#slidePic ul'),
                  pList = $('#slidePic ul li'),
                  len = pList.length;
              var elePrev = $('#prev'),
                  eleNext = $('#next');
              var w = 224,
                  num = 3;
              p.css('width', w * len);
              if (len <= num) eleNext.addClass('gray');
              function prev() {
                  if (elePrev.hasClass('gray')) {
                      return
                  }
                  p.animate({
                      marginTop: -(--i) * w
                  }, 500);
                  if (i < len - num) {
                      eleNext.removeClass('gray')
                  }
                  if (i == 0) {
                      elePrev.addClass('gray')
                  }
              }
              function next() {
                  if (eleNext.hasClass('gray')) {
                      return
                  }
                  p.animate({
                      marginTop: -(++i) * w
                  }, 500);
                  if (i != 0) {
                      elePrev.removeClass('gray')
                  }
                  if (i == len - num) {
                      eleNext.addClass('gray')
                  }
              }
              elePrev.bind('click', prev);
              eleNext.bind('click', next);
          });
      </script>
    </div>
    <div class="clear"></div>
  </div>
</div>
<form id="buynow_form" method="post" action="http://localhost/yinuovip/www/shop/index.php">
  <input id="act" name="act" type="hidden" value="buy" />
  <input id="op" name="op" type="hidden" value="buy_step1" />
  <input id="cart_id" name="cart_id[]" type="hidden"/>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/sns.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.F_slider.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/waypoints.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/prism-min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.raty/jquery.raty.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script src="<?php echo RESOURCE_SITE_URL; ?>/js/lm_home.js"></script>
<script type="text/javascript">

    /** 辅助浏览 **/
    jQuery(function($){
        // 产品图片
        var count = $("#imageMenu li").length - 5; /* 显示 6 个 li标签内容 */
        var interval = $("#imageMenu li:first").width();
        var curIndex = 0;

        $('.scrollbutton').click(function(){
            if( $(this).hasClass('disabled') ) return false;

            if ($(this).hasClass('smallImgUp')) --curIndex;
            else ++curIndex;

            $('.scrollbutton').removeClass('disabled');
            if (curIndex == 0) $('.smallImgUp').addClass('disabled');
            if (curIndex == count-1) $('.smallImgDown').addClass('disabled');

            $("#imageMenu ul").stop(false, true).animate({"marginLeft" : -curIndex*interval + "px"}, 800);
        });
        // 解决 ie6 select框 问题
        $.fn.decorateIframe = function(options) {
            if ($.browser.msie && $.browser.version < 7) {
                var opts = $.extend({}, $.fn.decorateIframe.defaults, options);
                $(this).each(function() {
                    var $myThis = $(this);
                    //创建一个IFRAME
                    var divIframe = $("<iframe />");
                    divIframe.attr("id", opts.iframeId);
                    divIframe.css("position", "absolute");
                    divIframe.css("display", "none");
                    divIframe.css("display", "block");
                    divIframe.css("z-index", opts.iframeZIndex);
                    divIframe.css("border");
                    divIframe.css("top", "0");
                    divIframe.css("left", "0");
                    if (opts.width == 0) {
                        divIframe.css("width", $myThis.width() + parseInt($myThis.css("padding")) * 2 + "px");
                    }
                    if (opts.height == 0) {
                        divIframe.css("height", $myThis.height() + parseInt($myThis.css("padding")) * 2 + "px");
                    }
                    divIframe.css("filter", "mask(color=#fff)");
                    $myThis.append(divIframe);
                });
            }
        }
        $.fn.decorateIframe.defaults = {
            iframeId: "decorateIframe1",
            iframeZIndex: -1,
            width: 0,
            height: 0
        }
        //放大镜视窗
        $("#bigView").decorateIframe();
        //点击到中图
        var midChangeHandler = null;

        $("#imageMenu li img").bind("click", function(){
            console.log($(this).attr("id"));
            if ($(this).attr("id") != "onlickImg") {
                var list_key = $(this).attr("nc-data-index");
                var image_json = $('#image_json').val();
                var list_arr = new Array();
                var list_arr = JSON.parse(image_json);
                var image = list_arr[list_key]['_mid'];
                midChange(list_key,image);
                $("#imageMenu li").removeAttr("id");
                $(this).parent().attr("id", "onlickImg");
            }
        }).bind("mouseover", function(){
            if ($(this).attr("id") != "onlickImg") {
                var list_key = $(this).attr("nc-data-index");
                var image_json = $('#image_json').val();
                var list_arr = new Array();
                var list_arr = JSON.parse(image_json);
                var image = list_arr[list_key]['_mid'];
                midChange(list_key,image);
                $("#imageMenu li").removeAttr("id");
                $(this).parent().attr("id", "onlickImg");
            }
        }).bind("mouseout", function(){
            if($(this).attr("id") != "onlickImg"){
                $(this).removeAttr("style");
            }
        });
        function midChange(key,src) {
            $("#midimg").attr("src", src).load(function() {
                $(".content-img").attr("nc-data-index",key);
                changeViewImg(key);
            });
        }
        //大视窗看图
        function mouseover(e) {
            if ($("#winSelector").css("display") == "none") {
                $("#winSelector,#bigView").show();
            }
            $("#winSelector").css(fixedPosition(e));
            e.stopPropagation();
        }
        function mouseOut(e) {
            if ($("#winSelector").css("display") != "none") {
                $("#winSelector,#bigView").hide();
            }
            e.stopPropagation();
        }
        $("#midimg").mouseover(mouseover); //中图事件
        $("#midimg,#winSelector").mousemove(mouseover).mouseout(mouseOut); //选择器事件

        var $divWidth = $("#winSelector").width(); //选择器宽度
        var $divHeight = $("#winSelector").height(); //选择器高度
        var $imgWidth = $("#midimg").width(); //中图宽度
        var $imgHeight = $("#midimg").height(); //中图高度
        var $viewImgWidth = $viewImgHeight = $height = null; //IE加载后才能得到 大图宽度 大图高度 大图视窗高度

        function changeViewImg(key) {
            var image_json = $('#image_json').val();
            var list_arr = new Array();
            var list_arr = JSON.parse(image_json);
            var image = list_arr[key]['_big'];
            $("#bigView img").attr("src", image);
        }
        changeViewImg(0);
        $("#bigView").scrollLeft(0).scrollTop(0);
        function fixedPosition(e) {
            if (e == null) {
                return;
            }
            var $imgLeft = $("#midimg").offset().left; //中图左边距
            var $imgTop = $("#midimg").offset().top; //中图上边距
            X = e.pageX - $imgLeft - $divWidth / 2; //selector顶点坐标 X
            Y = e.pageY - $imgTop - $divHeight / 2; //selector顶点坐标 Y
            X = X < 0 ? 0 : X;
            Y = Y < 0 ? 0 : Y;
            X = X + $divWidth > $imgWidth ? $imgWidth - $divWidth : X;
            Y = Y + $divHeight > $imgHeight ? $imgHeight - $divHeight : Y;

            if ($viewImgWidth == null) {
                $viewImgWidth = $("#bigView img").outerWidth();
                $viewImgHeight = $("#bigView img").height();
                if ($viewImgWidth < 200 || $viewImgHeight < 200) {
                    $viewImgWidth = $viewImgHeight = 800;
                }

            }
            var scrollX = X * $viewImgWidth / $imgWidth;
            var scrollY = Y * $viewImgHeight / $imgHeight;
            $("#bigView img").css({ "left": scrollX * -1, "top": scrollY * -1 });
//    $("#bigView").css({ "top": 0, "left": $(".preview").offset().left + $(".preview").width() - 132 });

            return { left: X, top: Y };
        }

        $(".list-img").mousemove(function(){
            $(".content-video").hide();
            <?php if (!empty($output['auction_video'])) {?>
            $(".content-img").show();
            <?php }?>
        });
        $(".list-video-img").mousemove(function(){
            $(".content-video").show();
            <?php if (!empty($output['auction_video'])) {?>
            $(".content-img").hide();
            <?php }?>
        });


        $('#imageMenu li').hover(function(){
            $(this).siblings().removeClass('list-img-hover');
            $(this).addClass('list-img-hover');
        });


    });


    //收藏分享处下拉操作
    jQuery.divselect = function(divselectid,inputselectid) {
        var inputselect = $(inputselectid);
        $(divselectid).mouseover(function(){
            var ul = $(divselectid+" ul");
            ul.slideDown("fast");
            if(ul.css("display")=="none"){
                ul.slideDown("fast");
            }
        });
        $(divselectid).live('mouseleave',function(){
            $(divselectid+" ul").hide();
        });
    };
    $(function(){
        //赠品处滚条
        $('#ncsGoodsGift').perfectScrollbar({suppressScrollX:true});
        // 加入购物车
        $('a[nctype="addcart_submit"]').click(function(){
            if (typeof(allow_buy) != 'undefined' && allow_buy === false) return ;
            addcart(100009, checkQuantity(),'addcart_callback');
        });
        // 立即购买
        $('a[nctype="buynow_submit"]').click(function(){
            if (typeof(allow_buy) != 'undefined' && allow_buy === false) return ;
            buynow(100009,checkQuantity());
        });
        // 到货通知
        //浮动导航  waypoints.js
        $('#main-nav').waypoint(function(event, direction) {
            $(this).parent().parent().parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });

        // 分享收藏下拉操作
        $.divselect("#handle-l");
        $.divselect("#handle-r");

        // 规格选择
        $('dl[nctype="nc-spec"]').find('a').each(function(){
            $(this).click(function(){
                if ($(this).hasClass('hovered')) {
                    return false;
                }
                $(this).parents('ul:first').find('a').removeClass('hovered');
                $(this).addClass('hovered');
                checkSpec();
            });
        });

    });

    function checkSpec() {
        var spec_param = [{"sign":"","url":"http:\/\/localhost\/yinuovip\/www\/shop\/index.php?act=goods&op=index&goods_id=100009"}];
        var spec = new Array();
        $('ul[nctyle="ul_sign"]').find('.hovered').each(function(){
            var data_str = ''; eval('data_str =' + $(this).attr('data-param'));
            spec.push(data_str.valid);
        });
        spec1 = spec.sort(function(a,b){
            return a-b;
        });
        var spec_sign = spec1.join('|');
        $.each(spec_param, function(i, n){
            if (n.sign == spec_sign) {
                window.location.href = n.url;
            }
        });
    }

    // 验证购买数量
    function checkQuantity(){
        var quantity = parseInt($("#quantity").val());
        if (quantity < 1) {
            alert("请填写购买数量");
            $("#quantity").val('1');
            return false;
        }
        max = parseInt($('[nctype="goods_stock"]').text());
        if(quantity > max){
            alert("库存不足");
            return false;
        }
        return quantity;
    }

    // 立即购买js
    function buynow(goods_id,quantity,chain_id,area_id,area_name,area_id_2){
        login_dialog();
    }
    $(function(){
        //选择地区查看运费
        $('#transport_pannel>a').click(function(){
            var id = $(this).attr('nctype');
            if (id=='undefined') return false;
            var _self = this,tpl_id = '0';
            var url = 'index.php?act=goods&op=calc&rand='+Math.random();
            $('#transport_price').css('display','none');
            $('#loading_price').css('display','');
            $.getJSON(url, {'id':id,'tid':tpl_id}, function(data){
                if (data == null) return false;
                if(data != 'undefined') {$('#nc_kd').html('运费：<em>' + data + '</em>元');}else{'卖家承担运费';}
                $('#transport_price').css('display','');
                $('#loading_price').css('display','none');
                $('#ncrecive').html($(_self).html());
            });
        });
        $("#nc-bundling").load('index.php?act=goods&op=get_bundling&goods_id=100009', function(){
            if($(this).html() != '') {
                $(this).show();
            }
        });
        $("#consulting_demo").load('index.php?act=goods&op=consulting&goods_id=100009&store_id=1', function(){
            // Membership card
            $(this).find('[nctype="mcard"]').membershipCard({type:'shop'});
        });

        /** goods.php **/

        // 商品内容部分折叠收起侧边栏控制
        $('#fold').click(function(){
            $('.ncs-goods-layout').toggleClass('expanded');
        });
        // 商品内容介绍Tab样式切换控制
        $('#categorymenu').find("li").click(function(){
            $('#categorymenu').find("li").removeClass('current');
            $(this).addClass('current');
        });
        // 商品详情默认情况下显示全部
        $('#tabAuctionIntro').click(function(){
            $('.bd').css('display','none');
            $('#ncAuctionIntro').css('display','');
        });
        // 点击出价记录隐藏其他以及其标题栏
        $('#tabBidRecord').click(function(){
            $('.bd').css('display','none');
            $('#nc_bid_list').css('display','');
            // 异步获取出价列表
            $('#nc_bid_list').load('index.php?act=auctions&op=get_bid_list&auction_id=<?php echo $output['auction_info']['auction_id']; ?>');
        });
        // 点击服务保障隐藏其他以及其标题
        $('#tabServiceGuarantee').click(function(){
            $('.bd').css('display','none');
            $('#ncServiceGuarantee').css('display','');
            // 异步获取服务保障
            $('#ncServiceGuarantee').load('index.php?act=auctions&op=getServiceGuarantee');
        });
        // 点击保证金须知隐藏其他以及其标题
        $('#tabBondNotice').click(function(){
            $('.bd').css('display','none');
            $('#ncBondNotice').css('display','');
            // 异步获取保证金须知
            $('#ncBondNotice').load('index.php?act=auctions&op=getBondNotice');
        });
        //商品排行Tab切换
        $(".ncs-top-tab > li > a").mouseover(function(e) {
            if (e.target == this) {
                var tabs = $(this).parent().parent().children("li");
                var panels = $(this).parent().parent().parent().children(".ncs-top-panel");
                var index = $.inArray(this, $(this).parent().parent().find("a"));
                if (panels.eq(index)[0]) {
                    tabs.removeClass("current ").eq(index).addClass("current ");
                    panels.addClass("hide").eq(index).removeClass("hide");
                }
            }
        });
        //信用评价动态评分打分人次Tab切换
        $(".ncs-rate-tab > li > a").mouseover(function(e) {
            if (e.target == this) {
                var tabs = $(this).parent().parent().children("li");
                var panels = $(this).parent().parent().parent().children(".ncs-rate-panel");
                var index = $.inArray(this, $(this).parent().parent().find("a"));
                if (panels.eq(index)[0]) {
                    tabs.removeClass("current ").eq(index).addClass("current ");
                    panels.addClass("hide").eq(index).removeClass("hide");
                }
            }
        });

        //触及显示缩略图
        $('.goods-pic > .thumb').hover(
            function(){
                $(this).next().css('display','block');
            },
            function(){
                $(this).next().css('display','none');
            }
        );


    });

    var $cur_area_list,$cur_tab,next_tab_id = 0,cur_select_area = [],calc_area_id = '',calced_area = [],cur_select_area_ids = [];
    $(document).ready(function(){
        $("#ncs-freight-selector").hover(function() {
            //如果店铺没有设置默认显示区域，马上异步请求
            if (typeof nc_a === "undefined") {
                $.getJSON(SITEURL + "/index.php?act=index&op=json_area&callback=?", function(data) {
                    nc_a = data;
                    $cur_tab = $('#ncs-stock').find('li[data-index="0"]');
                    _loadArea(0);
                });
            }
            $(this).addClass("hover");
            $(this).on('mouseleave',function(){
                $(this).removeClass("hover");
            });
        });

        //验证码秒数倒计时
        function StepTimes() {
            $num = parseInt($('#show_times').html());
            $num = $num - 1;
            $('#show_times').html($num);
            if ($num <= 0) {
                ALLOW_SEND = !ALLOW_SEND;
                $('.send_success_tips').hide();
            } else {
                setTimeout(StepTimes,1000);
            }
        }
        //发送短信验证码
        var ALLOW_SEND = true;

        $('#send_auth_code').on('click',function(){
            if ($('#phone').val() == '') return false;
            if (!ALLOW_SEND) return;
            ALLOW_SEND = !ALLOW_SEND;
            $('#sending').show();
            $('#is_vcode').val(1);
            $.getJSON('index.php?act=auctions&op=send_modify_mobile',{mobile:$('#phone').val()},function(data){
                if (data.state == 'true') {
                    $('#sending').hide();
                    $('.send_success_tips').show();
                    $('#show_times').html(60);
                    setTimeout(StepTimes,1000);
                } else {
                    ALLOW_SEND = !ALLOW_SEND;
                    $('#sending').hide();
                    $('.send_success_tips').hide();
                    showDialog(data.msg,'error','','','','','','','','',2);
                }
            });
        });

        // 提交订阅提醒表单
        $('#mobile_submit').on('click', function () {
            var mobile = $('#phone').val();
            var is_vcode = $('#is_vcode').val();
            var vcode = $('#vcode').val();
            if ($('#is_remind').is(':checked')) {
                var is_remind = 1;
            } else {
                var is_remind = 0;
            }
            if ($('#is_remind_app').is(':checked')) {
                var is_remind_app = 1;
            } else {
                var is_remind_app = 0;
            }
            var auction_id = $('#auction_id').val();
            $.ajax({
                url: 'index.php?act=auctions&op=auction_remind',
                type: 'post',
                data: {mobile: mobile, vcode: vcode, is_remind: is_remind, is_remind_app: is_remind_app, auction_id: auction_id, is_vcode: is_vcode},
                dataType: 'json',
                success: function (data) {
                    if (data.code == 'ok') {
                        showDialog(data.message, 'succ', '','','','','','','',2);
                        $('.cd-popup1').removeClass('is-visible1');
                        $('#remind_msg').html('已设置提醒');
                        $('#remind_btn').removeClass('cd-popup-trigger1');

                        setTimeout("location.reload()",2000);
                    } else {
                        showDialog(data.message, 'error','','','','','','','',2);
                    }
                }
            });
        });

        // 提交支付保证金订单
        $('#margin_submit').on('click', function () {
            if ($('#is_margin_submit').is(':checked')) {
                var margin_num = "<?php echo $output['auction_info']['auction_bond'] ?>";
                var auction_id = $('#auction_id').val();
                var val=parseInt($('#buynum').val());
                window.location.href = AUCTION_SITE_URL+'/index.php?act=auction_buy&op=show_margin_order&auction_id='+auction_id+'&is_anonymous=1'+'&margin_amount='+val;
            } else {
                showDialog('请同意竞拍协议', 'error','','','','','','','',2);
            }
        });
        $('#cancel_remind_btn').on('click', function () {
            ajax_get_confirm(
                '您确定要取消订阅吗？',
                AUCTION_SITE_URL+'/index.php?act=auctions&op=cancel_auction_remind&relation_id=<?php echo isset($output['relation_info']['relation_id']) ? $output['relation_info']['relation_id'] : ''; ?>&auction_id=<?php echo $output['auction_info']['auction_id'] ?>'
            );
        })
    });


    //拍卖者实时刷新

    $(function($){
        function Request_pub(m) {
            var sValue = location.search.match(new RegExp("[\?\&]" + m + "=([^\&]*)(\&?)", "i"));
            return sValue ? sValue[1] : sValue;
        }
        function strToJson(str){
            return JSON.parse(str);
        }

        function addCookie(name,value,expireHours){
            var cookieString=name+"="+escape(value)+"; path=/";
            //判断是否设置过期时间
            if(expireHours>0){
                var date=new Date();
                date.setTime(date.getTime()+expireHours*3600*1000);
                cookieString=cookieString+";expires="+date.toGMTString();
            }
            document.cookie=cookieString;
        }

        function getCookie(name){
            var strcookie=document.cookie;
            var arrcookie=strcookie.split("; ");
            for(var i=0;i<arrcookie.length;i++){
                var arr=arrcookie[i].split("=");
                if(arr[0]==name)return unescape(arr[1]);
            }
            return null;
        }

        function delCookie(name){//删除cookie
            var exp = new Date();
            exp.setTime(exp.getTime() - 1);
            var cval=getCookie(name);
            if(cval!=null) document.cookie= name + "="+cval+"; path=/;expires="+exp.toGMTString();
        }


        if(getCookie('count_rows')){
            delCookie('count_rows');
        }
        // auto url detection
        var auctionUrl="/auction/index.php?act=auctions&op=get_bid_list&ajax_poll=1&auction_id="+Request_pub('id');
        function init_lm_page(){
            $.ajax({
                url: auctionUrl,
                cache: false,
                success: function(data){
                    var result=strToJson(data);
                    if(!result.bid_log_list.length){
                        var nullHtml="<tr>\
                         <td>无出价记录</td>\
                         </tr>";
                        $('#auction_content_lm tr').after(html);
                        return false;
                    }
                    if(getCookie('count_rows')&&getCookie('count_rows')==result.count_rows){
                    }else{
                        $('#auction_content_lm tr:gt(0)').remove();
                        var html='';
                        html+="<tr>" +
                            "<td>" +
                            "<span style='background: #800000'>领先</span>" +
                            "</td>" +
                            "<td>"+result.bid_log_list[0].member_name+"</td>" +
                            "<td>¥"+result.bid_log_list[0].offer_num+"</td>" +
                            "</tr>";

                        $('#now_price_lm').text('¥'+parseInt(result.bid_log_list[0].offer_num)+'');
                        var auction_increase_base=(parseInt($('#lm_daili_jiajia').text())*2)+(parseInt(result.bid_log_list[0].offer_num));
//                        $('#lm_daili_price').text(parseInt(result.auction_increase_base));
                        $('#lm_daili_price').text('¥'+parseInt(auction_increase_base));
                        $('#now_price_lm_num').text(result.count_rows);


                        $.each(result.bid_log_list,function(i,e){
                            if(i>0){
                                html+='<tr>\
                            <td>\
                            <span style="background: #898989">出局</span>\
                            </td>\
                            <td>'+e.member_name+'</td>\
                            <td>¥'+e.offer_num+'</td>\
                        </tr>';
                            }
                        });
                        $('#auction_content_lm tr').after(html);
                        addCookie('count_rows',result.count_rows);
                    }
                    setTimeout(init_lm_page,1000);

                }
            });
        }
        init_lm_page();


        // 保证金可加减
        var minRecord=$(".buy-num").attr('data-min');
        $(".buy-num").val(parseInt(minRecord));
        //出价，减
        $(".minus").click(function () {
            var buynum = $(".buy-num").val();
            if (buynum >= minRecord + 1) {
                $(".buy-num").val(parseInt(buynum - 1));
            }
        });
        //出价
        $(".add").click(function () {
            var buynum = parseInt($(".buy-num").val());
            var maxNum=1000000000;
            if (buynum < 999999999) {
                $(".buy-num").val(parseInt(buynum + 1));
            }else{
                $(".buy-num").val(parseInt(maxNum));
            }
        });


    });

</script>

