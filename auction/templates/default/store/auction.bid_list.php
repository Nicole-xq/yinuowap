<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/10 0010
 * Time: 16:49
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');?>
<div class="content bd" id="ncBidRecord">
    <table width="960" border="0" cellspacing="0" cellpadding="0" class="auction-record">
        <tr style="background: #eeeeee">
            <td>状态</td>
            <td>竞买号</td>
            <td>价格</td>
            <td>时间</td>
        </tr>
        <?php if ($output['bid_log_list_b']) { ?>
            <?php foreach ($output['bid_log_list_b'] as $key => $value) { ?>
                <tr>
                    <td>
                        <?php if ($key == 0 && $_GET['curpage'] <= 1) { ?>
                            <span style="background: #800000">领先</span>
                        <?php } else { ?>
                            <span style="background: #898989">出局</span>
                        <?php } ?>
                    </td>
                    <td><?php echo $value['member_name'] ?></td>
                    <td><?php echo $value['offer_num'] ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $value['created_at']) ?></td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <div class="ncs-norecord">无人出价</div>
        <?php } ?>
    </table>
    <div class="pagination">
        <?php echo $output['bid_showpage'] ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nc_bid_list').find('.demo').ajaxContent({
            event:'click', //mouseover
            loaderType:"img",
            loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#nc_bid_list'
        });
    });
</script>