<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/20 0020
 * Time: 17:14
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');?>

<?php echo $output['content'] ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#<?php echo $output['id'] ?>').find('.demo').ajaxContent({
            event:'click', //mouseover
            loaderType:"img",
            loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#<?php echo $output['id'] ?>'
        });
    });
</script>