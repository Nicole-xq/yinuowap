<?php
/**
 * 拍卖商品列表
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;
use Shopnc\Log;

defined('InShopNC') or exit('Access Invalid!');

class searchControl extends BaseAuctionControl
{


    //每页显示商品数
    const PAGESIZE = 32;

    //模型对象
    private $_model_search;

    public function indexOp()
    {
        redirect(SHOP_SITE_URL);
        Language::read('home_goods_class_index');
        $this->_model_search = Model('vendue_search');
        //默认分类，从而显示相应的属性和品牌
        $default_classid = intval($_GET['cate_id']);
        if (intval($_GET['cate_id']) > 0) {
            $goods_class_array = $this->_model_search->getLeftCategory(array($_GET['cate_id']));
        } elseif ($_GET['keyword'] != '') {
            if (cookie('auction_his_sh') == '') {
                $his_sh_list = array();
            } else {
                $his_sh_list = explode('~', cookie('auction_his_sh'));
            }
            if (strlen($_GET['keyword']) <= 30 && !in_array($_GET['keyword'],$his_sh_list)) {
                if (array_unshift($his_sh_list, $_GET['keyword']) > 8) {
                    array_pop($his_sh_list);
                }
            }
            setNcCookie('auction_his_sh', implode('~', $his_sh_list),2592000);
            //从TAG中查找分类
            $goods_class_array = $this->_model_search->getTagCategory($_GET['keyword']);
            //取出第一个分类作为默认分类，从而显示相应的属性和品牌
            $default_classid = $goods_class_array[0];
            $goods_class_array = $this->_model_search->getLeftCategory($goods_class_array, 1);
        }
        Tpl::output('goods_class_array', $goods_class_array);
        Tpl::output('default_classid', $default_classid);

        //全文搜索搜索参数
        $indexer_searcharr = $_GET;

        //搜索消费者保障服务
        $search_ci_arr = array();
        $search_ci_str = '';
        if ($_GET['ci'] && $_GET['ci'] != 0 && is_string($_GET['ci'])) {
            //处理参数
            $search_ci= $_GET['ci'];
            $search_ci_arr = explode('_',$search_ci);
            $search_ci_str = $search_ci.'_';
            $indexer_searcharr['search_ci_arr'] = $search_ci_arr;
        }

        //优先从全文索引库里查找
//        list($goods_list,$indexer_count) = $this->_model_search->indexerSearch($indexer_searcharr,self::PAGESIZE);

        $goods_list = null;
        $indexer_count = null;

        //获得经过属性过滤的商品信息
        list($goods_param, $brand_array, $initial_array, $attr_array, $checked_brand, $checked_attr) = $this->_model_search->getAttr($_GET, $default_classid);
        Tpl::output('brand_array', $brand_array);
        Tpl::output('initial_array', $initial_array);
        Tpl::output('attr_array', $attr_array);
        Tpl::output('checked_brand', $checked_brand);
        Tpl::output('checked_attr', $checked_attr);
        //查询消费者保障服务
        $contract_item = array();
        if (C('contract_allow') == 1) {
            $contract_item = Model('contract')->getContractItemByCache();
            //去掉拍品保障
            foreach ($contract_item as $key => $value) {
                if ($value['is_auction'] == 1) {
                    unset($contract_item[$key]);
                }
            }
        }
        Tpl::output('contract_item',$contract_item);

        //商品分类筛选
        $model_goods_class = Model('goods_class');

        $model_auctions = Model('auctions');
        if (!is_null($goods_list)) {
            //全文搜索
            pagecmd('setEachNum',self::PAGESIZE);
            pagecmd('setTotalNum',$indexer_count);

        } else {
        //查库搜索

        //处理排序
        $order = 'auction_id desc';
        if (in_array($_GET['key'], array('1', '2', '3'))) {
            $sequence = $_GET['order'] == '1' ? 'desc' : 'asc';
            $order = str_replace(array('1', '2', '3'), array('bid_number', 'auction_click', 'current_price'), $_GET['key']);
            $order .= ' ' . $sequence;
        }

        // 字段
        $fields = "*";

        //构造消费者保障服务字段
        if ($contract_item) {
            foreach ($contract_item as $citem_key=>$citem_val) {
                //$fields .= ",contract_{$citem_key}";
            }
        }
        $goods_class = Model('goods_class')->getGoodsClassForCacheModel();
        $condition = array();
        if (isset($goods_param['class'])) {
            $condition['gc_id_' . $goods_param['class']['depth']] = $goods_param['class']['gc_id'];
        }


        if ($_GET['keyword'] != '') {
            $condition['auction_name'] = array('like', '%' . $_GET['keyword'] . '%');
        }
        if($_GET['state'] && $_GET['state'] == 4){
            $condition['string'] =  array('exp','(auction_start_time <= '.(TIMESTAMP).') AND ((auction_end_time > '.(TIMESTAMP).' AND auction_end_true_t is Null) OR (auction_end_true_t > '.(TIMESTAMP).'))') ;
        }elseif($_GET['state'] && $_GET['state'] == 3){
            $condition['string'] =  array('exp','(auction_start_time > '.(TIMESTAMP).')') ;
        }elseif($_GET['state'] && $_GET['state'] == 5){
            $condition['string'] =  array('exp','((auction_end_time <= '.(TIMESTAMP).' and auction_end_true_t is Null) or (auction_end_true_t <= '.(TIMESTAMP).')) ') ;
        }

        //消费者保障服务
        if ($contract_item && $search_ci_arr) {
            foreach ($search_ci_arr as $ci_val) {
                $condition["contract_{$ci_val}"] = 1;
            }
        }
        if (isset($goods_param['goodsid_array'])){
            $condition['auction_id'] = array('in', $goods_param['goodsid_array']);
        }

        if (C('dbdriver') == 'oracle') {
            $oracle_fields = array();
            $fields = explode(',', $fields);
            foreach ($fields as $val) {
                $oracle_fields[] = 'min('.$val.') '.$val;
            }
            $fields = implode(',', $oracle_fields);
        }
        $count = $model_auctions->getAuctionsCount($condition);
        $goods_list = $model_auctions->getAuctionList($condition, $fields,'', $order, 0,self::PAGESIZE, $count);
    }
        Tpl::output('count', $count);
        Tpl::output('search_ci_str', $search_ci_str);
        Tpl::output('search_ci_arr', $search_ci_arr);
        Tpl::output('show_page', $model_auctions->showpage());


//        if($_GET['paint_tag_name']){
//            $paint_tag_name = $_GET['paint_tag_name'];
//        }
//        Tpl::output('paint_tag_name', $paint_tag_name);

        if (!empty($goods_list)) {
            //查库搜索
            $commonid_array = array(); // 商品公共id数组
            $storeid_array = array();       // 店铺id数组
            foreach ($goods_list as $value) {
                $commonid_array[] = $value['auction_id'];
                $storeid_array[] = $value['store_id'];
            }
            $commonid_array = array_unique($commonid_array);
            $storeid_array = array_unique($storeid_array);
            // 商品多图
            $goodsimage_more = $model_auctions->getAuctionsImageList(array('auction_id' => array('in', $commonid_array)), '*', 'is_default desc,auction_image_id asc');
            // 店铺
            $store_list = Model('store')->getStoreMemberIDList($storeid_array);

            //搜索的关键字
            $search_keyword = $_GET['keyword'];
            foreach ($goods_list as $key => $value) {
                foreach ($goodsimage_more as $v) {
                    if ($value['auction_id'] == $v['auction_id'] && $value['store_id'] == $v['store_id'] && $v['is_default'] == 1) {
                        $goods_list[$key]['image'][] = $v['auction_image'];
                    }
                }
                // 店铺的开店会员编号
                $store_id = $value['store_id'];
                $goods_list[$key]['member_id'] = $store_list[$store_id]['member_id'];
                $goods_list[$key]['store_domain'] = $store_list[$store_id]['store_domain'];

                //将关键字置红
                if ($search_keyword) {
                    $goods_list[$key]['auction_name_highlight'] = str_replace($search_keyword, '<font style="color:#f00;">' . $search_keyword . '</font>', $value['auction_name']);
                } else {
                    $goods_list[$key]['auction_name_highlight'] = $value['auction_name'];
                }

            }
        }
        Tpl::output('goods_list', $goods_list);
        if ($_GET['keyword'] != '') {
            Tpl::output('show_keyword', $_GET['keyword']);
        } else {
            Tpl::output('show_keyword', $goods_param['class']['gc_name']);
        }

        // SEO
        if ($_GET['keyword'] == '') {
            $seo_class_name = $goods_param['class']['gc_name'];
            if (is_numeric($_GET['cate_id']) && empty($_GET['keyword'])) {
                $seo_info = $model_goods_class->getKeyWords(intval($_GET['cate_id']));
                if (empty($seo_info[1])) {
                    $seo_info[1] = C('site_name') . ' - ' . $seo_class_name;
                }
                Model('seo')->type($seo_info)->param(array('name' => $seo_class_name))->show();
            }
        } elseif ($_GET['keyword'] != '') {
            Tpl::output('html_title', (empty($_GET['keyword']) ? '' : $_GET['keyword'] . ' - ') . C('site_name') . L('nc_common_search'));
        }

        // 当前位置导航
        $nav_link_list = $model_goods_class->getGoodsClassNav(intval($_GET['cate_id']), 1, 1);
        Tpl::output('nav_link_list', $nav_link_list);

        // 得到自定义导航信息
        $nav_id = intval($_GET['nav_id']) ? intval($_GET['nav_id']) : 0;
        Tpl::output('index_sign', $nav_id);

        // 地区
        $province_array = Model('area')->getTopLevelAreas();
        Tpl::output('province_array', $province_array);

        loadfunc('search');

        Tpl::showpage('search');
    }



}