<?php
/**
 * 默认展示页面
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class special_detailsControl extends BaseAuctionControl
{
	//每页显示商品数
	const PAGESIZE = 8;

	public function __construct()
	{
		parent::__construct();
	}

	//拍卖首页
	public function indexOp()
	{
		if(!$_GET['special_id']){
			showDialog('参数错误');
		}
		$special_id = $_GET['special_id'];

		$model_special = Model('auction_special');
		$model_auctions = Model('auctions');
		$model_member = Model('member');
		$special_info = $model_special->getSpecialInfo(array('special_id' => $special_id));
		if($special_info['special_state'] == 10 || $special_info['special_state'] == 30){
			showDialog('未审核通过的专场');
		}
		$data = array();
		$data['special_click'] = $special_info['special_click'] + 1;
		$model_special->editSpecial($data,array('special_id'=>$special_id));
		$special_info['adv_image_path'] = getVendueLogo($special_info['adv_image']);
		$goods_info['special_remain_date'] = ($special_info['special_end_time'] - $special_info['special_start_time']) / 86400;
		$special_info['auction_count']= $model_special->getSpecialGoodsCount(array('special_id' => $special_id , 'auction_sp_state' => 1));

		//处理排序
		$order = 'auction_id desc';
		if (in_array($_GET['key'], array('1', '2', '3'))) {
			$sequence = $_GET['order'] == '1' ? 'desc' : 'asc';
			$order = str_replace(array('1', '2', '3'), array('bid_number', 'auction_click', 'current_price'), $_GET['key']);
			$order .= ' ' . $sequence;
		}
        $model_auctions = Model('auctions');
		$special_goods_list = $model_auctions->getAuctionList(array('special_id' => $special_id),'','',$order);
//		dd();
//        print_R($special_goods_list);exit;
		$all_bid_number = 0;
		$all_price = 0.00;
        $special_click = 0;
		foreach($special_goods_list as $key => $value){
			$special_goods_list[$key]['auction_image_path'] = cthumb($value['auction_image'],360);
			$special_goods_list[$key]['auction_remain_date'] = ($value['auction_end_time'] - $value['auction_start_time']) / 86400;
			$all_bid_number += $value['bid_number'];
			if($value['is_liupai'] == 0 && $value['state'] == 5){
				$all_price += $value['current_price'];
			}
            $special_click += $value['auction_click'];;
		}
		$special_info['all_bid_number'] = $all_bid_number;
		$special_info['all_price'] = $all_price;
		$special_info['special_click'] = $special_click;
		Tpl::output('special_goods_list' , $special_goods_list);
		Tpl::output('special_info' , $special_info);
		$rec_special_list = $model_special->getSpecialList(array('is_rec'=>1,'special_end_time'=>array('gt',time()),'special_preview_start'=>array('elt',time())));
		foreach($rec_special_list as $k=>$v){
            $special_auction_list = $model_auctions->getAuctionList(['special_id'=>$v['special_id']],'auction_id,auction_click,bid_number,is_liupai,auction_end_time,auction_end_true_t');
            $all_bid_number = 0;
            $jian_num = 0;
            $special_click = 0;
            foreach($special_auction_list as $auction_info){
                if($auction_info['is_liupai'] == 0 && ($auction_info['auction_end_time'] < time() && empty($auction_info['auction_end_true_t'])) || (!empty($auction_info['auction_end_true_t']) && $auction_info['auction_end_true_t'] < time())){
                    $jian_num += 1;
                }
                $all_bid_number += $auction_info['bid_number'];
                $special_click += $auction_info['auction_click'];
            }
			$rec_special_list[$k]['special_click'] = $special_click;
			$rec_special_list[$k]['all_bid_number'] = $all_bid_number;
			$rec_special_list[$k]['jian_num'] = $jian_num;
		}

		Tpl::output('rec_special_list' , $rec_special_list);
		loadfunc('search');

		$member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
		Tpl::output('member_info' , $member_info);

		$model_special_notice = Model('special_notice');
		if($special_info['special_start_time'] <= time() && $special_info['special_end_time'] > time()){
			$special_notice_info = $model_special_notice->getSpecialNoticeInfo(array('member_id'=>$_SESSION['member_id'],'special_id'=>$special_id,'sn_type'=>2));
		}elseif($special_info['special_start_time'] > time()){
			$special_notice_info = $model_special_notice->getSpecialNoticeInfo(array('member_id'=>$_SESSION['member_id'],'special_id'=>$special_id,'sn_type'=>1));
		}
		Tpl::output('special_notice_info' , $special_notice_info);
		$model_sc_log = Model('sc_log');
		$log_array = array();
		$log_array['special_id'] = $special_id;
		$model_sc_log->addSc($log_array);
		Tpl::showpage('special_details');
	}

	/**
	 * 发送短信验证码
	 * */
	public function send_modify_mobileOp()
	{
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
				array("input"=>$_GET["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号码'),
		);
		$error = $obj_validate->validate();
		if ($error != ''){
			exit(json_encode(array('state'=>'false','msg'=>$error)));
		}

		$model_member = Model('member');

		//发送频率验证
		$member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$_SESSION['member_id']));
		if (!empty($member_common_info['send_mb_time'])) {
			if (TIMESTAMP - $member_common_info['send_mb_time'] < 58) {
				exit(json_encode(array('state'=>'false','msg'=>'请60秒以后再次发送短信')));
			}
		}

		// 生成验证码
		$verify_code = rand(1000,9999);

		//组装发送内容
		$model_tpl = Model('mail_templates');
		$tpl_info = $model_tpl->getTplInfo(array('code'=>'auction_mobile'));
		$param = array();
		$param['site_name'] = C('site_name');
		$param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
		$param['verify_code'] = $verify_code;
		$message    = ncReplaceText($tpl_info['content'],$param);
		// 发送
		$sms = new Sms();
		$result = $sms->send($_GET["mobile"],$message);

		// 发送成功回调
		if ($result) {
			$data = array();
			$data['auth_code'] = $verify_code;
			$data['send_acode_time'] = TIMESTAMP;
			$data['send_mb_time'] = TIMESTAMP;
			$update = $model_member->editMemberCommon($data,array('member_id'=>$_SESSION['member_id']));
			if (!$update) {
				exit(json_encode(array('state'=>'false','msg'=>'系统发生错误，如有疑问请与管理员联系')));
			}
			exit(json_encode(array('state'=>'true','msg'=>'发送成功')));
		} else {
			exit(json_encode(array('state'=>'false','msg'=>'发送失败')));
		}
	}

	/**
	 * 确认会员订阅
	 * */
	public function special_remindOp()
	{
		$mobile = $_POST['mobile'];
		$is_vcode = $_POST['is_vcode'];
		$vcode = $_POST['vcode'];
		$special_id = $_POST['special_id'];
		$notice_type = $_POST['notice_type'];
		$remind_time = $_POST['remind_time'];
		$model_member = Model('member');
		$model_special = Model('auction_special');
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
				array("input"=>$_POST["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号'),
		);
		$error = $obj_validate->validate();
		if ($error != ''){
			json_api('error', $error);
		}
		//如果是获取验证码用户进行手机验证
		if ($is_vcode == 1) {
			$condition = array();
			$condition['member_id'] = $_SESSION['member_id'];
			$condition['auth_code'] = intval($vcode);
			$member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
			if (!$member_common_info) {
				json_api('error', '手机验证码错误，请重新输入');
			}
			if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
				json_api('error','手机验证码已过期，请重新获取验证码');
			}
		}

		$member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);

		//建立订阅关系
		$model_special_notice = Model('special_notice');
		//查询专场信息
		$special_info  = $model_special->getSpecialInfo(array('special_id'=>$special_id));
		$insert['special_id'] = $special_id;
		$insert['special_name'] = $special_info['special_name'];
		$insert['member_id'] = $_SESSION['member_id'];
		$insert['sn_mobile'] = $mobile;
		$insert['sn_email'] = $member_info['member_email'];
		$insert['sn_type'] = $notice_type;
		$insert['sn_remind_time'] = $remind_time;
		$model_special_notice->addSpecialNotice($insert);

		json_api('ok', '订阅提醒成功');
	}

	/*
     * 取消订阅
     *
     * */
	public function cancel_special_remindOp()
	{
		$special_id = intval($_GET['special_id']);
		$sn_type = intval($_GET['sn_type']);
		if (empty($special_id)) {
			showDialog('参数错误','','error');
		}
		$model_special_notice = Model('special_notice');
		$condition = array();
		$condition['special_id'] = $special_id;
		$condition['sn_type'] = $sn_type;
		$condition['member_id'] = $_SESSION['member_id'];

		$notice_info = $model_special_notice->getSpecialNoticeInfo($condition);
		if(empty($notice_info)){
			showDialog('已经发送过消息通知，取消失败','','error');
		}
		$result = $model_special_notice->delSpecialNotice($condition);
		if ($result) {
			showDialog('专场取消订阅成功','reload','succ');
		} else {
			showDialog('专场取消订阅失败','','error');
		}
	}

}