<?php
/**
 * 默认展示页面
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class indexControl extends BaseAuctionControl{
    //每页显示商品数
    const PAGESIZE = 8;

    public function __construct() {
        parent::__construct();
    }
    //拍卖首页
    public function indexOp(){
        $model_setting = Model('setting');
        $list_setting = $model_setting->getListSetting();
        if ($list_setting['auction_advpic'] != ''){
            $list = unserialize($list_setting['auction_advpic']);
        }
        if($list_setting['auction_right_setting'] != ''){
            $right_list = unserialize($list_setting['auction_right_setting']);
        }
        Tpl::output('list', $list);
        Tpl::output('right_list', $right_list);
        Tpl::output('setting', $list_setting);
        $model_sc_log = Model('sc_log');
        $model_artist = Model('artist_vendue');
        $artist_list = $model_artist->getArtist_vendueList(array('is_rec'=>1),'','artist_sort asc,artist_vendue_id desc','',5);
        Tpl::output('artist_list', $artist_list);
        $model_au_head = Model('auction_headline');
        $au_head_list = $model_au_head->getAu_headList(array('is_show'=>1),'','au_head_sort asc');
        Tpl::output('au_head_list', $au_head_list);
        $model_auctions = Model('auctions');
        $hot_auctions = $model_auctions->getHotAuctionGoodsList(15);
        Tpl::output('hot_auctions', $hot_auctions);

        //专场时间
        $month = date('m',TIMESTAMP);
        $year = date('Y',TIMESTAMP);
        Tpl::output('month', $month);
        $time = date('m-d',TIMESTAMP);
        Tpl::output('time', $time);
        $sid = '20,11,12,13';
        $model_auction_special = Model('auction_special');
        $date1 = strtotime(date('Y-m-d',TIMESTAMP));
        $month_list = $this->getMonthDaysOp($year.'-'.$month);
        foreach($month_list as $k=>$v){
            $special_start_time = strtotime($year.'-'.$v['time']);
            $special_end_time = strtotime($year.'-'.$v['time']) + 24*3600-1;
            $condition1 =  " (special_start_time <= '".($special_end_time)."') and ((special_end_time >= '".($special_end_time)."') or ((special_end_time >= '".($special_start_time)."') and (special_end_time < '".($special_end_time)."'))) and is_open = 1 and special_state in(". $sid .") " ;
            $month_list[$k]['special_all_count'] = $model_auction_special->getSpecialCount($condition1);
            $month_list[$k]['special_all_click'] = $model_sc_log->getScCount(array('add_time'=>array('between',array($special_start_time,$special_end_time))));
            unset($special_start_time);unset($special_end_time);unset($condition1);
        }
        Tpl::output('month_list', $month_list);


        $condition = [
            'special_rate_time'=>['gt',time()],
            'special_rate'=>['gt',0],
        ];
        $order = 'special_rate_time asc';
        $special_list = $model_auction_special->getSpecialList($condition,'',$order);
        $store_list = [];
        $i = 1;
        foreach($special_list as $key => &$special_info){
            $special_info['day_num'] = Logic('auction')->getInterestDay($special_info['special_rate_time'],$special_info['special_end_time']);//计息天数
            if (!in_array($special_info['day_num'],[30,31,90,91,180,181])){
                continue;
            }
            $special_info['wap_image_path'] = getVendueLogo($special_info['wap_image']);
            $special_info['special_remain_rate_time'] = $special_info['special_rate_time']-TIMESTAMP;
            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$special_info['special_id']],'auction_id,auction_click,bid_number');
            $special_click = 0;
            foreach($special_auction_list as $k=>$v){
                $special_click += $v['auction_click'];
            }
            $special_info['special_click'] = $special_click;
            $store_list[] = $special_info;
            if($i++>=3){
                break;
            }
        }
        Tpl::output('store_list', $store_list);

        $condition2 =  "  (special_start_time <= '".($date1 + 86400 -1)."') and ((special_end_time >= '".($date1 + 86400 -1)."') or ((special_end_time > '".(TIMESTAMP)."') and (special_end_time < '".($date1 + 86400 -1)."'))) and is_open = 1 and special_state in(". $sid .")" ;
        $count = $model_auction_special->getSpecialCount($condition2);
        $special_list = $model_auction_special->getSpecialList($condition2,'','special_end_time asc','');
        Tpl::output('test',count($special_list));
        foreach($special_list as $k=>$v){
            $remain_time = $v['special_end_time'] - $v['special_start_time'];
            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$v['special_id']],'auction_id,auction_click,bid_number,is_liupai,auction_end_time,auction_end_true_t');
            $all_bid_number = 0;
            $jian_num = 0;
            $special_click = 0;
            foreach($special_auction_list as $auction_info){
                if($auction_info['is_liupai'] == 0 && ($auction_info['auction_end_time'] < time() && empty($auction_info['auction_end_true_t'])) || (!empty($auction_info['auction_end_true_t']) && $auction_info['auction_end_true_t'] < time())){
                    $jian_num += 1;
                }
                $all_bid_number += $auction_info['bid_number'];
                $special_click += $auction_info['auction_click'];
            }
            if($v['special_start_time'] >time()){
                $special_list1[$v['special_start_time']]['special_id'] = $v['special_id'];
                $special_list1[$v['special_start_time']]['special_name'] = $v['special_name'];
                $special_list1[$v['special_start_time']]['special_state'] = $v['special_state'];
                $special_list1[$v['special_start_time']]['special_click'] = $special_click;
                $special_list1[$v['special_start_time']]['special_image'] = $v['special_image'];
                $special_list1[$v['special_start_time']]['special_end_time'] = $v['special_end_time'];
                $special_list1[$v['special_start_time']]['special_start_time'] = $v['special_start_time'];
                $special_list1[$v['special_start_time']]['delivery_mechanism'] = $v['delivery_mechanism'];
                $special_list1[$v['special_start_time']]['all_bid_number'] = $all_bid_number;
                $special_list1[$v['special_start_time']]['jian_num'] = $jian_num;
                $special_list1[$v['special_start_time']]['special_rate'] = $v['special_rate'];
            }else{
//                $special_list1[$remain_time]['special_id'] = $v['special_id'];
//                $special_list1[$remain_time]['special_name'] = $v['special_name'];
//                $special_list1[$remain_time]['special_state'] = $v['special_state'];
//                $special_list1[$remain_time]['special_click'] = $special_click;
//                $special_list1[$remain_time]['special_image'] = $v['special_image'];
//                $special_list1[$remain_time]['special_end_time'] = $v['special_end_time'];
//                $special_list1[$remain_time]['special_start_time'] = $v['special_start_time'];
//                $special_list1[$remain_time]['delivery_mechanism'] = $v['delivery_mechanism'];
//                $special_list1[$remain_time]['all_bid_number'] = $all_bid_number;
//                $special_list1[$remain_time]['jian_num'] = $jian_num;
//                $special_list1[$remain_time]['special_rate'] = $v['special_rate'];
                $tmp = array();
                $tmp['special_id'] = $v['special_id'];
                $tmp['special_name'] = $v['special_name'];
                $tmp['special_state'] = $v['special_state'];
                $tmp['special_click'] = $special_click;
                $tmp['special_image'] = $v['special_image'];
                $tmp['special_end_time'] = $v['special_end_time'];
                $tmp['special_start_time'] = $v['special_start_time'];
                $tmp['delivery_mechanism'] = $v['delivery_mechanism'];
                $tmp['all_bid_number'] = $all_bid_number;
                $tmp['jian_num'] = $jian_num;
                $tmp['special_rate'] = $v['special_rate'];
                $special_list1[] = $tmp;
            }

        }
        if(!empty($special_list1)){
            ksort($special_list1);
        }
        Tpl::output('special_list', $special_list1);
        Tpl::output('special_num', $count);

        $start_time1 = strtotime(date('Y-m-d',time()))+24*3600;
        $end_time1 = $start_time1+7*24*3600-1;
        $data['is_open'] = 1;
        $data['special_state'] = array('in',array(20,11,12,13));
        $data['special_start_time'] = array('between',array($start_time1,$end_time1));
        $special_count = $model_auction_special->getSpecialCount($data);
        Tpl::output('special_count', $special_count);

        Tpl::showpage('index');
    }
    //艺术家列表页
    public function artist_listOp(){
        Tpl::output('index_sign','artist');
        $model_artist = Model('artist_vendue');
        $model_classify = Model('artist_classify');
        //$model_grade = Model('artist_grade');//根本没用到,不知道要干什么,先干掉
        $list = $model_classify->getArtist_classifyList(array(),'','artist_classify_id asc');
//        $grade_list = $model_grade->getArtist_gradeList(array(),'','artist_grade_id asc');//根本没用到,不知道要干什么,先干掉
        foreach($list as $k=>$v){
//                $list[$k][$v['artist_classify_id']][$key] = $model_grade->getArtist_gradeInfo(array('artist_grade_id'=>$val['artist_grade_id']));
                $list[$k]['artist_list'] =  $model_artist->getArtist_StoreWebList(array('artist_classify'=>$v['artist_classify_id'],'store_apply_state'=>40,'store_state'=>1   ),'','artist_sort asc,artist_vendue_id desc');
        }

        Tpl::output('artist_list', $list);
        Tpl::showpage('artist_list');
    }

    //艺术家详情页
    public function artist_detailOp(){
        Tpl::setLayout('auction_artist_layout');
        Tpl::output('index_sign','artist');
        $artist_id =$_GET['artist_id'];
        $model_artist = Model('artist_vendue');
        $artist_info = $model_artist->getArtist_StoreInfo(array('artist_vendue_id'=>$artist_id));
        if(empty($artist_info)){
            showMessage(Language::get('wrong_argument'),'','html','error');
        }
        if(!empty($artist_info)){
            $artist_info['artist_represent'] = unserialize($artist_info['artist_represent']);
            $artist_info['artist_awards'] = unserialize($artist_info['artist_awards']);
            $artist_info['artist_works'] = unserialize($artist_info['artist_works']);
        }
        $data = array();
        $data['artist_click'] = $artist_info['artist_click'] +1;
        $model_artist->editArtist_vendue($data,array('artist_vendue_id'=>$artist_id));

        $model_area = Model('area');

        $city_info = $model_area->getAreaInfo(array('area_id'=>$artist_info['city_id']));
        $area_info = $model_area->getAreaInfo(array('area_id'=>$artist_info['area_id']));
        $artist_info['area_name'] = $city_info['area_name'];
        $artist_info['city_name'] = $area_info['area_name'];
        $artist_info['artist_works'] = unserialize($artist_info['artist_works']);
        Tpl::output('artist_info', $artist_info);

        /** @var storeModel  $model_store */
        $model_store = Model('store');
        $where = array(
            'store_state' => 1,
            'member_id' => $artist_info['member_id']
        );
        $store_data = $model_store->getStoreInfo($where);
        Tpl::output('store_info', $store_data);

        Tpl::showpage('artist_detail_new');
    }


    //艺术家详情页
    public function artist_detail_storeOp(){

        $store_id = $_REQUEST['store_id'];
        if(empty($store_id)){
            goto END;
        }

        /** @var storeModel  $model_store */
        $model_store = Model('store');
        $where = array(
            'store_state' => 1,
            'store_id' => $store_id
        );
        $store_data = $model_store->getStoreInfo($where);

        if($store_data){
            $model_artist = Model('artist_vendue');
            $artist_info = $model_artist->getArtist_StoreInfo(array('member_id'=>$store_data['member_id']));

            if($artist_info){
                $_GET['artist_id'] = $artist_info['artist_vendue_id'];
                $this->artist_detailOp();
                exit();
            }
        }

        END:
        redirect(urlShop('show_store','index',array('store_id'=>$store_data['store_id']), $store_data['store_domain']));
    }

    /**
     * 老版本艺术家页面
     */
    public function artist_detail1Op(){
        Tpl::output('index_sign','artist');
        $artist_id =$_GET['artist_id'];
        $model_artist = Model('artist_vendue');
        $artist_info = $model_artist->getArtist_StoreInfo(array('artist_vendue_id'=>$artist_id));
        if(empty($artist_info)){
            showMessage(Language::get('wrong_argument'),'','html','error');
        }
        if(!empty($artist_info)){
            $artist_info['artist_represent'] = unserialize($artist_info['artist_represent']);
            $artist_info['artist_awards'] = unserialize($artist_info['artist_awards']);
            $artist_info['artist_works'] = unserialize($artist_info['artist_works']);
        }
        $data = array();
        $data['artist_click'] = $artist_info['artist_click'] +1;
        $model_artist->editArtist_vendue($data,array('artist_vendue_id'=>$artist_id));
        Tpl::output('artist_info', $artist_info);

        Tpl::showpage('artist_detail');
    }

    public function store_top_detailOp($store_vendue_id){
        $model_store_vendue = Model('store_vendue');
        $model_store_trans = Model('store_trans');
        $model_area = Model('area');
        $store_au_info = $model_store_vendue->getStore_vendueSInfo(array('store_vendue_id'=>$store_vendue_id));
        if(empty($store_au_info)){
            showMessage(Language::get('wrong_argument'),'','html','error');
        }
        $area_info = $model_area->getAreaInfo(array('area_id'=>$store_au_info['city_id']));
        $store_au_info['area_name'] = $area_info['area_name'];
        if($store_au_info['store_type'] == 1){
            $model_artist_vendue = Model('artist_vendue');
            $artist_vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('store_vendue_id'=>$store_vendue_id));
            $store_au_info['artist_intro'] = $artist_vendue_info['artist_resume'];
        }
        Tpl::output('store_au_info', $store_au_info);
        $store_trans_info = $model_store_trans->getStore_transInfo(array('store_vendue_id'=>$store_vendue_id));
        Tpl::output('store_trans_info', $store_trans_info);
        $model_favorites = Model('favorites');
        $attention_count = $model_favorites->getStoreFavoritesCountByStoreId($store_au_info['store_id']);
//        $model_attention = Model('store_attention');
//        $attention_count = $model_attention->getAttentionCount(array('store_vendue_id'=>$store_vendue_id));
        Tpl::output('attention_count', $attention_count);
        $fav_info = $model_favorites->getOneFavorites(array('store_id'=>$store_au_info['store_id'],'member_id'=>$_SESSION['member_id']));
        Tpl::output('fav_info', $fav_info);
//        $store_attention_info = $model_attention->getAttentionInfo(array('store_vendue_id'=>$store_vendue_id,'member_id'=>$_SESSION['member_id']));
//        Tpl::output('store_attention_info', $store_attention_info);

    }


    //商家详情页
    public function store_au_detailOp(){
        redirect(SHOP_SITE_URL);
        exit();
        $model_store_vendue = Model('store_vendue');
        $this->store_top_detailOp($_GET['store_vendue_id']);
        $store_vendue_id = $_GET['store_vendue_id'];
        $store_au_info = $model_store_vendue->getStore_vendueSInfo(array('store_vendue_id'=>$store_vendue_id));
        $model_auctions = Model('auctions');
        //处理排序
        $order = 'auction_id desc';
        if (in_array($_GET['key'], array('3'))) {
            $sequence = $_GET['order'] == '1' ? 'desc' : 'asc';
            $order = str_replace(array('3'), array('current_price'), $_GET['key']);
            $order .= ' ' . $sequence;
        }

        if($_GET['state'] && $_GET['state'] == 4){
            $condition =  "(auction_start_time <= '".(TIMESTAMP)."') and ((auction_end_time > '".(TIMESTAMP)."' and auction_end_true_t is Null) or (auction_end_true_t > '".(TIMESTAMP)."'))  and store_id = (". $store_au_info['store_id'] .")" ;
        }elseif($_GET['state'] && $_GET['state'] == 3){
            $condition =  "(auction_start_time > '".(TIMESTAMP)."') and store_id = (". $store_au_info['store_id'] .")" ;
        }elseif($_GET['state'] && $_GET['state'] == 5){
            $condition =  "((auction_end_time <= '".(TIMESTAMP)."' and auction_end_true_t is Null) or (auction_end_true_t <= '".(TIMESTAMP)."'))  and store_id = (". $store_au_info['store_id'] .")" ;
        }else{
            $condition =  "store_id = (". $store_au_info['store_id'] .")" ;
        }
//        if($_GET['state'] && $_GET['state'] != 0){
//            $condition['state'] = $_GET['state'];
//        }else{
//            $condition['state']   = array('in',array(1,3,4,5));
//        }
//        $condition['store_id'] = $store_au_info['store_id'];
        $count = $model_auctions->getAuctionsCount($condition);
        $auction_list= $model_auctions->getAuctionList($condition, '','', $order, 0,self::PAGESIZE, $count);
        foreach($auction_list as $k=>$v){
            if($v['auction_end_true_t'] != ''){
                $auction_list[$k]['auction_end_time'] = $v['auction_end_true_t'];
            }
        }
        Tpl::output('auction_list', $auction_list);
        Tpl::output('show_page', $model_auctions->showpage());
        loadfunc('search');
        Tpl::showpage('store_auction.detail');

    }

    //店铺拍卖之专场
    public function store_specialOp(){
        redirect(SHOP_SITE_URL);
        exit();
        $store_vendue_id = $_GET['store_vendue_id'];
        $model_special = Model('auction_special');
        $count = $model_special->getSpecialCount(array('store_vendue_id'=>$store_vendue_id,'special_state'=>array('in',array(11,12,13,20))));
        $special_list = $model_special->getSpecialList(array('store_vendue_id'=>$store_vendue_id,'special_state'=>array('in',array(11,12,13,20))),'','',self::PAGESIZE,0,$count);
        $model_auctions = Model('auctions');
        foreach($special_list as $k=>$v){
            $special_auction_list = $model_special->getSpecialGoodsList(array('special_id'=>$v['special_id'],'auction_sp_state'=>1));
            $special_list[$k]['special_count'] = $model_special->getSpecialGoodsCount(array('auction_sp_state'=>1));
            if($v['special_end_time'] <= time()){
                $all_amount = 0;
                $jian_num = 0;
                foreach($special_auction_list as $key=>$val){
                    $auction_info = $model_auctions->getAuctionsInfo(array('auction_id'=>$val['auction_id'],'state'=>1,'is_liupai'=>0));
                    $all_amount += $auction_info['current_price'];
                    if(!empty($auction_info)){
                        $jian_num +=1;
                    }

                }
                $special_list[$k]['all_amount'] = $all_amount;
                $special_list[$k]['jian_num'] = $jian_num;
            }elseif($v['special_start_time'] <= time() && $v['special_end_time'] > time()){
                $all_bid_number = 0;
                foreach($special_auction_list as $key=>$val){
                    $auction_info = $model_auctions->getAuctionsInfo(array('auction_id'=>$val['auction_id']));
                    $all_bid_number += $auction_info['bid_number'];
                }
                $special_list[$k]['all_bid_number'] = $all_bid_number;
            }


        }
        $this->store_top_detailOp($store_vendue_id);
        Tpl::output('special_list', $special_list);
        Tpl::output('show_page', $model_auctions->showpage());
        Tpl::showpage('store_special.detail');
    }

    //资讯
    public function store_inform_detailOp(){
        $store_vendue_id = $_GET['store_vendue_id'];
        $model_store_inform = Model('store_inform');
        $store_inform_info = $model_store_inform->getStore_informInfo(array('store_vendue_id'=>$store_vendue_id));
        $store_inform_info['inform_list'] = unserialize($store_inform_info['inform_url']);
        $this->store_top_detailOp($store_vendue_id);
        Tpl::output('store_inform_info', $store_inform_info);
        $model_auctions = Model('auctions');
        $hot_auctions = $model_auctions->getAuctionList(array('recommended'=>1));
        Tpl::output('hot_auctions', $hot_auctions);
        Tpl::showpage('store_inform.detail');

    }

    //关注
    public function guanzhuOp(){
        $member_id = $_GET['member_id'];
        $store_id = $_GET['store_id'];
        $store_vendue_id = $_GET['store_vendue_id'];
        $model_attention = Model('store_attention');
        $insert_array = array();
        $insert_array['member_id'] = $member_id;
        $insert_array['store_id'] = $store_id;
        $insert_array['store_vendue_id'] = $store_vendue_id;

        $result = $model_attention->addAttention($insert_array);
        if(!$result){
            showMessage('关注失败', urlVendue('index','store_au_detail',array('store_vendue_id'=>$store_vendue_id)), '', 'error');
        }else{
            showMessage('关注成功', urlVendue('index','store_au_detail',array('store_vendue_id'=>$store_vendue_id)), '', 'succ');
        }

    }

    //取消关注
    public function quguanzhuOp(){
        $member_id = $_GET['member_id'];
        $store_id = $_GET['store_id'];
        $store_vendue_id = $_GET['store_vendue_id'];
        $model_attention = Model('store_attention');
        $condition = array();
        $condition['member_id'] = $member_id;
        $condition['store_id'] = $store_id;
        $condition['store_vendue_id'] = $store_vendue_id;

        $result = $model_attention->delAttention($condition);
        if(!$result){
            showMessage('取消关注失败', urlVendue('index','store_au_detail',array('store_vendue_id'=>$store_vendue_id)), '', 'error');
        }else{
            showMessage('取消关注成功', urlVendue('index','store_au_detail',array('store_vendue_id'=>$store_vendue_id)), '', 'succ');
        }

    }

    /**
     * 增加店铺关注
     */
    public function favoritestoreOp(){
        $fav_id = intval($_GET['fid']);
        if ($fav_id <= 0){
            echo json_encode(array('done'=>false,'msg'=>'关注失败'));
            die;
        }
        $favorites_model = Model('favorites');
        //判断是否已经收藏
        $favorites_info = $favorites_model->getOneFavorites(array('fav_id'=>"$fav_id",'fav_type'=>'store','member_id'=>"{$_SESSION['member_id']}"));
        if(!empty($favorites_info)){
            echo json_encode(array('done'=>false,'msg'=>'您已关注过该店铺'));
            die;
        }
        //判断店铺是否为当前会员所有
        if ($fav_id == $_SESSION['store_id']){
            echo json_encode(array('done'=>false,'msg'=>'不能关注自己的店铺'));
            die;
        }
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$fav_id));
        //添加收藏
        $insert_arr = array();
        $insert_arr['member_id'] = $_SESSION['member_id'];
        $insert_arr['member_name'] = $_SESSION['member_name'];
        $insert_arr['fav_id'] = $fav_id;
        $insert_arr['fav_type'] = 'store';
        $insert_arr['fav_time'] = time();
        $result = $favorites_model->addFavorites($insert_arr);
        if ($result){
            //增加收藏数量
            $store_model = Model('store');
            $store_model->editStore(array('store_collect'=>array('exp', 'store_collect+1')), array('store_id' => $fav_id));
            echo json_encode(array('done'=>true,'msg'=>'关注成功','url'=>'index.php?act=index&op=store_au_detail&store_vendue_id='.$store_vendue_info['store_vendue_id']));
            die;
        }else{
            echo json_encode(array('done'=>false,'msg'=>'关注失败'));
            die;
        }
    }

    /**
     * 取消关注
     *
     * @param
     * @return
     */
    public function delfavoritesOp(){
        if (!$_GET['fav_id'] || !$_GET['type']){
            showDialog('取消关注失败','','error');
        }
        if (!preg_match_all('/^[0-9,]+$/',$_GET['fav_id'], $matches)) {
            showDialog('参数错误','','error');
        }
        $fav_id = trim($_GET['fav_id'],',');
        if (!in_array($_GET['type'], array('goods', 'store'))) {
            showDialog('参数错误','','error');
        }
        $type = $_GET['type'];
        $favorites_model = Model('favorites');
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$_GET['fav_id']));
        $fav_arr = explode(',',$fav_id);
        if (!empty($fav_arr) && is_array($fav_arr)){
            $favorites_list = $favorites_model->getFavoritesList(array('fav_id'=>array('in', $fav_arr),'fav_type'=>"$type",'member_id'=>$_SESSION['member_id']));
            if (!empty($favorites_list) && is_array($favorites_list)){
                $fav_arr = array();
                foreach ($favorites_list as $k=>$v){
                    $fav_arr[] = $v['fav_id'];
                }
                $result = $favorites_model->delFavorites(array('fav_id'=>array('in', $fav_arr),'fav_type'=>"$type",'member_id'=>"{$_SESSION['member_id']}"));
                if (!empty($fav_arr) && $result){
                    //更新收藏数量
                    $store_model = Model('store');
                    $store_model->editStore(array('store_collect'=>array('exp', 'store_collect - 1')),array('store_id'=>array('in', $fav_arr)));
                    showDialog('取消关注成功','index.php?act=index&op=store_au_detail&store_vendue_id='.$store_vendue_info['store_vendue_id'],'succ');
                }
            }else {
                showDialog('取消关注失败','','error');
            }
        }else {
            showDialog('取消关注失败','','error');
        }
    }

    //获取日期
    function getMonthDaysOp($month= "this month",$format = "m-d") {
        if($_GET['type'] && $_GET['type'] == 'html'){
            $month = $_POST['data_param'];
            $start = strtotime("first day of $month");
            $end = strtotime("last day of $month");
            $days = array();
            for($i=$start;$i<=$end;$i+=24*3600) {
                $days[]['time'] = date($format, $i);
            }
            $year = date('Y',TIMESTAMP);
            $model_auction_special = Model('auction_special');
            $model_sc_log = Model('sc_log');
            $sid = '20,11,12,13';
            foreach($days as $k=>$v){
                $special_start_time = strtotime($year.'-'.$v['time']);
                $special_end_time = strtotime($year.'-'.$v['time']) + 24*3600-1;
                $condition1 =  " (special_start_time <= '".($special_end_time)."') and ((special_end_time >= '".($special_end_time)."') or ((special_end_time >= '".($special_start_time)."') and (special_end_time < '".($special_end_time)."'))) and is_open = 1 and special_state in(". $sid .") " ;
                $days[$k]['special_all_count'] = $model_auction_special->getSpecialCount($condition1);
                $days[$k]['special_all_click'] = $model_sc_log->getScCount(array('add_time'=>array('between',array($special_start_time,$special_end_time))));
                unset($special_start_time);unset($special_end_time);unset($condition1);
            }
            echo json_encode($days);
        }else{
            $start = strtotime("first day of $month");
            $end = strtotime("last day of $month");
            $days = array();
            for($i=$start;$i<=$end;$i+=24*3600) {
                $days[]['time'] = date($format, $i);
            }
            return $days;
        }
    }


    public function special_ajaxOp(){
        $count = 10;
        if(!empty($_GET['start'])  && $_GET['start'] != 0){
            $start = max(0,intval($_GET['start']));
        }else{
            $start = 0;
        }
        $model_auction_special = Model('auction_special');
        $start_time = strtotime(date('Y-m-d',time()))+24*3600;
        $end_time = $start_time+7*24*3600-1;

        $condition['is_open'] = 1;
        $condition['special_state'] = array('in',array(20,11,12,13));
        $condition['special_start_time'] = array('between',array($start_time,$end_time));
        $special_list = $model_auction_special->getSpecialList($condition,'','special_start_time asc','',"$start,$count");
        foreach($special_list as $k=>$v){
            $special_list[$k]['special_start_date'] = date('m月d日 H:i',$v['special_start_time']);
        }
        $scount = (int)$model_auction_special->getSpecialCount($condition);
        if($scount >= $count){
            $special_count = $scount - $count;
            $num = $count + $start;
        }else{
            $special_count = 0;
            $num = $scount;
        }

        echo json_encode(array('special_list'=>$special_list,'special_num'=>$special_count,'num'=>$num));
    }





}