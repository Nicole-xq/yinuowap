<?php
/**
 * 拍卖支付入口
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/25 0025
 * Time: 11:55
 * @author ADKi
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_paymentControl extends BaseAuctionControl
{
    public function __construct() {
        Language::read('common,home_layout');
    }

    /**
     * 支付保证金
     * */
    public function margin_orderOp()
    {
        // 诺币
        $nuobi_money = 0;
        $order_sn = $_POST['order_sn'];
        $payment_code = $_POST['payment_code'];
        $url = C('shop_site_url').'/index.php?act=member_auction';
        // 需要支付保证金为0时直接跳转的参数
        if (is_null($order_sn)) {
            $order_sn = $_GET['order_sn'];
        }

        if(!preg_match('/^\d{16}$/',$order_sn)){
            showMessage('参数错误','','html','error');
        }

        //获取订单信息
        $logic_auction_order = Logic('auction_order');
        //验证订单是否有效
        $result = $logic_auction_order->VerifyMarginOrderPay($order_sn);

        if(!$result['state']) {
            showMessage($result['msg'], $url, 'html', 'error');
        }

        $order_info = $result['data'];
        //如果保证金金额为0，或者已经被保证账户支付完成的,转到支付成功页
        $state = $order_info['margin_amount'] - $order_info['account_margin_amount'] - $order_info['points_amount'] - $order_info['api_pay_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'];
        if ($state == 0) {
            // 更新订单状态
            $model_margin_orders = Model('margin_orders');
            $data_order['order_state'] = 1;
            $data_order['payment_time'] = TIMESTAMP;
            $result_1 = $model_margin_orders->editOrder($data_order,array('margin_id'=>$order_info['margin_id']));

            // 更新会员和拍品关系，增加除了保证金账户支付金额部分以外的保证金冻结金额
            /** @var auction_orderLogic $logic_auction_order */
            $logic_auction_order = Logic('auction_order');
            $result_2 = $logic_auction_order->auction_member_bond($order_info);

            if ($result_1 && $result_2) {
                redirect('index.php?act=auction_buy&op=pay_ok&order_sn='.$order_info['order_sn'].'&margin_id='.$order_info['margin_id'].'&order_amount='.ncPriceFormat($order_info['margin_amount']));
            } else {
                showMessage('支付失败请重试','index.php?act=member_order','html','error');
            }
        }
        //消耗诺币（积分） 记录日志
       if($nuobi_money > 0){
           $logic_auction_pay = Logic('auction_pay');
           // 诺币支付，如果完全支付更细会员和拍品关系表
            $result = $logic_auction_pay->NuobiPay($order_sn, $_SESSION['member_id'], $_SESSION['member_name'], 0);
            $order_info = $result['data'];
        }
        //站内余额支付
        $order_info = $this->_pd_pay($order_info,$_POST);
        //计算本次需要在线支付金额
        $api_pay_amount = 0;
        if ($order_info['order_type'] == 0) {
            $api_pay_amount = floatval(ncPriceFormat($order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount']));
        } else {
            $api_pay_amount = ncPriceFormat($order_info['margin_amount']);
        }

        //如果所需支付金额为0，转到支付成功页
        if (empty($api_pay_amount)) {
            redirect('index.php?act=auction_buy&op=pay_ok&order_sn='.$order_info['order_sn'].'&margin_id='.$order_info['margin_id'].'&order_amount='.ncPriceFormat($order_info['margin_amount']));
        }

        // 线上支付更新API time
        if ($order_info['order_type'] == 0) {
            $result = Model('margin_orders')->editOrder(array('api_pay_time'=>TIMESTAMP),array('margin_id'=>$order_info['margin_id']));
        } else {
            $result = true;
        }


        if(!$result) {
            showMessage('更新订单信息发生错误，请重新支付', $url, 'html', 'error');
        }
        // 获取网站支付方式是否开启
        $result = Logic('payment')->getPaymentInfo($payment_code);

        if(!$result['state']) {
            showMessage($result['msg'], $url, 'html', 'error');
        }
        $payment_info = $result['data'];

        $order_info['api_pay_amount'] = ncPriceFormat($api_pay_amount);

        //转到第三方API支付，和线下支付
        $this->_api_pay($order_info, $payment_info);

    }

    /**
     * 尾款支付
     * */
    public function auction_orderOp()
    {
        // 诺币
        $nuobi_money = floatval($_POST['nuobi_money']);
        $order_sn = $_POST['order_sn'];
        $payment_code = $_POST['payment_code'];
        $url = C('shop_site_url').'/index.php?act=member_auction&op=have_auction';
        // 需要支付保证金为0时直接跳转的参数
        if (is_null($order_sn)) {
            $order_sn = $_GET['order_sn'];
        }

        if(!preg_match('/^\d{16}$/',$order_sn)){
            showMessage('参数错误','','html','error');
        }

        //获取订单信息
        $logic_auction_order = Logic('auction_order');
        $logic_auction_pay = Logic('auction_pay');
        //验证订单是否有效
        $result = $logic_auction_order->VerifyAuctionOrderPay($order_sn);

        if(!$result['state']) {
            showMessage($result['msg'], $url, 'html', 'error');
        }
        $order_info = $result['data'];
        $margin_info = Model('margin_orders')->getOrderInfo(array('auction_order_id'=>$order_info['order_id']));
        $order_info['margin_amount'] = $margin_info['margin_amount'];
        //如果金额为0，或者已经被保证账户支付完成的,转到支付成功页
        $pay_money = $order_info['order_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $order_info['points_amount'] - $order_info['api_pay_amount'];

        if($pay_money < $order_info['margin_amount']){//如果保证金金额大于订单金额
            /** @var auction_payLogic $logic_auction_order */
            $logic_auction_pay = Logic('auction_pay');

            if($logic_auction_pay->marginPayOrder($order_info, $pay_money)){
                redirect('index.php?act=auction_buy&op=pay_auction_ok&order_sn='.$order_info['order_sn'].'&auction_order_id='.$order_info['order_id'].'&order_amount='.ncPriceFormat($order_info['order_amount']));
            }else{
                return array('error' => '更新订单信息发生错误，请重新支付');
            }

        }

        //如果金额为0，或者已经被保证账户支付完成的,转到支付成功页
        $state = $order_info['order_amount'] - $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $order_info['points_amount'] - $order_info['api_pay_amount'];

        if ($state == 0) {
            redirect('index.php?act=auction_buy&op=pay_auction_ok&order_sn='.$order_info['order_sn'].'&auction_order_id='.$order_info['order_id'].'&order_amount='.ncPriceFormat($order_info['order_amount']));
        }

        //消耗诺币（积分） 记录日志
        if($nuobi_money > 0){
            // 诺币支付，如果完全支付订单表 20 支付完成状态
            $result = $logic_auction_pay->NuobiPay($order_sn, $_SESSION['member_id'], $_SESSION['member_name'], 1);
            $order_info = $result['data'];
        }
        //站内余额支付
        $order_info = $this->_pd_pay($order_info,$_POST, 1);
        //计算本次需要在线支付金额
        $api_pay_amount = 0;
        if ($order_info['payment_code'] != 'underline') {
            $api_pay_amount = floatval(ncPriceFormat($order_info['order_amount'] - $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $order_info['points_amount']));
        } else {
            $api_pay_amount = ncPriceFormat($order_info['order_amount'] - $order_info['margin_amount']);
        }

        //如果所需支付金额为0，转到支付成功页
        if (empty($api_pay_amount)) {
            redirect('index.php?act=auction_buy&op=pay_auction_ok&order_sn='.$order_info['order_sn'].'&auction_order_id='.$order_info['order_id'].'&order_amount='.ncPriceFormat($order_info['order_amount']));
        }

        // 线上支付更新API time
        if ($order_info['payment_code'] != 'underline') {
            $result = Model('auction_order')->setOrder(array('auction_order_id'=>$order_info['auction_order_id']), array('api_pay_time'=>TIMESTAMP));
        } else {
            $result = true;
        }


        if(!$result) {
            showMessage('更新订单信息发生错误，请重新支付', $url, 'html', 'error');
        }
        // 获取网站支付方式是否开启
        $result = Logic('payment')->getPaymentInfo($payment_code);

        if(!$result['state']) {
            showMessage($result['msg'], $url, 'html', 'error');
        }
        $payment_info = $result['data'];

        $order_info['api_pay_amount'] = ncPriceFormat($api_pay_amount);

        //转到第三方API支付，和线下支付
        $this->_api_pay($order_info, $payment_info, 1);
    }

    /**
     * 站内余额支付(充值卡、预存款支付)
     *
     * @param $order_info array 订单详情
     * @param $post array Post数据
     * @param $type int 0 保证金 1 尾款
     * @return array
     */
    private function _pd_pay($order_info, $post, $type = 0) {
        if (empty($post['password'])) {
            return $order_info;
        }
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
        if ($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($post['password'])) {
            return $order_info;
        }

        if ($buyer_info['available_rc_balance'] == 0) {
            $post['rcb_pay'] = null;
        }

        if ($buyer_info['available_predeposit'] == 0) {
            $post['pd_pay'] = null;
        }
        if (floatval($order_info['rcb_amount']) > 0 || floatval($order_info['pd_amount']) > 0) {
            return $order_info;
        }
        try {
            $model_member->beginTransaction();
            $logic_auction_pay = Logic('auction_pay');
            //使用充值卡支付
            if (!empty($post['rcb_pay'])) {
                $order_info = $logic_auction_pay->rcbPay($order_info, $post, $buyer_info, $type);
            }

            //使用预存款支付
            if (!empty($post['pd_pay'])) {
                $order_info = $logic_auction_pay->pdPay($order_info, $post, $buyer_info, $type);
            }
            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            showMessage($e->getMessage(), '', 'html', 'error');
        }

        return $order_info;
    }

    /**
     * 第三方在线支付接口
     * 线下支付
     * @param $type int 类型 0 保证金 1
     */
    private function _api_pay($order_info, $payment_info, $type = 0) {
        //兼容之前的支付接口 pay_sn 和 order_type ,订单名称：subject
        if ($type == 0) {
            $order_sn = $order_info['order_sn'];
            $order_info['pay_sn'] = $order_sn;
            // 支付接口判断订单类型
            $order_info['is_margin'] = true;
            $order_info['subject'] = '保证金订单：'.$order_sn;
            $order_id = $order_info['margin_id'];
        } elseif ($type == 1) {
            $order_sn = $order_info['order_sn'];
            $order_info['pay_sn'] = $order_sn;
            // 支付接口判断订单类型
            $order_info['is_margin'] = false;
            $order_info['subject'] = '拍品尾款订单：'.$order_sn;
            $order_id = $order_info['auction_order_id'];
        }

        if ($payment_info['payment_code'] != 'underline') {
            // 实例化支付类
            $payment_api = new $payment_info['payment_code']($payment_info,$order_info);
        }
        if($payment_info['payment_code'] == 'chinabank') {
            $payment_api->submit();
        } elseif ($payment_info['payment_code'] == 'wxpay') {
            if (!extension_loaded('curl')) {
                showMessage('系统curl扩展未加载，请检查系统配置', '', 'html', 'error');
            }
            Tpl::setDir('buy');
            Tpl::setLayout('buy_layout');

            Tpl::output('order_list',array($order_info));
            Tpl::output('args','buyer_id='.$_SESSION['member_id'].'&order_id='.$order_id);
            Tpl::output('api_pay_amount',$order_info['api_pay_amount']);
            Tpl::output('pay_url',base64_encode(encrypt($payment_api->get_payurl(),MD5_KEY)));
            Tpl::output('nav_list', rkcache('nav',true));
            Tpl::showpage('payment.wxpay');
        } elseif ($payment_info['payment_code'] == 'underline') {
            // 线下支付
            Tpl::setDir('buy');
            Tpl::setLayout('buy_layout');
            Tpl::output('order_list',array($order_info));
            Tpl::output('args','buyer_id='.$_SESSION['member_id'].'&order_id='.$order_id);
            Tpl::output('api_pay_amount',$order_info['api_pay_amount']);
            Tpl::output('pay_sn',$order_sn);
            Tpl::output('is_margin', $order_info['is_margin'] ? 1 : 0);
            Tpl::output('payment_info',unserialize($payment_info['payment_config']));
            Tpl::showpage('payment.underline');
        } else {
            @header("Location: ".$payment_api->get_payurl());
        }
        exit();
    }

    /**
     * 通知处理(支付宝异步通知和网银在线自动对账)
     *
     */
    public function notifyOp(){
        switch ($_GET['payment_code']) {
            case 'alipay':
                $success = 'success'; $fail = 'fail'; break;
            case 'chinabank':
                $success = 'ok'; $fail = 'error'; break;
            default:
                exit();
        }

        $order_type = $_POST['extra_common_param'];
        // 商城订单号
        $out_trade_no = $_POST['out_trade_no'];
        // 外部支付订单号
        $trade_no = $_POST['trade_no'];

        //参数判断
        if(!preg_match('/^\d{16}$/',$out_trade_no)) exit($fail);

        $logic_payment = Logic('payment');

        if ($order_type == 'margin') {
            // 保证金订单
            // 获取订单信息
            $result = $logic_payment->getMgOrderInfo($out_trade_no);
            $order_info = $result['data'];
            // 判断订单状态是否已经支付
            if (intval($order_info['api_pay_state'])) {
                exit($success);
            }
            // 计算第三方支付的金额
            $api_pay_amount = $order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount'];

        } elseif ($order_type == 'tail') {
            // 拍卖订单
            // 获取订单
            $result = $logic_payment->getAtOrderInfo($out_trade_no);
            $order_info = $result['data'];
            //订单存在被系统自动取消的可能性
            if (!in_array($order_info['order_state'],array(ORDER_STATE_NEW,ORDER_STATE_CANCEL))) {
                exit($success);
            }

            // 计算api支付金额
            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];

        } else {
            exit();
        }

        $order_pay_info = $result['data'];

        //取得支付方式
        $result = $logic_payment->getPaymentInfo($_GET['payment_code']);
        if (!$result['state']) {
            exit($fail);
        }
        $payment_info = $result['data'];

        //创建支付接口对象
        $payment_api = new $payment_info['payment_code']($payment_info,$order_pay_info);

        //对进入的参数进行远程数据判断
        $verify = $payment_api->notify_verify();
        if (!$verify) {
            exit($fail);
        }

        // 支付成功更新订单状态
        if ($order_type == 'margin') {
            $result = $logic_payment->updateMgOrder($payment_info['payment_code'], $order_pay_info, $trade_no);
        } elseif($order_type == 'tail'){
            $result = $logic_payment->updateAtOrder($payment_info['payment_code'], $order_pay_info, $trade_no);
        }

        if ($result['state']) {
            //记录消费日志
            if ($order_type == 'margin') {
                $log_buyer_id = $order_pay_info['buyer_id'];
                $log_buyer_name = $order_pay_info['buyer_name'];
                $log_desc = '保证金订单使用'.orderPaymentName($payment_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;
            } else if ($order_type == 'tail') {
                $log_buyer_id = $order_pay_info['buyer_id'];
                $log_buyer_name = $order_pay_info['buyer_name'];
                $log_desc = '拍卖订单使用'.orderPaymentName($payment_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;
            }
            QueueClient::push('addConsume', array('member_id'=>$log_buyer_id,'member_name'=>$log_buyer_name,
                'consume_amount'=>ncPriceFormat($api_pay_amount),'consume_time'=>TIMESTAMP,'consume_remark'=>$log_desc));
        }

        exit($result['state'] ? $success : $fail);
    }

    /**
     * 支付接口返回
     *
     */
    public function returnOp(){
        $order_type = $_GET['extra_common_param'];
        if ($order_type == 'margin') {
            $act = 'member_auction';
        } elseif($order_type == 'tail') {
            $act = 'member_auction&op=have_auction';
        } else {
            exit();
        }

        $out_trade_no = $_GET['out_trade_no'];
        $trade_no = $_GET['trade_no'];
        $url = SHOP_SITE_URL.'/index.php?act='.$act;

        //对外部交易编号进行非空判断
        if(!preg_match('/^\d{16}$/',$out_trade_no)) {
            showMessage('参数错误',$url,'','html','error');
        }

        $logic_payment = Logic('payment');

        if ($order_type == 'margin') {

            $result = $logic_payment->getMgOrderInfo($out_trade_no);
            if(!$result['state']) {
                showMessage($result['msg'], $url, 'html', 'error');
            }
            $order_info = $result['data'];
            if ($order_info['api_pay_state']) {
                $payment_state = 'success';
            }

            //支付成功页面展示在线支付了多少金额
            $order_info['api_pay_amount'] =  $order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount'];

        }elseif ($order_type == 'tail') {

            $result = $logic_payment->getAtOrderInfo($out_trade_no);
            if(!$result['state']) {
                showMessage($result['msg'], $url, 'html', 'error');
            }
            $order_info = $result['data'];
            if (!in_array($order_info['order_state'],array(ORDER_STATE_NEW))) {
                $payment_state = 'success';
            }

            //支付成功页面展示在线支付了多少金额
            $order_info['api_pay_amount'] = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];

        }
        $order_pay_info = $result['data'];
        $api_pay_amount = $order_info['api_pay_amount'];

        if ($payment_state != 'success') {
            //取得支付方式
            $result = $logic_payment->getPaymentInfo($_GET['payment_code']);
            if (!$result['state']) {
                showMessage($result['msg'],$url,'html','error');
            }
            $payment_info = $result['data'];

            //创建支付接口对象
            $payment_api = new $payment_info['payment_code']($payment_info,$order_pay_info);

            //返回参数判断
            $verify = $payment_api->return_verify();
            if(!$verify) {
                showMessage('支付数据验证失败',$url,'html','error');
            }

            //取得支付结果
            $pay_result = $payment_api->getPayResult($_GET);
            if (!$pay_result) {
                showMessage('非常抱歉，您的订单支付没有成功，请您后尝试',$url,'html','error');
            }

            //更改订单支付状态
            if ($order_type == 'margin') {
                $result = $logic_payment->updateMgOrder($payment_info['payment_code'], $order_pay_info, $trade_no);
            } else if ($order_type == 'tail') {
                $result = $logic_payment->updateAtOrder($payment_info['payment_code'], $order_pay_info, $trade_no);
            }
            if (!$result['state']) {
                showMessage('支付状态更新失败',$url,'html','error');
            } else {
                //记录消费日志
                if ($order_type == 'margin') {
                    $log_buyer_id = $order_pay_info['buyer_id'];
                    $log_buyer_name = $order_pay_info['buyer_name'];
                    $log_desc = '保证金订单使用'.orderPaymentName($payment_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;
                } else if ($order_type == 'tail') {
                    $log_buyer_id = $order_pay_info['buyer_id'];
                    $log_buyer_name = $order_pay_info['buyer_name'];
                    $log_desc = '拍卖订单使用'.orderPaymentName($payment_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;
                }
                QueueClient::push('addConsume', array('member_id'=>$log_buyer_id,'member_name'=>$log_buyer_name,
                    'consume_amount'=>ncPriceFormat($api_pay_amount),'consume_time'=>TIMESTAMP,'consume_remark'=>$log_desc));
            }
        }

        //支付成功后跳转
        if ($order_type == 'margin') {
            $pay_ok_url = C('auction_site_url').'/index.php?act=auction_buy&op=pay_ok&order_sn='.$out_trade_no.'&margin_id='.$order_info['margin_id'].'&order_amount='.ncPriceFormat($order_info['margin_amount']);
        } elseif ($order_type == 'tail') {
            $pay_ok_url = C('auction_site_url').'/index.php?act=auction_buy&op=pay_ok&order_sn='.$out_trade_no.'&order_id='.$order_info['auction_order_id'].'&order_amount='.ncPriceFormat($order_info['order_amount']);
        }

        if ($payment_info['payment_code'] == 'tenpay') {
            showMessage('',$pay_ok_url,'tenpay');
        } else {
            redirect($pay_ok_url);
        }
    }

    /**
     * 接收微信请求，接收productid和用户的openid等参数，执行（【统一下单API】返回prepay_id交易会话标识
     */
    public function wxpay_returnOp() {
        $result = Logic('payment')->getPaymentInfo('wxpay');
        if (!$result['state']) {
            \Shopnc\log::record('wxpay not found','RUN');
        }
        new wxpay($result['data'],array());
        require_once BASE_PATH.'/api/payment/wxpay/native_notify.php';
    }

    /**
     * 支付成功，更新订单状态
     */
    public function wxpay_notifyOp() {
        $result = Logic('payment')->getPaymentInfo('wxpay');
        if (!$result['state']) {
            \Shopnc\log::record('wxpay not found','RUN');
        }
        new wxpay($result['data'],array());
        require_once BASE_PATH.'/api/payment/wxpay/notify.php';
    }

    /**
     * 上传线下付款凭证
     * @return [type] [description]
     */
    public function pay_underline_orderOp(){
        // 1.接收参数
        $url = SHOP_SITE_URL.'/index.php?act=member_order';

        // 2.获取订单信息验证是否是新订单
        if (intval($_POST['is_margin']) == 1) {
            echo 1;
            // 保证金
            $model_margin_orders = Model('margin_orders');
            $result = $model_margin_orders->getOrderInfo(array('order_sn' => $_POST['pay_sn']));
            $state = $result['order_state'] == 1;
            $type = 0;
            $order_sn = $result['order_sn'];
        } else {
            echo 2;
            // 尾款
            $model_auction_order = Model('order');
            $result = $model_auction_order->getOrderInfo(array('order_sn' => $_POST['pay_sn']));
            $state = $result['order_state'] != 10;
            $type = 1;
            $order_sn = $result['auction_order_sn'];
        }
        if (!$result) {
            showMessage('订单不存在',$url,'html','error');
        }
        if ($state) {
            showMessage('订单状态异常',$url,'html','error');
        }

        // 3.上传图片
        if(empty($_POST['pay_voucher'])) {
            showMessage('请上传付款凭证','','','error');
        }
        $logic_auction_order = Logic('auction_order');
        // 4.修改订单状态
        $result = $logic_auction_order->changeOrderStateInConfirm($order_sn, $_POST['pay_voucher'], $type);
        if ($result['state']) {
            $url = SHOP_SITE_URL.'/index.php?act=member_auction';
            showMessage('您的凭证已提交平台确认，请耐心等待', $url, 'html');
        } else {
            showMessage('操作失败，请重试', $url, 'html','error');
        }
    }
}
