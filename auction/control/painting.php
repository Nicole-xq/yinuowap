<?php
/**
 * 书画展示页面
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class paintingControl extends BasePaintingControl{
    public function __construct() {
        parent::__construct();
    }
    //书画首页
    public function indexOp(){
        $model_co_store = Model('cooper_store');
        $co_store_list = $model_co_store->getcooper_storeList(array('is_show'=>1));
        Tpl::output('co_store_list',$co_store_list);
        $model_paint_tag = Model('painting_tag');
        $paint_tag_list = $model_paint_tag->getpainting_tagList(array());
        Tpl::output('paint_tag_list',$paint_tag_list);
        $model_paint_module = Model('paint_module');
        $paint_module_list1 = $model_paint_module->getpaint_moduleList(array('paint_module_show'=>1));
        if(!empty($paint_module_list1)){
            foreach($paint_module_list1 as $k=>$value){
                $paint_module_list[$value['paint_module_id']] = $model_paint_module->getpaint_moduleInfo(array('paint_module_id'=>$value['paint_module_id']));
            }
        }

        $model_paint_rec = Model('paint_auction_rec');
        $model_auctions = Model('auctions');
        if(!empty($paint_module_list)){
            foreach($paint_module_list as $k=>$v){
                $tag_rec  = $model_paint_rec->getPaintRecTagList(array('module_title_id'=>$v['paint_module_id']),'','','module_title_id,tag_name,auction_id','tag_name');
                foreach($tag_rec as $key=>$val){
                    $paint_auction_info = $model_paint_rec->getPaintRecTagList(array('module_title_id'=>$v['paint_module_id'],'tag_name'=>$val['tag_name']),'','','module_title_id,tag_name,auction_id');
                    foreach($paint_auction_info as $kk=>$vv ){
                        $paint_module_list[$k]['tag_rec'][$key]['tag_name'] = $vv['tag_name'];
                        $paint_module_list[$k]['tag_rec'][$key]['paint_rec'][$vv['auction_id']] = $model_auctions->getAuctionsInfo(array('auction_id'=>$vv['auction_id']));
                        if(!empty($paint_module_list[$k]['tag_rec'][$key]['paint_rec'])){
                            foreach($paint_module_list[$k]['tag_rec'][$key]['paint_rec'] as $auction_id=>$auction_info){
                                $paint_module_list[$k]['tag_rec'][$key]['paint_rec'][$auction_id]['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                                $paint_module_list[$k]['tag_rec'][$key]['paint_rec'][$auction_id]['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);

                            }
                        }

                    }
                }

            }
        }


        $special_rec = Model('special_rec');
        $model_special = Model('auction_special');
        $special_rec_list= $special_rec->getSpecialRecLists(array());
        if(!empty($special_rec_list)){
            foreach($special_rec_list as $key=>$val){
                $val['rec_auction'] = explode(',',$val['rec_auction']);
                foreach($val['rec_auction'] as $kk=>$vv){
                    $special_rec_list[$key]['auction_list'][$kk] = $model_auctions->getAuctionsInfo(array('auction_id'=>$vv));
                }
                $auction_count = $model_special->getSpecialGoodsCount(array('special_id'=>$val['special_id'],'auction_sp_state'=>1));
                $special_goods_list = $model_special->getSpecialGoodsLists(array('special_id'=>$val['special_id'],'auction_sp_state'=>1));
                $special_rec_list[$key]['auction_count'] = $auction_count;
                $person_num = 0;
                foreach($special_goods_list as $k=>$v){
                    $person_num += $v['num_of_applicants'];
                }

                $special_rec_list[$key]['person_num'] = $person_num;
            }
        }
        $model_setting = Model('setting');
        $list_setting = $model_setting->getListSetting();
        if ($list_setting['painting_banner_setting'] != ''){
            $list = unserialize($list_setting['painting_banner_setting']);
        }
        Tpl::output('list', $list);
        Tpl::output('special_rec_list',$special_rec_list);
        Tpl::output('paint_module_list',$paint_module_list);

        Tpl::showpage('painting_index');
    }

    public function paint_ajaxOp(){
        $tag_name = $_POST['tag_name'];
        $paint_module_id = $_POST['paint_module_id'];
        $model_paint_rec = Model('paint_auction_rec');
        $model_auctions = Model('auctions');
        $paint_auction_info = $model_paint_rec->getPaintRecTagList(array('module_title_id'=>$paint_module_id,'tag_name'=>$tag_name),'','','module_title_id,tag_name,auction_id');
        $auction_list = array();
        if(!empty($paint_auction_info)){
            foreach($paint_auction_info as $kk=>$vv ){
                $auction_list[$kk]  = $model_auctions->getAuctionsInfo(array('auction_id'=>$vv['auction_id']));
                if($auction_list[$kk]['auction_end_true_t'] != ''){
                    $auction_list[$kk]['time_cha'] = $auction_list[$kk]['auction_end_true_t'] - TIMESTAMP;
                }else{
                    $auction_list[$kk]['time_cha'] = $auction_list[$kk]['auction_end_time'] - TIMESTAMP;
                    $auction_list[$kk]['auction_end_true_t'] = $auction_list[$kk]['auction_end_time'];
                }
                $auction_list[$kk]['auction_start_date'] =date('m月d日 H:i',$auction_list[$kk]['auction_start_time']);
                $auction_list[$kk]['auction_end_date'] =date('m月d日 H:i',$auction_list[$kk]['auction_end_true_t']);
                if($auction_list[$kk]['auction_start_time'] > time()){
                    $auction_list[$kk]['is_kaipai'] = 0;
                }elseif(($auction_list[$kk]['auction_end_time'] < time() && empty($auction_list[$kk]['auction_end_true_t'])) || (!empty($auction_list[$kk]['auction_end_true_t']) && $auction_list[$kk]['auction_end_true_t'] < time())){
                    $auction_list[$kk]['is_kaipai'] = 2;
                }elseif($auction_list[$kk]['auction_start_time'] <= time() && ( ($auction_list[$kk]['auction_end_time'] >= time() && empty($auction_list[$kk]['auction_end_true_t'])) || (!empty($auction_list[$kk]['auction_end_true_t']) && $auction_list[$kk]['auction_end_true_t'] >= time()))){
                    $auction_list[$kk]['is_kaipai'] = 1;
                }
            }
        }

        echo json_encode($auction_list);

    }



}