<?php
/**
 * 拍品详情页面
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/5 0005
 * Time: 16:01
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctionsControl extends StoreAuctionControl
{
    protected $model_auctions;
    public function __construct() {
        $this->model_auctions = Model('auctions');
        parent::__construct();
    }

    /**
     * 拍品首页
     * */
    public function indexOp()
    {
        $auction_id = intval($_GET['id']);
        //获取拍品详细信息
        $auction_detail = $this->model_auctions->getAuctionDetail($auction_id);
        $auction_info = $auction_detail['auction_info'];
        $auction_info['interest_last_time'] = (strtotime($auction_info['interest_last_date']) - time()) > 0 && $auction_info['auction_bond_rate']!=0?strtotime($auction_info['interest_last_date']) - time():0;
//        print_R($auction_info);
        $goods_info = Logic('goods')->getGoodsCommonInfo($auction_info['goods_id']);
        $auction_info = array_merge($auction_info,$goods_info);
//        print_R($auction_info);exit;
        $logic_auction = Logic('auction');

        // 获取拍卖流程文章
        $result = $logic_auction->getAuctionArticle();
        $article_list = $result['data']['article_list'];

        //检查店铺状态，更多拍品
        $this->getStoreInfo($auction_info['store_id'], $auction_info);

        if (empty($auction_info)) {
            showMessage('该拍品不存在', C('auction_site_url'), 'html', 'error');
        }

        // 检查是否有人出过价
        if ($auction_info['current_price'] == 0.00 || empty($auction_info['current_price'])) {
            $auction_info['current_price'] = $auction_info['auction_start_price'];
        }
        //拍品预展开始倒计时时间
        $auction_info['auction_remaining_preview_start_time'] =  $auction_info['auction_preview_start'] >time() ? $auction_info['auction_preview_start'] - time() : '';

        // 拍品出价记录，处理匿名
        $bid_log_list = $logic_auction->getBidList($auction_id, 8);
        $bid_log_list_r = $bid_log_list['bid_log_list'];

        //获取当前会员跟拍品关系
        if ($_SESSION['member_id']) {
            $model_relation = Model('auction_member_relation');
            $model_member = Model('member');
            $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
            $relation_info = $model_relation->getRelationInfoByMemberAuctionID($_SESSION['member_id'], $auction_id);

            //检查会员是否关注该商品
            $model_favorites = Model('favorites');
            $is_favorites = $model_favorites->getOneFavorites(array('fav_id' => $auction_id, 'member_id' => $_SESSION['member_id'], 'fav_type' => 'auction'));
            $if_store_favorites = $model_favorites->getOneFavorites(array('member_id' => $_SESSION['member_id'],'fav_type' => 'store','fav_id' =>  $auction_info['store_id']));

        } else {
            $relation_info = array();
        }
        // 当前位置导航
        $nav_link_list = Model('goods_class')->getGoodsClassNav($auction_info['gc_id'], 0, 1);

        $nav_link_list[] = array('title' => $auction_info['auction_name']);
        Tpl::output('image_list', $auction_detail['image_list']);
        Tpl::output('image_json' , json_encode($auction_info['image_list']));
        Tpl::output('auction_image', $auction_detail['auction_image']);
        Tpl::output('auction_video', $auction_detail['video_path']);
        Tpl::output('auction_info', $auction_info);
        Tpl::output('is_favorites', $is_favorites);
        Tpl::output('if_store_favorites', $if_store_favorites);
        Tpl::output('nav_link_list', $nav_link_list);
        Tpl::output('relation_info', $relation_info);
        Tpl::output('member_info', $member_info);
        Tpl::output('bid_log_list_r', $bid_log_list_r);
        Tpl::output('auction_article_list', $article_list);

        Tpl::showpage('auctions');
    }

    /**
     * 发送短信验证码
     * */
    public function send_modify_mobileOp()
    {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_GET["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号码'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            exit(json_encode(array('state'=>'false','msg'=>$error)));
        }

        $model_member = Model('member');

        //发送频率验证
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$_SESSION['member_id']));
        if (!empty($member_common_info['send_mb_time'])) {
            if (TIMESTAMP - $member_common_info['send_mb_time'] < 58) {
                exit(json_encode(array('state'=>'false','msg'=>'请60秒以后再次发送短信')));
            }
        }

        // 生成验证码
        $verify_code = rand(1000,9999);

        //组装发送内容
        $model_tpl = Model('mail_templates');
        $tpl_info = $model_tpl->getTplInfo(array('code'=>'auction_mobile'));
        $param = array();
        $param['site_name'] = C('site_name');
        $param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
        $param['verify_code'] = $verify_code;
        $message    = ncReplaceText($tpl_info['content'],$param);
        // 发送
        $sms = new Sms();
        $result = $sms->send($_GET["mobile"],$message);

        // 发送成功回调
        if ($result) {
            $data = array();
            $data['auth_code'] = $verify_code;
            $data['send_acode_time'] = TIMESTAMP;
            $data['send_mb_time'] = TIMESTAMP;
            $update = $model_member->editMemberCommon($data,array('member_id'=>$_SESSION['member_id']));
            if (!$update) {
                exit(json_encode(array('state'=>'false','msg'=>'系统发生错误，如有疑问请与管理员联系')));
            }
            exit(json_encode(array('state'=>'true','msg'=>'发送成功')));
        } else {
            exit(json_encode(array('state'=>'false','msg'=>'发送失败')));
        }
    }

    /**
     * 确认会员订阅
     * */
    public function auction_remindOp()
    {
        $mobile = $_POST['mobile'];
        $is_vcode = $_POST['is_vcode'];
        $vcode = $_POST['vcode'];
        $is_remind = 1;
        $is_remind_app = 0;
        $auction_id = $_POST['auction_id'];

        $model_member = Model('member');
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_id));
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            json_api('error', $error);
        }
        //如果是获取验证码用户进行手机验证
        if ($is_vcode == 1) {
            $condition = array();
            $condition['member_id'] = $_SESSION['member_id'];
            $condition['auth_code'] = intval($vcode);
            $member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
            if (!$member_common_info) {
                json_api('error', '手机验证码错误，请重新输入');
            }
            if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
                json_api('error','手机验证码已过期，请重新获取验证码');
            }
        }

        //建立订阅关系
        $model_auction_member_relation = Model('auction_member_relation');
        //查询是否已经存在
        $relation = $model_auction_member_relation->getRelationInfoByMemberAuctionID($_SESSION['member_id'], $auction_id);
        $insert['auction_id'] = $auction_id;
        $insert['member_id'] = $_SESSION['member_id'];
        $insert['is_remind'] = $is_remind;
        $insert['member_mobile'] = $mobile;
        $insert['is_remind_app'] = $is_remind_app;
        $insert['member_name'] = $_SESSION['member_name'];
        $insert['remind_time'] = $auction_info['auction_end_time'] - AUCTION_OVER_REMIND;
        $insert['auction_name'] = $auction_info['auction_name'];
        if (empty($relation)) {
            $model_auction_member_relation->addRelation($insert);
        } else {
            $model_auction_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }
        // 设置提醒人数 +1
        $model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('auction_id' => $auction_id));

        json_api('ok', '订阅提醒成功');
    }

    /*
     * 取消订阅
     *
     * */
    public function cancel_auction_remindOp() 
    {
        $relation_id = intval($_GET['relation_id']);
        $auction_id = intval($_GET['auction_id']);
        if (empty($relation_id)) {
            showDialog('无效的订阅ID','','error');
        }
        $model_auction_member_relation = Model('auction_member_relation');
        $model_auctions = Model('auctions');
        $update = array(
            'is_remind' => 0,
            'is_remind_app' => 0,
        );
        $result_1 = $model_auction_member_relation->setRelationInfo($update, array('relation_id' => $relation_id));

        // 设置提醒人数 -1
        $result_2 = $model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num-1')), array('auction_id' => $auction_id));
        if ($result_1&&$result_2) {
            showDialog('拍品取消订阅成功','reload','succ');
        } else {
            showDialog('拍品取消订阅失败','','error');
        }
    }


    /**
     * 会员出价
     * */
    public function member_offerOp()
    {
        $auction_id = intval($_POST['auction_id']);
        $offer_num = intval($_POST['offer_num']);

        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
        // 验证出价
        if ($offer_num < $auction_info['auction_increase_range'] + $auction_info['current_price']) {
            json_api(1, '出价过低，不符合要求');
        }
        // 验证拍卖是否已经结束
        if (empty($auction_info['auction_end_true_t'])) {
            if (time() > $auction_info['auction_end_time']) {
                json_api(1,'拍卖已结束');
            }
        } else {
            if (time() > $auction_info['auction_end_true_t']) {
                json_api(1,'拍卖已结束');
            }
        }

        // 出价写入关系表
        $model_auction_member_relation = Model('auction_member_relation');
        $update = array(
            'is_offer' => 1,
            'offer_num' => $offer_num
        );
        $condition = array(
            'auction_id' => $auction_id,
            'member_id' => $_SESSION['member_id']
        );
        $result = $model_auction_member_relation->setRelationInfo($update, $condition);
        if (!$result) {
            json_api(1, '更新关系表失败');
        }

        $relation_info = $model_auction_member_relation->getRelationInfoByMemberAuctionID($_SESSION['member_id'], $auction_id);

        // 写入出价日志
        $model_bid_log = Model('bid_log');
        $param = array(
            'member_id' => $_SESSION['member_id'],
            'auction_id' => $auction_id,
            'created_at' => time(),
            'offer_num' => $offer_num,
            'member_name' => $_SESSION['member_name'],
            'is_anonymous' => $relation_info['is_anonymous'],
        );
        $result = $model_bid_log->addBid($param);
        if (!$result) {
            json_api(1, '写入出价日志失败');
        }

        //标记保证金订单已出价
        Model('margin_orders')->editOrder(array('is_bid'=>1),array('member_id'=>$_SESSION['member_id'],'auction_id'=>$auction_id,'order_state'=>1,'refund_state'=>0));

        // 更新拍品最新价格
        $update = array(
            'current_price' => $offer_num,
            'bid_number' => array('exp', 'bid_number+1'),
            'current_price_time' => time(),
        );
        $result = $model_auctions->editAuctions($update, array('auction_id' => $auction_id));
        if (!$result) {
            json_api(1, '更新拍品价格失败');
        }
        Logic('auction_agent')->updateCurrentPrice($auction_id);
        json_api(0, '出价成功');
    }

    /**
     * 获取出价列表
     * */
    public function get_bid_listOp()
    {
        $auction_id = intval($_GET['auction_id']);
        if($auction_id <= 0){
            showMessage('拍品ID异常','','html','error');
        }
        $page = $_GET['page'] ? $_GET['page'] : 8;
        $logic_auction = Logic('auction');
        $bid_log_list = $logic_auction->getBidList($auction_id, '', $page);
        $bid_showpage = $bid_log_list['showPage'];
        $bid_log_list_b = $bid_log_list['bid_log_list'];
        if ($_GET['ajax_poll'] == 1){
            exit(json_encode(['bid_log_list'=>$bid_log_list_b,'count_rows'=>$bid_log_list['count_rows']]));
        }
        Tpl::output('bid_log_list_b', $bid_log_list_b);
        Tpl::output('bid_showpage', $bid_showpage);
        Tpl::showpage('auction.bid_list', 'null_layout');
    }


    /**
     * 代理出价
     * */
    public function agent_submitOp()
    {
        $result = Logic('auction_agent')->setAgentPrice($_POST, $_SESSION['member_id']);
        exit(json_encode($result));
    }

    /*
     * 获取服务保障
     * */
    public function getServiceGuaranteeOp()
    {
        $result = Model('article')->getArticleOne(9, 15);
        $content = '';
        if ($result){
            $content = $result['article_content'];
        }
        Tpl::output('content', $content);
        Tpl::output('id', 'ncServiceGuarantee');
        Tpl::showpage('auction.tab_content', 'null_layout');
    }

    /*
     * 获取保证金须知
     * */
    public function getBondNoticeOp()
    {
        $result = Model('article')->getArticleOne(9, 16);
        $content = '';
        if ($result){
            $content = $result['article_content'];
        }
        Tpl::output('content', $content);
        Tpl::output('id', 'ncBondNotice');
        Tpl::showpage('auction.tab_content', 'null_layout');
    }

}
