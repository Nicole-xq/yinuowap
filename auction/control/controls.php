<?php
/**
 * 拍卖行前台父类
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');

class Control{
    /**
     * 检查短消息数量
     *
     */
    protected function checkMessage() {
        if($_SESSION['member_id'] == '') return ;
        //判断cookie是否存在
        $cookie_name = 'msgnewnum'.$_SESSION['member_id'];
        if (cookie($cookie_name) != null){
            $countnum = intval(cookie($cookie_name));
        }else {
            $message_model = Model('message');
            $countnum = $message_model->countNewMessage($_SESSION['member_id']);
            setNcCookie($cookie_name,"$countnum",2*3600);//保存2小时
        }
        Tpl::output('message_num',$countnum);
    }

    /**
     *  输出头部的公用信息
     *
     */
    protected function showLayout() {
        $this->checkMessage();//短消息检查
        $this->article();//文章输出

        $this->showCartCount();

        //热门搜索
        Tpl::output('hot_search',@explode(',',C('auction_hot_search')));

        if (C('auction_rec_search') != '') {
            $rec_search_list = @unserialize(C('auction_rec_search'));
        }
        Tpl::output('rec_search_list',is_array($rec_search_list) ? $rec_search_list : array());
        //历史搜索
        if (cookie('auction_his_sh') != '') {
            $his_search_list = explode('~', cookie('auction_his_sh'));
        }
        Tpl::output('his_search_list',is_array($his_search_list) ? $his_search_list : array());

        $model_class = Model('goods_class');
        $goods_class = $model_class->get_all_category();
        Tpl::output('show_goods_class',$goods_class);//商品分类

        //获取导航
        Tpl::output('nav_list', rkcache('nav',true));
        //查询保障服务项目
        Tpl::output('contract_list',Model('contract')->getContractItemByCache());
    }

    /**
     * 显示购物车数量
     */
    protected function showCartCount() {
        if (cookie('cart_goods_num') != null){
            $cart_num = intval(cookie('cart_goods_num'));
        }else {
            //已登录状态，存入数据库,未登录时，优先存入缓存，否则存入COOKIE
            if($_SESSION['member_id']) {
                $save_type = 'db';
            } else {
                $save_type = 'cookie';
            }
            $cart_num = Model('cart')->getCartNum($save_type,array('buyer_id'=>$_SESSION['member_id']));//查询购物车商品种类
        }
        Tpl::output('cart_goods_num',$cart_num);
    }

    /**
     * 系统公告
     */
    protected function system_notice() {
        $model_message  = Model('article');
        $condition = array();
        $condition['ac_id'] = 1;
        $condition['article_position_in'] = ARTICLE_POSIT_ALL.','.ARTICLE_POSIT_BUYER;
        $condition['limit'] = 5;
        $article_list  = $model_message->getArticleList($condition);
        Tpl::output('system_notice',$article_list);
    }

    /**
     * 输出会员等级
     * @param bool $is_return 是否返回会员信息，返回为true，输出会员信息为false
     */
    protected function getMemberAndGradeInfo($is_return = false){
        $member_info = array();
        //会员详情及会员级别处理
        if($_SESSION['member_id']) {
            $model_member = Model('member');
            $member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
            if ($member_info){
                $member_gradeinfo = $model_member->getOneMemberGrade(intval($member_info['member_exppoints']));
                $member_info = array_merge($member_info,$member_gradeinfo);
                $member_info['voucher_count'] = Model('voucher')->getCurrentAvailableVoucherCount($_SESSION['member_id']);
                $member_info['redpacket_count'] = Model('redpacket')->getCurrentAvailableRedpacketCount($_SESSION['member_id']);
                $member_info['security_level'] = $model_member->getMemberSecurityLevel($member_info);
            }
        }
        if ($is_return == true){//返回会员信息
            return $member_info;
        } else {//输出会员信息
            Tpl::output('member_info',$member_info);
        }
    }

    /**
     * 验证会员是否登录
     *
     */
    protected function checkLogin(){
        if ($_SESSION['is_login'] !== '1'){
            $ref_url = request_uri();
            if ($_GET['inajax']){
                showDialog('','','js',"login_dialog();",200);
            }else {
                @header("location: " . urlLogin('login', 'index', array('ref_url' => $ref_url)));
            }
            exit;
        }
    }

    //文章输出
    protected function article() {

        if (C('cache_open')) {
            if ($article = rkcache("index/article")) {
                Tpl::output('show_article', $article['show_article']);
                Tpl::output('article_list', $article['article_list']);
                return;
            }
        } else {
            if (file_exists(BASE_DATA_PATH.'/cache/index/article.php')){
                include(BASE_DATA_PATH.'/cache/index/article.php');
                Tpl::output('show_article', $show_article);
                Tpl::output('article_list', $article_list);
                return;
            }
        }

        $model_article_class    = Model('article_class');
        $model_article  = Model('article');
        $show_article = array();//商城公告
        $article_list   = array();//下方文章
        $notice_class   = array('notice');
        $code_array = array('member','store','payment','sold','service','about');
        $notice_limit   = 5;
        $faq_limit  = 5;

        $class_condition    = array();
        $class_condition['home_index'] = 'home_index';
        $class_condition['order'] = 'ac_sort asc';
        $article_class  = $model_article_class->getClassList($class_condition);
        $class_list = array();
        if(!empty($article_class) && is_array($article_class)){
            foreach ($article_class as $key => $val){
                $ac_code = $val['ac_code'];
                $ac_id = $val['ac_id'];
                $val['list']    = array();//文章
                $class_list[$ac_id] = $val;
            }
        }

        $condition  = array();
        $condition['article_show'] = '1';
        $condition['field'] = 'article.article_id,article.ac_id,article.article_url,article_class.ac_code,article.article_position,article.article_title,article.article_time,article_class.ac_name,article_class.ac_parent_id';
        $condition['order'] = 'article_sort asc,article_time desc';
        $condition['limit'] = '300';
        $article_array  = $model_article->getJoinList($condition);
        if(!empty($article_array) && is_array($article_array)){
            foreach ($article_array as $key => $val){
                if ($val['ac_code'] == 'notice' && !in_array($val['article_position'],array(ARTICLE_POSIT_SHOP,ARTICLE_POSIT_ALL))) continue;
                $ac_id = $val['ac_id'];
                $ac_parent_id = $val['ac_parent_id'];
                if($ac_parent_id == 0) {//顶级分类
                    $class_list[$ac_id]['list'][] = $val;
                } else {
                    $class_list[$ac_parent_id]['list'][] = $val;
                }
            }
        }
        if(!empty($class_list) && is_array($class_list)){
            foreach ($class_list as $key => $val){
                $ac_code = $val['ac_code'];
                if(in_array($ac_code,$notice_class)) {
                    $list = $val['list'];
                    array_splice($list, $notice_limit);
                    $val['list'] = $list;
                    $show_article[$ac_code] = $val;
                }
                if (in_array($ac_code,$code_array)){
                    $list = $val['list'];
                    $val['class']['ac_name']    = $val['ac_name'];
                    array_splice($list, $faq_limit);
                    $val['list'] = $list;
                    $article_list[] = $val;
                }
            }
        }
        if (C('cache_open')) {
            wkcache('index/article', array(
                'show_article' => $show_article,
                'article_list' => $article_list,
            ));
        } else {
            $string = "<?php\n\$show_article=".var_export($show_article,true).";\n";
            $string .= "\$article_list=".var_export($article_list,true).";\n?>";
            file_put_contents(BASE_DATA_PATH.'/cache/index/article.php',($string));
        }

        Tpl::output('show_article',$show_article);
        Tpl::output('article_list',$article_list);
    }

    /**
     * 自动登录
     */
    protected function auto_login() {
        $data = cookie('auto_login');
        if (empty($data)) {
            return false;
        }
        $model_member = Model('member');
        if ($_SESSION['is_login']) {
            $model_member->auto_login();
        }
        $member_id = intval(decrypt($data, MD5_KEY));
        if ($member_id <= 0) {
            return false;
        }
        $member_info = $model_member->getMemberInfoByID($member_id);
        $model_member->createSession($member_info);
    }
}

class BaseAuctionControl extends Control{
    /**
     * 构造函数
     */
    public function __construct(){
        if(!C('site_status')) halt(C('closed_reason'));
        /**
         * 判断拍卖市场是否关闭
         */
        if (C('auction_isuse') != '1'){
            header('location: '.SHOP_SITE_URL);die;
        }
        //输出头部的公用信息
        $this->showLayout();
        //输出会员信息
        $this->getMemberAndGradeInfo(false);

        Language::read('common,home_layout,core_lang_index');

        Tpl::setDir('home');

        Tpl::setLayout('home_layout');

        /**
         * 获取导航
         */
        Tpl::output('nav_list', rkcache('nav',true));

        // 自动登录
        $this->auto_login();
    }
}

/**
 * 店铺拍卖商品父类
 * */
class StoreAuctionControl extends Control{

    protected $store_info;
    protected $store_decoration_only = false;

    /**
     * 构造函数
     */
    public function __construct(){
        if(!C('site_status')) halt(C('closed_reason'));
        /**
         * 判断拍卖市场是否关闭
         */
        if (C('auction_isuse') != '1'){
            header('location: '.SHOP_SITE_URL);die;
        }
        //输出头部的公用信息
        $this->showLayout();
        //输出会员信息
        $this->getMemberAndGradeInfo(false);

        Language::read('common,home_layout,core_lang_index');

        Tpl::setDir('store');

        Tpl::setLayout('home_layout');

        /**
         * 获取导航
         */
        Tpl::output('nav_list', rkcache('nav',true));

        // 自动登录
        $this->auto_login();
    }

    /**
     * 获取店铺信息
     * */
    protected function getStoreInfo($store_id, $auction_info = null) {
        $model_store = Model('store');
        $store_info = $model_store->getStoreOnlineInfoByID($store_id);
        if(empty($store_info)) {
            showMessage(L('nc_store_close'), '', '', 'error');
        }
        if ($_COOKIE['dregion']) {
            $store_info['deliver_region'] = $_COOKIE['dregion'];
        }
        if (strpos($store_info['deliver_region'],'|')) {
            $store_info['deliver_region'] = explode('|', $store_info['deliver_region']);
            $store_info['deliver_region_ids'] = explode(' ', $store_info['deliver_region'][0]);
            $store_info['deliver_region_names'] = explode(' ', $store_info['deliver_region'][1]);
        }
        $this->outputStoreInfo($store_info, $auction_info);
    }

    /**
     * 检查店铺开启状态
     *
     * @param int $store_id 店铺编号
     * @param string $msg 警告信息
     */
    protected function outputStoreInfo($store_info, $auction_info = null) {
        if (!$this->store_decoration_only) {
            $model_contract = Model('contract');
            //获取店铺入驻保障服务
            $store_pcontract = $model_contract->getContract(array('ct_storeid' => $store_info['store_id'], 'ct_joinstate' => 2, 'ct_closestate' => 1));
            $pcontract_info = $model_contract->getContractItemByCache();
            //拿出店铺入驻的服务保障
            $store_pcontract_ids  = array();
            if (!empty($store_pcontract)) {
                foreach ($store_pcontract as $value) {
                    $store_pcontract_ids[] = $value['ct_id'];
                }
            }
            unset($store_pcontract);

            if (!empty($pcontract_info)) {
                foreach ($pcontract_info as $value) {
                    if (in_array($value['cti_id'], $store_pcontract_ids)) {
                        $store_pcontract[] = $value;
                    }
                }
            }
            //更多拍品
            $model_auctions = Model('auctions');
            $more_auctions = $model_auctions->getAuctionList(array('store_id' => $store_info['store_id'], 'state' => 4), '*', '', '', 8);

            //自家店铺不够搜索同类目拍品
            $count = count($more_auctions);
            if ($count < 8) {
                $limit = 8 - $count;
                $model_auctions_class = $model_auctions->getAuctionList(array('gc_id' => $auction_info['gc_id'], 'store_id' => array('neq', $store_info['store_id']), 'state' => 4), '*', '', '', $limit);

                for ($i = $count; $i < 8; $i++) {
                    if (!empty($model_auctions_class))
                    {
                        $array_0 = array_splice($model_auctions_class, 0, 1);
                        $more_auctions[$i] = $array_0['0'];
                    }
                }
            }
        }

        Tpl::output('store_info', $store_info);
        Tpl::output('page_title', $store_info['store_name']);
        Tpl::output('store_pcontract', $store_pcontract);
        Tpl::output('more_auctions', $more_auctions);
    }
}

/**
 * 拍品购买父类，必须登录
 * */
class AuctionsBuyControl extends Control {

    protected function __construct(){
        $auction_id = $_POST['auction_id'];
        if (intval($auction_id) < 0) {
            showDialog('拍卖商品异常', 'reload', 'error');
        }
        if (!$_SESSION['member_id']){
            redirect(urlLogin('login', 'index', array('ref_url' => request_uri())));
        }
        //验证该会员是否禁止购买
        if(!$_SESSION['is_buy']){
            showMessage(Language::get('cart_buy_noallow'),'','html','error');
        }
        Language::read('common,home_layout');
        Tpl::setDir('buy');
        Tpl::setLayout('buy_layout');
        if ($_GET['column'] && strtoupper(CHARSET) == 'GBK'){
            $_GET = Language::getGBK($_GET);
        }
        if(!C('site_status')) halt(C('closed_reason'));
        //获取导航
        Tpl::output('nav_list', rkcache('nav',true));

        Tpl::output('contract_list',Model('contract')->getContractItemByCache());
    }
}

/**
 * 书画中心control父类
 */
class BasePaintingControl extends Control {
    public function __construct(){
        Language::read('common,home_layout');
        //输出头部的公用信息
        $this->showLayout();

        Tpl::setDir('home');
        Tpl::setLayout('home_layout');

        if ($_GET['column'] && strtoupper(CHARSET) == 'GBK'){
            $_GET = Language::getGBK($_GET);
        }
        if(!C('site_status')) halt(C('closed_reason'));

        //判断系统是否开启书画功能
        if (C('painting_isuse') != 1 ){
            header('location: '.SHOP_SITE_URL);die;
        }
        Tpl::output('index_sign','painting');
    }

}

