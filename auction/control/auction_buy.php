<?php
/**
 * 拍品购买付款
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/22 0022
 * Time: 14:58
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_buyControl extends AuctionsBuyControl
{
    /**
     * @var auction_buyLogic
     */
    protected $logic_auction_buy;

    public function __construct()
    {
        parent::__construct();
        Language::read('home_cart_index');
        //注入logic 类
        $this->logic_auction_buy = Logic('auction_buy');
    }

    /**
     * 保证金支付选项页面
     * */
    public function show_margin_orderOp()
    {
        $auction_id = intval($_GET['auction_id']);
        $is_anonymous = intval($_GET['is_anonymous']);
        $margin_amount = intval($_GET['margin_amount']);
        // 如果设置匿名
        if ($is_anonymous != 1) {
            $is_anonymous = 0;
        }
        //获取界面 会员 店铺 拍品 收货地址 支付方式信息
        $result = $this->logic_auction_buy->show_margin_order($auction_id, $_SESSION['member_id']);

        //店铺信息
        Tpl::output('store_info', $this->logic_auction_buy->store_info);
        //拍品信息
        $this->logic_auction_buy->auction_info['margin_amount'] = $margin_amount;
        Tpl::output('auction_info', $this->logic_auction_buy->auction_info);
        //用户收货地址
        Tpl::output('address_info', $result['address_info']);
        //支付方式是否能选非线上支付
        Tpl::output('deny_edit_payment', $result['deny_edit_payment']);
        //不提供增值税发票时抛出true(模板使用)
        Tpl::output('vat_deny', $result['vat_deny']);
        //增值税发票哈希值(php验证使用)
        Tpl::output('vat_hash', $result['vat_hash']);
        //输出默认使用的发票信息
        Tpl::output('inv_info', $result['inv_info']);
        //保证金支付标识
        Tpl::output('is_margin_pay', true);
        //拍卖是否已经结束
        Tpl::output('is_auction_end_true_t', $result['is_auction_end_true_t']);
        // 是否匿名拍卖
        Tpl::output('is_anonymous', $is_anonymous);
        Tpl::output('buy_step', 'step2');
        Tpl::showpage('buy_1');
    }

    /**
     * 生成支付保证金订单
     * */
    public function create_margin_orderOp()
    {
        $auction_id = $_POST['auction_id'];
        $pay_name = $_POST['pay_name'];
        $address_id  = $_POST['address_id'];
        $is_margin = $_POST['is_margin'];
        $is_anonymous = $_POST['is_anonymous'];
        if ($is_margin != 1) {
            showDialog('支付保证金异常', 'reload', 'error');
        }

        //创建订单，记录线上或者线下支付方式，检查是否存在会员和拍品关系，不存在则创建。
        $result = $this->logic_auction_buy->create_margin_order($auction_id, $_SESSION['member_id'], $pay_name, $address_id, $is_anonymous, 1,$_POST);

        if(!$result['state']) {
            showMessage($result['msg'], urlAuction('auctions', 'index', array('id' => $_REQUEST['auction_id'])), 'html', 'error');
        }
        //转向到商城支付页面
        redirect('index.php?act=auction_buy&op=pay_order&order_sn='.$result['data']['order_sn']);
    }

    /**
     * 支付，显示支付方式选择页面
     *
     * */
    public function pay_orderOp()
    {
        $order_sn = $_GET['order_sn'];

        //验证订单号
        if (!preg_match('/^\d{16}$/',$order_sn)){
            showMessage("无效的订单号",'index.php?act=auctions','html','error');
        }

        $model_margin_orders = Model('margin_orders');
        //查询订单信息
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));

        if (empty($order_info)) {
            showMessage('未找到需要支付的订单','./','html','error');
        }

        // 排除非自己订单
        if ($order_info['buyer_id'] != $_SESSION['member_id']) {
            showMessage('未找到需要支付的订单','./','html','error');
        }

        // 排除线下支付保证金账户支付
        if ($order_info['order_type'] != 1 && $order_info['account_margin_amount'] == 0) {
            $logic_auction_order = Logic('auction_order');
            // 保证金账户支付
            $result = $logic_auction_order->MarginPay($_SESSION['member_id'], $order_info['order_sn']);
            // 更新订单内容
            $order_info = $result['data']['order_info'];
        }

        // 获取下单支付页面的信息，线下支付直接跳转，支付金额为0，跳转到支付逻辑
        $result = $this->getOrderPay($order_info, $_SESSION['member_id']);

        unset($order_info);
        list($if_underline, $order_info, $pay, $payment_list) = $result;
        Tpl::output('if_underline', $if_underline);
        Tpl::output('order_info',$order_info);
        Tpl::output('pay',$pay);
        Tpl::output('payment_list',$payment_list);
        // 标识 购买流程执行第几步
        Tpl::output('buy_step','step3');
        Tpl::showpage('buy_2');
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        static $if_underline = false;
        //显示支付金额和支付方式
        if ($order_info['payment_code'] != 'underline') {
            // 保证金总数
            $pay['pay_amount_online'] = $order_info['margin_amount'] - $order_info['account_margin_amount'];
            // 充值卡支付金额
            $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
            // 预存款支付金额
            $pay['payd_pd_amount'] = $order_info['pd_amount'];
            // 诺币支付
            $pay['payd_points_amount'] = $order_info['points_amount'];
            // 保证金账户支付
            $pay['account_margin_amount'] = $order_info['account_margin_amount'];

            // 未支付金额
            $pay['payd_diff_amount'] = $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'] - $order_info['account_margin_amount'];
            $order_info['payment_type'] = '在线支付';
            $pay['order_type'] = 0;
        } else {
            if ($if_underline != true) {
                $if_underline = true;
            }
            $pay['pay_amount_underline'] = $order_info['margin_amount']-$order_info['account_margin_amount'];
            $order_info['payment_type'] = '线下支付';
            $pay['order_type'] = 1;
        }
        //如果保证金金额为0，转到支付订单逻辑
        if ($order_info['margin_amount'] - $order_info['account_margin_amount'] == 0) {
            redirect('index.php?act=auction_payment&op=margin_order&order_sn='.$order_info['order_sn']);
        }

        if ($if_underline) {
            $html = '<html><head></head><body>';
            $html .= '<form method="post" name="E_FORM" action="'.APP_SITE_URL.'/index.php?act=auction_payment&op=margin_order">';
            $html .= '<input type="hidden" name="order_sn" value="'.$order_info['order_sn'].'">';
            $html .= '<input type="hidden" name="payment_code" value="underline">';
            $html .= '</form><script type="text/javascript">document.E_FORM.submit();</script>';
            $html .= '</body></html>';
            echo $html;exit;
        }

        //是否显示站内余额操作(如果以前没有使用站内余额支付过且非货到付款)
        $pay['if_show_pdrcb_select'] = ($pay['pay_amount_underline'] == 0 && $pay['payd_rcb_amount'] == 0 && $pay['payd_pd_amount'] == 0 && $pay['payd_points_amount']);

        //输出订单描述
        if (empty($pay['pay_amount_online']) && !empty($pay['pay_amount_underline'])) {
            $pay['order_remind'] = '下单成功，请尽快完成线下支付。';
        } elseif (empty($pay['pay_amount_underline']) && !empty($pay['pay_amount_online'])) {
            $pay['order_remind'] = '请您在'.(ORDER_AUTO_CANCEL_TIME*60).'分钟内完成支付，逾期订单将自动取消。 ';
        } else {
            $pay['order_remind'] = '保证金支付成功';
        }

        if ($pay['pay_amount_online'] > 0) {
            //显示支付接口列表
            $model_payment = Model('payment');
            $condition = array();
            $payment_list = $model_payment->getPaymentOpenList($condition);
            if (!empty($payment_list)) {
                unset($payment_list['predeposit']);
                unset($payment_list['offline']);
                if ($if_underline) {
                    unset($payment_list['alipay']);
                    unset($payment_list['tenpay']);
                    unset($payment_list['chinabank']);
                    unset($payment_list['wxpay']);
                } else {
                    unset($payment_list['underline']);
                }
            }
            if (empty($payment_list)) {
                showMessage('暂未找到合适的支付方式',urlShop('member_auction'),'html','error');
            }
        }
        if ($pay['if_show_pdrcb_select']) {
            //显示预存款、支付密码、充值卡
            $available_predeposit = $available_rc_balance = 0;
            $buyer_info = Model('member')->getMemberInfoByID($member_id);
            if (floatval($buyer_info['available_predeposit']) > 0) {
                $pay['member_pd'] = $buyer_info['available_predeposit'];
            }
            if (floatval($buyer_info['available_rc_balance']) > 0) {
                $pay['member_rcb'] = $buyer_info['available_rc_balance'];
            }
            $pay['member_paypwd'] = $buyer_info['member_paypwd'] ? true : false;
            $pay['member_points'] = $buyer_info['member_points'];
            $pay['points_money'] = ncPriceFormat($buyer_info['member_points']/100);
        }
        // 返回需要的数组
        return array(
            $if_underline,
            $order_info,
            $pay,
            $payment_list,
        );
    }

    /**
     * 加载买家收货地址
     *
     */
    public function load_addrOp() {
        $model_addr = Model('address');
        //如果传入ID，先删除再查询
        if (!empty($_GET['id']) && intval($_GET['id']) > 0) {
            $model_addr->delAddress(array('address_id'=>intval($_GET['id']),'member_id'=>$_SESSION['member_id']));
        }
        $condition = array();
        $condition['member_id'] = $_SESSION['member_id'];
        if (!C('delivery_isuse')) {
            $condition['dlyp_id'] = 0;
            $order = 'dlyp_id asc,address_id desc';
        }
        $list = $model_addr->getAddressList($condition,$order);
        Tpl::output('address_list',$list);
        Tpl::showpage('buy_address.load','null_layout');
    }

    /**
     * 添加新的收货地址
     *
     */
    public function add_addrOp(){
        $model_addr = Model('address');
        if (chksubmit()){
            $count = $model_addr->getAddressCount(array('member_id'=>$_SESSION['member_id']));
            if ($count >= 20) {
                exit(json_encode(array('state'=>false,'msg'=>'最多允许添加20个有效地址')));
            }
            //验证表单信息
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["true_name"],"require"=>"true","message"=>Language::get('cart_step1_input_receiver')),
                array("input"=>$_POST["area_id"],"require"=>"true","validator"=>"Number","message"=>Language::get('cart_step1_choose_area'))
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                $error = strtoupper(CHARSET) == 'GBK' ? Language::getUTF8($error) : $error;
                exit(json_encode(array('state'=>false,'msg'=>$error)));
            }
            $data = array();
            $data['member_id'] = $_SESSION['member_id'];
            $data['true_name'] = $_POST['true_name'];
            $data['area_id'] = intval($_POST['area_id']);
            $data['city_id'] = intval($_POST['city_id']);
            $data['area_info'] = $_POST['region'];
            $data['address'] = $_POST['address'];
            $data['tel_phone'] = $_POST['tel_phone'];
            $data['mob_phone'] = $_POST['mob_phone'];
            $insert_id = $model_addr->addAddress($data);
            if ($insert_id){
                exit(json_encode(array('state'=>true,'addr_id'=>$insert_id)));
            }else {
                exit(json_encode(array('state'=>false,'msg'=>'新地址添加失败')));
            }
        } else {
            Tpl::showpage('buy_address.add','null_layout');
        }
    }

    /**
     * 支付成功页面
     */
    public function pay_okOp() {
        $order_sn = $_GET['order_sn'];
        if (!preg_match('/^\d{16}$/',$order_sn)){
            showMessage(Language::get('cart_order_pay_not_exists'), urlAuction('member_auction'),'html','error');
        }

        //查询支付单信息
        $model_margin_orders= Model('margin_orders');
        $pay_info = $model_margin_orders->getOrderInfo(array('order_sn'=>$order_sn,'buyer_id'=>$_SESSION['member_id']));
        if(empty($pay_info)){
            showMessage(Language::get('cart_order_pay_not_exists'), urlAuction('member_auction'),'html','error');
        }
        Tpl::output('pay_info',$pay_info);

        Tpl::output('buy_step','step4');
        Tpl::showpage('buy_3');
    }

    /**
     * 显示支付尾款选择支付方式
     * */
    public function show_auction_orderOp()
    {
        $auction_order_id = $_GET['auction_order_id'];
        $margin_id = $_GET['margin_id'];
        // 获取支付尾款的必要信息
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->show_auction_order($auction_order_id, $margin_id, $_SESSION['member_id']);
        if (!$result['state']) {
            showDialog($result['msg'], urlAuction('member_auction', 'have_auction'), 'error');
        }
//        print_R($result);exit;
        //不提供增值税发票时抛出true(模板使用)
        Tpl::output('vat_deny', $result['data']['vat_deny']);
        //增值税发票哈希值(php验证使用)
        Tpl::output('vat_hash', $result['data']['vat_hash']);
        //输出默认使用的发票信息
        Tpl::output('inv_info', $result['data']['inv_info']);

        Tpl::output('store_info',$result['data']['store_info']);
        Tpl::output('auction_order_info',$result['data']['auction_order_info']);
        Tpl::output('margin_order_info',$result['data']['margin_order_info']);
        Tpl::output('auction_info',$result['data']['auction_info']);

        Tpl::output('buy_step', 'step2');
        Tpl::showpage('auction_buy_1');
    }

    /*
     * 更新尾款订单信息
     * */
    public function update_auction_orderOp() 
    {
        // 查询发票信息和相关记录进行更新
        $result = $this->logic_auction_buy->update_auction_order($_POST, $_SESSION['member_id']);
//        print_R($_POST);
//        print_R($result);exit;
        if ($result['state']) {
            //转向到商城支付页面
            redirect('index.php?act=auction_buy&op=pay_auction_order&order_sn='.$result['data']['order_sn']);
        } else {
            showDialog($result['msg'], urlAuction('member_auction', 'have_auction'), 'error');
        }
    }

    /**
     * 已拍到的商品支付尾款页面
     * */
    public function pay_auction_orderOp()
    {
        $auction_order_sn = $_GET['order_sn'];
        //验证订单号
        if (!preg_match('/^\d{16}$/',$auction_order_sn)){
            showMessage("无效的订单号", urlAuction('member_auction', 'have_auction'),'html','error');
        }

        $model_auction_order = Model('order');
        $condition = array(
            'order_sn' => $auction_order_sn
        );
        $order_info = $model_auction_order->getOrderInfo($condition);
        $result = $this->getAuctionOrderPay($order_info, $_SESSION['member_id']);
        unset($order_info);
        list($if_underline, $order_info, $pay, $payment_list) = $result;
        Tpl::output('if_underline', $if_underline);
        Tpl::output('order_info',$order_info);
        Tpl::output('pay',$pay);
        Tpl::output('payment_list',$payment_list);
        Tpl::output('buy_step','step3');
        Tpl::showpage('auction_buy_2');
    }

    /*
     * 尾款支付成功页面
     * */
    public function pay_auction_okOp()
    {
        $auction_order_sn = $_GET['order_sn'];
        if (!preg_match('/^\d{16}$/',$auction_order_sn)){
            showMessage(Language::get('cart_order_pay_not_exists'), urlAuction('member_auction', 'have_auction') ,'html','error');
        }

        //查询支付单信息
        $model_auction_order= Model('auction_order');
        $pay_info = $model_auction_order->getInfo(array('auction_order_sn'=>$auction_order_sn,'buyer_id'=>$_SESSION['member_id']));
        if(empty($pay_info)){
            showMessage(Language::get('cart_order_pay_not_exists'),urlAuction('member_auction', 'have_auction'),'html','error');
        }
        Tpl::output('pay_info',$pay_info);

        Tpl::output('buy_step','step4');
        Tpl::showpage('buy_3');
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getAuctionOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        $if_underline = false;
        //显示支付金额和支付方式
        $margin_info = Model('margin_orders')->getOrderInfo(array('auction_order_id'=>$order_info['order_id']));
        if ($order_info['payment_code'] != 'underline') {
            // 需要支付尾款
            if($order_info['order_amount'] < $margin_info['margin_amount']){//保证金大于订单金额
                $pay['pay_amount_online'] = 0;
                $pay['margin_pay'] = $order_info['order_amount'];
            }else{
                $pay['pay_amount_online'] = $order_info['order_amount'] - $margin_info['margin_amount'];
                $pay['margin_pay'] = $margin_info['margin_amount'];
            }

            // 充值卡支付金额
            $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
            // 预存款支付金额
            $pay['payd_pd_amount'] = $order_info['pd_amount'];
            // 诺币支付
            $pay['payd_points_amount'] = $order_info['points_amount'];
            // 未支付金额
            $pay['payd_diff_amount'] = $pay['pay_amount_online'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'];
            $order_info['payment_type'] = '在线支付';
            $pay['order_type'] = 0;
        } else {
            if ($if_underline != true) {
                $if_underline = true;
            }
            $pay['pay_amount_underline'] = $order_info['order_amount'] - $margin_info['margin_amount'];
            $order_info['payment_type'] = '线下支付';
            $pay['order_type'] = 1;
        }
        //如果订单未支付金额为0，转到支付订单逻辑
        if ($order_info['order_amount'] - $margin_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'] == 0) {
            redirect('index.php?act=auction_payment&op=auction_order&order_sn='.$order_info['order_sn']);
        }

        if ($if_underline) {
            $html = '<html><head></head><body>';
            $html .= '<form method="post" name="E_FORM" action="'.APP_SITE_URL.'/index.php?act=auction_payment&op=auction_order">';
            $html .= '<input type="hidden" name="order_sn" value="'.$order_info['order_sn'].'">';
            $html .= '<input type="hidden" name="payment_code" value="underline">';
            $html .= '</form><script type="text/javascript">document.E_FORM.submit();</script>';
            $html .= '</body></html>';
            echo $html;exit;
        }

        //是否显示站内余额操作(如果以前没有使用站内余额支付过且非货到付款)
        $pay['if_show_pdrcb_select'] = ($pay['pay_amount_underline'] == 0 && $pay['payd_rcb_amount'] == 0 && $pay['payd_pd_amount'] == 0 && $pay['payd_points_amount']);

        //输出订单描述
        if (empty($pay['pay_amount_online']) && !empty($pay['pay_amount_underline'])) {
            $pay['order_remind'] = '下单成功，请尽快完成线下支付。';
        } elseif (empty($pay['pay_amount_underline']) && !empty($pay['pay_amount_online'])) {
            $pay['order_remind'] = '请您在'.(ORDER_AUTO_CANCEL_TIME*60).'分钟内完成支付，逾期订单将自动取消。 ';
        } else {
            $pay['order_remind'] = '拍卖订单支付成功';
        }

        if ($pay['pay_amount_online'] >= 0) {
            //显示支付接口列表
            $model_payment = Model('payment');
            $condition = array();
            $payment_list = $model_payment->getPaymentOpenList($condition);
            if (!empty($payment_list)) {
                unset($payment_list['predeposit']);
                unset($payment_list['offline']);
                if ($if_underline) {
                    unset($payment_list['alipay']);
                    unset($payment_list['tenpay']);
                    unset($payment_list['chinabank']);
                    unset($payment_list['wxpay']);
                } else {
                    unset($payment_list['underline']);
                }
            }
            if (empty($payment_list)) {
                showMessage('暂未找到合适的支付方式','index.php?act=member_auction&op=have_auction','html','error');
            }
        }
        if ($pay['if_show_pdrcb_select']) {
            //显示预存款、支付密码、充值卡、诺币支付
            $available_predeposit = $available_rc_balance = 0;
            $buyer_info = Model('member')->getMemberInfoByID($member_id);
            if (floatval($buyer_info['member_points']) > 0) {
                $pay['member_points'] = $buyer_info['member_points'];
                $pay['points_money'] = ncPriceFormat($buyer_info['member_points']/100);
            }
            if (floatval($buyer_info['available_predeposit']) > 0) {
                $pay['member_pd'] = $buyer_info['available_predeposit'];
            }
            if (floatval($buyer_info['available_rc_balance']) > 0) {
                $pay['member_rcb'] = $buyer_info['available_rc_balance'];
            }
            $pay['member_paypwd'] = $buyer_info['member_paypwd'] ? true : false;


        }
        // 返回需要的数组
        return array(
            $if_underline,
            $order_info,
            $pay,
            $payment_list,
        );
    }

    /**
     * AJAX验证支付密码
     */
    public function check_pd_pwdOp(){
        if (empty($_GET['password'])) exit('0');
        $buyer_info = Model('member')->getMemberInfoByID($_SESSION['member_id'],'member_paypwd');
        echo ($buyer_info['member_paypwd'] != '' && $buyer_info['member_paypwd'] === md5($_GET['password'])) ? '1' : '0';
    }


    /**
     * 加载买家发票列表，最多显示10条
     *
     */
    public function load_invOp() {
        $logic_buy = Logic('buy');

        $condition = array();
        if ($logic_buy->buyDecrypt($_GET['vat_hash'], $_SESSION['member_id']) == 'allow_vat') {
        } else {
            Tpl::output('vat_deny',true);
            $condition['inv_state'] = 1;
        }
        $condition['member_id'] = $_SESSION['member_id'];

        $model_inv = Model('invoice');
        //如果传入ID，先删除再查询
        if (intval($_GET['del_id']) > 0) {
            $model_inv->delInv(array('inv_id'=>intval($_GET['del_id']),'member_id'=>$_SESSION['member_id']));
        }
        $list = $model_inv->getInvList($condition,10);
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                if ($value['inv_state'] == 1) {
                    $list[$key]['content'] = '普通发票'.' '.$value['inv_title'].' '.$value['inv_content'];
                } else {
                    $list[$key]['content'] = '增值税发票'.' '.$value['inv_company'].' '.$value['inv_code'].' '.$value['inv_reg_addr'];
                }
            }
        }
        Tpl::output('inv_list',$list);
        Tpl::showpage('buy_invoice.load','null_layout');
    }

    /**
     * 新增发票信息
     *
     */
    public function add_invOp(){
        $model_inv = Model('invoice');
        if (chksubmit()){
            //如果是增值税发票验证表单信息
            if ($_POST['invoice_type'] == 2) {
                if (empty($_POST['inv_company']) || empty($_POST['inv_code']) || empty($_POST['inv_reg_addr'])) {
                    exit(json_encode(array('state'=>false,'msg'=>Language::get('nc_common_save_fail','UTF-8'))));
                }
            }
            $data = array();
            if ($_POST['invoice_type'] == 1) {
                $data['inv_state'] = 1;
                $data['inv_title'] = $_POST['inv_title_select'] == 'person' ? '个人' : $_POST['inv_title'];
                $data['inv_content'] = $_POST['inv_content'];
            } else {
                $data['inv_state'] = 2;
                $data['inv_company'] = $_POST['inv_company'];
                $data['inv_code'] = $_POST['inv_code'];
                $data['inv_reg_addr'] = $_POST['inv_reg_addr'];
                $data['inv_reg_phone'] = $_POST['inv_reg_phone'];
                $data['inv_reg_bname'] = $_POST['inv_reg_bname'];
                $data['inv_reg_baccount'] = $_POST['inv_reg_baccount'];
                $data['inv_rec_name'] = $_POST['inv_rec_name'];
                $data['inv_rec_mobphone'] = $_POST['inv_rec_mobphone'];
                $data['inv_rec_province'] = $_POST['vregion'];
                $data['inv_goto_addr'] = $_POST['inv_goto_addr'];
            }
            $data['member_id'] = $_SESSION['member_id'];
            //转码
            $data = strtoupper(CHARSET) == 'GBK' ? Language::getGBK($data) : $data;
            $insert_id = $model_inv->addInv($data);
            if ($insert_id) {
                exit(json_encode(array('state'=>'success','id'=>$insert_id)));
            } else {
                exit(json_encode(array('state'=>'fail','msg'=>Language::get('nc_common_save_fail','UTF-8'))));
            }
        } else {
            Tpl::showpage('buy_address.add','null_layout');
        }
    }
}
