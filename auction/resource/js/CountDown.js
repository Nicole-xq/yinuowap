/**
 * 使用方法
 *  <div id="saleCountDown"></div>
 *  
 * js 文件中这么写
 * //活动倒计时
    $("#saleCountDown").ncCountDown({
        time: ncGlobal.promotionCountDownTime,// 时间
        unitHour:"小时",
        unitMinute:"分钟",
        unitSecond:"秒",
        end: function() {
            //倒计时结束回调
        }
    });
 */

(function(window, $) {

    /**
     * 默认设置
     * @type {{}}
     */
    var defaultSetting = {
        time: 10000,
        cndays: '',
        cnhours: '',
        cnminutes: '',
        cnseconds: '',
        //时间单位
        unitDay:"天",
        unitHour:":",
        unitMinute:":",
        unitSecond:"",

        end:function(){

        }
    };
    var CountDown = function(element, options) {

        this.$element = $(element);
        this.options = $.extend({}, defaultSetting, options);
        this.time = this.options.time;
        if (this.time === 0) return;
        this._timer = {};
        this._init();
    };
    /**
     * 初始化
     * @private
     */
    CountDown.prototype._init = function() {
        this._buildElememt();
        this._createTimer();

    };
    /**
     * 创建元素
     * @private
     */
    CountDown.prototype._buildElememt = function() {
        var that = this,
            eleArray,
            initTime = {
                days: Math.floor(that.time / (1 * 60 * 60 * 24)),
                hours: Math.floor(that.time / (1 * 60 * 60)) % 24,
                minutes: Math.floor(that.time / (1 * 60)) % 60,
                seconds: Math.floor(that.time / 1) % 60
            };


        ["days", "hours", "minutes", "seconds"].forEach(function(value, index) {
            that["$" + value] = $("<em>", {
                'class': that.options["cn" + value],
                'html': initTime[value] >= 10 || value == "days" ? initTime[value] : "0" + initTime[value]
            });
        });
        eleArray = new Array(this.$days, this.options.unitDay, this.$hours, this.options.unitHour, this.$minutes, this.options.unitMinute, this.$seconds , this.options.unitSecond);
        this.$element.append(eleArray);
    };
    /**
     * 创建定时器
     * @private
     */
    CountDown.prototype._createTimer = function() {
        var that = this;
        this._timer = setInterval(function() {
            if (that.time > 0) {
                that.time = parseInt(that.time) - 1;
                var days = Math.floor(that.time / (1 * 60 * 60 * 24));
                var hours = Math.floor(that.time / (1 * 60 * 60)) % 24;
                var minutes = Math.floor(that.time / (1 * 60)) % 60;
                var seconds = Math.floor(that.time / 1) % 60;

                if (days < 0) days = 0;
                if (hours < 0) hours = 0;
                if (minutes < 0) minutes = 0;
                if (seconds < 0) seconds = 0;
                that.$days.html(days);
                that.$hours.html(hours >= 10 ? hours : "0" + hours + '');
                that.$minutes.html(minutes >= 10 ? minutes : "0" + minutes + '');
                that.$seconds.html(seconds >= 10 ? seconds : "0" + seconds + '');
            } else {
                clearInterval(that._timer);
                typeof that.options.end == 'function' && that.options.end();
            }
        }, 1000);
    };

    /////
    function Plugin(options) {
        return this.each(function() {
            new CountDown(this, options);
        });
    }

    $.fn.ncCountDown = Plugin;
})(window, jQuery);