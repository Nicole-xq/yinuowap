<?php
/**
 * 拍卖板块初始化文件
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
define('APP_ID','auction');
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));

require __DIR__ . '/../shopnc.php';

define('TPL_NAME',TPL_SHOP_NAME);
define('SHOP_TEMPLATES_URL', SHOP_SITE_URL.'/templates/'.TPL_NAME);
define('SHOP_RESOURCE_SITE_URL',SHOP_SITE_URL.DS.'resource');
define('APP_SITE_URL', C('auction_site_url'));
define('TPL_NAME',TPL_AUCTION_NAME);
define('VENDUE_RESOURCE_SITE_URL',APP_SITE_URL.DS.'resource');
define('VENDUE_TEMPLATES_URL',APP_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);

Shopnc\Core::runApplication();