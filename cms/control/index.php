<?php
/**
 * cms首页
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class indexControl extends CMSHomeControl{

    public function __construct() {
        parent::__construct();
        Tpl::output('index_sign','index');
    }
    public function indexOp(){
        $model_artist = Model('artist_vendue');
        $artist_list = $model_artist->getArtist_vendueList(array('is_rec'=>1),'','artist_sort asc,artist_vendue_id desc','',5);
        Tpl::output('artist_list', $artist_list);
        $model_cms_tag = Model('cms_tag');
        $cms_tag_list = $model_cms_tag->getList(array(),'','tag_sort asc,tag_id desc');
        Tpl::output('cms_tag_list', $cms_tag_list);
        $model_article_class = Model('cms_article_class');
        $class_list = $model_article_class->getList(array(),'','class_sort asc,class_id desc');
        $model_article = Model('cms_article');
        foreach($class_list as $k=>$v){
            $class_list[$k][$v['class_id']] = $model_article->getListWithClassName(array('article_state'=>3,'article_class_id'=>$v['class_id'],'article_commend_flag'=>1),'','article_sort asc,article_id desc','*',1);
        }
        Tpl::output('class_list', $class_list);
        $article_list = $model_article->getListWithClassName(array('article_state'=>3),6,'article_sort asc,article_id desc','*');
        Tpl::output('show_page', $model_article->showpage(2));
        Tpl::output('article_list', $article_list);
        $re_zixun= $model_article->getList(array('article_state'=>3),'','article_click desc','*',5);
        Tpl::output('re_zixun', $re_zixun);
        Tpl::showpage('index1');
    }

    public function article_infoOp(){
        $article_id = $_GET['article_id'];
        $model_article = Model('cms_article');
        $article_info = $model_article->getOne(array('article_id'=>$article_id));
        $article_info['article_pic'] = getCMSArticleImageUrl('',$article_info['article_image']);
        echo json_encode(array('article_info'=>$article_info));
    }



}
