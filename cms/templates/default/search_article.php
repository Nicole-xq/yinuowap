<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo CMS_TEMPLATES_URL;?>/css/base1.css" rel="stylesheet" type="text/css">
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/base.css" rel="stylesheet" type="text/css">-->
<link href="<?php echo CMS_TEMPLATES_URL;?>/css/home_header.css" rel="stylesheet" type="text/css">
<script src="<?php echo RESOURCE_SITE_URL;?>/js/html5shiv.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/respond.min.js"></script>

<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script>


<body>
<link href="<?php echo CMS_TEMPLATES_URL;?>/css/layout1.css" rel="stylesheet" type="text/css">
<link href="<?php echo CMS_TEMPLATES_URL;?>/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo CMS_TEMPLATES_URL;?>/css/zixun.css" rel="stylesheet" type="text/css">
<style>
  /*弹出层*/
  .cd-popup1,.cd-popup2{position:fixed;left:0;top:0;height:100%;width:100%;background-color:rgba(0,0,0,.5);opacity:0;visibility:hidden;-webkit-transition:opacity .3s 0s,visibility 0s .3s;-moz-transition:opacity .3s 0s,visibility 0s .3s;transition:opacity .3s 0s,visibility 0s .3s;z-index:9999}
  .cd-popup1.is-visible1,.cd-popup2.is-visible2{opacity:1;visibility:visible;-webkit-transition:opacity .3s 0s,visibility 0s 0s;-moz-transition:opacity .3s 0s,visibility 0s 0s;transition:opacity .3s 0s,visibility 0s 0s}
  .cd-popup-container1,.cd-popup-container2{position:relative; left:50%; top:50%; margin-top:-120px; margin-left:-250px; padding:50px 40px 10px;width:500px;height:240px;background:#FFF;border-radius:.4rem .4rem .4rem .4rem;box-shadow:0 0 20px rgba(0,0,0,.2);-webkit-transform:translateY(-60px);-moz-transform:translateY(-60px);-ms-transform:translateY(-60px);-o-transform:translateY(-60px);transform:translateY(-60px);-webkit-backface-visibility:hidden;-webkit-transition-property:-webkit-transform;-moz-transition-property:-moz-transform;transition-property:transform;-webkit-transition-duration:.3s;-moz-transition-duration:.3s;-ms-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}
  .cd-popup-close{position:absolute;right:10px;top:10px;z-index:10;width:auto;height:1.25rem;display:block;font-size:14px}
  .is-visible1 .cd-popup-container1,.is-visible2 .cd-popup-container2{-webkit-transform:translateY(0);-moz-transform:translateY(0);-ms-transform:translateY(0);-o-transform:translateY(0);transform:translateY(0)}
  .cd-popup-box div div img{ display:block; width:80px; height:auto}
  .cd-popup-box div div h4{ text-align:left; font-size:0.9rem; float:left; width:80%; padding-left:20px}
  .cd-popup-box div div h4 b{ color:#F32613; display:block; padding-top:0.8rem}
  .cd-popup-box .share{ margin-top:45px}
  .cd-popup-box .share .sharebord .item input[type="text"]{ width:78%; float:left}
  .cd-popup-box .share .sharebord .item button{ width:18%; height:30px; float:right; background:#F32613; color:#fff; border:none; border-radius:0.3rem; font-family:"Hiragino Sans GB","Microsoft Yahei",arial,宋体,"Helvetica Neue",Helvetica,STHeiTi,sans-serif; font-weight:normal; cursor:pointer}
  .cd-popup-box .bdsharebuttonbox{ margin-top:30px;}
  .qrcodeTable{ margin-top:26px}
  .qrcodeTable h4{ padding:2rem 0 0 1.5rem; width:40%}
  #qrcodeTable{ display:block}
  .cd-popup-box .w-55{ width:55%}
  .cd-popup-box .w-45{ width:45%}
  .cd-popup-box .share-qrcode{ position:absolute; right:1.5rem; top:3rem}
  /*.bdsharebuttonbox a{ background:url(../images/share_icon.png) no-repeat !important;}*/
  .bdsharebuttonbox a:first-child{cursor:default; padding:0 4px 0 0; line-height:28px; font-size:13px;background:none !important}
  .share-title h4{font-size:16px;font-weight:bold}
  .share-title p{font-size:12px;padding-top:8px;padding-left:20px;width:400px;}
  .public-nav-layout .site-menu li a.current{background:none}
  .public-nav-layout .site-menu li .current:hover{background:#404040}
</style>
<!--首页新添加开始-->
<div class="middle-box news">
  <div id="new-tab" class="fl">
    <?php if(!empty($output['article_list']) && is_array($output['article_list'])) {?>
    <?php foreach($output['article_list'] as $value) {?>
    <div class="news-list-s">

          <a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$value['article_id']))?>">
            <div class="fl"><span><?php echo $value['class_name']?></span><img src="<?php echo getCMSArticleImageUrl($value['article_attachment_path'], $value['article_image'], 'list');?>" width="148" height="148" alt=""/></div>
            <div class="fr">
              <h1><?php echo $value['article_title'];?></h1>
              <h2><?php echo date('Y-m-d',$value['article_publish_time'])?>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:void(0);" class="article_share" data-gid="<?php echo $value['article_id']?>"><i class="fa fa-share-square-o" aria-hidden="true"></i>&nbsp;分享</a>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$value['article_id']))?>"><i class="fa fa-commenting-o" aria-hidden="true"></i>&nbsp;评论</a></h2>
              <p><?php echo $value['article_abstract'];?></p>
            </div>
          </a></div>

        <div class="clear"></div><?php } ?>
        <div class="pagination"> <?php echo $output['show_page'];?> </div>
        <div class="clear"></div>
      <?php } else { ?>
        <div class="no-content-b"><i class="search"></i><?php echo $lang['no_record'];?></div>
      <?php } ?>



  </div>
  <div class="hot-list fr">
    <div class="news-label fr">
      <h1>资讯标签</h1>
      <?php if(!empty($output['cms_tag_list'])){?>
        <div>
          <?php foreach($output['cms_tag_list'] as $k=>$v){?>
            <span <?php if($v['tag_id'] == $output['tag_id']){?>style="background: #000;"<?php }?>><a href="<?php echo urlCMS('article','article_search',array('tag_id'=>$v['tag_id']))?>" <?php if($v['tag_id'] == $output['tag_id']){?>style="color: #FFF;"<?php }?>><?php echo $v['tag_name']?></a></span>
          <?php }?>
        </div>
      <?php }?>
    </div>
    <div class="hot-goods fr">
      <img src="templates/default/images/zixun/hot-news.jpg" width="280" height="auto" alt=""/>
      <ul>
        <?php if(!empty($output['hot_auctions'])){?>
          <?php foreach($output['hot_auctions'] as $k=>$v){?>
            <li>
              <a href="<?php echo urlVendue('auctions','index',array('id'=>$v['auction_id']))?>" title="">
                <img src="<?php echo cthumb($v['auction_image'], 240,$v['store_id']);?>"  alt=""/>
                <div class="title"><?php echo $v['auction_name']?></div>
                <div class="info"><span class="fl">当前价&nbsp;<b><?php if($v['current_price'] != 0.00){echo ncPriceFormatForList($v['current_price']);}else{echo ncPriceFormatForList($v['auction_start_price']);}?></b></span><span class="fr"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;<b><?php echo $v['bid_number']?></b>&nbsp;次</span></div>
              </a>
            </li>
          <?php }?>
        <?php }?>

      </ul>
    </div>
  </div>
  <div class="clear"></div>
</div>

<!-- 立即推广-->
<div class="cd-popup1 cd-popup-box">
  <div class="cd-popup-container1">
    <div id="distri_info" class="share-title"></div>
    <div class="clear"></div>
    <div class="share">
      <div class="sharebord">
        <div class="item">
          <input type="text" id="link1" readonly><button class="clip_button">复制链接</button>
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="bdsharebuttonbox">
      <a href="javascript:void(0)">分享至：</a>
      <a href="javascript:void(0)" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
      <a href="javascript:void(0)" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
      <a href="javascript:void(0)" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
      <a href="javascript:void(0)" class="bds_sqq" data-cmd="sqq" title="分享到QQ好友"></a>
      <a href="javascript:void(0)" class="share"  data-cmd="more" title="" style="margin:6px 0 0"></a>
    </div>
    <a href="#0" class="cd-popup-close"><i class="fa fa-close" aria-hidden="true"></i>&nbsp;关闭</a>
  </div>
</div>
<script type="text/javascript" src="<?php echo CMS_RESOURCE_SITE_URL;?>/js/zclip/ZeroClipboard.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function(){

    //打开窗口
    $('.article_share').on('click', function(event){
      var g_id = $(this).attr('data-gid');
      $.ajax({
        type:'get',
        url:'index.php?act=index&op=article_info&article_id='+g_id,
        dataType:"json",
        success:function(res){
          var datas = res.article_info;
          var html = '<img src="'+datas.article_pic+'" class="fl"/>';
          html += '<h4>'+datas.article_title+'</h4>';
          html += '<p class="fr limited-words limited-words-2">'+datas.article_abstract+'</p>';
          $('#distri_info').html(html);
          $('#link1').val("<?php echo urlCMS('article','article_detail')?>&article_id="+datas.article_id);
/*          window._bd_share_config={
            "common":{
              "bdText":datas.article_title,
              "bdPic":datas.article_pic,
              "bdUrl":"<?php echo urlCMS('article','article_detail')?>&article_id="+datas.article_id
            },
            "share":{
              "bdSize":"24"
            },
            "selectShare":{
              "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
            }
          };
          with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];*/
          event.preventDefault();
          $('.cd-popup1').addClass('is-visible1');

        },
        error:function(error){
          showError('操作失败');
        }
      });
    });
    //关闭窗口
    $('.cd-popup1').on('click', function(event){
      if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup1') ) {
        event.preventDefault();
        $(this).removeClass('is-visible1');
      }
    });

    //ESC关闭
    $(document).keyup(function(event){
      if(event.which=='27'){
        $('.cd-popup2').removeClass('is-visible2');
      }
    });

    //复制
    var client = new ZeroClipboard($('.clip_button'));
    client.on( 'ready', function(event) {

      client.on('copy', function(event) {
        event.clipboardData.setData('text/plain',$('#link1').val());
      });

      client.on('aftercopy', function(event) {
        alert('你已经成功复制本地址，请直接粘贴推荐给你的朋友!');
      });
    });


  });
</script>
</body>

