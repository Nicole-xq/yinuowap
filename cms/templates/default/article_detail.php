<?php defined('InShopNC') or exit('Access Invalid!');?>
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/base1.css" rel="stylesheet" type="text/css">-->
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/base.css" rel="stylesheet" type="text/css">-->
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/home_header.css" rel="stylesheet" type="text/css">-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/html5shiv.js"></script>-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/respond.min.js"></script>-->
<!---->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/jquery.js"></script>-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/common.js" charset="utf-8"></script>-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/jquery-ui/jquery.ui.js"></script>-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/jquery.validation.min.js"></script>-->
<!--<script src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script>-->
<script type="text/javascript" src="<?php echo CMS_RESOURCE_SITE_URL;?>/js/waypoints.js"></script>
<script>
    var tb_line=true;
</script>

<body>
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/layout1.css" rel="stylesheet" type="text/css">-->
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/layout.css" rel="stylesheet" type="text/css">-->
<!--<link href="--><?php //echo CMS_TEMPLATES_URL;?><!--/css/zixun.css" rel="stylesheet" type="text/css">-->

<div class="middle-box news-detail">
  <div class="fl">
    <div class="nch-breadcrumb wrapper">
      <span>位置</span><span class="arrow">&gt;</span>
      <span><a href="<?php echo urlCMS('index','index')?>">艺术品资讯</a></span><span class="arrow">&gt;</span>
      <span><?php echo $output['article_detail']['article_title']?></span>
    </div>
    <div class="clear"></div>
    <div class="news-content">
      <h1><?php echo $output['article_detail']['article_title']?></h1>
            <span style="margin:0 auto 16px;display:block;width:220px"><?php echo date('Y-m-d',$output['article_detail']['article_publish_time'])?>&nbsp;&nbsp;&nbsp;&nbsp;
                <!--<i class="fa fa-share-square-o" aria-hidden="true"></i><a href="javascript:void(0);" id="btn_sns_share" data-title="<?php echo $output['article_detail']['article_title'];?>" data-image="<?php echo getCMSArticleImageUrl($output['article_detail']['article_attachment_path'], $output['article_detail']['article_image'], 'list');?>" data-publisher="<?php echo empty($output['article_detail']['article_author'])?$lang['cms_text_guest']:$output['article_detail']['article_author'];?>" data-origin="<?php echo empty($output['article_detail']['article_origin'])?C('site_name'):$output['article_detail']['article_origin'];?>" data-publish_time="<?php echo date('Y-m-d',$output['article_detail']['article_publish_time']);?>" data-abstract="<?php echo $output['article_detail']['article_abstract'];?>" class="cms-share" title="<?php echo $lang['cms_text_share_count'];?>"><i></i><font><?php echo $lang['cms_text_share'];?></font></a>-->
              <div class="bdsharebuttonbox" style="height:16px;float:right"><a href="#" class="share" style="background-image:none;font-size:14px;padding:0;margin:2px 0 0" data-cmd="more"><i class="fa fa-share-square-o" aria-hidden="true"></i>&nbsp;分享</a><a href="#article_comment" style="background:none;font-size:14px;padding:0;margin:2px 0 0">&nbsp;&nbsp;&nbsp;<i class="fa fa-commenting-o" aria-hidden="true"></i>&nbsp;评论</a></div>

            </span>

     <div class="clear"></div>
      <p><?php echo $output['article_detail']['article_content']?></p>

      <div style="margin-top: 20px">
        <?php if(!empty($output['article_pre'])){?>
          <a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$output['article_pre']['article_id']))?>" class="all-block" style="text-align: right;margin-bottom: 4px">上一篇:<?php echo $output['article_pre']['article_title']?></a>
        <?php }else{?>
          <a class="all-block" style="text-align: right;margin-bottom: 4px">上一篇:暂无</a>
        <?php }?>

        <?php if(!empty($output['article_after'])){?>
          <a href="<?php echo urlCMS('article','article_detail',array('article_id'=>$output['article_after']['article_id']))?>" class="all-block" style="text-align: right">下一篇:<?php echo $output['article_after']['article_title']?></a>
        <?php }else{?>
          <a class="all-block" style="text-align: right">下一篇:暂无</a>
        <?php }?>

      </div>
    </div>

    <article class="article-detail-content" id="article_comment">
      <?php if(intval(C('cms_attitude_flag')) === 1 && intval($output['article_detail']['article_attitude_flag']) === 1) { ?>
        <section  class="article-attitude">
          <!-- 心情 -->
          <?php require('templates/default/article_attitude.php');?>
        </section>
      <?php } ?>
      <?php if(intval(C('cms_comment_flag')) === 1 && intval($output['article_detail']['article_comment_flag']) === 1) { ?>
        <section class="article-comment">
          <!-- 评论 -->
          <?php require('comment.php');?>
        </section>
      <?php } ?>
    </article>
  </div>
  <div class="hot-list fr">
    <div class="hot-goods fr">
      <h1>精品推荐</h1>
      <ul>
        <?php if(!empty($output['hot_auctions'])){?>
          <?php foreach($output['hot_auctions'] as $k=>$v){?>
            <li>
              <a href="<?php echo urlVendue('auctions','index',array('id'=>$v['auction_id']))?>" title="">
                <img src="<?php echo cthumb($v['auction_image'], 240,$v['store_id']);?>"  alt=""/>
                <div class="title"><?php echo $v['auction_title']?></div>
                <div class="info"><span class="fl">当前价&nbsp;<b><?php if($v['current_price'] != 0.00){echo ncPriceFormatForList($v['current_price']);}else{echo ncPriceFormatForList($v['auction_start_price']);}?></b></span><span class="fr"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;<b><?php echo $v['bid_number']?></b>&nbsp;次</span></div>
              </a>
            </li>
          <?php }?>
        <?php }?>

      </ul>
    </div>
  </div>
  <div class="clear"></div>
</div>
<!--<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"1","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"share":{},"image":{"viewList":[],"viewText":"分享到：","viewSize":"16"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":[]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>-->
</body>
<?php require('widget_sns_share.php');?>

