<?php
defined('InShopNC') or exit('Access Invalid!');

$lang['stat_newmember']				= '用户';
$lang['stat_artist']				= '艺术家';


$lang['stat_newstore']				= '新增店铺';
$lang['stat_storesale']			    = '店铺销量';
$lang['stat_storedegree']			= '店铺等级';
$lang['stat_storelist']			    = '店铺详细';

$lang['stat_sale_income']			= '销售收入统计';
$lang['stat_predeposit']			= '预存款统计';
$lang['stat_goods_sale']			= '商品销售明细';

$lang['stat_promotion']			    = '促销统计';
$lang['stat_group']			        = '团购统计';

$lang['stat_refund']			    = '退款统计';
$lang['stat_evalstore']			    = '店铺动态评分';

$lang['stat_sale']			        = '订单统计';
$lang['stat_margin']			    = '保证金订单统计';

$lang['stat_goods_pricerange']	    = '价格销量';
$lang['stat_hotgoods']	            = '热卖商品';

$lang['stat_industryscale']	        = '行业规模';
$lang['stat_industryrank']	        = '行业排行';
$lang['stat_industryprice']	        = '价格分析';
$lang['stat_industrygeneral']	    = '概况总览';
$lang['stat_generalindex']	        = '统计概述';
$lang['stat_pricerange_setting']    = '价格区间设置';
$lang['stat_setting']               = '统计设置';
$lang['stat_goodspricerange']       = '商品价格区间';
$lang['stat_orderpricerange']       = '订单金额区间';

$lang['stat_validorder_explain']	= "符合以下任何一种条件的订单即为有效订单：1、采用在线支付方式支付并且已付款；2、采用货到付款方式支付并且交易已完成";