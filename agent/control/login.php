<?php
/**
 * 登录
 *
 * 包括 登录 验证 退出 操作
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class LoginControl extends SystemControl {

    /**
     * 不进行父类的登录验证，所以增加构造方法重写了父类的构造方法
     */
    public function __construct(){
        Language::read('common,layout,login');
        $result = chksubmit(true,true,'num');
        if ($result){
            if ($result === -11){
                showMessage('非法请求');
            }elseif ($result === -12){
                showMessage(L('login_index_checkcode_wrong'));
            }
            if (process::islock('agent')) {
                showMessage('您的操作过于频繁，请稍后再试');
            }
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["user_name"], "require"=>"true", "message"=>L('login_index_username_null')),
                array("input"=>$_POST["password"],  "require"=>"true", "message"=>L('login_index_password_null')),
                array("input"=>$_POST["captcha"],   "require"=>"true", "message"=>L('login_index_checkcode_null')),
            );
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage(L('error').$error);
            } else {
                $model_agent = Model('agent_account');
                $array  = array();
                $array['agent_account']    = $_POST['user_name'];
                $array['agent_pwd']= md5(trim($_POST['password']));
                $agent_info = $model_agent->getAgentAccountInfo($array);
                if(is_array($agent_info) and !empty($agent_info)) {
                    if ($agent_info['agent_id'] > 0) {
                        $area_agent_info = Model('area_agent')->getAreaAgentInfo(['agent_id'=>$agent_info['agent_id']]);
                    }
                    $agent_all_info = array_merge($agent_info,$area_agent_info);
                    $this->systemSetKey($agent_all_info);
                    $model_agent->updateAgentAccount(['id'=>$agent_info['id']],['last_login_time'=>TIMESTAMP]);
                    $this->log(L('nc_login'),1);
                    process::clear('agent');
                    @header('Location: index.php');exit;
                } else {
                    process::addprocess('agent');
                    showMessage(L('login_index_username_password_wrong'),'index.php?act=login&op=login');
                }
            }
        }
        Tpl::output('html_title',L('login_index_need_login'));
        Tpl::showpage('login','login_layout');
    }
    public function loginOp(){}
    public function indexOp(){}
}
