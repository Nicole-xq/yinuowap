<?php
/**
 * 区域代理考核
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class area_assessmentControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 新增区域代理
     */
    public function indexOp(){

        $agent_info = Model('area_agent')->getAreaAgentInfo(['agent_id'=>intval($this->admin_info['agent_id'])]);
        $condition = [
            'commission_lv'=>$agent_info['commission_lv'],
            'agent_type'=>$agent_info['area_type'],
        ];
        $model_assessment = Model('agent_assessment');
        $assessment_info = $model_assessment->getAgentAssessmentInfo($condition);
        $assessment_info['curr_bzj_content'] = unserialize($assessment_info['curr_bzj_content']);
        $assessment_info['curr_goods_content'] = unserialize($assessment_info['curr_goods_content']);
        $assessment_info['curr_artist_content'] = unserialize($assessment_info['curr_artist_content']);

        Tpl::output('agent_info',$agent_info);
        Tpl::output('assessment_info',$assessment_info);
        Tpl::showpage('area_assessment');
    }


}
