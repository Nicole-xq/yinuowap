<?php
/**
 * 返佣记录
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class commission_listControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('commission_list');
    }

    /**
     * 区域代理管理XML
     */
    public function get_xmlOp(){
        $condition = array();
        $condition['agent_id'] = $this->admin_info['agent_id'];
        $model_agent = Model('area_agent');
        $agent_type_arr = $model_agent->getAreaType();
        if ($_POST['query'] != '' && in_array($_POST['query'],array(1,2,3,4,5))) {
            $condition['commission_type'] = $_POST['query'];
            $param[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
//            $agent_list = $model_agent->getAreaAgentList($param);
//            $agent_id = empty($agent_list)?0:$agent_list[0]['agent_id'];
//            $agent_name = empty($agent_list)?'':$agent_list[0]['agent_name'];
//            $condition['agent_id'] = $agent_id;
        }
        $order = '';
        $param = ['id','cdate','commission_status'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $model_agent_order = Model('agent_order');
        $agent_order_list = $model_agent_order->getList($condition, '*', $page, $order);
        $data = array();
        $data['now_page'] = $model_agent_order->shownowpage();
        $data['total_num'] = $model_agent_order->gettotalnum();

        foreach ($agent_order_list as $value) {
            switch($value['commission_type']){
                case 1:
                    //1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻
                    $type = '商品订单';
                    break;
                case 2:
                    $type = '拍卖订单';
                    break;
                case 3:
                    $type = '保证金返佣';
                    break;
                case 4:
                    $type = '合伙人升级';
                    break;
                case 5:
                    $type = '艺术家入驻';
                    break;
                default:
                    $type = '';
                    break;
            }
            $param = array();
//            $param['operation'] = "<a class='btn blue' href='index.php?act=area_agent&op=agent_add&agent_id=" . $value['agent_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['id'] = $value['id'];
            $agent_info = $model_agent->getAreaAgentInfo(array('agent_id'=>$value['agent_id']));
//            $param['agent_id'] = $value['agent_id'];
//            $param['agent_name'] = $agent_info['agent_name'];
//            $param['agent_type'] = $agent_type_arr[$value['agent_type']];
//            $param['agent_lv'] = $value['agent_lv'];
            $param['type'] = $type;
            $param['rate_num'] = $value['rate_num'].'%';
            $param['order_price'] = $value['order_price'];
            $param['description'] = $value['description'];
//            $param['from_member_id'] = $value['from_member_id'];
            $param['commission_amount'] = $value['commission_amount'];
            $param['commission_status'] = $value['commission_status'] == 0?'未结算':'已结算';
            $param['finish_time'] = $value['commission_status'] == 1?$value['finish_time']:'';
            $param['cdate'] = $value['cdate'];
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

}
