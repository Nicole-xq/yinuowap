<?php
/**
 * 统计管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class stat_tradeControl extends SystemControl{
    private $links = array(
        array('url'=>'act=stat_trade&op=orderCount','lang'=>'stat_sale'),
        array('url'=>'act=stat_trade&op=marginCount','lang'=>'stat_margin')
    );

    private $search_arr;//处理后的参数

    public function __construct(){
        parent::__construct();
        Language::read('stat');
        import('function.statistics');
        import('function.datehelper');
        $model = Model('stat');
        //存储参数
        $this->search_arr = $_REQUEST;
        //处理搜索时间
        if (!isset($this->search_arr['op']) || in_array($_REQUEST['op'],array('index','orderCount','marginCount'))){
            $this->search_arr = $model->dealwithSearchTime($this->search_arr);
            //获得系统年份
            $year_arr = getSystemYearArr();
            //获得系统月份
            $month_arr = getSystemMonthArr();
            //获得本月的周时间段
            $week_arr = getMonthWeekArr($this->search_arr['week']['current_year'], $this->search_arr['week']['current_month']);
            Tpl::output('year_arr', $year_arr);
            Tpl::output('month_arr', $month_arr);
            Tpl::output('week_arr', $week_arr);
        }
        Tpl::output('search_arr', $this->search_arr);
    }

    public function indexOp() {
        $this->orderCountOp();
    }

    /**
     * 区域代理订单统计
     */
    public function orderCountOp(){
        $model = Model('stat');
        //存储参数
        $this->search_arr = $_REQUEST;
        //处理搜索时间
        $this->search_arr = $model->dealwithSearchTime($this->search_arr);
        //获得系统年份
        $year_arr = getSystemYearArr();
        //获得系统月份
        $month_arr = getSystemMonthArr();
        //获得本月的周时间段
        $week_arr = getMonthWeekArr($this->search_arr['week']['current_year'], $this->search_arr['week']['current_month']);
        Tpl::output('year_arr', $year_arr);
        Tpl::output('month_arr', $month_arr);
        Tpl::output('week_arr', $week_arr);
        Tpl::output('search_arr', $this->search_arr);

        if ($this->search_arr['op'] == 'orderCount'){
            $actived ='orderCount';
            $commission_type = 1;
        }else{
            $actived = 'marginCount';
            $commission_type = 3;
        }

        Tpl::output('commission_type', $commission_type);
        Tpl::output('top_link',$this->sublink($this->links, $actived));
        Tpl::showpage('stat.order_count');
    }

    /**
     * 输出区域代理平台订单总数据
     */
    public function get_plat_saleOp(){
        $where = $this->_param_data();
        $count_arr = Model('agent_order')->getRowCount($where,' COUNT(*) as count_num, SUM(order_price) as price_amount');
        echo '<div class="title"><h3>订单情况一览</h3></div>';
        echo '<dl class="row"><dd class="opt"><ul class="nc-row">';
        echo '<li title="总销售额：'. number_format(($count_arr['price_amount']),2).'元"><h4>总销售额</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$count_arr['price_amount'].'"></h2><h6>元</h6></li>';
        echo '<li title="总订单量：'. intval($count_arr['count_num']).'笔"><h4>总订单量</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'. $count_arr['count_num'].'"></h2><h6>笔</h6></li>';
        echo '</ul></dd><dl>';
        exit();
    }

    /**
     * 输出区域代理订单统计XML数据
     */
    public function get_sale_xmlOp(){
        $model = Model('agent_order');
        $where = $this->_param_data();
        $list = $model->getList($where, '*', intval($_POST['rp']));
        $commission_status = [0=>'未结算',1=>'已结算',2=>'已打款'];
        $stat_data = [];
        foreach ($list as $k => $v){
            $out_array = [
                'order_no'=>$v['order_no'],
                'description'=>$v['description'],
                'order_price'=>$v['order_price'],
                'commission_status'=>$commission_status[$v['commission_status']],
                'finish_time'=>$v['finish_time'],
                'cdate'=>$v['cdate']
            ];
            $stat_data[$v['id']] = $out_array;
        }

        $data = [
            'now_page'=>$model->shownowpage(),
            'total_num'=>$model->gettotalnum(),
            'list'=>$stat_data,
        ];
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 检索条件
     */
    private function _param_data(){
        //默认统计当前数据
        if(!$this->search_arr['search_type']){
            $this->search_arr['search_type'] = 'day';
        }
        //计算昨天和今天时间
        if($this->search_arr['search_type'] == 'day'){
            $etime = $this->search_arr['search_time']." 23:59:59";
            $curr_stime = $this->search_arr['search_time']." 00:00:00";
        } elseif ($this->search_arr['search_type'] == 'week'){
            $current_weekarr = explode('|', $this->search_arr['searchweek_week']);
            $etime = $current_weekarr[1]." 23:59:59";
            $curr_stime = $current_weekarr[0]." 00:00:00";
        } elseif ($this->search_arr['search_type'] == 'month'){
            $last_day = getMonthLastDay($this->search_arr['searchmonth_year'],$this->search_arr['searchmonth_month']);
            $etime = date("Y-m-d 23:59:59",$last_day);
            $curr_stime = $this->search_arr['searchmonth_year'].'-'.$this->search_arr['searchmonth_month']."-01 00:00:00";
        }
        $where = [
            'commission_type'=>intval($this->search_arr['commission_type']),
            'cdate'=>['between',[$curr_stime,$etime]],
            'agent_id'=>$this->admin_info['agent_id'],
        ];

        return $where;
    }

    /**
     * 订单走势
     */
    public function sale_trendOp(){
        $model = Model('agent_order');
        //默认统计当前数据
        $where = $this->_param_data();
        $stattype = trim($_GET['type']);
        if($stattype == 'ordernum'){
            $field = ' COUNT(*) as ordernum ';
            $stat_arr['title'] = '订单量统计';
            $stat_arr['yAxis'] = '订单量';
        } else {
            $stattype = 'orderamount';
            $field = ' SUM(order_price) as orderamount ';
            $stat_arr['title'] = '订单销售额统计';
            $stat_arr['yAxis'] = '订单销售额';
        }
        if($this->search_arr['search_type'] == 'day'){
            $curr_day = strtotime($this->search_arr['search_time']);
            $last_day = $curr_day - 24*60*60;

            $today_day = @date('d', $curr_day);//今天日期
            $yesterday_day = @date('d', $last_day);//昨天日期

            $curr_date = date('Y-m-d',$curr_day)." 23:59:59";
            $last_date = date('Y-m-d',$last_day)." 00:00:00";
            $where['cdate'] = ['between',[$last_date,$curr_date]];

            //构造横轴数据
            for($i=0; $i<24; $i++){
                //统计图数据
                $curr_arr[$i] = 0;//今天
                $up_arr[$i] = 0;//昨天
                //统计表数据
                $currlist_arr[$i]['timetext'] = $i;

                $uplist_arr[$i]['val'] = 0;
                $currlist_arr[$i]['val'] = 0;
                //横轴
                $stat_arr['xAxis']['categories'][] = "$i";
            }

            $field .= " ,DATE_FORMAT(cdate, '%d')  as dayval,DATE_FORMAT(cdate, '%H') as hourval ";
            if (C('dbdriver') == 'mysql') {
                $_group = 'dayval,hourval';
            } elseif (C('dbdriver') == 'oracle') {
                $_group = "DATE_FORMAT(cdate, '%d'),DATE_FORMAT(cdate, '%H')";
            }
            $orderlist = $model->getList($where, $field, '', '', '', $_group);

            foreach((array)$orderlist as $k => $v){
                $v['hourval'] = intval($v['hourval']);
                $v['dayval'] = intval($v['dayval']);
                if($today_day == $v['dayval']){
                    $curr_arr[$v['hourval']] = intval($v[$stattype]);
                    $currlist_arr[$v['hourval']]['val'] = $v[$stattype];
                }
                if($yesterday_day == $v['dayval']){
                    $up_arr[$v['hourval']] = intval($v[$stattype]);
                    $uplist_arr[$v['hourval']]['val'] = $v[$stattype];
                }
            }
            $stat_arr['series'][0]['name'] = '昨天';
            $stat_arr['series'][0]['data'] = array_values($up_arr);
            $stat_arr['series'][1]['name'] = '今天';
            $stat_arr['series'][1]['data'] = array_values($curr_arr);
        }

        if($this->search_arr['search_type'] == 'week'){
            $current_weekarr = explode('|', $this->search_arr['searchweek_week']);
            $searchtime_arr[0] = strtotime($current_weekarr[0])-86400*7;
            $searchtime_arr[1] = strtotime($current_weekarr[1])+86400-1;

            $up_week = @date('W', $searchtime_arr[0]);//上周
            $curr_week = @date('W', $searchtime_arr[1]);//本周

            $curr_date = date('Y-m-d',$searchtime_arr[1])." 23:59:59";
            $last_date = date('Y-m-d',$searchtime_arr[0])." 00:00:00";
            $where['cdate'] = ['between',[$last_date,$curr_date]];

            //构造横轴数据
            for($i=1; $i<=7; $i++){
                //统计图数据
                $up_arr[$i] = 0;
                $curr_arr[$i] = 0;
                $tmp_weekarr = getSystemWeekArr();
                //统计表数据
                $uplist_arr[$i]['timetext'] = $tmp_weekarr[$i];
                $currlist_arr[$i]['timetext'] = $tmp_weekarr[$i];
                $uplist_arr[$i]['val'] = 0;
                $currlist_arr[$i]['val'] = 0;
                //横轴
                $stat_arr['xAxis']['categories'][] = $tmp_weekarr[$i];
                unset($tmp_weekarr);
            }
            $field .= ',WEEKOFYEAR(cdate) as weekval,WEEKDAY(cdate)+1 as dayofweekval ';
            if (C('dbdriver') == 'mysql') {
                $_group = 'weekval,dayofweekval';
            } elseif (C('dbdriver') == 'oracle') {
                $_group = 'WEEKOFYEAR(cdate),WEEKDAY(cdate)+1';
            }
            $orderlist = $model->getList($where, $field, '', '', '', $_group);
            foreach((array)$orderlist as $k => $v){
                if ($up_week == $v['weekval']){
                    $up_arr[$v['dayofweekval']] = intval($v[$stattype]);
                    $uplist_arr[$v['dayofweekval']]['val'] = intval($v[$stattype]);
                }
                if ($curr_week == $v['weekval']){
                    $curr_arr[$v['dayofweekval']] = intval($v[$stattype]);
                    $currlist_arr[$v['dayofweekval']]['val'] = intval($v[$stattype]);
                }
            }
            $stat_arr['series'][0]['name'] = '上周';
            $stat_arr['series'][0]['data'] = array_values($up_arr);
            $stat_arr['series'][1]['name'] = '本周';
            $stat_arr['series'][1]['data'] = array_values($curr_arr);
        }

        if($this->search_arr['search_type'] == 'month'){
            $searchtime_arr[0] = strtotime($this->search_arr['searchmonth_year'].'-'.$this->search_arr['searchmonth_month']."-01 -1 month");
            $searchtime_arr[1] = getMonthLastDay($this->search_arr['searchmonth_year'],$this->search_arr['searchmonth_month'])+86400-1;

            $up_month = date('m',$searchtime_arr[0]);
            $curr_month = date('m',$searchtime_arr[1]);
            //计算横轴的最大量（由于每个月的天数不同）
            $up_dayofmonth = date('t',$searchtime_arr[0]);
            $curr_dayofmonth = date('t',$searchtime_arr[1]);
            $x_max = $up_dayofmonth > $curr_dayofmonth ? $up_dayofmonth : $curr_dayofmonth;

            //构造横轴数据
            for($i=1; $i<=$x_max; $i++){
                //统计图数据
                $up_arr[$i] = 0;
                $curr_arr[$i] = 0;
                //统计表数据
                $currlist_arr[$i]['timetext'] = $i;
                $uplist_arr[$i]['val'] = 0;
                $currlist_arr[$i]['val'] = 0;
                //横轴
                $stat_arr['xAxis']['categories'][] = $i;
            }
            $curr_date = date('Y-m-d',$searchtime_arr[1])." 23:59:59";
            $last_date = date('Y-m-d',$searchtime_arr[0])." 00:00:00";
            $where['cdate'] = ['between',[$last_date,$curr_date]];

            $field .= ',MONTH(cdate) as monthval,day(cdate) as dayval ';
            if (C('dbdriver') == 'mysql') {
                $_group = 'monthval,dayval';
            } elseif (C('dbdriver') == 'oracle') {
                $_group = 'MONTH(cdate),day(cdate)';
            }
            $orderlist = $model->getList($where, $field, '', '', '', $_group);
            foreach($orderlist as $k => $v){
                if ($up_month == $v['monthval']){
                    $up_arr[$v['dayval']] = intval($v[$stattype]);
                    $uplist_arr[$v['dayval']]['val'] = intval($v[$stattype]);
                }
                if ($curr_month == $v['monthval']){
                    $curr_arr[$v['dayval']] = intval($v[$stattype]);
                    $currlist_arr[$v['dayval']]['val'] = intval($v[$stattype]);
                }
            }
            $stat_arr['series'][0]['name'] = '上月';
            $stat_arr['series'][0]['data'] = array_values($up_arr);
            $stat_arr['series'][1]['name'] = '本月';
            $stat_arr['series'][1]['data'] = array_values($curr_arr);
        }
        $stat_json = getStatData_LineLabels($stat_arr);
        Tpl::output('stat_json',$stat_json);
        Tpl::output('stattype',$stattype);
        Tpl::showpage('stat.line_chart','null_layout');
    }

}
