<?php
/**
 * 区域代理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class area_agentControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 新增区域代理
     */
    public function indexOp(){
        if (chksubmit()){
            if (intval($_POST['agent_id']) != $this->admin_info['agent_id']){
                showMessage('操作失败');
            }
            $insert_array = [
                'agent_address'=>trim($_POST['agent_address']),
                'edit_time'=>time()
            ];
            //工商营业执照
            if ($_FILES['business_license_image']['name'] != '') {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_MOBILE . '/agent');
                $result = $upload->upfile('business_license_image');
                if ($result) {
                    $insert_array['business_license_image'] = $upload->file_name;
                } else {
                    showMessage($upload->error);
                }
            }
            $conditions = ['agent_id'=>intval($_POST['agent_id'])];
            $result = Model('area_agent')->updateAreaAgent($conditions,$insert_array);
            if ($result){
                showMessage('操作成功');
            }else {
                showMessage('操作失败');
            }
        }
        $agent_info = Model('area_agent')->getAreaAgentInfo(['agent_id'=>intval($this->admin_info['agent_id'])]);
        Tpl::output('agent_info',$agent_info);
        Tpl::showpage('agent_add');
    }

}
