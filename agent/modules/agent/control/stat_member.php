<?php
/**
 * 统计管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class stat_memberControl extends SystemControl{
    private $links = array(
        array('url'=>'act=stat_member&op=newmember','lang'=>'stat_newmember'),
        array('url'=>'act=stat_member&op=artist','lang'=>'stat_artist')
    );

    private $search_arr;//处理后的参数

    public function __construct(){
        parent::__construct();
        Language::read('stat');
        import('function.statistics');
        import('function.datehelper');
        $model = Model('stat');
        //存储参数
        $this->search_arr = $_REQUEST;
        //处理搜索时间
        if (!isset($this->search_arr['op']) || in_array($_REQUEST['op'],array('index','newmember','artist'))){
            $this->search_arr = $model->dealwithSearchTime($this->search_arr);
            //获得系统年份
            $year_arr = getSystemYearArr();
            //获得系统月份
            $month_arr = getSystemMonthArr();
            //获得本月的周时间段
            $week_arr = getMonthWeekArr($this->search_arr['week']['current_year'], $this->search_arr['week']['current_month']);
            Tpl::output('year_arr', $year_arr);
            Tpl::output('month_arr', $month_arr);
            Tpl::output('week_arr', $week_arr);
        }
        Tpl::output('search_arr', $this->search_arr);
    }

    public function indexOp() {
        $this->newmemberOp();
    }

    /**
     * 新增会员
     */
    public function newmemberOp(){
        if(!$this->search_arr['search_type']){
            $this->search_arr['search_type'] = 'day';
        }
        $statlist = array();//统计数据列表
        //新增总数数组
        $count_arr = array('up'=>0,'curr'=>0);
        $where = array();
        $admin_info = $this->admin_info;
        if ($admin_info['area_type'] == 3){
            $where['member_areaid'] = $admin_info['county_id'];
        }elseif ($admin_info['area_type'] == 2){
            $where['member_cityid'] = $admin_info['city_id'];
        }elseif ($admin_info['area_type'] == 1){
            $where['member_provinceid'] = $admin_info['province_id'];
        }

        $field = ' COUNT(*) as allnum ';
        if($this->search_arr['search_type'] == 'day'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_day($where,$field,$statlist);
        }
        elseif ($this->search_arr['search_type'] == 'week'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_week($where,$field,$statlist);
        }
        elseif ($this->search_arr['search_type'] == 'month'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_month($where,$field,$statlist);
        }
        //计算同比
        foreach ((array)$currlist_arr as $k => $v){
            $tmp = array();
            $tmp['timetext'] = $v['timetext'];
            $tmp['seartime'] = $v['stime'].'|'.$v['etime'];
            $tmp['currentdata'] = $v['val'];
            $tmp['updata'] = $uplist_arr[$k]['val'];
            $tmp['tbrate'] = getTb($tmp['updata'], $tmp['currentdata']);
            $statlist['data'][]  = $tmp;
        }
        //计算总结同比
        $count_arr['tbrate'] = getTb($count_arr['up'], $count_arr['curr']);


        //得到统计图数据
        $stat_arr['title'] = '新增用户统计';
        $stat_arr['yAxis'] = '新增用户数';
        $stat_json = getStatData_LineLabels($stat_arr);
        Tpl::output('stat_json',$stat_json);
        Tpl::output('statlist',$statlist);
        Tpl::output('count_arr',$count_arr);
        Tpl::output('top_link',$this->sublink($this->links, 'newmember'));
        Tpl::showpage('stat.member');
    }

    /**
     * 按照天统计
     * @param array $where
     * @param string $field
     * @param array $statlist
     * @param int $stat_type{1:会员2:艺术家}
     * @return array
     */
    private function search_type_day($where,$field,$statlist,$stat_type = 1){
        //构造横轴数据
        for($i=0; $i<24; $i++){
            //统计图数据
            $curr_arr[$i] = 0;//今天
            $up_arr[$i] = 0;//昨天
            //统计表数据
            $currlist_arr[$i]['timetext'] = $i;

            //方便搜索会员列表，计算开始时间和结束时间
            $currlist_arr[$i]['stime'] = $this->search_arr['day']['search_time']+$i*3600;
            $currlist_arr[$i]['etime'] = $currlist_arr[$i]['stime']+3600;

            $uplist_arr[$i]['val'] = 0;
            $currlist_arr[$i]['val'] = 0;
            //横轴
            $stat_arr['xAxis']['categories'][] = "$i";
        }
        $stime = $this->search_arr['day']['search_time'] - 86400;//昨天0点
        $etime = $this->search_arr['day']['search_time'] + 86400 - 1;//今天24点
        //总计的查询时间
        $count_arr = array('up'=>0,'curr'=>0);
        $count_arr['seartime'] = ($stime+86400).'|'.$etime;

        $today_day = @date('d', $this->search_arr['day']['search_time']);//今天日期
        $yesterday_day = @date('d', $stime);//昨天日期

        if ($stat_type == 1){
            $stat_field = 'member_time';
            $stat_method = "statByMember";
        }elseif ($stat_type == 2){
            $stat_field = 'store_time';
            $stat_method = "statByArtist";
        }
        $where[$stat_field] = array('between',array($stime,$etime));
        $field .= ' ,DAY(FROM_UNIXTIME('.$stat_field.')) as dayval,HOUR(FROM_UNIXTIME('.$stat_field.')) as hourval ';
        if (C('dbdriver') == 'mysql') {
            $_group = 'dayval,hourval';
        } elseif (C('dbdriver') == 'oracle') {
            $_group = "DAY(FROM_UNIXTIME('.$stat_field.')),HOUR(FROM_UNIXTIME('.$stat_field.'))";
        }
        $memberlist = Model('stat')->$stat_method($where, $field, 0, '', $_group);
        if($memberlist){
            foreach($memberlist as $k => $v){
                if($today_day == $v['dayval']){
                    $curr_arr[$v['hourval']] = intval($v['allnum']);
                    $currlist_arr[$v['hourval']]['val'] = intval($v['allnum']);
                    $count_arr['curr'] += intval($v['allnum']);
                }
                if($yesterday_day == $v['dayval']){
                    $up_arr[$v['hourval']] = intval($v['allnum']);
                    $uplist_arr[$v['hourval']]['val'] = intval($v['allnum']);
                    $count_arr['up'] += intval($v['allnum']);
                }
            }
        }
        $stat_arr['series'][0]['name'] = '昨天';
        $stat_arr['series'][0]['data'] = array_values($up_arr);
        $stat_arr['series'][1]['name'] = '今天';
        $stat_arr['series'][1]['data'] = array_values($curr_arr);

        //统计数据标题
        $statlist['headertitle'] = array('小时','昨天','今天','同比');

        return [$currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist];
    }

    /**
     * 按照周统计
     * @param array $where
     * @param string $field
     * @param array $statlist
     * @param int $stat_type{1:会员2:艺术家}
     * @return array
     */
    private function search_type_week($where,$field,$statlist,$stat_type = 1){
        $current_weekarr = explode('|', $this->search_arr['week']['current_week']);
        $stime = strtotime($current_weekarr[0])-86400*7;
        $etime = strtotime($current_weekarr[1])+86400-1;
        //总计的查询时间
        $count_arr['seartime'] = ($stime+86400*7).'|'.$etime;

        $up_week = @date('W', $stime);//上周
        $curr_week = @date('W', $etime);//本周

        //构造横轴数据
        for($i=1; $i<=7; $i++){
            //统计图数据
            $up_arr[$i] = 0;
            $curr_arr[$i] = 0;
            $tmp_weekarr = getSystemWeekArr();
            //统计表数据
            $currlist_arr[$i]['timetext'] = $tmp_weekarr[$i];
            //方便搜索会员列表，计算开始时间和结束时间
            $currlist_arr[$i]['stime'] = strtotime($current_weekarr[0])+($i-1)*86400;
            $currlist_arr[$i]['etime'] = $currlist_arr[$i]['stime']+86400 - 1;

            $uplist_arr[$i]['val'] = 0;
            $currlist_arr[$i]['val'] = 0;
            //横轴
            $stat_arr['xAxis']['categories'][] = $tmp_weekarr[$i];
            unset($tmp_weekarr);
        }
        if ($stat_type == 1){
            $stat_field = 'member_time';
            $stat_method = "statByMember";
        }elseif ($stat_type == 2){
            $stat_field = 'store_time';
            $stat_method = "statByArtist";
        }
        $where[$stat_field] = array('between', array($stime,$etime));
        $field .= ',WEEKOFYEAR(FROM_UNIXTIME('.$stat_field.')) as weekval,WEEKDAY(FROM_UNIXTIME('.$stat_field.'))+1 as dayofweekval ';
        if (C('dbdriver') == 'mysql') {
            $_group = 'weekval,dayofweekval';
        } elseif (C('dbdriver') == 'oracle') {
            $_group = 'WEEKOFYEAR(FROM_UNIXTIME('.$stat_field.')),WEEKDAY(FROM_UNIXTIME('.$stat_field.'))+1';
        }
        $memberlist = Model('stat')->$stat_method($where, $field, 0, '', $_group);
        if($memberlist){
            foreach($memberlist as $k => $v){
                if ($up_week == intval($v['weekval'])){
                    $up_arr[$v['dayofweekval']] = intval($v['allnum']);
                    $uplist_arr[$v['dayofweekval']]['val'] = intval($v['allnum']);
                    $count_arr['up'] += intval($v['allnum']);
                }
                if ($curr_week == $v['weekval']){
                    $curr_arr[$v['dayofweekval']] = intval($v['allnum']);
                    $currlist_arr[$v['dayofweekval']]['val'] = intval($v['allnum']);
                    $count_arr['curr'] += intval($v['allnum']);
                }
            }
        }

        $stat_arr['series'][0]['name'] = '上周';
        $stat_arr['series'][0]['data'] = array_values($up_arr);
        $stat_arr['series'][1]['name'] = '本周';
        $stat_arr['series'][1]['data'] = array_values($curr_arr);

        //统计数据标题
        $statlist['headertitle'] = array('星期','上周','本周','同比');

        return [$currlist_arr, $uplist_arr, $count_arr, $stat_arr,$statlist];
    }

    /**
     * 按照月统计
     * @param array $where
     * @param string $field
     * @param array $statlist
     * @param int $stat_type
     * @return array
     */
    private function search_type_month($where,$field,$statlist,$stat_type = 1){
        $stime = strtotime($this->search_arr['month']['current_year'].'-'.$this->search_arr['month']['current_month']."-01 -1 month");
        $etime = getMonthLastDay($this->search_arr['month']['current_year'],$this->search_arr['month']['current_month'])+86400-1;
        //总计的查询时间
        $count_arr['seartime'] = strtotime($this->search_arr['month']['current_year'].'-'.$this->search_arr['month']['current_month']."-01").'|'.$etime;

        $up_month = date('m',$stime);
        $curr_month = date('m',$etime);
        //计算横轴的最大量（由于每个月的天数不同）
        $up_dayofmonth = date('t',$stime);
        $curr_dayofmonth = date('t',$etime);
        $x_max = $up_dayofmonth > $curr_dayofmonth ? $up_dayofmonth : $curr_dayofmonth;

        //构造横轴数据
        for($i=1; $i<=$x_max; $i++){
            //统计图数据
            $up_arr[$i] = 0;
            $curr_arr[$i] = 0;
            //统计表数据
            $currlist_arr[$i]['timetext'] = $i;
            //方便搜索会员列表，计算开始时间和结束时间
            $currlist_arr[$i]['stime'] = strtotime($this->search_arr['month']['current_year'].'-'.$this->search_arr['month']['current_month']."-01")+($i-1)*86400;
            $currlist_arr[$i]['etime'] = $currlist_arr[$i]['stime']+86400 - 1;

            $uplist_arr[$i]['val'] = 0;
            $currlist_arr[$i]['val'] = 0;
            //横轴
            $stat_arr['xAxis']['categories'][] = $i;
            unset($tmp_montharr);
        }
        if ($stat_type == 1){
            $stat_field = 'member_time';
            $stat_method = "statByMember";
        }elseif ($stat_type == 2){
            $stat_field = 'store_time';
            $stat_method = "statByArtist";
        }
        $where[$stat_field] = array('between', array($stime,$etime));
        $field .= ',MONTH(FROM_UNIXTIME('.$stat_field.')) as monthval,day(FROM_UNIXTIME('.$stat_field.')) as dayval ';
        if (C('dbdriver') == 'mysql') {
            $_group = 'monthval,dayval';
        } else if (C('dbdriver') == 'oracle') {
            $_group = 'MONTH(FROM_UNIXTIME('.$stat_field.')),day(FROM_UNIXTIME('.$stat_field.'))';
        }
        $memberlist = Model('stat')->$stat_method($where, $field, 0, '', $_group);
        if($memberlist){
            foreach($memberlist as $k => $v){
                if ($up_month == $v['monthval']){
                    $up_arr[$v['dayval']] = intval($v['allnum']);
                    $uplist_arr[$v['dayval']]['val'] = intval($v['allnum']);
                    $count_arr['up'] += intval($v['allnum']);
                }
                if ($curr_month == $v['monthval']){
                    $curr_arr[$v['dayval']] = intval($v['allnum']);
                    $currlist_arr[$v['dayval']]['val'] = intval($v['allnum']);
                    $count_arr['curr'] += intval($v['allnum']);
                }
            }
        }
        $stat_arr['series'][0]['name'] = '上月';
        $stat_arr['series'][0]['data'] = array_values($up_arr);
        $stat_arr['series'][1]['name'] = '本月';
        $stat_arr['series'][1]['data'] = array_values($curr_arr);
        //统计数据标题
        $statlist['headertitle'] = array('日期','上月','本月','同比');

        return [$currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist];
    }

    /**
     * 新增艺术家
     */
    public function artistOp(){
        if(!$this->search_arr['search_type']){
            $this->search_arr['search_type'] = 'day';
        }
        $statlist = array();//统计数据列表
        //新增总数数组
        $count_arr = array('up'=>0,'curr'=>0);
        $where = array();
        $admin_info = $this->admin_info;
        if ($admin_info['area_type'] == 3){
            $where['area_id'] = $admin_info['county_id'];
        }elseif ($admin_info['area_type'] == 2){
            $where['city_id'] = $admin_info['city_id'];
        }elseif ($admin_info['area_type'] == 1){
            $where['province_id'] = $admin_info['province_id'];
        }

        $field = ' COUNT(*) as allnum ';
        if($this->search_arr['search_type'] == 'day'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_day($where,$field,$statlist,2);
        }
        elseif ($this->search_arr['search_type'] == 'week'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_week($where,$field,$statlist,2);
        }
        elseif ($this->search_arr['search_type'] == 'month'){
            list($currlist_arr, $uplist_arr, $count_arr, $stat_arr, $statlist) = self::search_type_month($where,$field,$statlist,2);
        }
        //计算同比
        foreach ((array)$currlist_arr as $k => $v){
            $tmp = array();
            $tmp['timetext'] = $v['timetext'];
            $tmp['seartime'] = $v['stime'].'|'.$v['etime'];
            $tmp['currentdata'] = $v['val'];
            $tmp['updata'] = $uplist_arr[$k]['val'];
            $tmp['tbrate'] = getTb($tmp['updata'], $tmp['currentdata']);
            $statlist['data'][]  = $tmp;
        }
        //计算总结同比
        $count_arr['tbrate'] = getTb($count_arr['up'], $count_arr['curr']);


        //得到统计图数据
        $stat_arr['title'] = '新增艺术家统计';
        $stat_arr['yAxis'] = '新增艺术家数';
        $stat_json = getStatData_LineLabels($stat_arr);
        Tpl::output('stat_json',$stat_json);
        Tpl::output('statlist',$statlist);
        Tpl::output('count_arr',$count_arr);
        Tpl::output('top_link',$this->sublink($this->links, 'artist'));
        Tpl::showpage('stat.member');
    }

}
