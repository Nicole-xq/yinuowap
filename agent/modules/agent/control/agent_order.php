<?php
/**
 * 统计管理（区域代理返佣分析）
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class agent_orderControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        $query = [
            'end_date'=>date('Y-m-d',time()),
            'start_date'=>date('Y-m-01',time()),
        ];
        Tpl::output('query', $query);
        Tpl::showpage('agent_order');
    }


    /**
     * 输出销售收入统计XML数据
     */
    public function get_income_xmlOp(){
        $county_id = $this->admin_info['county_id'];
        $province_id = $this->admin_info['province_id'];
        $city_id = $this->admin_info['city_id'];
        if (empty($county_id) && $city_id > 0){
            $condition = "city_id = {$city_id} and area_id > 0 and agent_type > 2";
        }elseif (empty($city_id) && $province_id > 0){
            $condition = "province_id = {$province_id} and city_id > 0 and agent_type > 1";
        }else{
            echo Tpl::flexigridXML([]);exit();
        }
        $agent_order_model = Model('agent_order');
        $area_agent_model = Model('area_agent');

        $area_type = $area_agent_model->getAreaType();

        $start_date = trim($_GET['query_start_date']);
        $end_date = trim($_GET['query_end_date']);
        $agent_id = intval($_GET['agent_id']) ;
        if ($start_date && $end_date) {
            $condition .= " and cdate between '".$start_date."' and '".$end_date."'";
        } elseif ($start_date) {
            $condition .= " and cdate >= '".$start_date."' ";
        } elseif ($end_date) {
            $condition .= " and cdate <= '".$end_date."' ";
        }
        $condition .= intval($_GET['agent_id']) ? " and agent_id = ".$agent_id : '';
        $page_rows = intval($_POST['rp']);
        $cur_page = intval($_POST['curpage']);
        if ($page_rows < 1) {
            $page_rows = 10;
        }
        $field = 'agent_id,agent_type';
        $list = $agent_order_model->getAgentListBySQL($condition,$field,$cur_page-1,$page_rows);
        $agent_stat = [];
        foreach ($list as $k => $v){
            $row = $area_agent_model->field('agent_name')->where(array('agent_id' => $v['agent_id']))->find();
            $out_array = [
                'agent_id'=>$v['agent_id'],
                'agent_name'=>$row['agent_name'],
                'agent_type'=>$area_type[$v['agent_type']],
                'sum_order_price_1'=>$v['sum_order_price_1'],
                'sum_commission_amount_1'=>$v['sum_commission_amount_1'],
                'sum_order_price_2'=>$v['sum_order_price_2'],
                'sum_commission_amount_2'=>$v['sum_commission_amount_2'],
                'sum_order_price_3'=>$v['sum_order_price_3'],
                'sum_commission_amount_3'=>$v['sum_commission_amount_3'],
                'sum_order_price_4'=>$v['sum_order_price_4'],
                'sum_commission_amount_4'=>$v['sum_commission_amount_4'],
                'sum_order_price_5'=>$v['sum_order_price_5'],
                'sum_commission_amount_5'=>$v['sum_commission_amount_5'],
            ];
            $agent_stat[$v['agent_id']] = $out_array;
        }
        $total_num_obj = $agent_order_model->getCountNum($condition);
        foreach ($total_num_obj as $key => $value) {
            $total_num = $value;
        }
        $data = [
            'now_page'=>$cur_page,
            'total_num'=>$total_num['nums'],
            'list'=>$agent_stat
        ];

        echo Tpl::flexigridXML($data);exit();
    }


}
