<?php
/**
 * 对账单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class statementControl extends SystemControl{
    public $state_arr = array(
        '0'=>'平台审核中',
        '1'=>'代理审核中',
        '2'=>'待打款',
        '3'=>'代理审核异常',
        '4'=>'已打款',
        '5'=>'已完成'
    );
    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('statement_list');
    }

    /**
     * 列表数据XML
     */
    public function get_xmlOp(){
        $model_statement = Model('agent_statement');
        $condition = array();
        $condition['agent_id'] = $this->admin_info['agent_id'];
        $condition['state'] = array('in','1,2,3,4,5,6');
        $data = array();
        $statement_list = $model_statement->getList($condition);
//        dd();
        $data['now_page'] = $model_statement->shownowpage();
        $data['total_num'] = $model_statement->gettotalnum();
        foreach ($statement_list as $value) {
            $param = array();
            if($value['state'] == 1){
                //对账
                $param['operation'] = "<a class='btn blue' href='index.php?act=statement&op=statement_info&id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>开始对账</a>";
            }elseif($value['state'] == 4){
                //确认收款
                $param['operation'] = "<a class='btn blue' href='javascript:updateInfo({$value['id']},5)'><i class='fa fa-pencil-square-o'></i>确认收款</a>";
            }else{
                $param['operation'] = "";
            }
            $param['id'] = $value['id'];
            $param['title'] = $value['title'];
            $param['statement_no'] = $value['statement_no'];
            $param['total_amount'] = $value['total_amount'];
            $param['cdate'] = $value['cdate'];
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    public function statement_infoOp(){
        $statement_id = intval($_GET['id']);
        if ($statement_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $agent_statement = Model('agent_statement');
        $statement_info = $agent_statement->getInfo(array('id'=>$statement_id));
        if (!$statement_info){
            showMessage('参数错误','','html','error');
        }
        $statement_info['state_str'] = $this->state_arr[$statement_info['state']];
        Tpl::output('statement_info',$statement_info);
        Tpl::showpage('statement_info');
    }

    public function update_statementOp(){
        $model_statement = Model('agent_statement');
        $id = $_POST['id'];
        $type = $_POST['type'];
        $data = array(
            'state'=>$type,
            'udate'=>date('Y-m-d H:i:s')
        );
        if($type == 3){
            $data['comment'] = $_POST['comment'];
        }
        $re = $model_statement->updateInfo(array('id'=>$id,'agent_id'=>$this->admin_info['agent_id']),$data);
        echo $re;exit;
    }

}
