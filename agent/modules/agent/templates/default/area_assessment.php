<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=area_agent" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>个人中心 - 考核指标</h3>
                <h5></h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>可从管理平台查看区域代理考核相关信息。</li>
        </ul>
    </div>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agent_name">代理名称 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['agent_name'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_address">代理区域 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['area_info'];?></span>
                </dd>
            </dl>
            <div class="title">
                <h3>当月保证金-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['curr_bzj_content'] as $k=>$v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" value="<?=$v['index']?>" class="input-txt valid" style="width:100px !important;" readonly />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" value="<?=$v['rate']?>" class="input-txt valid" style="width:30px !important;" readonly />%
                    </dd>
                </dl>
            <?php }?>
            <div class="title">
                <h3>当月订单-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['curr_goods_content'] as $v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" value="<?=$v['index']?>" class="input-txt valid" style="width:100px !important;" readonly />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" value="<?=$v['rate']?>" class="input-txt valid" style="width:30px !important;" readonly />%
                    </dd>
                </dl>
            <?php }?>
            <div class="title">
                <h3>当月艺术家-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['curr_artist_content'] as $v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" value="<?=$v['index']?>" style="width:100px !important;" readonly />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" value="<?=$v['rate']?>" style="width:30px !important;" readonly />%
                    </dd>
                </dl>
            <?php }?>
        </div>
</div>
