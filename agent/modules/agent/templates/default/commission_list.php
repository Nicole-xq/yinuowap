<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>返佣记录</h3>
                <h5>区域用户返佣记录 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>订单类型过滤,请填写对应数字</li>
            <li>1:普通商品购买返佣,2:拍卖商品返佣,3:保证金返佣,4:合伙人升级,5:艺术家入驻</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=commission_list&op=get_xml',
            colModel : [
                {display: 'ID', name : 'id', width : 80, sortable : true, align: 'center'},
                {display: '订单类型', name : 'type', width : 100, align: 'center'},
                {display: '返佣比率', name : 'rate_num', width : 100, align: 'center'},
                {display: '订单金额', name : 'order_price', width : 80, align: 'center'},
                {display: '描述', name : 'description', width : 200, align: 'center'},
                {display: '返佣金额', name : 'commission_amount', width : 80, align: 'center'},
                {display: '返佣状态', name : 'commission_status', width : 80, align: 'center'},
                {display: '结算时间', name : 'finish_time', width : 120, sortable : true, align: 'center'},
                {display: '创建时间', name : 'cdate', width : 120, sortable : true, align: 'center'},
            ],
            buttons : [
//                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],
            searchitems : [
                {display: '订单类型', name : 'commission_type'},
            ],
            selectitems:[],
            sortname: "id",
            sortorder: "desc",
            title: '返佣记录'
        });

    });


    // 更新代理状态
//    function fg_change_state(id) {
//        $.getJSON('index.php?act=area_agent&op=change_agent_state', {id:id}, function(data){
//            if (data.state) {
//                showSucc(data.msg);
//                $("#flexigrid").flexReload();
//            } else {
//                showError(data.msg);
//            }
//        });
//    }
</script>
