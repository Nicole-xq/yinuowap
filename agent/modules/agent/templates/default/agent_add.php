<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=area_agent" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>个人中心 - 信息展示</h3>
                <h5>查看个人信息，编辑</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>可从管理平台手动编辑区域代理相关信息。</li>
            <li>标识“*”的选项为必填项，其余为选填项。</li>
        </ul>
    </div>
    <form id="agent_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" value="<?php echo $output['agent_info']['agent_id'];?>" name="agent_id" id="agent_id">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agent_name"><em>*</em>代理名称 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['agent_name'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_address"><em>*</em>代理区域 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['area_info'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="linkman_name"><em>*</em>联系人名称 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['linkman_name'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="linkman_mobile"><em>*</em>联系人手机号 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['linkman_mobile'];?></span>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_address">联系人地址 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['agent_address'];?>" id="agent_address" name="agent_address" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>代理类型 :</label>
                </dt>
                <dd class="opt">
                    <?php if($output['agent_info']['agent_type'] == 1){ ?>
                     <span>自然人</span>
                    <?php } ?>
                    <?php if($output['agent_info']['agent_type'] == 2){ ?>
                        企业法人
                    <?php } ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="bank_householder">银行卡-户主 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['bank_householder'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="bank_number">银行卡-账号 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['bank_number'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="bank_accounts_address">银行卡-支行 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['bank_accounts_address'];?></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="identity_card_number">身份证号 :</label>
                </dt>
                <dd class="opt">
                    <span><?php echo $output['agent_info']['identity_card_number'];?></span>
                </dd>
            </dl>
            <dl class="row" id="bl_image" <?php if($output['agent_info']['agent_type'] == 1) {?> style="display: none;" <?php } ?>     >
                <dt class="tit">
                    <label for="business_license_image">营业执照 :</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show">
                <span class="show">
                    <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>">
                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>>')" onMouseOut="toolTip()"></i>
                    </a>
                </span>
                        <span class="type-file-box">
                  <input name="business_license_image" type="file" class="type-file-file" id="business_license_image" size="30" hidefocus="true" value="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>">
                </span>
                    </div>
                    <span class="err"></span>
                    <p class="notic">展示图片，建议大小640x640像素</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">

    $(function(){
        //图片上传
        var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
        $(textButton).insertBefore("#business_license_image");
        $("#business_license_image").change(function(){
            $("#textfield1").val($("#business_license_image").val());
        });
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
                $("#agent_form").submit();
        });
    });
</script>
