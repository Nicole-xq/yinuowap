<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>对账单</h3>
                <h5>区域代理月度对账单 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>订单类型过滤,请填写对应数字</li>
            <li>1:普通商品购买返佣,2:拍卖商品返佣,3:保证金返佣,4:合伙人升级,5:艺术家入驻</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=statement&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 120, sortable : false, align: 'center', className: 'handle'},
                {display: 'ID', name : 'id', width : 80, sortable : true, align: 'center'},
                {display: '标题', name : 'title', width : 200, align: 'center'},
                {display: '对账单号', name : 'statement_no', width : 150, align: 'center'},
                {display: '总金额(元)', name : 'total_amount', width : 80, align: 'center'},
                {display: '创建时间', name : 'cdate', width : 120, sortable : true, align: 'center'},
            ],
            buttons : [
//                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],
            selectitems:[],
            sortname: "id",
            sortorder: "desc",
            title: '对账单'
        });

    });
    function updateInfo(id,type){
    var url='index.php?act=statement&op=update_statement';
    $.post(url,{'id':id,'type':type},function(re){
        if(re == 1){
            showSucc('成功!');
            location.reload();
        }else{
            showError('失败!');
            location.reload();
        }
    },'json');
    }
</script>
