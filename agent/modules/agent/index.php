<?php
/**
 * 区域代理板块初始化文件
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

define('BASE_PATH',str_replace('\\','/',dirname(dirname(dirname(__FILE__)))));
define('MODULES_BASE_PATH',str_replace('\\','/',dirname(__FILE__)));
require __DIR__ . '/../../../shopnc.php';

define('AGENT_SITE_URL', C('agent_site_url'));
define('APP_SITE_URL', AGENT_SITE_URL.'/modules/agent');
define('TPL_NAME',TPL_ADMIN_NAME);
define('ADMIN_TEMPLATES_URL',AGENT_SITE_URL.'/templates/'.TPL_NAME);
define('ADMIN_RESOURCE_URL',AGENT_SITE_URL.'/resource');
define('SHOP_TEMPLATES_URL',SHOP_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',MODULES_BASE_PATH.'/templates/'.TPL_NAME);
define('MODULE_NAME', 'agent');

Shopnc\Core::runApplication(MODULES_BASE_PATH);
