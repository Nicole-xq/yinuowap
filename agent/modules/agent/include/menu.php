<?php
/**
 * 菜单
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
$_menu['agent'] = array (
        'name' => '个人中心',
        'child' => array(
                array(
                        'name' => $lang['nc_config'], 
                        'child' => array(
                                'area_agent' => '信息展示',
                                'area_assessment' => '考核指标',
                                'commission_list' => '返佣记录',
                                'statement'=>'对账单'
                        )
                ),
                array(
                    'name' => $lang['nc_stat'],
                    'child' => array(
                        'stat_member' => '会员统计',
                        'stat_trade' => '销量统计',
                        'agent_order' => '下级代理统计',
                        )
                )
        )
);