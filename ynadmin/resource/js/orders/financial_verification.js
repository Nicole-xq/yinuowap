function fv_predeposit(data) {
    if(data.pdr_id > 0){
        var url = 'index.php?act=predeposit&op=edit_order_state';
        fv_edit(url,data);
    }
}
function fv_margin_orders(data) {
    if(data.margin_id > 0){
        var url = 'index.php?act=margin_order&op=edit_order_state';
        fv_edit(url,data);
    }
}
function fv_edit(url,data) {
    if(confirm('确认将此订单标记为'+data.msg+'吗？')){
        $.post(url, data, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg);
            }
        },'json');
    }
}