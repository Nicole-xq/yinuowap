$(document).ready(function(){
    var d;
    var image_object;
	$('a[nctype="nyroModal"]').nyroModal();
    //文件上传
    var textButton1="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />";
    $(textButton1).insertBefore("#special_image");
    $("#special_image").change(function(){
        $("#textfield1").val($("#special_image").val());
    });

    var textButton2="<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button' id='button2' value='选择上传...' class='type-file-button' />";
    $(textButton2).insertBefore("#special_background");
    $("#special_background").change(function(){
        $("#textfield2").val($("#special_background").val());
    });
    var textButton3="<input type='text' name='textfield' id='textfield3' class='type-file-text' /><input type='button' name='button' id='button3' value='选择上传...' class='type-file-button' />";
    $(textButton3).insertBefore("#special_share_logo");
    $("#special_share_logo").change(function(){
        $("#textfield3").val($("#special_share_logo").val());
    });
    $("input[nctype='cms_image']").live("change", function(){
		var src = getFullPath($(this)[0]);
		$(this).parent().prev().find('.low_source').attr('src',src);
		$(this).parent().find('input[class="type-file-text"]').val($(this).val());
	});


    $('#add_album').fileupload({
        dataType: 'json',
        url: 'index.php?act=cms_special&op=special_image_upload&type=add_album',
        formData: {name:'add_album'},
        add: function (e,data) {
            data.submit();
        },
        done: function (e,data) {
            // console.log(data);
            result = data.result;
            KE.appendHtml('#special_content', '<img src="'+ result.file_url + '">');
            // insert_editor(result.file_url);
        }
    });
    $('#add_album2').fileupload({
        dataType: 'json',
        url: 'index.php?act=cms_special&op=special_image_upload&type=add_album2',
        formData: {name:'add_album2'},
        add: function (e,data) {
            data.submit();
        },
        done: function (e,data) {
            // console.log(data);
            result = data.result;
            KE.appendHtml('#special_mobile_content', '<img src="'+ result.file_url + '">');
            // insert_editor(result.file_url);
        }
    });


    $('#mobile_add_album').fileupload({
        dataType: 'json',
        url: 'index.php?act=cms_special&op=special_image_upload&type=mobile_add_album',
        formData: {name:'mobile_add_album'},
        add: function (e,data) {
            data.submit();
        },
        done: function (e,data) {
            console.log(data);
            result = data.result;
            insert_mobile_img(result.file_url);
        }
    });
    insert_mobile_img = function(data){
        _data = new Object;
        _data.type = 'image';
        _data.value = data;
        _rs = mDataInsert(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-image"></div>')
            .append('<div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_rpl" href="javascript:void(0);">替换</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>')
            .append('<div class="content"><div class="image-div"><img src="' + data + '"></div></div>')
            .append('<div class="cover"></div>').appendTo('div[nctype="mobile_pannel"]');

    }
    // 插入数据
    mDataInsert = function(data){
        _m_data = mDataGet();
        _m_data.push(data);
        return mDataSet(_m_data);
    }
    // 数据移动
    // type 0上移  1下移
    mDataMove = function(index, type) {
        _m_data = mDataGet();
        _data = _m_data.splice(index, 1);
        if (type) {
            index += 1;
        } else {
            index -= 1;
        }
        _m_data.splice(index, 0, _data[0]);
        return mDataSet(_m_data);
    }
    // 数据移除
    mDataRemove = function(index){
        _m_data = mDataGet();
        _m_data.splice(index, 1);     // 删除数据
        return mDataSet(_m_data);
    }
    // 替换数据
    mDataReplace = function(index, data){
        _m_data = mDataGet();
        _m_data.splice(index, 1, data);
        return mDataSet(_m_data);
    }
    // 获取数据
    mDataGet = function(){
        _m_body = $('input[name="m_body"]').val();
        if (_m_body == '' || _m_body == 'false') {
            var _m_data = new Array;
        } else {
            eval('var _m_data = ' + _m_body);
        }
        return _m_data;
    }
    // 设置数据
    mDataSet = function(data){
        var _i_c = 0;
        var _i_c_m = 20;
        var _t_c = 0;
        var _t_c_m = 5000;
        var _sign = true;
        $.each(data, function(i, n){
            if (n.type == 'image') {
                _i_c += 1;
                if (_i_c > _i_c_m) {
                    alert('只能选择'+_i_c_m+'张图片');
                    _sign = false;
                    return false;
                }
            } else if (n.type == 'text') {
                _t_c += n.value.length;
                if (_t_c > _t_c_m) {
                    alert('只能输入'+_t_c_m+'个字符');
                    _sign = false;
                    return false;
                }
            }
        });
        if (!_sign) {
            return false;
        }
        $('span[nctype="img_count_tip"]').html('还可以选择图片<em>' + (_i_c_m - _i_c) + '</em>张');
        $('span[nctype="txt_count_tip"]').html('还可以输入<em>' + (_t_c_m - _t_c) + '</em>字');
        _data = JSON.stringify(data);
        $('input[name="m_body"]').val(_data);
        return true;
    }
    // 转码
    toTxt = function(str) {
        var RexStr = /\<|\>|\"|\'|\&|\\/g
        str = str.replace(RexStr, function(MatchStr) {
            switch (MatchStr) {
            case "<":
                return "";
                break;
            case ">":
                return "";
                break;
            case "\"":
                return "";
                break;
            case "'":
                return "";
                break;
            case "&":
                return "";
                break;
            case "\\":
                return "";
                break;
            default:
                break;
            }
        })
        return str;
    }
    // 添加文字按钮，显示文字输入框
    $('a[nctype="mb_add_txt"]').click(function(){
        console.log(3333);
        $('div[nctype="mea_txt"]').show();
        $('a[nctype="meai_cancel"]').click();
        $('div[nctype="mobile_editor_area"]').find('textarea[nctype="meat_content"]').focus();
    });
    $('div[nctype="mobile_editor_area"]').find('textarea[nctype="meat_content"]').unbind().charCount({
        allowed: 500,
        warning: 50,
        counterContainerID: 'meat_content_count',
        firstCounterText:   '还可以输入',
        endCounterText:     '字',
        errorCounterText:   '已经超出'
    });
    // 关闭 文字输入框按钮
    $('a[nctype="meat_cancel"]').click(function(){
        $(this).parents('div[nctype="mea_txt"]').find('textarea[nctype="meat_content"]').val('').end().hide();
    });
    // 提交 文字输入框按钮
    $('a[nctype="meat_submit"]').click(function(){
        var _c = toTxt($('textarea[nctype="meat_content"]').val().replace(/[\r\n]/g,''));
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataInsert(_data);
        if (!_rs) {
            return false;
        }
        $('<div class="module m-text"></div>')
            .append('<div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_edit" href="javascript:void(0);">编辑</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>').appendTo('div[nctype="mobile_pannel"]');

        $('a[nctype="meat_cancel"]').click();
    });
    /* 插入编辑器 */
function insert_editor(file_path) {
    KE.appendHtml('special_content', '<img src="'+ file_path + '">');
}
// 初始化控制面板
    mbPannelInit = function(){
        $('div[nctype="mobile_pannel"]')
            .find('a[nctype^="mp_"]').show().end()
            .find('.module')
            .first().find('a[nctype="mp_up"]').hide().end().end()
            .last().find('a[nctype="mp_down"]').hide();
    }
// 显示隐藏控制面板
    $('div[nctype="mobile_pannel"]').on('click', '.module', function(){
        mbPannelInit();
        $(this).siblings().removeClass('current').end().addClass('current');
    });
    // 上移
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="mp_up"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove(_parents.index(), 0);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertBefore(_parents.prev()).end().remove();
        mbPannelInit();
    });
    // 下移
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="mp_down"]', function(){
        var _parents = $(this).parents('.module:first');
        _rs = mDataMove(_parents.index(), 1);
        if (!_rs) {
            return false;
        }
        _parents.clone().insertAfter(_parents.next()).end().remove();
        mbPannelInit();
    });
    // 删除
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="mp_del"]', function(){
        var _parents = $(this).parents('.module:first');
        mDataRemove(_parents.index());
        _parents.remove();
        mbPannelInit();
    });
    // 编辑
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="mp_edit"]', function(){
        $('a[nctype="meat_cancel"]').click();
        var _parents = $(this).parents('.module:first');
        var _val = _parents.find('.text-div').html();
        $(this).parents('.module:first').html('')
            .append('<div class="content"></div>').find('.content')
            .append('<div class="ncsc-mea-text" nctype="mea_txt"></div>')
            .find('div[nctype="mea_txt"]')
            .append('<p id="meat_content_count" class="text-tip">')
            .append('<textarea class="textarea valid" data-old="' + _val + '" nctype="meat_content">' + _val + '</textarea>')
            .append('<div class="button"><a class="ncsc-btn ncsc-btn-blue" nctype="meat_edit_submit" href="javascript:void(0);">确认</a><a class="ncsc-btn ml10" nctype="meat_edit_cancel" href="javascript:void(0);">取消</a></div>')
            .append('<a class="text-close" nctype="meat_edit_cancel" href="javascript:void(0);">X</a>')
            .find('#meat_content_count').html('').end()
            .find('textarea[nctype="meat_content"]').unbind().charCount({
                allowed: 500,
                warning: 50,
                counterContainerID: 'meat_content_count',
                firstCounterText:   '还可以输入',
                endCounterText:     '字',
                errorCounterText:   '已经超出'
            });
    });
    // 编辑提交
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="meat_edit_submit"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = toTxt(_parents.find('textarea[nctype="meat_content"]').val().replace(/[\r\n]/g,''));
        var _cl = _c.length;
        if (_cl == 0 || _cl > 500) {
            return false;
        }
        _data = new Object;
        _data.type = 'text';
        _data.value = _c;
        _rs = mDataReplace(_parents.index(), _data);
        if (!_rs) {
            return false;
        }
        _parents.html('').append('<div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_edit" href="javascript:void(0);">编辑</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>')
            .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
            .append('<div class="cover"></div>');

    });
    // 编辑关闭
    $('div[nctype="mobile_pannel"]').on('click', '[nctype="meat_edit_cancel"]', function(){
        var _parents = $(this).parents('.module:first');
        var _c = _parents.find('textarea[nctype="meat_content"]').attr('data-old');
        _parents.html('').append('<div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_edit" href="javascript:void(0);">编辑</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>')
        .append('<div class="content"><div class="text-div">' + _c + '</div></div>')
        .append('<div class="cover"></div>');
    });
    //图片上传
    $("#picture_image_upload").fileupload({
        dataType: 'json',
            url: "index.php?act=cms_special&op=special_image_upload&type=special_image_upload",
            add: function(e,data) {
            console.log(data);
                $.each(data.files, function(index, file) {
					var image_content = '<li id=' + file.name.replace('.', '_') + ' class="picture">';
                    image_content += '<div class="size-64x64"><span class="thumb size-64x64"><i></i><img src="' + LOADING_IMAGE + '" alt="" /></span></div>';
                    image_content += '<p class="handle">';
                    image_content += '</p>';

                    $("#special_image_list").append(image_content);
                });
                data.submit();
            },
            done: function (e,data) {
                result = data.result;
                var $image_box = $('#' + result.origin_file_name.replace('.', '_'));
                if(result.status == "success") {
                    $image_box.find('img').attr('src', result.file_url);

                    var $image_handle = $image_box.find('.handle');
					var image_handle = '<a nctype="btn_show_image_insert_link" image_url="'+result.file_url+'" class="insert-link tooltip" title="以图片链接模式插入专题页">&nbsp;</a>';
                    image_handle += '<a nctype="btn_show_image_insert_hot_point" image_name="'+result.file_name+'" image_url="'+result.file_url+'" class="insert-hotpoint tooltip " title="以热点链接模式插入专题页">&nbsp;</a>';
                    image_handle += '<a nctype="btn_drop_special_image" image_name="'+result.file_name+'" class="delete tooltip " title="删除该图片">&nbsp;</a>';
                    $image_handle.html(image_handle);

                    var image_hidden = '<input name="special_image_all[]" type="hidden" value="'+result.file_name+'" />';
                    $image_handle.after(image_hidden);

                    $image_box.attr('id', '');
                } else {
                    $image_box.remove();
                    alert(result.error);
                }
            }
    });

    //图片删除
    $("[nctype='btn_drop_special_image']").live("click",function(){
        var image_object = $(this).parents("li");
        var image_name = $(this).attr("image_name");
        $.getJSON("index.php?act=cms_special&op=special_image_drop", { image_name: image_name }, function(result){
                if(result.status == "success") {
                    image_object.remove();
                } else {
                    showError(result.error);
                }
        });
    });

    //插入图片链接对话框
    $("[nctype='btn_show_image_insert_link']").live('click', function(){
        $("#_dialog_image_insert_link").find("img").attr("src", $(this).attr("image_url"));
        html_form('dialog_image_insert_link', '图片链接', $("#_dialog_image_insert_link").html(), 640);
    });

    //插入图片链接
    $("[nctype='btn_image_insert_link']").live('click', function(){
        var html = $("#special_content").val();
        var item = $(this).parents("div");
        var link = item.find("[nctype='_image_insert_link']").val();
        html += "<div class='special-content-link'>";
        if(link != "") {
            html += "<a href=" + link +" target='_blank'>";
        }
        html += $("<div />").append(item.find("img").clone()).html(); 
        if(link != "") {
            html += "</a>";
        }
        html += "</div>";
        $("#special_content").val(html);
        special_view_update();
        DialogManager.close("dialog_image_insert_link");
    });

    //插入热点图
    $("[nctype='btn_show_image_insert_hot_point']").live('click', function(){
        $("#_dialog_image_insert_hot_point").find("img").attr("src", $(this).attr("image_url"));
        d = html_form('dialog_image_insert_hot_point', '图片热点', $("#_dialog_image_insert_hot_point").html(), 1040);
        var count = 0;
        
        var dialog_object = $("#fwin_dialog_image_insert_hot_point");
        var filename = $(this).attr("image_name").replace(".","");
        image_object = dialog_object.find("[nctype='img_hot_point']");
        var x1 = dialog_object.find("[nctype='x1']");
        var y1 = dialog_object.find("[nctype='y1']");
        var x2 = dialog_object.find("[nctype='x2']");
        var y2 = dialog_object.find("[nctype='y2']");
        var w = dialog_object.find("[nctype='w']");
        var h = dialog_object.find("[nctype='h']");
        var url = dialog_object.find("[nctype='url']");

        image_object.imgAreaSelect({ 
            handles: true,
                fadeSpeed: 200, 
                onSelectChange: preview 
        });

        image_object.attr("usemap","#"+filename);
        image_object.after('<map id="'+filename+'" name="'+filename+'"></map>');

        $("[nctype='delete_hot_point']").live("click",function(){
            var key = $(this).attr("name");
            $("#"+key).remove();
            $("."+key).remove();
        });

        $("[nctype='select_hot_point']").live("click",function(){
            var xy = $(this).attr("name");
            var xyarray = xy.split(",");
            var ias = image_object.imgAreaSelect({ instance: true });
            ias.setSelection(xyarray[0],xyarray[1],xyarray[2],xyarray[3],true);
            ias.setOptions({ show: true });
            ias.update();
        });

        dialog_object.find("[nctype='btn_hot_point_commit']").click(function(){
            count++;
            var key = filename + count;
            var xy = x1.val()+','+y1.val()+','+x2.val()+','+y2.val()+',';
            image_object.parent().find("map").append('<area id='+key+' shape="rect" coords="'+xy+'" href="'+url.val()+'" target="_blank" />');
            dialog_object.find("[nctype='list']").append('<li class="'+key+'">热点区域'+count+'<span>('+url.val()+')</span><a nctype="select_hot_point" name="'+xy+'" href="javascript:void(0);" class="ncap-btn ncap-btn-blue mr5"><i class="fa fa-location-arrow"></i>选中</a><a nctype="delete_hot_point" class="ncap-btn ncap-btn-red" name="'+key+'" href="javascript:void(0);"><i class="fa fa-trash"></i>删除</a></li>');
            image_object.after('<div class='+key+' style="width:'+w.val()+'px;height:'+h.val()+'px;position:absolute;left:'+x1.val()+'px;top:'+y1.val()+'px;border:1px solid #cccccc;">'+count+'</div>');
            url.val('');
            var ias = image_object.imgAreaSelect({ instance: true });
            ias.cancelSelection();
        });

        $(".dialog_close_button").unbind().bind("click", function() {
            var ias = image_object.imgAreaSelect({ instance: true });
            ias.cancelSelection();
            DialogManager.close("dialog_image_insert_hot_point");
        });
    });

    //插入图片链接
    $("[nctype='btn_image_insert_hot_point']").live('click', function(){
        var html = $("#special_content").val();
        var hot_point = $(this).parents("div").find(".special-hot-point").clone(); 
        hot_point.find("div").remove();
        hot_point = $("<div />").append(hot_point).html(); 
        html += hot_point;
        $("#special_content").val(html);
        special_view_update();
        var ias = image_object.imgAreaSelect({ instance: true });
        ias.cancelSelection();
        DialogManager.close("dialog_image_insert_hot_point");
    });

    //图片删除
    $("[nctype='btn_drop_special_image']").click(function() {
        $(this).parents(".picture").remove();
    });

    //插入商品对话框
    $("#btn_show_special_insert_goods").click(function(){
        html_form('dialog_special_insert_goods', '插入商品', $("#_dialog_special_insert_goods").html(), 640);
    });

    //选择商品
    $("[nctype='btn_special_goods']").live('click', function(){
        var goods_list = $(this).parents("div").find("[nctype='_special_goods_list']");
        var link_item = $(this).parents("div").find("[nctype='_input_goods_link']");
        var link = link_item.val();
        link_item.val('');
        if(link != '') {
        var url = encodeURIComponent(link);
        $.getJSON("index.php?act=cms_special&op=goods_info_by_url", { url: url}, function(data){
            if(data.result == "true") {
                var temp = '<li nctype="btn_goods_select"><dl>'; 
                temp += '<dt class="name"><a href="'+data.url+'" target="_blank">'+data.title+'</a></dt>';
                temp += '<dd class="image"><a href="'+data.url+'" target="_blank"><img title="'+data.title+'" src="'+data.image+'" /></a></dd>';
                temp += '<dd class="price">价格：<em>'+data.price+'</em></dd>';
                temp += '<dd class="taobao-item-delete" nctype="btn_special_goods_delete" title="删除添加的商品">&nbsp;</dd>';
                temp += '</dl></li>';
                $(goods_list).append(temp);
            } else {
                alert(data.message);
            }
        });
        }
    });

    //删除商品
    $("[nctype='btn_special_goods_delete']").live('click', function(){
        $(this).parent().parent().remove();
    });

    //插入商品列表
    $("[nctype='btn_special_insert_goods']").live('click', function(){
        var html = $("#special_content").val();
        html += "<div class='special-content-goods-list'>";
        var goods_list = $("<div />").append($(this).parents("div").find("[nctype='_special_goods_list']").clone());
        goods_list.find("i").remove();
        goods_list.find("[nctype='btn_special_goods_delete']").remove();
        html += goods_list.html(); 
        html += "</div>";
        $("#special_content").val(html);
        special_view_update();
        DialogManager.close("dialog_special_insert_goods");
    });

    //编辑模式
    $("#btn_content_edit").click(function(){
        $(this).attr("class", "current");
        $("#btn_content_view").attr("class", "");
        $("#div_content_edit").show();
        $("#div_content_view").hide();
    });

    //预览模式
    $("#btn_content_view").click(function(){
        $(this).attr("class", "current");
        $("#btn_content_edit").attr("class", "");
        $("#div_content_edit").hide();
        $("#div_content_view").show();
    });

    //更新预览窗口
    $("#special_content").change(function(){
        special_view_update();
    });

    function special_view_update() {
        $("#div_content_view").html($("#special_content").val());
    }

    special_view_update();

});

//选取相关
function preview(img, selection) {
    if (!selection.width || !selection.height)
        return;
    var scaleX = 100 / selection.width;
    var scaleY = 100 / selection.height;
    $('#preview img').css({
        width: Math.round(scaleX * 300),
            height: Math.round(scaleY * 300),
            marginLeft: -Math.round(scaleX * selection.x1),
            marginTop: -Math.round(scaleY * selection.y1)
    });
    var dialog_object = $("#fwin_dialog_image_insert_hot_point");
    dialog_object.find("[nctype='x1']").val(selection.x1);
    dialog_object.find("[nctype='y1']").val(selection.y1);
    dialog_object.find("[nctype='x2']").val(selection.x2);
    dialog_object.find("[nctype='y2']").val(selection.y2);
    dialog_object.find("[nctype='w']").val(selection.width);
    dialog_object.find("[nctype='h']").val(selection.height);
}

