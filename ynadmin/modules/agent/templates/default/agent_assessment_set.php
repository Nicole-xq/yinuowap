<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    #flexigrid tr div{height:100%;text-align:center;margin:0 auto;}
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>区域代理考核配置管理</h3>
                <h5>网站所有区域代理考核配置索引及管理 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>通过区域代理考核配置管理，你可以进行查看、编辑代理资料等操作</li>
            <li>你可以根据条件搜索考核配置，然后选择相应的操作</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=agent_assessment_set&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 20, sortable : false, align: 'center', className: 'handle'},
                {display: '考核配置名称', name : 'assessment_set_name', width : 100, sortable : true, align: 'left'},
                {display: '区域类型', name : 'agent_type', width : 50, align: 'center'},
                {display: '上月保证金', name : 'last_bzj_content', width : 150, sortable : true, align: 'center'},
                {display: '上月订单', name : 'last_goods_content', width : 150, sortable : true, align: 'center'},
                {display: '上月艺术家', name : 'last_artist_content', width : 150, sortable : true, align: 'center'},
                {display: '当月保证金', name : 'curr_bzj_content', width : 150, sortable : true, align: 'center'},
                {display: '当月订单', name : 'curr_goods_content', width : 150, sortable : true, align: 'center'},
                {display: '当月艺术家', name : 'curr_artist_content', width : 150, sortable : true, align: 'center'},
                {display: '编辑时间', name : 'edit_time', width : 80, sortable : true, align: 'center'}
            ],
            /*buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],*/
            searchitems : [
                {display: '考核ID', name : 'id'},
                {display: '考核配置名称', name : 'assessment_set_name'}
            ],
            sortname: "id",
            sortorder: "desc",
            title: '区域代理考核配置列表'
        });

    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=agent_assessment_set&op=agent_add_assessment';
        }
    }
</script>
