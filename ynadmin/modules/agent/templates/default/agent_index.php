<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>区域代理管理</h3>
                <h5>网站所有区域代理索引及管理 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>通过区域代理管理，你可以进行查看、编辑代理资料等操作</li>
            <li>你可以根据条件搜索代理，然后选择相应的操作</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=area_agent&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 20, sortable : false, align: 'center', className: 'handle'},
                {display: '代理ID', name : 'agent_id', width : 50, sortable : true, align: 'center'},
                {display: '区域代理名称', name : 'agent_name', width : 100, sortable : true, align: 'left'},
                {display: '联系人名称', name : 'linkman_name', width : 80, align: 'left'},
                {display: '联系人手机号', name : 'linkman_mobile', width : 100, align: 'center'},
                {display: '代理账号', name : 'agent_account', width : 80, align: 'center'},
                {display: '代理类型', name : 'agent_type', width : 80, align: 'center'},
                {display: '区域类型', name : 'area_type', width : 80, align: 'center'},
                {display: '代理区域', name : 'area_info', width : 200, align: 'center'},
                {display: '代理状态', name : 'agent_state', width : 80, align: 'center'},
                {display: '编辑时间', name : 'edit_time', width : 80, sortable : true, align: 'center'},
                {display: '创建时间', name : 'reg_time', width : 80, sortable : true, align: 'center'},
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],
            searchitems : [
                {display: '区域代理ID', name : 'agent_id'},
                {display: '区域代理名称', name : 'agent_name'},
                {display: '区域名称', name : 'area_info'}
            ],
            sortname: "agent_id",
            sortorder: "desc",
            title: '区域代理列表'
        });

    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=area_agent&op=agent_add';
        }
    }

    // 更新代理状态
    function fg_change_state(id) {
        $.getJSON('index.php?act=area_agent&op=change_agent_state', {id:id}, function(data){
            if (data.state) {
                showSucc(data.msg);
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg);
            }
        });
    }
</script>
