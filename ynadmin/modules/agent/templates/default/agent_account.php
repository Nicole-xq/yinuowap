<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="eject_con">
  <?php if ($output['error']) {?>
  <div class="error">参数错误</div>
  <?php } else {?>
  <form method="post" action="index.php?act=area_agent&op=agent_account_set" id="account_form">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['agent_account_info']['id']; ?>" />
    <input type="hidden" name="agent_id" value="<?php echo $output['agent_account_info']['agent_id']; ?>" />
    <?php if ($output['agent_account_info']['id']>0){ ?>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agent_account"><em>*</em>登录账号 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_account_info']['agent_account'];?>" class="input-txt" readonly  />
                    <span class="err"></span>
                    <p class="notic">生成后不可更改</p>
                </dd>
            </dl>
        </div>
    <?php }else { ?>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agent_account"><em>*</em>登录账号 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_account_info']['agent_account'];?>" name="agent_account" id="agent_account" class="input-txt"  />
                    <span class="err"></span>
                    <p class="notic">3-15位字符，可由中文、英文、数字及“_”、“-”组成，生成后不可更改</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_account">默认密码 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_account_info']['agent_pwd'];?>" name="agent_pwd" id="agent_pwd" class="input-txt" readonly />
                    <span class="err"></span>
                    <p class="notic">默认密码不可更改</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    <?php }?>
  </form>
  <?php }?>
</div>
<script type="text/javascript">

 $(function(){

    $("#submitBtn").click(function(){
        if($("#account_form").valid()){
            $("#account_form").submit();
        }
    });
    $('#account_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            agent_account: {
                required : true,
                remote   : {
                    url :'index.php?act=area_agent&op=ajax&branch=check_agent_account',
                    type:'get',
                    data:{
                        agent_name : function(){
                            return $('#agent_account').val();
                        }
                    }
                }
            },
        },
        messages : {
            agent_account : {
                required: '<i class="icon-exclamation-sign"></i>请填写登录账号',
                remote   : '<i class="fa fa-exclamation-circle"></i>登录账号已存在'
            }
        }
    });
});
</script> 
