<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>区域代理返佣配置管理</h3>
                <h5>网站所有区域代理返佣配置索引及管理 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>通过区域代理返佣配置管理，你可以进行查看、编辑代理资料等操作</li>
            <li>你可以根据条件搜索返佣配置，然后选择相应的操作</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=agent_commission_set&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 20, sortable : false, align: 'center', className: 'handle'},
                {display: '返佣ID', name : 'id', width : 80, sortable : true, align: 'center'},
                {display: '返佣配置名称', name : 'commission_set_name', width : 150, sortable : true, align: 'left'},
                {display: '区域类型', name : 'agent_type', width : 100, align: 'center'},
                {display: '秀才', name : 'dz_num', width : 80, align: 'left'},
                {display: '举人', name : 'wdl_num', width : 80, align: 'left'},
                {display: '贡士', name : 'zl_num', width : 80, align: 'left'},
                {display: '进士', name : 'sy_num', width : 80, align: 'left'},
                {display: '艺术家入驻', name : 'artist_num', width : 80, align: 'left'},
                {display: '广告', name : 'gg_num', width : 80, align: 'left'},
                {display: '销售', name : 'goods_num', width : 80, align: 'left'},
                {display: '保证金', name : 'bzj_num', width : 80, align: 'left'},
                {display: '编辑时间', name : 'edit_time', width : 80, sortable : true, align: 'center'},
                {display: '创建时间', name : 'create_time', width : 80, sortable : true, align: 'center'}
            ],
            /*buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],*/
            searchitems : [
                {display: '返佣ID', name : 'id'},
                {display: '返佣配置名称', name : 'commission_set_name'}
            ],
            sortname: "id",
            sortorder: "desc",
            title: '区域代理返佣配置列表'
        });

    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=agent_commission_set&op=agent_add_commission';
        }
    }
</script>
