<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=area_agent" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>区域代理管理 - <?php echo $lang['nc_new']?>区域代理</h3>
                <h5>网站所有区域代理索引及管理</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>可从管理平台手动添加一名区域代理，并填写相关信息。</li>
            <li>标识“*”的选项为必填项，其余为选填项。</li>
            <li>新增区域代理后可从代理列表中找到该条数据，并再次进行编辑操作，但该代理名称不可变。</li>
        </ul>
    </div>
    <form id="agent_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" value="<?php echo $output['agent_info']['agent_id'];?>" name="agent_id" id="agent_id">
        <input type="hidden" value="<?php echo $output['agent_info']['county_id'];?>" name="county_id" id="_area_3">
        <input type="hidden" value="<?php echo $output['agent_info']['city_id'];?>" name="city_id" id="_area_2">
        <input type="hidden" value="<?php echo $output['agent_info']['province_id'];?>" name="province_id" id="_area_1">
        <input type="hidden" value="<?php echo $output['agent_info']['area_id'];?>" name="area_id" id="_area">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agent_name"><em>*</em>代理名称 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['agent_name'];?>" name="agent_name" id="agent_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">3-15位字符，可由中文、英文、数字及“_”、“-”组成，添加后不可更改。</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_address"><em>*</em>代理区域 :</label>
                </dt>
                <dd class="opt">
                    <input type="hidden" id="area_info" name="area_info" value="<?php echo $output['agent_info']['area_info']; ?>" />
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="linkman_name"><em>*</em>联系人名称 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['linkman_name'];?>" id="linkman_name" name="linkman_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="linkman_mobile"><em>*</em>联系人手机号 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['linkman_mobile'];?>" id="linkman_mobile" name="linkman_mobile" class="input-txt">
                    <span class="err"></span></dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="agent_address">联系人地址 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['agent_address'];?>" id="agent_address" name="agent_address" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>代理类型 :</label>
                </dt>
                <dd class="opt">
                    <label><input type="radio" <?php if($output['agent_info']['agent_type'] == 1){ ?>checked="checked"<?php } ?> value="1" name="agent_type">自然人</label>
                    <label><input type="radio" <?php if($output['agent_info']['agent_type'] == 2){ ?>checked="checked"<?php } ?> value="2" name="agent_type">企业法人</label>
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="bank_householder">银行卡-户主 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['bank_householder'];?>" id="bank_householder" name="bank_householder" class="input-txt">
                    <span class="err"></span> </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="bank_number">银行卡-账号 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['bank_number'];?>" id="bank_number" name="bank_number" class="input-txt">
                    <span class="err"></span> </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="bank_accounts_address">银行卡-支行 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['bank_accounts_address'];?>" id="bank_accounts_address" name="bank_accounts_address" class="input-txt">
                    <span class="err"></span> </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="identity_card_number">身份证号 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['agent_info']['identity_card_number'];?>" id="identity_card_number" name="identity_card_number" class="input-txt">
                    <span class="err"></span> </dd>
            </dl>
            <dl class="row" id="bl_image" <?php if($output['agent_info']['agent_type'] == 1) {?> style="display: none;" <?php } ?>     >
                <dt class="tit">
                    <label for="business_license_image">营业执照 :</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show">
                <span class="show">
                    <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>">
                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>>')" onMouseOut="toolTip()"></i>
                    </a>
                </span>
                        <span class="type-file-box">
                  <input name="business_license_image" type="file" class="type-file-file" id="business_license_image" size="30" hidefocus="true" value="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/agent/'.$output['agent_info']['business_license_image'];?>">
                </span>
                    </div>
                    <span class="err"></span>
                    <p class="notic">展示图片，建议大小640x640像素</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">

    $(function(){
        //图片上传
        var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
        $(textButton).insertBefore("#business_license_image");
        $("#business_license_image").change(function(){
            $("#textfield1").val($("#business_license_image").val());
        });
        //地区加载
        $("#area_info").nc_region();
        //更换区域代理类型时
        $("input[name='agent_type']").click(function () {
            var cur_val = $(this).val();
            if(cur_val == 1){
                $("#bl_image").hide();
            }
            if(cur_val == 2){
                $("#bl_image").show();
            }
        });
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
            if($("#agent_form").valid()){
                $("#agent_form").submit();
            }
        });
        $('#agent_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                agent_name: {
                    required : true,
                    remote   : {
                        url :'index.php?act=area_agent&op=ajax&branch=check_agent_name',
                        type:'get',
                        data:{
                            agent_name : function(){
                                return $('#agent_name').val();
                            },
                            agent_id : function(){
                                return $('#agent_id').val();
                            }
                        }
                    }
                },
                area_info: {
                    required : true,
                },
                linkman_name: {
                    required : true,
                },
                linkman_mobile : {
                    digits: true,
                    minlength: 11,
                    maxlength: 11
                }
            },
            messages : {
                agent_name: {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写代理名称',
                    remote   : '<i class="fa fa-exclamation-circle"></i>代理名称已存在'
                },
                area_info : {
                    required : '<i class="fa fa-exclamation-circle"></i>请选择代理区域'
                },
                linkman_name : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写联系人名称'
                },
                linkman_mobile  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写联系人手机号',
                    minlength   : '<i class="fa fa-exclamation-circle"></i>请完善手机号',
                    maxlength   : '<i class="fa fa-exclamation-circle"></i>请完善手机号'
                }
            }
        });
    });
</script>
