<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="<?php echo getReferer();?>" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>区域代理管理 - 账单明细 </h3>
        <h5>区域代理月度对账单</h5>
      </div>
    </div>
  </div>
  <?php if (floatval($output['bill_info']['ob_order_book_totals']) > 0) { ?>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
<!--      <li>未退定金金额是预定订单中已经被取消，但系统未退定金的总金额</li>-->
<!--      <li>默认未退定金金额会累加到平台应付金额中</li>-->
    </ul>
  </div>
  <?php } ?>
  <div class="ncap-form-default">
    <div class="title">
      <h3><?php echo $output['statement_info']['title'];?></h3>
    </div>
    <dl class="row">
      <dt class="tit">结算单号</dt>
      <dd class="opt"><?php echo $output['statement_info']['statement_no'];?></dd>
    </dl>
    <dl class="row">
      <dt class="tit">出账日期</dt>
      <dd class="opt"><?php echo date('Y-m-d',strtotime($output['statement_info']['cdate']));?></dd>
    </dl>
    <dl class="row">
      <dt class="tit"></dt>
      <dd class="opt"><?php echo ncPriceFormat($output['statement_info']['total_amount']);?> = <?php echo ncPriceFormat($output['statement_info']['goods_amount']).'*'.$output['statement_info']['goods_rate'].'%';?> (订单佣金) + <?php echo ncPriceFormat($output['statement_info']['margin_amount']).'*'.$output['statement_info']['margin_rate'].'%';?> (保证金佣金) + <?php echo ncPriceFormat($output['statement_info']['artist_amount']).'*'.$output['statement_info']['artist_rate'].'%';?> (艺术家佣金) + <?php echo ncPriceFormat($output['statement_info']['partner_amount']).'*'.$output['statement_info']['partner_rate'].'%';?> (合伙人佣金) +(调整金额)
      </dd>
    </dl>
      <dl class="row">
      <dt class="tit">调整金额</dt>
      <dd class="opt">
          <input id="other_amount" value="<?php echo ncPriceFormat($output['statement_info']['other_amount']);?>">
      </dd>
    </dl>
      <dl class="row">
      <dt class="tit">备注</dt>
      <dd class="opt">
          <textarea id="sys_comment" rows="5"><?php echo $output['statement_info']['comment'];?></textarea>
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">结算状态</dt>
      <dd class="opt"><?php echo $output['statement_info']['state_str'];?></dd>
    </dl>
    <div class="bot">
        <?php if(in_array($output['statement_info']['state'],array(0,3))){?>
            <a class="ncap-btn-big ncap-btn-green mr10" onclick="if (confirm('确认调整吗?')){return true;}else{return false;}" href="javascript:updateInfo(<?php echo $output['statement_info']['id'];?>,1)">调整对账单</a>
            <a class="ncap-btn-big ncap-btn-green mr10" onclick="if (confirm('审核后将无法撤销，进入下一步代理确认环节，确认审核吗?')){return true;}else{return false;}" href="javascript:updateInfo(<?php echo $output['statement_info']['id'];?>,2);">审核</a>
        <?php }elseif($output['statement_info']['state'] == 2){?>
            <a class="ncap-btn-big ncap-btn-green mr10" onclick="if (confirm('确定标记为已打款吗?')){return true;}else{return false;}" href="javascript:updateInfo(<?php echo $output['statement_info']['id'];?>,3);">确认已打款</a>
        <?php }?>
    </div>
  </div>
  <div class="homepage-focus" nctype="sellerTplContent">
    <!--<div class="title">
      <ul class="tab-base nc-row">
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=order" class="<?php echo ($_GET['query_type'] == '' || $_GET['query_type'] == 'order') ? 'current' : '';?>">订单列表</a></li>
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=refund" class="<?php echo $_GET['query_type'] == 'refund' ? 'current' : '';?>">退单列表</a></li>
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=cost" class="<?php echo $_GET['query_type'] == 'cost' ? 'current' : '';?>">店铺费用</a></li>
        <?php if (floatval($output['bill_info']['ob_order_book_totals']) > 0) { ?>
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=book" class="<?php echo $_GET['query_type'] == 'book' ? 'current' : '';?>">未退定金</a></li>
        <?php } ?>
        <?php if (floatval($output['bill_info']['ob_dis_pay_amount']) > 0) { ?>
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=dis" class="<?php echo $_GET['query_type'] == 'dis' ? 'current' : '';?>">分销佣金</a></li>
        <?php } ?>
      </ul>
    </div>-->
    <?php include template($output['tpl_name']);?>
  </div>
</div>
<script>
    function updateInfo(id,type){
        var url='index.php?act=statement&op=update_statement';
        $.post(url,{'id':id,'type':type,'other_amount':$('#other_amount').val(),'comment':$('#sys_comment').val()},function(re){
            if(re == 1){
                showSucc('成功!');
                window.setTimeout("javascript:location.href='index.php?act=statement&op=index'", 1000);
            }else{
                showError('失败!');
            }
        },'json');
    }
</script>
