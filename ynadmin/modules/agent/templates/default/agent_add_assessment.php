<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=agent_assessment_set" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>区域代理管理 - <?php echo $lang['nc_new']?>下个月考核设置</h3>
                <h5>网站所有区域代理考核配置管理</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>标识“*”的选项为必填项，其余为选填项</li>
            <li>编辑考核配置后可从考核列表中找到该条数据，并再次进行编辑操作</li>
        </ul>
    </div>
    <form id="agent_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" value="<?php echo $output['assessment_info']['id'];?>" name="id" id="id">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="commission_set_name"><em>*</em>考核配置名称 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['assessment_info']['assessment_set_name'];?>" name="assessment_set_name" id="assessment_set_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">3-15位字符，可由中文、英文、数字及“_”、“-”组成</p>
                </dd>
            </dl>
            <div class="title">
                <h3>下个月保证金-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['next_bzj_content'] as $k=>$v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" name="bzj_index[]" value="<?=$v['index']?>" class="input-txt valid" style="width:100px !important;" />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" name="bzj_rate[]" value="<?=$v['rate']?>" class="input-txt valid" style="width:30px !important;" />%
                    </dd>
                </dl>
            <?php }?>
            <div class="title">
                <h3>下个月订单-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['next_goods_content'] as $v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" name="goods_index[]" value="<?=$v['index']?>" class="input-txt valid" style="width:100px !important;" />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" name="goods_rate[]" value="<?=$v['rate']?>" class="input-txt valid" style="width:30px !important;" />%
                    </dd>
                </dl>
            <?php }?>
            <div class="title">
                <h3>下个月艺术家-考核标准</h3><h5><p class="notic">根据达到指标倒序排列</p></h5>
            </div>
            <?php foreach ($output['assessment_info']['next_artist_content'] as $v){?>
                <dl class="row">
                    <dt class="tit">
                        <label>指标 :</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" name="artist_index[]" value="<?=$v['index']?>" class="input-txt valid" style="width:100px !important;" />元
                        <label style="padding-left: 100px;">比例 :</label>
                        <input type="text" name="artist_rate[]" value="<?=$v['rate']?>" class="input-txt valid" style="width:30px !important;" />%
                    </dd>
                </dl>
            <?php }?>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">

    $(function(){
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
            if($("#agent_form").valid()){
                $("#agent_form").submit();
            }
        });
        $('#agent_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                commission_set_name: {
                    required : true
                }
            },
            messages : {
                commission_set_name: {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写考核配置名称'
                }
            }
        });
    });
</script>
