<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>区域代理对账单</h3>
                <h5>网站所有区域代理月度对账单索引及管理 </h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
<!--            <li>通过区域代理管理，你可以进行查看、编辑代理资料等操作</li>-->
<!--            <li>你可以根据条件搜索代理，然后选择相应的操作</li>-->
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=statement&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 20, sortable : false, align: 'center', className: 'handle'},
                {display: 'ID', name : 'id', width : 80, sortable : true, align: 'center'},
                {display: '区域代理ID', name : 'agent_id', width : 40, sortable : true, align: 'left'},
                {display: '账单号', name : 'statement_no', width : 110, sortable : true, align: 'center'},
                {display: '账单标题', name : 'title', width : 160, sortable : true, align: 'left'},
                {display: '总额', name : 'total_amount', width : 80, align: 'center'},
                {display: '订单返佣', name : 'goods_amount', width : 80, align: 'left'},
                {display: '订单返佣比率', name : 'goods_rate', width : 60, align: 'center'},
                {display: '保证金返佣', name : 'margin_amount', width : 80, align: 'center'},
                {display: '保证金返佣比率', name : 'margin_rate', width : 60, align: 'center'},
                {display: '艺术家返佣', name : 'artist_amount', width : 80, align: 'center'},
                {display: '艺术家返佣比率', name : 'artist_rate', width : 60, align: 'center'},
                {display: '合伙人返佣', name : 'partner_amount', width : 80, align: 'center'},
                {display: '合伙人返佣比率', name : 'partner_rate', width : 60, align: 'center'},
                {display: '调剂金额', name : 'other_amount', width : 80, align: 'center'},
                {display: '订单状态', name : 'commission_status', width : 80, align: 'center'},
                {display: '账单生成时间', name : 'cdate', width : 100, align: 'center'},
                {display: '最后更新时间', name : 'udate', width : 100, align: 'center'},
            ],
            buttons : [
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出csv文件,如果不选中行，将导出列表所有数据', onpress : fg_operate}
        ],
            searchitems : [
                {display: '区域代理ID', name : 'agent_id'},
                {display: '对账单名称', name : 'title'},
            ],
            sortname: "id",
            sortorder: "desc",
            title: '区域代理列表'
        });

    });


    function fg_operate(name, grid) {
        if (name == 'csv') {
          var itemlist = new Array();
            if($('.trSelected',grid).length>0){
                $('.trSelected',grid).each(function(){
                  itemlist.push($(this).attr('data-id'));
                });
            }
            fg_csv(itemlist);
        }
    }

    function fg_csv(ids) {
        id = ids.join(',');
        window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_statement&id='+id;
    }
</script>
