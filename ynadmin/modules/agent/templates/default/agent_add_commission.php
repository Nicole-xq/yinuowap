<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=agent_commission_set" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>区域代理管理 - <?php echo $lang['nc_new']?>返佣设置</h3>
                <h5>网站所有区域代理返佣配置管理</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>可从管理平台手动添加一条区域代理返佣配置记录</li>
            <li>标识“*”的选项为必填项，其余为选填项</li>
            <li>新增返佣配置后可从返佣列表中找到该条数据，并再次进行编辑操作</li>
        </ul>
    </div>
    <form id="agent_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" value="<?php echo $output['commission_info']['id'];?>" name="id" id="id">
        <input type="hidden" value="<?php echo $output['commission_info']['area_id'];?>" name="area_id" id="_area">
        <input type="hidden" value="" name="county_id" id="_area_3">
        <input type="hidden" value="" name="city_id" id="_area_2">
        <input type="hidden" value="" name="province_id" id="_area_1">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="commission_set_name"><em>*</em>返佣配置名称 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['commission_info']['commission_set_name'];?>" name="commission_set_name" id="commission_set_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">3-15位字符，可由中文、英文、数字及“_”、“-”组成</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="dz_num"><em>*</em>升级秀才返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?=$output['commission_info']['dz_num']?>" id="dz_num" name="dz_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推秀才级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="wdl_num"><em>*</em>升级举人返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?=$output['commission_info']['wdl_num']?>" id="wdl_num" name="wdl_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推举人级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="zl_num"><em>*</em>升级贡士返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['zl_num']?>" id="zl_num" name="zl_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推贡士级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="sy_num"><em>*</em>升级进士返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['sy_num']?>" id="sy_num" name="sy_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推进士级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="artist_num"><em>*</em>艺术家入驻返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['artist_num']?>" id="artist_num" name="artist_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推艺术家返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="gg_num"><em>*</em>广告返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['gg_num']?>" id="gg_num" name="gg_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推广告行为返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="xs_num"><em>*</em>销售返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['goods_num']?>" id="goods_num" name="goods_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推销售行为返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="bzj_num"><em>*</em>保证金返佣 :</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['commission_info']['bzj_num']?>" id="bzj_num" name="bzj_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推保证金返佣百分比</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">

    $(function(){
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
            if($("#agent_form").valid()){
                $("#agent_form").submit();
            }
        });
        $('#agent_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                commission_set_name: {
                    required : true
                },
                wdl_num: {
                    required : true
                },
                zl_num : {
                    required : true
                },
                sy_num : {
                    required : true
                },
                artist_num : {
                    required : true
                },
                gg_num : {
                    required : true
                },
                goods_num : {
                    required : true
                },
                bzj_num : {
                    required : true
                }
            },
            messages : {
                commission_set_name: {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写返佣配置名称'
                },
                dz_num : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写升级秀才返佣比例'
                },
                wdl_num : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写升级举人返佣比例'
                },
                zl_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写升级贡士返佣比例'
                },
                sy_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写升级进士返佣比例'
                },
                artist_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写艺术家入驻返佣比例'
                },
                gg_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写广告返佣比例'
                },
                goods_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写销售返佣比例'
                },
                bzj_num  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写保证金返佣比例'
                }
            }
        });
    });
</script>
