<?php
/**
 * 菜单
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
$_menu['agent'] = array (
        'name' => '区域代理',
        'child' => array(
                array(
                        'name' => $lang['nc_config'], 
                        'child' => array(
                                'agent_commission_set' => '返佣配置',
                                'agent_assessment_set' => '考核配置',
                        )
                ),
                array(
                        'name' => '区域代理',
                        'child' => array(
                                'area_agent' => '列表管理',
                                'agent_order' => '返佣记录',
                                'statement' => '对账单'
                        )
                )
        )
);