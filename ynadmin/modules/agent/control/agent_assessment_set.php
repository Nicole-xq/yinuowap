<?php
/**
 * 区域代理考核配置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class agent_assessment_setControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('agent_assessment_set');
    }

    /**
     * 区域代理管理XML
     */
    public function get_xmlOp(){
        $condition = [];
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = ['id','assessment_set_name','edit_time','create_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $model_assessment = Model('agent_assessment');
        $assessment_list = $model_assessment->getAgentAssessmentList($condition, '*', $page, $order);

        $data = array();
        $data['now_page'] = $model_assessment->shownowpage();
        $data['total_num'] = $model_assessment->gettotalnum();
        $agent_type = Model('agent_commission')->getAgentType();

        foreach ($assessment_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=agent_assessment_set&op=agent_add_assessment&id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['assessment_set_name'] = $value['assessment_set_name'];
            $param['agent_type'] = $agent_type[$value['agent_type']];
            $param['last_bzj_content'] = self::assessmentHtml($value['last_bzj_content']);
            $param['last_goods_content'] = self::assessmentHtml($value['last_goods_content']);
            $param['last_artist_content'] = self::assessmentHtml($value['last_artist_content']);
            $param['curr_bzj_content'] = self::assessmentHtml($value['curr_bzj_content']);
            $param['curr_goods_content'] = self::assessmentHtml($value['curr_goods_content']);
            $param['curr_artist_content'] = self::assessmentHtml($value['curr_artist_content']);
            $param['edit_time'] = date('Y-m-d', $value['edit_time']);
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    private function assessmentHtml($content_arr){
        $content_arr = unserialize($content_arr);
        $html_text = '';
        foreach($content_arr as $val){
            $html_text .= "<p>{$val['index']}元 <=>{$val['rate']}%</p>";
        }

        return $html_text;
    }

    /**
     * 编辑区域代理考核
     */
    public function agent_add_assessmentOp(){
        $model_assessment = Model('agent_assessment');
        if (chksubmit()){
            $insert_array = [
                'assessment_set_name'=>strval($_POST['assessment_set_name']),
                'next_bzj_content'=>self::makeUpData($_POST['bzj_index'],$_POST['bzj_rate']),
                'next_goods_content'=>self::makeUpData($_POST['goods_index'],$_POST['goods_rate']),
                'next_artist_content'=>self::makeUpData($_POST['artist_index'],$_POST['artist_rate']),
                'edit_time'=>time()
            ];
            if ($_POST['id']){
                $conditions = ['id'=>intval($_POST['id'])];
                $result = $model_assessment->updateAgentAssessment($conditions,$insert_array);
                $this->log('编辑区域代理考核配置'.'[ID:'.$_POST['id'].']',1);
            }else{
                $insert_array['create_time'] = time();
                $result = $model_assessment->addAgentAssessment($insert_array);
            }
            if ($result){
                $url = [
                    [
                        'url'=>'index.php?act=agent_assessment_set&op=index',
                        'msg'=>'考核配置列表',
                    ],
                    [
                        'url'=>'index.php?act=agent_assessment_set&op=agent_add_assessment&id='.intval($_POST['id']),
                        'msg'=>'继续操作考核配置',
                    ],
                ];
                showMessage('操作成功',$url);
            }else {
                showMessage('操作失败');
            }

        }
        if ($_GET['id']){
            $assessment_info = $model_assessment->getAgentAssessmentInfo(['id'=>intval($_GET['id'])]);
            $assessment_info['next_bzj_content'] = unserialize($assessment_info['next_bzj_content']);
            $assessment_info['next_goods_content'] = unserialize($assessment_info['next_goods_content']);
            $assessment_info['next_artist_content'] = unserialize($assessment_info['next_artist_content']);
            Tpl::output('assessment_info',$assessment_info);
        }

        Tpl::showpage('agent_add_assessment');
    }


    /**
     * Des 数据整理
     * @param array $key_arr
     * @param array $value_arr
     * @param int $num_row 默认考核标准的个数
     * @return array
     */
    private function makeUpData($key_arr, $value_arr, $num_row = 5){
        $re_arr = [];
        arsort($key_arr);
        $index_arr = array_keys($key_arr);
        $key = 0;
        while($key < $num_row){
            $re_arr[] = [
                'index' => $key_arr[$index_arr[$key]],
                'rate'  => $value_arr[$index_arr[$key]]
            ];
            $key++;
        }

        return serialize($re_arr);
    }


}


