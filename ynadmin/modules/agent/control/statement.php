<?php
/**
 * 区域代理对账单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class statementControl extends SystemControl{
    public $state_arr = array(
        '0'=>'平台审核中',
        '1'=>'代理审核中',
        '2'=>'待打款',
        '3'=>'代理审核异常',
        '4'=>'已打款',
        '5'=>'已完成'
    );

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('statement_index');
    }

    /**
     * 区域代理管理XML
     */
    public function get_xmlOp(){
        $model_agent_statement = Model('agent_statement');
        $condition = array();
        $model_agent = Model('area_agent');
        $agent_type_arr = $model_agent->getAreaType();
        if ($_POST['query'] != '') {
            $param[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
            $agent_list = $model_agent->getAreaAgentList($param);
            $agent_id = empty($agent_list)?0:$agent_list[0]['agent_id'];
            $agent_name = empty($agent_list)?'':$agent_list[0]['agent_name'];
            $condition['agent_id'] = $agent_id;
        }
        $order = '';
        $param = ['id','cdate','commission_status'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $statement_list = $model_agent_statement->getList($condition, '*', $page, $order);
        $data = array();
        $data['now_page'] = $model_agent_statement->shownowpage();
        $data['total_num'] = $model_agent_statement->gettotalnum();

        foreach ($statement_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=statement&op=statement_info&statement_id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['id'] = $value['id'];
            $agent_info = $model_agent->getAreaAgentInfo(array('agent_id'=>$value['agent_id']));
            $param['agent_id'] = $value['agent_id'];
            $param['statement_no'] = $value['statement_no'];
            $param['title'] = $value['title'];
            $param['total_amount'] = $value['total_amount'];
            $param['goods_amount'] = $value['goods_amount'];
            $param['goods_rate'] = $value['goods_rate'].'%';
            $param['margin_amount'] = $value['margin_amount'];
            $param['margin_rate'] = $value['margin_rate'].'%';
            $param['artist_amount'] = $value['artist_amount'];
            $param['artist_rate'] = $value['artist_rate'].'%';
            $param['partner_amount'] = $value['partner_amount'];
            $param['partner_rate'] = $value['partner_rate'].'%';
            $param['other_amount'] = $value['other_amount'];
            $param['state'] = $this->state_arr[$value['state']];
            $param['cdate'] = $value['cdate'];
            $param['udate'] = $value['udate'];
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 区域代理月度对账单详情
     */
    public function statement_infoOp(){
        $statement_id = intval($_GET['statement_id']);
        if ($statement_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $agent_statement = Model('agent_statement');
        $statement_info = $agent_statement->getInfo(array('id'=>$statement_id));
        if (!$statement_info){
            showMessage('参数错误','','html','error');
        }

//        $order_condition = array();
//        $order_condition['order_state'] = ORDER_STATE_SUCCESS;
//        $order_condition['store_id'] = $bill_info['ob_store_id'];
//        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
//        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
//        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
//        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']) : null;
//        $end_unixtime = $if_end_date ? $end_unixtime+86400-1 : null;
//        if ($if_start_date || $if_end_date) {
//            $order_condition['finnshed_time'] = array('between',"{$start_unixtime},{$end_unixtime}");
//        } else {
//            $order_condition['finnshed_time'] = array('between',"{$bill_info['ob_start_date']},{$bill_info['ob_end_date']}");
//        }
//        if ($_GET['query_type'] == 'refund') {
//            $sub_tpl_name = 'bill_order_bill.show.refund_list';
//        } elseif ($_GET['query_type'] == 'cost') {
//            $sub_tpl_name = 'bill_order_bill.show.cost_list';
//        } elseif ($_GET['query_type'] == 'dis') {
//            $sub_tpl_name = 'bill_order_bill.show.dis_list';
//        } elseif ($_GET['query_type'] == 'book') {
//            $sub_tpl_name = 'bill_order_bill.show.order_book_list';
//        } else {
//            //订单列表
//            $sub_tpl_name = 'bill_order_bill.show.order_list';
//        }

//        Tpl::output('tpl_name',$sub_tpl_name);
        $statement_info['state_str'] = $this->state_arr[$statement_info['state']];
        Tpl::output('statement_info',$statement_info);
        Tpl::showpage('statement_info');
    }

    public function update_statementOp(){
        $model_statement = Model('agent_statement');
        $id = $_POST['id'];
        $type = $_POST['type'];
        $data = array();
        if($type == 2 || $type == 3){
            $data = array(
                'state'=>$type == 2?1:4,
                'udate'=>date('Y-m-d H:i:s')
            );
        }else{

            $other_amount = $_POST['other_amount'];
            $comment = $_POST['comment'];

            $statement_info = $model_statement->getInfo(array('id'=>$id));
            $total = sprintf("%.2f",$statement_info['goods_amount']*($statement_info['goods_rate
        ']/100)) + sprintf("%.2f",$statement_info['margin_amount']*($statement_info['margin_rate']/100)) + sprintf("%.2f",$statement_info['artist_amount']*($statement_info['artist_rate']/100)) + sprintf("%.2f",$statement_info['partner_amount']*($statement_info['partner_rate']/100)) + sprintf("%.2f",$other_amount);
            $data = array(
                'other_amount'=>$other_amount,
                'comment'=>$comment,
                'total_amount'=>$total,
                'udate'=>date('Y-m-d H:i:s')
            );
        }
        $re = $model_statement->updateInfo(array('id'=>$id),$data);
        if($re !=0 && $type == 1){
            $tmp = array(
                'other_amount'=>array(
                    'old'=>$statement_info['other_amount'],
                    'new'=>$other_amount
                ),
                'comment'=>array(
                    'old'=>$statement_info['comment'],
                    'new'=>$comment
                ),
                'total_amount'=>array(
                    'old'=>$statement_info['total_amount'],
                    'new'=>$total
                )

            );
            $param = array(
                'admin_id'=>$this->admin_info['id'],
                'statement_id'=>$id,
                'content'=>json_encode($tmp),
                'cdate'=>date('Y-m-d H:i:s')
            );
            Model('statement_change_log')->table('statement_change_log')->insert($param);
//            dd();
        }
        echo $re;exit;
    }

    public function export_statementOp()
    {
        $model_agent_statement = Model('agent_statement');
        if($_POST['query'] != ''){
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        if (preg_match('/^[\d,]+$/', $_GET['id'])) {
            $_GET['id'] = explode(',',trim($_GET['id'],','));
            $condition['id'] = array('in',$_GET['id']);
        }
        $order = '';
        $param = ['id', 'cdate', 'commission_status'];
        if(in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))){
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $data = $model_agent_statement->getList($condition, '*', $page, $order);

        $export_data = array();
        $export_data[0] = array('账单编号', '区域代理id', '标题', '总佣金', '订单佣金', '订单考核', '保证金佣金', '保证金考核', '艺术家佣金', '艺术家考核', '合伙人佣金', '合伙人考核', '调整金额', '备注');
        $totals = 0;
        $goods_totals = 0;
        $margin_totals = 0;
        $artist_totals = 0;
        $partner_totals = 0;
        $other_totals = 0;
        foreach($data as $k => $v){
            $export_data[$k + 1][] = $v['statement_no'];
            $export_data[$k + 1][] = $v['agent_id'];
            $export_data[$k + 1][] = $v['title'];
            $totals += $export_data[$k + 1][] = $v['total_amount'];
            $goods_totals += $export_data[$k + 1][] = ceil($v['goods_amount'] * ($v['goods_rate'] / 100));
            $export_data[$k + 1][] = $v['goods_rate'] . '%';
            $margin_totals += $export_data[$k + 1][] = ceil($v['margin_amount'] * ($v['margin_rate'] / 100));
            $export_data[$k + 1][] = $v['margin_rate'] . '%';
            $artist_totals += $export_data[$k + 1][] = ceil($v['artist_amount'] * ($v['artist_rate'] / 100));
            $export_data[$k + 1][] = $v['artist_rate'] . '%';
            $partner_totals += $export_data[$k + 1][] = ceil($v['partner_amount'] * ($v['partner_rate'] / 100));
            $export_data[$k + 1][] = $v['partner_rate'] . '%';
            $other_totals += $export_data[$k + 1][] = $v['other_amount'];
            $export_data[$k + 1][] = $v['comment'];

        }
        $count = count($export_data);
        $export_data[$count][] = '';
        $export_data[$count][] = '';
        $export_data[$count][] = '合计';
        $export_data[$count][] = $totals;
        $export_data[$count][] = $goods_totals;
        $export_data[$count][] = '';
        $export_data[$count][] = $margin_totals;
        $export_data[$count][] = '';
        $export_data[$count][] = $artist_totals;
        $export_data[$count][] = '';
        $export_data[$count][] = $partner_totals;
        $export_data[$count][] = '';
        $export_data[$count][] = $other_totals;
        $csv = new Csv();
        $export_data = $csv->charset($export_data, CHARSET, 'gbk');
        $csv->filename = 'statement';
        $csv->export($export_data);
    }

}
