<?php
/**
 * 区域代理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class area_agentControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('agent_index');
    }

    /**
     * 区域代理管理XML
     */
    public function get_xmlOp(){
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = ['agent_id','agent_name','edit_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $model_agent = Model('area_agent');
        $agent_type_display = $model_agent->getAgentType();
        $area_type_display = $model_agent->getAreaType();
        $agent_list = $model_agent->getAreaAgentList($condition, '*', $page, $order);

        $data = array();
        $data['now_page'] = $model_agent->shownowpage();
        $data['total_num'] = $model_agent->gettotalnum();

        foreach ($agent_list as $value) {
            $account_info = Model('agent_account')->getAgentAccountInfo(['agent_id'=>$value['agent_id']], 'id,agent_account');
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=area_agent&op=agent_add&agent_id=" . $value['agent_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] .= $account_info['id'] > 0 ? "" : "<a class='btn blue' href='javascript:void(0)' onclick=\"ajax_form('agent_account_set','设置代理“".$value['agent_name']."”的账号信息','index.php?act=area_agent&op=agent_account_set&agent_id=".$value['agent_id']."','640')\" ><i class=''>设置账号</i></a>";
            $param['agent_id'] = $value['agent_id'];
            $param['agent_name'] = $value['agent_name'];
            $param['linkman_name'] = $value['linkman_name'];
            $param['linkman_mobile'] = $value['linkman_mobile'];
            $param['agent_account'] = $account_info['agent_account'];
            $param['agent_type'] = $agent_type_display[$value['agent_type']];
            $param['area_type'] = $area_type_display[$value['area_type']];
            $param['area_info'] = $value['area_info'];
            $param['agent_state'] = $value['agent_state'] ==  '1' ? "<span class='yes'><a href='javascript:void(0)' onclick=\"fg_change_state('".$value['agent_id']."')\" ><i class='fa fa-check-circle'></i>正常</a></span>" : "<span class='no'><a href='javascript:void(0)' onclick=\"fg_change_state('".$value['agent_id']."')\" ><i class='fa fa-ban'></i>禁用</a></span>";
            $param['edit_time'] = date('Y-m-d', $value['edit_time']);
            $param['reg_time'] = date('Y-m-d', $value['reg_time']);
            $data['list'][$value['agent_id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 新增区域代理
     */
    public function agent_addOp(){
        $model_agent = Model('area_agent');
        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["agent_name"], "require"=>"true", "message"=>'代理名称不为空'),
                array("input"=>$_POST["linkman_name"], "require"=>"true", "message"=>'联系人不为空'),
                array("input"=>$_POST["linkman_mobile"], "require"=>"true", 'validator'=>'mobile', "message"=>'请填写正确的联系人手机号')
            );
            $error = $obj_validate->validate();
            $exist_condition = [
                'area_id'=>intval($_POST['area_id']),
                'agent_state'=>1
            ];
            $_POST['agent_id'] ? $exist_condition['agent_id'] = ['neq',intval($_POST['agent_id'])] : '';

            $agent_info_exist = $model_agent->getAreaAgentInfo($exist_condition, 'agent_id');
            if ($agent_info_exist){
                $error = '该区域已经存在代理';
            }
            if ($error != ''){
                showMessage($error);exit;
            }
            $area_type = $_POST['province_id'] ?  1 : 0;
            $area_type = $_POST['city_id'] ?  2 : $area_type;
            $area_type = $_POST['county_id'] ?  3 : $area_type;
            $insert_array = [
                'agent_name'=>trim($_POST['agent_name']),
                'agent_address'=>trim($_POST['agent_address']),
                'agent_type'=>intval($_POST['agent_type']),
                'area_type'=>$area_type,
                'linkman_name'=>trim($_POST['linkman_name']),
                'linkman_mobile'=>trim($_POST['linkman_mobile']),
                'edit_time'=>time(),
                'area_id'=>intval($_POST['area_id']),
                'county_id'=>intval($_POST['county_id']),
                'city_id'=>intval($_POST['city_id']),
                'province_id'=>intval($_POST['province_id']),
                'area_info'=>strval($_POST['area_info']),
                'bank_householder'=>strval($_POST['bank_householder']),
                'bank_number'=>strval($_POST['bank_number']),
                'bank_accounts_address'=>strval($_POST['bank_accounts_address']),
                'identity_card_number'=>strval($_POST['identity_card_number']),
            ];
            //工商营业执照
            if ($_FILES['business_license_image']['name'] != '') {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_MOBILE . '/agent');
                $result = $upload->upfile('business_license_image');
                if ($result) {
                    $insert_array['business_license_image'] = $upload->file_name;
                } else {
                    showMessage($upload->error);
                }
            }
            if ($_POST['agent_id']){
                $conditions = ['agent_id'=>intval($_POST['agent_id'])];
                $result = $model_agent->updateAreaAgent($conditions,$insert_array);
                $this->log('编辑区域代理'.'[ID:'.$_POST['agent_id'].']',1);
            }else{
                $insert_array['reg_time'] = time();
                $result = $model_agent->addAreaAgent($insert_array);
                $this->log('添加区域代理'.'[ '.$insert_array['agent_name'].']',1);
            }
            if ($result){
                $url = [
                    [
                        'url'=>'index.php?act=area_agent&op=index',
                        'msg'=>'区域代理列表',
                    ],
                    [
                        'url'=>'index.php?act=area_agent&op=agent_add&agent_id='.$_POST['agent_id'],
                        'msg'=>'继续操作区域代理',
                    ],
                ];
                showMessage('操作成功',$url);
            }else {
                showMessage('操作失败');
            }

        }
        if ($_GET['agent_id']){
            $agent_info = $model_agent->getAreaAgentInfo(['agent_id'=>intval($_GET['agent_id'])]);
            Tpl::output('agent_info',$agent_info);
        }
        Tpl::showpage('agent_add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        if($_GET['branch'] == 'check_agent_name') { // 区域代理名称验证
            $condition = [
                'agent_name'=>strval($_GET['agent_name']),
                'agent_id'=>['neq',intval($_GET['agent_id'])]
            ];
            $agent_info = Model('area_agent')->getAreaAgentInfo($condition, 'agent_id');
        }elseif ($_GET['branch'] == 'check_agent_account') { // 区域代理登录名验证
            $condition = [
                'agent_account'=>strval($_GET['agent_account']),
            ];
            $agent_info = Model('agent_account')->getAgentAccountInfo($condition, 'id');
        }

        if (empty($agent_info)){
            echo 'true';exit;
        }else {
            echo 'false';exit;
        }
    }

    /**
     * 更改区域代理状态
     */
    public function change_agent_stateOp(){
        $agent_id = intval($_GET['id']);
        if (empty($agent_id)){
            exit(json_encode(array('state'=>false,'msg'=>'参数不完整')));
        }
        $agent_model = Model('area_agent');
        $agent_info = $agent_model->getAreaAgentInfo(['agent_id'=>$agent_id], 'agent_id,area_id,agent_state');
        $update_arr['agent_state'] = 0;
        if ($agent_info['agent_state'] == 2){//验证一个地区仅有一个正常状态代理，其他的均为禁用状态
            $condition = [
                'area_id'=>$agent_info['area_id'],
                'agent_id'=>['neq',$agent_info['agent_id']],
                'agent_state'=>1,
            ];
            $agent_info_exist = $agent_model->getAreaAgentInfo($condition, 'agent_id');
            if ($agent_info_exist){
                exit(json_encode(array('state'=>false,'msg'=>'该区域已经存在代理')));
            }
            $update_arr['agent_state'] = 1;
        }else{
            $update_arr['agent_state'] = 2;
        }

        $result = $agent_model->updateAreaAgent(['agent_id'=>$agent_id],$update_arr);
        if ($result){
            $this->log('变更区域代理状态'.'[ID：'.$agent_id.']',1);
            exit(json_encode(array('state'=>true,'msg'=>'状态变更成功')));
        }else {
            exit(json_encode(array('state'=>false,'msg'=>'状态变更失败')));
        }

    }

    /**
     * 设置代理账号
     */
    public function agent_account_setOp() {
        $agent_pwd = '111111';
        $model_agent = Model('agent_account');
        if (chksubmit()) {
            $insert = [
                    'agent_account'=>strval($_POST['agent_account']),
                    'agent_pwd'=>md5($agent_pwd),
                    'agent_id'=>intval($_POST['agent_id'])
                ];
            $exist_condition = [
                'agent_account'=>strval($_POST['agent_account'])
            ];
            $account_info_exist = $model_agent->getAgentAccountInfo($exist_condition, 'id');
            if ($account_info_exist){
                showMessage('该区域已经存在代理');exit;
            }
            if ($_POST['id']){
                $insert['edit_time'] = time();
                $conditions = ['id'=>intval($_POST['id'])];
                $result = $model_agent->updateAgentAccount($conditions,$insert);
            }else{
                $insert['create_time'] = time();
                $result = $model_agent->addAgentAccount($insert);
            }
            $result ? showDialog('操作成功', 'reload', 'succ') : showMessage('操作失败');
        }
        $agent_id = intval($_GET['agent_id']);
        $agent_account_info = $model_agent->getAgentAccountInfo(['agent_id'=>$agent_id],'id,agent_account');
        $agent_account_info['agent_pwd'] = $agent_pwd;
        $agent_account_info['agent_id'] = $agent_id;
        Tpl::output('agent_account_info', $agent_account_info);
        Tpl::showpage('agent_account', 'null_layout');
    }




}
