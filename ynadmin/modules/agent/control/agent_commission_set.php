<?php
/**
 * 区域代理佣金配置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class agent_commission_setControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('agent_commission_set');
    }

    /**
     * 区域代理管理XML
     */
    public function get_xmlOp(){
        $condition = [];
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = ['id','commission_set_name','edit_time','create_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $model_commission = Model('agent_commission');
        $commission_list = $model_commission->getAgentCommissionList($condition, '*', $page, $order);

        $data = array();
        $data['now_page'] = $model_commission->shownowpage();
        $data['total_num'] = $model_commission->gettotalnum();
        $agent_type = $model_commission->getAgentType();

        foreach ($commission_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=agent_commission_set&op=agent_add_commission&id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['id'] = $value['id'];
            $param['commission_set_name'] = $value['commission_set_name'];
            $param['agent_type'] = $agent_type[$value['agent_type']];
            $param['wdl_num'] = $value['wdl_num'];
            $param['zl_num'] = $value['zl_num'];
            $param['sy_num'] = $value['sy_num'];
            $param['artist_num'] = $value['artist_num'];
            $param['gg_num'] = $value['gg_num'];
            $param['goods_num'] = $value['goods_num'];
            $param['bzj_num'] = $value['bzj_num'];
            $param['edit_time'] = date('Y-m-d', $value['edit_time']);
            $param['create_time'] = date('Y-m-d', $value['create_time']);
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 新增区域代理
     */
    public function agent_add_commissionOp(){
        $model_commission = Model('agent_commission');
        if (chksubmit()){
            $insert_array = [
                'commission_set_name'=>strval($_POST['commission_set_name']),
                'wdl_num'=>floatval($_POST['wdl_num']),
                'zl_num'=>floatval($_POST['zl_num']),
                'sy_num'=>floatval($_POST['sy_num']),
                'artist_num'=>floatval($_POST['artist_num']),
                'gg_num'=>floatval($_POST['gg_num']),
                'goods_num'=>floatval($_POST['goods_num']),
                'bzj_num'=>floatval($_POST['bzj_num']),
                'edit_time'=>time()
            ];
            if ($_POST['id']){
                $conditions = ['id'=>intval($_POST['id'])];
                $result = $model_commission->updateAgentCommission($conditions,$insert_array);
                $this->log('编辑区域代理返佣配置'.'[ID:'.$_POST['id'].']',1);
            }else{
                $insert_array['create_time'] = time();
                $result = $model_commission->addAgentCommission($insert_array);
            }
            if ($result){
                $url = [
                    [
                        'url'=>'index.php?act=agent_commission_set&op=index',
                        'msg'=>'返佣配置列表',
                    ],
                    [
                        'url'=>'index.php?act=agent_commission_set&op=agent_add_commission',
                        'msg'=>'继续操作返佣配置',
                    ],
                ];
                showMessage('操作成功',$url);
            }else {
                showMessage('操作失败');
            }

        }
        if ($_GET['id']){
            $commission_info = $model_commission->getAgentCommissionInfo(['id'=>intval($_GET['id'])]);
            Tpl::output('commission_info',$commission_info);
        }

        Tpl::showpage('agent_add_commission');
    }


}


