<?php
/**
 * 投票活动
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class vote_activityControl extends SystemControl{

    const VOTE_ACTIVITY_LINK = 'index.php?act=vote_activity&op=vote_activity_list';

    public function __construct(){
        parent::__construct();
        Language::read('cms');
    }

    public function indexOp() {
        $this->vote_activity_listOp();
    }

    /**
     * 会销列表
     **/
    public function vote_activity_listOp() {
        Tpl::showpage("vote_activity.index");
    }

    /**
     * cms专题列表
     **/
    public function vote_activity_list_xmlOp() {
        $model_activity = Model('vote_activity');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = array();
        $list = $model_activity->getList($condition, $page, 'id desc');
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $fields_array = array('id','title');
            foreach ($list as $k => $v){
                $out_array = getFlexigridArray(array(),$fields_array,$v);
                $out_array['start_time'] = $v['start_time'];
                $out_array['end_time'] = $v['end_time'];
                $out_array['cdate'] = $v['add_time'];
                $operation = '';
                $operation .= '<a href="index.php?act=vote_activity&op=vote_activity_edit&id='.$v['id'].'" class="btn red" ><i class="fa fa-trash-o"></i>编辑</a>';
                $operation .= '<a href="'.WAP_SITE_URL."/tmpl/member/activity_info.html?act_id={$v['id']}".'" class="btn green" target="_blank" ><i class="fa fa-list-alt"></i>查看</a>';
                $out_array['operation'] = $operation;
                $out_list[$v['id']] = $out_array;
            }
        }

        $data = array();
        $data['now_page'] = $model_activity->shownowpage();
        $data['total_num'] = $model_activity->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 添加活动
     **/
    public function vote_activity_addOp() {
        Tpl::showpage('vote_activity.add');
    }

    /**
     * cms专题编辑
     */
    public function vote_activity_editOp() {
        $id = intval($_GET['id']);
        if(empty($id)) {
            showMessage(Language::get('param_error'),'','','error');
        }

        $model_vote_activity = Model('vote_activity');
        $detail = $model_vote_activity->getInfo(array('id'=>$id));
        $detail['wap_url'] = WAP_SITE_URL."/tmpl/member/activity_info.html?act_id={$id}";
        if(empty($detail)) {
            showMessage(Language::get('param_error'),'','','error');
        }

        Tpl::output('detail', $detail);
        $this->show_menu('special_edit');
        Tpl::showpage('vote_activity.add');
    }

    /**
     * cms专题保存
     **/
    public function vote_activity_saveOp() {
//        print_R($_POST);exit;
        $param = array();
        $param['title'] = $_POST['vote_activity_title'];
        $special_image = $this->vote_activity_image_upload('vote_activity_image');
        if(!empty($special_image)) {
            $param['image'] = $special_image;
            if(!empty($_POST['old_vote_activity_image'])) {
                $this->vote_activity_image_drop($_POST['old_vote_activity_image']);
            }
        }
        $vote_activity_share_log = $this->vote_activity_image_upload('vote_activity_share_logo');
        if(!empty($vote_activity_share_log)) {
            $param['share_logo'] = $vote_activity_share_log;
            if(!empty($_POST['old_vote_activity_share_image'])) {
                $this->vote_activity_image_drop($_POST['old_vote_activity_share_image']);
            }
        }
//        print_R($_FILES);exit;

        if(!empty($_POST['vote_activity_image_all'])) {
            $vote_activity_image_all = array();
            foreach ($_POST['vote_activity_image_all'] as $value) {
                $image = array();
                $image['image_name'] = $value;
                $vote_activity_image_all[] = $image;
            }
            $param['image_all'] = serialize($vote_activity_image_all);
        } else {
            $param['image_all'] = '';
        }
        $param['content'] = $_POST['vote_activity_content'];
        $param['description'] = $_POST['vote_activity_description'];
        $param['start_time'] = $_POST['start_time'];
        $param['end_time'] = $_POST['end_time'].' 23:59:59';
        $param['vote_type'] = $_POST['vote_activity_vote_type'];
        $param['vote_num'] = $_POST['vote_activity_vote_num'];
        $param['vote_member_num'] = $_POST['vote_activity_vote_member_num'];
        $param['vote_explain'] = $_POST['vote_activity_explain'];
        $param['short_content'] = $_POST['vote_activity_short_content'];
//        $param['background_color'] = empty($_POST['exhibition_background_color'])?'#FFFFFF':$_POST['exhibition_background_color'];
//        $param['repeat'] = empty($_POST['exhibition_repeat'])?'no-repeat':$_POST['exhibition_repeat'];
        $param['modify_time'] = date('Y-m-d H:i:s');
        $admin_info = $this->getAdminInfo();
        $param['publish_id'] = $admin_info['id'];
//        if($_POST['exhibition_state'] == 'publish') {
//        } else {
//            $param['exhibition_state'] = 1;
//        }
        $model_vote_activity = Model('vote_activity');
        if(empty($_POST['vote_activity_id'])) {
            $param['add_time'] = date('Y-m-d H:i:s');
            $result = $model_vote_activity->save($param);
        } else {
            $model_vote_activity->modify($param, array('id'=>$_POST['vote_activity_id']));
            $result = $_POST['vote_activity_id'];
        }
//        dd();
        if($result) {
            if($_POST['vote_activity_state'] == 'publish') {
                $this->generate_html($result);
            }
            $this->log(Language::get('vote_activity_log_save').$result, 1);
            showMessage(Language::get('nc_common_save_succ'), self::VOTE_ACTIVITY_LINK);
        } else {
            $this->log(Language::get('vote_activity_log_save').$result, 0);
            showMessage(Language::get('nc_common_save_fail'), self::VOTE_ACTIVITY_LINK);
        }
    }

    /**
     * 专题详细页
     */
    public function exhibition_detailOp() {
        $this->get_exhibition_detail($_GET['special_id']);
    }

    private function get_exhibition_detail($special_id) {
        $model_exhibition = Model('exhibition_setting');
        $exhibition_detail = $model_exhibition->getOne(array('id'=>$special_id));
        Tpl::output('exhibition_detail', $exhibition_detail);
        Tpl::showpage('exhibition.detail', 'null_layout');
    }

    /**
     * cms生成静态文件
     */
    private function generate_html($special_id) {
        $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
        if(!is_dir($html_path)){
            if (!@mkdir($html_path, 0755)){
                showMessage(Language::get('exhibition_build_fail'),'','','error');
            }
        }

        ob_start();
        $this->get_exhibition_detail($special_id);
        $result = file_put_contents($html_path.md5('special'.$special_id).'.html', ob_get_clean());
        if(!$result) {
            showMessage(Language::get('exhibition_build_fail'),'','','error');
        }
    }

    /**
     * cms专题删除
     */
    public function exhibition_dropOp() {
        $condition = array();
        $condition['id'] = array('in', $_REQUEST['id']);
        $model_exhibition = Model('exhibition_setting');
        $list = $model_exhibition->getList($condition);
        if(!empty($list)) {
            $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
            foreach ($list as $value) {
                //删除图片
                $this->exhibition_image_drop($value['background']);
                $this->exhibition_image_drop($value['image']);
                $image_list = unserialize($value['image_all']);
                if(!empty($image_list)) {
                    foreach ($image_list as $value_image) {
                        $this->exhibition_image_drop($value_image['image_name']);
                    }
                }
                //删除静态文件
                $static_file = $html_path.md5('exhibition'.$value['id']).'.html';
                if(is_file($static_file)) {
                    unlink($static_file);
                }
            }
        }
        $result = $model_exhibition->drop($condition);
        if($result) {
            $this->log('删除会销'.$_REQUEST['id'], 1);
            showMessage(Language::get('nc_common_del_succ'),'');
        } else {
            $this->log('删除会销'.$_REQUEST['id'], 0);
            showMessage(Language::get('nc_common_del_fail'),'');
        }
    }

    /**
     * 上传图片
     */
    private function vote_activity_image_upload($image) {
        if(!empty($_FILES[$image]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'vote_activity');
            $result = $upload->upfile($image);
            if(!$result) {
                showMessage($upload->error);
            }
            return $upload->file_name;
        }
    }

    /**
     * 图片删除
     */
    private function vote_activity_image_drop($image) {
        $file = getVoteActivityImageUrl($image);
        if(is_file($file)) {
            unlink($file);
        }
    }

    /**
     * 图片上传
     */
    public function vote_activity_image_uploadOp() {
//        print_R($_FILES);exit;
        $type = $_GET['type'];
        $data = array();
        $data['status'] = 'success';
        if(!empty($_FILES[$type]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'vote_activity');

            $result = $upload->upfile($type);
            if(!$result) {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
            $data['file_name'] = $upload->file_name;
            $data['origin_file_name'] = $_FILES[$type]['name'];
            $data['file_url'] = getVoteActivityImageUrl($upload->file_name);
        }
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }

    /**
     * 专题图片删除
     */
    public function vote_activity_image_dropOp() {
        $data = array();
        $data['status'] = 'success';
        $this->vote_activity_image_drop($_GET['image_name']);
        echo json_encode($data);
    }

    /**
     * 图片商品添加
     */
    public function goods_info_by_urlOp() {
        $url = urldecode($_GET['url']);
        if(empty($url)) {
            self::return_json(Language::get('param_error'),'false');
        }
        $model_goods_info = Model('goods_info_by_url');
        $result = $model_goods_info->get_goods_info_by_url($url);
        if($result) {
            self::echo_json($result);
        } else {
            self::return_json(Language::get('param_error'),'false');
        }
    }

    /**
     * 获取专题状态列表
     */
    private function get_state_list() {
        $array = array();
        $array[self::SPECIAL_STATE_DRAFT] = Language::get('cms_text_draft');
        $array[self::SPECIAL_STATE_PUBLISHED] = Language::get('cms_text_published');
        return $array;
    }


    private function return_json($message,$result='true') {
        $data = array();
        $data['result'] = $result;
        $data['message'] = $message;
        self::echo_json($data);
    }

    private function echo_json($data) {
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }

    private function show_menu($menu_key) {
        $menu_array = array(
            'special_list'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_manage'),'menu_url'=>'index.php?act=cms_special&op=exhibition_list'),
            'special_add'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_new'),'menu_url'=>'index.php?act=cms_special&op=exhibition_add'),
            'special_edit'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_edit'),'menu_url'=>'###'),
        );
        if($menu_key != 'special_edit') {
            unset($menu_array['special_edit']);
        }
        $menu_array[$menu_key]['menu_type'] = 'text';
        Tpl::output('menu',$menu_array);
    }

}
