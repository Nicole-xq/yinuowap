
<?php
/**
 * 协议设定
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class mb_agreementControl extends SystemControl{
    public function __construct(){
        parent::__construct();
        Language::read('mobile');
    }

    public function indexOp() {
        $this->agreement_listOp();
    }

    /**
     * 列表展示
     * */
    public function agreement_listOp() {
        $model_mb_agreement = Model('mb_agreement');
        $mb_agreement_list = $model_mb_agreement->getMbAgreementList();
        foreach ($mb_agreement_list as $k => $v) {
            $mb_agreement_list[$k]['agreement_content'] = unserialize($v['agreement_content']);
        }
        Tpl::output('mb_agreement_list', $mb_agreement_list);

        Tpl::showpage('mb_agreement.list');
    }

    /**
     * 编辑
     * */
    public function agreement_editOp() {
        $payment_id = intval($_GET["agreement_id"]);
        $model_mb_agreement = Model('mb_agreement');
        $mb_agreement_info = $model_mb_agreement->getMbAgreementInfo(array('agreement_id' => $payment_id));
        Tpl::output('agreement', $mb_agreement_info);
        Tpl::showpage('mb_agreement.edit');
    }

    /**
     * 保存
     * */
    public function agreement_saveOp(){
        if (empty($_POST['agreement_id'])) showMessage('参数错误');

        $param  = array();
        $param['agreement_title'] = trim($_POST['agreement_title']);
        $param['agreement_content']= trim($_POST['agreement_content']);
        $param['last_modify_time']  = TIMESTAMP;
        /** @var mb_agreementModel $model_agreement */
        $model_agreement  = Model('mb_agreement');
        $condition = array('agreement_id' => intval($_POST['agreement_id']));
        $result = $model_agreement->editMbAgreement($param, $condition);
        if($result) {
            showMessage(Language::get('nc_common_save_succ'), urlAdminMobile('mb_agreement', 'agreement_list'));
        } else {
            showMessage(Language::get('nc_common_save_fail'));
        }

    }
    /**
     * 图片上传
     */
    public function agreement_image_uploadOp() {
        $type = $_GET['type'];
        $data = array();
        $data['status'] = 'success';
        if(!empty($_FILES[$type]['name'])) {
            $upload = new UploadFile();

            $upload->set('default_dir', 'agreement/img');

            $result = $upload->upfile('add_album');
            if(!$result) {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
            $data['file_name'] = $upload->file_name;
            $data['origin_file_name'] = $_FILES[$type]['name'];
            $data['file_url'] = UPLOAD_SITE_URL.DS.'agreement/img'.DS.$upload->file_name;
        }
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }
}