<?php
/**
 * 会销配置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class exhibition_settingControl extends SystemControl{

    const LINK_CMS_SPECIAL = 'index.php?act=exhibition_setting&op=exhibition_list';
    //专题状态草稿箱
    const SPECIAL_STATE_DRAFT = 1;
    //专题状态待审核
    const SPECIAL_STATE_PUBLISHED = 2;

    public function __construct(){
        parent::__construct();
        Language::read('cms');
    }

    public function indexOp() {
        $this->exhibition_listOp();
    }

    /**
     * 会销列表
     **/
    public function exhibition_listOp() {
        $this->show_menu('special_list');
        Tpl::showpage("exhibition_setting.index");
    }

    /**
     * cms专题列表
     **/
    public function exhibition_list_xmlOp() {
        $model_special = Model('exhibition_setting');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = array();
        $list = $model_special->getList($condition, $page, 'id desc');
        $state_list = $this->get_state_list();
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $fields_array = array('id','title','state');
            foreach ($list as $k => $v){
                $out_array = getFlexigridArray(array(),$fields_array,$v);
                $out_array['add_time'] = date('Y-m-d', $v['add_time']);
                $out_array['entry_end_time'] = $v['entry_end_time'] ? date('Y-m-d', $v['entry_end_time']) : '----';
//                $out_array['image'] = '<a href="javascript:;" class="pic-thumb-tip" onmouseout="toolTip()" onmouseover="toolTip(\'<img src='. ($v['image'] ? getCMSSpecialImageUrl($v['image']) : ADMIN_TEMPLATES_URL . '/images/preview.png'). '>\')"><i class="fa fa-picture-o"></i></a>';
                $out_array['state'] = $state_list[$v['state']];
                $operation = '';
//                $operation .= '<a href="javascript:;" class="btn red" onclick="fg_operation_del('.$v['id'].');"><i class="fa fa-trash-o"></i>删除</a>';
                $operation .= '<a href="index.php?act=exhibition_setting&op=exhibition_edit&id='.$v['id'].'" class="btn red" ><i class="fa fa-trash-o"></i>编辑</a>';
                $operation .= '<a href="http://test.yinuovip.com/wap/tmpl/member/exhibition.html?ex_id='.$v['id'].'" class="btn red" target="_blank"><i class="fa fa-trash-o"></i>查看</a>';
//                $operation .= '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';
////                if($v['state'] == '2') {
//                    $wap_url =  getCMSWapSpecialUrl($v['id']);
//                    $operation .= '<li><a href="'.$wap_url.'" target="_blank">查看wap专题页面</a></li>';
//                    $operation .= '<li><a href="'.$v['link'].'" target="_blank">查看pc专题页面</a></li>';
////                } else {
////                    $operation .= '<li><a href="index.php?act=cms_special&op=cms_detail&id='.$v['id'].'" target="_blank">预览专题页面</a></li>';
////                }
//                $operation .= '<li><a href="index.php?act=cms_special&op=exhibition_edit&special_id='.$v['id'].'">编辑专题内容</a></li>';
//                $operation .= '</ul></span>';
                $out_array['operation'] = $operation;
                $out_list[$v['id']] = $out_array;
            }
        }

        $data = array();
        $data['now_page'] = $model_special->shownowpage();
        $data['total_num'] = $model_special->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * cms专题添加
     **/
    public function exhibition_addOp() {
        $model_exhibition = Model('exhibition_setting');
        $this->show_menu('exhibition_add');
        Tpl::output('special_type_array', $model_exhibition->getSpecialTypeArray());
        Tpl::showpage('exhibition.add');
    }

    /**
     * cms专题编辑
     */
    public function exhibition_editOp() {
        $id = intval($_GET['id']);
        if(empty($id)) {
            showMessage(Language::get('param_error'),'','','error');
        }

        $model_exhibition = Model('exhibition_setting');
        $detail = $model_exhibition->getOne(array('id'=>$id));
        $detail['wap_url'] = "http://test.yinuovip.com/wap/tmpl/member/exhibition.html?ex_id={$id}";
        if(empty($detail)) {
            showMessage(Language::get('param_error'),'','','error');
        }
//        if(!empty($special_detail['special_mobile_content'])){
//            $special_detail['mb_body'] = unserialize($special_detail['special_mobile_content']);
//            if (is_array($special_detail['mb_body'])) {
//                $mobile_body = '[';
//                foreach ($special_detail['mb_body'] as $val ) {
//                    $mobile_body .= '{"type":"' . $val['type'] . '","value":"' . $val['value'] . '"},';
//                }
//                $mobile_body = rtrim($mobile_body, ',') . ']';
//            }
//            $special_detail['special_mobile_content'] = $mobile_body;
//        }
//        print_R(json_decode($special_detail['special_mobile_content'],true));
//        echo 22;
//print_R($special_detail);exit;
//        $detail['wap_url'] = getCMSWapSpecialUrl($special_id);
//        $special_detail['pc_url'] = getCMSSpecialUrl($special_id);
        $detail['entry_end_time'] = date('Y-m-d',$detail['entry_end_time']);

        Tpl::output('detail', $detail);
//        Tpl::output('special_type_array', $model_special->getSpecialTypeArray());
        $this->show_menu('special_edit');
        Tpl::showpage('exhibition.add');
    }

    /**
     * cms专题保存
     **/
    public function exhibition_saveOp() {
//        print_R($_POST);exit;
        $param = array();
        $param['title'] = $_POST['exhibition_title'];
        $special_image = $this->exhibition_image_upload('special_image');
        if(!empty($special_image)) {
            $param['image'] = $special_image;
            if(!empty($_POST['old_exhibition_image'])) {
                $this->exhibition_image_drop($_POST['old_exhibition_image']);
            }
        }
        $exhibition_share_log = $this->exhibition_image_upload('exhibition_share_logo');
        if(!empty($exhibition_share_log)) {
            $param['share_logo'] = $exhibition_share_log;
            if(!empty($_POST['old_exhibition_share_image'])) {
                $this->exhibition_image_drop($_POST['old_exhibition_share_image']);
            }
        }
//        print_R($_FILES);exit;
        $exhibition_background = $this->exhibition_image_upload('exhibition_background');
        if(!empty($exhibition_background)) {
            $param['background'] = $exhibition_background;
            if(!empty($_POST['old_exhibition_background'])) {
                $this->exhibition_image_drop($_POST['old_exhibition_background']);
            }
        }
        if(!empty($_POST['exhibition_image_all'])) {
            $exhibition_image_all = array();
            foreach ($_POST['exhibition_image_all'] as $value) {
                $image = array();
                $image['image_name'] = $value;
                $exhibition_image_all[] = $image;
            }
            $param['image_all'] = serialize($exhibition_image_all);
        } else {
            $param['image_all'] = '';
        }
        $param['margin_top'] = intval($_POST['exhibition_margin_top']);
        $param['content'] = $_POST['exhibition_content'];
        $param['description'] = $_POST['exhibition_description'];
        $param['background_color'] = empty($_POST['exhibition_background_color'])?'#FFFFFF':$_POST['exhibition_background_color'];
//        $param['repeat'] = empty($_POST['exhibition_repeat'])?'no-repeat':$_POST['exhibition_repeat'];
        $param['modify_time'] = time();
        $param['entry_end_time'] = _POST['entry_end_time'] ? strtotime($_POST['entry_end_time'].' 23:59:59') : '';
        $admin_info = $this->getAdminInfo();
        $param['publish_id'] = $admin_info['id'];
//        if($_POST['exhibition_state'] == 'publish') {
            $param['state'] = 2;
//        } else {
//            $param['exhibition_state'] = 1;
//        }
        $model_exhibition = Model('exhibition_setting');
        if(empty($_POST['exhibition_id'])) {
            $param['add_time'] = time();
            $result = $model_exhibition->save($param);
        } else {
            $model_exhibition->modify($param, array('id'=>$_POST['exhibition_id']));
            $result = $_POST['exhibition_id'];
        }
//        dd();
        if($result) {
            if($_POST['exhibition_state'] == 'publish') {
                $this->generate_html($result);
            }
            $this->log(Language::get('cms_log_special_save').$result, 1);
            showMessage(Language::get('nc_common_save_succ'), self::LINK_CMS_SPECIAL);
        } else {
            $this->log(Language::get('cms_log_special_save').$result, 0);
            showMessage(Language::get('nc_common_save_fail'), self::LINK_CMS_SPECIAL);
        }
    }

    /**
     * 专题详细页
     */
    public function exhibition_detailOp() {
        $this->get_exhibition_detail($_GET['special_id']);
    }

    private function get_exhibition_detail($special_id) {
        $model_exhibition = Model('exhibition_setting');
        $exhibition_detail = $model_exhibition->getOne(array('id'=>$special_id));
        Tpl::output('exhibition_detail', $exhibition_detail);
        Tpl::showpage('exhibition.detail', 'null_layout');
    }

    /**
     * cms生成静态文件
     */
    private function generate_html($special_id) {
        $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
        if(!is_dir($html_path)){
            if (!@mkdir($html_path, 0755)){
                showMessage(Language::get('exhibition_build_fail'),'','','error');
            }
        }

        ob_start();
        $this->get_exhibition_detail($special_id);
        $result = file_put_contents($html_path.md5('special'.$special_id).'.html', ob_get_clean());
        if(!$result) {
            showMessage(Language::get('exhibition_build_fail'),'','','error');
        }
    }

    /**
     * cms专题删除
     */
    public function exhibition_dropOp() {
        $condition = array();
        $condition['id'] = array('in', $_REQUEST['id']);
        $model_exhibition = Model('exhibition_setting');
        $list = $model_exhibition->getList($condition);
        if(!empty($list)) {
            $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
            foreach ($list as $value) {
                //删除图片
                $this->exhibition_image_drop($value['background']);
                $this->exhibition_image_drop($value['image']);
                $image_list = unserialize($value['image_all']);
                if(!empty($image_list)) {
                    foreach ($image_list as $value_image) {
                        $this->exhibition_image_drop($value_image['image_name']);
                    }
                }
                //删除静态文件
                $static_file = $html_path.md5('exhibition'.$value['id']).'.html';
                if(is_file($static_file)) {
                    unlink($static_file);
                }
            }
        }
        $result = $model_exhibition->drop($condition);
        if($result) {
            $this->log('删除会销'.$_REQUEST['id'], 1);
            showMessage(Language::get('nc_common_del_succ'),'');
        } else {
            $this->log('删除会销'.$_REQUEST['id'], 0);
            showMessage(Language::get('nc_common_del_fail'),'');
        }
    }

    /**
     * 上传图片
     */
    private function exhibition_image_upload($image) {
        if(!empty($_FILES[$image]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'exhibition');
            $result = $upload->upfile($image);
            if(!$result) {
                showMessage($upload->error);
            }
            return $upload->file_name;
        }
    }

    /**
     * 图片删除
     */
    private function exhibition_image_drop($image) {
        $file = getCMSSpecialImagePath($image);
        if(is_file($file)) {
            unlink($file);
        }
    }

    /**
     * 图片上传
     */
    public function exhibition_image_uploadOp() {
//        print_R($_FILES);exit;
        $type = $_GET['type'];
        $data = array();
        $data['status'] = 'success';
        if(!empty($_FILES[$type]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'exhibition');

            $result = $upload->upfile($type);
            if(!$result) {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
            $data['file_name'] = $upload->file_name;
            $data['origin_file_name'] = $_FILES[$type]['name'];
            $data['file_url'] = getExhibitionImageUrl($upload->file_name);
        }
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }

    /**
     * 专题图片删除
     */
    public function exhibition_image_dropOp() {
        $data = array();
        $data['status'] = 'success';
        $this->exhibition_image_drop($_GET['image_name']);
        echo json_encode($data);
    }

    /**
     * 图片商品添加
     */
    public function goods_info_by_urlOp() {
        $url = urldecode($_GET['url']);
        if(empty($url)) {
            self::return_json(Language::get('param_error'),'false');
        }
        $model_goods_info = Model('goods_info_by_url');
        $result = $model_goods_info->get_goods_info_by_url($url);
        if($result) {
            self::echo_json($result);
        } else {
            self::return_json(Language::get('param_error'),'false');
        }
    }

    /**
     * 获取专题状态列表
     */
    private function get_state_list() {
        $array = array();
        $array[self::SPECIAL_STATE_DRAFT] = Language::get('cms_text_draft');
        $array[self::SPECIAL_STATE_PUBLISHED] = Language::get('cms_text_published');
        return $array;
    }


    private function return_json($message,$result='true') {
        $data = array();
        $data['result'] = $result;
        $data['message'] = $message;
        self::echo_json($data);
    }

    private function echo_json($data) {
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }

    private function show_menu($menu_key) {
        $menu_array = array(
            'special_list'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_manage'),'menu_url'=>'index.php?act=cms_special&op=exhibition_list'),
            'special_add'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_new'),'menu_url'=>'index.php?act=cms_special&op=exhibition_add'),
            'special_edit'=>array('menu_type'=>'link','menu_name'=>Language::get('nc_edit'),'menu_url'=>'###'),
        );
        if($menu_key != 'special_edit') {
            unset($menu_array['special_edit']);
        }
        $menu_array[$menu_key]['menu_type'] = 'text';
        Tpl::output('menu',$menu_array);
    }

}
