<?php
/**
 * APP公告--uMeng推送
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class mb_broadcastControl extends SystemControl{
    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('mb_broadcast.list');
    }

    /**
     * 推送通知列表
     */
    public function get_list_xmlOp() {
        $model_broadcast = Model('app_broadcast');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $list = $model_broadcast->getBroadcastList([],'*',$page);
        $out_list = array();
        foreach ($list as $k => $v){
            if ($v['state'] == 1){
                $operation = '';
                $state = '已发布';
            }else{
                $operation = '<a class="btn orange" href="index.php?act=mb_broadcast&op=add_broadcast&id='.$v['id'].'"><i class="fa fa-pencil-square-o"></i>编辑</a><a class="btn orange" href="index.php?act=mb_broadcast&op=send_broadcast&id='.$v['id'].'"><i class="fa fa-bullhorn"></i>发布</a>';
                $state = '未发布';
            }
            $out_array = [
                'title'=> $v['title'],
                'subtitle'=> $v['subtitle'],
                'content'=> $v['content'],
                'link'=> $v['link'],
                'state'=> $state,
                'admin_name'=> $v['admin_name'],
                'add_time'=> date('Y-m-d H:i:s',$v['add_time']),
                'operation'=> $operation,
            ];
            $out_list[$v['id']] = $out_array;
        }
        $data = array();
        $data['now_page'] = $model_broadcast->shownowpage();
        $data['total_num'] = $model_broadcast->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }


    /**
     * 新增公告
     */
    public function add_broadcastOp() {
        $model_broadcast = Model('app_broadcast');
        if (chksubmit()) {
            $_array = array();
            $_array['title'] = $_POST['title'];
            $_array['subtitle'] = $_POST['subtitle'];
            $_array['content'] = $_POST['content'];
            $_array['link'] = $_POST['link'];
            $_array['admin_name'] = $this->admin_info['name'];;
            $_array['add_time'] = time();
            if ($_POST['id']){
                $broadcast_id = $model_broadcast->editBroadcast(['id'=>intval($_POST['id'])], $_array) ;
                $msg_log = '编辑手机端推送公告，编号'.$_POST['id'];
            }else{
                $broadcast_id = $model_broadcast->addBroadcast($_array);
                $msg_log = '新增手机端推送公告，编号'.$_POST['id'];
            }
            if ($broadcast_id) {
                $this->log($msg_log);
                showMessage(Language::get('nc_common_save_succ'),'index.php?act=mb_broadcast&op=index');
            } else {
                showMessage(Language::get('nc_common_save_fail'));
            }
        }
        if ($_GET['id']){
            $broadcast_info = $model_broadcast->getBroadcastInfo(['id'=>intval($_GET['id'])]);
            Tpl::output('broadcast_info',$broadcast_info);
        }
        Tpl::showpage('mb_broadcast.add');
    }

    /**
     * 新增公告
     */
    public function send_broadcastOp() {
        if (empty($_GET['id'])){
            showMessage('发布参数有无');
        }
        $model_broadcast = Model('app_broadcast');
        $broadcast_info = $model_broadcast->getBroadcastInfo(['id'=>intval($_GET['id'])]);
        if (empty($broadcast_info)){
            showMessage('无效发布');
        }
        Logic('app_push_message')->sendAppBroadCast($broadcast_info);
        $res_data = $model_broadcast->editBroadcast(['id'=>intval($_GET['id'])], ['state'=>1]);
        if ($res_data) {
            showMessage('已发布','index.php?act=mb_broadcast&op=index');
        } else {
            showMessage('未发布');
        }
    }

    /**
     * 单播
     */
    public function android_unicastOp() {
        $message_body = [
            'device_tokens'=>'AmHYrXKcuTpsp4j3K2aNXIBeu7MjtpqAzfVIemroiMAr',
            'ticker'=>'测试ticker',
            'title'=>'测试单播',
            'text'=>'通知文字描述',
            'after_open'=>'go_app',
            'production_mode'=>true,
            'extra'=>[
                'url'=>'http://app.test.yinuovip.com/wap/tmpl/member/predepositlog.html',
            ],
        ];
        $response = Logic('app_push_message')->androidUnicast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            //output_error('单播失败');
        }

    }

    /**
     * 组播
     */
    public function android_groupcastOp() {
        $message_body = [
            'ticker'=>'测试ticker',
            'title'=>'测试单播',
            'text'=>'通知文字描述',
            'after_open'=>'go_app',
            'production_mode'=>true,
            'where'=>[
                ['member_id'=>'104676']
            ],
            'extra'=>[
                'url'=>'http://app.test.yinuovip.com/wap/tmpl/member/predepositlog.html',
            ],
        ];
        $response = Logic('app_push_message')->androidGroupcast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            //output_error('单播失败');
        }
    }

    /**
     * 自定义广播
     */
    public function android_customizedcastOp() {
        $message_body = [
            'ticker'=>'测试ticker收到回复',
            'title'=>'付款成功提醒',
            'text'=>'付款成功提醒描述111',
            'after_open'=>'go_app',
            'production_mode'=>true,
            'alias_type'=>'YiNuoApp',
            'alias'=>'104584',
            'extra'=>[
                'url'=>'http://app.test.yinuovip.com/wap/tmpl/member/prede',
            ],
        ];
        $response = Logic('app_push_message')->androidCustomizedcast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            //output_error('单播失败');
        }
    }

    /**
     * 广播
     */
    public function android_broadcastOp() {
        $message_body = [
            'appkey'=>'5a3a1e57b27b0a38630003ae',
            'app_master_secret'=>'jgu9rblmv0pudsbzneizpc8tc6gachnv',
            'timestamp'=>'1516937939',
            'ticker'=>'android test broadcast',
            'title'=>'Android广播测试',
            'text'=>'通知文字描述',
            'after_open'=>'go_app',
            'production_mode'=>true,
            'extra'=>['test'=>'helloworld'],
        ];

        $response = Logic('app_push_message')->androidBroadcast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            //output_error('单播失败');
        }
    }
    /**
     * 单播
     */
    public function ios_unicastOp() {
        $message_body = [
            'device_tokens'=>'391ffda167bebb2122648cde1183ee089d625c836c787b90274027870fab9dcf',
            'alert'=>[
                'title'=>'IOS 个性化测试',
                'subtitle'=>'IOS subtitle设置数据提示',
                'body'=>'IOS body通知文字描述到回复'
            ],
            'alias_type'=>'YiNuoApp',
            'alias'=>'95810',
            'badge'=> 0,
            'sound'=>'chime',
            'production_mode'=>true,
            'extra'=>[
                'url'=>'http://app.test.yinuovip.com/wap/tmpl/member/predepositlog.html',
            ],
        ];


        $response = Logic('app_push_message')->iOSUnicast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            // output_error('单播失败');
        }
    }
    /**
     * 广播
     */
    public function ios_broadcastOp() {
        $message_body = [
            'appkey'=>'5a3a1e57b27b0a38630003ae',
            'app_master_secret'=>'jgu9rblmv0pudsbzneizpc8tc6gachnv',
            'timestamp'=>'1516937939',
            'alert'=>'IOS 广播测试',
            'badge'=> 0,
            'sound'=>'chime',
            'production_mode'=>true,
            'extra'=>['test'=>'helloworld'],
        ];

        $response = Logic('app_push_message')->iOSBroadcast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['dasta']);
        }else{
            // output_error('单播失败');
        }
    }

    /**
     * IOS自定义广播
     */
    public function ios_customizedcastOp() {
        $message_body = [
            'alert'=>[
                'title'=>'付款成功提醒',
                'subtitle'=>'IOS subtitle设置数据提示',
                'body'=>'付款成功提醒描述111'
            ],
            'badge'=> 0,
            'sound'=>'chime',
            'production_mode'=>true,
            'alias_type'=>'YiNuoApp',
            'alias'=>'104676',
            'extra'=>[
                'url'=>'http://app.test.yinuovip.com/wap/tmpl/member/predepositlog.html',
            ],
        ];
        $response = Logic('app_push_message')->iosCustomizedcast($message_body);
        // {"ret":"SUCCESS","data":{"msg_id":"uuhjanq151668748636901"}}
        if ($response['ret'] == 'SUCCESS'){
            output_data($response['data']);
        }else{
            //output_error('自定义广播失败');
        }
    }

}
