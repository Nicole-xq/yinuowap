<?php
/**
 * 引导页设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class mb_boot_pageControl extends SystemControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 引导页列表页
     */
    public function indexOp(){
        Tpl::showpage('mb_boot_page.list');
    }

    /**
     * 获取引导页数据
     */
    public function get_boot_xmlOp(){
        $model_boot = Model('mb_boot_page');
        $condition = array();
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $boot_list = $model_boot->getBootPageList($condition,$page);
        $false_flag = '<span class="no"><i class="fa fa-ban"></i>否</span>';
        $true_flag = '<span class="yes"><i class="fa fa-check-circle"></i>是</span>';
        foreach ($boot_list as $k => &$v){
            $operation = "<a class='btn blue' href='index.php?act=mb_boot_page&op=boot_add&id=".$v['id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            array_unshift($v,$operation);
            $boot_image = UPLOAD_SITE_URL.'/'.ATTACH_MOBILE.'/boot'.'/'.$v['boot_image'];
            $v['boot_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".$boot_image.">\")'><i class='fa fa-picture-o'></i></a>";
            $v['boot_state'] = $v['boot_state'] == 1 ? $false_flag : $true_flag;
            $v['share_state'] = $v['share_state'] == 1 ? $false_flag : $true_flag;
            $v['add_time'] = date('Y-m-d H:i',$v['add_time']);
            $v['update_time'] = $v['update_time'] > 0 ? date('Y-m-d H:i',$v['update_time']) : '---';
        }
        unset($v);
        $data = array();
        $data['now_page'] = $model_boot->shownowpage();
        $data['total_num'] = $model_boot->gettotalnum();
        $data['list'] = $boot_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 新增引导页
     */
    public function boot_addOp() {
        $model_boot = Model('mb_boot_page');
        if (chksubmit()) {
            $addData = [
                'title'      => $_POST['title'],
                'wap_link'   => $_POST['wap_link'],
                'share_state'=> intval($_POST['share_state']),
                'boot_state' => intval($_POST['boot_state']),
                'share_desc' => strval($_POST['share_desc'])
            ];
            intval($_POST['id']) > 0 ? $addData['update_time'] = time() : $addData['add_time'] = time();
            //上传图片
            if ($_FILES['boot_image']['name'] != '') {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_MOBILE . '/boot');
                $result = $upload->upfile('boot_image');
                if ($result) {
                    $addData['boot_image'] = $upload->file_name;
                } else {
                    showMessage($upload->error);
                }
            }
            $result = intval($_POST['id']) > 0 ? $model_boot->updateBootData(['id'=>intval($_POST['id'])],$addData) : $model_boot->addData($addData);
            if ($result) {
                $id = $_POST['id'] ? $_POST['id'] : $result;
                intval($_POST['boot_state']) == 2 ? $model_boot->updateBootData(['id'=>['neq',$id]],['boot_state'=>1]) : '';
                showMessage(Language::get('nc_common_save_succ'),'index.php?act=mb_boot_page&op=index');
            } else {
                showMessage(Language::get('nc_common_save_fail'));
            }
        }
        if (intval($_GET['id']) > 0){
            $model_boot = Model('mb_boot_page');
            $boot_info = $model_boot->getBootPageInfo(['id'=>intval($_GET['id'])], '*');
            Tpl::output('boot_info',$boot_info);
        }
        Tpl::showpage('mb_boot.add');
    }

}