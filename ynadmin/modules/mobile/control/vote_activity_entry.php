<?php
/**
 * 投票活动报名
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class vote_activity_entryControl extends SystemControl{

    const vote_activity_entry_LINK = 'index.php?act=vote_activity_entry&op=vote_activity_entry_list';
    public $state_arr = array(
        0=>'待审核',
        1=>'审核通过',
        2=>'审核不通过',
        3=>'获奖',
        4=>'未获奖'
    );

    public function __construct(){
        parent::__construct();
        Language::read('cms');
    }

    public function indexOp() {
        $this->vote_activity_entry_listOp();
    }

    /**
     * 会销列表
     **/
    public function vote_activity_entry_listOp() {
        Tpl::showpage("vote_activity_entry.index");
    }

    /**
     * cms专题列表
     **/
    public function vote_activity_entry_list_xmlOp() {
        $model_vote_activity_entry = Model('vote_activity_entry');
        $model_activity = Model('vote_activity');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = array();
        $list = $model_vote_activity_entry->getList($condition, $page, 'id desc');
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $fields_array = array('id','title');
            foreach ($list as $k => $v){
                $out_array['operation'] = '';
                $out_array['id'] = $v['id'];
                $activity_info = $model_activity->getInfo(array('id'=>$v['activity_id']));
                $out_array['activity_id'] = $activity_info['id'];
                $out_array['title'] = $activity_info['title'];
                $out_array['entry_no'] = $v['entry_no'];
                $out_array['member_id'] = $v['member_id'];
                $out_array['state'] = $this->state_arr[$v['state']];
                $out_array['cdate'] =$v['add_time'];
                $operation = '';
                $operation .= '<a href="index.php?act=vote_activity_entry&op=vote_activity_entry_edit&id='.$v['id'].'" class="btn red" ><i class="fa fa-trash-o"></i>编辑</a>';
                $out_array['operation'] = $operation;
                $out_list[$v['id']] = $out_array;
            }
        }
//print_R($out_list);exit;
        $data = array();
        $data['now_page'] = $model_activity->shownowpage();
        $data['total_num'] = $model_activity->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 添加活动
     **/
    public function vote_activity_entry_addOp() {
        $model_vote_activity = Model('vote_activity');
        $list = $model_vote_activity->getList(array('end_time'=>array('gt',date('Y-m-d H:i:s'))));
//        dd();
        Tpl::output('activity',$list);
//        $model_vote_activity_entry = Model('vote_activity_entry');
//        $this->show_menu('exhibition_add');
//        Tpl::output('special_type_array', $model_vote_activity_entry->getSpecialTypeArray());
        Tpl::showpage('vote_activity_entry.add');
    }

    /**
     * cms专题编辑
     */
    public function vote_activity_entry_editOp() {
        $id = intval($_GET['id']);
        if(empty($id)) {
            showMessage(Language::get('param_error'),'','','error');
        }

        $model_vote_activity_entry = Model('vote_activity_entry');
        $detail = $model_vote_activity_entry->getInfo(array('id'=>$id));
        $detail['wap_url'] = "http://test.yinuovip.com/wap/tmpl/member/exhibition.html?ex_id={$id}";
        if(empty($detail)) {
            showMessage(Language::get('param_error'),'','','error');
        }
        Tpl::output('detail', $detail);
        Tpl::showpage('vote_activity_entry.add');
    }

    /**
     * 编辑保存
     **/
    public function vote_activity_entry_saveOp() {
//        print_R($_POST);exit;
        $model_vote_activity_entry = Model('vote_activity_entry');
        if(empty($_POST['vote_activity_entry_id'])) {
            $member_id = $_POST['member_id'];
            $activity_id = $_POST['activity_id'];
            $tmp = $model_vote_activity_entry->getInfo(array('member_id'=>$member_id,'activity_id'=>$activity_id));
            if(!empty($tmp)){
                showMessage('用户不可重复报名', self::vote_activity_entry_LINK);
            }
        }
        $param = array();
        $special_image = $this->vote_activity_entry_image_upload('image');
        if(!empty($special_image)) {
            $param['image'] = $special_image;
            if(!empty($_POST['old_vote_activity_entry_image'])) {
                $this->vote_activity_entry_image_drop($_POST['old_vote_activity_entry_image']);
            }
        }
//        $vote_activity_entry_share_log = $this->vote_activity_entry_image_upload('vote_activity_entry_share_logo');
//        if(!empty($vote_activity_entry_share_log)) {
//            $param['share_logo'] = $vote_activity_entry_share_log;
//            if(!empty($_POST['old_vote_activity_entry_share_image'])) {
//                $this->vote_activity_entry_image_drop($_POST['old_vote_activity_entry_share_image']);
//            }
//        }
//        print_R($_FILES);exit;

        if(!empty($_POST['vote_activity_entry_image_all'])) {
            $vote_activity_entry_image_all = array();
            foreach ($_POST['vote_activity_entry_image_all'] as $value) {
                $image = array();
                $image['image_name'] = $value;
                $vote_activity_entry_image_all[] = $image;
            }
            $param['image_all'] = serialize($vote_activity_entry_image_all);
        } else {
            $param['image_all'] = '';
        }
        $param['description'] = $_POST['vote_activity_entry_content'];
        $param['auther'] = $_POST['auther'];
        $param['name'] = $_POST['name'];
//        $param['description'] = $_POST['vote_activity_entry_description'];
//        $param['background_color'] = empty($_POST['exhibition_background_color'])?'#FFFFFF':$_POST['exhibition_background_color'];
//        $param['repeat'] = empty($_POST['exhibition_repeat'])?'no-repeat':$_POST['exhibition_repeat'];
        $param['modify_time'] = date('Y-m-d H:i:s');
        $admin_info = $this->getAdminInfo();
        $param['publish_id'] = $admin_info['id'];
//        if($_POST['exhibition_state'] == 'publish') {
//        } else {
//            $param['exhibition_state'] = 1;
//        }
        $model_vote_activity_entry = Model('vote_activity_entry');
        $tmp = 4;//审核通过不会超过万人....
        $entry_no = $_POST['activity_id'];
        $count = $model_vote_activity_entry->getCount(array('activity_id'=>$_POST['activity_id'],'state'=>1));
        if(strlen($count)<$tmp){
            for($i = 0;$i<$tmp-strlen($count);$i++){
                $entry_no .='0';
            }
        }
        $entry_no .=$count+1;
        if(empty($_POST['vote_activity_entry_id'])) {

//            $param['entry_no'] = $entry_no;
            $param['add_time'] = date('Y-m-d H:i:s');
            $param['activity_id'] = $_POST['activity_id'];
            $param['member_id'] = $_POST['member_id'];
            $result = $model_vote_activity_entry->save($param);
        } else {
            switch($_POST['vote_activity_entry_state']){
                case 'check_false':
                    $model_vote_activity_entry->modify(array('state'=>2), array('id'=>$_POST['vote_activity_entry_id']));
                    break;
                default :
                    $param['entry_no'] = $entry_no;
                    $param['state'] = 1;
                    $model_vote_activity_entry->modify($param, array('id'=>$_POST['vote_activity_entry_id']));
                    break;
            }
            $result = $_POST['vote_activity_entry_id'];
            if($result){
                $info = $model_vote_activity_entry->getInfo(array('id'=>$result));
                $member = Model('member')->getMemberInfo(array('member_id'=>$info['member_id']));
                $param = array();
                $param['code'] = $_POST['vote_activity_entry_state'] == 'check_false'?'vote_activity_check_fail_notice':'vote_activity_check_success_notice';
                $param['member_id'] = $member['member_id'];
                $param['number']['mobile'] = $member['member_mobile'];
                $param['param'] = array();
                QueueClient::push('sendMemberMsg', $param);
            }
        }
//        dd();
        if($result) {

//            if($_POST['vote_activity_entry_state'] == 'publish') {
//                $this->generate_html($result);
//            }

            $this->log(Language::get('vote_activity_entry_log_save').$result, 1);
            showMessage(Language::get('nc_common_save_succ'), self::vote_activity_entry_LINK);
        } else {
            $this->log(Language::get('vote_activity_entry_log_save').$result, 0);
            showMessage(Language::get('nc_common_save_fail'), self::vote_activity_entry_LINK);
        }
    }

    /**
     * 专题详细页
     */
    public function exhibition_detailOp() {
        $this->get_exhibition_detail($_GET['special_id']);
    }

    private function get_exhibition_detail($special_id) {
        $model_exhibition = Model('exhibition_setting');
        $exhibition_detail = $model_exhibition->getOne(array('id'=>$special_id));
        Tpl::output('exhibition_detail', $exhibition_detail);
        Tpl::showpage('exhibition.detail', 'null_layout');
    }

    /**
     * cms生成静态文件
     */
    private function generate_html($special_id) {
        $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
        if(!is_dir($html_path)){
            if (!@mkdir($html_path, 0755)){
                showMessage(Language::get('exhibition_build_fail'),'','','error');
            }
        }

        ob_start();
        $this->get_exhibition_detail($special_id);
        $result = file_put_contents($html_path.md5('special'.$special_id).'.html', ob_get_clean());
        if(!$result) {
            showMessage(Language::get('exhibition_build_fail'),'','','error');
        }
    }

    /**
     * cms专题删除
     */
    public function exhibition_dropOp() {
        $condition = array();
        $condition['id'] = array('in', $_REQUEST['id']);
        $model_exhibition = Model('exhibition_setting');
        $list = $model_exhibition->getList($condition);
        if(!empty($list)) {
            $html_path = BASE_UPLOAD_PATH.DS.ATTACH_CMS.DS.'special_html'.DS;
            foreach ($list as $value) {
                //删除图片
                $this->exhibition_image_drop($value['background']);
                $this->exhibition_image_drop($value['image']);
                $image_list = unserialize($value['image_all']);
                if(!empty($image_list)) {
                    foreach ($image_list as $value_image) {
                        $this->exhibition_image_drop($value_image['image_name']);
                    }
                }
                //删除静态文件
                $static_file = $html_path.md5('exhibition'.$value['id']).'.html';
                if(is_file($static_file)) {
                    unlink($static_file);
                }
            }
        }
        $result = $model_exhibition->drop($condition);
        if($result) {
            $this->log('删除会销'.$_REQUEST['id'], 1);
            showMessage(Language::get('nc_common_del_succ'),'');
        } else {
            $this->log('删除会销'.$_REQUEST['id'], 0);
            showMessage(Language::get('nc_common_del_fail'),'');
        }
    }

    /**
     * 上传图片
     */
    private function vote_activity_entry_image_upload($image) {
        if(!empty($_FILES[$image]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'vote_activity_entry');
            $result = $upload->upfile($image);
            if(!$result) {
                showMessage($upload->error);
            }
            return $upload->file_name;
        }
    }


    /**
     * 图片删除
     */
    private function vote_activity_entry_image_drop($image) {
        $file = getVoteActivityEntryImageUrl($image);
        if(is_file($file)) {
            unlink($file);
        }
    }

    /**
     * 图片上传
     */
    public function vote_activity_entry_image_uploadOp() {
//        print_R($_FILES);exit;
        $type = $_GET['type'];
        $data = array();
        $data['status'] = 'success';
        if(!empty($_FILES[$type]['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_CMS.DS.'vote_activity_entry');

            $result = $upload->upfile($type);
            if(!$result) {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
            $data['file_name'] = $upload->file_name;
            $data['origin_file_name'] = $_FILES[$type]['name'];
            $data['file_url'] = getVoteActivityEntryImageUrl($upload->file_name);
        }
        if (strtoupper(CHARSET) == 'GBK'){
            $data = Language::getUTF8($data);//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        }
        echo json_encode($data);
    }

    /**
     * 专题图片删除
     */
    public function vote_activity_entry_image_dropOp() {
        $data = array();
        $data['status'] = 'success';
        $this->vote_activity_entry_image_drop($_GET['image_name']);
        echo json_encode($data);
    }

    /**
     * 图片商品添加
     */
    public function goods_info_by_urlOp() {
        $url = urldecode($_GET['url']);
        if(empty($url)) {
            self::return_json(Language::get('param_error'),'false');
        }
        $model_goods_info = Model('goods_info_by_url');
        $result = $model_goods_info->get_goods_info_by_url($url);
        if($result) {
            self::echo_json($result);
        } else {
            self::return_json(Language::get('param_error'),'false');
        }
    }

    public function get_member_infoOp(){
        $name = $_GET['user_name'];
        $member_info = Model('member')->getMemberInfo(array('member_name'=>$name),'member_name,member_mobile,member_id,member_type');
        if(!empty($member_info)){
            $top_member = Logic('j_member_distribute')->get_top_member($member_info['member_id']);
            $top_member_info = array();
            if(!empty($top_member)){
                $top_member_info = Model('member')->getMemberInfo(array('member_id'=>$top_member['top_member']),'member_type');
            }
            $member_info['top_member'] = !empty($top_member)?$top_member['top_member_name']:'';
            $member_info['member_type_name'] = $this->distribute_str[$member_info['member_type']];
            $member_info['top_member_type_name'] = $this->distribute_str[$top_member_info['member_type']];
            $distribute_list = Model('distribution_config')->getConfigList(array('id'=>array('in','1,2,3')));
            foreach($distribute_list as $k=>$v){
                if($v['type_id'] <= $member_info['member_type']){
                    unset($distribute_list[$k]);
                }
            }
            $member_info['type_list'] = $distribute_list;
        }
        echo json_encode($member_info);exit;
    }


}
