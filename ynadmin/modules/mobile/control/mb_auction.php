<?php
/**
 * 手机拍卖首页专题
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class mb_auctionControl extends SystemControl{
    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
//        $this->className_listOp();
        $this->special_editOp();
    }

    /**
     * 分类名称列表
     */
    public function className_listOp() {
        Tpl::showpage('mb_className.list');
    }

    /**
     * 输出分类名称列表XML数据
     */
    public function get_className_xmlOp() {
        $model_mb_special = Model('mb_class');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = [];
        $list = $model_mb_special->getMbClassList($condition,$page);
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $fields_array = array('mb_class_id','mb_class_name');
            foreach ($list as $k => $v){
                $out_array = getFlexigridArray(array(),$fields_array,$v);
                $out_array['mb_class_name'] = '<span nc_type="edit_class_name" column_id="'.$v['mb_class_id'].
                    '" title="可编辑" class="editable tooltip w270">'.$v['mb_class_name'].'</span>';
                $operation = '';
                if($v['mb_class_id'] != 1){
                    $operation .= '<a class="btn red" href="javascript:fg_operation_del('.$v['mb_class_id'].');"><i class="fa fa-trash-o"></i>删除</a>';
                }
                $operation .= '<a class="btn blue" href="'.urlAdminMobile('mb_auction', 'special_edit', array('mb_class_id' => $v['mb_class_id'])).'"><i class="fa fa-pencil-square-o"></i>编辑</a>';
                $out_array['operation'] = $operation;
                $out_list[$v['mb_class_id']] = $out_array;
            }
        }

        $data = array();
        $data['now_page'] = $model_mb_special->shownowpage();
        $data['total_num'] = $model_mb_special->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 保存分类名称
     */
    public function class_saveOp() {
        $model_mb_special = Model('mb_class');

        $param = array();
        $param['mb_class_name'] = $_POST['mb_class_name'];
        $result = $model_mb_special->addMbClass($param);
        if($result) {
            $this->log('添加手机拍卖分类名称' . '[ID:' . $result. ']', 1);
            showMessage(L('nc_common_save_succ'), urlAdminMobile('mb_auction', 'className_list'));
        } else {
            $this->log('添加手机拍卖分类名称' . '[ID:' . $result. ']', 0);
            showMessage(L('nc_common_save_fail'), urlAdminMobile('mb_auction', 'className_list'));
        }
    }

    /**
     * 编辑分类名称
     */
    public function update_class_nameOp() {
        $model_mb_special = Model('mb_class');

        $param = array();
        $param['mb_class_name'] = $_GET['value'];
        $result = $model_mb_special->editMbClass($param, $_GET['id']);

        $data = array();
        if($result) {
            $this->log('保存手机拍卖分类名称' . '[ID:' . $result. ']', 1);
            $data['result'] = true;
        } else {
            $this->log('保存手机拍卖分类名称' . '[ID:' . $result. ']', 0);
            $data['result'] = false;
            $data['message'] = '保存失败';
        }
        echo json_encode($data);die;
    }

    /**
     * 删除分类名称
     */
    public function class_delOp() {
        $model_mb_special = Model('mb_class');

        $result = $model_mb_special->delMbClassByID($_POST['mb_class_id']);

        if($result) {
            $this->log('删除手机拍卖分类名称' . '[ID:' . $_POST['mb_class_id'] . ']', 1);
            showMessage(L('nc_common_del_succ'), urlAdminMobile('mb_auction', 'className_list'));
        } else {
            $this->log('删除手机拍卖分类名称' . '[ID:' . $_POST['mb_class_id'] . ']', 0);
            showMessage(L('nc_common_del_fail'), urlAdminMobile('mb_auction', 'className_list'));
        }
    }



    /**
     * 编辑专题
     */
    public function special_editOp() {
        $model_mb_special = Model('mb_class');

        $special_item_list = $model_mb_special->getMbSpecialItemListByID($model_mb_special::INDEX_SPECIAL_ID);
        Tpl::output('list', $special_item_list);
        Tpl::output('page', $model_mb_special->showpage(2));

        Tpl::output('module_list', $model_mb_special->getMbSpecialModuleList());
//        Tpl::output('mb_class_id', $_GET['mb_class_id']);
        Tpl::output('special_id', $model_mb_special::INDEX_SPECIAL_ID);

        Tpl::showpage('mb_auction_item.list');
    }

    /**
     * 专题项目添加
     */
    public function special_item_addOp() {
        $model_mb_special = Model('mb_class');

        $param = array();
        $param['item_type'] = $_POST['item_type'];
        $param['special_id'] = $_POST['special_id'];
        //广告只能添加一个
        if($param['item_type'] == 'adv_list') {
            $result = $model_mb_special->isMbSpecialItemExist($param);
            if($result) {
                echo json_encode(array('error' => '广告条板块只能添加一个'));die;
            }
        }

         //导航只能添加一个
        if($param['item_type'] == 'sem') {
            $result = $model_mb_special->isMbSpecialItemExist($param);
            if($result) {
                echo json_encode(array('error' => '专题列表板块只能添加一个'));die;
            }
        }

        //专场板块最多添加三个
        if($param['item_type'] == 'special') {
            $result = $model_mb_special->isMbSpecialItemExist($param);
            if($result >= 3) {
                echo json_encode(array('error' => '专场列表板块最多只能添加三个'));die;
            }
        }

        $item_info = $model_mb_special->addMbSpecialItem($param);
        if($item_info) {
            echo json_encode($item_info);die;
        } else {
            echo json_encode(array('error' => '添加失败'));die;
        }
    }

    /**
     * 专题项目删除
     */
    public function special_item_delOp() {
        $model_mb_special = Model('mb_class');

        $condition = array();
        $condition['item_id'] = $_POST['item_id'];

        $result = $model_mb_special->delMbSpecialItem($condition, $_POST['special_id']);
        if($result) {
            echo json_encode(array('message' => '删除成功'));die;
        } else {
            echo json_encode(array('error' => '删除失败'));die;
        }
    }

    /**
     * 专题项目编辑
     */
    public function special_item_editOp() {
        $model_mb_special = Model('mb_class');

        $item_info = $model_mb_special->getMbSpecialItemInfoByID($_GET['item_id']);
        Tpl::output('item_info', $item_info);

        Tpl::showpage('mb_auction_item.edit');
    }

    /**
     * 专题项目保存
     */
    public function special_item_saveOp() {
        /** @var mb_classModel $model_mb_special */
        $model_mb_special = Model('mb_class');
        $result = $model_mb_special->editMbSpecialItemByID(array('item_data' => $_POST['item_data']), $_POST['item_id'], $_POST['special_id']);

        if($result) {

            showMessage(L('nc_common_save_succ'), urlAdminMobile('mb_auction', 'special_edit', array('special_id'=>$_POST['special_id'])));

        } else {
            showMessage(L('nc_common_save_succ'), '');
        }
    }

    /**
     * 图片上传
     */
    public function special_image_uploadOp() {
        $data = array();
        if(!empty($_FILES['special_image']['name'])) {
            $prefix = 's' . $_POST['special_id'];
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_MOBILE . DS . 'special' . DS . $prefix);
            $upload->set('fprefix', $prefix);
            $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));

            $result = $upload->upfile('special_image');
            if(!$result) {
                $data['error'] = $upload->error;
            }
            $data['image_name'] = $upload->file_name;
            $data['image_url'] = getMbSpecialImageUrl($data['image_name']);
        }
        echo json_encode($data);
    }

    /**
     * 商品列表
     */
    public function goods_listOp() {
        $model_auctions = Model('auctions');

        $condition = array();
        $condition['auction_name'] = array('like', '%' . $_GET['keyword'] . '%');
        $condition['state'] = array('in',array(0));
        $goods_list = $model_auctions->getAuctionList($condition, 'auction_id,auction_name,store_id,current_price,auction_start_price,auction_image,auction_start_time,auction_end_time','','','', 10);
        Tpl::output('goods_list', $goods_list);
        Tpl::output('show_page', $model_auctions->showpage());
        Tpl::showpage('mb_auction_widget.auctions', 'null_layout');
    }

    public function qucai_listOp(){
        $model_guess = Model('p_guess');

        $condition = array();
        $condition['gs_name'] = array('like', '%' . $_GET['keyword'] . '%');
        $condition['verify_state'] = 1;
        $condition['gs_state'] = array('in',array(1,2));
        $condition['end_time'] = array('gt', time());
        $goods_list = $model_guess->getGuessList($condition, 'gs_id,gs_act_id,gs_min_price,gs_max_price,gs_state,start_time,end_time','', 10);
        Tpl::output('goods_list', $goods_list);
        Tpl::output('show_page', $model_guess->showpage());
        Tpl::showpage('mb_auction_widget.guess', 'null_layout');
    }

    //专场列表
    public function special_listOp(){
        $model_special = Model('auction_special');

        $condition = array();
        $condition['special_name'] = array('like', '%' . $_GET['keyword'] . '%');
        $goods_list = $model_special->getSpecialOpenList($condition, 'special_id,special_name,special_start_time,special_end_time,special_image,delivery_mechanism','','', 10);
        Tpl::output('goods_list', $goods_list);
        Tpl::output('show_page', $model_special->showpage());
        Tpl::showpage('mb_auction_widget.special', 'null_layout');
    }

    /**
     * 商店列表
     * @return [type] [description]
     */
    public function store_listOp(){
        $model_store = Model('store');
        $condition = array();
        $condition['store_name'] = array('like','%'. $_GET['keyword'].'%');
        $store_list = $model_store->getStoreOnlineList($condition,10);
        Tpl::output('store_list', $store_list);
        Tpl::output('show_page', $model_store->showpage());
        Tpl::showpage('mb_auction_widget.store','null_layout');
    }

    /**
     * 更新项目排序
     */
    public function update_item_sortOp() {
        $item_id_string = $_POST['item_id_string'];
        $special_id = $_POST['special_id'];
        if(!empty($item_id_string)) {
            $model_mb_special = Model('mb_class');
            $item_id_array = explode(',', $item_id_string);
            $index = 0;
            foreach ($item_id_array as $item_id) {
                $result = $model_mb_special->editMbSpecialItemByID(array('item_sort' => $index), $item_id, $special_id);
                $index++;
            }
        }
        $data = array();
        $data['message'] = '操作成功';
        echo json_encode($data);
    }

    /**
     * 更新项目启用状态
     */
    public function update_item_usableOp() {
        $model_mb_special = Model('mb_class');
        $result = $model_mb_special->editMbSpecialItemUsableByID($_POST['usable'], $_POST['item_id'], $_POST['special_id']);
        $data = array();
        if($result) {
            $data['message'] = '操作成功';
        } else {
            $data['error'] = '操作失败';
        }
        echo json_encode($data);
    }

}
