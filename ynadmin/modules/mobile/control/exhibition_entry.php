<?php
/**
 * 会销配置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class exhibition_entryControl extends SystemControl{

    const LINK_CMS_SPECIAL = 'index.php?act=exhibition_setting&op=exhibition_list';
    //专题状态草稿箱
    const SPECIAL_STATE_DRAFT = 1;
    //专题状态待审核
    const SPECIAL_STATE_PUBLISHED = 2;

    public function __construct(){
        parent::__construct();
        Language::read('cms');
    }

    public function indexOp() {
//        $this->exhibition_listOp();
        Tpl::showpage("exhibition_entry.index");
    }

    /**
     * cms专题列表
     **/
    public function exhibition_list_xmlOp() {
        $model_exhibition_setting = Model('exhibition_setting');
        $model_exhibition_entry = Model('exhibition_entry');
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = array();
        if($_POST['query'] !=''){
            if($_POST['qtype'] == 'ex_name'){
                $ex_list = $model_exhibition_setting->getList(array('title'=>array('like',"%{$_POST['query']}%")));
                if(!empty($ex_list)){
                    foreach($ex_list as $ex){
                        $ex_id_arr[] = $ex['id'];
                    }
                    $condition['ex_id'] = array('in',implode(',',$ex_id_arr));
                }else{
                    $condition['ex_id'] = 0;
                }
            } else {
                if($_POST['qtype'] == 'member_real_name'){
                    $condition[$_POST['qtype']] = array('like',"%{$_POST['query']}%");
                }else{
                    $condition[$_POST['qtype']] = $_POST['query'];
                }
            }
        }
        $list = $model_exhibition_entry->getList($condition, $page, 'id desc');
//dd();
        $out_list = array();
        if(!empty($list)){
            foreach($list as $v){
                $out = array();
                $out['exhibition_id'] = $v['ex_id'];
                if(isset($exhibition_arr[$v['ex_id']])){
                    $out['exhibition_name'] = $exhibition_arr[$v['ex_id']];
                }else{
                    $ex_info = $model_exhibition_setting->getOne(array('id'=>$v['ex_id']));
                    $exhibition_arr[$v['ex_id']] = $ex_info['title'];
                    $out['exhibition_name'] = $exhibition_arr[$v['ex_id']];
                }
                $out['member_id'] = $v['member_id'];
                $out['member_name'] = $v['member_real_name'];
                $out['member_mobile'] = $v['member_using_mobile'];
                $out['top_member_id'] = $v['member_top'];
                $out['top_member_name'] = $v['member_top_name'];
                $out['top_member_mobile'] = $v['member_top_mobile'];
                $out['content'] = $v['content'];
                $out['cdate'] = $v['add_time'] == 0?'':date('Y-m-d H:i:s',$v['add_time']);
                $out_list[] = $out;
            }
        }
        $data = array();
        $data['now_page'] = $model_exhibition_entry->shownowpage();
        $data['total_num'] = $model_exhibition_entry->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }


}
