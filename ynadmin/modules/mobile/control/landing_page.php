<?php
/**
 * 落地页设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class landing_pageControl extends SystemControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 落地页列表页
     */
    public function indexOp(){
        Tpl::showpage('landing_page.list');
    }

    /**
     * 获取落地页数据
     */
    public function get_landing_xmlOp(){
        $model_landing = Model('landing_page');
        $condition = array();
        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $landing_list = $model_landing->getLandingPageList($condition,$page);
        $false_flag = '<span class="no"><i class="fa fa-ban"></i>否</span>';
        $true_flag = '<span class="yes"><i class="fa fa-check-circle"></i>是</span>';
        foreach ($landing_list as $k => &$v){
            $operation = "<a class='btn blue' href='index.php?act=landing_page&op=landing_add&id=".$v['id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $operation .= "<a class='btn blue' href='index.php?act=landing_page&op=landing_relation_goods&id=".$v['id']."'><i class='fa fa-link' aria-hidden='true'></i>关联商品</a>";
            array_unshift($v,$operation);
            $banner_image = UPLOAD_SITE_URL.'/'.ATTACH_MOBILE.'/boot'.'/'.$v['banner_image'];
            $footer_image = UPLOAD_SITE_URL.'/'.ATTACH_MOBILE.'/boot'.'/'.$v['footer_image'];
            $v['banner_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".$banner_image.">\")'><i class='fa fa-picture-o'></i></a>";
            $v['footer_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".$footer_image.">\")'><i class='fa fa-picture-o'></i></a>";
            $v['show_goods_num'] = $v['show_goods_num'];
            $v['state'] = $v['state'] == 1 ? $false_flag : $true_flag;
            $v['add_time'] = date('Y-m-d H:i',$v['add_time']);
            $v['modify_time'] = $v['modify_time'] > 0 ? date('Y-m-d H:i',$v['modify_time']) : '---';
        }
        unset($v);
        $data = array();
        $data['now_page'] = $model_landing->shownowpage();
        $data['total_num'] = $model_landing->gettotalnum();
        $data['list'] = $landing_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 新增引导页
     */
    public function landing_addOp() {
        $model_landing = Model('landing_page');
        if (chksubmit()) {
            $addData = [
                'title'      => strval($_POST['title']),
                'page_name'   => strval($_POST['page_name']),
                'state'=> intval($_POST['state']),
                'content' => strval($_POST['content']),
                'show_goods_num' => intval($_POST['show_goods_num']),
                'modify_time' => time(),
            ];
            intval($_POST['id']) > 0 ? '' : $addData['add_time'] = time();
            //上传图片
            if ($_FILES['banner_image']['name'] != '') {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_MOBILE . '/boot');
                $result = $upload->upfile('banner_image');
                if ($result) {
                    $addData['banner_image'] = $upload->file_name;
                } else {
                    showMessage($upload->error);
                }
            }
            if ($_FILES['footer_image']['name'] != '') {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_MOBILE . '/boot');
                $result = $upload->upfile('footer_image');
                if ($result) {
                    $addData['footer_image'] = $upload->file_name;
                } else {
                    showMessage($upload->error);
                }
            }
            $result = intval($_POST['id']) > 0 ? $model_landing->updateLandingData(['id'=>intval($_POST['id'])],$addData) : $model_landing->addData($addData);
            if ($result) {
                showMessage(Language::get('nc_common_save_succ'),'index.php?act=landing_page&op=index');
            } else {
                showMessage(Language::get('nc_common_save_fail'));
            }
        }
        if (intval($_GET['id']) > 0){
            $model_landing = Model('landing_page');
            $landing_info = $model_landing->getLandingPageInfo(['id'=>intval($_GET['id'])], '*');
            Tpl::output('landing_info',$landing_info);
        }
        Tpl::showpage('landing_page.add');
    }

    /**
     * 关联商品
     */
    public function landing_relation_goodsOp(){
        $landing_page = Model('landing_page');
        $landing_info = $landing_page->getLandingPageInfo(['id'=>intval($_GET['id'])],'id,page_name,title');
        if (empty($landing_info['id'])){
            showMessage('数据不存在');
        }
        $landing_relation_goods = Model('landing_relation_goods');
        $field = 'landing_relation_goods.*,goods.goods_id,goods.goods_name,goods.goods_price,goods.goods_promotion_price,goods.goods_image,goods.store_id';
        $landing_goods_list = $landing_relation_goods->getLandingGoodsList(['landing_page_id'=>$landing_info['id']],$field);
        if (!empty($landing_goods_list)) {
            foreach ($landing_goods_list as $k => $v) {
                $landing_goods_list[$k]['goods_image'] = thumb($v,240);
            }
        }

        Tpl::output('landing_info', $landing_info);
        Tpl::output('landing_goods_list', $landing_goods_list);
        Tpl::output('landing_goods_list_json',json_encode($landing_goods_list));
        Tpl::showpage('landing_relation_goods');
    }

    /**
     * 关联商品编辑
     */
    public function saveOp(){
        $landing_page_id = intval($_POST['landing_page_id']);
        if (!chksubmit()) {
            showMessage('非法提交');
        }
        $landing_goods = Model('landing_relation_goods');
        $del = $landing_goods->delLandingGoods(['landing_page_id'=>$landing_page_id]);
        if (!$del) {
            showMessage('保存失败');
        }

        $data = array();
        if (is_array($_POST['goods_id_list'])) {
            foreach ($_POST['goods_id_list'] as $k => $goods_id) {
                $data[$k]['landing_page_id'] = $landing_page_id;
                $data[$k]['goods_id'] = $goods_id;
                $data[$k]['introduce'] = strval($_POST['introduce'][$goods_id]);
            }
        }
        $insert = $landing_goods->addData($data);
        if ($insert) {
            showMessage('保存成功','index.php?act=landing_page&op=index');
        }else{
            showMessage('保存失败');
        }
    }


}