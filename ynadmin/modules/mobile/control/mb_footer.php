<?php
/**
 * 底部设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class mb_footerControl extends SystemControl
{

    public function __construct()
    {
        parent::__construct();
    }


    public function indexOp(){
        $model_mb_special = Model('mb_special');

        $item_info = $model_mb_special->getMbSpecialItemInfo(array('item_type' => 'nav_list'));
        Tpl::output('item_info', $item_info);

        Tpl::showpage('mb_footer.index');
    }

    public function special_item_saveOp() {
        $model_mb_special = Model('mb_special');

        $result = $model_mb_special->editMbSpecialItemByID(array('item_data' => $_POST['item_data']), $_POST['item_id'], $_POST['special_id']);

        if($result) {
            if($_POST['special_id'] == $model_mb_special::INDEX_SPECIAL_ID) {
                showMessage(L('nc_common_save_succ'));
            } else {
                showMessage(L('nc_common_save_succ'));
            }
        } else {
            showMessage(L('nc_common_save_succ'), '');
        }
    }
}