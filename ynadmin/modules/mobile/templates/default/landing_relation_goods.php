<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=landing_page&op=index" title="返回推广落地页列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>关联落地页商品</h3>
                <h5>推广落地页显示推荐商品信息</h5>
            </div>
        </div>
    </div>
    <form id="goods_form" method="post" action='index.php?act=landing_page&op=save'>
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default" id="explanation">
            <dl class="row">
                <dt class="tit">
                    <label>落地页名称</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['landing_info']['page_name'];?>
                    <input type="hidden" name="landing_page_id" id="landing_page_id" value="<?php echo $output['landing_info']['id'];?>"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>落地页标题</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['landing_info']['title'];?>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>关联商品</label>
                </dt>
                <dd class="opt">
                    <input type="text" placeholder="搜索商品名称" value="" name="goods_name" id="goods_name" maxlength="20" class="input-txt">
                    <a id="goods_search" href="JavaScript:void(0);" class="ncap-btn mr5"><?php echo $lang['nc_search'];?></a></dd>
            </dl>
            <dl class="row" id="selected_goods_list">
                <dt class="tit">已选中关联商品</dt>
                <dd class="opt">
                    <input type="hidden" name="valid_recommend" id="valid_recommend" value="">
                    <span class="err"></span>
                    <ul class="dialog-goodslist-s1 goods-list scrollbar-box" style="">
                    </ul>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">选择要关联的商品</dt>
                <dd class="opt">
                    <div id="show_recommend_goods_list" class="show-recommend-goods-list scrollbar-box"></div>
                    <!--<p class="notic">最多可推荐6个商品</p>-->
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(){
            if ($('input[name="goods_id_list[]"]').size() == 0) {
                $('#valid_recommend').rules('add',{
                    required: true,
                    messages: {required : '<i class="fa fa-exclamation-circle"></i>请选择推荐商品'}
                });
            }else{
                $('#valid_recommend').rules('remove');
            }

            if($("#goods_form").valid()){
                $("#goods_form").submit();
            }
        });
        $('#goods_search').on('click',function(){
            $('#valid_recommend').rules('remove');
            if($("#goods_form").valid()){
                var admin_site_url = '<?php echo ADMIN_SITE_URL;?>';
                $('#show_recommend_goods_list').load(admin_site_url+'/modules/shop/index.php?act=goods_recommend&op=get_goods_list&goods_name='+$('#goods_name').val());
            }
        });

        //表单验证
        $('#goods_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                paint_module_title : {
                    required : function (){
                        if ($('input[name="goods_id_list[]"]').size() > 0){
                            return false
                        }else {
                            var _tmp = $('#module_name').children('select');
                            if (_tmp.val() == 0) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            messages : {
                paint_module_title : {
                    required : '<i class="fa fa-exclamation-circle"></i>请选择模块名称'
                }
            }
        });

    });

    function select_recommend_goods(goods_id) {
        var introduce = '';
        if (typeof goods_id == 'object') {
            var goods_name = goods_id['goods_name'];
            var goods_image = goods_id['goods_image'];
             introduce = goods_id['introduce'];
            var goods_id = goods_id['goods_id'];
        } else {
            var goods = $("#show_recommend_goods_list img[goods_id='"+goods_id+"']");
            var goods_image = goods.attr("src");
            var goods_name = goods.attr("goods_name");
        }
        var obj = $("#selected_goods_list");
        if(obj.find("img[goods_id='"+goods_id+"']").size()>0) return;//避免重复
        if(obj.find("ul>li").size()>=6){
            //alert('最多可推荐6个商品');
            //return false;
        }
        var text_append = '';
        text_append += '<div onclick="del_recommend_goods(this,'+goods_id+');" class="goods-pic landing_page">';
        text_append += '<span class="ac-ico"></span>';
        text_append += '<span class="thumb size-72x72">';
        text_append += '<i></i>';
        text_append += '<img width="72" goods_id="'+goods_id+'" src="'+goods_image+'" />';
        text_append += '</span></div>';
        text_append += '<div class="goods-name landing_page">';
        text_append += '<a href="<?php echo C('auction_site_url')?>/index.php?act=auctions&id='+goods_id+'" target="_blank">';
        text_append += goods_name+'</a>';
        text_append += '</div>';
        text_append += '<textarea name="introduce['+goods_id+']" rows="20" cols="30">'+introduce+'</textarea><input name="goods_id_list[]" value="'+goods_id+'" type="hidden">';
        obj.find("ul").append('<li style="width: 200px;" class="landingfloat" >'+text_append+'</li>');
        <?php if (!$_GET['paint_module_id']) { ?>
        $('#paint_module_title').val($('#module_name').children('select').val());
        <?php } ?>

    }
    function del_recommend_goods(obj,auction_id) {
        $(obj).parent().remove();
    }

    var goods_list_json = $.parseJSON('<?php echo $output['landing_goods_list_json'];?>');
    $.each(goods_list_json,function(k,v){
        select_recommend_goods(v);
    });

</script>
