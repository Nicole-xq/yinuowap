<?php //print_r($item_data);exit;?>
<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
    .mb-item-edit-content {
        background: #EFFAFE url(<?php echo ADMIN_TEMPLATES_URL;
?>/images/cms_edit_bg_line.png) repeat-y scroll 0 0;
    }
</style>
<?php if($item_edit_flag) { ?>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>从右侧筛选按钮，点击添加按钮完成添加</li>
            <li>鼠标移动到已有店铺上，会出现删除按钮可以对店铺进行删除</li>
            <li>操作完成后点击保存编辑按钮进行保存</li>
        </ul>
    </div>
<?php } ?>
<div class="index_block goods-list">
    <?php if($item_edit_flag) { ?>
        <h3>品牌旗舰板块</h3>
    <?php } ?>
    <div class="title">
        <?php if($item_edit_flag) { ?>
            <h5>标题：</h5>
            <input id="home1_title" type="text" class="txt w200" name="item_data[title]" value="<?php echo $item_data['title'];?>">
        <?php } else { ?>
            <span><?php echo $item_data['title'];?></span>
        <?php } ?>
    </div>
    <div nctype="item_content" class="content">
        <?php if($item_edit_flag) { ?>
            <h5>内容：</h5>
        <?php } ?>
        <?php if(!empty($item_data['item']) && is_array($item_data['item'])) {?>
            <?php foreach($item_data['item'] as $item_value) {?>
                <div nctype="item_image" class="item">
                    <div class="goods-pic"><img nctype="goods_image" src="<?php echo getMbSpecialImageUrl($item_value['image']);?>" alt=""></div>
                    <div class="goods-name" nctype="goods_name"><?php echo $item_value['store_name'];?></div>
                    <div class="goods-price" nctype="goods_price">关注：<?php echo $item_value['store_collect'];?><img src="<?php echo $item_value['store_logo'];?>"></div>
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_type" name="item_data[item][<?php echo $item_value['image'];?>][type]" value="<?php echo $item_value['type'];?>" type="hidden">
                        <input nctype="image_name" name="item_data[item][<?php echo $item_value['image'];?>][image]" value="<?php echo $item_value['image'];?>" type="hidden">
                        <input nctype="image_store_name" name="item_data[item][<?php echo $item_value['image'];?>][store_name]" value="<?php echo $item_value['store_name'];?>" type="hidden">
                        <input nctype="image_store_id" name="item_data[item][<?php echo $item_value['image'];?>][store_id]" value="<?php echo $item_value['store_id'];?>" type="hidden">
                        <input nctype="image_store_collect" name="item_data[item][<?php echo $item_value['image'];?>][store_collect]" value="<?php echo $item_value['store_collect'];?>" type="hidden">
                        <input nctype="image_data" name="item_data[item][<?php echo $item_value['image'];?>][data]" value="<?php echo $item_value['data'];?>" type="hidden">
                        <input nctype="image_store_logo" name="item_data[item][<?php echo $item_value['image'];?>][store_logo]" value="<?php echo $item_value['store_logo'];?>" type="hidden">
                        <a nctype="btn_del_item_image" href="javascript:;"><i class="fa fa-trash-o"></i>删除</a>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php if($item_edit_flag) { ?>
    <div class="search-goods">
        <h3>选择店铺添加</h3>
        <h5>店铺关键字：</h5>
        <input id="txt_store_name" type="text" class="txt w200" name="">
        <a id="btn_mb_special_store_search" class="ncap-btn" href="javascript:;" style="vertical-align: top; margin-left: 5px;">搜索</a>
        <div id="mb_special_store_list"></div>
    </div>
<?php } ?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_mb_special_store_search').on('click', function() {
            var url = '<?php echo urlAdminMobile('mb_auction', 'store_list');?>';
            var keyword = $('#txt_store_name').val();
            if(keyword) {
                $('#mb_special_store_list').load(url + '&' + $.param({keyword: keyword}));
            }
        });
    });
</script>