<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>移动端各协议设定</h3>
                <h5>手机客户端协议设置</h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>此处列出了手机支持的协议，点击编辑可以设置协议内容</li>
        </ul>
    </div>
    <table class="flex-table">
        <thead>
        <tr>
            <th width="24" align="center" class="sign"><i class="ico-check"></i></th>
            <th width="80" align="center" class="handle-s"><?php echo $lang['nc_handle'];?></th>
            <th width="240" align="left">协议标题</th>
            <th width="120" align="center">时间</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($output['mb_agreement_list']) && is_array($output['mb_agreement_list'])){ ?>
            <?php foreach($output['mb_agreement_list'] as $k => $v) { ?>
                <tr>
                    <td class="sign"><i class="ico-check"></i></td>
                    <td class="handle-s"><a href="<?php echo urlAdminMobile('mb_agreement', 'agreement_edit', array('agreement_id' => $v['agreement_id']));?>" class="btn purple"><i class="fa fa-pencil-square-o"></i><?php echo $lang['nc_edit']?></a></td>
                    <td><?php echo $v['agreement_title'];?></td>
                    <td><?php echo date('Y-m-d H:i:s',$v['last_modify_time']);?></td>
                    <td></td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function(){
        $('.flex-table').flexigrid({
            height:'auto',// 高度自动
            usepager: false,// 不翻页
            striped:false,// 不使用斑马线
            resizable: false,// 不调节大小
            title: '协议列表',// 表格标题
            reload: false,// 不使用刷新
            columnControl: false,// 不使用列控制
        });
    });
</script>
