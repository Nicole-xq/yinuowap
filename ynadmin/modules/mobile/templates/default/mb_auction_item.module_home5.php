<?php defined('InShopNC') or exit('Access Invalid!');?>
<?php if($item_edit_flag) { ?>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
        </div>
        <ul>
            <li>鼠标移动到内容上出现编辑按钮可以对内容进行修改</li>
            <li>操作完成后点击保存编辑按钮进行保存</li>
        </ul>
    </div>
<?php } ?>
<div class="index_block home2">
    <?php if($item_edit_flag) { ?>
        <h3>模型版块布局E</h3>
    <?php } ?>
    <div class="title">
        <?php if($item_edit_flag) { ?>
            <h5>标题：</h5>
            <input id="home1_title" type="text" class="txt w200" name="item_data[title]" value="<?php echo $item_data['title'];?>">
        <?php } else { ?>
            <span><?php echo $item_data['title'];?></span>
        <?php } ?>
    </div>
    <div class="content">
        <?php if($item_edit_flag) { ?>
            <h5>内容：</h5>
        <?php } ?>
        <div class="home5_1">
            <div class="home5_1_1">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][0]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][0][image]" type="hidden" value="<?php echo $item_data['item'][0]['image'];?>">
                        <input nctype="image_type" name="item_data[item][0][type]" type="hidden" value="<?php echo $item_data['item'][0]['type'];?>">
                        <input nctype="image_data" name="item_data[item][0][data]" type="hidden" value="<?php echo $item_data['item'][0]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*260" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
            <div class="home5_1_2">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][1]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][1][image]" type="hidden" value="<?php echo $item_data['item'][1]['image'];?>">
                        <input nctype="image_type" name="item_data[item][1][type]" type="hidden" value="<?php echo $item_data['item'][1]['type'];?>">
                        <input nctype="image_data" name="item_data[item][1][data]" type="hidden" value="<?php echo $item_data['item'][1]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*260" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="home5_2">
            <div class="home5_2_1">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][2]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][2][image]" type="hidden" value="<?php echo $item_data['item'][2]['image'];?>">
                        <input nctype="image_type" name="item_data[item][2][type]" type="hidden" value="<?php echo $item_data['item'][2]['type'];?>">
                        <input nctype="image_data" name="item_data[item][2][data]" type="hidden" value="<?php echo $item_data['item'][2]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
            <div class="home5_2_2">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][3]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][3][image]" type="hidden" value="<?php echo $item_data['item'][3]['image'];?>">
                        <input nctype="image_type" name="item_data[item][3][type]" type="hidden" value="<?php echo $item_data['item'][3]['type'];?>">
                        <input nctype="image_data" name="item_data[item][3][data]" type="hidden" value="<?php echo $item_data['item'][3]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
            <div class="home5_2_3">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][4]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][4][image]" type="hidden" value="<?php echo $item_data['item'][4]['image'];?>">
                        <input nctype="image_type" name="item_data[item][4][type]" type="hidden" value="<?php echo $item_data['item'][4]['type'];?>">
                        <input nctype="image_data" name="item_data[item][4][data]" type="hidden" value="<?php echo $item_data['item'][4]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
            <div class="home5_2_4">
                <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][5]['image']);?>" alt="">
                    <?php if($item_edit_flag) { ?>
                        <input nctype="image_name" name="item_data[item][5][image]" type="hidden" value="<?php echo $item_data['item'][5]['image'];?>">
                        <input nctype="image_type" name="item_data[item][5][type]" type="hidden" value="<?php echo $item_data['item'][5]['type'];?>">
                        <input nctype="image_data" name="item_data[item][5][data]" type="hidden" value="<?php echo $item_data['item'][5]['data'];?>">
                        <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                    <?php } ?>
                </div>
            </div>
            </div>
            <div class="home5_3">
                <div class="home5_3_1">
                    <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][6]['image']);?>" alt="">
                        <?php if($item_edit_flag) { ?>
                            <input nctype="image_name" name="item_data[item][6][image]" type="hidden" value="<?php echo $item_data['item'][6]['image'];?>">
                            <input nctype="image_type" name="item_data[item][6][type]" type="hidden" value="<?php echo $item_data['item'][6]['type'];?>">
                            <input nctype="image_data" name="item_data[item][6][data]" type="hidden" value="<?php echo $item_data['item'][6]['data'];?>">
                            <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="home5_3_2">
                    <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][7]['image']);?>" alt="">
                        <?php if($item_edit_flag) { ?>
                            <input nctype="image_name" name="item_data[item][7][image]" type="hidden" value="<?php echo $item_data['item'][7]['image'];?>">
                            <input nctype="image_type" name="item_data[item][7][type]" type="hidden" value="<?php echo $item_data['item'][7]['type'];?>">
                            <input nctype="image_data" name="item_data[item][7][data]" type="hidden" value="<?php echo $item_data['item'][7]['data'];?>">
                            <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="home5_3_3">
                    <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][8]['image']);?>" alt="">
                        <?php if($item_edit_flag) { ?>
                            <input nctype="image_name" name="item_data[item][8][image]" type="hidden" value="<?php echo $item_data['item'][8]['image'];?>">
                            <input nctype="image_type" name="item_data[item][8][type]" type="hidden" value="<?php echo $item_data['item'][8]['type'];?>">
                            <input nctype="image_data" name="item_data[item][8][data]" type="hidden" value="<?php echo $item_data['item'][8]['data'];?>">
                            <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="home5_3_4">
                    <div nctype="item_image" class="item"> <img nctype="image" src="<?php echo getMbSpecialImageUrl($item_data['item'][9]['image']);?>" alt="">
                        <?php if($item_edit_flag) { ?>
                            <input nctype="image_name" name="item_data[item][9][image]" type="hidden" value="<?php echo $item_data['item'][9]['image'];?>">
                            <input nctype="image_type" name="item_data[item][9][type]" type="hidden" value="<?php echo $item_data['item'][9]['type'];?>">
                            <input nctype="image_data" name="item_data[item][9][data]" type="hidden" value="<?php echo $item_data['item'][9]['data'];?>">
                            <a nctype="btn_edit_item_image" data-desc="320*130" href="javascript:;"><i class="fa fa-pencil-square-o"></i>编辑</a>
                        <?php } ?>
                    </div>
                </div>
        </div>
    </div>
</div>
