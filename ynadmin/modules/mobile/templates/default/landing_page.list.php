<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>落地页设置列表</h3>
                <h5>mobile终端适用</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>">
                <?php echo $lang['nc_prompts'];?>
            </h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>每个引导页记录只有一个开启状态，其他的是关闭状态</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    function update_flex(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=landing_page&op=get_landing_xml',
            colModel : [
                {display: '操作', name : 'operation', width:150, sortable : false, align: 'center'},
                {display: 'ID', name : 'id', width : 50, sortable : false, align: 'center'},
                {display: '推广页名称', name : 'page_name', width : 150, sortable : false, align: 'left'},
                {display: '落地页标题', name : 'title', width : 150, sortable : false, align: 'left'},
                {display: 'banner图片', name : 'banner_image', width : 70, sortable : false, align: 'left'},
                {display: 'footer图片', name : 'footer_image', width : 70, sortable : false, align: 'left'},
                {display: '商品数目', name : 'show_goods_num', width : 70, sortable : true, align: 'center'},
                {display: '发布状态', name : 'state', width : 50, sortable : false, align: 'center'},
                {display: '更新时间', name : 'modify_time', width : 150, sortable : false, align: 'center'},
                {display: '添加时间', name : 'add_time', width : 150, sortable : false, align: 'center'},
                {display: '描述', name : 'content', width : 200, sortable : false, align: 'left'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增落地页', name : 'add', bclass : 'add', title : '新增落地页', onpress : fg_operation_add }
            ],
            usepager: true,
            rp: 15,
            title: '引导页列表'
        });
    }
    function fg_operation_add(name, bDiv){
        var _url = 'index.php?act=landing_page&op=landing_add';
        window.location.href = _url;
    }
    $(function(){
        update_flex();
    });
</script>
