<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .mb-item-edit-content .search-goods-list li{
        height:90px !important;
    }
    .mb-item-edit-content .search-goods-list .goods-price {
        color: #777 !important;
    }
</style>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){ ?>

    <ul class="search-goods-list">
        <?php foreach($output['goods_list'] as $key => $value){ ?>
            <li>
                <div class="goods-name"><?php echo $value['auction_name'];?></div>
                <div class="goods-price">￥<?php if($value['current_price'] != 0.00){echo $value['current_price'];}else{echo $value['auction_start_price'];}?></div>
                <div class="goods-pic"><img title="<?php echo $value['auction_name'];?>" src="<?php echo cthumb($value['auction_image'], 60,$value['store_id']);?>" /></div>
                <div class="goods-price">开拍时间<?php echo date('m月d日 H:i',$value['auction_start_time']);?></div>
                <div class="goods-price">结束时间<?php echo date('m月d日 H:i',$value['auction_end_time']);?></div>
                <?php if ($value['auction_end_time'] > time()) { ?>
                    <a nctype="btn_add_goods" data-goods-id="<?php echo $value['auction_id'];?>" data-goods-name="<?php echo $value['auction_name'];?>" data-goods-price="<?php if($value['current_price'] != 0.00){echo $value['current_price'];}else{echo $value['auction_start_price'];}?>" data-goods-image="<?php echo cthumb($value['auction_image'], 60,$value['store_id']);?>" href="javascript:;">添加</a>
                <?php } else {?>
                    <a href="javascript:void(0);">已结束</a>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div id="goods_pagination" class="pagination"> <?php echo $output['show_page'];?> </div>
<?php }else { ?>
    <p class="no-record"><?php echo $lang['nc_no_record'];?></p>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#goods_pagination').find('.demo').ajaxContent({
            event:'click',
            loaderType:"img",
            loadingMsg:"<?php echo ADMIN_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#mb_special_goods_list'
        });
    });
</script>
