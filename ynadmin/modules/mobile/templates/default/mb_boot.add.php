<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=mb_boot_page&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>引导页 - <?php echo $lang['nc_new'];?></h3>
        <h5>手机客户端接收引导页设置</h5>
      </div>
    </div>
  </div>
  <form id="post_form" method="post" enctype="multipart/form-data"  name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['boot_info']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
            <label for="title"><em>*</em>引导页标题</label>
        </dt>
        <dd class="opt">
            <input type="text" name="title" id="title" class="input-txt" value="<?php echo $output['boot_info']['title'];?>">
            <span class="err"></span>
            <p class="notic">填写后，可以区分不同的引导页</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="boot_image">引导图片</label>
        </dt>
        <dd class="opt">
            <div class="input-file-show">
                <span class="show">
                    <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['boot_info']['boot_image'];?>">
                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['boot_info']['boot_image'];?>>')" onMouseOut="toolTip()"></i>
                    </a>
                </span>
                <span class="type-file-box">
                  <input name="boot_image" type="file" class="type-file-file" id="boot_image" size="30" hidefocus="true" value="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['boot_info']['boot_image'];?>">
                </span>
            </div>
            <span class="err"></span>
            <p class="notic">展示图片，建议大小640x640像素</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">当前状态</dt>
        <dd class="opt">
            <div class="onoff">
                <label for="boot_state_2" class="cb-enable  <?php if($output['boot_info']['boot_state']==2){ ?> selected <?php }?>" title="<?php echo $lang['open'];?>"><?php echo $lang['open'];?></label>
                <label for="boot_state_1" class="cb-disable  <?php if($output['boot_info']['boot_state']==1){ ?> selected <?php }?>" title="<?php echo $lang['close'];?>"><?php echo $lang['close'];?></label>
                <input id="boot_state_2" name="boot_state" <?php if($output['boot_info']['boot_state']==2){ ?> checked="checked" <?php }?> value="2" type="radio">
                <input id="boot_state_1" name="boot_state" <?php if($output['boot_info']['boot_state']==1){ ?> checked="checked" <?php }?> value="1" type="radio">
            </div>
            <p class="notic">启用后，其他的分享页设置将会关闭</p>
        </dd>
      </dl>
      <dl class="row">
         <dt class="tit">启用分享</dt>
         <dd class="opt">
            <div class="onoff">
                    <label for="share_state_2" class="cb-enable <?php if($output['boot_info']['share_state']==2){ ?> selected <?php }?>" title="<?php echo $lang['open'];?>"><?php echo $lang['open'];?></label>
                    <label for="share_state_1" class="cb-disable <?php if($output['boot_info']['share_state']==1){ ?> selected <?php }?>" title="<?php echo $lang['close'];?>"><?php echo $lang['close'];?></label>
                    <input id="share_state_2" name="share_state" <?php if($output['boot_info']['share_state']==2){ ?> checked="checked" <?php }?> value="2" type="radio">
                    <input id="share_state_1" name="share_state" <?php if($output['boot_info']['share_state']==1){ ?> checked="checked" <?php }?> value="1" type="radio">
            </div>
            <p class="notic">启用后，该分享页有分享到：微博、微信、QQ</p>
         </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="wap_link"><em>*</em>链接地址</label>
        </dt>
        <dd class="opt">
            <input type="text" name="wap_link" id="wap_link" class="input-txt" value="<?php echo $output['boot_info']['wap_link'];?>">
            <span class="err"></span>
            <p class="notic">填写后，跳转不同的页面URL</p>
        </dd>
      </dl>
      <dl class="row">
         <dt class="tit">
            <label for="share_desc">分享描述</label>
         </dt>
         <dd class="opt">
            <textarea name="share_desc" rows="6" class="tarea" id="share_desc"><?php echo $output['boot_info']['share_desc'];?></textarea>
            <span class="err"></span>
            <p class="notic">分享描述文字摘要</p>
         </dd>
      </dl>
      <div class="bot"> <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
    //图片上传
    var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
    $(textButton).insertBefore("#boot_image");
    $("#boot_image").change(function(){
        $("#textfield1").val($("#boot_image").val());
    });
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){
            $("#post_form").submit();
    	}
	});
	$("#post_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            title : {
				required : true
            },
            wap_link : {
                required   : true
            }
        },
        messages : {
            title : {
                required : "<i class='fa fa-exclamation-circle'></i>填写标题"
            },
            wap_link : {
                required : "<i class='fa fa-exclamation-circle'></i>填写链接地址"
            }
        }
	});
});
</script> 
