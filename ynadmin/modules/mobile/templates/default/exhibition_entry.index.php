<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>会销报名列表</h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
<!--      <li>--><?php //echo $lang['cms_special_list_tip1'];?><!--</li>-->
<!--      <li>会销类型分为资讯和商城，资讯会销将出现在资讯频道内，商城会销出现在商城使用商城统一风格</li>-->
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script>
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=exhibition_entry&op=exhibition_list_xml',
        colModel : [
//            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: '会销ID', name : 'exhibition_id', width : 50, sortable : false, align: 'center'},
            {display: '会销名称', name : 'exhibition_name', width : 250, sortable : false, align: 'center'},
            {display: '受邀人会员id', name : 'member_id', width : 80, sortable : false, align: 'center'},
            {display: '受邀人姓名', name : 'member_name', width : 120, sortable : false, align: 'center'},
            {display: '受邀人手机号', name : 'member_mobile', width : 80, sortable : false, align: 'center'},
            {display: '邀请人ID', name : 'top_member_id', width : 50, sortable : false, align: 'center'},
            {display: '邀请人姓名', name : 'top_member_name', width : 120, sortable : false, align: 'center'},
            {display: '邀请人手机号', name : 'top_member_mobile', width : 80, sortable : false, align: 'center'},
            {display: '备注', name : 'content', width : 250, sortable : false, align: 'center'},
            {display: '报名时间', name : 'cdate', width : 150, sortable : false, align: 'center'},
        ],
        searchitems : [
            {display: '会销名称', name : 'ex_name'},
            {display: '会销ID', name : 'ex_id'},
            {display: '邀请人ID', name : 'member_top'},
            {display: '邀请人名称', name : 'member_top_name'},
            {display: '受邀人姓名', name : 'member_real_name'},
            {display: '受邀人手机', name : 'member_using_mobile'},
            ],
        usepager: true,
        rp: 15,
        title: '会销报名列表'
    });
}

$(function(){
    update_flex();
});
//删除会销
function fg_operation_del(id){
    if (confirm('确定删除？')) {
        window.location.href = 'index.php?act=exhibition_entry&op=exhibition_drop&id='+id;
    }
}
function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=exhibition_entry&op=exhibition_add';
    }
}
</script>