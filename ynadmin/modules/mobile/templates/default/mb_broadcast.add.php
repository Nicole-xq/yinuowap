<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=mb_broadcast" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>推送公告 - <?php echo $lang['nc_new'];?></h3>
        <h5>手机客户端接收网站公告信息</h5>
      </div>
    </div>
  </div>
  <form id="post_form" method="post" name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['broadcast_info']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="log_type_v"><em>*</em>公告标题</label>
        </dt>
        <dd class="opt">
          <input type="text" value="<?php echo $output['broadcast_info']['title'];?>" name="title" id="title" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="log_type_v"><em>*</em>公告副标题</label>
        </dt>
        <dd class="opt">
            <input type="text" value="<?php echo $output['broadcast_info']['subtitle'];?>" name="subtitle" id="subtitle" class="input-txt">
            <span class="err"></span>
            <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="log_type_v"><em>*</em>公告跳转链接</label>
        </dt>
        <dd class="opt">
            <input type="text" value="<?php echo $output['broadcast_info']['link'];?>" name="link" id="link" class="input-txt">
            <span class="err"></span>
            <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>公告描述</label>
        </dt>
        <dd class="opt">
          <textarea name="content" rows="3" class="tarea" id="content" ><?php echo $output['broadcast_info']['content'];?></textarea>
          <span class="err"></span>
          <p class="notic">建议最多输入30个字符。</p>
        </dd>
      </dl>
      <div class="bot"> <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){
            $("#post_form").submit();
    	}
	});
	$("#post_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            title : {
				required : true
            },
            subtitle : {
                required   : true
            },
            link : {
                required   : true
            },
            content : {
                required   : true
            }
        },
        messages : {
            title : {
                required : "<i class='fa fa-exclamation-circle'></i>标题不能为空"
            },
            subtitle : {
                required : "<i class='fa fa-exclamation-circle'></i>副标题不能为空"
            },
            link : {
                required : "<i class='fa fa-exclamation-circle'></i>链接不能为空"
            },
            content : {
                required : "<i class='fa fa-exclamation-circle'></i>描述不能为空"
            }
        }
	});
});
</script> 
