<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=landing_page&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>落地页 - <?php echo $lang['nc_new'];?></h3>
        <h5>手机客户端接收落地页设置</h5>
      </div>
    </div>
  </div>
  <form id="post_form" method="post" enctype="multipart/form-data"  name="form1">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['landing_info']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
            <label for="title"><em>*</em>推广页名称</label>
        </dt>
        <dd class="opt">
            <input type="text" name="page_name" id="page_name" class="input-txt" value="<?php echo $output['landing_info']['page_name'];?>">
            <span class="err"></span>
            <p class="notic">填写后，可以区分不同的落地页</p>
        </dd>
      </dl>
      <dl class="row">
            <dt class="tit">
                <label for="title"><em>*</em>展示标题</label>
            </dt>
            <dd class="opt">
                <input type="text" name="title" id="title" class="input-txt" value="<?php echo $output['landing_info']['title'];?>">
                <span class="err"></span>
                <p class="notic">用于展示不同的落地页的标题</p>
            </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="boot_image">banner图片</label>
        </dt>
        <dd class="opt">
            <div class="input-file-show">
                <span class="show">
                    <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['banner_image'];?>">
                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['banner_image'];?>>')" onMouseOut="toolTip()"></i>
                    </a>
                </span>
                <span class="type-file-box">
                  <input name="banner_image" type="file" class="type-file-file" id="banner_image" size="30" hidefocus="true" value="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['banner_image'];?>">
                </span>
            </div>
            <span class="err"></span>
            <p class="notic">落地页的头部banner图</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="boot_image">footer图片</label>
        </dt>
        <dd class="opt">
            <div class="input-file-show">
            <span class="show">
                <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['footer_image'];?>">
                    <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['footer_image'];?>>')" onMouseOut="toolTip()"></i>
                </a>
            </span>
            <span class="type-file-box">
              <input name="footer_image" type="file" class="type-file-file" id="footer_image" size="30" hidefocus="true" value="<?php echo UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$output['landing_info']['banner_image'];?>">
            </span>
            </div>
            <span class="err"></span>
            <p class="notic">落地页的底部图</p>
        </dd>
      </dl>
      <dl class="row">
         <dt class="tit">当前发布状态</dt>
         <dd class="opt">
            <div class="onoff">
                    <label for="landing_state_2" class="cb-enable <?php if($output['landing_info']['state']==2){ ?> selected <?php }?>" title="<?php echo $lang['open'];?>"><?php echo $lang['open'];?></label>
                    <label for="landing_state_1" class="cb-disable <?php if($output['landing_info']['state']==1){ ?> selected <?php }?>" title="<?php echo $lang['close'];?>"><?php echo $lang['close'];?></label>
                    <input id="landing_state_2" name="state" <?php if($output['landing_info']['state']==2){ ?> checked="checked" <?php }?> value="2" type="radio">
                    <input id="landing_state_1" name="state" <?php if($output['landing_info']['state']==1){ ?> checked="checked" <?php }?> value="1" type="radio">
            </div>
            <p class="notic">开启发布后，就可以直接推广了</p>
         </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="show_goods_num">显示商品数</label>
        </dt>
        <dd class="opt">
            <input type="text" class="input-txt" name="show_goods_num" value="<?php echo $output['landing_info']['show_goods_num'];?>">
            <span class="err"></span>
            <p class="notic">显示推广落地页的商品数目</p>
        </dd>
      </dl>
      <dl class="row">
         <dt class="tit">
            <label for="share_desc">推广落地页描述</label>
         </dt>
         <dd class="opt">
            <textarea name="content" rows="6" class="tarea" id="content"><?php echo $output['landing_info']['content'];?></textarea>
            <span class="err"></span>
            <p class="notic">落地页描述文字摘要</p>
         </dd>
      </dl>
      <div class="bot"> <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
    //图片上传
    var textButtonHeader="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
    var textButtonFooter="<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button' id='button2' value='选择上传...' class='type-file-button' />"
    $(textButtonHeader).insertBefore("#banner_image");
    $(textButtonFooter).insertBefore("#footer_image");
    $("#banner_image").change(function(){
        $("#textfield1").val($("#banner_image").val());
    });
    $("#footer_image").change(function(){
        $("#textfield2").val($("#footer_image").val());
    });
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){
            $("#post_form").submit();
    	}
	});
	$("#post_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            title : {
				required : true
            },
            page_name : {
                required   : true
            }
        },
        messages : {
            title : {
                required : "<i class='fa fa-exclamation-circle'></i>填写标题"
            },
            page_name : {
                required : "<i class='fa fa-exclamation-circle'></i>填写推广页名称"
            }
        }
	});
});


</script>

