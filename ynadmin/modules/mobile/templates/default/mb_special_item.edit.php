<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
#cla_elems{max-height: 600px;overflow-y: scroll;}
</style>
<div class="page">
  <!-- 页面导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <?php if($output['item_info']['special_id'] > 1) { ?>
      <a id="btn_back" href="<?php echo urlAdminMobile('mb_special', 'special_edit', array('special_id' => $output['item_info']['special_id']));?>" class="back"  title="返回上一级"><i class="fa fa-arrow-circle-o-left"></i></a>
      <?php } elseif($output['item_info']['special_id'] == 0) { ?>
      <a id="btn_back" href="<?php echo urlAdminMobile('mb_special', 'index_edit');?>" class="back" title="返回上一级"><i class="fa fa-arrow-circle-o-left"></i></a>
      <?php } elseif($output['item_info']['special_id'] == 1){?>
          <a id="btn_back" href="<?php echo urlAdminMobile('mb_special', 'app_index');?>" class="back" title="返回上一级"><i class="fa fa-arrow-circle-o-left"></i></a>
        <?php }?>
      <div class="subject">
        <h3><?php echo $output['item_title'];?></h3>
        <h5>手机客户端首页/专题页模板设置</h5>
      </div>
    </div>
  </div>
  <form id="form_item" action="<?php echo urlAdminMobile('mb_special', 'special_item_save');?>" method="post">
    <input type="hidden" name="special_id" value="<?php echo $output['item_info']['special_id'];?>">
    <input type="hidden" name="item_id" value="<?php echo $output['item_info']['item_id'];?>">
    <?php $item_data = $output['item_info']['item_data'];?>
    <?php $item_edit_flag = true;?>
    <div id="item_edit_content" class="mb-item-edit-content">
      <?php if($output['item_info']['item_type'] != 'nav_list'){ ?>
          <?php require('mb_special_item.module_' . $output['item_info']['item_type'] . '.php');?>
      <?php }?>
    </div>
    <div class="bot"><a id="btn_save" class="ncap-btn-big ncap-btn-green" href="javascript:;">保存编辑</a> </div>
  </form>
</div>
<div id="dialog_item_edit_image" style="display:none;">
   <?php if ($output['item_info']['item_type'] != 'sem' && $output['item_info']['item_type'] != 'cla' ) :?>
        <div class="s-tips"><i class="fa fa-lightbulb-o"></i>请按提示尺寸制作上传图片，以达到手机客户端及Wap手机商城最佳显示效果。</div>
   <?php endif;?>
  <div class="upload-thumb"> <img style="display: block;margin: 0 auto;" id="dialog_item_image" src="" alt=""></div>
  <input id="dialog_item_image_name" type="hidden" value="<?php if ($output['item_info']['item_type'] == 'sem') echo time();?>">
  <input id="dialog_type" type="hidden">
  <form id="form_image" action="">
    <div class="ncap-form-default">
         <?php if ($output['item_info']['item_type'] != 'sem' && $output['item_info']['item_type'] != 'cla') :?>
              <dl class="row">
                <dt class="tit">选择要上传的图片：</dt>
                <dd class="opt">
                  <div class="input-file-show"><span class="type-file-box">
                    <input type='text' name='textfield' id='textfield' class='type-file-text' />
                    <input type='button' name='button' id='button' value='选择上传...' class='type-file-button' />
                    <input id="btn_upload_image" type="file" name="special_image" class="type-file-file" size="30" hidefocus="true" >
                    </span> </div>
                  <p id="dialog_image_desc" class="notic"></p>
                </dd>
              </dl>
       <?php endif;?>
        <?php if ($output['item_info']['item_type'] == 'sem') :?>
       <dl class="row">
        <dt class="tit">专题名称：</dt>
        <dd class="opt">
          <input id="dialog_item_image_sem" type="text" class="txt w200 marginright marginbot vatop">
        </dd>
      </dl>
    <?php endif;?>
    <?php if ($output['item_info']['item_type'] == 'cla') :?>
       <dl class="row">
        <dt class="tit">分类名称：</dt>
        <dd class="opt">
          <input id="dialog_item_image_cla" type="text" class="txt w200 marginright marginbot vatop">
        </dd>
      </dl>
    <?php endif;?>
    <?php if ($output['item_info']['item_type'] == 'store_top'):?>
      <input type="hidden" id="dialog_item_image_store_name">
      <input type="hidden" id="dialog_item_image_store_collect">
      <input type="hidden" id="dialog_item_image_store_logo">
      <input type="hidden" id="dialog_item_image_store_id">
    <?php endif;?>
    <?php if($output['item_info']['item_type'] != 'cla'):?>
      <dl class="row">
        <dt class="tit">操作类型：</dt>
        <dd class="opt">
          <select id="dialog_item_image_type" name="" class="vatop">
            <option value="">-请选择-</option>
            <option value="keyword">商品关键字</option>
            <option value="auction">拍品关键字</option>
            <option value="special">专题编号</option>
            <option value="goods">商品编号</option>
            <option value="pai">拍品编号</option>
            <option value="url">链接</option>
            <option value="store">店铺编号</option>
            <option value="guess">趣猜编号</option>
          </select>
          <input id="dialog_item_image_data" type="text" class="txt w200 marginright marginbot vatop">
          <p id="dialog_item_image_desc" class="notic"></p>
        </dd>
      </dl>
  <?php else: ?>
      <input id="dialog_item_image_type" value="class" type="hidden"/>
      <dl class="row" id="cla_elems">
        <dt class="tit">添加子元素：</dt>
        <dd class="opt">
          <div class="cla_elem_item">
          <div class="input-file-show">
            <span class="show">
                <i class="fa fa-picture-o" onmouseover="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_COMMON.'/'.C('default_goods_image'));?>>')" onmouseout="toolTip()">
                </i>
            </span>
            <span class="type-file-box">
              <input name="special_image" class="type-file-file cla_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效" type="file">
              <input name="textfield" class="type-file-text" type="text">
              <input name="button" value="选择上传..." class="type-file-button" type="button">
              <input type="hidden" class="e_images" value="">
            </span>
          </div>
        <br/>
            <select  name="" class="e_type">
            <option value="">-请选择-</option>
            <option value="keyword">商品关键字</option>
            <option value="auction">拍品关键字</option>
            <option value="special">专题编号</option>
            <option value="goods">商品编号</option>
            <option value="pai">拍品编号</option>
            <option value="url">链接</option>
            <option value="store">店铺编号</option>
            <option value="guess">趣猜编号</option>
          </select>
          <input type="text" class="txt w200 marginright marginbot e_data">
          <p><br/></p>
          </div>
        <a nctype="btn_add_item_elem"  class="ncap-btn"  data-desc="640*340" href="javascript:;"><i class="fa fa-plus"></i>添加新的子元素</a>
        </dd>
      </dl>
  <?php endif;?>
      <div class="bot"><a id="btn_save_item" class="ncap-btn-big ncap-btn-green" href="javascript:;">保存</a></div>
    </div>
  </form>
</div>
<script id="item_image_template" type="text/html">
    <div nctype="item_image" class="item" <?php  if ($output['item_info']['item_type'] == 'sem'):?>style="height:30px"<?php endif;?>>
        <?php if ($output['item_info']['item_type'] != 'sem' && $output['item_info']['item_type'] != 'cla' && $output['item_info']['item_type'] != 'store_top'):?>
            <img nctype="image" src="<%=image%>" alt="" width="100%" height="auto">
        <?php endif;?>
        <?php if ($output['item_info']['item_type'] == 'store_top'):?>
        <div class="goods-pic"><img nctype="goods_image" src="<%=image%>" alt=""></div>
        <div class="goods-name" nctype="goods_name"><%=image_store_name%></div>
        <div class="goods-price" nctype="goods_price">关注：<%=image_store_collect%><img src="<%=image_store_logo%>"></div>
        <?php endif;?>
        <?php if ($output['item_info']['item_type'] == 'sem'):?>
                <%=image_sem%>
        <?php endif;?>
        <?php if ($output['item_info']['item_type'] == 'cla'):?>
                <%=image_cla%>
                <p><br/></p>
                <% var imgs = image_e_images.split(",");%>
                <%for(var i in imgs){%>
                <img src="<%=imgs[i]%>" width="40px" height="40px">
                <%}%>
        <?php endif;?>
        <input nctype="image_name" name="item_data[item][<%=image_name%>][image]" type="hidden" value="<%=image_name%>">
        <input nctype="image_type" name="item_data[item][<%=image_name%>][type]" type="hidden" value="<%=image_type%>">
        <input nctype="image_data" name="item_data[item][<%=image_name%>][data]" type="hidden" value="<%=image_data%>">
        <?php if ($output['item_info']['item_type'] == 'sem'):?>
            <input nctype="image_sem" name="item_data[item][<%=image_name%>][sem]" type="hidden" value="<%=image_sem%>">
        <?php endif?>
        <?php if ($output['item_info']['item_type'] == 'cla'):?>
            <input nctype="image_cla" name="item_data[item][<%=image_name%>][cla]" type="hidden" value="<%=image_cla%>">
            <input nctype="image_e_images" name="item_data[item][<%=image_name%>][e_images]" type="hidden" value="<%=image_e_images%>">
            <input nctype="image_e_type" name="item_data[item][<%=image_name%>][e_type]" type="hidden" value="<%=image_e_type%>">
            <input nctype="image_e_data" name="item_data[item][<%=image_name%>][e_data]" type="hidden" value="<%=image_e_data%>">
        <?php endif;?>
        <?php if ($output['item_info']['item_type'] == 'store_top'):?>
            <input nctype="image_store_name" name="item_data[item][<%=image_name%>][store_name]" type="hidden" value="<%=image_store_name%>">
            <input nctype="image_store_id" name="item_data[item][<%=image_name%>][store_id]" type="hidden" value="<%=image_store_id%>">
            <input nctype="image_store_logo" name="item_data[item][<%=image_name%>][store_logo]" type="hidden" value="<%=image_store_logo%>">
            <input nctype="image_store_collect" name="item_data[item][<%=image_name%>][store_collect]" type="hidden" value="<%=image_store_collect%>">
        <?php endif;?>
        <a href="javascript:;">
            <span class="btn_del_item_image"><i class="fa fa-trash-o"></i>删除 | </span>
            <span class="btn_move_up_item_image"><i class="fa fa-arrow-up"></i> 上移 | </span>
            <span class="btn_move_down_item_image"><i class="fa fa-arrow-down"></i> 下移</span>
        </a>
    </div>
</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/template.min.js" charset="utf-8"></script>
<script type="text/javascript">
    var url_upload_image = '<?php echo urlAdminMobile('mb_special', 'special_image_upload');?>';

    $(document).ready(function(){
        var $current_content = null;
        var $current_image = null;
        var $current_image_name = null;
        var $current_image_type = null;
        var $current_image_data = null;
        var old_image = '';
        var $dialog_item_image = $('#dialog_item_image');
        var $dialog_item_image_name = $('#dialog_item_image_name');
        var special_id = <?php echo $output['item_info']['special_id'];?>;

        //保存
        $('#btn_save').on('click', function() {
            $('#form_item').submit();
        });

        //编辑图片
        $('[nctype="btn_edit_item_image"]').on('click', function() {
            //初始化当前图片对象
            $item_image = $(this).parents('[nctype="item_image"]');
            $current_image = $item_image.find('[nctype="image"]');
            $current_image_name = $item_image.find('[nctype="image_name"]');
            $current_image_type = $item_image.find('[nctype="image_type"]');
            $current_image_data = $item_image.find('[nctype="image_data"]');
            <?php if ($output['item_info']['item_type'] == 'sem'):?>
            $current_image_sem = $item_image.find('[nctype="image_sem"]');
            <?php endif;?>
            $('#dialog_item_image').attr('src', $current_image.attr('src'));
            $('#dialog_item_image_name').val($current_image_name.val());
            $('#dialog_item_image_type').val($current_image_type.val());
            $('#dialog_item_image_data').val($current_image_data.val());
            <?php if ($output['item_info']['item_type'] == 'sem'):?>
            $('#dialog_item_image_sem').val($current_image_sem.val());
            <?php endif;?>
            <?php if ($output['item_info']['item_type'] == 'home5'){?>
            $('#dialog_image_desc').text('大图推荐图片尺寸 374*188，小图推荐图片尺寸 188*188');
            <?php }elseif($output['item_info']['item_type'] == 'home6'){?>
            $('#dialog_image_desc').text('推荐图片尺寸' + '188*188');
            <?php }else{?>
            $('#dialog_image_desc').text('推荐图片尺寸' + $(this).attr('data-desc'));
            <?php }?>
            $('#dialog_type').val('edit');
            change_image_type_desc($('#dialog_item_image_type').val());
            $('#dialog_item_edit_image').nc_show_dialog({
                width: 600,
                title: '编辑'
            });
        });

        //添加图片
        $('[nctype="btn_add_item_image"]').on('click', function() {
            $dialog_item_image.hide();

            $dialog_item_image_name.val('');
            <?php if($output['item_info']['item_type'] == 'sem' || $output['item_info']['item_type'] == 'cla'):?>
                var ins_num = randomNum(100000,999999);
                $dialog_item_image_name.val(ins_num);
                $('#dialog_item_image_sem').val('');
                $('#dialog_item_image_data').val('');
                $('#dialog_item_image_type').val('');

                $('#dialog_item_image_cla').val('');
                $('.e_data').val('');
                $('.e_type').val('');
                $('.e_images').val('');
                $('.fa-picture-o').attr('onmouseover','');
            <?php endif;?>
            $current_content = $(this).parent().find('[nctype="item_content"]');
            <?php if ($output['item_info']['item_type'] == 'home5'){?>
            $('#dialog_image_desc').text('大图推荐图片尺寸 374*188，小图推荐图片尺寸 188*188');
            <?php }elseif($output['item_info']['item_type'] == 'home6'){?>
            $('#dialog_image_desc').text('推荐图片尺寸' + '188*188');
            <?php }else{?>
            $('#dialog_image_desc').text('推荐图片尺寸' + $(this).attr('data-desc'));
            <?php }?>
            $('#dialog_type').val('add');
            change_image_type_desc($('#dialog_item_image_type').val());
            $('#dialog_item_edit_image').nc_show_dialog({
                width: 600,
                title: '添加'
            });
            $('.del').parent().remove();
        });

        //banner删除图片
        $('#item_edit_content').on('click', 'span.btn_del_item_image', function() {
            $(this).parents('[nctype="item_image"]').remove();
        });

        //删除图片
        $('#item_edit_content').on('click', '[nctype="btn_del_item_image"]', function() {
            $(this).parents('[nctype="item_image"]').remove();
        });

        //banner图片上移
        $('#item_edit_content').on('click', 'span.btn_move_up_item_image', function() {
            var cur_item = $(this).parents('[nctype="item_image"]');
            var cur_prev_item = cur_item.prev('[nctype="item_image"]');
            if(cur_prev_item.length > 0) {
                cur_prev_item.before(cur_item);
            } else {
                showError('已经是第一个了');
            }
        });
        //banner图片下移
        $('#item_edit_content').on('click', 'span.btn_move_down_item_image', function() {
            var cur_item = $(this).parents('[nctype="item_image"]');
            var cur_next_item = cur_item.next('[nctype="item_image"]');
            if(cur_next_item.length > 0) {
                cur_next_item.after(cur_item);
            } else {
                showError('已经是最后一个了');
            }
        });

        //图片上传
        $("#btn_upload_image").fileupload({
            dataType: 'json',
            url: url_upload_image,
            formData: {special_id: special_id},
            add: function(e, data) {
                old_image = $dialog_item_image.attr('src');
                $dialog_item_image.attr('src', LOADING_IMAGE);
                data.submit();
            },
            done: function (e, data) {
                var result = data.result;
                if(typeof result.error === 'undefined') {
                    $dialog_item_image.attr('src', result.image_url);
                    $dialog_item_image.show();
                    $dialog_item_image_name.val(result.image_name);
                } else {
                    $dialog_item_image.attr('src') = old_image;
                    showError(result.error);
                }
            }
        });

         //分类图片上传图片上传
        $(".cla_img").click(function(){
          $(this).fileupload({
            dataType: 'json',
            url: url_upload_image,
            formData: {special_id: special_id},
            add: function(e, data) {
                var old_image = $(this).parent().parent().find('.nyroModal').attr('href');
                $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+LOADING_IMAGE+">')");
                $(this).parent().parent().find('.e_images').val(LOADING_IMAGE);
                data.submit();
            },
            done: function (e, data) {
                var result = data.result;
                if(typeof result.error === 'undefined') {
                    $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+result.image_url+">')");
                    $(this).parent().parent().find('.e_images').val(result.image_url);
                } else {
                   $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+old_image+">')");
                   $(this).parent().parent().find('.e_images').val(old_image);
                   showError(result.error);
                }
            }
          });
        });

        $('#btn_save_item').on('click', function() {
            var type = $('#dialog_type').val();
            if(type == 'edit') {
                edit_item_image_save();
            } else {
                <?php if($output['item_info']['item_type'] != 'sem' && $output['item_info']['item_type'] !='cla'): ?>
                if($dialog_item_image_name.val() == '') {
                    showError('请上传图片');
                    return false;
                }
                <?php endif;?>
                add_item_image_save();
            }
            $('#dialog_item_edit_image').hide();
        });

        function edit_item_image_save() {
            $current_image.attr('src', $('#dialog_item_image').attr('src'));
            $current_image_name.val($('#dialog_item_image_name').val());
            $current_image_type.val($('#dialog_item_image_type').val());
            $current_image_data.val($('#dialog_item_image_data').val());
            <?php if($output['item_info']['item_type'] == 'sem'): ?>
            $current_image_sem.val($('#dialog_item_image_sem').val());
            <?php endif;?>
            <?php if($output['item_info']['itme_type'] == 'cla'):?>
            $current_image_cla.val($('#dialog_item_image_cla').val());
            <?php endif;?>
        }

        function add_item_image_save() {
            var $html_item_image = $('#html_item_image');
            var item = {};
            item.image = $('#dialog_item_image').attr('src');
            item.image_name = $('#dialog_item_image_name').val();
            item.image_type = $('#dialog_item_image_type').val();
            item.image_data = $('#dialog_item_image_data').val();
            <?php if($output['item_info']['item_type'] == 'sem'):?>
            item.image_sem = $('#dialog_item_image_sem').val();
            <?php endif;?>
            <?php if($output['item_info']['item_type'] == 'cla'):?>
            var e_links = '';
            var e_images = '';
            var e_data = '';
            var e_type = '';
            $('.e_images').each(function(){
              e_images += $(this).val()+',';
            })
            e_images = e_images.substring(0,e_images.length-1);
            $('.e_links').each(function(){
              e_links += $(this).val()+',';
            })
            e_links = e_links.substring(0,e_links.length-1);
            $('.e_data').each(function(){
              e_data += $(this).val()+',';
            })
            e_data= e_data.substring(0,e_data.length-1);
            $('.e_type').each(function(){
              e_type += $(this).val()+',';
            })
            e_type= e_type.substring(0,e_type.length-1);
            item.image_e_links = e_links;
            item.image_e_images = e_images;
            item.image_e_data = e_data;
            item.image_e_type = e_type;
            item.image_cla = $('#dialog_item_image_cla').val();
            <?php endif;?>
            <?php if($output['item_info']['item_type'] == 'store_top'):?>
              item.image_store_id = $('#dialog_item_image_store_id').val();
              item.image_store_name = $('#dialog_item_image_store_name').val();
              item.image_store_collect = $('#dialog_item_image_store_collect').val();
              item.image_store_logo = $('#dialog_item_image_store_logo').val();
            <?php endif;?>
            $current_content.append(template.render('item_image_template', item));
        }


        $('#dialog_item_image_type').on('change', function() {
            change_image_type_desc($(this).val());
        });


        function change_image_type_desc(type) {
            var desc_array = {};
            var desc = '操作类型一共六种，对应点击以后的操作。';
            if(type != '') {
                desc_array['keyword'] = '商品关键字类型会根据搜索关键字跳转到商品搜索页面，输入框填写搜索关键字。';
                desc_array['auction'] = '拍品关键字类型会根据搜索关键字跳转到拍品搜索页面，输入框填写搜索关键字。';
                desc_array['special'] = '专题编号会跳转到指定的专题，输入框填写专题编号。';
                desc_array['goods'] = '商品编号会跳转到指定的商品详细页面，输入框填写商品编号。';
                desc_array['pai'] = '拍品编号会跳转到指定的拍品详细页面，输入框填写拍品编号。';
                desc_array['url'] = '链接会跳转到指定链接，输入框填写完整的URL。';
                desc_array['store'] = '链接会跳转到指定店铺，输入框填写店铺编号';
                desc_array['guess'] = '链接会跳转到指定趣猜详细页面，输入框填写趣猜编号';
                desc = desc_array[type];
            }
            $('#dialog_item_image_desc').text(desc);
        }
        function randomNum(minNum,maxNum){
            switch(arguments.length){
                case 1:
                    return parseInt(Math.random()*minNum+1);
                break;
                case 2:
                    return parseInt(Math.random()*(maxNum-minNum+1)+minNum);
                break;
                default:
                    return 0;
                break;
            }
          }

           $('#mb_special_store_list').on('click', '[nctype="btn_add_goods"]', function() {
              $('#dialog_item_image_store_name').val($(this).attr('data-store-name'));
              $('#dialog_item_image_store_collect').val($(this).attr('data-store-collect'));
              $('#dialog_item_image_store_logo').val($(this).attr('data-store-logo'));
              $('#dialog_item_image_store_id').val($(this).attr('data-store-id'));
              $dialog_item_image.hide();
              $dialog_item_image_name.val('');
              $current_content = $('[nctype="item_content"]');
              $('#dialog_item_image_type').val('store');
              $('#dialog_item_image_data').val($(this).attr('data-store-id'));
              $('#dialog_image_desc').text('推荐图片尺寸' + '375*375');
              $('#dialog_type').val('add');
              change_image_type_desc($('#dialog_item_image_type').val());
              $('#dialog_item_edit_image').nc_show_dialog({
                  width: 600,
                  title: '添加'
              });
         });

        $('a[nctype="btn_add_item_elem"]').on('click', function() {
            obj = $('#cla_elems').find('a[nctype="btn_add_item_elem"]');
            var add_html = '';
            add_html += '<div class="cla_elem_item">';
            add_html += '<div class="input-file-show">';
            add_html += '<span class="show">';
            add_html += '<i class="fa fa-picture-o" onmouseover="toolTip('+'<img src=<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_COMMON.'/'.C('default_goods_image'));?>>'+')" onmouseout="toolTip()">';
            add_html += '</i>';
            add_html += '</span>';
            add_html += '<span class="type-file-box">';
            add_html += '<input name="special_image" class="type-file-file cla_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效" type="file">';
            add_html += '<input name="textfield" class="type-file-text" type="text">';
            add_html += '<input name="button" value="选择上传..." class="type-file-button" type="button">';
            add_html += '<input type="hidden" class="e_images" value="">';
            add_html += '</span>';
            add_html += '</div>';
            add_html += '<br/>';
            add_html += '<select  name="" class="e_type">';
            add_html += '<option value="">-请选择-</option>';
            add_html += '<option value="keyword">商品关键字</option>';
            add_html += '<option value="auction">拍品关键字</option>';
            add_html += '<option value="special">专题编号</option>';
            add_html += '<option value="goods">商品编号</option>';
            add_html += '<option value="pai">拍品编号</option>';
            add_html += '<option value="url">链接</option>';
            add_html += '<option value="store">店铺编号</option>';
            add_html += '<option value="guess">趣猜编号</option>';
            add_html += '</select>';
            add_html += '<input type="text" class="txt w200 marginright marginbot e_data">';
            add_html += '<span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn">删除</a></span>';
            add_html += '<p><br/></p>';
            add_html += '</div>';
            obj.before(add_html);
             //分类图片上传图片上传
        $(".cla_img").click(function(){
          $(this).fileupload({
            dataType: 'json',
            url: url_upload_image,
            formData: {special_id: special_id},
            add: function(e, data) {
                var old_image = $(this).parent().parent().find('.nyroModal').attr('href');
                $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+LOADING_IMAGE+">')");
                $(this).parent().parent().find('.e_images').val(LOADING_IMAGE);
                data.submit();
            },
            done: function (e, data) {
                var result = data.result;
                if(typeof result.error === 'undefined') {
                    $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+result.image_url+">')");
                    $(this).parent().parent().find('.e_images').val(result.image_url);
                } else {
                   $(this).parent().parent().find('.fa-picture-o').attr('onmouseover',"toolTip('<img src="+old_image+">')");
                   $(this).parent().parent().find('.e_images').val(old_image);
                   showError(result.error);
                }
            }
          });
        });
        });
         $('#cla_elems').find('.del').live('click', function(){
            $(this).parent().remove();
        });
    });
    </script>