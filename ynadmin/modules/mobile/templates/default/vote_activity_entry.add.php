<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=vote_activity_entry&op=vote_activity_entry_list" title="返回专题列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>作品管理--新增/编辑作品</h3>
<!--        <h5>新建活动</h5>-->
      </div>
    </div>
  </div>
    <?php if(empty($output['detail'])){?>
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="u_name">用户名</label>
        </dt>
        <dd class="opt">
          <span><input type="text" class="input-txt" id="u_name" name="u_name" value="" /><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="getMember">查询</a></span>
          <span class="err" style="color:red"></span>
          <p class="notic"></p>
        </dd>
      </dl>
    </div>
    <?php }?>
  <form id="add_form" method="post" enctype="multipart/form-data" action="index.php?act=vote_activity_entry&op=vote_activity_entry_save">
    <input name="vote_activity_entry_id" type="hidden" value="<?php if(!empty($output['detail'])) echo $output['detail']['id'];?>" />
    <input id="vote_activity_entry_state" name="vote_activity_entry_state" type="hidden" value="" />
    <div class="ncap-form-default">
        <?php if(empty($output['detail'])){?>
          <dl class="row">
            <dt class="tit">
              <label for="member_id"><em>*</em>活动</label>
            </dt>
            <dd class="opt">
              <select id="activity_id" name="activity_id">
                <?php foreach($output['activity'] as $activity){?>
                <option value="<?=$activity['id'];?>"><?=$activity['title'];?></option>
                <?php }?>
              </select>
              <span class="err"></span>
              <p class="notic"></p>
            </dd>
          </dl>
        <?php }else{ ?>
            <input id="activity_id" name="activity_id" type="hidden" value="<?=$output['detail']['activity_id'];?>" />
        <?php }?>
      <dl class="row">
        <dt class="tit">
          <label for="member_id"><em>*</em>会员ID</label>
        </dt>
        <dd class="opt">
          <input id="member_id" name="member_id" class="input-txt" type="text" value="<?php if(!empty($output['detail'])) echo $output['detail']['member_id'];?>" readonly/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label for="vote_activity_entry_vote_num"><em>*</em>作者</label>
        </dt>
        <dd class="opt">
          <input id="auther" name="auther" class="input-txt" type="text" value="<?php if(!empty($output['detail'])) echo $output['detail']['auther'];?>"/>
          <span class="err"></span>
          <p class="notic">作者</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="name"><em>*</em>作品名称</label>
        </dt>
        <dd class="opt">
          <input id="name" name="name" class="input-txt" type="text" value="<?php if(!empty($output['detail'])) echo $output['detail']['name'];?>"/>
          <span class="err"></span>
          <p class="notic">作品名称</p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>作品图片</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php if(!empty($output['detail']['image'])){ echo getVoteActivityEntryImageUrl($output['detail']['image']);} else {echo ADMIN_TEMPLATES_URL . '/images/preview.png';}?>"><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php if(!empty($output['detail']['image'])){ echo getVoteActivityEntryImageUrl($output['detail']['image']);} else {echo ADMIN_TEMPLATES_URL . '/images/preview.png';}?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input class="type-file-file" id="image" name="image" type="file" size="30" hidefocus="true" nctype="cms_image" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            <input name="old_vote_activity_entry_image" type="hidden" value="<?php echo $output['detail']['image'];?>" />
            </span></div>
          <span class="err"></span>
          <p class="notic"><span class="vatop rowform"><?php echo $lang['special_image_explain'];?></span></p>
        </dd>
      </dl>

      <dl class="row">
        <dt class="tit">介绍</dt>
        <dd id="ncProductDetails1" class="opt">
          <div class="tabs1">
            <div id="panel-2" class="ui-tabs-panel ui-tabs-hide">
              <?php showEditor('vote_activity_entry_content',$output['detail']['description'],'100%','480px','visibility:hidden;',"false",$output['editor_multimedia'], 'all', 'vote_activity_entry_content');?>
              <div class="hr8">
                <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="add_album2" id="add_album2" multiple>
                  </span>
                    <p><i class="icon-upload-alt" data_type="0" nctype="add_album_i"></i>图片上传</p>
                  </a>
                </div>
              </div>
              <p id="des_demo1"></p>
            </div>
          </div>
        </dd>
      </dl>


    </div>


    <div class="ncap-form-default">
      <div class="bot">
        <!-- <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-blue" id="btn_draft"><?php echo $lang['vote_activity_entry_draft'];?></a> -->
        <?php if(!empty($output['detail']['id'])){?>
            <?php if(in_array($output['detail']['state'],array(0,3))){?>
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="check_true">审核通过</a>
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-red" id="check_false">审核失败</a>
            <?php }?>
        <?php }else{?>
            <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="btn_publish">发布</a>
        <?php }?>


      </div>
    </div>
  </form>
  <!-- 插入图片链接对话框 -->
  <div id="_dialog_image_insert_link" style="display:none;">
    <div class="upload_adv_dialog dialog-image-insert-link">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['vote_activity_entry_image_link_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <dl class="row">
          <dt class="tit">插入图片预览</dt>
          <dd class="opt">
            <div class="dialog-pic-thumb"><a><img alt="" src=""></a></div>
          </dd>
        </dl>
        <dl class="row" id="upload_adv_type">
          <dt class="tit"><?php echo $lang['vote_activity_entry_image_link_url'];?></dt>
          <dd class="opt">
            <input nctype="_image_insert_link" type="text" class="input-txt" placeholder="http://"/>
            <p class="notic"><?php echo $lang['vote_activity_entry_image_link_url_explain'];?>如不填加任何链接请保持默认。</p>
          </dd>
        </dl>
        <div class="bot"><a nctype="btn_image_insert_link" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" ><?php echo $lang['cms_text_save'];?></a></div>
      </div>
    </div>
  </div>
  <!-- 插入图片热点对话框 -->
  <div id="_dialog_image_insert_hot_point" style="display:none;">
    <div class="dialog-image-insert-hot-point">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['vote_activity_entry_image_link_hot_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <div ncytpe="div_image_insert_hot_point" class="special-hot-point"><img nctype="img_hot_point" alt="" src="<?php echo $image_url;?>"> </div>
        <dl class="row">
          <dt class="tit"><?php echo $lang['vote_activity_entry_image_link_hot_url'];?></dt>
          <dd class="opt">
            <input nctype="x1" type="hidden" />
            <input nctype="y1" type="hidden" />
            <input nctype="x2" type="hidden" />
            <input nctype="y2" type="hidden" />
            <input nctype="w" type="hidden" />
            <input nctype="h" type="hidden" />
            <input nctype="url" type="text" class="input-txt" placeholder="http://" />
            <a class="ncap-btn" nctype="btn_hot_point_commit" href="javascript:void(0);"><i class="fa fa-plus"></i>添加热点</a>
            <p class="notic"><?php echo $lang['vote_activity_entry_image_link_url_explain'];?></p>
          </dd>
        </dl>
        <dl class="row">
          <dt class="tit">已添加的热点区域</dt>
          <dd class="opt">
            <ul nctype="list" class="hot-point-list">
            </ul>
          </dd>
        </dl>
        <div class="bot"><a nctype="btn_image_insert_hot_point" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" ><?php echo $lang['vote_activity_entry_insert_editor'];?></a></div>
      </div>
    </div>
  </div>
  <!-- 插入商品对话框 -->
  <div id="_dialog_vote_activity_entry_insert_goods" style="display:none;">
    <div class="upload_adv_dialog dialog-special-insert-goods">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['vote_activity_entry_goods_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <dl class="row">
          <dt class="tit"> <?php echo $lang['vote_activity_entry_goods_url'];?></dt>
          <dd class="opt">
            <input nctype="_input_goods_link" type="text" class="input-txt"/>
            <a class="ncap-btn" nctype="btn_vote_activity_entry_goods" href="javascript:void(0);"><?php echo $lang['cms_text_save'];?></a>
            <p class="notic"><?php echo $lang['vote_activity_entry_goods_explain3'];?></p>
          </dd>
        </dl>
        <div class="dialog-goods">
          <ul nctype="_vote_activity_entry_goods_list" class="special-goods-list">
          </ul>
        </div>
        <div class="bot"><a nctype="btn_vote_activity_entry_insert_goods" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green"><?php echo $lang['vote_activity_entry_insert_editor'];?></a></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<!--<script type="text/javascript" src="--><?php //echo ADMIN_RESOURCE_URL;?><!--/js/cms/vote_activity_entry.js" charset="utf-8"></script>-->
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<!--<script src="--><?php //echo SHOP_RESOURCE_SITE_URL;?><!--/js/store_goods_add.step2.js"></script>-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#getMember').click(function(){
            getInfo();
        });
        $("#btn_draft").click(function() {
            $("#vote_activity_entry_state").val("draft");
            $("#add_form").submit();
        });
        $("#btn_publish").click(function() {
            $("#vote_activity_entry_state").val("publish");
            $("#add_form").submit();
        });
        $("#check_true").click(function() {
            $("#vote_activity_entry_state").val("check_true");
            $("#add_form").submit();
        });
        $("#check_false").click(function() {
            $("#vote_activity_entry_state").val("check_false");
            $("#add_form").submit();
        });
        $('#add_form').validate({
            errorPlacement: function(error, element){
                error.appendTo(element.parents("tr").prev().find('td:first'));
            },
            rules : {
                <?php if(empty($output['detail'])) {?>
                image: {
                    required : true
                },
                <?php } ?>
                member_id: {
                    required : true,
                },
                auther: {
                    required : true,
                },
                name: {
                    required : true,
                },
            },
            messages : {
                <?php if(empty($output['detail'])) {?>
                image: {
                    required : "<?php echo $lang['vote_activity_entry_image_error'];?>"
                },
                <?php } ?>
                member_id: {
                    required : "会员ID必填",
                },
                auther: {
                    required : "作者必填",
                },
                name: {
                    required : "作品名称必填",
                },
            }
        });


        $('#add_album').fileupload({
            dataType: 'json',
            url: 'index.php?act=vote_activity_entry&op=vote_activity_entry_image_upload&type=add_album',
            formData: {name: 'add_album'},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                result = data.result;
                KE_vote_activity_entry_content.appendHtml('mobile', '<img src="'+ result.file_url + '">');
            }
        });
        $('#add_album2').fileupload({
            dataType: 'json',
            url: 'index.php?act=vote_activity_entry&op=vote_activity_entry_image_upload&type=add_album2',
            formData: {name: 'add_album2'},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                result = data.result;
                console.log(result.file_url);
                KE_vote_activity_entry_content.appendHtml('vote_activity_entry_content', '<img src="'+ result.file_url + '">');
            }
        });

    });
    function getInfo(){
        var u_name = $('#u_name').val();
        var url = 'index.php?act=vote_activity_entry&op=get_member_info&user_name='+u_name;
        $.ajax({
            type:'get',
            url:url,
            dataType:'json',
            success:function(res){
                if(!res.member_id){
                    alert('用户不存在');
                    window.location.reload();
                }
                $('#member_id').val(res.member_id);
            }
        });
    }
</script>