<div class="prism-player" id="J_prismPlayer"></div>
<script>
  var player = new prismplayer({
      id: "J_prismPlayer", // 容器id
      source: "<?php echo $output['movie_info']['play_url']; ?>",  // 视频url 支持互联网可直接访问的视频地址
      autoplay: false,      // 自动播放
      width: "100%",       // 播放器宽度
      height: "400px",      // 播放器高度
    skinLayout: [
        {
            "align":"blabs",
            "x":0,
            "y":0,
            "name":"controlBar",
            "children":[
                {
                    "align":"tlabs",
                    "x":0,
                    "y":0,
                    "name":"progress"
                },{
                    "align":"tl",
                    "x":15,
                    "y":26,
                    "name":"playButton"
                },{
                    "align":"tl",
                    "x":10,
                    "y":24,
                    "name":"timeDisplay"
                },{
                    "align":"tr",
                    "x":20,
                    "y":23,
                    "name":"streamButton"
                },{
                    "align":"tr",
                    "x":20,
                    "y":25,
                    "name":"volume"
                }
            ]
        }
    ]
  });
</script>