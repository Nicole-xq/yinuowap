<?php defined('InShopNC') or exit('Access Invalid!');?>
<?php if(!empty($output['store_list']) && is_array($output['store_list'])){ ?>

<ul class="search-goods-list">
  <?php foreach($output['store_list'] as $key => $value){ ?>
  <li>
    <div class="goods-name"><?php echo $value['store_name'];?></div>
    <div class="goods-price">关注：<?php echo $value['store_collect'];?></div>
    <div class="goods-pic"><img title="<?php echo $value['store_name'];?>" src="<?php echo getStoreLogo($value['store_label'],'store_logo');?>" /></div>
    <a nctype="btn_add_goods" data-store-id="<?php echo $value['store_id'];?>" data-store-name="<?php echo $value['store_name'];?>"  data-store-logo="<?php echo getStoreLogo($value['store_label'],'store_logo');?>" data-store-collect="<?php echo $value['store_collect'];?>" data-store-id="<?php echo $value['store_id'];?>" href="javascript:;">添加</a> </li>
  <?php } ?>
</ul>
<div id="goods_pagination" class="pagination"> <?php echo $output['show_page'];?> </div>
<?php }else { ?>
<p class="no-record"><?php echo $lang['nc_no_record'];?></p>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#goods_pagination').find('.demo').ajaxContent({
            event:'click', 
            loaderType:"img",
            loadingMsg:"<?php echo ADMIN_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#mb_special_goods_list'
        });
    });
</script> 
