<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .mb-item-edit-content .search-goods-list .goods-price{
        color:#777 !important;
    }
</style>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){ ?>

    <ul class="search-goods-list">
        <?php foreach($output['goods_list'] as $key => $value){ ?>
            <li>
                <div class="goods-name"><?php echo $value['special_name'];?></div>
                <div class="goods-price">开拍时间<?php echo date('m月d日 H:i',$value['special_start_time']);?></div>
                <div class="goods-price">结束时间<?php echo date('m月d日 H:i',$value['special_end_time']);?></div>
                <div class="goods-pic"><img title="<?php echo $value['special_name'];?>" src= "<?php echo getVendueLogo($value['special_image']);?>" /></div>

                <a nctype="btn_add_goods" data-goods-id="<?php echo $value['special_id'];?>" data-goods-name="<?php echo $value['special_name'];?>" data-goods-price="<?php echo $value['gs_min_price'];?>-<?php echo $value['gs_max_price'];?>" data-goods-image="<?php echo getVendueLogo($value['special_image']);?>" href="javascript:;">添加</a> </li>
        <?php } ?>
    </ul>
    <div id="goods_pagination" class="pagination"> <?php echo $output['show_page'];?> </div>
<?php }else { ?>
    <p class="no-record"><?php echo $lang['nc_no_record'];?></p>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#goods_pagination').find('.demo').ajaxContent({
            event:'click',
            loaderType:"img",
            loadingMsg:"<?php echo ADMIN_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#mb_special_goods_list'
        });
    });
</script>
