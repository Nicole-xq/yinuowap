<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="page">
    <!-- 页面导航 -->
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>模板设置</h3>
                <h5>手机客户端首页/分类名称模板设置</h5>
            </div>

        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>点击添加分类名称按钮可以添加新的分类名称，分类名称可以点击后直接修改</li>
            <li>点击编辑按钮对分类名称内容进行修改</li>
            <li>点击删除按钮可以删除整个分类名称</li>
        </ul>
    </div>

    <div id="flexigrid"></div>
</div>
<form id="del_form" action="<?php echo urlAdminMobile('mb_auction', 'class_del');?>" method="post">
    <input type="hidden" id="del_class_id" name="mb_class_id">
</form>
<div id="dialog_add_mb_class" style="display:none;">
    <form id="add_form" method="post" action="<?php echo urlAdminMobile('mb_auction', 'class_save');?>">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="special_desc"><em>*</em>分类名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="" name="mb_class_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot"><a id="submit" href="javascript:void(0)" class="ncap-btn-big ncap-btn-green"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.edit.js"></script>
<script type="text/javascript">
    function update_flex(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=mb_auction&op=get_className_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '编号', name : 'mb_class_id', width : 150, sortable : false, align: 'center'},
                {display: '分类名称', name : 'mb_class_name',  width : 360, sortable : false, align: 'left'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增分类名称', name : 'add', bclass : 'add', title : '新增分类名称', onpress : fg_operation_add }
            ],
            usepager: true,
            rp: 15,
            title: '分类名称列表'
        });
    }

    //编辑分类名称
    $('span[nc_type="edit_class_name"]').live('click', function() {
        if($(this).attr("edit")>0) return;
        $(this).inline_edit({act: 'mb_auction',op: 'update_class_name'});
        $(this).attr("edit",1);
    });
    //添加分类名称
    function fg_operation_add() {
        $('#dialog_add_mb_class').nc_show_dialog({title: '新增分类名称'});
    }
    //删除分类名称
    function fg_operation_del(special_id){
        if(confirm('确认删除?')) {
            $('#del_class_id').val(special_id);
            $('#del_form').submit();
        }
    }
    $(function(){
        update_flex();
        //提交
        $("#submit").click(function(){
            $("#add_form").submit();
        });

        $('#add_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                mb_class_name : {
                    required : true
                }
            },
            messages : {
                mb_class_name : {
                    required : "<i class='fa fa-exclamation-circle'></i>分类名称不能为空"
                }
            }
        });
    });
</script>
