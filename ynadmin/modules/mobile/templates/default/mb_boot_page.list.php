<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>引导页设置列表</h3>
                <h5>进入首页后优先弹出的引导浮层</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>">
                <?php echo $lang['nc_prompts'];?>
            </h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
        </div>
        <ul>
            <li>每个引导页记录只有一个开启状态，其他的是关闭状态</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    function update_flex(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=mb_boot_page&op=get_boot_xml',
            colModel : [
                {display: '操作', name : 'operation', width:50, sortable : false, align: 'center', className: 'handle-s'},
                {display: 'ID', name : 'id', width : 50, sortable : false, align: 'center'},
                {display: '引导页标题', name : 'title', width : 200, sortable : false, align: 'left'},
                {display: '图片', name : 'boot_image', width : 50, sortable : false, align: 'left'},
                {display: '链接地址', name : 'wap_link', width : 200, sortable : false, align: 'left'},
                {display: '点击数', name : 'click_num', width : 50, sortable : true, align: 'left'},
                {display: '点赞数', name : 'thumbs_up_num', width : 50, sortable : true, align: 'left'},
                {display: '分享数', name : 'share_num', width : 50, sortable : true, align: 'left'},
                {display: '是否分享', name : 'share_state', width : 50, sortable : false, align: 'center'},
                {display: '开启状态', name : 'boot_state', width : 50, sortable : false, align: 'center'},
                {display: '添加时间', name : 'add_time', width : 150, sortable : false, align: 'center'},
                {display: '更新时间', name : 'update_time', width : 150, sortable : false, align: 'center'},
                {display: '分享描述', name : 'share_desc', width : 100, sortable : false, align: 'left'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增引导页', name : 'add', bclass : 'add', title : '新增引导页', onpress : fg_operation_add }
            ],
            usepager: true,
            rp: 15,
            title: '引导页列表'
        });
    }
    function fg_operation_add(name, bDiv){
        var _url = 'index.php?act=mb_boot_page&op=boot_add';
        window.location.href = _url;
    }
    $(function(){
        update_flex();
    });
</script>
