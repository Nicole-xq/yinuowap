<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>会销管理</h3>
        <h5>会销页面设置与模板编辑</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
<!--      <li>--><?php //echo $lang['cms_special_list_tip1'];?><!--</li>-->
      <li>会销类型分为资讯和商城，资讯会销将出现在资讯频道内，商城会销出现在商城使用商城统一风格</li>
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script>
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=exhibition_setting&op=exhibition_list_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: 'ID', name : 'special_title', width : 50, sortable : false, align: 'center'},
            {display: '标题', name : 'special_title', width : 250, sortable : false, align: 'center'},
            {display: '状态', name : 'special_state',  width : 160, sortable : false, align: 'left'},
            {display: '创建时间', name : 'special_add_time', width : 120, sortable : false, align: 'center'},
            {display: '报名截止时间', name : 'entry_end_time', width : 120, sortable : false, align: 'center'},
        ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增会销', name : 'add', bclass : 'add', title : '新增会销', onpress : fg_operation }
        ],
        usepager: true,
        rp: 15,
        title: '会销列表'
    });
}

$(function(){
    update_flex();
});
//删除会销
function fg_operation_del(id){
    if (confirm('确定删除？')) {
        window.location.href = 'index.php?act=exhibition_setting&op=exhibition_drop&id='+id;
    }
}
function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=exhibition_setting&op=exhibition_add';
    }
}
</script>