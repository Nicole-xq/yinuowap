<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>报名管理</h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
<!--      <li>--><?php //echo $lang['cms_special_list_tip1'];?><!--</li>-->
<!--      <li>会销类型分为资讯和商城，资讯会销将出现在资讯频道内，商城会销出现在商城使用商城统一风格</li>-->
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script>
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=vote_activity_entry&op=vote_activity_entry_list_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: 'ID', name : 'id', width : 50, sortable : false, align: 'center'},
            {display: '活动ID', name : 'activity_id', width : 50, sortable : false, align: 'center'},
            {display: '活动标题', name : 'title', width : 250, sortable : false, align: 'center'},
            {display: '作品编号', name : 'entry_no', width : 250, sortable : false, align: 'center'},
            {display: '用户ID', name : 'end_time', width : 250, sortable : false, align: 'center'},
            {display: '状态', name : 'cdate', width : 120, sortable : false, align: 'center'},
            {display: '创建时间', name : 'cdate', width : 120, sortable : false, align: 'center'},
        ],
        search:[

        ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增报名', name : 'add', bclass : 'add', title : '新增报名', onpress : fg_operation }
        ],
        usepager: true,
        rp: 15,
        title: '报名列表'
    });
}

$(function(){
    update_flex();
});
//删除会销
function fg_operation_del(id){
    if (confirm('确定删除？')) {
        window.location.href = 'index.php?act=vote_activity_entry&op=vote_activity_entry_drop&id='+id;
    }
}
function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=vote_activity_entry&op=vote_activity_entry_add';
    }
}
</script>