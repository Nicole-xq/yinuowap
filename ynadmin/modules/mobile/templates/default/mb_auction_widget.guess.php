<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .mb-item-edit-content .search-goods-list li{
        height:90px !important;
    }

</style>
<?php if(!empty($output['goods_list']) && is_array($output['goods_list'])){ ?>

    <ul class="search-goods-list">
        <?php foreach($output['goods_list'] as $key => $value){ ?>
            <li>
                <div class="goods-name"><?php echo $value['gs_name'];?></div>
                <div class="goods-price">￥<?php echo $value['gs_min_price'];?>-￥<?php echo $value['gs_max_price'];?></div>
                <div class="goods-pic"><img title="<?php echo $value['gs_name'];?>" src= "<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$value['gs_img'];?>" /></div>
                <div class="goods-price" style="color:#777;">开始时间<?php echo date('m月d日 H:i',$value['start_time']);?></div>
                <div class="goods-price" style="color:#777;">结束时间<?php echo date('m月d日 H:i',$value['end_time']);?></div>
                <a nctype="btn_add_goods" data-goods-id="<?php echo $value['gs_id'];?>" data-goods-name="<?php echo $value['gs_name'];?>" data-goods-price="<?php echo $value['gs_min_price'];?>-<?php echo $value['gs_max_price'];?>" data-goods-image="<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$value['gs_img'];?>" href="javascript:;">添加</a> </li>
        <?php } ?>
    </ul>
    <div id="goods_pagination" class="pagination"> <?php echo $output['show_page'];?> </div>
<?php }else { ?>
    <p class="no-record"><?php echo $lang['nc_no_record'];?></p>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#goods_pagination').find('.demo').ajaxContent({
            event:'click',
            loaderType:"img",
            loadingMsg:"<?php echo ADMIN_TEMPLATES_URL;?>/images/transparent.gif",
            target:'#mb_special_goods_list'
        });
    });
</script>
