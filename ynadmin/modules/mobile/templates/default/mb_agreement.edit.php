<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=mb_agreement" title="返回分类图片列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3><?php echo $lang['link_index_mb_agreement'];?> - <?php echo $lang['nc_edit'];?>“<?php echo $output['agreement']['agreement_title'];?>”</h3>
                <h5><?php echo $lang['link_index_mb_agreement_subhead'];?></h5>
            </div>
        </div>
    </div>
    <form id="add_form" method="post" enctype="multipart/form-data" action="index.php?act=mb_agreement&op=agreement_save">
        <input name="agreement_id" type="hidden" value="<?php if(!empty($output['agreement'])) echo $output['agreement']['agreement_id'];?>" />
        <input id="special_state" name="special_state" type="hidden" value="" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="agreement_title"><em>*</em><?php echo $lang['link_index_title'];?></label>
                </dt>
                <dd class="opt">
                    <input id="agreement_title" name="agreement_title" class="input-txt" type="text" value="<?php if(!empty($output['agreement'])) echo $output['agreement']['agreement_title'];?>"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em><?php echo $lang['link_index_agreement_content'];?></label>
                </dt>
                <dd id="ncProductDetails" class="opt">
                    <div class="tabs">
                        <div id="panel-1" class="ui-tabs-panel">
                            <?php showEditor('agreement_content',$output['agreement']['agreement_content'],'100%','480px','visibility:hidden;',"false",$output['editor_multimedia'], 'all', 'agreement_content');?>
                            <div class="hr8">
                                <div class="ncsc-upload-btn">
                                    <a href="javascript:void(0);">
                                        <span><input type="file" hidefocus="true" size="1" class="input-file" name="add_album" id="add_album" multiple></span>
                                        <p><i class="icon-upload-alt" data_type="0" nctype="add_album_i"></i>图片上传</p>
                                    </a>
                                </div>
                            </div>
                            <p class="notic"><span class="vatop rowform">建议上传宽度400像素的图片</span></p>
                            <p id="des_demo"></p>
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
        <div class="ncap-form-default">
            <div class="bot">
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="btn_publish"><?php echo $lang['nc_submit'];?></a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_draft").click(function() {
            $("#special_state").val("draft");
            $("#add_form").submit();
        });
        $("#btn_publish").click(function() {
            $("#special_state").val("publish");
            $("#add_form").submit();
        });

        $('#add_album').fileupload({
            dataType: 'json',
            url: 'index.php?act=mb_agreement&op=agreement_image_upload&type=add_album',
            formData: {name: 'add_album'},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                result = data.result;
                KE_agreement_content.appendHtml('special_mobile', '<img src="'+ result.file_url + '">');
            }
        });
    });
</script>