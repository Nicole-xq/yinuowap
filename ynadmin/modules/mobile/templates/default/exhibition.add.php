<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=exhibition_setting&op=exhibition_list" title="返回专题列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>会销管理 -  新增会销页</h3>
        <h5>新建会销</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data" action="index.php?act=exhibition_setting&op=exhibition_save">
    <input name="exhibition_id" type="hidden" value="<?php if(!empty($output['detail'])) echo $output['detail']['id'];?>" />
    <input id="exhibition_state" name="exhibition_state" type="hidden" value="" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="exhibition_title"><em>*</em><?php echo $lang['cms_text_title'];?></label>
        </dt>
        <dd class="opt">
          <input id="exhibition_title" name="exhibition_title" class="input-txt" type="text" value="<?php if(!empty($output['detail'])) echo $output['detail']['title'];?>"/>
          <span class="err"></span>
          <p class="notic"><?php echo $lang['exhibition_title_explain'];?></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="exhibition_description"><em>*</em>描述</label>
        </dt>
        <dd class="opt">
          <input id="exhibition_description" name="exhibition_description" class="input-txt" type="text" value="<?php if(!empty($output['detail'])) echo $output['detail']['description'];?>"/>
          <span class="err"></span>
          <p class="notic">分享使用</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
            <label for="exhibition_description"><em>*</em>报名截止时间</label>
        </dt>
        <dd class="opt">
            <input type="text" id="entry_end_time" name="entry_end_time" data-dp="1" class="s-input-txt" value="<?php if(!empty($output['detail'])) echo $output['detail']['entry_end_time'];?>" />
            <span class="err"></span>
            <p class="notic"></p>
        </dd>
      </dl>

      <dl class="row">
        <dt class="tit">
          <label><em>*</em>微信分享logo</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php if(!empty($output['detail']['share_logo'])){ echo getExhibitionImageUrl($output['detail']['share_logo']);} else {echo ADMIN_TEMPLATES_URL . '/images/preview.png';}?>"><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php if(!empty($output['detail']['share_logo'])){ echo getExhibitionImageUrl($output['detail']['share_logo']);} else {echo ADMIN_TEMPLATES_URL . '/images/preview.png';}?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input class="type-file-file" id="exhibition_share_logo" name="exhibition_share_logo" type="file" size="30" hidefocus="true" nctype="cms_image" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            <input name="old_exhibition_share_image" type="hidden" value="<?php echo $output['detail']['share_logo'];?>" />
            </span></div>
          <span class="err"></span>
          <p class="notic"><span class="vatop rowform"><?php echo $lang['special_image_explain'];?></span></p>
        </dd>
      </dl>

      <dl class="row">
        <dt class="tit">wap链接</dt>
        <dd class="opt">
          <?php if($output['detail']['wap_url'] !=''){?>
            <a target="_blank" href="<?php echo $output['detail']['wap_url'];?>"><?php echo $output['detail']['wap_url'];?> </a>
          <?php }?>
        </dd>
      </dl>

      <dl class="row">
        <dt class="tit">wap<?php echo $lang['exhibition_content'];?></dt>
        <dd id="ncProductDetails1" class="opt">
          <div class="tabs1">
            <div id="panel-2" class="ui-tabs-panel ui-tabs-hide">
              <?php showEditor('exhibition_content',$output['detail']['content'],'100%','480px','visibility:hidden;',"false",$output['editor_multimedia'], 'all', 'exhibition_content');?>
              <div class="hr8">
                <div class="ncsc-upload-btn"> <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="add_album2" id="add_album2" multiple>
                  </span>
                    <p><i class="icon-upload-alt" data_type="0" nctype="add_album_i"></i>图片上传</p>
                  </a>
                </div>
              </div>
              <p id="des_demo1"></p>
            </div>
          </div>
        </dd>
      </dl>


    </div>


    <div class="ncap-form-default">
      <div class="bot">
        <!-- <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-blue" id="btn_draft"><?php echo $lang['exhibition_draft'];?></a> -->
        <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="btn_publish">发布</a>
      </div>
    </div>
  </form>
  <!-- 插入图片链接对话框 -->
  <div id="_dialog_image_insert_link" style="display:none;">
    <div class="upload_adv_dialog dialog-image-insert-link">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['exhibition_image_link_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <dl class="row">
          <dt class="tit">插入图片预览</dt>
          <dd class="opt">
            <div class="dialog-pic-thumb"><a><img alt="" src=""></a></div>
          </dd>
        </dl>
        <dl class="row" id="upload_adv_type">
          <dt class="tit"><?php echo $lang['exhibition_image_link_url'];?></dt>
          <dd class="opt">
            <input nctype="_image_insert_link" type="text" class="input-txt" placeholder="http://"/>
            <p class="notic"><?php echo $lang['exhibition_image_link_url_explain'];?>如不填加任何链接请保持默认。</p>
          </dd>
        </dl>
        <div class="bot"><a nctype="btn_image_insert_link" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" ><?php echo $lang['cms_text_save'];?></a></div>
      </div>
    </div>
  </div>
  <!-- 插入图片热点对话框 -->
  <div id="_dialog_image_insert_hot_point" style="display:none;">
    <div class="dialog-image-insert-hot-point">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['exhibition_image_link_hot_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <div ncytpe="div_image_insert_hot_point" class="special-hot-point"><img nctype="img_hot_point" alt="" src="<?php echo $image_url;?>"> </div>
        <dl class="row">
          <dt class="tit"><?php echo $lang['exhibition_image_link_hot_url'];?></dt>
          <dd class="opt">
            <input nctype="x1" type="hidden" />
            <input nctype="y1" type="hidden" />
            <input nctype="x2" type="hidden" />
            <input nctype="y2" type="hidden" />
            <input nctype="w" type="hidden" />
            <input nctype="h" type="hidden" />
            <input nctype="url" type="text" class="input-txt" placeholder="http://" />
            <a class="ncap-btn" nctype="btn_hot_point_commit" href="javascript:void(0);"><i class="fa fa-plus"></i>添加热点</a>
            <p class="notic"><?php echo $lang['exhibition_image_link_url_explain'];?></p>
          </dd>
        </dl>
        <dl class="row">
          <dt class="tit">已添加的热点区域</dt>
          <dd class="opt">
            <ul nctype="list" class="hot-point-list">
            </ul>
          </dd>
        </dl>
        <div class="bot"><a nctype="btn_image_insert_hot_point" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" ><?php echo $lang['exhibition_insert_editor'];?></a></div>
      </div>
    </div>
  </div>
  <!-- 插入商品对话框 -->
  <div id="_dialog_exhibition_insert_goods" style="display:none;">
    <div class="upload_adv_dialog dialog-special-insert-goods">
      <div class="s-tips"><i class="fa fa-lightbulb-o"></i><?php echo $lang['exhibition_goods_explain1'];?></div>
      <div class="ncap-form-default" id="upload_adv_type">
        <dl class="row">
          <dt class="tit"> <?php echo $lang['exhibition_goods_url'];?></dt>
          <dd class="opt">
            <input nctype="_input_goods_link" type="text" class="input-txt"/>
            <a class="ncap-btn" nctype="btn_exhibition_goods" href="javascript:void(0);"><?php echo $lang['cms_text_save'];?></a>
            <p class="notic"><?php echo $lang['exhibition_goods_explain3'];?></p>
          </dd>
        </dl>
        <div class="dialog-goods">
          <ul nctype="_exhibition_goods_list" class="special-goods-list">
          </ul>
        </div>
        <div class="bot"><a nctype="btn_exhibition_insert_goods" href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green"><?php echo $lang['exhibition_insert_editor'];?></a></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/cms/exhibition.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<!--<script src="--><?php //echo SHOP_RESOURCE_SITE_URL;?><!--/js/store_goods_add.step2.js"></script>-->
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-dp]').datepicker({dateFormat: 'yy-mm-dd'});

        $("#btn_draft").click(function() {
            $("#exhibition_state").val("draft");
            $("#add_form").submit();
        });
        $("#btn_publish").click(function() {
            $("#exhibition_state").val("publish");
            $("#add_form").submit();
        });
        $('#add_form').validate({
            errorPlacement: function(error, element){
                error.appendTo(element.parents("tr").prev().find('td:first'));
            },
            rules : {
                <?php if(empty($output['detail'])) {?>
                exhibition_image: {
                    required : true
                },
                <?php } ?>
                exhibition_title: {
                    required : true,
                    maxlength : 40,
                    minlength : 4
                },
                entry_end_time: {
                    required : true
                }
            },
            messages : {
                <?php if(empty($output['detail'])) {?>
                exhibition_image: {
                    required : "<?php echo $lang['exhibition_image_error'];?>"
                },
                <?php } ?>
                exhibition_title: {
                    required : "<?php echo $lang['cms_title_not_null'];?>",
                    maxlength : "<?php echo $lang['cms_title_max'];?>",
                    minlength : "<?php echo $lang['cms_title_min'];?>"
                },
                entry_end_time: {
                    required : "请选择报名截止时间"
                }
            }
        });


        $('#add_album').fileupload({
            dataType: 'json',
            url: 'index.php?act=exhibition_setting&op=exhibition_image_upload&type=add_album',
            formData: {name: 'add_album'},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                result = data.result;
                KE_exhibition_content.appendHtml('mobile', '<img src="'+ result.file_url + '">');
            }
        });
        $('#add_album2').fileupload({
            dataType: 'json',
            url: 'index.php?act=exhibition_setting&op=exhibition_image_upload&type=add_album2',
            formData: {name: 'add_album2'},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                result = data.result;
                console.log(result.file_url);
                KE_exhibition_content.appendHtml('exhibition_content', '<img src="'+ result.file_url + '">');
            }
        });
    });
</script>