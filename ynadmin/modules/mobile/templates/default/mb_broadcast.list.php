<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="page"> 
  <!-- 页面导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>推送公告</h3>
        <h5>手机客户端接收网站公告信息</h5>
      </div>
    </div>
  </div>
  <div id="flexigrid"></div>
</div>
</div>
<script type="text/javascript">
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=mb_broadcast&op=get_list_xml',
        colModel : [
            {display: '标题', name : 'title', width : 100, sortable : false, align: 'center'},
            {display: '副标题', name : 'subtitle', width : 200, sortable : false, align: 'center'},
            {display: '内容', name : 'content', width : 300, sortable : false, align: 'center'},
            {display: '公告打开链接', name : 'link', width : 200, sortable : false, align: 'left'},
            {display: '状态', name : 'state', width : 100, sortable : false, align: 'center'},
            {display: '操作人员', name : 'admin_name', width : 150, sortable : false, align: 'center'},
            {display: '添加时间', name : 'state',  width : 150, sortable : false, align: 'center'},
            {display: '操作', name : 'operation', width : 200, sortable : false, align: 'center', className: 'handle-s'},
        ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增公告', name : 'add', bclass : 'add', title : '新增公告', onpress : fg_operation_add }
        ],
        usepager: true,
        rp: 15,
        title: '公告列表'
    });
}
function fg_operation_add(name, bDiv){
    window.location.href = 'index.php?act=mb_broadcast&op=add_broadcast';
}
$(function(){
    update_flex();
});
</script> 
