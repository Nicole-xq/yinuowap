<?php
/**
 * 拍品保证金订单
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/14 0014
 * Time: 15:12
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class margin_orderControl extends SystemControl
{
    /**
     * 每次导出订单数量
     * @var int
     */
    const EXPORT_SIZE = 200;

    public function __construct()
    {
        parent::__construct ();
        Language::read('trade');
    }

    /**
     * 订单列表
     * */
    public function indexOp()
    {
        //显示支付接口列表(搜索)
        $payment_list = Model('payment')->getPaymentOpenList();
        $payment_list['wxpay'] = array(
            'payment_code' => 'wxpay',
            'payment_name' => '微信支付'
        );
        Tpl::output('payment_list',$payment_list);
        Tpl::showpage('margin_order.index');
    }

    /**
     * 获取表格内容
     * */
    public function get_xmlOp()
    {
        $model_margin_orders = Model('margin_orders');
        $model_auctions = Model('auctions');
        $login_auction = Logic('auction');
        $condition  = array();

        $this->_get_condition($condition);
        $sort_fields = array('buyer_name','store_name','margin_id','payment_code','auction_name','order_state','margin_amount','order_from','rcb_amount','pd_amount','points_amount','account_margin_amount','payment_time','trade_no','finnshed_time','refund_amount','buyer_id','store_id');
        $sort_fields[] = 'refund_state';
        $sort_fields[] = 'default_state';
        $sort_fields[] = 'api_refund_state';
        $sort_fields[] = 'rufund_money';
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }

        // 获取数据列表
        $order_list = $model_margin_orders->getOrderList2($condition,'*','',$order,0,$_POST['rp']);
        $data = array();
        $data['now_page'] = $model_margin_orders->shownowpage();
        $data['total_num'] = $model_margin_orders->gettotalnum();
        foreach ($order_list as $key => $order_info) {
            $auctions_goods = $model_auctions->getAuctionsInfo(['auction_id'=>$order_info['auction_id']],'auction_id,store_id,auction_start_time,auction_end_time,auction_bond_rate,interest_last_date');
            $auction_bond_rate = $auctions_goods['auction_bond_rate']?$auctions_goods['auction_bond_rate']:'0.00';
            $bond_day = 0;
            //已支付  and  支付时间 < 最后计息日期
            if($order_info['order_state'] == 1 && $order_info['payment_time'] < $auctions_goods['interest_last_date']){
                if($auctions_goods['store_id']==0){
                    $bond_day = $login_auction->getInterestDay($order_info['payment_time'],$auctions_goods['auction_start_time']);
                }else{
                    $bond_day = $login_auction->getInterestDay($order_info['payment_time'],$auctions_goods['auction_end_time']);
                }
            }
            $order_info['if_system_cancel'] = $model_margin_orders->getOrderOperateState('system_cancel',$order_info);
            $order_info['if_system_receive_pay'] = $model_margin_orders->getOrderOperateState('system_receive_pay',$order_info);
            $order_info['if_system_underline_confirm'] = $model_margin_orders->getOrderOperateState('system_underline_confirm',$order_info);
            $order_info['state_desc'] = orderMarginState($order_info);
            $list = array();$operation_detail = '';
            $list['operation'] = "";
            $margin_id = $order_info['margin_id'];
            if ($order_info['order_state'] == 1) {
                if($order_info['financial_verification']){
                    $list['operation'] .= "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_margin_orders({margin_id:$margin_id,msg:'未校验'})\" style='background: #cccccc;border-color: #cccccc'>已校验</a>";
                }else{
                    $list['operation'] .= "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_margin_orders({margin_id:$margin_id,msg:'已校验'})\" style='background-color: #1BBC9D;color: #ffffff;border-color: #16A086'>未校验</a>";
                }
            }else {
                $list['operation'] .= "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
            }
            if ($order_info['if_system_cancel']) {
                $operation_detail .= "<li><a href=\"javascript:void(0);\" onclick=\"fg_cancel({$order_info['margin_id']})\">取消订单</a></li>";
            }
            if ($order_info['if_system_receive_pay']) {
                $op_name = $order_info['system_receive_pay_op_name'] ? $order_info['system_receive_pay_op_name'] : '收到货款';
                $operation_detail .= "<li><a href=\"index.php?act=margin_order&op=change_state&state_type=receive_pay&margin_id={$order_info['margin_id']}\">{$op_name}</a></li>";
            }
            if ($order_info['if_system_underline_confirm']) {
                $operation_detail .= "<li><a href=\"index.php?act=margin_order&op=change_state&state_type=ud_confirm&margin_id={$order_info['margin_id']}\">确认凭据</a></li>";
            }

            if($order_info['order_type'] == 1 && $order_info['order_state'] == 1 ){
                $operation_detail .= "<li><a target='_blank' href=\"".UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_voucher'.DS.$order_info['pay_voucher']."\">查看支付凭据</a></li>";
            }
            if ($operation_detail) {
                $list['operation'] .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>{$operation_detail}</ul>";
            }

            $list['order_sn'] = $order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']);
            $list['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
            $list['buyer_name'] = $order_info['buyer_name'];
            $list['auction_id'] = $order_info['auction_id'];
            $list['created_at'] = date('Y-m-d H:i:s',$order_info['created_at']);
            $list['trade_no'] = $order_info['trade_no'];
            $list['margin_amount'] = ncPriceFormat($order_info['margin_amount']);
            $list['bond_rate'] = $auction_bond_rate."%";
            $list['day_num'] = $bond_day;
            $list['order_state'] = $order_info['state_desc'];
            $list['refund_state'] = $this->disposeapiRefundState($order_info['refund_state']);
            $list['default_state'] = $this->disposeDefaultState($order_info['default_state']);
            $list['payment_code'] = orderPaymentName($order_info['payment_code']);
            $list['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
            $list['rcb_amount'] = ncPriceFormat($order_info['rcb_amount']);
            $list['pd_amount'] = ncPriceFormat($order_info['pd_amount']);
            $list['points_amount'] = ncPriceFormat($order_info['points_amount']);
            $list['account_margin_amount'] = ncPriceFormat($order_info['account_margin_amount']);
            $list['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
            $list['store_id'] = $order_info['store_id'];
            $list['store_name'] = $order_info['store_name'];
            $list['goods_name'] = $order_info['auction_name'];
            $list['buyer_id'] = $order_info['buyer_id'];
            $data['list'][$order_info['margin_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
    public function disposeDefaultState($defaultState)
    {
        //是否违约：0:不违约，1:违约，2:释放退款
        switch ($defaultState)
        {
            case '1':
                $msg = "<font color='red'>违约</font>";
                break;
            case '2':
                $msg = '释放退款';
                break;
            default:
                $msg = '不违约';
                break;
        }
        return $msg;
    }

    public function disposeApiRefundState($apiRefundState)
    {
        //0: 未微信退款，1: 微信退款成功，2: 微信退款失败
        switch ($apiRefundState)
        {
            case '1':
                $msg = "<font color='green'>微信退款成功</font>";
                break;
            case '2':
                $msg = "<font color='red'>微信退款失败</font>";
                break;
            default:
                $msg = '未微信退款';
                break;
        }
        return $msg;
    }

    /**
     * 处理搜索条件
     * */
    private function _get_condition(& $condition)
    {
        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('order_sn','store_name','buyer_name'))) {
            $condition[$_REQUEST['qtype']] = array('like',"%{$_REQUEST['query']}%");
        }
        if ($_GET['keyword'] != '' && in_array($_GET['keyword_type'],array('order_sn','store_name','buyer_name','buyer_phone','auction_name'))) {
            if ($_GET['jq_query']) {
                $condition[$_GET['keyword_type']] = $_GET['keyword'];
            } else {
                $condition[$_GET['keyword_type']] = array('like',"%{$_GET['keyword']}%");
            }
        }
        if (!in_array($_GET['qtype_time'],array('created_at','payment_time','finnshed_time'))) {
            $_GET['qtype_time'] = null;
        }
        $if_start_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_time ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_time ? strtotime($_GET['query_end_date']): null;
        if ($_GET['qtype_time'] && ($start_unixtime || $end_unixtime)) {
            $condition[$_GET['qtype_time']] = array('time',array($start_unixtime,$end_unixtime));
        }
        if($_GET['payment_code']) {
            if ($_GET['payment_code'] == 'wxpay') {
                $condition['payment_code'] = array('in',array('wxpay','wx_saoma','wx_jsapi'));
            } elseif($_GET['payment_code'] == 'alipay') {
                $condition['payment_code'] = array('in',array('alipay','ali_native'));
            } else {
                $condition['payment_code'] = $_GET['payment_code'];
            }
        }
        if(in_array($_GET['order_state'],array('0','1','2','3','4'))){
            $condition['order_state'] = $_GET['order_state'];
        }
        if (floatval($_GET['query_start_amount']) > 0 && floatval($_GET['query_end_amount']) > 0 && $_GET['query_amount']) {
            $condition[$_GET['query_amount']] = array('between',floatval($_GET['query_start_amount']).','.floatval($_GET['query_end_amount']));
        }
        if(in_array($_GET['order_from'],array('1','2'))){
            $condition['order_from'] = $_GET['order_from'];
        }
    }

    /**
     * 平台订单状态操作
     *
     */
    public function change_stateOp() {
        $margin_id = intval($_GET['margin_id']);
        if($margin_id <= 0){
            showMessage(L('miss_order_number'),$_POST['ref_url'],'html','error');
        }
        $model_margin_orders = Model('margin_orders');

        //获取订单详细
        $condition = array();
        $condition['margin_id'] = $margin_id;
        $order_info = $model_margin_orders->getOrderInfo($condition);
        if ($_GET['state_type'] == 'cancel') {
            $result = $this->_order_cancel($order_info);
        } elseif ($_GET['state_type'] == 'receive_pay') {
            $result = $this->_order_receive_pay($order_info,$_POST);
        } elseif ($_GET['state_type'] == 'ud_confirm') {
            $result = $this->_order_ud_confirm($order_info,$_POST);
        }
        if (!$result['state']) {
            showMessage($result['msg'],$_POST['ref_url'],'html','error');
        } else {
            showMessage($result['msg'],$_POST['ref_url']);
        }
    }

    /**
     * 系统取消订单
     */
    private function _order_cancel($order_info) {
        $model_margin_orders = Model('margin_orders');
        $logic_auction_order = Logic('auction_order');
        $if_allow = $model_margin_orders->getOrderOperateState('system_cancel',$order_info);
        if (!$if_allow) {
            return callback(false,'无权操作');
        }
        // 取消订单
        $result =  $logic_auction_order->changeOrderStateCancel($order_info,'admin', $this->admin_info['name'],'',true);

        if ($result['state']) {
            $this->log(L('margin_order_log_cancel').','.L('order_number').':'.$order_info['order_sn'],1);
        }
        if ($result['state']) {
            exit(json_encode(array('state'=>true,'msg'=>'取消成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'取消失败')));
        }
    }

    /**
     * 系统收到货款
     * @throws Exception
     */
    private function _order_receive_pay($order_info, $post) {
        $model_margin_orders = Model('margin_orders');
        $logic_auction_order = Logic('auction_order');
        $order_info['if_system_receive_pay'] = $model_margin_orders->getOrderOperateState('system_receive_pay',$order_info);
        if (!$order_info['if_system_receive_pay']) {
            return callback(false,'无权操作');
        }

        if (!chksubmit()) {
            Tpl::output('order_info',$order_info);
            //显示支付接口列表
            $payment_list = Model('payment')->getPaymentOpenList();
            //去掉预存款和货到付款
            foreach ($payment_list as $key => $value){
                if ($value['payment_code'] == 'predeposit' || $value['payment_code'] == 'offline') {
                    unset($payment_list[$key]);
                }
            }
            Tpl::output('payment_list',$payment_list);
            Tpl::showpage('margin_order.receive_pay');
            exit();
        }

        $result = $logic_auction_order->changeOrderReceivePay($order_info,'admin',$this->admin_info['name'],$post);
        if ($result['state']) {
            $this->log('将订单改为已收款状态,'.L('order_number').':'.$order_info['order_sn'],1);
            //记录消费日志
            $api_pay_amount = $order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount'];
            QueueClient::push('addConsume', array('member_id'=>$order_info['buyer_id'],'member_name'=>$order_info['buyer_name'],
                'consume_amount'=>$api_pay_amount,'consume_time'=>TIMESTAMP,'consume_remark'=>'管理员更改订单为已收款状态，订单号：'.$order_info['order_sn']));
        }
        return $result;
    }

    /**
     * 系统确认凭据
     * @throws Exception
     */
    private function _order_ud_confirm($order_info, $post) {
        $model_margin_orders = Model('margin_orders');
        $logic_auction_order = Logic('auction_order');
        $order_info['if_system_underline_confirm'] = $model_margin_orders->getOrderOperateState('system_underline_confirm',$order_info);
        if (!$order_info['if_system_underline_confirm']) {
            return callback(false,'无权操作');
        }

        if (!chksubmit()) {
            Tpl::output('order_info',$order_info);
            Tpl::showpage('margin_order.ud_confirm');
            exit();
        }

        $post['payment_code'] = 'underline';

        $result = $logic_auction_order->changeOrderReceivePay($order_info,'admin',$this->admin_info['name'],$post);
        if ($result['state']) {
            $this->log('将订单改为已收款状态,'.L('order_number').':'.$order_info['order_sn'],1);
            //记录消费日志
            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount'];
            QueueClient::push('addConsume', array('member_id'=>$order_info['buyer_id'],'member_name'=>$order_info['buyer_name'],
                'consume_amount'=>$api_pay_amount,'consume_time'=>TIMESTAMP,'consume_remark'=>'管理员更改订单为已收款状态，订单号：'.$order_info['order_sn']));
        }
        return $result;
    }

    /**
     * 财务校验
     * @throws Exception
     */
    public function edit_order_stateOp() {
        $margin_id = intval($_POST['margin_id']);
        if($margin_id <= 0){
            exit(json_encode(array('state'=>false,'msg'=>'参数丢了')));
        }
        $model_margin_orders = Model('margin_orders');

        //获取订单详细
        $condition = array();
        $condition['margin_id'] = $margin_id;
        $order_info = $model_margin_orders->getOrderInfo($condition);
        if(empty($order_info)){
            exit(json_encode(array('state'=>false,'msg'=>'参数有误')));
        }

        if ($order_info['financial_verification'] == 1) {
            $data = ['financial_verification'=>0];
            $mag = '未校验';
        } else {
            $data = ['financial_verification'=>1];
            $mag = '已校验';
        }
        $result = $model_margin_orders->editOrder($data, ['margin_id'=>$order_info['margin_id']]);
        if ($result) {
            $this->log('将订单改为'. $mag .'状态,'.L('order_number').':'.$order_info['order_sn'],1);
            exit(json_encode(array('state'=>true,'msg'=>'修改成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'修改失败')));
        }
    }

    /**
     * 导出
     *
     */
    public function export_step1Op(){
        $lang   = Language::getLangContent();
        $model_order = Model('margin_orders');
        $condition  = array();
        if (preg_match('/^[\d,]+$/', $_GET['margin_id'])) {
            $_GET['margin_id'] = explode(',',trim($_GET['margin_id'],','));
            $condition['margin_id'] = array('in',$_GET['margin_id']);
        }
        $this->_get_condition($condition);

        $sort_fields = array('margin_id','buyer_name','store_name','payment_code','order_state','margin_amount','order_from','rcb_amount','pd_amount','payment_time','finnshed_time','buyer_id','store_id');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        } else {
            $order = 'margin_id desc';
        }

        if (!is_numeric($_GET['curpage'])){
            $count = $model_order->getOrderCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=margin_order&op=index');
                Tpl::showpage('export.excel');
            }else{  //如果数量小，直接下载
                $data = $model_order->getOrderList($condition,'*','',$order,self::EXPORT_SIZE);
                $this->createExcel($data);
            }
        }else{  //下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $data = $model_order->getOrderList($condition,'*','',$order,"{$limit1},{$limit2}");
            $this->createExcel($data);
        }
    }

    /**
     * 生成excel
     *
     * @param array $data
     */
    private function createExcel($data = array()){
        Language::read('export');
        import('libraries.excel');
        $excel_obj = new Excel();
        $excel_data = array();
        //设置样式
        $excel_obj->setStyle(array('id'=>'s_title','Font'=>array('FontName'=>'宋体','Size'=>'12','Bold'=>'1')));
        //header
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单编号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单来源');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'买家ID');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'买家账号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'下单时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单金额(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'年化利率');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'周期(天)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'支付状态');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'支付方式');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'支付时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'充值卡支付(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'预存款支付(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'诺币支付(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'保证金余额支付(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单完成时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'店铺ID');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'店铺名称');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'商品名称');

        $model_auctions = Model('auctions');
        $login_auction = Logic('auction');
        foreach ((array)$data as $k=>$order_info){
            $auctions_goods = $model_auctions->getAuctionsInfo(['auction_id'=>$order_info['auction_id']],'auction_id,auction_end_time,auction_bond_rate,interest_last_date');
            $auction_bond_rate = $auctions_goods['auction_bond_rate']?$auctions_goods['auction_bond_rate']:'0.00';
            $bond_day = 0;
            //已支付  and  支付时间 < 最后计息日期
            if($order_info['order_state'] == 1 && $order_info['payment_time'] < $auctions_goods['interest_last_date']){
                $bond_day = $login_auction->getInterestDay($order_info['payment_time'],$auctions_goods['auction_end_time']);
            }
            $order_info['state_desc'] = orderMarginState($order_info);
            $list = array();
            $list['order_sn'] = $order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']);
            $list['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];
            $list['created_at'] = date('Y-m-d H:i:s',$order_info['created_at']);
            $list['margin_amount'] = ncPriceFormat($order_info['margin_amount']);
            $list['bond_rate'] = $auction_bond_rate."%";
            $list['day_num'] = $bond_day;
            $list['order_state'] = $order_info['state_desc'];
            $list['payment_code'] = orderPaymentName($order_info['payment_code']);
            $list['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
            $list['rcb_amount'] = ncPriceFormat($order_info['rcb_amount']);
            $list['pd_amount'] = ncPriceFormat($order_info['pd_amount']);
            $list['points_amount'] = ncPriceFormat($order_info['points_amount']);
            $list['account_margin_amount'] = ncPriceFormat($order_info['account_margin_amount']);
            $list['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
            $list['store_id'] = $order_info['store_id'];
            $list['store_name'] = $order_info['store_name'];
            $list['goods_name'] = $order_info['auction_name'];

            $tmp = array();
            $tmp[] = array('data'=>$list['order_sn']);
            $tmp[] = array('data'=>$list['order_from']);
            $tmp[] = array('data'=>$list['buyer_id']);
            $tmp[] = array('data'=>$list['buyer_name']);
            $tmp[] = array('data'=>$list['created_at']);
            $tmp[] = array('data'=>$list['margin_amount']);
            $tmp[] = array('data'=>$list['bond_rate']);
            $tmp[] = array('data'=>$list['day_num']);
            $tmp[] = array('data'=>$list['order_state']);
            $tmp[] = array('data'=>$list['payment_code']);
            $tmp[] = array('data'=>$list['payment_time']);
            $tmp[] = array('data'=>$list['rcb_amount']);
            $tmp[] = array('data'=>$list['pd_amount']);
            $tmp[] = array('data'=>$list['points_amount']);
            $tmp[] = array('data'=>$list['account_margin_amount']);
            $tmp[] = array('data'=>$list['finnshed_time']);
            $tmp[] = array('data'=>$list['store_id']);
            $tmp[] = array('data'=>$list['store_name']);
            $tmp[] = array('data'=>$list['goods_name']);
            $excel_data[] = $tmp;
        }
        $excel_data = $excel_obj->charset($excel_data,CHARSET);
        $excel_obj->addArray($excel_data);
        $excel_obj->addWorksheet($excel_obj->charset(L('exp_od_order'),CHARSET));
        $excel_obj->generateXML('order-'.$_GET['curpage'].'-'.date('Y-m-d-H',time()));
    }
}
