<?php
/**
 * 专场管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_specialControl extends SystemControl{

    private $param = null;
    private $special_info = null;
    private $_links = array(
        array('url'=>'act=auction_special&op=all_special','text'=>'专场列表'),
        array('url'=>'act=auction_special&op=underway_special','text'=>'正在进行专场'),
        array('url'=>'act=auction_special&op=check_ok','text'=>'已审核专场'),
        array('url'=>'act=auction_special&op=index','text'=>'未审核专场')
//        array('url'=>'act=auction_special&op=platform_special','text'=>'平台专场')
    );

    public function __construct(){
        parent::__construct();
        Language::read('store,store_grade');
    }

    public function indexOp() {
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'index'));

        Tpl::showpage('auction_special.index');
    }

    public function all_specialOp() {
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'all_special'));

        Tpl::showpage('auction_special.all');
    }

    /**
     * 正在进行专场
     * */
    public function underway_specialOp(){
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'underway_special'));

        Tpl::showpage('auction_special.underway');
    }

    /**
     * 已审核专场
     */
    public function check_okOp(){

        Tpl::output('top_link',$this->sublink($this->_links,'check_ok'));

        Tpl::showpage('auction_special.ok');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_auction_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        if(!$_GET['is_all']){
            $condition['special_state'] = 10;
        }else{
            $condition['special_state'] = array('neq',0);
            $_GET['is_all'] == 2 && $condition['special_state'] = array('in',array(11,12));
        }
        $condition['special_sponsor'] = 1;
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = 'special_id desc';
        $param = array('special_id','store_id','store_name','special_name','special_image','special_state', 'special_start_time', 'special_end_time', 'special_preview_start','special_add_time','special_rate_time', 'auction_num', 'participants', 'special_rate');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();
        foreach ($special_list as $value) {
            $param = array();
            $operation = '';
            if($value['special_state'] == 10){
                $operation = "<a class='btn green' href='index.php?act=auction_special&op=special_check&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>审核</a>";
            }else{
                $operation = "<a class='btn green' href='index.php?act=auction_special&op=special_check&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>修改</a>";
            }
            $operation .= "<a class='btn green' href='index.php?act=auction_special&op=special_auction&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>查看专场商品</a>";
            $param['operation'] = $operation;

            $param['special_id'] = $value['special_id'];
            $param['store_id'] = $value['store_id'];
            $param['store_name'] = $value['store_name'];
            $param['special_name'] = $value['special_name'];
//            $param['special_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getVendueLogo($value['special_image']).">\")'><i class='fa fa-picture-o'></i></a>";
//            $param['special_state'] = '未审核';
            $param['special_start_time'] = date('Y-m-d H:i:s', $value['special_start_time']);
            $param['special_end_time'] = date('Y-m-d H:i:s', $value['special_end_time']);
            $param['special_preview_start'] = $value['special_preview_start'] ? date('Y-m-d H:i:s', $value['special_preview_start']) : '----';
            $param['special_rate_time'] = $value['special_rate_time'] ? date('Y-m-d H:i:s', $value['special_rate_time']) : '----';
            $param['auction_num'] = $value['auction_num'];//TODO 拍品件数
            $_GET['is_all'] == 2 && $param['participants'] = $value['participants'];
            $param['special_rate'] = $value['special_rate'].'%';
            $param['state'] = $value['special_state'] == 10?'未审核':$value['special_state'] == 30?'审核失败':'审核成功';
            $param['auction_state'] = $this->get_auction_state($value);

            $data['list'][$value['special_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 输出XML数据
     */
    public function check_get_xmlOp() {
        $model_auction_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        $condition['special_state'] = array('not in','0,10');
        $condition['special_sponsor'] = 1;
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = 'special_id desc';
        $param = array('store_id','store_name','special_name','special_image','special_state','special_start_time','special_end_time','special_preview_start','special_add_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();
        foreach ($special_list as $value) {
            $param = array();
            $operation = '';
            $operation .= "<a class='btn green' href='index.php?act=auction_special&op=special_check&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>查看</a>";
            $operation .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>";
            if ($value['is_rec'] == '1') {
                $operation .= '<li><a href="javascript:;" data-href="' . urlAdminShop('auction_special', 'special_rec', array(
                        'special_id' => $value['special_id'],
                        'rec' => 0,
                    )) . '">取消推荐</a></li>';
            } else {
                $operation .= '<li><a href="javascript:;" data-href="' . urlAdminShop('auction_special', 'special_rec', array(
                        'special_id' => $value['special_id'],
                        'rec' => 1,
                    )) . '">推荐专场</a></li>';
            }
            $operation .= "<li><a href='index.php?act=auction_special&op=special_auction&special_id=".$value['special_id']."'>查看专场商品</a></li>";
            if($value['special_preview_start'] > time()){
                $operation .= "<li><a href='javascript:void(0);' onclick=\"set_auction('" . $value['special_id'] . "')\">设置专场参数</a></li>";
            }
            $operation .= "</ul></span>";
            $param['operation'] = $operation;
            $param['special_id'] = $value['special_id'];
            $param['store_id'] = $value['store_id'];
            $param['store_name'] = $value['store_name'];
            $param['special_name'] = $value['special_name'];
            $param['special_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getVendueLogo($value['special_image']).">\")'><i class='fa fa-picture-o'></i></a>";
            if($value['special_state'] == 20 || $value['special_state'] == 11 || $value['special_state'] == 12 || $value['special_state'] == 13){
                $param['special_state'] = '审核成功';
            }elseif($value['special_state'] == 30){
                $param['special_state'] = '审核失败';
            }

            $param['special_start_time'] = date('Y-m-d H:i:s', $value['special_start_time']);
            $param['special_end_time'] = date('Y-m-d H:i:s', $value['special_end_time']);
            $param['special_preview_start'] = date('Y-m-d H:i:s', $value['special_preview_start']);
            $param['special_add_time'] = date('Y-m-d H:i:s', $value['special_add_time']);
            $data['list'][$value['special_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    //专场审核
    public function special_checkOp(){
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
        $rate_arr = Model('special_rate_config')->getList();
        $tmp = array();
        if(!empty($rate_arr)){
            foreach($rate_arr as $v){
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['rate'] = $v['rate'];
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['max_rate'] = $v['max_rate'];
            }
        }
//        echo '<pre>';
//        print_R($tmp);exit;
        Tpl::output('special_info', $special_info);
        Tpl::output('rate_arr',$tmp);
        Tpl::showpage('auction_special.detail');
    }


    //查看专场商品
    public function special_auctionOp(){
        $model_auction_special = Model('auction_special');
        $count = $model_auction_special->getSpecialGoodsCount(array('special_id'=>$_GET['special_id']));
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
//        print_R($special_info);exit;
        $special_auction_list = $model_auction_special->getSpecialGoodsList(array('special_id'=>$_GET['special_id']),'','','',0,12,$count);
//        print_R($special_auction_list);exit;
        $auction_list = Model('auctions')->getAuctionList(array('special_id'=>$_GET['special_id']),'','','',count($special_auction_list));
        foreach($special_auction_list as $k=>$v){
            if(in_array($special_info['special_state'],array(0,10,30))){
                $auction_info = Model('goods_auctions')->getGoodsAuctionInfo(array('id'=>$v['goods_auction_id']));
                $special_auction_list[$k]['current_price'] = $auction_info['auction_start_price'];
                $special_auction_list[$k]['bid_number'] = 0;
                $special_auction_list[$k]['real_participants'] = -1;
            }else{
                $special_auction_list[$k]['current_price'] = $auction_list[$k]['current_price'];
                $special_auction_list[$k]['bid_number'] = $auction_list[$k]['bid_number'];
                $special_auction_list[$k]['auction_id'] = $auction_list[$k]['auction_id'];
                $special_auction_list[$k]['real_participants'] = $auction_list[$k]['real_participants'];
            }
        }
//        print_R($special_auction_list);exit;
        Tpl::output('special_auction_list', $special_auction_list);
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
        Tpl::output('special_info', $special_info);
        Tpl::output('show_page', $model_auction_special->showpage());
        Tpl::showpage('auction_special.goods');
    }

    /**
     * 审核
     */
    public function special_verifyOp() {
        $model_auction_special = Model('auction_special');
        $this->special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_POST['special_id']));
        $param = array(10, 11, 12, 13, 20, 30);
        $special_state = intval($this->special_info['special_state']);
        if (in_array($special_state, $param)) {
            $this->param = array();
            $upload = new UploadFile();
            //上传专场缩略图
            if (!empty($_FILES['special_logo']['name'])){
                $upload->set('default_dir', ATTACH_VENDUE);
                $upload->set('thumb_ext',   '');
                $upload->set('file_name','');
                $upload->set('ifremove',false);
                $result = $upload->upfile('special_logo');
                if ($result){
                    $this->param['special_image'] = $upload->file_name;
                }else {
                    showDialog($upload->error);
                }
            }
            //上传专场广告图
            if (!empty($_FILES['adv_image']['name'])){
                $upload->set('default_dir', ATTACH_VENDUE);
                $upload->set('thumb_ext',   '');
                $upload->set('file_name','');
                $upload->set('ifremove',false);
                $result = $upload->upfile('adv_image');
                if ($result){
                    $this->param['adv_image'] = $upload->file_name;
                }else {
                    showDialog($upload->error);
                }
            }
            //上传wap广告图
            if (!empty($_FILES['wap_image']['name'])){
                $upload->set('default_dir', ATTACH_VENDUE);
                $upload->set('thumb_ext',   '');
                $upload->set('file_name','');
                $upload->set('ifremove',false);
                $result = $upload->upfile('wap_image');
                if ($result){
                    $this->param['wap_image'] = $upload->file_name;
                }else {
                    showDialog($upload->error);
                }
            }
            $this->param['special_name'] = $_POST['special_name'];
            if($special_state == 10){
                if(empty($_POST['special_preview_start']) || empty($_POST['special_start_time']) || empty($_POST['special_end_time'])){
                    showMessage('参数错误','');
                }

                $this->param['special_state'] = $_POST['verify_type'] === 'pass' ? 20 : 30;
                $this->param['special_check_message'] = $_POST['check_message'];
                $this->param['special_type'] = empty($_POST['auction_type']) ? 0 : 1;
                if($_POST['auction_rate_time']){
                    $this->param['special_rate_time'] = strtotime($_POST['auction_rate_time']);
                }
                $this->param['special_preview_start'] = strtotime($_POST['special_preview_start']);
                $this->param['special_start_time'] = strtotime($_POST['special_start_time']);
                $this->param['special_end_time'] = strtotime($_POST['special_end_time']);
                $this->param['special_rate'] = $_POST['auction_bond_rate'];
                if($_POST['auction_bond_rate'] > 0){
                    $this->param['partner_lv_1_wdl_rate'] = $_POST['partner_1_wdl'];
                    $this->param['partner_lv_1_zl_rate'] = $_POST['partner_1_zl'];
                    $this->param['partner_lv_1_sy_rate'] = $_POST['partner_1_sy'];
                    $this->param['partner_lv_2_wdl_rate'] = $_POST['partner_2_wdl'];
                    $this->param['partner_lv_2_zl_rate'] = $_POST['partner_2_zl'];
                    $this->param['partner_lv_2_sy_rate'] = $_POST['partner_2_sy'];
                }
                $this->special_verify_pass();
            }else{
                $this->special_verify_save();
            }
        }else{
            showMessage('参数错误','');
        }
    }

    /*
     * 专场修改操作
     * */
    private function special_verify_save() {
        try{
            $model_auction_special = Model('auction_special');
            $model_auction_special->beginTransaction();
            $result = $model_auction_special->editSpecial($this->param,array('special_id'=>$_POST['special_id']));
            if(!$result){
                throw new Exception('失败');
            }
            $model_auction_special->commit();
            $this->del_img();
            $this->log('修改专场信息[ID:'. $_POST['special_id'] .']',1);
                showMessage('保存成功','index.php?act=auction_special&op=all_special');
        }catch(Exception $e){
            $model_auction_special->rollback();
            showMessage('保存失败'.$e,'index.php?act=auction_special&op=all_special');
        }
    }

    /*
     * 专场审核操作
     * */
    private function special_verify_pass() {
        try{
            $model_auction_special = Model('auction_special');
            $model_auction_special->beginTransaction();
            $result = $model_auction_special->editSpecial($this->param,array('special_id'=>$_POST['special_id']));
            if(!$result){
                throw new Exception('失败');
            }
            if($this->param['special_state'] == 30){
                $auction_special_list = $model_auction_special->getSpecialGoodsLists(array('special_id'=>$_POST['special_id']));
                foreach($auction_special_list as $v){
                    $tmp[] = $v['goods_id'];
                }
                $re1 = Model('goods')->editGoods(array('goods_storage'=>array('exp','goods_storage+1')),array('goods_id'=>array('in',implode(',',$tmp))));
                goto END;
            }
            $condition = array(
                'special_id'=>$_POST['special_id']
            );
            $list = $model_auction_special->getSpecialGoodsList($condition,'goods_auction_id', '','','',''  );
            foreach($list as $v){
                $id_list[] = $v['goods_auction_id'];
            }
            $special_info = $model_auction_special->getSpecialInfo($condition);
            $param = array(
                'auction_start_time'=>$special_info['special_start_time'],
                'auction_end_time'=>$special_info['special_end_time'],
                'special_id'=>$_POST['special_id'],
                'auction_type'=>empty($_POST['auction_type'])?0:1,
                'auction_bond_rate'=>$_POST['auction_bond_rate'],
                'interest_last_date'=>$_POST['auction_rate_time'],
                'auction_preview_start'=>strtotime($_POST['special_preview_start'])
            );
            $re = $this->create_auction($id_list,$param);
            if(!$re){
                throw new Exception('失败');
            }
            END:
            $model_auction_special->commit();
            $this->del_img();
            $this->log('审核专场信息[ID:'. $_POST['special_id'] .']',1);
                showMessage('专场申请审核完成','index.php?act=auction_special&op=index');
        }catch(Exception $e){
            $model_auction_special->rollback();
            showMessage('专场申请审核失败'.$e,'index.php?act=auction_special&op=index');
        }
    }

    private function del_img(){
        //判断有没有之前的图片，如果有则删除
        if (!empty($this->special_info['special_image']) && !empty($this->param['special_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['special_image']);
        }
        if (!empty($this->special_info['adv_image']) && !empty($this->param['adv_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['adv_image']);
        }
        if (!empty($this->special_info['wap_image']) && !empty($this->param['wap_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['wap_image']);
        }
    }

    //平台专场列表
    public function platform_specialOp(){
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'platform_special'));

        Tpl::showpage('platform_special.index');
    }

    //输出XML
    public function platform_special_xmlOp(){
        $model_auction_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        $condition['special_sponsor'] = 0;
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_id','store_name','special_name','special_image','special_state','special_start_time','special_end_time','special_preview_start','special_add_time','is_open');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();
        foreach ($special_list as $value) {
            $param = array();
            $o = '';
            if ($value['is_open'] == 0 || $value['special_end_time'] < time()) {
                $o .= '<a class="btn red confirm-del-on-click" href="javascript:;" data-href="index.php?act=auction_special&op=del&special_id=' .
                    $value['special_id'] .
                    '"><i class="fa fa-trash-o"></i>删除</a>';
            }

            $o .= '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            $o .= '<li><a href="index.php?act=auction_special&op=edit&special_id=' .
                $value['special_id'] .
                '">编辑专场</a></li>';

            $o .= '<li><a href="index.php?act=auction_special&op=detail&id=' .
                $value['special_id'] .
                '">处理申请</a></li>';

            $o .= '</ul></span>';

            $param['operation'] = $o;
            $param['special_id'] = $value['special_id'];
            $param['special_name'] = $value['special_name'];
            $param['special_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getVendueLogo($value['special_image']).">\")'><i class='fa fa-picture-o'></i></a>";
            $param['special_start_time'] = date('Y-m-d H:i:s', $value['special_start_time']);
            $param['special_end_time'] = date('Y-m-d H:i:s', $value['special_end_time']);
            $param['special_preview_start'] = date('Y-m-d H:i:s', $value['special_preview_start']);
            $param['special_add_time'] = date('Y-m-d H:i:s', $value['special_add_time']);
            $param['is_open'] = $value['is_open'] == 1 ? '<span class="yes"><i class="fa fa-check-circle"></i>开启</span>' : '<span class="no"><i class="fa fa-ban"></i>关闭</span>';
            $data['list'][$value['special_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    //新增平台专场
    public function newOp(){
        //新建处理
        if($_POST['form_submit'] != 'ok'){
            Tpl::showpage('platform_special.add');
            exit;
        }
        //提交表单

        if (!empty($_FILES['special_image']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $result = $upload->upfile('special_image');
            if ($result){
                $_POST['special_image'] = $upload->file_name;
            }else {
                showMessage($upload->error);
            }
        }
        if (!empty($_FILES['adv_image']['name'])){
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('adv_image');
            if ($result){
                $_POST['adv_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        //保存
        $model_auction_special = Model('auction_special');
        $data = array();
        $data['special_name'] = $_POST['special_name'];
        $data['special_add_time'] = TIMESTAMP;
        $data['special_start_time'] = strtotime($_POST['special_start_time']);
        $data['special_end_time'] = strtotime($_POST['special_end_time']);
        $data['special_preview_start'] = strtotime($_POST['special_preview_start']);
        $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : '';
        $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : '';
        $data['special_sponsor'] = 0;
//        $data['delivery_mechanism'] = $_POST['delivery_mechanism'];
        $data['special_state'] = 20;
        $data['is_open'] = $_POST['is_open'];
        $return = $model_auction_special->addSpecial($data, true);
        if($return){
            showMessage('添加平台专场成功','index.php?act=auction_special&op=platform_special');
        }else{
            showMessage('添加平台专场失败');
        }
    }

    /**
     * 删除平台专场
     */
    public function delOp()
    {
        $specialIds = array();
        foreach (explode(',', (string) $_REQUEST['special_id']) as $i) {
            $specialIds[(int) $i] = null;
        }
        unset($specialIds[0]);
        $specialIds = array_keys($specialIds);

        if (empty($specialIds)) {
            $this->jsonOutput('请选择专场');
        }

        try{
            // 删除数据先删除广告图片以及专场缩略图，节省空间资源
            foreach ($specialIds as $v) {
                $this->delBanner($v);
            }
        } catch (Exception $e) {
            $this->jsonOutput($e->getMessage());
        }

        $id = implode(",", $specialIds);

        $model_auction_special = Model('auction_special');
        //获取可以删除的数据
        $condition =  " (is_open = 0 or special_end_time < '".(TIMESTAMP)."') and special_id in (". $id .") and special_sponsor = 0" ;
        $special_list = $model_auction_special->getSpecialList($condition);
        if (empty($special_list)){//没有符合条件的活动信息直接返回成功信息
            $this->jsonOutput();
        }
        $id_arr = array();
        foreach ($special_list as $v){
            $id_arr[] = $v['special_id'];
        }
        $id_new = implode(",", $id_arr);

        //只有关闭或者过期的活动，能删除
        if($model_auction_special->delSpecialGoods(array('special_id'=>array('in',$id_new)))){
            if($model_auction_special->delSpecialForAdmin(array('special_id'=>array('in',$id_new)))){
                $this->jsonOutput();
            }
        }

        $this->jsonOutput('操作失败');
    }

    /**
     * 编辑活动/保存编辑活动
     */
    public function editOp(){
        if($_POST['form_submit'] != 'ok'){
            if(empty($_GET['special_id'])){
                showMessage(Language::get('miss_argument'));
            }
            $model_auction_special = Model('auction_special');
            $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
            Tpl::output('auction_special_info',$auction_special_info);
            Tpl::showpage('platform_special.edit');
            exit;
        }

        if (!empty($_FILES['special_image']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $result = $upload->upfile('special_image');
            if ($result){
                $_POST['special_image'] = $upload->file_name;
            }else {
                showMessage($upload->error);
            }
        }
        if (!empty($_FILES['adv_image']['name'])){
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('adv_image');
            if ($result){
                $_POST['adv_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        //保存
        $model_auction_special = Model('auction_special');
        $data = array();
        $data['special_name'] = $_POST['special_name'];
        $data['special_add_time'] = TIMESTAMP;
        $data['special_start_time'] = strtotime($_POST['special_start_time']);
        $data['special_end_time'] = strtotime($_POST['special_end_time']);
        $data['special_preview_start'] = strtotime($_POST['special_preview_start']);
        $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));

        $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : $auction_special_info['special_image'];
        $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : $auction_special_info['adv_image'];

//        $data['delivery_mechanism'] = $_POST['delivery_mechanism'];
        $data['is_open'] = $_POST['is_open'];
        $return = $model_auction_special->editSpecial($data, array('special_id'=>intval($_POST['special_id'])));
        if($return){
            if (!empty($auction_special_info['special_image']) && !empty($_POST['special_image'])){
                @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$auction_special_info['special_image']);
            }
            if (!empty($auction_special_info['adv_image']) && !empty($_POST['adv_image'])){
                @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$auction_special_info['adv_image']);
            }

            showMessage('编辑平台专场成功','index.php?act=auction_special&op=platform_special');
        }else{
            showMessage('编辑平台专场失败');
        }
    }

    /**
     * 专场细节列表
     */
    public function detailOp()
    {
        $states = array(
            '待审核',
            '已通过',
            '已拒绝',
        );
        Tpl::output('states', $states);
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_REQUEST['id']));

        Tpl::output('special_info', $special_info);

        Tpl::showpage('special_detail.index');
    }

    /**
     * 专场细节列表XML
     */
    public function detail_xmlOp()
    {
        $condition = array();

        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_id','store_name','special_id','auction_id','auction_name','auction_image','auction_start_price','current_price','bid_number');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $condition['special_id'] = $_GET['id'];
        $page = $_POST['rp'];
        $model_auction_special = Model('auction_special');
        //专场拍品列表
        $list = $model_auction_special->getSpecialGoodsList($condition,'', $order,'','',$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();

        $states = array(
            '待审核',
            '已通过',
            '已拒绝',
        );
        foreach ($list as $val) {
            $o = '<a class="btn green" href="' .
                urlVendue('auctions', 'index', array('id' => $val['auction_id'])) .
                '"><i class="fa fa-list-alt"></i>查看</a>';

            $o .= '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            if ($val['auction_sp_state'] != 1) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="index.php?act=auction_special&op=deal&state=1&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">通过</a></li>';
            }

            if ($val['auction_sp_state'] != 2) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="index.php?act=auction_special&op=deal&state=2&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">拒绝</a></li>';
            }

            if ($val['auction_sp_state'] != 1) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="' .
                    'index.php?act=auction_special&op=del_detail&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">删除</a></li>';
            }

            $o .= '</ul></span>';

            $i = array();
            $i['operation'] = $o;

            $i['auction_name'] = $val['auction_name'];

            $i['store_name'] = '<a target="_blank" href="' .
                urlShop('show_store', 'index', array('store_id' => $val['store_id'])) .
                '">' .
                $val['store_name'] .
                '</a>';

            $i['auction_sp_state'] = $states[(int) $val['auction_sp_state']];

            $data['list'][$val['special_auction_id']] = $i;
        }

        echo Tpl::flexigridXML($data);
        exit;
    }

    /**
     * 专场内容处理
     */
    public function dealOp()
    {
        $ids = array();
        foreach (explode(',', (string) $_REQUEST['special_auction_id']) as $i) {
            $ids[(int) $i] = null;
        }
        unset($ids[0]);
        $ids = array_keys($ids);

        if (empty($ids)) {
            showMessage('请选择拍品');
        }

        // 获取id
        $id = implode(',', $ids);

        //创建活动内容对象
        $model_auction_special = Model('auction_special');
        if($model_auction_special->updatesGoods(array('auction_sp_state'=>intval($_GET['state'])),$id)){
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }

    }

    /**
     * 删除专场内容
     */
    public function del_detailOp()
    {
        $ids = array();
        foreach (explode(',', (string) $_REQUEST['special_auction_id']) as $i) {
            $ids[(int) $i] = null;
        }
        unset($ids[0]);
        $ids = array_keys($ids);

        if (empty($ids)) {
            showMessage('请选择拍品');
        }

        // 获取id
        $id = implode(',', $ids);

        $model_auction_special = Model('auction_special');
        //条件
        $condition_arr = array();
        $condition_arr['special_auction_id_in'] = $id;
        $condition_arr['auction_sp_state_in'] = "'0','2'";//未审核和已拒绝
        if($model_auction_special->delGoodsList($condition_arr)){
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    /**
     * 推荐
     */
    public function special_recOp()
    {
        $model_special= Model('auction_special');
        $update_array['is_rec'] = $_GET['rec'] == '1' ? 1 : 0;
        $where_array['special_id'] = $_GET['special_id'];
        $result = $model_special->editSpecial($update_array, $where_array);
//        echo '<pre>';print_r($result);exit;
        if ($result) {
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }


    /**
     * 根据专场编号删除广告图片以及专场缩略图
     *
     * @param int $id
     */
    private function delBanner($id){
        $model_auction_special = Model('auction_special');
        $row    = $model_auction_special->getSpecialInfo(array('special_id'=>$id));
        //删除图片文件
        @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$row['special_image']);
        @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$row['adv_image']);
    }

    public function get_special_infoOp(){
        $special_id = $_GET['id'];
        if (chksubmit()) {
            $auction_bond_rate = $_POST['auction_bond_rate'];
            $special_id = intval($_POST['special_id']);
            $rate_time = $_POST['auction_rate_time'];
            $model_auction_special = Model('auction_special');
            //专场拍品列表
            $condition = array(
                'special_id'=>$special_id,
            );
            $model_auction_special->editSpecial(array('special_rate'=>$auction_bond_rate,'special_rate_time'=>strtotime($rate_time)),array('special_id'=>$special_id));
            $list = $model_auction_special->getSpecialGoodsList($condition,'auction_id', '','','',''  );
            foreach($list as $v){
                $id_list[] = $v['auction_id'];
            }
//            print_R($list);exit;
            $model_auction = Model('auctions');
            $update = array(
                'auction_bond_rate'=>$auction_bond_rate,
                'interest_last_date'=>$rate_time
            );
            $re = $model_auction->editAuctionsById($update,$id_list);
//            print_R($_POST);
//            echo $re;exit;
            //TODO  将专场下所有商品保证金利率刷新
            showDialog(L('nc_common_op_succ'), '', 'succ', '$("#flexigrid").flexReload();CUR_DIALOG.close();');
        }
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
//        print_R($special_info);exit;
        Tpl::output('special_info', $special_info);
        Tpl::showpage('auctions_setting','null_layout');
    }

    /**
     * 拍品状态
     * @param $info
     * @return string
     */
    private function get_auction_state($info){
        if ($info['special_state'] == 11) {
            $re = '预展中';
        } elseif ($info['special_state'] == 12) {
            $re = '拍卖中';
        } elseif ($info['special_state'] == 13) {
            $re = '拍卖结束';
        } elseif ($info['special_state'] == 20) {
            $re = '等待预展';
        } else {
            $re = '----';
        }
        return $re;
    }

    /**
     * 审核通过后,创建真正的拍品
     * @param $id_list
     */
    private function create_auction($id_list,$param){
        if(!empty($id_list)){
            $tmp = array(
                'auction_state'=>2,
                'auction_sum'=>'`auction_sum` + 1'
            );
            Model('goods_auctions')->updateGoodsAuctions($tmp,array('cur_special_id'=>$param['special_id']));
            foreach($id_list as $v){
                $info = Model('goods_auctions')->getGoodsAuctionInfo(array('id'=>$v));
                if(empty($info)){
                    return false;
                }
                $info['delivery_mechanism'] = $info['store_name'];
                $info['state'] = 0;
                $info['auction_lock'] = 1;
                $info['auction_add_time'] = time();
                $info = array_merge($info,$param);
                unset($info['id']);
                unset($info['cur_special_id']);
                unset($info['last_special_id']);
                unset($info['auction_sum']);
                unset($info['auction_state']);
                unset($info['del_flag']);
                $id = Model('auctions')->addAuction($info);
//                dd();
                if(!$id){
                    return false;
                }
            }
        }
        return true;

    }
}
