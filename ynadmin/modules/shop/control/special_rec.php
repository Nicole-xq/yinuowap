<?php
/**
 * 书画中部专场管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class special_recControl extends SystemControl{

    private $_links = array(
        array('url'=>'act=special_rec&op=index','text'=>'专场列表'),
        array('url'=>'act=special_rec&op=rec_ok','text'=>'已推荐专场')
    );

    public function __construct(){
        parent::__construct();
        Language::read('store,store_grade');
    }

    public function indexOp() {
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'index'));

        Tpl::showpage('special_list.index');
    }



    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_auction_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        $condition['special_state'] = array('in',array(20,11,12,13));

        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_id','store_name','special_name','special_image','special_state','special_start_time','special_end_time','special_preview_start','special_add_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);
        $model_rec = Model('special_rec');
        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();
        foreach ($special_list as $value) {
            $param = array();
            $operation = '';
            $operation .= "<a class='btn green' href='index.php?act=auction_special&op=special_check&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>查看</a>";
            $rec_info = $model_rec->getSpecialRecInfo(array('special_id'=>$value['special_id']));
            if(empty($rec_info)){
                $operation .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>";
                $operation .= '<li><a href="index.php?act=special_rec&op=special_rec&special_id='.$value['special_id'].'">书画中部推荐专场</a></li>';
                $operation .= "</ul></span>";
            }
            $param['operation'] = $operation;
            $param['store_id'] = $value['store_id'];
            $param['store_name'] = $value['store_name'];
            $param['special_name'] = $value['special_name'];
            $param['special_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getVendueLogo($value['special_image']).">\")'><i class='fa fa-picture-o'></i></a>";

            $param['special_start_time'] = date('Y-m-d H:i:s', $value['special_start_time']);
            $param['special_end_time'] = date('Y-m-d H:i:s', $value['special_end_time']);
            $param['special_preview_start'] = date('Y-m-d H:i:s', $value['special_preview_start']);
            $param['special_add_time'] = date('Y-m-d H:i:s', $value['special_add_time']);
            $data['list'][$value['special_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function special_recOp(){
        $special_id = $_GET['special_id'];
        $model_rec = Model('special_rec');
        $special_rec = $model_rec->getSpecialRecInfo(array('special_id'=>$special_id));
        $rec_list = explode(',',$special_rec['rec_auction']);
        $auction_list = array();
        if ($special_id > 0) {
            if (!empty($rec_list)) {
                $auction_list = Model('auctions')->getAuctionList(array('auction_id'=>array('in',$rec_list)),'auction_name,auction_id,auction_image');
                if (!empty($auction_list)) {
                    foreach ($auction_list as $k => $v) {
                        $auction_list[$k]['auction_image'] = cthumb($v['auction_image'],240,$v['store_id']);
                    }
                }
            }
        }

        Tpl::output('goods_list_json',json_encode($auction_list));
        Tpl::output('goods_list', $auction_list);
        Tpl::output('rec_info', is_array($special_rec) ? $special_rec : array());
        Tpl::showpage('special_rec.add');
    }

    public function rec_okOp(){
        Tpl::output('top_link',$this->sublink($this->_links,'rec_ok'));
        Tpl::showpage('special_rec.index');
    }

    public function get_rec_xmlOp(){
        $model_rec = Model('special_rec');
        $model_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_id','store_name','special_name','special_image','special_state','special_start_time','special_end_time','special_preview_start','special_add_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $rec_special_list = $model_rec->getSpecialRecLists($condition,'', $order,'',$page);

        $data = array();
        $data['now_page'] = $model_rec->shownowpage();
        $data['total_num'] = $model_rec->gettotalnum();
        foreach ($rec_special_list as $value) {
            $param = array();
            $operation = '';
            $operation .= "<a class='btn green' href='index.php?act=special_rec&op=special_rec&special_id=".$value['special_id']."'><i class='fa fa-list-alt'></i>编辑</a>";
            $operation .= '<a class="btn red confirm-del-on-click" href="javascript:;" data-href="index.php?act=special_rec&op=delete&special_id=' .
                $value['special_id'] .
                '"><i class="fa fa-trash-o"></i>删除</a>';

            $param['operation'] = $operation;
            $param['special_name'] = $value['special_name'];
            $param['special_start_time'] = date('m-d', $value['special_start_time']);
            $param['special_end_time'] = date('m-d', $value['special_end_time']);
            $auction_count = $model_special->getSpecialGoodsCount(array('special_id'=>$value['special_id'],'auction_sp_state'=>1));
            $special_goods_list = $model_special->getSpecialGoodsLists(array('special_id'=>$value['special_id'],'auction_sp_state'=>1));
            $param['auction_count'] = $auction_count;
            $person_num = 0;
            foreach($special_goods_list as $k=>$v){
                $person_num += $v['num_of_applicants'];
            }

            $param['person_num'] = $person_num;
            $param['special_rec_pic'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".co_storeImage($value['special_rec_pic']).">\")'><i class='fa fa-picture-o'></i></a>";
            $param['rec_sort'] = $value['rec_sort'];
            $data['list'][$value['special_rec_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 保存
     */
    public function saveOp(){
        $special_id = $_POST['special_id'];

        if (!chksubmit()) {
            showMessage('非法提交');
        }
        $model_rec = Model('special_rec');

        $rec_info = $model_rec->getSpecialRecInfo(array('special_id'=>$special_id));
        $data = array();
        if (!empty($_FILES['special_pic']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_PAINTING);
            $result = $upload->upfile('special_pic');
            if ($result) {
                $_POST['special_pic'] = $upload->file_name;
            } else {
                showMessage($upload->error, '', '', 'error');
            }
        }
        if(!empty($rec_info) && $rec_info['special_rec_pic'] != ''){
            $data['special_rec_pic'] = !empty($_POST['special_pic'])?trim($_POST['special_pic']):$rec_info['special_rec_pic'];
        }else{
            $data['special_rec_pic'] = !empty($_POST['special_pic'])?trim($_POST['special_pic']):'';
        }

        if (is_array($_POST['goods_id_list'])) {
            $data['rec_auction'] = implode(',',$_POST['goods_id_list']);
        }
        $data['rec_sort'] = trim($_POST['rec_sort']);
        $data['special_id'] = $special_id;
        if(!empty($rec_info)){
            $insert = $model_rec->editSpecialRec($data,array('special_id'=>$special_id));
        }else{
            $insert = $model_rec->addSpecialRec($data);
        }

        if ($insert) {
            showMessage('保存成功','index.php?act=special_rec&op=rec_ok');
        }
    }

    /**
     * 删除
     */
    public function deleteOp() {
        $model_rec = Model('special_rec');
        $ids = array();
        foreach (explode(',', (string) $_REQUEST['special_id']) as $i) {
            $ids[(int) $i] = null;
        }
        unset($ids[0]);
        $ids = array_keys($ids);

        if (empty($ids)) {
            showMessage('请选择专场');
        }

        // 获取id
        $id = implode(',', $ids);

        if($model_rec->delSpecialRec($id)){
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    public function get_auction_listOp(){
        $model_special = Model('auction_special');
        $condition = array();
        if (!empty($_GET['goods_name'])) {
            $condition['auction_name'] = array('like',"%{$_GET['goods_name']}%");
        }
        $condition['special_id'] = $_GET['special_id'];

        $goods_list = $model_special->getSpecialGoodsLists($condition,'*');
        $html = "<ul class=\"dialog-goodslist-s2\">";
        foreach($goods_list as $v) {
            $url = urlVendue('auctions', 'index', array('id' => $v['auction_id']));
            $img = cthumb($v['auction_image'],240,$v['store_id']);
            $html .= <<<EOB
            <li>
            <div class="goods-pic" onclick="select_recommend_goods({$v['auction_id']});">
            <span class="ac-ico"></span>
            <span class="thumb size-72x72">
            <i></i>
            <img width="72" src="{$img}" goods_name="{$v['auction_name']}" goods_id="{$v['auction_id']}" title="{$v['auction_name']}">
            </span>
            </div>
            <div class="goods-name">
            <a target="_blank" href="{$url}">{$v['auction_name']}</a>
            </div>
            </li>
EOB;
        }
        $admin_tpl_url = ADMIN_TEMPLATES_URL;
        $html .= '<div class="clear"></div></ul><div id="pagination" class="pagination">'.$model_special->showpage(1).'</div><div class="clear"></div>';
        $html .= <<<EOB
        <script>
        $('#pagination').find('.demo').ajaxContent({
                event:'click',
                loaderType:"img",
                loadingMsg:"{$admin_tpl_url}/images/transparent.gif",
                target:'#show_recommend_goods_list'
            });
        </script>
EOB;
        echo $html;
    }





}
