<?php
/**
 * 拍品管理
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/3 0003
 * Time: 11:16
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctionsControl extends SystemControl
{
    private $links = array(
        array('url'=>'act=auctions&op=index','text'=>'所有拍品'),
        array('url'=>'act=auctions&op=new','text'=>'新增拍品'),
//        array('url'=>'act=auctions&op=novice_list','text'=>'新手拍品'),
//        array('url'=>'act=auctions&op=normal_list','text'=>'普通拍品'),
//        array('url'=>'act=auctions&op=waitverify_list','text'=>'等待审核'),
//        array('url'=>'act=auctions&op=lockup_list','text'=>'未通过审核'),
//        array('url'=>'act=auctions&op=auctions_set','text'=>'拍品设置'),
    );

    const EXPORT_SIZE = 2000;

    public function __construct() {
        parent::__construct ();
        Tpl::output('top_link',$this->sublink($this->links,$_GET['op']));
    }

    public function indexOp() {
        $this->auctionsOp();
    }

    /**
     * 拍品管理
     * */
    public function auctionsOp()
    {
        //父类列表，只取到第二级
        $gc_list = Model('goods_class')->getGoodsClassList(array('gc_parent_id' => 0));
        Tpl::output('gc_list', $gc_list);

        Tpl::showpage('auctions.index');
    }

    /**
     * 新手拍品管理
     */
    public function novice_listOp() {
        Tpl::output('type', 'novice');
        Tpl::showpage('auctions.index');
    }

    /**
     * 普通拍品管理
     */
    public function normal_listOp() {
        Tpl::output('type', 'normal');
        Tpl::showpage('auctions.index');
    }

//    /**
//     * 违规下架商品管理
//     */
//    public function lockup_listOp() {
//        Tpl::output('type', 'lockup');
//        Tpl::showpage('auctions.index');
//    }

//    /**
//     * 等待审核商品管理
//     */
//    public function waitverify_listOp() {
//        Tpl::output('type', 'waitverify');
//        Tpl::showpage('auctions.index');
//    }

    /**
     * 审核商品
     */
    public function auctions_verifyOp(){
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfoByID($_GET['id']);
        if (chksubmit()) {
            $auction_id = intval($_POST['auction_id']);
            $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
            if ($auction_id <= 0) {
                showDialog(L('nc_common_op_fail'), 'reload');
            }
            $update = array();
            $update['state'] = intval($_POST['verify_state']);
           /* if ($update['state'] == 1) {
                $update['auction_lock'] = 1;
            }*/
            $update['auction_verifyremark'] = trim($_POST['verify_reason']);
            //$update['auction_bond_rate'] = floatval($_POST['auction_bond_rate']);
            //$update['interest_last_date'] = $_POST['interest_last_time'];
            $where = array();
            $where['auction_id'] = $auction_id;

            $model_auctions = Model('auctions');
            if (intval($_POST['verify_state']) == 2) {
                $model_auctions->editAuctionsVerifyFail($update, $where, $auction_info['store_id']);
            } else {
                $model_auctions->editAuctions($update, $where);
            }
            showDialog(L('nc_common_op_succ'), '', 'succ', '$("#flexigrid").flexReload();CUR_DIALOG.close();');
        }
        Tpl::output('auctions_info', $auction_info);
        Tpl::showpage('auctions.verify_remark', 'null_layout');
    }

    /**
     * 违规下架
     */
    public function auction_lockupOp() {
        $auction_info = Model('auctions')->getAuctionsInfoByID($_GET['id']);
        if (chksubmit()) {
            $auction_id = intval($_POST['auction_id']);
            $auction_info = Model('auctions')->getAuctionsInfoByID($auction_id);
            if ($auction_id <= 0) {
                showDialog(L('nc_common_op_fail'), 'reload');
            }
            $update = array();
            $update['auction_verifyremark'] = trim($_POST['close_reason']);
            $update['state'] = 2;
            $update['auction_lock'] = 1;

            $where = array();
            $where['auction_id'] = $auction_id;

            Model('auctions')->editAuctionsVerifyFail($update, $where, $auction_info['store_id']);
            showDialog(L('nc_common_op_succ'), '', 'succ', '$("#flexigrid").flexReload();CUR_DIALOG.close()');
        }
        Tpl::output('auction_info', $auction_info);
        Tpl::showpage('auctions.close_remark', 'null_layout');
    }

    /**
     * 删除拍品
     * */
    public function auction_delOp()
    {
        $auction_id = intval($_GET['id']);
        if ($auction_id <= 0) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        Model('auctions')->delAuctions(array('auction_id' => $auction_id));
        $this->log('删除拍品[ID:'.$auction_id.']',1);
        exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
    }
    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $condition = array();
        if ($_GET['auction_name'] != '') {
            $condition['auction_name'] = array('like', '%' . $_GET['auction_name'] . '%');
        }
        if ($_GET['auction_id'] != '') {
            $condition['auction_id'] = array('like', '%' . $_GET['auction_id'] . '%');
        }
        if ($_GET['store_name'] != '') {
            $condition['store_name'] = array('like', '%' . $_GET['store_name'] . '%');
        }
        if ($_GET['auction_state'] != '') {
            $condition['is_liupai'] = $_GET['auction_state'];
        }
        if ($_GET['state'] != '') {
            switch($_GET['state']){
                case 1://等待预展
                    $condition['auction_preview_start'] = array('gt',time());
                    break;
                case 2://预展中
                    $condition['auction_preview_start'] = array('elt',time());
                    $condition['auction_start_time'] = array('gt',time());
                    break;
                case 3://拍卖中
                    $condition['auction_start_time'] = array('elt',time());
                    $condition['auction_end_time'] = array('gt',time());
                    break;
                case 4://拍卖结束
                    $condition['auction_end_time'] = array('elt',time());
                    break;
                default:
                    break;
            }
        }
        if ($_GET['pay_state'] != '') {

            $condition['state'] = $_GET['state'];
        }
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('auction_id', 'auction_name', 'auction_preview_start', 'auction_start_time', 'auction_end_time', 'auction_end_true_t', 'state', 'auction_image', 'gc_id'
        , 'gc_name', 'store_id', 'store_name', 'is_own_shop', 'auction_add_time', 'auction_bond', 'auction_start_price'
        , 'auction_increase_range', 'auction_reserve_price', 'auction_number', 'delivery_mechanism', 'number_of_applicants'
        , 'set_reminders_number', 'click_volume'
        );
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];

        switch ($_GET['type']) {
//            // 禁售
//            case 'lockup':
//                $auctions_list = $model_auctions->getAuctionsLockUpList($condition, '*', $page, $order);
//                break;
//            // 等待审核
//            case 'waitverify':
//                $auctions_list = $model_auctions->getAuctionsWaitVerifyList($condition, '*', $page, $order);
//                break;
            // 新手拍品
            case 'novice':
                $condition['auction_type'] = '1';
                $auctions_list = $model_auctions->getAuctionSpecialList($condition, '*', '', $order, 0 ,$page);
                break;
            // 普通拍品
            case 'normal':
                $condition['auction_type'] = '0';
                $auctions_list = $model_auctions->getAuctionSpecialList($condition, '*', '', $order, 0 ,$page);
                break;
            // 全部拍品
            default:
                $auctions_list = $model_auctions->getAuctionSpecialList($condition, '*', '', $order, 0 ,$page);
                break;
        }
//dd();
        // 拍品状态
//        $auctions_state = $this->getAuctionsState();


        $data = array();
        $data['now_page'] = $model_auctions->shownowpage();
        $data['total_num'] = $model_auctions->gettotalnum();
        foreach ($auctions_list as $value) {
            $param = array();
            $operation = '';
//            $operation .= "<a class='btn red' href='javascript:void(0);' onclick=\"fg_lonkup('" . $value['auction_id'] . "')\"><i class='fa fa-ban'></i>结束拍卖</a>";
            if ($value['recommended'] == '1') {
                $operation .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions', 'auctions_rec', array(
                            'auction_id' => $value['auction_id'],
                            'rec' => 0,
                        )) . '">取消推荐</a>';
            } else {
                $operation .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions', 'auctions_rec', array(
                            'auction_id' => $value['auction_id'],
                            'rec' => 1,
                        )) . '">推荐拍品</a>';
            }
            $operation .= "<span class='btn'><em><i class='fa'></i>更多 <i class='arrow'></i></em><ul>";
            $operation .= "<li><a href='" . urlAuction('auctions', 'index', array('id' => $value['auction_id'])) . "' target=\"_blank\">查看拍品详细</a></li>";
            $operation .= "<li><a href='" . urlAdminShop('auctions', 'auctions_edit', array('auction_id' => $value['auction_id'])) . "'>编辑拍品信息</a></li>";
            $operation .= "</ul>";
            $param['operation'] = $operation;
            $param['auction_id'] = $value['auction_id'];
//            $param['goods_id'] = $value['goods_id'];
            $param['auction_name'] = $value['auction_name'];
            $param['auction_bond'] = ncPriceFormat($value['auction_bond']);
            if($value['auction_bond_rate']>0){
                $param['auction_bond'] .= '(利息'.ncPriceFormat($value['auction_bond_rate']).'%)';
            }
            $param['auction_start_price'] = ncPriceFormat($value['auction_start_price']);
            $param['auction_increase_range'] = ncPriceFormat($value['auction_increase_range']);
            $param['auction_reserve_price'] = ncPriceFormat($value['auction_reserve_price']);
            $param['auction_preview_start'] = date('Y-m-d H:i', $value['auction_preview_start']);
            $param['special_rate_time'] = date('Y-m-d H:i', $value['special_rate_time']);
            $param['auction_start_time'] = date('Y-m-d H:i', $value['auction_start_time']);
            $param['auction_end_time'] = date('Y-m-d H:i', $value['auction_end_time']);
            $param['store_id'] = $value['store_id'] > 0 ? $value['store_id'] : "无";
            $param['store_name'] = $value['store_name'];
            $param['auction_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".cthumb($value['auction_image'],'60').">\")'><i class='fa fa-picture-o'></i></a>";
            $param['state'] = $this->get_auction_state($value);
            $param['auction_state'] = $value['is_liupai'] == 0?'成交':'流拍';
            $param['bid_number'] = $value['bid_number'];
            $param['order_amount'] = '';
            $param['buyer_name'] = '';
            $param['pay_state'] = '';
            $order_info = Model('order')->getOrderInfo(array('auction_id'=>$value['auction_id']));
            if(!empty($order_info)){
                $param['order_amount'] = $order_info['order_amount'];
                $param['buyer_name'] = $order_info['buyer_name'];
                $param['pay_state'] = $order_info['payment_time']?'已付款':'未付款';
            }
//            $param['state'] = $auctions_state[$value['state']];
//            if(empty($value['auction_video'])){
//                $param['auction_video'] = '';
//            }else{
//                if(file_exists(BASE_UPLOAD_PATH . '/' . ATTACH_GOODS . '/' . $value['store_id'] .'/' . 'goods_video' . '/' . $value['auction_video'])){
//                    $param['auction_video'] = "<a href='javascript:void(0);' onclick='fg_see_video(".$value['auction_id'].")'><i class='fa fa-file-video-o'></i></a>";
//                }else{
//                    $param['auction_video'] = '';
//                }
//            }
//            $param['gc_id'] = $value['gc_id'];
//            $param['gc_name'] = $value['gc_name'];
//            $param['is_own_shop'] = $value['is_own_shop'] == 1 ? '平台自营' : '入驻商户';
//            $param['auction_add_time'] = date('Y-m-d', $value['auction_add_time']);
//            $param['auction_end_true_t'] = $value['auction_end_true_t'] ? date('Y-m-d H:i', $value['auction_end_true_t']) : '未结束';
//            $param['recommended'] = $value['recommended'] == '1'
//                ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>'
//                : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $data['list'][$value['auction_id']] = $param;
            $data['recommended'] = $value['recommended'];
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /*
     * 修改拍品信息
     * */
    public function auctions_editOp(){
        $model_auctions = Model('auctions');
        if(!empty($_POST['verify_type']) && $_POST['verify_type'] == 'save'){
            $auction_id = $_POST['auction_id'];
            $auctions = Model('auctions');
            $auctions_info = $auctions->getAuctionsInfo(array('auction_id'=>$auction_id));

            $update_array = array();
            $update_array['auction_name'] = $_POST['auction_name'];
            $update_array['auction_start_price'] = $_POST['auction_start_price'];
            $update_array['auction_increase_range'] = $_POST['auction_increase_range'];
            $update_array['auction_reserve_price'] = $_POST['auction_reserve_price'];
            if($_POST['num_of_applicants']){
                $update_array['num_of_applicants'] = $_POST['num_of_applicants'];
            }
            if($_POST['set_reminders_num']){
                $update_array['set_reminders_num'] = $_POST['set_reminders_num'];
            }
            if($_POST['auction_click']){
                $update_array['auction_click'] = $_POST['auction_click'];
            }
            //上传专场图片
            if (!empty($_FILES['auctions_img']['name'])){
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_VENDUE);
                $upload->set('thumb_ext',   '');
                $upload->set('file_name','');
                $upload->set('ifremove',false);
                $result = $upload->upfile('auctions_img');
                if ($result){
                    $update_array['auction_image'] = $upload->file_name;
                }else {
                    showDialog($upload->error);
                }
            }
            try{
                $result = $auctions->editAuctions($update_array, array('auction_id'=>$auction_id)) ;
                if(!$result){
                    throw new Exception('失败');
                }
                //判断有没有之前的图片，如果有则删除
                if ($update_array['auction_image']) {
                    if (!empty($auctions_info['auction_image']) && !empty($_POST['auction_image'])) {
                        @unlink(BASE_UPLOAD_PATH . DS . ATTACH_COMMON . DS . $auctions_info['auction_image']);
                    }
                }
                $this->log('编辑拍品管理拍品信息[ID:'. $auction_id .']成功');
                showMessage(L('nc_common_save_succ'), 'index.php?act=auctions');
            }catch(Exception $e){
                $auctions->rollback();
                $this->log('编辑拍品管理拍品信息[ID:'. $auction_id .']失败');
                showMessage(L('nc_common_save_fail'), 'index.php?act=auctions');
            }
        }
        $auctions_info = $model_auctions->getAuctionsInfo(array('auction_id'=>$_GET['auction_id']));
        Tpl::output('auctions_info', $auctions_info);

        Tpl::showpage('auctions.detail');
    }


    /**
     * 商品状态
     * @return array
     */
    private function getAuctionsState() {
        return array(
            '0' => '待审核',
            '1' => '审核通过',
            '2' => '审核失败',
            '3' => '拍卖预展',
            '4' => '拍卖开始',
            '5' => '拍卖结束'
        );
    }

    /**
     * csv导出
     */
    public function export_csvOp() {
        $model_auctions = Model('auctions');
        $condition = array();
        $limit = false;
        if ($_GET['id'] != '') {
            $id_array = explode(',', $_GET['id']);
            $condition['auction_id'] = array('in', $id_array);
        }
        if ($_GET['auction_name'] != '') {
            $condition['auction_name'] = array('like', '%' . $_GET['auction_name'] . '%');
        }
        if ($_GET['auction_id'] != '') {
            $condition['auction_id'] = array('like', '%' . $_GET['auction_id'] . '%');
        }
        if ($_GET['store_name'] != '') {
            $condition['store_name'] = array('like', '%' . $_GET['store_name'] . '%');
        }
        if (intval($_GET['cate_id']) > 0) {
            $condition['gc_id'] = intval($_GET['cate_id']);
        }
        if ($_GET['state'] != '') {
            $condition['state'] = $_GET['state'];
        }
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('auction_id', 'auction_name', 'auction_preview_start', 'auction_start_time', 'auction_end_time', 'auction_end_true_t', 'state', 'auction_image', 'gc_id'
        , 'gc_name', 'store_id', 'store_name', 'is_own_shop', 'auction_add_time', 'auction_bond', 'auction_start_price'
        , 'auction_increase_range', 'auction_reserve_price', 'auction_number', 'delivery_mechanism', 'number_of_applicants'
        , 'set_reminders_number', 'click_volume'
        );
        if (in_array($_REQUEST['sortname'], $param) && in_array($_REQUEST['sortorder'], array('asc', 'desc'))) {
            $order = $_REQUEST['sortname'] . ' ' . $_REQUEST['sortorder'];
        }
        if (!is_numeric($_GET['curpage'])){
            switch ($_GET['type']) {
                // 未通过审核
                case 'lockup':
                    $count = $model_auctions->getAuctionsLockUpCount($condition);
                    break;
                // 等待审核
                case 'waitverify':
                    $count = $model_auctions->getAuctionsWaitVerifyCount($condition);
                    break;
                // 全部商品
                default:
                    $count = $model_auctions->getAuctionsCount($condition);
                    break;
            }
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $array = array();
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=auctions&op=index');
                Tpl::showpage('export.excel');
                exit();
            }
        } else {
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $limit = $limit1 .','. $limit2;
        }
        switch ($_GET['type']) {
            // 禁售
            case 'lockup':
                $auctions_list = $model_auctions->getAuctionsLockUpList($condition, '*', null, $order, $limit);
                break;
            // 等待审核
            case 'waitverify':
                $auctions_list = $model_auctions->getAuctionsWaitVerifyList($condition, '*', null, $order, $limit);
                break;
            // 全部商品
            default:
                $auctions_list = $model_auctions->getAuctionList($condition, '*', null, $order, $limit);
                break;
        }
        $this->createCsv($auctions_list);
    }

    /**
     * 生成csv文件
     */
    private function createCsv($auctions_list) {

        // 商品状态
        $auctions_state = $this->getAuctionsState();

        $data = array();
        foreach ($auctions_list as $value) {
            $param = array();
            $param['auction_id'] = $value['auction_id'];
            $param['auction_name'] = $value['auction_name'];
            $param['auction_bond'] = ncPriceFormat($value['auction_bond']);
            $param['auction_start_price'] = ncPriceFormat($value['auction_start_price']);
            $param['auction_increase_range'] = ncPriceFormat($value['auction_increase_range']);
            $param['auction_reserve_price'] = ncPriceFormat($value['auction_reserve_price']);
            $param['state'] = $auctions_state[$value['state']];
            $param['auction_image'] = cthumb($value['auction_image']);
            $param['gc_id'] = $value['gc_id'];
            $param['gc_name'] = $value['gc_name'];
            $param['store_id'] = $value['store_id'];
            $param['store_name'] = $value['store_name'];
            $param['is_own_shop'] = $value['is_own_shop'] == 1 ? '平台自营' : '入驻商户';
            $param['auction_add_time'] = date('Y-m-d', $value['auction_add_time']);
            $param['auction_preview_start'] = date('Y-m-d H:i', $value['auction_preview_start']);
            $param['auction_start_time'] = date('Y-m-d H:i', $value['auction_start_time']);
            $param['auction_end_time'] = date('Y-m-d H:i', $value['auction_end_time']);
            $param['auction_end_true_t'] = $value['auction_end_true_t'] ? date('Y-m-d H:i', $value['auction_end_true_t']) : '未结束';
            $data[$value['auction_id']] = $param;
        }

        $header = array(
            'auction_id' => 'ID',
            'auction_name' => '拍品名称',
            'auction_bond' => '拍品保证金(元)',
            'auction_start_price' => '起拍价(元)',
            'auction_increase_range' => '加价幅度(元)',
            'auction_reserve_price' => '保留价(元)',
            'state' => '拍品状态',
            'auction_image' => '拍品主图',
            'gc_id' => '分类ID',
            'gc_name' => '分类名称',
            'store_id' => '店铺ID',
            'store_name' => '店铺名称',
            'is_own_shop' => '是否是自营店',
            'auction_add_time' => '拍品添加时间',
            'auction_preview_start' => '预展开始时间',
            'auction_start_time' => '开始拍卖时间',
            'auction_end_time' => '结束拍卖时间',
            'auction_end_true_t' => '拍品真实结束时间',
        );
        \Shopnc\Lib::exporter()->output('auctions_list' .$_GET['curpage'] . '-'.date('Y-m-d'), $data, $header);
    }

     /**
     * 推荐
     */
    public function auctions_recOp()
    {
        $model= Model('auctions');
        $update_array['recommended'] = $_GET['rec'] == '1' ? 1 : 0;
        $where_array['auction_id'] = $_GET['auction_id'];
        $result = $model->editAuctions($update_array, $where_array);

        if ($result) {
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    /**
     * 预览视频
     */
    public function see_videoOp(){
        if(!$_GET['id']){
            showMessage('参数非法');
        }
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfoByID($_GET['id'],'auction_video,store_id');
        Tpl::output('auction_info' , $auction_info );
        Tpl::showpage('auctions_see_video' , 'null_layout');
    }

    /**
     * ajax获取商品列表
     */
    public function get_auctions_sku_listOp() {
        $commonid = $_GET['commonid'];
        if ($commonid <= 0) {
            showDialog('参数错误', '', '', 'CUR_DIALOG.close();');
        }
        $model_auctions = Model('auctions');
        $auctions_list = $model_auctions->getAuctionList(array('auction_id'=>$commonid), 'auction_id,auction_name,auction_image,auction_reserve_price,commis_level_1,commis_level_2');
        if (empty($auctions_list)) {
            showDialog('参数错误', '', '', 'CUR_DIALOG.close();');
        }

        foreach ($auctions_list as $key => $val) {
            $auctions_list[$key]['auction_image'] = cthumb($val['auction_image'],'60');
            $auctions_list[$key]['url'] = urlAuction('auctions', 'index', array('id' => $val['auction_id']));
        }

        Tpl::output('auctions_list', $auctions_list);
        Tpl::showpage('auction.sku_list', 'null_layout');
    }

    /**
     * 设置商品返佣比例
     * */
    public function auctions_precent_setOp(){
        $auction_id = intval($_GET['auction_id']);
        $percent = floatval($_GET['per']);
        $column = trim($_GET['column']);
        Model()->table('auctions')->where(array('auction_id' => $auction_id))->update(array($column => $percent));
    }

    /**
     * 拍品状态
     * @param $info
     * @return string
     */
    private function get_auction_state($info){
        if(time() < $info['auction_preview_start']){
            $re = '等待预展';
        }elseif(time() < $info['auction_start_time']){
            $re = '预展中';
        }elseif(time() < $info['auction_end_time']){
            $re = '拍卖中';
        }else{
            $re = '拍卖结束';
        }
        return $re;
    }

//    /**
//     * 拍品设置
//     */
//    public function auctions_setOp() {
//        $model_setting = Model('setting');
//        if (chksubmit()){
//            $update_array = array();
//            $update_array['auction_commis_1'] = $_POST['auction_commis_1'];
//            $update_array['auction_commis_2'] = $_POST['auction_commis_2'];
//            $result = $model_setting->updateSetting($update_array);
//            if ($result === true){
//                $this->log('编辑拍品设置',1);
//                showMessage('编辑成功');
//            }else {
//                $this->log('编辑拍品设置',0);
//                showMessage('编辑失败');
//            }
//        }
//        $list_setting = $model_setting->getListSetting();
//        Tpl::output('list_setting',$list_setting);
//
//        Tpl::output('top_link',$this->sublink($this->links,'auctions_set'));
//        Tpl::showpage('auctions.setting');
//    }
}