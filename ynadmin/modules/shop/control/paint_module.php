<?php
/**
 * 书画-模块名称
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class paint_moduleControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('paint_module.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_paint_module = Model('paint_module');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('paint_module_id', 'paint_module_name', 'paint_module_show', 'paint_module_sort');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        // 模块名称列表
        $paint_module_list = $model_paint_module->getpaint_moduleList($condition, '*', $page, $order);


        $data = array();
        $data['now_page'] = $model_paint_module->shownowpage();
        $data['total_num'] = $model_paint_module->gettotalnum();
        foreach ($paint_module_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick=\"fg_del(".$value['paint_module_id'].")\"><i class='fa fa-trash-o'></i>删除</a>";
            $operation .= "<a class='btn blue' href='index.php?act=paint_module&op=module_edit&paint_module_id=" . $value['paint_module_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] = $operation;
            $param['paint_module_id'] = $value['paint_module_id'];
            $param['paint_module_name'] = $value['paint_module_name'];
            $param['paint_module_show'] = $value['paint_module_show'] == 0 ? '<span class="no"><i class="fa fa-ban"></i>否</span>' : '<span class="yes"><i class="fa fa-check-circle"></i>是</span>';
            $param['paint_module_sort'] = $value['paint_module_sort'];
            $data['list'][$value['paint_module_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }


    /**
     * 增加模块名称
     */
    public function module_addOp(){
        $model_paint_module = Model('paint_module');
        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["paint_module_name"], "require"=>"true", "message"=>'模块名称不能为空'),
                array("input"=>$_POST["paint_module_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序仅可以为数字'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['paint_module_name'] = trim($_POST['paint_module_name']);
                $insert_array['paint_module_sort'] = intval($_POST['paint_module_sort']);
                $insert_array['paint_module_show'] = trim($_POST['paint_module_show']);

                $result = $model_paint_module->addpaint_module($insert_array);
                if ($result){
                    $url = array(
                        array(
                            'url'=>'index.php?act=paint_module&op=module_add',
                            'msg'=>'继续新增模块名称',
                        ),
                        array(
                            'url'=>'index.php?act=paint_module&op=index',
                            'msg'=>'返回模块名称列表',
                        )
                    );
                    showMessage('新增模块名称成功',$url);
                }else {
                    showMessage('新增模块名称失败');
                }
            }
        }


        Tpl::showpage('paint_module.add');
    }

    /**
     * 模块名称编辑
     */
    public function module_editOp(){
        $model_paint_module = Model('paint_module');

        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["paint_module_name"], "require"=>"true", "message"=>'模块名称不能为空'),
                array("input"=>$_POST["paint_module_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序仅可以为数字'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $where = array();
                $where['paint_module_id'] = intval($_POST['paint_module_id']);
                $update_array = array();
                $update_array['paint_module_name'] = trim($_POST['paint_module_name']);
                $update_array['paint_module_sort'] = intval($_POST['paint_module_sort']);
                $update_array['paint_module_show'] = trim($_POST['paint_module_show']);
                $result = $model_paint_module->editpaint_module($update_array,$where);
                if ($result){
                    $url = array(
                        array(
                            'url'=>'index.php?act=paint_module&op=module_edit&paint_module_id='.intval($_POST['paint_module_id']),
                            'msg'=>'继续编辑模块名称',
                        ),
                        array(
                            'url'=>'index.php?act=paint_module&op=index',
                            'msg'=>'返回模块名称列表',
                        )
                    );
                    showMessage('编辑模块名称成功',$url);
                }else {
                    showMessage('编辑模块名称失败');
                }
            }
        }

        $paint_module_info = $model_paint_module->getpaint_moduleInfo(array('paint_module_id' => intval($_GET['paint_module_id'])));
        if (empty($paint_module_info)){
            showMessage('参数错误');
        }
        Tpl::output('paint_module_array',$paint_module_info);


        Tpl::showpage('paint_module.edit');
    }

    /**
     * 删除模块名称
     */
    public function module_delOp(){
        $paint_module_id = intval($_GET['id']);
        if ($paint_module_id <= 0) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        Model('paint_module')->delpaint_module(array('paint_module_id' => $paint_module_id));
        exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        $model_paint_module = Model('paint_module');
        switch ($_GET['branch']){
            /**
             * 验证模块名称是否有重复
             */
            case 'check_module_name':
                $condition['paint_module_name'] = trim($_GET['paint_module_name']);
                $condition['paint_module_id'] = array('neq', intval($_GET['id']));
                $result = $model_paint_module->getpaint_moduleList($condition);
                if (empty($result)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }








}