<?php
/**
 * 统计概述
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class stat_extensionControl extends SystemControl
{
    private $extension_type = array('校园', '机构', '个人');
    private $extension_class = array('线上', '线下');

    public function __construct()
    {
        parent::__construct();
        Language::read('stat');
        import('function.statistics');
    }

    public function indexOp()
    {
        $this->extensionOp();
    }

    /**
     * 微信关注|取关分析
     */
    public function extensionOp()
    {
        /** @var extensionModel $model_extension */
        $model_extension = Model('extension');

        //统计的每月1号0点
        $month = date('Y-m', time());
        $stat_time = strtotime($month);

        list($stat_month_json, $stat_count) = $this->statistics($stat_time);

        $stat_count['total_follow'] = $model_extension->getSum();

        Tpl::output('stat_month_json', $stat_month_json);
        Tpl::output('stat_content', $this->statContent($stat_count));
        Tpl::output('stat_time', $month);
        Tpl::showpage('stat.extension.index');
    }

    /**
     * 微信关注|取关分析
     */
    public function extension_logOp()
    {
        $extension_code = $_GET['extension_code'];

        /** @var extensionModel $model_extension */
        $model_extension = Model('extension');

        //统计的每月1号0点
        $month = date('Y-m', time());
        $stat_time = strtotime($month);

        list($stat_month_json, $stat_count) = $this->statistics($stat_time, $extension_code);

        $extension_info = $model_extension->getOne(['extension_code' => $extension_code]);

        Tpl::output('stat_month_json', $stat_month_json);
        Tpl::output('stat_content', $this->statContent($stat_count));
        Tpl::output('member_id', $extension_info['member_id']);
        Tpl::output('stat_time', $month);
        Tpl::showpage('stat.extension_log.index');
    }

    public function get_extension_xmlOp()
    {
        /** @var extensionModel $model_extension */
        $model_extension = Model('extension');

        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('id', 'member_id', 'member_mobile', 'extension_code', 'extension_name', 'extension_type',
            'extension_class', 'area_info', 'total_follow', 'total_register', 'month_follow', 'month_cancel', 'add_time',
            'modify_time', 'qrcode_img');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $extension_list = $model_extension->getList($condition, "*", $page, $order);

        $data = array();
        if (!empty($extension_list)) {
            foreach ($extension_list as $value) {
                $url = urlAdminShop('stat_extension', 'extension_log', ['extension_code' => $value['extension_code']]);
                $operation = "<a class='btn green' href='" . $url . "'><i class='fa fa-list-alt'></i>查看</a>";

                $tmp = array();
                $tmp['operation'] = $operation;
                $tmp['id'] = $value['id'];
                $tmp['member_id'] = $value['member_id'];
                $tmp['member_mobile'] = $value['member_mobile'];
                $tmp['extension_code'] = $value['extension_code'];
                $tmp['extension_name'] = $value['extension_name'];
                $tmp['extension_type'] = $this->extension_type[$value['extension_type']];
                $tmp['extension_class'] = $this->extension_class[$value['extension_class']];
                $tmp['area_info'] = $value['area_info'];
                $tmp['total_follow'] = $value['total_follow'];
                $tmp['month_follow'] = $value['month_follow'];
                $tmp['month_cancel'] = $value['month_cancel'];
                $tmp['add_time'] = !empty($value['add_time']) ? date('Y-m-d H:i:s', $value['add_time']) : '----';
                $data['list'][] = $tmp;
            }
        }

        $data['now_page'] = $model_extension->shownowpage();
        $data['total_num'] = $model_extension->gettotalnum();
        echo Tpl::flexigridXML($data);
        exit();
    }

    /**
     * 构造图表信息
     * */
    private function statistics($stat_time, $extension_code = '')
    {
        /** @var extension_logModel $model_extension_log */
        $model_extension_log = Model('extension_log');

        /*
         * 本月关注走势
         */
        //构造横轴数据
        $stat_arr['xAxis']['categories'][] = '0';
        for ($i = $stat_time; $i < time(); $i += 86400) {
            //横轴
            $stat_arr['xAxis']['categories'][] = date('m/d', $i);
        }

        $where = array();
        $where['add_time|first_unsubscribe_time'] = array('gt', $stat_time);
        if (!empty($extension_code)) {
            $where['extension_code'] = $extension_code;
        }
        $stat_list = $model_extension_log->getList($where);

        $statcount = [
            'month_follow' => 0,
            'month_cancel' => 0,
        ];
        $add_count = [];
        $cancel_count = [];
        for ($i = 0; $i <= date('d', time()); $i++) {
            $add_count[$i] = 0;
            $cancel_count[$i] = 0;
        }

        if ($stat_list) {
            foreach ($stat_list as $key => $v) {
                if ($stat_time < $v['add_time']) {
                    $add_key = (int)date('d', $v['add_time']);
                    $add_count[$add_key]++;
                    $statcount['month_follow']++;
                }
                if ($stat_time < $v['first_unsubscribe_time']) {
                    $cancel_key = (int)date('d', $v['first_unsubscribe_time']);
                    $cancel_count[$cancel_key]++;
                    $statcount['month_cancel']++;
                }
            }
        }

        $stat_arr['series'][0]['name'] = '关注';
        $stat_arr['series'][0]['data'] = $add_count;//array_values($add_count);
        $stat_arr['series'][1]['name'] = '取关';
        $stat_arr['series'][1]['data'] = $cancel_count;//array_values($cancel_count);
        //得到统计图数据
        $stat_arr['title'] = date('Y-m', $stat_time) . ' 走势图';
        $stat_arr['yAxis'] = '关注 取关';

        $stat_json = getStatData_LineLabels($stat_arr);
        unset($stat_arr);

        return [$stat_json, $statcount];
    }

    private function statContent($stat_count)
    {
        $total_follow = intval($stat_count['total_follow']);
        $month_follow = intval($stat_count['month_follow']);
        $month_cancel = intval($stat_count['month_cancel']);
        $content = '<ul class="nc-row"><li title="总关注：' . $total_follow . '"><h4>当前总关注</h4>';
        $content .= '<h2 id="count-number" class="timer" data-speed="1500" data-to="' . $total_follow . '">' . $month_cancel . '</h2>';
        $content .= '</li><li title="月关注：' . $month_follow . '"><h4>月关注</h4>';
        $content .= '<h2 id="count-number" class="timer" data-speed="1500" data-to="' . $month_follow . '">' . $month_cancel . '</h2>';
        $content .= '</li><li title="月取关：' . $month_cancel . '"><h4>月取关</h4>';
        $content .= '<h2 id="count-number" class="timer" data-speed="1500" data-to="' . $month_cancel . '">' . $month_cancel . '</h2>';
        $content .= '</li></ul>';

        return $content;
    }
}
