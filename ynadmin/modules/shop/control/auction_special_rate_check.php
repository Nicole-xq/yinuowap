<?php
/**
 * 专场管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_special_rate_checkControl extends SystemControl{


    public function __construct(){
        parent::__construct();
//        Language::read('store,store_grade');
    }

    public function indexOp() {

        Tpl::showpage('auction_special_rate_check.index');
    }


    /**
     * 正在进行专场
     * */
    public function underway_specialOp(){
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'underway_special'));

        Tpl::showpage('auction_special.underway');
    }

    /**
     * 已审核专场
     */
    public function check_okOp(){

        Tpl::output('top_link',$this->sublink($this->_links,'check_ok'));

        Tpl::showpage('auction_special.ok');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_auction_special_rate_check = Model('auction_special_rate_check');
        $page = $_POST['rp'];
        $condition = array();
        $config_list = $model_auction_special_rate_check->getList($condition,'', '',$page);
        //专场列表
//        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);
        $data = array();
        $data['now_page'] = $model_auction_special_rate_check->shownowpage();
        $data['total_num'] = $model_auction_special_rate_check->gettotalnum();
        foreach ($config_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn green' href='index.php?act=auction_special_rate_check&op=config_info&id=".$value['id']."'><i class='fa fa-list-alt'></i>编辑</a>";

            $param['title'] = $value['title'];
            $param['rate'] = $value['rate'];
            $param['max_rate'] = $value['max_rate'];
            $param['deal_rate'] = $value['deal_rate'];

            $data['list'][$value['id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 专场细节列表
     */
    public function config_infoOp()
    {
        $model_auction_special_rate_check = Model('auction_special_rate_check');
        $config_info = $model_auction_special_rate_check->getInfo($_GET['id']);

        Tpl::output('info', $config_info);

        Tpl::showpage('auction_special_rate_check.edit');
    }

    public function save_infoOp(){
        $id = $_POST['config_id'];
        $rate = $_POST['rate'];
        $max_rate = $_POST['max_rate'];
        $deal_rate = $_POST['deal_rate'];
        $model_auction_special_rate_check = Model('auction_special_rate_check');
        $update = [
            'rate'=>$rate,
            'max_rate'=>$max_rate,
            'deal_rate'=>$deal_rate
        ];
        $config_info = $model_auction_special_rate_check->updateInfo(['id'=>$id], $update);
        if($config_info){
            showMessage('保存成功','index.php?act=auction_special_rate_check&op=index');
        }else{
            showMessage('保存失败','index.php?act=auction_special_rate_check&op=index');
        }
    }

}
