<?php
/**
 * 定制管理列表
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class customizeControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp(){
        Tpl::showpage('customize.index');
    }

    public function get_xmlOp(){
        $model_customize = Model('customize');
        $condition  = array();
        $pageSize = !empty($_POST['rp']) ? intval($_POST['rp']) : 15;
        $this->_get_condition($condition);
        $customize_list = $model_customize->getCodeList($condition, $fields = '*' , $pageSize, $order = 'id desc');

        $data = array();
        $data['now_page'] = $model_customize->shownowpage();
        $data['total_num'] = $model_customize->gettotalnum();
        $member_id_arr = [];
        foreach ($customize_list as $v) {
            if ($v['member_id'] > 0){
                $member_id_arr[] = $v['member_id'];
            }
        }
        $field = 'member_id,member_name';
        $condition = ['member_id'=>['in',implode(',',array_unique($member_id_arr))]];
        $member_list = Model('member')->getMemberList($condition, $field);
        $member_list = array_under_reset($member_list,'member_id');
        foreach ($customize_list as $k => $value) {
            $list = [
                'id'=>$value['id'],
                'cus_name'=>$value['cus_name'],
                'cus_mobile'=>$value['cus_mobile'],
                'add_time'=>date('Y-m-d',$value['add_time']),
                'member_name'=>$member_list[$value['member_id']]['member_name'],
                'member_id'=>$value['member_id'],
                'content'=>'<span title="'.$value['content'].'">'.$value['content'].'</span>',
            ];
            $data['list'][$value['id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }


    /**
     * 处理搜索条件
     * @param array $condition
     */
    private function _get_condition(&$condition) {
        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('cus_name','cus_mobile'))) {
            $condition[$_REQUEST['qtype']] = array('like',"%{$_REQUEST['query']}%");
        }
    }


}
