<?php
/**
 * 会员管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class memberControl extends SystemControl{
    const EXPORT_SIZE = 1000;
    public function __construct(){
        parent::__construct();
        Language::read('member');
    }

    public function indexOp() {
        $this->memberOp();
    }

    /**
     * 会员管理
     */
    public function memberOp(){
        Tpl::showpage('member.index');
    }

    /**
     * 会员修改
     */
    public function member_editOp(){
        $lang   = Language::getLangContent();
        $model_member = Model('member');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
            array("input"=>$_POST["member_email"], "require"=>"true", 'validator'=>'Email', "message"=>$lang['member_edit_valid_email']),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $update_array = array();
                $update_array['member_id']          = intval($_POST['member_id']);
                if (!empty($_POST['member_passwd'])){
                    $update_array['member_passwd'] = md5($_POST['member_passwd']);
                }
                $update_array['member_email']       = $_POST['member_email'];
                $update_array['member_truename']    = $_POST['member_truename'];
                $update_array['member_sex']         = $_POST['member_sex'];
                $update_array['member_qq']          = $_POST['member_qq'];
                $update_array['member_ww']          = $_POST['member_ww'];
                $update_array['inform_allow']       = $_POST['inform_allow'];
                $update_array['is_buy']             = $_POST['isbuy'];
                $update_array['is_allowtalk']       = $_POST['allowtalk'];
                $update_array['is_operator']        = $_POST['is_operator'];
                if(isset($_POST['member_type']) && is_numeric($_POST['member_type'])){
                    $update_array['member_type']        = intval($_POST['member_type']);
                }else{
                    $bind_name = trim($_POST['bind_member']);
                    if($bind_name != ''){
                        $condition['member_name']   = $bind_name;
                        $bind_member_info = $model_member->getMemberInfo($condition);
                        $update_array['bind_member_id'] = $bind_member_info['member_id'];
                        $update_array['bind_member_name'] = $bind_member_info['member_name'];
                        $update_array['bind_time'] = time();
                    }
                }

                if (!empty($_POST['member_avatar'])){
                    $update_array['member_avatar'] = $_POST['member_avatar'];
                }
                $result = $model_member->editMember(array('member_id'=>intval($_POST['member_id'])),$update_array);
                if ($result){
                    $url = array(
                    array(
                    'url'=>'index.php?act=member&op=member',
                    'msg'=>$lang['member_edit_back_to_list'],
                    ),
                    array(
                    'url'=>'index.php?act=member&op=member_edit&member_id='.intval($_POST['member_id']),
                    'msg'=>$lang['member_edit_again'],
                    ),
                    );
                    $this->log(L('nc_edit,member_index_name').'[ID:'.$_POST['member_id'].']',1);
                    isset($update_array['member_type']) ? $model_member->editMemberDistribute(['member_type'=>intval($_POST['member_type'])], ['member_id'=>intval($_POST['member_id'])]) : '';
                    showMessage($lang['member_edit_succ'],$url);
                }else {
                    showMessage($lang['member_edit_fail']);
                }
            }
        }
        $condition['member_id'] = intval($_GET['member_id']);
        $member_array = $model_member->getMemberInfo($condition);
        $distribution_config_list = Model('distribution_config')->getConfigList();

        Tpl::output('distribution_list',$distribution_config_list);
        Tpl::output('member_array',$member_array);
        Tpl::showpage('member.edit');
    }

    /**
     * 新增会员
     */
    public function member_addOp(){
        $lang   = Language::getLangContent();
        $model_member = Model('member');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["member_name"], "require"=>"true", "message"=>$lang['member_add_name_null']),
                array("input"=>$_POST["member_passwd"], "require"=>"true", "message"=>'密码不能为空'),
                array("input"=>$_POST["member_email"], "require"=>"true", 'validator'=>'Email', "message"=>$lang['member_edit_valid_email'])
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['member_name']    = trim($_POST['member_name']);
                $insert_array['member_passwd']  = trim($_POST['member_passwd']);
                $insert_array['member_email']   = trim($_POST['member_email']);
                $insert_array['member_truename']= trim($_POST['member_truename']);
                $insert_array['member_sex']     = trim($_POST['member_sex']);
                $insert_array['member_type']    = 0;
                $insert_array['member_qq']      = trim($_POST['member_qq']);
                $insert_array['member_ww']      = trim($_POST['member_ww']);
                //默认允许举报商品
                $insert_array['inform_allow']   = '1';
                if (!empty($_POST['member_avatar'])){
                    $insert_array['member_avatar'] = trim($_POST['member_avatar']);
                }

                $result = $model_member->addMember($insert_array);
                if ($result){
                    $url = array(
                    array(
                    'url'=>'index.php?act=member&op=member',
                    'msg'=>$lang['member_add_back_to_list'],
                    ),
                    array(
                    'url'=>'index.php?act=member&op=member_add',
                    'msg'=>$lang['member_add_again'],
                    ),
                    );
                    $this->log(L('nc_add,member_index_name').'[ '.$_POST['member_name'].']',1);
                    showMessage($lang['member_add_succ'],$url);
                }else {
                    showMessage($lang['member_add_fail']);
                }
            }
        }
        Tpl::showpage('member.add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 验证会员是否重复
             */
            case 'check_user_name':
                $model_member = Model('member');
                $condition['member_name']   = $_GET['member_name'];
                $condition['member_id'] = array('neq',intval($_GET['member_id']));
                $list = $model_member->getMemberInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
            /**
             * 验证邮件是否重复
             */
            case 'check_email':
                $model_member = Model('member');
                $condition['member_email'] = $_GET['member_email'];
                $condition['member_id'] = array('neq',intval($_GET['member_id']));
                $list = $model_member->getMemberInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
            /**
             * 验证用户是否是存在
             */
            case 'check_member':
                $model_member = Model('member');
                $condition['member_name']   = trim($_GET['bind_member']);
                $list = $model_member->getMemberInfo($condition);
                if (!empty($list) && in_array($list['member_type'],array('2','3')) || trim($_GET['bind_member']) == ''){
                    echo 'true';exit;
                } else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $member_grade = $model_member->getMemberGradeArr();
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $this->_get_condition($condition);
        $order = '';
        $param = array('member_id','member_name','member_avatar','member_email','member_mobile','member_sex','member_truename'
                ,'member_time','member_login_time','member_login_ip','member_points','member_exppoints','member_grade','available_predeposit'
                ,'freeze_predeposit','available_rc_balance','freeze_rc_balance','inform_allow','is_buy','is_allowtalk','member_state','member_type'
        );
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $member_list = $model_member->getMemberList($condition, '*', $page, $order);

        $sex_array = $this->get_sex();

        $type_arr = $this->_getMemberType();

        $data = array();
        $data['now_page'] = $model_member->shownowpage();
        $data['total_num'] = $model_member->gettotalnum();
        foreach ($member_list as $value) {
            $member_distribute = $model_member->getUpperMember(['member_id'=>$value['member_id']],'top_member_name,dis_code');
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=member&op=member_edit&member_id=" . $value['member_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = "<img src=".getMemberAvatarForID($value['member_id'])." class='user-avatar' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getMemberAvatarForID($value['member_id']).">\")'>".$value['member_name'];
            $param['member_type'] = $type_arr[$value['member_type']];
            $param['member_role'] = getRoleName($value['member_role']);
            $param['member_email'] = $value['member_email'];
            $param['member_mobile'] = $value['member_mobile'];
            $param['member_sex'] = $sex_array[$value['member_sex']];
            $param['member_truename'] = $value['member_truename'];
            $param['top_member_name'] = $member_distribute['top_member_name'];
            $param['dis_code'] = $member_distribute['dis_code'];
            $param['member_distribute_num'] = $model_member->getCountDistribute(['top_member'=>$value['member_id']]);
            $param['member_time'] = date('Y-m-d H:i:s', $value['member_time']);
            $param['registered_source'] = $value['registered_source'];
            $param['member_login_time'] = date('Y-m-d H:i:s', $value['member_login_time']);
            $param['member_login_ip'] = $value['member_login_ip'];
            $param['member_points'] = $value['member_points'];
            $param['member_exppoints'] = $value['member_exppoints'];
            $param['member_grade'] = ($t = $model_member->getOneMemberGrade($value['member_exppoints'], false, $member_grade))?$t['level_name']:'';
            $param['available_predeposit'] = ncPriceFormat($value['available_predeposit']);
            $param['freeze_predeposit'] = ncPriceFormat($value['freeze_predeposit']);
            $param['available_rc_balance'] = ncPriceFormat($value['available_rc_balance']);
            $param['freeze_rc_balance'] = ncPriceFormat($value['freeze_rc_balance']);
            $param['inform_allow'] = $value['inform_allow'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $param['is_buy'] = $value['is_buy'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $param['is_allowtalk'] = $value['is_allowtalk'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $data['list'][$value['member_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 性别
     * @return multitype:string
     */
    private function get_sex() {
        $array = array();
        $array[1] = '男';
        $array[2] = '女';
        $array[3] = '保密';
        return $array;
    }
    /**
     * 会员类型
     */
    private function _getMemberType(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $arr = array();
        foreach($re as $v){
            $arr[$v['type_id']] = $v['name'];
        }
        return $arr;
    }
    /**
     * csv导出
     */
    public function export_csvOp() {
        $model_member = Model('member');
        $condition = array();
        $limit = false;
        if ($_GET['id'] != '') {
            $id_array = explode(',', $_GET['id']);
            $condition['member_id'] = array('in', $id_array);
        }
        if ($_GET['query'] != '') {
            $condition[$_GET['qtype']] = array('like', '%' . $_GET['query'] . '%');
        }
        $order = '';
        $param = array('member_id','member_name','member_avatar','member_email','member_mobile','member_sex','member_truename','member_birthday'
                ,'member_time','member_login_time','member_login_ip','member_points','member_exppoints','member_grade','available_predeposit'
                ,'freeze_predeposit','available_rc_balance','freeze_rc_balance','inform_allow','is_buy','is_allowtalk','member_state','member_type'
        );
        if (in_array($_GET['sortname'], $param) && in_array($_GET['sortorder'], array('asc', 'desc'))) {
            $order = $_GET['sortname'] . ' ' . $_GET['sortorder'];
        }
        if (!is_numeric($_GET['curpage'])){
            $count = $model_member->getMemberCount($condition);
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $array = array();
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=member&op=index');
                Tpl::showpage('export.excel');
                exit();
            }
        } else {
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $limit = $limit1 .','. $limit2;
        }

        $member_list = $model_member->getMemberList($condition, '*', null, $order, $limit);
        $this->createCsv($member_list);
    }
    /**
     * 生成csv文件
     */
    private function createCsv($member_list) {
        $model_member = Model('member');
        $member_grade = $model_member->getMemberGradeArr();
        // 性别
        $sex_array = $this->get_sex();
        //会员类型
        $type_arr = $this->_getMemberType();
        $data = array();
        foreach ($member_list as $value) {
            $param = array();
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = $value['member_name'];
            $param['member_avatar'] = getMemberAvatarForID($value['member_id']);
            $param['member_type'] = $type_arr[$value['member_type']];
            $param['member_email'] = $value['member_email'];
            $param['member_mobile'] = $value['member_mobile'];
            $param['member_sex'] = $sex_array[$value['member_sex']];
            $param['member_truename'] = $value['member_truename'];
            $param['member_birthday'] = $value['member_birthday'];
            $param['member_time'] = date('Y-m-d', $value['member_time']);
            $param['member_login_time'] = date('Y-m-d', $value['member_login_time']);
            $param['member_login_ip'] = $value['member_login_ip'];
            $param['member_points'] = $value['member_points'];
            $param['member_exppoints'] = $value['member_exppoints'];
            $param['member_grade'] = ($t = $model_member->getOneMemberGrade($value['member_exppoints'], false, $member_grade))?$t['level_name']:'';
            $param['available_predeposit'] = ncPriceFormat($value['available_predeposit']);
            $param['freeze_predeposit'] = ncPriceFormat($value['freeze_predeposit']);
            $param['available_rc_balance'] = ncPriceFormat($value['available_rc_balance']);
            $param['freeze_rc_balance'] = ncPriceFormat($value['freeze_rc_balance']);
            $param['inform_allow'] = $value['inform_allow'] ==  '1' ? '是' : '否';
            $param['is_buy'] = $value['is_buy'] ==  '1' ? '是' : '否';
            $param['is_allowtalk'] = $value['is_allowtalk'] ==  '1' ? '是' : '否';
            $param['member_state'] = $value['member_state'] ==  '1' ? '是' : '否';
            $data[$value['member_id']] = $param;
        }

        $header = array(
                'member_id' => '会员ID',
                'member_name' => '会员名称',
                'member_avatar' => '会员头像',
                'member_type' => '会员类型',
                'member_email' => '会员邮箱',
                'member_mobile' => '会员手机',
                'member_sex' => '会员性别',
                'member_truename' => '真实姓名',
                'member_birthday' => '出生日期',
                'member_time' => '注册时间',
                'member_login_time' => '最后登录时间',
                'member_login_ip' => '最后登录IP',
                'member_points' => '会员诺币',
                'member_exppoints' => '会员经验',
                'member_grade' => '会员等级',
                'available_predeposit' => '可用预存款(元)',
                'freeze_predeposit' => '冻结预存款(元)',
                'available_rc_balance' => '可用充值卡(元)',
                'freeze_rc_balance' => '冻结充值卡(元)',
                'inform_allow' => '允许举报',
                'is_buy' => '允许购买',
                'is_allowtalk' => '允许咨询',
                'member_state' => '允许登录'
        );
        \Shopnc\Lib::exporter()->output('member_list' .$_GET['curpage'] . '-'.date('Y-m-d'), $data, $header);
    }

    public function to_sendOp(){
        if(chksubmit()){
            //处理发送逻辑
            $send_id = intval($_POST['radio_box']);
            $member_param = unserialize(cookie('send_param'));
            setNcCookie('send_param','',-1);
            $this->batchSend($send_id,$member_param);
            showMessage(L('nc_common_op_succ'),'index.php?act=message&op=global_tpl');
        }
        //暂先记录下要发送的会员条件
        $condition = [];
        $this->_get_condition($condition);
        setNcCookie('send_param',serialize($condition),2*3600);

        $condition = [];
        $field = 'id,tpl_title,send_type';
        $global_models = Model('global_msg_tpl');
        $global_list = $global_models->getGlobalMsgTplList($condition,$field,$group = '',$order = 'id DESC');
        $send_type = $global_models->getSendType();
        foreach ($global_list as $key=>$val){
            $global_list[$key]['send_type'] = $send_type[$val['send_type']];
        }
        Tpl::output('global_list',$global_list);
        Tpl::showpage('to_send', 'null_layout');
    }



    /**
     * 处理搜索条件
     */
    private function _get_condition(&$condition){
        if(!in_array($_GET['qtype_time'], array('member_time'))){
            $_GET['qtype_time'] = null;
        }
        $if_start_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/', $_GET['query_start_date']);
        $if_end_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/', $_GET['query_end_date']);
        $start_unixtime = $if_start_time ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_time ? strtotime($_GET['query_end_date']) : null;
        if($_GET['qtype_time'] && ($start_unixtime || $end_unixtime)){
            $condition[$_GET['qtype_time']] = array('time', array($start_unixtime, $end_unixtime));
        }
        if($_GET['registered_source']){
            $condition['registered_source'] = $_GET['registered_source'];
            //$condition['registered_source'] = array('like', '%' . $_GET['registered_source'] . '%');
        }
        if ($_GET['id'] != '') {
            $id_array = explode(',', $_GET['id']);
            $condition['member_id'] = array('in', $id_array);
        }
        if ($_GET['query'] != '') {
            $condition[$_GET['qtype']] = array('like', '%' . $_GET['query'] . '%');
        }
        return $condition;
    }


    /**
     * 批量发送
     * @param $send_id
     * @param $send_member_param
     */
    private function batchSend($send_id, $send_member_param) {
        $member_list = Model('member')->getMemberList($send_member_param, 'member_id,member_mobile',null,'member_id DESC',false);

        $send_member_id_arr = [];
        foreach ($member_list as $value){
            $param = [
                'code'      => 'global_msg_tpl',
                'member_id' => $value['member_id'],
                'param'     => ['global_send_id'=>$send_id],
            ];
            if ($value['member_mobile']){
                $param['number']['mobile'] = $value['member_mobile'];
            }
            $send_member_id_arr[] = $value['member_id'];
            QueueClient::push('sendMemberMsg', $param);
        }
        Model('global_msg_tpl')->editMemberMsgTpl(['id'=>$send_id], ['send_state'=>2,'send_time'=>time()]);
        Model('global_msg_log')->addGlobalMsgLog(['global_id'=>$send_id,'send_member_id_str'=>implode(',',$send_member_id_arr),'add_time'=>time()]);
    }

    /**
     * 用户账户余额详情
     */
    public function member_accountOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $member_model = Model('member');
        $member_info = $member_model->getMemberInfo(['member_id'=>$member_id],'member_id,member_name,member_mobile,available_predeposit,freeze_predeposit');
        if (!$member_info){
            showMessage('参数错误','','html','error');
        }
        $pd_cash = Model('predeposit')->getPdCashInfo(['pdc_id'=>intval($_GET['pdc_id'])], $fields = 'pdc_amount');
        $member_info['pdc_amount'] = ncPriceFormat($pd_cash['pdc_amount']);
        //充值统计
        $member_info['recharge_sum'] = Model('predeposit')->getPdRechargeSum(['pdr_member_id'=>$member_id,'pdr_payment_state'=>1],'pdr_amount');
        //提现统计
        $member_info['enchashment_sum'] = Model('predeposit')->getPdCashSum(['pdc_member_id'=>$member_id,'pdc_payment_state'=>1],'pdc_amount');
        //订单取消金额统计
        $member_info['order_channel_sum'] = Model('predeposit')->getPdLogSum(array('lg_member_id'=>$member_id,'lg_type'=>array('in','order_cancel,refund')),'lg_av_amount');
        //预存款冻结统计
        //$member_info['freeze_enchashment_sum'] = Model('predeposit')->getPdCashSum(['pdc_member_id'=>$member_id,'pdc_payment_state'=>0],'pdc_amount');
        //预存款支付订单统计
        $member_info['order_pay_sum'] = Model('order')->getPdSum(['buyer_id'=>$member_id,'pd_amount'=>['gt',0]],'pd_amount');
        //保证金支付统计
        $member_info['earnest_sum'] = Model('margin_orders')->getMarginSum(['buyer_id'=>$member_id,'api_pay_state'=>1],'api_pay_amount');
        //保证金冻结统计
        $tmp_1 = Model('margin_orders')->getMarginSum(['buyer_id'=>$member_id,'order_state'=>['neq','1']],'pd_amount');
        $tmp_2 = Model('margin_orders')->getMarginSum(['buyer_id'=>$member_id,'refund_state'=>0,'order_state'=>1],'margin_amount');
        $member_info['auction_sum'] = $tmp_1+$tmp_2;
        //奖励金统计
        $member_info['commission_sum'] = Model('member_commission')->getCommissionSum(['dis_member_id'=>$member_id,'dis_commis_state'=>1],'commission_amount');

        $member_info['result_totals_1'] = $member_info['recharge_sum'] + $member_info['earnest_sum'] + $member_info['commission_sum'] + $member_info['order_channel_sum'];
        $member_info['result_totals_2'] = $member_info['enchashment_sum'] + $member_info['freeze_predeposit'] + $member_info['order_pay_sum'] + $member_info['auction_sum'];

        $member_info['result_totals'] = ncPriceFormat($member_info['result_totals_1']) - ncPriceFormat($member_info['result_totals_2']);

        Tpl::output('member_info',$member_info);
        Tpl::showpage('member_account');
    }

    /**
     * 充值列表
     */
    public function recharge_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_pd = Model('predeposit');
        $recharge_list = $model_pd->getPdRechargeList(['pdr_member_id'=>$member_id,'pdr_payment_state'=>1],$_POST['rp'],'*','pdr_id desc');
        $data = [
            'now_page'=>$model_pd->shownowpage(),
            'total_num'=>$model_pd->gettotalnum(),
        ];
        foreach ($recharge_list as $recharge_info) {
            $pdr_id = $recharge_info['pdr_id'];
            if($recharge_info['financial_verification']){
                $operation = "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_predeposit({pdr_id:$pdr_id,msg:'未校验'})\" style='background: #cccccc;border-color: #cccccc'><i class='fa fa-check-circle-o'></i>校验</a>";
            }else{
                $operation = "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_predeposit({pdr_id:$pdr_id,msg:'已校验'})\" style='background-color: #1BBC9D;color: #ffffff;border-color: #16A086'><i class='fa fa-check-circle-o'></i>校验</a>";
            }
            $list = [
                'operation'=>$operation,
                'pdr_sn'=>$recharge_info['pdr_sn'],
                'pdr_add_time'=>date('Y-m-d H:i:s',$recharge_info['pdr_add_time']),
                'pdr_payment_name'=>$recharge_info['pdr_payment_name'],
                'pdr_amount'=>$recharge_info['pdr_amount'],
                'pdr_payment_state'=>intval($recharge_info['pdr_payment_state']) ? '已支付' : '未支付',
                'pdr_sn'=>$recharge_info['pdr_sn'],
            ];
            $data['list'][$recharge_info['pdr_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
    /**
     * 提现列表
     */
    public function enchashment_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_pd = Model('predeposit');
        $enchashment_list = $model_pd->getPdCashList(['pdc_member_id'=>$member_id,'pdc_payment_state'=>1],$_POST['rp'],'*','pdc_id desc');
        $data = [
            'now_page'=>$model_pd->shownowpage(),
            'total_num'=>$model_pd->gettotalnum(),
        ];
        foreach ($enchashment_list as $enchashment_info) {
            $list = [
                'pdr_sn'=>$enchashment_info['pdc_sn'],
                'pdc_add_time'=>date('Y-m-d H:i:s',$enchashment_info['pdc_add_time']),
                'pdc_amount'=>$enchashment_info['pdc_amount'],
                'pdr_payment_state'=>intval($enchashment_info['pdc_payment_state']) ? '已支付' : '未支付',
            ];
            $data['list'][$enchashment_info['pdc_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 预存款支付订单列表
     */
    public function orderPay_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_order = Model('order');
        $condition = [
            'buyer_id'=>$member_id,
            'pd_amount'=>['gt',0],
        ];
        $orderPay_list = $model_order->getOrderList($condition,$_POST['rp'],'*');
        $data = [
            'now_page'=>$model_order->shownowpage(),
            'total_num'=>$model_order->gettotalnum(),
        ];
        foreach ($orderPay_list as $order_info) {
            $list = [
                'order_sn'=>$order_info['order_sn'].str_replace(array(1,2,3,4), array(null,' [预定]','[门店自提]',' [拼团]'), $order_info['order_type']),
                'order_from'=> str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']),
                'add_times'=> date('Y-m-d H:i:s',$order_info['add_time']),
                'order_amount'=> ncPriceFormat($order_info['order_amount']),
                'order_state'=> orderState($order_info),
                'pay_sn'=> empty($order_info['pay_sn']) ? '' : $order_info['pay_sn'],
                'payment_code'=> orderPaymentName($order_info['payment_code']),
                'payment_time'=> !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '',
                'finnshed_time'=> !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '',
                'refund_amount'=> ncPriceFormat($order_info['refund_amount']),
                'points_amount'=> ncPriceFormat($order_info['points_amount']),
                'pd_amount'=> ncPriceFormat($order_info['pd_amount']),
            ];
            $data['list'][$order_info['order_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }


    /**
     * 保证金支付金额列表
     */
    public function earnest_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_margin = Model('margin_orders');
        $condition = [
            'buyer_id'=>$member_id,
            'api_pay_state'=>1
        ];
        $earnest_list = $model_margin->getOrderList2($condition,'*','','margin_id DESC',0,$_POST['rp']);
        $data = [
            'now_page'=>$model_margin->shownowpage(),
            'total_num'=>$model_margin->gettotalnum(),
        ];
        foreach ($earnest_list as $order_info) {
            $margin_id = $order_info['margin_id'];
            if($order_info['order_state'] == 1){
                if($order_info['financial_verification']){
                    $operation = "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_margin_orders({margin_id:$margin_id,msg:'未校验'})\" style='background: #cccccc;border-color: #cccccc'><i class='fa fa-check-circle-o'></i>校验</a>";
                }else{
                    $operation = "<a class='btn' href=\"javascript:void(0)\" onclick=\"fv_margin_orders({margin_id:$margin_id,msg:'已校验'})\" style='background-color: #1BBC9D;color: #ffffff;border-color: #16A086'><i class='fa fa-check-circle-o'></i>校验</a>";
                }
            }
            $list = [
                'operation'=>$operation,
                'order_sn'=>$order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']),
                'order_from'=>str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']),
                'created_at'=>date('Y-m-d H:i:s',$order_info['created_at']),
                'margin_amount'=>ncPriceFormat($order_info['margin_amount']),
                'order_state'=> orderState($order_info),
                'payment_code'=> orderPaymentName($order_info['payment_code']),
                'payment_time'=> !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d H:i:s',$order_info['payment_time'])) : '',
                'finnshed_time'=> !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '',
                'rcb_amount'=> ncPriceFormat($order_info['rcb_amount']),
                'pd_amount'=> ncPriceFormat($order_info['pd_amount']),
                'points_amount'=> ncPriceFormat($order_info['points_amount']),
                'account_margin_amount'=> ncPriceFormat($order_info['account_margin_amount']),
                'refund_amount'=> ncPriceFormat($order_info['refund_amount']),
                'api_pay_amount'=> ncPriceFormat($order_info['api_pay_amount']),
            ];
            $data['list'][$order_info['margin_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
    
    /**
     * 保证金冻结列表
     */
    public function auction_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_margin = Model('margin_orders');
        $condition = [
            'buyer_id'=>$member_id,
            'refund_state'=>0,
            'order_state'=>1,
        ];
        //print_r($condition);
        $earnest_list = $model_margin->getOrderList2($condition,'*','','margin_id DESC',0,$_POST['rp']);
        $data = [
            'now_page'=>$model_margin->shownowpage(),
            'total_num'=>$model_margin->gettotalnum(),
        ];
        
        //print_r($earnest_list);die();
        foreach ($earnest_list as $order_info) {
            $list = [
                'order_sn'=>$order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']),
                'order_from'=>str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']),
                'created_at'=>date('Y-m-d H:i:s',$order_info['created_at']),
                'margin_amount'=>ncPriceFormat($order_info['margin_amount']),
                'order_state'=> orderMarginState($order_info),
                'payment_code'=> orderPaymentName($order_info['payment_code']),
                'payment_time'=> !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '',
                'finnshed_time'=> !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '',
                'pd_amount'=> ncPriceFormat($order_info['pd_amount']),
                'points_amount'=> ncPriceFormat($order_info['points_amount']),
                'account_margin_amount'=> ncPriceFormat($order_info['account_margin_amount']),
                'refund_amount'=> ncPriceFormat($order_info['refund_amount']),
            ];
            $data['list'][$order_info['margin_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 已结算的返佣金列表
     */
    public function commission_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_commission = Model('member_commission');
        $condition = [
            'dis_member_id'=>$member_id,
            'dis_commis_state'=>1
        ];
        $commission_list = $model_commission->getCommissionList($condition,'*',$_POST['rp'],'log_id desc');
        $data = [
            'now_page'=>$model_commission->shownowpage(),
            'total_num'=>$model_commission->gettotalnum(),
        ];
        foreach ($commission_list as $commission_info) {
            $list = [
                'log_id'=>$commission_info['log_id'],
                'commission_time'=>date('Y-m-d H:i:s',$commission_info['commission_time']),
                'commission_amount'=>ncPriceFormat($commission_info['commission_amount']),
                'goods_name'=>$commission_info['goods_name']
            ];
            $data['list'][$commission_info['log_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 预存款变动明细列表
     */
    public function predeposit_list_xmlOp(){
        $member_id = intval($_GET['member_id']);
        if ($member_id <= 0 ) {
            exit();
        }
        $model_predeposit = Model('predeposit');
        $condition = [
            'lg_member_id'=>$member_id
        ];
        $predeposit_list = $model_predeposit->getPdLogList($condition,$_POST['rp'],'*','lg_id desc');
        $data = [
            'now_page'=>$model_predeposit->shownowpage(),
            'total_num'=>$model_predeposit->gettotalnum(),
        ];
        foreach ($predeposit_list as $predeposit_info) {
            $list = [
                'lg_id'=>$predeposit_info['lg_id'],
                'lg_av_amount'=>ncPriceFormat($predeposit_info['lg_av_amount']),
                'lg_freeze_amount'=>ncPriceFormat($predeposit_info['lg_freeze_amount']),
                'lg_freeze_predeposit'=>ncPriceFormat($predeposit_info['lg_freeze_predeposit']),
                'lg_available_amount'=>ncPriceFormat($predeposit_info['lg_available_amount']),
                'lg_add_time'=>date('Y-m-d H:i:s', $predeposit_info['lg_add_time']),
                'lg_desc'=>$predeposit_info['lg_desc'],
            ];
            $data['list'][$predeposit_info['lg_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
}
