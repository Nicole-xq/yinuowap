<?php
/**
 * 出价
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_bid_logControl extends SystemControl{

    public function indexOp(){
        Tpl::showpage('auctions_bid_log.index');
    }
    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        // 设置页码参数名称
        $condition['member_id'] = array('neq',0);
        $order = '';

        $page   = new Page();
        $page->setEachNum($_POST['rp']);
        $page->setStyle('admin');
        $condition['order']="bid_id desc";
        //列表
        $list = Model('bid_log')->getList($condition, $page);
        $data = array();
        $data['now_page'] = $page->get('now_page');
        $data['total_num'] = $page->get('total_num');
        foreach ((array)$list as $value) {
            $param = array();
            $param['bid_id'] = $value['bid_id'];
            $param['member_name'] = $value['member_name'];
            $param['auction_id'] = $value['auction_id'];
            $param['offer_num'] = $value['offer_num'];
            $param['created_at'] = date("Y-m-d H:i:s",$value['created_at']);
            $condition2['auction_id']=$value['auction_id'];
            $auctions = Model("")->table("auctions")->where($condition2)->find();
            $auction_special_info = Model('auction_special')->getSpecialInfo(array('special_id'=>$auctions['special_id']));
            $param['auction_name']=$auctions['auction_name'];
            $param['special_name']=$auction_special_info['special_name'];
            $data['list'][$value['bid_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }
}