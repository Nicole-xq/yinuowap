<?php
/**
 * 商品趣猜
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class promotion_guessControl extends SystemControl{

    public function __construct(){
        parent::__construct();
        Language::read('groupbuy');
        $this->act_guess_state_array =Model('p_guess')->getGuessStateArray();
    }

    public function indexOp() {
        $this->guess_listOp();
    }

    /**
     * 进行中趣猜列表，只可推荐
     *
     */
    public function guess_listOp()
    {
        Tpl::output('act_guess_state_array',  $this->act_guess_state_array);
        Tpl::showpage('guess.list');
    }

    /**
     * 趣猜活动管理
     * @return [type] [description]
     */
    public function guess_list_xmlOp()
    {
        $condition = array();
        if ($_REQUEST['advanced']) {
            if (strlen($q = trim((string) $_REQUEST['name']))) {
                $condition['name'] = array('like', '%' . $q . '%');
            }
            if (strlen($q = trim((string) $_REQUEST['store_name']))) {
                $condition['store_name'] = array('like', '%' . $q . '%');
            }
            if (($q = (int) $_REQUEST['state']) > 0) {
                $condition['state'] = $q;
            }

            $pdates = array();
            if (strlen($q = trim((string) $_REQUEST['pdate1'])) && ($q = strtotime($q . ' 00:00:00'))) {
                $pdates[] = "tend >= {$q}";
            }
            if (strlen($q = trim((string) $_REQUEST['pdate2'])) && ($q = strtotime($q . ' 00:00:00'))) {
                $pdates[] = "tstart <= {$q}";
            }
            if ($pdates) {
                $condition['pdates'] = array(
                    'exp',
                    implode(' or ', $pdates),
                );
            }

        } else {
            switch ($_REQUEST['qtype']) {
                case 'id':
                    $condition['id'] = array('like', '%'.$_REQUEST['query'].'%');
                    break;
                case 'name':
                    $condition['name'] = array('like', '%'.$_REQUEST['query'].'%');
                    break;
                case 'store_name':
                    $condition['store_name'] = array('like', '%'.$_REQUEST['query'].'%');
                    break;
            }
        }

        switch ($_REQUEST['sortname']) {
            case 'tstart':
                $sort = 'tstart';
                break;
            case 'tend':
                $sort = 'tend';
                break;
            case 'name':
                $sort = 'name';
                break;
            case 'store_name':
                $sort = 'store_name';
                break;
            case 'recommended':
                $sort = 'recommended';
                break;
            default:
                $sort = 'id';
                break;
        }
        if ($_REQUEST['sortorder'] != 'asc') {
            $sort .= ' desc';
        }

        $model_act_guess = Model('p_act_guess');
        $act_guess_list = (array) $model_act_guess->getGusActList($condition, $_REQUEST['rp'], $sort);
        $flippedOwnShopIds = array_flip(Model('store')->getOwnShopIds());
        $data = array();
        $data['now_page'] = $model_act_guess->shownowpage();
        $data['total_num'] = $model_act_guess->gettotalnum();

        foreach ($act_guess_list as $val) {
            $u = SHOP_SITE_URL . "/index.php?act=show_guess&op=guess_list&gs_act_id=" . $val['id'];
            
            $o = '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            $o .= '<li><a class="confirm-on-click" href="' . $u . '" target="_blank">查看活动</a></li>';

            if ($val['state'] == 10) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="' . urlAdminShop('promotion_guess', 'guess_review_pass', array(
                    'id' => $val['id'],
                )) . '">批准活动</a></li>';
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="' . urlAdminShop('promotion_guess', 'guess_review_fail', array(
                    'id' => $val['id'],
                )) . '">拒绝活动</a></li>';
            }
            if ($val['recommended'] == '1') {
                $o .= '<li><a href="javascript:;" data-href="' . urlAdminShop('promotion_guess', 'guess_rec', array(
                    'id' => $val['id'],
                    'rec' => 0,
                )) . '">取消推荐</a></li>';
            } else {
                $o .= '<li><a href="javascript:;" data-href="' . urlAdminShop('promotion_guess', 'guess_rec', array(
                    'id' => $val['id'],
                    'rec' => 1,
                )) . '">推荐活动</a></li>';
            }

            $o .= '</ul></span>';

            $i = array();
            $i['operation'] = $o;
            $i['id'] = $val['id'];
            $i['groupbuy_name'] = $val['name'];

            $i['store_name'] = '<a target="_blank" href="'
                . urlShop('show_store', 'index', array('store_id'=>$val['store_id']))
                . '">' . $val['store_name'] . '</a>';

            if (isset($flippedOwnShopIds[$val['store_id']])) {
                $i['store_name'] .= '<span class="ownshop">[自营]</span>';
            }   
            if (empty($val['gs_act_img'])) {
                $gi = gthumb($val['groupbuy_image'], 'small'); 
            } else {
                $gi = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess'.DS.$val['gs_act_img'];
            }
            $i['groupbuy_image'] = <<<EOB
<a href="javascript:;" class="pic-thumb-tip" onMouseOut="toolTip()" onMouseOver="toolTip('<img src=\'{$gi}\'>')">
<i class='fa fa-picture-o'></i></a>
EOB;

            $i['start_time_text'] = date("Y-m-d H:i:s",$val['tstart']);
            $i['end_time_text'] = date("Y-m-d H:i:s",$val['tend']);
            $i['recommended'] = $val['recommended'] == '1'
                ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>'
                : '<span class="no"><i class="fa fa-ban"></i>否</span>';

           // $i['groupbuy_state_text'] = $this->act_guess_state_array[$val['state']];

            $data['list'][$val['id']] = $i;
        }
        echo Tpl::flexigridXML($data);
        exit;
    }

    /** 
     * 趣猜单场待审核列表
     * @return [type] [description]
     */
    public function guess_verify_list_xmlOp()
    {
        $condition = array();
        if ($_REQUEST['advanced']) {
            if (strlen($q = trim((string) $_REQUEST['name']))) {
                $condition['gs_name'] = array('like', '%' . $q . '%');
            }
            if (strlen($q = trim((string) $_REQUEST['store_name']))) {
                $condition['store_name'] = array('like', '%' . $q . '%');
            }
            if (($q = (int) $_REQUEST['state']) > 0) {
                $condition['state'] = $q;
            }

            $pdates = array();
            if (strlen($q = trim((string) $_REQUEST['pdate1'])) && ($q = strtotime($q . ' 00:00:00'))) {
                $pdates[] = "end_time >= {$q}";
            }
            if (strlen($q = trim((string) $_REQUEST['pdate2'])) && ($q = strtotime($q . ' 00:00:00'))) {
                $pdates[] = "start_time <= {$q}";
            }
            if ($pdates) {
                $condition['pdates'] = array(
                    'exp',
                    implode(' or ', $pdates),
                );
            }

        } else {
            switch ($_REQUEST['qtype']) {
                case 'name':
                    $condition['gs_name'] = array('like', '%'.$_REQUEST['query'].'%');
                    break;
                case 'store_name':
                    $condition['store_name'] = array('like', '%'.$_REQUEST['query'].'%');
                    break;
            }
        }

        switch ($_REQUEST['sortname']) {
            case 'start_time':
                $sort = 'start_time';
                break;
            case 'end_time':
                $sort = 'end_time';
                break;
            default:
                $sort = 'gs_id';
                break;
        }
        if ($_REQUEST['sortorder'] != 'asc') {
            $sort .= ' desc';
        }

        $model_guess = Model('p_guess');
        $condition['verify_state'] = 0;
        $guess_list = (array) $model_guess->getGuessList($condition,$field = '*',$sort,$_REQUEST['rp']);
        $flippedOwnShopIds = array_flip(Model('store')->getOwnShopIds());
        $data = array();
        $data['now_page'] = $model_guess->shownowpage();
        $data['total_num'] = $model_guess->gettotalnum();

        foreach ($guess_list as $val) {
            $u = "index.php?act=promotion_guess&op=guess_verify_detail&guess_id=" . $val['gs_id'];
            
            $o = '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            $o .= '<li><a class="confirm-on-click" href="' . $u . '">趣猜审核</a></li>';

            $o .= '</ul></span>';

            $i = array();
            $i['operation'] = $o;

            $i['gs_name'] = $val['gs_name'];

            $i['store_name'] = '<a target="_blank" href="'
                . urlShop('show_store', 'index', array('store_id'=>$val['store_id']))
                . '">' . $val['store_name'] . '</a>';

            if (isset($flippedOwnShopIds[$val['store_id']])) {
                $i['store_name'] .= '<span class="ownshop">[自营]</span>';
            }   
            if (empty($val['gs_img'])) {
                $gi = gthumb($val['groupbuy_image'], 'small'); 
            } else {
                $gi = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$val['gs_img'];
            }
            $i['gs_image'] = <<<EOB
<a href="javascript:;" class="pic-thumb-tip" onMouseOut="toolTip()" onMouseOver="toolTip('<img src=\'{$gi}\'>')">
<i class='fa fa-picture-o'></i></a>
EOB;

            $i['start_time'] = date("Y-m-d H:i:s",$val['start_time']);
            $i['end_time'] = date("Y-m-d H:i:s",$val['end_time']);
            $i['gs_verify_text'] = $this->act_guess_state_array[$val['state']];
            $data['list'][$val['gs_id']] = $i;
        }
        echo Tpl::flexigridXML($data);
        exit;
    }




    

    /**
     * 推荐
     */
    public function guess_recOp()
    {
        $model= Model('p_act_guess');
        $update_array['recommended'] = $_GET['rec'] == '1' ? 1 : 0;
        $where_array['id'] = $_GET['id'];
        $result = $model->editGuess($update_array, $where_array);

        if ($result) {
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

     /**
     * 审核通过
     */
    public function guess_review_passOp(){
        $id = intval($_REQUEST['id']);

        $model_guess = Model('p_act_guess');
        $result = $model_guess->reviewPassGuess($id);
        if($result) {
            $this->log('通过趣猜活动申请，趣猜编号'.$id,null);
            // 添加队列
            // $groupbuy_info = $model_groupbuy->getGroupbuyInfo(array('groupbuy_id' => $groupbuy_id));
            // Model('cron')->addCron(array('exetime' => $groupbuy_info['start_time'], 'exeid' => $groupbuy_info['goods_commonid'], 'type' => 5));
            // Model('cron')->addCron(array('exetime' => $groupbuy_info['end_time'], 'exeid' => $groupbuy_info['goods_commonid'], 'type' => 6));

            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    /**
     * 审核失败
     */
    public function guess_review_failOp(){
        $id = intval($_REQUEST['id']);

        $model_guess = Model('p_act_guess');
        $result = $model_guess->reviewFailGuess($id);
        if($result) {
            $this->log('拒绝趣猜活动申请，趣猜编号'.$id,null);

            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    /** 
     * 趣猜待审核列表
     * @return [type] [description]
     */
    public function guess_verify_listOp(){
        Tpl::output('act_guess_state_array',  $this->act_guess_state_array);
        Tpl::showpage('guess_verify');        
    }

    /**
     * 待审核趣猜详情页
     * @return [type] [description]
     */
    public function guess_verify_detailOp(){
        $model_guess = Model('p_guess');
         if (chksubmit()) {
            $gs_id = $_POST['gs_id'];
            $verify_state = $_POST['verify_state'];
            $verify_desc = '';
            if ($verify_state == 2) {
                $verify_desc = $_POST['verify_desc'];
            }
            $result = $model_guess->editGuess(array('verify_state' => $verify_state, 'verify_desc' => $verify_desc),array('gs_id' => $gs_id));   
            if ($result) {
                showMessage('操作成功', 'index.php?act=promotion_guess&op=guess_verify_list', 'html');
            } else {
                showMessage('操作失败，请重试', '', '', 'error');
            }
         }
        $guess_info = $model_guess->getGuessInfo(array('gs_id' => intval($_GET['guess_id'])));
        Tpl::output('guess_info', $guess_info);
        $g_goods_list = $model_guess->getGuessGoodsList(array('gs_id' => intval($_GET['guess_id'])));
        if (!empty($g_goods_list)) {
            $goodsid_array = array();
            foreach ($g_goods_list as $val) {
                $goodsid_array[] = $val['goods_id'];
            }
            $goods_list = Model('goods')->getGoodsList(array('goods_id' => array('in', $goodsid_array)), 'goods_id,goods_price,goods_image,goods_name');
            Tpl::output('goods_list', array_under_reset($goods_list, 'goods_id'));
        }
        Tpl::output('g_goods_list', $g_goods_list);
        Tpl::showpage('verify_detail');
    }

    public function guess_common_bar_saveOp() {
        $model_setting = Model('setting');
        $list_setting = $model_setting->getListSetting();
        if ($list_setting['guess_advpic'] != ''){
            $list = unserialize($list_setting['guess_advpic']);
        }
        Tpl::output('list', $list);
        $input = array();
        if (chksubmit()){
            //上传图片
            $update_array = array();
            foreach($_FILES as $k=>$v){
                $newk = substr($k,8,strrpos($k,'_'));
                if(!empty($v['name'])) {
                    $upload = new UploadFile();
                    $upload->set('default_dir',ATTACH_AUCTION);
                    $result = $upload->upfile($k);
                    if(!$result) {
                        showMessage($upload->error);
                    }
                    $input[$newk]['pic'] = $upload->file_name;
                }else{
                    if($list[$newk]){
                        $input[$newk]['pic'] = $list[$newk]['pic'];
                    }else{
                        $input[$newk]['pic'] = '';
                    }


                }
                $input[$newk]['url'] = $_POST['adv_url_'.$newk];
            }

            if (count($input) > 0){
                $update_array['guess_advpic'] = serialize($input);
            }else{
                $update_array['guess_advpic'] = '';
            }

            $result = $model_setting->updateSetting($update_array);
            if ($result === true){
                showMessage('设置广告成功');
            }else {
                showMessage('设置广告失败');
            }
        }

        Tpl::showpage('adv_list');
    }
}
