<?php
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
/**
 * 尊享专区轮播图
 * Created by Test, 2018/10/16 15:24.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */
class slideshowControl extends SystemControl
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexOp()
    {
        $this->listOp();
    }

    public function listOp()
    {
        Tpl::showpage('slideshow.list');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_slideshow = Model('slideshow');
        // 设置页码参数名称
        $condition = array();
        $condition['is_delete'] = 0;

        $page = $_POST['rp'];
        $order = "id ASC";
        $slideshow_list = $model_slideshow->getList($condition, $page, $order);

        $data = array();
        $data['now_page'] = $model_slideshow->shownowpage();
        $data['total_num'] = $model_slideshow->gettotalnum();
        foreach ($slideshow_list as $value) {
            $param = array();
            $operation = "<a class='btn blue' href='index.php?act=slideshow&op=edit&id=".$value['id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $operation .= "<a class='btn red' href=\"javascript:void(0);\" onclick=\"fg_delete('".$value['id']."');\"><i class='fa fa-trash-o'></i>删除</a>";

            $param['operation'] = $operation;
            $param['id'] = $value['id'];
            $param['title'] = $value['title'];
            $param['state'] = $value['state']?'生效':'未生效';
            $data['list'][$value['id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function addOp()
    {
        if (chksubmit())
        {
            $title = $_POST['slideshow_title'];
            $url = $_POST['url'];
            $sort = $_POST['sort'];
            $param = array(
                    'title' => $title,
                    'url' => $url,
                    'sort' => $sort !=''?$sort:99,
                );
            if (!empty($_FILES['slideshow_img']['name'])) {//上传广告图片
                $upload = new UploadFile();
                $upload->set('default_dir',ATTACH_SLIDESHOW);
                $upload->upfile('slideshow_img');
                $param['image'] = $upload->file_name;
            }
            try
            {
                $re = model('slideshow')->addInfo($param);
            }
            catch (Exception $ex)
            {
                showMessage('新增失败', '', 'html', 'error');
            }
            showMessage('操作成功', urlAdminShop('slideshow', 'list'));
            return;
        }

        Tpl::showpage('slideshow.add');
    }

    public function delOp() {
        $id = intval($_GET['id']);
        if ($id <= 0) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        $model_slideshow = Model('slideshow');

        $re = $model_slideshow->updateInfo(['is_delete'=>1],['id'=>$id]);
        if ($re != 1) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
    }

    public function editOp()
    {
        $slideshowModel = model('slideshow');
        $slideshow_info = $slideshowModel->getInfo(['id'=>$_GET['id'],'is_delete'=>0]);

        if (!$slideshow_info) {
            showMessage('轮播图不存在', '', 'html', 'error');
        }

        if (chksubmit()) {

            $saveArray = array();
            $saveArray['title'] = $_POST['slideshow_title'];
            $saveArray['url'] = $_POST['url'];
            $saveArray['sort'] = $_POST['sort'] ;
            $saveArray['state'] = $_POST['state'];
            if (!empty($_FILES['slideshow_img']['name'])) {//上传广告图片
                $upload = new UploadFile();
                $upload->set('default_dir',ATTACH_SLIDESHOW);
                $upload->upfile('slideshow_img');
                $saveArray['image'] = $upload->file_name;
            }
            $re = $slideshowModel->updateInfo($saveArray, array(
                'id' => $_GET['id'],
            ));
            if($re !=1){
                showMessage('操作失败', urlAdminShop('slideshow', 'list'));
            }


            showMessage('操作成功', urlAdminShop('slideshow', 'list'));
        }

        Tpl::output('slideshow_array', $slideshow_info);
        Tpl::showpage('slideshow.edit');
    }
}