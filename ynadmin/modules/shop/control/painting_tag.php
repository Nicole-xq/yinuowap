<?php
/**
 * 书画-标签
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class painting_tagControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('paint_tag.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_paint_tag = Model('painting_tag');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('paint_tag_id', 'paint_tag_name', 'paint_tag_class', 'class_id', 'paint_tag_sort');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        // 标签列表
        $paint_tag_list = $model_paint_tag->getpainting_tagList($condition, '*', $page, $order);


        $data = array();
        $data['now_page'] = $model_paint_tag->shownowpage();
        $data['total_num'] = $model_paint_tag->gettotalnum();
        foreach ($paint_tag_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick=\"fg_del(".$value['paint_tag_id'].")\"><i class='fa fa-trash-o'></i>删除</a>";
            $operation .= "<a class='btn blue' href='index.php?act=painting_tag&op=tag_edit&paint_tag_id=" . $value['paint_tag_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] = $operation;
            $param['paint_tag_id'] = $value['paint_tag_id'];
            $param['paint_tag_name'] = $value['paint_tag_name'];
            $param['paint_tag_sort'] = $value['paint_tag_sort'];
            $data['list'][$value['paint_tag_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }


    /**
     * 增加标签
     */
    public function tag_addOp(){
        $model_paint_tag = Model('painting_tag');
        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["paint_tag_name"], "require"=>"true", "message"=>'标签不能为空'),
                array("input"=>$_POST["paint_tag_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序仅可以为数字'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['paint_tag_name'] = trim($_POST['paint_tag_name']);
                $insert_array['class_id']   = $_POST['class_id'];
                $insert_array['paint_tag_sort'] = intval($_POST['paint_tag_sort']);
                $insert_array['paint_tag_class'] = trim($_POST['paint_tag_class']);

                $result = $model_paint_tag->addpainting_tag($insert_array);
                if ($result){
                    $url = array(
                        array(
                            'url'=>'index.php?act=painting_tag&op=tag_add',
                            'msg'=>'继续新增标签',
                        ),
                        array(
                            'url'=>'index.php?act=painting_tag&op=index',
                            'msg'=>'返回标签列表',
                        )
                    );
                    showMessage('新增标签成功',$url);
                }else {
                    showMessage('新增标签失败');
                }
            }
        }
        // 一级商品分类
        $gc_list = Model('goods_class')->getGoodsClassListByParentId(0);
        Tpl::output('gc_list', $gc_list);


        Tpl::showpage('paint_tag.add');
    }

    /**
     * 标签编辑
     */
    public function tag_editOp(){
        $model_paint_tag = Model('painting_tag');

        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["paint_tag_name"], "require"=>"true", "message"=>'标签不能为空'),
                array("input"=>$_POST["paint_tag_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序仅可以为数字'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $where = array();
                $where['paint_tag_id'] = intval($_POST['paint_tag_id']);
                $update_array = array();
                $update_array['paint_tag_name'] = trim($_POST['paint_tag_name']);
                $update_array['paint_tag_sort'] = intval($_POST['paint_tag_sort']);
                $update_array['class_id']   = $_POST['class_id'];
                $update_array['paint_tag_class'] = trim($_POST['paint_tag_class']);
                $result = $model_paint_tag->editpainting_tag($update_array,$where);
                if ($result){
                    $url = array(
                        array(
                            'url'=>'index.php?act=painting_tag&op=tag_edit&paint_tag_id='.intval($_POST['paint_tag_id']),
                            'msg'=>'继续编辑标签',
                        ),
                        array(
                            'url'=>'index.php?act=painting_tag&op=index',
                            'msg'=>'返回标签列表',
                        )
                    );
                    showMessage('编辑标签成功',$url);
                }else {
                    showMessage('编辑标签失败');
                }
            }
        }

        $paint_tag_info = $model_paint_tag->getpainting_tagInfo(array('paint_tag_id' => intval($_GET['paint_tag_id'])));
        if (empty($paint_tag_info)){
            showMessage('参数错误');
        }
        Tpl::output('paint_tag_array',$paint_tag_info);

        // 一级商品分类
        $gc_list = Model('goods_class')->getGoodsClassListByParentId(0);
        Tpl::output('gc_list', $gc_list);


        Tpl::showpage('paint_tag.edit');
    }

    /**
     * 删除标签
     */
    public function tag_delOp(){
        $paint_tag_id = intval($_GET['id']);
        if ($paint_tag_id <= 0) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        Model('painting_tag')->delpainting_tag(array('paint_tag_id' => $paint_tag_id));
        exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        $model_paint_tag = Model('painting_tag');
        switch ($_GET['branch']){
            /**
             * 验证标签名称是否有重复
             */
            case 'check_tag_name':
                $condition['paint_tag_name'] = trim($_GET['paint_tag_name']);
                $condition['paint_tag_id'] = array('neq', intval($_GET['id']));
                $result = $model_paint_tag->getpainting_tagList($condition);
                if (empty($result)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }








}