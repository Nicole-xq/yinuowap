<?php
/**
 * 分类管理
 *
 * @author 汪继君 <675429469@qq.com>
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_classControl extends SystemControl{
	/**
     * 分类管理
     */
    public function indexOp(){
        Tpl::showpage('auctions_class.index');
    }
    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = $_POST['query'];
        }
        $order = '';
        $param = array('auctions_class_id', 'auctions_class_fid');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $condition['order'] = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page   = new Page();
        $page->setEachNum($_POST['rp']);
        $page->setStyle('admin');
        //列表
        $class_list = Model('auctions_class')->getList($condition, $page);
        $data = array();
        $data['now_page'] = $page->get('now_page');
        $data['total_num'] = $page->get('total_num');
        foreach ((array)$class_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick='fg_del(". $value['auctions_class_id'] .")'><i class='fa fa-trash-o'></i>删除</a><a class='btn blue' href='index.php?act=auctions_class&op=auctions_class_edit&c_id=".$value['auctions_class_id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] = $operation;
            $param['auctions_class_id'] = $value['auctions_class_id'];
            $param['auctions_class_name'] = $value['auctions_class_name'];            
            $param['auctions_class_fid'] = $value['auctions_class_fid'];
            if ($value['auctions_class_fid']==0) {
            	$param['auctions_class_father_name']="-----";
            }else{
            	$father_class=Model('auctions_class')->getOneById($value['auctions_class_fid']);
            	$param['auctions_class_father_name']=$father_class['auctions_class_name']; 
            }
            $data['list'][$value['auctions_class_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function auctions_class_addOp(){
    	$class_type = Model('auctions_class');

        if (chksubmit()){
        	$class_array['auctions_class_fid']     = $_POST['auctions_class_fid'];
            $class_array['auctions_class_name']   = $_POST['auctions_class_name'];
            $class_id=$class_type->add($class_array);
            if ($class_id) {
            	$this->log('添加分类'.'['.$_POST['auctions_class_name'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_class&op=index');
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }
        // 一级分类列表
        $condition['auctions_class_fid']=0;
        $gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select();;
        //print_r($gc_list);exit;
        Tpl::output('gc_list', $gc_list);

        Tpl::showpage('auctions_class.add');
           
    }
    public function auctions_class_editOp(){
    	$class_type = Model('auctions_class');
    	$class=Model('auctions_class')->getOneById($_GET['c_id']);
    	Tpl::output('class',$class);
        if (chksubmit()){

        	$class_array['auctions_class_fid']     = $_POST['auctions_class_fid'];
            $class_array['auctions_class_name']   = $_POST['auctions_class_name'];
            $class_id=$class_type->updates($class_array,$_POST['auctions_class_id']);
            if ($class_id) {
            	$this->log('更新分类'.'[ID:'.$_POST['auctions_class_id'].']',null);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_class&op=index');
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }
        // 一级分类列表
        $condition['auctions_class_fid']=0;
        $gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select();
        //print_r($gc_list);exit;
        Tpl::output('gc_list', $gc_list);

        Tpl::showpage('auctions_class.edit');
    }
    public function auctions_class_delOp(){
    	$id=$_GET['id'];
    	if(empty($id)) {
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
        //echo $id;exit;
        $class_type = Model('auctions_class');
        $class=Model('auctions_class')->getOneById($id);
        //为上级分类时删除全部下级分类
        if ($class['auctions_class_fid']==0) {
        	$list=Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>$id))->delete();
        }
        $return = $class_type->del($id);

        if ($return) {
        	$this->log('删除分类'.'[ID:'.$id.']',1);
            exit(json_encode(array('state'=>true,'msg'=>L('type_index_del_succ'))));
        }else{
            $this->log(L('nc_delete,type_index_type_name').'[ID:'.$id.']',0);
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
    }
}