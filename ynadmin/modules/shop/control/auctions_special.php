<?php
/**
 * 专场管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_specialControl extends SystemControl{

    private $param = null;
    private $special_info = null;
    private $_links = array(
        array('url'=>'act=auctions_special&op=index','text'=>'平台专场列表'),
        array('url'=>'act=auctions_special&op=add','text'=>'新增专场')
    );

    public function __construct(){
        parent::__construct();
        Language::read('store,store_grade');
    }

    public function indexOp() {
        //输出子菜单
        Tpl::output('top_link',$this->sublink($this->_links,'index'));

        Tpl::showpage('auctions_special.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
       $model_auction_special = Model('auction_special');
        // 设置页码参数名称
        $condition = array();
        $condition['special_sponsor'] = 0;
        $condition['store_id'] = 0;
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('special_id','store_id','store_name','special_name','special_image',
            'special_state','special_start_time','special_end_time','special_preview_start',
            'special_add_time','is_open', 'special_type');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //专场列表
        $special_list = $model_auction_special->getSpecialList($condition,'', $order,$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();
        foreach ($special_list as $value) {
            $param = array();
            $o = '<a class=\'btn red\' target="_blank" href="' .
                getMainConfig('auction_url') . "/profitDetails?special_id=" . $value['special_id'] . '">预览</a>';
            if ($value['is_rec'] == '1') {
                $o .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions_special', 'special_rec', array(
                            'special_id' => $value['special_id'],
                            'rec' => 0,
                        )) . '">取消推荐</a>';
            } else {
                $o .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions_special', 'special_rec', array(
                            'special_id' => $value['special_id'],
                            'rec' => 1,
                        )) . '">推荐专场</a>';
            }
            $o .= '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            $o .= '<li><a href="index.php?act=auctions_special&op=edit&special_id=' .
                $value['special_id'] .
                '">编辑专场</a></li>';
            $o .= '<li><a href="index.php?act=auctions_special&op=special_copy&special_id=' .
                $value['special_id'] .
                '">复制专场</a></li>';
            if ($value['special_end_time']==0) {
                $o .= '<li><a href="index.php?act=auctions_special&op=goods&special_id=' .
                $value['special_id'] .
                '">添加拍品</a></li>';
            }
            $o .= '</ul></span>';

            $param['operation'] = $o;
            $param['special_id'] = $value['special_id'];
            $param['special_name'] = $value['special_name'];
            $param['special_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getVendueLogo($value['special_image']).">\")'><i class='fa fa-picture-o'></i></a>";
            $param['special_type_text'] = $this->disposeSpecialType($value['special_type']);
            $param['special_start_time'] = $value['special_start_time'] ? date('Y-m-d H:i:s', $value['special_start_time']) : '未设置';
            $param['special_end_time'] = $value['special_end_time'] ? date('Y-m-d H:i:s', $value['special_end_time']) : '未设置';
            $param['special_preview_start'] = $value['special_preview_start'] ? date('Y-m-d H:i:s', $value['special_preview_start']) : '未设置';
            $param['special_add_time'] = $value['special_add_time'] ? date('Y-m-d H:i:s', $value['special_add_time']) : '未设置';
            $param['special_type_text'] = \App\Models\ShopncAuctionSpecial::$specialTypeText[intval($value['special_type'])];
            $param['is_open'] = $value['is_open'] == 1 ? '<span class="yes"><i class="fa fa-check-circle"></i>开启</span>' : '<span class="no"><i class="fa fa-ban"></i>关闭</span>';
            $data['list'][$value['special_id']] = $param;
        }
        echo Tpl::flexigridXML($data);
    }

    public function special_copyOp()
    {
        $special_id = $_GET['special_id'];
        $condition['special_id'] = $special_id;
        $info = Model("")->table("auction_special")->where($condition)->find();
        if ($info) {
            $data = $info;
            unset($data['special_id'], $data['auction_num'], $data['special_click'], $data['participants']);
            //其他数据填写
            $data['special_add_time'] = 0;
            $data['special_start_time'] = 0;
            $data['special_end_time'] = 0;
            $data['special_preview_start'] = 0;
            $data['special_state'] = 20;
            $data['special_check_message'] = '';
            $data['is_open'] = 0;
            $data['special_rate_time'] = 0;
            $id = Model("")->table('auction_special')->insert($data);
            if ($id) {
                showMessage('添加平台专场成功', 'index.php?act=auctions_special&op=index', 'html', 'succ', 1, 0);
            } else {
                showMessage(Language::get('nc_common_op_fail'));
            }
        } else {
            showMessage(Language::get('nc_common_op_fail'));
        }
    }

    public function disposeSpecialType($specialType)
    {
        //拍品类型: 0,普通拍品,1,新手拍品 2.新手专区
        switch ($specialType) {
            case '1':
                $typeName = '新手拍品';
                break;
            case '2':
                $typeName = '新手专区';
                break;
            default:
                $typeName = '普通拍品';
                break;
        }
        return $typeName;
    }
    
    public function goodsOp(){
        $special_id=$_GET['special_id'];
        $condition['store_id']=0;
        $condition['special_id']=$special_id;
        $list=Model("")->table("auctions")->where($condition)->select();
        $admin_info = $this->admin_info;
        Tpl::output('admin_info', $admin_info);
        Tpl::output('list',$list);
        //print_r($list);
        Tpl::showpage('auctions_special.goods');
    }
    public function get_add_goodsOp(){
        $admin = false;
        $keyword=$_GET['keyword'];
        $condition['store_id']=0;
        $condition['special_id']=0;
        $condition['auction_name|auctions_artist_name'] = array(array('like','%'.$keyword.'%'));
        $list=Model("")->table("auctions")->where($condition)->limit(100)->select();
        $html="<table>";
        $html.="<tr><td>拍品名称</td><td>起拍价</td><td>加价幅度</td>";
        $html.="<td>添加时间</td><td>库位号</td>";
        if ($this->admin_info['gid'] == 1) {
            $admin = true;
            $html.="<td>保留价</td>";
        }
        $html.="<td>拍品保证金</td><td>拍品类型</td><td>操作</td></tr>";
        if ($list) {
            foreach ($list as $key => $value) {
                $html.="<tr>";
                $html.="<td>".$value['auction_name']."</td>";
                $html.="<td>".$value['auction_start_price']."</td>";
                $html.="<td>".$value['auction_increase_range']."</td>";
                $html.="<td>".date('Y-m-d H:i:s', $value['auction_add_time'])."</td>";
                $html.="<td>".$value['warehouse_location']."</td>";
                if ($admin) {
                    $html.="<td>".$value['auction_reserve_price']."</td>";
                }
                $html.="<td>".$value['auction_bond']."</td>";
                if ($value['auction_type'] == 0) {
                    $auctionType = '普通拍品';
                } elseif ($value['auction_type'] == 1) {
                    $auctionType = '新手拍品';
                } else {
                    $auctionType = '新手专区';
                }
                $html.="<td>".$auctionType."</td>";
                $html.="<td><a href='JavaScript:void(0);'' class='ncap-btn-big ncap-btn-green' onclick='bind_goods(".$value['auction_id'].")'><span>绑定</span></a></td>";
                $html.="</tr>";
            }
        }
        $html.="<table>";
        echo $html;
        //Tpl::output('list',$list);
        //Tpl::showpage('auctions_special.get_add_goods');
    }
    public function add_good_to_specialOp(){
        $auction_id=$_GET['id'];
        $condition['auction_id']=$auction_id;
        $condition['store_id']=0;
        $special_id=$_GET['special_id'];
        $info = Model("")->table("auctions")->where($condition)->find();
        $model_auction_special = Model('auction_special');
        $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
        if ($info && $auction_special_info) {
            //$data['auction_id']=$info['auction_id'];
            $data['special_id']=$auction_special_info['special_id'];
            $data['auction_preview_start']=$auction_special_info['special_preview_start'];
            $data['auction_start_time']=$auction_special_info['special_start_time'];
            $data['interest_last_date']=date('Y-m-d H:i:s',$auction_special_info['special_rate_time']);
            $data['auction_bond_rate']=$auction_special_info['special_rate'];
            //查询拍卖延长时间
            $model_auction_special_rate_check = Model('auction_config');
            $config_info = $model_auction_special_rate_check->getInfo(1);
            $duration_time=$config_info['duration_time']*60;
            $data['auction_end_time']=$data['auction_start_time']+$duration_time;
            $data['auction_end_true_t']=$data['auction_end_time'];
            //print_r($data);
            $id = Model("")->table("auctions")->where(array('auction_id'=>$info['auction_id']))->update($data);
            //echo $id;die();
            if ($id !== false) {
                $sdata['special_id']=$auction_special_info['special_id'];
                $sdata['auction_num']=$auction_special_info['auction_num']+1;
                Model("")->table("auction_special")->update($sdata);
                $html="<tr>";
                $html.="<td>".$info['auction_name']."</td>";
                $html.="<td>".$info['auction_start_price']."</td>";
                $html.="<td>".$info['auction_increase_range']."</td>";
                $html.="<td>".date('Y-m-d H:i:s', $info['auction_add_time'])."</td>";
                $html.="<td>".$info['warehouse_location']."</td>";
                if ($this->admin_info['gid'] == 1) {
                    $html .= "<td>" . $info['auction_reserve_price'] . "</td>";
                }
                $html.="<td>".$info['auction_bond']."</td>";
                if ($info['auction_type'] == 0) {
                    $type = '普通拍品';
                } elseif ($info['auction_type'] == 1) {
                    $type = '新手拍品';
                } else {
                    $type = '新手专区';
                }
                $html.="<td>".$type."</td>";
                $html.="<td onclick=\"remove_goods(".$info['auction_id'].")\">移除</td>";
                $html.="</tr>";
                echo $html;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    public function remove_good_to_specialOp(){
        $auction_id=$_GET['id'];
        $condition['auction_id']=$auction_id;
        $condition['store_id']=0;
        $special_id=$_GET['special_id'];
        $info = Model("")->table("auctions")->where($condition)->find();
        $model_auction_special = Model('auction_special');
        $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
        if ($info && $auction_special_info) {
            $where = [
                'auction_id' => $auction_id,
                'order_state' => 1
            ];
            $marginAmount = Model("")
                ->table("margin_orders")
                ->where($where)
                ->sum('margin_amount');
            if ($marginAmount > 0) {
                //该拍品已经有人缴纳保证金不能移除
                echo 1;
                die;
            }
            //其他数据填写
            $auction_data['goods_id']=0;
            $auction_data['goods_common_id']=0;
            $auction_data['store_id']=0;
            $auction_data['auction_add_time']=time();
            $auction_data['auction_start_time']=0;
            $auction_data['auction_end_time']=0;
            $auction_data['special_id']='';
            $auction_data['auction_preview_start']=0;
            $auction_data['auction_bond_rate']=0;
            $auction_data['auction_end_true_t']=0;
            $auction_data['state']=0;
            $auction_data['is_liupai']=0;
            $auction_data['current_price']=0.00;
            $auction_data['bid_number']=0;
            $auction_data['num_of_applicants']=0;
            $auction_data['real_participants']=0;
            $auction_data['set_reminders_num']=0;
            $auction_data['auction_collect']=0;
            $auction_data['current_price_time']=null;
            $id = Model("")->table("auctions")->where(array('auction_id'=>$auction_id))->update($auction_data);
            if ($id !== false) {
                echo 'success';
                die;
            } else {
                echo 0;
                die;
            }
        } else {
            echo 0;
            die;
        }
    }

    private function del_img(){
        //判断有没有之前的图片，如果有则删除
        if (!empty($this->special_info['special_image']) && !empty($this->param['special_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['special_image']);
        }
        if (!empty($this->special_info['adv_image']) && !empty($this->param['adv_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['adv_image']);
        }
        if (!empty($this->special_info['wap_image']) && !empty($this->param['wap_image'])){
            @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$this->special_info['wap_image']);
        }
    }



    //新增平台专场
    public function addOp(){
        //新建处理
        if($_POST['form_submit'] != 'ok'){
            Tpl::showpage('auctions_special.add');
            exit;
        }
        //提交表单
        if (!empty($_FILES['special_image']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $result = $upload->upfile('special_image');
            if ($result){
                $_POST['special_image'] = $upload->file_name;
            }else {
                showMessage($upload->error);
            }
        }
        if (!empty($_FILES['adv_image']['name'])){
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('adv_image');
            if ($result){
                $_POST['adv_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        //保存
        $model_auction_special = Model('auction_special');
        $data = array();
        $data['special_name'] = $_POST['special_name'];
        $data['special_add_time'] = TIMESTAMP;
        $data['special_start_time'] = strtotime($_POST['special_start_time']);
        $data['special_preview_start'] = strtotime($_POST['special_preview_start']);
        $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : '';
        $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : '';
        $data['wap_image'] =$data['special_image'];
        $data['special_type'] =$_POST['special_type'];
        $data['auction_num'] = 1;
        $data['special_sponsor'] = 0;
        $data['is_pd_pay']=$_POST['is_pd_pay'];
        if ($_POST['is_pd_pay']==1) {
            $_POST['special_rate']=0;
        }

        if ($_POST['special_rate']>0) {
            $data['special_rate_time']= strtotime($_POST['special_rate_time']);
            $data['special_rate']=$_POST['special_rate'];
        }
        $data['store_id']=0;
        $data['special_state'] = 20;
        $data['is_open'] = $_POST['is_open'];


        $rate_arr = Model('special_rate_config')->getList();
        $tmp = array();
        if(!empty($rate_arr)){
            foreach($rate_arr as $v){
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['rate'] = $v['rate'];
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['max_rate'] = $v['max_rate'];
            }
        }
        //返佣比例
        if($_POST['special_rate'] > 0){
            $data['partner_lv_1_wdl_rate'] = $tmp['partner_1_wdl']['rate'];
            $data['partner_lv_1_zl_rate'] = $tmp['partner_1_zl']['rate'];
            $data['partner_lv_1_sy_rate'] = $tmp['partner_1_sy']['rate'];
            $data['partner_lv_2_wdl_rate'] = $tmp['partner_2_wdl']['rate'];
            $data['partner_lv_2_zl_rate'] = $tmp['partner_2_zl']['rate'];
            $data['partner_lv_2_sy_rate'] = $tmp['partner_2_sy']['rate'];
        }
        $data['share_content']=$_POST['share_content'];
        $return = $model_auction_special->addSpecial($data, true);

        if($return){
            showMessage('添加平台专场成功','index.php?act=auctions_special&op=index');
        }else{
            showMessage('添加平台专场失败');
        }
    }
    public function testOp(){
        $rate_arr = Model('special_rate_config')->getList();
        $tmp = array();
        if(!empty($rate_arr)){
            foreach($rate_arr as $v){
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['rate'] = $v['rate'];
                $tmp['partner_'.$v['partner_lv'].'_'.$v['partner_lv_type']]['max_rate'] = $v['max_rate'];
            }
        }
        print_r($tmp);
    }

    /**
     * 删除平台专场
     */
    public function delOp()
    {
        $specialIds = array();
        foreach (explode(',', (string) $_REQUEST['special_id']) as $i) {
            $specialIds[(int) $i] = null;
        }
        unset($specialIds[0]);
        $specialIds = array_keys($specialIds);

        if (empty($specialIds)) {
            $this->jsonOutput('请选择专场');
        }

        try{
            // 删除数据先删除广告图片以及专场缩略图，节省空间资源
            foreach ($specialIds as $v) {
                $this->delBanner($v);
            }
        } catch (Exception $e) {
            $this->jsonOutput($e->getMessage());
        }

        $id = implode(",", $specialIds);

        $model_auction_special = Model('auction_special');
        //获取可以删除的数据
        $condition =  " (is_open = 0 or special_end_time < '".(TIMESTAMP)."') and special_id in (". $id .") and special_sponsor = 0" ;
        $special_list = $model_auction_special->getSpecialList($condition);
        if (empty($special_list)){//没有符合条件的活动信息直接返回成功信息
            $this->jsonOutput();
        }
        $id_arr = array();
        foreach ($special_list as $v){
            $id_arr[] = $v['special_id'];
        }
        $id_new = implode(",", $id_arr);

        //只有关闭或者过期的活动，能删除
        if($model_auction_special->delSpecialGoods(array('special_id'=>array('in',$id_new)))){
            if($model_auction_special->delSpecialForAdmin(array('special_id'=>array('in',$id_new)))){
                $this->jsonOutput();
            }
        }

        $this->jsonOutput('操作失败');
    }

    /**
     * 编辑活动/保存编辑活动
     */
    public function editOp(){
        if($_POST['form_submit'] != 'ok'){
            if(empty($_GET['special_id'])){
                showMessage(Language::get('miss_argument'));
            }
            $model_auction_special = Model('auction_special');
            $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
            if ($auction_special_info['is_pd_pay']==1) {
                $auction_special_info['is_pd_pay']="诺币专场";
            }else{
                $auction_special_info['is_pd_pay']="普通专场";
            }

            $auction_special_info['special_type_text'] = \App\Models\ShopncAuctionSpecial::$specialTypeText[intval($auction_special_info['special_type'])];
            Tpl::output('auction_special_info',$auction_special_info);
            Tpl::showpage('auctions_special.edit');
            exit;
        }


        if (!empty($_FILES['special_image']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $result = $upload->upfile('special_image');
            if ($result){
                $_POST['special_image'] = $upload->file_name;
            }else {
                showMessage($upload->error);
            }
        }
        if (!empty($_FILES['adv_image']['name'])){
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('adv_image');
            if ($result){
                $_POST['adv_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        //保存
        /** @var auction_specialModel $model_auction_special */
        $model_auction_special = Model('auction_special');
        $data = array();
        $data['special_name'] = $_POST['special_name'];
        //$data['special_add_time'] = TIMESTAMP;
        $data['special_start_time'] = strtotime($_POST['special_start_time']);
        $data['special_end_time'] = 0;
        $data['special_preview_start'] = strtotime($_POST['special_preview_start']);
        $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_GET['special_id']));
        if ($auction_special_info['is_open'] && $_POST['is_open']) {
            showMessage('平台专场已开启，无法更新');
        }
        $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : $auction_special_info['special_image'];
        $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : $auction_special_info['adv_image'];
        $data['wap_image'] =$data['special_image'];
//        $data['delivery_mechanism'] = $_POST['delivery_mechanism'];
        $data['is_open'] = $_POST['is_open'];
        $data['share_content']=$_POST['share_content'];
        if ($_POST['special_rate']>0) {
            $data['special_rate_time']= strtotime($_POST['special_rate_time']);
            $data['special_rate']=$_POST['special_rate'];
        }
        //print_r($data);die();
        $return = $model_auction_special->editSpecial($data, array('special_id'=>intval($_POST['special_id'])));
        if($return !== false){
            //专场关闭, 同时关闭该专场下的拍品
            if ($data['is_open'] == 0) {
                $currentTime = time();
                $auction_data['auction_end_time'] = $auction_data['auction_end_true_t'] = $currentTime;
                //$auctionList = Model("")->table("auctions")->where(array('special_id'=>$auction_special_info['special_id']))->select();
                //TODO 待确定之后缴纳保证金可以进行删除之后删除代码
//                if ($auctionList && is_array($auctionList)) {
//                    $auctionIds = [];
//                    foreach ($auctionList as $auction) {
//                        $where = [
//                            'auction_id' => $auction['auction_id'],
//                            'order_state' => 1
//                        ];
//                        $marginAmount = Model("")
//                            ->table("margin_orders")
//                            ->where($where)
//                            ->sum('margin_amount');
//                        if ($marginAmount > 0) {
//                            $auctionIds[] = $auction['auction_id'];
//                        }
//                    }
//                    if (!empty($auctionIds)) {
//                        showMessage(implode(",",$auctionIds) . '拍品已经缴纳保证金','index.php?act=auctions_special&op=index');
//                    }
//                }
            } else {
                $auction_data['auction_preview_start'] = $data['special_preview_start'];
                $auction_data['auction_start_time'] = $data['special_start_time'];
                //查询拍卖延长时间
                $model_auction_special_rate_check = Model('auction_config');
                $config_info = $model_auction_special_rate_check->getInfo(1);
                $duration_time = $config_info['duration_time'] * 60;
                $auction_data['auction_end_time'] = $data['special_start_time'] + $duration_time;
                $auction_data['auction_end_true_t'] = $data['special_start_time'] + $duration_time;
                if (isset($data['special_rate_time'])) {
                    $auction_data['interest_last_date'] = date('Y-m-d H:i:s', $data['special_rate_time']);
                    $auction_data['auction_bond_rate'] = $data['special_rate'];
                }
            }
            //print_r($auction_data);
            $id = Model("")->table("auctions")->where(array('special_id'=>$auction_special_info['special_id']))->update($auction_data);
            if (!empty($auction_special_info['special_image']) && !empty($_POST['special_image'])){
                @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$auction_special_info['special_image']);
            }
            if (!empty($auction_special_info['adv_image']) && !empty($_POST['adv_image'])){
                @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$auction_special_info['adv_image']);
            }

            showMessage('编辑平台专场成功','index.php?act=auctions_special&op=index');
        }else{
            showMessage('编辑平台专场失败');
        }
    }

    /**
     * 专场细节列表
     */
    public function detailOp()
    {
        $states = array(
            '待审核',
            '已通过',
            '已拒绝',
        );
        Tpl::output('states', $states);
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$_REQUEST['id']));

        Tpl::output('special_info', $special_info);

        Tpl::showpage('special_detail.index');
    }

    /**
     * 专场细节列表XML
     */
    public function detail_xmlOp()
    {
        $condition = array();

        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_id','store_name','special_id','auction_id','auction_name','auction_image','auction_start_price','current_price','bid_number');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $condition['special_id'] = $_GET['id'];
        $page = $_POST['rp'];
        $model_auction_special = Model('auction_special');
        //专场拍品列表
        $list = $model_auction_special->getSpecialGoodsList($condition,'', $order,'','',$page);

        $data = array();
        $data['now_page'] = $model_auction_special->shownowpage();
        $data['total_num'] = $model_auction_special->gettotalnum();

        $states = array(
            '待审核',
            '已通过',
            '已拒绝',
        );
        foreach ($list as $val) {
            $o = '<a class="btn green" href="' .
                urlVendue('auctions', 'index', array('id' => $val['auction_id'])) .
                '"><i class="fa fa-list-alt"></i>查看</a>';

            $o .= '<span class="btn"><em><i class="fa fa-cog"></i>设置<i class="arrow"></i></em><ul>';

            if ($val['auction_sp_state'] != 1) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="index.php?act=auction_special&op=deal&state=1&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">通过</a></li>';
            }

            if ($val['auction_sp_state'] != 2) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="index.php?act=auction_special&op=deal&state=2&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">拒绝</a></li>';
            }

            if ($val['auction_sp_state'] != 1) {
                $o .= '<li><a class="confirm-on-click" href="javascript:;" data-href="' .
                    'index.php?act=auction_special&op=del_detail&special_auction_id=' .
                    $val['special_auction_id'] .
                    '">删除</a></li>';
            }

            $o .= '</ul></span>';

            $i = array();
            $i['operation'] = $o;

            $i['auction_name'] = $val['auction_name'];

            $i['store_name'] = '<a target="_blank" href="' .
                urlShop('show_store', 'index', array('store_id' => $val['store_id'])) .
                '">' .
                $val['store_name'] .
                '</a>';

            $i['auction_sp_state'] = $states[(int) $val['auction_sp_state']];

            $data['list'][$val['special_auction_id']] = $i;
        }

        echo Tpl::flexigridXML($data);
        exit;
    }

    /**
     * 专场内容处理
     */
    public function dealOp()
    {
        $ids = array();
        foreach (explode(',', (string) $_REQUEST['special_auction_id']) as $i) {
            $ids[(int) $i] = null;
        }
        unset($ids[0]);
        $ids = array_keys($ids);

        if (empty($ids)) {
            showMessage('请选择拍品');
        }

        // 获取id
        $id = implode(',', $ids);

        //创建活动内容对象
        $model_auction_special = Model('auction_special');
        if($model_auction_special->updatesGoods(array('auction_sp_state'=>intval($_GET['state'])),$id)){
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }

    }

    /**
     * 删除专场内容
     */
    public function del_detailOp()
    {
        $ids = array();
        foreach (explode(',', (string) $_REQUEST['special_auction_id']) as $i) {
            $ids[(int) $i] = null;
        }
        unset($ids[0]);
        $ids = array_keys($ids);

        if (empty($ids)) {
            showMessage('请选择拍品');
        }

        // 获取id
        $id = implode(',', $ids);

        $model_auction_special = Model('auction_special');
        //条件
        $condition_arr = array();
        $condition_arr['special_auction_id_in'] = $id;
        $condition_arr['auction_sp_state_in'] = "'0','2'";//未审核和已拒绝
        if($model_auction_special->delGoodsList($condition_arr)){
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }

    /**
     * 推荐
     */
    public function special_recOp()
    {
        $model_special= Model('auction_special');
        $update_array['is_rec'] = $_GET['rec'] == '1' ? 1 : 0;
        $where_array['special_id'] = $_GET['special_id'];
        $result = $model_special->editSpecial($update_array, $where_array);
//        echo '<pre>';print_r($result);exit;
        if ($result) {
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }


    /**
     * 根据专场编号删除广告图片以及专场缩略图
     *
     * @param int $id
     */
    private function delBanner($id){
        $model_auction_special = Model('auction_special');
        $row    = $model_auction_special->getSpecialInfo(array('special_id'=>$id));
        //删除图片文件
        @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$row['special_image']);
        @unlink(BASE_UPLOAD_PATH.DS.ATTACH_VENDUE.DS.$row['adv_image']);
    }

    public function get_special_infoOp(){
        $special_id = $_GET['id'];
        if (chksubmit()) {
            $auction_bond_rate = $_POST['auction_bond_rate'];
            $special_id = intval($_POST['special_id']);
            $rate_time = $_POST['auction_rate_time'];
            $model_auction_special = Model('auction_special');
            //专场拍品列表
            $condition = array(
                'special_id'=>$special_id,
            );
            $model_auction_special->editSpecial(array('special_rate'=>$auction_bond_rate,'special_rate_time'=>strtotime($rate_time)),array('special_id'=>$special_id));
            $list = $model_auction_special->getSpecialGoodsList($condition,'auction_id', '','','',''  );
            foreach($list as $v){
                $id_list[] = $v['auction_id'];
            }
//            print_R($list);exit;
            $model_auction = Model('auctions');
            $update = array(
                'auction_bond_rate'=>$auction_bond_rate,
                'interest_last_date'=>$rate_time
            );
            $re = $model_auction->editAuctionsById($update,$id_list);
//            print_R($_POST);
//            echo $re;exit;
            //TODO  将专场下所有商品保证金利率刷新
            showDialog(L('nc_common_op_succ'), '', 'succ', '$("#flexigrid").flexReload();CUR_DIALOG.close();');
        }
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
//        print_R($special_info);exit;
        Tpl::output('special_info', $special_info);
        Tpl::showpage('auctions_setting','null_layout');
    }

    /**
     * 拍品状态
     * @param $info
     * @return string
     */
    private function get_auction_state($info){
        if ($info['special_state'] == 11) {
            $re = '预展中';
        } elseif ($info['special_state'] == 12) {
            $re = '拍卖中';
        } elseif ($info['special_state'] == 13) {
            $re = '拍卖结束';
        } elseif ($info['special_state'] == 20) {
            $re = '等待预展';
        } else {
            $re = '----';
        }
        return $re;
    }

    /**
     * 审核通过后,创建真正的拍品
     * @param $id_list
     */
    private function create_auction($id_list,$param){
        if(!empty($id_list)){
            $tmp = array(
                'auction_state'=>2,
                'auction_sum'=>'`auction_sum` + 1'
            );
            Model('goods_auctions')->updateGoodsAuctions($tmp,array('cur_special_id'=>$param['special_id']));
            foreach($id_list as $v){
                $info = Model('goods_auctions')->getGoodsAuctionInfo(array('id'=>$v));
                if(empty($info)){
                    return false;
                }
                $info['delivery_mechanism'] = $info['store_name'];
                $info['state'] = 0;
                $info['auction_lock'] = 1;
                $info['auction_add_time'] = time();
                $info = array_merge($info,$param);
                unset($info['id']);
                unset($info['cur_special_id']);
                unset($info['last_special_id']);
                unset($info['auction_sum']);
                unset($info['auction_state']);
                unset($info['del_flag']);
                $id = Model('auctions')->addAuction($info);
//                dd();
                if(!$id){
                    return false;
                }
            }
        }
        return true;

    }
}
