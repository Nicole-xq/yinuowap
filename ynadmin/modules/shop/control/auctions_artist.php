<?php
/**
 * 分类管理
 *
 * @author 汪继君 <675429469@qq.com>
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_artistControl extends SystemControl{
	/**
     * 分类管理
     */
    public function indexOp(){
        Tpl::showpage('auctions_artist.index');
    }
    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = $_POST['query'];
        }
        $order = '';
        $param = array('auctions_artist_id');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $condition['order'] = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page   = new Page();
        $page->setEachNum($_POST['rp']);
        $page->setStyle('admin');
        //列表
        $class_list = Model('auctions_artist')->getList($condition, $page);
        //print_r($class_list);die();
        $data = array();
        $data['now_page'] = $page->get('now_page');
        $data['total_num'] = $page->get('total_num');
        foreach ((array)$class_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick='fg_del(". $value['auctions_artist_id'] .")'><i class='fa fa-trash-o'></i>删除</a>";
            $param['operation'] = $operation;
            $param['auctions_artist_id'] = $value['auctions_artist_id'];
            $param['auctions_artist_name'] = $value['auctions_artist_name'];
            if ($value['auctions_artist_img']) {
                $url=UPLOAD_SITE_URL.DS.ATTACH_ARTIST.DS.$value['auctions_artist_img'];
            }else{
                $url="";
            }
            

            $param['auctions_artist_img'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".$url.">\")'><i class='fa fa-picture-o'></i></a>";

            $data['list'][$value['auctions_artist_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function auctions_artist_addOp(){
    	$class_type = Model('auctions_artist');

        if (chksubmit()){

        	$class_array['auctions_artist_name']     = $_POST['auctions_artist_name'];

            if (!empty($_FILES['auctions_artist_img']['name'])) {
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_ARTIST);
                $result = $upload->upfile('auctions_artist_img');
                if ($result){
                    $class_array['auctions_artist_img'] = $upload->file_name;
                }else {
                    showMessage($upload->error);
                }
            }           

            $class_array['auction_artist_summary']   = $_POST['auction_artist_summary'];

            $class_id=$class_type->add($class_array);
            if ($class_id) {
            	$this->log('添加艺术家'.'['.$_POST['auctions_artist_name'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_artist&op=index');
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }
 
        Tpl::showpage('auctions_artist.add');
           
    }

    public function auctions_artist_delOp(){
    	$id=$_GET['id'];
    	if(empty($id)) {
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
        //echo $id;exit;
        $class_type = Model('auctions_artist');
        $return = $class_type->del($id);

        if ($return) {
        	$this->log('删除艺术家'.'[ID:'.$id.']',1);
            exit(json_encode(array('state'=>true,'msg'=>L('type_index_del_succ'))));
        }else{           
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
    }

    public function auctions_artist_listOp(){
        $keyword=$_GET['keyword'];
        $condition['auctions_artist_name']=array(array('like','%'.$keyword.'%'));
        $list = Model("")->table("auctions_artist")->where($condition)->select();
        foreach ((array)$list as $key => $value) {
            if ($value['auctions_artist_img']) {
                $url=UPLOAD_SITE_URL.DS.ATTACH_ARTIST.DS.$value['auctions_artist_img'];
            }else{
                $url="";
            }
            $list[$key]['url']=$url;
        }
        Tpl::output('list', $list);
        Tpl::showpage('auctions_artist.list');
    }
    public function auctions_artist_infoOp(){
        $id=$_GET['id'];
        $condition['auctions_artist_id']=$id;
        $info = Model("")->table("auctions_artist")->where($condition)->find();
        if ($info['auctions_artist_img']) {
            $info['url']=UPLOAD_SITE_URL.DS.ATTACH_ARTIST.DS.$info['auctions_artist_img'];
        }else{
            $info['url']="";
        }
        Tpl::output('info', $info);
        Tpl::showpage('auctions_artist.info');
    }
}