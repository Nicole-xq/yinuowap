<?php
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class auctions_commissionControl extends SystemControl{

    /**
     * 返佣记录列表
     */
    public function indexOp(){
        $commission_model = Model('member_commission');
        $commission_type = $commission_model->getCommissionType();
        Tpl::output('commission_type', $commission_type);
        Tpl::showpage('aunctions_commission.index');
    }

    /**
     * 获取返佣记录列表xml
     */
    public function get_xmlOp(){
        $model_commis = Model('member_commission');

        $condition = array();
        //var_dump($_GET);
        if ($_GET['keyword_type']) {
            switch ($_GET['keyword_type'])
            {
                case 'dis_member_phone':
                    if ($_GET['keyword'] != '') {
                        $memberModel = Model('member');
                        $memberId = $memberModel->getMemberInfo(['member_mobile' => $_GET['keyword']], 'member_id');
                        if (!empty($memberId['member_id'])) {
                            $condition['dis_member_id'] = $memberId['member_id'];
                        }
                    }
                break;
                    case 'from_member_phone':
                        if ($_GET['keyword'] != '') {
                            $memberModel = Model('member');
                            $memberId = $memberModel->getMemberInfo(['member_mobile' => $_GET['keyword']], 'member_id');
                            if (!empty($memberId['member_id'])) {
                                $condition['from_member_id'] = $memberId['member_id'];
                            }
                        }
                break;
                case 'dis_member_name':
                    if ($_GET['keyword'] != '') {
                        $condition['dis_member_name'] = array('like', '%' . $_GET['keyword'] . '%');
                    }
                break;
                case 'from_member_name':
                    if ($_GET['keyword'] != '') {
                        $condition['from_member_name'] = array('like', '%' . $_GET['keyword'] . '%');
                    }
                break;
                default:
                    break;
            }
        }
        $condition['commission_type']=array('in','7,8,9');
        $order = 'log_id desc';
        $sort_fields = array('dis_member_name','order_sn','commission_type','from_member_name','goods_pay_amount','goods_commission','pay_refund','goods_commission_refund','commission_amount','commission_refund','dis_commis_state','commission_time','add_time');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }

        $commis_list = $model_commis->getCommissionList($condition, '*', $_POST['rp'], $order);

        $data = array();
        $data['now_page'] = $model_commis->shownowpage();
        $data['total_num'] = $model_commis->gettotalnum();
        // 数据处理
        $this->_commission_data_list($data,$commis_list);
        // 底部统计
        //$data['list'][] = $this->setFooterValueByType($model_commis->getCommissionAmountByType($condition));
        //print_r($data);
        exit(Tpl::flexigridXML($data));
    }
    /*数据处理*/
    private function _commission_data_list(&$data, $commis_list, $excel = false){
        /** @var member_commissionModel $model_commis */
        $model_commis = Model('member_commission');
        $commission_type = $model_commis->getCommissionType();
        $member_type_display = $this->getMemberType(array_keys(array_under_reset($commis_list,'dis_member_id')));
        foreach ((array)$commis_list as $commis) {
            $list = array();
            $set_value = $this->setValueByType($commis);
            $list['operation'] = $set_value['href'] ? "<a class='btn green' href=".$set_value['href']." class='url'><i class='fa fa-list-alt'></i>查看</a>" : '';
//            $list['commission_type'] = str_replace(array(1,2), array('商城','拍卖'),$commis['commission_type']);
            $list['commission_type'] = $commission_type[$commis['commission_type']];
            if($commis['commission_type'] == 1&&$commis['order_goods_id'] == 0){
                $list['commission_type'] = '线下'.$list['commission_type'];
                $list['operation'] = '';
            }
            //$list['dis_member_id'] = $commis['dis_member_id'];

            $list['dis_member_name'] = $commis['dis_member_name'];
            $list['member_type_display'] = $member_type_display[$commis['dis_member_id']]['member_type_display'];
            //$list['dis_commis_lv'] = str_replace(array(1,2), array('一级','二级'), $commis['top_lv']);
            if ($commis['from_member_id'] == $commis['dis_member_id']) {
                $list['dis_commis_lv'] = '自己';
            } else {
                $model_member_distribute = Model('member_distribute');
                $condition = ['member_id' => $commis['from_member_id']];
                $distribute_info = $model_member_distribute->where($condition)->find();
                if ($distribute_info['top_member'] == $commis['dis_member_id']) {
                    $list['dis_commis_lv'] = '上一级';
                } else {
                    $list['dis_commis_lv'] = '上二级';
                }
            }
            $list['goods_name'] = $commis['goods_name'];
            $list['dis_commis_rate'] = $commis['dis_commis_rate'].'%'.$set_value['dis_commis_rate_flag'];
            $list['goods_pay_amount'] = ncPriceFormat($commis['goods_pay_amount']);
            $list['order_sn'] = $commis['order_sn'];
            $list['commission_amount'] = ncPriceFormat($commis['commission_amount']);
            $list['dis_commis_state'] = str_replace(array(0,1), array('未结算','已结算'), $commis['dis_commis_state']);
            $list['add_time'] = date('Y-m-d H:i:s',$commis['add_time']);
            $list['commission_time'] = $commis['commission_time'] >0 ? date('Y-m-d H:i:s',$commis['commission_time']) : '--';
            $list['from_member_name'] = $commis['from_member_name'];
            $list['special_rate_day'] = $set_value['special_rate_day'];

            if($excel){ //如果是excel
                unset($list['operation']);
                $tmp = array();
                foreach ($list as $item){
                    $tmp[] = array('data'=>$item);
                }
                $data[] = $tmp;
            }else{
                $data['list'][$commis['log_id']] = $list;
            }
        }
    }
    /**
     * 拼接统计相关数据
     * @param array $commission_amount
     * @return array
     */
    private function setFooterValueByType($commission_amount){
        $footer_data=[
            'operation'=>'订单总金额：',
            'commission_type'=>0.00,
            'dis_member_id'=>'拍卖：',
            'dis_member_name'=>0.00,
            'member_type_display'=>'保证金利息：',
            'dis_commis_lv'=>0.00,
            'dis_commis_rate'=>'合伙人升级：',
            'goods_pay_amount'=>0.00,
            'commission_amount'=>'艺术家入驻',
            'dis_commis_state'=>0.00
        ];
        foreach ($commission_amount as $value){
            switch ($value['commission_type']){
                case '1'://商品订单
                    $footer_data['operation'] = '订单总金额：';
                    $footer_data['commission_type'] = $value['commission_amount'];
                    break;
                case '2'://拍卖
                    $footer_data['dis_member_id'] = '拍卖：';
                    $footer_data['dis_member_name'] = $value['commission_amount'];
                    break;
                case '3'://保证金利息
                    $footer_data['member_type_display'] = '保证金利息：';
                    $footer_data['dis_commis_lv'] = $value['commission_amount'];
                    break;
                case '4'://合伙人升级
                    $footer_data['dis_commis_rate'] = '合伙人升级：';
                    $footer_data['goods_pay_amount'] = $value['commission_amount'];
                    break;
                case '5'://艺术家入驻
                    $footer_data['commission_amount'] = '艺术家入驻：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                case '6'://艺术家入驻
                    $footer_data['commission_amount'] = '保证金收益利息：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                case '7'://艺术家入驻
                    $footer_data['commission_amount'] = '竞拍返佣：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                case '8'://艺术家入驻
                    $footer_data['commission_amount'] = '成交返佣：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                case '9'://艺术家入驻
                    $footer_data['commission_amount'] = '保证金返佣：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                default :
                    break;
            }
        }
        return $footer_data;
    }
    /**
     * 获取会员等级
     * @param array $member_id_arr
     * @return array
     */
    private function getMemberType($member_id_arr){
        $member_type_display = [];
        if (!empty($member_id_arr)){
            $member_type_display = Model('member')->getMemberList(['member_id'=>['in',$member_id_arr]], 'member_id,member_type');
            $member_type_config = Model('distribution_config')->getConfigList();
            $member_type_config = array_under_reset($member_type_config,'type_id');
            foreach ($member_type_display as $key=>&$value){
                $value['member_type_display'] = $member_type_config[$value['member_type']]['name'];
            }
            unset($value);
            $member_type_display = array_under_reset($member_type_display,'member_id');
        }
        return $member_type_display;
    }
    /**
     * 获取会员等级
     * @param array $commis
     * @return array
     */
    private function setValueByType($commis){
        $setValue=['special_rate_day','dis_commis_rate_flag'];
        switch ($commis['commission_type']){
            case '1'://商品订单
                $setValue['href'] = urlAdminShop('order', 'show_order', array('order_id' =>$commis['order_id']));
                break;
            case '2'://拍卖
                break;
            case '3'://保证金利息
                $setValue['href'] = urlAdminShop('margin_order', 'index', array('order_sn' =>$commis['order_sn']));
                $margin_orders_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$commis['order_sn']], 'margin_id,pay_date_number');
                $setValue['special_rate_day'] = $margin_orders_info['pay_date_number'] > 0 ? $margin_orders_info['pay_date_number'].'天' : '';
                $setValue['dis_commis_rate_flag'] = "年化";
                break;
            case '4'://合伙人升级
                break;
            case '5'://艺术家入驻
                $setValue['href'] = urlAdminShop('store', 'store_joinin_detail', array('member_id' =>$commis['from_member_id']));
                break;
            case '6'://保证金利息
                $setValue['href'] = urlAdminSystem('commission', 'commission_detail', array('order_sn' =>$commis['order_sn'],'id'=>$commis['log_id']));
                break;
            default :
                break;
        }
        return $setValue;
    }

}
?>