<?php
/**
 * 商品分析
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class margin_refund_dataControl extends SystemControl{
    private $links = array(
        array('url'=>'act=margin_refund_data&op=yesterday&type=-1','lang'=>'yesterday'),
        array('url'=>'act=margin_refund_data&op=today&type=0','lang'=>'today'),
        array('url'=>'act=margin_refund_data&op=tomorrow&type=1','lang'=>'tomorrow'),
        array('url'=>'act=margin_refund_data&op=tomorrow2&type=2','lang'=>'tomorrow2'),
        array('url'=>'act=margin_refund_data&op=other&type=3','lang'=>'other')
    );
    public function __construct(){
        parent::__construct();
        Language::read('margin_order_data');
        import('function.statistics');
        import('function.datehelper');

    }
    public function indexOp() {
        $type = isset($_GET['type'])?$_GET['type']:0;
        switch($type){
            case -1:
                $data_type = 'yesterday';
            break;
            case 0:
                $data_type = 'today';
            break;
            case 1:
                $data_type = 'tomorrow';
            break;
            case 2:
                $data_type = 'tomorrow2';
            break;
            case 3:
                $data_type = 'other';
            break;
            default:
                $data_type = 'today';
                $type = 0;
            break;
        }
        Tpl::output('data_type',$type);
        Tpl::output('top_link',$this->sublink($this->links, $data_type));
        Tpl::showpage('margin_order.detail2');
    }

    public function get_top_dataOp(){
        $type = $_GET['type'];
        $model_margin_order = Model('margin_orders');
        switch($type){
            case -1:
                $title = '昨日统计';
            break;
            case 0:
                $title = '当日统计';
            break;
            case 1:
                $title = '明日统计';
            break;
            case 2:
                $title = '后日统计';
            break;
            case 3:
                $title = '自定义周期统计';
            break;
            default:
                $title = '当日统计';
                $type = 0;
            break;
        }
        if($type == 3){
            $o_start = $_GET['query_start_date'];
            $o_end = $_GET['query_end_date'];
            $auction_id_list = Model('auctions')->where("FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') >= '{$o_start}' and FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') <='{$o_end}'")->select();
        }else{
            $date = date('Y-m-d',time()+ $type*86400);
            $auction_id_list = Model('auctions')->where("FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') = '{$date}'")->select();
        }
        $auction_id_arr = array();
        if(!empty($auction_id_list)){
            foreach($auction_id_list as $v){
                $auction_id_arr[] = $v['auction_id'];
            }
        }
        $order_num = 0;
        $amount = 0;
        if(!empty($auction_id_arr)){
            $condition = array(
                'order_state'=>1,
                'auction_id'=>array('in',$auction_id_arr)
            );
            $order_num = $model_margin_order->where($condition)->count();
            $amount = $model_margin_order->field('sum(margin_amount) amount')->where($condition)->find();
            $amount = $amount['amount'];
        }
        echo '<div class="title"><h3>'.$title.'</h3></div>';
		echo '<dl class="row"><dd class="opt"><ul class="nc-row">';
        echo '<li title="订单总金额：'. number_format($amount,2).'元"><h4>订单总金额:</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$amount.'"></h2><h6>元</h6></li>';
        echo '<li title="订单总量：'. $order_num.'单"><h4>订单总量:</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$order_num.'"></h2><h6>单</h6></li>';

		echo '</ul></dd><dl>';
        exit;
    }

    public function get_xmlOp(){
        $type = $_GET['type'];
        $page = intval($_POST['rp']);
        $model_margin_order = Model('margin_orders');
        $auction_id_arr = array();
        if($type == 3){
            $o_start = $_GET['query_start_date'];
            $o_end = $_GET['query_end_date'];
            $auction_id_list = Model('auctions')->where("FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') >= '{$o_start}' and FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') <='{$o_end}'")->select();
        }else{
            $date = date('Y-m-d',time()+ $type*86400);
            $auction_id_list = Model('auctions')->where("FROM_UNIXTIME(auction_end_time,'%Y-%m-%d') = '{$date}'")->select();
        }
        if(!empty($auction_id_list)){
            foreach($auction_id_list as $v){
                $auction_id_arr[] = $v['auction_id'];
            }
        }
        $condition = array(
            'order_state'=>1,
            'auction_id'=>array('in',$auction_id_arr)
        );
        $list = $model_margin_order->getList($condition,'*','','',0,$page);

        if(!empty($list)){
            foreach($list as $order_info){
                $tmp = array();
                $tmp['order_sn'] = $order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']);
                $tmp['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
                $tmp['created_at'] = date('Y-m-d H:i:s',$order_info['created_at']);
                $tmp['margin_amount'] = ncPriceFormat($order_info['margin_amount']);
                $tmp['order_state'] =  orderMarginState($order_info);
                $tmp['payment_code'] = orderPaymentName($order_info['payment_code']);
                $tmp['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
                $tmp['rcb_amount'] = ncPriceFormat($order_info['rcb_amount']);
                $tmp['pd_amount'] = ncPriceFormat($order_info['pd_amount']);
                $tmp['points_amount'] = ncPriceFormat($order_info['points_amount']);
                $tmp['account_margin_amount'] = ncPriceFormat($order_info['account_margin_amount']);
                $tmp['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
                $tmp['store_id'] = $order_info['store_id'];
                $tmp['store_name'] = $order_info['store_name'];
                $tmp['goods_name'] = $order_info['auction_name'];
                $tmp['buyer_id'] = $order_info['buyer_id'];
                $tmp['buyer_name'] = $order_info['buyer_name'];
                $data['list'][$order_info['margin_id']] = $tmp;
            }
        }
        $data['now_page'] = $model_margin_order->shownowpage();
        $data['total_num'] = $model_margin_order->gettotalnum();
        echo Tpl::flexigridXML($data);exit();
    }

}
