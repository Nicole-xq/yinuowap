<?php
/**
 * 书画首页banner设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class painting_bannerControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        $model_setting = Model('setting');
        $list_setting = $model_setting->getListSetting();
        if ($list_setting['painting_banner_setting'] != ''){
            $list = unserialize($list_setting['painting_banner_setting']);
        }
        Tpl::output('list', $list);
        $input = array();
        if (chksubmit()){
            //上传图片
            $update_array = array();

            if(!empty($_FILES['banner_image']['name'])) {
                $upload = new UploadFile();
                $upload->set('default_dir',ATTACH_PAINTING);
                $result = $upload->upfile('banner_image');
                if(!$result) {
                    showMessage($upload->error);
                }
                $input['pic'] = $upload->file_name;
            }else{
                if($list['pic'] != ''){
                    $input['pic'] = $list['pic'];
                }else{
                    $input['pic'] = '';
                }
            }
            $input['url'] = $_POST['banner_url'];

            if (count($input) > 0){
                $update_array['painting_banner_setting'] = serialize($input);
            }else{
                $update_array['painting_banner_setting'] = '';
            }

            $result = $model_setting->updateSetting($update_array);
            if ($result === true){
                showMessage('设置成功');
            }else {
                showMessage('设置失败');
            }
        }

        Tpl::showpage('painting_banner');
    }




}