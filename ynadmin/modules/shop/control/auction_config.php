<?php
/**
 * 专场管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_configControl extends SystemControl{


    public function __construct(){
        parent::__construct();
//        Language::read('store,store_grade');
    }

    /**
     * 专场细节列表
     */
    public function indexOp()
    {
        $model_auction_special_rate_check = Model('auction_config');
        $config_info = $model_auction_special_rate_check->getInfo(1);

        Tpl::output('info', $config_info);

        Tpl::showpage('auction_config.edit');
    }

    public function save_infoOp(){
        $id = 1;
        $duration_time = $_POST['duration_time'];
        $add_time = $_POST['add_time'];
        $model_auction_special_rate_check = Model('auction_config');
        $config_info = $model_auction_special_rate_check->updateInfo(array('id'=>$id),array('duration_time'=>$duration_time,'add_time'=>$add_time));
        if($config_info){
            showMessage('保存成功','index.php?act=auction_config&op=index');
        }else{
            showMessage('保存失败','index.php?act=auction_config&op=index');
        }
    }

}
