<?php
/**
 * 资金统计管理
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class stat_capitalControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        if ($_GET['get_stat_data']==1){

            $predeposit_where = ['member_id'=>['gt',0],'member_state'=>1];
            $commission_where = ['dis_member_id'=>['gt',0],'dis_commis_state'=>1];
            $enchashment_where = ['pdc_member_id'=>['gt',0],'pdc_payment_state'=>1];
            $auction_where = ['buyer_id'=>['gt',0],'refund_state'=>0,'order_state'=>1];
            $api_margin_where = ['buyer_id'=>['gt',0],'api_pay_state'=>1];
            $recharge_sum_where = ['pdr_member_id'=>['gt',0],'pdr_payment_state'=>1];
            $api_order_where = ['order_state'=>['egt',20],'api_pay_time'=>['gt',0],'order_type'=>1];
            $goods_order_where = ['order_state'=>['egt',20],'order_type'=>1];

            if (trim($_GET['query_start_date']) && trim($_GET['query_end_date'])) {
                $start_time = strtotime($_GET['query_start_date']);
                $end_time = strtotime($_GET['query_end_date']. "  23:59:59");
                $commission_where['add_time'] = ['between', "$start_time,$end_time"];
                $enchashment_where['pdc_add_time'] = ['between', "$start_time,$end_time"];
                $api_margin_where['created_at'] = ['between', "$start_time,$end_time"];
                $recharge_sum_where['pdr_add_time'] = ['between', "$start_time,$end_time"];
                $api_order_where['api_pay_time'] = ['between', "$start_time,$end_time"];
                $goods_order_where['add_time'] = ['between', "$start_time,$end_time"];
            }
            //现存保证及总金额
            $margin_sum = Model('margin_orders')->getMarginSum($auction_where,'margin_amount');
            // 预存款总金额
            $predeposit_sum = Model('member')->getApredepositSum($predeposit_where,'available_predeposit');
            // 奖励金
            $commission_sum = Model('member_commission')->getCommissionSum($commission_where,'commission_amount');
            // 提现
            $enchashment_sum = Model('predeposit')->getPdCashSum($enchashment_where,'pdc_amount');
            // 商品购买总金额
            $goods_order_sum = Model('order')->getPdSum($goods_order_where,'order_amount');
            // 第三方 充值
            $recharge_sum = Model('predeposit')->getPdRechargeSum($recharge_sum_where,'pdr_amount');
            // 第三方 保证金
            $api_margin_sum = Model('margin_orders')->getMarginSum($api_margin_where,'api_pay_amount');
            // 第三方 购买
            $api_order_sum = Model('order')->getPdSum($api_order_where,'order_amount - rcb_amount - pd_amount - rpt_amount - points_amount');

            // 第三方 总额
            $api_sum = number_format($recharge_sum + $api_margin_sum +$api_order_sum);

            echo '<div class="title"><h3>销售收入情况一览</h3></div>';
            echo '<dl class="row"><dt class="tit"><B>第三方支付总金额：</B></dt><dd class="opt"><span>'.$api_sum.'</span> 元  = <span>'. number_format($recharge_sum).'</span>（第三方支付充值） + <span>'. number_format($api_margin_sum).'</span>（保证金充值）+ <span>'. number_format($api_order_sum).'</span>（第三方支付产品）</dd></dl>';
            echo '<dl class="row"><dt class="tit"><B>提现成功总金额：</B></dt><dd class="opt"><span>'. number_format($enchashment_sum,2).'</span> 元</dd></dl>';
            echo '<dl class="row"><dt class="tit"><B>奖励金总金额：</B></dt><dd class="opt"><span>'. number_format($commission_sum,2).'</span> 元</dd></dl>';
            echo '<dl class="row"><dt class="tit"><B>商品购买总金额：</B></dt><dd class="opt"><span>'. number_format($goods_order_sum,2).'</span> 元</dd></dl>';
            echo '<dl class="row"><dt class="tit"><B>现存预存款总金额：</B></dt><dd class="opt"><span>'. number_format($predeposit_sum,2).'</span> 元</dd></dl>';
            echo '<dl class="row"><dt class="tit"><B>现存保证金总金额：</B></dt><dd class="opt"><span>'. number_format($margin_sum,2).'</span> 元</dd></dl>';
            exit();
        }else{
            $query = [
                'end_date'=>date('Y-m-d',time()),
                'start_date'=>date('Y-m-d',time()),
            ];
            Tpl::output('query', $query);
            Tpl::showpage('stat_capital');
        }
    }





}
