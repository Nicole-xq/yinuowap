<?php
/**
 * 自营店铺推荐分类设置
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class shop_categoryControl extends SystemControl
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexOp()
    {
        $this->listOp();
    }

    public function listOp()
    {
        Tpl::showpage('shop_category.list');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_shop_category = Model('recommend_category');

        //店铺列表
        $order = '';
        $param = array('id', 'category_name');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $category_list = $model_shop_category->get_list($order);

//        $ids = array();
//        foreach ($store_list as $s) {
//            $ids[$s['id']] = null;
//        }
//
//        $ids = array_keys($ids);
//        $stores_with_goods = model('goods')->where(array('id' => array('in', $ids)))->field('distinct id')->key('id')->select();

        $data = array();
        $data['now_page'] = 1;
        $data['total_num'] = count($category_list);//分页简单粗暴,因为.....推荐分类不会多 (^_^)
        foreach ($category_list as $value) {
            $param = array();
            $operation = "<a class='btn blue' href='index.php?act=shop_category&op=edit&id=".$value['id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $operation .= "<a class='btn blue' href=\"javascript:void(0);\" onclick=\"fg_delete('".$value['id']."');\"><i class='fa fa-pencil-square-o'></i>取消推荐</a>";
            $param['operation'] = $operation;
            $param['id'] = $value['id'];
            $param['category_name'] = $value['category_name'];
            $param['img'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=". categoryImage($value['img']).">\")'><i class='fa fa-picture-o'></i></a>";
            $data['list'][$value['id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function editOp(){
        $id = $_GET['id'];
        if(chksubmit()){
            $id = $_POST['id'];
            $category_id = $_POST['category_id'];
            $img = $_POST['img'];
            $info = Model('store_goods_class')->getStoreGoodsClassInfo(array('stc_id'=>$category_id),'stc_name');
            $re = Model('recommend_category')->update_info($id,array('category_id'=>$category_id,'category_name'=>$info['stc_name'],'img'=>$img));
            if($re){
                showMessage('success');
            }else{
                showMessage('fail');
            }
        }
        $info = Model('recommend_category')->get_info($id);
        $store_id = C('store.zq_id');
        $store_goods_class_list = Model('store_goods_class')->getStoreGoodsClassList(array('store_id'=>$store_id));
        $info['list'] = $store_goods_class_list;
//        print_R($info);exit;
        Tpl::output('list',$info);
        Tpl::showpage('shop_category.edit');
    }

    public function addOp(){
        if(chksubmit()){
            $param = array();
            $param['category_id'] = $_POST['category_id'];
            $param['img'] = $_POST['img'];
            $info = Model('store_goods_class')->getStoreGoodsClassInfo(array('stc_id'=>$param['category_id']),'stc_name');
            $param['category_name'] = $info['stc_name'];
            $re = Model('recommend_category')->add_info($param);
            if($re){
                showMessage('success');
            }else{
                showMessage('fail');
            }
        }
        $store_id = C('store.zq_id');
        $store_goods_class_list = Model('store_goods_class')->getStoreGoodsClassList(array('store_id'=>$store_id));
        Tpl::output('list',$store_goods_class_list);
        Tpl::showpage('shop_category.add');
    }

    public function delOp(){
        $id = $_GET['id'];
        $re = Model('recommend_category')->update_info($id,array('status'=>1));
        if($re){
            echo json_encode(array('state'=>1));exit;
        }else{
            echo json_encode(array('msg'=>'失败'));exit;
        }
    }


}
