<?php
/**
 * 书画-合作商家
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class cooper_storeControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('cooper_store.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_co_store = Model('cooper_store');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('cooper_store_id', 'cooper_store_url', 'cooper_store_pic', 'cooper_store_sort', 'is_show');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        // 合作商家列表
        $co_store_list = $model_co_store->getcooper_storeList($condition, '*', $page, $order);


        $data = array();
        $data['now_page'] = $model_co_store->shownowpage();
        $data['total_num'] = $model_co_store->gettotalnum();
        foreach ($co_store_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick=\"fg_del(".$value['cooper_store_id'].")\"><i class='fa fa-trash-o'></i>删除</a>";
            $operation .= "<a class='btn blue' href='index.php?act=cooper_store&op=cooper_store_edit&cooper_store_id=" . $value['cooper_store_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] = $operation;
            $param['cooper_store_id'] = $value['cooper_store_id'];
            $param['cooper_store_pic'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=". co_storeImage($value['cooper_store_pic']).">\")'><i class='fa fa-picture-o'></i></a>";
            $param['cooper_store_url'] = $value['cooper_store_url'];
            $param['cooper_store_sort'] = $value['cooper_store_sort'];
            $param['is_show'] = $value['is_show'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $data['list'][$value['cooper_store_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }


    /**
     * 增加合作商家
     */
    public function cooper_store_addOp(){
        $model_co_store = Model('cooper_store');
        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["cooper_store_url"], "require"=>"true", "message"=>'合作商家链接不能为空'),
                array("input"=>$_POST["cooper_store_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'请填写正确的排序'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                if (!empty($_FILES['cooper_store_pic']['name'])){
                    $upload = new UploadFile();
                    $upload->set('default_dir',ATTACH_PAINTING);
                    $result = $upload->upfile('cooper_store_pic');
                    if ($result){
                        $_POST['cooper_store_pic'] = $upload->file_name;
                    }else {
                        showMessage($upload->error,'','','error');
                    }
                }
                $insert_array = array();
                $insert_array['cooper_store_url'] = trim($_POST['cooper_store_url']);
                if(!empty($_POST['cooper_store_pic'])){
                    $insert_array['cooper_store_pic'] = trim($_POST['cooper_store_pic']);
                }
                $insert_array['is_show'] = trim($_POST['is_show']);
                $insert_array['cooper_store_sort'] = intval($_POST['cooper_store_sort']);

                $result = $model_co_store->addcooper_store($insert_array);
                if ($result){
                    $url = array(
                        array(
                            'url'=>'index.php?act=cooper_store&op=cooper_store_add',
                            'msg'=>'继续新增合作商家',
                        ),
                        array(
                            'url'=>'index.php?act=cooper_store&op=index',
                            'msg'=>'返回合作商家列表',
                        )
                    );
                    showMessage('新增合作商家成功',$url);
                }else {
                    showMessage('新增合作商家失败');
                }
            }
        }


        Tpl::showpage('cooper_store.add');
    }

    /**
     * 合作商家编辑
     */
    public function cooper_store_editOp(){
        $model_co_store = Model('cooper_store');

        if (chksubmit()){
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["cooper_store_url"], "require"=>"true", "message"=>'合作商家链接不能为空'),
                array("input"=>$_POST["cooper_store_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'请填写正确的排序'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                if (!empty($_FILES['cooper_store_pic']['name'])){
                    $upload = new UploadFile();
                    $upload->set('default_dir',ATTACH_PAINTING);
                    $result = $upload->upfile('cooper_store_pic');
                    if ($result){
                        $_POST['cooper_store_pic'] = $upload->file_name;
                    }else {
                        showMessage($upload->error,'','','error');
                    }
                }
                $co_store_info = $model_co_store->getcooper_storeInfo(array('cooper_store_id' => intval($_POST['cooper_store_id'])));
                $where = array();
                $where['cooper_store_id'] = intval($_POST['cooper_store_id']);
                $update_array = array();
                $update_array['cooper_store_url'] = trim($_POST['cooper_store_url']);
                if (!empty($_POST['cooper_store_pic'])){
                    $update_array['cooper_store_pic'] = $_POST['cooper_store_pic'];
                }
                $update_array['is_show'] = intval($_POST['is_show']);
                $update_array['cooper_store_sort'] = intval($_POST['cooper_store_sort']);
                $result = $model_co_store->editcooper_store($update_array,$where);
                if ($result){
                    if (!empty($_POST['cooper_store_pic']) && !empty($co_store_info['cooper_store_pic'])){
                        @unlink(BASE_UPLOAD_PATH.DS.ATTACH_PAINTING.DS.$co_store_info['cooper_store_pic']);
                    }
                    $url = array(
                        array(
                            'url'=>'index.php?act=cooper_store&op=cooper_store_edit&cooper_store_id='.intval($_POST['cooper_store_id']),
                            'msg'=>'继续编辑合作商家',
                        ),
                        array(
                            'url'=>'index.php?act=cooper_store&op=index',
                            'msg'=>'返回合作商家列表',
                        )
                    );
                    showMessage('编辑合作商家成功',$url);
                }else {
                    showMessage('编辑合作商家失败');
                }
            }
        }

        $co_store_info = $model_co_store->getcooper_storeInfo(array('cooper_store_id' => intval($_GET['cooper_store_id'])));
        if (empty($co_store_info)){
            showMessage('参数错误');
        }
        Tpl::output('co_store_array',$co_store_info);


        Tpl::showpage('cooper_store.edit');
    }

    /**
     * 删除合作商家
     */
    public function cooper_store_delOp(){
        $cooper_store_id = intval($_GET['id']);
        if ($cooper_store_id <= 0) {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
        Model('cooper_store')->delcooper_store(array('cooper_store_id' => $cooper_store_id));
        exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
    }








}