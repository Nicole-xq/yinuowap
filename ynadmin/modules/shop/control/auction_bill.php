<?php
/**
 * 拍卖订单结算管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class auction_billControl extends SystemControl{
    /**
     * 每次导出订单数量
     * @var int
     */
    const EXPORT_SIZE = 1000;

    public function __construct(){
        parent::__construct();
    }
    public function indexOp(){
        Tpl::showpage('auction_bill.index');
    }

    public function get_bill_xmlOp(){
        $model_bill = Model('auction_order_bill');
        $condition = array();

        if (!empty($_REQUEST['os_month']) && !preg_match('/^20\d{4}$/',$_REQUEST['os_month'])) {
            exit();
        }
        list($condition,$order) = $this->_get_bill_condition($condition);

        $bill_list = $model_bill->getOrderBillList($condition,'*',$_POST['rp'],$order);
        $data = array();
        $data['now_page'] = $model_bill->shownowpage();
        $data['total_num'] = $model_bill->gettotalnum();
        foreach ($bill_list as $bill_info) {
            $list = array();
            if (in_array($bill_info['ob_state'],array(2,3))) {
                $list['operation'] = "<a class=\"btn orange\" href=\"index.php?act=auction_bill&op=show_bill&ob_id={$bill_info['ob_id']}\"><i class=\"fa fa-gavel\"></i>处理</a>";
            } else {
                $list['operation'] = "<a class=\"btn green\" href=\"index.php?act=auction_bill&op=show_bill&ob_id={$bill_info['ob_id']}\"><i class=\"fa fa-list-alt\"></i>查看</a>";
            }
			$list['ob_id'] = $bill_info['ob_id'];
            $list['ob_start_date'] = date('Y-m-d',$bill_info['ob_start_date']);
            $list['ob_end_date'] = date('Y-m-d',$bill_info['ob_end_date']);
            $list['ob_order_totals'] = ncPriceFormat($bill_info['ob_order_totals']);
            $list['ob_commis_totals'] = ncPriceFormat($bill_info['ob_commis_totals']);
            $list['ob_result_totals'] = ncPriceFormat($bill_info['ob_result_totals']);
            $list['ob_create_date'] = date('Y-m-d',$bill_info['ob_create_date']);
            $list['ob_state'] = billState($bill_info['ob_state']);
            $list['ob_store_name'] = $bill_info['ob_store_name'];
            $list['ob_store_id'] = $bill_info['ob_store_id'];
            $data['list'][$bill_info['ob_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 某店铺某月订单列表
     *
     */
    public function show_billOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $bill_info = $model_bill->getOrderBillInfo(array('ob_id'=>$ob_id));
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }
        $sub_tpl_name = 'auction_bill.show.code_list';

        Tpl::output('tpl_name',$sub_tpl_name);
        Tpl::output('bill_info',$bill_info);
        Tpl::showpage('auction_bill.show');
    }

    public function get_bill_info_xmlOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            exit();
        }
        $model_bill = Model('auction_order_bill');
        $bill_info = $model_bill->getOrderBillInfo(array('ob_id'=>$ob_id));
        if (!$bill_info){
            exit();
        }
        $model_order = Model('auction_order');

        $order_condition = array();
        $order_condition['order_state'] = ORDER_STATE_SUCCESS;
        $order_condition['store_id'] = $bill_info['ob_store_id'];
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']) : null;
        $end_unixtime = $if_end_date ? $end_unixtime+86400-1 : null;
        if ($if_start_date || $if_end_date) {
            $order_condition['finnshed_time'] = array('between',"{$start_unixtime},{$end_unixtime}");
        } else {
            $order_condition['finnshed_time'] = array('between',"{$bill_info['ob_start_date']},{$bill_info['ob_end_date']}");
        }
            if ($_POST['query'] != '' && in_array($_POST['qtype'],array('order_sn','buyer_name'))) {
                $order_condition[$_POST['qtype']] = array('like',"%{$_POST['query']}%");
            }
            if ($_GET['order_sn'] != ''){
                $order_condition['order_sn'] = array('like',"%{$_GET['order_sn']}%");
            }
            if ($_GET['buyer_name'] != ''){
                if ($_GET['jq_query']) {
                    $order_condition['buyer_name'] = $_GET['buyer_name'];
                } else {
                    $order_condition['buyer_name'] = array('like',"%{$_GET['buyer_name']}%");
                }
            }

            $sort_fields = array('order_amount','add_time','finnshed_time','buyer_id','store_id');
            if (in_array($_POST['sortorder'],array('asc','desc')) && in_array($_POST['sortname'],$sort_fields)) {
                $order = $_POST['sortname'].' '.$_POST['sortorder'];
            }
        $order_list = $model_order->getList($order_condition, $_POST['rp'], '*', $order);

        $data = array();
        $data['now_page'] = $model_order->shownowpage();
        $data['total_num'] = $model_order->gettotalnum();
            foreach ($order_list as $order_info) {
                $list = array();
                $list['operation'] = "<a class=\"btn green\" href=\"index.php?act=auction_order&op=show_order&order_id={$order_info['auction_order_id']}\"><i class=\"fa fa-list-alt\"></i>查看</a>";
                $list['order_sn'] = $order_info['auction_order_sn'];
                $list['order_amount'] = ncPriceFormat($order_info['order_amount']);
                $list['commis_amount'] = ncPriceFormat($order_info['order_amount'] * $order_info['commis_rate']/100);
                $list['add_time'] = date('Y-m-d',$order_info['add_time']);
                $list['finnshed_time'] = date('Y-m-d',$order_info['finnshed_time']);
                $list['buyer_name'] = $order_info['buyer_name'];
                $list['buyer_id'] = $order_info['buyer_id'];
                $list['store_name'] = $order_info['store_name'];
                $list['store_id'] = $order_info['store_id'];
                $data['list'][$order_info['auction_order_id']] = $list;
            }
        exit(Tpl::flexigridXML($data));
    }

    public function bill_checkOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_id'] = $ob_id;
        $condition['ob_state'] = BILL_STATE_STORE_COFIRM;
        $update = $model_bill->editOrderBill(array('ob_state'=>BILL_STATE_SYSTEM_CHECK),$condition);
        if ($update){
            $this->log('审核账单,账单号：'.$ob_id,1);
            showMessage('审核成功，账单进入付款环节');
        }else{
            $this->log('审核账单，账单号：'.$ob_id,0);
            showMessage('审核失败','','html','error');
        }
    }

    /**
     * 账单付款
     *
     */
    public function bill_payOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_id'] = $ob_id;
        $condition['ob_state'] = BILL_STATE_SYSTEM_CHECK;
        $bill_info = $model_bill->getOrderBillInfo($condition);
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }
        if (chksubmit()){
            if (!preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['pay_date'])) {
                showMessage('参数错误','','html','error');
            }
            $input = array();
            $input['ob_pay_content'] = $_POST['pay_content'];
            $input['ob_pay_date'] = strtotime($_POST['pay_date']);
            $input['ob_state'] = BILL_STATE_SUCCESS;
            $update = $model_bill->editOrderBill($input,$condition);
            if ($update){
                // 发送店铺消息
                $param = array();
                $param['code'] = 'store_bill_gathering';
                $param['store_id'] = $bill_info['ob_store_id'];
                $param['param'] = array(
                    'bill_no' => $bill_info['ob_id']
                );
                QueueClient::push('sendStoreMsg', $param);

                $this->log('账单付款,账单号：'.$ob_id,1);
                showMessage('保存成功','index.php?act=auction_bill');
            }else{
                $this->log('账单付款,账单号：'.$ob_id,1);
                showMessage('保存失败','','html','error');
            }
        }else{
            Tpl::showpage('auction_bill.pay');
        }
    }

    /**
     * 打印结算单
     *
     */
    public function bill_printOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_id'] = $ob_id;
        $condition['ob_state'] = BILL_STATE_SUCCESS;
        $bill_info = $model_bill->getOrderBillInfo($condition);
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }

        Tpl::output('bill_info',$bill_info);

        Tpl::showpage('auction_bill.print','null_layout');
    }


    /**
     * 导出平台月出账单表
     *
     */
    public function export_billOp(){
        $model_bill = Model('auction_order_bill');
        $condition = array();

        if (preg_match('/^[\d,]+$/', $_GET['ob_id'])) {
            $_GET['ob_id'] = explode(',',trim($_GET['ob_id'],','));
            $condition['ob_id'] = array('in',$_GET['ob_id']);
        }
        list($condition,$order) = $this->_get_bill_condition($condition);

        if (!is_numeric($_GET['curpage'])){
            $count = $model_bill->getOrderBillCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){
                //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','javascript:history.back(-1)');
                Tpl::showpage('export.excel');
                exit();
            }
            $limit = false;
        }else{
            //下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $limit = "{$limit1},{$limit2}";
        }
        $data = $model_bill->getOrderBillList($condition,'*','',$order,$limit);
        $export_data = array();
        $export_data[0] = array('账单编号','开始日期','结束日期','订单金额','佣金金额','本期应结','出账日期','账单状态','店铺','店铺ID');
        $ob_order_totals = 0;
        $ob_commis_totals = 0;
        $ob_result_totals = 0;
        foreach ($data as $k => $v) {
            $export_data[$k+1][] = $v['ob_id'];
            $export_data[$k+1][] = date('Y-m-d',$v['ob_start_date']);
            $export_data[$k+1][] = date('Y-m-d',$v['ob_end_date']);
            $ob_order_totals += $export_data[$k+1][] = $v['ob_order_totals'];
            $ob_commis_totals += $export_data[$k+1][] = $v['ob_commis_totals'];
            $ob_result_totals += $export_data[$k+1][] = $v['ob_result_totals'];
            $export_data[$k+1][] = date('Y-m-d',$v['ob_create_date']);
            $export_data[$k+1][] = billState($v['ob_state']);
            $export_data[$k+1][] = $v['ob_store_name'];
            $export_data[$k+1][] = $v['ob_store_id'];
        }
        $count = count($export_data);
        $export_data[$count][] = '';
        $export_data[$count][] = '';
        $export_data[$count][] = '合计';
        $export_data[$count][] = $ob_order_totals;
        $export_data[$count][] = $ob_commis_totals;
        $export_data[$count][] = $ob_result_totals;
        $csv = new Csv();
        $export_data = $csv->charset($export_data,CHARSET,'gbk');
        $csv->filename = 'auction_bill-'.$_GET['os_month'];
        $csv->export($export_data);
    }

    /**
     * 导出兑换码明细CSV
     *
     */
    public function export_orderOp(){
        $ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $bill_info = $model_bill->getOrderBillInfo(array('ob_id'=>$ob_id));
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }
        $model_order = Model('auction_order');
        $condition = array();
        $condition['order_state'] = ORDER_STATE_SUCCESS;
        $condition['store_id'] = $bill_info['ob_store_id'];
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']) : null;
        $end_unixtime = $if_end_date ? $end_unixtime+86400-1 : null;
        if ($if_start_date || $if_end_date) {
            $condition['finnshed_time'] = array('between',"{$start_unixtime},{$end_unixtime}");
        } else {
            $condition['finnshed_time'] = array('between',"{$bill_info['ob_start_date']},{$bill_info['ob_end_date']}");
        }
        if (preg_match('/^[\d,]+$/', $_GET['order_id'])) {
            $_GET['order_id'] = explode(',',trim($_GET['order_id'],','));
            $condition['order_id'] = array('in',$_GET['order_id']);
        }

        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('order_sn','buyer_name'))) {
            $condition[$_REQUEST['qtype']] = array('like',"%{$_REQUEST['query']}%");
        }
        if ($_GET['order_sn'] != ''){
            $condition['order_sn'] = array('like',"%{$_GET['order_sn']}%");
        }
        if ($_GET['buyer_name'] != ''){
            if ($_GET['jq_query']) {
                $condition['buyer_name'] = $_GET['buyer_name'];
            } else {
                $condition['buyer_name'] = array('like',"%{$_GET['buyer_name']}%");
            }
        }

        $sort_fields = array('order_amount','commis_amount','add_time','finnshed_time','buyer_id','store_id');
        if (in_array($_GET['sortorder'],array('asc','desc')) && in_array($_GET['sortname'],$sort_fields)) {
            $order = $_GET['sortname'].' '.$_GET['sortorder'];
        }
        if (!is_numeric($_GET['curpage'])){
            $count = $model_order->getOrderCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){
                //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=auction_bill&op=show_bill&ob_id='.$ob_id);
                Tpl::showpage('export.excel');
                exit();
            }else{
                //如果数量小，直接下载
                $data = $model_order->getList($condition,self::EXPORT_SIZE);
            }
        }else{
            //下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $data = $model_order->getList($condition,self::EXPORT_SIZE,'*',$order,"{$limit1},{$limit2}");
        }
        $export_data = array();
        $export_data[0] = array('订单编号','下单时间','成交时间','订单金额','佣金金额','商家','商家编号','买家','买家编号','商品');
        if ($_GET['query_type'] == 'timeout') {
            $export_data[0][1] = '过期时间';
        }

        $order_totals = 0;
        $commis_totals = 0;
        $k = 0;
        foreach ($data as $v) {
            $export_data[$k+1][] = $v['auction_order_sn'];
            $export_data[$k+1][] = date('Y-m-d',$v['add_time']);
            $export_data[$k+1][] = date('Y-m-d',$v['finnshed_time']);
            $order_totals += $export_data[$k+1][] = $v['order_amount'];
            $commis_totals += $export_data[$k+1][] = ncPriceFormat($v['order_amount'] * $v['commis_rate']/100);
            $export_data[$k+1][] = $v['store_name'];
            $export_data[$k+1][] = $v['store_id'];
            $export_data[$k+1][] = $v['buyer_name'];
            $export_data[$k+1][] = $v['buyer_id'];
            $export_data[$k+1][] = $v['auction_name'];
            $k++;
        }
        $count = count($export_data);
        $export_data[$count][] = '合计';
        $export_data[$count][] =  '';
        $export_data[$count][] = '';
        $export_data[$count][] = $order_totals;
        $export_data[$count][] = $commis_totals;
        $csv = new Csv();
        $export_data = $csv->charset($export_data,CHARSET,'gbk');
        $file_name = 'auction_order';
        $csv->filename = $ob_id.'-'.$file_name;
        $csv->export($export_data);
    }

    /**
     * 合并相同代码
     */
    private function _get_bill_condition($condition) {
        if ($_GET['query_year'] && $_GET['query_month']) {
            $_GET['os_month'] = intval($_GET['query_year'].$_GET['query_month']);
        } elseif ($_GET['query_year']) {
            $condition['os_month'] = array('between',$_GET['query_year'].'01,'.$_GET['query_year'].'12');
        }
        if (!empty($_GET['os_month'])) {
            $condition['os_month'] = intval($_GET['os_month']);
        }
        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('ob_no','ob_store_name'))) {
            $condition[$_REQUEST['qtype']] = $_REQUEST['query'];
        }
        if (is_numeric($_GET["ob_state"])) {
            $condition['ob_state'] = intval($_GET["ob_state"]);
        }
        if (is_numeric($_GET["ob_no"])) {
            $condition['ob_no'] = intval($_GET["ob_no"]);
        }
        if (is_numeric($_GET["ob_id"])) {
            $condition['ob_id'] = intval($_GET["ob_id"]);
        }
        if ($_GET['ob_store_name'] != ''){
            if ($_GET['jq_query']) {
                $condition['ob_store_name'] = $_GET['ob_store_name'];
            } else {
                $condition['ob_store_name'] = array('like',"%{$_GET['ob_store_name']}%");
            }
        }
        $sort_fields = array('ob_id','ob_start_date','ob_end_date','ob_order_totals','ob_shipping_totals','ob_commis_totals','ob_order_return_totals','ob_commis_return_totals','ob_store_cost_totals','ob_result_totals','ob_create_date','ob_state','ob_store_id');
        if (in_array($_REQUEST['sortorder'],array('asc','desc')) && in_array($_REQUEST['sortname'],$sort_fields)) {
            $order = $_REQUEST['sortname'].' '.$_REQUEST['sortorder'];
        } else {
            $order = 'ob_id desc';
        }
        return array($condition,$order);
    }
}
