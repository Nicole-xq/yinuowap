<?php
/**
 * 商家拍卖管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class store_vendueControl extends SystemControl{
    const EXPORT_SIZE = 1000;


    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('store_vendue.index');
    }



    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_store_vendue = Model('store_vendue');
        $model_area = Model('area');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('store_vendue_id','store_vendue_name','store_id','store_name','store_vendue_logo','add_time','area_id','oz_type','store_vendue_zy','store_apply_state');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];
        $condition['store_type'] = 0;

        //商家拍卖列表
        $store_vendue_list = $model_store_vendue->getStore_vendueSList($condition, $page, $order);


        $data = array();
        $data['now_page'] = $model_store_vendue->shownowpage();
        $data['total_num'] = $model_store_vendue->gettotalnum();

        foreach ($store_vendue_list as $value) {
            $area_info = $model_area->getAreaInfo(array('area_id'=>$value['city_id']));
            $param = array();
            if($value['store_apply_state'] == 10){
                $operation = "<a class='btn orange' href='index.php?act=store_vendue&op=store_vendue_check&store_vendue_id=".$value['store_vendue_id']."'><i class='fa fa-list-alt'></i>审核</a>"  ;
            }elseif($value['store_apply_state'] == 11){
                $operation = "<a class='btn green' href='index.php?act=store_vendue&op=store_vendue_check&store_vendue_id=".$value['store_vendue_id']."'><i class='fa fa-list-alt'></i>查看</a><span class='btn'><em><i class='fa fa-cog'></i>" . L('nc_set') . " <i class='arrow'></i></em><ul><li><a href='index.php?act=store_vendue&op=store_vendue_edit&store_vendue_id=" . $value['store_vendue_id'] . "'>编辑商家拍卖信息</a></li>";
            }else{
                $operation = "<a class='btn green' href='index.php?act=store_vendue&op=store_vendue_check&store_vendue_id=".$value['store_vendue_id']."'><i class='fa fa-list-alt'></i>查看</a>";
            }
            $param['operation'] = $operation;
            $param['store_vendue_id'] = $value['store_vendue_id'];
            $param['store_id'] = $value['store_id'];
            $param['store_name'] = $value['store_name'];
            $param['store_vendue_logo'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getStoreLogo($value['store_avatar']).">\")'><i class='fa fa-picture-o'></i></a>";
            $param['add_time'] = date('Y-m-d', $value['store_time']);
            $param['area_id'] = $area_info['area_name'];
            $param['oz_type'] = $value['oz_type'];
            $param['store_vendue_zy'] = $value['store_zy'];
            if($value['store_apply_state'] == 10){$state = '待审核';}elseif($value['store_apply_state'] == 20){
                $state = '审核成功';
            }elseif($value['store_apply_state'] == 30){
                $state = '审核失败';
            }elseif($value['store_apply_state'] == 40){
                $state = '开启拍卖成功';
            }elseif($value['store_apply_state'] == 11){
                $state = '付款完成';
            }
            $param['store_apply_state'] = $state;

            $data['list'][$value['store_vendue_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    //审核

    public function store_vendue_checkOp(){
        $model_store_vendue = Model('store_vendue');
        $model_area = Model('area');
        $vendue_info = $model_store_vendue->getStore_vendueSInfo(array('store_vendue_id'=>$_GET['store_vendue_id']));
        $area_info = $model_area->getAreaInfo(array('area_id'=>$vendue_info['city_id']));
        $vendue_info['area_name'] = $area_info['area_name'];
        // 查询入驻申请详细信息
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfoByID($vendue_info['store_id']);

        $model_store_joinin = Model('store_joinin');
        $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$store_info['member_id']));

        $check_detail_title = '查看';
        if(in_array(intval($vendue_info['store_apply_state']), array(10))) {
            $check_detail_title = '审核';
        }
        $model_store_class = Model('store_class');
        $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$vendue_info['sc_id']));
        $store_info['vendue_bail'] = $sc_info['vendue_bail'];
        Tpl::output('store_info', $store_info);

        Tpl::output('check_detail_title', $check_detail_title);
        Tpl::output('vendue_info', $vendue_info);
        Tpl::output('joinin_detail', $joinin_detail);
        Tpl::output('store_info', $store_info);
        Tpl::showpage('store_vendue.detail');
    }

    /**
     * 审核
     */
    public function store_vendue_verifyOp() {
        $model_store_vendue = Model('store_vendue');
        $vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_vendue_id'=>$_POST['store_vendue_id']));
        switch (intval($vendue_info['store_apply_state'])) {
            case 10:
                $this->store_vendue_verify_pass();
                break;
            default:
                showMessage('参数错误','');
                break;
        }
    }

    private function store_vendue_verify_pass() {
        $param = array();
        $param['store_apply_state'] = $_POST['verify_type'] === 'pass' ? 20 : 30;
        $param['check_message'] = $_POST['check_message'];
        $param['store_class_auction_commis_rates'] = implode(',', $_POST['auction_commis_rates']);
        $model_store_vendue = Model('store_vendue');
        $result = $model_store_vendue->editStore_vendue($param,array('store_vendue_id'=>$_POST['store_vendue_id']));
        if ($param['store_apply_state'] == 20) {
            unset($param);
            $param['store_class_auction_commis_rates'] = implode(',', $_POST['auction_commis_rates']);
            // 更新店铺申请加入表
            $model_store_joinin = Model('store_joinin');
            $model_store_joinin->modify($param, array('member_id'=>$_POST['member_id']));
        }
        if ($result) {
            showMessage('商家拍卖申请审核完成','index.php?act=store_vendue&op=index');
        }
    }


    /**
     * 商家拍卖编辑
     */
    public function store_vendue_editOp(){

        $model_store_vendue = Model('store_vendue');
        $model_area = Model('area');
        $vendue_info = $model_store_vendue->getStore_vendueSInfo(array('store_vendue_id'=>$_GET['store_vendue_id']));
        // 查询入驻申请详细信息
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfoByID($vendue_info['store_id']);

        $model_store_joinin = Model('store_joinin');
        $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$store_info['member_id']));

        //保存
        if (chksubmit()){
            $update_array = array();
            $update_array['is_pay'] = $_POST['is_pay'];
            if($_POST['is_pay'] == 1){
                $update_array['store_apply_state'] = 40;
            }

            $result = $model_store_vendue->editStore_vendue($update_array, array('store_vendue_id' => $_POST['store_vendue_id']));
            // 更新店铺绑定分类拍品佣金比例
            $model_store_bind_class = Model('store_bind_class');
            $store_bind_class = unserialize($joinin_detail['store_class_ids']);
            $store_class_auction_commis_rates = explode(',', $joinin_detail['store_class_auction_commis_rates']);
            for($i=0, $length=count($store_bind_class); $i<$length; $i++) {
                list($class1, $class2, $class3) = explode(',', $store_bind_class[$i]);
                $condition = array('class_3' => $class3, 'store_id' => $store_info['store_id']);
                $update = array('auction_commis_rate' => $store_class_auction_commis_rates[$i]);
                $result = $model_store_bind_class->editStoreBindClass($update, $condition);
            }

            if ($result){
                showMessage('编辑成功','index.php?act=store_vendue&op=index');
            }else {
                showMessage('编辑失败');
            }
        }

        $area_info = $model_area->getAreaInfo(array('area_id'=>$vendue_info['city_id']));
        $vendue_info['area_name'] = $area_info['area_name'];
        Tpl::output('vendue_info', $vendue_info);
        $model_store_class = Model('store_class');
        $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$vendue_info['sc_id']));
        $store_info['vendue_bail'] = $sc_info['vendue_bail'];
        Tpl::output('store_info', $store_info);
        Tpl::output('joinin_detail', $joinin_detail);
        Tpl::showpage('store_vendue.edit');
    }




}
