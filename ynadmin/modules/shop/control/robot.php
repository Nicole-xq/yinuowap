<?php
/**
 * 机器人账号设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class robotControl extends SystemControl{
    public function __construct(){
        parent::__construct();
    }

    public function indexOp(){
        Tpl::showpage('robot.list');
    }

    /**
     * 输出列表XML数据
     */
    public function get_robot_xmlOp() {
        $model_robot = Model('robot');

        $page = intval($_POST['rp']);
        if ($page < 1) {
            $page = 15;
        }
        $condition = array();
        if ($_POST['qtype'] == 'robot_name') {
            $condition[$_POST['qtype']] = array('like', '%' . trim($_POST['query']) . '%');
        }
        $list = $model_robot->getList($condition,$page);
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $fields_array = array('robot_name');
            foreach ($list as $k => $v){
                $out_array = getFlexigridArray(array(),$fields_array,$v);
                //$operation = '<a class="btn blue" href="index.php?act=robot&op=edit&id='.$v['robot_id'].'" target="_blank"><i class="fa fa-pencil-square-o"></i>编辑</a>';
                //$out_array['operation'] = $operation;
                $out_array['update_time'] = date('Y-m-d H:i:s',$v['update_time']);
                $out_list[$v['robot_id']] = $out_array;
            }
        }

        $data = array();
        $data['now_page'] = $model_robot->shownowpage();
        $data['total_num'] = $model_robot->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 新增
     */
    public function addOp() {
        $model_robot = Model('robot');
        if (chksubmit()) {
            $array = array();
            $array['robot_name'] = $_POST['robot_name'];
            $array['description'] = $_POST['description'];
            $array['update_time'] = time();

            $state = $model_robot->add($array);
            if ($state) {
                showMessage(Language::get('nc_common_save_succ'),'index.php?act=robot');
            } else {
                showMessage(Language::get('nc_common_save_fail'));
            }
        }
        Tpl::showpage('robot.add');
    }
    public function check_nameOp(){
                $model_robot = Model('robot');
                $condition['robot_name'] = $_GET['robot_name'];
                $array = $model_robot->getInfo($condition);
                if (!empty($array)){
                    exit('false');
                }else {
                    exit('true');
                }
                break;
    }

}
