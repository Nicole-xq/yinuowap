<?php
/**
 * 拍品保证金订单
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/14 0014
 * Time: 15:12
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class margin_listControl extends SystemControl
{
    /**
     * 每次导出订单数量
     * @var int
     */
    const EXPORT_SIZE = 200;

    public function __construct()
    {
        parent::__construct ();
        Language::read('trade');
    }

    /**
     * 订单列表
     * */
    public function indexOp()
    {
        //显示支付接口列表(搜索)
        $payment_list = Model('payment')->getPaymentOpenList();
        $payment_list['wxpay'] = array(
            'payment_code' => 'wxpay',
            'payment_name' => '微信支付'
        );
        Tpl::output('payment_list',$payment_list);
        Tpl::showpage('margin_list.index');
    }

    /**
     * 获取表格内容
     * */
    public function get_xmlOp()
    {
        $model_margin_orders = Model('margin_list');
        $condition  = array();

        $order="margin_list_id desc";

        // 获取数据列表
        $order_list = $model_margin_orders->getOrderList($condition,'*','',$order,0,$_POST['rp']);
        $data = array();
        $data['now_page'] = $model_margin_orders->shownowpage();
        $data['total_num'] = $model_margin_orders->gettotalnum();
        foreach ($order_list as $key => $order_info) {
            $operation = "<a class='btn blue' href='index.php?act=margin_list&op=edit&id=".$order_info['margin_list_id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $list['operation']=$operation;
            $list['margin_list_id']=$order_info['margin_list_id'];
            $list['list_sn']=$order_info['list_sn'];
            $list['buyer_id']=$order_info['buyer_id'];
            $list['buyer_name']=$order_info['buyer_name'];
            $list['all_money']=$order_info['all_money'];
            $list['pay_num']=$order_info['pay_num'];
            $list['last_payment_code']=$order_info['last_payment_code'];
            $list['last_pay_time']=date('Y-m-d H:i:s',$order_info['last_pay_time']);
            $condition2['auction_id']=$order_info['auction_id'];
            $auctions = Model("")->table("auctions")->where($condition2)->find();
            $auction_special_info = Model('auction_special')->getSpecialInfo(array('special_id'=>$auctions['special_id']));
            $list['auction_name']=$auctions['auction_name'];
            $list['special_name']=$auction_special_info['special_name'];
            $data['list'][$order_info['margin_list_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
    public function editOp(){
        $order_state=array(
            "0"=>"未支付",
            "1"=>"已支付",
            "2"=>"提交线下支付凭证",
            "3"=>"线上支付部分支付",
            "4"=>"取消支付",
            "5"=>"支付失败"
        );
        $refund_state=array(
            "0"=>"未退款",
            "1"=>"已退款",
            "2"=>"已扣除",
        );
        $lock_state = array(
            "0"=>"未锁",
            "1"=>"锁定",
        );
        $id=$_GET['id'];
        $map['margin_list_id']=$id;
        $info=Model("")->table("margin_list")->where($map)->find();
        $map2['buyer_id']=$info['buyer_id'];
        $map2['auction_id']=$info['auction_id'];

        $list=Model("")->table("margin_orders")->where($map2)->select();

        foreach ($list as $key => $value) {
            $list[$key]['order_state']=$order_state[$value['order_state']];
            $list[$key]['refund_state']=$refund_state[$value['refund_state']];
            $list[$key]['lock_state']=$lock_state[$value['lock_state']];
        }
        Tpl::output('info',$info);
        Tpl::output('list',$list);
        Tpl::showpage('margin_list.edit');
    }
}
?>