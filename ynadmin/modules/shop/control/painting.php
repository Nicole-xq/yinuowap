<?php
/**
 * 书画首页
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class paintingControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('painting.index');
    }

    /**
     * 新增
     */
    public function addOp(){

        $model_paint_tag = Model('painting_tag');
        $paint_tag_list = $model_paint_tag->getpainting_tagList(array());
        Tpl::output('paint_tag_list', $paint_tag_list);
        $tag_name = $_GET['tag_name'];
        $model_paint_module = Model('paint_module');
        $paint_module_id = intval($_GET['paint_module_id']);
        $paint_module_info = $model_paint_module->getpaint_moduleInfo(array('paint_module_id'=>$paint_module_id));
        Tpl::output('paint_module_info', $paint_module_info);
        $auction_list = array();

            $rec_list = Model('paint_auction_rec')->getPaintRecTagList(array('paint_module_id'=>$paint_module_id,'tag_name'=>$tag_name),'','','*','','auction_id');
            if (!empty($rec_list)) {
                $auction_list = Model('auctions')->getAuctionList(array('auction_id'=>array('in',array_keys($rec_list))),'auction_name,auction_id,auction_image');
                if (!empty($auction_list)) {
                    foreach ($auction_list as $k => $v) {
                        $auction_list[$k]['auction_image'] = cthumb($v['auction_image'],240,$v['store_id']);
                    }
                }
            }


        Tpl::output('goods_list_json',json_encode($auction_list));
        Tpl::output('goods_list', $auction_list);
        Tpl::output('rec_info', is_array($rec_list) ? current($rec_list) : array());

        $paint_module_list= $model_paint_module->getpaint_moduleList(array('paint_module_show'=>1));
        Tpl::output('paint_module_list', $paint_module_list);

        Tpl::showpage('painting.add');
    }

    /**
     * 保存
     */
    public function saveOp(){
        $tag_name = $_POST['tag_name'];
        $module_title_id = $_POST['paint_module_title'];

        if (!chksubmit()) {
            showMessage('非法提交');
        }
        $model_rec = Model('paint_auction_rec');
        $del = $model_rec->delPaintRec(array('tag_name' => $tag_name,'module_title_id'=>$module_title_id));
        if (!$del) {
            showMessage('保存失败');
        }

        $data = array();
        if (is_array($_POST['goods_id_list'])) {
            foreach ($_POST['goods_id_list'] as $k => $auction_id) {
                $data[$k]['tag_name'] = $_POST['tag_name'];
                $data[$k]['module_title_id'] = $_POST['paint_module_title'];
                $data[$k]['auction_id'] = $auction_id;
            }
        }
        $insert = $model_rec->addPaintRec($data);
        if ($insert) {
            showMessage('保存成功','index.php?act=painting&op=index');
        }
    }

    public function get_xmlOp(){
        $model_rec = Model('paint_auction_rec');
        $condition  = array();
        $sort_fields = array('paint_rec_id');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like',"%{$_POST['query']}%");
        }
        $total_num = $model_rec->getPaintRecCount($condition,'distinct tag_name');
        $rec_list = $model_rec->getPaintRecTagList($condition,$_POST['rp'],$order,'count(*) as rec_count,tag_name,paint_module_name,module_title_id,min(paint_rec_id) as paint_rec_id','tag_name','',$total_num);

        $data = array();
        $data['now_page'] = $model_rec->shownowpage();
        $data['total_num'] = $total_num;
        foreach ($rec_list as $v) {
            $list = array();
            $list['operation'] = "<a class='btn red' onclick=\"fg_delete({$v['paint_rec_id']},'{$v['tag_name']}')\"><i class='fa fa-trash-o'></i>删除</a><a class='btn blue' href='index.php?act=painting&op=add&tag_name={$v['tag_name']}&paint_module_id={$v['module_title_id']}'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $list['rec_title'] = $v['paint_module_name'];
            $list['rec_tag'] = $v['tag_name'];
            $list['rec_count'] = $v['rec_count'];
            $data['list'][$v['paint_rec_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 删除
     */
    public function deleteOp() {
        $model_rec = Model('paint_auction_rec');
        $condition = array();
        $del_id = $_GET['del_id'];
        $tag_name = $_GET['tag_name'];
        $rec_info = $model_rec->getPaintRecInfo(array('paint_rec_id'=>$del_id));
        $condition['module_title_id'] = $rec_info['module_title_id'];
        $condition['tag_name'] = $tag_name;
        $del = $model_rec->delPaintRec($condition);
        if (!$del){
            $this->log('删除书画推荐商品失败',0);
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }else{
            $this->log('成功删除书画推荐商品',1);
            exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
        }
    }

    public function get_goods_listOp(){
        $model_auctions = Model('auctions');
        $condition = array();
        if (!empty($_GET['goods_name'])) {
            $condition['auction_name'] = array('like',"%{$_GET['goods_name']}%");
        }
        $condition['state'] = array('in',array(0,1));
        $goods_list = $model_auctions->getAuctionList($condition,'*','','','',8);
        $html = "<ul class=\"dialog-goodslist-s2\">";
        foreach($goods_list as $v) {
            $url = urlVendue('auctions', 'index', array('id' => $v['auction_id']));
            $img = cthumb($v['auction_image'],240,$v['store_id']);
            $html .= <<<EOB
            <li>
            <div class="goods-pic" onclick="select_recommend_goods({$v['auction_id']});">
            <span class="ac-ico"></span>
            <span class="thumb size-72x72">
            <i></i>
            <img width="72" src="{$img}" goods_name="{$v['auction_name']}" goods_id="{$v['auction_id']}" title="{$v['auction_name']}">
            </span>
            </div>
            <div class="goods-name">
            <a target="_blank" href="{$url}">{$v['auction_name']}</a>
            </div>
            </li>
EOB;
        }
        $admin_tpl_url = ADMIN_TEMPLATES_URL;
        $html .= '<div class="clear"></div></ul><div id="pagination" class="pagination">'.$model_auctions->showpage().'</div><div class="clear"></div>';
        $html .= <<<EOB
        <script>
        $('#pagination').find('.demo').ajaxContent({
                event:'click',
                loaderType:"img",
                loadingMsg:"{$admin_tpl_url}/images/transparent.gif",
                target:'#show_recommend_goods_list'
            });
        </script>
EOB;
        echo $html;
    }




}


