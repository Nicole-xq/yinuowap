<?php
/**
 * 分类管理
 *
 * @author 汪继君 <675429469@qq.com>
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_specControl extends SystemControl{
	/**
     * 分类管理
     */
    public function indexOp(){
        Tpl::showpage('auctions_spec.index');
    }
    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = $_POST['query'];
        }
        $order = '';
        $param = array('auctions_spec_id', 'auctions_spec_fid');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $condition['order'] = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page   = new Page();
        $page->setEachNum($_POST['rp']);
        $page->setStyle('admin');
        //列表
        $class_list = Model('auctions_spec')->getList($condition, $page);
        $data = array();
        $data['now_page'] = $page->get('now_page');
        $data['total_num'] = $page->get('total_num');
        foreach ((array)$class_list as $value) {
            $param = array();
            $operation = "<a class='btn red' href='javascript:void(0);' onclick='fg_del(". $value['auctions_spec_id'] .")'><i class='fa fa-trash-o'></i>删除</a><a class='btn blue' href='index.php?act=auctions_spec&op=auctions_spec_edit&c_id=".$value['auctions_spec_id']."'><i class='fa fa-pencil-square-o'></i>编辑</a><a class='btn blue' href='index.php?act=auctions_spec&op=auctions_spec_class_list&id=".$value['auctions_spec_id']."'><i class='fa fa-pencil-square-o'></i>下级属性</a>";
            $param['operation'] = $operation;
            $param['auctions_spec_id'] = $value['auctions_spec_id'];
            $param['auctions_spec_name'] = $value['auctions_spec_name'];            
         	if ($value['auctions_class_id_lv_1']) {
         	   	$first_class=Model('auctions_class')->getOneById($value['auctions_class_id_lv_1']);
         	   	if ($first_class) {
         	   		$param['auctions_class_name_lv_1']=$first_class['auctions_class_name'];
         	   	}else{
         	   		$param['auctions_class_name_lv_1']="---";
         	   	}
         	}else{
         	   		$param['auctions_class_name_lv_1']="---";
         	}   
			if ($value['auctions_class_id_lv_2']) {
         	   	$second_class=Model('auctions_class')->getOneById($value['auctions_class_id_lv_2']);
         	   	if ($first_class) {
         	   		$param['auctions_class_name_lv_2']=$second_class['auctions_class_name'];
         	   	}else{
         	   		$param['auctions_class_name_lv_2']="---";
         	   	}
         	}else{
         	   		$param['auctions_class_name_lv_2']="---";
         	}   


            $data['list'][$value['auctions_spec_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function auctions_spec_addOp(){
    	$class_type = Model('auctions_spec');

        if (chksubmit()){
            $class_array['auctions_spec_name']   = $_POST['auctions_spec_name'];
            $class_array['auctions_class_id_lv_1'] = $_POST['auctions_class_id_lv_1'];
            $class_array['auctions_class_id_lv_2'] = $_POST['auctions_class_id_lv_2'];
            $class_id=$class_type->add($class_array);
            if ($class_id) {
            	$this->log('添加规格'.'['.$_POST['auctions_spec_name'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_spec&op=index');
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }
        // 一级分类列表
        $gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select();;
        //print_r($gc_list);exit;
        Tpl::output('gc_list', $gc_list);

        Tpl::showpage('auctions_spec.add');
           
    }
    public function auctions_spec_editOp(){
    	$class_type = Model('auctions_spec');
    	$class=Model('auctions_spec')->getOneById($_GET['c_id']);
    	//print_r($class);
    	//print_r($class);exit;
    	Tpl::output('class',$class);
        if (chksubmit()){
			$class_array['auctions_spec_name']   = $_POST['auctions_spec_name'];
            $class_array['auctions_class_id_lv_1'] = $_POST['auctions_class_id_lv_1'];
            $class_array['auctions_class_id_lv_2'] = $_POST['auctions_class_id_lv_2'];
            //print_r($class_array);exit;
            $class_id=$class_type->updates($class_array,$_POST['auctions_spec_id']);
            if ($class_id) {
            	$this->log('更新分类'.'[ID:'.$_POST['auctions_spec_id'].']',null);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_spec&op=index');
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }
        // 一级分类列表
        $gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select(); 
        //print_r($gc_list);exit;
        Tpl::output('gc_list', $gc_list);
        // 二级分类列表
        $gc_list2 = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>$class['auctions_class_id_lv_1']))->select();
        //print_r($gc_list);exit;
        Tpl::output('gc_list2', $gc_list2);
        Tpl::showpage('auctions_spec.edit');
    }
    public function auctions_spec_delOp(){
    	$id=$_GET['id'];
    	if(empty($id)) {
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
        //echo $id;exit;
        $class_type = Model('auctions_spec');
        $class=Model('auctions_spec')->getOneById($id);

        $return = $class_type->del($id);

        if ($return) {
        	$this->log('删除分类'.'[ID:'.$id.']',1);
            exit(json_encode(array('state'=>true,'msg'=>L('type_index_del_succ'))));
        }else{
            //$this->log(L('nc_delete,type_index_type_name').'[ID:'.$id.']',0);
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
    }
    public function auctions_spec_class_optionOp(){
    	$id=$_GET['id'];
    	//echo $id;exit;
    	$gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>$id))->select();
    	Tpl::output('gc_list', $gc_list);
        Tpl::showpage('auctions_spec.class_option');
    }
    public function get_auctions_spec_class_optionOp(){
        $id=$_GET['id'];
        $id2=$_GET['id2'];
        $spec_class=array();
        $model = Model('auctions_spec');
        $map['auctions_class_id_lv_1']=$id;
        $map['auctions_class_id_lv_2']=$id2;
        $gc_list = Model('auctions_spec')->table('auctions_spec')->where($map)->select();
        foreach ($gc_list as $key => $value) {
            $list = Model('')->table('auctions_spec_class')->where(array('auctions_spec_id'=>$value['auctions_spec_id']))->select();
            if (!empty($list)) {
                $spec_class[$value['auctions_spec_id']]=$list;
            }else{
                unset($gc_list[$key]);
            }
        }
        Tpl::output('gc_list', $gc_list);
        Tpl::output('spec_class', $spec_class);
        Tpl::showpage('auctions_spec.class_option_get');
    }
    public function auctions_spec_class_listOp(){
        $model = Model('auctions_spec_class');
        $id = intval($_GET['id']);
        $info=Model('auctions_spec')->getOneById($id);
        //print_r($info);exit;
        //列表
        $list = Model('')->table('auctions_spec_class')->where(array('auctions_spec_id'=>$id))->select();

        $title = '"' . $info['auctions_spec_name'] . '"的属性';
        $deep = 2;
        Tpl::output('deep', $deep);
        Tpl::output('title', $title);
        Tpl::output('id', $id);
        Tpl::output('list',$list);
        Tpl::showpage('auctions_spec.class_list');

    }
    public function auctions_spec_class_addOp(){
    	$model = Model('auctions_spec_class');
        $id = intval($_GET['auctions_spec_id']);
        $info=Model('auctions_spec')->getOneById($id);
        //print_r($info);
		Tpl::output('info', $info);

		if (chksubmit()){
            $class_array['auctions_spec_class_name']   = $_POST['auctions_spec_class_name'];
            $class_array['auctions_spec_id'] = $id;
            $class_id=$model->add($class_array);
            if ($class_id) {
            	$this->log('添加属性'.'['.$_POST['auctions_spec_class_name'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_spec&op=auctions_spec_class_list&id='.$id);
            }else{
            	showMessage(Language::get('nc_common_op_fail'));
            }
        }

    	Tpl::showpage('auctions_spec_class.add');
    }
    public function ajaxOp(){
    	$model = Model('auctions_spec_class');
    	$id=intval($_GET['id']);
    	$info=$model->getOneById($id);
    	if ($info) {
    		$class_array[$_GET['branch']]   = $_GET['value'];
	        //print_r($class_array);exit;
	        $class_id=$model->updates($class_array,$id);
	        $return = true;
    	}else{
			$return = false;
    	}
    	exit(json_encode(array('result'=>$return)));
    }
    public function auctions_spec_class_delOp(){
    	$id=$_GET['id'];
    	print_r($_GET);

    	if(empty($id)) {
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }
        //echo $id;exit;
        $model = Model('auctions_spec_class');
        $class=$model->getOneById($id);

        $return = $model->del($id);
        //echo $return;
        if ($return) {
        	$this->log('删除分类属性'.'[ID:'.$id.']',1);
            exit(json_encode(array('state'=>true,'msg'=>L('type_index_del_succ'))));
        }else{
            $this->log(L('nc_delete,type_index_type_name').'[ID:'.$id.']',0);
            exit(json_encode(array('state'=>false,'msg'=>L('param_error'))));
        }


    }
}