<?php
/**
 * 拍卖商品订单
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/14 0014
 * Time: 15:17
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auction_orderControl extends SystemControl
{
    private $links = array(
        array('url'=>'act=auction_order','text'=>'订单列表'),
        array('url'=>'act=auction_order&op=refund_all','text'=>'退款订单'),
        array('url'=>'act=auction_order&op=unShipped','text'=>'待发货'),
        array('url'=>'act=auction_order&op=shipped','text'=>'已发货'),
    );
    /**
     * 每次导出订单数量
     * @var int
     */
    const EXPORT_SIZE = 1000;
    public function __construct()
    {
        parent::__construct ();
        Language::read('trade');
        Tpl::output('top_link',$this->sublink($this->links,$_GET['op']));
    }
    
    public function indexOp(){
        //显示支付接口列表(搜索)
        /** @var paymentModel $payment */
        $payment = Model('payment');
        $payment_list = $payment->getPaymentOpenList();
        $payment_list['wxpay'] = array(
            'payment_code' => 'wxpay',
            'payment_name' => '微信支付'
        );
        Tpl::output('payment_list',$payment_list);
        Tpl::showpage('auction_order.index');
    }

    public function get_xmlOp(){
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition  = array();

        $this->_get_condition($condition);
        if (isset($_REQUEST['qtype']) && $_REQUEST['qtype'] == 'auction_name' && !empty($_REQUEST['query'])) {
            $auctionWhere = [
                'auction_name' => ['like',"%{$_REQUEST['query']}%"]
            ];
            /** @var auctionsModel $auctionInfo */
            $auctionList = Model('auctions')->where($auctionWhere)->limit(20)->select();
            if (!empty($auctionList)) {
                $condition['auction_id'] = [
                    'in',
                    array_column($auctionList, 'auction_id')
                ];
            }
        }
        //$condition['store_id']=0;
        $condition['order_type']=4;
        $sort_fields = array('buyer_name','store_name','order_id','payment_code','order_state','order_amount','order_from','payment_time','finnshed_time','buyer_id','store_id');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
        if (isset($_GET['order_type'])) {
            if ($_GET['order_type'] == 'shipped') {
                //已发货--状态已发货
                $condition['order_state'] = ORDER_STATE_SEND;
                $order = 'payment_time desc';
            } elseif ($_GET['order_type'] = 'unShipped') {
                //待发货--状态已付款
                $condition['order_state'] = ORDER_STATE_PAY;
                $order = 'payment_time desc';
            }
        }
        $order_list = $model_order->getOrderList($condition,$_POST['rp'],'*',$order);
        $data = array();
        $data['now_page'] = $model_order->shownowpage();
        $data['total_num'] = $model_order->gettotalnum();

        $login_auction_order = Logic('auction_order');

        foreach ($order_list as $k => $order_info) {
            $auction_info = Model('auctions')->getAuctionsInfoByID($order_info['auction_id'], 'goods_id,auction_name,warehouse_location');
            $order_info['state_desc'] = orderState($order_info);
            $order_info['if_system_receive_pay'] = $login_auction_order->getOrderOperateState('system_receive_pay', $order_info);
            $order_info['if_system_underline_confirm'] = $login_auction_order->getOrderOperateState('system_underline_confirm', $order_info);
            $list = array();
            $operation_detail = '';
            if ($order_info['refund_state'] == 0 || $order_info['refund_state'] == 1) {
                $list['operation'] = "<a class=\"btn green\" href=\"index.php?act=auction_order&op=show_order&order_id={$order_info['order_id']}\"><i class=\"fa fa-list-alt\"></i>查看</a>";
            } else {
                if ($order_info['refund_state'] == 3) {
                    $list['operation'] = "<a class=\"btn orange\" href=\"index.php?act=auction_order&op=refund_order_edit&order_id={$order_info['order_id']}\"><i class=\"fa fa-gavel\"></i>处理</a>";
                } else {
                    $list['operation'] = "<a class=\"btn green\" href=\"index.php?act=auction_order&op=refund_order_show&order_id={$order_info['order_id']}\"><i class=\"fa fa-list-alt\"></i>查看</a>";
                }
            }

            if ($order_info['if_system_receive_pay']) {
                $op_name = $order_info['system_receive_pay_op_name'] ? $order_info['system_receive_pay_op_name'] : '收到货款';
                $operation_detail .= "<li><a href=\"index.php?act=auction_order&op=change_state&state_type=receive_pay&order_id={$order_info['order_id']}\">{$op_name}</a></li>";
            }
            if ($order_info['if_system_underline_confirm']) {
                $operation_detail .= "<li><a href=\"index.php?act=auction_order&op=change_state&state_type=ud_confirm&order_id={$order_info['order_id']}\">确认凭据</a></li>";
            }
            $pay_detail = '';
            if ($order_info['order_state'] == 10 && ($order_info['add_time']+3600*48)<TIMESTAMP && $order_info['refund_state'] != 2) {
                $operation_detail .= "<li><a href=\"javascript:void(0);\" onclick=\"pay_time_48({$order_info['order_id']})\">买家违约</a></li>";
                $pay_detail = " <i class='fa fa-bell-o' title='超过48小时未支付'></i>";
            }
            if ($order_info['order_state'] == 20 && ($order_info['payment_time']+3600*72)<TIMESTAMP && $order_info['refund_state'] != 2) {
                $operation_detail .= "<li><a href=\"javascript:void(0);\" onclick=\"pay_time_72({$order_info['order_id']})\">商家违约</a></li>";
                $pay_detail = " <i class='fa fa-bell-o' title='超过3天未发货'></i>";
            }
            if ($order_info['order_state']==20&&$order_info['refund_state']==0) {
                $operation_detail .= "<li><a href=\"index.php?act=auction_order&op=shipping_code&order_id={$order_info['order_id']}\">发货</a></li>";
            }
            if ($operation_detail) {
                $list['operation'] .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>{$operation_detail}</ul>";
            }
            $list['order_sn'] = $pay_detail.$order_info['order_sn'];
            $list['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
            $list['add_times'] = date('Y-m-d H:i:s',$order_info['add_time']);
			$list['order_amount'] = ncPriceFormat($order_info['order_amount']);
			if (!empty($order_info['api_pay_amount']) && $order_info['api_pay_amount'] != 0) {
                $list['order_amount'] .= " ({$order_info['api_pay_amount']})";
            }
			$list['order_state'] = $order_info['state_desc'];
			$list['payment_code'] = orderPaymentName($order_info['payment_code']);
			$list['pay_sn'] = $order_info['pay_sn'];
			//$list['payment_code'] = $order_info['payment_name'];
			$list['payment_time'] = !empty($order_info['payment_time']) ? date('Y-m-d H:i:s',$order_info['payment_time']) : '';
			$list['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
			$list['goods_id'] = $auction_info['goods_id'];
			$list['auction_name'] = $auction_info['auction_name'];
			$list['warehouse_location'] = $auction_info['warehouse_location'];
			$list['store_id'] = $order_info['store_id'];
			$list['store_name'] = $order_info['store_name'];
			$list['buyer_id'] = $order_info['buyer_id'];
			$list['buyer_name'] = $order_info['buyer_name'];
			$data['list'][$order_info['order_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }
    public function shipping_codeOp(){
        $order_id = intval($_GET['order_id']);
        if (chksubmit()){
            $update_order['shipping_code']=$_POST['shipping_code'];
            $re1=Model("")->table("orders")->where(array('order_id'=>$order_id))->update($update_order);
            //echo $re1;
            $update_order2['shipping_time']=time();
            $update_order2['shipping_express_id']=$_POST['shipping_express_id'];
            $re2=Model("")->table("order_common")->where(array('order_id'=>$order_id))->update($update_order2);
            if ($re1 && $re2) {
                Model("")->table("orders")->where(array('order_id'=>$order_id))->update(array('order_state'=>30, 'delay_time'=>time()));
                $this->log('发货订单ID'.'['.$order_id.']',1);
                showMessage(Language::get('nc_common_op_succ'),'index.php?act=auction_order&op=index');
            }else{
                showMessage(Language::get('nc_common_op_fail'));
            }
        }
        $order_info = Model("")->table("orders")->where(array('order_id'=>$order_id))->find();
        $auction_info = Model("")->table("auctions")->where(array('auction_id'=>$order_info['auction_id']))->find();
        $list=Model("")->table("express")->select();
        //print_r($list);
        Tpl::output('list',$list);
        Tpl::output('auction_info',$auction_info);
        Tpl::showpage('order.shipping_code');
    }

    /*
     * 全部退款订单
     * */
    public function refund_allOp()
    {
        Tpl::showpage('auction_order.refund');
    }


    /**
     * 待发货订单列表
     * @Date: 2019/5/11 0011
     * @author: Mr.Liu
     */
    public function shippedOp()
    {
        Tpl::showpage('auction_order.shipped');
    }

    /**
     * 已发货订单列表
     * @Date: 2019/5/11 0011
     * @author: Mr.Liu
     */
    public function unShippedOp()
    {
        Tpl::showpage('auction_order.unshipped');
    }


    /*
     * 获取全部退款订单
     * */
    public function get_refund_orderOp()
    {
        $model_order = Model('order');
        $condition  = array(
            'refund_state' => array('in', '2,3'),
        );

        $this->_get_condition($condition);

        $sort_fields = array('buyer_name','store_name','order_id','payment_code','order_state','order_amount','order_from','payment_time','finnshed_time','buyer_id','store_id');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
        //dd($_REQUEST);
        $order_list = $model_order->getOrderList($condition,$_POST['rp'],'*',$order);
        $data = array();
        $data['now_page'] = $model_order->shownowpage();
        $data['total_num'] = $model_order->gettotalnum();

        $login_auction_order = Logic('auction_order');

        foreach ($order_list as $k => $order_info) {
            //$order_info['if_system_receive_pay'] = $model_order->getOrderOperateState('system_receive_pay',$order_info);
            //$order_info['if_system_underline_confirm'] = $model_order->getOrderOperateState('system_underline_confirm',$order_info);
            $order_info['state_desc'] = orderState($order_info);
            $order_info['if_system_receive_pay'] = $login_auction_order->getOrderOperateState('system_receive_pay', $order_info);
            $order_info['if_system_underline_confirm'] = $login_auction_order->getOrderOperateState('system_underline_confirm', $order_info);
            $list = array();
            if ($order_info['refund_state'] == 2) {
                $list['operation'] = "<a class=\"btn green\" href=\"index.php?act=auction_order&op=refund_order_show&order_id={$order_info['order_id']}\"><i class=\"fa fa-list-alt\"></i>查看</a>";
            } else {
                $list['operation'] = "<a class=\"btn orange\" href=\"index.php?act=auction_order&op=refund_order_edit&order_id={$order_info['order_id']}\"><i class=\"fa fa-gavel\"></i>处理</a>";
            }
            $pay_detail = '';
            $list['auction_order_sn'] = $pay_detail.$order_info['auction_order_sn'];
            $list['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
            $list['add_times'] = date('Y-m-d H:i:s',$order_info['add_time']);
            $list['order_amount'] = ncPriceFormat($order_info['order_amount']);
            $list['order_state'] = $order_info['state_desc'];
            $list['payment_code'] = orderPaymentName($order_info['payment_code']);
            $list['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
            $list['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
            $list['auction_name'] = $order_info['auction_name'];
            $list['store_id'] = $order_info['store_id'];
            $list['store_name'] = $order_info['store_name'];
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];
            $data['list'][$order_info['auction_order_id']] = $list;
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 平台订单状态操作
     *
     */
    public function change_stateOp() {
        $order_id = intval($_GET['order_id']);
        $model_order = Model('order');

        //获取订单详细
        $condition = array();
        $condition['order_id'] = $order_id;
        $order_info = $model_order->getOrderInfo($condition);
        if ($_GET['state_type'] == 'receive_pay') {
            $result = $this->_order_receive_pay($order_info,$_POST);
        } elseif ($_GET['state_type'] == 'ud_confirm') {
             $result = $this->_order_ud_confirm($order_info,$_POST);
        }
        if (!$result['state']) {
            showMessage($result['msg'],$_POST['ref_url'],'html','error');
        } else {
            showMessage($result['msg'],$_POST['ref_url']);
        }
    }

    /**
     * 系统收到货款
     * @throws Exception
     */
    private function _order_receive_pay($order_info, $post) {

        $logic_auction_order = Logic('auction_order');

        $order_info['if_system_receive_pay'] = $logic_auction_order->getOrderOperateState('system_receive_pay',$order_info);
        if (!$order_info['if_system_receive_pay']) {
            return callback(false,'无权操作');
        }

        if (!chksubmit()) {
            Tpl::output('order_info',$order_info);
            //显示支付接口列表
            $payment_list = Model('payment')->getPaymentOpenList();
            //去掉预存款和货到付款
            foreach ($payment_list as $key => $value){
                if ($value['payment_code'] == 'predeposit' || $value['payment_code'] == 'offline') {
                    unset($payment_list[$key]);
                }
            }
            Tpl::output('payment_list',$payment_list);
            Tpl::showpage('auction_order.receive_pay');
            exit();
        }

        // 修改订单状态 20
        $result = $logic_auction_order->changeAuctionOrderReceivePay($order_info,'admin',$this->admin_info['name'],$post);
        if ($result['state']) {
            $this->log('将订单改为已收款状态,'.L('order_number').':'.$order_info['auction_order_sn'],1);
            //记录消费日志
            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];
            QueueClient::push('addConsume', array('member_id'=>$order_info['buyer_id'],'member_name'=>$order_info['buyer_name'],
                'consume_amount'=>$api_pay_amount,'consume_time'=>TIMESTAMP,'consume_remark'=>'管理员更改订单为已收款状态，订单号：'.$order_info['auction_order_sn']));
        }
        return $result;

    }

      /**
     * 系统确认凭据
     * @throws Exception
     */
    private function _order_ud_confirm($order_info, $post) {
        $logic_auction_order = Logic('auction_order');

        $order_info['if_system_underline_confirm'] = $logic_auction_order->getOrderOperateState('system_underline_confirm',$order_info);
        if (!$order_info['if_system_underline_confirm']) {
            return callback(false,'无权操作');
        }

        if (!chksubmit()) {
            Tpl::output('order_info',$order_info);
            Tpl::showpage('auction_order.ud_confirm');
            exit();
        }

        $post['payment_code'] = 'underline';

        // 修改订单状态 20
        $result = $logic_auction_order->changeAuctionOrderReceivePay($order_info,'admin',$this->admin_info['name'],$post);

        if ($result['state']) {
            $this->log('将订单改为已收款状态,'.L('order_number').':'.$order_info['auction_order_sn'],1);
            //记录消费日志
            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];
            QueueClient::push('addConsume', array('member_id'=>$order_info['buyer_id'],'member_name'=>$order_info['buyer_name'],
                'consume_amount'=>$api_pay_amount,'consume_time'=>TIMESTAMP,'consume_remark'=>'管理员更改订单为已收款状态，订单号：'.$order_info['auction_order_sn']));
        }
        return $result;
    }

    /**
     * 买家违约
     *
     */
    public function pay_time_48Op(){
        $order_id = intval($_GET['order_id']);
        $model_order    = Model('auction_order');
        $order_info = $model_order->getInfo(array('auction_order_id'=>$order_id));
        if ($order_info['order_state'] == 10 && ($order_info['add_time']+3600*48)<TIMESTAMP) {
            $margin_amount = $order_info['margin_amount'];
                if ($margin_amount>0) {
                    Model('member')->editMember(
                        array('member_id' => $order_info['buyer_id']),
                        array(
                            'freeze_margin' => array('exp','freeze_margin-'.$margin_amount)
                        )
                    );
                    Logic('auction_order')->MarginLog($order_info['buyer_id'], $order_info['buyer_name'], 5, $order_info['margin_amount'], $order_info['auction_order_sn']);
                }
            $result = $model_order->setOrder(array('auction_order_id'=>$order_id),array('close_time'=> TIMESTAMP,'order_state'=> 0,'close_reason'=> '48小时内没有支付尾款，平台没收保证金'));
        }
        if ($result) {
            exit(json_encode(array('state'=>true,'msg'=>'操作成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'操作失败')));
        }
    }

    /**
     * 商家违约
     *
     */
    public function pay_time_72Op(){
        $order_id = intval($_GET['order_id']);
        $model_order    = Model('auction_order');
        $order_info = $model_order->getInfo(array('auction_order_id'=>$order_id));
        if ($order_info['order_state'] == 20 && ($order_info['payment_time']+3600*72)<TIMESTAMP) {
            $order_amount = $order_info['order_amount'];
                if ($order_amount>0) {
                    $log_array = array();
                    $log_array['member_id'] = $order_info['buyer_id'];
                    $log_array['member_name'] = $order_info['buyer_name'];
                    $log_array['order_sn'] = $order_info['auction_order_sn'];
                    $log_array['amount'] = $order_amount;
                    Model('predeposit')->changePd('auction_refund72', $log_array);
                }
            $result = $model_order->setOrder(array('auction_order_id'=>$order_id),array('close_time'=> TIMESTAMP,'order_state'=> 0,'close_reason'=> '商家3天内未发货，平台退款'));
        }
        if ($result) {
            exit(json_encode(array('state'=>true,'msg'=>'操作成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'操作失败')));
        }
    }

    /**
     * 查看订单
     *
     */
    public function show_orderOp(){
        $order_id = intval($_GET['order_id']);
        $model_order    = Model('order');
        $order_info = $model_order->getOrderInfo(array('order_id'=>$order_id));

        //如果订单已取消，取得取消原因、时间，操作人
        if ($order_info['order_state'] == ORDER_STATE_CANCEL) {
            $order_info['close_info'] = $model_order->getOrderLogInfo(array('order_id' => $order_info['order_id']), 'log_id desc');
        }
        //追加返回订单扩展表信息
        $extend_order_common = $model_order->getOrderCommonInfo(array('order_id'=>$order_info['order_id']));
        $order_info['reciver_name'] = $extend_order_common['reciver_name'];
        $order_info['buyer_address'] = unserialize($extend_order_common['reciver_info']);
        $order_info['ship_time']=$extend_order_common['shipping_time'];
        $express=Model('express')->where(array('id'=>$extend_order_common['shipping_express_id']))->find();
        //print_r($express);
        $order_info['express_info']['e_name']=$express['e_name'];
        //print_r($order_info);
        //print_r($extend_order_common);
        //商家信息
        $store_info = Model('store')->getStoreInfo(array('store_id'=>$order_info['store_id']));
        Tpl::output('store_info',$store_info);

        //拍卖商品信息
        $auction_info = Model('auctions')->getAuctionsInfoByID($order_info['auction_id'], $fields = '*');

        Tpl::output('auction_info',$auction_info);
        Tpl::output('order_info',$order_info);

        Tpl::showpage('auction_order.view');
    }
    
    /*
     * 编辑退款订单
     * */
    public function refund_order_editOp()
    {
        $order_id = intval($_GET['order_id']);
        $model_order    = Model('auction_order');
        $order_info = $model_order->getInfo(array('auction_order_id'=>$order_id));
        if (chksubmit()) {
            // 进行退款
            if ($order_info['refund_state'] != '3') {//检查状态,防止页面刷新不及时造成数据错误
                showMessage('保存失败');
            }
            $logic_auction_refund = Logic('auction_refund');
            // 执行站内退款
            $result = $logic_auction_refund->auction_refund($order_info, $_POST['admin_message']);
            if ($result['state']) {
                showMessage($result['msg'],'index.php?act=auction_order&op=index');
            } else {
                showMessage($result['msg']);
            }
        }
        Tpl::output('order_info',$order_info);
        Tpl::showpage('auction_refund.edit');
    }

    /*
     * 查看退款订单
     * */
    public function refund_order_showOp()
    {
        $order_id = intval($_GET['order_id']);
        $model_order    = Model('auction_order');
        $order_info = $model_order->getInfo(array('auction_order_id'=>$order_id));
        Tpl::output('order_info',$order_info);
        Tpl::showpage('auction_refund.view');
    }

    /**
     * 导出
     *
     */
    public function export_step1Op(){
        $lang   = Language::getLangContent();

        $model_order = Model('order');
        $condition  = array();
        if (preg_match('/^[\d,]+$/', $_GET['order_id'])) {
            $_GET['order_id'] = explode(',',trim($_GET['order_id'],','));
            $condition['order_id'] = array('in',$_GET['order_id']);
        }
        $this->_get_condition($condition);
        $sort_fields = array('buyer_name','store_name','order_id','payment_code','order_state','order_amount','order_from','payment_time','finnshed_time','buyer_id','store_id','warehouse_location');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        } else {
            $order = 'order_id desc';
        }

        if (!is_numeric($_GET['curpage'])){
            $count = $model_order->getOrderCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=auction_order&op=index');
                Tpl::showpage('export.excel');
            }else{  //如果数量小，直接下载
                $data = $model_order->getOrderList($condition,self::EXPORT_SIZE);
                $this->createExcel($data);
            }
        }else{  //下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $data = $model_order->getOrderList($condition,self::EXPORT_SIZE,'*',$order,"{$limit1},{$limit2}");
            $this->createExcel($data);
        }
    }

    /**
     * 生成excel
     *
     * @param array $data
     */
    private function createExcel($data = array()){
        Language::read('export');
        import('libraries.excel');
        $excel_obj = new Excel();
        $excel_data = array();
        //设置样式
        $excel_obj->setStyle(array('id'=>'s_title','Font'=>array('FontName'=>'宋体','Size'=>'12','Bold'=>'1')));
        //header
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单编号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单来源');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'下单时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单金额(元)');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单状态');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'支付方式');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'支付时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单完成时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'商品ID');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'商品');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'商品库位号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'店铺ID');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'店铺名称');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'买家ID');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'买家账号');
        //data
        foreach ((array)$data as $k=>$order_info){
            $auction_info = Model('auctions')->getAuctionsInfoByID($order_info['auction_id'], 'goods_id,auction_name,warehouse_location');
            $order_info['state_desc'] = orderState($order_info);
            $list = array();
            $list['order_sn'] = $order_info['order_sn'];
            $list['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
            $list['add_time'] = date('Y-m-d H:i:s',$order_info['add_time']);
            $list['order_amount'] = ncPriceFormat($order_info['order_amount']);
            $list['order_state'] = $order_info['state_desc'];
            $list['payment_code'] = orderPaymentName($order_info['payment_code']);
            $list['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
            $list['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
            $list['goods_id'] = $auction_info['goods_id'];
            $list['auction_name'] = $auction_info['auction_name'];
            $list['warehouse_location'] = $auction_info['warehouse_location'];
            $list['store_id'] = $order_info['store_id'];
            $list['store_name'] = $order_info['store_name'];
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];

            $tmp = array();
            $tmp[] = array('data'=>$list['order_sn']);
			$tmp[] = array('data'=>$list['order_from']);
            $tmp[] = array('data'=>$list['add_time']);
            $tmp[] = array('data'=>$list['order_amount']);
            $tmp[] = array('data'=>$list['order_state']);
            $tmp[] = array('data'=>$list['payment_code']);
			$tmp[] = array('data'=>$list['payment_time']);
            $tmp[] = array('data'=>$list['finnshed_time']);
            $tmp[] = array('data'=>$list['goods_id']);
            $tmp[] = array('data'=>$list['auction_name']);
            $tmp[] = array('data'=>$list['warehouse_location']);
            $tmp[] = array('data'=>$list['store_id']);
            $tmp[] = array('data'=>$list['store_name']);
            $tmp[] = array('data'=>$list['buyer_id']);
            $tmp[] = array('data'=>$list['buyer_name']);
            $excel_data[] = $tmp;
        }
        $excel_data = $excel_obj->charset($excel_data,CHARSET);
        $excel_obj->addArray($excel_data);
        $excel_obj->addWorksheet($excel_obj->charset(L('exp_od_order'),CHARSET));
        $excel_obj->generateXML('order-'.$_GET['curpage'].'-'.date('Y-m-d-H',time()));
    }

    /**
     * 处理搜索条件
     */
    private function _get_condition(& $condition) {
        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('order_sn','store_name','buyer_name'))) {
            $condition[$_REQUEST['qtype']] = array('like',"%{$_REQUEST['query']}%");
        }
        if ($_GET['keyword'] != '' && in_array($_GET['keyword_type'],array('order_sn','store_name','buyer_name'))) {
            if ($_GET['jq_query']) {
                $condition[$_GET['keyword_type']] = $_GET['keyword'];
            } else {
                $condition[$_GET['keyword_type']] = array('like',"%{$_GET['keyword']}%");
            }
        }
        $condition['order_type'] = 4;
    }
    public function send_good(){

    }
}