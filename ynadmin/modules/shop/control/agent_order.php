<?php
/**
 * 统计管理（区域代理返佣分析）
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class agent_orderControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        $query = [
            'end_date'=>date('Y-m-d',time()),
            'start_date'=>date('Y-m-01',time()),
        ];
        Tpl::output('query', $query);
        Tpl::showpage('agent_order');
    }


    /**
     * 输出销售收入统计XML数据
     */
    public function get_income_xmlOp(){
        $agent_order_model = Model('agent_order');
        $area_agent_model = Model('area_agent');

        $area_type = $area_agent_model->getAreaType();

        $condition = '';
        $start_date = trim($_GET['query_start_date']);
        $end_date = trim($_GET['query_end_date']);
        $agent_id = intval($_GET['agent_id']) ;
        if ($start_date && $end_date) {
            $condition = " cdate between '".$start_date."' and '".$end_date." 23:59:59'";
            $member_time = ['between',[strtotime($start_date),strtotime($end_date.' 23:59:59')]];
        } elseif ($start_date) {
            $condition = " cdate >= '".$start_date."' ";
            $member_time = ['gt',strtotime($start_date)];
        } elseif ($end_date) {
            $condition = " cdate <= '".$end_date."' ";
            $member_time = ['lt',strtotime($end_date)];
        }
        $condition .= intval($_GET['agent_id']) ? " and agent_id = ".$agent_id : '';
        $page_rows = intval($_POST['rp']);
        $cur_page = intval($_POST['curpage']);
        if ($page_rows < 1) {
            $page_rows = 10;
        }
        $field = 'agent_id,agent_type,province_id,city_id,area_id';
        $list = $agent_order_model->getAgentListBySQL($condition,$field,$cur_page-1,$page_rows);

        $agent_stat = [];
        foreach ($list as $k => $v){
            $row = $area_agent_model->field('agent_name')->where(array('agent_id' => $v['agent_id']))->find();
            $out_array = [
                'agent_id'=>$v['agent_id'],
                'agent_name'=>$row['agent_name'],
                'agent_type'=>$area_type[$v['agent_type']],
                'sum_order_price_1'=>$v['sum_order_price_1'],
                'sum_commission_amount_1'=>$v['sum_commission_amount_1'],
                'sum_order_price_2'=>$v['sum_order_price_2'],
                'sum_commission_amount_2'=>$v['sum_commission_amount_2'],
                'sum_order_price_3'=>$v['sum_order_price_3'],
                'sum_commission_amount_3'=>$v['sum_commission_amount_3'],
                'sum_order_price_4'=>$v['sum_order_price_4'],
                'reg_num'=>$this->count_reg_num($v,$member_time),
                'sum_commission_amount_4'=>$v['sum_commission_amount_4'],
                'sum_order_price_5'=>$v['sum_order_price_5'],
                'sum_commission_amount_5'=>$v['sum_commission_amount_5'],
            ];
            $agent_stat[$v['agent_id']] = $out_array;
        }
        $total_num_obj = $agent_order_model->getCountNum($condition);
        foreach ($total_num_obj as $key => $value) {
            $total_num = $value;
        }
        $data = [
            'now_page'=>$cur_page,
            'total_num'=>$total_num['nums'],
            'list'=>$agent_stat
        ];

        echo Tpl::flexigridXML($data);exit();
    }

    /*
     * 统计注册会员数据
     * @param array $param
     * @param array $member_time
     * @return int
     */
    private function count_reg_num($param,$member_time){
        $reg_where = [
            'member_time'=>$member_time,
        ];
        if ($param['agent_type'] == 3){
            $reg_where['member_provinceid'] = intval($param['province_id']);
            $reg_where['member_cityid'] = intval($param['city_id']);
            $reg_where['member_areaid'] = intval($param['area_id']);
        }elseif ($param['agent_type'] == 2){
            $reg_where['member_provinceid'] = intval($param['province_id']);
            $reg_where['member_cityid'] = intval($param['city_id']);
            $reg_where['member_areaid'] = ['gt',0];
        }elseif ($param['agent_type'] == 1){
            $reg_where['member_provinceid'] = intval($param['province_id']);
            $reg_where['member_cityid'] = ['gt',0];
        }

        return Model('member')->getMemberCount($reg_where);
    }


}
