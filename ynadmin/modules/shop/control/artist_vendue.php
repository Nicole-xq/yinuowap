<?php
/**
 * 艺术家拍卖管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class artist_vendueControl extends SystemControl{
    const EXPORT_SIZE = 1000;


    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('artist_vendue.index');
    }



    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_artist_vendue = Model('artist_vendue');
        $model_store = Model('store');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('artist_vendue_id','artist_name','store_name','artist_job_title','add_time','artist_state');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];

        //艺术家拍卖列表
        $artist_vendue_list = $model_artist_vendue->getArtist_StoreList($condition, $page, $order);


        $data = array();
        $data['now_page'] = $model_artist_vendue->shownowpage();
        $data['total_num'] = $model_artist_vendue->gettotalnum();

        foreach ($artist_vendue_list as $value) {
          $store_info  = $model_store->getStoreInfo(array('store_id'=>$value['store_id']));
            $param = array();
            if($value['store_apply_state'] == 10){
                $operation = "<a class='btn orange' href='index.php?act=artist_vendue&op=artist_vendue_check&artist_vendue_id=".$value['artist_vendue_id']."'><i class='fa fa-list-alt'></i>审核</a>"  ;
            }elseif($value['store_apply_state'] == 11){
                $operation = "<a class='btn green' href='index.php?act=artist_vendue&op=artist_vendue_check&artist_vendue_id=".$value['artist_vendue_id']."'><i class='fa fa-list-alt'></i>查看</a><span class='btn'><em><i class='fa fa-cog'></i>" . L('nc_set') . " <i class='arrow'></i></em><ul><li><a href='index.php?act=artist_vendue&op=artist_vendue_edit&artist_vendue_id=" . $value['artist_vendue_id'] . "'>编辑艺术家拍卖信息</a></li>";
            }else{
                $operation = "<a class='btn green' href='index.php?act=artist_vendue&op=artist_vendue_check&artist_vendue_id=".$value['artist_vendue_id']."'><i class='fa fa-list-alt'></i>查看</a>";
            }
            $param['operation'] = $operation;
            $param['artist_vendue_id'] = $value['artist_vendue_id'];
            $param['artist_name'] = $value['artist_name'];
            $param['store_name'] = $value['store_name'];
            $param['artist_job_title'] = $value['artist_job_title'];
            $param['add_time'] = date('Y-m-d', $store_info['store_time']);
            if($value['store_apply_state'] == 10){$state = '待审核';}elseif($value['store_apply_state'] == 20){
                $state = '审核成功';
            }elseif($value['store_apply_state'] == 30){
                $state = '审核失败';
            }elseif($value['store_apply_state'] == 40){
                $state = '开启拍卖成功';
            }elseif($value['store_apply_state'] == 11){
                $state = '付款完成';
            }
            $param['artist_state'] = $state;

            $data['list'][$value['artist_vendue_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    //审核

    public function artist_vendue_checkOp(){
        $model_artist_vendue = Model('artist_vendue');
        $model_store = Model('store');
        $vendue_info = $model_artist_vendue->getArtist_StoreInfo(array('artist_vendue_id'=>$_GET['artist_vendue_id']));
        $store_info  = $model_store->getStoreInfo(array('store_id'=>$vendue_info['store_id']));
        $check_detail_title = '查看';
        if(in_array(intval($vendue_info['store_apply_state']), array(10))) {
            $check_detail_title = '审核';
        }

        $model_store_joinin = Model('store_joinin');
        $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$store_info['member_id']));

        if(!empty($vendue_info)){
            $vendue_info['artist_represent'] = unserialize($vendue_info['artist_represent']);
            $vendue_info['artist_awards'] = unserialize($vendue_info['artist_awards']);
            $vendue_info['artist_works'] = unserialize($vendue_info['artist_works']);
        }
        $vendue_info['store_time'] = $store_info['store_time'];

        Tpl::output('check_detail_title', $check_detail_title);
        Tpl::output('vendue_info', $vendue_info);
        $model_store_class = Model('store_class');
        $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$store_info['sc_id']));
        $store_info['vendue_bail'] = $sc_info['vendue_bail'];
        Tpl::output('store_info', $store_info);
        Tpl::output('joinin_detail', $joinin_detail);
        Tpl::showpage('artist_vendue.detail');
    }

    /**
     * 审核
     */
    public function artist_vendue_verifyOp() {
        $model_artist_vendue = Model('artist_vendue');
        $vendue_info = $model_artist_vendue->getArtist_StoreInfo(array('artist_vendue_id'=>$_POST['artist_vendue_id']));
        switch (intval($vendue_info['store_apply_state'])) {
            case 10:
                $this->artist_vendue_verify_pass();
                break;
            default:
                showMessage('参数错误','');
                break;
        }
    }

    private function artist_vendue_verify_pass() {
        $param = array();
        $param['store_apply_state'] = $_POST['verify_type'] === 'pass' ? 20 : 30;
        $param['check_message'] = $_POST['check_message'];
        $param['store_class_auction_commis_rates'] = implode(',', $_POST['auction_commis_rates']);
        $model_store_vendue = Model('store_vendue');
        $model_artist_vendue = Model('artist_vendue');
        $artist_vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('artist_vendue_id'=>$_POST['artist_vendue_id']));
        $result = $model_store_vendue->editStore_vendue($param,array('store_vendue_id'=>$artist_vendue_info['store_vendue_id']));
        if ($param['store_apply_state'] == 20) {
            unset($param);
            $param['store_class_auction_commis_rates'] = implode(',', $_POST['auction_commis_rates']);
            // 更新店铺申请加入表
            $model_store_joinin = Model('store_joinin');
            $model_store_joinin->modify($param, array('member_id'=>$_POST['member_id']));
        }
        if ($result) {
            showMessage('艺术家拍卖申请审核完成','index.php?act=artist_vendue&op=index');
        }
    }


    /**
     * 艺术家拍卖编辑
     */
    public function artist_vendue_editOp(){

        $model_artist_vendue = Model('artist_vendue');
        $model_store_vendue = Model('store_vendue');
        $vendue_info = $model_artist_vendue->getArtist_StoreInfo(array('artist_vendue_id'=>$_GET['artist_vendue_id']));
        $model_store = Model('store');
        $store_info  = $model_store->getStoreInfo(array('store_id'=>$vendue_info['store_id']));
        //保存
        if (chksubmit()){
            $update_array = array();
            $update_array['is_pay'] = $_POST['is_pay'];
            if($_POST['is_pay'] == 1){
                $update_array['store_apply_state'] = 40;
            }

            $artist_vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('artist_vendue_id'=>$_POST['artist_vendue_id']));
            $model_store_joinin = Model('store_joinin');
            $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$store_info['member_id']));
            $result = $model_store_vendue->editStore_vendue($update_array, array('store_vendue_id' => $artist_vendue_info['store_vendue_id']));
            // 更新店铺绑定分类拍品佣金比例
            $model_store_bind_class = Model('store_bind_class');
            $store_bind_class = unserialize($joinin_detail['store_class_ids']);
            $store_class_auction_commis_rates = explode(',', $joinin_detail['store_class_auction_commis_rates']);
            for($i=0, $length=count($store_bind_class); $i<$length; $i++) {
                list($class1, $class2, $class3) = explode(',', $store_bind_class[$i]);
                $condition = array('class_3' => $class3, 'store_id' => $store_info['store_id']);
                $update = array('auction_commis_rate' => $store_class_auction_commis_rates[$i]);
                $result = $model_store_bind_class->editStoreBindClass($update, $condition);
            }
            if ($result){
                showMessage('编辑成功','index.php?act=artist_vendue&op=index');
            }else {
                showMessage('编辑失败');
            }
        }

        if(!empty($vendue_info)){
            $vendue_info['artist_represent'] = unserialize($vendue_info['artist_represent']);
            $vendue_info['artist_awards'] = unserialize($vendue_info['artist_awards']);
            $vendue_info['artist_works'] = unserialize($vendue_info['artist_works']);
        }
        $vendue_info['store_time'] = $store_info['store_time'];
        Tpl::output('vendue_info', $vendue_info);
        $model_store_class = Model('store_class');
        $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$store_info['sc_id']));
        $store_info['vendue_bail'] = $sc_info['vendue_bail'];
        Tpl::output('store_info', $store_info);
        Tpl::showpage('artist_vendue.edit');
    }




}
