<?php
/**
 * 趣猜中奖
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class promotion_guess_winnerControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        $this->winner_listOp();
    }

    /*
     * 趣猜中奖列表
     * */
    public function winner_listOp(){
        Tpl::showpage('guess_winner.list');
    }

    /*
     * 中奖列表xml数据
     * */
    public function winner_list_xmlOp(){

        $condition['gs_state'] = array('in',array(2,3));
        // 搜索
        switch ($_REQUEST['qtype']) {
            case 'gs_id':
                $condition['gs_id'] = array('like', '%'.$_REQUEST['query'].'%');
                break;
            case 'gs_name':
                $condition['gs_name'] = array('like', '%'.$_REQUEST['query'].'%');
                break;
            case 'store_name':
                $condition['store_name'] = array('like', '%'.$_REQUEST['query'].'%');
                break;
        }
        // 排序
        $sort = 'gs_id';
        if(!empty($_REQUEST['sortname'])){
            $sort = $_REQUEST['sortname'];
        }
        if ($_REQUEST['sortorder'] != 'asc') {
            $sort .= ' desc';
        }

        //实例
        $model_guess_offer = Model('guess_offer');
        $model_guess = Model('p_guess');
        $model_order = Model('order');
        //用户趣猜关联详情
        $guess_list = $model_guess_offer->get_guess_offer_list($condition, $_REQUEST['rp'], '', $sort);
        $guess_list1 = array_combine(array_column($guess_list,'gs_id'),$guess_list);
        $gs_id = array_column($guess_list,'gs_id');

        //趣猜活动详情
        $act_guess_list = (array) $model_guess->getGuessList1(array('gs_id'=>array('in',$gs_id)),'gs_id,store_name,joined_times','gs_id desc');
        $act_guess_list1 = array_combine(array_column($act_guess_list,'gs_id'),$act_guess_list);
        //订单详情
        $order_list = $model_order->getNormalOrderList(array('gs_id'=>array('in',$gs_id)),'','order_id,gs_id,add_time','gs_id desc');
        $order_list1 = array_combine(array_column($order_list,'gs_id'),$order_list);

        $data = array();
        $data['now_page'] = $model_guess_offer->shownowpage();
        $data['total_num'] = $model_guess_offer->gettotalnum();

        foreach ($guess_list1 as $key=>$val) {
            $i = array();
            $i['operation'] = '<a class="btn" href="javascript:void(0);"><i class=\'fa fa-list-alt\'></i>无订单</a>';
            if(!empty($order_list1[$key])){
                $url = "index.php?act=order&op=show_order&order_id=". $order_list1[$key]['order_id'] ;
                $i['operation'] = '<a class="btn green" href='. $url .'><i class=\'fa fa-list-alt\'></i>查看订单</a>';
            }
            $i['gs_id'] = $val['gs_id'];
            $i['gs_name'] = $val['gs_name'];
            $i['store_name'] = $act_guess_list1[$key]['store_name'];
            $i['joined_times'] = $act_guess_list1[$key]['joined_times'];
            $i['member_id'] = $val['member_id'];
            $i['member_name'] = $val['member_name'];
            $i['gs_offer_price'] = $val['gs_offer_price'];
            $i['end_time'] = date("Y-m-d H:i:s",$val['end_time']);

            $i['gs_state'] = '否';
            $i['order_time'] = ' ';
            if(!empty($order_list1[$key])){
                $i['gs_state'] = '是';
                $i['order_time'] = date("Y-m-d H:i:s",$order_list1[$key]['add_time']);
            }
            $data['list'][$key] = $i;
        }
        echo Tpl::flexigridXML($data);
        exit;
    }
}
