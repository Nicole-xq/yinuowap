<?php
/**
 * 进出账统计
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/01/31
 * Time: 18:08
 */


use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class stat_accountsControl extends SystemControl{
    private $test_member_id = array();
    private $links = array(
        array('url'=>'act=stat_accounts&op=index','lang'=>'stat_in_accounts_top_up'),
        array('url'=>'act=stat_accounts&op=in_accounts_margin','lang'=>'stat_in_accounts_margin'),
        array('url'=>'act=stat_accounts&op=in_accounts_order','lang'=>'stat_in_accounts_order'),
        array('url'=>'act=stat_accounts&op=out_accounts','lang'=>'stat_out_accounts'),
    );

    public function __construct(){
        parent::__construct();
        Language::read('stat');
        $this->test_member_id = C('test_member_id');
    }

    /**
     * 充值进账列表
     * */
    public function indexOp(){
        Tpl::output('top_link',$this->sublink($this->links,'index'));
        Tpl::showpage('stat_accounts.index');
    }

    /**
     * 商品订单进账列表
     * */
    public function in_accounts_orderOp(){
        Tpl::output('top_link',$this->sublink($this->links,'in_accounts_order'));
        Tpl::showpage('stat_accounts.index');
    }

    /**
     * 保证金进账列表
     * */
    public function in_accounts_marginOp(){
        Tpl::output('top_link',$this->sublink($this->links,'in_accounts_margin'));
        Tpl::showpage('stat_accounts.index');
    }

    /**
     * 提现出账列表
     * */
    public function out_accountsOp(){
        Tpl::output('top_link',$this->sublink($this->links,'out_accounts'));
        Tpl::showpage('stat_accounts.index');
    }


    public function get_xmlOp(){
        switch ($_GET['type']) {
            case 'index' :
                $data = $this->_in_accounts_top_up();
                break;
            case 'in_accounts_order' :
                $data = $this->_in_accounts_order();
                break;
            case 'in_accounts_margin' :
                $data = $this->_in_accounts_margin();
                break;
            case 'out_accounts' :
                $data = $this->_out_accounts();
                break;
            default :
                $data = [];
        }
        exit(Tpl::flexigridXML($data));
    }

    /**
     * 商品订单
     * */
    private function _in_accounts_order(){
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition = ['api_pay_amount'=>['gt',0]];
        $param = ['member_name'=>'buyer_name','created_at'=>'add_time','payment_code'=>'payment_code','test_id'=>'buyer_id'];
        $this->_condition($condition,$param);

        $order = 'order_id desc';
        $param = ['order_id','order_sn','buyer_id','buyer_name','payment_code','pdr_amount','add_time','payment_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $order_list = $model_order->getOrderList($condition, $_POST['rp'],'*',$order);

        $data = array();
        $data['now_page'] = $model_order->shownowpage();
        $data['total_num'] = $model_order->gettotalnum();
        foreach ($order_list as $key => $value) {
            $param = array();
            $param['order_id'] = $value['order_id'];
            $param['order_sn'] = $value['order_sn'];
            $param['buyer_id'] = $value['buyer_id'];
            $param['buyer_name'] = $value['buyer_name'];
            $param['payment_code'] = orderPaymentName($value['payment_code']);
            $param['api_pay_amount'] = $value['api_pay_amount'];
            $param['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
            $param['payment_time'] = date('Y-m-d H:i:s',$value['payment_time']);

            $data['list'][] = $param;
        }
        return $data;
    }

    /**
     * 保证金订单
     * */
    private function _in_accounts_margin(){
        $model_margin_orders = Model('margin_orders');
        $condition = ['api_pay_state'=>1];
        $param = ['member_name'=>'buyer_name','created_at'=>'created_at','payment_code'=>'payment_code','test_id'=>'buyer_id'];
        $this->_condition($condition,$param);

        $order = 'margin_id desc';
        $param = ['margin_id', 'order_sn', 'buyer_id', 'buyer_name', 'payment_code', 'api_pay_amount', 'created_at', 'payment_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $margin_orders_list = $model_margin_orders->getMemberOrderList($condition, $_POST['rp'],'*',$order);

        $data = [];
        $data['now_page'] = $model_margin_orders->shownowpage();
        $data['total_num'] = $model_margin_orders->gettotalnum();
        foreach ($margin_orders_list as $key => $value) {
            $param = array();
            $param['margin_id'] = $value['margin_id'];
            $param['order_sn'] = $value['order_sn'];
            $param['buyer_id'] = $value['buyer_id'];
            $param['buyer_name'] = $value['buyer_name'];
            $param['payment_code'] = orderPaymentName($value['payment_code']);
            $param['api_pay_amount'] = $value['api_pay_amount'];
            $param['created_at'] = date('Y-m-d H:i:s', $value['created_at']);
            $param['payment_time'] = date('Y-m-d H:i:s', $value['payment_time']);

            $data['list'][] = $param;
        }
        return $data;
    }

    /**
     * 充值
     * */
    private function _in_accounts_top_up(){
        $model_pd = Model('predeposit');
        $condition = ['pdr_payment_state'=>1];
        $param = ['member_name'=>'pdr_member_name','created_at'=>'pdr_add_time','payment_code'=>'pdr_payment_code','test_id'=>'pdr_member_id'];
        $this->_condition($condition,$param);

        $order = 'pdr_id desc';
        $param = ['pdr_id','pdr_sn','pdr_member_id','pdr_member_name','pdr_payment_name','pdr_payment_code','pdr_amount','pdr_add_time','pdr_payment_time'];

        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $recharge_list = $model_pd->getPdRechargeList($condition,$_POST['rp'],'*',$order);

        $data = [];
        $data['now_page'] = $model_pd->shownowpage();
        $data['total_num'] = $model_pd->gettotalnum();
        foreach ($recharge_list as $key => $value) {
            $param = array();
            $param['pdr_id'] = $value['pdr_id'];
            $param['pdr_sn'] = $value['pdr_sn'];
            $param['pdr_member_id'] = $value['pdr_member_id'];
            $param['pdr_member_name'] = $value['pdr_member_name'];
            $param['pdr_payment_code'] = orderPaymentName($value['pdr_payment_code']);
            $param['pdr_amount'] = $value['pdr_amount'];
            $param['pdr_add_time'] = date('Y-m-d H:i:s', $value['pdr_add_time']);
            $param['pdr_payment_time'] = date('Y-m-d H:i:s', $value['pdr_payment_time']);

            $data['list'][] = $param;
        }
        return $data;
    }

    /**
     * 提现
     * */
    private function _out_accounts(){
        $model_pd = Model('predeposit');
        $condition = ['pdc_payment_state'=>1];
        $param = ['member_name'=>'pdc_member_name','created_at'=>'pdc_add_time','test_id'=>'pdc_member_id'];
        $this->_condition($condition,$param);

        $order = 'pdc_id desc';
        $param = ['pdc_id', 'pdc_sn', 'pdc_member_id', 'pdc_member_name', 'pdc_payment_name', 'pdc_amount', 'pdc_add_time', 'pdc_payment_time'];
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $recharge_list = $model_pd->getPdCashList($condition,$_POST['rp'],'*',$order);

        $data = [];
        $data['now_page'] = $model_pd->shownowpage();
        $data['total_num'] = $model_pd->gettotalnum();
        foreach ($recharge_list as $key => $value) {
            $param = array();
                $param['pdc_id'] = $value['pdc_id'];
                $param['pdc_sn'] = $value['pdc_sn'];
                $param['pdc_member_id'] = $value['pdc_member_id'];
                $param['pdc_member_name'] = $value['pdc_member_name'];
                $param['pdc_bank_name'] = $value['pdc_bank_name'];
                $param['pdc_amount'] = $value['pdc_amount'];
                $param['pdc_add_time'] = date('Y-m-d H:i:s', $value['pdc_add_time']);
                $param['pdc_payment_time'] = date('Y-m-d H:i:s', $value['pdc_payment_time']);

            $data['list'][] = $param;
        }
        return $data;
    }

    /**
     * 输出平台总数据
     */
    public function get_plat_incomeOp(){
        $model_pd = Model('predeposit');

        // 提现金额
        $condition = ['pdc_payment_state'=>1];
        $param = ['member_name'=>'pdc_member_name','created_at'=>'pdc_add_time','test_id'=>'pdc_member_id'];
        $this->_condition($condition,$param);
        $pd_cash_sum = $model_pd->getPdCashSum($condition,'pdc_amount');

        // 充值金额
        $condition = ['pdr_payment_state'=>1];
        $param = ['member_name'=>'pdr_member_name','created_at'=>'pdr_add_time','payment_code'=>'pdr_payment_code','test_id'=>'pdr_member_id'];
        $this->_condition($condition,$param);
        $pd_recharge_sum = $model_pd->getPdRechargeSum($condition,'pdr_amount');

        // 保证金金额
        $condition = ['api_pay_state'=>1];
        $param = ['member_name'=>'buyer_name','created_at'=>'created_at','payment_code'=>'payment_code','test_id'=>'buyer_id'];
        $this->_condition($condition,$param);
        $margin_orders_sum = Model('margin_orders')->getMarginSum($condition,'api_pay_amount');

        // 商品订单金额
        $condition = ['order_state'=>['in',[0,20,30,40]]];
        $param = ['member_name'=>'buyer_name','created_at'=>'add_time','payment_code'=>'payment_code','test_id'=>'buyer_id'];
        $this->_condition($condition,$param);
        $orders_sum = Model('order')->getPdSum($condition,'api_pay_amount');

        echo '<div class="title"><h3>进出账总统计</h3></div>';
        echo '<dl class="row"><dd class="opt"><ul class="nc-row">';
        echo '<li title="总进账金额：'. number_format($pd_recharge_sum + $margin_orders_sum + $orders_sum,2).'元"><h4>总进账</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.($pd_recharge_sum + $margin_orders_sum + $orders_sum).'"></h2><h6>元</h6></li>';
        echo '<li title="总出账金额：'. number_format($pd_cash_sum,2).'元"><h4>总出账</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$pd_cash_sum.'"></h2><h6>元</h6></li>';
        echo '<li title="充值：'. (number_format($pd_recharge_sum,2)).'元"><h4>充值</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'. $pd_recharge_sum.'"></h2><h6>元</h6></li>';
        echo '<li title="保证金：'. number_format($margin_orders_sum,2).'元"><h4>保证金</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'. $margin_orders_sum.'"></h2><h6>元</h6></li>';
        echo '<li title="商品：'. number_format($orders_sum,2).'元"><h4>商品</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$orders_sum.'"></h2><h6>元</h6></li>';
        echo '</ul></dd><dl>';
        exit();
    }

    private function _condition(& $condition, $param = []){
        // 搜索
        if ($_POST['qtype'] && $_POST['query']) {
            $condition[$_POST['qtype']] = array('like',"%{$_POST['query']}%");
        }

        // 高级搜素
        if(!empty($this->test_member_id) && !empty($param['test_id'])){
            $condition[$param['test_id']] = ['not in',$this->test_member_id];
        }
        if(isset($param['member_name']) && !empty($param['member_name'])){
            if($_GET['keyword'] && $param['member_name']){
                $condition[$param['member_name']] = array('like',"%{$_GET['keyword']}%");
            }
        }

        if(isset($param['created_at']) && !empty($param['created_at'])){
            if (trim($_GET['query_start_date']) && trim($_GET['query_end_date'])) {
                $sdate = strtotime($_GET['query_start_date']);
                $edate = strtotime($_GET['query_end_date']);
                if($sdate > $edate){
                    $sdate += 86399;
                    $condition[$param['created_at']] = array('between', "$edate,$sdate");
                }else{
                    $edate += 86399;
                    $condition[$param['created_at']] = array('between', "$sdate,$edate");
                }
            } elseif (trim($_GET['query_start_date'])) {
                $sdate = strtotime($_GET['query_start_date']);
                $condition[$param['created_at']] = array('egt', $sdate);
            } elseif (trim($_GET['query_end_date'])) {
                $edate = strtotime($_GET['query_end_date']) + 86399;
                $condition[$param['created_at']] = array('elt', $edate);
            }
        }

        if(isset($param['payment_code']) && !empty($param['payment_code'])){
            switch ($_GET['pay_type']){
                case 1:
                    $condition[$param['payment_code']] = ['in',['wxpay','wx_jsapi','wxpay_jsapi','wx_saoma']];
                    break;
                case 2:
                    $condition[$param['payment_code']] = ['in',['alipay','ali_native','alipay_native','AopClient']];
                    break;
                case 3:
                    $condition[$param['payment_code']] = 'lklpay';
                    break;
                case 4:
                    $condition[$param['payment_code']] = 'underline';
                    break;
                default :
                    // ……
            }
        }
    }

    public function get_linksOp(){
        echo $this->sublink($this->links,$_GET['type']);
        exit();
    }
}