<?php
/**
 * 拍品管理
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/3 0003
 * Time: 11:16
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class auctions_goodControl extends SystemControl
{
    //1:拍乐赚 2:新手专区 3:推广拍品 4:捡漏拍品
    private $links = array(
        array('url'=>'act=auctions_good&op=index','text'=>'所有拍品'),
        array('url'=>'act=auctions_good&op=add&type=1','text'=>'新增拍乐赚'),
        array('url'=>'act=auctions_good&op=add&type=3','text'=>'新增推广拍品'),
        array('url'=>'act=auctions_good&op=add&type=4','text'=>'新增捡漏拍品')
    );

    const EXPORT_SIZE = 2000;

    public function __construct() {
        parent::__construct ();
        Tpl::output('top_link',$this->sublink($this->links,$_GET['op']));
    }

    public function indexOp() {
        $gc_list = Model('goods_class')->getGoodsClassList(array('gc_parent_id' => 0));
        Tpl::output('gc_list', $gc_list);

        Tpl::showpage('auctions_good.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $condition = array();
//        $condition['store_id']=0;
        if ($_GET['auction_name'] != '') {
            $condition['auction_name'] = array('like', '%' . $_GET['auction_name'] . '%');
        }
        if ($_GET['auction_id'] != '') {
            $condition['auction_id'] = array('like', '%' . $_GET['auction_id'] . '%');
        }
        if ($_GET['store_name'] != '') {
            $condition['store_name'] = array('like', '%' . $_GET['store_name'] . '%');
        }
        if ($_GET['auction_state'] != '') {
            $condition['is_liupai'] = $_GET['auction_state'];
        }
        if ($_GET['state'] != '') {
            switch($_GET['state']){
                case 1://等待预展
                    $condition['auction_preview_start'] = array('gt',time());
                    break;
                case 2://预展中
                    $condition['auction_preview_start'] = array('elt',time());
                    $condition['auction_start_time'] = array('gt',time());
                    break;
                case 3://拍卖中
                    $condition['auction_start_time'] = array('elt',time());
                    $condition['auction_end_time'] = array('gt',time());
                    break;
                case 4://拍卖结束
                    $condition['auction_end_time'] = array('elt',time());
                    break;
                default:
                    break;
            }
        }
        if ($_GET['pay_state'] != '') {

            $condition['state'] = $_GET['state'];
        }
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('auction_id', 'auction_name', 'auction_preview_start', 'auction_start_time', 'auction_end_time', 'auction_end_true_t', 'state', 'auction_image', 'gc_id'
        , 'gc_name', 'store_id', 'store_name', 'is_own_shop', 'auction_add_time', 'auction_bond', 'auction_start_price', 'auction_type'
        , 'auction_increase_range', 'auction_reserve_price', 'auction_number', 'delivery_mechanism', 'number_of_applicants'
        , 'set_reminders_number', 'click_volume', 'warehouse_location'
        );
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];

        switch ($_GET['type']) {
//            // 禁售
//            case 'lockup':
//                $auctions_list = $model_auctions->getAuctionsLockUpList($condition, '*', $page, $order);
//                break;
//            // 等待审核
//            case 'waitverify':
//                $auctions_list = $model_auctions->getAuctionsWaitVerifyList($condition, '*', $page, $order);
//                break;
            // 新手拍品
            case 'novice':
                $condition['auction_type'] = '1';
                $auctions_list = $model_auctions->getAuctionSpecialList($condition, '*', '', $order, 0 ,$page);
                break;
            // 普通拍品
            case 'normal':
                $condition['auction_type'] = '0';
                $auctions_list = $model_auctions->getAuctionSpecialList($condition, '*', '', $order, 0 ,$page);
                break;
            // 全部拍品
            default:
                $auctions_list = $model_auctions->getAuctionList($condition, '*', '', $order, 0 ,$page);
                break;
        }
//dd();
        // 拍品状态
//        $auctions_state = $this->getAuctionsState();

		$model_auction_special = Model('auction_special');
        $data = array();
        $data['now_page'] = $model_auctions->shownowpage();
        $data['total_num'] = $model_auctions->gettotalnum();
        foreach ($auctions_list as $value) {
            $param = array();
//            $operation = '';
            $operation = '<a class=\'btn red\' target="_blank" href="' .
                getMainConfig('auction_url') . "/auctionCommodityDetails?auction_id=" . $value['auction_id'] . '">预览</a>';
//            $operation .= "<a class='btn red' href='javascript:void(0);' onclick=\"fg_lonkup('" . $value['auction_id'] . "')\"><i class='fa fa-ban'></i>结束拍卖</a>";
            if ($value['recommended'] == '1') {
                $operation .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions_good', 'auctions_rec', array(
                            'auction_id' => $value['auction_id'],
                            'rec' => 0,
                        )) . '">取消推荐</a>';
            } else {
                $operation .= '<a class=\'btn red\' href="javascript:;" data-href="' . urlAdminShop('auctions_good', 'auctions_rec', array(
                            'auction_id' => $value['auction_id'],
                            'rec' => 1,
                        )) . '">推荐拍品</a>';
            }
            $order_info = array();
            $order_info = Model('order')->getOrderInfo(array('auction_id'=>$value['auction_id']));
            $operation .= "<span class='btn'><em><i class='fa'></i>更多 <i class='arrow'></i></em><ul>";
            $operation .= "<li><a href='" . urlAdminShop('auctions_good', 'auctions_show', array('auction_id' => $value['auction_id'])) . "' target=\"_blank\">查看拍品详细</a></li>";
			if (!$value['auction_start_time'] || $value['auction_start_time'] >= time() || empty($value['special_id'])) {
            	$operation .= "<li><a href='" . urlAdminShop('auctions_good', 'auctions_edit', array('auction_id' => $value['auction_id'])) . "'>编辑拍品信息</a></li>";
            } else {
            	$operation .= "<li><a href='" . urlAdminShop('auctions_good', 'auctions_edit2', array('auction_id' => $value['auction_id'])) . "'>编辑拍品信息</a></li>";
            }
            if (!empty($order_info)) {
            	$operation .= "<li><a href='" . urlAdminShop('auction_order', 'show_order', array('order_id' => $order_info['order_id'])) . "'>订单详情</a></li>";
            }
            $operation .= "<li><a href='" . urlAdminShop('auctions_good', 'auction_copy', array('auction_id' => $value['auction_id'])) . "'>拍品复制</a></li>";
            if ($this->admin_info['gid'] == 1) {
                $operation .= "<li><a href='" . urlAdminShop('auctions_good', 'set_reserve_price', array('auction_id' => $value['auction_id'])) . "'>设置保留价</a></li>";
            }
            $operation .= "</ul>";
            $param['operation'] = $operation;
            $param['auction_id'] = $value['auction_id'];
            $param['auction_image'] = "<a href='javascript:void(0);' class='pic-thumb-tip' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".cthumb($value['auction_image'],'60','0').">\")'><i class='fa fa-picture-o'></i></a>";
            $param['auction_name'] = $value['auction_name'];
            $param['auction_type_text'] = \App\Models\ShopncAuction::$auctionTypeText[intval($value['auction_type'])];
            $lv1=Model('auctions_class')->getOneById($value['auctions_class_lv1']);
			$lv2=Model('auctions_class')->getOneById($value['auctions_class_lv2']);
			$param['class']=$lv1['auctions_class_name']."-".$lv2['auctions_class_name'];
            $param['auction_start_time'] = $value['auction_start_time'] ? date('Y-m-d H:i:s', $value['auction_start_time']) : '未设置';
			if ($value['special_id']) {
				$param['spec_type']="已关联";
        		$special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$value['special_id']));
        		$param['warehouse_location']=$value['warehouse_location'];
        		$param['special_name']=$special_info['special_name'];
        		$param['state']= $this->get_auction_state($value);
			}else{
				$param['spec_type']="未关联";
				$param['warehouse_location']="---";
				$param['special_name']="---";
				$param['state']= "---";
			}
            $param['auction_bond'] = ncPriceFormat($value['auction_bond']);
            if($value['auction_bond_rate']>0){
                $param['auction_bond'] .= '(利息'.ncPriceFormat($value['auction_bond_rate']).'%)';
            }
            $param['auction_start_price'] = ncPriceFormat($value['auction_start_price']);
            $param['auction_cost'] = ncPriceFormat($value['auction_cost']);
            $param['auction_increase_range'] = ncPriceFormat($value['auction_increase_range']);
            if (empty($value['auction_reserve_price']) || $value['auction_reserve_price'] == 0) {
                $param['auction_reserve_price'] = "<font color='red'>未设置<font>";
            } elseif ($this->admin_info['gid'] == 1) {
                $param['auction_reserve_price'] = $value['auction_reserve_price'] ?: 0;
            } else {
                $param['auction_reserve_price'] = "<font color='green'>已设置<font>";
            }
            $param['order_amount'] = '';
            $param['buyer_name'] = '';
            $param['pay_state'] = '未售出';
            
            if(!empty($order_info)){
                $param['order_amount'] = $order_info['order_amount'];
                $param['buyer_name'] = $order_info['buyer_name'];
                $param['pay_state'] = $order_info['payment_time']?'已付款':'未付款';
            }


            $data['list'][$value['auction_id']] = $param;
            $data['recommended'] = $value['recommended'];
        }
       echo Tpl::flexigridXML($data);exit();
    }
	/**
     * 拍品状态
     * @param $info
     * @return string
     */
    private function get_auction_state($info){
        if(time() < $info['auction_preview_start']){
            $re = '等待预展';
        }elseif(time() < $info['auction_start_time']){
            $re = '预展中';
        }elseif(time() < $info['auction_end_time']){
            $re = '拍卖中';
        }else{
            $re = '拍卖结束';
        }
        return $re;
    }

    public function addOp(){
        $type = $_REQUEST['type'];
    	$gc_list = Model('auctions_class')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select();
		Tpl::output('gc_list', $gc_list);
		if (chksubmit()){
		    $goodsId = $_POST['goods_id'];
		    $goodsInfo = Model('')->table('goods')->where(['goods_id' => $goodsId])->find();
            if ($goodsInfo['goods_state'] == 3) {
                showMessage(Language::get('nc_common_op_fail'));
            }
			$array['goods_id'] = $array['goods_common_id'] = $goodsId;
			$array['auction_name'] = $goodsInfo['goods_name'];
			$array['store_id'] = $goodsInfo['store_id'];
			$array['store_name'] = $goodsInfo['store_name'];
			$array['auction_bond'] = isset($_POST['auction_bond']) ? $_POST['auction_bond'] : 0;
			$array['auction_bond_rate'] = isset($_POST['auction_bond_rate']) ? $_POST['auction_bond_rate'] : 0;
			$array['interest_last_date'] = isset($_POST['interest_last_date']) ? $_POST['interest_last_date'] : '';
			$array['auction_type'] = $_POST['auction_type'];
			$array['auction_start_price'] = $array['current_price'] = isset($_POST['auction_start_price'])
                ? $_POST['auction_start_price'] : 0.00;
			$array['auction_increase_range'] = isset($_POST['auction_increase_range'])
                ? $_POST['auction_increase_range'] : 0;
            $array['auction_reference_price'] = isset($_POST['auction_reference_price'])
             ? $_POST['auction_reference_price'] : 0;
            $array['auction_reserve_price'] = isset($_POST['auction_reserve_price'])
             ? $_POST['auction_reserve_price'] : 0;
            $array['auction_cost'] = isset($_POST['auction_cost'])
             ? $_POST['auction_cost'] : 0;
            $array['delivery_mechanism'] = $goodsInfo['store_name'];
            $array['warehouse_location'] = isset($_POST['warehouse_location']) ? $_POST['warehouse_location'] : '';
            //分享描述
            $array['share_content']=isset($_POST['share_content'])
             ? $_POST['share_content'] : '';
            $array['auction_image']=$goodsInfo['goods_image'];
            $array['auction_video']=$goodsInfo['video_url'];
            //是否包邮快递费用
            $array['free_post']=isset($_POST['free_post'])
             ? $_POST['free_post'] : 1;
            $array['express_fee']=isset($_POST['express_fee'])
             ? $_POST['express_fee'] : 0.00;
			//其他数据填写
			$array['auction_add_time']=time();
			$array['auction_start_time']=strtotime($_POST['auction_start_time']);
			$array['auction_preview_start']=isset($_POST['auction_preview_start'])
                ? strtotime($_POST['auction_preview_start']) : 0;
            $config=Model("auction_config")->getInfo(1);
            $array['duration_time']=$config['duration_time'];
			if (!empty($_POST['auction_end_time'])) {
                $array['auction_end_time']=$array['auction_end_true_t']=strtotime($_POST['auction_end_time']);
            } else {
                $array['auction_end_time']=$array['auction_end_true_t']=$array['auction_start_time']+$array['duration_time'];
            }
			$id=Model("")->table('auctions')->insert($array);
			//上拍后拍品锁定
			Model("")->table('goods')->where(['goods' => $goodsId])->update(['goods_state' => 3]);
			//echo $sql;die();
			//die();
			if ($id) {
				$this->log('添加拍品'.'['.$_POST['auction_name'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_good&op=index');
            }else{
            	/*print_r($array);
            	die();*/
            	showMessage(Language::get('nc_common_op_fail'));
            }
		}
		//输出搜索产品和ID
        $condition['goods_state'] = array('neq',3);
        $search_goods_list = Model('goods')->table('goods')->where($condition)->field('goods_id,goods_name')->order('goods_id desc')->limit(50)->select();
        Tpl::output('search_goods_list', $search_goods_list);
		switch ($type)
		{
		    //拍乐赚
		    case '1':
                Tpl::showpage('auctions_normal_good.add');
		    break;
            //新手专区
            case '2':
                Tpl::showpage('auctions_newbie_good.add');
                break;
		    //推广拍品
		    case '3':
                Tpl::showpage('auctions_rate_good.add');
		    break;
            //捡漏拍品
            case '4':
                Tpl::showpage('auctions_picker_good.add');
                break;
		    default:
                Tpl::showpage('auctions_good.add');
            break;
		}
    }
    /**
     * 推荐
     */
    public function auctions_recOp()
    {
        $model= Model('auctions');
        $update_array['recommended'] = $_GET['rec'] == '1' ? 1 : 0;
        $where_array['auction_id'] = $_GET['auction_id'];
        $result = $model->editAuctions($update_array, $where_array);

        if ($result) {
            $this->jsonOutput();
        } else {
            $this->jsonOutput('操作失败');
        }
    }
    public function auctions_showOp(){
    	$auction_id=$_GET['auction_id'];
    	$condition['auction_id']=$auction_id;
    	$info = Model("")->table("auctions")->where($condition)->find();
        $info['auction_type_text'] = \App\Models\ShopncAuction::$auctionTypeText[intval($info['auction_type'])];
    	$goods['goods_image']=$info['auction_image'];
    	$goods['store_id']=0;
    	$info['img_path']=thumb($goods,'360');
    	$imgArr=json_decode($info['other_images']);
    	if (!empty($imgArr)) {
    		foreach ($imgArr as $key => $value) {
    			$goods['goods_image']=$value;
    			$info[$key]=thumb($goods,'360');
    		}
    	}
    	$info['class1']=Model('auctions_class')->getOneById($info['auctions_class_lv1']);
    	$info['class2']=Model('auctions_class')->getOneById($info['auctions_class_lv2']);
    	$specArr=json_decode($info['auctions_spec'],true);
    	$info['specArr']=array();
    	foreach ($specArr as $key => $value) {
    		$spec=Model('auctions_spec')->getOneById($key);
    		$spec_class=Model('auctions_spec_class')->getOneById($value);
    		$info['specArr'][$spec['auctions_spec_name']]=$spec_class['auctions_spec_class_name'];
    	}
    	$admin_info = $this->admin_info;
		Tpl::output('admin_info', $admin_info);
		Tpl::output('info', $info);
		//print_r($info);
        Tpl::showpage('auctions_good.show');
    }
    public function auctions_editOp(){
        $auctionInfo = Model("")->table('auctions')->where(array('auction_id'=>$_REQUEST['auction_id']))->find();
        if ((empty($auctionInfo) || $auctionInfo['auction_start_time'] <= time()) && !empty($value['special_id'])) {
            showMessage('拍卖已经开始 无法更改 !');
        }
    	if (chksubmit()){
    		//$array['auction_id'] = $_POST['auction_id'];
			$array['auction_name'] = $_POST['auction_name'];
			$array['store_name'] = $_POST['store_name'];
			$array['auction_bond'] = $_POST['auction_bond'];
			$array['auction_start_price'] = $_POST['auction_start_price'];
            $array['current_price']=$_POST['auction_start_price'];
			$array['auction_increase_range'] = $_POST['auction_increase_range'];
			$array['auction_reserve_price'] = $_POST['auction_reserve_price'];
            $array['auction_cost'] = $_POST['auction_cost'];
            $array['auction_reference_price'] = $_POST['auction_reference_price'];
            $array['auction_type'] = $_POST['auction_type'];
			$array['delivery_mechanism'] = $_POST['delivery_mechanism'];
			$array['warehouse_location'] = $_POST['warehouse_location'];
			//图片处理
			//上传配置，启用oss
			$store_id = 0;			
			//封面图
			if (!empty($_FILES['auction_image']['name'])) {
				//上传配置，启用oss
				$upload = new UploadFile();
		        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
		        $upload->set('max_size', C('image_max_filesize'));
		        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
		        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
		        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
		        $upload->set('fprefix', $store_id);
		        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
                $result = $upload->upfile('auction_image',true);
                if ($result){
                    $array['auction_image'] = $upload->file_name;
                }else {
                    showMessage($upload->error."1");
                }
            }
            //明细图
            if (!empty($_FILES['auctions_img1']['name'])) {
            	//上传配置，启用oss
            	$upload = new UploadFile();
		        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
		        $upload->set('max_size', C('image_max_filesize'));
		        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
		        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
		        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
		        $upload->set('fprefix', $store_id);
		        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
                $result1 = $upload->upfile('auctions_img1',true);
                if ($result1){
                    $img['auctions_img1'] = $upload->file_name;
                }else {
                    showMessage($upload->error."2");
                }
            }
            if (!empty($_FILES['auctions_img2']['name'])) {
            	//上传配置，启用oss
            	$upload = new UploadFile();
		        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
		        $upload->set('max_size', C('image_max_filesize'));
		        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
		        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
		        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
		        $upload->set('fprefix', $store_id);
		        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
                $result2 = $upload->upfile('auctions_img2',true);
                if ($result2){
                    $img['auctions_img2'] = $upload->file_name;
                }else {
                    showMessage($upload->error."3");
                }
            }
            if (!empty($_FILES['auctions_img3']['name'])) {
            	//上传配置，启用oss
            	$upload = new UploadFile();
		        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
		        $upload->set('max_size', C('image_max_filesize'));
		        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
		        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
		        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
		        $upload->set('fprefix', $store_id);
		        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
                $result3 = $upload->upfile('auctions_img3',true);
                if ($result3){
                    $img['auctions_img3'] = $upload->file_name;
                }else {
                    showMessage($upload->error."4");
                }
            }
            if (!empty($_FILES['auctions_img4']['name'])) {
            	$upload = new UploadFile();
		        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
		        $upload->set('max_size', C('image_max_filesize'));
		        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
		        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
		        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
		        $upload->set('fprefix', $store_id);
		        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
                $result4 = $upload->upfile('auctions_img4',true);
                if ($result4){
                    $img['auctions_img4'] = $upload->file_name;
                }else {
                    showMessage($upload->error."5");
                }
            }
            if ($img && is_array($img)) {
                $array['other_images'] = json_encode($img);
            }
            //图片处理完毕
			$array['auctions_class_lv1'] = $_POST['auctions_class_lv1'];
			$array['auctions_class_lv2'] = $_POST['auctions_class_lv2'];
			//规格处理
			$spec_class=array();
	        $model = Model('auctions_spec');
	        $map['auctions_class_id_lv_1']=$array['auctions_class_lv1'];
	        $map['auctions_class_id_lv_2']=$array['auctions_class_lv2'];
	        $gc_list = Model('auctions_spec')->table('auctions_spec')->where($map)->select();
	        foreach ($gc_list as $key => $value) {
	        	if (!empty($_POST[$value['auctions_spec_id']])) {
	        		$spec_class[$value['auctions_spec_id']]=$_POST[$value['auctions_spec_id']];
	        	}
	        }
	        //print_r($spec_class);die();
	        $array['auctions_spec']=json_encode($spec_class);
			//规格处理结束
			$array['auctions_long'] = $_POST['long'];
			$array['auctions_width'] = $_POST['width'];
			$array['auctions_height'] = $_POST['height'];
			$array['create_age'] = $_POST['create_age'];
            //print_r($_POST['brand_description']);
            $brand_description =str_replace('src=&quot;/data/upload/shop/editor/','src=&quot;http://'.$_SERVER['HTTP_HOST'].'/data/upload/shop/editor/',$_POST['brand_description']);
            //print_r($_SERVER);
            //print_r($brand_description);die();
			$array['auctions_summary'] = htmlspecialchars($brand_description);
			//艺术家
			if ($_POST['auctions_artist_id']) {
				$array['auctions_artist_id'] = $_POST['auctions_artist_id'];
				unset($condition);
				$condition['auctions_artist_id']=$array['auctions_artist_id'];
        		$info = Model("")->table("auctions_artist")->where($condition)->find();
        		$array['auctions_artist_name']=$info['auctions_artist_name'];
			}
            //分享描述
            $array['share_content']=$_POST['share_content'];
			$id=Model("")->table('auctions')->where(array('auction_id'=>$_POST['auction_id']))->update($array);
			//echo $sql;die();
			//die();
			if ($id) {
				$this->log('编辑拍品'.'['.$_POST['auction_id'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_good&op=index');
            }else{
            	/*print_r($array);
            	die();*/
            	showMessage(Language::get('nc_common_op_fail'));
            }
		}
    	$auction_id=$_GET['auction_id'];
    	$condition['auction_id']=$auction_id;
    	$info = Model("")->table("auctions")->where($condition)->find();
    	$goods['goods_image']=$info['auction_image'];
    	$goods['store_id']=0;
    	$info['img_path']=thumb($goods,'60');
    	$imgArr=json_decode($info['other_images']);
    	if (!empty($imgArr)) {
    		foreach ($imgArr as $key => $value) {
    			$goods['goods_image']=$value;
    			$info[$key]=thumb($goods,'60');
    		}
    	}
    	
    	//一级分类
    	$gc_list = Model('')->table('auctions_class')->where(array('auctions_class_fid'=>0))->select();
		Tpl::output('gc_list', $gc_list);
		//二级分类
		$gc_list2 = Model('')->table('auctions_class')->where(array('auctions_class_fid'=>$info['auctions_class_lv1']))->select();
		Tpl::output('gc_list2', $gc_list2);
		$specArr=json_decode($info['auctions_spec'],true);
		$info['specArr']=$specArr;
		foreach ($specArr as $key => $value) {
			//规格
			$spec[$key]=Model('auctions_spec')->getOneById($key);
			//属性
			$spec_class[$key] = Model('')->table('auctions_spec_class')->where(array('auctions_spec_id'=>$key))->select();
		}		
		//艺术家
		$conditiona['auctions_artist_id']=$info['auctions_artist_id'];
        $artist = Model("")->table("auctions_artist")->where($conditiona)->find();
        //print_r($artist);
        $info['url']=UPLOAD_SITE_URL.DS.ATTACH_ARTIST.DS.$artist['auctions_artist_img'];
        $admin_info = $this->admin_info;
        Tpl::output('admin_info', $admin_info);
		Tpl::output('spec', $spec);
		Tpl::output('spec_class', $spec_class);
		Tpl::output('info', $info);
    	Tpl::showpage('auctions_good.edit');
    }
    public function auctions_edit2Op(){
    	if (chksubmit()){
    		//$array['auction_id'] = $_POST['auction_id'];
			$array['auction_click'] = $_POST['auction_click'];
			$array['num_of_applicants'] = $_POST['num_of_applicants'];
			$array['set_reminders_num'] = $_POST['set_reminders_num'];
			$array['auction_cost'] = $_POST['auction_cost'];
			$id=Model("")->table('auctions')->where(array('auction_id'=>$_POST['auction_id']))->update($array);
			if ($id) {
				$this->log('编辑拍品'.'['.$_POST['auction_id'].']',1);
            	showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_good&op=index');
            }else{
            	/*print_r($array);
            	die();*/
            	showMessage(Language::get('nc_common_op_fail'));
            }
		}
    	$auction_id=$_GET['auction_id'];
    	$condition['auction_id']=$auction_id;
    	$info = Model("")->table("auctions")->where($condition)->find();
    	Tpl::output('info', $info);
    	Tpl::showpage('auctions_good.edit2');
    }
    //复制拍品
    public function auction_copyOp(){
        $auction_id=$_GET['auction_id'];
        $condition['auction_id']=$auction_id;
        $info = Model("")->table("auctions")->where($condition)->find();
        if ($info) {
            $data=$info;
            unset($data['auction_id'], $data['state'], $data['is_liupai'], $data['bid_number'], $data['num_of_applicants']);
            unset($data['real_participants'], $data['set_reminders_num'], $data['auction_collect'], $data['current_price_time'], $data['bid_number'], $data['num_of_applicants']);
            //其他数据填写
            $data['goods_id']=0;
            $data['goods_common_id']=0;
            $data['store_id']=0;
            $data['auction_add_time']=time();
            $data['auction_start_time']=0;
            $data['auction_end_time']=0;
            $data['special_id']='';
            $data['auction_preview_start']=0;
            $data['auction_bond_rate']=0;
            $data['auction_end_true_t']=0;
            $data['current_price']=$data['auction_start_price'];
            $id=Model("")->table('auctions')->insert($data);
            if ($id) {
                $this->log('添加拍品'.'['.$_POST['auction_name'].']',1);
                showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_good&op=index', 'html', 'succ', 1, 0);
            }else{
                showMessage(Language::get('nc_common_op_fail'));
            }
        }else{
            showMessage(Language::get('nc_common_op_fail'));
        }
    }

    //设置保留价
    public function set_reserve_priceOp()
    {
        if (chksubmit()){
            $array['auction_reserve_price'] = $_POST['auction_reserve_price'];
            $id=Model("")->table('auctions')->where(array('auction_id'=>$_POST['auction_id']))->update($array);
            //echo $sql;die();
            //die();
            if ($id) {
                $this->log('编辑拍品'.'['.$_POST['auction_id'].']',1);
                showMessage(Language::get('nc_common_op_succ'),'index.php?act=auctions_good&op=index');
            }else{
                /*print_r($array);
                die();*/
                showMessage(Language::get('nc_common_op_fail'));
            }
        }
        $auction_id=$_GET['auction_id'];
        $condition['auction_id']=$auction_id;
        $info = Model("")->table("auctions")->where($condition)->find();
        Tpl::output('info', $info);
        Tpl::showpage('auctions_good.edit3');
    }
}