<?php
/**
 * 内部员工业绩统计
 *
 * Created by PhpStorm.
 * User: Tuebee
 * Date: 18/01/05
 * Time: 10:25
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class staff_taskControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp(){
        Tpl::showpage('staff_task.index');
    }

    public function get_xmlOp(){
        $member_model = Model('member');
        $model_order  = Model('order');
        $model_margin = Model('margin_orders');
        $times = ['start_time'=>0,'end_time'=>time()];
        $condition = array('member_role'=>'1');
        $condition2 = array('member'=>'','order'=>[]);
        $this->_get_condition($condition,$condition2,$times);

        $page = $_POST['rp'];
        $field = 'member_id,member_name,member_truename';
        $member_list = $member_model->getMemberList($condition,$field,$page);

        $data = array();
        $data['now_page'] = $member_model->shownowpage();
        $data['total_num'] = $member_model->gettotalnum();
        foreach ($member_list as $key => $value) {
            $member_id = $value['member_id'];
            $bottom_member = 0;
            $bottom_member_2 = 0;
            $bottom_margin = 0;
            $bottom_margin_2 = 0;
            $bottom_order = 0;
            $bottom_order_2 = 0;

            $member_condition = '(top_member='.$member_id.' or top_member_2='.$member_id.')';
            $distribute_count = $member_model->getCountDistribute($member_condition);
            $distribute_page = ceil($distribute_count/1000);
            for ($i=1;$i<=$distribute_page;$i++) {
                $limit1 = (($i - 1) * 1000);
                $limit2 = 1000;
                $result = $member_model->getMemberDistributeList($member_condition,'member_id,add_time,top_member,top_member_2','','',"{$limit1},{$limit2}");
                foreach ($result as $v) {
                    if ($v['top_member'] == $member_id) {
                        if($times['start_time'] < $v['add_time'] && $v['add_time'] < $times['end_time']){
                            $bottom_member++;
                        }
                        $bottom_margin += $model_margin->getMarginSum('(buyer_id = '. $v['member_id'] .')'.$condition2['order'][1],'margin_amount');
                        $bottom_order += $model_order->getPdSum('(buyer_id ='. $v['member_id'] .')'.$condition2['order'][2],'order_amount');
                    } else if ($v['top_member_2'] == $member_id) {
                        if($times['start_time'] < $v['add_time'] && $v['add_time'] < $times['end_time']){
                            $bottom_member_2++;
                        }
                        $bottom_margin_2 += $model_margin->getMarginSum('(buyer_id = '. $v['member_id'] .')'.$condition2['order'][1],'margin_amount');
                        $bottom_order_2 += $model_order->getPdSum('(buyer_id = '. $v['member_id'] .')'.$condition2['order'][2],'order_amount');
                    }
                }
            }
            $param = array();
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = $value['member_name'];
            $param['member_truename'] = $value['member_truename'];
            $param['bottom_member'] = $bottom_member;
            $param['bottom_member_2'] = $bottom_member_2;
            $param['bottom_margin'] = ncPriceFormat($bottom_margin);
            $param['bottom_margin_2'] = ncPriceFormat($bottom_margin_2);
            $param['bottom_order'] = ncPriceFormat($bottom_order);
            $param['bottom_order_2'] = ncPriceFormat($bottom_order_2);
            $data['list'][] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 处理搜索条件
     */
    private function _get_condition(& $condition,& $condition2,& $times){
        if ($_POST['qtype'] == 'member_name' && $_POST['query'] != '') {
            $condition['member_name'] = array('like', '%' . $_POST['query'] . '%');
        }
        if($_GET['keyword']){
            $condition['member_name'] = array('like', '%' . $_GET['keyword'] . '%');
        }
        $times['start_time'] = (int)strtotime($_GET['query_start_date']);
        $_GET['query_end_date'] and $times['end_time'] = (int)strtotime($_GET['query_end_date'])+86399;
        $between = '';
        if(!empty($times['start_time']) || !empty($times['end_time'])){
            $between = ' and (payment_time BETWEEN '.$times['start_time'].' AND '.$times['end_time'].')';
        }
        $condition2['order'][1] = ' and (order_state = 1)'.$between;
        $condition2['order'][2] = ' and (order_state = 20)'.$between;
    }
}

