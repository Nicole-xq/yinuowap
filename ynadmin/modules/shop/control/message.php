<?php
/**
 * 消息通知
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class messageControl extends SystemControl{
    private $links = array(
        array('url'=>'act=message&op=seller_tpl', 'lang'=>'seller_tpl'),
        array('url'=>'act=message&op=member_tpl', 'lang'=>'member_tpl'),
        array('url'=>'act=message&op=global_tpl', 'lang'=>'global_tpl'),
    );
    public function __construct(){
        parent::__construct();
        Language::read('setting,message');
    }

    public function indexOp() {
        $this->seller_tplOp();
    }

    /**
     * 商家消息模板
     */
    public function seller_tplOp() {
        $mstpl_list = Model('store_msg_tpl')->getStoreMsgTplList(array());
        Tpl::output('mstpl_list', $mstpl_list);
        Tpl::output('top_link',$this->sublink($this->links,'seller_tpl'));
        Tpl::showpage('message.seller_tpl');
    }

    /**
     * 商家消息模板编辑
     */
    public function seller_tpl_editOp() {
        if (chksubmit()) {
            $code = trim($_POST['code']);
            $type = trim($_POST['type']);
            if (empty($code) || empty($type)) {
                showMessage(L('param_error'));
            }
            switch ($type) {
                case 'message':
                    $this->seller_tpl_update_message();
                    break;
                case 'short':
                    $this->seller_tpl_update_short();
                    break;
                case 'mail':
                    $this->seller_tpl_update_mail();
                    break;
            }
        }
        $code = trim($_GET['code']);
        if (empty($code)) {
            showMessage(L('param_error'));
        }

        $where = array();
        $where['smt_code'] = $code;
        $smtpl_info = Model('store_msg_tpl')->getStoreMsgTplInfo($where);
        Tpl::output('smtpl_info', $smtpl_info);
        $this->links[] = array('url'=>'act=message&op=seller_tpl_edit','lang'=>'seller_tpl_edit');
        Tpl::output('top_link',$this->sublink($this->links,'seller_tpl_edit'));
        Tpl::showpage('message.seller_tpl.edit');
    }

    /**
     * 商家消息模板更新站内信
     */
    private function seller_tpl_update_message() {
        $message_content = trim($_POST['message_content']);
        if (empty($message_content)) {
            showMessage('请填写站内信模板内容。');
        }
        // 条件
        $where = array();
        $where['smt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['smt_message_switch'] = intval($_POST['message_switch']);
        $update['smt_message_content'] = $message_content;
        $update['smt_message_forced'] = intval($_POST['message_forced']);
        $result = Model('store_msg_tpl')->editStoreMsgTpl($where, $update);
        $this->seller_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新短消息
     */
    private function seller_tpl_update_short() {
        $short_content = trim($_POST['short_content']);
        if (empty($short_content)) {
            showMessage('请填写短消息模板内容。');
        }
        // 条件
        $where = array();
        $where['smt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['smt_short_switch'] = intval($_POST['short_switch']);
        $update['smt_short_content'] = $short_content;
        $update['smt_short_forced'] = intval($_POST['short_forced']);
        $result = Model('store_msg_tpl')->editStoreMsgTpl($where, $update);
        $this->seller_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新邮件
     */
    private function seller_tpl_update_mail() {
        $mail_subject = trim($_POST['mail_subject']);
        $mail_content = trim($_POST['mail_content']);
        if ((empty($mail_subject) || empty($mail_content))) {
            showMessage('请填写邮件模板内容。');
        }
        // 条件
        $where = array();
        $where['smt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['smt_mail_switch'] = intval($_POST['mail_switch']);
        $update['smt_mail_subject'] = $mail_subject;
        $update['smt_mail_content'] = $mail_content;
        $update['smt_mail_forced'] = intval($_POST['mail_forced']);
        $result = Model('store_msg_tpl')->editStoreMsgTpl($where, $update);
        $this->seller_tpl_update_showmessage($result);
    }

    private function seller_tpl_update_showmessage($result) {
        if ($result) {
            showMessage(L('nc_common_op_succ'), urlAdminShop('message', 'seller_tpl'));
        } else {
            showMessage(L('nc_common_op_fail'));
        }
    }

    /**
     * 用户消息模板
     */
    public function member_tplOp() {
        $condition = array();
        if(!C('distribute_isuse')){
            $condition['mmt_code'] = array('neq','trad_change');
        }
        $mmtpl_list = Model('member_msg_tpl')->getMemberMsgTplList($condition);
        Tpl::output('mmtpl_list', $mmtpl_list);
        Tpl::output('top_link',$this->sublink($this->links,'member_tpl'));
        Tpl::showpage('message.member_tpl');
    }

    /**
     * 用户消息模板编辑
     */
    public function member_tpl_editOp() {
        if (chksubmit()) {
            $code = trim($_POST['code']);
            $type = trim($_POST['type']);
            if (empty($code) || empty($type)) {
                showMessage(L('param_error'));
            }
            switch ($type) {
                case 'message':
                    $this->member_tpl_update_message();
                    break;
                case 'short':
                    $this->member_tpl_update_short();
                    break;
                case 'mail':
                    $this->member_tpl_update_mail();
                    break;
                case 'wx':
                    $this->member_tpl_update_wx();
                    break;
                case 'app':
                    $this->member_tpl_update_app();
                    break;
            }
        }
        $code = trim($_GET['code']);
        if (empty($code)) {
            showMessage(L('param_error'));
        }

        $where = array();
        $where['mmt_code'] = $code;
        $mmtpl_info = Model('member_msg_tpl')->getMemberMsgTplInfo($where);
        Tpl::output('mmtpl_info', $mmtpl_info);
        $this->links[] = array('url'=>'act=message&op=member_tpl_edit','lang'=>'member_tpl_edit');
        Tpl::output('top_link',$this->sublink($this->links,'member_tpl_edit'));
        Tpl::showpage('message.member_tpl.edit');
    }

    /**
     * 商家消息模板更新站内信
     */
    private function member_tpl_update_message() {
        $message_content = trim($_POST['message_content']);
        if (empty($message_content)) {
            showMessage('请填写站内信模板内容。');
        }
        $message_subject = trim($_POST['message_subject']);
        if (empty($message_subject)) {
            showMessage('请填写站内信标题。');
        }
        // 条件
        $where = array();
        $where['mmt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['mmt_message_switch'] = intval($_POST['message_switch']);
        $update['mmt_message_content'] = $message_content;
        $update['mmt_message_subject'] = $message_subject;
        //上传图片
        if ($_FILES['mmt_message_logo']['name'] != '') {
            $upload = new UploadFile();
            $upload->set('default_dir', ATTACH_MOBILE . '/boot');
            $result = $upload->upfile('mmt_message_logo');
            if ($result) {
                $update['mmt_message_logo'] = $upload->file_name;
            } else {
                showMessage($upload->error);
            }
        }
        $result = Model('member_msg_tpl')->editMemberMsgTpl($where, $update);
        $this->member_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新短消息
     */
    private function member_tpl_update_short() {
        $short_content = trim($_POST['short_content']);
        if (empty($short_content)) {
            showMessage('请填写短消息模板内容。');
        }
        // 条件
        $where = array();
        $where['mmt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['mmt_short_switch'] = intval($_POST['short_switch']);
        $update['mmt_short_content'] = $short_content;
        $result = Model('member_msg_tpl')->editMemberMsgTpl($where, $update);
        $this->member_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新邮件
     */
    private function member_tpl_update_mail() {
        $mail_subject = trim($_POST['mail_subject']);
        $mail_content = trim($_POST['mail_content']);
        if ((empty($mail_subject) || empty($mail_content))) {
            showMessage('请填写邮件模板内容。');
        }
        // 条件
        $where = array();
        $where['mmt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['mmt_mail_switch'] = intval($_POST['mail_switch']);
        $update['mmt_mail_subject'] = $mail_subject;
        $update['mmt_mail_content'] = $mail_content;
        $result = Model('member_msg_tpl')->editMemberMsgTpl($where, $update);
        $this->member_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新微信
     */
    private function member_tpl_update_wx() {
        $wx_subject = trim($_POST['wx_subject']);
        if ((empty($wx_subject))) {
            showMessage('请填写微信消息模板内容。');
        }
        // 条件
        $where = array();
        $where['mmt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['mmt_wx_switch'] = intval($_POST['wx_switch']);
        $update['mmt_wx_subject'] = $wx_subject;
        $result = Model('member_msg_tpl')->editMemberMsgTpl($where, $update);
        $this->member_tpl_update_showmessage($result);
    }

    /**
     * 商家消息模板更新站内信
     */
    private function member_tpl_update_app() {
        $app_content = trim($_POST['app_content']);
        if (empty($app_content)) {
            showMessage('请填写站app模板内容。');
        }
        $app_subject = trim($_POST['app_subject']);
        if (empty($app_subject)) {
            showMessage('请填写标题。');
        }
        $app_link = trim($_POST['app_link']);
        if (empty($app_link)) {
            showMessage('请填写消息点开链接');
        }
        // 条件
        $where = array();
        $where['mmt_code'] = trim($_POST['code']);
        // 数据
        $update = array();
        $update['mmt_app_switch'] = intval($_POST['app_switch']);
        $update['mmt_app_content'] = $app_content;
        $update['mmt_app_subject'] = $app_subject;
        $update['mmt_app_link'] = $app_link;
        $result = Model('member_msg_tpl')->editMemberMsgTpl($where, $update);
        $this->member_tpl_update_showmessage($result);
    }

    private function member_tpl_update_showmessage($result) {
        if ($result) {
            showMessage(L('nc_common_op_succ'), urlAdminShop('message', 'member_tpl'));
        } else {
            showMessage(L('nc_common_op_fail'));
        }
    }

    /**
     * 用户消息模板
     */
    public function global_tplOp() {
        Tpl::output('top_link',$this->sublink($this->links,'global_tpl'));
        Tpl::showpage('message.global_tpl');
    }

    /**
     * 输出发送全局列表XML数据
     */
    public function get_global_xmlOp() {
        $model_global = Model('global_msg_tpl');
        $page = intval($_POST['rp']) > 0 ? intval($_POST['rp']) : 15;
        $condition = array();
        if ($_POST['qtype'] == 'tpl_title') {
            $condition[$_POST['qtype']] = array('like', '%' . trim($_POST['query']) . '%');
        }
        $list = $model_global->getGlobalMsgTplList($condition,'*',$group = '','id DESC',$page);
        $out_list = array();
        if (!empty($list) && is_array($list)){
            $send_type = $model_global->getSendType();
            $fields_array = array('operation','tpl_title','mmt_message_subject','mmt_message_content','mmt_short_content','send_type','send_state','send_time');
            foreach ($list as $k => $v){
                $out_array = getFlexigridArray(array(),$fields_array,$v);
                if ($v['send_state'] == 2) {
                    $out_array['send_state'] = '<span class="yes"><i class="fa fa-check-circle"></i>是</span>';
                } else {
                    $out_array['send_state'] = '<span class="no"><i class="fa fa-ban"></i>否</span>';
                }
                $out_array['send_type'] = $send_type[$v['send_type']];
                $out_array['send_time'] = $v['send_time'] ? date('Y-m-d H:i',$v['send_time']) : '----';
                //$out_array['operation'] = '<a class="btn green" href="'.urlShop('message','index',array('id'=> $v['id'])).'" target="_blank"><i class="fa fa-list-alt"></i>查看</a>';
                $out_list[$v['id']] = $out_array;
            }
        }
        $data = array();
        $data['now_page'] = $model_global->shownowpage();
        $data['total_num'] = $model_global->gettotalnum();
        $data['list'] = $out_list;
        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 推送模板编辑
     */
    public function global_tpl_editOp() {
        $model_global = Model('global_msg_tpl');
        if (chksubmit()) {
            if (intval($_POST['send_way']) == 2 && empty($_POST['send_time'])) {
                showMessage('请选择发送时间');
            }
            if ($_POST['mmt_message_switch'] && $_POST['mmt_short_switch']){
                $send_type = 1;
            }elseif ($_POST['mmt_message_switch'] && !$_POST['mmt_short_switch']){
                $send_type = 2;
            }elseif (!$_POST['mmt_message_switch'] && $_POST['mmt_short_switch']){
                $send_type = 3;
            }
            $global_array = array();
            $global_array['tpl_title'] = strval($_POST['tpl_title']);
            $global_array['mmt_message_switch'] = intval($_POST['mmt_message_switch']);
            $global_array['mmt_message_subject'] = strval($_POST['mmt_message_subject']);
            $global_array['mmt_message_content'] = strval($_POST['mmt_message_content']);
            $global_array['mmt_short_switch'] = intval($_POST['mmt_short_switch']);
            $global_array['mmt_short_content'] = strval($_POST['mmt_short_content']);

            $global_array['send_type'] = $send_type;
            $global_array['send_way'] = intval($_POST['send_way']);
            $global_array['send_time'] = intval($_POST['send_way']) == 2 ? strtotime($_POST['send_time']) : '';
            $global_array['add_time'] = time();

            $state = $model_global->addGlobalMsgTpl($global_array);
            if ($state) {
                showMessage(Language::get('nc_common_save_succ'),'index.php?act=message&op=global_tpl');
            } else {
                showMessage(Language::get('nc_common_save_fail'));
            }
        }
        Tpl::showpage('message.global_tpl.edit');
    }
}
