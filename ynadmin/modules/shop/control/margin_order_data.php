<?php
/**
 * 商品分析
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class margin_order_dataControl extends SystemControl{
    private $links = array(
        array('url'=>'act=margin_order_data&op=yesterday_data&type=yesterday','lang'=>'yesterday'),
        array('url'=>'act=margin_order_data&op=today_data&type=today','lang'=>'today'),
        array('url'=>'act=margin_order_data&op=week_data&type=week','lang'=>'week'),
        array('url'=>'act=margin_order_data&op=month_data&type=month','lang'=>'month'),
        array('url'=>'act=margin_order_data&op=other_data&type=other','lang'=>'other')
    );
    public function __construct(){
        parent::__construct();
        Language::read('margin_order_data');
        import('function.statistics');
        import('function.datehelper');

    }
    public function indexOp() {
        $type = isset($_GET['type'])?$_GET['type']:'today';
        Tpl::output('data_type',$type.'_data');
        Tpl::output('top_link',$this->sublink($this->links, $type.'_data'));
        Tpl::showpage('margin_order.detail');
    }

    public function get_top_dataOp(){
        $type = $_GET['type'];
        $model_margin_order = Model('margin_orders');
        switch($type){
            case 'yesterday':
                $yesterday = date('Y-m-d',strtotime('-1day'));
                $yesterday_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$yesterday}'")->count();
                $yesterday_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$yesterday}'")->find();
                $data = array(
                    'title'=>'昨日数据',
                    'order_num'=>$yesterday_order_num,
                    'amount'=>$yesterday_amount['amount'] !=''?$yesterday_amount['amount']:0
                );
                break;
            case 'today_data':
                $today = date('Y-m-d');
                $today_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'")->count();
                $today_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'")->find();
                $data = array(
                    'title'=>'当日数据',
                    'order_num'=>$today_order_num,
                    'amount'=>$today_amount['amount'] !=''?$today_amount['amount'] : 0
                );
                break;
            case 'week_data':
                $tmp_w = date('N',time());
                $w_start = date('Y-m-d',time()-($tmp_w-1)*86400);
                $w_end = date('Y-m-d',time()+(7-$tmp_w)*86400);
                $week_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$w_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$w_end}'")->count();
                $week_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$w_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$w_end}'")->find();
                $data = array(
                    'title'=>'本周数据',
                    'order_num'=>$week_order_num,
                    'amount'=>$week_amount['amount'] !=''?$week_amount['amount']:0
                );
                break;
            case 'month_data':
                $m_start = date('Y-m-01');
                $m_end = date('Y-m-31');
                $month_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$m_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$m_end}'")->count();
                $month_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$m_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$m_end}'")->find();
                $data = array(
                    'title'=>'本月数据',
                    'order_num'=>$month_order_num,
                    'amount'=>$month_amount['amount'] !=''?$month_amount['amount']:0
                );
                break;
            case 'other_data':
                $o_start = $_GET['query_start_date'];
                $o_end = $_GET['query_end_date'];
                $other_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$o_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$o_end}'")->count();
                $other_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$o_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$o_end}'")->find();
                $data = array(
                    'title'=>'自定义周期数据',
                    'order_num'=>$other_order_num,
                    'amount'=>$other_amount['amount'] !=''?$other_amount['amount']:0
                );
                break;
            default :
                $today = date('Y-m-d');
                $today_order_num = $model_margin_order->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'")->count();
                $today_amount = $model_margin_order->field('sum(margin_amount) amount')->where("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'")->find();
                $data = array(
                    'title'=>'当日数据',
                    'order_num'=>$today_order_num,
                    'amount'=>$today_amount['amount'] !=''?$today_amount['amount'] : 0
                );
                break;
        }
        echo '<div class="title"><h3>'.$data['title'].'</h3></div>';
		echo '<dl class="row"><dd class="opt"><ul class="nc-row">';
        echo '<li title="订单总金额：'. number_format($data['amount'],2).'元"><h4>订单总金额:</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$data['amount'].'"></h2><h6>元</h6></li>';
        echo '<li title="订单总量：'. $data['order_num'].'单"><h4>订单总量:</h4><h2 id="count-number" class="timer" data-speed="1500" data-to="'.$data['order_num'].'"></h2><h6>单</h6></li>';

		echo '</ul></dd><dl>';
        exit;
    }

    public function get_xmlOp(){
        $type = $_GET['type'];
        $page = intval($_POST['rp']);
        $model_margin_order = Model('margin_orders');
        switch($type){
            case 'yesterday':
                $yesterday = date('Y-m-d',strtotime('-1day'));
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$yesterday}'",'*','','',0,$page);
                break;
            case 'today_data':
                $today = date('Y-m-d');
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'",'*','','',0,$page);
                break;
            case 'week_data':
                $tmp_w = date('N',time());
                $w_start = date('Y-m-d',time()-($tmp_w-1)*86400);
                $w_end = date('Y-m-d',time()+(7-$tmp_w)*86400);
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$w_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$w_end}'",'*','','',0,$page);
                break;
            case 'month_data':
                $m_start = date('Y-m-01');
                $m_end = date('Y-m-31');
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$m_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$m_end}'",'*','','',0,$page);
                break;
            case 'other_data':
                $o_start = $_GET['query_start_date'];
                $o_end = $_GET['query_end_date'];
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') >= '{$o_start}' and FROM_UNIXTIME(payment_time,'%Y-%m-%d') <= '{$o_end}'",'*','','',0,$page);
                break;
            default :
                $today = date('Y-m-d');
                $list = $model_margin_order->getList("order_state = 1 and FROM_UNIXTIME(payment_time,'%Y-%m-%d') = '{$today}'",'*','','',0,$page);

                break;
        }
        if(!empty($list)){
            foreach($list as $order_info){
                $tmp = array();
                $tmp['order_sn'] = $order_info['order_sn'].str_replace(array(0,1), array('[线上支付]','[线下支付]'), $order_info['order_type']);
                $tmp['order_from'] = str_replace(array(1,2), array('PC端','移动端'),$order_info['order_from']);
                $tmp['created_at'] = date('Y-m-d H:i:s',$order_info['created_at']);
                $tmp['margin_amount'] = ncPriceFormat($order_info['margin_amount']);
                $tmp['order_state'] = orderMarginState($order_info);
                $tmp['payment_code'] = orderPaymentName($order_info['payment_code']);
                $tmp['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His',$order_info['payment_time'])) ? date('Y-m-d H:i:s',$order_info['payment_time']) : date('Y-m-d',$order_info['payment_time'])) : '';
                $tmp['rcb_amount'] = ncPriceFormat($order_info['rcb_amount']);
                $tmp['pd_amount'] = ncPriceFormat($order_info['pd_amount']);
                $tmp['points_amount'] = ncPriceFormat($order_info['points_amount']);
                $tmp['account_margin_amount'] = ncPriceFormat($order_info['account_margin_amount']);
                $tmp['finnshed_time'] = !empty($order_info['finnshed_time']) ? date('Y-m-d H:i:s',$order_info['finnshed_time']) : '';
                $tmp['store_id'] = $order_info['store_id'];
                $tmp['store_name'] = $order_info['store_name'];
                $tmp['goods_name'] = $order_info['auction_name'];
                $tmp['buyer_id'] = $order_info['buyer_id'];
                $tmp['buyer_name'] = $order_info['buyer_name'];
                $data['list'][$order_info['margin_id']] = $tmp;
            }
        }
        $data['now_page'] = $model_margin_order->shownowpage();
        $data['total_num'] = $model_margin_order->gettotalnum();
        echo Tpl::flexigridXML($data);exit();
    }

}
