<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>专场审核</h3>
                <h5>对签约人和拍卖公司申请的专场进行审核</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>

<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=auction_special&op=get_xml&is_all=2',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '专场ID', name : 'special_id', width : 50, sortable : true, align: 'center'},
                {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
                {display: '店铺名称', name : 'store_name', width : 150, sortable : false, align: 'left'},
                {display: '专场名称', name : 'special_name', width : 120, sortable : true, align: 'left'},
                {display: '专场开始时间', name : 'special_start_time', width : 150, sortable : true, align: 'center'},
                {display: '专场结束时间', name : 'special_end_time', width : 150, sortable : true, align: 'center'},
                {display: '预展开始时间', name : 'special_preview_start', width : 150, sortable : true, align: 'center'},
                {display: '认筹结束时间', name : 'special_rate_time', width : 150, sortable : true, align: 'center'},
                {display: '拍品件数', name : 'auction_num', width : 150, sortable : true, align: 'center'},
                {display: '参与人数', name : 'participants', width : 150, sortable : true, align: 'center'},
                {display: '保证金年化收益率', name : 'special_rate', width : 150, sortable : true, align: 'center'},
                {display: '审核状态', name : 'special_state', width : 80, sortable : true, align: 'center'},
                {display: '拍卖状态', name : 'special_state', width : 150, sortable : true, align: 'center'},
            ],
            searchitems : [
                {display: '店铺名称', name : 'store_name', isdefault: true},
                {display: '专场名称', name : 'special_name'}
            ],
            sortname: "special_id",
            sortorder: "desc",
            title: '未审核专场列表'
        });

    });

</script>