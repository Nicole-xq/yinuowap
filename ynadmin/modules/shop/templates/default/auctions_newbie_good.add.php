<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="index.php?act=auctions_good&op=index" title="返回分类列表"><i
                        class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>新增新手专区</a></h3>
                <h5>新手专区新增</h5>
            </div>
        </div>
    </div>
    <form id="add_form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="form_submit" value="ok"/>
        <div class="ncap-form-default">
<!--            <dl class="row">-->
<!--                <dt class="tit">-->
<!--                    <label for="goods_id"><em>*</em>商品ID</label>-->
<!--                </dt>-->
<!--                <dd class="opt">-->
<!--                    <input type="text" id="goods_id" name="goods_id" class="input-txt">-->
<!--                    <span class="err"></span>-->
<!--                </dd>-->
<!--            </dl>-->
            <dl class="row">
                <dt class="tit">
                    <label for="goods_id"><em>*</em>商品ID</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="goods_id" list="goods_id" maxlength="20">
                    <datalist id="goods_id" style="display:none;">
                        <?php if(isset($output['search_goods_list']) && !empty($output['search_goods_list']) ) {?>
                            <?php foreach ($output['search_goods_list'] as $val) {?>
                                <option value="<?php echo $val['goods_id'];?>"><?php echo $val['goods_name'];?></option>
                            <?php }?>
                        <?php }?>
                    </datalist>
                </dd>
            </dl>
            <input type="hidden" name="auction_type" value="2">

            <dl class="row">
                <dt class="tit">
                    <label for="auction_bond"><em>*</em>拍品保证金</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_bond" name="auction_bond" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_bond"><em>*</em>拍品保证金</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_bond" name="auction_bond" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_start_price"><em>*</em>拍品起拍价</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_start_price" name="auction_start_price" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_reference_price"><em>*</em>拍品参考价</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_reference_price" name="auction_reference_price" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_cost"><em>*</em>拍品成本价格</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_cost" name="auction_cost" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_increase_range"><em>*</em>拍品加价幅度</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_increase_range" name="auction_increase_range" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>预展开始时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_preview_start" name="auction_preview_start" class="input-txt" onClick="WdatePicker({dateFmt:'yyyy-M-d H:mm:ss',minDate: '%y-%M-%d' })" />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>开始时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_start_time" name="auction_start_time" class="input-txt" onClick="WdatePicker({dateFmt:'yyyy-M-d H:mm:ss',minDate: '%y-%M-%d' })"  />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>结束时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_end_time" name="auction_end_time" class="input-txt" onClick="WdatePicker({dateFmt:'yyyy-M-d H:mm:ss',minDate: '%y-%M-%d' })"  />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label for="warehouse_location"><em></em>库位号</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="warehouse_location" name="warehouse_location" class="input-txt"
                           placeholder="A12031:A区12号货架第31号库位">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="share_content"><em>*</em>分享描述</label>
                </dt>
                <dd class="opt">
                    <textarea name="share_content" rows="6" class="tarea" id="share_content"></textarea>
                    <span class="err"></span>
                </dd>
            </dl>
            <div class="bot" id="artist"></div>
            <div class="bot">
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green"
                   id="submitBtn"><span><?php echo $lang['nc_submit']; ?></span></a></div>
        </div>
    </form>
</div>
<script language="javascript" type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/My97DatePicker/WdatePicker.js"></script>
<script>
    //按钮先执行验证再提交表单
    $(function () {
        $('#add_form').validate({
            errorPlacement: function (error, element) {
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules: {
                goods_id: {
                    required: true,
                    number: true
                },auction_bond: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auction_start_price: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auction_reference_price: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auction_increase_range: {
                    required: true,
                    number: true,
                    min: 0,
                },
                share_content: {
                    required: true,
                }
            },
            messages: {
                goods_id: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字'
                },
                auction_bond: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_start_price: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_reference_price: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_cost: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_increase_range: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                share_content: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                },
            }
        });

        $("#submitBtn").click(
            function () {
                if ($("#add_form").valid()) {
                    $("#add_form").submit();
                }
            }
        );
        $("#search").click(
            function () {
                var keyword = $("#keyword").val();
                if (keyword) {
                    htmlobj = $.ajax({
                        url: 'index.php?act=auctions_artist&op=auctions_artist_list&keyword=' + keyword,
                        async: false
                    });
                    $("#artist").html(htmlobj.responseText);
                } else {
                    alert('艺术家名字不能为空');
                }
            }
        );
        var textButton = "<input type='text' name='textfield' id='textfield' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton).insertBefore("#auction_image");
        $("#auction_image").change(function () {
            $("#textfield").val($("#auction_image").val());
        });

        var textButton1 = "<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton1).insertBefore("#auctions_img1");
        $("#auctions_img1").change(function () {
            $("#textfield1").val($("#auctions_img1").val());
        });
        var textButton2 = "<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton2).insertBefore("#auctions_img2");
        $("#auctions_img2").change(function () {
            $("#textfield2").val($("#auctions_img2").val());
        });
        var textButton3 = "<input type='text' name='textfield' id='textfield3' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton3).insertBefore("#auctions_img3");
        $("#auctions_img3").change(function () {
            $("#textfield3").val($("#auctions_img3").val());
        });
        var textButton4 = "<input type='text' name='textfield' id='textfield4' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton4).insertBefore("#auctions_img4");
        $("#auctions_img4").change(function () {
            $("#textfield4").val($("#auctions_img4").val());
        });
    });

    function bind_artist(id) {
        htmlobj = $.ajax({url: 'index.php?act=auctions_artist&op=auctions_artist_info&id=' + id, async: false});
        $("#add_artist").html(htmlobj.responseText);
    }
</script>