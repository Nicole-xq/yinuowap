<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>拍品管理</h3>
                <h5>商城所有拍品索引及管理</h5>
            </div>
            <?php echo $output['top_link'];?> </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li><?php echo $lang['goods_index_help1'];?></li>
            <li><?php echo $lang['goods_index_help2'];?></li>
            <li>设置项中可以查看拍品详细。查看拍品详细，跳转到拍品详细页。</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" name="formSearch" id="formSearch">
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dt>拍品名称</dt>
                        <dd>
                            <label>
                                <input type="text" value="" name="auction_name" id="auction_name" class="s-input-txt" placeholder="输入拍品全称或关键字">
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>所属店铺</dt>
                        <dd>
                            <label>
                                <input type="text" value="" name="store_name" id="store_name" class="s-input-txt" placeholder="输入拍品所属店铺名称">
                            </label>
                        </dd>
                    </dl>

                    <dl>
                        <dt>拍品状态</dt>
                        <dd>
                            <label>
                                <select name="state" class="s-select">
                                    <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                  <option value="1">等待预展</option>
                                  <option value="2">预展中</option>
                                  <option value="3">拍卖中</option>
                                  <option value="4">拍卖结束</option>
                                </select>
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>拍卖结果</dt>
                        <dd>
                            <label>
                                <select name="auction_state" class="s-select">
                                    <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                    <option value="0">成交</option>
                                    <option value="1">流拍</option>
                                </select>
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>付款状态</dt>
                        <dd>
                            <label>
                                <select name="pay_state" class="s-select">
                                    <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                    <option value="1">已付款</option>
                                    <option value="2">未付款</option>
                                </select>
                            </label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom"><a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a><a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容"><i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?></a></div>
        </form>
    </div>
    <script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
    <script type="text/javascript">
        gcategoryInit('gcategory');
    </script>

</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=auctions_good&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '拍品ID', name : 'auction_image', width : 50, sortable : true, align: 'center'},
                {display: '拍品图片', name : 'auction_image', width : 60, sortable : true, align: 'center'},
                {display: '拍品名称', name : 'auction_name', width : 150, sortable : false, align: 'left'},
                {display: '拍品类型', name : 'auction_type_text', width : 100, sortable : false, align: 'left'},
                {display: '分类', name : 'auction_name', width : 150, sortable : false, align: 'left'},
                {display: '开始时间', name : 'auction_start_time', width : 100, sortable : false, align: 'left'},
                {display: '关联状态', name : 'auction_name', width : 100, sortable : false, align: 'left'},
                {display: '库位号', name : 'warehouse_location', width : 150, sortable : false, align: 'left'},
                {display: '专场名字', name : 'auction_name', width : 120, sortable : false, align: 'left'},
                {display: '拍品状态', name : 'state', width : 60, sortable : true, align: 'center'},
                {display: '保证金（元）', name : 'auction_bond', width : 100, sortable : true, align: 'center'},
                {display: '起拍价（元）', name : 'auction_start_price', width : 100, sortable : true, align: 'center'},
                {display: '加价幅度（元）', name : 'auction_increase_range', width : 100, sortable : true, align: 'center'},
                {display: '保留价（元）', name : 'auction_reserve_price', width : 100, sortable : true, align: 'center'},
                {display: '成交价', name : 'order_amount', width : 60, sortable : true, align: 'center'},
                {display: '竞拍人', name : 'buyer_name', width : 60, sortable : true, align: 'center'},
                {display: '付款状态', name : 'pay_state', width : 60, sortable : true, align: 'center'},
            ],
            buttons : [
                {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出CVS文件', onpress : fg_operation }
            ],
            searchitems : [
                {display: 'ID', name : 'auction_id'},
                {display: '拍品名称', name : 'auction_name'},
                {display: '店铺ID', name : 'store_id'},
                {display: '店铺名称', name : 'store_name'},
            ],
            sortname: "auction_id",
            sortorder: "desc",
            title: '拍品列表'
        });

        // 高级搜索提交
        $('#ncsubmit').click(function(){
            $("#flexigrid").flexOptions({url: 'index.php?act=auctions&op=get_xml&'+$("#formSearch").serialize(),query:'',qtype:''}).flexReload();
        });

        // 高级搜索重置
        $('#ncreset').click(function(){
            $("#flexigrid").flexOptions({url: 'index.php?act=auctions&op=get_xml'}).flexReload();
            $("#formSearch")[0].reset();
        });
    });

    function fg_operation(name, bDiv) {
        if (name == 'csv') {
            if ($('.trSelected', bDiv).length == 0) {
                if (!confirm('您确定要下载全部数据吗？')) {
                    return false;
                }
            }
            var itemids = new Array();
            $('.trSelected', bDiv).each(function(i){
                itemids[i] = $(this).attr('data-id');
            });
            fg_csv(itemids);
        }
    }

    function fg_csv(ids) {
        id = ids.join(',');
        window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_csv&type=<?php echo $output['type'];?>&id=' + id;
    }


    //拍品结束拍卖
    function fg_lonkup(ids) {
        _uri = "index.php?act=auctions&op=auction_lockup&id=" + ids;
        CUR_DIALOG = ajax_form('auctions_lockup', '违规下架理由', _uri, 640);
    }
    //设置返佣
    function fg_sku(commonid) {
        CUR_DIALOG = ajax_form('login','拍品"' + commonid +'"的SKU列表','<?php echo urlAdminShop('auctions', 'get_auctions_sku_list');?>&commonid=' + commonid, 680);
    }

    // 删除
    function fg_del(id) {
        if(confirm('删除后将不能恢复，确认删除这项吗？')){
            $.getJSON('index.php?act=auctions&op=auction_del', {id:id}, function(data){
                if (data.state) {
                    $("#flexigrid").flexReload();
                } else {
                    showError(data.msg)
                }
            });
        }
    }
    // 拍品审核
    function fg_verify(ids) {
        _uri = "index.php?act=auctions&op=auctions_verify&id=" + ids;
        CUR_DIALOG = ajax_form('auctions_verify', '审核拍品', _uri, 640);
    }

    //预览视频
    function fg_see_video(ids) {
        _uri = "index.php?act=auctions&op=see_video&id=" + ids;
        CUR_DIALOG = ajax_form('see_video', '预览视频', _uri, 640);
    }

    $('a[data-href]').live('click', function() {
    if ($(this).hasClass('confirm-on-click') && !confirm('确定"'+$(this).text()+'"?')) {
        return false;
    }

    $.getJSON($(this).attr('data-href'), function(d) {
        if (d && d.result) {
            $("#flexigrid").flexReload();
        } else {
            alert(d && d.message || '操作失败！');
        }
    });
});

</script>
