<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo ADMIN_RESOURCE_URL?>/js/admin.js" type="text/javascript"></script>
<form method="post" name="form1" id="form1" class="ncap-form-dialog" action="<?php echo urlAdminShop('auction_special', 'get_special_info');?>">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" value="<?php echo $output['special_info']['special_id'];?>" name="special_id">
    <div class="ncap-form-default">
        <dl class="row">
            <dt class="tit">审核拍品ID</dt><dd class="opt"><?php echo $output['special_info']['special_id'];?></dd></dl>
        <dl class="row">
            <dt class="tit">审核拍品名称</dt><dd class="opt"><?php echo $output['special_info']['special_name'];?></dd></dl>

        <dl class="row" nctype="reason">
            <dt class="tit">
                <label for="auction_bond_rate">专场保证金利息</label>
            </dt>
            <dd class="opt">
                <input name="auction_bond_rate" value="<?=$output['special_info']['special_rate'];?>" type="text" class="text w30" />%<span></span>
          <p class="hint">认购时间结束前提交保证金的会员，保证金计息，最大10% </p>
            </dd>
        </dl>
        <dl class="row" nctype="reason">
            <dt class="tit">
                <label for="auction_bond_rate">专场认购时间</label>
            </dt>
            <dd class="opt">
                <input name="auction_rate_time" id="auction_rate_time" value="<?= $output['special_info']['special_rate_time']?date('Y-m-d H:i:s',$output['special_info']['special_rate_time']):'';?>" type="text" class="text w150" /><span></span>
          <p class="hint">保证金收益时间(超过此时间,保证金将无法获得收益)</p>
            </dd>
        </dl>
        <div class="bot"><a href="javascript:void(0);" class="ncap-btn-big ncap-btn-green" nctype="btn_submit"><?php echo $lang['nc_submit'];?></a></div>
    </div>
</form>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<script>
  $('#auction_rate_time').datetimepicker({controlType: 'select'});
    $(function(){
        $('a[nctype="btn_submit"]').click(function(){
            if($("#form1").valid()) ajaxpost('form1', '', '', 'onerror');
        });
    $('#form1').validate({
        errorPlacement: function(error, element){
            $(element).nextAll('span').append(error);
        },
        rules : {
            auction_bond_rate : {
                number   : true,
                min:0,
                max:10
            }
        },
        messages : {
            auction_bond_rate : {
                number   : '<i class="fa fa-exclamation-circle"></i>请填写正确的保证金利息',
                min   : '<i class="fa fa-exclamation-circle"></i>请填写正确的保证金利息',
                max   : '<i class="fa fa-exclamation-circle"></i>请填写正确的保证金利息'
            }
        }
    });
    });
</script>