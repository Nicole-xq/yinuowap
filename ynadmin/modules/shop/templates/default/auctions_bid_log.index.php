<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>出价管理</h3>
        <h5>出价管理</h5>
      </div>
    </div>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=auctions_bid_log&op=get_xml',
        colModel : [
            {display: 'ID', name : 'bid_id', width : 40, sortable : true, align: 'center'},
            {display: '会员名称', name : 'member_name', width : 120, sortable : false, align: 'center'},
            {display: '拍品ID', name : 'auction_id', width : 120, sortable : false, align: 'center'},
            {display: '出价价格', name : 'offer_num', width : 120, sortable : true, align: 'center'},
            {display: '出价时间', name : 'created_at', width : 150, sortable : false, align: 'center'},
            {display: '拍品名称', name : 'auction_name', width : 150, sortable : false, align: 'center'},
            {display: '专场名称', name : 'special_name', width : 150, sortable : false, align: 'center'},
            ],
        sortname: "bid_id",
        sortorder: "desc",
        title: '出价'
    });
});
</script>