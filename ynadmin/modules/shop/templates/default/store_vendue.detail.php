<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=store_vendue&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>商家拍卖管理 - 查看商家“<?php echo $output['vendue_info']['store_name'];?>”的店铺拍卖信息</h3>
            </div>
        </div>
    </div>


    <form id="form_store_verify" action="index.php?act=store_vendue&op=store_vendue_verify" method="post">
        <input id="verify_type" name="verify_type" type="hidden" />
        <input name="store_vendue_id" type="hidden" value="<?php echo $output['vendue_info']['store_vendue_id'];?>" />
        <input name="member_id" type="hidden" value="<?php echo $output['store_info']['member_id'];?>" />
        <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
            <thead>
            <tr>
                <th colspan="20">商家拍卖基本信息</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>店铺名称：</th>
                <td><?php echo $output['vendue_info']['store_name'];?></td>
            </tr>
            <tr>
                <th>所在地区：</th>
                <td><?php echo $output['vendue_info']['area_name'];?> </td>
            </tr>
            <tr>
                <th>机构类型：</th>
                <td colspan="20"><?php echo $output['vendue_info']['oz_type'];?></td>
            </tr>
            <tr>
                <th>主营：</th>
                <td colspan="20"><?php echo $output['vendue_info']['store_zy'];?></td>
            </tr>
            <tr>
                <th>拍卖logo：</th>
                <td><a nctype="nyroModal"  href="<?php echo getStoreLogo($output['vendue_info']['store_avatar']);?>"> <img src="<?php echo getStoreLogo($output['vendue_info']['store_avatar']);?>" alt="" /> </a></td>
            </tr>
            <tr>
                <th>入驻时间：</th>
                <td><?php echo date('Y-m-d',$output['vendue_info']['store_time']);?></td>
            </tr>
            <tr>
                <th>简介：</th>
                <td><?php echo $output['vendue_info']['store_vendue_intro'];?></td>
            </tr>
            <tr>
                <th>拍卖保障金：</th>
                <td>
                    <?php echo ncPriceFormat($output['store_info']['vendue_bail'])?>
                </td>
            </tr>
            <tr>
                <th>经营类目：</th>
                <td colspan="2"><table border="0" cellpadding="0" cellspacing="0" id="table_category" class="type">
                        <thead>
                        <tr>
                            <th>分类1</th>
                            <th>分类2</th>
                            <th>分类3</th>
                            <th>比例</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $store_class_names = unserialize($output['joinin_detail']['store_class_names']);?>
                        <?php if(!empty($store_class_names) && is_array($store_class_names)) {?>
                            <?php $store_class_auction_commis_rates = explode(',', $output['joinin_detail']['store_class_auction_commis_rates']);?>
                            <?php for($i=0, $length = count($store_class_names); $i < $length; $i++) {?>
                                <?php list($class1, $class2, $class3) = explode(',', $store_class_names[$i]);?>
                                <tr>
                                    <td><?php echo $class1;?></td>
                                    <td><?php echo $class2;?></td>
                                    <td><?php echo $class3;?></td>
                                    <td><?php if(intval($output['vendue_info']['store_apply_state']) === 10) {?>
                                            <input type="text" nctype="commis_rate" value="<?php echo $store_class_auction_commis_rates[$i];?>" name="auction_commis_rates[]" class="w100" />
                                            %
                                        <?php } else { ?>
                                            <?php echo $store_class_auction_commis_rates[$i];?> %
                                        <?php } ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table></td>
            </tr>
            <?php if(in_array(intval($output['vendue_info']['store_apply_state']), array(10))) { ?>
                <tr>
                    <th>审核意见：</th>
                    <td colspan="2"><textarea id="check_message" name="check_message"></textarea></td>
                </tr>
            <?php } ?>
            <?php if(in_array(intval($output['vendue_info']['store_apply_state']), array(11,40))){?>
                <tr>
                    <th>上传付款凭证：</th>
                    <td><img src="<?php echo getVendueLogo($output['vendue_info']['paying_money_certificate'])?>" />
                        <span></span></td>
                </tr>
                <tr>
                    <th>备注：</th>
                    <td><?php echo $output['vendue_info']['paying_money_certif_exp']?>
                        <span></span></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <?php if(in_array(intval($output['vendue_info']['store_apply_state']), array(10))) { ?>
            <div id="validation_message" style="color:red;display:none;"></div>
            <div class="bottom"><a id="btn_pass" class="ncap-btn-big ncap-btn-green mr10" href="JavaScript:void(0);">通过</a><a id="btn_fail" class="ncap-btn-big ncap-btn-red" href="JavaScript:void(0);">拒绝</a> </div>
        <?php } ?>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('a[nctype="nyroModal"]').nyroModal();

        $('#btn_fail').on('click', function() {
            if($('#check_message').val() == '') {
                $('#validation_message').text('请输入审核意见');
                $('#validation_message').show();
                return false;
            } else {
                $('#validation_message').hide();
            }
            if(confirm('确认拒绝申请？')) {
                $('#verify_type').val('fail');
                $('#form_store_verify').submit();
            }
        });
        $('#btn_pass').on('click', function() {

                $('#validation_message').hide();
                if(confirm('确认通过申请？')) {
                    $('#verify_type').val('pass');
                    $('#form_store_verify').submit();
                }

        });
    });
</script>