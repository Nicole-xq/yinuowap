<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .ui-datepicker {
        width: 20em;
        padding: .2em .2em 0;
        display: none;
    }
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="index.php?act=auction_special&op=platform_special" title="返回平台专场列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>平台专场管理 - 新增</a></h3>
            </div>
        </div>
    </div>
    <form id="add_form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="activity_title"><em>*</em>专场名称：</label>
                </dt>
                <dd class="opt">
                    <input name="special_name" type="text" class="text w400" value="" />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
<!--            <dl class="row">-->
<!--                <dt class="tit">-->
<!--                    <label><em>*</em>送拍机构：</label>-->
<!--                </dt>-->
<!--                <dd class="opt">-->
<!--                    <input name="delivery_mechanism" type="text" class="text w200" value="" />-->
<!--                    <span class="err"></span>-->
<!--                    <p class="notic"></p>-->
<!--                </dd>-->
<!--            </dl>-->
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>专场缩略图：</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="type-file-box">
            <input type="file" class="type-file-file" id="special_image" name="special_image" size="30" hidefocus="true"  nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
            </span></div>
                    <span class="err"></span>
                    <p class="notic">支持jpg、jpeg、gif、png格式</p>
                    <input type="hidden" name="special_pic" id="special_pic" value=""/>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>专场广告图：：</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="type-file-box">
            <input type="file" class="type-file-file" id="adv_image" name="adv_image" size="30" hidefocus="true"  nc_type="upload_activity_banner" title="点击按钮选择文件并提交表单后上传生效">
            </span></div>
                    <span class="err"></span>
                    <p class="notic">支持jpg、jpeg、gif、png格式</p>
                    <input type="hidden" name="adv_pic" id="adv_pic" value=""/>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>专场开始时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="special_start_time" name="special_start_time" class="input-txt" />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>专场结束时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="special_end_time" name="special_end_time" class="input-txt"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>预展开始时间：</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="special_preview_start" name="special_preview_start" class="input-txt" />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label for="activity_sort">状态</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="is_open1" class="cb-enable selected" >开启</label>
                        <label for="is_open0" class="cb-disable">关闭</label>
                        <input id="is_open1" name="is_open" checked="checked" value="1" type="radio">
                        <input id="is_open0" name="is_open" value="0" type="radio">
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
        </div>
    </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script>
    //按钮先执行验证再提交表单
    $(function(){$("#submitBtn").click(function(){
        if($("#add_form").valid()){
            $("#add_form").submit();
        }
    });
    });
    $(document).ready(function(){
        $('#special_start_time').datetimepicker({
            controlType: 'select'
        });

        $('#special_end_time').datetimepicker({
            controlType: 'select'
        });
        $('#special_preview_start').datetimepicker({
            controlType: 'select'
        });


        //专场预展开始时间不能先于现在
        $.validator.addMethod('greaterThanNowDate', function(value,element){
            var date1 = new Date();
            var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
            return date1 < date2;
        }, '');
        //预展结束时间不能先于预展开始时间
        $.validator.addMethod('greaterThanPreStartTime', function(value,element){
            var start_date = $("#special_preview_start").val();
            var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
            var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
            return date1 < date2;
        }, '');



        //专场结束时间不能先于专场开始时间
        $.validator.addMethod('greaterThanStartTime', function(value,element){
            var start_date = $("#special_start_time").val();
            var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
            var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
            return date1 < date2;
        }, '');
        $("#add_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parents('dl').find('span.err');
                error_td.append(error);
            },
            rules : {
                special_name: {
                    required : true,
                    minlength   : 5,
                    maxlength   : 20
                },
//                delivery_mechanism: {
//                    required : true
//                },
                special_preview_start: {
                    required : true,
                    greaterThanNowDate: true
                },
                special_start_time: {
                    required : true,
                    greaterThanPreviewEnd: true
                },
                special_end_time: {
                    required : true,
                    greaterThanStartTime: true
                },
                special_pic: {
                    required : true,
                    accept : 'png|jpe?g|gif'
                },
                adv_pic: {
                    required : true,
                    accept : 'png|jpe?g|gif'
                }
            },
            messages : {
                special_name: {
                    required : '<i class="icon-exclamation-sign"></i>专场名称不能为空',
                    minlength   : '<i class="icon-exclamation-sign"></i>专场名称最少是5个汉字',
                    maxlength   : '<i class="icon-exclamation-sign"></i>专场名称最多是20个汉字'
                },
//                delivery_mechanism: {
//                    required : '<i class="icon-exclamation-sign"></i>送拍机构不能为空'
//                },
                special_preview_start: {
                    required : '<i class="icon-exclamation-sign"></i>预展开始时间不能为空',
                    greaterThanNowDate: '<i class="icon-exclamation-sign"></i>不能早于现在'
                },
                special_start_time: {
                    required    : '<i class="icon-exclamation-sign"></i>专场开始时间不能为空',
                    greaterThanPreviewEnd: '<i class="icon-exclamation-sign"></i>应为专场预展结束时间'
                },
                special_end_time: {
                    required    : '<i class="icon-exclamation-sign"></i>专场结束时间不能为空',
                    greaterThanStartTime: '<i class="icon-exclamation-sign"></i>不能早于专场开始时间'
                },
                special_pic: {
                    required : '<i class="icon-exclamation-sign"></i>请设置专场缩略图',
                    accept   : '<i class="fa fa-exclamation-circle"></i>图片限于png,gif,jpeg,jpg格式'
                },
                adv_pic: {
                    required : '<i class="icon-exclamation-sign"></i>请设置专场广告图',
                    accept   : '<i class="fa fa-exclamation-circle"></i>图片限于png,gif,jpeg,jpg格式'
                }
            }
        });
    });

    $(function(){
// 模拟活动页面横幅Banner上传input type='file'样式
        var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
        $(textButton).insertBefore("#special_image");
        $("#special_image").change(function(){
            $("#textfield1").val($("#special_image").val());
            $('#special_pic').val($("#special_image").val());
        });
        var textButton1="<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton1).insertBefore("#adv_image");
        $("#adv_image").change(function(){
            $("#textfield2").val($("#adv_image").val());
            $('#adv_pic').val($("#adv_image").val());
        });
    });
</script>