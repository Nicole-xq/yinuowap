<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<style type="text/css">
  .ncap-goods-sku .title h4, .ncap-goods-sku .content span{
    width: 14%;
    padding: 3px 1%;
  }
</style>
<div class="ncap-goods-sku">
    <div class="title">
        <h4>SKU编号</h4>
        <h4>SKU图片</h4>
        <h4>SKU库存</h4>
        <h4>SKU价格(元)</h4>
        <h4>一级返佣比例</h4>
        <h4>二级返佣比例</h4>
    </div>
    <div class="content">
        <ul>
            <?php foreach ($output['goods_list'] as $val) { ?>
                <li>
                    <span><?php echo $val['goods_id']; ?></span>
                    <span>
                      <img src="<?php echo $val['goods_image']; ?>" onMouseOver="toolTip('<img src=<?php echo $val['goods_image']; ?>>')" onMouseOut="toolTip()">
                    </span> 
                    <span><?php echo $val['goods_storage']; ?></span> 
                    <span><?php echo $val['goods_price']; ?></span>
                    <span>
                      <input type="text" name="commis_level_1[<?php echo $val['goods_id']; ?>]" class="w40" goods-id="<?php echo $val['goods_id']; ?>" data-type="commis_level_1" value="<?php echo $val['commis_level_1']; ?>">&nbsp;%
                    </span>
                    <span>
                      <input type="text" name="commis_level_2[<?php echo $val['goods_id']; ?>]" class="w40" goods-id="<?php echo $val['goods_id']; ?>" data-type="commis_level_2" value="<?php echo $val['commis_level_2']; ?>">&nbsp;%
                    </span>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //自动加载滚动条
        $('.content').perfectScrollbar();
        $('input').blur(function () {
            var percent = parseFloat($(this).val());
            var goods_id = parseInt($(this).attr('goods-id'));
            var column = $(this).attr('data-type');
            if (percent <= 0 || percent > 100) {
                return false;
            }
            $.get('index.php?act=goods&op=goods_precent_set&per=' + percent + '&goods_id=' + goods_id + '&column=' + column);
        });
    });
</script>