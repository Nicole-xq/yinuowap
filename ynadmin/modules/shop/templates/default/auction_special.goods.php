<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
     .big-list{width:1232px;margin: 0 auto;}
     .big-list li{width:280px; height: 410px;margin: 13px; border: #e6e6e6 solid 1px; background: #f6f6f6;float: left;}
     .big-list li a{display:block;width: 100%; height: 410px;}
     .big-list li:hover{-moz-box-shadow:0px 3px 6px #dadada; -webkit-box-shadow:0px 3px 6px #dadada; box-shadow:0px 3px 6px #dadada;transition:all 0.2s ease-in-out 0.1s;-moz-transition:all 0.2s ease-in-out 0.1s;-ms-transition:all 0.2s ease-in-out 0.1s;-o-transition:all 0.2s ease-in-out 0.1s;-webkit-transition:all 0.2s ease-in-out 0.1s;}
     .big-list li a img{ display:block; width: 280px; height: 280px}
     .big-list .new-tab-list{width: 252px; padding:14px;}
     .big-list h1{padding:6px 0 3px ;font-size:14px; color: #000;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
     .big-list span{font-size:14px; color: #666;line-height: 28px}
     .big-list span b{color: #800000}
     .big-list .new-tab-list-title{width:190px;}
     .big-list .new-tab-list-title h1{line-height: 26px}
    .big-list .offer{width: 54px; height:64px; background: #000}
     .big-list .offer b:first-child{padding: 12px 0 0; font-size:16px;}
     .big-list .offer b{display: block; font-size:12px; color:#fff; text-align: center;}
     .big-list .time{margin: 12px 0 0}
     .big-list .time .time-remain em{display:inline-block;padding: 0 6px;margin: 0 4px; background: url(../images/new-index/time-bg.jpg) center repeat-x;color:#fff; font-weight: bold;font-size: 13px;}
     .big-list .time .sell{color:#000}
    .fr{float:right;}
    .fl{float:left;}
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=auction_special&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>专场管理 - 查看商家“<?php echo $output['special_info']['store_name'];?>”的专场商品信息</h3>
            </div>
        </div>
    </div>
<ul class="big-list">
    <?php foreach($output['special_auction_list'] as $k=>$v){?>
    <li>
        <a href="<?php echo urlVendue('auctions','index',array('id'=>$v['auction_id']))?>" target="_blank" title="">
            <img src="<?php echo cthumb($v['auction_image'], 240,$v['store_id']);?>" alt=""/>
            <div class="new-tab-list fr">
                <div class="new-tab-list-title fl">
                    <h1><?php echo $v['auction_name'];?></h1>
                    <span>当前价&nbsp;&nbsp;<b><?php if($v['current_price'] != 0.00){echo ncPriceFormatForList($v['current_price']);}else{echo ncPriceFormatForList($v['auction_start_price']);}?></b></span>
                    <?php if($v['real_participants'] >= 0){?>
                        <br><span>当前参与人数：<?php echo $v['real_participants']?>人</span>
                    <?php }?>
                </div>
                <div class="offer fr">
                    <b><?php echo $v['bid_number'];?></b>
                    <b>次出价</b>
                </div>
                <div class="clear"></div>
<!--                <dd class="time">-->
<!--                    <span class="sell">距结束&nbsp;</span>-->
<!--						<span class="time-remain" count_down="--><?php //echo $val['end_time']-TIMESTAMP; ?><!--">-->
<!--						<em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒 </span>-->
<!--                </dd>-->
            </div>
        </a>
    </li>
    <?php }?>

</ul>
    <div class="clear"></div>
    <div class="tc mt20 mb20">
        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
    </div>
</div>