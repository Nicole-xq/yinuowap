<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=robot" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>机器人账号管理 - 新增机器人账号</h3>
        <h5>商城的机器人账号管理</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="robot_name"><em>*</em>机器人名称</label>
        </dt>
        <dd class="opt">
          <input type="text" id="robot_name" name="robot_name" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表
$(document).ready(function(){
	//按钮先执行验证再提交表单
	$("#submitBtn").click(function(){
	    if($("#add_form").valid()){
	     $("#add_form").submit();
		}
	});
	
	$("#add_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
		
        rules : {
            robot_name : {
                required : true,
				minlength: 3,
				maxlength: 10,
				remote	: {
                    url :'index.php?act=robot&op=check_name',
                    type:'get',
                    data:{
                    	robot_name : function(){
                            return $('#robot_name').val();
                        }
                    }
                }
            }
        },
        messages : {
            robot_name : {
                required : '<i class="fa fa-exclamation-circle"></i>机器人名称不能为空',
				minlength: '<i class="fa fa-exclamation-circle"></i>名称长度为3-10',
				maxlength: '<i class="fa fa-exclamation-circle"></i>名称长度为3-10',
				remote   : '<i class="fa fa-exclamation-circle"></i>该名称已存在'
            }
        }
	});
});
</script> 
