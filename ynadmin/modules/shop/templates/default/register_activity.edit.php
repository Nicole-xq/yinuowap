<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=register_activity&op=activity" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>编辑</h3>
        <h5><?php echo $lang['activity_index_manage_subhead']; ?></h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['activity']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="activity_title"><em>*</em>奖励类型</label>
        </dt>
        <dd class="opt">
            <label for="type">
              <input type="radio" value="0" id="type" name="type" <?php if($output['activity']['type'] == 0){?>checked<?php }?>>红包
            </label>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>奖品编码</label>
        </dt>
        <dd class="opt">
          <input type="text" id="code" name="code" class="input-txt" value="<?=$output['activity']['code'];?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="start_time"><em>*</em>开始时间</label>
        </dt>
        <dd class="opt">
          <input type="text" id="start_time" class="input-txt" name="start_time" value="<?php echo date('Y-m-d',$output['activity']['start_time']);?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="end_time"><em>*</em>结束时间</label>
        </dt>
        <dd class="opt">
          <input type="text" id="end_time" class="input-txt" name="end_time" value="<?php if(!empty($output['activity']['end_time']))echo date('Y-m-d',$output['activity']['end_time']);?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="activity_sort">状态</label>
        </dt>
        <dd class="opt">
          <div class="onoff">
            <label for="activity_state1" class="cb-enable <?php echo $output['activity']['status'] == 1?'selected':'';?>" >开</label>
            <label for="activity_state0" class="cb-disable <?php echo $output['activity']['status'] == 0?'selected':'';?>">关</label>
            <input id="activity_state1" name="status" <?php if($output['activity']['status'] == 1){ ?>checked="checked"<?php } ?> value="1" type="radio">
            <input id="activity_state0" name="status" <?php if($output['activity']['status'] == 0){ ?>checked="checked"<?php } ?> value="0" type="radio">
          </div>
          <p class="notic"></p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#add_form").valid()){
     $("#add_form").submit();
	}
	});
});
$(document).ready(function(){
	$("#start_time").datepicker();
	$("#end_time").datepicker();
	$("#add_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
	        	activity_title: {
	    		required : true
	    	},
	    	activity_start_date: {
	    		required : true,
				date      : false
	    	},
	    	activity_end_date: {
	    		required : true,
				date      : false
	    	},
	    	activity_sort: {
	    		required : true,
	    		min:0,
	    		max:255
	    	}
        },
        messages : {
        	activity_title: {
	    		required : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_title_null'];?>'
	    	},
	    	activity_start_date: {
	    		required : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_startdate_null'];?>'
	    	},
	    	activity_end_date: {
	    		required : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_enddate_null'];?>'
	    	},
	    	activity_sort: {
	    		required : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_sort_null'];?>',
	    		min:'<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_sort_minerror'];?>',
	    		max:'<i class="fa fa-exclamation-circle"></i><?php echo $lang['activity_new_sort_maxerror'];?>'
	    	}
        }
	});
});

$(function(){
// 模拟活动页面横幅Banner上传input type='file'样式
	var textButton="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />"
    $(textButton).insertBefore("#activity_banner");
    $("#activity_banner").change(function(){
	$("#textfield1").val($("#activity_banner").val());
    });
$('.nyroModal').nyroModal();
});
</script>