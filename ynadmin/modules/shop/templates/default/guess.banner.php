<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>趣猜管理</h3>
      </div>
     <ul class="tab-base nc-row">
        <li><a href="index.php?act=promotion_guess">活动列表</a></li>
        <li><a href="index.php?act=promotion_guess&op=guess_verify_list">待审核列表</a></li>
        <li><a href="JavaScript:void(0);" class="current">趣猜公共banner</a></li>
      </ul>
    </div>
  </div>

  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>图片要求使用宽度为1920像素，高度为480像素jpg/gif/png格式的图片。</li>
    </ul>
  </div>
  <form id="live_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label>趣猜公共banner</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/guess_banner'.DS.$output['list_setting']['guess_banner'];?>"/><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/guess_banner'.DS.$output['list_setting']['guess_banner'];?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="guess_banner" type="file" class="type-file-file" id="guess_banner" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield1' id='textfield1' class='type-file-text' />
            <input type='button' name='button1' id='button1' value='选择上传...' class='type-file-button' />
            </span></div>
          <label title="请输入图片要跳转的链接地址" class="ml5"><i class="fa fa-link"></i>
            <input class="input-txt ml5" type="text" name="guess_banner_link"  value="<?php echo $output['list_setting']['guess_banner_link']?>" placeholder="请输入图片要跳转的链接地址" />
          </label><span class="err"></span>
          <p class="notic"> 如需跳转请在后方添加以http://开头的链接地址。</p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
//按钮先执行验证再提交表单
$(function(){
    // 图片js
    $("#guess_banner").change(function(){$("#textfield1").val($("#guess_banner").val());});
	$('.nyroModal').nyroModal();
    $('#live_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parents('dd').children('span.err');
            error_td.append(error);
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            guess_banner_link: {
                url : true
            },
        },
        messages : {
            guess_banner_link: {
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
        }
    });
    $("#submitBtn").click(function(){
        $("#live_form").submit();
    });
});
</script>
