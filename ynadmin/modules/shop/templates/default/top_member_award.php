<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo ADMIN_RESOURCE_URL?>/js/admin.js" type="text/javascript"></script>
<form method="post" name="form1" id="form1" class="ncap-form-dialog" action="<?php echo urlAdminShop('register_activity', 'get_setting_info');?>">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" value="<?php echo $output['special_info']['special_id'];?>" name="special_id">
    <div class="ncap-form-default">
        <dl class="row" nctype="reason">
            <dt class="tit">
                <label for="auction_bond_rate">推荐奖励红包ID</label>
            </dt>
            <dd class="opt">
                <input name="award_id" value="<?=$output['award_id'];?>" type="text" class="text w30" /><span></span>
          <p class="hint">请确保红包设置,可重复领取.有效期.剩余数量</p>
            </dd>
        </dl>
        <div class="bot"><a href="javascript:void(0);" class="ncap-btn-big ncap-btn-green" nctype="btn_submit"><?php echo $lang['nc_submit'];?></a></div>
    </div>
</form>
<script>
    $(function(){
        $('a[nctype="btn_submit"]').click(function(){
            if($("#form1").valid()) ajaxpost('form1', '', '', 'onerror');
        });
    $('#form1').validate({
        errorPlacement: function(error, element){
            $(element).nextAll('span').append(error);
        },
        rules : {
            auction_bond_rate : {
                number   : true,
                min:0
//                max:10
            }
        },
        messages : {
            auction_bond_rate : {
                number   : '<i class="fa fa-exclamation-circle"></i>请填写正确的红包ID',
                min   : '<i class="fa fa-exclamation-circle"></i>请填写正确的红包ID'
//                max   : '<i class="fa fa-exclamation-circle"></i>请填写正确的保证金利息'
            }
        }
    });
    });
</script>