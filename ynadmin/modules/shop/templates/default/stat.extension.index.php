<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>推广统计</h3>
                <h5>平台针对通过渠道的公众号关注、取关人数统计</h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title']; ?>"><?php echo $lang['nc_prompts']; ?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span']; ?>"></span></div>
        <ul>
            <li>总关注从始至今的还在关注人数</li>
            <li>月(关注|取消)记录从本月初至今的关注量，每月底会自动为0。</li>
        </ul>
    </div>

    <div class="ncap-form-all ncap-stat-general-single">
        <div class="title">
            <h3>关注情况一览</h3>
        </div>
        <dl class="row">
            <dd class="opt">
                <?php echo $output['stat_content']; ?>
            </dd>
        </dl>
    </div>

    <div class="ncap-stat-chart">
        <div class="title">
            <h3><?php echo $output['stat_time']; ?> 关注走势</h3>
        </div>
        <div id="container" class=" " style="height:400px"></div>
    </div>

    <div id="flexigrid"></div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/statistics.js"></script>
<script>
    $(function () {
        $("#flexigrid").flexigrid({
            url: 'index.php?act=stat_extension&op=get_extension_xml',
            colModel: [
                {display: '操作', name: 'operation', width: 60, sortable: false, align: 'center', className: 'handle-s'},
                {display: 'ID', name: 'id', width: 80, sortable: true, align: 'center'},
                {display: '用户ID', name: 'member_id', width: 80, sortable: false, align: 'center'},
                {display: '手机号', name: 'member_mobile', width: 110, sortable: true, align: 'center'},
                {display: '邀请码', name: 'extension_code', width: 80, sortable: true, align: 'center'},
                {display: '渠道名称', name: 'extension_name', width: 150, sortable: true, align: 'center'},
                {display: '渠道分类', name: 'extension_type', width: 50, sortable: true, align: 'center'},
                {display: '渠道种类', name: 'extension_class', width: 50, sortable: true, align: 'center'},
                {display: '地区', name: 'area_info', width: 150, sortable: true, align: 'center'},
                {display: '当前总关注量', name: 'total_follow', width: 80, sortable: true, align: 'center'},
                {display: '本月关注量', name: 'month_follow', width: 80, sortable: true, align: 'center'},
                {display: '本月取关量', name: 'month_cancel', width: 80, sortable: true, align: 'center'},
                {display: '创建时间', name: 'add_time', width: 150, sortable: true, align: 'center'}
            ],
            searchitems: [
                {display: '用户ID', name: 'member_id'},
                {display: '手机号', name: 'member_mobile'},
                {display: '邀请码', name: 'extension_code'}
            ],
            sortname: "id",
            sortorder: "desc",
            rp: 10,
            title: '推广列表'
        });

        $('#container').highcharts(<?php echo $output['stat_month_json'];?>);
    });
</script>