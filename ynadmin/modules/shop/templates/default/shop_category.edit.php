<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=shop_category" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>推荐分类"<?=$output['list']['category_name'];?>"</h3>
      </div>
    </div>
  </div>
  <form id="shop_category_form" method="post" name="form1" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="id" value="<?php echo $output['list']['id']?>" />
      <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">推荐分类</dt>
        <dd class="opt" id="gcategory">
          <select id="category_id" name="category_id" style="width: auto;">
            <option value="0">-请选择-</option>
            <?php if(!empty($output['list']['list']) && is_array($output['list']['list']) ) {?>
            <?php foreach ($output['list']['list'] as $category) {?>
            <option value="<?php echo $category['stc_id'];?>" <?php if($output['list']['category_id'] == $category['stc_id']){ ?>selected<?php }?>><?php echo $category['stc_name'];?></option>
            <?php }?>
            <?php }?>
          </select>
          <span id="error_message" style="color:red;"></span>
        </dd>
      </dl>
      </div>
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">分类图片</dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_SCATEGORY.'/'.$output['list']['img']; ?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.ATTACH_SCATEGORY.'/'.$output['list']['img']; ?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input class="type-file-file" id="_pic" name="_pic" type="file" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            <input type="text" name="img" id="shop_category_pic" class="type-file-text" />
            <input type="button" name="button" id="button" value="选择上传..." class="type-file-button" />
            </span></div>
          <span class="err"></span>
          <p class="notic"><?php echo $lang['shop_category_index_upload_tips'].$lang['shop_category_add_support_type'];?>gif,jpg,png</p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
function call_back(picname){
	$('#shop_category_pic').val(picname);
	$('#view_img').attr('src','<?php echo UPLOAD_SITE_URL.'/'.ATTACH_SCATEGORY;?>/'+picname);
}

//按钮先执行验证再提交表单
$(function(){
// 点击查看图片
	$('.nyroModal').nyroModal();
    // 编辑分类时清除分类信息
    $('.edit_gcategory').click(function(){
        $('input[name="class_id"]').val('');
        $('input[name="shop_category_class"]').val('');
    });

	$("#submitBtn").click(function(){
		if($("#shop_category_form").valid()){
		 $("#shop_category_form").submit();
		}
	});
	$('input[class="type-file-file"]').change(uploadChange);
	function uploadChange(){
		var filepath=$(this).val();
		var extStart=filepath.lastIndexOf(".");
		var ext=filepath.substring(extStart,filepath.length).toUpperCase();
		if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
			alert("file type error");
			$(this).attr('value','');
			return false;
		}
		if ($(this).val() == '') return false;
		ajaxFileUpload();
	}
	function ajaxFileUpload()
	{
		$.ajaxFileUpload
		(
			{
				url : '<?php echo ADMIN_SITE_URL?>/index.php?act=common&op=pic_upload&form_submit=ok&uploadpath=<?php echo ATTACH_SCATEGORY;?>',
				secureuri:false,
				fileElementId:'_pic',
				dataType: 'json',
				success: function (data, status)
				{
					if (data.status == 1){
						ajax_form('cutpic','<?php echo $lang['nc_cut'];?>','<?php echo ADMIN_SITE_URL?>/index.php?act=common&op=pic_cut&type=shop_category&x=200&y=200&resize=1&ratio=3&url='+data.url,690);
					}else{
						alert(data.msg);
					}
					$('input[class="type-file-file"]').bind('change',uploadChange);
				},
				error: function (data, status, e)
				{
					alert('上传失败');$('input[class="type-file-file"]').bind('change',uploadChange);
				}
			}
		)
	};
	jQuery.validator.addMethod("initial", function(value, element) {
		return /^[A-Za-z0-9]$/i.test(value);
	}, "");
	$("#shop_category_form").validate({
		errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            shop_category_name : {
                required : true,
                remote   : {
                    url :'index.php?act=shop_category&op=ajax&branch=check_shop_category_name',
                    type:'get',
                    data:{
                        shop_category_name : function(){
                            return $('#shop_category_name').val();
                        },
                        id  : '<?php echo $output['shop_category_array']['shop_category_id']?>'
                    }
                }
            },
            shop_category_initial : {
                initial  : true
            },
            shop_category_sort : {
                number   : true
            }
        },
        messages : {
            shop_category_name : {
                required : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['shop_category_add_name_null'];?>',
                remote   : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['shop_category_add_name_exists'];?>'
            },
            shop_category_initial : {
                initial : '<i class="fa fa-exclamation-circle"></i>请填写正确首字母'
            },
            shop_category_sort  : {
                number   : '<i class="fa fa-exclamation-circle"></i><?php echo $lang['shop_category_add_sort_int'];?>'
            }
        }
	});
});

gcategoryInit('gcategory');
</script>
