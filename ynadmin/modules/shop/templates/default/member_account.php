<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="<?php echo getReferer();?>" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>会员账户管理 </h3>
      </div>
    </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>充值（成功）列表；提现（成功）列表；奖励金（商品订单返佣、保证金返佣、保证金利息、合伙人升级、艺术家入驻佣金）列表</li>
      <li>预付款支付订单（用预存款支付的商品订单）列表；保证金支付（第三方支付宝或是微信支付的金额）列表；冻结保证金金额（拍卖商品未结算的成功支付金额）列表；</li>
    </ul>
  </div>
  <div class="ncap-form-default">
    <dl class="row">
      <dt class="tit">用户ID：</dt>
      <dd class="opt">
          <?php echo $output['member_info']['member_id'];?>
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">用户名称：</dt>
      <dd class="opt">
          <?php echo $output['member_info']['member_name'];?>
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">当前申请提现金额：</dt>
      <dd class="opt">
          <?php echo $output['member_info']['pdc_amount'];?> 元
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">实际账户金额：</dt>
      <dd class="opt">
          <?php echo $output['member_info']['available_predeposit'];?> 元
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">实际预存款冻结金额：</dt>
      <dd class="opt">
          <?php echo $output['member_info']['freeze_predeposit'];?> 元
      </dd>
    </dl>
    <dl class="row">
      <dt class="tit">平台统计金额：</dt>
      <dd class="opt">
          <?php echo ncPriceFormat($output['member_info']['result_totals']);?> 元 =
          <?php echo ncPriceFormat($output['member_info']['recharge_sum']);?> (充值金额/元) +
          <?php echo ncPriceFormat($output['member_info']['earnest_sum']);?> (保证金支付金额/元) +
          <?php echo ncPriceFormat($output['member_info']['commission_sum']);?> (奖励金/元) -
          <?php echo ncPriceFormat($output['member_info']['enchashment_sum']);?> (提现金额/元) -
          <?php echo ncPriceFormat($output['member_info']['order_pay_sum']);?> (预存款订单支付金额/元) -
          <?php echo ncPriceFormat($output['member_info']['freeze_predeposit']);?> (预存款冻结金额/元) -
          <?php echo ncPriceFormat($output['member_info']['auction_sum']);?> (冻结保证金金额/元)+
          <?php echo ncpriceFormat($output['member_info']['order_channel_sum']);?>(订单取消释放金额/元)
      </dd>
    </dl>
  </div>
  <div class="homepage-focus" nctype="sellerTplContent">
    <div class="title">
      <ul class="tab-base nc-row">
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=recharge" class="<?php echo ($_GET['query_type'] == '' || $_GET['query_type'] == 'recharge') ? 'current' : '';?>">充值列表</a></li>
          <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=earnest" class="<?php echo $_GET['query_type'] == 'earnest' ? 'current' : '';?>">保证金支付列表</a></li>
          <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=commission" class="<?php echo $_GET['query_type'] == 'commission' ? 'current' : '';?>">奖励金列表</a></li>
          <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=enchashment" class="<?php echo $_GET['query_type'] == 'enchashment' ? 'current' : '';?>">提现列表</a></li>
          <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=order_pay" class="<?php echo $_GET['query_type'] == 'order_pay' ? 'current' : '';?>">预存款支付订单列表</a></li>
          <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=auction" class="<?php echo $_GET['query_type'] == 'auction' ? 'current' : '';?>">冻结保证金金额列表</a></li>
        <li><a href="index.php?<?php echo $_SERVER['QUERY_STRING'];?>&query_type=predeposit" class="<?php echo $_GET['query_type'] == 'predeposit' ? 'current' : '';?>">预存款变动明细列表</a></li>
      </ul>
    </div>
    <?php include template('member_account_check_list');?>
  </div>
</div>
