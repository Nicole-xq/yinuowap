<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>尊享专区轮播图</h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>.................</li>
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=slideshow&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: 'ID', name : 'id', width : 40, sortable : true, align: 'center'},
            {display: '名称', name : 'title', width : 150, sortable : false, align: 'left'},
            {display: '是否生效', name : 'state', width : 150, sortable : false, align: 'left'},
            ],
        buttons : [
			{display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '添加一条新数据到列表', onpress : fg_operation }
        ],
//        searchitems : [
//            {display: '店铺名称', name : 'store_name', isdefault: true},
//            {display: '店主账号', name : 'member_name'},
//            {display: '商家账号', name : 'seller_name'}
//            ],
//        sortname: "id",
//        sortorder: "asc",
        title: '轮播图列表'
    });
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=slideshow&op=add';
    }
}

function fg_delete(id) {
    if(confirm('删除后将不能恢复，确认删除这项吗？')){
        $.getJSON('index.php?act=slideshow&op=del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
}
</script>
