<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="javascript:history.back(-1)" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>拍卖订单 - 处理退款“退款单号：<?php echo $output['order_info']['auction_order_sn']; ?>”</h3>
                <h5>拍卖订单退款处理</h5>
            </div>
        </div>
    </div>
    <form id="post_form" method="post" action="index.php?act=auction_order&op=refund_order_edit&order_id=<?php echo $output['order_info']['auction_order_id']; ?>">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default">
            <div class="title">
                <h3>买家退款申请</h3>
            </div>
            <dl class="row">
                <dt class="tit">申请时间</dt>
                <dd class="opt"><?php echo date('Y-m-d H:i:s',$output['order_info']['buyer_refund_time']); ?> </dd>
            </dl>
            <dl class="row">
                <dt class="tit">商品名称</dt>
                <dd class="opt">
                    <?php if ($output['order_info']['auction_id'] > 0) { ?>
                        <a href="<?php echo urlAuction('auctions','index',array('id'=> $output['order_info']['auction_id']));?>" target="_blank"><?php echo $output['order_info']['auction_name']; ?></a>
                    <?php }else { ?>
                        <?php echo $output['order_info']['auction_name']; ?>
                    <?php } ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">退款金额</dt>
                <dd class="opt"><?php echo ncPriceFormat($output['order_info']['order_amount']); ?>
                    <span id="pay_amount">
            <?php if ($output['order_info']['api_refund_amount'] > 0) { ?>
                (已完成在线退款金额 <?php echo ncPriceFormat($output['order_info']['api_refund_amount']); ?>)
            <?php } ?>
            </span>
                </dd>
            </dl>
            <div class="title">
                <h3>商家退款处理</h3>
            </div>
            <dl class="row">
                <dt class="tit">处理时间</dt>
                <dd class="opt"><?php echo date('Y-m-d H:i:s',$output['order_info']['seller_time']); ?> </dd>
            </dl>
            <div class="title">
                <h3>订单支付信息</h3>
            </div>
            <dl class="row">
                <dt class="tit">支付方式</dt>
                <dd class="opt"><?php echo orderPaymentName($output['order_info']['payment_code']);?></dd>
            </dl>
            <dl class="row">
                <dt class="tit">订单总额</dt>
                <dd class="opt"><?php echo ncPriceFormat($output['order_info']['order_amount']);?></dd>
            </dl>
            <?php if ($output['order_info']['points_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">诺币支付金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['points_amount']);?></dd>
                </dl>
            <?php } ?>
            <?php if ($output['order_info']['margin_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">保证金支付金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['margin_amount']);?></dd>
                </dl>
            <?php } ?>
            <?php if ($output['order_info']['rcb_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">充值卡支付金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['rcb_amount']);?></dd>
                </dl>
            <?php } ?>
            <?php if ($output['order_info']['pd_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">预存款支付金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['pd_amount']);?></dd>
                </dl>
            <?php } ?>
            <?php if ($output['order_info']['api_pay_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">在线支付金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['api_pay_amount']);?></dd>
                </dl>
                <?php if (empty($output['order_info']['api_refund_time']) && $output['order_info']['api_pay_amount'] > $output['order_info']['api_refund_amount']) { ?>
                    <?php if (in_array($output['order_info']['payment_code'],array('wxpay','wx_jsapi','wx_saoma'))) { ?>
                        <dl class="row">
                            <dt class="tit"></dt>
                            <dd class="opt"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="wxpayBtn">确认微信退款</a></dd>
                        </dl>
                    <?php } ?>
                    <?php if ($output['order_info']['payment_code'] == 'alipay') { ?>
                        <dl class="row">
                            <dt class="tit"></dt>
                            <dd class="opt">
                                <a href="<?php echo ADMIN_SITE_URL;?>/index.php?act=refund_auction&op=alipay&refund_id=<?php echo $output['order_info']['auction_order_id']; ?>" target="_blank">支付宝退款</a>
                                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="alipayBtn">退款查询</a>
                            </dd>
                        </dl>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if ($output['order_info']['refund_amount'] > 0) { ?>
                <dl class="row">
                    <dt class="tit">已退款金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['refund_amount']);?></dd>
                </dl>
            <?php } ?>
            <input type="hidden" id="is_points" name="is_points" value="<?php echo $output['order']['is_points']?>"/>
            <input type="hidden" id="online_amount" name="online_amount" value="<?php echo $output['order']['online_amount']?>"/>
            <div class="title">
                <h3>平台退款审核</h3>
            </div>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>平台备注</label>
                </dt>
                <dd class="opt">
                    <textarea id="admin_message" name="admin_message" class="tarea"></textarea>
                    <span class="err"></span>
                    <p class="notic">系统默认退款到“站内余额”，如果“在线退款”到原支付账号，建议在备注里说明，方便核对。</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/refund.js"></script>
<script type="text/javascript">
    $(function(){
        $('.nyroModal').nyroModal();
        $("#submitBtn").click(function(){
            if($("#post_form").valid()){
                if(confirm('提交后将不能恢复，确认吗？')) $("#post_form").submit();
            }
        });
        $("#wxpayBtn").click(function(){
            var ajaxurl = '<?php echo ADMIN_SITE_URL;?>/index.php?act=refund_auction&op=wxpay&refund_id=<?php echo $output['order_info']['auction_order_id']; ?>';
            show_msg(ajaxurl);
        });
        $("#alipayBtn").click(function(){
            var ajaxurl = '<?php echo ADMIN_SITE_URL;?>/index.php?act=refund_auction&op=get_detail&refund_id=<?php echo $output['order_info']['auction_order_id']; ?>';
            show_msg(ajaxurl);
        });

        $.validator.addMethod('checkPrice', function(value,element){
            _r_points = parseFloat($('input[name="return_points"]').val());
            _p_amont = parseFloat(<?php echo ($output['order']['points_amount']-$output['order']['return_points'])?>);
            _price = parseFloat($('input[name="online_amount"]').val());
            _refund_price = parseFloat(<?php echo $output['refund']['refund_amount']?>);
            if (_r_points > _p_amont) {
                return false;
            }else {
                if(_r_points + _price < _refund_price){
                    return false;
                }else {
                    return true;
                }

            }
        }, '');
        $('#post_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                admin_message : {
                    required   : true
                }
            },
            messages : {
                admin_message  : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写备注信息'
                }
            }
        });
    });
</script>