<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auctions_class&&op=auctions_spec_class_list&id=<?php echo $output['info']['auctions_spec_id']; ?>" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>规格(<?php echo $output['info']['auctions_spec_name']; ?>)属性- 新增</h3>
        <h5>拍品规格属性新增</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auctions_spec_class_name"><em>*</em>属性名称</label>
        </dt>
        <dd class="opt">
          <input type="text" id="auctions_spec_class_name" name="auctions_spec_class_name" class="input-txt" >
          <span class="err"></span>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
  $('#add_form').validate({
    errorPlacement: function(error, element){
      var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      auctions_spec_class_name: {
        required : true,
      }      
    },
    messages : {      
      auctions_spec_class_name : {
        required : '<i class="fa fa-exclamation-circle"></i>必填',               
      }
    }
  });
  $("#submitBtn").click(function(){
    if($("#add_form").valid()){
       $("#add_form").submit();
  	}
	});
});

</script>