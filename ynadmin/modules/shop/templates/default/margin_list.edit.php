<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=margin_list&op=index" title="返回分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>保证金展示</a></h3>
        <h5>保证金展示</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="auctions_class_id" value="<?php echo $output['class']['auctions_class_id'] ?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>结算编号</label>
        </dt>
        <dd class="opt">
          <?php echo $info['list_sn']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>买家ID</label>
        </dt>
        <dd class="opt">
          <?php echo $info['buyer_id']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>买家名字</label>
        </dt>
        <dd class="opt">
          <?php echo $info['buyer_name']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>总金额</label>
        </dt>
        <dd class="opt">
          <?php echo $info['all_money']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>支付次数</label>
        </dt>
        <dd class="opt">
          <?php echo $info['pay_num']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>最后支付时间</label>
        </dt>
        <dd class="opt">
          <?php echo date('Y-m-d H:i:s',$info['last_pay_time']); ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auctions_class_name"><em></em>保证金订单列表</label>
        </dt>
        <dd class="opt">
          <table>
            <tr>
              <td>订单编号</td>
              <td>买家姓名</td>
              <td>创建时间</td>
              <td>保证金金额</td>
              <td>支付状态</td>
              <td>退款状态</td>
              <td>锁定状态</td>
            </tr>
            <?php if(!empty($output['list'])){ ?>
              <?php foreach($output['list'] as $k => $v){ ?>
                <tr>
                  <td><?php echo $v['order_sn']; ?></td>
                  <td><?php echo $v['buyer_name']; ?></td>
                  <td><?php echo date('Y-m-d H:i:s',$v['创建时间']); ?></td>
                  <td><?php echo $v['margin_amount']; ?></td>
                  <td><?php echo $v['order_state']; ?></td>
                  <td><?php echo $v['refund_state']; ?></td>
                  <td><?php echo $v['lock_state']; ?></td>
                </tr>
              <?php } ?>
            <?php } ?>
          </table>
        </dd>
      </dl>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#add_form").valid()){
     $("#add_form").submit();
  }
  });
});
</script>