<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
    .d_inline {
        display: inline;
    }
</style>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=store_vendue&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>商家拍卖管理 - 编辑商家“<?php echo $output['vendue_info']['store_name'];?>”的拍卖信息</h3>
            </div>
        </div>
    </div>
    <div class="homepage-focus" nctype="editStoreContent">
        <div class="title">
            <h3>编辑商家拍卖信息</h3>
        </div>

        <form id="edit_form" enctype="multipart/form-data" method="post" action="">
            <input type="hidden" name="form_submit" value="ok" />
            <input type="hidden" name="store_vendue_id" value="<?php echo $output['vendue_info']['store_vendue_id'];?>" />
            <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
                <thead>
                <tr>
                    <th colspan="20">商家拍卖基本信息</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>店铺名称：</th>
                    <td><?php echo $output['vendue_info']['store_name'];?></td>
                </tr>
                <tr>
                    <th>所在地区：</th>
                    <td><?php echo $output['vendue_info']['area_name'];?> </td>
                </tr>
                <tr>
                    <th>机构类型：</th>
                    <td colspan="20"><?php echo $output['vendue_info']['oz_type'];?></td>
                </tr>
                <tr>
                    <th>主营：</th>
                    <td colspan="20"><?php echo $output['vendue_info']['store_zy'];?></td>
                </tr>
                <tr>
                    <th>拍卖logo：</th>
                    <td><a nctype="nyroModal"  href="<?php echo getStoreLogo($output['vendue_info']['store_avatar']);?>"> <img src="<?php echo getStoreLogo($output['vendue_info']['store_avatar']);?>" alt="" /> </a></td>
                </tr>
                <tr>
                    <th>入驻时间：</th>
                    <td><?php echo date('Y-m-d',$output['vendue_info']['store_time']);?></td>
                </tr>
                <tr>
                    <th>简介：</th>
                    <td><?php echo $output['vendue_info']['store_vendue_intro'];?></td>
                </tr>
                <tr>
                    <th>拍卖保障金：</th>
                    <td>
                        <?php echo ncPriceFormat($output['store_info']['vendue_bail'])?>
                    </td>
                </tr>
                <tr>
                    <th>上传付款凭证：</th>
                    <td><img src="<?php echo getVendueLogo($output['vendue_info']['paying_money_certificate'])?>" />
                        <span></span></td>
                </tr>
                <tr>
                    <th>备注：</th>
                    <td><?php echo $output['vendue_info']['paying_money_certif_exp']?>
                        <span></span></td>
                </tr>
                <tr>
                    <th>是否支付保证金：</th>
                    <td><input type="radio" name="is_pay" value="1" checked="checked">是&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="is_pay" value="0">否</td>
                </tr>


                </tbody>
            </table>

            <div><a id="submitBtn" class="ncap-btn-big ncap-btn-green" href="JavaScript:void(0);"><?php echo $lang['nc_submit'];?></a></div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    $(function(){
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
            $("#edit_form").submit();
        });
    });
</script>
