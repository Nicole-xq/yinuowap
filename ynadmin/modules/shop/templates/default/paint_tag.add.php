<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=painting_tag&op=index" title="返回标签列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>书画标签管理 - 新增</h3>
            </div>
        </div>
    </div>
    <form id="tag_form" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>标签名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="" name="paint_tag_name" id="paint_tag_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">所属分类</dt>
                <dd class="opt">
                    <div id="gcategory">
                        <input type="hidden" value="" name="class_id" class="mls_id">
                        <input type="hidden" value="" name="paint_tag_class" class="mls_name">
                        <select class="class-select">
                            <option value="0"><?php echo $lang['nc_please_choose'];?></option>
                            <?php if(!empty($output['gc_list'])){ ?>
                                <?php foreach($output['gc_list'] as $k => $v){ ?>
                                    <?php if ($v['gc_parent_id'] == 0) {?>
                                        <option value="<?php echo $v['gc_id'];?>"><?php echo $v['gc_name'];?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <span class="err"></span>
                    <p class="notic">选择分类，可关联大分类或更具体的下级分类。</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">排序</dt>
                <dd class="opt">
                    <input type="text" value="0" name="paint_tag_sort" id="paint_tag_sort" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">数字范围为0~255，数字越小越靠前</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script>

    $(function(){
        $("#submitBtn").click(function(){
            if($("#tag_form").valid()){
                $("#tag_form").submit();
            }
        });

        $("#tag_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                paint_tag_name : {
                    required : true,
                    remote   : {
                        url :'index.php?act=painting_tag&op=ajax&branch=check_tag_name',
                        type:'get',
                        data:{
                            paint_tag_name : function(){
                                return $('#paint_tag_name').val();
                            },
                            id  : ''
                        }
                    }
                },

                paint_tag_sort : {
                    required : true,
                    number   : true
                }
            },
            messages : {
                paint_tag_name : {
                    required : '<i class="fa fa-exclamation-circle"></i>标签不能为空',
                    remote   : '<i class="fa fa-exclamation-circle"></i>标签已经存在'
                },

                paint_tag_sort  : {
                    required : '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    number   : '<i class="fa fa-exclamation-circle"></i>排序仅可以为数字'
                }
            }
        });
    });

    gcategoryInit('gcategory');
</script>
