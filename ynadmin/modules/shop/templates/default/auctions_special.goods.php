<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .ui-datepicker {
        width: 20em;
        padding: .2em .2em 0;
        display: none;
    }
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="index.php?act=auctions_special&op=index" title="返回平台专场列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>平台专场管理 - 新增拍品</a></h3>
            </div>
        </div>
    </div>
    <div class="ncap-form-default">
        <dl class="row">
                <dt class="tit">
                    <label for="activity_title"><em>*</em>拍品信息</label>
                </dt>
                <dd class="opt">
                    <table id="goods_list" border="6">
                        <tr>
                            <td>拍品名称</td>
                            <td>起拍价</td>
                            <td>加价幅度</td>
                            <td>添加时间</td>
                            <td>库位号</td>
                            <?php $admin = false; if ($admin_info['gid'] == 1) {
                                $admin = true; ?>
                            <td>保留价</td>
                            <?php }?>
                            <td>拍品保证金</td>
                            <td>拍品类型</td>
                            <td>操作</td>
                        </tr>
                        <?php if(!empty($output['list'])){ ?>
                            <?php foreach($output['list'] as $k => $v){ ?>
                                <tr id="tr_<?php echo $v['auction_id'];?>" style="cursor: pointer">
                                    <td><?php echo $v['auction_name'];?></td>
                                    <td><?php echo $v['auction_start_price'];?></td>
                                    <td><?php echo $v['auction_increase_range'];?></td>
                                    <td><?php echo date('Y-m-d H:i:s', $v['auction_add_time']);?></td>
                                    <td><?php echo $v['warehouse_location'];?></td>
                                    <?php if ($admin) { ?>
                                    <td><?php echo $v['auction_reserve_price'];?></td>
                                    <?php }?>
                                    <td><?php echo $v['auction_bond'];?></td>
                                    <td><?php if ($v['auction_type'] == 0) {
                                            echo '普通拍品';
                                        } elseif ($v['auction_type'] == 1) {
                                            echo '新手拍品';
                                        } else {
                                            echo '新手专区';
                                        } ?></td>
                                    <td onclick="remove_goods(<?php echo $v['auction_id'];?>)">移除</td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        
                    </table>
                </dd>
            </dl>
            <dl class="row">
            <dt class="tit">
                <label><em></em>新增拍品</label>
            </dt>
            <dd class="opt">
                <input type="text"  id="keyword" class="input-txt" placeholder="搜索" > 
                &nbsp;&nbsp;&nbsp;
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="search"><span>搜索</a>
            </dd>
        </dl>
        <div class="bot" id="artist">
          
        </div>
    </div>
</div>
<script>
 $(function(){   
$("#search").click(
    function(){
      var keyword = $("#keyword").val();
      if (keyword) {
        htmlobj=$.ajax({url:'index.php?act=auctions_special&op=get_add_goods&keyword='+keyword,async:false});
        $("#artist").html(htmlobj.responseText);
      }else{
        alert('搜索不能为空');
      }
    }
);
});
function bind_goods(id){
    var special_id=<?php echo $special_id; ?>;
    var keyword = $("#keyword").val();
    check_id=$.ajax({url:'index.php?act=auctions_special&op=add_good_to_special&id='+id+'&special_id='+special_id,async:false});
    if (check_id.responseText!=0) {
        $("#goods_list").append(check_id.responseText);
        htmlobj=$.ajax({url:'index.php?act=auctions_special&op=get_add_goods&keyword='+keyword,async:false});
        $("#artist").html(htmlobj.responseText);
    }else{
        alert('添加失败!');
    }

}

 function remove_goods(id){
     var special_id=<?php echo $special_id; ?>;
     var keyword = $("#keyword").val();
     check_id=$.ajax({url:'index.php?act=auctions_special&op=remove_good_to_special&id='+id+'&special_id='+special_id,async:false});
     console.log(check_id.responseText);
     if (check_id.responseText == 'success') {
         $("#tr_"+id).remove();
     } else if (check_id.responseText == 1) {
        alert('该拍品已经有人缴纳保证金不能移除!');
     } else {
         alert('移除失败!');
     }
 }
</script>