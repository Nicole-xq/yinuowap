<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>书画专场推荐</h3>
                <h5>书画页面推荐专场管理</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>

<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=special_rec&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
                {display: '店铺名称', name : 'store_name', width : 150, sortable : false, align: 'left'},
                {display: '专场名称', name : 'special_name', width : 120, sortable : true, align: 'left'},
                {display: '专场缩略图', name : 'special_image', width: 60, sortable : false, align : 'center'},
                {display: '专场开始时间', name : 'special_start_time', width : 150, sortable : true, align: 'center'},
                {display: '专场结束时间', name : 'special_end_time', width : 150, sortable : true, align: 'center'},
                {display: '预展开始时间', name : 'special_preview_start', width : 150, sortable : true, align: 'center'},
                {display: '专场申请时间', name : 'special_add_time', width : 150, sortable : true, align: 'center'}
            ],
            searchitems : [
                {display: '店铺名称', name : 'store_name', isdefault: true},
                {display: '专场名称', name : 'special_name'}
            ],
            sortname: "special_id",
            sortorder: "asc",
            title: '专场列表'
        });

    });

</script>