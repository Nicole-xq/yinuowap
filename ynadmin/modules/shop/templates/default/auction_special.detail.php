<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .name {
        width: 10% !important;
        display:block;float:left;
    }

    .number1 {
        width: 90% !important;
        display:block;float:left;
    }

    .fr {
        float: right !important;
        display: inline;
        width:45%;
    }
    .fr span {
        text-align: right;
        font-size: 14px;
        margin-right: 20px;
        display:block;float:right;
    }
    .fr p {
        margin: 0 0 4px;
        line-height: 21px;
        height:84px;
    }
    .fr h2 {
        font-size: 24px;
        text-align: center;
        color: #000;
        margin: 0 0 12px;
        margin-top:10px;
    }
    .works-list {
        width: 100%;
        border: #f3f3f3 solid 1px;
        float: left;
        margin: 0 0 1px 1px;
    }
    .store-joinin tbody td img{
        max-width: 180px !important;
        max-height: 168px !important;
    }


</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=auction_special&op=check_ok" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>专场管理 - 查看商家“<?php echo $output['special_info']['store_name'];?>”的专场信息</h3>
            </div>
        </div>
    </div>

    <form id="form_store_verify" enctype="multipart/form-data" action="index.php?act=auction_special&op=special_verify" method="post">
        <input id="verify_type" name="verify_type" type="hidden" />
        <input name="special_id" type="hidden" value="<?php echo $output['special_info']['special_id'];?>" />
        <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
            <thead>
            <tr>
                <th colspan="20">专场基本信息</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th class="w150">店铺名称：</th>
                <td><?php echo $output['special_info']['store_name'];?></td>
            </tr>
            <tr>
                <th>专场名称：</th>
                <td>
                    <input name="special_name" id="special_name" class="text w150 hasDatepicker" type="text" value="<?php echo $output['special_info']['special_name'];?>">
                </td>
            </tr>
            <tr>
                <th>拍卖类型：</th>
                <td style="color: red">
                    <?php if (in_array(intval($output['special_info']['special_state']), array(10,30))) { ?>
                        <input name="auction_type" id="auction_type" type="radio" value="0" <?php if(empty($output['special_info']['auction_type'])){echo "checked='checked'";}?>>普通拍品 &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="auction_type" id="auction_type" type="radio" value="1" <?php if(!empty($output['special_info']['auction_type'])){echo "checked='checked'";}?>>新手拍品
                    <?php } else { ?>
                        <?php if(empty($output['special_info']['special_type'])) {echo "普通拍品";} else {echo "新手拍品";}?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <th>专场缩略图：</th>
                <td>
                    <div class="input-file-show">
                        <span class="show">
                            <a class="nyroModal" rel="gal" href="<?php echo getVendueLogo($output['special_info']['special_image'])?>">
                                <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo getVendueLogo($output['special_info']['special_image']);?>>')" onMouseOut="toolTip()"/>
                                </i> </a></span><span class="type-file-box">
                            <input type="text" name="textfield" id="textfield1" class="type-file-text" />
                            <input type="button" name="button" id="button1" value="选择更换..." class="type-file-button" />
                            <input class="type-file-file" id="special_logo" name="special_logo" type="file" size="30" hidefocus="true" nc_type="change_special_logo" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                        </span>
                    </div>
                    <span class="err"></span>
                    <p class="notic">建议上传图片大小的尺寸为520px*220px</p>
                </td>
            </tr>
            <tr>
                <th>专场广告图：</th>
                <td>
                    <div class="input-file-show">
                        <span class="show">
                            <a class="nyroModal" rel="gal" href="<?php echo getVendueLogo($output['special_info']['adv_image'])?>">
                                <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo getVendueLogo($output['special_info']['adv_image']);?>>')" onMouseOut="toolTip()"/>
                                </i> </a></span><span class="type-file-box">
                            <input type="text" name="textfield2" id="textfield2" class="type-file-text" />
                            <input type="button" name="button2" id="button2" value="选择更换..." class="type-file-button" />
                            <input class="type-file-file" id="adv_image" name="adv_image" type="file" size="30" hidefocus="true" nc_type="change_special_logo" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                        </span>
                    </div>
                    <span class="err"></span>
                    <p class="notic">建议上传图片大小的尺寸为1920px*300px</p>
                </td>
            </tr>
            <tr>
                <th>wap广告图：</th>
                <td>
                    <div class="input-file-show">
                        <span class="show">
                            <a class="nyroModal" rel="gal" href="<?php echo getVendueLogo($output['special_info']['wap_image'])?>">
                                <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo getVendueLogo($output['special_info']['wap_image']);?>>')" onMouseOut="toolTip()"/>
                                </i> </a></span><span class="type-file-box">
                            <input type="text" name="textfield3" id="textfield3" class="type-file-text" />
                            <input type="button" name="button3" id="button3" value="选择更换..." class="type-file-button" />
                            <input class="type-file-file" id="wap_image" name="wap_image" type="file" size="30" hidefocus="true" nc_type="change_special_logo" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                        </span>
                    </div>
                    <span class="err"></span>
                    <p class="notic">建议上传图片大小的尺寸为1920px*300px</p>
                </td>
            </tr>
            <tr>
                <th>专场开始时间：</th>
                <td colspan="20">
                    <?php if (in_array(intval($output['special_info']['special_state']), array(10,30))) { ?>
                        <div style="float: left">
                            <input name="special_start_time" id="special_start_time" class="text w150" type="text" value="<?php echo date('Y-m-d H:i:s',$output['special_info']['special_start_time']);?>">
                        </div>
                        <div class="bottom" id="automatic_times"> <a href="javascript:void(0)" class="ncap-btn ncap-btn-green ">自动设定</a></div>
                    <?php } else { ?>
                        <?php echo date('Y-m-d H:i:s',$output['special_info']['special_start_time']);?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <th>专场结束时间：</th>
                <td colspan="20">
                    <?php if (in_array(intval($output['special_info']['special_state']), array(10,30))) { ?>
                        <p><input name="special_end_time" id="special_end_time" class="text w150" type="text" value="<?php echo date('Y-m-d H:i:s',$output['special_info']['special_end_time']);?>"></p>
                        <p><span id="special_end_time_remind"></span></p>
                    <?php } else { ?>
                        <?php echo date('Y-m-d H:i:s',$output['special_info']['special_end_time']);?>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <th>预展开始时间：</th>
                <td colspan="20">
                <?php if (in_array(intval($output['special_info']['special_state']), array(10,30))) { ?>
                   <input name="special_preview_start" id="special_preview_start" value="<?= $output['special_info']['special_preview_start']?date('Y-m-d H:i:s',$output['special_info']['special_preview_start']):'';?>" type="text" class="text w150" />
                <?php } else { ?>
                    <?= $output['special_info']['special_preview_start']?date('Y-m-d H:i:s',$output['special_info']['special_preview_start']):'';?>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <th>最后计息时间：</th>
                <td colspan="20">
                <?php if (in_array(intval($output['special_info']['special_state']), array(10,30))) { ?>
                    <div style="float: left">
                        <p><input name="auction_rate_time" id="auction_rate_time" value="<?= $output['special_info']['special_rate_time']?date('Y-m-d H:i:s',$output['special_info']['special_rate_time']):'';?>" type="text" class="text w150" /></p>
                        <p><input type="text" id="auction_rate_day" style="width: 30px">天</p>
                        <p><span id="auction_rate_day_remind"></span></p>
                    </div>
                    <div class="bottom" id="automatic_times_1"><a href="javascript:void(0)" class="ncap-btn ncap-btn-green ">自动设定</a></div>
                <?php } else { ?>
                    <?= $output['special_info']['special_rate_time']?date('Y-m-d H:i:s',$output['special_info']['special_rate_time']):'';?>
                <?php } ?>
                </td>
            </tr>
            <tr>
              <th>利率</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="auction_bond_rate" name="auction_bond_rate" value="<?=$output['special_info']['special_rate'];?>" type="text" class="text w30" />%(利率为0时,上级利率加成不生效)</td>
                <?php } else { ?>
                    <td><?=$output['special_info']['special_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上一级利率(举人)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_1_wdl" name="partner_1_wdl" max_rate="<?=$output['rate_arr']['partner_1_wdl']['max_rate'];?>" value="<?=$output['rate_arr']['partner_1_wdl']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_1_wdl']['max_rate'];?>)
                    <p><span id="partner_1_wdl_err" style="color:red;"></span></p></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_1_wdl_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上一级利率(贡士)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_1_zl" name="partner_1_zl" max_rate="<?=$output['rate_arr']['partner_1_zl']['max_rate'];?>" value="<?=$output['rate_arr']['partner_1_zl']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_1_zl']['max_rate'];?>)
                    <p><span id="partner_1_zl_err" style="color:red;"></span></p></td></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_1_zl_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上一级利率(进士)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_1_sy" name="partner_1_sy" max_rate="<?=$output['rate_arr']['partner_1_sy']['max_rate'];?>" value="<?=$output['rate_arr']['partner_1_sy']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_1_sy']['max_rate'];?>)
                    <p><span id="partner_1_sy_err" style="color:red;"></span></p></td></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_1_sy_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上两级利率(举人)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_2_wdl" name="partner_2_wdl" max_rate="<?=$output['rate_arr']['partner_2_wdl']['max_rate'];?>" value="<?=$output['rate_arr']['partner_2_wdl']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_2_wdl']['max_rate'];?>)
                    <p><span id="partner_2_wdl_err" style="color:red;"></span></p></td></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_2_wdl_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上两级利率(贡士)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_2_zl" name="partner_2_zl" max_rate="<?=$output['rate_arr']['partner_2_zl']['max_rate'];?>" value="<?=$output['rate_arr']['partner_2_zl']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_2_zl']['max_rate'];?>)
                    <p><span id="partner_2_zl_err" style="color:red;"></span></p></td></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_2_zl_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>上两级利率(进士)</th>
                <?php if (in_array(intval($output['special_info']['special_state']), array(10, 30))) { ?>
                    <td><input id="partner_2_sy" name="partner_2_sy" max_rate="<?=$output['rate_arr']['partner_2_sy']['max_rate'];?>" value="<?=$output['rate_arr']['partner_2_sy']['rate'];?>" type="text" class="text w30" />%(峰值<?=$output['rate_arr']['partner_2_sy']['max_rate'];?>)
                    <p><span id="partner_2_sy_err" style="color:red;"></span></p></td></td>
                <?php } else { ?>
                    <td><?=$output['special_info']['partner_lv_2_sy_rate'];?>%</td>
                <?php } ?>
            </tr>
            <tr>
              <th>专场申请时间：</th>
              <td colspan="20"><?php echo date('Y-m-d H:i:s',$output['special_info']['special_add_time']);?></td>
            </tr>


            <?php if(in_array(intval($output['special_info']['special_state']), array(10))) { ?>
                <tr>
                    <th>审核意见：</th>
                    <td colspan="2"><textarea id="check_message" name="check_message"></textarea></td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
        <div id="validation_message" style="color:red;display:none;"></div>
        <div class="bottom">
        <?php if(in_array(intval($output['special_info']['special_state']), array(10))) { ?>
            <a id="btn_pass" class="ncap-btn-big ncap-btn-green mr10" href="JavaScript:void(0);">通过</a>
            <a id="btn_fail" class="ncap-btn-big ncap-btn-red" href="JavaScript:void(0);">拒绝</a>
        <?php } else { ?>
            <a id="btn_save" class="ncap-btn-big ncap-btn-green mr10" href="JavaScript:void(0);">保存修改</a>
        <?php } ?>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#auction_rate_time').datetimepicker({controlType: 'select'});
        $('#special_start_time').datetimepicker({controlType: 'select'});
        $('#special_end_time').datetimepicker({controlType: 'select'});
        $('#special_preview_start').datetimepicker({controlType: 'select'});
        $('a[nctype="btn_submit"]').click(function(){
            if($("#form1").valid()) ajaxpost('form1', '', '', 'onerror');
        });
        $('a[nctype="nyroModal"]').nyroModal();

        $('#btn_fail').on('click', function() {
            if($('#check_message').val() == '') {
                $('#validation_message').text('请输入审核意见');
                $('#validation_message').show();
                return false;
            } else {
                $('#validation_message').hide();
            }
            if(confirm('确认拒绝申请？')) {
                $('#verify_type').val('fail');
                $('#form_store_verify').submit();
            }
        });

        $('#btn_pass').on('click', function() {
            var err = false;
            if($('#special_preview_start').val()) {
                $('#validation_message').hide();
            } else {
                $('#validation_message').text('请输入预展开始时间').show();
                return false;
            }
//            alert($('#partner_1_wdl').attr('max_rate'));
//            return false;
            if($('#auction_bond_rate').val() > 0){
                if(parseInt($('#partner_1_wdl').val()) > parseInt($('#partner_1_wdl').attr('max_rate'))){
                    $('#partner_1_wdl_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_1_wdl_err').html('');
                }
                if(parseInt($('#partner_1_zl').val()) > parseInt($('#partner_1_zl').attr('max_rate'))){
                    $('#partner_1_zl_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_1_zl_err').html('');
                }
                if(parseInt($('#partner_1_sy').val()) > parseInt($('#partner_1_sy').attr('max_rate'))){
                    $('#partner_1_sy_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_1_sy_err').html('');
                }
                if(parseInt($('#partner_2_wdl').val()) > parseInt($('#partner_2_wdl').attr('max_rate'))){
                    $('#partner_2_wdl_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_2_wdl_err').html('');
                }
                if(parseInt($('#partner_2_zl').val()) > parseInt($('#partner_2_zl').attr('max_rate'))){
                    $('#partner_2_zl_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_2_zl_err').html('');
                }
                if(parseInt($('#partner_2_sy').val()) > parseInt($('#partner_2_sy').attr('max_rate'))){
                    $('#partner_2_sy_err').html('超出峰值');
                    err = true;
                }else{
                    $('#partner_2_sy_err').html('');
                }
                if(err == true){
                    return false;
                }
            }
            if(confirm('确认通过申请？')) {
                $('#verify_type').val('pass');
                $('#form_store_verify').submit();
            }
        });

        $('#btn_save').on('click', function() {
            $('#validation_message').hide();
            if(confirm('确认保存信息？')) {
                $('#verify_type').val('save');
                $('#form_store_verify').submit();
            }
        });
    });
</script>
<script type="text/javascript">
    // 模拟网站LOGO上传input type='file'样式
    $(function(){
        $("#special_logo").change(function(){
            $("#textfield1").val($(this).val());
        });
        $("#adv_image").change(function(){
            $("#textfield2").val($(this).val());
        });
        $("#wap_image").change(function(){
            $("#textfield3").val($(this).val());
        });
        // 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("图片限于png,gif,jpeg,jpg格式");
                $(this).attr('value','');
                return false;
            }
        });
        // 点击查看图片
        $('.nyroModal').nyroModal();
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#automatic_times').click(function () {
            //拍卖开始时间戳
            var times = Date.parse(new Date($('#special_start_time').val()));
            var d = times + 86400 * 2 *1000;
            v_times(d);
            var special_end_time = formatDate(new Date(d));
            d = times - 86400 * 3 *1000;
            var special_preview_start = formatDate(new Date(d));
            $('#special_end_time').val(special_end_time);
            $('#special_preview_start').val(special_preview_start);
            $('#auction_rate_time').val('');
            $('#auction_rate_day').val('');
            $('#auction_bond_rate').val('0.00');
            $('#auction_rate_day_remind').html('');
        });

        $('#automatic_times_1').click(function () {
            if ($('#auction_rate_day').val() == '' || $('#auction_rate_day').val() < 1) {
                $('#auction_rate_day_remind').css("color","red").html('请输入天数');
                return false;
            }
            //计息天数
            var auction_rate_day = parseInt($('#auction_rate_day').val());
//            auction_rate_day +=2 ;
            //认筹结束时间戳
            var times = Date.parse(new Date($('#auction_rate_time').val()));
            //结束时间
            var d = times + 86400 * 1000 * auction_rate_day;
            var special_end_time = formatDate(new Date(d));
            v_times(d);
            //起拍时间
            d -= 86400 * 2 *1000;
            var special_start_time = formatDate(new Date(d));
            //预展时间
            d = times - 86400 * 7 *1000;
            var special_preview_start = formatDate(new Date(d));
            //展示
            $('#special_start_time').val(special_start_time);
            $('#special_end_time').val(special_end_time);
            $('#special_preview_start').val(special_preview_start);
        });

        $('#auction_rate_day').change(function () {
            if($(this).val() != ''){
                $('#auction_rate_day_remind').html('');
            }
        });
        $('#special_end_time').change(function () {
            var times = Date.parse(new Date($(this).val()));
            v_times(times);
        })
    });
    function formatDate(now) {
        var year=now.getFullYear();
        var month=now.getMonth()+1;
        var date=now.getDate();
        var hour=now.getHours();
        var minute=now.getMinutes();
        var second=now.getSeconds();
        return year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second;
    }
    function v_times(times=0) {
        var timestamp = Date.parse(new Date());
        if(times < timestamp){
            $('#special_end_time_remind').css("color","red").html('提示：结束时间未超过当前时间');
        }else{
            $('#special_end_time_remind').html('');
        }
    }
</script>