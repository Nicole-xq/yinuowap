<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
    .ncap-stat-general-single dl span{
        color: #2cbca3;
        font-size: 20px;
        font-weight: normal;
    }
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>资金统计</h3>
                <h5>平台针对销售量的各项数据统计</h5>
            </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>第三方总金额：第三方支付充值 + 保证金充值 + 第三方支付产品</li>
            <li>提现成功总金额：平台所有客户提现成功的总金额</li>
            <li>奖励金总金额：商品订单返佣 + 保证金返佣 + 保证金利息 + 合伙人升级 + 艺术家入驻佣金</li>
            <li>商品购买总金额：平台所有客户的购买商品的总金额</li>
            <li>现存预存款总金额：平台所有客户的预存款总金额</li>
            <li>现存保证金总金额：平台所有客户的保证金总金额</li>
        </ul>
    </div>
    <div class="ncap-form-default ncap-stat-general-single"></div>

    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" action="index.php" name="formSearch" id="formSearch">
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dt>日期筛选</dt>
                        <dd>
                            <label>
                                <input readonly id="query_start_date" placeholder="请选择起始时间" name="query_start_date" value="<?php echo $output['query']['start_date'];?>" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="<?php echo $output['query']['start_date'];?>" type="text" class="s-input-txt" />
                            </label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom">
                <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/statistics.js"></script>
<script>
    function update_flex(){
        $('.ncap-stat-general-single').load('index.php?act=stat_capital&op=index&get_stat_data=1&'+$("#formSearch").serialize());
    }
    $(function () {
        //绑定时间控件
        $('#query_start_date').datepicker();
        $('#query_end_date').datepicker();

        update_flex();
        $('#ncsubmit').click(function(){
            update_flex();
        });

        $('#searchBarOpen').click();
    });

</script>