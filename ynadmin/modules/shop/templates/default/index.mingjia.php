<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['nc_web_index'];?></h3>
        <h5><?php echo $lang['nc_web_index_subhead'];?></h5>
      </div>
       <ul class="tab-base nc-row">
        <li><a href="index.php?act=web_config&op=web_config"><?php echo '板块区';?></a></li>
        <li><a href="index.php?act=web_config&op=focus_edit"><?php echo '焦点区';?></a></li>
        <li><a href="index.php?act=web_config&op=sale_edit"><?php echo '促销区';?></a></li>
        <li><a href="JavaScript:void(0);" class="current"><?php echo '名师新作';?></a></li>
        <li><a href="index.php?act=web_config&op=recommend"><?php echo '推荐活动';?></a></li>
      </ul>
    </div>
  </div>

  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
  </div>
  <form id="live_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label>名师新作1(图片/链接)</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous1_img'];?>"/><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous1_img'];?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="famous1_img" type="file" class="type-file-file" id="famous1_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield1' id='textfield1' class='type-file-text' />
            <input type='button' name='button1' id='button1' value='选择上传...' class='type-file-button' />
            </span></div>
          <label title="请输入图片要跳转的链接地址" class="ml5"><i class="fa fa-link"></i>
            <input class="input-txt ml5" type="text" name="famous1_link"  value="<?php echo $output['list_setting']['famous1_link']?>" placeholder="请输入图片要跳转的链接地址" required/>
          </label>
          <p><br/></p> 
        </dd>
        <dt class="tit"><label>名师新作1(藏品作者)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_author" value="<?php echo $output['list_setting']['famous1_author'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作1(标题)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_title" value="<?php echo $output['list_setting']['famous1_title'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作1(材料工艺)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_name" value="<?php echo $output['list_setting']['famous1_name'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作1(尺寸规格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_spec" value="<?php echo $output['list_setting']['famous1_spec'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作1(题材)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_theme" value="<?php echo $output['list_setting']['famous1_theme'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作1(风格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous1_manner" value="<?php echo $output['list_setting']['famous1_manner'];?>" required /><p><br/></p></dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>名师新作2(图片/链接)</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous2_img'];?>"/><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous2_img'];?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="famous2_img" type="file" class="type-file-file" id="famous2_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield2' id='textfield2' class='type-file-text' />
            <input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />
            </span></div>
          <label title="请输入图片要跳转的链接地址" class="ml5"><i class="fa fa-link"></i>
            <input class="input-txt ml5" type="text" name="famous2_link"  value="<?php echo $output['list_setting']['famous2_link']?>" placeholder="请输入图片要跳转的链接地址" required/>
          </label>
          <p><br/></p> 
        </dd>
        <dt class="tit"><label>名师新作2(藏品作者)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_author" value="<?php echo $output['list_setting']['famous2_author'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作2(标题)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_title" value="<?php echo $output['list_setting']['famous2_title'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作2(材料工艺)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_name" value="<?php echo $output['list_setting']['famous2_name'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作2(尺寸规格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_spec" value="<?php echo $output['list_setting']['famous2_spec'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作2(题材)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_theme" value="<?php echo $output['list_setting']['famous2_theme'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作2(风格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous2_manner" value="<?php echo $output['list_setting']['famous2_manner'];?>" required /><p><br/></p></dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>名师新作3(图片/链接)</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous3_img'];?>"/><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['famous3_img'];?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="famous3_img" type="file" class="type-file-file" id="famous3_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield3' id='textfield3' class='type-file-text' />
            <input type='button' name='button3' id='button3' value='选择上传...' class='type-file-button' />
            </span></div>
          <label title="请输入图片要跳转的链接地址" class="ml5"><i class="fa fa-link"></i>
            <input class="input-txt ml5" type="text" name="famous3_link"  value="<?php echo $output['list_setting']['famous3_link']?>" placeholder="请输入图片要跳转的链接地址" required/>
          </label>
           <p><br/></p> 
        </dd>
        <dt class="tit"><label>名师新作3(名称)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_author" value="<?php echo $output['list_setting']['famous3_author'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作3(标题)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_title" value="<?php echo $output['list_setting']['famous3_title'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作3(材料工艺)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_name" value="<?php echo $output['list_setting']['famous3_name'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作3(尺寸规格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_spec" value="<?php echo $output['list_setting']['famous3_spec'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作3(题材)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_theme" value="<?php echo $output['list_setting']['famous3_theme'];?>" required /><p><br/></p></dd>
        <dt class="tit"><label>名师新作3(风格)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="famous3_manner" value="<?php echo $output['list_setting']['famous3_manner'];?>" required /><p><br/></p></dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a> <!-- <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-red ml10" id="clearBtn">清空数据</a> --></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
//按钮先执行验证再提交表单
$(function(){
    // 图片js
    $("#famous1_img").change(function(){$("#textfield1").val($("#famous1_img").val());});
    $("#famous2_img").change(function(){$("#textfield2").val($("#famous2_img").val());});
    $("#famous3_img").change(function(){$("#textfield3").val($("#famous3_img").val());});
	$('.nyroModal').nyroModal();
    $('#live_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parents('dd').children('span.err');
            error_td.append(error);
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            famous1_link: {
                url : true
            },
            famous2_link:{
                url : true
            },
            famous3_link:{
                url : true
            },
        },
        messages : {
            famous1_link: {
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
            famous2_link:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
            famous3_link:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
        }
    });

    $("#submitBtn").click(function(){
        $("#live_form").submit();
    });
});
</script>