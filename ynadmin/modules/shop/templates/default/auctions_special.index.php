<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>平台专场管理</h3>
                <h5>平台商城管理与店铺申请商品审核</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        var flexUrl = 'index.php?act=auctions_special&op=get_xml';

        $("#flexigrid").flexigrid({
            url: flexUrl,
            colModel: [
                {display: '操作', name : 'operation', width : 165, sortable : false, align: 'center', className: 'handle'},
                {display: '专场ID', name : 'special_id', width : 120, sortable : false, align: 'left'},
                {display: '专场名称', name : 'special_name', width : 120, sortable : true, align: 'left'},
                {display: '专场缩略图', name : 'special_image', width: 60, sortable : false, align : 'center'},
                {display: '拍品类型', name : 'special_type_text', width: 60, sortable : false, align : 'center'},
                {display: '专场开始时间', name : 'special_start_time', width : 150, sortable : true, align: 'center'},
                {display: '专场结束时间', name : 'special_end_time', width : 150, sortable : true, align: 'center'},
                {display: '预展开始时间', name : 'special_preview_start', width : 150, sortable : true, align: 'center'},
                {display: '专场申请时间', name : 'special_add_time', width : 150, sortable : true, align: 'center'},
                {display: '状态', name: 'is_open', width: 80, sortable: false, align: 'center'}
            ],
            searchitems: [
                {display: '专场名称', name : 'special_name', isdefault: true}
            ],
            sortname: "special_id",
            sortorder: "desc",
            title: '平台专场列表'
        });


        $("input[data-dp='1']").datepicker({dateFormat: 'yy-mm-dd'});

    });
    $('a[data-href]').live('click', function() {
        if ($(this).hasClass('confirm-del-on-click') && !confirm('确定删除?')) {
            return false;
        }

        $.getJSON($(this).attr('data-href'), function(d) {
            if (d && d.result) {
                $("#flexigrid").flexReload();
            } else {
                alert(d && d.message || '操作失败！');
            }
        });
    });

    $.getJSON($(this).attr('data-href'), function(d) {
        if (d && d.result) {
            $("#flexigrid").flexReload();
        } else {
            alert(d && d.message || '操作失败！');
        }
    });


</script>
