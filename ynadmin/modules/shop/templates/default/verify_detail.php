<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_SITE_URL;?>/templates/default/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo SHOP_SITE_URL;?>/templates/default/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="<?php echo SHOP_SITE_URL;?>/resource/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="<?php echo SHOP_SITE_URL;?>/resource/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<style>
.pic_list .small_pic ul li {
	height: 100px;
}
.ui-sortable-helper {
	border: dashed 1px #F93;
	box-shadow: 2px 2px 2px rgba(153,153,153, 0.25);
	filter: alpha(opacity=75);
	-moz-opacity: 0.75;
	opacity: .75;
	cursor: ns-resize;
}
.ui-sortable-helper td {
	background-color: #FFC !important;
}
.ajaxload {
	display: block;
	width: 16px;
	height: 16px;
	margin: 100px 300px;
}
.ncsc-goods-default-pic .goodspic-uplaod .upload-thumb {
    width: 520px;
    height: 220px;
}
.ncsc-goods-default-pic .goodspic-uplaod .upload-thumb img {
    max-width: 520px;
    max-height: 220px;
}
</style>
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=promotion_guess&op=guess_verify_list" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>趣猜审核</h3>
        <h5>趣猜活动查看及审核</h5>
      </div>
    </div>
  </div>
<input id="level2_flag" type="hidden" value="false" />
<input id="level3_flag" type="hidden" value="false" />
<div class="wrap">
  <div class="ncsc-form-default"> 
    <!-- 说明 -->
      <dl>
        <dt><?php echo '趣猜名称'.$lang['nc_colon'];?></dt>
        <dd>
          <p><?php echo $output['guess_info']['gs_name'];?></p>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '中奖折扣价'.$lang['nc_colon'];?></dt>
        <dd>
          <p><?php echo $output['guess_info']['gs_discount_price'];?></p>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '商品总计'.$lang['nc_colon'];?></dt>
        <dd>
          <p><?php echo $output['guess_info']['gs_cost_price'];?>（已选商品总计）</p>
        </dd>
      </dl>
      <dl>
        <dt><?php echo '趣猜商品'.$lang['nc_colon'];?></dt>
        <dd>
          <table class="ncsc-default-table mb15">
            <thead>
              <tr>
                <th class="tl" colspan="2">商品名称</th>
                <th class="w90">商品原价</th>
                <th class="w90">中奖折扣价</th>
              </tr>
            </thead>
            <tbody nctype="bundling_data"  class="bd-line tip" title="<?php echo $lang['bundling_add_goods_explain'];?>">

              <?php if(!empty($output['g_goods_list'])){?>
              <?php foreach($output['g_goods_list'] as $val){?>
              <?php if (isset($output['goods_list'][$val['goods_id']])) {?>
              <tr id="bundling_tr_<?php echo $val['goods_id']?>" class="off-shelf">
             <td class="w50"><div class="shelf-state"><div class="pic-thumb"><img src="<?php echo cthumb($output['goods_list'][$val['goods_id']]['goods_image'], 60, $_SESSION['store_id']);?>" ncname="<?php echo $output['goods_list'][$val['goods_id']]['goods_image'];?>" nctype="bundling_data_img">
                    </div></div>
                </td>
                <td class="tl"><dl class="goods-name">
                    <dt style="width: 300px;"><?php echo $output['goods_list'][$val['goods_id']]['goods_name'];?></dt>
                  </dl></td>
                <td class="goods-price w90" nctype="bundling_data_price"><?php echo ncPriceFormat($output['goods_list'][$val['goods_id']]['goods_price']);?></td>
                <td class="w90"><?php echo $val['goods_store_price'];?><?php echo ncPriceFormat($val['gs_goods_price']);?></td>
              </tr>
              <?php }?>
              <?php }?>
              <?php }?>
            </tbody>
          </table>
          <div class="div-goods-select-box">
            <div id="bundling_add_goods_ajaxContent"></div>
            <a id="bundling_add_goods_delete" class="close" href="javascript:void(0);" style="display: none; right: -10px;">X</a></div>
        </dd>
      </dl>
      <dl>
      <dt>活动图片：</dt>
      <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb">
              <?php if($output['guess_info']['gs_img']){
                      $img = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$output['guess_info']['gs_img'];
                    } else {
                      $img = UPLOAD_SITE_URL.DS.'shop/common/default_goods_image_240.gif';
                    }
                ?>
               <img id="gs_img" nctype="goods_image" src="<?php echo $img;?>"> 
              </div>
              <input id="arg_gs_img" type="hidden" name="gs_img" value=""/>
              <span></span>
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input hidefocus="true" size="1" class="input-file" name="gs_img_upload" type="file">
                  </span>
                  </a> </div>
              </div>
            </div>
          </div>
      </dd>
    </dl>
      <dl>
        <dt>市场估值：</dt>
        <dd>
          <p>
            <?php echo $output['guess_info']['gs_estimate_price'];?> </p>
        </dd>
      </dl>
      <dl>
        <dt>竞猜区间：</dt>
        <dd>
          <p>
          <?php echo $output['guess_info']['gs_min_price'];?> ~ <?php echo $output['guess_info']['gs_max_price'];?>
           </p>
        </dd>
      </dl>
      <dl>
        <dt>竞猜价：</dt>
        <dd>
          <p><?php echo $output['guess_info']['gs_right_price'];?></p>
        </dd>
      </dl>
      <dl>
      <dl>
        <dt>赠送诺币：</dt>
        <dd>
          <p>
            <?php echo $output['guess_info']['points'];?>
          </p>
        </dd>
      </dl>
      <dl>
        <dt>运费承担：</dt>
        <dd>
        <p>
        <?php if($output['guess_info']['gs_freight_choose'] == 1):?>
          <?php echo "卖家承担";?> 
        <?php endif;?>
        <?php if($output['guess_info']['gs_freight_choose'] == 0):?>
          <?php echo "买承担".$output['guess_info']['gs_right_price']."元";?> 
        <?php endif;?>
        </p>
        </dd>
      </dl>
<script src="<?php echo ADMIN_RESOURCE_URL?>/js/admin.js" type="text/javascript"></script>
<form method="post" name="form1" id="form1" class="ncap-form-dialog" action="<?php echo urlAdminShop('promotion_guess', 'guess_verify_detail');?>">
  <input type="hidden" name="form_submit" value="ok" />
  <input type="hidden" value="<?php echo  $output['guess_info']['gs_id'];?>" name="gs_id">
  <div class="ncap-form-default">
    <dl class="row">
      <dt class="tit">
        <label>审核通过</label>
      </dt>
      <dd class="opt">
        <div class="onoff">
          <label for="rewrite_enabled"  class="cb-enable selected" title="<?php echo $lang['nc_yes'];?>"><?php echo $lang['nc_yes'];?></label>
          <label for="rewrite_disabled" class="cb-disable" title="<?php echo $lang['nc_no'];?>"><?php echo $lang['nc_no'];?></label>
          <input id="rewrite_enabled" name="verify_state" checked="checked" value="1" type="radio">
          <input id="rewrite_disabled" name="verify_state" value="2" type="radio">
        </div>
        <p class="notic"><?php echo $lang['open_rewrite_tips'];?></p>
      </dd>
    </dl>
    <dl class="row" nctype="reason" style="display: none">
      <dt class="tit">
        <label for="verify_reason">未通过理由</label>
      </dt>
      <dd class="opt">
        <textarea rows="6" class="tarea" cols="60" name="verify_desc" id="verify_reason"></textarea>
      </dd>
    </dl>
    <div class="bot"><input type="submit" class="ncap-btn-big ncap-btn-green" nctype="btn_submit" value="<?php echo $lang['nc_submit'];?>"></div>
  </div>
</form>
<script>
$(function(){
    // $('a[nctype="btn_submit"]').click(function(){
    //     ajaxpost('form1', '', '', 'onerror');
    // });
    $('input[name="verify_state"]').click(function(){
        if ($(this).val() == 1) {
            $('dl[nctype="reason"]').hide();
        } else {
            $('dl[nctype="reason"]').show();
        }
    });
});
</script>
  </div>
</div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script> 
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_bundling.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
