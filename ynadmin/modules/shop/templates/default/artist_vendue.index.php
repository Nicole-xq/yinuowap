<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>艺术家拍卖管理</h3>
                <h5>编辑支付拍卖保障金，开启拍卖功能</h5>
            </div>
        </div>
    </div>

    <div id="flexigrid"></div>

</div>
<script type="text/javascript">
    $(function() {
        $("#flexigrid").flexigrid({
            url: 'index.php?act=artist_vendue&op=get_xml',
            colModel: [
                {display: '操作', name: 'operation', width: 150, sortable: false, align: 'center', className: 'handle'},
                {display: '艺术家拍卖ID', name: 'artist_vendue_id', width: 80, sortable: true, align: 'center'},
                {display: '艺术家拍卖名称', name: 'artist_name', width: 150, sortable: false, align: 'left'},
                {display: '店铺名称', name: 'store_name', width: 120, sortable: false, align: 'left'},
                {display: '艺术家职称', name: 'artist_job_title', width: 150, sortable: false, align: 'center'},
                {display: '入驻时间', name: 'add_time', width: 100, sortable: true, align: 'center'},
                {display: '当前状态', name: 'artist_state', width: 80, sortable: true, align: 'center'}

            ],
            searchitems: [
                {display: '艺术家拍卖名称', name: 'artist_name', isdefault: true},
                {display: '店铺名称', name: 'store_name'}
            ],
            sortname: "add_time",
            sortorder: "desc",
            title: '艺术家拍卖列表'
        });
    });



</script>