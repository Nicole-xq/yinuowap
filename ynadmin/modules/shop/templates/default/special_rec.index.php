<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>书画专场推荐</h3>
                <h5>书画频道页推荐专场管理</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=special_rec&op=get_rec_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '推荐名称', name : 'special_name', width : 200, sortable : false, align: 'left'},
                {display: '开始时间', name : 'special_start_time', width : 150, sortable : false, align: 'left'},
                {display: '结束时间', name : 'special_end_time', width : 150, sortable : false, align: 'left'},
                {display: '拍品数量', name : 'auction_count', width : 80, sortable : true, align: 'left'},
                {display: '参拍人数', name : 'person_num', width : 80, sortable : true, align: 'left'},
                {display: '推荐图片', name : 'special_rec_pic', width : 100, sortable : false, align: 'left'},
                {display: '排序', name : 'rec_sort', width : 80, sortable : true, align: 'left'}
            ],

            sortname: "special_id",
            sortorder: "desc",
            title: '书画专场推荐列表'
        });
    });
    $('a[data-href]').live('click', function() {
        if ($(this).hasClass('confirm-on-click') && !confirm('确定"'+$(this).text()+'"?')) {
            return false;
        }

        $.getJSON($(this).attr('data-href'), function(d) {
            if (d && d.result) {
                $("#flexigrid").flexReload();
            } else {
                alert(d && d.message || '操作失败！');
            }
        });
    });
</script>
