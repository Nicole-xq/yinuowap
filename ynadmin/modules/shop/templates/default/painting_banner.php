<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<!--<script type="text/javascript" src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/jquery.Jcrop/jquery.Jcrop.js"></script>-->
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style>
    .ml5 input.red{
        border:1px red dashed;}
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>书画首页banner设置</h3>
                <h5>编辑书画首页banner</h5>
            </div>
        </div>
    </div>

    <form method="post" enctype="multipart/form-data" name="form1" id="adv_form">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default" nctype="adv">
            <dl class="row">
                <dt class="tit">
                    <label>书画首页banner图片</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_PAINTING.DS.$output['list']['pic']);?>"><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_PAINTING.DS.$output['list']['pic']);?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
                    <input name="banner_image" id="banner_image"  type="file" class="type-file-file"  size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
                    <input type='text' name='textfield' id="textfield"  class='type-file-text' />
                    <input type='button' name='button' id='button' value='选择上传...' class='type-file-button' />
                                    <input type="hidden" name="banner_pic" id="banner_pic" value="<?php echo $output['list']['pic']?>"/>
                    </span>
                    </div>
                    <span id="error"></span>
                    <label title="" class="ml5"><i class="fa fa-link"></i><input class="input-txt ml5" type="text"  name="banner_url" value="<?php echo $output['list']['url'];?>" placeholder="请输入跳转的链接地址" /></label>
                    <p class="notic">推荐图片宽为1920px，高为466px</p>
                </dd>
            </dl>

        </div>

        <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn" ><?php echo $lang['nc_submit'];?></a></div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    // 模拟网站LOGO上传input type='file'样式
    $(function(){
        $("#banner_image").change(function(){
            $("#textfield").val($(this).val());
            $('#banner_pic').val($(this).val());
        });
// 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("图片限于png,gif,jpeg,jpg格式");
                $(this).attr('value','');
                return false;
            }
        });



        $('#adv_form').find('a[nctype="del"]').live('click', function(){
            $(this).parents('dl:first').remove();
        });

        // 点击查看图片
        $('.nyroModal').nyroModal();
        $('#time_zone').attr('value','<?php echo $output['list']['time_zone'];?>');

        $("#submitBtn").click(function(){
            var flag = true;
            $('.ml5 input').each(function(){
                if(!$(this).val()){
                    $(this).addClass('red');
                    flag = false;
                }else{
                    $(this).removeClass('red');
                }
            });
            if(!$('#banner_pic').val()){
                $('#error').html('<font style="color:#ff0000;">请上传图片</font>');
                flag = false;
            } else{
                $('#error').html('');
            }
            if(flag){
                $("#adv_form").submit();
            }
        });



    });




</script>