<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>书画模块名称管理</h3>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=paint_module&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '模块名称ID', name : 'paint_module_id', width : 100, sortable : true, align: 'center'},
                {display: '模块名称', name : 'paint_module_name', width : 150, sortable : false, align: 'left'},
                {display: '显示', name : 'paint_module_show', width: 80, sortable : true, align : 'center'},
                {display: '排序', name : 'paint_module_sort', width: 60, sortable : true, align : 'center'}

            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '添加一条新数据到列表', onpress : fg_operation }
            ],
            searchitems : [
                {display: '模块名称ID', name : 'paint_module_id', isdefault: true},
                {display: '模块名称', name : 'paint_module_name'}
            ],
            sortname: "paint_module_id",
            sortorder: "desc",
            title: '模块名称列表'
        });
    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=paint_module&op=module_add';
        }

    }


    //删除
    function fg_del(id) {
        if(!confirm('删除后将不能恢复，确认删除这项吗？')){
            return false;
        }
        $.getJSON('index.php?act=paint_module&op=module_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
</script>
