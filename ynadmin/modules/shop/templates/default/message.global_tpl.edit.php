<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
  .lm_fs_panel{
    display: inline-block;
    margin-left: 15px;
  }
  .lm_fs_panel p{
    display: inline-block;
  }
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=message&op=global_tpl" title="返回全局推送列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>全局推送消息管理 - 新增全局消息模板</a></h3>
            </div>
        </div>
    </div>
    <form id="global_form" method="post" name="form1">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="tpl_title"><em>*</em>消息模板名称</label>
                </dt>
                <dd class="opt">
                    <input id="tpl_title" name="tpl_title" value="" class="input-txt" type="text" maxlength="20">
                    <span class="err"></span>
                    <p class="notic">仅内部查看用</p>
                </dd>
            </dl>
            <div class="title">
                <h3 style="display:inline-block;">发送内容</h3>
                <div class="lm_fs_panel">
                  <p>
                    <input type="radio" id="lm_fs_on" value="1" name="mmt_message_switch" checked>
                    <label for="lm_fs_on">开启</label>
                  </p>

                  <p>
                    <input type="radio" id="lm_fs_off" value="0" name="mmt_message_switch">
                    <label for="lm_fs_off">关闭</label>
                  </p>

                </div>
            </div>
            <div class="lm_kz_con">
              <dl class="row">
                <dt class="tit">
                  <label>消息标题</label>
                </dt>
                <dd class="opt">
                  <input id="mmt_message_subject" name="mmt_message_subject" value="" class="input-txt" type="text">
                  <span class="err"></span>
                  <p class="notic">最多20个字</p>
                </dd>
              </dl>
              <dl class="row">
                <dt class="tit">
                  <label>消息内容</label>
                </dt>
                <dd class="opt">
                  <textarea name="mmt_message_content" rows="6" class="tarea" id="mmt_message_content" ></textarea>
                  <span class="err"></span>
                  <p class="notic">最多500个字</p>
                </dd>
              </dl>
            </div>
            <div class="title">
              <h3 style="display:inline-block;">短信</h3>
              <div class="lm_fs_panel">
                <p>
                  <input type="radio" id="lm_dx_on" value="1" name="mmt_short_switch" checked>
                  <label for="lm_dx_on">开启</label>
                </p>
                <p>
                  <input type="radio" id="lm_dx_off" value="0" name="mmt_short_switch">
                  <label for="lm_dx_off">关闭</label>
                </p>
              </div>
            </div>

            <dl class="row lm_kz_con">
                <dt class="tit">
                    <label for="short_content">短信内容</label>
                </dt>
                <dd class="opt">
                    <textarea name="mmt_short_content" rows="6" class="tarea" id="mmt_short_content" ></textarea>
                    <span class="err"></span>
                    <p class="notic">60个汉字作为一条短信</p>
                </dd>
            </dl>
            <div class="title"><h3>发送方式</h3></div>
            <dl class="row">
                <dt class="tit">
                    <label for="send_way">立即发送</label>
                </dt>
                <dd class="opt">
                    <input type="radio" name="send_way" value="1" checked>
                </dd>
            </dl>
            <!--<dl class="row">
                <dt class="tit">
                    <label for="send_way">定时发送</label>
                </dt>
                <dd class="opt">
                    <input type="radio" name="send_way" value="2">
                    <input readonly id="send_time" placeholder="请选择起始时间" name=send_time value="" type="text" class="s-input-txt" />
                </dd>
            </dl>-->
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script>
    //按钮先执行验证再提交表单
    $(function(){
        $('#send_time').datepicker();

        $("#submitBtn").click(function(){
            if($("#global_form").valid()){
                $("#global_form").submit();
            }
        });
        $('.lm_fs_panel p').click(function () {
            var self = $(this);
          if(self.find('input[type="radio"]').prop('checked')){
              if(self.find('input[type="radio"]').attr('id').indexOf('on')!=-1){
                  //显示
                  self.parents('.title').next('.lm_kz_con').show();
              }else{
                  self.parents('.title').next('.lm_kz_con').hide();
              }
          }
        });
        $("#global_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                tpl_title : {
                    required : true
                }
            },
            messages : {
                tpl_title : {
                    required : "<i class='fa fa-exclamation-circle'></i>消息模板名称不能为空"
                }
            }
        });
    });

</script>
