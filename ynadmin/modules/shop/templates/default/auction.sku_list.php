<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<style type="text/css">
  .ncap-goods-sku .title h4, .ncap-goods-sku .content span{
    width: 14%;
    padding: 3px 1%;
  }
</style>
<div class="ncap-goods-sku">
    <div class="title">
        <h4 style="width: 38%">拍品名称</h4>
        <h4 style="width: 10%">拍品图片</h4>
        <h4>保留价(元)</h4>
        <h4>一级返佣比例</h4>
        <h4>二级返佣比例</h4>
    </div>
    <div class="content">
        <ul>
            <?php foreach ($output['auctions_list'] as $val) { ?>
                <li>
                    <span style="width: 38%"><?php echo $val['auction_name']; ?></span>
                    <span style="width: 10%">
                      <img src="<?php echo $val['auction_image']; ?>" onMouseOver="toolTip('<img src=<?php echo $val['auction_image']; ?>>')" onMouseOut="toolTip()">
                    </span>
                    <span><?php echo $val['auction_reserve_price']; ?></span>
                    <span>
                      <input type="text" name="commis_level_1[<?php echo $val['auction_id']; ?>]" class="w40" auction-id="<?php echo $val['auction_id']; ?>" data-type="commis_level_1" value="<?php echo $val['commis_level_1']; ?>">&nbsp;%
                    </span>
                    <span>
                      <input type="text" name="commis_level_2[<?php echo $val['auction_id']; ?>]" class="w40" auction-id="<?php echo $val['auction_id']; ?>" data-type="commis_level_2" value="<?php echo $val['commis_level_2']; ?>">&nbsp;%
                    </span>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //自动加载滚动条
        $('.content').perfectScrollbar();
        $('input').blur(function () {
            var percent = parseFloat($(this).val());
            var auction_id = parseInt($(this).attr('auction-id'));
            var column = $(this).attr('data-type');
            if (percent <= 0 || percent > 100) {
                return false;
            }
            $.get('index.php?act=auctions&op=auctions_precent_set&per=' + percent + '&auction_id=' + auction_id + '&column=' + column);
        });
    });
</script> 