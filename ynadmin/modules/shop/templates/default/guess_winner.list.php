<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>趣猜中奖</h3>
            </div>

        </div>
    </div>


    <div id="flexigrid"></div>

    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" name="formSearch" id="formSearch">
            <input type="hidden" name="advanced" value="1" />
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dt>活动名称</dt>
                        <dd>
                            <input type="text" name="name" class="s-input-txt" placeholder="请输入活动名称关键字" />
                        </dd>
                    </dl>
                    <dl>
                        <dt>店铺名称</dt>
                        <dd>
                            <input type="text" name="store_name" class="s-input-txt" placeholder="请输入店铺名称关键字" />
                        </dd>
                    </dl>
                    <dl>
                        <dt>状态</dt>
                        <dd>
                            <select name="state" class="s-select">
                                <?php foreach ((array) $output['act_guess_state_array'] as $sk => $sv) { ?>
                                    <option value="<?php echo $sk; ?>"><?php echo $sv; ?></option>
                                <?php } ?>
                            </select>
                        </dd>
                    </dl>
                    <dl>
                        <dt>活动时期筛选</dt>
                        <dd>
                            <label>
                                <input type="text" name="pdate1" data-dp="1" class="s-input-txt" placeholder="结束时间不晚于" />
                            </label>
                            <label>
                                <input type="text" name="pdate2" data-dp="1" class="s-input-txt" placeholder="开始时间不早于" />
                            </label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom">
                <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green">提交查询</a>
                <a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容"><i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?></a>
            </div>
        </form>
    </div>

</div>

<script>
    $(function(){
        var flexUrl = 'index.php?act=promotion_guess_winner&op=winner_list_xml';

        $("#flexigrid").flexigrid({
            url: flexUrl,
            colModel: [
                {display: '操作', name: 'operation', width: 150, sortable: false, align: 'left', className: 'handle'},
                {display: '活动ID', name: 'gs_id', width: 80, sortable: true, align: 'left'},
                {display: '活动名称', name: 'gs_name', width: 200, sortable: true, align: 'left'},
                {display: '店铺名称', name: 'store_name', width: 200, sortable: false, align: 'left'},
                {display: '参与人数', name: 'joined_times', width: 80, sortable: false, align: 'center'},
                {display: '中奖人ID', name: 'member_id', width: 50, sortable: true, align: 'center'},
                {display: '中奖人帐号', name: 'member_name', width: 120, sortable: true, align: 'center'},
                {display: '中猜价', name: 'gs_offer_price', width: 80, sortable: true, align: 'right'},
                {display: '中奖时间', name: 'end_time', width: 120, sortable: true, align: 'center'},
                {display: '是否下单', name: 'gs_state', width: 60, sortable: false, align: 'center'},
                {display: '下单时间', name: 'order_time', width: 120, sortable: false, align: 'center'},
            ],
            searchitems: [
                {display: '活动ID', name: 'gs_id', isdefault: true},
                {display: '活动名称', name: 'gs_name'},
            ],
            sortname: "id",
            sortorder: "desc",
            title: '趣猜中奖列表'
        });

        // 高级搜索提交
        $('#ncsubmit').click(function(){
            $("#flexigrid").flexOptions({url: flexUrl + '&' + $("#formSearch").serialize(),query:'',qtype:''}).flexReload();
        });

        // 高级搜索重置
        $('#ncreset').click(function(){
            $("#flexigrid").flexOptions({url: flexUrl}).flexReload();
            $("#formSearch")[0].reset();
        });

        $('[data-dp]').datepicker({dateFormat: 'yy-mm-dd'});

    });

    $('a[data-href]').live('click', function() {
        if ($(this).hasClass('confirm-on-click') && !confirm('确定"'+$(this).text()+'"?')) {
            return false;
        }

        $.getJSON($(this).attr('data-href'), function(d) {
            if (d && d.result) {
                $("#flexigrid").flexReload();
            } else {
                alert(d && d.message || '操作失败！');
            }
        });
    });

</script>
