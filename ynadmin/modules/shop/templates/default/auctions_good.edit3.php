<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auctions_good&op=index" title="返回分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>编辑拍品</a></h3>
        <h5>拍品编辑</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="auction_id" value="<?php echo $info['auction_id']; ?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auction_name"><em>*</em>拍品名称</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_name']; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auction_reserve_price"><em>*</em>保留价金额</label>
        </dt>
        <dd class="opt">
          <input type="text" id="auction_reserve_price" name="auction_reserve_price" class="input-txt" value="<?php echo $info['auction_reserve_price'] ?: 0; ?>"  >
          <span class="err"></span>
        </dd>
      </dl>
      
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
  $('#add_form').validate({
    errorPlacement: function(error, element){
      var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      auction_reserve_price: {
        required : true,
        number:true,
        min:0,
      }
    },
    messages : {
      auction_reserve_price : {
        required : '<i class="fa fa-exclamation-circle"></i>必填', 
        number : '<i class="fa fa-exclamation-circle"></i>必填是数字',
        min : '<i class="fa fa-exclamation-circle"></i>必填大于0',
      }
    }
  });

  $("#submitBtn").click(
    function(){
      if($("#add_form").valid()){
         $("#add_form").submit();
    	}
  	}
  );
});
</script>