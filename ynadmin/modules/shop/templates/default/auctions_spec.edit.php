<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auctions_spec&op=index" title="返回分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>规格- 新增</h3>
        <h5>拍品规格新增</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="auctions_spec_id" value="<?php echo $output['class']['auctions_spec_id'] ?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auctions_spec_name"><em>*</em>规格名称</label>
        </dt>
        <dd class="opt">
          <input type="text" id="auctions_spec_name" name="auctions_spec_name" class="input-txt" value="<?php echo $output['class']['auctions_spec_name'] ?>" > 
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>一级分类</label>
        </dt>
        <dd class="opt">
          <select class="class-select" name="auctions_class_id_lv_1" id="auctions_class_id_lv_1">
              <option value="0">选择分类</option>
              <?php if(!empty($output['gc_list'])){ ?>
              <?php foreach($output['gc_list'] as $k => $v){ ?>
              <?php if ($v['auctions_class_fid'] == 0) {?>
              <option value="<?php echo $v['auctions_class_id'];?>"
                <?php if ($v['auctions_class_id']==$output['class']['auctions_class_id_lv_1']) {
                  echo "selected";
                } ?> 
              ><?php echo $v['auctions_class_name'];?></option>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </select>
          <span class="err"></span>         
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em></em>二级分类</label>
        </dt>
        <dd class="opt">
          <select class="class-select" name="auctions_class_id_lv_2" id="auctions_class_id_lv_2">
              <option value="0">选择分类</option>
              <?php if(!empty($output['gc_list2'])){ ?>
              <?php foreach($output['gc_list2'] as $k => $v){ ?>
              <option value="<?php echo $v['auctions_class_id'];?>"
                <?php if ($v['auctions_class_id']==$output['class']['auctions_class_id_lv_2']) {
                  echo "selected";
                } ?> 
              ><?php echo $v['auctions_class_name'];?></option>
s              <?php } ?>
              <?php } ?>
            </select>
          <span class="err"></span>         
        </dd>
      </dl>
      
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
  $('#add_form').validate({
    errorPlacement: function(error, element){
      var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      auctions_spec_name: {
        required : true,
      },
      auctions_class_id_lv_1:{
        required : true,
      }
    },
    messages : {      
      auctions_spec_name : {
        required : '<i class="fa fa-exclamation-circle"></i>必填',               
      },
      auctions_class_id_lv_1:{
        required : '<i class="fa fa-exclamation-circle"></i>必选', 
      }
    }
  });

  $("#submitBtn").click(function(){
    if($("#add_form").valid()){
       $("#add_form").submit();
  	}
	});
  $("#auctions_class_id_lv_1").change(function(){
    var lv1=$(this).children('option:selected').val();
    htmlobj=$.ajax({url:'index.php?act=auctions_spec&op=auctions_spec_class_option&id='+lv1,async:false});
    $("#auctions_class_id_lv_2").html(htmlobj.responseText);
  });
});

</script>