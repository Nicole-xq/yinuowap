<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['nc_message_set'];?></h3>
        <h5><?php echo $lang['nc_message_set_subhead'];?></h5>
      </div>
      <?php echo $output['top_link'];?> </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>平台可以选择开启一种或多种消息通知方式。</li>
      <li>短消息、邮件需要用户绑定手机、邮箱后才能正常接收。</li>
      <li class="red">编辑完成后请清理“用户消息模板”缓存。</li>
    </ul>
  </div>
    <div id="flexigrid"></div>
</div>
<script>
    function update_flex(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=message&op=get_global_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 5, sortable : false, align: 'center', className: 'handle'},
                {display: '模板标题', name : 'tpl_title', width : 150, sortable : false, align: 'center'},
                {display: '消息标题', name : 'mmt_message_subject',  width : 150, sortable : false, align: 'center'},
                {display: '消息内容', name : 'mmt_message_content', width : 200, sortable : false, align: 'center'},
                {display: '短信内容', name : 'mmt_short_content',  width : 200, sortable : false, align: 'center'},
                {display: '发送类型', name : 'send_type',  width : 100, sortable : false, align: 'center'},
                {display: '发送状态', name : 'send_state',  width : 50, sortable : false, align: 'center'},
                {display: '最新发送时间', name : 'send_time',  width : 150, sortable : false, align: 'center'},
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新建全局推送模板', name : 'add', bclass : 'add', title : '新增频道', onpress : fg_operation_add }
            ],
            searchitems : [
                {display: '模板标题', name : 'tpl_title'}
            ],
            usepager: true,
            rp: 15,
            title: '全局推送消息列表'
        });
    }
    function fg_operation_add(name, bDiv){
        var _url = 'index.php?act=message&op=global_tpl_edit';
        window.location.href = _url;
    }
    function fg_operation_del(channel_id){
        if(confirm('删除后将不能恢复，确认删除这项吗？')){
            var _url = 'index.php?act=web_channel&op=del_channel&channel_id='+channel_id;
            $.getJSON(_url, function(data){
                if (data.state) {
                    $("#flexigrid").flexReload();
                } else {
                    showError(data.msg)
                }
            });
        }
    }

    $(function(){
        update_flex();
    });

</script>