<?php defined('InShopNC') or exit('Access Invalid!');?>
<div id="flexigrid"></div>
<script type="text/javascript">
$(function(){
    var query_type = '<?php echo $_GET['query_type'];?>';
    if (query_type == 'recharge' || query_type == ''){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=recharge_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '操作', name : 'operation', width : 80, sortable : false, align: 'center'},
                {display: '充值单号', name : 'pdr_sn', width : 150, sortable : false, align: 'center'},
                {display: '创建时间', name : 'pdr_add_time', width : 150, sortable : true, align: 'left'},
                {display: '支付方式', name : 'pdr_payment_name', width: 100, sortable : true, align : 'left'},
                {display: '充值金额', name : 'pdr_amount', width : 100, sortable : false, align: 'left'},
                {display: '状态', name : 'pdr_payment_state', width : 100, sortable : true, align: 'left'}
            ],
            title: '账单-充值列表'
        });
    }else if(query_type == 'enchashment'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=enchashment_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '申请单号', name : 'pdr_sn', width : 150, sortable : false, align: 'center'},
                {display: '申请时间', name : 'pdr_add_time', width : 150, sortable : true, align: 'left'},
                {display: '提现金额', name : 'pdr_amount', width : 100, sortable : false, align: 'left'},
                {display: '状态', name : 'pdr_payment_state', width : 100, sortable : true, align: 'left'}
            ],
            title: '账单-提现列表'
        });
    }else if(query_type == 'order_pay'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=orderPay_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '订单编号', name : 'order_sn', width : 150, sortable : false, align: 'center'},
                {display: '订单来源', name : 'order_from', width : 50, sortable : true, align: 'center'},
                {display: '下单时间', name : 'add_times', width : 150, sortable : true, align: 'center'},
                {display: '订单金额（元）', name : 'order_amount', width : 100, sortable : false, align: 'left'},
                {display: '订单状态', name : 'order_state', width : 100, sortable : true, align: 'left'},
                {display: '支付单号', name : 'pay_sn', width : 150, sortable : true, align: 'left'},
                {display: '支付方式', name : 'payment_code', width : 100, sortable : true, align: 'left'},
                {display: '支付时间', name : 'payment_time', width : 150, sortable : true, align: 'left'},
                {display: '订单完成时间', name : 'finnshed_time', width : 150, sortable : true, align: 'left'},
                {display: '诺币支付（元）', name : 'points_amount', width : 100, sortable : true, align: 'left'},
                {display: '退款金额（元）', name : 'refund_amount', width : 100, sortable : true, align: 'left'},
                {display: '预存款支付（元）', name : 'pd_amount', width : 100, sortable : true, align: 'left'},
            ],
            title: '账单-预存款订单支付列表'
        });
    }else if(query_type == 'earnest'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=earnest_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '操作', name : 'operation', width : 80, sortable : false, align: 'center'},
                {display: '订单编号', name : 'order_sn', width : 180, sortable : false, align: 'center'},
                {display: '订单来源', name : 'order_from', width : 50, sortable : true, align: 'center'},
                {display: '下单时间', name : 'created_at', width : 150, sortable : false, align: 'left'},
                {display: '订单金额', name : 'margin_amount', width : 100, sortable : false, align: 'left'},
                {display: '订单状态', name : 'order_state', width : 50, sortable : true, align: 'left'},
                {display: '支付方式', name : 'payment_code', width : 100, sortable : true, align: 'left'},
                {display: '支付时间', name : 'payment_time', width : 150, sortable : true, align: 'left'},
                {display: '完成时间', name : 'finnshed_time', width : 150, sortable : true, align: 'left'},
                {display: '充值卡支付（元）', name : 'rcb_amount', width : 100, sortable : true, align: 'left'},
                {display: '预存款支付（元）', name : 'pd_amount', width : 100, sortable : true, align: 'left'},
                {display: '诺币支付（元）', name : 'points_amount', width : 100, sortable : true, align: 'left'},
                {display: '保证金金额支付（元）', name : 'account_margin_amount', width : 100, sortable : true, align: 'left'},
                {display: '退款金额（元）', name : 'refund_amount', width : 100, sortable : true, align: 'left'},
                {display: '第三方支付金额（元）', name : 'api_pay_amount', width : 100, sortable : true, align: 'left'},
            ],
            title: '账单-保证金支付金额列表'
        });
    }else if(query_type == 'commission'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=commission_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '返佣ID', name : 'log_id', width : 150, sortable : false, align: 'center'},
                {display: '结算时间', name : 'commission_time', width : 150, sortable : true, align: 'center'},
                {display: '奖励金额', name : 'commission_amount', width : 100, sortable : false, align: 'left'},
                {display: '奖励描述', name : 'goods_name', width : 500, sortable : true, align: 'left'}
            ],
            title: '账单-奖励金结算列表'
        });
    }else if(query_type == 'auction'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=auction_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '订单编号', name : 'order_sn', width : 180, sortable : false, align: 'center'},
                {display: '订单来源', name : 'order_from', width : 50, sortable : true, align: 'center'},
                {display: '下单时间', name : 'created_at', width : 150, sortable : false, align: 'left'},
                {display: '订单金额', name : 'margin_amount', width : 100, sortable : false, align: 'left'},
                {display: '订单状态', name : 'order_state', width : 50, sortable : true, align: 'left'},
                {display: '支付方式', name : 'payment_code', width : 100, sortable : true, align: 'left'},
                {display: '支付时间', name : 'payment_time', width : 150, sortable : true, align: 'left'},
                {display: '完成时间', name : 'finnshed_time', width : 150, sortable : true, align: 'left'},
                {display: '预存款支付（元）', name : 'pd_amount', width : 100, sortable : true, align: 'left'},
                {display: '诺币支付（元）', name : 'points_amount', width : 100, sortable : true, align: 'left'},
                {display: '保证金金额支付（元）', name : 'account_margin_amount', width : 100, sortable : true, align: 'left'},
                {display: '退款金额（元）', name : 'refund_amount', width : 100, sortable : true, align: 'left'},
            ],
            title: '账单-冻结保证金列表'
        });
    }else if(query_type == 'predeposit'){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member&op=predeposit_list_xml&member_id=<?php echo $_GET['member_id'];?>',
            colModel : [
                {display: '日志ID', name : 'lg_id', width : 50, sortable : false, align: 'center'},
                {display: '更新金额（元）', name : 'lg_av_amount', width : 100, sortable : false, align: 'left'},
                {display: '冻结金额（元）', name : 'lg_freeze_amount', width : 100, sortable : true, align: 'left'},
                {display: '冻结预存款总额（元）', name : 'lg_freeze_predeposit', width : 150, sortable : true, align: 'left'},
                {display: '可用预存款总额（元）', name : 'lg_available_amount', width : 150, sortable : true, align: 'left'},
                {display: '添加时间', name : 'lg_add_time', width : 150, sortable : true, align: 'left'},
                {display: '日志描述', name : 'lg_desc', width : 350, sortable : true, align: 'left'},
            ],
            title: '预存款变动明细列表'
        });
    }

});
</script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/orders/financial_verification.js"></script>