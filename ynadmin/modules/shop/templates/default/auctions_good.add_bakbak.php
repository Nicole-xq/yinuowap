<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="index.php?act=auctions_good&op=index" title="返回分类列表"><i
                        class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>新增拍品</a></h3>
                <h5>拍品新增</h5>
            </div>
        </div>
    </div>
    <form id="add_form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="form_submit" value="ok"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="auction_name"><em>*</em>拍品名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_name" name="auction_name" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="store_name"><em>*</em>店铺名字</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="store_name" name="store_name" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_bond"><em>*</em>拍品保证金</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_bond" name="auction_bond" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_start_price"><em>*</em>拍品起拍价</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_start_price" name="auction_start_price" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_reference_price"><em>*</em>拍品参考价</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_reference_price" name="auction_reference_price" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_cost"><em>*</em>拍品成本价格</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_cost" name="auction_cost" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_increase_range"><em>*</em>拍品加价幅度</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="auction_increase_range" name="auction_increase_range" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <!--      <dl class="row">-->
            <!--        <dt class="tit">-->
            <!--          <label for="auction_reserve_price"><em>*</em>拍品保留价</label>-->
            <!--        </dt>-->
            <!--        <dd class="opt">-->
            <!--          <input type="text" id="auction_reserve_price" name="auction_reserve_price" class="input-txt" >-->
            <!--          <span class="err"></span>-->
            <!--        </dd>-->
            <!--      </dl>      -->
            <dl class="row">
                <dt class="tit">
                    <label for="delivery_mechanism"><em></em>送拍机构</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="delivery_mechanism" name="delivery_mechanism" class="input-txt">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="warehouse_location"><em></em>库位号</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="warehouse_location" name="warehouse_location" class="input-txt"
                           placeholder="A12031:A区12号货架第31号库位">
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auction_image"><em></em>封面图</label>
                </dt>
                <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show">
                  <span class="type-file-box">
                  <input type="file" class="type-file-file" id="auction_image" name="auction_image" size="30"
                         hidefocus="true" nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
                  </span>
                </div>
                <span class="err"></span>
                <input type="hidden" name="auctions_img" id="auctions_img" value=""/>
              </span>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em></em>明细图</label>
                </dt>
                <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show"><span class="type-file-box">
        <input type="file" class="type-file-file" id="auctions_img1" name="auctions_img1" size="30" hidefocus="true"
               nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
        </span>
      </div>
                <span class="err"></span>
                <input type="hidden" name="auctions_img1" id="auctions_img1" value=""/>
              </span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>明细图</label>
                </dt>
                <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show"><span class="type-file-box">
        <input type="file" class="type-file-file" id="auctions_img2" name="auctions_img2" size="30" hidefocus="true"
               nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
        </span>
      </div>
                <span class="err"></span>
                <input type="hidden" name="auctions_img2" id="auctions_img2" value=""/>
              </span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>明细图</label>
                </dt>
                <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show"><span class="type-file-box">
        <input type="file" class="type-file-file" id="auctions_img3" name="auctions_img3" size="30" hidefocus="true"
               nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
        </span>
      </div>
                <span class="err"></span>
                <input type="hidden" name="auctions_img3" id="auctions_img3" value=""/>
              </span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>明细图</label>
                </dt>
                <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show"><span class="type-file-box">
        <input type="file" class="type-file-file" id="auctions_img4" name="auctions_img4" size="30" hidefocus="true"
               nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
        </span>
      </div>
                <span class="err"></span>
                <input type="hidden" name="auctions_img4" id="auctions_img4" value=""/>
              </span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="is_pd_pay"><em>*</em>拍品类型</label>
                </dt>
                <dd class="opt">
                    <select class="class-select" name="auction_type" id="auction_type">
                        <option value="0" selected="selected">普通拍品</option>
                        <option value="2">新手专区</option>
                    </select>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auctions_class_lv1"><em>*</em>一级分类</label>
                </dt>
                <dd class="opt">
                    <select class="class-select" name="auctions_class_lv1" id="auctions_class_lv1">
                        <option value="">选择分类</option>
                        <?php if (!empty($output['gc_list'])) { ?>
                            <?php foreach ($output['gc_list'] as $k => $v) { ?>
                                <?php if ($v['auctions_class_fid'] == 0) { ?>
                                    <option value="<?php echo $v['auctions_class_id']; ?>"><?php echo $v['auctions_class_name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="auctions_class_lv2"><em>*</em>二级分类</label>
                </dt>
                <dd class="opt">
                    <select class="class-select" name="auctions_class_lv2" id="auctions_class_lv2">
                        <option value="">选择分类</option>
                    </select>
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>规格</label>
                </dt>
                <dd class="opt" id="guige">

                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="long"><em></em>大小</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="long" class="input-txt" placeholder="长">*<input type="text" name="width"
                                                                                             class="input-txt"
                                                                                             placeholder="宽">*<input
                            type="text" name="height" class="input-txt" placeholder="高">
                    <span class="err"></span>

                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="create_age"><em>*</em>创作年代</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="create_age" id="create_age" class="input-txt">
                    <span class="err"></span>

                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>作品简介</label>
                </dt>
                <dd class="opt">
                    <?php showEditor('brand_description'); ?>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="share_content"><em>*</em>分享描述</label>
                </dt>
                <dd class="opt">
                    <textarea name="share_content" rows="6" class="tarea" id="share_content"></textarea>
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>艺术家</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="keyword" class="input-txt" placeholder="搜索">
                    &nbsp;&nbsp;&nbsp;
                    <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="search"><span>搜索</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="add_artist"></span>
                </dd>
            </dl>
            <div class="bot" id="artist"></div>
            <div class="bot">
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green"
                   id="submitBtn"><span><?php echo $lang['nc_submit']; ?></span></a></div>
        </div>
    </form>
</div>
<script>
    //按钮先执行验证再提交表单
    $(function () {
        $('#add_form').validate({
            errorPlacement: function (error, element) {
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules: {
                auction_name: {
                    required: true,
                },
                store_name: {
                    required: true,
                },
                auction_bond: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auction_start_price: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auction_reference_price: {
                    required: true,
                    number: true,
                    min: 0,
                },
                auctions_class_lv1: {
                    required: true,
                },
                auctions_class_lv2: {
                    required: true,
                },
                auction_increase_range: {
                    required: true,
                    number: true,
                    min: 0,
                },
                // auction_reserve_price: {
                //   required : true,
                //   number:true,
                //   min:0,
                // },
                create_age: {
                    required: true,
                },
                share_content: {
                    required: true,
                }
            },
            messages: {
                auction_name: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                },
                store_name: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                },
                auction_bond: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_start_price: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_reference_price: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auction_cost: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                auctions_class_lv1: {
                    required: '<i class="fa fa-exclamation-circle"></i>必选',
                },
                auctions_class_lv2: {
                    required: '<i class="fa fa-exclamation-circle"></i>必选',
                },
                auction_increase_range: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                    number: '<i class="fa fa-exclamation-circle"></i>必填是数字',
                    min: '<i class="fa fa-exclamation-circle"></i>必填大于0',
                },
                // auction_reserve_price : {
                //   required : '<i class="fa fa-exclamation-circle"></i>必填',
                //   number : '<i class="fa fa-exclamation-circle"></i>必填是数字',
                //   min : '<i class="fa fa-exclamation-circle"></i>必填大于0',
                // },
                create_age: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                },
                share_content: {
                    required: '<i class="fa fa-exclamation-circle"></i>必填',
                },
            }
        });

        $("#submitBtn").click(
            function () {
                if ($("#add_form").valid()) {
                    $("#add_form").submit();
                }
            }
        );
        $("#search").click(
            function () {
                var keyword = $("#keyword").val();
                if (keyword) {
                    htmlobj = $.ajax({
                        url: 'index.php?act=auctions_artist&op=auctions_artist_list&keyword=' + keyword,
                        async: false
                    });
                    $("#artist").html(htmlobj.responseText);
                } else {
                    alert('艺术家名字不能为空');
                }
            }
        );
        $("#auctions_class_lv1").change(function () {
            var lv1 = $(this).children('option:selected').val();
            htmlobj = $.ajax({
                url: 'index.php?act=auctions_spec&op=auctions_spec_class_option&id=' + lv1,
                async: false
            });
            $("#auctions_class_lv2").html(htmlobj.responseText);
        });
        $("#auctions_class_lv2").change(function () {
            var lv1 = $("#auctions_class_lv1").children('option:selected').val()
            var lv2 = $(this).children('option:selected').val();
            htmlobj = $.ajax({
                url: 'index.php?act=auctions_spec&op=get_auctions_spec_class_option&id=' + lv1 + '&id2=' + lv2,
                async: false
            });
            $("#guige").html(htmlobj.responseText);
        });
        var textButton = "<input type='text' name='textfield' id='textfield' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton).insertBefore("#auction_image");
        $("#auction_image").change(function () {
            $("#textfield").val($("#auction_image").val());
        });

        var textButton1 = "<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton1).insertBefore("#auctions_img1");
        $("#auctions_img1").change(function () {
            $("#textfield1").val($("#auctions_img1").val());
        });
        var textButton2 = "<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton2).insertBefore("#auctions_img2");
        $("#auctions_img2").change(function () {
            $("#textfield2").val($("#auctions_img2").val());
        });
        var textButton3 = "<input type='text' name='textfield' id='textfield3' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton3).insertBefore("#auctions_img3");
        $("#auctions_img3").change(function () {
            $("#textfield3").val($("#auctions_img3").val());
        });
        var textButton4 = "<input type='text' name='textfield' id='textfield4' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton4).insertBefore("#auctions_img4");
        $("#auctions_img4").change(function () {
            $("#textfield4").val($("#auctions_img4").val());
        });
    });

    function bind_artist(id) {
        htmlobj = $.ajax({url: 'index.php?act=auctions_artist&op=auctions_artist_info&id=' + id, async: false});
        $("#add_artist").html(htmlobj.responseText);
    }
</script>