<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auction_order&op=index" title="返回订单列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>订单- 发货</a></h3>
        <h5>发货</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
        <dl class="row">
            <dt class="tit">
                <label for="shipping_express_id"><em>*</em>拍品名称</label>
            </dt>
            <dd class="opt">
                <label for="shipping_express_id"><?php echo $output['auction_info']['auction_name'];?></label>
            </dd>
        </dl>
      <dl class="row">
        <dt class="tit">
          <label for="shipping_express_id"><em>*</em>配送公司</label>
        </dt>
        <dd class="opt">
          <select class="class-select" name="shipping_express_id" id="shipping_express_id">
              <?php if(!empty($output['list'])){ ?>
              <?php foreach($output['list'] as $k => $v){ ?>
              <option value="<?php echo $v['id'];?>"><?php echo $v['e_name'];?></option>
              <?php } ?>
              <?php } ?>
            </select>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="shipping_code"><em>*</em>快递单号</label>
        </dt>
        <dd class="opt">
          <input type="text" id="shipping_code" name="shipping_code" class="input-txt" >
          <span class="err"></span>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
  $('#add_form').validate({
    errorPlacement: function(error, element){
      var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      shipping_code: {
        required : true,
      }      
    },
    messages : {      
      shipping_code : {
        required : '<i class="fa fa-exclamation-circle"></i>必填',               
      }
    }
  });
  $("#submitBtn").click(function(){
    if($("#add_form").valid()){
     $("#add_form").submit();
     }
  });
  
  var textButton1="<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton1).insertBefore("#auctions_artist_img");
        $("#auctions_artist_img").change(function(){
            $("#textfield2").val($("#auctions_artist_img").val());
            $('#adv_pic').val($("#auctions_artist_img").val());
        });
});
</script>