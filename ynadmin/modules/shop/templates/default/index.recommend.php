<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['nc_web_index'];?></h3>
        <h5><?php echo $lang['nc_web_index_subhead'];?></h5>
      </div>
       <ul class="tab-base nc-row">
        <li><a href="index.php?act=web_config&op=web_config"><?php echo '板块区';?></a></li>
        <li><a href="index.php?act=web_config&op=focus_edit"><?php echo '焦点区';?></a></li>
        <li><a href="index.php?act=web_config&op=sale_edit"><?php echo '促销区';?></a></li>
        <li><a href="index.php?act=web_config&op=famous"><?php echo '名师新作';?></a></li>
        <li><a href="JavaScript:void(0);" class="current"><?php echo '推荐活动';?></a></li>
      </ul>
    </div>
  </div>

  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
  </div>
  <form id="live_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label>推荐活动1(图片/链接)</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['rec_act_img'];?>"/><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.$output['list_setting']['rec_act_img'];?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="rec_act_img" type="file" class="type-file-file" id="rec_act_img" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield1' id='textfield1' class='type-file-text' />
            <input type='button' name='button1' id='button1' value='选择上传...' class='type-file-button' />
            </span></div>
          <label title="请输入图片要跳转的链接地址" class="ml5">
            <input class="input-txt ml5" type="text" name="rec_act_link1"  value="<?php echo $output['list_setting']['rec_act_link1']?>" placeholder="请输入图片要跳转的链接地址" />
          </label><span class="err"></span>
          <p class="notic">如需跳转请在后方添加以http://开头的链接地址。</p>
        </dd>
        <dt class="tit"><label>推荐活动1(名称)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="rec_act_name1" value="<?php echo $output['list_setting']['rec_act_name1'];?>" /><p><br/></p></dd>
      </dl>
      <!-- <dl class="row">
        <dt class="tit">
          <label>推荐活动2(链接)</label>
        </dt>
        <dd class="opt">
          <label title="请输入图片要跳转的链接地址" class="ml5">
            <input class="input-txt ml5" type="text" name="rec_act_link2"  value="<?php echo $output['list_setting']['rec_act_link2']?>" placeholder="请输入图片要跳转的链接地址" />
          </label><span class="err"></span>
          <p class="notic">如需跳转请在后方添加以http://开头的链接地址。</p>
        </dd>
        <dt class="tit"><label>推荐活动2(名称)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="rec_act_name2" value="<?php echo $output['list_setting']['rec_act_name2'];?>" /><p><br/></p></dd>
      </dl> -->
      <!--   <dl class="row">
        <dt class="tit">
          <label>推荐活动3(链接)</label>
        </dt>
        <dd class="opt">
          
          <label title="请输入图片要跳转的链接地址" class="ml5">
            <input class="input-txt ml5" type="text" name="rec_act_link3"  value="<?php echo $output['list_setting']['rec_act_link3']?>" placeholder="请输入图片要跳转的链接地址" />
          </label><span class="err"></span>
          <p class="notic">如需跳转请在后方添加以http://开头的链接地址。</p>
        </dd>
        <dt class="tit"><label>推荐活动3(名称)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="rec_act_name3" value="<?php echo $output['list_setting']['rec_act_name3'];?>" /><p><br/></p></dd>
      </dl> -->
      <dl class="row">
        <dt class="tit">
          <label>更多活动(链接)</label>
        </dt>
        <dd class="opt">
          <label title="请输入图片要跳转的链接地址" class="ml5">
            <input class="input-txt ml5" type="text" name="rec_act_link4"  value="<?php echo $output['list_setting']['rec_act_link4']?>" placeholder="请输入图片要跳转的链接地址" />
          </label><span class="err"></span>
          <p class="notic">如需跳转请在后方添加以http://开头的链接地址。</p>
        </dd>
        <!-- <dt class="tit"><label>推荐活动4(名称)</label></dt>
        <dd class="opt"><input class="input-txt ml5" type = "text" name="rec_act_name4" value="<?php echo $output['list_setting']['rec_act_name4'];?>" /><p><br/></p></dd> -->
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a> <!-- <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-red ml10" id="clearBtn">清空数据</a> --></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
//按钮先执行验证再提交表单
$(function(){
    // 图片js
    $("#rec_act_img").change(function(){$("#textfield1").val($("#rec_act_img").val());});
	$('.nyroModal').nyroModal();
    $('#live_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parents('dd').children('span.err');
            error_td.append(error);
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            rec_act_link1:{
                url : true
            },
            rec_act_link2:{
                url : true
            },
            rec_act_link3:{
                url : true
            },
             rec_act_link4:{
                url : true
            },
        },
        messages : {
            rec_act_link1:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
            rec_act_link2:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
            rec_act_link3:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
             rec_act_link4:{
                url : '<i class="fa fa-exclamation-circle"></i>链接地址格式不正确'
            },
        }
    });

    $("#submitBtn").click(function(){
        $("#live_form").submit();
    });
});
</script>