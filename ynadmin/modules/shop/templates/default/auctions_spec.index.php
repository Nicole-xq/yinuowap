<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>规格管理</h3>
        <h5>拍品规格的管理</h5>
      </div>
    </div>
  </div>
  <!-- <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li><?php echo $lang['type_index_prompts_one'];?></li>
    </ul>
  </div> -->
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=auctions_spec&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 350, sortable : false, align: 'center', className: 'handle'},
            {display: '规格ID', name : 'auctions_class_id', width : 40, sortable : true, align: 'center'},
            {display: '规格名称', name : 'auctions_class_name', width : 120, sortable : false, align: 'center'},
            {display: '一级分类', name : 'auctions_class_id_lv_1', width : 120, sortable : true, align: 'center'},
            {display: '二级分类', name : 'auctions_class_id_lv_2', width : 150, sortable : true, align: 'center'},
            ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增规格', name : 'add', bclass : 'add', title : '新增规格', onpress : fg_operation }
        ],
        searchitems : [
            {display: '规格名称', name : 'like_auctions_spec_name'},
            ],
        sortname: "auctions_spec_id",
        sortorder: "asc",
        title: '规格列表'
    });
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=auctions_spec&op=auctions_spec_add';
    }
}
function fg_del(id) {
    if(confirm('删除后将不能恢复，确认删除这项吗？')){
        $.getJSON('index.php?act=auctions_spec&op=auctions_spec_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
}
</script>