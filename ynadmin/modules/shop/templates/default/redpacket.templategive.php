<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <!-- 页面导航 -->
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>赠送红包</h3>
        <h5>通过ID向用户赠送红包</h5>
      </div>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <form id="add_form" method="post" >
    <input type="hidden" id="form_submit" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="rpt_t_id">红包ID</label>
        </dt>
        <dd class="opt">
          <input type="text" id="rpt_t_id" name="rpt_t_id" class="input-txt">
          <span class="err"></span>
          <p class="notic">请填写免费赠送红包的红包模板ID</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="member_id">会员ID</label>
        </dt>
        <dd class="opt">
          <input type="text" id="member_id" name="member_id" value="<?php echo $output['setting']['member_id'];?>" class="input-txt">
          <span class="err"></span>
          <p class="notic">请填写会员ID</p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#submitBtn").click(function(){
        $("#add_form").submit();
        });
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
           rpt_t_id: {
                required : true,
                digits : true
            },
            member_id: {
                required : true,
                digits : true
            }

        },
        messages : {
            rpt_t_id: {
                  required : '<i class="fa fa-exclamation-circle"></i>请填写红包模板ID',
                  digits : '<i class="fa fa-exclamation-circle"></i>红包模板ID应为数字'
                },
            member_id:{
                 required: '<i class="fa fa-exclamation-circle"></i>请填写用户ID',
                 digits : '<i class="fa fa-exclamation-circle"></i>用户ID应为数字'
            }
        }
    });
});
</script>