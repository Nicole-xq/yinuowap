<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
    .d_inline {
        display: inline;
    }
    .name {
        width: 50% !important;
    }

    .number1 {
        width: 200px !important;
    }
</style>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=store_vendue&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>艺术家拍卖管理 - 编辑商家“<?php echo $output['vendue_info']['store_name'];?>”的拍卖信息</h3>
            </div>
        </div>
    </div>
    <div class="homepage-focus" nctype="editStoreContent">
        <div class="title">
            <h3>编辑艺术家拍卖信息</h3>
        </div>

        <form id="edit_form" enctype="multipart/form-data" method="post" action="">
            <input type="hidden" name="form_submit" value="ok" />
            <input type="hidden" name="artist_vendue_id" value="<?php echo $output['vendue_info']['artist_vendue_id'];?>" />
            <input name="member_id" type="hidden" value="<?php echo $output['vendue_info']['member_id'];?>" />
            <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
                <thead>
                <tr>
                    <th colspan="20">艺术家拍卖基本信息</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="w150">艺术家拍卖名称：</th>
                    <td><?php echo $output['vendue_info']['artist_name'];?></td>
                </tr>
                <tr>
                    <th>店铺名称：</th>
                    <td><?php echo $output['vendue_info']['store_name'];?></td>
                </tr>
                <tr>
                    <th>艺术家职称：</th>
                    <td><?php echo $output['vendue_info']['artist_job_title'];?></td>
                </tr>
                <tr>
                    <th>个人简历：</th>
                    <td colspan="20"><?php echo $output['vendue_info']['artist_resume'];?></td>
                </tr>
                <tr>
                    <th>艺术家年代表：</th>
                    <td colspan="20"><?php foreach($output['vendue_info']['artist_represent'] as $k=>$v){?>
                            <div class="ncs-message-title"><span class="name"><?php if($v['repres_time'] != ''){?><?php echo $v['repres_time'];?>年<?php }?></span><span class="number1"><?php echo $v['repres_intro'];?></span></div>
                        <?php }?></td>
                </tr>
                <tr>
                    <th>获奖情况：</th>
                    <td><?php foreach($output['vendue_info']['artist_awards'] as $k=>$v){?>
                            <div class="ncs-message-title"><span class="name"><?php if($v['awards_time'] != ''){?><?php echo $v['awards_time'];?>年<?php }?></span><span class="number1"><?php echo $v['awards_intro'];?></span></div>
                        <?php }?></td>
                </tr>
                <tr>
                    <th>获奖作品：</th>
                    <td><?php foreach($output['vendue_info']['artist_works'] as $k=>$v){?>
                            <div class="ncs-message-title">
                                <div style="float:left;" class="name">
                                    <div style="float:left;"><a nctype="nyroModal"  href="<?php echo getVendueLogo($v['work_pic']);?>">
                                            <img src="<?php echo getVendueLogo($v['work_pic']);?>" alt="" /> </a></div>
                                    <div style="float:left;">
                                        <div class="number1" style="text-align:center;font-weight:bold;font-size:16px;"><?php echo $v['work_name'];?></div>
                                        <div style="text-align:center;font-weight:bold;font-size:16px;"><?php echo $v['work_intro'];?></div>
                                        <div><?php echo $v['work_time'];?></div>
                                    </div>
                                </div>

                            </div>
                        <?php }?></td>
                </tr>
                <tr>
                    <th>入驻时间：</th>
                    <td><?php echo date('Y-m-d',$output['vendue_info']['store_time']);?></td>
                </tr>
                <tr>
                    <th>拍卖保障金：</th>
                    <td>
                        <?php echo ncPriceFormat($output['store_info']['vendue_bail'])?>
                    </td>
                </tr>
                <tr>
                    <th>上传付款凭证：</th>
                    <td><img src="<?php echo getVendueLogo($output['vendue_info']['paying_money_certificate'])?>" />
                        <span></span></td>
                </tr>
                <tr>
                    <th>备注：</th>
                    <td><?php echo $output['vendue_info']['paying_money_certif_exp']?>
                        <span></span></td>
                </tr>
                <tr>
                    <th>是否支付保证金：</th>
                    <td><input type="radio" name="is_pay" value="1" checked="checked">是&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="is_pay" value="0" >否</td>
                </tr>


                </tbody>
            </table>

            <div><a id="submitBtn" class="ncap-btn-big ncap-btn-green" href="JavaScript:void(0);"><?php echo $lang['nc_submit'];?></a></div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    $(function(){
        //按钮先执行验证再提交表单
        $("#submitBtn").click(function(){
            $("#edit_form").submit();
        });
    });
</script>
