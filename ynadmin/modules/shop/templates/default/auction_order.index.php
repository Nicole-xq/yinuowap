<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>拍卖订单管理</h3>
        <h5>管理拍单订单收款和退款</h5>
      </div>
        <?php echo $output['top_link'];?>
    </div>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=auction_order&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: '订单编号', name : 'order_sn', width : 150, sortable : false, align: 'left'},
			{display: '订单来源', name : 'order_from', width : 50, sortable : false, align : 'center'},           
			{display: '下单时间', name : 'order_id', width : 140, sortable : true, align: 'left'},
			{display: '订单金额(元)', name : 'order_amount', width : 120, sortable : true, align: 'left'},
			{display: '订单状态', name : 'order_state', width: 60, sortable : false, align : 'center'},    
			{display: '支付方式', name : 'payment_code', width: 140, sortable : false, align : 'center'},
			{display: '支付单号', name : 'pay_sn', width: 60, sortable : false, align : 'center'},
			{display: '支付时间', name : 'payment_time', width: 140, sortable : true, align : 'left'},
			{display: '订单完成时间', name : 'finnshed_time', width: 120, sortable : true, align : 'left'},    
			{display: '商品ID', name : 'auction_id', width : 80, sortable : true, align: 'left'},
			{display: '商品', name : 'auction_name', width : 120, sortable : true, align: 'left'},
			{display: '商品库位号', name : 'warehouse_location', width : 80, sortable : true, align: 'left'},
            {display: '店铺ID', name : 'store_id', width : 80, sortable : false, align: 'center'},
			{display: '店铺名称', name : 'store_name', width : 100, sortable : false, align: 'left'}, 
			{display: '买家ID', name : 'buyer_id', width : 40, sortable : true, align: 'center'},
			{display: '买家账号', name : 'buyer_name', width : 150, sortable : false, align: 'left'}
            ],
        buttons : [
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出excel文件,如果不选中行，将导出列表所有数据', onpress : fg_operate }
        ],
        searchitems : [
            {display: '订单编号', name : 'order_sn', isdefault: true},
            {display: '买家账号', name : 'buyer_name'},
            {display: '店铺名称', name : 'store_name'},
            {display: '拍品名称', name : 'auction_name'}
            ],
        sortname: "order_id",
        sortorder: "desc",
        title: '拍卖订单明细'
    });
});
function fg_operate(name, grid) {
    if (name == 'csv') {
    	var itemlist = new Array();
        if($('.trSelected',grid).length>0){
            $('.trSelected',grid).each(function(){
            	itemlist.push($(this).attr('data-id'));
            });
        }
        fg_csv(itemlist);
    }
}
function fg_csv(ids) {
    id = ids.join(',');
    window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_step1&order_id=' + id;
}
function pay_time_48(id) {
	if(confirm('操作后将不能恢复，确认吗？')){
	$.ajax({
        type: "GET",
        dataType: "json",
        url: "index.php?act=auction_order&op=pay_time_48",
        data: "order_id="+id,
        success: function(data){
            if (data.state){
                $("#flexigrid").flexReload();
            } else {
            	alert(data.msg);
            }
        }
    });
    }
}
function pay_time_72(id) {
	if(confirm('操作后将不能恢复，确认吗？')){
	$.ajax({
        type: "GET",
        dataType: "json",
        url: "index.php?act=auction_order&op=pay_time_72",
        data: "order_id="+id,
        success: function(data){
            if (data.state){
                $("#flexigrid").flexReload();
            } else {
            	alert(data.msg);
            }
        }
    });
    }
}
</script> 
