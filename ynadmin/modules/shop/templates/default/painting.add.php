<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=painting&op=index" title="返回书画商品推荐列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>书画商品推荐 - <?php echo $_GET['tag_name'] ? '编辑“'.$output['rec_info']['tag_name'].'"下的推荐商品' : '新增书画推荐商品';?></h3>
                <h5>书画页面推荐商品管理</h5>
            </div>
        </div>
    </div>
    <form id="goods_form" method="post" action='index.php?act=painting&op=save'>
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default" id="explanation">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>模块名称</label>
                </dt>
                <dd class="opt">
                    <?php if ($_GET['paint_module_id']) { ?>
                        <?php echo $output['rec_info']['paint_module_name'];?>
                    <?php } else { ?>
                    <div id="module_name">
                    <select class="class-select">
                        <option value="0"><?php echo $lang['nc_please_choose'];?></option>
                        <?php if(!empty($output['paint_module_list'])){ ?>
                            <?php foreach($output['paint_module_list'] as $k => $v){ ?>
                                <option value="<?php echo $v['paint_module_id'];?>"><?php echo $v['paint_module_name'];?></option>
                            <?php } ?>
                        <?php } ?>
                        </select>
                        </div>
                    <?php }?>

                    <input type="hidden" name="paint_module_title" id="paint_module_title" value="<?php echo $_GET['paint_module_id'];?>"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>推荐标签</label>
                </dt>
                <dd class="opt">
                    <?php if ($_GET['paint_module_id']) { ?>
                        <?php echo $output['rec_info']['tag_name'];?>
                        <input type="hidden" name="tag_name"  value="<?php echo $output['rec_info']['tag_name'];?>" />
                    <?php }else{?>
                    <input type="text" name="tag_name" id="tag_name" value="<?php echo $output['rec_info']['tag_name'];?>" />
                    <span class="err"></span>
                    <p class="notic"></p>
                    <?php }?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>商品推荐</label>
                </dt>
                <dd class="opt">
                    <input type="text" placeholder="搜索商品名称" value="" name="goods_name" id="goods_name" maxlength="20" class="input-txt">
                    <a id="goods_search" href="JavaScript:void(0);" class="ncap-btn mr5"><?php echo $lang['nc_search'];?></a></dd>
            </dl>
            <dl class="row" id="selected_goods_list">
                <dt class="tit">已推荐商品</dt>
                <dd class="opt">
                    <input type="hidden" name="valid_recommend" id="valid_recommend" value="">
                    <span class="err"></span>
                    <ul class="dialog-goodslist-s1 goods-list scrollbar-box">
                    </ul>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">选择要推荐的商品</dt>
                <dd class="opt">
                    <div id="show_recommend_goods_list" class="show-recommend-goods-list scrollbar-box"></div>
                    <p class="notic">最多可推荐12个商品</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(){
            if ($('input[name="goods_id_list[]"]').size() == 0) {
                $('#valid_recommend').rules('add',{
                    required: true,
                    messages: {required : '<i class="fa fa-exclamation-circle"></i>请选择推荐商品'}
                });
            }else{
                $('#valid_recommend').rules('remove');
            }

            if($("#goods_form").valid()){
                $("#goods_form").submit();
            }
        });
        $('#goods_search').on('click',function(){
            $('#valid_recommend').rules('remove');
            if($("#goods_form").valid()){

                $('#show_recommend_goods_list').load('index.php?act=painting&op=get_goods_list&goods_name='+$('#goods_name').val());
            }
        });

        //表单验证
        $('#goods_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                tag_name : {
                    required : true
                },
                paint_module_title : {
                    required : function (){
                        if ($('input[name="goods_id_list[]"]').size() > 0){
                            return false
                        }else {
                            var _tmp = $('#module_name').children('select');
                            if (_tmp.val() == 0) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            messages : {
                tag_name : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写推荐标签'
                },
                paint_module_title : {
                    required : '<i class="fa fa-exclamation-circle"></i>请选择模块名称'
                }
            }
        });

    });

    function select_recommend_goods(auction_id) {
        if (typeof auction_id == 'object') {
            var auction_name = auction_id['auction_name'];
            var auction_pic = auction_id['auction_image'];
            var auction_id = auction_id['auction_id'];
        } else {
            var goods = $("#show_recommend_goods_list img[goods_id='"+auction_id+"']");
            var auction_pic = goods.attr("src");
            var auction_name = goods.attr("goods_name");
        }
        var obj = $("#selected_goods_list");
        if(obj.find("img[goods_id='"+auction_id+"']").size()>0) return;//避免重复
        if(obj.find("ul>li").size()>=12){
            alert('最多可推荐12个商品');
            return false;
        }
        var text_append = '';
        text_append += '<div onclick="del_recommend_goods(this,'+auction_id+');" class="goods-pic">';
        text_append += '<span class="ac-ico"></span>';
        text_append += '<span class="thumb size-72x72">';
        text_append += '<i></i>';
        text_append += '<img width="72" goods_id="'+auction_id+'" title="'+auction_name+'" goods_name="'+auction_name+'" src="'+auction_pic+'" />';
        text_append += '</span></div>';
        text_append += '<div class="goods-name">';
        text_append += '<a href="<?php echo C('auction_site_url')?>/index.php?act=auctions&id='+auction_id+'" target="_blank">';
        text_append += auction_name+'</a>';
        text_append += '</div>';
        text_append += '<input name="goods_id_list[]" value="'+auction_id+'" type="hidden">';
        obj.find("ul").append('<li>'+text_append+'</li>');
        <?php if (!$_GET['paint_module_id']) { ?>
        $('#paint_module_title').val($('#module_name').children('select').val());
        <?php } ?>

    }
    function del_recommend_goods(obj,auction_id) {
        $(obj).parent().remove();
    }

    var goods_list_json = $.parseJSON('<?php echo $output['goods_list_json'];?>');
    $.each(goods_list_json,function(k,v){
        select_recommend_goods(v);
    });

</script>
