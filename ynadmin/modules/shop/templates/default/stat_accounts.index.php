<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>进出账统计</h3>
                <h5></h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?=$lang['nc_prompts_title'];?>"><?=$lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?=$lang['nc_prompts_span']?>"></span> </div>
        <ul>
            <li>统计付款平台通过第三方进账，以及出账</li>
            <li>统计统计不展示(/角/分)单位。鼠标移动到金额上，可显示详细金额</li>
        </ul>
    </div>
    <div class="ncap-form-all ncap-stat-general-single">
        <div class="title"><h3>进出账总统计</h3></div>
        <dl class="row">
            <dd class="opt">
                <ul class="nc-row">
                    <li title="总进账金额：<?=$output['stat_money']['total_in']?>元">
                        <h4>总进账金额</h4>
                        <h2 id="count-number" class="timer" data-speed="1500" data-to="<?=$output['stat_money']['total_in']?>"></h2>
                        <h6>元</h6>
                    </li>
                    <li title="总出账金额：<?=$output['stat_money']['total_out']?>元">
                        <h4>总出账金额</h4>
                        <h2 id="count-number" class="timer" data-speed="1500" data-to="<?=$output['stat_money']['total_out']?>"></h2>
                        <h6>元</h6>
                    </li>
                    <li title="充值总进账金额：<?=$output['stat_money']['cash_in']?>元">
                        <h4>充值总进账金额</h4>
                        <h2 id="count-number" class="timer" data-speed="1500" data-to="<?=$output['stat_money']['cash_in']?>"></h2>
                        <h6>元</h6>
                    </li>
                    <li title="保证金总进账金额：<?=$output['stat_money']['margin_in']?>元">
                        <h4>保证金总进账金额</h4>
                        <h2 id="count-number" class="timer" data-speed="1500" data-to="<?=$output['stat_money']['margin_in']?>"></h2>
                        <h6>元</h6>
                    </li>
                    <li title="商品总进账金额：<?=$output['stat_money']['orders_in']?>元">
                        <h4>商品总进账金额</h4>
                        <h2 id="count-number" class="timer" data-speed="1500" data-to="<?=$output['stat_money']['orders_in']?>"></h2>
                        <h6>元</h6>
                    </li>
                </ul>
            </dd>
        </dl>
    </div>
    <div id="table_nav"><?php echo $output['top_link'];?></div>
    <div id="flexigrid"></div>
    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" name="formSearch" id="formSearch">
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dd>
                            <label>
                                <input type="text" value="<?= $_GET['keyword']?>" placeholder="请输入会员名称" name="keyword" class="s-input-txt">
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>创建日期筛选</dt>
                        <dd>
                            <label>
                                <input readonly id="query_start_date" placeholder="请选择起始时间" name=query_start_date value="<?= $_GET['query_start_date']?>" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="<?= $_GET['query_end_date']?>" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <span id="datetime_notice" style="color: red"></span>
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>第三方支付方式</dt>
                        <dd>
                            <label>
                                <select name="pay_type" class="s-select"><?= $_GET['query_end_date']?>
                                    <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                    <option value="1" <?php if($_GET['pay_type']==1){echo 'selected';}?> >微信支付</option>
                                    <option value="2" <?php if($_GET['pay_type']==2){echo 'selected';}?> >支付宝支付</option>
                                    <option value="3" <?php if($_GET['pay_type']==3){echo 'selected';}?> >拉卡拉</option>
                                    <option value="4" <?php if($_GET['pay_type']==4){echo 'selected';}?> >线下支付</option>
                                </select>
                            </label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom">
                <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a>
                <a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容">
                    <i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?>
                </a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript">
    var type = "<?= $_GET['op'] ?>";
    function update_flex(){
        var flexigrid_searchitems = [];
        flexigrid_searchitems['index'] = [
            {display: '单号', name : 'pdr_sn'},
            {display: '会员ID', name : 'pdr_member_id'},
            {display: '会员名称', name : 'pdr_member_name'}
        ];
        flexigrid_searchitems['in_accounts_margin'] = [
            {display: '单号', name : 'order_sn'},
            {display: '会员ID', name : 'buyer_id'},
            {display: '会员名称', name : 'buyer_name'}
        ];
        flexigrid_searchitems['in_accounts_order'] = [
            {display: '单号', name : 'order_sn'},
            {display: '会员ID', name : 'buyer_id'},
            {display: '会员名称', name : 'buyer_name'}
        ];
        flexigrid_searchitems['out_accounts'] = [
            {display: '单号', name : 'pdc_sn'},
            {display: '会员ID', name : 'pdc_member_id'},
            {display: '会员名称', name : 'pdc_member_name'}
        ];
        var flexigrid_colModel = [];
        flexigrid_colModel['index'] = [
            {display: '编号', name : 'pdr_id', width : 80, sortable : false, align: 'center'},
            {display: '订单号', name : 'pdr_sn', width : 150, sortable : false, align: 'center'},
            {display: '会员ID', name : 'pdr_member_id', width : 120, sortable : false, align: 'center'},
            {display: '会员名称', name : 'pdr_member_name', width : 120, sortable : false, align: 'center'},
            {display: '支付方式', name : 'pdr_payment_code', width : 120, sortable : false, align: 'center'},
            {display: '支付金额', name : 'pdr_amount', width : 120, sortable : false, align: 'center'},
            {display: '订单创建时间', name : 'pdr_add_time', width : 120, sortable : false, align: 'center'},
            {display: '支付完成时间', name : 'pdr_payment_time', width : 120, sortable : false, align: 'center'}
        ];
        flexigrid_colModel['in_accounts_margin'] = [
            {display: '编号', name : 'margin_id', width : 80, sortable : false, align: 'center'},
            {display: '订单号', name : 'order_sn', width : 150, sortable : false, align: 'center'},
            {display: '会员ID', name : 'buyer_id', width : 120, sortable : false, align: 'center'},
            {display: '会员名称', name : 'buyer_name', width : 120, sortable : false, align: 'center'},
            {display: '支付方式', name : 'payment_code', width : 120, sortable : false, align: 'center'},
            {display: '支付金额', name : 'api_pay_amount', width : 120, sortable : false, align: 'center'},
            {display: '订单创建时间', name : 'created_at', width : 120, sortable : false, align: 'center'},
            {display: '支付完成时间', name : 'payment_time', width : 120, sortable : false, align: 'center'}
        ];
        flexigrid_colModel['in_accounts_order'] = [
            {display: '编号', name : 'order_id', width : 80, sortable : false, align: 'center'},
            {display: '订单号', name : 'order_sn', width : 150, sortable : false, align: 'center'},
            {display: '会员ID', name : 'buyer_id', width : 120, sortable : false, align: 'center'},
            {display: '会员名称', name : 'buyer_name', width : 120, sortable : false, align: 'center'},
            {display: '支付方式', name : 'payment_code', width : 120, sortable : false, align: 'center'},
            {display: '支付金额', name : 'api_pay_amount', width : 120, sortable : false, align: 'center'},
            {display: '订单创建时间', name : 'api_pay_time', width : 120, sortable : false, align: 'center'},
            {display: '支付完成时间', name : 'payment_time', width : 120, sortable : false, align: 'center'}
        ];
        flexigrid_colModel['out_accounts'] = [
            {display: '编号', name : 'pdc_id', width : 80, sortable : false, align: 'center'},
            {display: '订单号', name : 'pdc_sn', width : 150, sortable : false, align: 'center'},
            {display: '会员ID', name : 'pdc_member_id', width : 120, sortable : false, align: 'center'},
            {display: '会员名称', name : 'pdc_member_name', width : 120, sortable : false, align: 'center'},
            {display: '提现方式', name : 'pdc_bank_name', width : 120, sortable : false, align: 'center'},
            {display: '提现金额', name : 'pdc_amount', width : 120, sortable : false, align: 'center'},
            {display: '申请时间', name : 'pdc_add_time', width : 120, sortable : false, align: 'center'},
            {display: '完成时间', name : 'pdc_payment_time', width : 120, sortable : false, align: 'center'}
        ];
        // 编辑导航
        $('#table_nav').load('index.php?act=stat_accounts&op=get_links&type=' + type ,
            function(){
                $('#table_nav a').not('.current').each(function () {
                    $(this).attr('href',$(this).attr('href')+'&'+$("#formSearch").serialize());
                });
            });
        $('.ncap-stat-general-single').load('index.php?act=stat_accounts&op=get_plat_income&' + '&' + $("#formSearch").serialize(),
            function(){
                $('.timer').each(count);
            });
        $("#flexigrid").flexigrid({
            rp : 10,
            url: 'index.php?act=stat_accounts&op=get_xml&type=' + type + '&' + $("#formSearch").serialize(),
            colModel : flexigrid_colModel[type],
            searchitems : flexigrid_searchitems[type],
            title: '统计列表'
        });
    }

    $(function () {
        //绑定时间控件
        $('#query_start_date').datepicker();
        $('#query_end_date').datepicker();

        update_flex();
        $('#ncsubmit').click(function(){
            $('.flexigrid').after('<div id="flexigrid"></div>').remove();
            update_flex();
        });

        // 高级搜索重置
        $('#ncreset').click(function(){
            $('#formSearch')[0].reset();
            $('.flexigrid').after('<div id="flexigrid"></div>').remove();
            update_flex();
        });

        $('#searchBarOpen').click();
    });
</script>