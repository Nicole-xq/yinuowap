<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=paint_module&op=index" title="返回模块名称列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>书画模块名称管理 - 编辑模块名称“<?php echo $output['paint_module_array']['paint_module_name']?>”</h3>
            </div>
        </div>
    </div>
    <form id="module_form" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="paint_module_id" value="<?php echo $output['paint_module_array']['paint_module_id']?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>模块名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['paint_module_array']['paint_module_name']?>" name="paint_module_name" id="paint_module_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label>显示</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="paint_module_show1" class="cb-enable <?php if($output['paint_module_array']['paint_module_show'] == '1'){ ?>selected<?php } ?>" ><?php echo $lang['nc_yes'];?></label>
                        <label for="paint_module_show0" class="cb-disable <?php if($output['paint_module_array']['paint_module_show'] == '0'){ ?>selected<?php } ?>" ><?php echo $lang['nc_no'];?></label>
                        <input id="paint_module_show1" name="paint_module_show" <?php if($output['paint_module_array']['paint_module_show'] == '1'){ ?>checked="checked"<?php } ?>  value="1" type="radio">
                        <input id="paint_module_show0" name="paint_module_show" <?php if($output['paint_module_array']['paint_module_show'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">排序</dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['paint_module_array']['paint_module_sort']?>" name="paint_module_sort" id="paint_module_sort" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">数字范围为0~255，数字越小越靠前</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script>

    $(function(){
        $("#submitBtn").click(function(){
            if($("#module_form").valid()){
                $("#module_form").submit();
            }
        });

        $("#module_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                paint_module_name : {
                    required : true,
                    remote   : {
                        url :'index.php?act=paint_module&op=ajax&branch=check_module_name',
                        type:'get',
                        data:{
                            paint_module_name : function(){
                                return $('#paint_module_name').val();
                            },
                            id  : '<?php echo $output['paint_module_array']['paint_module_id']?>'
                        }
                    }
                },

                paint_module_sort : {
                    required : true,
                    number   : true
                }
            },
            messages : {
                paint_module_name : {
                    required : '<i class="fa fa-exclamation-circle"></i>模块名称不能为空',
                    remote   : '<i class="fa fa-exclamation-circle"></i>模块名称已经存在'
                },

                paint_module_sort  : {
                    required : '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    number   : '<i class="fa fa-exclamation-circle"></i>排序仅可以为数字'
                }
            }
        });
    });

</script>
