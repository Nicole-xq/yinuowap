<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>拍品保证金</h3>
                <h5>商城拍卖保证金订单管理</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>点击查看操作将显示订单（包括订单物品）的详细信息</li>
            <li>点击取消操作可以取消订单（在线支付但未付款的订单）</li>
            <li>如果平台已确认收到买家的付款，但系统支付状态并未变更，可以点击收到货款操作(仅限于下单后7日内可更改收款状态)，并填写相关信息后更改订单支付状态</li>
        </ul>
    </div>
    <div id="flexigrid"></div>

</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=member_rate_config&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'left', className: 'handle'},
                {display: '标题', name : 'order_sn', width : 170, sortable : false, align: 'left'},
                {display: '竞价返佣', name : 'bid_rate', width : 300, sortable : true, align : 'center'},
                {display: '成交返佣', name : 'deal_rate', width : 300, sortable : true, align: 'left'},

            ],
            buttons : [
//                {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出excel文件,如果不选中行，将导出列表所有数据', onpress : fg_operate }
            ],
//            searchitems : [
//                {display: '订单编号', name : 'order_sn', isdefault: true},
//                {display: '买家账号', name : 'buyer_name'},
//                {display: '店铺名称', name : 'store_name'}
//            ],
//            query: <?php //if ($_GET['order_sn']) { echo $_GET['order_sn']; ?><!-- --><?php //} else {?>// '' <?php //}?>//,
            sortname: "id",
            sortorder: "desc",
            title: '返佣奖励配置'
        });
    });
    function fg_operate(name, grid) {
        if (name == 'csv') {
            var itemlist = new Array();
            if($('.trSelected',grid).length>0){
                $('.trSelected',grid).each(function(){
                    itemlist.push($(this).attr('data-id'));
                });
            }
            fg_csv(itemlist);
        }
    }

</script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/orders/financial_verification.js"></script>