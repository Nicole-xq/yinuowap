<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="javascript:history.back(-1)" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>拍卖订单 - 查看退单“退单编号：<?php echo $output['order_info']['auction_order_sn']; ?>”</h3>
                <h5>拍卖订单退款详细</h5>
            </div>
        </div>
    </div>
    <div class="ncap-form-default">
        <div class="title">
            <h3>买家退款申请</h3>
        </div>
        <dl class="row">
            <dt class="tit">申请时间</dt>
            <dd class="opt"><?php echo date('Y-m-d H:i:s',$output['order_info']['buyer_refund_time']); ?> </dd>
        </dl>
        <dl class="row">
            <dt class="tit">商品名称</dt>
            <dd class="opt">
                <?php if ($output['order_info']['auction_id'] > 0) { ?>
                    <a href="<?php echo urlAuction('auctions','index',array('id'=> $output['order_info']['auction_id']));?>" target="_blank"><?php echo $output['order_info']['auction_name']; ?></a>
                <?php }else { ?>
                    <?php echo $output['order_info']['auction_name']; ?>
                <?php } ?>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">退款金额</dt>
            <dd class="opt"><?php echo ncPriceFormat($output['order_info']['refund_amount']); ?> </dd>
        </dl>

        <div class="title">
            <h3>商家退款处理</h3>
        </div>
        <dl class="row">
            <dt class="tit">审核结果</dt>
            <dd class="opt">同意退款</dd>
        </dl>

        <dl class="row">
            <dt class="tit">处理时间</dt>
            <dd class="opt"><?php echo $output['order_info']['seller_time'] ? date('Y-m-d H:i:s',$output['order_info']['seller_time']) : null; ?> </dd>
        </dl>
        <?php if ($output['order_info']['refund_state'] == 2) { ?>
            <div class="title">
                <h3>平台退款审核</h3>
            </div>
            <dl class="row">
                <dt class="tit">平台确认</dt>
                <dd class="opt">已完成</dd>
            </dl>
            <dl class="row">
                <dt class="tit">平台备注</dt>
                <dd class="opt"><?php echo $output['order_info']['admin_message'] ?></dd>
            </dl>
            <dl class="row">
                <dt class="tit">处理时间</dt>
                <dd class="opt"><?php echo $output['order_info']['finnshed_time'] ? date('Y-m-d H:i:s',$output['order_info']['finnshed_time']) : null; ?> </dd>
            </dl>
            <?php if ($output['order_info']['refund_state'] == 2) { ?>
                <div class="title">
                    <h3>退款详细</h3>
                </div>
                <dl class="row">
                    <dt class="tit">支付方式</dt>
                    <dd class="opt"><?php echo orderPaymentName($output['order_info']['payment_code']);?></dd>
                </dl>
                <dl class="row">
                    <dt class="tit">在线退款金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['api_refund_amount']); ?> </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">保证金退款金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['margin_amount_refund']); ?> </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">预存款退款金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['pd_amount_refund']); ?> </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">充值卡退款金额</dt>
                    <dd class="opt"><?php echo ncPriceFormat($output['order_info']['rcb_amount_refund']); ?> </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">退诺币数：</dt>
                    <dd class="opt"><?php echo $output['order_info']['points_amount_refund']; ?> </dd>
                </dl>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    $(function(){
        $('.nyroModal').nyroModal();
    });
</script>