<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['member_index_manage']?></h3>
        <h5><?php echo $lang['member_shop_manage_subhead']?></h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li><?php echo $lang['member_index_help1'];?></li>
      <li><?php echo $lang['member_index_help2'];?></li>
    </ul>
  </div>
  <div id="flexigrid"></div>
  <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
  <div class="ncap-search-bar">
    <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
    <div class="title">
      <h3>高级搜索</h3>
    </div>
    <form method="get" name="formSearch" id="formSearch">
      <div id="searchCon" class="content">
        <div class="layout-box">
          <dl>
            <dt>日期筛选</dt>
            <dd>
              <label>
                <select class="s-select" name="qtype_time">
                  <option value="member_time" selected="selected">注册时间</option>
                </select>
              </label>
              <label>
                <input readonly id="query_start_date" placeholder="请选择起始时间" name=query_start_date value="" type="text" class="s-input-txt" />
              </label>
              <label>
                <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="" type="text" class="s-input-txt" />
              </label>
            </dd>
          </dl>
            <dl>
                <dt>注册来源</dt>
                <dd>
                    <label>
                        <input  name="registered_source" value="" type="text" class="s-input-txt" />
                    </label>
                </dd>
            </dl>
        </div>
      </div>
      <div class="bottom"> <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a><a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容"><i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?></a></div>
    </form>
  </div>
</div>
<script type="text/javascript">
$(function(){
    $('#query_start_date').datepicker();
    $('#query_end_date').datepicker();

    // 高级搜索提交
    $('#ncsubmit').click(function(){
        $("#flexigrid").flexOptions({url: 'index.php?act=member&op=get_xml&'+$("#formSearch").serialize(),query:'',qtype:''}).flexReload();
    });

    // 高级搜索重置
    $('#ncreset').click(function(){
        $("#flexigrid").flexOptions({url: 'index.php?act=member&op=get_xml'}).flexReload();
        $("#formSearch")[0].reset();
    });


    $("#flexigrid").flexigrid({
        url: 'index.php?act=member&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 60, sortable : false, align: 'center', className: 'handle-s'},
            {display: '会员ID', name : 'member_id', width : 50, sortable : true, align: 'center'},
            {display: '会员名称', name : 'member_name', width : 150, sortable : true, align: 'left'},
            {display: '会员等级', name : 'member_type', width : 120, sortable : true, align: 'center'},
            {display: '会员角色', name : 'member_role', width : 60, sortable : true, align: 'center'},
            {display: '会员邮箱', name : 'member_email', width : 150, sortable : true, align: 'left'},
            {display: '会员手机', name : 'member_mobile', width : 100, sortable : true, align: 'center'},
            {display: '会员性别', name : 'member_sex', width : 50, sortable : true, align: 'center'},
            {display: '会员昵称', name : 'member_truename', width : 80, sortable : true, align: 'left'},
            {display: '上级会员名称', name : 'top_member_name', width : 150, sortable : false, align: 'left'},
            {display: '邀请码', name : 'dis_code', width : 80, sortable : false, align: 'center'},
            {display: '一级好友人数', name : 'member_distribute_num', width : 80, sortable : false, align: 'center'},
            {display: '注册时间', name : 'member_time', width : 140, sortable : true, align: 'center'},
            {display: '注册来源', name : 'registered_source', width : 140, sortable : true, align: 'center'},
            {display: '最后登录时间', name : 'member_login_time', width : 140, sortable : true, align: 'center'},
            {display: '最后登录IP', name : 'member_login_ip', width : 100, sortable : true, align: 'center'},
            {display: '会员诺币', name : 'member_points', width : 80, sortable : true, align: 'center'},
            {display: '会员经验', name : 'member_exppoints', width : 60, sortable : true, align: 'center'},
            {display: '会员等级', name : 'member_grade', width : 60, sortable : false, align: 'center'},
            {display: '可用预存款(元)', name : 'available_predeposit', width : 90, sortable : true, align: 'center', className: 'normal'},
            {display: '冻结预存款(元)', name : 'freeze_predeposit', width : 90, sortable : true, align: 'center', className: 'abnormal'},
            {display: '可用充值卡(元)', name : 'available_rc_balance', width : 90, sortable : true, align: 'center', className: 'normal'},
            {display: '冻结充值卡(元)', name : 'freeze_rc_balance', width : 90, sortable : true, align: 'center', className: 'abnormal'},
            {display: '允许举报', name : 'inform_allow', width : 50, sortable : true, align: 'center'},
            {display: '允许购买', name : 'is_buy', width : 50, sortable : true, align: 'center'},
            {display: '允许咨询', name : 'is_allowtalk', width : 50, sortable : true, align: 'center'}
            ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation },
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出CVS文件', onpress : fg_operation },
            {display: '<i class="fa fa-plus"></i>群发消息', name : 'msg_add', bclass : 'msg_add', title : '消息群发', onpress : send_msg }


            ],
        searchitems : [
            {display: '会员ID', name : 'member_id'},
            {display: '会员名称', name : 'member_name'},
            {display: '会员手机', name : 'member_mobile'}
            ],
        sortname: "member_id",
        sortorder: "desc",
        title: '商城会员列表'
    });
	
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=member&op=member_add';
    }
    if (name == 'csv') {
        if ($('.trSelected', bDiv).length == 0) {
            if (!confirm('您确定要下载全部数据吗？')) {
                return false;
            }
        }
        var itemids = new Array();
        $('.trSelected', bDiv).each(function(i){
            itemids[i] = $(this).attr('data-id');
        });
        fg_csv(itemids);
    }
}

function fg_csv(ids) {
    id = ids.join(',');
    window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_csv&id=' + id;
}

function send_msg(name, bDiv){
    var itemids = new Array();
    $('.trSelected', bDiv).each(function(i){
        itemids[i] = $(this).attr('data-id');
    });
    id = itemids.join(',');

    _uri = $("#flexigrid").flexSimpleSearchQueryString()+'&op=to_send&id=' + id;
    CUR_DIALOG = ajax_form('top_member_award', '', _uri, 640);
}
</script> 

