<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
.ncm-goods-gift {
	text-align: left;
}
.ncm-goods-gift ul {
    display: inline-block;
    font-size: 0;
    vertical-align: middle;
}
.ncm-goods-gift li {
    display: inline-block;
    letter-spacing: normal;
    margin-right: 4px;
    vertical-align: top;
    word-spacing: normal;
}
.ncm-goods-gift li a {
    background-color: #fff;
    display: table-cell;
    height: 30px;
    line-height: 0;
    overflow: hidden;
    text-align: center;
    vertical-align: middle;
    width: 30px;
}
.ncm-goods-gift li a img {
    max-height: 30px;
    max-width: 30px;
}
</style>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="javascript:history.back(-1)" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>拍卖订单管理</h3>
      </div>
    </div>
  </div>
  <div class="ncap-order-style">
    <div class="titile">
      <h3></h3>
    </div>
<div class="ncap-order-flow">

      <ol class="num5">
        <li class="current">
          <h5>生成订单</h5>
          <i class="fa fa-arrow-circle-right"></i>
          <time><?php echo date('Y-m-d H:i:s',$output['order_info']['add_time']);?></time>
        </li>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_CANCEL) { ?>
        <li class="current">
          <h5>取消订单</h5>
          <time></time>
        </li>
        <?php } else { ?>
        <li class="<?php if(intval($output['order_info']['payment_time']) && $output['order_info']['order_state'] == ORDER_STATE_PAY) echo 'current'; ?>">
          <h5>完成付款</h5>
          <i class="fa fa-arrow-circle-right"></i>
          <time><?php echo intval(date('His',$output['order_info']['payment_time'])) ? date('Y-m-d H:i:s',$output['order_info']['payment_time']) : date('Y-m-d',$output['order_info']['payment_time']);?></time>
        </li>
        <li class="<?php if($output['order_info']['order_state'] == ORDER_STATE_SEND) echo 'current'; ?>">
          <h5>商家发货</h5>
          <i class="fa fa-arrow-circle-right"></i>
          <time><?php echo $output['order_info']['finnshed_time'] ? date('Y-m-d H:i:s',$output['order_info']['finnshed_time']) : null; ?></time>
        </li>
        <li class="<?php if(intval($output['order_info']['order_state'] == ORDER_STATE_SUCCESS)) { ?>current<?php } ?>">
          <h5>收货确认</h5>
          <time><?php echo $output['order_info']['finnshed_time'] ? date('Y-m-d H:i:s',$output['order_info']['finnshed_time']) : null;?></time>
        </li>
        <?php } ?>
    </ol>
    </div>

    <div class="ncap-order-details">
      <div class="tabs-panels">
        <div class="misc-info">
          <h4>下单/支付</h4>
          <dl>
            <dt><?php echo $lang['order_number'];?><?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['order_sn'];?></dd>
            <dt>订单来源<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo str_replace(array(1,2), array('PC端','移动端'), $output['order_info']['order_from']);?></dd>
            <dt><?php echo $lang['order_time'];?><?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo date('Y-m-d H:i:s',$output['order_info']['add_time']);?></dd>
          </dl>
          <dl>
            <dt><?php echo $lang['payment'];?><?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo orderPaymentName($output['order_info']['payment_code']);?></dd>
            <dt><?php echo $lang['payment_time'];?><?php echo $lang['nc_colon'];?></dt>
            <dd>
                <?php if(intval($output['order_info']['payment_time'])){?>
                <?php echo intval(date('His',$output['order_info']['payment_time'])) ? date('Y-m-d H:i:s',$output['order_info']['payment_time']) : date('Y-m-d',$output['order_info']['payment_time']);?>
                <?php }?>
                </dd>
          </dl>
          <?php if ($output['order_info']['order_state'] == ORDER_STATE_CANCEL) { ?>
          <dl>
            <dt>订单取消原因：</dt>
            <dd><?php echo $output['order_info']['close_info'];?></dd>
          </dl>
          <?php }?>
          <?php if ($output['order_info']['pay_voucher']) { ?>
          <dl>
            <dt>支付凭证：</dt>
            <dd><img src="<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_voucher'.DS.$output['order_info']['pay_voucher'];?>"/></dd>
          </dl>
          <?php }?>
        </div>
        <div class="addr-note">
          <h4>购买/收货方信息</h4>
          <dl>
            <dt><?php echo $lang['buyer_name'];?><?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['buyer_name'];?></dd>
            <dt>联系方式<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['buyer_address']['phone'];?></dd>
          </dl>
          <dl>
            <dt>收货人姓名<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['reciver_name'];?></dd>
            <dt>收货地址<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['buyer_address']['address'];?></dd>
          </dl>
        </div>

        <div class="contact-info">
          <h4>发货信息</h4>
          <dl>
            <dt><?php echo $lang['store_name'];?><?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['store_name'];?></dd><dt>店主名称<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['store_info']['seller_name'];?></dd>
            <dt>联系电话<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['store_info']['store_phone'];?></dd>
          </dl>
          <dl>
            <dt>发货时间<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['ship_time'] ? date('Y-m-d H:i:s',$output['order_info']['ship_time']) : null; ?></dd>
            <dt>快递公司<?php echo $lang['nc_colon'];?></dt>
            <dd><?php echo $output['order_info']['express_info']['e_name'];?></dd>
            <dt>物流单号<?php echo $lang['nc_colon'];?></dt>
            <dd>
              <?php if($output['order_info']['shipping_code'] != ''){?>
              <?php echo $output['order_info']['shipping_code'];?>
              <?php }?>
            </dd>
          </dl>
        </div>
        <div class="goods-info">
          <h4><?php echo $lang['product_info'];?></h4>
          <table>
            <thead>
              <tr>
                <th colspan="2">商品</th>
                  <th>库位号</th>
                  <th>价格</th>
                  <th>佣金比例</th>
                <th>收取佣金</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="w30">
                    <div class="goods-thumb">
                        <a href="#">
                            <img alt="<?php echo $lang['product_pic'];?>" src="<?php echo cthumb($output['auction_info']['auction_image'], 60, $value['store_id']);?>" />
                        </a>
                    </div>
                </td>
                <td style="text-align: center;"><?php echo $output['auction_info']['auction_name'];?></td>
                <td style="text-align: center;"><?php echo $output['auction_info']['warehouse_location'];?></td>
                <td class="w80"><?php echo $lang['currency'].ncPriceFormat($output['order_info']['order_amount']);?></td>
                <td class="w60"><?php echo $output['auction_info']['commis_rate'].'%';?></td>
                <td class="w80"><?php echo ncPriceFormat($output['order_info']['order_amount']*$output['order_info']['commis_rate']/100);?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="total-amount">
          <h3><?php echo $lang['order_total_price'];?><?php echo $lang['nc_colon'];?><strong class="red_common"><?php echo $lang['currency'].ncPriceFormat($output['order_info']['order_amount']);?></strong></h3>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".tabs-nav > li > a").mousemove(function(e) {
            if (e.target == this) {
                var tabs = $(this).parent().parent().children("li");
                var panels = $(this).parents('.ncap-order-details:first').children(".tabs-panels");
                var index = $.inArray(this, $(this).parents('ul').find("a"));
                if (panels.eq(index)[0]) {
                    tabs.removeClass("current").eq(index).addClass("current");
                   panels.addClass("tabs-hide").eq(index).removeClass("tabs-hide");
                }
            }
        });
    });
</script>
