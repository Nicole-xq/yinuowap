<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=goods_class&op=goods_class&gc_id=<?php echo $output['parent_id']?>" title="返回上级分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3> <?php echo $output['title']?></h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <form method='post'>
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="submit_type" id="submit_type" value="" />
    <table class="flex-table">
      <thead>
        <tr>
          <th width="24" align="center" class="sign"><i class="ico-check"></i></th>
          <th width="200" class="handle" align="center"><?php echo $lang['nc_handle'];?></th>
          <th width="200" align="center">属性名称</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['list']) && is_array($output['list'])){ ?>
        <?php foreach($output['list'] as $k => $v){ ?>
        <tr data-id="<?php echo $v['auctions_spec_class_id'];?>">
          <td width="24" class="sign"><i class="ico-check"></i></td>
          <td width="200" class="handle">
            <a class="btn red" href="javascript:void(0);" onclick="fg_del(<?php echo $v['auctions_spec_class_id'];?>);"><i class="fa fa-trash-o"></i><?php echo $lang['nc_del'];?></a>
          </td>
          <td width="200" class="auctions_spec_class_name"><span title="<?php echo $lang['nc_editable'];?>" column_id="<?php echo $v['auctions_spec_class_id'];?>" fieldname="auctions_spec_class_name" nc_type="inline_edit" class="editable "><?php echo $v['auctions_spec_class_name'];?></span></td>          
        </tr>
        <?php } ?>
        <?php }else { ?>
        <tr>
          <td class="no-data" colspan="100"><i class="fa fa-exclamation-circle"></i><?php echo $lang['nc_no_record'];?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
    $('.flex-table').flexigrid({
        height:'auto',// 高度自动
        usepager: false,// 不翻页
        striped:false,// 不使用斑马线
        resizable: false,// 不调节大小
        title: '分类<?php echo $output['title']?>',// 表格标题
        reload: false,// 不使用刷新
        columnControl: false,// 不使用列控制
        buttons : [
                   {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', onpress : fg_operation },
               ]
    });
    $('span[nc_type="inline_edit"]').inline_edit({act: 'auctions_spec',op: 'ajax'});
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=auctions_spec&op=auctions_spec_class_add&auctions_spec_id=<?php echo $output['id']?>';
    }
    if (name == 'del') {
        if ($('.trSelected', bDiv).length == 0) {
            showError('请选择要操作的数据项！');
        }
        var itemids = new Array();
        $('.trSelected', bDiv).each(function(i){
            itemids[i] = $(this).attr('data-id');
        });
        fg_del(itemids);
    }
}
function fg_del(ids) {
    if (typeof ids == 'number') {
        var ids = new Array(ids.toString());
    };
    id = ids.join(',');
    if(confirm('删除后将不能恢复，确认删除这项吗？')){
        $.getJSON('index.php?act=auctions_spec&op=auctions_spec_class_del', {id:id}, function(data){
            if (data.state) {
                location.reload();
            } else {
                showError(data.msg)
            }
        });
    }
}
</script>