<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>标签管理</h3>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=painting_tag&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '标签ID', name : 'paint_tag_id', width : 40, sortable : true, align: 'center'},
                {display: '标签名称', name : 'paint_tag_name', width : 150, sortable : false, align: 'left'},
                {display: '标签排序', name : 'paint_tag_sort', width: 60, sortable : true, align : 'center'}

            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '添加一条新数据到列表', onpress : fg_operation }
            ],
            searchitems : [
                {display: '标签ID', name : 'paint_tag_id', isdefault: true},
                {display: '标签名称', name : 'paint_tag_name'}
            ],
            sortname: "paint_tag_id",
            sortorder: "desc",
            title: '标签列表'
        });
    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=painting_tag&op=tag_add';
        }

    }


    //删除
    function fg_del(id) {
        if(!confirm('删除后将不能恢复，确认删除这项吗？')){
            return false;
        }
        $.getJSON('index.php?act=painting_tag&op=tag_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
</script>
