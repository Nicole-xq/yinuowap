<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>保证金分析</h3>
        <h5>平台针对保证金订单的各项数据统计</h5>
      </div>
      <?php echo $output['top_link'];?> </div>
  </div>
  <div class="ncap-form-all ncap-stat-general-single">
    <div class="title">
      <h3>保证金订单情况一览</h3>
    </div>
    <dl class="row">
      <dd class="opt">
        <ul class="nc-row">
          <li title="收款金额：<?php echo number_format($output['plat_data']['oot'],2); ?>元">
            <h4>收款金额</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['oot'],2); ?>"></h2>
            <h6>元</h6>
          </li>
          <li title="退款金额：<?php echo number_format($output['plat_data']['oort'],2); ?>元">
            <h4>退款金额</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['oort'],2); ?>"></h2>
            <h6>元</h6>
          </li>
          <li title="实收金额：<?php echo number_format($output['plat_data']['oot']-$output['plat_data']['oort'],2); ?>元">
            <h4>实收金额</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['oot']-$output['plat_data']['oort'],2); ?>"></h2>
            <h6>元</h6>
          </li>
          <li title="佣金总额：<?php echo number_format($output['plat_data']['oct'],2); ?>元">
            <h4>佣金总额</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['oct'],2); ?>"></h2>
            <h6>元</h6>
          </li>
           <li title="店铺费用：<?php echo number_format($output['plat_data']['osct'],2); ?>元">
            <h4>佣金总额</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['osct'],2); ?>"></h2>
            <h6>元</h6>
          </li>
          <li title="总收入：<?php echo number_format($output['plat_data']['ort'],2); ?>元">
            <h4>总收入</h4>
            <h2 id="count-number" class="timer" data-speed="1500" data-to="<?php echo number_format($output['plat_data']['ort'],2); ?>"></h2>
            <h6>元</h6>
          </li>
        </ul>
      </dd>
    </dl>
  </div>
  <div id="flexigrid"></div>
  <?php if($output['data_type'] == 'other_data'){?>
  <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
  <div class="ncap-search-bar">
    <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
    <div class="title">
      <h3>高级搜索</h3>
    </div>
    <form method="get" action="index.php" name="formSearch" id="formSearch">
      <div id="searchCon" class="content">
        <div class="layout-box">
            <dl>
                <dt>日期筛选</dt>
                <dd>
                    <label>
                        <input readonly id="query_start_date" placeholder="请选择起始时间" name="query_start_date" value="" type="text" class="s-input-txt" />
                    </label>
                    <label>
                        <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="" type="text" class="s-input-txt" />
                    </label>
                </dd>
            </dl>
        </div>
      </div>
      <div class="bottom"> <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a><a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容"><i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?></a></div>
    </form>
  </div>
  <?php }?>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/statistics.js"></script>
<script>
function update_flex(){
	$('.ncap-stat-general-single').load("index.php?act=margin_order_data&op=get_top_data&type=<?php echo $output['data_type'];?>&"+$("#formSearch").serialize(),
		function(){
			$('.timer').each(count);
     	});

    $("#flexigrid").flexigrid({
        url: 'index.php?act=margin_order_data&op=get_xml&type=<?php echo $output['data_type'];?>&'+$("#formSearch").serialize(),
        colModel : [
            {display: '订单编号', name : 'order_sn', width : 200, sortable : false, align: 'left'},
            {display: '订单来源', name : 'order_from', width : 50, sortable : true, align : 'center'},
            {display: '下单时间', name : 'created_at', width : 140, sortable : true, align: 'left'},
            {display: '订单金额(元)', name : 'margin_amount', width : 140, sortable : true, align: 'left'},
            {display: '订单状态', name : 'order_state', width: 60, sortable : true, align : 'center'},
            {display: '支付方式', name : 'payment_code', width: 130, sortable : true, align : 'center'},
            {display: '支付时间', name : 'payment_time', width: 140, sortable : true, align : 'left'},
            {display: '充值卡支付(元)', name : 'rcb_amount', width : 100, sortable : true, align: 'center'},
            {display: '预存款支付(元)', name : 'pd_amount', width : 100, sortable : true, align: 'center'},
            {display: '诺币支付(元)', name : 'points_amount', width : 120, sortable : false, align : 'left'},
            {display: '保证金余额支付(元)', name : 'account_margin_amount', width : 120, sortable : false, align : 'left'},
            {display: '订单完成时间', name : 'finnshed_time', width: 140, sortable : true, align : 'left'},
            {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
            {display: '店铺名称', name : 'store_name', width : 200, sortable : true, align: 'left'},
            {display: '商品名称', name : 'goods_name', width : 200, sortable : true, align: 'left'},
            {display: '买家ID', name : 'buyer_id', width : 60, sortable : true, align: 'center'},
            {display: '买家名称', name : 'buyer_name', width : 150, sortable : true, align: 'left'}
            ],
//        buttons : [
//            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'excel', bclass : 'csv', title : '导出EXCEL文件', onpress : fg_operation }
//        ],
        sortname: "ob_id",
        sortorder: "desc",
        usepager: true,
        rp: 15,
        title: '保证金订单明细列表'
    });
}
$(function () {
    //绑定时间控件
    $('#query_start_date').datepicker();
    $('#query_end_date').datepicker();

	update_flex();
	$('#ncsubmit').click(function(){
	    $('.flexigrid').after('<div id="flexigrid"></div>').remove();
	    update_flex();
    });

    // 高级搜索重置
    $('#ncreset').click(function(){
        $('.flexigrid').after('<div id="flexigrid"></div>').remove();
        update_flex();
    });

	$('#searchBarOpen').click();
});
function fg_operation(name, bDiv){
    var stat_url = 'index.php?act=stat_trade&op=income';
    get_search_excel(stat_url,bDiv);
}
</script>