<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>合作商家管理</h3>
                <h5></h5>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=cooper_store&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '合作商家ID', name : 'cooper_store_id', width : 80, sortable : true, align: 'center'},
                {display: '合作商家图片', name : 'cooper_store_pic', width : 120, sortable : false, align: 'left'},
                {display: '合作商家链接', name : 'cooper_store_url', width : 300, sortable : false, align: 'left'},
                {display: '合作商家排序', name : 'cooper_store_sort', width: 100, sortable : true, align : 'center'},
                {display: '是否显示', name : 'is_show', width: 60, sortable : true, align : 'center'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '添加一条新数据到列表', onpress : fg_operation }
            ],
            searchitems : [
                {display: '合作商家ID', name : 'cooper_store_id', isdefault: true}
            ],
            sortname: "cooper_store_id",
            sortorder: "desc",
            title: '合作商家列表'
        });
    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=cooper_store&op=cooper_store_add';
        }

    }


    //删除
    function fg_del(id) {
        if(!confirm('删除后将不能恢复，确认删除这项吗？')){
            return false;
        }
        $.getJSON('index.php?act=cooper_store&op=cooper_store_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
</script>
