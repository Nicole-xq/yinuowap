<?php defined('InShopNC') or exit('Access Invalid!');?>

    <div class="page">
        <div class="fixed-bar">
            <div class="item-title">
                <div class="subject">
                    <h3>拍品管理</h3>
                    <h5>商城所有拍品索引及管理</h5>
                </div>
                <?php echo $output['top_link'];?>
            </div>
        </div>
        <form method="post" name="form_goodsverify">
            <input type="hidden" name="form_submit" value="ok" />
            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit">
                        <label for="auction_commis_1">拍品一级返佣比例：</label>
                    </dt>
                    <dd class="opt">
                        <input name="auction_commis_1" id="auction_commis_1" value="<?php echo intval($output['list_setting']['auction_commis_1']);?>" type="text" class="input-txt">
                        <span class="err"></span>
                        <p class="notic">拍品一级返佣比例</p>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label for="auction_commis_2">拍品二级返佣比例：</label>
                    </dt>
                    <dd class="opt">
                        <input name="auction_commis_2" id="auction_commis_2" value="<?php echo intval($output['list_setting']['auction_commis_2']);?>" type="text" class="input-txt">
                        <span class="err"></span>
                        <p class="notic">拍品二级返佣比例</p>
                    </dd>
                </dl>
                <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" onclick="document.form_goodsverify.submit()"><?php echo $lang['nc_submit'];?></a></div>
            </div>
        </form>
    </div>
