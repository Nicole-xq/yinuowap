<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<style>
  .ncap-form-dialog{
    margin: 0 auto !important;
  }
  .ncap-form-dialog .ncap-form-default{
    margin: 0 auto !important;
  }
  .lm_add_table {
    border: 1px solid #ddd;
    width: 100%;
    max-width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
  }

  .lm_add_table th {
    border-bottom-width: 2px;
    border: 1px solid #ddd;
    vertical-align: bottom;
    padding: 8px;
    line-height: 1.42857143;
    text-align: left;
    font-size: 14px;
    color: #333;
  }

  .lm_add_table td {
    border: 1px solid #ddd;
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    font-size: 14px;
    color: #333;
  }

  .lm_add_table tr td:first-child {
    width: 30px;
    text-align: center;
  }

  .lm_add_table thead tr {
    background-color: #f9f9f9;
  }

  .lm_add_table tbody tr:hover,
  .lm_add_table tbody tr.active{
    background: #d9edf7;
  }

  .lm_add_table tbody tr{
    cursor: pointer;
  }
  .lm_add_table tr td:not(:first-child) input{
    border: none;
    outline: none;
    box-shadow: none !important;
    background: transparent;
    cursor: pointer;
  }
  .lm_table_con{
    max-height:350px;
    overflow:scroll;
  }
</style>
<script src="<?php echo ADMIN_RESOURCE_URL ?>/js/admin.js" type="text/javascript"></script>
<form method="post" name="form1" id="form1" class="ncap-form-dialog"
      action="<?php echo urlAdminShop('member', 'to_send'); ?>">
  <input type="hidden" name="form_submit" value="ok"/>
  <div class="ncap-form-default">
    <div class="lm_table_con">
      <table border="1" cellspacing="0" cellpadding="0" width="100%" class="lm_add_table">
        <thead>
        <tr>
          <th></th>
          <th>消息模板描述</th>
          <th>类型</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ((array) $output['global_list'] as $key=>$val) { ?>
          <tr>
            <td>
              <input  name="radio_box" type="radio" value="<?php echo $val['id']?>">
            </td>
            <td><?php echo $val['tpl_title']; ?></td>
            <td><?php echo $val['send_type']; ?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="bot">
        <a href="javascript:void(0);" class="ncap-btn-big ncap-btn-green" nctype="btn_submit">
            <?php echo $lang['nc_submit']; ?>
        </a>
        <span class="err"></span>
    </div>
  </div>
</form>
<script>
    $(function () {
        $('a[nctype="btn_submit"]').click(function () {
            if($("#form1").valid()){
                $("#form1").submit();
            }
        });
        $('#form1').validate({
            errorPlacement: function (error, element) {
                $(".err").append(error);
            },
            rules: {
                radio_box : {
                    required : true
                }
            },
            messages: {
                radio_box : {
                    required : "<i class='fa fa-exclamation-circle'></i>请选择消息模板"
                }
            }
        });
    });
</script>