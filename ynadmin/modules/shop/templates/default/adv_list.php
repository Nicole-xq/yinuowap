<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<!--<script type="text/javascript" src="--><?php //echo RESOURCE_SITE_URL;?><!--/js/jquery.Jcrop/jquery.Jcrop.js"></script>-->
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style>
    .ml5 input.red{
        border:1px red dashed;}
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>广告设置</h3>
                <h5>编辑广告</h5>
            </div>
            <ul class="tab-base nc-row">
                <li><a href="index.php?act=promotion_guess">活动列表</a></li>
                <li><a href="index.php?act=promotion_guess&op=guess_verify_list">待审核列表</a></li>
                <li><a href="JavaScript:void(0);"  class="current">趣猜公共banner</a></li>
            </ul>
        </div>
    </div>

    <form method="post" enctype="multipart/form-data" name="form1" id="adv_form">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default" nctype="adv">
            <?php if(!empty($output['list'])){?>
                <?php foreach($output['list'] as $k=>$v){?>
                    <dl class="row">
                        <dt class="tit">
                            <label>首页广告轮播</label>
                        </dt>
                        <dd class="opt">
                            <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_AUCTION.DS.$v['pic']);?>"><i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.(ATTACH_AUCTION.DS.$v['pic']);?>>')" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
                    <input name="adv_pic_<?php echo $k?>"  data-id="<?php echo $k?>" type="file" class="type-file-file" id="adv_pic_<?php echo $k?>" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
                    <input type='text' name='textfield' id='textfield<?php echo $k?>' class='type-file-text' />
                    <input type='button' name='button' id='button<?php echo $k?>' value='选择上传...' class='type-file-button' />
                    </span></div>
                            <label title="" class="ml5"><i class="fa fa-link"></i><input class="input-txt ml5" type="text" data-id="<?php echo $k?>" name="adv_url_<?php echo $k?>" value="<?php echo $v['url'];?>" placeholder="请输入广告的链接地址" /></label>
                            <span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i>删除</a></span>
                            <p class="notic"></p>
                        </dd>
                    </dl>
                    <?php }?>
            <?php }else{?>
                <dl class="row">
                    <dt class="tit">
                        <label>首页广告轮播</label>
                    </dt>
                    <dd class="opt">
                        <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href=""><i class="fa fa-picture-o" onMouseOver="toolTip()" onMouseOut="toolTip()"></i></a></span><span class="type-file-box">
            <input name="adv_pic_1" type="file" data-id="1" class="type-file-file" id="adv_pic_1" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>
            <input type='text' name='textfield' id='textfield1' class='type-file-text' />
            <input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />
            </span></div>
                        <label title="" class="ml5"><i class="fa fa-link"></i><input class="input-txt ml5" type="text" data-id="1" name="adv_url_1" value="" placeholder="请输入广告的链接地址" /></label>
                        <span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i>删除</a></span>
                        <p class="notic"></p>
                    </dd>
                </dl>
            <?php }?>
            <a nctype="btn_add_item_image"  class="ncap-btn"  data-desc="640*340" href="javascript:;"><i class="fa fa-plus"></i>添加新的广告条</a>
        </div>

        <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn" ><?php echo $lang['nc_submit'];?></a></div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    // 模拟网站LOGO上传input type='file'样式
    $(function(){
        $("#adv_pic_1").change(function(){
            $("#textfield1").val($(this).val());
        });
// 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("图片限于png,gif,jpeg,jpg格式");
                $(this).attr('value','');
                return false;
            }
            var key = $(this).attr('data-id');
            $("#textfield"+ key).val($(this).val());
            $("#adv_pic_" + key).change(function(){var thiskey = $(this).attr('data-id');$("#textfield"+ thiskey).val($(this).val());});
        });

        $('a[nctype="btn_add_item_image"]').on('click', function() {
            obj = $('div[nctype="adv"]').find('a[nctype="btn_add_item_image"]');
            len = $('div[nctype="adv"]').children('dl').length;
            key = 'k'+len+Math.floor(Math.random()*100);
            var add_html = '';
            add_html += '<dl class="row"> <dt class="tit"> <label>首页广告轮播</label> </dt> <dd class="opt"><div class="input-file-show"><span class="show">';
            add_html += '<a class="nyroModal" rel="gal" href=""><i class="fa fa-picture-o" onMouseOver="toolTip()" onMouseOut="toolTip()"></i></a></span>';
            add_html += '<span class="type-file-box"><input name="adv_pic_'+ key+'" data-id="'+ key+'" type="file" class="type-file-file" id="adv_pic_'+ key+'" size="30" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效"/>';
            add_html += '<input type="text" name="textfield" id="textfield'+ key+'" class="type-file-text"/>';
            add_html += '<input type="button" name="button" id="button'+ key+'" value="选择上传..." class="type-file-button" />';
            add_html += '</span></div>';
            add_html += '<label title="" class="ml5"><i class="fa fa-link"></i><input class="input-txt ml5" type="text" data-id="'+ key+'" name="adv_url_'+ key+'" value="" placeholder="请输入广告的链接地址" /></label>'+
                '<span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i>删除</a></span><p class="notic"></p> </dd> </dl>';
            obj.before(add_html);

            $("#adv_pic_" + key).change(function(){var thiskey = $(this).attr('data-id');$("#textfield"+ thiskey).val($(this).val());});
            $('input[class="type-file-file"]').change(function(){
                var filepath=$(this).val();
                var extStart=filepath.lastIndexOf(".");
                var ext=filepath.substring(extStart,filepath.length).toUpperCase();
                if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                    alert("图片限于png,gif,jpeg,jpg格式");
                    $(this).attr('value','');
                    return false;
                }
            });

        });

        $('#adv_form').find('a[nctype="del"]').live('click', function(){
            $(this).parents('dl:first').remove();
        });

        // 点击查看图片
        $('.nyroModal').nyroModal();
        $('#time_zone').attr('value','<?php echo $output['list']['time_zone'];?>');

        $("#submitBtn").click(function(){
            var flag = true;
            $('.ml5 input').each(function(){
                if(!$(this).val()){
                    $(this).addClass('red');
                    flag = false;
                }else{
                    $(this).removeClass('red');
                }
            });
            if(flag){
                $("#adv_form").submit();
            }
        });



    });




</script>