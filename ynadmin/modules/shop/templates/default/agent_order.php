<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>区域代理统计分析</h3>
        <h5>平台针对区域代理的各项返佣数据、注册人数统计</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
        <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
        <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
        <li>显示代理的每种类型的返佣信息</li>
        <li>默认显示当前月份的，如需查看其他的月份，请重新设置 "高级搜索" 的日期筛选值</li>
    </ul>
  </div>
  <div id="flexigrid"></div>
  <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
  <div class="ncap-search-bar">
    <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
    <div class="title">
      <h3>高级搜索</h3>
    </div>
    <form id="formSearch">
      <div id="searchCon" class="content">
        <div class="layout-box">
            <dl>
                <dd>
                    <input type="text" value="" placeholder="请输入代理ID" name="agent_id" class="s-input-txt">
                </dd>
            </dl>
            <dl>
                <dt>日期筛选</dt>
                <dd>
                    <label>
                        <input id="query_start_date" placeholder="请选择起始时间" name="query_start_date" value="<?php echo $output['query']['start_date'];?>" type="text" class="s-input-txt" />
                    </label>
                    <label>
                        <input id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="<?php echo $output['query']['end_date'];?>" type="text" class="s-input-txt" />
                    </label>
                </dd>
            </dl>
        </div>
      </div>
      <div class="bottom">
          <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/statistics.js"></script>
<script>
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=agent_order&op=get_income_xml&'+$("#formSearch").serialize(),
        colModel : [
            {display: '代理ID', name : 'agent_id', width : 50, sortable : false, align: 'center'},
            {display: '代理名称', name : 'agent_name', width : 200, sortable : false, align: 'center'},
            {display: '代理类型', name : 'agent_type', width : 50, sortable : true, align: 'center'},
            {display: '订单金额', name : 'sum_order_price_1',  width : 100, sortable : false, align: 'center'},
            {display: '订单返佣', name : 'sum_commission_amount_1',  width : 80, sortable : false, align: 'center'},
            {display: '拍卖金额', name : 'sum_order_price_2',  width : 100, sortable : false, align: 'center'},
            {display: '拍卖返佣', name : 'sum_commission_amount_2',  width : 80, sortable : false, align: 'center'},
            {display: '保证金额', name : 'sum_order_price_3',  width : 100, sortable : false, align: 'center'},
            {display: '保证金返佣', name : 'sum_commission_amount_3',  width : 80, sortable : false, align: 'center'},
            {display: '合伙人金额', name : 'sum_order_price_4',  width : 100, sortable : false, align: 'center'},
            {display: '注册会员数', name : 'reg_num',  width : 100, sortable : false, align: 'center'},
            {display: '合伙人返佣', name : 'sum_commission_amount_4',  width : 80, sortable : false, align: 'center'},
            {display: '艺术家金额', name : 'sum_order_price_5',  width : 100, sortable : false, align: 'center'},
            {display: '艺术家返佣', name : 'sum_commission_amount_5',  width : 80, sortable : false, align: 'center'},
           ],
        rp: 10,
        title: '区域代理返佣统计列表'
    });
}
$(function () {
    //绑定时间控件
    $('#query_start_date').datepicker();
    $('#query_end_date').datepicker();

	update_flex();
	$('#ncsubmit').click(function(){
	    $('.flexigrid').after('<div id="flexigrid"></div>').remove();
	    update_flex();
    });
});
</script>