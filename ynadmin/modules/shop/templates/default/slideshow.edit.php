<?php defined('InShopNC') or exit('Access Invalid!');?>
<style type="text/css">
  .ncsc-upload-btn { vertical-align: top; display: inline-block; *display: inline/*IE7*/; width: 80px; height: 30px; margin: 5px 5px 0 0; *zoom:1;}
.ncsc-upload-btn a { display: block; position: relative; z-index: 1;}
.ncsc-upload-btn span { width: 80px; height: 30px; position: absolute; left: 0; top: 0; z-index: 2; cursor: pointer;}
.ncsc-upload-btn .input-file { width: 80px; height: 30px; padding: 0; margin: 0; border: none 0; opacity:0; filter: alpha(opacity=0); cursor: pointer; }
.ncsc-upload-btn p { font-size: 12px; line-height: 20px; background-color: #F5F5F5; color: #999; text-align: center; color: #666; width: 78px; height: 20px; padding: 4px 0; border: solid 1px; border-color: #DCDCDC #DCDCDC #B3B3B3 #DCDCDC; position: absolute; left: 0; top: 0; z-index: 1;}
.ncsc-upload-btn p i { vertical-align: middle; margin-right: 4px;}
.ncsc-upload-btn a:hover p { background-color: #E6E6E6; color: #333; border-color: #CFCFCF #CFCFCF #B3B3B3 #CFCFCF;}a.ncbtn-mini,
</style>
<div class="page">
  <div class="fixed-bar">
      <?php if($_GET['enjoy_zone']>0){ ?>
         <div class="item-title"><a class="back" href="index.php?act=brand&op=enjoy_zone_brand" title="返回品牌列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <?php } else { ?>
          <div class="item-title"><a class="back" href="index.php?act=brand&op=brand>" title="返回品牌列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <?php } ?>

      <div class="subject">
        <h3><?php echo $lang['brand_index_brand'];?> - <?php echo $lang['nc_edit'];?>品牌“<?php echo $output['brand_array']['brand_name']?>”</h3>
        <h5><?php echo $lang['brand_index_brand_subhead'];?></h5>
      </div>
    </div>
  </div>
  <form id="slideshow_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">

      <dl class="row">
        <dt class="tit">
          <label><em>*</em>标题</label>
        </dt>
        <dd class="opt">
          <input type="text" value="<?php echo $output['slideshow_array']['title'];?>" name="slideshow_title" id="slideshow_title" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>URL</label>
        </dt>
        <dd class="opt">
          <input type="text" value="<?php echo $output['slideshow_array']['url'];?>" name="url" id="url" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="pic">图片</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_SLIDESHOW.DS.$output['slideshow_array']['image'];?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo $output['slideshow_array']['image'];?>>')" onMouseOut="toolTip()"></i> </a></span><span class="type-file-box">
            <input class="type-file-file" id="slideshow_img" name="slideshow_img" type="file" accept="image/png,image/jpeg,image/jpg" nc_type="change_pic" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            <input type="text" name="textfield" id="textfield1" class="type-file-text" />
            <input type="button" name="button" id="button1" value="选择上传..." class="type-file-button" />
            </span></div>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">是否生效</dt>
        <dd class="opt">
          <div class="onoff">
            <label for="state1" class="cb-enable <?php if($output['slideshow_array']['state'] == '1'){ ?>selected<?php } ?>"><?php echo $lang['nc_yes'];?></label>
            <label for="state2" class="cb-disable <?php if($output['slideshow_array']['state'] == '0'){ ?>selected<?php } ?>"><?php echo $lang['nc_no'];?></label>
            <input id="state1" name="state" <?php if($output['slideshow_array']['state'] == '1'){ ?>checked="checked"<?php } ?>  value="1" type="radio">
            <input id="state2" name="state" <?php if($output['slideshow_array']['state'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
          </div>
          <p class="notic"><?php echo $lang['brand_index_recommend_tips'];?></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">排序</dt>
        <dd class="opt">
          <input type="text" value="<?php echo $output['slideshow_array']['sort'];?>" name="sort" id="sort" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>

//按钮先执行验证再提交表单
$(function(){

// 点击查看图片
	$('.nyroModal').nyroModal();
	$("#submitBtn").click(function(){
		 $("#slideshow_form").submit();
	});
	$('input[class="type-file-file"]').change(uploadChange);
	function uploadChange(){
		var filepath=$(this).val();
		var extStart=filepath.lastIndexOf(".");
		var ext=filepath.substring(extStart,filepath.length).toUpperCase();
		if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
			alert("file type error");
			$(this).attr('value','');
			return false;
		}
		if ($(this).val() == '') return false;
	}

	jQuery.validator.addMethod("initial", function(value, element) {
		return /^[A-Za-z0-9]$/i.test(value);
	}, "");

});



</script>
