<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auctions_good&op=index" title="返回分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>查看拍品</a></h3>
        <h5>拍品查看</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auction_name"><em>*</em>拍品名称</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_name']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="store_name"><em>*</em>店铺名字</label>
        </dt>
        <dd class="opt">
          <?php echo $info['store_name']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auction_bond"><em>*</em>拍品保证金</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_bond']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auction_start_price"><em>*</em>拍品起拍价</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_start_price']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auction_reference_price"><em>*</em>拍品参考价</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_reference_price']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="auction_increase_range"><em>*</em>拍品加价幅度</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_increase_range']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <?php if ($admin_info['gid'] == 1) { ?>
      <dl class="row">
        <dt class="tit">
          <label for="auction_reserve_price"><em>*</em>拍品保留价</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auction_reserve_price']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <?php }?>
      <dl class="row">
        <dt class="tit">
          <label for="delivery_mechanism"><em></em>送拍机构</label>
        </dt>
        <dd class="opt">
          <?php echo $info['delivery_mechanism']; ?>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
            <dt class="tit">
                <label for="auction_image"><em></em>封面图</label>
            </dt>
            <dd class="opt">
              <img src="<?php echo $info['img_path']; ?>">
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label><em></em>明细图</label>
            </dt>
            <dd class="opt">
              <img src="<?php echo $info['auctions_img1']; ?>">
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label><em></em>明细图</label>
            </dt>
            <dd class="opt">
              <img src="<?php echo $info['auctions_img2']; ?>">
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label><em></em>明细图</label>
            </dt>
            <dd class="opt">
              <img src="<?php echo $info['auctions_img3']; ?>">
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label><em></em>明细图</label>
            </dt>
            <dd class="opt">
              <img src="<?php echo $info['auctions_img4']; ?>">
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label for="auctions_class_lv1"><em>*</em>拍品类型</label>
            </dt>
            <dd class="opt">
                <?php echo $info['auction_type_text']; ?>
            </dd>
        </dl>
        <dl class="row">
        <dt class="tit">
          <label for="auctions_class_lv1"><em>*</em>一级分类</label>
        </dt>
        <dd class="opt">
          <?php echo $info['class1']['auctions_class_name']; ?>        
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label for="auctions_class_lv2"><em>*</em>二级分类</label>
        </dt>
        <dd class="opt">
          <?php echo $info['class2']['auctions_class_name']; ?>         
        </dd>
      </dl>
<dl class="row">
        <dt class="tit">
          <label><em></em>规格</label>
        </dt>
        <dd class="opt" id="guige">
          <?php if(!empty($info['specArr'])){ ?>
            <?php foreach($info['specArr'] as $k => $v){ ?>
                <p>
                  <?php echo $k; ?>：<?php echo $v; ?>
                </p>
              <?php } ?>
          <?php } ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="long"><em></em>大小</label>
        </dt>
        <dd class="opt">
          <?php echo $info['auctions_long']; ?>*<?php echo $info['auctions_width']; ?>*<?php echo $info['auctions_height']; ?>          
          <span class="err"></span>

        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="create_age"><em>*</em>创作年代</label>
        </dt>
        <dd class="opt">
          <?php echo $info['create_age']; ?>
          <span class="err"></span>

        </dd>
      </dl>
      <dl class="row">
            <dt class="tit">
                <label><em></em>作品简介</label>
            </dt>
            <dd class="opt">
              <?php echo $info['auctions_summary']; ?>
              <span class="err"></span>
              <p class="notic"></p>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label><em></em>艺术家</label>
            </dt>
            <dd class="opt">
                <?php echo $info['auctions_artist_name']; ?>
            </dd>
        </dl>
    </div>
  </form>
</div>