<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=special_rec&op=rec_ok" title="返回书画专场推荐列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>书画专场推荐</h3>
                <h5>书画页面推荐专场管理</h5>
            </div>
        </div>
    </div>
    <form id="goods_form" method="post" action='index.php?act=special_rec&op=save' enctype="multipart/form-data">
        <input type="hidden" name="special_id" id="special_id" value="<?php echo $_GET['special_id']?>"/>
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default" id="explanation">
            <dl class="row">
                <dt class="tit">推荐图片</dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PAINTING.'/'.$output['rec_info']['special_rec_pic']; ?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.'/'.ATTACH_PAINTING.'/'.$output['rec_info']['special_rec_pic']; ?>>')" onMouseOut="toolTip()"/></i> </a></span><span class="type-file-box">
            <input type="text" name="rec_pic" id="rec_pic" class="type-file-text" />
            <input type="button" name="button2" id="button2" value="选择上传..." class="type-file-button" />
            <input class="type-file-file" id="special_pic" name="special_pic" type="file" size="30" hidefocus="true" nc_type="change_special_pic" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            </span></div>
                    <input name="rec_image" id="rec_image" type="hidden" value="<?php echo $output['rec_info']['special_rec_pic']?>"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>推荐拍品</label>
                </dt>
                <dd class="opt">
                    <input type="text" placeholder="搜索拍品名称" value="" name="goods_name" id="goods_name" maxlength="20" class="input-txt">
                    <a id="goods_search" href="JavaScript:void(0);" class="ncap-btn mr5"><?php echo $lang['nc_search'];?></a></dd>
            </dl>
            <dl class="row" id="selected_goods_list">
                <dt class="tit">已推荐拍品</dt>
                <dd class="opt">
                    <input type="hidden" name="valid_recommend" id="valid_recommend" value="">
                    <span class="err"></span>
                    <ul class="dialog-goodslist-s1 goods-list scrollbar-box">
                    </ul>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">选择要推荐的拍品</dt>
                <dd class="opt">
                    <div id="show_recommend_goods_list" class="show-recommend-goods-list scrollbar-box"></div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">排序</dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['rec_info']['rec_sort']?>" name="rec_sort" id="rec_sort" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">数字越小越靠前</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript">
    $(function(){
        $("#special_pic").change(function(){
            $("#rec_pic").val($(this).val());
            $('#rec_image').val($(this).val());
        });
        // 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("<?php echo $lang['default_img_wrong'];?>");
                $(this).attr('value','');
                return false;
            }
        });
// 点击查看图片
        $('.nyroModal').nyroModal();
        $('#time_zone').attr('value','');
        $("#submitBtn").click(function(){
            if ($('input[name="goods_id_list[]"]').size() != 3) {
                $('#valid_recommend').rules('add',{
                    required: true,
                    messages: {required : '<i class="fa fa-exclamation-circle"></i>请选择3推荐拍品'}
                });
            }else{
                $('#valid_recommend').rules('remove');
            }

            if($("#goods_form").valid()){
                $("#goods_form").submit();
            }
        });
        $('#goods_search').on('click',function(){
            $('#valid_recommend').rules('remove');

                $('#show_recommend_goods_list').load('index.php?act=special_rec&op=get_auction_list&goods_name='+$('#goods_name').val()+'&special_id='+$('#special_id').val());

        });

        //表单验证
        $('#goods_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                rec_image:{
                    required : true
                },
                rec_sort : {
                    required : true,
                    number   : true
                }

            },
            messages : {
                rec_image:{
                    required : '<i class="fa fa-exclamation-circle"></i>推荐图片不能为空'
                },
                rec_sort: {
                    required: '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    number: '<i class="fa fa-exclamation-circle"></i>请填写正确的排序'
                }
            }

        });

    });

    function select_recommend_goods(auction_id) {
        if (typeof auction_id == 'object') {
            var auction_name = auction_id['auction_name'];
            var auction_pic = auction_id['auction_image'];
            var auction_id = auction_id['auction_id'];
        } else {
            var goods = $("#show_recommend_goods_list img[goods_id='"+auction_id+"']");
            var auction_pic = goods.attr("src");
            var auction_name = goods.attr("goods_name");
        }
        var obj = $("#selected_goods_list");
        if(obj.find("img[goods_id='"+auction_id+"']").size()>0) return;//避免重复
        if(obj.find("ul>li").size() > 2){
            alert('请推荐3个商品');
            return false;
        }
        var text_append = '';
        text_append += '<div onclick="del_recommend_goods(this,'+auction_id+');" class="goods-pic">';
        text_append += '<span class="ac-ico"></span>';
        text_append += '<span class="thumb size-72x72">';
        text_append += '<i></i>';
        text_append += '<img width="72" goods_id="'+auction_id+'" title="'+auction_name+'" goods_name="'+auction_name+'" src="'+auction_pic+'" />';
        text_append += '</span></div>';
        text_append += '<div class="goods-name">';
        text_append += '<a href="<?php echo C('auction_site_url')?>/index.php?act=auctions&id='+auction_id+'" target="_blank">';
        text_append += auction_name+'</a>';
        text_append += '</div>';
        text_append += '<input name="goods_id_list[]" value="'+auction_id+'" type="hidden">';
        obj.find("ul").append('<li>'+text_append+'</li>');

    }
    function del_recommend_goods(obj,auction_id) {
        $(obj).parent().remove();
    }

    var goods_list_json = $.parseJSON('<?php echo $output['goods_list_json'];?>');
    $.each(goods_list_json,function(k,v){
        select_recommend_goods(v);
    });

</script>
