<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>书画商品推荐推荐</h3>
                <h5>书画频道页推荐商品管理</h5>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=painting&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '模块名称', name : 'rec_title', width : 300, sortable : false, align: 'left'},
                {display: '标签名称', name : 'rec_tag', width : 300, sortable : false, align: 'left'},
                {display: '推荐商品数量', name : 'rec_count', width : 100, sortable : false, align : 'center'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operate }
            ],
            sortname: "module_title_id",
            sortorder: "desc",
            title: '书画商品推荐列表'
        });
    });
    function fg_operate(name, bDiv) {
        if (name == 'del') {
            if($('.trSelected',bDiv).length>0){
                var itemlist = new Array();
                $('.trSelected',bDiv).each(function(){
                    itemlist.push($(this).attr('data-id'));
                });
                fg_delete(itemlist);
            } else {
                return false;
            }
        } else if (name == 'add') {
            window.location.href = 'index.php?act=painting&op=add';
        }
    }

    function fg_delete(id,name) {
        if (typeof id == 'number') {
            var id = new Array(id.toString());
        };
        if(confirm('删除后将不能恢复，确认删除这 ' + id.length + ' 项吗？')){
            id = id.join(',');
        } else {
            return false;
        }
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "index.php?act=painting&op=delete",
            data: "del_id="+id+"&tag_name=" +name,
            success: function(data){
                if (data.state){
                    $("#flexigrid").flexReload();
                } else {
                    alert(data.msg);
                }
            }
        });
    }
</script>
