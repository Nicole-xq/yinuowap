<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <!-- <a class="back" href="index.php?act=member_rate_config&op=index" title="返回专题列表"><i class="fa fa-arrow-circle-o-left"></i></a> -->
      <div class="subject">
        <h3>设置拍卖时长</h3>
        <h5>设置拍卖时长</h5>
      </div>
    </div>
  </div>
  <form id="save_form" method="post" enctype="multipart/form-data" action="index.php?act=auction_config&op=save_info">    
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="duration_time"><em>*</em>拍卖时长</label>
        </dt>
        <dd class="opt">
          <input id="duration_time" name="duration_time" class="input-txt" type="text" value="<?php if(!empty($output['info'])) echo $output['info']['duration_time'];?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label for="add_time"><em>*</em>延长时长</label>
        </dt>
        <dd class="opt">
          <input id="add_time" name="add_time" class="input-txt" type="text" value="<?php if(!empty($output['info'])) echo $output['info']['add_time'];?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>

    </div>


    <div class="ncap-form-default">
      <div class="bot">
        <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="btn_save">发布</a>
      </div>
    </div>
  </form>

</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/cms/exhibition.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_save').click(function(){
            if(confirm('确认修改比率吗？')) {

                if(parseInt($('#duration_time').val())<0 || parseInt($('#add_time').val()) < 0){
                    alert('时长不可为负数');
                    return false;
                }

                $('#save_form').submit();
            }
        });

    });
</script>