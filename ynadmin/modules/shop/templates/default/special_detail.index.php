<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="index.php?act=auction_special&op=platform_special" title="返回活动列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>平台专场管理 - 处理“<?php echo $output['special_info']['special_name']; ?>”专场的申请</h3>
                <h5>平台商城管理与店铺申请商品审核</h5>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        var flexUrl = 'index.php?act=auction_special&op=detail_xml&id=<?php echo $output['special_info']['special_id']; ?>';

        $("#flexigrid").flexigrid({
            url: flexUrl,
            colModel: [
                {display: '操作', name: 'operation', width: 150, sortable: false, align: 'center', className: 'handle'},
                {display: '商品名称', name: 'auction_name', width: 420, sortable: false, align: 'left'},
                {display: '所属店铺', name: 'store_name', width: 150, sortable: false, align: 'center'},
                {display: '状态', name: 'auction_sp_state', width: 80, sortable: 1, align: 'center'}
            ],
            buttons: [
                {
                    display: '<i class="fa fa-check"></i>批量通过',
                    name: 'add',
                    bclass: 'add',
                    title: '将选定行数据批量通过',
                    onpress: function() {
                        var ids = [];
                        $('.trSelected[data-id]').each(function() {
                            ids.push($(this).attr('data-id'));
                        });
                        if (ids.length < 1 || !confirm('确定批量通过?')) {
                            return false;
                        }
                        var href = 'index.php?act=auction_special&op=deal&state=1&special_auction_id=__IDS__'.replace('__IDS__', ids.join(','));
                        performReq(href);
                    }
                },
                {
                    display: '<i class="fa fa-ban"></i>批量拒绝',
                    name: 'csv',
                    bclass: 'csv',
                    title: '将选定行数据批量拒绝',
                    onpress: function() {
                        var ids = [];
                        $('.trSelected[data-id]').each(function() {
                            ids.push($(this).attr('data-id'));
                        });
                        if (ids.length < 1 || !confirm('确定批量拒绝?')) {
                            return false;
                        }
                        var href = 'index.php?act=auction_special&op=deal&state=2&special_auction_id=__IDS__'.replace('__IDS__', ids.join(','));
                        performReq(href);
                    }
                },
                {
                    display: '<i class="fa fa-trash"></i>批量删除',
                    name: 'del',
                    bclass: 'del',
                    title: '将选定行数据批量删除',
                    onpress: function() {
                        var ids = [];
                        $('.trSelected[data-id]').each(function() {
                            ids.push($(this).attr('data-id'));
                        });
                        if (ids.length < 1 || !confirm('确定删除?')) {
                            return false;
                        }
                        var href = 'index.php?act=auction_special&op=del_detail&special_auction_id=__IDS__'.replace('__IDS__', ids.join(','));
                        performReq(href);
                    }
                }
            ],
            searchitems: [
                {display: '商品名称', name: 'auction_name', isdefault: true},
                {display: '所属店铺', name: 'store_name'}
            ],
            sortname: "special_auction_id",
            sortorder: "desc",
            title: '专场商品申请列表'
        });



        var performReq = function(url) {
            $.getJSON(url, function(d) {
                if (d && d.result) {
                    $("#flexigrid").flexReload();
                } else {
                    alert(d && d.message || '操作失败！');
                }
            });
        };

        $('a[data-href]').live('click', function() {
            if ($(this).hasClass('confirm-on-click') && !confirm('确定"'+this.innerHTML+'"?')) {
                return false;
            }

            performReq($(this).attr('data-href'));
        });

    });



</script>
