<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>商家拍卖管理</h3>
                <h5>编辑支付拍卖保障金，开启拍卖功能</h5>
            </div>
        </div>
    </div>

    <div id="flexigrid"></div>

</div>
<script type="text/javascript">
    $(function() {
        $("#flexigrid").flexigrid({
            url: 'index.php?act=store_vendue&op=get_xml',
            colModel: [
                {display: '操作', name: 'operation', width: 150, sortable: false, align: 'center', className: 'handle'},
                {display: '商家拍卖ID', name: 'store_vendue_id', width: 80, sortable: true, align: 'center'},
                {display: '商家ID', name: 'store_id', width: 80, sortable: true, align: 'left'},
                {display: '店铺名称', name: 'store_name', width: 120, sortable: false, align: 'left'},
                {display: '拍卖logo', name: 'store_vendue_logo', width: 60, sortable: false, align: 'center'},
                {display: '入驻时间', name: 'add_time', width: 100, sortable: false, align: 'center'},
                {display: '所在地区', name: 'area_id', width: 120, sortable: true, align: 'center'},
                {display: '机构类型', name: 'oz_type', width: 150, sortable: true, align: 'center'},
                {display: '主营', name: 'store_vendue_zy', width: 100, sortable: true, align: 'center'},
                {display: '当前状态', name: 'store_apply_state', width: 80, sortable: true, align: 'center'},

            ],
            searchitems: [
                {display: '商家拍卖名称', name: 'store_vendue_name', isdefault: true},
                {display: '店铺名称', name: 'store_name'}
            ],
            sortname: "store_vendue_id",
            sortorder: "asc",
            title: '商家拍卖列表'
        });
    });



</script>