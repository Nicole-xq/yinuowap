<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=member_rate_config&op=index" title="返回专题列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>编辑保证金配置</h3>
        <h5>编辑保证金配置</h5>
      </div>
    </div>
  </div>
  <form id="save_form" method="post" enctype="multipart/form-data" action="index.php?act=member_rate_config&op=save_info">
    <input name="config_id" type="hidden" value="<?php if(!empty($output['info'])) echo $output['info']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label>标题</label>
        </dt>
        <dd class="opt">
          <?=$output['info']['title'];?>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="bid_rate"><em>*</em>竞价返佣</label>
        </dt>
        <dd class="opt">
          <input id="bid_rate" name="bid_rate" class="input-txt" type="text" value="<?php if(!empty($output['info'])) echo $output['info']['bid_rate'];?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label for="deal_rate"><em>*</em>成交返佣</label>
        </dt>
        <dd class="opt">
          <input id="deal_rate" name="deal_rate" class="input-txt" type="text" value="<?php if(!empty($output['info'])) echo $output['info']['deal_rate'];?>"/>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>

    </div>


    <div class="ncap-form-default">
      <div class="bot">
        <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="btn_save">发布</a>
      </div>
    </div>
  </form>

</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link media="all" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/imgareaselect-animated.css" type="text/css" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/cms/exhibition.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_save').click(function(){
            if(confirm('确认修改比率吗？')) {

                if(parseInt($('#deal_rate').val())<0 || parseInt($('#bid_rate').val()) < 0){
                    alert('比率不可为负数');
                    return false;
                }

                $('#save_form').submit();
            }
        });

    });
</script>