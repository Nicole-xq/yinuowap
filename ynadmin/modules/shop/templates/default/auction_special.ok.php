<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>专场审核</h3>
                <h5>对签约人和拍卖公司申请的专场已经审核</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>

<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=auction_special&op=check_get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '专场ID', name : 'special_id', width : 40, sortable : false, align: 'center'},
                {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
                {display: '店铺名称', name : 'store_name', width : 150, sortable : false, align: 'left'},
                {display: '专场名称', name : 'special_name', width : 120, sortable : true, align: 'left'},
                {display: '专场缩略图', name : 'special_image', width: 60, sortable : false, align : 'center'},
                {display: '审核状态', name : 'special_state', width : 80, sortable : true, align: 'center'},
                {display: '专场开始时间', name : 'special_start_time', width : 150, sortable : true, align: 'center'},
                {display: '专场结束时间', name : 'special_end_time', width : 150, sortable : true, align: 'center'},
                {display: '预展开始时间', name : 'special_preview_start', width : 150, sortable : true, align: 'center'},
                {display: '专场申请时间', name : 'special_add_time', width : 150, sortable : true, align: 'center'}
            ],
            searchitems : [
                {display: '店铺名称', name : 'store_name', isdefault: true},
                {display: '专场名称', name : 'special_name'}
            ],
            sortname: "special_id",
            sortorder: "desc",
            title: '已审核专场列表'
        });

    });
    $('a[data-href]').live('click', function() {
        if ($(this).hasClass('confirm-on-click') && !confirm('确定"'+$(this).text()+'"?')) {
            return false;
        }

        $.getJSON($(this).attr('data-href'), function(d) {
            if (d && d.result) {
                $("#flexigrid").flexReload();
            } else {
                alert(d && d.message || '操作失败！');
            }
        });
    });

    // 设置专场统一保证金利率
    function set_auction(ids) {
        _uri = "index.php?act=auction_special&op=get_special_info&id=" + ids;
        CUR_DIALOG = ajax_form('auctions_setting', '', _uri, 640);
    }

    //设置专场认购截止时间(计息)
    function set_rate_time(){
        _uri = "index.php?act=auction_special&op=get_special_info&id=" + ids;
        CUR_DIALOG = ajax_form('auctions_setting', '', _uri, 640);
    }
</script>