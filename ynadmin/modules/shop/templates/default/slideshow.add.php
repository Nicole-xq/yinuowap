<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=brand&op=brand" title="返回品牌列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>尊享专区轮播图 - 新增</a></h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <form id="slideshow_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">

      <dl class="row">
        <dt class="tit">
          <label><em>*</em>标题</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="slideshow_title" id="slideshow_title" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>URL</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="url" id="url" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="pic">图片</label>
        </dt>
        <dd class="opt">
          <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="<?php echo $output['image'];?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo $output['image'];?>>')" onMouseOut="toolTip()"></i> </a></span><span class="type-file-box">
            <input class="type-file-file" id="slideshow_img" name="slideshow_img" type="file" accept="image/png,image/jpeg,image/jpg" nc_type="change_pic" hidefocus="true" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            <input type="text" name="textfield" id="textfield1" class="type-file-text" />
            <input type="button" name="button" id="button1" value="选择上传..." class="type-file-button" />
            </span></div>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">是否生效</dt>
        <dd class="opt">
          <div class="onoff">
            <label for="state1" class="cb-enable"><?php echo $lang['nc_yes'];?></label>
            <label for="state2" class="cb-disable selected"><?php echo $lang['nc_no'];?></label>
            <input id="state1" name="state" <?php if($output['state'] == '1'){ ?>checked="checked"<?php } ?>  value="1" type="radio">
            <input id="state2" name="state" <?php if($output['state'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
          </div>
          <p class="notic"><?php echo $lang['brand_index_recommend_tips'];?></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">排序</dt>
        <dd class="opt">
          <input type="text" value="99" name="sort" id="sort" class="input-txt">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js" charset="utf-8"></script>
<script>

$(function(){
  $("#slideshow_img").change(function(){
    $("#textfield1").val($(this).val());
  });
	$("#submitBtn").click(function(){
	     $("#slideshow_form").submit();
	});
	$('input[class="type-file-file"]').change(uploadChange);
	function uploadChange(){
		var filepath=$(this).val();
		var extStart=filepath.lastIndexOf(".");
		var ext=filepath.substring(extStart,filepath.length).toUpperCase();
		if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
			alert("file type error");
			$(this).attr('value','');
			return false;
		}
		if ($(this).val() == '') return false;
	}
	jQuery.validator.addMethod("initial", function(value, element) {
		return /^[A-Za-z0-9]$/i.test(value);
	}, "");

});

</script>
