<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>分类管理</h3>
        <h5>拍品分类的管理</h5>
      </div>
    </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li><?php echo $lang['type_index_prompts_one'];?></li>
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=auctions_class&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: '分类ID', name : 'auctions_class_id', width : 40, sortable : true, align: 'center'},
            {display: '分类名称', name : 'auctions_class_name', width : 120, sortable : false, align: 'center'},
            {display: '上级分类ID', name : 'auctions_class_fid', width : 120, sortable : true, align: 'center'},
            {display: '上级分类名称', name : 'auctions_class_father_name', width : 150, sortable : false, align: 'center'},
            ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增分类', name : 'add', bclass : 'add', title : '新增分类', onpress : fg_operation }
        ],
        searchitems : [
            {display: '分类名称', name : 'like_auctions_class_name'},
            ],
        sortname: "auctions_class_id",
        sortorder: "asc",
        title: '分类列表'
    });
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=auctions_class&op=auctions_class_add';
    }
}
function fg_del(id) {
    if(confirm('删除后将不能恢复，确认删除这项吗？')){
        $.getJSON('index.php?act=auctions_class&op=auctions_class_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
}
</script>