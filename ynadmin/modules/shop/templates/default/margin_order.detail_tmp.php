<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>会员统计</h3>
        <h5>平台针对会员的各项数据统计</h5>
      </div>
      <?php echo $output['top_link'];?> </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li><?php echo $lang['stat_validorder_explain'];?></li>
      <li>列表展示了时间段内所有会员有效订单的订单数量、下单商品数量和订单总金额统计数据，并可以点击列表上方的“导出数据”，将列表数据导出为Excel文件</li>
    </ul>
  </div>
  <div id="stat_tabs" class="  ui-tabs">
    <ul class="tab-base nc-row">
      <li><a href="#ordernum_div">订单量</a></li>
      <li><a href="#orderamount_div">订单金额</a></li>
    </ul>

    <!-- 下单量 -->
    <div id="ordernum_div">
      <div id="container_ordernum" style="min-height:400px; width: 100% !important;"></div>
      <div id="list_ordernum" class="m20"></div>
    </div>

    <!-- 订单金额 -->
    <div id="orderamount_div">
      <div id="container_orderamount" style="min-height:400px; width: 100% !important;"></div>
      <div id="list_orderamount"></div>
    </div>

  </div>
</div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL?>/js/statistics.js"></script>
<script>

$(function() {
	//切换登录卡
    $('#stat_tabs').tabs();
	//统计数据类型
	var s_type = $("#search_type").val();
	$('#search_time').datepicker({dateFormat: 'yy-mm-dd'});

	show_searchtime();
	$("#search_type").change(function(){
		show_searchtime();
	});

	//更新周数组
//	$("[name='searchweek_month']").change(function(){
//		var year = $("[name='searchweek_year']").val();
//		var month = $("[name='searchweek_month']").val();
//		$("[name='searchweek_week']").html('');
//		$.getJSON('<?php //echo ADMIN_SITE_URL?>///index.php?act=common&op=getweekofmonth',{y:year,m:month},function(data){
//	        if(data != null){
//	        	for(var i = 0; i < data.length; i++) {
//	        		$("[name='searchweek_week']").append('<option value="'+data[i].key+'">'+data[i].val+'</option>');
//			    }
//	        }
//	    });
//	});

	$('#container_ordernum').highcharts(<?php echo $output['statordernum_json'];?>);
//	$('#container_goodsnum').highcharts(<?php //echo $output['statorderamount_json'];?>//);
	$('#container_orderamount').highcharts(<?php echo $output['statorderamount_json'];?>);

	//加载详细列表
    $("#list_ordernum").flexigrid({
        url: 'index.php?act=margin_refund_data&op=get_xml',
        colModel : [
            {display: '订单编号', name : 'order_sn', width : 200, sortable : false, align: 'left'},
            {display: '订单来源', name : 'order_from', width : 50, sortable : true, align : 'center'},
            {display: '下单时间', name : 'created_at', width : 140, sortable : true, align: 'left'},
            {display: '订单金额(元)', name : 'margin_amount', width : 140, sortable : true, align: 'left'},
            {display: '订单状态', name : 'order_state', width: 60, sortable : true, align : 'center'},
            {display: '支付方式', name : 'payment_code', width: 130, sortable : true, align : 'center'},
            {display: '支付时间', name : 'payment_time', width: 140, sortable : true, align : 'left'},
            {display: '充值卡支付(元)', name : 'rcb_amount', width : 100, sortable : true, align: 'center'},
            {display: '预存款支付(元)', name : 'pd_amount', width : 100, sortable : true, align: 'center'},
            {display: '诺币支付(元)', name : 'points_amount', width : 120, sortable : false, align : 'left'},
            {display: '保证金余额支付(元)', name : 'account_margin_amount', width : 120, sortable : false, align : 'left'},
            {display: '订单完成时间', name : 'finnshed_time', width: 140, sortable : true, align : 'left'},
            {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
            {display: '店铺名称', name : 'store_name', width : 200, sortable : true, align: 'left'},
            {display: '商品名称', name : 'goods_name', width : 200, sortable : true, align: 'left'},
            {display: '买家ID', name : 'buyer_id', width : 60, sortable : true, align: 'center'},
            {display: '买家名称', name : 'buyer_name', width : 150, sortable : true, align: 'left'}
            ],
        buttons : [
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'excel', bclass : 'csv', title : '导出EXCEL文件', onpress : fg_operation_ordernum }
        ],
        usepager: true,
        rp: 15,
        title: '下单量明细列表'
    });
    $("#list_orderamount").flexigrid({
        url: 'index.php?act=margin_refund_data&op=get_xml',
        colModel : [
            {display: '订单编号', name : 'order_sn', width : 200, sortable : false, align: 'left'},
            {display: '订单来源', name : 'order_from', width : 50, sortable : true, align : 'center'},
            {display: '下单时间', name : 'created_at', width : 140, sortable : true, align: 'left'},
            {display: '订单金额(元)', name : 'margin_amount', width : 140, sortable : true, align: 'left'},
            {display: '订单状态', name : 'order_state', width: 60, sortable : true, align : 'center'},
            {display: '支付方式', name : 'payment_code', width: 130, sortable : true, align : 'center'},
            {display: '支付时间', name : 'payment_time', width: 140, sortable : true, align : 'left'},
            {display: '充值卡支付(元)', name : 'rcb_amount', width : 100, sortable : true, align: 'center'},
            {display: '预存款支付(元)', name : 'pd_amount', width : 100, sortable : true, align: 'center'},
            {display: '诺币支付(元)', name : 'points_amount', width : 120, sortable : false, align : 'left'},
            {display: '保证金余额支付(元)', name : 'account_margin_amount', width : 120, sortable : false, align : 'left'},
            {display: '订单完成时间', name : 'finnshed_time', width: 140, sortable : true, align : 'left'},
            {display: '店铺ID', name : 'store_id', width : 40, sortable : true, align: 'center'},
            {display: '店铺名称', name : 'store_name', width : 200, sortable : true, align: 'left'},
            {display: '商品名称', name : 'goods_name', width : 200, sortable : true, align: 'left'},
            {display: '买家ID', name : 'buyer_id', width : 60, sortable : true, align: 'center'},
            {display: '买家名称', name : 'buyer_name', width : 150, sortable : true, align: 'left'}
            ],
        buttons : [
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'excel', bclass : 'csv', title : '导出EXCEL文件', onpress : fg_operation_orderamount }
        ],
        usepager: true,
        rp: 15,
        title: '下单金额明细列表'
    });
    $("#list_goodsnum").flexigrid({
        url: 'index.php?act=stat_member&op=get_analyzeinfo_xml&type=goodsnum&t=<?php echo $output['searchtime'];?>',
        colModel : [
            {display: '操作', name : 'operation', width : 60, sortable : false, align: 'center', className: 'handle-s'},
            {display: '序号', name : 'number', width : 100, sortable : false, align: 'center'},
            {display: '会员名称', name : 'statm_membername',  width : 150, sortable : false, align: 'center'},
            {display: '商品件数', name : 'orderamount',  width : 150, sortable : false, align: 'center'}
            ],
        buttons : [
            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'excel', bclass : 'csv', title : '导出EXCEL文件', onpress : fg_operation_goodsnum }
        ],
        usepager: true,
        rp: 15,
        title: '下单商品件数明细列表'
    });

	$('#searchBarOpen').click();
	$('#ncsubmit').click(function(){
    	$('#formSearch').submit();
    });
});
function fg_operation_ordernum(name, bDiv){
    fg_operation_excel('ordernum',bDiv);
}
function fg_operation_orderamount(name, bDiv){
    fg_operation_excel('orderamount',bDiv);
}
function fg_operation_goodsnum(name, bDiv){
    fg_operation_excel('goodsnum',bDiv);
}
//Flexigrid导出
function fg_operation_excel(stat_type,obj){
    var stat_url = 'index.php?act=stat_member&op=analyzeinfo&exporttype=excel&t=<?php echo $output['searchtime'];?>&type='+stat_type;
    get_excel(stat_url,obj);
}
//展示搜索时间框
function show_searchtime(){
	s_type = $("#search_type").val();
	$("[id^='searchtype_']").hide();
	$("#searchtype_"+s_type).show();
}

</script>