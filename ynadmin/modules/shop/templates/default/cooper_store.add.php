<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=cooper_store&op=index" title="返回合作商家列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>合作商家管理 - 新增</a></h3>
            </div>
        </div>
    </div>
    <form id="store_form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">合作商家图片</dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="show"><a class="nyroModal" rel="gal" href="#"> <i class="fa fa-picture-o" onMouseOver="toolTip()" onMouseOut="toolTip()"/></i> </a></span><span class="type-file-box">
            <input type="text" name="textfield2" id="textfield2" class="type-file-text" />
            <input type="button" name="button2" id="button2" value="选择上传..." class="type-file-button" />
            <input class="type-file-file" id="cooper_store_pic" name="cooper_store_pic" type="file" size="30" hidefocus="true" nc_type="change_cooper_store_pic" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
            </span></div>
                    <span class="err"></span>
                    <p class="notic">合作商家图片要求宽度为132px,高度为83px,支持gif,jpg,png</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>合作商家链接</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="" name="cooper_store_url" id="cooper_store_url" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">是否显示</dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="is_show1" class="cb-enable selected" ><?php echo $lang['nc_yes'];?></label>
                        <label for="is_show0" class="cb-disable" ><?php echo $lang['nc_no'];?></label>
                        <input id="is_show1" name="is_show" checked="checked" value="1" type="radio">
                        <input id="is_show0" name="is_show" value="0" type="radio">
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">排序</dt>
                <dd class="opt">
                    <input type="text" value="0" name="cooper_store_sort" id="cooper_store_sort" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">数字越小越靠前</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script>
    $(function(){
        $("#cooper_store_pic").change(function(){
            $("#textfield2").val($(this).val());
        });
        // 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("<?php echo $lang['default_img_wrong'];?>");
                $(this).attr('value','');
                return false;
            }
        });
// 点击查看图片
        $('.nyroModal').nyroModal();
        $('#time_zone').attr('value','');

        $("#submitBtn").click(function(){
            if($("#store_form").valid()){
                $("#store_form").submit();
            }
        });

        $("#store_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                cooper_store_url : {
                    required : true
                },
                cooper_store_sort : {
                    required : true,
                    number   : true
                }
            },
            messages : {
                cooper_store_url : {
                    required : '<i class="fa fa-exclamation-circle">合作商家链接不能为空</i>'
                },
                cooper_store_sort  : {
                    required : '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    number   : '<i class="fa fa-exclamation-circle"></i>请填写正确的排序'
                }
            }
        });
    });

</script>
