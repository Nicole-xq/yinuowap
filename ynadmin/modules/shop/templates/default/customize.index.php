<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>定制管理</h3>
        <h5>定制咨询内容展示</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
    </div>
    <ul>
      <li>查看用户提交的定制内容</li>
    </ul>
  </div>
<div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=customize&op=get_xml',
        colModel : [
            {display: 'ID', name : 'id', width : 50, sortable : true, align: 'center'},
            {display: '姓名', name : 'cus_name', width : 100, sortable : true, align: 'left'},
            {display: '手机号', name : 'cus_mobile', width: 100, sortable : true, align : 'center'},
            {display: '添加时间', name : 'add_time', width: 120, sortable : true, align : 'center'},
            {display: '用户名', name : 'member_name', width: 150, sortable : true, align : 'center'},
            {display: '用户ID', name : 'member_id', width: 100, sortable : true, align : 'center'},
            {display: '定制内容', name : 'content', width : 800, sortable : true, align: 'left'},
            ],
         searchitems : [
            {display: '姓名', name : 'cus_name', isdefault: true},
            {display: '手机号', name : 'cus_mobile'},
            ],
        title: '定制咨询内容列表'
    });
});

</script>