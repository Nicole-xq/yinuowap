<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="javascript:history.back(-1)" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>拍品保证金</h3>
                <h5>商城拍卖保证金订单管理</h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>提交后，相同支付单号的未支付订单状态都变为已支付状态</li>
        </ul>
    </div>
    <form method="post" name="form1" id="form1" action="index.php?act=<?php echo $_GET['act'];?>&op=change_state&state_type=ud_confirm&margin_id=<?php echo intval($_GET['margin_id']);?>">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" value="<?php echo getReferer();?>" name="ref_url">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="site_name">订单编号</label>
                </dt>
                <dd class="opt"><?php echo $output['order_info']['order_sn'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="site_name">订单总金额 </label>
                </dt>
                <dd class="opt"><?php echo ncPriceFormat($output['order_info']['margin_amount']);?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="site_name">支付凭证 </label>
                </dt>
                <dd class="opt">
                    <img src="<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_voucher'.DS.$output['order_info']['pay_voucher'];?>">
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="site_name">确认时间</label>
                </dt>
                <dd class="opt">
                    <input readonly id="payment_time" class="" name="payment_time" value="" type="text" />
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" id="ncsubmit" class="ncap-btn-big ncap-btn-green"><?php echo $lang['nc_submit'];?></a> </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#payment_time').datepicker({dateFormat: 'yy-mm-dd',maxDate: '<?php echo date('Y-m-d',TIMESTAMP);?>'});
        $('#ncsubmit').click(function(){
            if($("#form1").valid()){
                if (confirm("操作提醒：<?php echo $output['order_info']['order_state'] == ORDER_STATE_CANCEL ? '\n该订单处于关闭状态':'';?>\n该操作不可撤销\n提交前请务必确认是否已收到付款\n继续操作吗?")){
                }else{
                    return false;
                }
                $('#form1').submit();
            }
        });
        $("#form1").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                payment_time : {
                    required : true
                }
            },
            messages : {
                payment_time : {
                    required : '<i class="fa fa-exclamation-circle"></i>请填写付款准确时间'
                }
            }
        });
    });
</script>