<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <a class="back" href="index.php?act=auctions_class&op=index" title="返回分类列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>艺术家- 新增</a></h3>
        <h5>拍品艺术家新增</h5>
      </div>
    </div>
  </div>
  <form id="add_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="auctions_artist_name"><em>*</em>艺术家名称</label>
        </dt>
        <dd class="opt">
          <input type="text" id="auctions_artist_name" name="auctions_artist_name" class="input-txt" >
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
            <dt class="tit">
                <label><em></em>艺术家头像</label>
            </dt>
            <dd class="opt">
              <span class="type-file-box">
                <div class="input-file-show"><span class="type-file-box">
        <input type="file" class="type-file-file" id="auctions_artist_img" name="auctions_artist_img" size="30" hidefocus="true"  nc_type="upload_activity_label" title="点击按钮选择文件并提交表单后上传生效">
        </span>
      </div>

                <span class="err"></span>
                <input type="hidden" name="special_pic" id="special_pic" value=""/>
            </dd>
        </dl>

      <dl class="row">
            <dt class="tit">
                <label><em></em>艺术家简介</label>
            </dt>
            <dd class="opt">
                <textarea style="width: 290px; height: 239px;" name="auction_artist_summary"></textarea>
            </dd>
        </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><span><?php echo $lang['nc_submit'];?></span></a></div>
    </div>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){
  $('#add_form').validate({
    errorPlacement: function(error, element){
      var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      auctions_artist_name: {
        required : true,
      }      
    },
    messages : {      
      auctions_artist_name : {
        required : '<i class="fa fa-exclamation-circle"></i>必填',               
      }
    }
  });
  $("#submitBtn").click(function(){
    if($("#add_form").valid()){
     $("#add_form").submit();
	   }
	});
  
  var textButton1="<input type='text' name='textfield' id='textfield2' class='type-file-text' /><input type='button' name='button2' id='button2' value='选择上传...' class='type-file-button' />"
        $(textButton1).insertBefore("#auctions_artist_img");
        $("#auctions_artist_img").change(function(){
            $("#textfield2").val($("#auctions_artist_img").val());
            $('#adv_pic').val($("#auctions_artist_img").val());
        });
});
</script>