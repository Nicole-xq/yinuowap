<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['store_grade'];?> </h3>
        <h5><?php echo $lang['store_grade_subhead'];?></h5>
      </div>
    </div>
  </div>
    <div id="flexigrid"></div>
</div>
<script>

    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=store_grade&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '级别', name : 'sg_sort', width : 80, sortable : true, align: 'center'},
                {display: '等级名称', name : 'sg_name', width : 120, sortable : false, align: 'center'},
                {display: '可发布商品数', name : 'sg_goods_limit', width: 120, sortable : false, align : 'center'},
                {display: '可上传图片数', name : 'sg_album_limit', width: 120, sortable : false, align : 'center'},
                {display: '可选模板套数', name : 'sg_template_number', width : 120, sortable : true, align: 'center'},
                {display: '收费标准', name : 'sg_price', width : 120, sortable : true, align: 'center'},
            ],
            searchitems : [
                {display: '级别', name : 'sg_sort', isdefault: true},
                {display: '等级名称', name : 'sg_name'},
            ],
            columnControl: false,// 不使用列控制
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', onpress : fg_operation }
            ],
            sortname: "sg_sort",
            sortorder: "desc",
            title: '店铺等级列表'
        });
    });

    function fg_operation(name, grid) {
        if (name == 'add') {
            window.location.href = 'index.php?act=store_grade&op=store_grade_add';
        }
    }
</script> 