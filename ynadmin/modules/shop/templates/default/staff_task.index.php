<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>内部员工业绩统计</h3>
                <h5></h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?=$lang['nc_prompts_title'];?>"><?=$lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?=$lang['nc_prompts_span']?>"></span> </div>
        <ul>
            <li>统计付款成功的（保证金 | 商品）订单。</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" name="formSearch" id="formSearch">
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dd>
                            <label>
                                <input type="text" value="" placeholder="请输入会员名称" name="keyword" class="s-input-txt">
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>日期筛选</dt>
                        <dd>
                            <label>
                                <input readonly id="query_start_date" placeholder="请选择起始时间" name=query_start_date value="" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <span id="datetime_notice" style="color: red"></span>
                            </label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom">
                <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a>
                <a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容">
                    <i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?>
                </a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#query_start_date').datepicker();
        $('#query_end_date').datepicker();
        // 高级搜索提交
        $('#ncsubmit').click(function(){
            var start = $('#query_start_date').val();
            var end = $('#query_end_date').val();
            var timestamp1 = Date.parse(new Date(start));
            var timestamp2 = Date.parse(new Date(end));

            if(timestamp1 && timestamp2 && (timestamp1 > timestamp2)){
                $("#datetime_notice").text('开始时间超过结束时间');
            } else {
                $("#datetime_notice").text('');
                $("#flexigrid").flexOptions({url: 'index.php?act=staff_task&op=get_xml&'+$("#formSearch").serialize(),query:'',qtype:''}).flexReload();
            }
        });
        // 高级搜索重置
        $('#ncreset').click(function(){
            $("#flexigrid").flexOptions({url: 'index.php?act=staff_task&op=get_xml'}).flexReload();
            $("#formSearch")[0].reset();
        });
        $("#flexigrid").flexigrid({
            url: 'index.php?act=staff_task&op=get_xml',
            colModel : [
                {display: '会员ID', name : 'member_id', width : 60, sortable : false, align: 'center'},
                {display: '会员名称', name : 'member_name', width : 150, sortable : false, align: 'center'},
                {display: '会员昵称', name : 'member_truename', width : 150, sortable : false, align: 'center'},
                {display: '下一级注册数量', name : 'distribute_lv_1', width : 100, sortable : false, align: 'right'},
                {display: '下二级注册数量', name : 'distribute_lv_2', width : 100, sortable : false, align: 'right'},
                {display: '下一级保证金总额', name : 'bottom_margin', width : 120, sortable : false, align: 'right'},
                {display: '下二级保证金总额', name : 'bottom_margin_2', width : 120, sortable : false, align: 'right'},
                {display: '下一级订单总额', name : 'bottom_order', width : 120, sortable : false, align: 'right'},
                {display: '下二级订单总额', name : 'bottom_order_2', width : 120, sortable : false, align: 'right'}
            ],
            searchitems : [
                {display: '会员名称', name : 'member_name'}
            ],
            title: '统计列表'
        });
    });
</script>
