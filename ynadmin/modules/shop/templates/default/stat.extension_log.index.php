<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript：void(0);" onclick="window.history.back()" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>推广统计</h3>
                <h5>平台针对通过渠道的公众号关注、取关人数统计</h5>
            </div>
        </div>
    </div>
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title']; ?>"><?php echo $lang['nc_prompts']; ?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span']; ?>"></span></div>
        <ul>
            <li>总关注从始至今的还在关注人数</li>
            <li>月(关注|取消)记录从本月初至今的关注量，每月底会自动为0。</li>
        </ul>
    </div>

    <div class="ncap-form-all ncap-stat-general-single">
        <div class="title">
            <h3>关注情况一览 <span>用户ID：<?php echo $output['member_id']; ?></span></h3>
        </div>
        <dl class="row">
            <dd class="opt">
                <?php echo $output['stat_content']; ?>
            </dd>
        </dl>
    </div>

    <div class="ncap-stat-chart">
        <div class="title">
            <h3><?php echo $output['stat_time']; ?> 关注走势</h3>
        </div>
        <div id="container" class=" " style="height:400px"></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL ?>/js/statistics.js"></script>
<script>
    $(function () {
        $('#container').highcharts(<?php echo $output['stat_month_json'];?>);
    });
</script>