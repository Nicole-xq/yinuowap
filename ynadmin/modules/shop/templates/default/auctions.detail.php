<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .name {
        width: 10% !important;
        display:block;float:left;
    }

    .number1 {
        width: 90% !important;
        display:block;float:left;
    }

    .fr {
        float: right !important;
        display: inline;
        width:45%;
    }
    .fr span {
        text-align: right;
        font-size: 14px;
        margin-right: 20px;
        display:block;float:right;
    }
    .fr p {
        margin: 0 0 4px;
        line-height: 21px;
        height:84px;
    }
    .fr h2 {
        font-size: 24px;
        text-align: center;
        color: #000;
        margin: 0 0 12px;
        margin-top:10px;
    }
    .works-list {
        width: 100%;
        border: #f3f3f3 solid 1px;
        float: left;
        margin: 0 0 1px 1px;
    }
    .store-joinin tbody td img{
        max-width: 180px !important;
        max-height: 168px !important;
    }


</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=auctions" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>拍品管理 - 查看商家“<?php echo $output['auctions_info']['store_name'];?>”的专场信息</h3>
            </div>
        </div>
    </div>
    <form id="form_store_verify" enctype="multipart/form-data" action="index.php?act=auctions&op=auctions_edit" method="post">
        <input id="verify_type" name="verify_type" type="hidden" />
        <input name="auction_id" type="hidden" value="<?php echo $output['auctions_info']['auction_id'];?>" />
        <table border="0" cellpadding="0" cellspacing="0" class="store-joinin">
            <thead>
            <tr>
                <th colspan="20">专场基本信息</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th class="w150">店铺名称：</th>
                <td><?php echo $output['auctions_info']['store_name'];?></td>
            </tr>
            <tr>
                <th>拍品名称：</th>
                <td>
                    <input name="auction_name" id="auction_name" class="text w150 hasDatepicker" type="text" value="<?php echo $output['auctions_info']['auction_name'];?>">
                </td>
            </tr>
            <tr>
                <th>拍品图片：</th>
                <td>
                    <dl>
                        <dt>
                        </dt>
                        <dd>
                            <div class="input-file-show">
                                <span class="show">
                                    <a class="nyroModal" rel="gal" href="<?= getVendueLogo($output['auctions_info']['auction_image'])?>">
                                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?= getVendueLogo($output['auctions_info']['auction_image']);?>>')" onMouseOut="toolTip()"/></i>
                                    </a>
                                </span>
                                <span class="type-file-box">
                                    <input type="text" name="textfield" id="textfield1" class="type-file-text" />
                                    <input type="button" name="button" id="button1" value="选择更换..." class="type-file-button" />
                                    <input class="type-file-file" id="auctions_img" name="auctions_img" type="file" size="30" hidefocus="true" nc_type="change_special_logo" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                                </span>
                            </div>
                            <span class="err"></span>
                            <p class="notic">建议上传图片大小的尺寸为1920px*300px</p>
                        </dd>
                    </dl>
                </td>
            </tr>
            <tr>
                <th>起拍价</th>
                    <td><input name="auction_start_price" value="<?=$output['auctions_info']['auction_start_price'];?>" type="text" class="text w150" /></td>
            </tr>
            <tr>
                <th>加价幅度</th>
                    <td><input name="auction_increase_range" value="<?=$output['auctions_info']['auction_increase_range'];?>" type="text" class="text w150" /></td>
            </tr>
            <tr>
                <th>保留价</th>
                    <td><input name="auction_reserve_price" value="<?=$output['auctions_info']['auction_reserve_price'];?>" type="text" class="text w150" /></td>
            </tr>
            <tr>
                <th>保证金</th>
                <td><?=$output['auctions_info']['auction_bond'];?> 元</td>
            </tr>
            <tr>
                <th>报名人数</th>
                <td><input name="num_of_applicants" value="<?=$output['auctions_info']['num_of_applicants'];?>" type="text" class="text w150" /></td>
            </tr>
            <tr>
                <th>提醒人数</th>
                <td><input name="set_reminders_num" value="<?=$output['auctions_info']['set_reminders_num'];?>" type="text" class="text w150" /></td>
            </tr>
            <tr>
                <th>围观次数</th>
                <td><input name="auction_click" value="<?=$output['auctions_info']['auction_click'];?>" type="text" class="text w150" /></td>
            </tr>
            </tbody>
        </table>
        <div id="validation_message" style="color:red;display:none;"></div>
        <div class="bottom">
            <a id="btn_save" class="ncap-btn-big ncap-btn-green mr10" href="JavaScript:void(0);">保存修改</a>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js" charset="utf-8"></script>
<script type="text/javascript">

    $(document).ready(function(){

        $('a[nctype="btn_submit"]').click(function(){
            if($("#form1").valid()) ajaxpost('form1', '', '', 'onerror');
        });

        $('a[nctype="nyroModal"]').nyroModal();

        $('#btn_save').on('click', function() {
            $('#validation_message').hide();
            if(confirm('确认保存信息？')) {
                $('#verify_type').val('save');
                $('#form_store_verify').submit();
            }
        });
    });

    // 模拟网站LOGO上传input type='file'样式
    $(function(){
        $("#special_logo").change(function(){
            $("#textfield1").val($(this).val());
        });
        // 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("图片限于png,gif,jpeg,jpg格式");
                $(this).attr('value','');

                return false;
            }
        });
        // 点击查看图片
        $('.nyroModal').nyroModal();
    });
</script>