<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>艺术家管理</h3>
                <h5></h5>
            </div>
        </div>
    </div>
    <div id="flexigrid"></div>    
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=auctions_artist&op=get_xml&type=<?php echo $output['type'];?>',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: 'ID', name : 'auction_id', width : 60, sortable : true, align: 'center'},
                {display: '艺术家名字', name : 'auction_name', width : 150, sortable : false, align: 'left'},                
                {display: '艺术家头像', name : 'auction_image', width : 60, sortable : true, align: 'center'},
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', onpress : fg_operation },
            ],
            searchitems : [
                {display: '拍品名称', name : 'auctions_artist_name'},
            ],
            sortname: "auctions_artist_id",
            sortorder: "desc",
            title: '艺术家列表'
        });
    });
    // 删除
    function fg_del(id) {
        if(confirm('删除后将不能恢复，确认删除这项吗？')){
            $.getJSON('index.php?act=auctions_artist&op=auctions_artist_del', {id:id}, function(data){
                if (data.state) {
                    $("#flexigrid").flexReload();
                } else {
                    showError(data.msg)
                }
            });
        }
    }
    function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=auctions_artist&op=auctions_artist_add';
    }
}
</script>
