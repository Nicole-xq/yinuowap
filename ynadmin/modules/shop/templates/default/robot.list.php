<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>机器人账号管理</h3>
        <h5>商城的机器人账号管理</h5>
      </div>
      <?php echo $output['top_link'];?>
    </div>
  </div>
  <div id="flexigrid"></div>
</div>
<script>
function update_flex(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=robot&op=get_robot_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 60, sortable : false, align: 'center', className: 'handle-s'},
            {display: '机器人名称', name : 'robot_name', width : 150, sortable : false, align: 'center'},
            {display: '添加时间', name : 'update_time', width : 150, sortable : false, align: 'center'}
            ],
        buttons : [
            {display: '<i class="fa fa-plus"></i>新增机器人账号', name : 'add', bclass : 'add', title : '新增机器人账号', onpress : fg_operation_add }
        ],
        searchitems : [
            {display: '机器人名称', name : 'robot_name'}
            ],
        usepager: true,
        rp: 15,
        title: '机器人账号列表'
    });
}
function fg_operation_add(name, bDiv){
    var _url = 'index.php?act=robot&op=add';
    window.location.href = _url;
}

$(function(){
	update_flex();
});

</script>