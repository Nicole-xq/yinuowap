<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>保证金列表</h3>
        <h5>保证金列表</h5>
      </div>
    </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li><?php echo $lang['type_index_prompts_one'];?></li>
    </ul>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=margin_list&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: 'ID', name : 'margin_list_id', width : 40, sortable : true, align: 'center'},
            {display: '编号', name : 'list_sn', width : 120, sortable : false, align: 'center'},
            {display: '买家ID', name : 'buyer_id', width : 120, sortable : true, align: 'center'},
            {display: '买家名字', name : 'buyer_name', width : 150, sortable : false, align: 'center'},
            {display: '总金额', name : 'all_money', width : 150, sortable : false, align: 'center'},
            {display: '支付次数', name : 'pay_num', width : 150, sortable : false, align: 'center'},
            {display: '最后支付方式', name : 'last_payment_code', width : 150, sortable : false, align: 'center'},
            {display: '最后支付时间', name : 'last_pay_time', width : 150, sortable : false, align: 'center'},
            {display: '作品信息', name : 'buyer_name', width : 150, sortable : false, align: 'center'},
            {display: '关联专场', name : 'buyer_name', width : 150, sortable : false, align: 'center'},
            ],
        sortname: "margin_list_id",
        sortorder: "asc",
        title: '保证金列表'
    });
});

function fg_operation(name, bDiv) {
    if (name == 'add') {
        window.location.href = 'index.php?act=margin_list&op=auctions_class_add';
    }
}
function fg_del(id) {
    if(confirm('删除后将不能恢复，确认删除这项吗？')){
        $.getJSON('index.php?act=auctions_class&op=auctions_class_del', {id:id}, function(data){
            if (data.state) {
                $("#flexigrid").flexReload();
            } else {
                showError(data.msg)
            }
        });
    }
}
</script>