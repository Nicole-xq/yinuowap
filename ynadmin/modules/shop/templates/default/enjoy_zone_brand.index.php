<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3><?php echo $lang['brand_index_brand'];?></h3>
        <h5><?php echo $lang['brand_index_brand_subhead'];?></h5>
      </div>
      <ul class="tab-base nc-row">
        <li><a href="index.php?act=brand"><?php echo $lang['nc_manage'];?></a></li>
        <li><a href="index.php?act=brand&op=brand_apply"><?php echo $lang['brand_index_to_audit'];?></a></li>
        <li><a href="index.php?act=brand&op=enjoy_zone_branch" class="current"><?php echo $lang['brand_index_enjoy_zone'];?></a></li>
      </ul>
    </div>
  </div>
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
  </div>
  <div id="flexigrid"></div>
</div>
<script type="text/javascript">
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=brand&op=get_enjoy_zone_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
            {display: '品牌ID', name : 'brand_id', width : 40, sortable : true, align: 'center'},
            {display: '品牌名称', name : 'brand_name', width : 150, sortable : false, align: 'left'},
            {display: '首字母', name : 'brand_initial', width : 120, sortable : true, align: 'center'},
            {display: '品牌图片', name : 'brand_pic', width : 120, sortable : false, align: 'left'},
            {display: '品牌排序', name : 'brand_enjoy_zone_sort', width: 60, sortable : true, align : 'center'},
            {display: '品牌推荐', name : 'brand_recommend', width: 60, sortable : true, align : 'center'},
            {display: '展示形式', name : 'show_type', width : 80, sortable : true, align: 'center'}
            ],
        searchitems : [
            {display: '品牌ID', name : 'brand_id', isdefault: true},
            {display: '品牌名称', name : 'brand_name'},
            {display: '首字母', name : 'brand_initial'}
            ],
        sortname: "brand_enjoy_zone_sort",
        sortorder: "desc",
        title: '品牌列表',
    });
});


</script>
