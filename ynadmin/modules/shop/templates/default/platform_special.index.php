<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>平台专场管理</h3>
                <h5>平台商城管理与店铺申请商品审核</h5>
            </div>
            <?php echo $output['top_link'];?>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>
<script>
    $(function(){
        var flexUrl = 'index.php?act=auction_special&op=platform_special_xml';

        $("#flexigrid").flexigrid({
            url: flexUrl,
            colModel: [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '专场ID', name : 'special_id', width : 120, sortable : false, align: 'left'},
                {display: '专场名称', name : 'special_name', width : 120, sortable : true, align: 'left'},
                {display: '专场缩略图', name : 'special_image', width: 60, sortable : false, align : 'center'},
                {display: '专场开始时间', name : 'special_start_time', width : 150, sortable : true, align: 'center'},
                {display: '专场结束时间', name : 'special_end_time', width : 150, sortable : true, align: 'center'},
                {display: '预展开始时间', name : 'special_preview_start', width : 150, sortable : true, align: 'center'},
                {display: '专场申请时间', name : 'special_add_time', width : 150, sortable : true, align: 'center'},
                {display: '状态', name: 'is_open', width: 80, sortable: false, align: 'center'}
            ],
            buttons: [
                {
                    display: '<i class="fa fa-plus"></i>新增专场',
                    name: 'add',
                    bclass: 'add',
                    title: '平台发起新专场',
                    onpress: function() {
                        location.href = 'index.php?act=auction_special&op=new';
                    }
                },
                {
                    display: '<i class="fa fa-trash"></i>批量删除',
                    name: 'del',
                    bclass: 'del',
                    title: '将选定行数据批量删除',
                    onpress: function() {
                        var ids = [];
                        $('.trSelected[data-id]').each(function() {
                            ids.push($(this).attr('data-id'));
                        });
                        if (ids.length < 1 || !confirm('确定删除?')) {
                            return false;
                        }
                        var href = 'index.php?act=auction_special&op=del&special_id=__IDS__'.replace('__IDS__', ids.join(','));

                        $.getJSON(href, function(d) {
                            if (d && d.result) {
                                $("#flexigrid").flexReload();
                            } else {
                                alert(d && d.message || '操作失败！');
                            }
                        });
                    }
                }
            ],
            searchitems: [
                {display: '专场名称', name : 'special_name', isdefault: true}
            ],
            sortname: "special_id",
            sortorder: "desc",
            title: '平台专场列表'
        });


        $("input[data-dp='1']").datepicker({dateFormat: 'yy-mm-dd'});

    });
    $('a[data-href]').live('click', function() {
        if ($(this).hasClass('confirm-del-on-click') && !confirm('确定删除?')) {
            return false;
        }

        $.getJSON($(this).attr('data-href'), function(d) {
            if (d && d.result) {
                $("#flexigrid").flexReload();
            } else {
                alert(d && d.message || '操作失败！');
            }
        });
    });




</script>
