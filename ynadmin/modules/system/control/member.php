<?php
/**
 * 会员管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class memberControl extends SystemControl{
    const EXPORT_SIZE = 1000;

//    private $links = array(
//        array('url'=>'act=member&op=index','text'=>'所有会员'),
//        array('url'=>'act=member&op=wd_index&type=1','text'=>'微合伙人升级'),
//        array('url'=>'act=member&op=zl_index&type=2','text'=>'战略合伙人升级'),
//        array('url'=>'act=member&op=sy_index&type=3','text'=>'事业合伙人升级'),
//    );

    public function __construct(){
        parent::__construct();
        Language::read('member');
    }

    public function indexOp() {
//        Tpl::output('top_link',$this->sublink($this->links,'index'));
        $this->memberOp();
    }

    public function dz_indexOp() {
//        Tpl::output('top_link',$this->sublink($this->links,'zl_index'));
        $this->memberOp();
    }

    public function zl_indexOp() {
//        Tpl::output('top_link',$this->sublink($this->links,'zl_index'));
        $this->memberOp();
    }

    public function wd_indexOp() {
//        Tpl::output('top_link',$this->sublink($this->links,'wd_index'));
        $this->memberOp();
    }

    public function sy_indexOp() {
//        Tpl::output('top_link',$this->sublink($this->links,'sy_index'));
        $this->memberOp();
    }



    /**
     * 会员管理
     */
    public function memberOp(){
        Tpl::showpage('member.index');
    }

    /**
     * 会员修改
     */
    public function member_editOp(){
        $lang   = Language::getLangContent();
        /** @var memberModel $model_member */
        $model_member = Model('member');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
            array("input"=>$_POST["member_email"], "require"=>"true", 'validator'=>'Email', "message"=>$lang['member_edit_valid_email']),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $update_array = array();
                $update_array['member_id']          = intval($_POST['member_id']);
                if (!empty($_POST['member_passwd'])){
                    $update_array['member_passwd'] = md5($_POST['member_passwd']);
                }
                $update_array['member_email']       = $_POST['member_email'];
                $update_array['member_truename']    = $_POST['member_truename'];
                $update_array['member_sex']         = $_POST['member_sex'];
                $update_array['member_qq']          = $_POST['member_qq'];
                $update_array['member_ww']          = $_POST['member_ww'];
                $update_array['member_state']       = $_POST['memberstate'];
                if(isset($_POST['member_role'])){
                    $update_array['member_role']        = (int)$_POST['member_role'];
                }
                if(isset($_POST['member_type']) && is_numeric($_POST['member_type'])){
                    $update_array['member_type']        = intval($_POST['member_type']);
                }else{
                    $bind_name = trim($_POST['bind_member']);
                    if($bind_name != ''){
                        $condition['member_name']   = $bind_name;
                        $bind_member_info = $model_member->getMemberInfo($condition);
                        $update_array['bind_member_id'] = $bind_member_info['member_id'];
                        $update_array['bind_member_name'] = $bind_member_info['member_name'];
                        $update_array['bind_time'] = time();
                    }
                }
                if (!empty($_POST['member_avatar'])){
                    $update_array['member_avatar'] = $_POST['member_avatar'];
                }
                $result = $model_member->editMember(array('member_id'=>intval($_POST['member_id'])),$update_array);
                if ($result){
                    $url = array(
                    array(
                    'url'=>'index.php?act=member&op=member',
                    'msg'=>$lang['member_edit_back_to_list'],
                    ),
                    array(
                    'url'=>'index.php?act=member&op=member_edit&member_id='.intval($_POST['member_id']),
                    'msg'=>$lang['member_edit_again'],
                    ),
                    );
                    $this->log(L('nc_edit,member_index_name').'[ID:'.$_POST['member_id'].']',1);
                    isset($update_array['member_type']) ? $model_member->editMemberDistribute(['member_type'=>intval($_POST['member_type'])], ['member_id'=>intval($_POST['member_id'])]) : '';
                    showMessage($lang['member_edit_succ'],$url);
                }else {
                    showMessage($lang['member_edit_fail']);
                }
            }
        }
        $condition['member_id'] = intval($_GET['member_id']);
        $member_array = $model_member->getMemberInfo($condition);
        $distribution_config_list = Model('distribution_config')->getConfigList();
        $member_role_list = C('member_role')?unserialize(C('member_role')):array();
        Tpl::output('distribution_list',$distribution_config_list);
        Tpl::output('member_array',$member_array);
        Tpl::output('member_role_list',$member_role_list);
        Tpl::showpage('member.edit');
    }

    /**
     * 新增会员
     */
    public function member_addOp(){
        $lang   = Language::getLangContent();
        $model_member = Model('member');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["member_name"], "require"=>"true", "message"=>$lang['member_add_name_null']),
                array("input"=>$_POST["member_passwd"], "require"=>"true", "message"=>'密码不能为空'),
                array("input"=>$_POST["member_email"], "require"=>"true", 'validator'=>'Email', "message"=>$lang['member_edit_valid_email'])
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['member_name']    = trim($_POST['member_name']);
                $insert_array['member_passwd']  = trim($_POST['member_passwd']);
                $insert_array['member_email']   = trim($_POST['member_email']);
                $insert_array['member_truename']= trim($_POST['member_truename']);
                $insert_array['member_sex']     = trim($_POST['member_sex']);
                $insert_array['member_type']    = 0;
                $insert_array['member_qq']      = trim($_POST['member_qq']);
                $insert_array['member_ww']      = trim($_POST['member_ww']);
                //默认允许举报商品
                $insert_array['inform_allow']   = '1';
                if (!empty($_POST['member_avatar'])){
                    $insert_array['member_avatar'] = trim($_POST['member_avatar']);
                }

                $result = $model_member->addMember($insert_array);
                if ($result){
                    $url = array(
                    array(
                    'url'=>'index.php?act=member&op=member',
                    'msg'=>$lang['member_add_back_to_list'],
                    ),
                    array(
                    'url'=>'index.php?act=member&op=member_add',
                    'msg'=>$lang['member_add_again'],
                    ),
                    );
                    $this->log(L('nc_add,member_index_name').'[ '.$_POST['member_name'].']',1);
                    showMessage($lang['member_add_succ'],$url);
                }else {
                    showMessage($lang['member_add_fail']);
                }
            }
        }
        Tpl::showpage('member.add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 验证会员是否重复
             */
            case 'check_user_name':
                $model_member = Model('member');
                $condition['member_name']   = $_GET['member_name'];
                $condition['member_id'] = array('neq',intval($_GET['member_id']));
                $list = $model_member->getMemberInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
            /**
             * 验证邮件是否重复
             */
            case 'check_email':
                $model_member = Model('member');
                $condition['member_email'] = $_GET['member_email'];
                $condition['member_id'] = array('neq',intval($_GET['member_id']));
                $list = $model_member->getMemberInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
            /**
             * 验证用户是否是存在
             */
            case 'check_member':
                $model_member = Model('member');
                $condition['member_name']   = trim($_GET['bind_member']);
                $list = $model_member->getMemberInfo($condition);
                if (!empty($list) && in_array($list['member_type'],array('2','3')) || trim($_GET['bind_member']) == ''){
                    echo 'true';exit;
                } else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_member = Model('member');
        $member_grade = $model_member->getMemberGradeArr();
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('member_id','member_name','member_avatar','member_email','member_mobile','member_sex','member_truename','td_amount'
                ,'member_time','member_login_time','member_login_ip','member_points','member_exppoints','member_grade','available_predeposit'
                ,'freeze_predeposit','available_rc_balance','freeze_rc_balance','inform_allow','is_buy','is_allowtalk','member_state','member_type','is_artist'
        );
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];

        $update_lev = false;
        if($query_type = $_REQUEST['type']){

            $distribute_info = model('member_distribute_type')->where(array('price' => array('gt', 0)))->order('price asc')->select();

            $distribute_ids = array(4);
            $update_lev = array();
            $check = false;
            foreach($distribute_info as $v){

                if($query_type == $v['type_id']){
                    $condition['td_amount'] = array('egt', $v['price']);
                    $check = true;
                }

                if($check){
                    $distribute_ids[] = $v['type_id'];
                    $update_lev[] = $v;
                }
            }

            $condition['member_type'] = array('not in', $distribute_ids);
        }

        $member_list = $model_member->getMemberList($condition, '*', $page, $order);

        $sex_array = $this->get_sex();

        $type_arr = $this->_getMemberType();

        $data = array();
        $data['now_page'] = $model_member->shownowpage();
        $data['total_num'] = $model_member->gettotalnum();


        foreach ($member_list as $value) {

            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=member&op=member_edit&member_id=" . $value['member_id'] . "&type=".$_REQUEST['type']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['operation'] .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>";
            $param['operation'] .= "<li><a href='javascript:void(0)' onclick=\"member_edit_distribute('" . $value['member_id'] . "')\">更改上级</a></li>";
            $param['operation'] .= "<li><a href='javascript:void(0)' onclick=\"member_edit_extension('" . $value['member_id'] . "')\">编辑推广渠道</a></li>";
            if($update_lev){
                foreach($update_lev as $lev_row){
                    if($value['td_amount'] >= $lev_row['price']){
                        $url = 'index.php?act=member&op=upgrade_lv&member_id='.$value['member_id'].'&type_id='.$lev_row['type_id'];
                        $href_str = "javascript:if(confirm('您确定要升级吗？'))window.location = '".$url."'";
                        $param['operation'] .= "<li><a href=\"" . $href_str . "\" >升级".$lev_row['name']."</a></li>";
                    }
                }
            }
            $param['operation'] .= "</ul>";

            $top_member = $model_member->getUpperMember(array('member_id' => $value['member_id']));
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = "<img src=".getMemberAvatarForID($value['member_id'])." class='user-avatar' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getMemberAvatarForID($value['member_id']).">\")'>".$value['member_name'];
            $param['member_type'] = $type_arr[$value['member_type']];
            $param['member_role'] = getRoleName($value['member_role']);
            $param['is_artist'] = $value['is_artist'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $param['top_member_name'] = $top_member['top_member_name'];
            $param['member_email'] = $value['member_email'];
            $param['member_mobile'] = $value['member_mobile'];
            $param['member_sex'] = $sex_array[$value['member_sex']];
            $param['member_truename'] = $value['member_truename'];
            $param['td_amount'] = $value['td_amount'];
            $param['member_time'] = date('Y-m-d H:i:s', $value['member_time']);
            $param['member_login_time'] = date('Y-m-d H:i:s', $value['member_login_time']);
            $param['member_login_ip'] = $value['member_login_ip'];
            $param['member_state'] = $value['member_state'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $data['list'][$value['member_id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }
    /**
     * 性别
     * @return multitype:string
     */
    private function get_sex() {
        $array = array();
        $array[1] = '男';
        $array[2] = '女';
        $array[3] = '保密';
        return $array;
    }

    /**
     * 会员类型
     */
    private function _getMemberType(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $arr = array();
        foreach($re as $v){
            $arr[$v['type_id']] = $v['name'];
        }
        return $arr;
    }

    /**
     * 变更上级用户
     */
    public function member_edit_distributeOp() {
        $lang   = Language::getLangContent();

        $model_member = Model('member');
        $model_member_commission = Model('member_commission');

        if(chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["member_id"], "require"=>"true", "message"=>'参数错误'),
                array("input"=>$_POST["change_top_member_id"], "require"=>"true", "message"=>$lang['member_edit_null']),
                array("input"=>$_POST["change_top_member_id"], "number"=>"true", "message"=>'参数错误'),
            );
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage($error);
            }else{
                $member_id = trim($_POST['member_id']);
                $new_top_id = trim($_POST['change_top_member_id']);

                if($member_id == $new_top_id){
                    showMessage('不能将该用户变更为该用户上级');
                }
/*                if($new_top_id > $member_id){
                    showMessage('<span style="font-size: 14px">您要变更的上级注册时间晚，不能改为上级</span>');
                }*/
                //用户信息
                $member_info = $model_member->getUpperMember(['member_id'=>$member_id]);
                if(empty($member_info)){
                    showMessage('参数错误');
                }

                $common_info = $model_member_commission->getCommissionInfo(['from_member_id'=>$member_id,'dis_commis_state'=>0]);
                if(!empty($common_info)){
                    showMessage('该用户有待返佣订单');
                }

                $common_info = $model_member_commission->getCommissionInfo(['dis_member_id'=>$member_id,'top_lv'=>1,'dis_commis_state'=>0]);
                if(!empty($common_info)){
                    showMessage('该用户下级有待返佣订单');
                }

                //新上级信息
                $new_top_info = $model_member->getUpperMember(['member_id'=>$new_top_id]);
                if(empty($new_top_info)){
                    showMessage('将要变更的上级不存在');
                }

                try{
                    $model_member->beginTransaction();

                    //变更用户的上级
                    $edit_array = [];
                    $edit_array['top_member'] = $new_top_info['member_id'];
                    $edit_array['top_member_2'] = $new_top_info['top_member'];
                    $edit_array['top_member_name'] = $new_top_info['member_name'];
                    $edit_array['member_comment'] = null;
                    $edit_array['member_comment_2'] = null;
                    $update = $model_member->editMemberDistribute($edit_array,['member_id'=>$member_id]);
                    if(!$update){
                        throw new Exception();
                    }

                    //变更用户下一级的上二级
                    $edit_array = [];
                    $edit_array['top_member_2'] = $new_top_info['member_id'];
                    $edit_array['member_comment_2'] = null;
                    $update = $model_member->editMemberDistribute($edit_array,['top_member'=>$member_id]);
                    if(!$update){
                        throw new Exception();
                    }

                    //获取用户的下一级总数
                    $condition = ['top_member' => $member_id];
                    $count_distribute = $model_member->getCountDistribute($condition);

                    // 变更用户原上级的lv和lv_2数量
                    $edit_array = array(
                        'distribute_lv_1'=>array('exp','distribute_lv_1-1'),
                        'distribute_lv_2'=>array('exp',"distribute_lv_2-". $count_distribute),
                    );
                    $update = $model_member->editMember(['member_id'=>$member_info['top_member']], $edit_array);
                    if(!$update){
                        throw new Exception();
                    }

                    // 变更用户新上级的lv和lv_2数量
                    $edit_array = array(
                        'distribute_lv_1'=>array('exp','distribute_lv_1+1'),
                        'distribute_lv_2'=>array('exp',"distribute_lv_2+". $count_distribute),
                    );
                    $update = $model_member->editMember(['member_id'=>$new_top_id], $edit_array);
                    if(!$update){
                        throw new Exception();
                    }

                    $insert_array = [];
                    $insert_array['member_id'] = $member_id;
                    $insert_array['member_name'] = $member_info['member_name'];
                    $insert_array['old_top_member_id'] = $member_info['top_member'];
                    $insert_array['old_top_member_name'] = $member_info['top_member_name'];
                    $insert_array['change_top_member_id'] = $new_top_info['member_id'];
                    $insert_array['change_top_member_name'] = $new_top_info['member_name'];
                    $insert_array['admin_id'] = $this->admin_info['id'];
                    $insert_array['admin_name'] = $this->admin_info['name'];
                    $insert_array['msg'] = $_POST['change_msg'];
                    $insert_array['add_time'] = TIMESTAMP;

                    $update = $model_member->table('top_member_log')->insert($insert_array);
                    if(!$update){
                        throw new Exception();
                    }

                    $model_member->commit();
                    $error = '修改成功';
                }catch (Exception $e) {
                    $model_member->rollback();
                    $error = '修改失败，请联系管理员';
                }

                showMessage($error);
            }

            exit();
        }

        $error = '参数错误';
        $member_id = trim($_GET['member_id']);
        if(empty($member_id)){
            showMessage($error);
        }

        $member_info = $model_member->getUpperMember(array('member_id' => $member_id), '*');

        if (empty($member_info)) {
            showMessage($error);
        }

        $top_info = [];
        if(!empty($member_info['top_member']) && $member_info['top_member'] > 0){
            $top_info = $model_member->getMemberInfo(array('member_id' => $member_info['top_member']), 'member_id,member_name');
        }

        $data = [
            'member_id' => $member_id,
            'member_name' => $member_info['member_name'],
            'old_top_member_id' => $top_info['member_id'],
            'old_top_member_name' => $top_info['member_name'],
        ];

        Tpl::output('edit_info', $data);
        Tpl::showpage('member_distribute.edit', 'null_layout');
    }

    /**
     * 编辑推广渠道
     */
    public function member_edit_extensionOp()
    {
        $lang = Language::getLangContent();
        /** @var memberModel $model_member */
        $model_member = Model('member');
        /** @var extensionModel $model_extension */
        $model_extension = Model('extension');

        if (chksubmit()) {
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input" => $_POST["member_id"], "require" => "true", "message" => '参数错误'),
                array("input" => $_POST["member_mobile"], "require" => "true", "message" => '请输入手机号'),
                array("input" => $_POST["extension_class"], "require" => "true", "message" => '请输入渠道名称'),
                array("input" => $_POST["area_info"], "require" => "true", "message" => '请选择地区'),
            );
            $error = $obj_validate->validate();
            if ($error != '') {
                showMessage($error);
            } else {
                $member_id = trim($_POST['member_id']);
                $condition = ['member_id' => $member_id];
                $distribute = $model_member->getUpperMember($condition);
                $dis_code = $distribute['dis_code'];

                /** @var wx_qrcode_createLogic $logic_qrcode */
                $logic_qrcode = Logic('wx_qrcode_create');
                $qrcode = $logic_qrcode->getQrcodeImg($member_id);
                if (empty($qrcode)) {
                    showMessage('生成邀请二维码失败');
                }

                //保存
                $edit_array = [];
                $edit_array['member_id'] = $member_id;
                $edit_array['extension_code'] = $dis_code;
                $edit_array['qrcode_img'] = $qrcode;
                $edit_array['member_mobile'] = trim($_POST['member_mobile']);
                $edit_array['extension_name'] = trim($_POST['extension_name']);
                $edit_array['extension_type'] = trim($_POST['extension_type']);
                $edit_array['extension_class'] = trim($_POST['extension_class']);
                $edit_array['area_info'] = trim($_POST['area_info']);

                $exist = $model_extension->isExist($condition);
                if ($exist) {
                    $edit_array['modify_time'] = TIMESTAMP;
                    $update = $model_extension->modify($edit_array, ['member_id' => $member_id]);
                } else {
                    $edit_array['add_time'] = TIMESTAMP;
                    $update = $model_extension->save($edit_array);
                }

                if ($update) {
                    showMessage('保存成功');
                } else {
                    showMessage('保存失败');
                }
            }
            exit();
        }

        $error_msg = '参数错误';
        $member_id = trim($_GET['member_id']);
        if (empty($member_id)) {
            showMessage($error_msg);
        }

        $field = 'member.member_id,member.member_name,member.member_mobile,member_distribute.dis_code';
        $condition['member_distribute.member_id'] = intval($_GET['member_id']);
        $member_array = $model_member->getMemberDistributeWithMemberList($condition, $field, '', '', '1');

        if (empty($member_array[0])) {
            showMessage($error_msg);
        }

        $extension_array = $model_extension->getOne(['member_id' => $member_id]);
        if ($extension_array) {
            $extension_array['qrcode_img'] = UPLOAD_SITE_URL . "/mobile/wx_qrcode/dis_code_{$extension_array['extension_code']}.jpg";
        }

        Tpl::output('member_array', $member_array[0]);
        Tpl::output('extension_array', $extension_array);
        Tpl::showpage('member_extension.edit', 'null_layout');
    }

    /**
     * csv导出
     */
    public function export_csvOp() {
        $model_member = Model('member');
        $condition = array();
        $limit = false;
        if ($_GET['id'] != '') {
            $id_array = explode(',', $_GET['id']);
            $condition['member_id'] = array('in', $id_array);
        }
        if ($_GET['query'] != '') {
            $condition[$_GET['qtype']] = array('like', '%' . $_GET['query'] . '%');
        }
        $order = '';
        $param = array('member_id','member_name','member_avatar','member_email','member_mobile','member_sex','member_truename','member_birthday'
                ,'member_time','member_login_time','member_login_ip','member_points','member_exppoints','member_grade','available_predeposit'
                ,'freeze_predeposit','available_rc_balance','freeze_rc_balance','inform_allow','is_buy','is_allowtalk','member_state','member_type'
        );
        if (in_array($_GET['sortname'], $param) && in_array($_GET['sortorder'], array('asc', 'desc'))) {
            $order = $_GET['sortname'] . ' ' . $_GET['sortorder'];
        }
        if (!is_numeric($_GET['curpage'])){
            $count = $model_member->getMemberCount($condition);
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $array = array();
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=member&op=index');
                Tpl::showpage('export.excel');
                exit();
            }
        } else {
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $limit = $limit1 .','. $limit2;
        }

        $member_list = $model_member->getMemberList($condition, '*', null, $order, $limit);
        $this->createCsv($member_list);
    }
    /**
     * 生成csv文件
     */
    private function createCsv($member_list) {
        $model_member = Model('member');
        $member_grade = $model_member->getMemberGradeArr();
        // 性别
        $sex_array = $this->get_sex();
        //会员类型
        $type_arr = $this->_getMemberType();
        $data = array();
        foreach ($member_list as $value) {
            $param = array();
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = $value['member_name'];
            $param['member_avatar'] = getMemberAvatarForID($value['member_id']);
            $param['member_type'] = $type_arr[$value['member_type']];
            $param['member_email'] = $value['member_email'];
            $param['member_mobile'] = $value['member_mobile'];
            $param['member_sex'] = $sex_array[$value['member_sex']];
            $param['member_truename'] = $value['member_truename'];
            $param['member_birthday'] = $value['member_birthday'];
            $param['member_time'] = date('Y-m-d', $value['member_time']);
            $param['member_login_time'] = date('Y-m-d', $value['member_login_time']);
            $param['member_login_ip'] = $value['member_login_ip'];
            $data[$value['member_id']] = $param;
        }

        $header = array(
                'member_id' => '会员ID',
                'member_name' => '会员名称',
                'member_avatar' => '会员头像',
                'member_type' => '会员类型',
                'member_email' => '会员邮箱',
                'member_mobile' => '会员手机',
                'member_sex' => '会员性别',
                'member_truename' => '真实姓名',
                'member_birthday' => '出生日期',
                'member_time' => '注册时间',
                'member_login_time' => '最后登录时间',
                'member_login_ip' => '最后登录IP'
        );
        \Shopnc\Lib::exporter()->output('member_list' .$_GET['curpage'] . '-'.date('Y-m-d'), $data, $header);
    }


    public function upgrade_lvOp(){

        $message = '失败';
        $id = intval($_GET['member_id']);
        $type_id = $_GET['type_id'];

        if (empty($id) || empty($type_id)) {
            goto END;
        }

        $msg = '';
        /** @var j_member_distributeLogic $j_member_distribute */
        $j_member_distribute = Logic('j_member_distribute');
        if($j_member_distribute->upgrade_lv($id, $type_id, $msg)){
            $this->log($msg, 1);
            $message = '成功!';
        }

        END:
        showMessage($message);

    }

}
