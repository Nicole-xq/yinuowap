<?php

/**
 *分销等级配置
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class distribution_configControl extends SystemControl{
    public function __construct(){
        parent::__construct();
        Language::read('setting');
    }

    public function indexOp(){
        Tpl::showpage('distribution_config.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_distribution_config = Model('distribution_config');
        $configList = $model_distribution_config->getConfigList();
        $data = array();
        $data['now_page'] = 1;//$model_distribution_config->shownowpage();
        $data['total_num'] = count($configList);
        foreach ($configList as &$value) {
            $tmp = array();
            $tmp['operation'] = "<a class='btn blue' href='index.php?act=distribution_config&op=edit_config&config_id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a><a class='btn blue' href='index.php?act=distribution_config&op=set_detail_config&level=1&config_id=" . $value['id'] . "'><i class='fa fa-plus'></i>设置一级</a><a class='btn blue' href='index.php?act=distribution_config&op=set_detail_config&level=2&config_id=" . $value['id'] . "'><i class='fa fa-plus'></i>设置二级</a>";
            $tmp['type_id'] = $value['type_id'];
            $tmp['name'] = $value['name'];
            $tmp['price'] = $value['price'];
            $tmp['last_modify_time'] = $value['last_modify_time'];
//            $value['operation'] = "<a class='btn blue' href='index.php?act=level_config&op=_edit&member_id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $data['list'][$value['id']] = $tmp;
        }
        echo Tpl::flexigridXML($data);exit();
    }

    public function edit_configOp(){
        $model_distribution_config = Model('distribution_config');
        if (chksubmit()){
            $update_arr = array();
            $update_arr['name'] = $_POST['name'];
            $update_arr['price'] = $_POST['price'];
            $update_arr['last_modify_time'] = date('Y-m-d H:i:s');
            $id = $_POST['distribution_id'];
            //上传专场图片
            if (!empty($_FILES['logo_img']['name'])){
                $upload = new UploadFile();
                $upload->set('default_dir', ATTACH_VENDUE);
                $upload->set('file_name','vendue_' . $_POST['type_id'] . '.jpg');
                $result = $upload->upfile('logo_img');
                if ($result){
                    $update_arr['logo_img'] = $upload->file_name;
                }else {
                    showDialog($upload->error);
                }
            }
            $model_distribution_config = Model('distribution_config');
            try{
                $re = $model_distribution_config->updateConfigById($id,$update_arr);
                if(!$re){
                    throw new Exception('失败');
                }
                //判断有没有之前的图片，如果有则删除
                if (!empty($auctions_info['logo_img']) && !empty($_POST['logo_img'])){
                    @unlink(BASE_UPLOAD_PATH.DS.ATTACH_COMMON.DS.$auctions_info['logo_img']);
                }
                $url = array(
                    array(
                        'url'=>'index.php?act=distribution_config&op=index',
                        'msg'=>'返回列表',
                    ),
                    array(
                        'url'=>'index.php?act=distribution_config&op=edit_config&config_id='.intval($_POST['distribution_id']),
                        'msg'=>'继续修改',
                        ),
                );
                showMessage('成功',$url);
            }catch(Exception $e){
                $model_distribution_config->rollback();
                showMessage('失败');
            }
        }
        $configId = intval($_GET['config_id']);
        $configArr = $model_distribution_config->getConfigById($configId);
        Tpl::output('configArr',$configArr);
        Tpl::showpage('distribution_config.edit');
    }

    /**
     * 设置返佣明细比例
     */
    public function set_detail_configOp(){
        $dis_detail_config = Model('distribution_detail_config');
        if (chksubmit()){
            $update_arr = [
                'dis_id'    =>$_POST['dis_id'],
                'level'     =>$_POST['level'],
                'type_id'   =>$_POST['type_id'],
                'dz_num'   =>$_POST['dz_num'],
                'wdl_num'   =>$_POST['wdl_num'],
                'zl_num'    =>$_POST['zl_num'],
                'sy_num'    =>$_POST['sy_num'],
                'artist_num'=>$_POST['artist_num'],
                'goods_num' =>$_POST['goods_num'],
                'bzj_num'   =>$_POST['bzj_num'],
            ];
            $id = $_POST['id'];
            if ($id){
                $condition = ['id'=>$id];
                $re = $dis_detail_config->updateDisDetail($condition,$update_arr);
            }else{
                $re = $dis_detail_config->addDisDetail($update_arr);
            }
            if ($re){
                $url = array(
                    array(
                        'url'=>'index.php?act=distribution_config&op=index',
                        'msg'=>'返回列表',
                    ),
                    array(
                        'url'=>'index.php?act=distribution_config&op=edit_config&config_id='.intval($_POST['distribution_id']),
                        'msg'=>'继续修改',
                    ),
                );
                showMessage('成功',$url);
            }else {
                showMessage('失败');
            }
        }
        $model_distribution_config = Model('distribution_config');
        $configId = intval($_GET['config_id']);
        $configArr = $model_distribution_config->getConfigById($configId);
        Tpl::output('configArr',$configArr);

        $level = intval($_GET['level']);
        $condition = [
            'dis_id' =>$configId,
            'level'  =>$level,
            'type_id'=>$configArr['type_id'],
        ];
        $detailArr = $dis_detail_config->getDisDetailInfo($condition, $field = '*');
        $detailArr['level'] = $level;
        Tpl::output('detailArr',$detailArr);

        Tpl::showpage('distribution_config.detail');
    }

}