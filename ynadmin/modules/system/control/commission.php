<?php
/**
 * 账号同步
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class commissionControl extends SystemControl{
    const EXPORT_SIZE = 1000;

    public function __construct(){
        parent::__construct();
        Language::read('setting');
    }

    /**
     * 返佣记录列表
     */
    public function indexOp(){
        $commission_model = Model('member_commission');
        $commission_type = $commission_model->getCommissionType();
        Tpl::output('commission_type', $commission_type);
        Tpl::showpage('commission.index');
    }

    /**
     * 获取返佣记录列表xml
     */
    public function get_xmlOp(){
        $model_commis = Model('member_commission');

        $condition  = array();
        $this->_get_condition($condition);

        $order = 'log_id desc';

        $sort_fields = array('dis_member_name','order_sn','commission_type','from_member_name','goods_pay_amount','goods_commission','pay_refund','goods_commission_refund','commission_amount','commission_refund','dis_commis_state','commission_time','add_time');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }

        $commis_list = $model_commis->getCommissionList($condition, '*', $_POST['rp'], $order);
        $data = array();
        $data['now_page'] = $model_commis->shownowpage();
        $data['total_num'] = $model_commis->gettotalnum();
        // 数据处理
        $this->_commission_data_list($data,$commis_list);
        // 底部统计
        $data['list'][] = $this->setFooterValueByType($model_commis->getCommissionAmountByType($condition));
        exit(Tpl::flexigridXML($data));
    }

    /*数据处理*/
    private function _commission_data_list(&$data, $commis_list, $excel = false){
        $model_commis = Model('member_commission');
        $commission_type = $model_commis->getCommissionType();
        $member_type_display = $this->getMemberType(array_keys(array_under_reset($commis_list,'dis_member_id')));
        foreach ((array)$commis_list as $commis) {
            $list = array();
            $set_value = $this->setValueByType($commis);
            $list['operation'] = $set_value['href'] ? "<a class='btn green' href=".$set_value['href']." class='url'><i class='fa fa-list-alt'></i>查看</a>" : '';
//            $list['commission_type'] = str_replace(array(1,2), array('商城','拍卖'),$commis['commission_type']);
            $list['commission_type'] = $commission_type[$commis['commission_type']];
            if($commis['commission_type'] == 1&&$commis['order_goods_id'] == 0){
                $list['commission_type'] = '线下'.$list['commission_type'];
                $list['operation'] = '';
            }
            //$list['dis_member_id'] = $commis['dis_member_id'];

            $list['dis_member_name'] = $commis['dis_member_name'];
            $list['member_type_display'] = $member_type_display[$commis['dis_member_id']]['member_type_display'];

            $list['goods_name'] = $commis['goods_name'];
            $list['dis_commis_rate'] = $commis['dis_commis_rate'].'%'.$set_value['dis_commis_rate_flag'];
            $list['goods_pay_amount'] = ncPriceFormat($commis['goods_pay_amount']);
            $list['order_sn'] = $commis['order_sn'];
            $list['commission_amount'] = ncPriceFormat($commis['commission_amount']);
            $list['dis_commis_state'] = str_replace(array(0,1), array('未结算','已结算'), $commis['dis_commis_state']);
            $list['add_time'] = date('Y-m-d H:i:s',$commis['add_time']);
            $list['commission_time'] = $commis['commission_time'] >0 ? date('Y-m-d H:i:s',$commis['commission_time']) : '--';
            $list['from_member_name'] = $commis['from_member_name'];
            $list['special_rate_day'] = $set_value['special_rate_day'];

            if($excel){ //如果是excel
                unset($list['operation']);
                $tmp = array();
                foreach ($list as $item){
                    $tmp[] = array('data'=>$item);
                }
                $data[] = $tmp;
            }else{
                $data['list'][$commis['log_id']] = $list;
            }
        }
    }

    /**
     * 获取会员等级
     * @param array $member_id_arr
     * @return array
     */
    private function getMemberType($member_id_arr){
        $member_type_display = [];
        if (!empty($member_id_arr)){
            $member_type_display = Model('member')->getMemberList(['member_id'=>['in',$member_id_arr]], 'member_id,member_type');
            $member_type_config = Model('distribution_config')->getConfigList();
            $member_type_config = array_under_reset($member_type_config,'type_id');
            foreach ($member_type_display as $key=>&$value){
                $value['member_type_display'] = $member_type_config[$value['member_type']]['name'];
            }
            unset($value);
            $member_type_display = array_under_reset($member_type_display,'member_id');
        }
        return $member_type_display;
    }

    /**
     * 获取会员等级
     * @param array $commis
     * @return array
     */
    private function setValueByType($commis){
        $setValue=['special_rate_day','dis_commis_rate_flag'];
        switch ($commis['commission_type']){
            case '1'://商品订单
                $setValue['href'] = urlAdminShop('order', 'show_order', array('order_id' =>$commis['order_id']));
                break;
            case '2'://拍卖
                break;
            case '3'://保证金利息
                $setValue['href'] = urlAdminShop('margin_order', 'index', array('order_sn' =>$commis['order_sn']));
                $margin_orders_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$commis['order_sn']], 'margin_id,pay_date_number');
                $setValue['special_rate_day'] = $margin_orders_info['pay_date_number'] > 0 ? $margin_orders_info['pay_date_number'].'天' : '';
                $setValue['dis_commis_rate_flag'] = "年化";
                break;
            case '4'://合伙人升级
                break;
            case '5'://艺术家入驻
                $setValue['href'] = urlAdminShop('store', 'store_joinin_detail', array('member_id' =>$commis['from_member_id']));
                break;
            case '6'://保证金利息
                $setValue['href'] = urlAdminSystem('commission', 'commission_detail', array('order_sn' =>$commis['order_sn'],'id'=>$commis['log_id']));
                break;
            default :
                break;
        }
        return $setValue;
    }

    /**
     * 拼接统计相关数据
     * @param array $commission_amount
     * @return array
     */
    private function setFooterValueByType($commission_amount){
        $footer_data=[
            'operation'=>'订单总金额：',
            'commission_type'=>0.00,
            'dis_member_id'=>'拍卖：',
            'dis_member_name'=>0.00,
            'member_type_display'=>'保证金利息：',
            'dis_commis_lv'=>0.00,
            'dis_commis_rate'=>'合伙人升级：',
            'goods_pay_amount'=>0.00,
            'commission_amount'=>'艺术家入驻',
            'dis_commis_state'=>0.00
        ];
        foreach ($commission_amount as $value){
            switch ($value['commission_type']){
                case '1'://商品订单
                    $footer_data['operation'] = '订单总金额：';
                    $footer_data['commission_type'] = $value['commission_amount'];
                    break;
                case '2'://拍卖
                    $footer_data['dis_member_id'] = '拍卖：';
                    $footer_data['dis_member_name'] = $value['commission_amount'];
                    break;
                case '3'://保证金利息
                    $footer_data['member_type_display'] = '保证金利息：';
                    $footer_data['dis_commis_lv'] = $value['commission_amount'];
                    break;
                case '4'://合伙人升级
                    $footer_data['dis_commis_rate'] = '合伙人升级：';
                    $footer_data['goods_pay_amount'] = $value['commission_amount'];
                    break;
                case '5'://艺术家入驻
                    $footer_data['commission_amount'] = '艺术家入驻：';
                    $footer_data['dis_commis_state'] = $value['commission_amount'];
                    break;
                default :
                    break;
            }
        }
        return $footer_data;
    }

    /**
     * 添加返佣记录
     */
    public function commis_addOp(){
        if(chksubmit()){
            $obj_validate = new Validate();
            $validate_arr[] = array("input"=>$_POST["dis_member_name"],"require"=>"true","message"=>'返佣收益人不能为空');
            $validate_arr[] = array("input"=>$_POST["from_member_name"],"require"=>"true","message"=>'返佣人不能为空');
            $validate_arr[] = array("input"=>$_POST["order_sn"],"require"=>"true","message"=>'订单编号不能为空');
            $validate_arr[] = array("input"=>$_POST["goods_id"],"require"=>"true","message"=>'商品编号必须为正整数');
            $validate_arr[] = array("input"=>$_POST["goods_pay_amount"],"require"=>"true",'validator'=>'Compare','operator'=>'>=','to'=>"{$_POST['commission_amount']}","message"=>'商品支付金额为0.01~999999999之间的数字且不小于返佣金额');
            $validate_arr[] = array("input"=>$_POST["goods_pay_amount"],"require"=>"true",'validator'=>'Compare','operator'=>'>=','to'=>"{$_POST['commission_amount']}","message"=>'商品支付金额为0.01~999999999之间的数字且不小于返佣金额');
            $validate_arr[] = array('input'=>$_POST['dis_commis_rate'],'require'=>'true','validator'=>'Range','min'=>0.1,'max'=>100,'message'=>'佣金比例为0.1~100之间的数字');
            $obj_validate->validateparam = $validate_arr;
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage(Language::get('error').$error,'','','error');
            }

            $model_member = Model('member');
            $to_member_name = trim($_POST['dis_member_name']);
            $to_member_info = $model_member->getMemberInfo(array('member_name'=>$to_member_name),'member_id');
            if(empty($to_member_info)){
                showMessage('返佣收益人不存在');
            }
            $from_member_name = trim($_POST['from_member_name']);
            $from_member_info = $model_member->getMemberInfo(array('member_name'=>$from_member_name),'member_id');
            if(empty($from_member_info)){
                showMessage('返佣人不存在');
            }
            $res = $this->_makeCommisData($_POST, $to_member_info, $from_member_info);
            if($res){
                showMessage('添加成功','index.php?act=commission');
            }else{
                showMessage('添加失败');
            }
        }
        Tpl::showpage('commission.add');
    }

    /**
     * 整理返佣数据
     * @param array $parma
     * @param array $t_member
     * @param array $f_member
     * @return array
     */
    private function _makeCommisData($parma = array(),$t_member = array(),$f_member = array()){
        $flag = false;
        $insert_data = array();
        if(!empty($parma) && !empty($t_member) && !empty($f_member)){
            $commission_amount = floatval($parma['commission_amount']);
            $dis_commis_rate = floatval($parma['dis_commis_rate']);
            $insert_data['dis_member_name'] = trim($parma['dis_member_name']);
            $insert_data['commission_type'] = intval($parma['commission_type']);
            $insert_data['order_sn'] = trim($parma['order_sn']);
            $insert_data['goods_id'] = intval($parma['goods_id']);
            $insert_data['goods_pay_amount'] = intval($parma['goods_pay_amount']);
            $insert_data['dis_commis_rate'] = $dis_commis_rate;
            $insert_data['commission_amount'] = $commission_amount;
            $insert_data['from_member_name'] = floatval($parma['from_member_name']);
            $insert_data['add_time'] = time();
            $insert_data['from_member_id'] = $f_member['member_id'];
            $insert_data['dis_member_id'] = $t_member['member_id'];
            $insert_data['goods_commission'] = $commission_amount / $dis_commis_rate * 100;

            if(intval($parma['commission_type']) == 1){
                //商城购买
                $flag = $this->_dealShipping($insert_data);
            }else{
                //拍卖购买
                $flag = $this->_dealAuction($insert_data);
            }
        }
        return $flag;
    }

    //处理商城返佣
    private function _dealShipping(& $insert_data){
        $flag = false;
        $order_model = Model('order');
        $order_info = $order_model->getOrderInfo(array('order_sn'=>$insert_data['order_sn']),'order_id,commission_state,order_state',array('order_goods'));
        if($order_info['order_state'] != 40){
            showMessage('添加失败，请完成订单后再添加返佣记录');
        }
        $goods_list = (array)$order_info['extend_order_goods'];
        $insert_data['order_id'] = $order_info['order_id'];
        $insert_data['dis_commis_state'] = $order_info['commission_state'];
        if($order_info['commission_state']){
            $insert_data['commission_time'] = time();
        }
        foreach($goods_list as $goods){
            if(!in_array($goods['goods_type'],array(5,9)) && $goods['goods_id'] == $insert_data['goods_id']){
                $insert_data['order_goods_id'] = $goods['rec_id'];
                $insert_data['goods_name'] = $goods['goods_name'];
                $insert_data['goods_image'] = $goods['goods_image'];
            }
        }
        $model_member = Model('member');
        $update_member = array();
        if($order_info['commission_state']){
            $update_member['available_commis'] = array('exp','available_commis+'.$insert_data['commission_amount']);
            $update_member['available_predeposit'] = array('exp','available_predeposit+'.$insert_data['commission_amount']);
        }else{
            $update_member['freeze_commis'] = array('exp','freeze_commis+'.$insert_data['commission_amount']);
        }
        $res = $model_member->editMember(array('member_id'=>$insert_data['dis_member_id']),$update_member);
        if($res){
            $flag = Model()->table('member_commission')->insert($insert_data);
        }
        return $flag;
    }

    //处理拍卖返佣
    private function _dealAuction(& $insert_data){
        $flag = false;
        $order_model = Model('auction_order');
        $order_info = $order_model->getInfo(array('auction_order_sn'=>$insert_data['order_sn']));
        if($order_info['order_state'] != 40){
            showMessage('添加失败，请完成订单后再添加返佣记录');
        }
        $insert_data['order_id'] = $order_info['auction_order_id'];
        $insert_data['dis_commis_state'] = $order_info['commission_state'];
        if($order_info['commission_state']){
            $insert_data['commission_time'] = time();
        }
        $insert_data['order_goods_id'] = $order_info['auction_id'];
        $insert_data['goods_name'] = $order_info['auction_name'];
        $insert_data['goods_image'] = $order_info['auction_image'];
        $model_member = Model('member');
        $update_member = array();
        if($order_info['commission_state']){
            $update_member['available_commis'] = array('exp','available_commis+'.$insert_data['commission_amount']);
            $update_member['available_predeposit'] = array('exp','available_predeposit+'.$insert_data['commission_amount']);
        }else{
            $update_member['freeze_commis'] = array('exp','freeze_commis+'.$insert_data['commission_amount']);
        }
        $res = $model_member->editMember(array('member_id'=>$insert_data['dis_member_id']),$update_member);
        if($res){
            $flag = Model()->table('member_commission')->insert($insert_data);
        }
        return $flag;
    }


    /**
     * 导出
     *
     */
    public function export_step1Op(){
        $lang = Language::getLangContent();

        $model_commis = Model('member_commission');
        $condition  = array();
        if (preg_match('/^[\d,]+$/', $_GET['log_id'])) {
            $_GET['log_id'] = explode(',',trim($_GET['log_id'],','));
            $condition['log_id'] = array('in',$_GET['log_id']);
        }
        $this->_get_condition($condition);
        $sort_fields = array('dis_member_name','order_sn','commission_type','from_member_name','goods_pay_amount','goods_commission','pay_refund','goods_commission_refund','commission_amount','commission_refund','dis_commis_state','commission_time');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        } else {
            $order = 'order_id desc';
        }

        if (!is_numeric($_GET['curpage'])){
            $count = $model_commis->getCommissionCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){   //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=commission&op=index');
                Tpl::showpage('export.excel');
            }else{  //如果数量小，直接下载
                $data = $model_commis->getCommissionList($condition, '*', self::EXPORT_SIZE, $order);
                $this->createExcel($data);
            }
        }else{  //下载
            $data = $model_commis->getCommissionList($condition, '*', self::EXPORT_SIZE, $order);
            $this->createExcel($data);
        }
    }

    /**
     * 生成excel
     *
     * @param array $data
     */
    private function createExcel($data = array()){
        Language::read('export');
        import('libraries.excel');
        $excel_obj = new Excel();
        $excel_data = array();
        //设置样式
        $excel_obj->setStyle(array('id'=>'s_title','Font'=>array('FontName'=>'宋体','Size'=>'12','Bold'=>'1')));
        //header
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣类型');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣账号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'会员等级');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣级别');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'商品名称');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣比例');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单金额');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'订单号');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣金额');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣状态');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'创建时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'返佣时间');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'来源会员');
        $excel_data[0][] = array('styleid'=>'s_title','data'=>'备注');

        $this->_commission_data_list($excel_data,$data,true);
        $excel_data = $excel_obj->charset($excel_data,CHARSET);
        $excel_obj->addArray($excel_data);
        $excel_obj->addWorksheet($excel_obj->charset('会员返佣详细记录',CHARSET));
        $excel_obj->generateXML('member_commission-'.$_GET['curpage'].'-'.date('Y-m-d-H',time()));
    }

    /**
     * 处理搜索条件
     */
    private function _get_condition(& $condition) {
        if ($_REQUEST['query'] != '' && in_array($_REQUEST['qtype'],array('order_sn','dis_member_name','from_member_name'))) {
            $condition[$_REQUEST['qtype']] = array('like',"%{$_REQUEST['query']}%");
        }
        if ($_GET['keyword'] != '' && in_array($_GET['keyword_type'],array('order_sn','dis_member_name','from_member_name'))) {
            if ($_GET['jq_query']) {
                $condition[$_GET['keyword_type']] = $_GET['keyword'];
            } else {
                $condition[$_GET['keyword_type']] = array('like',"%{$_GET['keyword']}%");
            }
        }
        if (!in_array($_GET['qtype_time'],array('add_time','commission_time'))) {
            $_GET['qtype_time'] = null;
        }
        $if_start_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_time = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_time ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_time ? strtotime($_GET['query_end_date']." 23:59:59"): null;
        if ($_GET['qtype_time'] && ($start_unixtime || $end_unixtime)) {
            $condition[$_GET['qtype_time']] = array('time',array($start_unixtime,$end_unixtime));
        }
        if(in_array($_GET['dis_commis_state'],array('0','1'))){
            $condition['dis_commis_state'] = $_GET['dis_commis_state'];
        }
        if(in_array($_GET['commission_type'],array('1','2','3','4','5','6'))){
            $condition['commission_type'] = $_GET['commission_type'];
        }
    }

    public function ajaxOp(){
        $type = trim($_GET['type']);
        switch($type){
            case 'check_member_name':
                $model_member = Model('member');
                $member_name = trim($_GET['member_name']);
                $member_info = $model_member->getMemberInfo(array('member_name'=>$member_name));
                if(!empty($member_info) && is_array($member_info)){
                    echo 'true';
                } else {
                    echo 'false';
                }
                break;
            case 'check_order_sn':
                $order_sn = trim($_GET['order_sn']);
                if(intval($_GET['commis_from']) == 1){
                    $model_order = Model('order');
                    $order_info = $model_order->getOrderInfo(array('order_sn'=>$order_sn));
                }elseif(intval($_GET['commis_from']) == 2){
                    $model_order = Model('auction_order');
                    $order_info = $model_order->getInfo(array('auction_order_sn'=>$order_sn));
                }
                if(!empty($order_info) && is_array($order_info)){
                    echo 'true';
                } else {
                    echo 'false';
                }
                break;
            case 'check_goods_id':
                $goods_id = intval($_GET['goods_id']);
                if(intval($_GET['commis_from']) == 1){
                    $model_goods = Model('goods');
                    $goods_info = $model_goods->getGoodsInfoByID($goods_id);
                }elseif(intval($_GET['commis_from']) == 2){
                    $model_goods = Model('auctions');
                    $goods_info = $model_goods->getAuctionsInfoByID($goods_id);
                }
                if(!empty($goods_info) && is_array($goods_info)){
                    echo 'true';
                } else {
                    echo 'false';
                }
                break;

        }
    }

    public function commission_detailOp(){
        $order_sn = $_GET['order_sn'];
        $commission_id = $_GET['id'];
        $margin_order = Model('margin_orders')->getOrderInfo(array('order_sn'=>$order_sn));
        $auction_info = Model('auctions')->getAuctionDetail($margin_order['auction_id']);
        $margin_order['payment_time'] = date('Y-m-d H:i:s',$margin_order['payment_time']);
        $commission = Model('member_commission')->getCommission(array('log_id'=>$commission_id));
        $margin_order['commission_msg'] = $commission[0]['goods_name'];
        $margin_order['commission_amount'] = $commission[0]['commission_amount'];
        $data = array(
            'margin_info'=>$margin_order,
            'auction_info'=>$auction_info
        );
        Tpl::output('data', $data);
        Tpl::showpage('commission.detail');
    }

}