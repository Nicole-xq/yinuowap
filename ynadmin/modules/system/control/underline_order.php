<?php

/**
 *线下升级合伙人
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class underline_orderControl extends SystemControl{

    private $links = array(
//        array('url'=>'act=member&op=index','text'=>'所有会员'),
        array('url'=>'act=member_level&op=dz_index&type=5','text'=>'秀才升级'),
        array('url'=>'act=member_level&op=wd_index&type=1','text'=>'举人升级'),
        array('url'=>'act=member_level&op=zl_index&type=2','text'=>'贡士升级'),
        array('url'=>'act=member_level&op=sy_index&type=3','text'=>'进士升级'),
    );
    private $distribute_str = array(
        '童生',
        '秀才',
        '举人',
        '贡士',
        '进士'
    );

    public function __construct(){
        parent::__construct();
        Language::read('member');
    }

    public function indexOp(){
//        echo 1;exit;
//        print_R($re);exit;
        Tpl::output('top_link',$this->sublink($this->links,'wd_index'));
        Tpl::showpage('underline_order.index');
    }

    public function get_member_infoOp(){
        $name = $_GET['user_name'];
        $member_info = Model('member')->getMemberInfo(array('member_name'=>$name),'member_name,member_mobile,member_id,member_type');
        if(!empty($member_info)){
            $top_member = Logic('j_member_distribute')->get_top_member($member_info['member_id']);
            $top_member_info = array();
            if(!empty($top_member)){
                $top_member_info = Model('member')->getMemberInfo(array('member_id'=>$top_member['top_member']),'member_type');
            }
            $member_info['top_member'] = !empty($top_member)?$top_member['top_member_name']:'';
            $member_info['member_type_name'] = $this->distribute_str[$member_info['member_type']];
            $member_info['top_member_type_name'] = $this->distribute_str[$top_member_info['member_type']];
            $distribute_list = Model('distribution_config')->getConfigList(array('id'=>array('in','1,2,3')));
            foreach($distribute_list as $k=>$v){
                if($v['type_id'] <= $member_info['member_type']){
                    unset($distribute_list[$k]);
                }
            }
            $member_info['type_list'] = $distribute_list;
        }
        echo json_encode($member_info);exit;
    }

    /**
     * 创建线下合伙人升级订单
     */
    public function create_orderOp(){
        $member_id = $_GET['member_id'];
        $type_id = $_GET['type_id'];
        $member_info = Model('member')->getMemberInfo(array('member_id'=>$member_id),'member_name,member_mobile,member_id,member_type');
        try{
            Model('distribution_config')->beginTransaction();
            $distribute_info = Model('distribution_config')->getConfigById($type_id);
            if(empty($distribute_info)){
                throw new Exception('未查询到合伙人配置信息');
            }
            $pay_sn = Logic('buy_1')->makePaySn($member_id);
            $pay_sn = '66'.date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
            $param = array(
                'order_sn'=>$pay_sn,
                'member_id'=>$member_id,
                'member_name'=>$member_info['member_name'],
                'type'=>$distribute_info['type_id'],
                'amount'=>$distribute_info['price'],
                'cdate'=>date('Y-m-d H:i:s')
            );
            Model('underline_partner_order')->updateInfo(array('order_status'=>2),array('member_id'=>$member_id,'order_status'=>0));
            $re = Model('underline_partner_order')->create_order($param);
            if(!$re){
                throw new Exception('订单创建失败');
            }
            $param = array(
                'admin_id'=>$this->admin_info['id'],
                'admin_name'=>$this->admin_info['name'],
                'content'=>"创建线下合伙人升级订单[member_id:{$member_id}]",
                'cdate'=>date('Y-m-d H:i:s')
            );
            $re1 = Model('admin_underline_order_log')->insert($param);
            if(!$re1){
                throw new Exception('系统日志添加失败');
            }
            Model('distribution_config')->commit();
        }catch(Exception $e){
            $pay_sn = '';
            Model('distribution_config')->rollback();
        }
        echo json_encode($pay_sn);exit;
    }

    public function check_orderOp(){
        $order_sn = $_GET['order_sn'];
        $order_info = Model('underline_partner_order')->getOrderInfo(array('order_sn'=>$order_sn));
        if(!empty($order_info)&&$order_info['order_status'] == 1){
            echo json_encode('success');exit;
        }
        echo json_encode('fail');exit;
    }




}