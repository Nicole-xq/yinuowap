<?php

/**
 *会员等级
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class member_levelControl extends SystemControl{

    private $links = array(
//        array('url'=>'act=member&op=index','text'=>'所有会员'),
        array('url'=>'act=member_level&op=dz_index&type=5','text'=>'秀才升级'),
        array('url'=>'act=member_level&op=wd_index&type=1','text'=>'举人升级'),
        array('url'=>'act=member_level&op=zl_index&type=2','text'=>'贡士升级'),
        array('url'=>'act=member_level&op=sy_index&type=3','text'=>'进士升级'),
    );

    public function __construct(){
        parent::__construct();
        Language::read('member');
    }

    public function indexOp(){
//        echo 1;exit;
//        print_R($re);exit;
        Tpl::output('top_link',$this->sublink($this->links,'wd_index'));
        Tpl::showpage('member_level.index');
    }

    public function dz_indexOp() {
        Tpl::output('top_link',$this->sublink($this->links,'dz_index'));
        $this->member_levelOp();
    }

    public function zl_indexOp() {
        Tpl::output('top_link',$this->sublink($this->links,'zl_index'));
        $this->member_levelOp();
    }

    public function wd_indexOp() {
        Tpl::output('top_link',$this->sublink($this->links,'wd_index'));
        $this->member_levelOp();
    }

    public function sy_indexOp() {
        Tpl::output('top_link',$this->sublink($this->links,'sy_index'));
        $this->member_levelOp();
    }

    public function member_levelOp(){
        Tpl::showpage('member_level.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        /** @var memberModel $model_member */
        $model_member = Model('member');
        $member_grade = $model_member->getMemberGradeArr();
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('member_id','member_name','member_avatar','member_email','member_mobile','member_sex','member_truename','td_amount'
                ,'member_time','member_login_time','member_login_ip','member_points','member_exppoints','member_grade','available_predeposit'
                ,'freeze_predeposit','available_rc_balance','freeze_rc_balance','inform_allow','is_buy','is_allowtalk','member_state','member_type'
        );
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];

        $update_lev = false;
        if($query_type = $_REQUEST['type']){

            $distribute_info = model('member_distribute_type')->where(array('price' => array('gt', 0)))->order('price asc')->select();

            $distribute_ids = array(4);
            $update_lev = array();
            $check = false;
            foreach($distribute_info as $v){

                if($query_type == $v['type_id']){
                    $condition['td_amount'] = array('egt', $v['price']);
                    $check = true;
                }

                if($check){
                    $distribute_ids[] = $v['type_id'];
                    $update_lev[] = $v;
                }
            }

            $condition['member_type'] = array('not in', $distribute_ids);
        }

        $member_list = $model_member->getMemberList($condition, '*', $page, $order);

        $sex_array = $this->get_sex();

        $type_arr = $this->_getMemberType();

        $data = array();
        $data['now_page'] = $model_member->shownowpage();
        $data['total_num'] = $model_member->gettotalnum();


        foreach ($member_list as $value) {

            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=member&op=member_edit&member_id=" . $value['member_id'] . "&type=".$_REQUEST['type']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            if($update_lev){
                $param['operation'] .= "<span class='btn'><em><i class='fa fa-cog'></i>设置 <i class='arrow'></i></em><ul>";

                foreach($update_lev as $lev_row){
                    if($value['td_amount'] >= $lev_row['price']){
                        $url = 'index.php?act=member&op=upgrade_lv&member_id='.$value['member_id'].'&type_id='.$lev_row['type_id'];
                        $href_str = "javascript:if(confirm('您确定要升级吗？'))window.location = '".$url."'";
                        $param['operation'] .= "<li><a href=\"" . $href_str . "\" >升级".$lev_row['name']."</a></li>";
                    }
                }

                $param['operation'] .= "</ul>";
            }


            $param['member_id'] = $value['member_id'];
            $param['member_name'] = "<img src=".getMemberAvatarForID($value['member_id'])." class='user-avatar' onMouseOut='toolTip()' onMouseOver='toolTip(\"<img src=".getMemberAvatarForID($value['member_id']).">\")'>".$value['member_name'];
            $param['member_type'] = $type_arr[$value['member_type']];
            $param['member_email'] = $value['member_email'];
            $param['member_mobile'] = $value['member_mobile'];
            $param['member_sex'] = $sex_array[$value['member_sex']];
            $param['member_truename'] = $value['member_truename'];
            $param['td_amount'] = $value['td_amount'];
            $param['member_time'] = date('Y-m-d H:i:s', $value['member_time']);
            $param['member_login_time'] = date('Y-m-d H:i:s', $value['member_login_time']);
            $param['member_login_ip'] = $value['member_login_ip'];
            $param['member_state'] = $value['member_state'] ==  '1' ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>' : '<span class="no"><i class="fa fa-ban"></i>否</span>';
            $data['list'][$value['member_id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    /**
     * 性别
     * @return multitype:string
     */
    private function get_sex() {
        $array = array();
        $array[1] = '男';
        $array[2] = '女';
        $array[3] = '保密';
        return $array;
    }

    /**
     * 会员类型
     */
    private function _getMemberType(){
        $config_model = Model('distribution_config');
        $re = $config_model->getConfigList();
        $arr = array();
        foreach($re as $v){
            $arr[$v['type_id']] = $v['name'];
        }
        return $arr;
    }



}