<?php

/**
 *线下升级合伙人
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class underline_order_listControl extends SystemControl{
    const EXPORT_SIZE = 1000;
    private $distribute_str = array(
        '童生',
        '秀才',
        '举人',
        '贡士',
        '进士'
    );

    public function __construct(){
        parent::__construct();
        Language::read('member');
    }

    public function indexOp(){
        Tpl::showpage('underline_order_list.index');
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_underline_order = Model('underline_partner_order');

        $condition = array();
        if($_POST['query'] !=''){
            $condition[$_POST['qtype']] = $_POST['query'];
        }
        $page = $_POST['rp'];
        $order_list = $model_underline_order->getOrderList($condition, '*', $page);
//        dd();
//        print_R($order_list);eixt;
        $data = array();
        $data['now_page'] = $model_underline_order->shownowpage();
        $data['total_num'] = $model_underline_order->gettotalnum();

        foreach ($order_list as $value) {
            $param = array();
            $param['operation'] = '';
            if($value['order_status'] == 0){
                $param['operation'] .= "<a class='btn blue' href='index.php?act=underline_order_list&op=order_info&order_id=" . $value['id'] . "'><i class='fa fa-pencil-square-o'></i>更改支付状态</a>";
            }
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = $value['member_name'];
            $param['type_name'] = '升级'.$this->distribute_str[$value['type']];
            $param['order_sn'] = $value['order_sn'];
            $param['amount'] = $value['amount'];
            $tmp = array('未支付','已支付','已取消');
            $param['order_status'] = $tmp[$value['order_status']];
            $param['trade_no'] = $value['trade_no'];
            $param['cdate'] = $value['cdate'];
            $param['msg'] = $value['msg'];
            $data['list'][$value['id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }

    public function order_infoOp(){
        $order_id = $_GET['order_id'];
        $order_info = Model('underline_partner_order')->getOrderInfo(array('id'=>$order_id,'order_status'=>0));
        $member_info = Model('member')->getMemberInfo(array('member_id'=>$order_info['member_id']),'member_name,member_mobile,member_id,member_type');
        $order_info['member_type'] = $this->distribute_str[$order_info['type']];
        $order_info['member_mobile'] = $member_info['member_mobile'];
        $top_member = Logic('j_member_distribute')->get_top_member($member_info['member_id']);
        $top_member_info = array();
        if(!empty($top_member)){
            $top_member_info = Model('member')->getMemberInfo(array('member_id'=>$top_member['top_member']),'member_type');
            $order_info['top_member_type'] = $this->distribute_str[$top_member_info['member_type']];
        }
        $order_info['special_sign'] = md5(md5('underline'.$order_info['order_sn']).'special_code');
        $order_info['admin_id'] = $this->admin_info['id'];
        $order_info['admin_name'] = $this->admin_info['name'];
        Tpl::output('order_info',$order_info);
        Tpl::showpage('underline_order_list.detail');
    }

    /**
     * csv导出
     */
    public function export_csvOp() {
        $model_underline_order = Model('underline_partner_order');
        $condition = array();
        $limit = false;
        if ($_GET['id'] != '') {
            $id_array = explode(',', $_GET['id']);
            $condition['id'] = array('in', $id_array);
        }
        if ($_GET['query'] != '') {
            $condition[$_GET['qtype']] = array('like', '%' . $_GET['query'] . '%');
        }

        $list = $model_underline_order->getOrderList($condition, '*', null,'id desc', $limit);
        $this->createCsv($list);
    }
    /**
     * 生成csv文件
     */
    private function createCsv($order_list) {

        $data = array();
        foreach ($order_list as $value) {
            $param = array();
            $param['member_id'] = $value['member_id'];
            $param['member_name'] = $value['member_name'];
            $param['type_name'] = '升级'.$this->distribute_str[$value['type']];
            $param['order_sn'] = $value['order_sn'];
            $param['amount'] = $value['amount'];
            $tmp = array('未支付','已支付','已取消');
            $param['order_status'] = $tmp[$value['order_status']];
            $param['trade_no'] = $value['trade_no'];
            $param['cdate'] = $value['cdate'];
            $param['msg'] = $value['msg'];
            $data[$value['id']] = $param;
        }

        $header = array(
                'member_id' => '会员ID',
                'member_name' => '会员名称',
                'type_name' => '订单类型',
                'order_sn' => '订单号',
                'amount' => '订单金额',
                'order_status' => '订单状态',
                'trade_no' => '外部单号',
                'cdate' => '创建时间',
                'msg' => '备注',
        );
        \Shopnc\Lib::exporter()->output('underline-'.date('Y-m-d'), $data, $header);
    }

}