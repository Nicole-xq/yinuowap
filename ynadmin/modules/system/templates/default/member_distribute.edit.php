<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<style type="text/css">
    .ncap-goods-sku .title h4, .ncap-goods-sku .content span{
        width: 14%;
        padding: 3px 1%;
    }
</style>
<div class="ncap-goods-sku">
    <div style="color: red">
        &nbsp;&nbsp;&nbsp;&nbsp;修改用户上级后，该用户上级、上二级备注，以及该用户所有下级用户的上二级备注，<br>
        &nbsp;&nbsp;&nbsp;&nbsp;会被清空无法找回，请谨慎操作
    </div>
    <form id="user_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="member_id" value="<?php echo $output['edit_info']['member_id'];?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><?php echo $lang['member_index_name']?>ID：</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['edit_info']['member_id'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><?php echo $lang['member_index_name']?>名称：</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['edit_info']['member_name'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>原上级<?php echo $lang['member_index_name']?>ID：</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['edit_info']['old_top_member_id'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>原上级<?php echo $lang['member_index_name']?>名称：</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['edit_info']['old_top_member_name'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>新上级<?php echo $lang['member_index_name']?>ID：</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="change_top_member_id">
                    <?php echo $output['edit_info']['change_top_member_id'];?>
                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>备注信息：</label>
                </dt>
                <dd class="opt">
                    <textarea name="change_msg" class="tarea"></textarea>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>

<!--自动关闭编辑页面-->
<div id="fwin_member"></div>

<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script type="text/javascript">
    $("#submitBtn").click(function(){
        if($("#user_form").valid()){
            var url = '<?php echo urlAdminSystem('member', 'member_edit_distribute');?>';
            var data = $("#user_form").serialize();
            $.post(url,data,function(data){
                $('#fwin_member').html(data);
            })
        }
    });

    $('#user_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules : {
            change_top_member_id   : {
                required : true,
                number : true
            }
        },
        messages : {
            change_top_member_id : {
                required: '<i class="fa fa-exclamation-circle"></i><?php echo $lang['member_edit_null']?>',
                number: '<i class="fa fa-exclamation-circle"></i>信息错误，请填写数字'
            }
        }
    });

</script>