<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>分销等级管理</h3>
        <h5>分销等级管理。。。。。</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
    </div>
    <ul>
      <li>针对各个会员级别的分销返、购买返佣以及投资返佣的参数设置</li>
    </ul>
  </div>
    <div id="flexigrid"></div>
</div>
<script>
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=distribution_config&op=get_xml',
        colModel : [
            {display: '操作', name : 'operation', width : 500, sortable : false, align: 'center', className: 'handle'},
            {display: '分销类型ID', name : 'type_id', width : 200, sortable : true, align: 'center'},
            {display: '等级名称', name : 'name', width : 120, sortable : true, align: 'center'},
            {display: '特定专区升级金额', name : 'price', width : 150, sortable : true, align: 'center'},
            {display: '最后修改时间', name : 'last_modify_time', width : 200, sortable : true, align: 'center'},
        ],
//        buttons : [
//            {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation },
//            {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出CVS文件', onpress : fg_operation }
//            ],
//        searchitems : [
//            {display: '会员ID', name : 'member_id'},
//            {display: '会员名称', name : 'member_name'}
//            ],
//        sortname: "type_id",
//        sortorder: "desc",
        title: '分销等级列表'
    });

});
</script>
