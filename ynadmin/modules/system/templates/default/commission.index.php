<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>返佣管理</h3>
                <h5>网站所有普通会员、特级会员返佣列表及管理</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>显示会员的每一笔返佣详细信息</li>
            <li>可手动添加会员返佣记录</li>
            <li>可导出当前显示结果</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
    <div class="ncap-search-ban-s" id="searchBarOpen"><i class="fa fa-search-plus"></i>高级搜索</div>
    <div class="ncap-search-bar">
        <div class="handle-btn" id="searchBarClose"><i class="fa fa-search-minus"></i>收起边栏</div>
        <div class="title">
            <h3>高级搜索</h3>
        </div>
        <form method="get" name="formSearch" id="formSearch">
            <div id="searchCon" class="content">
                <div class="layout-box">
                    <dl>
                        <dt>关键字搜索</dt>
                        <dd>
                            <label>
                                <select class="s-select" name="keyword_type">
                                    <option selected="selected" value="">-请选择-</option>
                                    <option value="order_sn">订单编号</option>
                                    <option value="dis_member_name">会员账号</option>
                                    <option value="from_member_name">返佣来源会员账号</option>
                                </select>
                            </label>
                            <label>
                                <input type="text" value="" placeholder="请输入关键字" name="keyword" class="s-input-txt">
                            </label>
                            <label>
                                <input type="checkbox" value="1" name="jq_query">精确
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>日期筛选</dt>
                        <dd>
                            <label>
                                <select class="s-select" name="qtype_time">
                                    <option selected="selected" value="">-请选择-</option>
                                    <option value="add_time">返佣创建时间</option>
                                    <option value="commission_time">返佣结算时间</option>
                                </select>
                            </label>
                            <label>
                                <input readonly id="query_start_date" placeholder="请选择起始时间" name=query_start_date value="" type="text" class="s-input-txt" />
                            </label>
                            <label>
                                <input readonly id="query_end_date" placeholder="请选择结束时间" name="query_end_date" value="" type="text" class="s-input-txt" />
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>返佣来源类型</dt>
                        <dd>
                            <label>
                                <select name="commission_type" class="s-select">
                                    <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                    <?php foreach ((array) $output['commission_type'] as $key=>$val) { ?>
                                        <option value="<?php echo $key;?>"><?php echo $val;?></option>
                                    <?php } ?>
                                </select>
                            </label>
                        </dd>
                    </dl>
                    <dl>
                        <dt>返佣结算状态</dt>
                        <dd>
                            <select name="dis_commis_state" class="s-select">
                                <option value=""><?php echo $lang['nc_please_choose'];?></option>
                                <option value="0">未结算</option>
                                <option value="1">已结算</option>
                            </select>
                        </dd>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bottom"> <a href="javascript:void(0);" id="ncsubmit" class="ncap-btn ncap-btn-green mr5">提交查询</a><a href="javascript:void(0);" id="ncreset" class="ncap-btn ncap-btn-orange" title="撤销查询结果，还原列表项所有内容"><i class="fa fa-retweet"></i><?php echo $lang['nc_cancel_search'];?></a></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#query_start_date').datepicker();
        $('#query_end_date').datepicker();
        // 高级搜索提交
        $('#ncsubmit').click(function(){
            $("#flexigrid").flexOptions({url: 'index.php?act=commission&op=get_xml&'+$("#formSearch").serialize(),query:'',qtype:''}).flexReload();
        });
        // 高级搜索重置
        $('#ncreset').click(function(){
            $("#flexigrid").flexOptions({url: 'index.php?act=commission&op=get_xml'}).flexReload();
            $("#formSearch")[0].reset();
        });
        $("#flexigrid").flexigrid({
            url: 'index.php?act=commission&op=get_xml',
            colModel : [
                /*若编辑colModel下的{display, name}后，请修改commission.php文件下createExcel方法，否则生成excel可能出现数据标题不一致*/
                {display: '操作', name : 'operation', width : 50, sortable : false, align: 'center', className: 'handle-s'},
                {display: '返佣类型', name : 'commission_type', width : 100, sortable : true, align : 'center'},
                {display: '返佣账号', name : 'dis_member_name', width : 120, sortable : true, align: 'left'},
                {display: '会员等级', name : 'member_type_display', width : 100, sortable : true, align: 'left'},
                {display: '返佣级别', name : 'dis_commis_lv', width : 80, sortable : true, align: 'left'},
                {display: '商品名称', name : 'goods_name', width : 150, sortable : true, align: 'left'},
                {display: '返佣比例', name : 'dis_commis_rate', width : 100, sortable : true, align: 'center'},
                {display: '订单金额', name : 'goods_pay_amount', width : 100, sortable : true, align: 'center'},
                {display: '订单号', name : 'order_sn', width : 130, sortable : true, align: 'center'},
                {display: '返佣金额', name : 'commission_amount', width: 100, sortable : true, align : 'center'},
                {display: '返佣状态', name : 'dis_commis_state', width : 100, sortable : true, align: 'center'},
                {display: '创建时间', name : 'add_time', width : 150, sortable : true, align: 'center'},
                {display: '返佣时间', name : 'commission_time', width : 150, sortable : true, align: 'center'},
                {display: '来源会员', name : 'from_member_name', width : 100, sortable : true, align: 'left'},
                {display: '备注', name : 'special_rate_day', width : 50, sortable : true, align: 'center'},
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operate },
                {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出excel文件,如果不选中行，将导出列表所有数据', onpress : fg_operate }
            ],
            searchitems : [
                {display: '订单编号', name : 'order_sn', isdefault: true},
                {display: '会员账号', name : 'dis_member_name'},
                {display: '返佣来源会员账号', name : 'from_member_name'}
            ],
            sortname: "log_id",
            sortorder: "desc",
            title: '会员返佣详细记录'
        });
    });
    function fg_operate(name, grid) {
        if (name == 'add') {
            window.location.href = 'index.php?act=commission&op=commis_add';
        }
        if (name == 'csv') {
            var itemlist = new Array();
            if($('.trSelected',grid).length>0){
                $('.trSelected',grid).each(function(){
                    itemlist.push($(this).attr('data-id'));
                });
            }
            fg_csv(itemlist);
        }
    }
    function fg_csv(ids) {
        id = ids.join(',');
        window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_step1&log_id=' + id;
    }
</script>
