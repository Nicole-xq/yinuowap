<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=distribution_config&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>参数配置</h3>
        <h5>分销等级基数参数配置</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明    -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>注意微合伙人分销返佣和购买返佣的金额叠加情况，分销返佣默认包含购买返佣。</li>
    </ul>
  </div>
  <form id="user_form" enctype="multipart/form-data" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="type_id" value="<?php echo $output['configArr']['type_id'];?>" />
    <input type="hidden" name="distribution_id" value="<?php echo $output['configArr']['id'];?>" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="name">名称</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt" id="name" name="name" value="<?php echo $output['configArr']['name'];?>" />
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="name">Logo</label>
        </dt>
        <dd class="opt">
            <div class="input-file-show">
                <span class="show">
                    <a class="nyroModal" rel="gal" href="<?= getVendueLogo($output['configArr']['logo_img'])?>">
                        <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?= getVendueLogo($output['configArr']['logo_img']);?>>')" onMouseOut="toolTip()"/></i></a>
                </span>
                <span class="type-file-box">
                    <input type="text" name="textfield" id="textfield" class="type-file-text" />
                    <input type="button" name="button" id="button" value="选择更换..." class="type-file-button" />
                    <input class="type-file-file" id="logo_img" name="logo_img" type="file" size="30" hidefocus="true" nc_type="change_special_logo" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                </span>
            </div>
            <span class="err"></span>
            <p class="notic">默认会员Logo图片请使用100*100像素jpg/gif/png格式的图片。</p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label for="price">金额</label>
        </dt>
        <dd class="opt">
          <input type="text" id="price" name="price" value="<?= $output['configArr']['price']?>" class="input-txt">
          <span class="err"></span>
          <p class="notic">分销等级基础投资金额要求.</p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script>
$(function(){
    $("#submitBtn").click(function(){
       if($("#user_form").valid()){
           $("#user_form").submit();
	   }
	});

    $('#user_form').validate({
        errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules: {
            name: {
                required: true,
            },
            price: {
                required: true,
            }
        },
        messages: {
                name: {
                    required : "请填写名称",
                },
                price: {
                    required : "请填写金额",
                }
        }
     });
});
</script>
