<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=commission" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>返佣管理 - 保证金利息信息</h3>
        <h5></h5>
      </div>
    </div>
  </div>
  <form id="commis_form" method="post" name="commisForm">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>返佣收益人</label>
        </dt>
        <dd class="opt">
          <span><?= $data['margin_info']['buyer_name'];?></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>保证金金额</label>
        </dt>
        <dd class="opt">
          <span><?= $data['margin_info']['margin_amount'].'元';?></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>收益金额</label>
        </dt>
        <dd class="opt">
          <span><?= $data['margin_info']['commission_amount'].'元';?></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>来源</label>
        </dt>
        <dd class="opt">
          <span><?= $data['margin_info']['commission_msg'];?></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label><em>*</em>保证金缴纳时间</label>
        </dt>
        <dd class="opt">
          <span><?= $data['margin_info']['payment_time'];?></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>



<!--      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn">--><?php //echo $lang['nc_submit'];?><!--</a></div>-->
    </div>
  </form>
</div>