<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>线下合伙人订单列表</h3>
        <h5>线下升级合伙人订单-历史记录</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明 -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span>
    </div>
    <ul>
      <li>仅做展示</li>
    </ul>
  </div>
    <div id="flexigrid"></div>
</div>
<script>
$(function(){
    $("#flexigrid").flexigrid({
        url: 'index.php?act=underline_order_list&op=get_xml',
        colModel : [
            {display: '操作', name : 'member_id', width : 120, sortable : true, align: 'center'},
            {display: '会员ID', name : 'member_id', width : 60, sortable : true, align: 'center'},
            {display: '会员名', name : 'member_name', width : 100, sortable : true, align: 'center'},
            {display: '订单类型', name : 'type_name', width : 100, sortable : true, align: 'left'},
            {display: '订单编号', name : 'order_sn', width : 150, sortable : true, align: 'center'},
            {display: '金额', name : 'amount', width : 120, sortable : true, align: 'center'},
            {display: '订单状态', name : 'order_status', width : 80, sortable : true, align: 'center'},
            {display: '外部单号', name : 'trade_no', width : 150, sortable : true, align: 'center'},
            {display: '订单创建时间', name : 'cdate', width : 150, sortable : true, align: 'left'},
            {display: '备注', name : 'msg', width : 150, sortable : true, align: 'left'},
            ],
        buttons : [
              {display: '<i class="fa fa-file-excel-o"></i>导出数据', name : 'csv', bclass : 'csv', title : '将选定行数据导出CVS文件', onpress : fg_operation },
            ],
        searchitems : [
            {display: '会员ID', name : 'member_id'},
            {display: '会员名称', name : 'member_name'},
            {display: '外部订单号', name : 'trade_no'},
            {display: '系统订单号', name : 'order_sn'},
            ],
        sortname: "member_id",
        sortorder: "desc",
        title: '商城会员列表'
    });

});

function fg_operation(name, bDiv) {
    if (name == 'csv') {
        if ($('.trSelected', bDiv).length == 0) {
            if (!confirm('您确定要下载全部数据吗？')) {
                return false;
            }
        }
        var itemids = new Array();
        $('.trSelected', bDiv).each(function(i){
            itemids[i] = $(this).attr('data-id');
        });
        fg_csv(itemids);
    }
}

function fg_csv(ids) {
    id = ids.join(',');
    window.location.href = $("#flexigrid").flexSimpleSearchQueryString()+'&op=export_csv&id=' + id;
}
</script>
