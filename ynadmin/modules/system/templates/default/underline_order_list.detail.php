<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=underline_order_list&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>订单信息</h3>
        <h5>线下合伙人升级订单信息</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明    -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>注意:手动升级操作不可逆,请谨慎!</li>
    </ul>
  </div>
    <input type="hidden" id="member_id" name="member_id" value="<?php echo $output['order_info']['member_id'];?>" />
    <div class="ncap-form-default" id="data">
      <dl class="row">
        <dt class="tit">
          <label>用户ID</label>
        </dt>
        <dd class="opt">
          <span id="mobile"><?php echo $output['order_info']['member_id'];?></span>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label>用户名</label>
        </dt>
        <dd class="opt">
          <span ><?php echo $output['order_info']['member_name'];?></span>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label>绑定手机号</label>
        </dt>
        <dd class="opt">
          <span ><?php echo $output['order_info']['member_mobile'];?></span>
        </dd>
      </dl>
        <dl class="row">
            <dt class="tit">
              <label>用户升级级别</label>
            </dt>
            <dd class="opt">
              <span ><?php echo $output['order_info']['member_type'];?></span>
            </dd>
          </dl>
        <dl class="row">
        <dt class="tit">
          <label>上级用户</label>
        </dt>
        <dd class="opt">
          <span id="mobile"><?php echo $output['order_info']['top_member_name'];?></span>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label>订单编号</label>
        </dt>
        <dd class="opt">
          <span id="mobile"><?php echo $output['order_info']['order_sn'];?></span>
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label>外部单号</label>
        </dt>
        <dd class="opt">
            <input type="text" id="trade_no">
        </dd>
      </dl>
        <dl class="row">
        <dt class="tit">
          <label>备注</label>
        </dt>
        <dd class="opt">
            <input type="text" id="msg">
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn">升级</a></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.qrcode.min.js"></script>

<script>
$(function(){
    $("#submitBtn").click(function(){
        if($('#trade_no').val() == ''){
            alert('请填写外部订单号');
            return false;
        }
        var url = "<?php echo MOBILE_SITE_URL;?>/index.php?act=payment&op=notify&payment_code=tlpay";
        $.ajax({
            type:'post',
            dataType:'json',
            data:"trxid="+$('#trade_no').val()+"&bizseq=underline<?php echo $output['order_info']['order_sn'];?>&trxstatus=0000&amount=<?php echo $output['order_info']['amount']*100;?>&aid=<?php echo $output['order_info']['admin_id'];?>&aname=<?php echo $output['order_info']['admin_name'];?>&special_sign=<?php echo $output['order_info']['special_sign'];?>&msg="+$('#msg').val(),
            url:url,
            success:function(re){
                if(re == 'success'){
                    alert('操作成功');
                    window.location.href='index.php?act=underline_order_list&op=index';
                }else{
                    alert('操作失败');
                }
            }
        });
	});

});
</script>
