<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <div class="subject">
        <h3>线下合伙人</h3>
        <h5>创建合伙人升级订单,升级合伙人</h5>
      </div>
    </div>
  </div>
  <!-- 操作说明    -->
  <div class="explanation" id="explanation">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
      <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
      <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
    <ul>
      <li>注意微合伙人分销返佣和购买返佣的金额叠加情况，分销返佣默认包含购买返佣。</li>
    </ul>
  </div>
  <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label for="u_name">用户名</label>
        </dt>
        <dd class="opt">
          <span><input type="text" class="input-txt" id="u_name" name="u_name" value="" /><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="getMember">查询</a></span>
          <span class="err" style="color:red"></span>
          <p class="notic"></p>
        </dd>
      </dl>
    </div>
  <form id="user_form" enctype="multipart/form-data" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" id="member_id" name="member_id" value="" />
    <div class="ncap-form-default" id="data" style="display: none;">
      <dl class="row">
        <dt class="tit">
          <label>用户绑定手机</label>
        </dt>
        <dd class="opt">
          <span id="mobile"></span>
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>当前级别</label>
        </dt>
        <dd class="opt">
            <span id="member_type"></span>
            <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>上级用户</label>
        </dt>
        <dd class="opt">
          <span id="top_member"></span>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>上级用户级别</label>
        </dt>
        <dd class="opt">
          <span id="top_member_type"></span>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>升级合伙人至</label>
        </dt>
        <dd class="opt">
          <span>
            <select id="type_list" name="distribute_type">
            </select>
          </span>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row order_sn_div" style="display:none;">
        <dt class="tit">
          <label>订单号</label>
        </dt>
        <dd class="opt">
          <span id="order_sn">

          </span>
          <span class="err"></span>
        </dd>
      </dl>
      <dl class="row order_sn_div" style="display:none;">
        <dt class="tit">
          <label>二维码</label>
        </dt>
        <dd class="opt">
          <div class="qrcode_test"></div>
          <span id="qrcode">

          </span>
          <span class="err"></span>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn">下一步</a></div>
    </div>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.qrcode.min.js"></script>

<script>
  var order_sn = '';
  $(document).keyup(function (e) {//捕获文档对象的按键弹起事件
      if (e.keyCode == 13) {//按键信息对象以参数的形式传递进来了
          getInfo();
      }
  });
  $(function(){
    $('#getMember').click(function(){
        getInfo();
    });
    $("#submitBtn").click(function(){
        var url = "index.php?act=underline_order&op=create_order&member_id="+$('#member_id').val()+"&type_id="+$('#type_list').val();
        $.ajax({
            type:'get',
            dataType:'json',
            url:url,
            success:function(re){
                order_sn = re;
                $('#qrcode').html('');
                $('#order_sn').html(re);
                if(re !=''){
                    $("#qrcode").qrcode({
                      render: "table",
                      width: 200,
                      height:200,
                      text: 'underline'+re
                    });

                    $('.order_sn_div').show();
                }
            }
        });
	});

    $('#user_form').validate({
        errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules: {
            name: {
                required: true,
            },
            price: {
                required: true,
            }
        },
        messages: {
                name: {
                    required : "请填写名称",
                },
                price: {
                    required : "请填写金额",
                }
        }
     });

    function check(){
        if(order_sn !=''){
            var url = "index.php?act=underline_order&op=check_order&order_sn="+order_sn;
            $.ajax({
                url:url,
                type:'get',
                dataType:'json',
                success:function(re){
                  if(re == 'success'){
                      alert('合伙人升级成功!');
                      window.location.reload();
                  }
                }
            });
        }
        setTimeout(check,1000);
    }
    setTimeout(check,1000);
});
  function getInfo(){
        var u_name = $('#u_name').val();
            var url = 'index.php?act=underline_order&op=get_member_info&user_name='+u_name;
            $.ajax({
                type:'get',
                url:url,
                dataType:'json',
                success:function(res){
                    if(!res.member_id){
                        alert('用户不存在');
                        window.location.reload();
                    }
                    $('#mobile').html(res.member_mobile);
                    $('#member_type').html(res.member_type_name);
                    $('#top_member_type').html(res.top_member_type_name);
                    $('#top_member').html(res.top_member);
                    $('#member_id').val(res.member_id);
                    $('#qrcode').html('');
                    $('#order_sn').html('');
                    $('.order_sn_div').hide();
                    var sel_obj = $('#type_list');
                    sel_obj.html('');
                    for(var tmp in res.type_list){
                        var tmp_html = "<option value="+res.type_list[tmp].id+">"+res.type_list[tmp].name+"</option>";
                        sel_obj.append(tmp_html);
                        sel_obj.attr('text',tmp.name);
                    }
                    $('#data').show();
                }
            });
    }
</script>
