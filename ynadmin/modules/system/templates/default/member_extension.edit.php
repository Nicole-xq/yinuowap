<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<!-- 操作说明 -->
<div class="explanation" id="explanation" style="width: 95%;margin-left: 1%;">
    <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
        <h4 title="<?php echo $lang['nc_prompts_title']; ?>"><?php echo $lang['nc_prompts']; ?></h4>
    </div>
    <ul>
        <li>编辑会员选项，会员名、推广码不可变，其余内容可重新填写，忽略或留空则保持原有信息数据。</li>
        <li>第一次保存会自动生成邀请二维码</li>
    </ul>
</div>
<form id="extension_form" method="post">
    <input type="hidden" name="form_submit" value="ok"/>
    <input type="hidden" name="member_id" value="<?php echo $output['member_array']['member_id']; ?>"/>
    <div class="ncap-form-default">
        <dl class="row">
            <dt class="tit">
                <label><?php echo $lang['member_index_name'] ?></label>
            </dt>
            <dd class="opt">
                <p class="notic"><?php echo $output['member_array']['member_name']; ?></p>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label for="extension_code">推广码</label>
            </dt>
            <dd class="opt">
                <p class="notic"><?php echo $output['member_array']['dis_code']; ?></p>
                <input type="hidden" value="<?php echo $output['member_array']['dis_code']; ?>" id="dis_code"
                       name="dis_code" class="input-txt">
                <span class="err"></span>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label class="member_mobile"><em>*</em>联系手机</label>
            </dt>
            <dd class="opt">
                <input type="text" value="<?php if ($output['extension_array']['member_mobile']) {
                    echo $output['extension_array']['member_mobile'];
                } else {
                    echo $output['member_array']['member_mobile'];
                } ?>" id="member_mobile" name="member_mobile" class="input-txt">
                <span class="err"></span>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label class="extension_name"><em>*</em>渠道名称</label>
            </dt>
            <dd class="opt">
                <input type="text" value="<?php echo $output['extension_array']['extension_name']; ?>"
                       id="extension_name" name="extension_name" class="input-txt">
                <span class="err"></span>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label class="extension_type"><em>*</em>渠道分类</label>
            </dt>
            <dd class="opt">
                <select name="extension_type" id="extension_type">
                    <option <?php if ($output['extension_array']['extension_type'] == 0){ ?>selected<?php } ?>
                            value="0">校园
                    </option>
                    <option <?php if ($output['extension_array']['extension_type'] == 1){ ?>selected<?php } ?>
                            value="1">机构
                    </option>
                    <option <?php if ($output['extension_array']['extension_type'] == 2){ ?>selected<?php } ?>
                            value="2">个人
                    </option>
                </select>
                <span class="err"></span>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label class="extension_class"><em>*</em>渠道种类</label>
            </dt>
            <dd class="opt">
                <select name="extension_class" id="extension_class">
                    <option <?php if ($output['extension_array']['extension_class'] == 0){ ?>selected<?php } ?>
                            value="0">线上
                    </option>
                    <option <?php if ($output['extension_array']['extension_class'] == 1){ ?>selected<?php } ?>
                            value="1">线下
                    </option>
                </select>
                <span class="err"></span>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label class="area_info"><em>*</em>地区</label>
            </dt>
            <dd class="opt">
                <input type="hidden" id="area_info" name="area_info"
                       value="<?php echo $output['extension_array']['area_info']; ?>"/>
                <span class="err"></span>
            </dd>
        </dl>
        <?php if ($output['extension_array']['qrcode_img']) { ?>
            <dl class="row">
                <dt class="tit">
                    <label class="area_info">该用户邀请二维码</label>
                </dt>
                <dd class="opt">
                    <a href="<?php echo $output['extension_array']['qrcode_img']; ?>" target="_blank">
                        <img src="<?php echo $output['extension_array']['qrcode_img']; ?>" alt="" style="height: 100px;">
                    </a>
                    <br>
                    <span class="err">点图片看原图</span>
                </dd>
            </dl>
        <?php } ?>
        <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green"
                            id="submitBtn"><?php echo $lang['nc_submit']; ?></a></div>
    </div>
</form>

<!--自动关闭编辑页面-->
<div id="fwin_member"></div>

<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL; ?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL; ?>/js/jquery.nyroModal.js"></script>

<script type="text/javascript">
    $(function () {
        //地区加载
        $("#area_info").nc_region();
        //验证表单再提交
        $("#submitBtn").click(function () {
            if ($("#extension_form").valid()) {
                var url = '<?php echo urlAdminSystem('member', 'member_edit_extension');?>';
                var data = $("#extension_form").serialize();
                $.post(url, data, function (data) {
                    $('#fwin_member').html(data);
                })
            }
        });
        $('#extension_form').validate({
            errorPlacement: function (error, element) {
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules: {
                member_mobile: {
                    required: true,
                    mobile: true,
                },
                extension_name: {
                    required: true,
                    maxlength: 30,
                },
                area_info: {
                    required: true,
                },
            },
            messages: {
                extension_name: {
                    required: "请输入内容。",
                    maxlength: "请输入不超过30个字符。",
                },
                member_mobile: {
                    required: "请输入内容",
                },
                area_info: {
                    required: "请选择地区",
                },

            }
        });
    });
</script>
