<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=distribution_config&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>参数配置</h3>
                <h5>分销等级基数参数配置</h5>
            </div>
        </div>
    </div>
    <!-- 操作说明    -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>注意微合伙人分销返佣和购买返佣的金额叠加情况，分销返佣默认包含购买返佣。</li>
        </ul>
    </div>
    <form id="user_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="dis_id" value="<?php echo $output['configArr']['id'];?>" />
        <input type="hidden" name="level" value="<?php echo $output['detailArr']['level'];?>" />
        <input type="hidden" name="id" value="<?php echo $output['detailArr']['id'];?>" />
        <input type="hidden" name="type_id" value="<?php echo $output['configArr']['type_id'];?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>名称</label>
                </dt>
                <dd class="opt"><?php echo $output['configArr']['name'];?></dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="dz_num"><em>*</em>秀才分销返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?=$output['detailArr']['dz_num']?>" id="dz_num" name="dz_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推秀才级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="wdl_num"><em>*</em>举人分销返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?=$output['detailArr']['wdl_num']?>" id="wdl_num" name="wdl_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推举人级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="member_truename"><em>*</em>贡士返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['detailArr']['zl_num']?>" id="zl_num" name="zl_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推贡士级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="sy_num"><em>*</em>进士返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['detailArr']['sy_num']?>" id="sy_num" name="sy_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推进士级别分销返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="artist_num"><em>*</em>艺术家返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['detailArr']['artist_num']?>" id="artist_num" name="artist_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推艺术家返佣百分比</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="goods_num"><em>*</em>购买返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['detailArr']['goods_num']?>" id="goods_num" name="goods_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推用户购买行为返佣百分比(商品总金额百分比,终身有效)</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label class="bzj_num"><em>*</em>投资返佣</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?= $output['detailArr']['bzj_num']?>" id="bzj_num" name="bzj_num" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">直推投资用户返佣百分比(百分比为用户投资年化收益的百分比,终身有效)</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?php echo ADMIN_RESOURCE_URL;?>/js/jquery.nyroModal.js"></script>

<script type="text/javascript">

$(function(){
    $("#submitBtn").click(function(){
        if($("#user_form").valid()){
            $("#user_form").submit();
        }
    });

    $('#user_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span.err');
            error_td.append(error);
        },
        rules: {
            dz_num: {
                required: true,
            },
            wdl_num: {
                required: true,
            },
            zl_num: {
                required: true,
            },
            sy_num: {
                required: true,
            },
            artist_num: {
                required: true,
            },
            goods_num: {
                required: true,
            },
            bzj_num: {
                required: true,
            }
        },
        messages: {
            dz_num: {
                required : "请填写秀才分销返佣",
            },
            wdl_num: {
                required : "请填写举人分销返佣",
            },
            zl_num: {
                required : "请填写贡士返佣",
            },
            sy_num: {
                required : "请填写进士返佣",
            },
            artist_num: {
                required : "请填写艺术家返佣",
            },
            goods_num: {
                required : "请填写购买返佣",
            },
            bzj_num: {
                required : "请填写投资返佣",
            }
        }
    });
});
</script>
