<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="index.php?act=article&op=article" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>返佣管理 - 添加返佣记录</h3>
        <h5>手动添加会员返佣记录信息</h5>
      </div>
    </div>
  </div>
  <form id="commis_form" method="post" name="commisForm">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncap-form-default">
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>返佣收益人</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="dis_member_name" id="dis_member_name" class="w270">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label>返佣来源</label>
        </dt>
        <dd class="opt">
          <input name="commission_type" checked="checked" value="1" type="radio">商城
          <input name="commission_type" value="2" type="radio">拍卖
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>订单编号</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="order_sn" id="order_sn" class="w300">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>商品编号</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="goods_id" id="goods_id" class="w100">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>商品支付金额</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="goods_pay_amount" id="goods_pay_amount" class="w100"> 元
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>返佣比例</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="dis_commis_rate" id="dis_commis_rate" class="w50"> %
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>返佣金额</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="commission_amount" id="commission_amount" class="w100"> 元
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>返佣来源用户</label>
        </dt>
        <dd class="opt">
          <input type="text" value="" name="from_member_name" id="from_member_name" class="w270">
          <span class="err"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
    </div>
  </form>
</div>

<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#commis_form").valid()){
      $("#commis_form").submit();
	  }
	});
});

$(document).ready(function(){
	$('#commis_form').validate({
    errorPlacement: function(error, element){
			var error_td = element.parent('dd').children('span.err');
      error_td.append(error);
    },
    rules : {
      dis_member_name : {
        required   : true,
        remote     : {
          url: "index.php?act=commission&op=ajax&type=check_member_name",
          typr:"get",
          data:{
            member_name : function(){
              return $('#dis_member_name').val();
            }
          }
        }
      },
      order_sn : {
        required   : true,
        remote     : {
          url: "index.php?act=commission&op=ajax&type=check_order_sn",
          typr:"get",
          data:{
            order_sn : function(){
              return $('#order_sn').val();
            },
            commis_from:function(){
              return $('commission_type').val();
            }
          }
        }
      },
      goods_id : {
        required   : true,
        number     : true,
        min        : 1,
        digits : true,
        remote     : {
          url: "index.php?act=commission&op=ajax&type=check_goods_id",
          typr:"get",
          data:{
            goods_id : function(){
              return $('#goods_id').val();
            },
            commis_from:function(){
              return $('commission_type').val();
            }
          }
        }
      },
      goods_pay_amount : {
        required : true,
        number : true,
        min : 0.01,
        max : 999999999
      },      
      dis_commis_rate : {
        required : true,
        number : true,
        min : 0.1,
        max : 100
      },
      commission_amount : {
        required : true,
        number : true,
        min : 0.01,
        max : function(){
          var p_amount = parseInt($('#goods_pay_amount').val());
          var d_rate = parseFloat($('#dis_commis_rate').val());
          return p_amount * d_rate/100;
        }
      },
      from_member_name : {
        required   : true,
        remote     : {
          url: "index.php?act=commission&op=ajax&type=check_member_name",
          typr:"get",
          data:{
            member_name : function(){
              return $('#from_member_name').val();
            }
          }
        }
      }
    },
    messages : {
      dis_member_name : {
        required   : '<i class="fa fa-exclamation-circle"></i>返佣收益人不能为空',
        remote     : '<i class="fa fa-exclamation-circle"></i>返佣收益人不存在'
      },
      order_sn : {
        required   : '<i class="fa fa-exclamation-circle"></i>订单编号不能为空',
        remote     : '<i class="fa fa-exclamation-circle"></i>订单不存在'
      },
      goods_id : {
        required   : '<i class="fa fa-exclamation-circle"></i>商品编号不能为空',
        number     : '<i class="fa fa-exclamation-circle"></i>商品编号必须为大于0的正整数',
        min        : '<i class="fa fa-exclamation-circle"></i>商品编号必须为大于0的正整数',
        digits     : '<i class="fa fa-exclamation-circle"></i>商品编号必须为大于0的正整数',
        remote     : '<i class="fa fa-exclamation-circle"></i>商品不存在'
      },
      goods_pay_amount : {
        required : '<i class="fa fa-exclamation-circle"></i>商品支付金额不能为空',
        number : '<i class="fa fa-exclamation-circle"></i>商品支付金额必须为0.01~999999999数字',
        min : '<i class="fa fa-exclamation-circle"></i>商品支付金额必须为0.01~999999999数字',
        max : '<i class="fa fa-exclamation-circle"></i>商品支付金额必须为0.01~999999999数字',
      },      
      dis_commis_rate : {
        required : '<i class="fa fa-exclamation-circle"></i>返佣比例不能为空',
        number : '<i class="fa fa-exclamation-circle"></i>返佣比例必须为0.01~999999999数字',
        min : '<i class="fa fa-exclamation-circle"></i>返佣比例必须为0.1~100数字',
        max : '<i class="fa fa-exclamation-circle"></i>返佣比例必须为0.1~100数字',
      },
      commission_amount : {
        required : '<i class="fa fa-exclamation-circle"></i>返佣金额不能为空',
        number : '<i class="fa fa-exclamation-circle"></i>返佣金额必须为0.01~999999999数字',
        min : '<i class="fa fa-exclamation-circle"></i>返佣金额必须为0.01~999999999数字',
        max : '<i class="fa fa-exclamation-circle"></i>返佣金额必须为0.01~999999999数字',
      },
      from_member_name : {
        required   : '<i class="fa fa-exclamation-circle"></i>返佣来源用户不能为空',
        remote     : '<i class="fa fa-exclamation-circle"></i>来源用户不存在'
      }
    }
  });
});
</script>