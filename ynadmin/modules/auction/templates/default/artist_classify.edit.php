<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=artist_classify&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>艺术家分类 - 编辑分类</h3>
            </div>
        </div>
    </div>
    <form id="classify_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="artist_classify_id" value="<?php echo $output['artist_classify_array']['artist_classify_id'];?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="artist_classify_name"><em>*</em>艺术家分类名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['artist_classify_array']['artist_classify_name'];?>" name="artist_classify_name" id="artist_classify_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(){
            if($("#classify_form").valid()){
                $("#classify_form").submit();
            }
        });
        $('#classify_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                artist_classify_name: {
                    required : true,
                    remote   : {
                        url :'index.php?act=artist_classify&op=ajax&branch=check_classify_name',
                        type:'get',
                        data:{
                            artist_classify_name : function(){
                                return $('#artist_classify_name').val();
                            },
                            artist_classify_id : '<?php echo $output['artist_classify_array']['artist_classify_id'];?>'
                        }
                    }
                }
            },
            messages : {
                artist_classify_name: {
                    required : '<i class="fa fa-exclamation-circle"></i>分类名称不能为空',
                    remote   : '<i class="fa fa-exclamation-circle"></i>分类名称已经存在'
                }
            }
        });
    });
</script>
