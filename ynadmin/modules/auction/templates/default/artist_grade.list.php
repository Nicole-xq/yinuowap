<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>艺术家等级管理</h3>
            </div>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=artist_grade&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 60, sortable : false, align: 'center', className: 'handle-s'},
                {display: '等级ID', name : 'artist_grade_id', width : 40, sortable : true, align: 'center'},
                {display: '等级名称', name : 'artist_grade_name', width : 150, sortable : true, align: 'left'}

            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],
            searchitems : [
                {display: '等级ID', name : 'artist_grade_id'},
                {display: '等级名称', name : 'artist_grade_name'}
            ],
            sortname: "artist_grade_id",
            sortorder: "desc",
            title: '艺术家等级列表'
        });

    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=artist_grade&op=grade_add';
        }

    }


</script>

