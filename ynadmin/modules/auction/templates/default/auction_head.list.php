<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .flexigrid .fbutton .show:hover, .flexigrid .fbutton.fbOver .show:hover {
        color: #E84C3D;
        border-color: #E84C3D;
    }
</style>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>拍卖头条管理</h3>
            </div>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom"><i class="fa fa-lightbulb-o"></i>
            <h4 title="<?php echo $lang['nc_prompts_title'];?>"><?php echo $lang['nc_prompts'];?></h4>
            <span id="explanationZoom" title="<?php echo $lang['nc_prompts_span'];?>"></span> </div>
        <ul>
            <li>批量显示，选中即可在前台播放</li>
        </ul>
    </div>
    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=auction_headline&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 150, sortable : false, align: 'center', className: 'handle'},
                {display: '排序', name : 'au_head_sort', width : 40, sortable : true, align: 'center'},
                {display: '标题', name : 'au_head_title', width : 240, sortable : true, align: 'left'},
                {display: '链接', name : 'au_head_url', width : 300, sortable : true, align: 'left'},
                {display: '显示', name : 'is_show', width: 80, sortable : true, align : 'center'},
                {display: '添加时间', name : 'add_time', width: 160, sortable : true, align : 'center'}
            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operate },
                {display: '<i class="fa fa-trash"></i>批量显示', name : 'show', bclass : 'show', title : '将选定行数据批量显示', onpress : fg_operate }
            ],
            sortname: "au_head_sort",
            sortorder: "asc",
            title: '拍卖头条列表'
        });
        $('input[name="q"]').prop('placeholder','搜索标题内容...');
    });
    function fg_operate(name, bDiv) {
        if (name == 'show') {
            if($('.trSelected',bDiv).length>0){
                var itemlist = new Array();
                $('.trSelected',bDiv).each(function(){
                    itemlist.push($(this).attr('data-id'));
                });
                fg_show(itemlist);
            } else {
                return false;
            }
        } else if (name == 'add') {
            window.location.href = 'index.php?act=auction_headline&op=au_head_add';
        }
    }

    function fg_show(id) {
        if (typeof id == 'number') {
            var id = new Array(id.toString());
        };
        if(confirm('确认选中这 ' + id.length + ' 项吗？')){
            id = id.join(',');
        } else {
            return false;
        }
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "index.php?act=auction_headline&op=show_edit",
            data: "au_head_id="+id,
            success: function(data){
                if (data.state){
                    $("#flexigrid").flexReload();
                } else {
                    alert(data.msg);
                }
            }
        });
    }



    function fg_delete(id) {
        if (typeof id == 'number') {
            var id = new Array(id.toString());
        };
        if(confirm('删除后将不能恢复，确认删除这 ' + id.length + ' 项吗？')){
            id = id.join(',');
        } else {
            return false;
        }
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "index.php?act=auction_headline&op=au_head_delete",
            data: "del_id="+id,
            success: function(data){
                if (data.state){
                    $("#flexigrid").flexReload();
                } else {
                    alert(data.msg);
                }
            }
        });
    }
</script>
