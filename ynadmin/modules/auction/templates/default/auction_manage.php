<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>拍卖行设置</h3>
                <h5>拍卖行基础信息设置</h5>
            </div>
        </div>
    </div>
    <form id="add_form" method="post" enctype="multipart/form-data" action="index.php?act=manage&op=manage_save">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="distribute_isuse">拍卖行开关</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="isuse_1" class="cb-enable <?php if($output['setting']['auction_isuse'] == '1'){ ?>selected<?php } ?>" title="<?php echo $lang['nc_open'];?>"><?php echo $lang['nc_open'];?></label>
                        <label for="isuse_0" class="cb-disable <?php if($output['setting']['auction_isuse'] == '0'){ ?>selected<?php } ?>" title="<?php echo $lang['nc_close'];?>"><?php echo $lang['nc_close'];?></label>
                        <input type="radio" id="isuse_1" name="auction_isuse" value="1" <?php echo $output['setting']['auction_isuse']==1?'checked=checked':''; ?>>
                        <input type="radio" id="isuse_0" name="auction_isuse" value="0" <?php echo $output['setting']['auction_isuse']==0?'checked=checked':''; ?>>
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="class_image"><?php echo '拍卖行'.'LOGO';?></label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show"><span class="show">
            <?php if(empty($output['setting']['auction_logo'])) { ?>
                <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_AUCTION.DS.'logo.png';?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_AUCTION.DS.'logo.png';?>>')" onMouseOut="toolTip()"></i></a>
            <?php } else { ?>
                <a class="nyroModal" rel="gal" href="<?php echo UPLOAD_SITE_URL.DS.ATTACH_AUCTION.DS.$output['setting']['auction_logo'];?>"> <i class="fa fa-picture-o" onMouseOver="toolTip('<img src=<?php echo UPLOAD_SITE_URL.DS.ATTACH_AUCTION.DS.$output['setting']['auction_logo'];?>>')" onMouseOut="toolTip()"></i> </a>
            <?php } ?>
            </span> <span class="type-file-box">
            <input name="auction_logo" type="file" class="type-file-file" id="auction_logo" size="30" hidefocus="true" nc_type="auction_image">
            </span></div>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <div class="bot"><a id="submit" href="javascript:void(0)" class="ncap-btn-big ncap-btn-green"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        //文件上传
        var textButton1="<input type='text' name='textfield' id='textfield1' class='type-file-text' /><input type='button' name='button' id='button1' value='选择上传...' class='type-file-button' />";
        $(textButton1).insertBefore("#auction_logo");
        $("#auction_logo").change(function(){
            $("#textfield1").val($("#auction_logo").val());
        });
        $("input[nc_type='auction_image']").live("change", function(){
            var src = getFullPath($(this)[0]);
            $(this).parent().prev().find('.low_source').attr('src',src);
            $(this).parent().find('input[class="type-file-text"]').val($(this).val());
        });

        $("#submit").click(function(){
            $("#add_form").submit();
        });

    });
</script>
