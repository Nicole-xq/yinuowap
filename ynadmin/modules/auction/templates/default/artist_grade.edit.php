<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=artist_grade&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>艺术家等级 - 编辑等级</h3>
            </div>
        </div>
    </div>
    <form id="grade_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="artist_grade_id" value="<?php echo $output['artist_grade_array']['artist_grade_id'];?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="artist_grade_name"><em>*</em>艺术家等级名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['artist_grade_array']['artist_grade_name'];?>" name="artist_grade_name" id="artist_grade_name" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(){
            if($("#grade_form").valid()){
                $("#grade_form").submit();
            }
        });
        $('#grade_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                artist_grade_name: {
                    required : true,
                    remote   : {
                        url :'index.php?act=artist_grade&op=ajax&branch=check_grade_name',
                        type:'get',
                        data:{
                            artist_grade_name : function(){
                                return $('#artist_grade_name').val();
                            },
                            artist_grade_id : '<?php echo $output['artist_grade_array']['artist_grade_id'];?>'
                        }
                    }
                }
            },
            messages : {
                artist_grade_name: {
                    required : '<i class="fa fa-exclamation-circle"></i>分类等级不能为空',
                    remote   : '<i class="fa fa-exclamation-circle"></i>分类等级已经存在'
                }
            }
        });
    });
</script>
