<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>艺术家分类管理</h3>
            </div>
        </div>
    </div>

    <div id="flexigrid"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#flexigrid").flexigrid({
            url: 'index.php?act=artist_classify&op=get_xml',
            colModel : [
                {display: '操作', name : 'operation', width : 60, sortable : false, align: 'center', className: 'handle-s'},
                {display: '分类ID', name : 'artist_classify_id', width : 40, sortable : true, align: 'center'},
                {display: '分类名称', name : 'artist_classify_name', width : 150, sortable : true, align: 'left'}

            ],
            buttons : [
                {display: '<i class="fa fa-plus"></i>新增数据', name : 'add', bclass : 'add', title : '新增数据', onpress : fg_operation }
            ],
            searchitems : [
                {display: '分类ID', name : 'artist_classify_id'},
                {display: '分类名称', name : 'artist_classify_name'}
            ],
            sortname: "artist_classify_id",
            sortorder: "desc",
            title: '艺术家分类列表'
        });

    });

    function fg_operation(name, bDiv) {
        if (name == 'add') {
            window.location.href = 'index.php?act=artist_classify&op=classify_add';
        }

    }


</script>

