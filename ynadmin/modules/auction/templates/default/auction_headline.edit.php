<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=auction_headline&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>拍卖头条管理 - 编辑头条“<?php echo $output['au_head_array']['au_head_title'];?>”</h3>
            </div>
        </div>
    </div>
    <form id="headline_form" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="au_head_id" value="<?php echo $output['au_head_array']['au_head_id'];?>" />
        <input type="hidden" name="ref_url" value="<?php echo getReferer();?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="article_title"><em>*</em>标题</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['au_head_array']['au_head_title'];?>" name="au_head_title" id="au_head_title" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label for="article_url"><em>*</em>链接地址</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['au_head_array']['au_head_url'];?>" name="au_head_url" id="au_head_url" class="input-txt">
                    <span class="err"></span>
                    <p class="notic">当填写"链接"后点击文章标题将直接跳转至链接地址，不显示文章内容。链接格式请以http://开头</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="if_show">显示</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="is_show1" class="cb-enable <?php if($output['au_head_array']['is_show'] == '1'){ ?>selected<?php } ?>" ><?php echo $lang['nc_yes'];?></label>
                        <label for="is_show0" class="cb-disable <?php if($output['au_head_array']['is_show'] == '0'){ ?>selected<?php } ?>" ><?php echo $lang['nc_no'];?></label>
                        <input id="is_show1" name="is_show" <?php if($output['au_head_array']['is_show'] == '1'){ ?>checked="checked"<?php } ?>  value="1" type="radio">
                        <input id="is_show0" name="is_show" <?php if($output['au_head_array']['is_show'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit"><em>*</em>排序</dt>
                <dd class="opt">
                    <input type="text" value="<?php echo $output['au_head_array']['au_head_sort'];?>" name="au_head_sort" id="au_head_sort" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script>
    //按钮先执行验证再提交表单
    $(function(){$("#submitBtn").click(function(){
        if($("#headline_form").valid()){
            $("#headline_form").submit();
        }
    });
    });
    //
    $(document).ready(function(){
        $('#headline_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },
            rules : {
                au_head_title : {
                    required   : true
                },

                au_head_url : {
                    required   : true,
                    url : true
                },

                article_sort : {
                    required   : true,
                    number   : true
                }
            },
            messages : {
                au_head_title : {
                    required : '<i class="fa fa-exclamation-circle"></i>标题不能为空'
                },
                au_head_url : {
                    required : '<i class="fa fa-exclamation-circle"></i>链接地址不能为空',
                    url : '<i class="fa fa-exclamation-circle"></i>链接地址错误'
                },

                au_head_sort  : {
                    required : '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    number   : '<i class="fa fa-exclamation-circle"></i>排序只能是数字'
                }
            }
        });

    });

</script>