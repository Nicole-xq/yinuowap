<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>艺术家编辑管理</h3>
            </div>
        </div>
    </div>

    <div id="flexigrid"></div>

</div>
<script type="text/javascript">
    $(function() {
        $("#flexigrid").flexigrid({
            url: 'index.php?act=artist_edit&op=get_xml',
            colModel: [
                {display: '操作', name: 'operation', width: 150, sortable: false, align: 'center', className: 'handle'},
                {display: '艺术家拍卖ID', name: 'artist_vendue_id', width: 80, sortable: true, align: 'center'},
                {display: '艺术家名称', name: 'artist_name', width: 150, sortable: false, align: 'left'},
                {display: '艺术家职称', name: 'artist_job_title', width: 150, sortable: false, align: 'center'},
                {display: '艺术家分类', name: 'artist_classify', width: 100, sortable: false, align: 'center'},
//                {display: '艺术家等级', name: 'artist_grade', width: 100, sortable: true, align: 'center'},
                {display: '是否推荐', name: 'is_rec', width: 80, sortable: true, align: 'center'},
                {display: '排序', name: 'artist_sort', width: 80, sortable: true, align: 'center'}

            ],
            searchitems: [
                {display: '艺术家名称', name: 'artist_name', isdefault: true}

            ],
            sortname: "artist_vendue_id",
            sortorder: "asc",
            title: '艺术家列表'
        });
    });



</script>