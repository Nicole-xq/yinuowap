<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="index.php?act=artist_edit&op=index" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>艺术家管理 - 编辑艺术家“<?php echo $output['vendue_info']['artist_name'];?>”</h3>
            </div>
        </div>
    </div>
    <form id="artist_form" enctype="multipart/form-data" method="post">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="artist_vendue_id" value="<?php echo $output['vendue_info']['artist_vendue_id'];?>" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>艺术家名称</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['vendue_info']['artist_name'];?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="member_passwd">艺术家职称</label>
                </dt>
                <dd class="opt">
                    <?php echo $output['vendue_info']['artist_job_title'];?>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label>艺术家分类标签</label>
                </dt>
                <dd class="opt">
                    <select name="artist_classify" id="artist_classify">
                        <option value="0">请选择</option>
                        <?php if(!empty($output['classify_list'])) {?>
                            <?php foreach($output['classify_list'] as $k=>$v){?>
                                <option value="<?php echo $v['artist_classify_id']?>" <?php if($output['vendue_info']['artist_classify'] == $v['artist_classify_id']){?>selected="selected"<?php }?>><?php echo $v['artist_classify_name']?></option>
                            <?php }?>
                        <?php }?>
                    </select>
                    <input type="hidden" name="classify_id" id="classify_id" value="<?php echo $output['vendue_info']['artist_classify'];?>"/>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>是否推荐</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="is_rec1" class="cb-enable <?php if($output['vendue_info']['is_rec'] == '1'){ ?>selected<?php } ?>" ><span>是</span></label>
                        <label for="is_rec0" class="cb-disable <?php if($output['vendue_info']['is_rec'] == '0'){ ?>selected<?php } ?>" ><span>否</span></label>
                        <input id="is_rec1" name="is_rec" <?php if($output['vendue_info']['is_rec'] == '1'){ ?>checked="checked"<?php } ?>  value="1" type="radio">
                        <input id="is_rec0" name="is_rec" <?php if($output['vendue_info']['is_rec'] == '0'){ ?>checked="checked"<?php } ?> value="0" type="radio">
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>排序</label>
                </dt>
                <dd class="opt">
                    <input type="text" class="input-txt" value="<?php echo $output['vendue_info']['artist_sort'];?>"  name="artist_sort" id="artist_sort"/>
                    <span></span>
                    <p class="notic">请填写自然数。艺术家将会根据排序进行由小到大排列显示。</p>
                </dd>
            </dl>
            <div class="bot"><a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" id="submitBtn"><?php echo $lang['nc_submit'];?></a></div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#artist_classify').each(function(k,v) {
            var option=$(v);
            option.on("click",function(){
                var hml=$(this).val();
                $('#classify_id').val(hml);
            })
        });
        $('#artist_grade').each(function(k,v) {
            var option=$(v);
            option.on("click",function(){
                var hml=$(this).val();
                $('#grade_id').val(hml);
            })
        });

        $("#submitBtn").click(function(){
            if($("#artist_form").valid()) {
                $("#artist_form").submit();
            }
        });

        $.validator.addMethod('getClssify', function(value,element){
            _artist_classify =  $('#classify_id').val();
            if(_artist_classify == 0 || _artist_classify == '') {
                return false;
            }else {
                return true;
            }
        }, '');
        $.validator.addMethod('getGrade', function(value,element){
            _artist_grade =  $('#grade_id').val();
            if(_artist_grade == 0 || _artist_grade == '') {
                return false;
            }else {
                return true;
            }
        }, '');
        $('#artist_form').validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd').children('span.err');
                error_td.append(error);
            },

            rules : {

                artist_sort: {
                    required : true,
                    digits	 : true
                },
                classify_id :{
                    getClssify : true
                }
//                grade_id :{
//                    getGrade : true
//                }
            },
            messages : {
                artist_sort: {
                    required : '<i class="fa fa-exclamation-circle"></i>排序不能为空',
                    digits   : '<i class="fa fa-exclamation-circle"></i>排序必须是数字'
                },
                classify_id :{
                    getClssify :'<i class="fa fa-exclamation-circle"></i>请选择艺术家分类标签'
                }
//                grade_id :{
//                    getGrade :'<i class="fa fa-exclamation-circle"></i>请选择艺术家等级'
//                }
            }
        });



    });
</script>


