<?php
/**
 * 拍卖行设置
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class manageControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        $this->manageOp();
    }

    /**
     * 拍卖行设置
     */
    public function manageOp() {
        $model_setting = Model('setting');
        $setting_list = $model_setting->getListSetting();
        Tpl::output('setting',$setting_list);
        Tpl::showpage('auction_manage');
    }

    /**
     * 保存拍卖设置
     */
    public function manage_saveOp(){
        $model_setting = Model('setting');
        $update_array = array();
        $update_array['auction_isuse'] = intval($_POST['auction_isuse']);
        $old_image = '';
        if(!empty($_FILES['auction_logo']['name'])) {
            $upload = new UploadFile();
            $upload->set('default_dir',ATTACH_AUCTION);
            $result = $upload->upfile('auction_logo');
            if(!$result) {
                showMessage($upload->error);
            }
            $update_array['auction_logo'] = $upload->file_name;
            $old_image = BASE_UPLOAD_PATH.DS.ATTACH_AUCTION.DS.C('auction_logo');
        }


        $result = $model_setting->updateSetting($update_array);
        if ($result === true){
            if(!empty($old_image) && is_file($old_image)) {
                unlink($old_image);
            }
            showMessage(Language::get('nc_common_save_succ'));
        }else {
            showMessage(Language::get('nc_common_save_fail'));
        }
    }
}