<?php
/**
 * 艺术家编辑管理界面
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class artist_editControl extends SystemControl{

    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('artist_edit.index');
    }



    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_artist_vendue = Model('artist_vendue');
        // 设置页码参数名称
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('artist_vendue_id','artist_name','artist_job_title','artist_sort');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }

        $page = $_POST['rp'];
        $condition['store_apply_state'] = 40;

        //艺术家拍卖列表
        $artist_vendue_list = $model_artist_vendue->getArtist_StoreList($condition, $page, $order);

        foreach($artist_vendue_list as $k=>$v){
            if($v['artist_classify'] != '' && $v['artist_grade'] == ''){
                $artist_vendue_list[$k] = $model_artist_vendue->getArtist_vendueClassifyInfo(array('artist_classify'=>$v['artist_classify'],'artist_vendue_id'=>$v['artist_vendue_id']));
            }elseif($v['artist_classify'] == '' && $v['artist_grade'] != ''){
                $artist_vendue_list[$k] = $model_artist_vendue->getArtist_vendueGradeInfo(array('artist_grade'=>$v['artist_grade'],'artist_vendue_id'=>$v['artist_vendue_id']));
            }elseif($v['artist_classify'] != '' && $v['artist_grade'] != ''){
                $artist_vendue_list[$k] = $model_artist_vendue->getArtist_vendueJoinInfo(array('artist_classify'=>$v['artist_classify'],'artist_grade'=>$v['artist_grade'],'artist_vendue_id'=>$v['artist_vendue_id']));
            }elseif($v['artist_classify'] == '' && $v['artist_grade'] == ''){
                $artist_vendue_list[$k] = $model_artist_vendue->getArtistInfo(array('artist_vendue_id'=>$v['artist_vendue_id']));
            }
        }

        $data = array();
        $data['now_page'] = $model_artist_vendue->shownowpage();
        $data['total_num'] = $model_artist_vendue->gettotalnum();

        foreach ($artist_vendue_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=artist_edit&op=artist_edit&artist_vendue_id=".$value['artist_vendue_id']."'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['artist_vendue_id'] = $value['artist_vendue_id'];
            $param['artist_name'] = $value['artist_name'];
            $param['artist_job_title'] = $value['artist_job_title'];
            if($value['artist_classify'] != ''){
                $param['artist_classify'] = $value['artist_classify_name'];
            }else{
                $param['artist_classify'] = '';
            }
//            if($value['artist_grade'] != ''){
//                $param['artist_grade'] = $value['artist_grade_name'];
//            }else{
//                $param['artist_grade'] = '';
//            }
            if($value['is_rec'] == 1){
                $param['is_rec'] = '是';
            }else{
                $param['is_rec'] = '否';
            }
            $param['artist_sort'] = $value['artist_sort'];
            $data['list'][$value['artist_vendue_id']] = $param;
        }

        echo Tpl::flexigridXML($data);exit();
    }


    /**
     * 艺术家编辑
     */
    public function artist_editOp(){

        $model_artist_vendue = Model('artist_vendue');
        $model_artist_classify = Model('artist_classify');
        $model_artist_grade = Model('artist_grade');

        //保存
        if (chksubmit()){
            $update_array = array();
            $update_array['is_rec'] = $_POST['is_rec'];
            $update_array['artist_classify'] = $_POST['artist_classify'];
//            $update_array['artist_grade'] = $_POST['artist_grade'];
            $update_array['artist_sort'] = $_POST['artist_sort'];
            $condition = [
                'artist_vendue_id' => ['neq',$_POST['artist_vendue_id']],
                'artist_sort' => $_POST['artist_sort']
            ];
            $sort_exist = $model_artist_vendue->getArtistInfo($condition, $order = '', 'artist_vendue_id');
            if (count($sort_exist) > 0){
                showMessage($_POST['artist_sort'].'排序已存在');
            }

            $result = $model_artist_vendue->editArtist_vendue($update_array, array('artist_vendue_id' => $_POST['artist_vendue_id']));
            if ($result){
                showMessage('编辑成功','index.php?act=artist_edit&op=index');
            }else {
                showMessage('编辑失败');
            }
        }

        $vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('artist_vendue_id'=>$_GET['artist_vendue_id']));
        $classify_list = $model_artist_classify->getArtist_classifyList(array());
        $grade_list = $model_artist_grade->getArtist_gradeList(array());

        Tpl::output('vendue_info', $vendue_info);
        Tpl::output('classify_list', $classify_list);
        Tpl::output('grade_list', $grade_list);
        Tpl::showpage('artist_edit.detail');
    }




}
