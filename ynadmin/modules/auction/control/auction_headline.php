<?php
/**
 * 拍卖头条
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class auction_headlineControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('auction_head.list');
    }
    /**
     * 编辑拍卖头条
     */
    public function au_head_editOp(){
        $model_au_head = Model('auction_headline');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["au_head_title"], "require"=>"true", "message"=>'标题不能为空'),
                array("input"=>$_POST["au_head_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序只能是数字'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $update_array = array();
                $update_array['au_head_title'] = trim($_POST['au_head_title']);
                $update_array['au_head_url'] = trim($_POST['au_head_url']);
                $update_array['is_show'] = trim($_POST['is_show']);
                $update_array['au_head_sort'] = trim($_POST['au_head_sort']);

                $result = $model_au_head->editAu_head($update_array,array('au_head_id'=>intval($_POST['au_head_id'])));
                if ($result){
                    showMessage('编辑拍卖头条成功','index.php?act=auction_headline&op=index');
                }else {
                    showMessage('编辑拍卖头条失败');
                }
            }
        }
        $condition['au_head_id'] = intval($_GET['au_head_id']);
        $au_head_array = $model_au_head->getAu_headInfo($condition);

        Tpl::output('au_head_array',$au_head_array);
        Tpl::showpage('auction_headline.edit');
    }

    /**
     * 新增拍卖头条
     */
    public function au_head_addOp(){
        $model_au_head = Model('auction_headline');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["au_head_title"], "require"=>"true", "message"=>'标题不能为空'),
                array("input"=>$_POST["au_head_sort"], "require"=>"true", 'validator'=>'Number', "message"=>'排序只能是数字'),

            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['au_head_title'] = trim($_POST['au_head_title']);
                $insert_array['au_head_url'] = trim($_POST['au_head_url']);
                $insert_array['is_show'] = trim($_POST['is_show']);
                $insert_array['au_head_sort'] = trim($_POST['au_head_sort']);
                $insert_array['add_time'] = time();

                $result = $model_au_head->addAu_head($insert_array);
                if ($result){
                    showMessage('新增拍卖头条成功','index.php?act=auction_headline&op=index');
                }else {
                    showMessage('新增拍卖头条失败');
                }
            }
        }
        Tpl::showpage('auction_headline.add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 验证会员是否重复
             */
            case 'check_classify_name':
                $model_artist_classify = Model('artist_classify');
                $condition['artist_classify_name']   = $_GET['artist_classify_name'];
                $condition['artist_classify_id'] = array('neq',intval($_GET['artist_classify_id']));
                $list = $model_artist_classify->getArtist_classifyInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_au_head = Model('auction_headline');
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('au_head_id','au_head__title','au_head__url','is_show','au_head_sort','add_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $au_head_list = $model_au_head->getAu_headList($condition, $page,$order,'*');

        $data = array();
        $data['now_page'] = $model_au_head->shownowpage();
        $data['total_num'] = $model_au_head->gettotalnum();
        foreach ($au_head_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn red' onclick=\"fg_delete({$value['au_head_id']})\"><i class='fa fa-trash-o'></i>删除</a><a class='btn blue' href='index.php?act=auction_headline&op=au_head_edit&au_head_id={$value['au_head_id']}'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['au_head_sort'] = $value['au_head_sort'];
            $param['au_head_title'] = $value['au_head_title'];
            $param['au_head_url'] = $value['au_head_url'];
            $param['is_show'] = $value['is_show'] == 0 ? '<span class="no"><i class="fa fa-ban"></i>否</span>' : '<span class="yes"><i class="fa fa-check-circle"></i>是</span>';
            $param['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
            $data['list'][$value['au_head_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }


    public function show_editOp(){
        $model_au_head = Model('auction_headline');
        if (preg_match('/^[\d,]+$/', $_GET['au_head_id'])) {
            $_GET['au_head_id'] = explode(',',trim($_GET['au_head_id'],','));
            $where = array('au_head_id' => array('in', $_GET['au_head_id']));
            $update = array();
            $update['is_show']        = 1;
            $model_au_head->all_edit($update,$where);
            exit(json_encode(array('state'=>true,'msg'=>'编辑成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'编辑失败')));
        }

    }



    public function au_head_deleteOp(){
        $model_au_head = Model('auction_headline');
        if (preg_match('/^[\d,]+$/', $_GET['del_id'])) {
            $_GET['del_id'] = explode(',',trim($_GET['del_id'],','));
            foreach ($_GET['del_id'] as $k => $v){
                $v = intval($v);
                $model_au_head->del($v);
            }
            exit(json_encode(array('state'=>true,'msg'=>'删除成功')));
        } else {
            exit(json_encode(array('state'=>false,'msg'=>'删除失败')));
        }
    }




}