<?php
/**
 * 艺术家等级
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class artist_gradeControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('artist_grade.list');
    }
    /**
     * 艺术家分类编辑
     */
    public function grade_editOp(){
        $model_artist_grade = Model('artist_grade');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["artist_grade_name"], "require"=>"true",  "message"=>'艺术家等级不能为空'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $update_array = array();
                $update_array['artist_grade_name']       = $_POST['artist_grade_name'];

                $result = $model_artist_grade->editArtist_grade($update_array,array('artist_grade_id'=>intval($_POST['artist_grade_id'])));
                if ($result){
                    showMessage('编辑艺术家等级成功','index.php?act=artist_grade&op=index');
                }else {
                    showMessage('编辑艺术家等级失败');
                }
            }
        }
        $condition['artist_grade_id'] = intval($_GET['artist_grade_id']);
        $artist_grade_array = $model_artist_grade->getArtist_gradeInfo($condition);

        Tpl::output('artist_grade_array',$artist_grade_array);
        Tpl::showpage('artist_grade.edit');
    }

    /**
     * 新增艺术家分类
     */
    public function grade_addOp(){
        $model_artist_grade = Model('artist_grade');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["artist_grade_name"], "require"=>"true", "message"=>'艺术家等级不能为空')

            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['artist_grade_name']    = trim($_POST['artist_grade_name']);

                $result = $model_artist_grade->addArtist_grade($insert_array);
                if ($result){
                    showMessage('新增艺术家等级成功','index.php?act=artist_grade&op=index');
                }else {
                    showMessage('新增艺术家等级失败');
                }
            }
        }
        Tpl::showpage('artist_grade.add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 验证会员是否重复
             */
            case 'check_grade_name':
                $model_artist_grade = Model('artist_grade');
                $condition['artist_grade_name']   = $_GET['artist_grade_name'];
                $condition['artist_grade_id'] = array('neq',intval($_GET['artist_grade_id']));
                $list = $model_artist_grade->getArtist_gradeInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_artist_grade = Model('artist_grade');
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('artist_grade_id','artist_grade_name');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $artist_grade_list = $model_artist_grade->getArtist_gradeList($condition, $page,'','*', $order);

        $data = array();
        $data['now_page'] = $model_artist_grade->shownowpage();
        $data['total_num'] = $model_artist_grade->gettotalnum();
        foreach ($artist_grade_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=artist_grade&op=grade_edit&artist_grade_id=" . $value['artist_grade_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['artist_grade_id'] = $value['artist_grade_id'];
            $param['artist_grade_name'] = $value['artist_grade_name'];
            $data['list'][$value['artist_grade_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }




}