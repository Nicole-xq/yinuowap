<?php
/**
 * 艺术家分类
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class artist_classifyControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        Tpl::showpage('artist_classify.list');
    }
    /**
     * 艺术家分类编辑
     */
    public function classify_editOp(){
        $model_artist_classify = Model('artist_classify');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["artist_classify_name"], "require"=>"true",  "message"=>'艺术家分类不能为空'),
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $update_array = array();
                $update_array['artist_classify_name']       = $_POST['artist_classify_name'];

                $result = $model_artist_classify->editArtist_classify($update_array,array('artist_classify_id'=>intval($_POST['artist_classify_id'])));
                if ($result){
                    showMessage('编辑艺术家分类信息成功','index.php?act=artist_classify&op=index');
                }else {
                    showMessage('编辑艺术家分类信息失败');
                }
            }
        }
        $condition['artist_classify_id'] = intval($_GET['artist_classify_id']);
        $artist_classify_array = $model_artist_classify->getArtist_classifyInfo($condition);

        Tpl::output('artist_classify_array',$artist_classify_array);
        Tpl::showpage('artist_classify.edit');
    }

    /**
     * 新增艺术家分类
     */
    public function classify_addOp(){
        $model_artist_classify = Model('artist_classify');
        /**
         * 保存
         */
        if (chksubmit()){
            /**
             * 验证
             */
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["artist_classify_name"], "require"=>"true", "message"=>'艺术家分类不能为空')

            );
            $error = $obj_validate->validate();
            if ($error != ''){
                showMessage($error);
            }else {
                $insert_array = array();
                $insert_array['artist_classify_name']    = trim($_POST['artist_classify_name']);

                $result = $model_artist_classify->addArtist_classify($insert_array);
                if ($result){
                    showMessage('新增艺术家分类信息成功','index.php?act=artist_classify&op=index');
                }else {
                    showMessage('新增艺术家分类信息失败');
                }
            }
        }
        Tpl::showpage('artist_classify.add');
    }

    /**
     * ajax操作
     */
    public function ajaxOp(){
        switch ($_GET['branch']){
            /**
             * 验证会员是否重复
             */
            case 'check_classify_name':
                $model_artist_classify = Model('artist_classify');
                $condition['artist_classify_name']   = $_GET['artist_classify_name'];
                $condition['artist_classify_id'] = array('neq',intval($_GET['artist_classify_id']));
                $list = $model_artist_classify->getArtist_classifyInfo($condition);
                if (empty($list)){
                    echo 'true';exit;
                }else {
                    echo 'false';exit;
                }
                break;
        }
    }

    /**
     * 输出XML数据
     */
    public function get_xmlOp() {
        $model_artist_classify = Model('artist_classify');
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }
        $order = '';
        $param = array('artist_classify_id','artist_artist_name');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $page = $_POST['rp'];
        $artist_classify_list = $model_artist_classify->getArtist_classifyList($condition, $page,'','*', $order);

        $data = array();
        $data['now_page'] = $model_artist_classify->shownowpage();
        $data['total_num'] = $model_artist_classify->gettotalnum();
        foreach ($artist_classify_list as $value) {
            $param = array();
            $param['operation'] = "<a class='btn blue' href='index.php?act=artist_classify&op=classify_edit&artist_classify_id=" . $value['artist_classify_id'] . "'><i class='fa fa-pencil-square-o'></i>编辑</a>";
            $param['artist_classify_id'] = $value['artist_classify_id'];
            $param['artist_classify_name'] = $value['artist_classify_name'];
            $data['list'][$value['artist_classify_id']] = $param;
        }
        echo Tpl::flexigridXML($data);exit();
    }




}