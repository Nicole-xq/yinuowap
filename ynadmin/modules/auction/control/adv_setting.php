<?php
/**
 * 拍卖行广告
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class adv_settingControl extends SystemControl{

    function __construct()
    {
        parent::__construct();
    }

    public function indexOp() {
        $model_setting = Model('setting');
        $list_setting = $model_setting->getListSetting();
        if ($list_setting['auction_advpic'] != ''){
            $list = unserialize($list_setting['auction_advpic']);
        }
        Tpl::output('list', $list);
        $input = array();
        if (chksubmit()){
            //上传图片
            $update_array = array();
            foreach($_FILES as $k=>$v){
                $newk = substr($k,8,strrpos($k,'_'));
                if(!empty($v['name'])) {
                    $upload = new UploadFile();
                    $upload->set('default_dir',ATTACH_AUCTION);
                    $result = $upload->upfile($k);
                    if(!$result) {
                        showMessage($upload->error);
                    }
                    $input[$newk]['pic'] = $upload->file_name;
                }else{
                    if($list[$newk]){
                        $input[$newk]['pic'] = $list[$newk]['pic'];
                    }else{
                        $input[$newk]['pic'] = '';
                    }


                }
                $input[$newk]['url'] = $_POST['adv_url_'.$newk];
            }

            if (count($input) > 0){
                $update_array['auction_advpic'] = serialize($input);
            }else{
                $update_array['auction_advpic'] = '';
            }

            $result = $model_setting->updateSetting($update_array);
            if ($result === true){
                showMessage('设置广告成功');
            }else {
                showMessage('设置广告失败');
            }
        }

        Tpl::showpage('adv_list');
    }




}