<?php
/**
 * 菜单
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

$_menu['auction'] = array(
    'name' => '拍卖行',
    'child' => array(
        array(
            'name' => $lang['nc_config'],
            'child' => array(
                'manage' => '拍卖行设置',
                'adv_setting' => '首页广告图',
                'auction_headline' => '拍卖头条',
                'search' => '搜索设置',
                'right_setting' => '首页右侧设置'
            )
        ),
        array(
            'name' => '艺术家设置',
            'child' => array(
                'artist_classify' => '艺术家分类标签',
//                'artist_grade' => '艺术家等级',
                'artist_edit'=> '艺术家编辑'

            )
        ),
    )
);