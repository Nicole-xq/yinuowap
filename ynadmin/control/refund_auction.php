<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/30 0030
 * Time: 16:28
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class refund_auctionControl extends SystemControl{
    public function __construct(){
        parent::__construct();
    }

    public function indexOp() {
        $this->get_detailOp();
    }

    /**
     * 微信退款
     *
     */
    public function wxpayOp() {
        $result = array('state'=>'false','msg'=>'参数错误，微信退款失败');
        $refund_id = intval($_GET['refund_id']);
        $model_auction_order = Model('auction_order');
        $condition = array();
        $condition['auction_order_id'] = $refund_id;
        $condition['refund_state'] = '3';
        $detail_array = $model_auction_order->getInfo($condition);//退款详细
        if(!empty($detail_array) && in_array($detail_array['payment_code'],array('wxpay','wx_jsapi','wx_saoma'))) {
            $order = $detail_array;//退款订单详细
            $order['payment_config'] = $this->getPaymentInfo($detail_array['payment_code']);
            $refund_amount = $order['api_pay_amount'];//本次在线退款总金额
            if ($refund_amount > 0) {
                $wxpay = $order['payment_config'];
                define('WXPAY_APPID', $wxpay['appid']);
                define('WXPAY_MCHID', $wxpay['mchid']);
                define('WXPAY_KEY', $wxpay['key']);
                $total_fee = $order['api_pay_amount']*100;//微信订单实际支付总金额(在线支付金额,单位为分)
                $refund_fee = $refund_amount*100;//本次微信退款总金额(单位为分)
                $api_file = BASE_PATH.DS.'api'.DS.'refund'.DS.'wxpay'.DS.'WxPay.Api.php';
                include $api_file;
                $input = new WxPayRefund();
                $input->SetTransaction_id($order['trade_no']);//微信订单号
                $input->SetTotal_fee($total_fee);
                $input->SetRefund_fee($refund_fee);
                $input->SetOut_refund_no($detail_array['batch_no']);//退款批次号
                $input->SetOp_user_id(WxPayConfig::MCHID);
                $data = WxPayApi::refund($input);
                if(!empty($data) && $data['return_code'] == 'SUCCESS') {//请求结果
                    if($data['result_code'] == 'SUCCESS') {//业务结果
                        $detail_array = array();
                        $detail_array['api_refund_amount'] = ncPriceFormat($data['refund_fee']/100);
                        $detail_array['api_refund_time'] = time();
                        $model_auction_order->setOrder(array('auction_order_id'=> $order['auction_order_id']), $detail_array);
                        $result['state'] = 'true';
                        $result['msg'] = '微信成功退款:'.$detail_array['api_refund_amount'];

                        $consume_array = array();
                        $consume_array['member_id'] = $order['buyer_id'];
                        $consume_array['member_name'] = $order['buyer_name'];
                        $consume_array['consume_amount'] = $detail_array['api_refund_amount'];
                        $consume_array['consume_time'] = time();
                        $consume_array['consume_remark'] = '微信在线退款成功（到账有延迟），退款退货单号：'.$order['auction_order_sn'];
                        QueueClient::push('addConsume', $consume_array);
                    } else {
                        $result['msg'] = '微信退款错误,'.$data['err_code_des'];//错误描述
                    }
                } else {
                    $result['msg'] = '微信接口错误,'.$data['return_msg'];//返回信息
                }
            }
        }
        exit(json_encode($result));
    }

    /**
     * 支付宝退款
     *
     */
    public function alipayOp() {
        $refund_id = intval($_GET['refund_id']);
        $model_auction_order = Model('auction_order');
        $condition = array();
        $condition['auction_order_id'] = $refund_id;
        $condition['refund_state'] = '3';
        $detail_array = $model_auction_order->getInfo($condition);//退款详细
        if(!empty($detail_array) && $detail_array['payment_code'] == 'alipay') {
            $order = $detail_array;//退款订单详细
            $order['payment_config'] = $this->getPaymentInfo($detail_array['payment_code']);
            $refund_amount = $order['api_pay_amount'];//本次在线退款总金额
            if ($refund_amount > 0) {
                $payment_config = $order['payment_config'];
                $alipay_config = array();
                $alipay_config['seller_email'] = $payment_config['alipay_account'];
                $alipay_config['partner'] = $payment_config['alipay_partner'];
                $alipay_config['key'] = $payment_config['alipay_key'];
                $api_file = BASE_PATH.DS.'api'.DS.'refund'.DS.'alipay'.DS.'alipay.class.php';
                include $api_file;
                $alipaySubmit = new AlipaySubmit($alipay_config);
                $parameter = getPara($alipay_config);
                $batch_no = $detail_array['batch_no'];
                $b_date = substr($batch_no,0,8);
                if($b_date != date('Ymd')) {
                    $batch_no = date('Ymd').substr($batch_no, 8);//批次号。支付宝要求格式为：当天退款日期+流水号。
                    $model_auction_order->setOrder(array('auction_order_id'=> $order['auction_order_id']), array('batch_no'=> $batch_no));
                }

                $parameter['notify_url'] = ADMIN_SITE_URL."/api/refund/alipay/at_notify_url.php";
                $parameter['batch_no'] = $batch_no;
                $parameter['detail_data'] = $order['trade_no'].'^'.$refund_amount.'^协商退款';//数据格式为：原交易号^退款金额^理由
                $pay_url = $alipaySubmit->buildRequestParaToString($parameter);
                @header("Location: ".$pay_url);
            }
        }
    }

    /**
     * 在线退款查询
     *
     */
    public function get_detailOp() {
        $result = array('state'=>'false','msg'=>'退款正在处理中或已失败，稍后查询');
        $refund_id = intval($_GET['refund_id']);
        $model_auction_order = Model('auction_order');
        $condition = array();
        $condition['auction_order_id'] = $refund_id;
        $detail_array = $model_auction_order->getInfo($condition);//退款详细
        if($detail_array['api_refund_time'] > 0) {
            $result = array('state'=>'true','msg'=>'成功退款:'.ncPriceFormat($detail_array['api_refund_amount']));
        }
        exit(json_encode($result));
    }

    /*
     * 查询支付方式配置信息
     * */
    protected function getPaymentInfo($payment_code)
    {
        $condition['payment_code'] = $payment_code;
        if(in_array($payment_code,array('wxpay','wx_jsapi','ali_native'))) {//手机客户端支付
            if($payment_code == 'wx_jsapi') {
                $condition['payment_code'] = 'wxpay_jsapi';
            }
            if($payment_code == 'ali_native') {
                $condition['payment_code'] = 'alipay_native';
            }
            $model_payment = Model('mb_payment');
            $payment_info = $model_payment->getMbPaymentInfo($condition);//接口参数
            $payment_info = $payment_info['payment_config'];
            $payment_config = $payment_info;
            if($payment_code == 'wxpay') {
                $payment_config['appid'] = $payment_info['wxpay_appid'];
                $payment_config['mchid'] = $payment_info['wxpay_partnerid'];
                $payment_config['key'] = $payment_info['wxpay_partnerkey'];
            }
            if($payment_code == 'wx_jsapi') {
                $payment_config['appid'] = $payment_info['appId'];
                $payment_config['mchid'] = $payment_info['partnerId'];
                $payment_config['key'] = $payment_info['apiKey'];
            }
        } else {
            if($payment_code == 'wx_saoma') {
                $condition['payment_code'] = 'wxpay';
            }
            $model_payment = Model('payment');
            $payment_info = $model_payment->getPaymentInfo($condition);//接口参数
            $payment_config = unserialize($payment_info['payment_config']);
        }
        return $payment_config;
    }
}
