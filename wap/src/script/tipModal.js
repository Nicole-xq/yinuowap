/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

// 组件默认配置
// var _defaultConfig = {
//
// };
// function tipModal(options) {
//     this.config     = $.extend({}, _defaultConfig, options || {});
//     this._init();
// }
//
// var prototype = {
//     _init: function() {
//
//     },
// }


// export default tipModal
// module.exports = {
//     tipModal: tipModal
// };
/**
 * html:内容
 * showCloseBtn／showOkBtn 是否显示取消／确认按钮  true: default:确认 || 文字
 * onCancelBtn / onOkBtn 取消/确认触发事件
 * onLoad / onCancelBtn 模态出现／关闭事件触发
 * timeout: 触发时间 默认3000ms number类型
 * autoClose：true/false  是否开启自动关闭 默认true
 * modalName：modal名称 默认为'tipModal_时间戳'的class方式
 * size:模态框尺寸大小：'small'\'middle'\'large'
 * animate：true/false default:bounceIn/bounceOut || ['bounceIn','bounceOut']  //来自animate.css
 * showTitle true/false default:false  default:标题 || 内容
 * showClose true/false default：true
 * onDocumentClickClose true/false default:true 点击背景是否可以关闭
 *
 */
(function ($) {
    var closeBase = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDI0MUJBMDdCNjFEMTFFNzhCMUY4NUU1NTkwQkI1NEQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDI0MUJBMDhCNjFEMTFFNzhCMUY4NUU1NTkwQkI1NEQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MjQxQkEwNUI2MUQxMUU3OEIxRjg1RTU1OTBCQjU0RCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MjQxQkEwNkI2MUQxMUU3OEIxRjg1RTU1OTBCQjU0RCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt+VBHYAAAHpSURBVHjarJRLL0NREMfbhoSIIJ4t8Yq3CFqKSKW18Rkk1nY2voBPI7bWFnSBRFtFQsU7sRGPxCvEI9RvZG5yc13trWjy69zOmfl3zp05x51Op13//cmTr2g0OoWZhrlwOLzyFyE0ajDzsO1R3ySEYJnFyB8EfZgkjMOsIToDD/oswj05CJaqoFddQ9+ibPkUE4A7XYgR3O1AsAwTh2p1TaAVMyoV4WMVvoACSJDkzyBYi9mEFpPgkjy4rd0nuAKTgkp1+QnesnmHElOirj5idox1j7UCFm8w/XCprjgiA5Yux02CIbOgbaWmZK9uT6wE1esfnYMIf8IYgmvWXHem4Ue4DiNNzIdreIRmXQ4gmLTLc2c7UQhXYVahVV1XEEEw9VuOx8Eovlp+p00z7cq5Uu2yjFoh3MOLzqS8z06qPcypUm1UXAVdOhFtWqXkJYnpdCxKcJNcDODTqkap6gxE0K8nr0imw+7k2Q1/gwrKmX6HYWuXiWnVcSuGJx3+Y9tKCW43Ccr7G7QbG3xHmF641Yp3yQ38qBRnl1wkGvQMQZL3soxbo9wRUA4fuqvEt6guprQp8i/dLO47vPrkMBzohS+vq8PYftLU5ZBTQdO1GYQ3PXknhugibMCI3Vl2ICy32CCsw8KXAAMArgnOjDm5TZQAAAAASUVORK5CYII=';
    $.fn.tipModal = function (method) {
        var elem = $(this),
            elemClass = 'tipModal',
            effectIn = 'fadeIn',
            effectOut = 'fadeOut',
            animTime,
            _options,
            elemObj,
            cancelBtn,
            okBtn;
        var methods = {
            init: function (params) {
                var _defaults = {
                    simple: true,
                    html: '内容',
                    showCloseBtn: false,
                    showOkBtn: true,
                    onCancelBtn: '',
                    onOkBtn:'',
                    onLoad: function () {
                    },
                    onClose: function () {
                    },
                    autoClose: true,
                    timeout: 3000,
                    modalName: '',
                    size: 'small',
                    onDocumentClickClose: true,
                    showTitle: false,
                    showClose: true,
                    css: {
                        width: '70%',
                    },
                    noBackgroundScroll:true
                };
                // animated
                _options = $.extend(_defaults, params);

                $('.' + elemClass).remove();


                var title = _options.showTitle && typeof _options.showTitle == 'boolean' ? '标题' : _options.showTitle,
                    imgSrc = _options.showClose ? closeBase : '';

                //CANNCAL BTN
                cancelBtn = '<div  class="tipModal_cancel tipModal_btn">' + returnChange(_options.showCloseBtn && typeof _options.showCloseBtn === 'string', _options.showCloseBtn, '取消') + '</div>'
                //OK BTN
                okBtn = '<div  class="tipModal_ok tipModal_btn">' + returnChange(_options.showOkBtn && typeof _options.showOkBtn === 'string', _options.showOkBtn, '确认') + '</div>'

                var tipModalContainer = $('<div class="tipModal">\
                    <div class="tipModal_mask"></div>\
                    <div class="tipModal_wrapper animated '+effectIn+'" style="width:' + _options.css.width + ';">\
                    <div class="tipModal_header ' + returnClass(_options.showTitle,'showtitle') + '">\
                    <h3 class="tipModal_title ' + returnClass(!_options.showTitle) + '">' + title + '</h3>\
                    <div class="tipModal_close ' + returnClass(!_options.showClose) + '" >\
                    <img src="' + imgSrc + '" alt="">\
                    </div>\
                    </div>\
                    <div class="tipModal_content ' + returnClass(_options.simple, 'simple') + '">\
                    ' + _options.html + '\
                    </div>\
                    <div class="tipModal_tools ' + returnClass(_options.showOkBtn && _options.showCloseBtn, 'allBtn') + ' ">\
                    ' + okBtn + '\
                    ' + cancelBtn + '\
                    </div>\
                    </div>\
                    </div>');

                $('body').append(tipModalContainer);

                elemObj = _options.modalName && $(_options.modalName) || $('.' + elemClass);

                // 模态加载
                if (_options.onLoad && $.isFunction(_options.onLoad)) {
                    _options.onLoad();
                }

                // 窗口关闭
                elemObj.on('destroyed', function () {
                    if (_options.onClose && $.isFunction(_options.onClose)) {
                        _options.onClose();
                    }
                });

                // 判断点击背景关闭模态
                if (_options.onDocumentClickClose) {
                    elemObj.find('.tipModal_mask').click(function(){
                        tipModalClose();
                    })
                }

                // 点击关闭按钮
                elemObj.find('.tipModal_close').on('click', function (event) {
                    tipModalClose();
                });

                // 点击确认按钮触发事件
                elemObj.find('.tipModal_ok').on('click', function (event) {
                    var ok;
                    if (_options.onOkBtn && $.isFunction(_options.onOkBtn)) {
                        ok = _options.onOkBtn(event,methods);
                    } else {
                        tipModalClose();
                    }
                });

                // 点击取消按钮触发事件
                elemObj.find('.tipModal_cancel').on('click', function (event) {
                    var ok;
                    if (_options.onCancelBtn && $.isFunction(_options.onCancelBtn)) {
                        ok = _options.onCancelBtn(event,methods);
                    } else {
                        tipModalClose();
                    }
                });

                // 当pc端esc后
                $('html').on('keydown.' + elemClass + 'Event', function (event) {
                    if (event.keyCode == 27) {
                        tipModalClose();
                    }
                });
            },
            hide: function () {
                tipModalClose();
            }
        };
        elemObj = $('.' + elemClass);
        $(document).on('click','.tipModal_close',function () {
            tipModalClose();
        });
        // 关闭模态
        function tipModalClose() {
            elemObj = $('.' + elemClass);
            reverseEffect();
            getAnimTime();
            setTimeout(function () {
                elemObj.remove();
                $('html.' + elemClass + 'Open').off('.' + elemClass + 'Event').removeClass(elemClass + 'Open');
            }, animTime);
        }

        // 获取动画时间
        function getAnimTime() {
            if (!animTime) {
                animTime = elemObj.find('.tipModal_wrapper').css('animation-duration');
                if (animTime != undefined) {
                    animTime = animTime.replace('s', '') * 1000;
                } else {
                    animTime = 0;
                }
            }
        }

        // 替换为动画结束效果
        function reverseEffect() {
            elemObj = $('.' + elemClass);
            var animClassOld = elemObj.find('.tipModal_wrapper').attr('class'),
                animClassNew = animClassOld.replace(effectIn, effectOut);
            elemObj.removeClass(animClassOld).addClass(animClassNew);
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }

        function returnClass(str, name) {
            var cs = name || 'lm_hidden';
            return str ? cs : '';
        }

        function returnChange(str, obj1, obj2) {
            return str ? obj1 : obj2;
        }

        $.event.special.destroyed = {
            remove: function (o) {
                if (o.handler) {
                    o.handler();
                }
            }
        }
    };
})(Zepto);
