$(function () {
    var id = getQueryString('id');
    if (!id) {
        location.href = '/wap/index.html';
    }
    $.getJSON(ApiUrl + '/index.php?act=landing', {id: id}, function (result) {
        if (result.code === 400) {
            location.href = '/wap/index.html';
        }

        var data = result.datas;
        var header_html = template.render('landing_header_content', data);
        var list_html = template.render('goods_list_content', data);

        $("#landing_header").html(header_html);
        $("#goods_list").html(list_html);

        var img = '<img src="' + data.landing_info.footer_image + '" alt="">';
        $("#landing_footer").html(img);
    });
});
