/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wap/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
module.exports = function(src) {
	if (typeof execScript !== "undefined")
		execScript(src);
	else
		eval.call(null, src);
}


/***/ }),
/* 1 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _NativeShare = __webpack_require__(3);

var _NativeShare2 = _interopRequireDefault(_NativeShare);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(5);
var $ = __webpack_require__(6);
__webpack_require__(9);
__webpack_require__(11);
__webpack_require__(13);

__webpack_require__(15);


__webpack_require__(17);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function (e, t) {
    "object" == ( false ? "undefined" : _typeof(exports)) && "object" == ( false ? "undefined" : _typeof(module)) ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports["NativeShare.js"] = t() : e["NativeShare.js"] = t();
}(undefined, function () {
    return function (e) {
        function t(r) {
            if (n[r]) return n[r].exports;
            var o = n[r] = { i: r, l: !1, exports: {} };
            return e[r].call(o.exports, o, o.exports, t), o.l = !0, o.exports;
        }

        var n = {};
        return t.m = e, t.c = n, t.d = function (e, n, r) {
            t.o(e, n) || Object.defineProperty(e, n, { configurable: !1, enumerable: !0, get: r });
        }, t.n = function (e) {
            var n = e && e.__esModule ? function () {
                return e.default;
            } : function () {
                return e;
            };
            return t.d(n, "a", n), n;
        }, t.o = function (e, t) {
            return Object.prototype.hasOwnProperty.call(e, t);
        }, t.p = "", t(t.s = 0);
    }([function (e, t, n) {
        "use strict";

        function r() {}

        function o(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : r,
                n = document.getElementsByTagName("script")[0],
                o = document.createElement("script");
            o.src = e, o.async = !0, n.parentNode.insertBefore(o, n), o.onload = t;
        }

        function i(e, t) {
            if (null == e) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            for (var n = Object(e), r = 1; r < arguments.length; r++) {
                var o = arguments[r];
                if (null != o) for (var i in o) {
                    Object.prototype.hasOwnProperty.call(o, i) && (n[i] = o[i]);
                }
            }
            return n;
        }

        function a(e) {
            if (se) location.href = e;else {
                var t = document.createElement("iframe");
                t.style.display = "none", t.src = e, document.body.appendChild(t), setTimeout(function () {
                    t && t.parentNode && t.parentNode.removeChild(t);
                }, 2e3);
            }
        }

        function c(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                n = [];
            for (var r in e) {
                t ? n.push(r + "=" + encodeURIComponent(e[r])) : n.push(r + "=" + e[r]);
            }return n.join("&");
        }

        function u(e) {
            var t = document.createElement("a");
            return t.href = e, t.hostname;
        }

        function l(e) {
            Oe ? Oe.content = e : document.head.insertAdjacentHTML("beforeend", '<meta name="description" content="' + e + '">');
        }

        function f(e) {
            je ? je.href = e : document.head.insertAdjacentHTML("beforeend", '<link rel="shortcut icon" href="' + e + '">');
        }

        function s(e) {
            document.title = e;
        }

        function p(e) {
            return c({
                share_id: 924053302,
                url: _e.encode(e.link),
                title: _e.encode(e.title),
                description: _e.encode(e.desc),
                previewimageUrl: _e.encode(e.icon),
                image_url: _e.encode(e.icon)
            });
        }

        function h() {
            a((se ? "mqqapi://share/to_fri?src_type=web&version=1&file_type=news" : "mqqapi://share/to_fri?src_type=isqqBrowser&version=1&file_type=news") + "&" + p(Se));
        }

        function b() {
            a((se ? "mqqapi://share/to_fri?file_type=news&src_type=web&version=1&generalpastboard=1&shareType=1&cflag=1&objectlocation=pasteboard&callback_type=scheme&callback_name=QQ41AF4B2A" : "mqqapi://share/to_qzone?src_type=isqqBrowser&version=1&file_type=news&req_type=1") + "&" + p(Se));
        }

        function y() {
            var e = { url: Se.link, title: Se.title, pic: Se.icon, desc: Se.desc };
            location.href = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?" + c(e, !0);
        }

        function d() {
            var e = { url: Se.link, title: Se.title, pic: Se.icon };
            location.href = "http://service.weibo.com/share/share.php?" + c(e, !0);
        }

        function w(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function v(e, t, n) {
            return t in e ? Object.defineProperty(e, t, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = n, e;
        }

        function g(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function m(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function _(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function O(e, t, n) {
            return t in e ? Object.defineProperty(e, t, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = n, e;
        }

        function j(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function S(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function k(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function P(e, t, n) {
            return t in e ? Object.defineProperty(e, t, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = n, e;
        }

        function C(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function q(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function T(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function E(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function D(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function x(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function Q(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function M(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function N(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function A(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function U(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function B(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function R(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function W(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function I(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function z(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function F(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function L(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function Z(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function J(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function H(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function X(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function G(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function K(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function V(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function Y(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function $(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function ee(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function te(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function ne(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        function re(e, t) {
            if (!(e instanceof t)) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
        }

        function oe(e, t) {
            if (!e) throw new ReferenceError("您的浏览器暂不支持，请使用浏览器分享!");
            return !t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) && "function" != typeof t ? e : t;
        }

        function ie(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("您的浏览器暂不支持，请使用浏览器分享!");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
        }

        Object.defineProperty(t, "__esModule", { value: !0 });
        var ae,
            ce = navigator.userAgent,
            ue = /(iPad).*OS\s([\d_]+)/.test(ce),
            le = /(iPod)(.*OS\s([\d_]+))?/.test(ce),
            fe = !ue && /(iPhone\sOS)\s([\d_]+)/.test(ce),
            se = ue || le || fe,
            pe = /(Android);?[\s\/]+([\d.]+)?/.test(ce),
            he = /micromessenger/i.test(ce),
            be = /QQ\/([\d\.]+)/.test(ce),
            ye = /Qzone\//.test(ce),
            de = /MQQBrowser/i.test(ce) && !he && !be,
            we = /UCBrowser/i.test(ce),
            ve = /mobile.*baidubrowser/i.test(ce),
            ge = /SogouMobileBrowser/i.test(ce),
            me = /baiduboxapp/i.test(ce),
            _e = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function encode(e) {
                var t,
                    n,
                    r,
                    o,
                    i,
                    a,
                    c,
                    u = "",
                    l = 0;
                for (e = _e._utf8_encode(e); l < e.length;) {
                    t = e.charCodeAt(l++), n = e.charCodeAt(l++), r = e.charCodeAt(l++), o = t >> 2, i = (3 & t) << 4 | n >> 4, a = (15 & n) << 2 | r >> 6, c = 63 & r, isNaN(n) ? a = c = 64 : isNaN(r) && (c = 64), u = u + this._keyStr.charAt(o) + this._keyStr.charAt(i) + this._keyStr.charAt(a) + this._keyStr.charAt(c);
                }return u;
            }, _utf8_encode: function _utf8_encode(e) {
                e = e.replace(/\r\n/g, "\n");
                for (var t = "", n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    r < 128 ? t += String.fromCharCode(r) : r > 127 && r < 2048 ? (t += String.fromCharCode(r >> 6 | 192), t += String.fromCharCode(63 & r | 128)) : (t += String.fromCharCode(r >> 12 | 224), t += String.fromCharCode(r >> 6 & 63 | 128), t += String.fromCharCode(63 & r | 128));
                }
                return t;
            }
        },
            Oe = document.querySelector("meta[name=description]"),
            je = document.querySelector("link[rel*=icon]"),
            Se = {
            link: location.href, title: function () {
                return document.title;
            }(), desc: function () {
                return Object(Oe).content || "";
            }(), icon: function () {
                return Object(je).href || location.protocol + "//" + location.hostname + "/favicon.ico";
            }(), from: "", success: r, fail: r, trigger: r
        },
            ke = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Pe = function () {
            function e(t) {
                w(this, e), this._shareData = Se, this._config = {
                    syncDescToTag: !1,
                    syncIconToTag: !1,
                    syncTitleToTag: !1
                }, this.setConfig(t);
            }

            return ke(e, [{
                key: "getShareData", value: function value() {
                    return i({}, this._shareData);
                }
            }, {
                key: "setShareData", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    i(this._shareData, e), this._config.syncDescToTag && l(this._shareData.desc), this._config.syncIconToTag && f(this._shareData.icon), this._config.syncTitleToTag && s(this._shareData.title);
                }
            }, {
                key: "setConfig", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    i(this._config, e);
                }
            }, {
                key: "getConfig", value: function value() {
                    return i({}, this._config);
                }
            }]), e;
        }(),
            Ce = Pe,
            qe = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Te = function (e) {
            function t(e) {
                g(this, t);
                var n = m(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return o("https://jsapi.qq.com/get?api=app.share"), n;
            }

            return _(t, e), qe(t, [{
                key: "call", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "default",
                        t = arguments[1];
                    this.setShareData(t);
                    var n = this.getShareData(),
                        r = this.constructor.commamdMap[String(e).toLowerCase()];
                    browser.app.share({
                        title: n.title,
                        description: n.desc,
                        url: n.link,
                        img_url: n.icon,
                        from: n.from,
                        to_app: r
                    });
                }
            }]), t;
        }(Ce);
        Te.commamdMap = (ae = {}, v(ae, "wechattimeline", 8), v(ae, "wechatfriend", 1), v(ae, "qqfriend", 4), v(ae, "qzone", 3), v(ae, "weibo", 11), v(ae, "copyurl", 10), v(ae, "more", 5), v(ae, "generateqrcode", 7), v(ae, "default", void 0), ae);
        var Ee,
            De = Te,
            xe = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Qe = function (e) {
            function t(e) {
                return j(this, t), S(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return k(t, e), xe(t, [{
                key: "call", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "default",
                        t = arguments[1];
                    this.setShareData(t);
                    var n = this.getShareData(),
                        r = this.constructor.commamdMap[String(e).toLowerCase()];
                    ucbrowser.web_shareEX ? ucbrowser.web_shareEX(JSON.stringify({
                        title: n.title,
                        content: n.desc,
                        sourceUrl: n.link,
                        imageUrl: n.icon,
                        source: n.from,
                        target: r
                    })) : ucbrowser.web_share(title, desc, link, r, "", from, "");
                }
            }]), t;
        }(Ce);
        Qe.commamdMap = (Ee = {}, O(Ee, "wechattimeline", "kWeixinFriend"), O(Ee, "wechatfriend", "kWeixin"), O(Ee, "qqfriend", "kQQ"), O(Ee, "qzone", "kQZone"), O(Ee, "weibo", "kSinaWeibo"), O(Ee, "default", void 0), Ee);
        var Me,
            Ne = Qe,
            Ae = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Ue = function (e) {
            function t(e) {
                return C(this, t), q(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return T(t, e), Ae(t, [{
                key: "call", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "default",
                        t = arguments[1];
                    this.setShareData(t);
                    var n = this.getShareData(),
                        r = this.constructor.commamdMap[String(e).toLowerCase()];
                    ucweb.startRequest("shell.page_share", [n.title, n.desc, n.link, r, "", n.from, n.icon]);
                }
            }]), t;
        }(Ce);
        Ue.commamdMap = (Me = {}, P(Me, "wechattimeline", "WechatTimeline"), P(Me, "wechatfriend", "WechatFriends"), P(Me, "qqfriend", "QQ"), P(Me, "qzone", "Qzone"), P(Me, "weibo", "SinaWeibo"), P(Me, "default", ""), Me);
        var Be = Ue,
            Re = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            We = function (e) {
            function t(e) {
                return E(this, t), D(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return x(t, e), Re(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                    var n = this.getShareData();
                    _flyflowNative.exec("bd_utils", "shareWebPage", JSON.stringify({
                        title: n.title,
                        content: n.desc,
                        landurl: n.link,
                        imageurl: n.icon,
                        shareSource: n.from
                    }), "");
                }
            }]), t;
        }(Ce),
            Ie = We,
            ze = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Fe = function (e) {
            function t(e) {
                return Q(this, t), M(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return N(t, e), ze(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                    var n = this.getShareData();
                    location.href = "baidubrowserapp://bd_utils?action=shareWebPage&params=" + encodeURIComponent(JSON.stringify({
                        title: n.title,
                        content: n.desc,
                        imageurl: n.icon,
                        landurl: n.link,
                        mediaType: 0,
                        share_type: "webpage"
                    }));
                }
            }]), t;
        }(Ce),
            Le = Fe,
            Ze = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Je = function (e) {
            function t(e) {
                return A(this, t), U(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return B(t, e), Ze(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                    var n = this.getShareData();
                    SogouMse.Utility.shareWithInfo({
                        shareTitle: n.title,
                        shareContent: n.desc,
                        shareImageUrl: n.icon,
                        shareUrl: n.link
                    });
                }
            }]), t;
        }(Ce),
            He = Je,
            Xe = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            Ge = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r);
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r);
        },
            Ke = function (e) {
            function t(e) {
                R(this, t);
                var n = W(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.setConfig(e), n;
            }

            return I(t, e), Xe(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                }
            }, {
                key: "setConfig", value: function value(e) {
                    Ge(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "setConfig", this).call(this, e), this.init(this.getConfig().wechatConfig);
                }
            }, {
                key: "init", value: function value(e) {
                    var t = this;
                    e && o("https://res.wx.qq.com/open/js/jweixin-1.2.0.js", function () {
                        wx.config(i({
                            debug: !1,
                            jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"]
                        }, e));
                        var n = t._shareData,
                            r = {};
                        Object.defineProperty(r, "trigger", {
                            get: function get() {
                                return function () {
                                    i(r, {
                                        title: n.title,
                                        desc: n.desc,
                                        link: n.link,
                                        imgUrl: n.icon,
                                        type: n.type,
                                        dataUrl: n.dataUrl,
                                        success: n.success,
                                        fail: n.fail,
                                        cancel: n.fail
                                    }), n.trigger.apply(n, arguments);
                                };
                            }, set: function set(e) {
                                n.trigger = e;
                            }, enumerable: !0
                        }), wx.ready(function () {
                            wx.onMenuShareAppMessage(r), wx.onMenuShareQQ(r), wx.onMenuShareQZone(r), wx.onMenuShareWeibo(r), wx.onMenuShareTimeline(r);
                        });
                    });
                }
            }]), t;
        }(Ce),
            Ve = Ke,
            Ye = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            $e = function (e) {
            function t(e) {
                return z(this, t), F(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }
            return L(t, e), Ye(t, [{
                key: "call", value: function value() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "default",
                        t = arguments[1];
                    if (this.setShareData(t), "weibo" !== (e = String(e).toLowerCase())) throw "qqfriend" === e ? h() : "qzone" === e && b(), new Error("您的浏览器暂不支持，请使用浏览器分享!");

                    d();
                }
            }]), t;
        }(Ce),
            et = $e,
            tt = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            nt = function (e) {
            function t(e) {
                Z(this, t);
                var n = J(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.init(), n;
            }

            return H(t, e), tt(t, [{
                key: "call", value: function value() {
                    var e = (arguments.length > 0 && void 0 !== arguments[0] && arguments[0], arguments[1]);
                    this.setShareData(e), mqq.ui.showShareMenu();
                }
            }, {
                key: "init", value: function value() {
                    var e = this;
                    o("https://open.mobile.qq.com/sdk/qqapi.js", function () {
                        var t = e._shareData;
                        mqq.ui.setOnShareHandler(function (e) {
                            mqq.ui.shareMessage({
                                back: !0,
                                share_type: e,
                                title: t.title,
                                desc: t.desc,
                                share_url: t.link,
                                image_url: t.icon,
                                sourceName: t.from
                            }, function (e) {
                                0 === e.retCode ? t.success(e) : t.fail(e);
                            });
                        });
                    });
                }
            }]), t;
        }(Ce),
            rt = nt,
            ot = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            it = function e(t, n, r) {
            null === t && (t = Function.prototype);
            var o = Object.getOwnPropertyDescriptor(t, n);
            if (void 0 === o) {
                var i = Object.getPrototypeOf(t);
                return null === i ? void 0 : e(i, n, r);
            }
            if ("value" in o) return o.value;
            var a = o.get;
            if (void 0 !== a) return a.call(r);
        },
            at = function (e) {
            function t(e) {
                X(this, t);
                var n = G(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.init(), n;
            }

            return K(t, e), ot(t, [{
                key: "setShareData", value: function value(e) {
                    it(t.prototype.__proto__ || Object.getPrototypeOf(t.prototype), "setShareData", this).call(this, e);
                    var n = this.getShareData();
                    u(n.link) !== location.hostname && (n.link = location.href, console.warn("安卓的QQ自带浏览器分享url必须跟页面url同一个域名，已自动为你设置为当前页面的url"));
                    try {
                        mqq.data.setShareInfo({
                            share_url: n.link,
                            title: n.title,
                            desc: n.desc,
                            image_url: n.icon
                        }, function (e) {
                            !0 !== e && console.warn(e);
                        });
                    } catch (e) {}
                }
            }, {
                key: "call", value: function value() {
                    var e = (arguments.length > 0 && void 0 !== arguments[0] && arguments[0], arguments[1]);
                    this.setShareData(e), mqq.ui.showShareMenu();
                }
            }, {
                key: "init", value: function value() {
                    var e = this;
                    o("https://open.mobile.qq.com/sdk/qqapi.js", function () {
                        e.setShareData();
                    });
                }
            }]), t;
        }(Ce),
            ct = at,
            ut = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            lt = function (e) {
            function t(e) {
                V(this, t);
                var n = Y(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
                return n.init(), n;
            }

            return $(t, e), ut(t, [{
                key: "call", value: function value() {
                    var e = this,
                        t = (arguments.length > 0 && void 0 !== arguments[0] && arguments[0], arguments[1]);
                    this.setShareData(t);
                    for (var n = this.getShareData(), r = [], o = [], i = [], a = [], c = 0; c < 5; c++) {
                        r.push(n.icon), a.push(n.link), o.push(n.title), i.push(n.desc);
                    }QZAppExternal.setShare(function (t) {
                        0 != t.code && (e.hasSomethingWrong = !0);
                    }, { type: "share", image: r, title: o, summary: i, shareURL: a });
                }
            }, {
                key: "setShareData", value: function value(e) {
                    try {
                        this.call("default", e);
                    } catch (e) {}
                }
            }, {
                key: "init", value: function value() {
                    var e = this;
                    o("https://qzonestyle.gtimg.cn/qzone/phone/m/v4/widget/mobile/jsbridge.js", function () {
                        e.call("default");
                    });
                }
            }]), t;
        }(Ce),
            ft = lt,
            st = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            pt = function (e) {
            function t(e) {
                return ee(this, t), te(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return ne(t, e), st(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                    var n = this.getShareData();
                    window.NativeShareFailCallback = n.fail, window.NativeShareSuccessCallback = n.success, location.href = "baiduboxapp://callShare?" + ["options=" + encodeURIComponent(JSON.stringify({
                        title: n.title,
                        imageUrl: "",
                        mediaType: "all",
                        content: n.desc,
                        linkUrl: n.link,
                        iconUrl: n.icon
                    })), "errorcallback=window.NativeShareFailCallback", "successcallback=window.NativeShareSuccessCallback"].join("&");
                }
            }]), t;
        }(Ce),
            ht = pt,
            bt = function () {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
                }
            }

            return function (t, n, r) {
                return n && e(t.prototype, n), r && e(t, r), t;
            };
        }(),
            yt = function (e) {
            function t(e) {
                return re(this, t), oe(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this, e));
            }

            return ie(t, e), bt(t, [{
                key: "call", value: function value(e, t) {
                    this.setShareData(t);
                    var n = this.getShareData();
                    window.NativeShareFailCallback = n.fail, window.NativeShareSuccessCallback = n.success, prompt("BdboxApp:" + JSON.stringify({
                        obj: "Bdbox_android_utils",
                        func: "callShare",
                        args: ['{\n                            imageUrl: "",\n                            mediaType: "all",\n                            title: "' + n.title + '",\n                            content: "' + n.desc + '",\n                            linkUrl: "' + n.link + '",\n                            iconUrl: "' + n.icon + '"\n                        }', "window.NativeShareSuccessCallback", "window.NativeShareFailCallback"]
                    }));
                }
            }]), t;
        }(Ce),
            dt = yt;
        n.d(t, "Share", function () {
            return Ce;
        }), n.d(t, "QQMobileBrowser", function () {
            return De;
        }), n.d(t, "UCIosBrowser", function () {
            return Ne;
        }), n.d(t, "UCAndroidBrowser", function () {
            return Be;
        }), n.d(t, "BaiduAndroidBrowser", function () {
            return Ie;
        }), n.d(t, "BaiduIosBrowser", function () {
            return Le;
        }), n.d(t, "SogouIosBrowser", function () {
            return He;
        }), n.d(t, "BaiduIos", function () {
            return ht;
        }), n.d(t, "BaiduAndroid", function () {
            return dt;
        }), n.d(t, "Wechat", function () {
            return Ve;
        }), n.d(t, "Others", function () {
            return et;
        }), n.d(t, "QQIos", function () {
            return rt;
        }), n.d(t, "QQAndroid", function () {
            return ct;
        }), n.d(t, "QZone", function () {
            return ft;
        }), n.d(t, "shareToQQ", function () {
            return h;
        }), n.d(t, "shareToQZone", function () {
            return b;
        }), n.d(t, "shareToWeibo4Web", function () {
            return d;
        }), n.d(t, "shareToQZone4Web", function () {
            return y;
        });
        var wt = void 0;
        wt = he ? Ve : be && se ? rt : be && pe ? ct : ye ? ft : de ? De : we && se ? Ne : we && pe ? Be : ve && pe ? Ie : ve && se ? Le : ge && se ? He : me && se ? ht : me && pe ? dt : et, window.NativeShare = wt, t.default = wt;
    }]);
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(4)(module)))

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

module.exports = global["$"] = __webpack_require__(7);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["Zepto"] = __webpack_require__(8);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),
/* 8 */
/***/ (function(module, exports) {

/* Zepto v1.1.6 - zepto event ajax form ie - zeptojs.com/license */

var Zepto = module.exports = (function() {
  var undefined, key, $, classList, emptyArray = [], slice = emptyArray.slice, filter = emptyArray.filter,
    document = window.document,
    elementDisplay = {}, classCache = {},
    cssNumber = { 'column-count': 1, 'columns': 1, 'font-weight': 1, 'line-height': 1,'opacity': 1, 'z-index': 1, 'zoom': 1 },
    fragmentRE = /^\s*<(\w+|!)[^>]*>/,
    singleTagRE = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
    tagExpanderRE = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
    rootNodeRE = /^(?:body|html)$/i,
    capitalRE = /([A-Z])/g,

    // special attributes that should be get/set via method calls
    methodAttributes = ['val', 'css', 'html', 'text', 'data', 'width', 'height', 'offset'],

    adjacencyOperators = [ 'after', 'prepend', 'before', 'append' ],
    table = document.createElement('table'),
    tableRow = document.createElement('tr'),
    containers = {
      'tr': document.createElement('tbody'),
      'tbody': table, 'thead': table, 'tfoot': table,
      'td': tableRow, 'th': tableRow,
      '*': document.createElement('div')
    },
    readyRE = /complete|loaded|interactive/,
    simpleSelectorRE = /^[\w-]*$/,
    class2type = {},
    toString = class2type.toString,
    zepto = {},
    camelize, uniq,
    tempParent = document.createElement('div'),
    propMap = {
      'tabindex': 'tabIndex',
      'readonly': 'readOnly',
      'for': 'htmlFor',
      'class': 'className',
      'maxlength': 'maxLength',
      'cellspacing': 'cellSpacing',
      'cellpadding': 'cellPadding',
      'rowspan': 'rowSpan',
      'colspan': 'colSpan',
      'usemap': 'useMap',
      'frameborder': 'frameBorder',
      'contenteditable': 'contentEditable'
    },
    isArray = Array.isArray ||
      function(object){ return object instanceof Array }

  zepto.matches = function(element, selector) {
    if (!selector || !element || element.nodeType !== 1) return false
    var matchesSelector = element.webkitMatchesSelector || element.mozMatchesSelector ||
                          element.oMatchesSelector || element.matchesSelector
    if (matchesSelector) return matchesSelector.call(element, selector)
    // fall back to performing a selector:
    var match, parent = element.parentNode, temp = !parent
    if (temp) (parent = tempParent).appendChild(element)
    match = ~zepto.qsa(parent, selector).indexOf(element)
    temp && tempParent.removeChild(element)
    return match
  }

  function type(obj) {
    return obj == null ? String(obj) :
      class2type[toString.call(obj)] || "object"
  }

  function isFunction(value) { return type(value) == "function" }
  function isWindow(obj)     { return obj != null && obj == obj.window }
  function isDocument(obj)   { return obj != null && obj.nodeType == obj.DOCUMENT_NODE }
  function isObject(obj)     { return type(obj) == "object" }
  function isPlainObject(obj) {
    return isObject(obj) && !isWindow(obj) && Object.getPrototypeOf(obj) == Object.prototype
  }
  function likeArray(obj) { return typeof obj.length == 'number' }

  function compact(array) { return filter.call(array, function(item){ return item != null }) }
  function flatten(array) { return array.length > 0 ? $.fn.concat.apply([], array) : array }
  camelize = function(str){ return str.replace(/-+(.)?/g, function(match, chr){ return chr ? chr.toUpperCase() : '' }) }
  function dasherize(str) {
    return str.replace(/::/g, '/')
           .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
           .replace(/([a-z\d])([A-Z])/g, '$1_$2')
           .replace(/_/g, '-')
           .toLowerCase()
  }
  uniq = function(array){ return filter.call(array, function(item, idx){ return array.indexOf(item) == idx }) }

  function classRE(name) {
    return name in classCache ?
      classCache[name] : (classCache[name] = new RegExp('(^|\\s)' + name + '(\\s|$)'))
  }

  function maybeAddPx(name, value) {
    return (typeof value == "number" && !cssNumber[dasherize(name)]) ? value + "px" : value
  }

  function defaultDisplay(nodeName) {
    var element, display
    if (!elementDisplay[nodeName]) {
      element = document.createElement(nodeName)
      document.body.appendChild(element)
      display = getComputedStyle(element, '').getPropertyValue("display")
      element.parentNode.removeChild(element)
      display == "none" && (display = "block")
      elementDisplay[nodeName] = display
    }
    return elementDisplay[nodeName]
  }

  function children(element) {
    return 'children' in element ?
      slice.call(element.children) :
      $.map(element.childNodes, function(node){ if (node.nodeType == 1) return node })
  }

  // `$.zepto.fragment` takes a html string and an optional tag name
  // to generate DOM nodes nodes from the given html string.
  // The generated DOM nodes are returned as an array.
  // This function can be overriden in plugins for example to make
  // it compatible with browsers that don't support the DOM fully.
  zepto.fragment = function(html, name, properties) {
    var dom, nodes, container

    // A special case optimization for a single tag
    if (singleTagRE.test(html)) dom = $(document.createElement(RegExp.$1))

    if (!dom) {
      if (html.replace) html = html.replace(tagExpanderRE, "<$1></$2>")
      if (name === undefined) name = fragmentRE.test(html) && RegExp.$1
      if (!(name in containers)) name = '*'

      container = containers[name]
      container.innerHTML = '' + html
      dom = $.each(slice.call(container.childNodes), function(){
        container.removeChild(this)
      })
    }

    if (isPlainObject(properties)) {
      nodes = $(dom)
      $.each(properties, function(key, value) {
        if (methodAttributes.indexOf(key) > -1) nodes[key](value)
        else nodes.attr(key, value)
      })
    }

    return dom
  }

  // `$.zepto.Z` swaps out the prototype of the given `dom` array
  // of nodes with `$.fn` and thus supplying all the Zepto functions
  // to the array. Note that `__proto__` is not supported on Internet
  // Explorer. This method can be overriden in plugins.
  zepto.Z = function(dom, selector) {
    dom = dom || []
    dom.__proto__ = $.fn
    dom.selector = selector || ''
    return dom
  }

  // `$.zepto.isZ` should return `true` if the given object is a Zepto
  // collection. This method can be overriden in plugins.
  zepto.isZ = function(object) {
    return object instanceof zepto.Z
  }

  // `$.zepto.init` is Zepto's counterpart to jQuery's `$.fn.init` and
  // takes a CSS selector and an optional context (and handles various
  // special cases).
  // This method can be overriden in plugins.
  zepto.init = function(selector, context) {
    var dom
    // If nothing given, return an empty Zepto collection
    if (!selector) return zepto.Z()
    // Optimize for string selectors
    else if (typeof selector == 'string') {
      selector = selector.trim()
      // If it's a html fragment, create nodes from it
      // Note: In both Chrome 21 and Firefox 15, DOM error 12
      // is thrown if the fragment doesn't begin with <
      if (selector[0] == '<' && fragmentRE.test(selector))
        dom = zepto.fragment(selector, RegExp.$1, context), selector = null
      // If there's a context, create a collection on that context first, and select
      // nodes from there
      else if (context !== undefined) return $(context).find(selector)
      // If it's a CSS selector, use it to select nodes.
      else dom = zepto.qsa(document, selector)
    }
    // If a function is given, call it when the DOM is ready
    else if (isFunction(selector)) return $(document).ready(selector)
    // If a Zepto collection is given, just return it
    else if (zepto.isZ(selector)) return selector
    else {
      // normalize array if an array of nodes is given
      if (isArray(selector)) dom = compact(selector)
      // Wrap DOM nodes.
      else if (isObject(selector))
        dom = [selector], selector = null
      // If it's a html fragment, create nodes from it
      else if (fragmentRE.test(selector))
        dom = zepto.fragment(selector.trim(), RegExp.$1, context), selector = null
      // If there's a context, create a collection on that context first, and select
      // nodes from there
      else if (context !== undefined) return $(context).find(selector)
      // And last but no least, if it's a CSS selector, use it to select nodes.
      else dom = zepto.qsa(document, selector)
    }
    // create a new Zepto collection from the nodes found
    return zepto.Z(dom, selector)
  }

  // `$` will be the base `Zepto` object. When calling this
  // function just call `$.zepto.init, which makes the implementation
  // details of selecting nodes and creating Zepto collections
  // patchable in plugins.
  $ = function(selector, context){
    return zepto.init(selector, context)
  }

  function extend(target, source, deep) {
    for (key in source)
      if (deep && (isPlainObject(source[key]) || isArray(source[key]))) {
        if (isPlainObject(source[key]) && !isPlainObject(target[key]))
          target[key] = {}
        if (isArray(source[key]) && !isArray(target[key]))
          target[key] = []
        extend(target[key], source[key], deep)
      }
      else if (source[key] !== undefined) target[key] = source[key]
  }

  // Copy all but undefined properties from one or more
  // objects to the `target` object.
  $.extend = function(target){
    var deep, args = slice.call(arguments, 1)
    if (typeof target == 'boolean') {
      deep = target
      target = args.shift()
    }
    args.forEach(function(arg){ extend(target, arg, deep) })
    return target
  }

  // `$.zepto.qsa` is Zepto's CSS selector implementation which
  // uses `document.querySelectorAll` and optimizes for some special cases, like `#id`.
  // This method can be overriden in plugins.
  zepto.qsa = function(element, selector){
    var found,
        maybeID = selector[0] == '#',
        maybeClass = !maybeID && selector[0] == '.',
        nameOnly = maybeID || maybeClass ? selector.slice(1) : selector, // Ensure that a 1 char tag name still gets checked
        isSimple = simpleSelectorRE.test(nameOnly)
    return (isDocument(element) && isSimple && maybeID) ?
      ( (found = element.getElementById(nameOnly)) ? [found] : [] ) :
      (element.nodeType !== 1 && element.nodeType !== 9) ? [] :
      slice.call(
        isSimple && !maybeID ?
          maybeClass ? element.getElementsByClassName(nameOnly) : // If it's simple, it could be a class
          element.getElementsByTagName(selector) : // Or a tag
          element.querySelectorAll(selector) // Or it's not simple, and we need to query all
      )
  }

  function filtered(nodes, selector) {
    return selector == null ? $(nodes) : $(nodes).filter(selector)
  }

  $.contains = document.documentElement.contains ?
    function(parent, node) {
      return parent !== node && parent.contains(node)
    } :
    function(parent, node) {
      while (node && (node = node.parentNode))
        if (node === parent) return true
      return false
    }

  function funcArg(context, arg, idx, payload) {
    return isFunction(arg) ? arg.call(context, idx, payload) : arg
  }

  function setAttribute(node, name, value) {
    value == null ? node.removeAttribute(name) : node.setAttribute(name, value)
  }

  // access className property while respecting SVGAnimatedString
  function className(node, value){
    var klass = node.className || '',
        svg   = klass && klass.baseVal !== undefined

    if (value === undefined) return svg ? klass.baseVal : klass
    svg ? (klass.baseVal = value) : (node.className = value)
  }

  // "true"  => true
  // "false" => false
  // "null"  => null
  // "42"    => 42
  // "42.5"  => 42.5
  // "08"    => "08"
  // JSON    => parse if valid
  // String  => self
  function deserializeValue(value) {
    try {
      return value ?
        value == "true" ||
        ( value == "false" ? false :
          value == "null" ? null :
          +value + "" == value ? +value :
          /^[\[\{]/.test(value) ? $.parseJSON(value) :
          value )
        : value
    } catch(e) {
      return value
    }
  }

  $.type = type
  $.isFunction = isFunction
  $.isWindow = isWindow
  $.isArray = isArray
  $.isPlainObject = isPlainObject

  $.isEmptyObject = function(obj) {
    var name
    for (name in obj) return false
    return true
  }

  $.inArray = function(elem, array, i){
    return emptyArray.indexOf.call(array, elem, i)
  }

  $.camelCase = camelize
  $.trim = function(str) {
    return str == null ? "" : String.prototype.trim.call(str)
  }

  // plugin compatibility
  $.uuid = 0
  $.support = { }
  $.expr = { }

  $.map = function(elements, callback){
    var value, values = [], i, key
    if (likeArray(elements))
      for (i = 0; i < elements.length; i++) {
        value = callback(elements[i], i)
        if (value != null) values.push(value)
      }
    else
      for (key in elements) {
        value = callback(elements[key], key)
        if (value != null) values.push(value)
      }
    return flatten(values)
  }

  $.each = function(elements, callback){
    var i, key
    if (likeArray(elements)) {
      for (i = 0; i < elements.length; i++)
        if (callback.call(elements[i], i, elements[i]) === false) return elements
    } else {
      for (key in elements)
        if (callback.call(elements[key], key, elements[key]) === false) return elements
    }

    return elements
  }

  $.grep = function(elements, callback){
    return filter.call(elements, callback)
  }

  if (window.JSON) $.parseJSON = JSON.parse

  // Populate the class2type map
  $.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
    class2type[ "[object " + name + "]" ] = name.toLowerCase()
  })

  // Define methods that will be available on all
  // Zepto collections
  $.fn = {
    // Because a collection acts like an array
    // copy over these useful array functions.
    forEach: emptyArray.forEach,
    reduce: emptyArray.reduce,
    push: emptyArray.push,
    sort: emptyArray.sort,
    indexOf: emptyArray.indexOf,
    concat: emptyArray.concat,

    // `map` and `slice` in the jQuery API work differently
    // from their array counterparts
    map: function(fn){
      return $($.map(this, function(el, i){ return fn.call(el, i, el) }))
    },
    slice: function(){
      return $(slice.apply(this, arguments))
    },

    ready: function(callback){
      // need to check if document.body exists for IE as that browser reports
      // document ready when it hasn't yet created the body element
      if (readyRE.test(document.readyState) && document.body) callback($)
      else document.addEventListener('DOMContentLoaded', function(){ callback($) }, false)
      return this
    },
    get: function(idx){
      return idx === undefined ? slice.call(this) : this[idx >= 0 ? idx : idx + this.length]
    },
    toArray: function(){ return this.get() },
    size: function(){
      return this.length
    },
    remove: function(){
      return this.each(function(){
        if (this.parentNode != null)
          this.parentNode.removeChild(this)
      })
    },
    each: function(callback){
      emptyArray.every.call(this, function(el, idx){
        return callback.call(el, idx, el) !== false
      })
      return this
    },
    filter: function(selector){
      if (isFunction(selector)) return this.not(this.not(selector))
      return $(filter.call(this, function(element){
        return zepto.matches(element, selector)
      }))
    },
    add: function(selector,context){
      return $(uniq(this.concat($(selector,context))))
    },
    is: function(selector){
      return this.length > 0 && zepto.matches(this[0], selector)
    },
    not: function(selector){
      var nodes=[]
      if (isFunction(selector) && selector.call !== undefined)
        this.each(function(idx){
          if (!selector.call(this,idx)) nodes.push(this)
        })
      else {
        var excludes = typeof selector == 'string' ? this.filter(selector) :
          (likeArray(selector) && isFunction(selector.item)) ? slice.call(selector) : $(selector)
        this.forEach(function(el){
          if (excludes.indexOf(el) < 0) nodes.push(el)
        })
      }
      return $(nodes)
    },
    has: function(selector){
      return this.filter(function(){
        return isObject(selector) ?
          $.contains(this, selector) :
          $(this).find(selector).size()
      })
    },
    eq: function(idx){
      return idx === -1 ? this.slice(idx) : this.slice(idx, + idx + 1)
    },
    first: function(){
      var el = this[0]
      return el && !isObject(el) ? el : $(el)
    },
    last: function(){
      var el = this[this.length - 1]
      return el && !isObject(el) ? el : $(el)
    },
    find: function(selector){
      var result, $this = this
      if (!selector) result = $()
      else if (typeof selector == 'object')
        result = $(selector).filter(function(){
          var node = this
          return emptyArray.some.call($this, function(parent){
            return $.contains(parent, node)
          })
        })
      else if (this.length == 1) result = $(zepto.qsa(this[0], selector))
      else result = this.map(function(){ return zepto.qsa(this, selector) })
      return result
    },
    closest: function(selector, context){
      var node = this[0], collection = false
      if (typeof selector == 'object') collection = $(selector)
      while (node && !(collection ? collection.indexOf(node) >= 0 : zepto.matches(node, selector)))
        node = node !== context && !isDocument(node) && node.parentNode
      return $(node)
    },
    parents: function(selector){
      var ancestors = [], nodes = this
      while (nodes.length > 0)
        nodes = $.map(nodes, function(node){
          if ((node = node.parentNode) && !isDocument(node) && ancestors.indexOf(node) < 0) {
            ancestors.push(node)
            return node
          }
        })
      return filtered(ancestors, selector)
    },
    parent: function(selector){
      return filtered(uniq(this.pluck('parentNode')), selector)
    },
    children: function(selector){
      return filtered(this.map(function(){ return children(this) }), selector)
    },
    contents: function() {
      return this.map(function() { return slice.call(this.childNodes) })
    },
    siblings: function(selector){
      return filtered(this.map(function(i, el){
        return filter.call(children(el.parentNode), function(child){ return child!==el })
      }), selector)
    },
    empty: function(){
      return this.each(function(){ this.innerHTML = '' })
    },
    // `pluck` is borrowed from Prototype.js
    pluck: function(property){
      return $.map(this, function(el){ return el[property] })
    },
    show: function(){
      return this.each(function(){
        this.style.display == "none" && (this.style.display = '')
        if (getComputedStyle(this, '').getPropertyValue("display") == "none")
          this.style.display = defaultDisplay(this.nodeName)
      })
    },
    replaceWith: function(newContent){
      return this.before(newContent).remove()
    },
    wrap: function(structure){
      var func = isFunction(structure)
      if (this[0] && !func)
        var dom   = $(structure).get(0),
            clone = dom.parentNode || this.length > 1

      return this.each(function(index){
        $(this).wrapAll(
          func ? structure.call(this, index) :
            clone ? dom.cloneNode(true) : dom
        )
      })
    },
    wrapAll: function(structure){
      if (this[0]) {
        $(this[0]).before(structure = $(structure))
        var children
        // drill down to the inmost element
        while ((children = structure.children()).length) structure = children.first()
        $(structure).append(this)
      }
      return this
    },
    wrapInner: function(structure){
      var func = isFunction(structure)
      return this.each(function(index){
        var self = $(this), contents = self.contents(),
            dom  = func ? structure.call(this, index) : structure
        contents.length ? contents.wrapAll(dom) : self.append(dom)
      })
    },
    unwrap: function(){
      this.parent().each(function(){
        $(this).replaceWith($(this).children())
      })
      return this
    },
    clone: function(){
      return this.map(function(){ return this.cloneNode(true) })
    },
    hide: function(){
      return this.css("display", "none")
    },
    toggle: function(setting){
      return this.each(function(){
        var el = $(this)
        ;(setting === undefined ? el.css("display") == "none" : setting) ? el.show() : el.hide()
      })
    },
    prev: function(selector){ return $(this.pluck('previousElementSibling')).filter(selector || '*') },
    next: function(selector){ return $(this.pluck('nextElementSibling')).filter(selector || '*') },
    html: function(html){
      return 0 in arguments ?
        this.each(function(idx){
          var originHtml = this.innerHTML
          $(this).empty().append( funcArg(this, html, idx, originHtml) )
        }) :
        (0 in this ? this[0].innerHTML : null)
    },
    text: function(text){
      return 0 in arguments ?
        this.each(function(idx){
          var newText = funcArg(this, text, idx, this.textContent)
          this.textContent = newText == null ? '' : ''+newText
        }) :
        (0 in this ? this[0].textContent : null)
    },
    attr: function(name, value){
      var result
      return (typeof name == 'string' && !(1 in arguments)) ?
        (!this.length || this[0].nodeType !== 1 ? undefined :
          (!(result = this[0].getAttribute(name)) && name in this[0]) ? this[0][name] : result
        ) :
        this.each(function(idx){
          if (this.nodeType !== 1) return
          if (isObject(name)) for (key in name) setAttribute(this, key, name[key])
          else setAttribute(this, name, funcArg(this, value, idx, this.getAttribute(name)))
        })
    },
    removeAttr: function(name){
      return this.each(function(){ this.nodeType === 1 && name.split(' ').forEach(function(attribute){
        setAttribute(this, attribute)
      }, this)})
    },
    prop: function(name, value){
      name = propMap[name] || name
      return (1 in arguments) ?
        this.each(function(idx){
          this[name] = funcArg(this, value, idx, this[name])
        }) :
        (this[0] && this[0][name])
    },
    data: function(name, value){
      var attrName = 'data-' + name.replace(capitalRE, '-$1').toLowerCase()

      var data = (1 in arguments) ?
        this.attr(attrName, value) :
        this.attr(attrName)

      return data !== null ? deserializeValue(data) : undefined
    },
    val: function(value){
      return 0 in arguments ?
        this.each(function(idx){
          this.value = funcArg(this, value, idx, this.value)
        }) :
        (this[0] && (this[0].multiple ?
           $(this[0]).find('option').filter(function(){ return this.selected }).pluck('value') :
           this[0].value)
        )
    },
    offset: function(coordinates){
      if (coordinates) return this.each(function(index){
        var $this = $(this),
            coords = funcArg(this, coordinates, index, $this.offset()),
            parentOffset = $this.offsetParent().offset(),
            props = {
              top:  coords.top  - parentOffset.top,
              left: coords.left - parentOffset.left
            }

        if ($this.css('position') == 'static') props['position'] = 'relative'
        $this.css(props)
      })
      if (!this.length) return null
      var obj = this[0].getBoundingClientRect()
      return {
        left: obj.left + window.pageXOffset,
        top: obj.top + window.pageYOffset,
        width: Math.round(obj.width),
        height: Math.round(obj.height)
      }
    },
    css: function(property, value){
      if (arguments.length < 2) {
        var computedStyle, element = this[0]
        if(!element) return
        computedStyle = getComputedStyle(element, '')
        if (typeof property == 'string')
          return element.style[camelize(property)] || computedStyle.getPropertyValue(property)
        else if (isArray(property)) {
          var props = {}
          $.each(property, function(_, prop){
            props[prop] = (element.style[camelize(prop)] || computedStyle.getPropertyValue(prop))
          })
          return props
        }
      }

      var css = ''
      if (type(property) == 'string') {
        if (!value && value !== 0)
          this.each(function(){ this.style.removeProperty(dasherize(property)) })
        else
          css = dasherize(property) + ":" + maybeAddPx(property, value)
      } else {
        for (key in property)
          if (!property[key] && property[key] !== 0)
            this.each(function(){ this.style.removeProperty(dasherize(key)) })
          else
            css += dasherize(key) + ':' + maybeAddPx(key, property[key]) + ';'
      }

      return this.each(function(){ this.style.cssText += ';' + css })
    },
    index: function(element){
      return element ? this.indexOf($(element)[0]) : this.parent().children().indexOf(this[0])
    },
    hasClass: function(name){
      if (!name) return false
      return emptyArray.some.call(this, function(el){
        return this.test(className(el))
      }, classRE(name))
    },
    addClass: function(name){
      if (!name) return this
      return this.each(function(idx){
        if (!('className' in this)) return
        classList = []
        var cls = className(this), newName = funcArg(this, name, idx, cls)
        newName.split(/\s+/g).forEach(function(klass){
          if (!$(this).hasClass(klass)) classList.push(klass)
        }, this)
        classList.length && className(this, cls + (cls ? " " : "") + classList.join(" "))
      })
    },
    removeClass: function(name){
      return this.each(function(idx){
        if (!('className' in this)) return
        if (name === undefined) return className(this, '')
        classList = className(this)
        funcArg(this, name, idx, classList).split(/\s+/g).forEach(function(klass){
          classList = classList.replace(classRE(klass), " ")
        })
        className(this, classList.trim())
      })
    },
    toggleClass: function(name, when){
      if (!name) return this
      return this.each(function(idx){
        var $this = $(this), names = funcArg(this, name, idx, className(this))
        names.split(/\s+/g).forEach(function(klass){
          (when === undefined ? !$this.hasClass(klass) : when) ?
            $this.addClass(klass) : $this.removeClass(klass)
        })
      })
    },
    scrollTop: function(value){
      if (!this.length) return
      var hasScrollTop = 'scrollTop' in this[0]
      if (value === undefined) return hasScrollTop ? this[0].scrollTop : this[0].pageYOffset
      return this.each(hasScrollTop ?
        function(){ this.scrollTop = value } :
        function(){ this.scrollTo(this.scrollX, value) })
    },
    scrollLeft: function(value){
      if (!this.length) return
      var hasScrollLeft = 'scrollLeft' in this[0]
      if (value === undefined) return hasScrollLeft ? this[0].scrollLeft : this[0].pageXOffset
      return this.each(hasScrollLeft ?
        function(){ this.scrollLeft = value } :
        function(){ this.scrollTo(value, this.scrollY) })
    },
    position: function() {
      if (!this.length) return

      var elem = this[0],
        // Get *real* offsetParent
        offsetParent = this.offsetParent(),
        // Get correct offsets
        offset       = this.offset(),
        parentOffset = rootNodeRE.test(offsetParent[0].nodeName) ? { top: 0, left: 0 } : offsetParent.offset()

      // Subtract element margins
      // note: when an element has margin: auto the offsetLeft and marginLeft
      // are the same in Safari causing offset.left to incorrectly be 0
      offset.top  -= parseFloat( $(elem).css('margin-top') ) || 0
      offset.left -= parseFloat( $(elem).css('margin-left') ) || 0

      // Add offsetParent borders
      parentOffset.top  += parseFloat( $(offsetParent[0]).css('border-top-width') ) || 0
      parentOffset.left += parseFloat( $(offsetParent[0]).css('border-left-width') ) || 0

      // Subtract the two offsets
      return {
        top:  offset.top  - parentOffset.top,
        left: offset.left - parentOffset.left
      }
    },
    offsetParent: function() {
      return this.map(function(){
        var parent = this.offsetParent || document.body
        while (parent && !rootNodeRE.test(parent.nodeName) && $(parent).css("position") == "static")
          parent = parent.offsetParent
        return parent
      })
    }
  }

  // for now
  $.fn.detach = $.fn.remove

  // Generate the `width` and `height` functions
  ;['width', 'height'].forEach(function(dimension){
    var dimensionProperty =
      dimension.replace(/./, function(m){ return m[0].toUpperCase() })

    $.fn[dimension] = function(value){
      var offset, el = this[0]
      if (value === undefined) return isWindow(el) ? el['inner' + dimensionProperty] :
        isDocument(el) ? el.documentElement['scroll' + dimensionProperty] :
        (offset = this.offset()) && offset[dimension]
      else return this.each(function(idx){
        el = $(this)
        el.css(dimension, funcArg(this, value, idx, el[dimension]()))
      })
    }
  })

  function traverseNode(node, fun) {
    fun(node)
    for (var i = 0, len = node.childNodes.length; i < len; i++)
      traverseNode(node.childNodes[i], fun)
  }

  // Generate the `after`, `prepend`, `before`, `append`,
  // `insertAfter`, `insertBefore`, `appendTo`, and `prependTo` methods.
  adjacencyOperators.forEach(function(operator, operatorIndex) {
    var inside = operatorIndex % 2 //=> prepend, append

    $.fn[operator] = function(){
      // arguments can be nodes, arrays of nodes, Zepto objects and HTML strings
      var argType, nodes = $.map(arguments, function(arg) {
            argType = type(arg)
            return argType == "object" || argType == "array" || arg == null ?
              arg : zepto.fragment(arg)
          }),
          parent, copyByClone = this.length > 1
      if (nodes.length < 1) return this

      return this.each(function(_, target){
        parent = inside ? target : target.parentNode

        // convert all methods to a "before" operation
        target = operatorIndex == 0 ? target.nextSibling :
                 operatorIndex == 1 ? target.firstChild :
                 operatorIndex == 2 ? target :
                 null

        var parentInDocument = $.contains(document.documentElement, parent)

        nodes.forEach(function(node){
          if (copyByClone) node = node.cloneNode(true)
          else if (!parent) return $(node).remove()

          parent.insertBefore(node, target)
          if (parentInDocument) traverseNode(node, function(el){
            if (el.nodeName != null && el.nodeName.toUpperCase() === 'SCRIPT' &&
               (!el.type || el.type === 'text/javascript') && !el.src)
              window['eval'].call(window, el.innerHTML)
          })
        })
      })
    }

    // after    => insertAfter
    // prepend  => prependTo
    // before   => insertBefore
    // append   => appendTo
    $.fn[inside ? operator+'To' : 'insert'+(operatorIndex ? 'Before' : 'After')] = function(html){
      $(html)[operator](this)
      return this
    }
  })

  zepto.Z.prototype = $.fn

  // Export internal API functions in the `$.zepto` namespace
  zepto.uniq = uniq
  zepto.deserializeValue = deserializeValue
  $.zepto = zepto

  return $
})()

;(function($){
  var _zid = 1, undefined,
      slice = Array.prototype.slice,
      isFunction = $.isFunction,
      isString = function(obj){ return typeof obj == 'string' },
      handlers = {},
      specialEvents={},
      focusinSupported = 'onfocusin' in window,
      focus = { focus: 'focusin', blur: 'focusout' },
      hover = { mouseenter: 'mouseover', mouseleave: 'mouseout' }

  specialEvents.click = specialEvents.mousedown = specialEvents.mouseup = specialEvents.mousemove = 'MouseEvents'

  function zid(element) {
    return element._zid || (element._zid = _zid++)
  }
  function findHandlers(element, event, fn, selector) {
    event = parse(event)
    if (event.ns) var matcher = matcherFor(event.ns)
    return (handlers[zid(element)] || []).filter(function(handler) {
      return handler
        && (!event.e  || handler.e == event.e)
        && (!event.ns || matcher.test(handler.ns))
        && (!fn       || zid(handler.fn) === zid(fn))
        && (!selector || handler.sel == selector)
    })
  }
  function parse(event) {
    var parts = ('' + event).split('.')
    return {e: parts[0], ns: parts.slice(1).sort().join(' ')}
  }
  function matcherFor(ns) {
    return new RegExp('(?:^| )' + ns.replace(' ', ' .* ?') + '(?: |$)')
  }

  function eventCapture(handler, captureSetting) {
    return handler.del &&
      (!focusinSupported && (handler.e in focus)) ||
      !!captureSetting
  }

  function realEvent(type) {
    return hover[type] || (focusinSupported && focus[type]) || type
  }

  function add(element, events, fn, data, selector, delegator, capture){
    var id = zid(element), set = (handlers[id] || (handlers[id] = []))
    events.split(/\s/).forEach(function(event){
      if (event == 'ready') return $(document).ready(fn)
      var handler   = parse(event)
      handler.fn    = fn
      handler.sel   = selector
      // emulate mouseenter, mouseleave
      if (handler.e in hover) fn = function(e){
        var related = e.relatedTarget
        if (!related || (related !== this && !$.contains(this, related)))
          return handler.fn.apply(this, arguments)
      }
      handler.del   = delegator
      var callback  = delegator || fn
      handler.proxy = function(e){
        e = compatible(e)
        if (e.isImmediatePropagationStopped()) return
        e.data = data
        var result = callback.apply(element, e._args == undefined ? [e] : [e].concat(e._args))
        if (result === false) e.preventDefault(), e.stopPropagation()
        return result
      }
      handler.i = set.length
      set.push(handler)
      if ('addEventListener' in element)
        element.addEventListener(realEvent(handler.e), handler.proxy, eventCapture(handler, capture))
    })
  }
  function remove(element, events, fn, selector, capture){
    var id = zid(element)
    ;(events || '').split(/\s/).forEach(function(event){
      findHandlers(element, event, fn, selector).forEach(function(handler){
        delete handlers[id][handler.i]
      if ('removeEventListener' in element)
        element.removeEventListener(realEvent(handler.e), handler.proxy, eventCapture(handler, capture))
      })
    })
  }

  $.event = { add: add, remove: remove }

  $.proxy = function(fn, context) {
    var args = (2 in arguments) && slice.call(arguments, 2)
    if (isFunction(fn)) {
      var proxyFn = function(){ return fn.apply(context, args ? args.concat(slice.call(arguments)) : arguments) }
      proxyFn._zid = zid(fn)
      return proxyFn
    } else if (isString(context)) {
      if (args) {
        args.unshift(fn[context], fn)
        return $.proxy.apply(null, args)
      } else {
        return $.proxy(fn[context], fn)
      }
    } else {
      throw new TypeError("expected function")
    }
  }

  $.fn.bind = function(event, data, callback){
    return this.on(event, data, callback)
  }
  $.fn.unbind = function(event, callback){
    return this.off(event, callback)
  }
  $.fn.one = function(event, selector, data, callback){
    return this.on(event, selector, data, callback, 1)
  }

  var returnTrue = function(){return true},
      returnFalse = function(){return false},
      ignoreProperties = /^([A-Z]|returnValue$|layer[XY]$)/,
      eventMethods = {
        preventDefault: 'isDefaultPrevented',
        stopImmediatePropagation: 'isImmediatePropagationStopped',
        stopPropagation: 'isPropagationStopped'
      }

  function compatible(event, source) {
    if (source || !event.isDefaultPrevented) {
      source || (source = event)

      $.each(eventMethods, function(name, predicate) {
        var sourceMethod = source[name]
        event[name] = function(){
          this[predicate] = returnTrue
          return sourceMethod && sourceMethod.apply(source, arguments)
        }
        event[predicate] = returnFalse
      })

      if (source.defaultPrevented !== undefined ? source.defaultPrevented :
          'returnValue' in source ? source.returnValue === false :
          source.getPreventDefault && source.getPreventDefault())
        event.isDefaultPrevented = returnTrue
    }
    return event
  }

  function createProxy(event) {
    var key, proxy = { originalEvent: event }
    for (key in event)
      if (!ignoreProperties.test(key) && event[key] !== undefined) proxy[key] = event[key]

    return compatible(proxy, event)
  }

  $.fn.delegate = function(selector, event, callback){
    return this.on(event, selector, callback)
  }
  $.fn.undelegate = function(selector, event, callback){
    return this.off(event, selector, callback)
  }

  $.fn.live = function(event, callback){
    $(document.body).delegate(this.selector, event, callback)
    return this
  }
  $.fn.die = function(event, callback){
    $(document.body).undelegate(this.selector, event, callback)
    return this
  }

  $.fn.on = function(event, selector, data, callback, one){
    var autoRemove, delegator, $this = this
    if (event && !isString(event)) {
      $.each(event, function(type, fn){
        $this.on(type, selector, data, fn, one)
      })
      return $this
    }

    if (!isString(selector) && !isFunction(callback) && callback !== false)
      callback = data, data = selector, selector = undefined
    if (isFunction(data) || data === false)
      callback = data, data = undefined

    if (callback === false) callback = returnFalse

    return $this.each(function(_, element){
      if (one) autoRemove = function(e){
        remove(element, e.type, callback)
        return callback.apply(this, arguments)
      }

      if (selector) delegator = function(e){
        var evt, match = $(e.target).closest(selector, element).get(0)
        if (match && match !== element) {
          evt = $.extend(createProxy(e), {currentTarget: match, liveFired: element})
          return (autoRemove || callback).apply(match, [evt].concat(slice.call(arguments, 1)))
        }
      }

      add(element, event, callback, data, selector, delegator || autoRemove)
    })
  }
  $.fn.off = function(event, selector, callback){
    var $this = this
    if (event && !isString(event)) {
      $.each(event, function(type, fn){
        $this.off(type, selector, fn)
      })
      return $this
    }

    if (!isString(selector) && !isFunction(callback) && callback !== false)
      callback = selector, selector = undefined

    if (callback === false) callback = returnFalse

    return $this.each(function(){
      remove(this, event, callback, selector)
    })
  }

  $.fn.trigger = function(event, args){
    event = (isString(event) || $.isPlainObject(event)) ? $.Event(event) : compatible(event)
    event._args = args
    return this.each(function(){
      // handle focus(), blur() by calling them directly
      if (event.type in focus && typeof this[event.type] == "function") this[event.type]()
      // items in the collection might not be DOM elements
      else if ('dispatchEvent' in this) this.dispatchEvent(event)
      else $(this).triggerHandler(event, args)
    })
  }

  // triggers event handlers on current element just as if an event occurred,
  // doesn't trigger an actual event, doesn't bubble
  $.fn.triggerHandler = function(event, args){
    var e, result
    this.each(function(i, element){
      e = createProxy(isString(event) ? $.Event(event) : event)
      e._args = args
      e.target = element
      $.each(findHandlers(element, event.type || event), function(i, handler){
        result = handler.proxy(e)
        if (e.isImmediatePropagationStopped()) return false
      })
    })
    return result
  }

  // shortcut methods for `.bind(event, fn)` for each event type
  ;('focusin focusout focus blur load resize scroll unload click dblclick '+
  'mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave '+
  'change select keydown keypress keyup error').split(' ').forEach(function(event) {
    $.fn[event] = function(callback) {
      return (0 in arguments) ?
        this.bind(event, callback) :
        this.trigger(event)
    }
  })

  $.Event = function(type, props) {
    if (!isString(type)) props = type, type = props.type
    var event = document.createEvent(specialEvents[type] || 'Events'), bubbles = true
    if (props) for (var name in props) (name == 'bubbles') ? (bubbles = !!props[name]) : (event[name] = props[name])
    event.initEvent(type, bubbles, true)
    return compatible(event)
  }

})(Zepto)

;(function($){
  var jsonpID = 0,
      document = window.document,
      key,
      name,
      rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
      scriptTypeRE = /^(?:text|application)\/javascript/i,
      xmlTypeRE = /^(?:text|application)\/xml/i,
      jsonType = 'application/json',
      htmlType = 'text/html',
      blankRE = /^\s*$/,
      originAnchor = document.createElement('a')

  originAnchor.href = window.location.href

  // trigger a custom event and return false if it was cancelled
  function triggerAndReturn(context, eventName, data) {
    var event = $.Event(eventName)
    $(context).trigger(event, data)
    return !event.isDefaultPrevented()
  }

  // trigger an Ajax "global" event
  function triggerGlobal(settings, context, eventName, data) {
    if (settings.global) return triggerAndReturn(context || document, eventName, data)
  }

  // Number of active Ajax requests
  $.active = 0

  function ajaxStart(settings) {
    if (settings.global && $.active++ === 0) triggerGlobal(settings, null, 'ajaxStart')
  }
  function ajaxStop(settings) {
    if (settings.global && !(--$.active)) triggerGlobal(settings, null, 'ajaxStop')
  }

  // triggers an extra global event "ajaxBeforeSend" that's like "ajaxSend" but cancelable
  function ajaxBeforeSend(xhr, settings) {
    var context = settings.context
    if (settings.beforeSend.call(context, xhr, settings) === false ||
        triggerGlobal(settings, context, 'ajaxBeforeSend', [xhr, settings]) === false)
      return false

    triggerGlobal(settings, context, 'ajaxSend', [xhr, settings])
  }
  function ajaxSuccess(data, xhr, settings, deferred) {
    var context = settings.context, status = 'success'
    settings.success.call(context, data, status, xhr)
    if (deferred) deferred.resolveWith(context, [data, status, xhr])
    triggerGlobal(settings, context, 'ajaxSuccess', [xhr, settings, data])
    ajaxComplete(status, xhr, settings)
  }
  // type: "timeout", "error", "abort", "parsererror"
  function ajaxError(error, type, xhr, settings, deferred) {
    var context = settings.context
    settings.error.call(context, xhr, type, error)
    if (deferred) deferred.rejectWith(context, [xhr, type, error])
    triggerGlobal(settings, context, 'ajaxError', [xhr, settings, error || type])
    ajaxComplete(type, xhr, settings)
  }
  // status: "success", "notmodified", "error", "timeout", "abort", "parsererror"
  function ajaxComplete(status, xhr, settings) {
    var context = settings.context
    settings.complete.call(context, xhr, status)
    triggerGlobal(settings, context, 'ajaxComplete', [xhr, settings])
    ajaxStop(settings)
  }

  // Empty function, used as default callback
  function empty() {}

  $.ajaxJSONP = function(options, deferred){
    if (!('type' in options)) return $.ajax(options)

    var _callbackName = options.jsonpCallback,
      callbackName = ($.isFunction(_callbackName) ?
        _callbackName() : _callbackName) || ('jsonp' + (++jsonpID)),
      script = document.createElement('script'),
      originalCallback = window[callbackName],
      responseData,
      abort = function(errorType) {
        $(script).triggerHandler('error', errorType || 'abort')
      },
      xhr = { abort: abort }, abortTimeout

    if (deferred) deferred.promise(xhr)

    $(script).on('load error', function(e, errorType){
      clearTimeout(abortTimeout)
      $(script).off().remove()

      if (e.type == 'error' || !responseData) {
        ajaxError(null, errorType || 'error', xhr, options, deferred)
      } else {
        ajaxSuccess(responseData[0], xhr, options, deferred)
      }

      window[callbackName] = originalCallback
      if (responseData && $.isFunction(originalCallback))
        originalCallback(responseData[0])

      originalCallback = responseData = undefined
    })

    if (ajaxBeforeSend(xhr, options) === false) {
      abort('abort')
      return xhr
    }

    window[callbackName] = function(){
      responseData = arguments
    }

    script.src = options.url.replace(/\?(.+)=\?/, '?$1=' + callbackName)
    document.head.appendChild(script)

    if (options.timeout > 0) abortTimeout = setTimeout(function(){
      abort('timeout')
    }, options.timeout)

    return xhr
  }

  $.ajaxSettings = {
    // Default type of request
    type: 'GET',
    // Callback that is executed before request
    beforeSend: empty,
    // Callback that is executed if the request succeeds
    success: empty,
    // Callback that is executed the the server drops error
    error: empty,
    // Callback that is executed on request complete (both: error and success)
    complete: empty,
    // The context for the callbacks
    context: null,
    // Whether to trigger "global" Ajax events
    global: true,
    // Transport
    xhr: function () {
      return new window.XMLHttpRequest()
    },
    // MIME types mapping
    // IIS returns Javascript as "application/x-javascript"
    accepts: {
      script: 'text/javascript, application/javascript, application/x-javascript',
      json:   jsonType,
      xml:    'application/xml, text/xml',
      html:   htmlType,
      text:   'text/plain'
    },
    // Whether the request is to another domain
    crossDomain: false,
    // Default timeout
    timeout: 0,
    // Whether data should be serialized to string
    processData: true,
    // Whether the browser should be allowed to cache GET responses
    cache: true
  }

  function mimeToDataType(mime) {
    if (mime) mime = mime.split(';', 2)[0]
    return mime && ( mime == htmlType ? 'html' :
      mime == jsonType ? 'json' :
      scriptTypeRE.test(mime) ? 'script' :
      xmlTypeRE.test(mime) && 'xml' ) || 'text'
  }

  function appendQuery(url, query) {
    if (query == '') return url
    return (url + '&' + query).replace(/[&?]{1,2}/, '?')
  }

  // serialize payload and append it to the URL for GET requests
  function serializeData(options) {
    if (options.processData && options.data && $.type(options.data) != "string")
      options.data = $.param(options.data, options.traditional)
    if (options.data && (!options.type || options.type.toUpperCase() == 'GET'))
      options.url = appendQuery(options.url, options.data), options.data = undefined
  }

  $.ajax = function(options){
    var settings = $.extend({}, options || {}),
        deferred = $.Deferred && $.Deferred(),
        urlAnchor
    for (key in $.ajaxSettings) if (settings[key] === undefined) settings[key] = $.ajaxSettings[key]

    ajaxStart(settings)

    if (!settings.crossDomain) {
      urlAnchor = document.createElement('a')
      urlAnchor.href = settings.url
      urlAnchor.href = urlAnchor.href
      settings.crossDomain = (originAnchor.protocol + '//' + originAnchor.host) !== (urlAnchor.protocol + '//' + urlAnchor.host)
    }

    if (!settings.url) settings.url = window.location.toString()
    serializeData(settings)

    var dataType = settings.dataType, hasPlaceholder = /\?.+=\?/.test(settings.url)
    if (hasPlaceholder) dataType = 'jsonp'

    if (settings.cache === false || (
         (!options || options.cache !== true) &&
         ('script' == dataType || 'jsonp' == dataType)
        ))
      settings.url = appendQuery(settings.url, '_=' + Date.now())

    if ('jsonp' == dataType) {
      if (!hasPlaceholder)
        settings.url = appendQuery(settings.url,
          settings.jsonp ? (settings.jsonp + '=?') : settings.jsonp === false ? '' : 'callback=?')
      return $.ajaxJSONP(settings, deferred)
    }

    var mime = settings.accepts[dataType],
        headers = { },
        setHeader = function(name, value) { headers[name.toLowerCase()] = [name, value] },
        protocol = /^([\w-]+:)\/\//.test(settings.url) ? RegExp.$1 : window.location.protocol,
        xhr = settings.xhr(),
        nativeSetHeader = xhr.setRequestHeader,
        abortTimeout

    if (deferred) deferred.promise(xhr)

    if (!settings.crossDomain) setHeader('X-Requested-With', 'XMLHttpRequest')
    setHeader('Accept', mime || '*/*')
    if (mime = settings.mimeType || mime) {
      if (mime.indexOf(',') > -1) mime = mime.split(',', 2)[0]
      xhr.overrideMimeType && xhr.overrideMimeType(mime)
    }
    if (settings.contentType || (settings.contentType !== false && settings.data && settings.type.toUpperCase() != 'GET'))
      setHeader('Content-Type', settings.contentType || 'application/x-www-form-urlencoded')

    if (settings.headers) for (name in settings.headers) setHeader(name, settings.headers[name])
    xhr.setRequestHeader = setHeader

    xhr.onreadystatechange = function(){
      if (xhr.readyState == 4) {
        xhr.onreadystatechange = empty
        clearTimeout(abortTimeout)
        var result, error = false
        if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304 || (xhr.status == 0 && protocol == 'file:')) {
          dataType = dataType || mimeToDataType(settings.mimeType || xhr.getResponseHeader('content-type'))
          result = xhr.responseText

          try {
            // http://perfectionkills.com/global-eval-what-are-the-options/
            if (dataType == 'script')    (1,eval)(result)
            else if (dataType == 'xml')  result = xhr.responseXML
            else if (dataType == 'json') result = blankRE.test(result) ? null : $.parseJSON(result)
          } catch (e) { error = e }

          if (error) ajaxError(error, 'parsererror', xhr, settings, deferred)
          else ajaxSuccess(result, xhr, settings, deferred)
        } else {
          ajaxError(xhr.statusText || null, xhr.status ? 'error' : 'abort', xhr, settings, deferred)
        }
      }
    }

    if (ajaxBeforeSend(xhr, settings) === false) {
      xhr.abort()
      ajaxError(null, 'abort', xhr, settings, deferred)
      return xhr
    }

    if (settings.xhrFields) for (name in settings.xhrFields) xhr[name] = settings.xhrFields[name]

    var async = 'async' in settings ? settings.async : true
    xhr.open(settings.type, settings.url, async, settings.username, settings.password)

    for (name in headers) nativeSetHeader.apply(xhr, headers[name])

    if (settings.timeout > 0) abortTimeout = setTimeout(function(){
        xhr.onreadystatechange = empty
        xhr.abort()
        ajaxError(null, 'timeout', xhr, settings, deferred)
      }, settings.timeout)

    // avoid sending empty string (#319)
    xhr.send(settings.data ? settings.data : null)
    return xhr
  }

  // handle optional data/success arguments
  function parseArguments(url, data, success, dataType) {
    if ($.isFunction(data)) dataType = success, success = data, data = undefined
    if (!$.isFunction(success)) dataType = success, success = undefined
    return {
      url: url
    , data: data
    , success: success
    , dataType: dataType
    }
  }

  $.get = function(/* url, data, success, dataType */){
    return $.ajax(parseArguments.apply(null, arguments))
  }

  $.post = function(/* url, data, success, dataType */){
    var options = parseArguments.apply(null, arguments)
    options.type = 'POST'
    return $.ajax(options)
  }

  $.getJSON = function(/* url, data, success */){
    var options = parseArguments.apply(null, arguments)
    options.dataType = 'json'
    return $.ajax(options)
  }

  $.fn.load = function(url, data, success){
    if (!this.length) return this
    var self = this, parts = url.split(/\s/), selector,
        options = parseArguments(url, data, success),
        callback = options.success
    if (parts.length > 1) options.url = parts[0], selector = parts[1]
    options.success = function(response){
      self.html(selector ?
        $('<div>').html(response.replace(rscript, "")).find(selector)
        : response)
      callback && callback.apply(self, arguments)
    }
    $.ajax(options)
    return this
  }

  var escape = encodeURIComponent

  function serialize(params, obj, traditional, scope){
    var type, array = $.isArray(obj), hash = $.isPlainObject(obj)
    $.each(obj, function(key, value) {
      type = $.type(value)
      if (scope) key = traditional ? scope :
        scope + '[' + (hash || type == 'object' || type == 'array' ? key : '') + ']'
      // handle data in serializeArray() format
      if (!scope && array) params.add(value.name, value.value)
      // recurse into nested objects
      else if (type == "array" || (!traditional && type == "object"))
        serialize(params, value, traditional, key)
      else params.add(key, value)
    })
  }

  $.param = function(obj, traditional){
    var params = []
    params.add = function(key, value) {
      if ($.isFunction(value)) value = value()
      if (value == null) value = ""
      this.push(escape(key) + '=' + escape(value))
    }
    serialize(params, obj, traditional)
    return params.join('&').replace(/%20/g, '+')
  }
})(Zepto)

;(function($){
  $.fn.serializeArray = function() {
    var name, type, result = [],
      add = function(value) {
        if (value.forEach) return value.forEach(add)
        result.push({ name: name, value: value })
      }
    if (this[0]) $.each(this[0].elements, function(_, field){
      type = field.type, name = field.name
      if (name && field.nodeName.toLowerCase() != 'fieldset' &&
        !field.disabled && type != 'submit' && type != 'reset' && type != 'button' && type != 'file' &&
        ((type != 'radio' && type != 'checkbox') || field.checked))
          add($(field).val())
    })
    return result
  }

  $.fn.serialize = function(){
    var result = []
    this.serializeArray().forEach(function(elm){
      result.push(encodeURIComponent(elm.name) + '=' + encodeURIComponent(elm.value))
    })
    return result.join('&')
  }

  $.fn.submit = function(callback) {
    if (0 in arguments) this.bind('submit', callback)
    else if (this.length) {
      var event = $.Event('submit')
      this.eq(0).trigger(event)
      if (!event.isDefaultPrevented()) this.get(0).submit()
    }
    return this
  }

})(Zepto)

;(function($){
  // __proto__ doesn't exist on IE<11, so redefine
  // the Z function to use object extension instead
  if (!('__proto__' in {})) {
    $.extend($.zepto, {
      Z: function(dom, selector){
        dom = dom || []
        $.extend(dom, $.fn)
        dom.selector = selector || ''
        dom.__Z = true
        return dom
      },
      // this is a kludge but works
      isZ: function(object){
        return $.type(object) === 'array' && '__Z' in object
      }
    })
  }

  // getComputedStyle shouldn't freak out when called
  // without a valid element as argument
  try {
    getComputedStyle(undefined)
  } catch(e) {
    var nativeGetComputedStyle = getComputedStyle;
    window.getComputedStyle = function(element){
      try {
        return nativeGetComputedStyle(element)
      } catch(e) {
        return null
      }
    }
  }
})(Zepto)

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)(__webpack_require__(10))

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "\"use strict\";\n\nvar _typeof = typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; };\n\n$.smVersion = \"0.6.2\", +function (a) {\n    \"use strict\";\n\n    var b = { autoInit: !1, showPageLoadingIndicator: !0, router: !0, swipePanel: \"left\", swipePanelOnlyClose: !0 };\n    a.smConfig = a.extend(b, a.config);\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    a.compareVersion = function (a, b) {\n        var c = a.split(\".\"),\n            d = b.split(\".\");\n        if (a === b) return 0;\n        for (var e = 0; e < c.length; e++) {\n            var f = parseInt(c[e]);\n            if (!d[e]) return 1;\n            var g = parseInt(d[e]);\n            if (g > f) return -1;\n            if (f > g) return 1;\n        }\n        return -1;\n    }, a.getCurrentPage = function () {\n        return a(\".page-current\")[0] || a(\".page\")[0] || document.body;\n    };\n}(Zepto), function (a) {\n    \"use strict\";\n\n    function b(a, b) {\n        function c(a) {\n            if (a.target === this) for (b.call(this, a), d = 0; d < e.length; d++) {\n                f.off(e[d], c);\n            }\n        }\n\n        var d,\n            e = a,\n            f = this;\n        if (b) for (d = 0; d < e.length; d++) {\n            f.on(e[d], c);\n        }\n    }\n\n    [\"width\", \"height\"].forEach(function (b) {\n        var c = b.replace(/./, function (a) {\n            return a[0].toUpperCase();\n        });\n        a.fn[\"outer\" + c] = function (a) {\n            var c = this;\n            if (c) {\n                var d = c[b](),\n                    e = { width: [\"left\", \"right\"], height: [\"top\", \"bottom\"] };\n                return e[b].forEach(function (b) {\n                    a && (d += parseInt(c.css(\"margin-\" + b), 10));\n                }), d;\n            }\n            return null;\n        };\n    }), a.support = function () {\n        var a = { touch: !!(\"ontouchstart\" in window || window.DocumentTouch && document instanceof window.DocumentTouch) };\n        return a;\n    }(), a.touchEvents = {\n        start: a.support.touch ? \"touchstart\" : \"mousedown\",\n        move: a.support.touch ? \"touchmove\" : \"mousemove\",\n        end: a.support.touch ? \"touchend\" : \"mouseup\"\n    }, a.getTranslate = function (a, b) {\n        var c, d, e, f;\n        return \"undefined\" == typeof b && (b = \"x\"), e = window.getComputedStyle(a, null), window.WebKitCSSMatrix ? f = new WebKitCSSMatrix(\"none\" === e.webkitTransform ? \"\" : e.webkitTransform) : (f = e.MozTransform || e.transform || e.getPropertyValue(\"transform\").replace(\"translate(\", \"matrix(1, 0, 0, 1,\"), c = f.toString().split(\",\")), \"x\" === b && (d = window.WebKitCSSMatrix ? f.m41 : 16 === c.length ? parseFloat(c[12]) : parseFloat(c[4])), \"y\" === b && (d = window.WebKitCSSMatrix ? f.m42 : 16 === c.length ? parseFloat(c[13]) : parseFloat(c[5])), d || 0;\n    }, a.requestAnimationFrame = function (a) {\n        return window.requestAnimationFrame ? window.requestAnimationFrame(a) : window.webkitRequestAnimationFrame ? window.webkitRequestAnimationFrame(a) : window.mozRequestAnimationFrame ? window.mozRequestAnimationFrame(a) : window.setTimeout(a, 1e3 / 60);\n    }, a.cancelAnimationFrame = function (a) {\n        return window.cancelAnimationFrame ? window.cancelAnimationFrame(a) : window.webkitCancelAnimationFrame ? window.webkitCancelAnimationFrame(a) : window.mozCancelAnimationFrame ? window.mozCancelAnimationFrame(a) : window.clearTimeout(a);\n    }, a.fn.dataset = function () {\n        var b = {},\n            c = this[0].dataset;\n        for (var d in c) {\n            var e = b[d] = c[d];\n            \"false\" === e ? b[d] = !1 : \"true\" === e ? b[d] = !0 : parseFloat(e) === 1 * e && (b[d] = 1 * e);\n        }\n        return a.extend({}, b, this[0].__eleData);\n    }, a.fn.data = function (b, c) {\n        var d = a(this).dataset();\n        if (!b) return d;\n        if (\"undefined\" == typeof c) {\n            var e = d[b],\n                f = this[0].__eleData;\n            return f && b in f ? f[b] : e;\n        }\n        for (var g = 0; g < this.length; g++) {\n            var h = this[g];\n            b in d && delete h.dataset[b], h.__eleData || (h.__eleData = {}), h.__eleData[b] = c;\n        }\n        return this;\n    }, a.fn.animationEnd = function (a) {\n        return b.call(this, [\"webkitAnimationEnd\", \"animationend\"], a), this;\n    }, a.fn.transitionEnd = function (a) {\n        return b.call(this, [\"webkitTransitionEnd\", \"transitionend\"], a), this;\n    }, a.fn.transition = function (a) {\n        \"string\" != typeof a && (a += \"ms\");\n        for (var b = 0; b < this.length; b++) {\n            var c = this[b].style;\n            c.webkitTransitionDuration = c.MozTransitionDuration = c.transitionDuration = a;\n        }\n        return this;\n    }, a.fn.transform = function (a) {\n        for (var b = 0; b < this.length; b++) {\n            var c = this[b].style;\n            c.webkitTransform = c.MozTransform = c.transform = a;\n        }\n        return this;\n    }, a.fn.prevAll = function (b) {\n        var c = [],\n            d = this[0];\n        if (!d) return a([]);\n        for (; d.previousElementSibling;) {\n            var e = d.previousElementSibling;\n            b ? a(e).is(b) && c.push(e) : c.push(e), d = e;\n        }\n        return a(c);\n    }, a.fn.nextAll = function (b) {\n        var c = [],\n            d = this[0];\n        if (!d) return a([]);\n        for (; d.nextElementSibling;) {\n            var e = d.nextElementSibling;\n            b ? a(e).is(b) && c.push(e) : c.push(e), d = e;\n        }\n        return a(c);\n    }, a.fn.show = function () {\n        function a(a) {\n            var c, d;\n            return b[a] || (c = document.createElement(a), document.body.appendChild(c), d = getComputedStyle(c, \"\").getPropertyValue(\"display\"), c.parentNode.removeChild(c), \"none\" === d && (d = \"block\"), b[a] = d), b[a];\n        }\n\n        var b = {};\n        return this.each(function () {\n            \"none\" === this.style.display && (this.style.display = \"\"), \"none\" === getComputedStyle(this, \"\").getPropertyValue(\"display\"), this.style.display = a(this.nodeName);\n        });\n    };\n}(Zepto), function (a) {\n    \"use strict\";\n\n    var b = {},\n        c = navigator.userAgent,\n        d = c.match(/(Android);?[\\s\\/]+([\\d.]+)?/),\n        e = c.match(/(iPad).*OS\\s([\\d_]+)/),\n        f = c.match(/(iPod)(.*OS\\s([\\d_]+))?/),\n        g = !e && c.match(/(iPhone\\sOS)\\s([\\d_]+)/);\n    if (b.ios = b.android = b.iphone = b.ipad = b.androidChrome = !1, d && (b.os = \"android\", b.osVersion = d[2], b.android = !0, b.androidChrome = c.toLowerCase().indexOf(\"chrome\") >= 0), (e || g || f) && (b.os = \"ios\", b.ios = !0), g && !f && (b.osVersion = g[2].replace(/_/g, \".\"), b.iphone = !0), e && (b.osVersion = e[2].replace(/_/g, \".\"), b.ipad = !0), f && (b.osVersion = f[3] ? f[3].replace(/_/g, \".\") : null, b.iphone = !0), b.ios && b.osVersion && c.indexOf(\"Version/\") >= 0 && \"10\" === b.osVersion.split(\".\")[0] && (b.osVersion = c.toLowerCase().split(\"version/\")[1].split(\" \")[0]), b.webView = (g || e || f) && c.match(/.*AppleWebKit(?!.*Safari)/i), b.os && \"ios\" === b.os) {\n        var h = b.osVersion.split(\".\");\n        b.minimalUi = !b.webView && (f || g) && (1 * h[0] === 7 ? 1 * h[1] >= 1 : 1 * h[0] > 7) && a('meta[name=\"viewport\"]').length > 0 && a('meta[name=\"viewport\"]').attr(\"content\").indexOf(\"minimal-ui\") >= 0;\n    }\n    var i = a(window).width(),\n        j = a(window).height();\n    b.statusBar = !1, b.webView && i * j === screen.width * screen.height ? b.statusBar = !0 : b.statusBar = !1;\n    var k = [];\n    if (b.pixelRatio = window.devicePixelRatio || 1, k.push(\"pixel-ratio-\" + Math.floor(b.pixelRatio)), b.pixelRatio >= 2 && k.push(\"retina\"), b.os && (k.push(b.os, b.os + \"-\" + b.osVersion.split(\".\")[0], b.os + \"-\" + b.osVersion.replace(/\\./g, \"-\")), \"ios\" === b.os)) for (var l = parseInt(b.osVersion.split(\".\")[0], 10), m = l - 1; m >= 6; m--) {\n        k.push(\"ios-gt-\" + m);\n    }b.statusBar ? k.push(\"with-statusbar-overlay\") : a(\"html\").removeClass(\"with-statusbar-overlay\"), k.length > 0 && a(\"html\").addClass(k.join(\" \")), b.isWeixin = /MicroMessenger/i.test(c), a.device = b;\n}(Zepto), function () {\n    \"use strict\";\n\n    function a(b, d) {\n        function e(a, b) {\n            return function () {\n                return a.apply(b, arguments);\n            };\n        }\n\n        var f;\n        if (d = d || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = d.touchBoundary || 10, this.layer = b, this.tapDelay = d.tapDelay || 200, this.tapTimeout = d.tapTimeout || 700, !a.notNeeded(b)) {\n            for (var g = [\"onMouse\", \"onClick\", \"onTouchStart\", \"onTouchMove\", \"onTouchEnd\", \"onTouchCancel\"], h = this, i = 0, j = g.length; j > i; i++) {\n                h[g[i]] = e(h[g[i]], h);\n            }c && (b.addEventListener(\"mouseover\", this.onMouse, !0), b.addEventListener(\"mousedown\", this.onMouse, !0), b.addEventListener(\"mouseup\", this.onMouse, !0)), b.addEventListener(\"click\", this.onClick, !0), b.addEventListener(\"touchstart\", this.onTouchStart, !1), b.addEventListener(\"touchmove\", this.onTouchMove, !1), b.addEventListener(\"touchend\", this.onTouchEnd, !1), b.addEventListener(\"touchcancel\", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (b.removeEventListener = function (a, c, d) {\n                var e = Node.prototype.removeEventListener;\n                \"click\" === a ? e.call(b, a, c.hijacked || c, d) : e.call(b, a, c, d);\n            }, b.addEventListener = function (a, c, d) {\n                var e = Node.prototype.addEventListener;\n                \"click\" === a ? e.call(b, a, c.hijacked || (c.hijacked = function (a) {\n                    a.propagationStopped || c(a);\n                }), d) : e.call(b, a, c, d);\n            }), \"function\" == typeof b.onclick && (f = b.onclick, b.addEventListener(\"click\", function (a) {\n                f(a);\n            }, !1), b.onclick = null);\n        }\n    }\n\n    var b = navigator.userAgent.indexOf(\"Windows Phone\") >= 0,\n        c = navigator.userAgent.indexOf(\"Android\") > 0 && !b,\n        d = /iP(ad|hone|od)/.test(navigator.userAgent) && !b,\n        e = d && /OS 4_\\d(_\\d)?/.test(navigator.userAgent),\n        f = d && /OS [6-7]_\\d/.test(navigator.userAgent),\n        g = navigator.userAgent.indexOf(\"BB10\") > 0,\n        h = !1;\n    a.prototype.needsClick = function (a) {\n        for (var b = a; b && \"BODY\" !== b.tagName.toUpperCase();) {\n            if (\"LABEL\" === b.tagName.toUpperCase() && (h = !0, /\\bneedsclick\\b/.test(b.className))) return !0;\n            b = b.parentNode;\n        }\n        switch (a.nodeName.toLowerCase()) {\n            case \"button\":\n            case \"select\":\n            case \"textarea\":\n                if (a.disabled) return !0;\n                break;\n            case \"input\":\n                if (d && \"file\" === a.type || a.disabled) return !0;\n                break;\n            case \"label\":\n            case \"iframe\":\n            case \"video\":\n                return !0;\n        }\n        return (/\\bneedsclick\\b/.test(a.className)\n        );\n    }, a.prototype.needsFocus = function (a) {\n        switch (a.nodeName.toLowerCase()) {\n            case \"textarea\":\n                return !0;\n            case \"select\":\n                return !c;\n            case \"input\":\n                switch (a.type) {\n                    case \"button\":\n                    case \"checkbox\":\n                    case \"file\":\n                    case \"image\":\n                    case \"radio\":\n                    case \"submit\":\n                        return !1;\n                }\n                return !a.disabled && !a.readOnly;\n            default:\n                return (/\\bneedsfocus\\b/.test(a.className)\n                );\n        }\n    }, a.prototype.sendClick = function (a, b) {\n        var c, d;\n        document.activeElement && document.activeElement !== a && document.activeElement.blur(), d = b.changedTouches[0], c = document.createEvent(\"MouseEvents\"), c.initMouseEvent(this.determineEventType(a), !0, !0, window, 1, d.screenX, d.screenY, d.clientX, d.clientY, !1, !1, !1, !1, 0, null), c.forwardedTouchEvent = !0, a.dispatchEvent(c);\n    }, a.prototype.determineEventType = function (a) {\n        return c && \"select\" === a.tagName.toLowerCase() ? \"mousedown\" : \"click\";\n    }, a.prototype.focus = function (a) {\n        var b,\n            c = [\"date\", \"time\", \"month\", \"number\", \"email\"];\n        d && a.setSelectionRange && -1 === c.indexOf(a.type) ? (b = a.value.length, a.setSelectionRange(b, b)) : a.focus();\n    }, a.prototype.updateScrollParent = function (a) {\n        var b, c;\n        if (b = a.fastClickScrollParent, !b || !b.contains(a)) {\n            c = a;\n            do {\n                if (c.scrollHeight > c.offsetHeight) {\n                    b = c, a.fastClickScrollParent = c;\n                    break;\n                }\n                c = c.parentElement;\n            } while (c);\n        }\n        b && (b.fastClickLastScrollTop = b.scrollTop);\n    }, a.prototype.getTargetElementFromEventTarget = function (a) {\n        return a.nodeType === Node.TEXT_NODE ? a.parentNode : a;\n    }, a.prototype.onTouchStart = function (a) {\n        var b, c, f;\n        if (a.targetTouches.length > 1) return !0;\n        if (b = this.getTargetElementFromEventTarget(a.target), c = a.targetTouches[0], d) {\n            if (f = window.getSelection(), f.rangeCount && !f.isCollapsed) return !0;\n            if (!e) {\n                if (c.identifier && c.identifier === this.lastTouchIdentifier) return a.preventDefault(), !1;\n                this.lastTouchIdentifier = c.identifier, this.updateScrollParent(b);\n            }\n        }\n        return this.trackingClick = !0, this.trackingClickStart = a.timeStamp, this.targetElement = b, this.touchStartX = c.pageX, this.touchStartY = c.pageY, a.timeStamp - this.lastClickTime < this.tapDelay && a.preventDefault(), !0;\n    }, a.prototype.touchHasMoved = function (a) {\n        var b = a.changedTouches[0],\n            c = this.touchBoundary;\n        return Math.abs(b.pageX - this.touchStartX) > c || Math.abs(b.pageY - this.touchStartY) > c;\n    }, a.prototype.onTouchMove = function (a) {\n        return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(a.target) || this.touchHasMoved(a)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0;\n    }, a.prototype.findControl = function (a) {\n        return void 0 !== a.control ? a.control : a.htmlFor ? document.getElementById(a.htmlFor) : a.querySelector(\"button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea\");\n    }, a.prototype.onTouchEnd = function (a) {\n        var b,\n            g,\n            h,\n            i,\n            j,\n            k = this.targetElement;\n        if (!this.trackingClick) return !0;\n        if (a.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, !0;\n        if (a.timeStamp - this.trackingClickStart > this.tapTimeout) return !0;\n        var l = [\"date\", \"time\", \"month\"];\n        if (-1 !== l.indexOf(a.target.type)) return !1;\n        if (this.cancelNextClick = !1, this.lastClickTime = a.timeStamp, g = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, f && (j = a.changedTouches[0], k = document.elementFromPoint(j.pageX - window.pageXOffset, j.pageY - window.pageYOffset) || k, k.fastClickScrollParent = this.targetElement.fastClickScrollParent), h = k.tagName.toLowerCase(), \"label\" === h) {\n            if (b = this.findControl(k)) {\n                if (this.focus(k), c) return !1;\n                k = b;\n            }\n        } else if (this.needsFocus(k)) return a.timeStamp - g > 100 || d && window.top !== window && \"input\" === h ? (this.targetElement = null, !1) : (this.focus(k), this.sendClick(k, a), d && \"select\" === h || (this.targetElement = null, a.preventDefault()), !1);\n        return d && !e && (i = k.fastClickScrollParent, i && i.fastClickLastScrollTop !== i.scrollTop) ? !0 : (this.needsClick(k) || (a.preventDefault(), this.sendClick(k, a)), !1);\n    }, a.prototype.onTouchCancel = function () {\n        this.trackingClick = !1, this.targetElement = null;\n    }, a.prototype.onMouse = function (a) {\n        return this.targetElement ? a.forwardedTouchEvent ? !0 : a.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (a.stopImmediatePropagation ? a.stopImmediatePropagation() : a.propagationStopped = !0, a.stopPropagation(), h || a.preventDefault(), !1) : !0 : !0;\n    }, a.prototype.onClick = function (a) {\n        var b;\n        return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : \"submit\" === a.target.type && 0 === a.detail ? !0 : (b = this.onMouse(a), b || (this.targetElement = null), b);\n    }, a.prototype.destroy = function () {\n        var a = this.layer;\n        c && (a.removeEventListener(\"mouseover\", this.onMouse, !0), a.removeEventListener(\"mousedown\", this.onMouse, !0), a.removeEventListener(\"mouseup\", this.onMouse, !0)), a.removeEventListener(\"click\", this.onClick, !0), a.removeEventListener(\"touchstart\", this.onTouchStart, !1), a.removeEventListener(\"touchmove\", this.onTouchMove, !1), a.removeEventListener(\"touchend\", this.onTouchEnd, !1), a.removeEventListener(\"touchcancel\", this.onTouchCancel, !1);\n    }, a.notNeeded = function (a) {\n        var b, d, e, f;\n        if (\"undefined\" == typeof window.ontouchstart) return !0;\n        if (d = +(/Chrome\\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {\n            if (!c) return !0;\n            if (b = document.querySelector(\"meta[name=viewport]\")) {\n                if (-1 !== b.content.indexOf(\"user-scalable=no\")) return !0;\n                if (d > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0;\n            }\n        }\n        if (g && (e = navigator.userAgent.match(/Version\\/([0-9]*)\\.([0-9]*)/), e[1] >= 10 && e[2] >= 3 && (b = document.querySelector(\"meta[name=viewport]\")))) {\n            if (-1 !== b.content.indexOf(\"user-scalable=no\")) return !0;\n            if (document.documentElement.scrollWidth <= window.outerWidth) return !0;\n        }\n        return \"none\" === a.style.msTouchAction || \"manipulation\" === a.style.touchAction ? !0 : (f = +(/Firefox\\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1], f >= 27 && (b = document.querySelector(\"meta[name=viewport]\"), b && (-1 !== b.content.indexOf(\"user-scalable=no\") || document.documentElement.scrollWidth <= window.outerWidth)) ? !0 : \"none\" === a.style.touchAction || \"manipulation\" === a.style.touchAction);\n    }, a.attach = function (b, c) {\n        return new a(b, c);\n    }, window.FastClick = a;\n}(), +function (a) {\n    \"use strict\";\n\n    function b(b) {\n        var c,\n            e = a(this),\n            f = (e.attr(\"href\"), e.dataset());\n        e.hasClass(\"open-popup\") && (c = f.popup ? f.popup : \".popup\", a.popup(c)), e.hasClass(\"close-popup\") && (c = f.popup ? f.popup : \".popup.modal-in\", a.closeModal(c)), e.hasClass(\"modal-overlay\") && (a(\".modal.modal-in\").length > 0 && d.modalCloseByOutside && a.closeModal(\".modal.modal-in\"), a(\".actions-modal.modal-in\").length > 0 && d.actionsCloseByOutside && a.closeModal(\".actions-modal.modal-in\")), e.hasClass(\"popup-overlay\") && a(\".popup.modal-in\").length > 0 && d.popupCloseByOutside && a.closeModal(\".popup.modal-in\");\n    }\n\n    var c = document.createElement(\"div\");\n    a.modalStack = [], a.modalStackClearQueue = function () {\n        a.modalStack.length && a.modalStack.shift()();\n    }, a.modal = function (b) {\n        b = b || {};\n        var e = \"\",\n            f = \"\";\n        if (b.buttons && b.buttons.length > 0) for (var g = 0; g < b.buttons.length; g++) {\n            f += '<span class=\"modal-button' + (b.buttons[g].bold ? \" modal-button-bold\" : \"\") + '\">' + b.buttons[g].text + \"</span>\";\n        }var h = b.extraClass || \"\",\n            i = b.title ? '<div class=\"modal-title\">' + b.title + \"</div>\" : \"\",\n            j = b.text ? '<div class=\"modal-text\">' + b.text + \"</div>\" : \"\",\n            k = b.afterText ? b.afterText : \"\",\n            l = b.buttons && 0 !== b.buttons.length ? \"\" : \"modal-no-buttons\",\n            m = b.verticalButtons ? \"modal-buttons-vertical\" : \"\";\n        e = '<div class=\"modal ' + h + \" \" + l + '\"><div class=\"modal-inner\">' + (i + j + k) + '</div><div class=\"modal-buttons ' + m + '\">' + f + \"</div></div>\", c.innerHTML = e;\n        var n = a(c).children();\n        return a(d.modalContainer).append(n[0]), n.find(\".modal-button\").each(function (c, d) {\n            a(d).on(\"click\", function (d) {\n                b.buttons[c].close !== !1 && a.closeModal(n), b.buttons[c].onClick && b.buttons[c].onClick(n, d), b.onClick && b.onClick(n, c);\n            });\n        }), a.openModal(n), n[0];\n    }, a.alert = function (b, c, e) {\n        return \"function\" == typeof c && (e = arguments[1], c = void 0), a.modal({\n            text: b || \"\",\n            title: \"undefined\" == typeof c ? d.modalTitle : c,\n            buttons: [{ text: d.modalButtonOk, bold: !0, onClick: e }]\n        });\n    }, a.confirm = function (b, c, e, f) {\n        return \"function\" == typeof c && (f = arguments[2], e = arguments[1], c = void 0), a.modal({\n            text: b || \"\",\n            title: \"undefined\" == typeof c ? d.modalTitle : c,\n            buttons: [{ text: d.modalButtonCancel, onClick: f }, { text: d.modalButtonOk, bold: !0, onClick: e }]\n        });\n    }, a.prompt = function (b, c, e, f) {\n        return \"function\" == typeof c && (f = arguments[2], e = arguments[1], c = void 0), a.modal({\n            text: b || \"\",\n            title: \"undefined\" == typeof c ? d.modalTitle : c,\n            afterText: '<input type=\"text\" class=\"modal-text-input\">',\n            buttons: [{ text: d.modalButtonCancel }, { text: d.modalButtonOk, bold: !0 }],\n            onClick: function onClick(b, c) {\n                0 === c && f && f(a(b).find(\".modal-text-input\").val()), 1 === c && e && e(a(b).find(\".modal-text-input\").val());\n            }\n        });\n    }, a.modalLogin = function (b, c, e, f) {\n        return \"function\" == typeof c && (f = arguments[2], e = arguments[1], c = void 0), a.modal({\n            text: b || \"\",\n            title: \"undefined\" == typeof c ? d.modalTitle : c,\n            afterText: '<input type=\"text\" name=\"modal-username\" placeholder=\"' + d.modalUsernamePlaceholder + '\" class=\"modal-text-input modal-text-input-double\"><input type=\"password\" name=\"modal-password\" placeholder=\"' + d.modalPasswordPlaceholder + '\" class=\"modal-text-input modal-text-input-double\">',\n            buttons: [{ text: d.modalButtonCancel }, { text: d.modalButtonOk, bold: !0 }],\n            onClick: function onClick(b, c) {\n                var d = a(b).find('.modal-text-input[name=\"modal-username\"]').val(),\n                    g = a(b).find('.modal-text-input[name=\"modal-password\"]').val();\n                0 === c && f && f(d, g), 1 === c && e && e(d, g);\n            }\n        });\n    }, a.modalPassword = function (b, c, e, f) {\n        return \"function\" == typeof c && (f = arguments[2], e = arguments[1], c = void 0), a.modal({\n            text: b || \"\",\n            title: \"undefined\" == typeof c ? d.modalTitle : c,\n            afterText: '<input type=\"password\" name=\"modal-password\" placeholder=\"' + d.modalPasswordPlaceholder + '\" class=\"modal-text-input\">',\n            buttons: [{ text: d.modalButtonCancel }, { text: d.modalButtonOk, bold: !0 }],\n            onClick: function onClick(b, c) {\n                var d = a(b).find('.modal-text-input[name=\"modal-password\"]').val();\n                0 === c && f && f(d), 1 === c && e && e(d);\n            }\n        });\n    }, a.showPreloader = function (b) {\n        return a.hidePreloader(), a.showPreloader.preloaderModal = a.modal({\n            title: b || d.modalPreloaderTitle,\n            text: '<div class=\"preloader\"></div>'\n        }), a.showPreloader.preloaderModal;\n    }, a.hidePreloader = function () {\n        a.showPreloader.preloaderModal && a.closeModal(a.showPreloader.preloaderModal);\n    }, a.showIndicator = function () {\n        a(\".preloader-indicator-modal\")[0] || a(d.modalContainer).append('<div class=\"preloader-indicator-overlay\"></div><div class=\"preloader-indicator-modal\"><span class=\"preloader preloader-white\"></span></div>');\n    }, a.hideIndicator = function () {\n        a(\".preloader-indicator-overlay, .preloader-indicator-modal\").remove();\n    }, a.actions = function (b) {\n        var e, f, g;\n        b = b || [], b.length > 0 && !a.isArray(b[0]) && (b = [b]);\n        for (var h, i = \"\", j = 0; j < b.length; j++) {\n            for (var k = 0; k < b[j].length; k++) {\n                0 === k && (i += '<div class=\"actions-modal-group\">');\n                var l = b[j][k],\n                    m = l.label ? \"actions-modal-label\" : \"actions-modal-button\";\n                l.bold && (m += \" actions-modal-button-bold\"), l.color && (m += \" color-\" + l.color), l.bg && (m += \" bg-\" + l.bg), l.disabled && (m += \" disabled\"), i += '<span class=\"' + m + '\">' + l.text + \"</span>\", k === b[j].length - 1 && (i += \"</div>\");\n            }\n        }h = '<div class=\"actions-modal\">' + i + \"</div>\", c.innerHTML = h, e = a(c).children(), a(d.modalContainer).append(e[0]), f = \".actions-modal-group\", g = \".actions-modal-button\";\n        var n = e.find(f);\n        return n.each(function (c, d) {\n            var f = c;\n            a(d).children().each(function (c, d) {\n                var h,\n                    i = c,\n                    j = b[f][i];\n                a(d).is(g) && (h = a(d)), h && h.on(\"click\", function (b) {\n                    j.close !== !1 && a.closeModal(e), j.onClick && j.onClick(e, b);\n                });\n            });\n        }), a.openModal(e), e[0];\n    }, a.popup = function (b, c) {\n        if (\"undefined\" == typeof c && (c = !0), \"string\" == typeof b && b.indexOf(\"<\") >= 0) {\n            var e = document.createElement(\"div\");\n            if (e.innerHTML = b.trim(), !(e.childNodes.length > 0)) return !1;\n            b = e.childNodes[0], c && b.classList.add(\"remove-on-close\"), a(d.modalContainer).append(b);\n        }\n        return b = a(b), 0 === b.length ? !1 : (b.show(), b.find(\".content\").scroller(\"refresh\"), b.find(\".\" + d.viewClass).length > 0 && a.sizeNavbars(b.find(\".\" + d.viewClass)[0]), a.openModal(b), b[0]);\n    }, a.pickerModal = function (b, c) {\n        if (\"undefined\" == typeof c && (c = !0), \"string\" == typeof b && b.indexOf(\"<\") >= 0) {\n            if (b = a(b), !(b.length > 0)) return !1;\n            c && b.addClass(\"remove-on-close\"), a(d.modalContainer).append(b[0]);\n        }\n        return b = a(b), 0 === b.length ? !1 : (b.show(), a.openModal(b), b[0]);\n    }, a.loginScreen = function (b) {\n        return b || (b = \".login-screen\"), b = a(b), 0 === b.length ? !1 : (b.show(), b.find(\".\" + d.viewClass).length > 0 && a.sizeNavbars(b.find(\".\" + d.viewClass)[0]), a.openModal(b), b[0]);\n    }, a.toast = function (b, c, d) {\n        var e = a('<div class=\"modal toast ' + (d || \"\") + '\">' + b + \"</div>\").appendTo(document.body);\n        a.openModal(e, function () {\n            setTimeout(function () {\n                a.closeModal(e);\n            }, c || 2e3);\n        });\n    }, a.openModal = function (b, c) {\n        b = a(b);\n        var e = b.hasClass(\"modal\"),\n            f = !b.hasClass(\"toast\");\n        if (a(\".modal.modal-in:not(.modal-out)\").length && d.modalStack && e && f) return void a.modalStack.push(function () {\n            a.openModal(b, c);\n        });\n        var g = b.hasClass(\"popup\"),\n            h = b.hasClass(\"login-screen\"),\n            i = b.hasClass(\"picker-modal\"),\n            j = b.hasClass(\"toast\");\n        e && (b.show(), b.css({ marginTop: -Math.round(b.outerHeight() / 2) + \"px\" })), j && b.css({ marginLeft: -Math.round(b.outerWidth() / 2 / 1.185) + \"px\" });\n        var k;\n        h || i || j || (0 !== a(\".modal-overlay\").length || g || a(d.modalContainer).append('<div class=\"modal-overlay\"></div>'), 0 === a(\".popup-overlay\").length && g && a(d.modalContainer).append('<div class=\"popup-overlay\"></div>'), k = a(g ? \".popup-overlay\" : \".modal-overlay\"));\n        b[0].clientLeft;\n        return b.trigger(\"open\"), i && a(d.modalContainer).addClass(\"with-picker-modal\"), h || i || j || k.addClass(\"modal-overlay-visible\"), b.removeClass(\"modal-out\").addClass(\"modal-in\").transitionEnd(function (a) {\n            b.hasClass(\"modal-out\") ? b.trigger(\"closed\") : b.trigger(\"opened\");\n        }), \"function\" == typeof c && c.call(this), !0;\n    }, a.closeModal = function (b) {\n        if (b = a(b || \".modal-in\"), \"undefined\" == typeof b || 0 !== b.length) {\n            var c = b.hasClass(\"modal\"),\n                e = b.hasClass(\"popup\"),\n                f = b.hasClass(\"toast\"),\n                g = b.hasClass(\"login-screen\"),\n                h = b.hasClass(\"picker-modal\"),\n                i = b.hasClass(\"remove-on-close\"),\n                j = a(e ? \".popup-overlay\" : \".modal-overlay\");\n            return e ? b.length === a(\".popup.modal-in\").length && j.removeClass(\"modal-overlay-visible\") : h || f || j.removeClass(\"modal-overlay-visible\"), b.trigger(\"close\"), h && (a(d.modalContainer).removeClass(\"with-picker-modal\"), a(d.modalContainer).addClass(\"picker-modal-closing\")), b.removeClass(\"modal-in\").addClass(\"modal-out\").transitionEnd(function (c) {\n                b.hasClass(\"modal-out\") ? b.trigger(\"closed\") : b.trigger(\"opened\"), h && a(d.modalContainer).removeClass(\"picker-modal-closing\"), e || g || h ? (b.removeClass(\"modal-out\").hide(), i && b.length > 0 && b.remove()) : b.remove();\n            }), c && d.modalStack && a.modalStackClearQueue(), !0;\n        }\n    }, a(document).on(\"click\", \" .modal-overlay, .popup-overlay, .close-popup, .open-popup, .close-picker\", b);\n    var d = a.modal.prototype.defaults = {\n        modalStack: !0,\n        modalButtonOk: \"确定\",\n        modalButtonCancel: \"取消\",\n        modalPreloaderTitle: \"加载中\",\n        modalContainer: document.body ? document.body : \"body\"\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = !1,\n        c = function c(_c) {\n        function d(a) {\n            a = new Date(a);\n            var b = a.getFullYear(),\n                c = a.getMonth(),\n                d = c + 1,\n                e = a.getDate(),\n                f = a.getDay();\n            return h.params.dateFormat.replace(/yyyy/g, b).replace(/yy/g, (b + \"\").substring(2)).replace(/mm/g, 10 > d ? \"0\" + d : d).replace(/m/g, d).replace(/MM/g, h.params.monthNames[c]).replace(/M/g, h.params.monthNamesShort[c]).replace(/dd/g, 10 > e ? \"0\" + e : e).replace(/d/g, e).replace(/DD/g, h.params.dayNames[f]).replace(/D/g, h.params.dayNamesShort[f]);\n        }\n\n        function e(b) {\n            if (b.preventDefault(), a.device.isWeixin && a.device.android && h.params.inputReadOnly && (this.focus(), this.blur()), !h.opened && (h.open(), h.params.scrollToInput)) {\n                var c = h.input.parents(\".content\");\n                if (0 === c.length) return;\n                var d,\n                    e = parseInt(c.css(\"padding-top\"), 10),\n                    f = parseInt(c.css(\"padding-bottom\"), 10),\n                    g = c[0].offsetHeight - e - h.container.height(),\n                    i = c[0].scrollHeight - e - h.container.height(),\n                    j = h.input.offset().top - e + h.input[0].offsetHeight;\n                if (j > g) {\n                    var k = c.scrollTop() + j - g;\n                    k + g > i && (d = k + g - i + f, g === i && (d = h.container.height()), c.css({ \"padding-bottom\": d + \"px\" })), c.scrollTop(k, 300);\n                }\n            }\n        }\n\n        function f(b) {\n            h.input && h.input.length > 0 ? b.target !== h.input[0] && 0 === a(b.target).parents(\".picker-modal\").length && h.close() : 0 === a(b.target).parents(\".picker-modal\").length && h.close();\n        }\n\n        function g() {\n            h.opened = !1, h.input && h.input.length > 0 && h.input.parents(\".content\").css({ \"padding-bottom\": \"\" }), h.params.onClose && h.params.onClose(h), h.destroyCalendarEvents();\n        }\n\n        var h = this,\n            i = {\n            monthNames: [\"一月\", \"二月\", \"三月\", \"四月\", \"五月\", \"六月\", \"七月\", \"八月\", \"九月\", \"十月\", \"十一月\", \"十二月\"],\n            monthNamesShort: [\"一月\", \"二月\", \"三月\", \"四月\", \"五月\", \"六月\", \"七月\", \"八月\", \"九月\", \"十月\", \"十一月\", \"十二月\"],\n            dayNames: [\"周日\", \"周一\", \"周二\", \"周三\", \"周四\", \"周五\", \"周六\"],\n            dayNamesShort: [\"周日\", \"周一\", \"周二\", \"周三\", \"周四\", \"周五\", \"周六\"],\n            firstDay: 1,\n            weekendDays: [0, 6],\n            multiple: !1,\n            dateFormat: \"yyyy-mm-dd\",\n            direction: \"horizontal\",\n            minDate: null,\n            maxDate: null,\n            touchMove: !0,\n            animate: !0,\n            closeOnSelect: !0,\n            monthPicker: !0,\n            monthPickerTemplate: '<div class=\"picker-calendar-month-picker\"><a href=\"#\" class=\"link icon-only picker-calendar-prev-month\"><i class=\"icon icon-prev\"></i></a><div class=\"current-month-value\"></div><a href=\"#\" class=\"link icon-only picker-calendar-next-month\"><i class=\"icon icon-next\"></i></a></div>',\n            yearPicker: !0,\n            yearPickerTemplate: '<div class=\"picker-calendar-year-picker\"><a href=\"#\" class=\"link icon-only picker-calendar-prev-year\"><i class=\"icon icon-prev\"></i></a><span class=\"current-year-value\"></span><a href=\"#\" class=\"link icon-only picker-calendar-next-year\"><i class=\"icon icon-next\"></i></a></div>',\n            weekHeader: !0,\n            scrollToInput: !0,\n            inputReadOnly: !0,\n            toolbar: !0,\n            toolbarCloseText: \"Done\",\n            toolbarTemplate: '<div class=\"toolbar\"><div class=\"toolbar-inner\">{{monthPicker}}{{yearPicker}}</div></div>'\n        };\n        _c = _c || {};\n        for (var j in i) {\n            \"undefined\" == typeof _c[j] && (_c[j] = i[j]);\n        }h.params = _c, h.initialized = !1, h.inline = !!h.params.container, h.isH = \"horizontal\" === h.params.direction;\n        var k = h.isH && b ? -1 : 1;\n        return h.animating = !1, h.addValue = function (a) {\n            if (h.params.multiple) {\n                h.value || (h.value = []);\n                for (var b, c = 0; c < h.value.length; c++) {\n                    new Date(a).getTime() === new Date(h.value[c]).getTime() && (b = c);\n                }\"undefined\" == typeof b ? h.value.push(a) : h.value.splice(b, 1), h.updateValue();\n            } else h.value = [a], h.updateValue();\n        }, h.setValue = function (a) {\n            h.value = a, h.updateValue();\n        }, h.updateValue = function () {\n            h.wrapper.find(\".picker-calendar-day-selected\").removeClass(\"picker-calendar-day-selected\");\n            var b, c;\n            for (b = 0; b < h.value.length; b++) {\n                var e = new Date(h.value[b]);\n                h.wrapper.find('.picker-calendar-day[data-date=\"' + e.getFullYear() + \"-\" + e.getMonth() + \"-\" + e.getDate() + '\"]').addClass(\"picker-calendar-day-selected\");\n            }\n            if (h.params.onChange && h.params.onChange(h, h.value, h.value.map(d)), h.input && h.input.length > 0) {\n                if (h.params.formatValue) c = h.params.formatValue(h, h.value);else {\n                    for (c = [], b = 0; b < h.value.length; b++) {\n                        c.push(d(h.value[b]));\n                    }c = c.join(\", \");\n                }\n                a(h.input).val(c), a(h.input).trigger(\"change\");\n            }\n        }, h.initCalendarEvents = function () {\n            function c(a) {\n                o || n || (n = !0, p = s = \"touchstart\" === a.type ? a.targetTouches[0].pageX : a.pageX, q = s = \"touchstart\" === a.type ? a.targetTouches[0].pageY : a.pageY, t = new Date().getTime(), z = 0, C = !0, B = void 0, v = w = h.monthsTranslate);\n            }\n\n            function d(a) {\n                if (n) {\n                    if (r = \"touchmove\" === a.type ? a.targetTouches[0].pageX : a.pageX, s = \"touchmove\" === a.type ? a.targetTouches[0].pageY : a.pageY, \"undefined\" == typeof B && (B = !!(B || Math.abs(s - q) > Math.abs(r - p))), h.isH && B) return void (n = !1);\n                    if (a.preventDefault(), h.animating) return void (n = !1);\n                    C = !1, o || (o = !0, x = h.wrapper[0].offsetWidth, y = h.wrapper[0].offsetHeight, h.wrapper.transition(0)), a.preventDefault(), A = h.isH ? r - p : s - q, z = A / (h.isH ? x : y), w = 100 * (h.monthsTranslate * k + z), h.wrapper.transform(\"translate3d(\" + (h.isH ? w : 0) + \"%, \" + (h.isH ? 0 : w) + \"%, 0)\");\n                }\n            }\n\n            function e(a) {\n                return n && o ? (n = o = !1, u = new Date().getTime(), 300 > u - t ? Math.abs(A) < 10 ? h.resetMonth() : A >= 10 ? b ? h.nextMonth() : h.prevMonth() : b ? h.prevMonth() : h.nextMonth() : -.5 >= z ? b ? h.prevMonth() : h.nextMonth() : z >= .5 ? b ? h.nextMonth() : h.prevMonth() : h.resetMonth(), void setTimeout(function () {\n                    C = !0;\n                }, 100)) : void (n = o = !1);\n            }\n\n            function f(b) {\n                if (C) {\n                    var c = a(b.target).parents(\".picker-calendar-day\");\n                    if (0 === c.length && a(b.target).hasClass(\"picker-calendar-day\") && (c = a(b.target)), 0 !== c.length && (!c.hasClass(\"picker-calendar-day-selected\") || h.params.multiple) && !c.hasClass(\"picker-calendar-day-disabled\")) {\n                        c.hasClass(\"picker-calendar-day-next\") && h.nextMonth(), c.hasClass(\"picker-calendar-day-prev\") && h.prevMonth();\n                        var d = c.attr(\"data-year\"),\n                            e = c.attr(\"data-month\"),\n                            f = c.attr(\"data-day\");\n                        h.params.onDayClick && h.params.onDayClick(h, c[0], d, e, f), h.addValue(new Date(d, e, f).getTime()), h.params.closeOnSelect && h.close();\n                    }\n                }\n            }\n\n            function g(a) {\n                o || n || (n = !0, p = s = \"touchstart\" === a.type ? a.targetTouches[0].pageX : a.pageX, q = s = \"touchstart\" === a.type ? a.targetTouches[0].pageY : a.pageY, t = new Date().getTime(), z = 0, C = !0, B = void 0, v = w = h.yearsTranslate);\n            }\n\n            function i(a) {\n                if (n) {\n                    if (r = \"touchmove\" === a.type ? a.targetTouches[0].pageX : a.pageX, s = \"touchmove\" === a.type ? a.targetTouches[0].pageY : a.pageY, \"undefined\" == typeof B && (B = !!(B || Math.abs(s - q) > Math.abs(r - p))), h.isH && B) return void (n = !1);\n                    if (a.preventDefault(), h.animating) return void (n = !1);\n                    C = !1, o || (o = !0, x = h.yearsPickerWrapper[0].offsetWidth, y = h.yearsPickerWrapper[0].offsetHeight, h.yearsPickerWrapper.transition(0)), a.preventDefault(), A = h.isH ? r - p : s - q, z = A / (h.isH ? x : y), w = 100 * (h.yearsTranslate * k + z), h.yearsPickerWrapper.transform(\"translate3d(\" + (h.isH ? w : 0) + \"%, \" + (h.isH ? 0 : w) + \"%, 0)\");\n                }\n            }\n\n            function j(a) {\n                return n && o ? (n = o = !1, u = new Date().getTime(), 300 > u - t ? Math.abs(A) < 10 ? h.resetYearsGroup() : A >= 10 ? b ? h.nextYearsGroup() : h.prevYearsGroup() : b ? h.prevYearsGroup() : h.nextYearsGroup() : -.5 >= z ? b ? h.prevYearsGroup() : h.nextYearsGroup() : z >= .5 ? b ? h.nextYearsGroup() : h.prevYearsGroup() : h.resetYearsGroup(), void setTimeout(function () {\n                    C = !0;\n                }, 100)) : void (n = o = !1);\n            }\n\n            function l() {\n                var b = (a(this).text(), h.container.find(\".picker-calendar-years-picker\"));\n                b.show().transform(\"translate3d(0, 0, 0)\"), h.updateSelectedInPickers(), b.on(\"click\", \".picker-calendar-year-unit\", h.pickYear);\n            }\n\n            function m() {\n                var a = h.container.find(\".picker-calendar-months-picker\");\n                a.show().transform(\"translate3d(0, 0, 0)\"), h.updateSelectedInPickers(), a.on(\"click\", \".picker-calendar-month-unit\", h.pickMonth);\n            }\n\n            var n,\n                o,\n                p,\n                q,\n                r,\n                s,\n                t,\n                u,\n                v,\n                w,\n                x,\n                y,\n                z,\n                A,\n                B,\n                C = !0;\n            h.container.find(\".picker-calendar-prev-month\").on(\"click\", h.prevMonth), h.container.find(\".picker-calendar-next-month\").on(\"click\", h.nextMonth), h.container.find(\".picker-calendar-prev-year\").on(\"click\", h.prevYear), h.container.find(\".picker-calendar-next-year\").on(\"click\", h.nextYear), h.container.find(\".current-year-value\").on(\"click\", l), h.container.find(\".current-month-value\").on(\"click\", m), h.wrapper.on(\"click\", f), h.params.touchMove && (h.yearsPickerWrapper.on(a.touchEvents.start, g), h.yearsPickerWrapper.on(a.touchEvents.move, i), h.yearsPickerWrapper.on(a.touchEvents.end, j), h.wrapper.on(a.touchEvents.start, c), h.wrapper.on(a.touchEvents.move, d), h.wrapper.on(a.touchEvents.end, e)), h.container[0].f7DestroyCalendarEvents = function () {\n                h.container.find(\".picker-calendar-prev-month\").off(\"click\", h.prevMonth), h.container.find(\".picker-calendar-next-month\").off(\"click\", h.nextMonth), h.container.find(\".picker-calendar-prev-year\").off(\"click\", h.prevYear), h.container.find(\".picker-calendar-next-year\").off(\"click\", h.nextYear), h.wrapper.off(\"click\", f), h.params.touchMove && (h.wrapper.off(a.touchEvents.start, c), h.wrapper.off(a.touchEvents.move, d), h.wrapper.off(a.touchEvents.end, e));\n            };\n        }, h.destroyCalendarEvents = function (a) {\n            \"f7DestroyCalendarEvents\" in h.container[0] && h.container[0].f7DestroyCalendarEvents();\n        }, h.yearsGroupHTML = function (a, b) {\n            a = new Date(a);\n            var c = a.getFullYear(),\n                d = new Date().getFullYear(),\n                e = 25,\n                f = c - Math.floor(e / 2),\n                g = \"\";\n            \"next\" === b && (f += e), \"prev\" === b && (f -= e);\n            for (var h = 0; 5 > h; h += 1) {\n                var i = \"\";\n                i += '<div class=\"picker-calendar-row\">';\n                for (var j = 0; 5 > j; j += 1) {\n                    i += f === d ? '<div class=\"picker-calendar-year-unit current-calendar-year-unit\" data-year=\"' + f + '\"><span>' + f + \"</span></div>\" : f === c ? '<div class=\"picker-calendar-year-unit picker-calendar-year-unit-selected\" data-year=\"' + f + '\"><span>' + f + \"</span></div>\" : '<div class=\"picker-calendar-year-unit\" data-year=\"' + f + '\"><span>' + f + \"</span></div>\", f += 1;\n                }i += \"</div>\", g += i;\n            }\n            return g = '<div class=\"picker-calendar-years-group\">' + g + \"</div>\";\n        }, h.pickYear = function () {\n            var b = a(this).text(),\n                c = h.wrapper.find(\".picker-calendar-month-current\").attr(\"data-year\");\n            h.yearsPickerWrapper.find(\".picker-calendar-year-unit\").removeClass(\"picker-calendar-year-unit-selected\"), a(this).addClass(\"picker-calendar-year-unit-selected\"), c !== b ? (h.setYearMonth(b), h.container.find(\".picker-calendar-years-picker\").hide().transform(\"translate3d(0, 100%, 0)\")) : h.container.find(\".picker-calendar-years-picker\").transform(\"translate3d(0, 100%, 0)\");\n        }, h.onYearsChangeEnd = function (a) {\n            h.animating = !1;\n            var b,\n                c,\n                d,\n                e = h.yearsPickerWrapper.children(\".picker-calendar-years-next\").find(\".picker-calendar-year-unit\").length;\n            if (\"next\" === a) {\n                var d = parseInt(h.yearsPickerWrapper.children(\".picker-calendar-years-next\").find(\".picker-calendar-year-unit\").eq(Math.floor(e / 2)).text());\n                b = h.yearsGroupHTML(new Date(d, h.currentMonth), \"next\"), h.yearsPickerWrapper.append(b), h.yearsPickerWrapper.children().first().remove(), h.yearsGroups = h.container.find(\".picker-calendar-years-group\");\n            }\n            if (\"prev\" === a) {\n                var d = parseInt(h.yearsPickerWrapper.children(\".picker-calendar-years-prev\").find(\".picker-calendar-year-unit\").eq(Math.floor(e / 2)).text());\n                c = h.yearsGroupHTML(new Date(d, h.currentMonth), \"prev\"), h.yearsPickerWrapper.prepend(c), h.yearsPickerWrapper.children().last().remove(), h.yearsGroups = h.container.find(\".picker-calendar-years-group\");\n            }\n            h.setYearsTranslate(h.yearsTranslate);\n        }, h.monthsGroupHTML = function (a) {\n            a = new Date(a);\n            for (var b = a.getMonth() + 1, c = new Date().getMonth() + 1, d = 1, e = \"\", f = 0; 3 > f; f += 1) {\n                var g = \"\";\n                g += '<div class=\"picker-calendar-row\">';\n                for (var i = 0; 4 > i; i += 1) {\n                    g += d === c ? '<div class=\"picker-calendar-month-unit current-calendar-month-unit\" data-month=\"' + (d - 1) + '\"><span>' + h.params.monthNames[d - 1] + \"</span></div>\" : d === b ? '<div class=\"picker-calendar-month-unit picker-calendar-month-selected\" data-month=\"' + (d - 1) + '\"><span>' + h.params.monthNames[d - 1] + \"</span></div>\" : '<div class=\"picker-calendar-month-unit\" data-month=\"' + (d - 1) + '\"><span>' + h.params.monthNames[d - 1] + \"</span></div>\", d += 1;\n                }g += \"</div>\", e += g;\n            }\n            return e = '<div class=\"picker-calendar-months-group\">' + e + \"</div>\";\n        }, h.pickMonth = function () {\n            var b = a(this).attr(\"data-month\"),\n                c = h.wrapper.find(\".picker-calendar-month-current\").attr(\"data-year\"),\n                d = h.wrapper.find(\".picker-calendar-month-current\").attr(\"data-month\");\n            h.monthsPickerWrapper.find(\".picker-calendar-month-unit\").removeClass(\"picker-calendar-month-unit-selected\"), a(this).addClass(\"picker-calendar-month-unit-selected\"), d !== b ? (h.setYearMonth(c, b), h.container.find(\".picker-calendar-months-picker\").hide().transform(\"translate3d(0, 100%, 0)\")) : h.container.find(\".picker-calendar-months-picker\").transform(\"translate3d(0, 100%, 0)\");\n        }, h.daysInMonth = function (a) {\n            var b = new Date(a);\n            return new Date(b.getFullYear(), b.getMonth() + 1, 0).getDate();\n        }, h.monthHTML = function (a, b) {\n            a = new Date(a);\n            var c = a.getFullYear(),\n                d = a.getMonth();\n            a.getDate();\n            \"next\" === b && (a = 11 === d ? new Date(c + 1, 0) : new Date(c, d + 1, 1)), \"prev\" === b && (a = 0 === d ? new Date(c - 1, 11) : new Date(c, d - 1, 1)), \"next\" !== b && \"prev\" !== b || (d = a.getMonth(), c = a.getFullYear());\n            var e = h.daysInMonth(new Date(a.getFullYear(), a.getMonth()).getTime() - 864e6),\n                f = h.daysInMonth(a),\n                g = new Date(a.getFullYear(), a.getMonth()).getDay();\n            0 === g && (g = 7);\n            var i,\n                j,\n                k,\n                l = [],\n                m = 6,\n                n = 7,\n                o = \"\",\n                p = 0 + (h.params.firstDay - 1),\n                q = new Date().setHours(0, 0, 0, 0),\n                r = h.params.minDate ? new Date(h.params.minDate).getTime() : null,\n                s = h.params.maxDate ? new Date(h.params.maxDate).getTime() : null;\n            if (h.value && h.value.length) for (j = 0; j < h.value.length; j++) {\n                l.push(new Date(h.value[j]).setHours(0, 0, 0, 0));\n            }for (j = 1; m >= j; j++) {\n                var t = \"\";\n                for (k = 1; n >= k; k++) {\n                    var u = k;\n                    p++;\n                    var v = p - g,\n                        w = \"\";\n                    0 > v ? (v = e + v + 1, w += \" picker-calendar-day-prev\", i = new Date(0 > d - 1 ? c - 1 : c, 0 > d - 1 ? 11 : d - 1, v).getTime()) : (v += 1, v > f ? (v -= f, w += \" picker-calendar-day-next\", i = new Date(d + 1 > 11 ? c + 1 : c, d + 1 > 11 ? 0 : d + 1, v).getTime()) : i = new Date(c, d, v).getTime()), i === q && (w += \" picker-calendar-day-today\"), l.indexOf(i) >= 0 && (w += \" picker-calendar-day-selected\"), h.params.weekendDays.indexOf(u - 1) >= 0 && (w += \" picker-calendar-day-weekend\"), (r && r > i || s && i > s) && (w += \" picker-calendar-day-disabled\"), i = new Date(i);\n                    var x = i.getFullYear(),\n                        y = i.getMonth();\n                    t += '<div data-year=\"' + x + '\" data-month=\"' + y + '\" data-day=\"' + v + '\" class=\"picker-calendar-day' + w + '\" data-date=\"' + (x + \"-\" + y + \"-\" + v) + '\"><span>' + v + \"</span></div>\";\n                }\n                o += '<div class=\"picker-calendar-row\">' + t + \"</div>\";\n            }\n            return o = '<div class=\"picker-calendar-month\" data-year=\"' + c + '\" data-month=\"' + d + '\">' + o + \"</div>\";\n        }, h.animating = !1, h.updateCurrentMonthYear = function (a) {\n            \"undefined\" == typeof a ? (h.currentMonth = parseInt(h.months.eq(1).attr(\"data-month\"), 10), h.currentYear = parseInt(h.months.eq(1).attr(\"data-year\"), 10)) : (h.currentMonth = parseInt(h.months.eq(\"next\" === a ? h.months.length - 1 : 0).attr(\"data-month\"), 10), h.currentYear = parseInt(h.months.eq(\"next\" === a ? h.months.length - 1 : 0).attr(\"data-year\"), 10)), h.container.find(\".current-month-value\").text(h.params.monthNames[h.currentMonth]), h.container.find(\".current-year-value\").text(h.currentYear);\n        }, h.onMonthChangeStart = function (a) {\n            h.updateCurrentMonthYear(a), h.months.removeClass(\"picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next\");\n            var b = \"next\" === a ? h.months.length - 1 : 0;\n            h.months.eq(b).addClass(\"picker-calendar-month-current\"), h.months.eq(\"next\" === a ? b - 1 : b + 1).addClass(\"next\" === a ? \"picker-calendar-month-prev\" : \"picker-calendar-month-next\"), h.params.onMonthYearChangeStart && h.params.onMonthYearChangeStart(h, h.currentYear, h.currentMonth);\n        }, h.onMonthChangeEnd = function (a, b) {\n            h.animating = !1;\n            var c, d, e;\n            h.wrapper.find(\".picker-calendar-month:not(.picker-calendar-month-prev):not(.picker-calendar-month-current):not(.picker-calendar-month-next)\").remove(), \"undefined\" == typeof a && (a = \"next\", b = !0), b ? (h.wrapper.find(\".picker-calendar-month-next, .picker-calendar-month-prev\").remove(), d = h.monthHTML(new Date(h.currentYear, h.currentMonth), \"prev\"), c = h.monthHTML(new Date(h.currentYear, h.currentMonth), \"next\")) : e = h.monthHTML(new Date(h.currentYear, h.currentMonth), a), (\"next\" === a || b) && h.wrapper.append(e || c), (\"prev\" === a || b) && h.wrapper.prepend(e || d), h.months = h.wrapper.find(\".picker-calendar-month\"), h.setMonthsTranslate(h.monthsTranslate), h.params.onMonthAdd && h.params.onMonthAdd(h, \"next\" === a ? h.months.eq(h.months.length - 1)[0] : h.months.eq(0)[0]), h.params.onMonthYearChangeEnd && h.params.onMonthYearChangeEnd(h, h.currentYear, h.currentMonth), h.updateSelectedInPickers();\n        }, h.updateSelectedInPickers = function () {\n            var a = parseInt(h.wrapper.find(\".picker-calendar-month-current\").attr(\"data-year\"), 10),\n                b = new Date().getFullYear(),\n                c = parseInt(h.wrapper.find(\".picker-calendar-month-current\").attr(\"data-month\"), 10),\n                d = new Date().getMonth(),\n                e = parseInt(h.yearsPickerWrapper.find(\".picker-calendar-year-unit-selected\").attr(\"data-year\"), 10),\n                f = parseInt(h.monthsPickerWrapper.find(\".picker-calendar-month-unit-selected\").attr(\"data-month\"), 10);\n            e !== a && (h.yearsPickerWrapper.find(\".picker-calendar-year-unit\").removeClass(\"picker-calendar-year-unit-selected\"), h.yearsPickerWrapper.find('.picker-calendar-year-unit[data-year=\"' + a + '\"]').addClass(\"picker-calendar-year-unit-selected\")), f !== c && (h.monthsPickerWrapper.find(\".picker-calendar-month-unit\").removeClass(\"picker-calendar-month-unit-selected\"), h.monthsPickerWrapper.find('.picker-calendar-month-unit[data-month=\"' + c + '\"]').addClass(\"picker-calendar-month-unit-selected\")), b !== a ? h.monthsPickerWrapper.find(\".picker-calendar-month-unit\").removeClass(\"current-calendar-month-unit\") : h.monthsPickerWrapper.find('.picker-calendar-month-unit[data-month=\"' + d + '\"]').addClass(\"current-calendar-month-unit\");\n        }, h.setYearsTranslate = function (a) {\n            a = a || h.yearsTranslate || 0, \"undefined\" == typeof h.yearsTranslate && (h.yearsTranslate = a), h.yearsGroups.removeClass(\"picker-calendar-years-current picker-calendar-years-prev picker-calendar-years-next\");\n            var b = 100 * -(a + 1) * k,\n                c = 100 * -a * k,\n                d = 100 * -(a - 1) * k;\n            h.yearsGroups.eq(0).transform(\"translate3d(\" + (h.isH ? b : 0) + \"%, \" + (h.isH ? 0 : b) + \"%, 0)\").addClass(\"picker-calendar-years-prev\"), h.yearsGroups.eq(1).transform(\"translate3d(\" + (h.isH ? c : 0) + \"%, \" + (h.isH ? 0 : c) + \"%, 0)\").addClass(\"picker-calendar-years-current\"), h.yearsGroups.eq(2).transform(\"translate3d(\" + (h.isH ? d : 0) + \"%, \" + (h.isH ? 0 : d) + \"%, 0)\").addClass(\"picker-calendar-years-next\");\n        }, h.nextYearsGroup = function (a) {\n            \"undefined\" != typeof a && \"object\" != (typeof a === \"undefined\" ? \"undefined\" : _typeof(a)) || (a = \"\", h.params.animate || (a = 0));\n            var b = !h.animating;\n            h.yearsTranslate--, h.animating = !0;\n            var c = 100 * h.yearsTranslate * k;\n            h.yearsPickerWrapper.transition(a).transform(\"translate3d(\" + (h.isH ? c : 0) + \"%, \" + (h.isH ? 0 : c) + \"%, 0)\"), b && h.yearsPickerWrapper.transitionEnd(function () {\n                h.onYearsChangeEnd(\"next\");\n            }), h.params.animate || h.onYearsChangeEnd(\"next\");\n        }, h.prevYearsGroup = function (a) {\n            \"undefined\" != typeof a && \"object\" != (typeof a === \"undefined\" ? \"undefined\" : _typeof(a)) || (a = \"\", h.params.animate || (a = 0));\n            var b = !h.animating;\n            h.yearsTranslate++, h.animating = !0;\n            var c = 100 * h.yearsTranslate * k;\n            h.yearsPickerWrapper.transition(a).transform(\"translate3d(\" + (h.isH ? c : 0) + \"%, \" + (h.isH ? 0 : c) + \"%, 0)\"), b && h.yearsPickerWrapper.transitionEnd(function () {\n                h.onYearsChangeEnd(\"prev\");\n            }), h.params.animate || h.onYearsChangeEnd(\"prev\");\n        }, h.resetYearsGroup = function (a) {\n            \"undefined\" == typeof a && (a = \"\");\n            var b = 100 * h.yearsTranslate * k;\n            h.yearsPickerWrapper.transition(a).transform(\"translate3d(\" + (h.isH ? b : 0) + \"%, \" + (h.isH ? 0 : b) + \"%, 0)\");\n        }, h.setMonthsTranslate = function (a) {\n            a = a || h.monthsTranslate || 0, \"undefined\" == typeof h.monthsTranslate && (h.monthsTranslate = a), h.months.removeClass(\"picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next\");\n            var b = 100 * -(a + 1) * k,\n                c = 100 * -a * k,\n                d = 100 * -(a - 1) * k;\n            h.months.eq(0).transform(\"translate3d(\" + (h.isH ? b : 0) + \"%, \" + (h.isH ? 0 : b) + \"%, 0)\").addClass(\"picker-calendar-month-prev\"), h.months.eq(1).transform(\"translate3d(\" + (h.isH ? c : 0) + \"%, \" + (h.isH ? 0 : c) + \"%, 0)\").addClass(\"picker-calendar-month-current\"), h.months.eq(2).transform(\"translate3d(\" + (h.isH ? d : 0) + \"%, \" + (h.isH ? 0 : d) + \"%, 0)\").addClass(\"picker-calendar-month-next\");\n        }, h.nextMonth = function (b) {\n            \"undefined\" != typeof b && \"object\" != (typeof b === \"undefined\" ? \"undefined\" : _typeof(b)) || (b = \"\", h.params.animate || (b = 0));\n            var c = parseInt(h.months.eq(h.months.length - 1).attr(\"data-month\"), 10),\n                d = parseInt(h.months.eq(h.months.length - 1).attr(\"data-year\"), 10),\n                e = new Date(d, c),\n                f = e.getTime(),\n                g = !h.animating;\n            if (h.params.maxDate && f > new Date(h.params.maxDate).getTime()) return h.resetMonth();\n            if (h.monthsTranslate--, c === h.currentMonth) {\n                var i = 100 * -h.monthsTranslate * k,\n                    j = a(h.monthHTML(f, \"next\")).transform(\"translate3d(\" + (h.isH ? i : 0) + \"%, \" + (h.isH ? 0 : i) + \"%, 0)\").addClass(\"picker-calendar-month-next\");\n                h.wrapper.append(j[0]), h.months = h.wrapper.find(\".picker-calendar-month\"), h.params.onMonthAdd && h.params.onMonthAdd(h, h.months.eq(h.months.length - 1)[0]);\n            }\n            h.animating = !0, h.onMonthChangeStart(\"next\");\n            var l = 100 * h.monthsTranslate * k;\n            h.wrapper.transition(b).transform(\"translate3d(\" + (h.isH ? l : 0) + \"%, \" + (h.isH ? 0 : l) + \"%, 0)\"), g && h.wrapper.transitionEnd(function () {\n                h.onMonthChangeEnd(\"next\");\n            }), h.params.animate || h.onMonthChangeEnd(\"next\");\n        }, h.prevMonth = function (b) {\n            \"undefined\" != typeof b && \"object\" != (typeof b === \"undefined\" ? \"undefined\" : _typeof(b)) || (b = \"\", h.params.animate || (b = 0));\n            var c = parseInt(h.months.eq(0).attr(\"data-month\"), 10),\n                d = parseInt(h.months.eq(0).attr(\"data-year\"), 10),\n                e = new Date(d, c + 1, -1),\n                f = e.getTime(),\n                g = !h.animating;\n            if (h.params.minDate && f < new Date(h.params.minDate).getTime()) return h.resetMonth();\n            if (h.monthsTranslate++, c === h.currentMonth) {\n                var i = 100 * -h.monthsTranslate * k,\n                    j = a(h.monthHTML(f, \"prev\")).transform(\"translate3d(\" + (h.isH ? i : 0) + \"%, \" + (h.isH ? 0 : i) + \"%, 0)\").addClass(\"picker-calendar-month-prev\");\n                h.wrapper.prepend(j[0]), h.months = h.wrapper.find(\".picker-calendar-month\"), h.params.onMonthAdd && h.params.onMonthAdd(h, h.months.eq(0)[0]);\n            }\n            h.animating = !0, h.onMonthChangeStart(\"prev\");\n            var l = 100 * h.monthsTranslate * k;\n            h.wrapper.transition(b).transform(\"translate3d(\" + (h.isH ? l : 0) + \"%, \" + (h.isH ? 0 : l) + \"%, 0)\"), g && h.wrapper.transitionEnd(function () {\n                h.onMonthChangeEnd(\"prev\");\n            }), h.params.animate || h.onMonthChangeEnd(\"prev\");\n        }, h.resetMonth = function (a) {\n            \"undefined\" == typeof a && (a = \"\");\n            var b = 100 * h.monthsTranslate * k;\n            h.wrapper.transition(a).transform(\"translate3d(\" + (h.isH ? b : 0) + \"%, \" + (h.isH ? 0 : b) + \"%, 0)\");\n        }, h.setYearMonth = function (a, b, c) {\n            \"undefined\" == typeof a && (a = h.currentYear), \"undefined\" == typeof b && (b = h.currentMonth), \"undefined\" != typeof c && \"object\" != (typeof c === \"undefined\" ? \"undefined\" : _typeof(c)) || (c = \"\", h.params.animate || (c = 0));\n            var d;\n            if (d = a < h.currentYear ? new Date(a, b + 1, -1).getTime() : new Date(a, b).getTime(), h.params.maxDate && d > new Date(h.params.maxDate).getTime()) return !1;\n            if (h.params.minDate && d < new Date(h.params.minDate).getTime()) return !1;\n            var e = new Date(h.currentYear, h.currentMonth).getTime(),\n                f = d > e ? \"next\" : \"prev\",\n                g = h.monthHTML(new Date(a, b));\n            h.monthsTranslate = h.monthsTranslate || 0;\n            var i,\n                j,\n                l = h.monthsTranslate,\n                m = !h.animating;\n            d > e ? (h.monthsTranslate--, h.animating || h.months.eq(h.months.length - 1).remove(), h.wrapper.append(g), h.months = h.wrapper.find(\".picker-calendar-month\"), i = 100 * -(l - 1) * k, h.months.eq(h.months.length - 1).transform(\"translate3d(\" + (h.isH ? i : 0) + \"%, \" + (h.isH ? 0 : i) + \"%, 0)\").addClass(\"picker-calendar-month-next\")) : (h.monthsTranslate++, h.animating || h.months.eq(0).remove(), h.wrapper.prepend(g), h.months = h.wrapper.find(\".picker-calendar-month\"), i = 100 * -(l + 1) * k, h.months.eq(0).transform(\"translate3d(\" + (h.isH ? i : 0) + \"%, \" + (h.isH ? 0 : i) + \"%, 0)\").addClass(\"picker-calendar-month-prev\")), h.params.onMonthAdd && h.params.onMonthAdd(h, \"next\" === f ? h.months.eq(h.months.length - 1)[0] : h.months.eq(0)[0]), h.animating = !0, h.onMonthChangeStart(f), j = 100 * h.monthsTranslate * k, h.wrapper.transition(c).transform(\"translate3d(\" + (h.isH ? j : 0) + \"%, \" + (h.isH ? 0 : j) + \"%, 0)\"), m && h.wrapper.transitionEnd(function () {\n                h.onMonthChangeEnd(f, !0);\n            }), h.params.animate || h.onMonthChangeEnd(f);\n        }, h.nextYear = function () {\n            h.setYearMonth(h.currentYear + 1);\n        }, h.prevYear = function () {\n            h.setYearMonth(h.currentYear - 1);\n        }, h.layout = function () {\n            var a,\n                b = \"\",\n                c = \"\",\n                d = h.value && h.value.length ? h.value[0] : new Date().setHours(0, 0, 0, 0),\n                e = h.yearsGroupHTML(d, \"prev\"),\n                f = h.yearsGroupHTML(d),\n                g = h.yearsGroupHTML(d, \"next\"),\n                i = '<div class=\"picker-calendar-years-picker\"><div class=\"picker-calendar-years-picker-wrapper\">' + (e + f + g) + \"</div></div>\",\n                j = '<div class=\"picker-calendar-months-picker\"><div class=\"picker-calendar-months-picker-wrapper\">' + h.monthsGroupHTML(d) + \"</div></div>\",\n                k = h.monthHTML(d, \"prev\"),\n                l = h.monthHTML(d),\n                m = h.monthHTML(d, \"next\"),\n                n = '<div class=\"picker-calendar-months\"><div class=\"picker-calendar-months-wrapper\">' + (k + l + m) + \"</div></div>\",\n                o = \"\";\n            if (h.params.weekHeader) {\n                for (a = 0; 7 > a; a++) {\n                    var p = a + h.params.firstDay > 6 ? a - 7 + h.params.firstDay : a + h.params.firstDay,\n                        q = h.params.dayNamesShort[p];\n                    o += '<div class=\"picker-calendar-week-day ' + (h.params.weekendDays.indexOf(p) >= 0 ? \"picker-calendar-week-day-weekend\" : \"\") + '\"> ' + q + \"</div>\";\n                }\n                o = '<div class=\"picker-calendar-week-days\">' + o + \"</div>\";\n            }\n            c = \"picker-modal picker-calendar \" + (h.params.cssClass || \"\");\n            var r = h.params.toolbar ? h.params.toolbarTemplate.replace(/{{closeText}}/g, h.params.toolbarCloseText) : \"\";\n            h.params.toolbar && (r = h.params.toolbarTemplate.replace(/{{closeText}}/g, h.params.toolbarCloseText).replace(/{{monthPicker}}/g, h.params.monthPicker ? h.params.monthPickerTemplate : \"\").replace(/{{yearPicker}}/g, h.params.yearPicker ? h.params.yearPickerTemplate : \"\")), b = '<div class=\"' + c + '\">' + r + '<div class=\"picker-modal-inner\">' + o + n + \"</div>\" + j + i + \"</div>\", h.pickerHTML = b;\n        }, h.params.input && (h.input = a(h.params.input), h.input.length > 0 && (h.params.inputReadOnly && h.input.prop(\"readOnly\", !0), h.inline || h.input.on(\"click\", e), a(document).on(\"beforePageSwitch\", function () {\n            h.input.off(\"click\", e), a(document).off(\"beforePageSwitch\");\n        }))), h.inline || a(\"html\").on(\"click\", f), h.opened = !1, h.open = function () {\n            var b = !1;\n            h.opened || (h.value || h.params.value && (h.value = h.params.value, b = !0), h.layout(), h.inline ? (h.container = a(h.pickerHTML), h.container.addClass(\"picker-modal-inline\"), a(h.params.container).append(h.container)) : (h.container = a(a.pickerModal(h.pickerHTML)), a(h.container).on(\"close\", function () {\n                g();\n            })), h.container[0].f7Calendar = h, h.wrapper = h.container.find(\".picker-calendar-months-wrapper\"), h.yearsPickerWrapper = h.container.find(\".picker-calendar-years-picker-wrapper\"), h.yearsGroups = h.yearsPickerWrapper.find(\".picker-calendar-years-group\"), h.monthsPickerWrapper = h.container.find(\".picker-calendar-months-picker-wrapper\"), h.months = h.wrapper.find(\".picker-calendar-month\"), h.updateCurrentMonthYear(), h.yearsTranslate = 0, h.setYearsTranslate(), h.monthsTranslate = 0, h.setMonthsTranslate(), h.initCalendarEvents(), b && h.updateValue()), h.opened = !0, h.initialized = !0, h.params.onMonthAdd && h.months.each(function () {\n                h.params.onMonthAdd(h, this);\n            }), h.params.onOpen && h.params.onOpen(h);\n        }, h.close = function () {\n            h.opened && !h.inline && a.closeModal(h.container);\n        }, h.destroy = function () {\n            h.close(), h.params.input && h.input.length > 0 && h.input.off(\"click\", e), a(\"html\").off(\"click\", f);\n        }, h.inline && h.open(), h;\n    };\n    a.fn.calendar = function (b) {\n        return this.each(function () {\n            var d = a(this);\n            if (d[0]) {\n                var e = {};\n                \"INPUT\" === d[0].tagName.toUpperCase() ? e.input = d : e.container = d, new c(a.extend(e, b));\n            }\n        });\n    }, a.initCalendar = function (b) {\n        var c = a(b ? b : document.body);\n        c.find(\"[data-toggle='date']\").each(function () {\n            a(this).calendar();\n        });\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = function b(_b) {\n        function c() {\n            if (g.opened) for (var a = 0; a < g.cols.length; a++) {\n                g.cols[a].divider || (g.cols[a].calcSize(), g.cols[a].setValue(g.cols[a].value, 0, !1));\n            }\n        }\n\n        function d(b) {\n            if (b.preventDefault(), a.device.isWeixin && a.device.android && g.params.inputReadOnly && (this.focus(), this.blur()), !g.opened) {\n                if (a.closeModal(a(\".picker-modal\")), g.open(), g.params.scrollToInput) {\n                    var c = g.input.parents(\".content\");\n                    if (0 === c.length) return;\n                    var d,\n                        e = parseInt(c.css(\"padding-top\"), 10),\n                        f = parseInt(c.css(\"padding-bottom\"), 10),\n                        h = c[0].offsetHeight - e - g.container.height(),\n                        i = c[0].scrollHeight - e - g.container.height(),\n                        j = g.input.offset().top - e + g.input[0].offsetHeight;\n                    if (j > h) {\n                        var k = c.scrollTop() + j - h;\n                        k + h > i && (d = k + h - i + f, h === i && (d = g.container.height()), c.css({ \"padding-bottom\": d + \"px\" })), c.scrollTop(k, 300);\n                    }\n                }\n                b.stopPropagation();\n            }\n        }\n\n        function e(b) {\n            g.opened && (g.input && g.input.length > 0 ? b.target !== g.input[0] && 0 === a(b.target).parents(\".picker-modal\").length && g.close() : 0 === a(b.target).parents(\".picker-modal\").length && g.close());\n        }\n\n        function f() {\n            g.opened = !1, g.input && g.input.length > 0 && g.input.parents(\".content\").css({ \"padding-bottom\": \"\" }), g.params.onClose && g.params.onClose(g), g.container.find(\".picker-items-col\").each(function () {\n                g.destroyPickerCol(this);\n            });\n        }\n\n        var g = this,\n            h = {\n            updateValuesOnMomentum: !1,\n            updateValuesOnTouchmove: !0,\n            rotateEffect: !1,\n            momentumRatio: 7,\n            freeMode: !1,\n            scrollToInput: !0,\n            inputReadOnly: !0,\n            toolbar: !0,\n            toolbarCloseText: \"确定\",\n            toolbarTemplate: '<header class=\"bar bar-nav\">                <button class=\"button button-link pull-right close-picker\">确定</button>                <h1 class=\"title\">请选择</h1>                </header>'\n        };\n        _b = _b || {};\n        for (var i in h) {\n            \"undefined\" == typeof _b[i] && (_b[i] = h[i]);\n        }g.params = _b, g.cols = [], g.initialized = !1, g.inline = !!g.params.container;\n        var j = a.device.ios || navigator.userAgent.toLowerCase().indexOf(\"safari\") >= 0 && navigator.userAgent.toLowerCase().indexOf(\"chrome\") < 0 && !a.device.android;\n        return g.setValue = function (a, b) {\n            for (var c = 0, d = 0; d < g.cols.length; d++) {\n                g.cols[d] && !g.cols[d].divider && (g.cols[d].setValue(a[c], b), c++);\n            }\n        }, g.updateValue = function () {\n            for (var b = [], c = [], d = 0; d < g.cols.length; d++) {\n                g.cols[d].divider || (b.push(g.cols[d].value), c.push(g.cols[d].displayValue));\n            }b.indexOf(void 0) >= 0 || (g.value = b, g.displayValue = c, g.params.onChange && g.params.onChange(g, g.value, g.displayValue), g.input && g.input.length > 0 && (a(g.input).val(g.params.formatValue ? g.params.formatValue(g, g.value, g.displayValue) : g.value.join(\" \")), a(g.input).trigger(\"change\")));\n        }, g.initPickerCol = function (b, c) {\n            function d() {\n                s = a.requestAnimationFrame(function () {\n                    m.updateItems(void 0, void 0, 0), d();\n                });\n            }\n\n            function e(b) {\n                u || t || (b.preventDefault(), t = !0, v = w = \"touchstart\" === b.type ? b.targetTouches[0].pageY : b.pageY, x = new Date().getTime(), F = !0, z = B = a.getTranslate(m.wrapper[0], \"y\"));\n            }\n\n            function f(b) {\n                if (t) {\n                    b.preventDefault(), F = !1, w = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY, u || (a.cancelAnimationFrame(s), u = !0, z = B = a.getTranslate(m.wrapper[0], \"y\"), m.wrapper.transition(0)), b.preventDefault();\n                    var c = w - v;\n                    B = z + c, A = void 0, q > B && (B = q - Math.pow(q - B, .8), A = \"min\"), B > r && (B = r + Math.pow(B - r, .8), A = \"max\"), m.wrapper.transform(\"translate3d(0,\" + B + \"px,0)\"), m.updateItems(void 0, B, 0, g.params.updateValuesOnTouchmove), D = B - C || B, E = new Date().getTime(), C = B;\n                }\n            }\n\n            function h(b) {\n                if (!t || !u) return void (t = u = !1);\n                t = u = !1, m.wrapper.transition(\"\"), A && (\"min\" === A ? m.wrapper.transform(\"translate3d(0,\" + q + \"px,0)\") : m.wrapper.transform(\"translate3d(0,\" + r + \"px,0)\")), y = new Date().getTime();\n                var c, e;\n                y - x > 300 ? e = B : (c = Math.abs(D / (y - E)), e = B + D * g.params.momentumRatio), e = Math.max(Math.min(e, r), q);\n                var f = -Math.floor((e - r) / o);\n                g.params.freeMode || (e = -f * o + r), m.wrapper.transform(\"translate3d(0,\" + parseInt(e, 10) + \"px,0)\"), m.updateItems(f, e, \"\", !0), g.params.updateValuesOnMomentum && (d(), m.wrapper.transitionEnd(function () {\n                    a.cancelAnimationFrame(s);\n                })), setTimeout(function () {\n                    F = !0;\n                }, 100);\n            }\n\n            function i(b) {\n                if (F) {\n                    a.cancelAnimationFrame(s);\n                    var c = a(this).attr(\"data-picker-value\");\n                    m.setValue(c);\n                }\n            }\n\n            var k = a(b),\n                l = k.index(),\n                m = g.cols[l];\n            if (!m.divider) {\n                m.container = k, m.wrapper = m.container.find(\".picker-items-col-wrapper\"), m.items = m.wrapper.find(\".picker-item\");\n                var n, o, p, q, r;\n                m.replaceValues = function (a, b) {\n                    m.destroyEvents(), m.values = a, m.displayValues = b;\n                    var c = g.columnHTML(m, !0);\n                    m.wrapper.html(c), m.items = m.wrapper.find(\".picker-item\"), m.calcSize(), m.setValue(m.values[0], 0, !0), m.initEvents();\n                }, m.calcSize = function () {\n                    g.params.rotateEffect && (m.container.removeClass(\"picker-items-col-absolute\"), m.width || m.container.css({ width: \"\" }));\n                    var b, c;\n                    b = 0, c = m.container[0].offsetHeight, n = m.wrapper[0].offsetHeight, o = m.items[0].offsetHeight, p = o * m.items.length, q = c / 2 - p + o / 2, r = c / 2 - o / 2, m.width && (b = m.width, parseInt(b, 10) === b && (b += \"px\"), m.container.css({ width: b })), g.params.rotateEffect && (m.width || (m.items.each(function () {\n                        var c = a(this);\n                        c.css({ width: \"auto\" }), b = Math.max(b, c[0].offsetWidth), c.css({ width: \"\" });\n                    }), m.container.css({ width: b + 2 + \"px\" })), m.container.addClass(\"picker-items-col-absolute\"));\n                }, m.calcSize(), m.wrapper.transform(\"translate3d(0,\" + r + \"px,0)\").transition(0);\n                var s;\n                m.setValue = function (b, c, e) {\n                    \"undefined\" == typeof c && (c = \"\");\n                    var f = m.wrapper.find('.picker-item[data-picker-value=\"' + b + '\"]').index();\n                    if (\"undefined\" != typeof f && -1 !== f) {\n                        var h = -f * o + r;\n                        m.wrapper.transition(c), m.wrapper.transform(\"translate3d(0,\" + h + \"px,0)\"), g.params.updateValuesOnMomentum && m.activeIndex && m.activeIndex !== f && (a.cancelAnimationFrame(s), m.wrapper.transitionEnd(function () {\n                            a.cancelAnimationFrame(s);\n                        }), d()), m.updateItems(f, h, c, e);\n                    }\n                }, m.updateItems = function (b, c, d, e) {\n                    \"undefined\" == typeof c && (c = a.getTranslate(m.wrapper[0], \"y\")), \"undefined\" == typeof b && (b = -Math.round((c - r) / o)), 0 > b && (b = 0), b >= m.items.length && (b = m.items.length - 1);\n                    var f = m.activeIndex;\n                    m.activeIndex = b, m.wrapper.find(\".picker-selected\").removeClass(\"picker-selected\"), g.params.rotateEffect && m.items.transition(d);\n                    var h = m.items.eq(b).addClass(\"picker-selected\").transform(\"\");\n                    if ((e || \"undefined\" == typeof e) && (m.value = h.attr(\"data-picker-value\"), m.displayValue = m.displayValues ? m.displayValues[b] : m.value, f !== b && (m.onChange && m.onChange(g, m.value, m.displayValue), g.updateValue())), g.params.rotateEffect) {\n                        (c - (Math.floor((c - r) / o) * o + r)) / o;\n                        m.items.each(function () {\n                            var b = a(this),\n                                d = b.index() * o,\n                                e = r - c,\n                                f = d - e,\n                                g = f / o,\n                                h = Math.ceil(m.height / o / 2) + 1,\n                                i = -18 * g;\n                            i > 180 && (i = 180), -180 > i && (i = -180), Math.abs(g) > h ? b.addClass(\"picker-item-far\") : b.removeClass(\"picker-item-far\"), b.transform(\"translate3d(0, \" + (-c + r) + \"px, \" + (j ? -110 : 0) + \"px) rotateX(\" + i + \"deg)\");\n                        });\n                    }\n                }, c && m.updateItems(0, r, 0);\n                var t,\n                    u,\n                    v,\n                    w,\n                    x,\n                    y,\n                    z,\n                    A,\n                    B,\n                    C,\n                    D,\n                    E,\n                    F = !0;\n                m.initEvents = function (b) {\n                    var c = b ? \"off\" : \"on\";\n                    m.container[c](a.touchEvents.start, e), m.container[c](a.touchEvents.move, f), m.container[c](a.touchEvents.end, h), m.items[c](\"click\", i);\n                }, m.destroyEvents = function () {\n                    m.initEvents(!0);\n                }, m.container[0].f7DestroyPickerCol = function () {\n                    m.destroyEvents();\n                }, m.initEvents();\n            }\n        }, g.destroyPickerCol = function (b) {\n            b = a(b), \"f7DestroyPickerCol\" in b[0] && b[0].f7DestroyPickerCol();\n        }, a(window).on(\"resize\", c), g.columnHTML = function (a, b) {\n            var c = \"\",\n                d = \"\";\n            if (a.divider) d += '<div class=\"picker-items-col picker-items-col-divider ' + (a.textAlign ? \"picker-items-col-\" + a.textAlign : \"\") + \" \" + (a.cssClass || \"\") + '\">' + a.content + \"</div>\";else {\n                for (var e = 0; e < a.values.length; e++) {\n                    c += '<div class=\"picker-item\" data-picker-value=\"' + a.values[e] + '\">' + (a.displayValues ? a.displayValues[e] : a.values[e]) + \"</div>\";\n                }d += '<div class=\"picker-items-col ' + (a.textAlign ? \"picker-items-col-\" + a.textAlign : \"\") + \" \" + (a.cssClass || \"\") + '\"><div class=\"picker-items-col-wrapper\">' + c + \"</div></div>\";\n            }\n            return b ? c : d;\n        }, g.layout = function () {\n            var a,\n                b = \"\",\n                c = \"\";\n            g.cols = [];\n            var d = \"\";\n            for (a = 0; a < g.params.cols.length; a++) {\n                var e = g.params.cols[a];\n                d += g.columnHTML(g.params.cols[a]), g.cols.push(e);\n            }\n            c = \"picker-modal picker-columns \" + (g.params.cssClass || \"\") + (g.params.rotateEffect ? \" picker-3d\" : \"\"), b = '<div class=\"' + c + '\">' + (g.params.toolbar ? g.params.toolbarTemplate.replace(/{{closeText}}/g, g.params.toolbarCloseText) : \"\") + '<div class=\"picker-modal-inner picker-items\">' + d + '<div class=\"picker-center-highlight\"></div></div></div>', g.pickerHTML = b;\n        }, g.params.input && (g.input = a(g.params.input), g.input.length > 0 && (g.params.inputReadOnly && g.input.prop(\"readOnly\", !0), g.inline || g.input.on(\"click\", d))), g.inline || a(\"html\").on(\"click\", e), g.opened = !1, g.open = function () {\n            g.opened || (g.layout(), g.opened = !0, g.inline ? (g.container = a(g.pickerHTML), g.container.addClass(\"picker-modal-inline\"), a(g.params.container).append(g.container)) : (g.container = a(a.pickerModal(g.pickerHTML)), a(g.container).on(\"close\", function () {\n                f();\n            })), g.container[0].f7Picker = g, g.container.find(\".picker-items-col\").each(function () {\n                var a = !0;\n                (!g.initialized && g.params.value || g.initialized && g.value) && (a = !1), g.initPickerCol(this, a);\n            }), g.initialized ? g.value && g.setValue(g.value, 0) : g.params.value && g.setValue(g.params.value, 0)), g.initialized = !0, g.params.onOpen && g.params.onOpen(g);\n        }, g.close = function () {\n            g.opened && !g.inline && a.closeModal(g.container);\n        }, g.destroy = function () {\n            g.close(), g.params.input && g.input.length > 0 && g.input.off(\"click\", d), a(\"html\").off(\"click\", e), a(window).off(\"resize\", c);\n        }, g.inline && g.open(), g;\n    };\n    a(document).on(\"click\", \".close-picker\", function () {\n        var b = a(\".picker-modal.modal-in\");\n        a.closeModal(b);\n    }), a.fn.picker = function (c) {\n        var d = arguments;\n        return this.each(function () {\n            if (this) {\n                var e = a(this),\n                    f = e.data(\"picker\");\n                if (!f) {\n                    var g = a.extend({ input: this, value: e.val() ? e.val().split(\" \") : \"\" }, c);\n                    f = new b(g), e.data(\"picker\", f);\n                }\n                \"string\" == typeof c && f[c].apply(f, Array.prototype.slice.call(d, 1));\n            }\n        });\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = new Date(),\n        c = function c(a) {\n        for (var b = [], c = 1; (a || 31) >= c; c++) {\n            b.push(10 > c ? \"0\" + c : c);\n        }return b;\n    },\n        d = function d(a, b) {\n        var d = new Date(b, parseInt(a) + 1 - 1, 1),\n            e = new Date(d - 1);\n        return c(e.getDate());\n    },\n        e = function e(a) {\n        return 10 > a ? \"0\" + a : a;\n    },\n        f = \"01 02 03 04 05 06 07 08 09 10 11 12\".split(\" \"),\n        g = function () {\n        for (var a = [], b = 1950; 2030 >= b; b++) {\n            a.push(b);\n        }return a;\n    }(),\n        h = {\n        rotateEffect: !1,\n        value: [b.getFullYear(), e(b.getMonth() + 1), e(b.getDate()), b.getHours(), e(b.getMinutes())],\n        onChange: function onChange(a, b, c) {\n            var e = d(a.cols[1].value, a.cols[0].value),\n                f = a.cols[2].value;\n            f > e.length && (f = e.length), a.cols[2].setValue(f);\n        },\n        formatValue: function formatValue(a, b, c) {\n            return c[0] + \"-\" + b[1] + \"-\" + b[2] + \" \" + b[3] + \":\" + b[4];\n        },\n        cols: [{ values: g }, { values: f }, { values: c() }, { divider: !0, content: \"  \" }, {\n            values: function () {\n                for (var a = [], b = 0; 23 >= b; b++) {\n                    a.push(b);\n                }return a;\n            }()\n        }, { divider: !0, content: \":\" }, {\n            values: function () {\n                for (var a = [], b = 0; 59 >= b; b++) {\n                    a.push(10 > b ? \"0\" + b : b);\n                }return a;\n            }()\n        }]\n    };\n    a.fn.datetimePicker = function (b) {\n        return this.each(function () {\n            if (this) {\n                var c = a.extend(h, b);\n                a(this).picker(c), b.value && a(this).val(c.formatValue(c, c.value, c.value));\n            }\n        });\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    function b(a, b) {\n        this.wrapper = \"string\" == typeof a ? document.querySelector(a) : a, this.scroller = $(this.wrapper).find(\".content-inner\")[0], this.scrollerStyle = this.scroller && this.scroller.style, this.options = {\n            resizeScrollbars: !0,\n            mouseWheelSpeed: 20,\n            snapThreshold: .334,\n            startX: 0,\n            startY: 0,\n            scrollY: !0,\n            directionLockThreshold: 5,\n            momentum: !0,\n            bounce: !0,\n            bounceTime: 600,\n            bounceEasing: \"\",\n            preventDefault: !0,\n            preventDefaultException: { tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT)$/ },\n            HWCompositing: !0,\n            useTransition: !0,\n            useTransform: !0,\n            eventPassthrough: void 0\n        };\n        for (var c in b) {\n            this.options[c] = b[c];\n        }this.translateZ = this.options.HWCompositing && f.hasPerspective ? \" translateZ(0)\" : \"\", this.options.useTransition = f.hasTransition && this.options.useTransition, this.options.useTransform = f.hasTransform && this.options.useTransform, this.options.eventPassthrough = this.options.eventPassthrough === !0 ? \"vertical\" : this.options.eventPassthrough, this.options.preventDefault = !this.options.eventPassthrough && this.options.preventDefault, this.options.scrollY = \"vertical\" === this.options.eventPassthrough ? !1 : this.options.scrollY, this.options.scrollX = \"horizontal\" === this.options.eventPassthrough ? !1 : this.options.scrollX, this.options.freeScroll = this.options.freeScroll && !this.options.eventPassthrough, this.options.directionLockThreshold = this.options.eventPassthrough ? 0 : this.options.directionLockThreshold, this.options.bounceEasing = \"string\" == typeof this.options.bounceEasing ? f.ease[this.options.bounceEasing] || f.ease.circular : this.options.bounceEasing, this.options.resizePolling = void 0 === this.options.resizePolling ? 60 : this.options.resizePolling, this.options.tap === !0 && (this.options.tap = \"tap\"), \"scale\" === this.options.shrinkScrollbars && (this.options.useTransition = !1), this.options.invertWheelDirection = this.options.invertWheelDirection ? -1 : 1, 3 === this.options.probeType && (this.options.useTransition = !1), this.x = 0, this.y = 0, this.directionX = 0, this.directionY = 0, this._events = {}, this._init(), this.refresh(), this.scrollTo(this.options.startX, this.options.startY), this.enable();\n    }\n\n    function c(a, b, c) {\n        var d = document.createElement(\"div\"),\n            e = document.createElement(\"div\");\n        return c === !0 && (d.style.cssText = \"position:absolute;z-index:9999\", e.style.cssText = \"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px\"), e.className = \"iScrollIndicator\", \"h\" === a ? (c === !0 && (d.style.cssText += \";height:5px;left:2px;right:2px;bottom:0\", e.style.height = \"100%\"), d.className = \"iScrollHorizontalScrollbar\") : (c === !0 && (d.style.cssText += \";width:5px;bottom:2px;top:2px;right:1px\", e.style.width = \"100%\"), d.className = \"iScrollVerticalScrollbar\"), d.style.cssText += \";overflow:hidden\", b || (d.style.pointerEvents = \"none\"), d.appendChild(e), d;\n    }\n\n    function d(b, c) {\n        this.wrapper = \"string\" == typeof c.el ? document.querySelector(c.el) : c.el, this.wrapperStyle = this.wrapper.style, this.indicator = this.wrapper.children[0], this.indicatorStyle = this.indicator.style, this.scroller = b, this.options = {\n            listenX: !0,\n            listenY: !0,\n            interactive: !1,\n            resize: !0,\n            defaultScrollbars: !1,\n            shrink: !1,\n            fade: !1,\n            speedRatioX: 0,\n            speedRatioY: 0\n        };\n        for (var d in c) {\n            this.options[d] = c[d];\n        }this.sizeRatioX = 1, this.sizeRatioY = 1, this.maxPosX = 0, this.maxPosY = 0, this.options.interactive && (this.options.disableTouch || (f.addEvent(this.indicator, \"touchstart\", this), f.addEvent(a, \"touchend\", this)), this.options.disablePointer || (f.addEvent(this.indicator, f.prefixPointerEvent(\"pointerdown\"), this), f.addEvent(a, f.prefixPointerEvent(\"pointerup\"), this)), this.options.disableMouse || (f.addEvent(this.indicator, \"mousedown\", this), f.addEvent(a, \"mouseup\", this))), this.options.fade && (this.wrapperStyle[f.style.transform] = this.scroller.translateZ, this.wrapperStyle[f.style.transitionDuration] = f.isBadAndroid ? \"0.001s\" : \"0ms\", this.wrapperStyle.opacity = \"0\");\n    }\n\n    var e = a.requestAnimationFrame || a.webkitRequestAnimationFrame || a.mozRequestAnimationFrame || a.oRequestAnimationFrame || a.msRequestAnimationFrame || function (b) {\n        a.setTimeout(b, 1e3 / 60);\n    },\n        f = function () {\n        function b(a) {\n            return f === !1 ? !1 : \"\" === f ? a : f + a.charAt(0).toUpperCase() + a.substr(1);\n        }\n\n        var c = {},\n            d = document.createElement(\"div\").style,\n            f = function () {\n            for (var a, b = [\"t\", \"webkitT\", \"MozT\", \"msT\", \"OT\"], c = 0, e = b.length; e > c; c++) {\n                if (a = b[c] + \"ransform\", a in d) return b[c].substr(0, b[c].length - 1);\n            }return !1;\n        }();\n        c.getTime = Date.now || function () {\n            return new Date().getTime();\n        }, c.extend = function (a, b) {\n            for (var c in b) {\n                a[c] = b[c];\n            }\n        }, c.addEvent = function (a, b, c, d) {\n            a.addEventListener(b, c, !!d);\n        }, c.removeEvent = function (a, b, c, d) {\n            a.removeEventListener(b, c, !!d);\n        }, c.prefixPointerEvent = function (b) {\n            return a.MSPointerEvent ? \"MSPointer\" + b.charAt(9).toUpperCase() + b.substr(10) : b;\n        }, c.momentum = function (a, b, c, d, f, g, h) {\n            function i() {\n                +new Date() - o > 50 && (h._execEvent(\"scroll\"), o = +new Date()), +new Date() - n < k && e(i);\n            }\n\n            var j,\n                k,\n                l = a - b,\n                m = Math.abs(l) / c;\n            m /= 2, m = m > 1.5 ? 1.5 : m, g = void 0 === g ? 6e-4 : g, j = a + m * m / (2 * g) * (0 > l ? -1 : 1), k = m / g, d > j ? (j = f ? d - f / 2.5 * (m / 8) : d, l = Math.abs(j - a), k = l / m) : j > 0 && (j = f ? f / 2.5 * (m / 8) : 0, l = Math.abs(a) + j, k = l / m);\n            var n = +new Date(),\n                o = n;\n            return e(i), { destination: Math.round(j), duration: k };\n        };\n        var g = b(\"transform\");\n        return c.extend(c, {\n            hasTransform: g !== !1,\n            hasPerspective: b(\"perspective\") in d,\n            hasTouch: \"ontouchstart\" in a,\n            hasPointer: a.PointerEvent || a.MSPointerEvent,\n            hasTransition: b(\"transition\") in d\n        }), c.isBadAndroid = /Android /.test(a.navigator.appVersion) && !/Chrome\\/\\d/.test(a.navigator.appVersion) && !1, c.extend(c.style = {}, {\n            transform: g,\n            transitionTimingFunction: b(\"transitionTimingFunction\"),\n            transitionDuration: b(\"transitionDuration\"),\n            transitionDelay: b(\"transitionDelay\"),\n            transformOrigin: b(\"transformOrigin\")\n        }), c.hasClass = function (a, b) {\n            var c = new RegExp(\"(^|\\\\s)\" + b + \"(\\\\s|$)\");\n            return c.test(a.className);\n        }, c.addClass = function (a, b) {\n            if (!c.hasClass(a, b)) {\n                var d = a.className.split(\" \");\n                d.push(b), a.className = d.join(\" \");\n            }\n        }, c.removeClass = function (a, b) {\n            if (c.hasClass(a, b)) {\n                var d = new RegExp(\"(^|\\\\s)\" + b + \"(\\\\s|$)\", \"g\");\n                a.className = a.className.replace(d, \" \");\n            }\n        }, c.offset = function (a) {\n            for (var b = -a.offsetLeft, c = -a.offsetTop; a = a.offsetParent;) {\n                b -= a.offsetLeft, c -= a.offsetTop;\n            }return { left: b, top: c };\n        }, c.preventDefaultException = function (a, b) {\n            for (var c in b) {\n                if (b[c].test(a[c])) return !0;\n            }return !1;\n        }, c.extend(c.eventType = {}, {\n            touchstart: 1,\n            touchmove: 1,\n            touchend: 1,\n            mousedown: 2,\n            mousemove: 2,\n            mouseup: 2,\n            pointerdown: 3,\n            pointermove: 3,\n            pointerup: 3,\n            MSPointerDown: 3,\n            MSPointerMove: 3,\n            MSPointerUp: 3\n        }), c.extend(c.ease = {}, {\n            quadratic: {\n                style: \"cubic-bezier(0.25, 0.46, 0.45, 0.94)\", fn: function fn(a) {\n                    return a * (2 - a);\n                }\n            }, circular: {\n                style: \"cubic-bezier(0.1, 0.57, 0.1, 1)\", fn: function fn(a) {\n                    return Math.sqrt(1 - --a * a);\n                }\n            }, back: {\n                style: \"cubic-bezier(0.175, 0.885, 0.32, 1.275)\", fn: function fn(a) {\n                    var b = 4;\n                    return (a -= 1) * a * ((b + 1) * a + b) + 1;\n                }\n            }, bounce: {\n                style: \"\", fn: function fn(a) {\n                    return (a /= 1) < 1 / 2.75 ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;\n                }\n            }, elastic: {\n                style: \"\", fn: function fn(a) {\n                    var b = .22,\n                        c = .4;\n                    return 0 === a ? 0 : 1 === a ? 1 : c * Math.pow(2, -10 * a) * Math.sin((a - b / 4) * (2 * Math.PI) / b) + 1;\n                }\n            }\n        }), c.tap = function (a, b) {\n            var c = document.createEvent(\"Event\");\n            c.initEvent(b, !0, !0), c.pageX = a.pageX, c.pageY = a.pageY, a.target.dispatchEvent(c);\n        }, c.click = function (a) {\n            var b,\n                c = a.target;\n            /(SELECT|INPUT|TEXTAREA)/i.test(c.tagName) || (b = document.createEvent(\"MouseEvents\"), b.initMouseEvent(\"click\", !0, !0, a.view, 1, c.screenX, c.screenY, c.clientX, c.clientY, a.ctrlKey, a.altKey, a.shiftKey, a.metaKey, 0, null), b._constructed = !0, c.dispatchEvent(b));\n        }, c;\n    }();\n    b.prototype = {\n        version: \"5.1.3\", _init: function _init() {\n            this._initEvents(), (this.options.scrollbars || this.options.indicators) && this._initIndicators(), this.options.mouseWheel && this._initWheel(), this.options.snap && this._initSnap(), this.options.keyBindings && this._initKeys();\n        }, destroy: function destroy() {\n            this._initEvents(!0), this._execEvent(\"destroy\");\n        }, _transitionEnd: function _transitionEnd(a) {\n            a.target === this.scroller && this.isInTransition && (this._transitionTime(), this.resetPosition(this.options.bounceTime) || (this.isInTransition = !1, this._execEvent(\"scrollEnd\")));\n        }, _start: function _start(a) {\n            if ((1 === f.eventType[a.type] || 0 === a.button) && this.enabled && (!this.initiated || f.eventType[a.type] === this.initiated)) {\n                !this.options.preventDefault || f.isBadAndroid || f.preventDefaultException(a.target, this.options.preventDefaultException) || a.preventDefault();\n                var b,\n                    c = a.touches ? a.touches[0] : a;\n                this.initiated = f.eventType[a.type], this.moved = !1, this.distX = 0, this.distY = 0, this.directionX = 0, this.directionY = 0, this.directionLocked = 0, this._transitionTime(), this.startTime = f.getTime(), this.options.useTransition && this.isInTransition ? (this.isInTransition = !1, b = this.getComputedPosition(), this._translate(Math.round(b.x), Math.round(b.y)), this._execEvent(\"scrollEnd\")) : !this.options.useTransition && this.isAnimating && (this.isAnimating = !1, this._execEvent(\"scrollEnd\")), this.startX = this.x, this.startY = this.y, this.absStartX = this.x, this.absStartY = this.y, this.pointX = c.pageX, this.pointY = c.pageY, this._execEvent(\"beforeScrollStart\");\n            }\n        }, _move: function _move(a) {\n            if (this.enabled && f.eventType[a.type] === this.initiated) {\n                this.options.preventDefault && a.preventDefault();\n                var b,\n                    c,\n                    d,\n                    e,\n                    g = a.touches ? a.touches[0] : a,\n                    h = g.pageX - this.pointX,\n                    i = g.pageY - this.pointY,\n                    j = f.getTime();\n                if (this.pointX = g.pageX, this.pointY = g.pageY, this.distX += h, this.distY += i, d = Math.abs(this.distX), e = Math.abs(this.distY), !(j - this.endTime > 300 && 10 > d && 10 > e)) {\n                    if (this.directionLocked || this.options.freeScroll || (d > e + this.options.directionLockThreshold ? this.directionLocked = \"h\" : e >= d + this.options.directionLockThreshold ? this.directionLocked = \"v\" : this.directionLocked = \"n\"), \"h\" === this.directionLocked) {\n                        if (\"vertical\" === this.options.eventPassthrough) a.preventDefault();else if (\"horizontal\" === this.options.eventPassthrough) return void (this.initiated = !1);\n                        i = 0;\n                    } else if (\"v\" === this.directionLocked) {\n                        if (\"horizontal\" === this.options.eventPassthrough) a.preventDefault();else if (\"vertical\" === this.options.eventPassthrough) return void (this.initiated = !1);\n                        h = 0;\n                    }\n                    h = this.hasHorizontalScroll ? h : 0, i = this.hasVerticalScroll ? i : 0, b = this.x + h, c = this.y + i, (b > 0 || b < this.maxScrollX) && (b = this.options.bounce ? this.x + h / 3 : b > 0 ? 0 : this.maxScrollX), (c > 0 || c < this.maxScrollY) && (c = this.options.bounce ? this.y + i / 3 : c > 0 ? 0 : this.maxScrollY), this.directionX = h > 0 ? -1 : 0 > h ? 1 : 0, this.directionY = i > 0 ? -1 : 0 > i ? 1 : 0, this.moved || this._execEvent(\"scrollStart\"), this.moved = !0, this._translate(b, c), j - this.startTime > 300 && (this.startTime = j, this.startX = this.x, this.startY = this.y, 1 === this.options.probeType && this._execEvent(\"scroll\")), this.options.probeType > 1 && this._execEvent(\"scroll\");\n                }\n            }\n        }, _end: function _end(a) {\n            if (this.enabled && f.eventType[a.type] === this.initiated) {\n                this.options.preventDefault && !f.preventDefaultException(a.target, this.options.preventDefaultException) && a.preventDefault();\n                var b,\n                    c,\n                    d = f.getTime() - this.startTime,\n                    e = Math.round(this.x),\n                    g = Math.round(this.y),\n                    h = Math.abs(e - this.startX),\n                    i = Math.abs(g - this.startY),\n                    j = 0,\n                    k = \"\";\n                if (this.isInTransition = 0, this.initiated = 0, this.endTime = f.getTime(), !this.resetPosition(this.options.bounceTime)) {\n                    if (this.scrollTo(e, g), !this.moved) return this.options.tap && f.tap(a, this.options.tap), this.options.click && f.click(a), void this._execEvent(\"scrollCancel\");\n                    if (this._events.flick && 200 > d && 100 > h && 100 > i) return void this._execEvent(\"flick\");\n                    if (this.options.momentum && 300 > d && (b = this.hasHorizontalScroll ? f.momentum(this.x, this.startX, d, this.maxScrollX, this.options.bounce ? this.wrapperWidth : 0, this.options.deceleration, this) : {\n                        destination: e,\n                        duration: 0\n                    }, c = this.hasVerticalScroll ? f.momentum(this.y, this.startY, d, this.maxScrollY, this.options.bounce ? this.wrapperHeight : 0, this.options.deceleration, this) : {\n                        destination: g,\n                        duration: 0\n                    }, e = b.destination, g = c.destination, j = Math.max(b.duration, c.duration), this.isInTransition = 1), this.options.snap) {\n                        var l = this._nearestSnap(e, g);\n                        this.currentPage = l, j = this.options.snapSpeed || Math.max(Math.max(Math.min(Math.abs(e - l.x), 1e3), Math.min(Math.abs(g - l.y), 1e3)), 300), e = l.x, g = l.y, this.directionX = 0, this.directionY = 0, k = this.options.bounceEasing;\n                    }\n                    return e !== this.x || g !== this.y ? ((e > 0 || e < this.maxScrollX || g > 0 || g < this.maxScrollY) && (k = f.ease.quadratic), void this.scrollTo(e, g, j, k)) : void this._execEvent(\"scrollEnd\");\n                }\n            }\n        }, _resize: function _resize() {\n            var a = this;\n            clearTimeout(this.resizeTimeout), this.resizeTimeout = setTimeout(function () {\n                a.refresh();\n            }, this.options.resizePolling);\n        }, resetPosition: function resetPosition(b) {\n            var c = this.x,\n                d = this.y;\n            if (b = b || 0, !this.hasHorizontalScroll || this.x > 0 ? c = 0 : this.x < this.maxScrollX && (c = this.maxScrollX), !this.hasVerticalScroll || this.y > 0 ? d = 0 : this.y < this.maxScrollY && (d = this.maxScrollY), c === this.x && d === this.y) return !1;\n            if (this.options.ptr && this.y > 44 && -1 * this.startY < $(a).height() && !this.ptrLock) {\n                d = this.options.ptrOffset || 44, this._execEvent(\"ptr\"), this.ptrLock = !0;\n                var e = this;\n                setTimeout(function () {\n                    e.ptrLock = !1;\n                }, 500);\n            }\n            return this.scrollTo(c, d, b, this.options.bounceEasing), !0;\n        }, disable: function disable() {\n            this.enabled = !1;\n        }, enable: function enable() {\n            this.enabled = !0;\n        }, refresh: function refresh() {\n            this.wrapperWidth = this.wrapper.clientWidth, this.wrapperHeight = this.wrapper.clientHeight, this.scrollerWidth = this.scroller.offsetWidth, this.scrollerHeight = this.scroller.offsetHeight, this.maxScrollX = this.wrapperWidth - this.scrollerWidth, this.maxScrollY = this.wrapperHeight - this.scrollerHeight, this.hasHorizontalScroll = this.options.scrollX && this.maxScrollX < 0, this.hasVerticalScroll = this.options.scrollY && this.maxScrollY < 0, this.hasHorizontalScroll || (this.maxScrollX = 0, this.scrollerWidth = this.wrapperWidth), this.hasVerticalScroll || (this.maxScrollY = 0, this.scrollerHeight = this.wrapperHeight), this.endTime = 0, this.directionX = 0, this.directionY = 0, this.wrapperOffset = f.offset(this.wrapper), this._execEvent(\"refresh\"), this.resetPosition();\n        }, on: function on(a, b) {\n            this._events[a] || (this._events[a] = []), this._events[a].push(b);\n        }, off: function off(a, b) {\n            if (this._events[a]) {\n                var c = this._events[a].indexOf(b);\n                c > -1 && this._events[a].splice(c, 1);\n            }\n        }, _execEvent: function _execEvent(a) {\n            if (this._events[a]) {\n                var b = 0,\n                    c = this._events[a].length;\n                if (c) for (; c > b; b++) {\n                    this._events[a][b].apply(this, [].slice.call(arguments, 1));\n                }\n            }\n        }, scrollBy: function scrollBy(a, b, c, d) {\n            a = this.x + a, b = this.y + b, c = c || 0, this.scrollTo(a, b, c, d);\n        }, scrollTo: function scrollTo(a, b, c, d) {\n            d = d || f.ease.circular, this.isInTransition = this.options.useTransition && c > 0, !c || this.options.useTransition && d.style ? (this._transitionTimingFunction(d.style), this._transitionTime(c), this._translate(a, b)) : this._animate(a, b, c, d.fn);\n        }, scrollToElement: function scrollToElement(a, b, c, d, e) {\n            if (a = a.nodeType ? a : this.scroller.querySelector(a)) {\n                var g = f.offset(a);\n                g.left -= this.wrapperOffset.left, g.top -= this.wrapperOffset.top, c === !0 && (c = Math.round(a.offsetWidth / 2 - this.wrapper.offsetWidth / 2)), d === !0 && (d = Math.round(a.offsetHeight / 2 - this.wrapper.offsetHeight / 2)), g.left -= c || 0, g.top -= d || 0, g.left = g.left > 0 ? 0 : g.left < this.maxScrollX ? this.maxScrollX : g.left, g.top = g.top > 0 ? 0 : g.top < this.maxScrollY ? this.maxScrollY : g.top, b = void 0 === b || null === b || \"auto\" === b ? Math.max(Math.abs(this.x - g.left), Math.abs(this.y - g.top)) : b, this.scrollTo(g.left, g.top, b, e);\n            }\n        }, _transitionTime: function _transitionTime(a) {\n            if (a = a || 0, this.scrollerStyle[f.style.transitionDuration] = a + \"ms\", !a && f.isBadAndroid && (this.scrollerStyle[f.style.transitionDuration] = \"0.001s\"), this.indicators) for (var b = this.indicators.length; b--;) {\n                this.indicators[b].transitionTime(a);\n            }\n        }, _transitionTimingFunction: function _transitionTimingFunction(a) {\n            if (this.scrollerStyle[f.style.transitionTimingFunction] = a, this.indicators) for (var b = this.indicators.length; b--;) {\n                this.indicators[b].transitionTimingFunction(a);\n            }\n        }, _translate: function _translate(a, b) {\n            if (this.options.useTransform ? this.scrollerStyle[f.style.transform] = \"translate(\" + a + \"px,\" + b + \"px)\" + this.translateZ : (a = Math.round(a), b = Math.round(b), this.scrollerStyle.left = a + \"px\", this.scrollerStyle.top = b + \"px\"), this.x = a, this.y = b, this.indicators) for (var c = this.indicators.length; c--;) {\n                this.indicators[c].updatePosition();\n            }\n        }, _initEvents: function _initEvents(b) {\n            var c = b ? f.removeEvent : f.addEvent,\n                d = this.options.bindToWrapper ? this.wrapper : a;\n            c(a, \"orientationchange\", this), c(a, \"resize\", this), this.options.click && c(this.wrapper, \"click\", this, !0), this.options.disableMouse || (c(this.wrapper, \"mousedown\", this), c(d, \"mousemove\", this), c(d, \"mousecancel\", this), c(d, \"mouseup\", this)), f.hasPointer && !this.options.disablePointer && (c(this.wrapper, f.prefixPointerEvent(\"pointerdown\"), this), c(d, f.prefixPointerEvent(\"pointermove\"), this), c(d, f.prefixPointerEvent(\"pointercancel\"), this), c(d, f.prefixPointerEvent(\"pointerup\"), this)), f.hasTouch && !this.options.disableTouch && (c(this.wrapper, \"touchstart\", this), c(d, \"touchmove\", this), c(d, \"touchcancel\", this), c(d, \"touchend\", this)), c(this.scroller, \"transitionend\", this), c(this.scroller, \"webkitTransitionEnd\", this), c(this.scroller, \"oTransitionEnd\", this), c(this.scroller, \"MSTransitionEnd\", this);\n        }, getComputedPosition: function getComputedPosition() {\n            var b,\n                c,\n                d = a.getComputedStyle(this.scroller, null);\n            return this.options.useTransform ? (d = d[f.style.transform].split(\")\")[0].split(\", \"), b = +(d[12] || d[4]), c = +(d[13] || d[5])) : (b = +d.left.replace(/[^-\\d.]/g, \"\"), c = +d.top.replace(/[^-\\d.]/g, \"\")), {\n                x: b,\n                y: c\n            };\n        }, _initIndicators: function _initIndicators() {\n            function a(a) {\n                for (var b = h.indicators.length; b--;) {\n                    a.call(h.indicators[b]);\n                }\n            }\n\n            var b,\n                e = this.options.interactiveScrollbars,\n                f = \"string\" != typeof this.options.scrollbars,\n                g = [],\n                h = this;\n            this.indicators = [], this.options.scrollbars && (this.options.scrollY && (b = {\n                el: c(\"v\", e, this.options.scrollbars),\n                interactive: e,\n                defaultScrollbars: !0,\n                customStyle: f,\n                resize: this.options.resizeScrollbars,\n                shrink: this.options.shrinkScrollbars,\n                fade: this.options.fadeScrollbars,\n                listenX: !1\n            }, this.wrapper.appendChild(b.el), g.push(b)), this.options.scrollX && (b = {\n                el: c(\"h\", e, this.options.scrollbars),\n                interactive: e,\n                defaultScrollbars: !0,\n                customStyle: f,\n                resize: this.options.resizeScrollbars,\n                shrink: this.options.shrinkScrollbars,\n                fade: this.options.fadeScrollbars,\n                listenY: !1\n            }, this.wrapper.appendChild(b.el), g.push(b))), this.options.indicators && (g = g.concat(this.options.indicators));\n            for (var i = g.length; i--;) {\n                this.indicators.push(new d(this, g[i]));\n            }this.options.fadeScrollbars && (this.on(\"scrollEnd\", function () {\n                a(function () {\n                    this.fade();\n                });\n            }), this.on(\"scrollCancel\", function () {\n                a(function () {\n                    this.fade();\n                });\n            }), this.on(\"scrollStart\", function () {\n                a(function () {\n                    this.fade(1);\n                });\n            }), this.on(\"beforeScrollStart\", function () {\n                a(function () {\n                    this.fade(1, !0);\n                });\n            })), this.on(\"refresh\", function () {\n                a(function () {\n                    this.refresh();\n                });\n            }), this.on(\"destroy\", function () {\n                a(function () {\n                    this.destroy();\n                }), delete this.indicators;\n            });\n        }, _initWheel: function _initWheel() {\n            f.addEvent(this.wrapper, \"wheel\", this), f.addEvent(this.wrapper, \"mousewheel\", this), f.addEvent(this.wrapper, \"DOMMouseScroll\", this), this.on(\"destroy\", function () {\n                f.removeEvent(this.wrapper, \"wheel\", this), f.removeEvent(this.wrapper, \"mousewheel\", this), f.removeEvent(this.wrapper, \"DOMMouseScroll\", this);\n            });\n        }, _wheel: function _wheel(a) {\n            if (this.enabled) {\n                a.preventDefault(), a.stopPropagation();\n                var b,\n                    c,\n                    d,\n                    e,\n                    f = this;\n                if (void 0 === this.wheelTimeout && f._execEvent(\"scrollStart\"), clearTimeout(this.wheelTimeout), this.wheelTimeout = setTimeout(function () {\n                    f._execEvent(\"scrollEnd\"), f.wheelTimeout = void 0;\n                }, 400), \"deltaX\" in a) 1 === a.deltaMode ? (b = -a.deltaX * this.options.mouseWheelSpeed, c = -a.deltaY * this.options.mouseWheelSpeed) : (b = -a.deltaX, c = -a.deltaY);else if (\"wheelDeltaX\" in a) b = a.wheelDeltaX / 120 * this.options.mouseWheelSpeed, c = a.wheelDeltaY / 120 * this.options.mouseWheelSpeed;else if (\"wheelDelta\" in a) b = c = a.wheelDelta / 120 * this.options.mouseWheelSpeed;else {\n                    if (!(\"detail\" in a)) return;\n                    b = c = -a.detail / 3 * this.options.mouseWheelSpeed;\n                }\n                if (b *= this.options.invertWheelDirection, c *= this.options.invertWheelDirection, this.hasVerticalScroll || (b = c, c = 0), this.options.snap) return d = this.currentPage.pageX, e = this.currentPage.pageY, b > 0 ? d-- : 0 > b && d++, c > 0 ? e-- : 0 > c && e++, void this.goToPage(d, e);\n                d = this.x + Math.round(this.hasHorizontalScroll ? b : 0), e = this.y + Math.round(this.hasVerticalScroll ? c : 0), d > 0 ? d = 0 : d < this.maxScrollX && (d = this.maxScrollX), e > 0 ? e = 0 : e < this.maxScrollY && (e = this.maxScrollY), this.scrollTo(d, e, 0), this._execEvent(\"scroll\");\n            }\n        }, _initSnap: function _initSnap() {\n            this.currentPage = {}, \"string\" == typeof this.options.snap && (this.options.snap = this.scroller.querySelectorAll(this.options.snap)), this.on(\"refresh\", function () {\n                var a,\n                    b,\n                    c,\n                    d,\n                    e,\n                    f,\n                    g = 0,\n                    h = 0,\n                    i = 0,\n                    j = this.options.snapStepX || this.wrapperWidth,\n                    k = this.options.snapStepY || this.wrapperHeight;\n                if (this.pages = [], this.wrapperWidth && this.wrapperHeight && this.scrollerWidth && this.scrollerHeight) {\n                    if (this.options.snap === !0) for (c = Math.round(j / 2), d = Math.round(k / 2); i > -this.scrollerWidth;) {\n                        for (this.pages[g] = [], a = 0, e = 0; e > -this.scrollerHeight;) {\n                            this.pages[g][a] = {\n                                x: Math.max(i, this.maxScrollX),\n                                y: Math.max(e, this.maxScrollY),\n                                width: j,\n                                height: k,\n                                cx: i - c,\n                                cy: e - d\n                            }, e -= k, a++;\n                        }i -= j, g++;\n                    } else for (f = this.options.snap, a = f.length, b = -1; a > g; g++) {\n                        (0 === g || f[g].offsetLeft <= f[g - 1].offsetLeft) && (h = 0, b++), this.pages[h] || (this.pages[h] = []), i = Math.max(-f[g].offsetLeft, this.maxScrollX), e = Math.max(-f[g].offsetTop, this.maxScrollY), c = i - Math.round(f[g].offsetWidth / 2), d = e - Math.round(f[g].offsetHeight / 2), this.pages[h][b] = {\n                            x: i,\n                            y: e,\n                            width: f[g].offsetWidth,\n                            height: f[g].offsetHeight,\n                            cx: c,\n                            cy: d\n                        }, i > this.maxScrollX && h++;\n                    }this.goToPage(this.currentPage.pageX || 0, this.currentPage.pageY || 0, 0), this.options.snapThreshold % 1 === 0 ? (this.snapThresholdX = this.options.snapThreshold, this.snapThresholdY = this.options.snapThreshold) : (this.snapThresholdX = Math.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width * this.options.snapThreshold), this.snapThresholdY = Math.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height * this.options.snapThreshold));\n                }\n            }), this.on(\"flick\", function () {\n                var a = this.options.snapSpeed || Math.max(Math.max(Math.min(Math.abs(this.x - this.startX), 1e3), Math.min(Math.abs(this.y - this.startY), 1e3)), 300);\n                this.goToPage(this.currentPage.pageX + this.directionX, this.currentPage.pageY + this.directionY, a);\n            });\n        }, _nearestSnap: function _nearestSnap(a, b) {\n            if (!this.pages.length) return { x: 0, y: 0, pageX: 0, pageY: 0 };\n            var c = 0,\n                d = this.pages.length,\n                e = 0;\n            if (Math.abs(a - this.absStartX) < this.snapThresholdX && Math.abs(b - this.absStartY) < this.snapThresholdY) return this.currentPage;\n            for (a > 0 ? a = 0 : a < this.maxScrollX && (a = this.maxScrollX), b > 0 ? b = 0 : b < this.maxScrollY && (b = this.maxScrollY); d > c; c++) {\n                if (a >= this.pages[c][0].cx) {\n                    a = this.pages[c][0].x;\n                    break;\n                }\n            }for (d = this.pages[c].length; d > e; e++) {\n                if (b >= this.pages[0][e].cy) {\n                    b = this.pages[0][e].y;\n                    break;\n                }\n            }return c === this.currentPage.pageX && (c += this.directionX, 0 > c ? c = 0 : c >= this.pages.length && (c = this.pages.length - 1), a = this.pages[c][0].x), e === this.currentPage.pageY && (e += this.directionY, 0 > e ? e = 0 : e >= this.pages[0].length && (e = this.pages[0].length - 1), b = this.pages[0][e].y), {\n                x: a,\n                y: b,\n                pageX: c,\n                pageY: e\n            };\n        }, goToPage: function goToPage(a, b, c, d) {\n            d = d || this.options.bounceEasing, a >= this.pages.length ? a = this.pages.length - 1 : 0 > a && (a = 0), b >= this.pages[a].length ? b = this.pages[a].length - 1 : 0 > b && (b = 0);\n            var e = this.pages[a][b].x,\n                f = this.pages[a][b].y;\n            c = void 0 === c ? this.options.snapSpeed || Math.max(Math.max(Math.min(Math.abs(e - this.x), 1e3), Math.min(Math.abs(f - this.y), 1e3)), 300) : c, this.currentPage = {\n                x: e,\n                y: f,\n                pageX: a,\n                pageY: b\n            }, this.scrollTo(e, f, c, d);\n        }, next: function next(a, b) {\n            var c = this.currentPage.pageX,\n                d = this.currentPage.pageY;\n            c++, c >= this.pages.length && this.hasVerticalScroll && (c = 0, d++), this.goToPage(c, d, a, b);\n        }, prev: function prev(a, b) {\n            var c = this.currentPage.pageX,\n                d = this.currentPage.pageY;\n            c--, 0 > c && this.hasVerticalScroll && (c = 0, d--), this.goToPage(c, d, a, b);\n        }, _initKeys: function _initKeys() {\n            var b,\n                c = { pageUp: 33, pageDown: 34, end: 35, home: 36, left: 37, up: 38, right: 39, down: 40 };\n            if (\"object\" == _typeof(this.options.keyBindings)) for (b in this.options.keyBindings) {\n                \"string\" == typeof this.options.keyBindings[b] && (this.options.keyBindings[b] = this.options.keyBindings[b].toUpperCase().charCodeAt(0));\n            } else this.options.keyBindings = {};\n            for (b in c) {\n                this.options.keyBindings[b] = this.options.keyBindings[b] || c[b];\n            }f.addEvent(a, \"keydown\", this), this.on(\"destroy\", function () {\n                f.removeEvent(a, \"keydown\", this);\n            });\n        }, _key: function _key(a) {\n            if (this.enabled) {\n                var b,\n                    c = this.options.snap,\n                    d = c ? this.currentPage.pageX : this.x,\n                    e = c ? this.currentPage.pageY : this.y,\n                    g = f.getTime(),\n                    h = this.keyTime || 0,\n                    i = .25;\n                switch (this.options.useTransition && this.isInTransition && (b = this.getComputedPosition(), this._translate(Math.round(b.x), Math.round(b.y)), this.isInTransition = !1), this.keyAcceleration = 200 > g - h ? Math.min(this.keyAcceleration + i, 50) : 0, a.keyCode) {\n                    case this.options.keyBindings.pageUp:\n                        this.hasHorizontalScroll && !this.hasVerticalScroll ? d += c ? 1 : this.wrapperWidth : e += c ? 1 : this.wrapperHeight;\n                        break;\n                    case this.options.keyBindings.pageDown:\n                        this.hasHorizontalScroll && !this.hasVerticalScroll ? d -= c ? 1 : this.wrapperWidth : e -= c ? 1 : this.wrapperHeight;\n                        break;\n                    case this.options.keyBindings.end:\n                        d = c ? this.pages.length - 1 : this.maxScrollX, e = c ? this.pages[0].length - 1 : this.maxScrollY;\n                        break;\n                    case this.options.keyBindings.home:\n                        d = 0, e = 0;\n                        break;\n                    case this.options.keyBindings.left:\n                        d += c ? -1 : 5 + this.keyAcceleration >> 0;\n                        break;\n                    case this.options.keyBindings.up:\n                        e += c ? 1 : 5 + this.keyAcceleration >> 0;\n                        break;\n                    case this.options.keyBindings.right:\n                        d -= c ? -1 : 5 + this.keyAcceleration >> 0;\n                        break;\n                    case this.options.keyBindings.down:\n                        e -= c ? 1 : 5 + this.keyAcceleration >> 0;\n                        break;\n                    default:\n                        return;\n                }\n                if (c) return void this.goToPage(d, e);\n                d > 0 ? (d = 0, this.keyAcceleration = 0) : d < this.maxScrollX && (d = this.maxScrollX, this.keyAcceleration = 0), e > 0 ? (e = 0, this.keyAcceleration = 0) : e < this.maxScrollY && (e = this.maxScrollY, this.keyAcceleration = 0), this.scrollTo(d, e, 0), this.keyTime = g;\n            }\n        }, _animate: function _animate(a, b, c, d) {\n            function g() {\n                var m,\n                    n,\n                    o,\n                    p = f.getTime();\n                return p >= l ? (h.isAnimating = !1, h._translate(a, b), void (h.resetPosition(h.options.bounceTime) || h._execEvent(\"scrollEnd\"))) : (p = (p - k) / c, o = d(p), m = (a - i) * o + i, n = (b - j) * o + j, h._translate(m, n), h.isAnimating && e(g), void (3 === h.options.probeType && h._execEvent(\"scroll\")));\n            }\n\n            var h = this,\n                i = this.x,\n                j = this.y,\n                k = f.getTime(),\n                l = k + c;\n            this.isAnimating = !0, g();\n        }, handleEvent: function handleEvent(a) {\n            switch (a.type) {\n                case \"touchstart\":\n                case \"pointerdown\":\n                case \"MSPointerDown\":\n                case \"mousedown\":\n                    this._start(a);\n                    break;\n                case \"touchmove\":\n                case \"pointermove\":\n                case \"MSPointerMove\":\n                case \"mousemove\":\n                    this._move(a);\n                    break;\n                case \"touchend\":\n                case \"pointerup\":\n                case \"MSPointerUp\":\n                case \"mouseup\":\n                case \"touchcancel\":\n                case \"pointercancel\":\n                case \"MSPointerCancel\":\n                case \"mousecancel\":\n                    this._end(a);\n                    break;\n                case \"orientationchange\":\n                case \"resize\":\n                    this._resize();\n                    break;\n                case \"transitionend\":\n                case \"webkitTransitionEnd\":\n                case \"oTransitionEnd\":\n                case \"MSTransitionEnd\":\n                    this._transitionEnd(a);\n                    break;\n                case \"wheel\":\n                case \"DOMMouseScroll\":\n                case \"mousewheel\":\n                    this._wheel(a);\n                    break;\n                case \"keydown\":\n                    this._key(a);\n                    break;\n                case \"click\":\n                    a._constructed || (a.preventDefault(), a.stopPropagation());\n            }\n        }\n    }, d.prototype = {\n        handleEvent: function handleEvent(a) {\n            switch (a.type) {\n                case \"touchstart\":\n                case \"pointerdown\":\n                case \"MSPointerDown\":\n                case \"mousedown\":\n                    this._start(a);\n                    break;\n                case \"touchmove\":\n                case \"pointermove\":\n                case \"MSPointerMove\":\n                case \"mousemove\":\n                    this._move(a);\n                    break;\n                case \"touchend\":\n                case \"pointerup\":\n                case \"MSPointerUp\":\n                case \"mouseup\":\n                case \"touchcancel\":\n                case \"pointercancel\":\n                case \"MSPointerCancel\":\n                case \"mousecancel\":\n                    this._end(a);\n            }\n        }, destroy: function destroy() {\n            this.options.interactive && (f.removeEvent(this.indicator, \"touchstart\", this), f.removeEvent(this.indicator, f.prefixPointerEvent(\"pointerdown\"), this), f.removeEvent(this.indicator, \"mousedown\", this), f.removeEvent(a, \"touchmove\", this), f.removeEvent(a, f.prefixPointerEvent(\"pointermove\"), this), f.removeEvent(a, \"mousemove\", this), f.removeEvent(a, \"touchend\", this), f.removeEvent(a, f.prefixPointerEvent(\"pointerup\"), this), f.removeEvent(a, \"mouseup\", this)), this.options.defaultScrollbars && this.wrapper.parentNode.removeChild(this.wrapper);\n        }, _start: function _start(b) {\n            var c = b.touches ? b.touches[0] : b;\n            b.preventDefault(), b.stopPropagation(), this.transitionTime(), this.initiated = !0, this.moved = !1, this.lastPointX = c.pageX, this.lastPointY = c.pageY, this.startTime = f.getTime(), this.options.disableTouch || f.addEvent(a, \"touchmove\", this), this.options.disablePointer || f.addEvent(a, f.prefixPointerEvent(\"pointermove\"), this), this.options.disableMouse || f.addEvent(a, \"mousemove\", this), this.scroller._execEvent(\"beforeScrollStart\");\n        }, _move: function _move(a) {\n            var b,\n                c,\n                d,\n                e,\n                g = a.touches ? a.touches[0] : a,\n                h = f.getTime();\n            this.moved || this.scroller._execEvent(\"scrollStart\"), this.moved = !0, b = g.pageX - this.lastPointX, this.lastPointX = g.pageX, c = g.pageY - this.lastPointY, this.lastPointY = g.pageY, d = this.x + b, e = this.y + c, this._pos(d, e), 1 === this.scroller.options.probeType && h - this.startTime > 300 ? (this.startTime = h, this.scroller._execEvent(\"scroll\")) : this.scroller.options.probeType > 1 && this.scroller._execEvent(\"scroll\"), a.preventDefault(), a.stopPropagation();\n        }, _end: function _end(b) {\n            if (this.initiated) {\n                if (this.initiated = !1, b.preventDefault(), b.stopPropagation(), f.removeEvent(a, \"touchmove\", this), f.removeEvent(a, f.prefixPointerEvent(\"pointermove\"), this), f.removeEvent(a, \"mousemove\", this), this.scroller.options.snap) {\n                    var c = this.scroller._nearestSnap(this.scroller.x, this.scroller.y),\n                        d = this.options.snapSpeed || Math.max(Math.max(Math.min(Math.abs(this.scroller.x - c.x), 1e3), Math.min(Math.abs(this.scroller.y - c.y), 1e3)), 300);\n                    this.scroller.x === c.x && this.scroller.y === c.y || (this.scroller.directionX = 0, this.scroller.directionY = 0, this.scroller.currentPage = c, this.scroller.scrollTo(c.x, c.y, d, this.scroller.options.bounceEasing));\n                }\n                this.moved && this.scroller._execEvent(\"scrollEnd\");\n            }\n        }, transitionTime: function transitionTime(a) {\n            a = a || 0, this.indicatorStyle[f.style.transitionDuration] = a + \"ms\", !a && f.isBadAndroid && (this.indicatorStyle[f.style.transitionDuration] = \"0.001s\");\n        }, transitionTimingFunction: function transitionTimingFunction(a) {\n            this.indicatorStyle[f.style.transitionTimingFunction] = a;\n        }, refresh: function refresh() {\n            this.transitionTime(), this.options.listenX && !this.options.listenY ? this.indicatorStyle.display = this.scroller.hasHorizontalScroll ? \"block\" : \"none\" : this.options.listenY && !this.options.listenX ? this.indicatorStyle.display = this.scroller.hasVerticalScroll ? \"block\" : \"none\" : this.indicatorStyle.display = this.scroller.hasHorizontalScroll || this.scroller.hasVerticalScroll ? \"block\" : \"none\", this.scroller.hasHorizontalScroll && this.scroller.hasVerticalScroll ? (f.addClass(this.wrapper, \"iScrollBothScrollbars\"), f.removeClass(this.wrapper, \"iScrollLoneScrollbar\"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = \"8px\" : this.wrapper.style.bottom = \"8px\")) : (f.removeClass(this.wrapper, \"iScrollBothScrollbars\"), f.addClass(this.wrapper, \"iScrollLoneScrollbar\"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = \"2px\" : this.wrapper.style.bottom = \"2px\")), this.options.listenX && (this.wrapperWidth = this.wrapper.clientWidth, this.options.resize ? (this.indicatorWidth = Math.max(Math.round(this.wrapperWidth * this.wrapperWidth / (this.scroller.scrollerWidth || this.wrapperWidth || 1)), 8), this.indicatorStyle.width = this.indicatorWidth + \"px\") : this.indicatorWidth = this.indicator.clientWidth, this.maxPosX = this.wrapperWidth - this.indicatorWidth, \"clip\" === this.options.shrink ? (this.minBoundaryX = -this.indicatorWidth + 8, this.maxBoundaryX = this.wrapperWidth - 8) : (this.minBoundaryX = 0, this.maxBoundaryX = this.maxPosX), this.sizeRatioX = this.options.speedRatioX || this.scroller.maxScrollX && this.maxPosX / this.scroller.maxScrollX), this.options.listenY && (this.wrapperHeight = this.wrapper.clientHeight, this.options.resize ? (this.indicatorHeight = Math.max(Math.round(this.wrapperHeight * this.wrapperHeight / (this.scroller.scrollerHeight || this.wrapperHeight || 1)), 8), this.indicatorStyle.height = this.indicatorHeight + \"px\") : this.indicatorHeight = this.indicator.clientHeight, this.maxPosY = this.wrapperHeight - this.indicatorHeight, \"clip\" === this.options.shrink ? (this.minBoundaryY = -this.indicatorHeight + 8, this.maxBoundaryY = this.wrapperHeight - 8) : (this.minBoundaryY = 0, this.maxBoundaryY = this.maxPosY), this.maxPosY = this.wrapperHeight - this.indicatorHeight, this.sizeRatioY = this.options.speedRatioY || this.scroller.maxScrollY && this.maxPosY / this.scroller.maxScrollY), this.updatePosition();\n        }, updatePosition: function updatePosition() {\n            var a = this.options.listenX && Math.round(this.sizeRatioX * this.scroller.x) || 0,\n                b = this.options.listenY && Math.round(this.sizeRatioY * this.scroller.y) || 0;\n            this.options.ignoreBoundaries || (a < this.minBoundaryX ? (\"scale\" === this.options.shrink && (this.width = Math.max(this.indicatorWidth + a, 8), this.indicatorStyle.width = this.width + \"px\"), a = this.minBoundaryX) : a > this.maxBoundaryX ? \"scale\" === this.options.shrink ? (this.width = Math.max(this.indicatorWidth - (a - this.maxPosX), 8), this.indicatorStyle.width = this.width + \"px\", a = this.maxPosX + this.indicatorWidth - this.width) : a = this.maxBoundaryX : \"scale\" === this.options.shrink && this.width !== this.indicatorWidth && (this.width = this.indicatorWidth, this.indicatorStyle.width = this.width + \"px\"), b < this.minBoundaryY ? (\"scale\" === this.options.shrink && (this.height = Math.max(this.indicatorHeight + 3 * b, 8), this.indicatorStyle.height = this.height + \"px\"), b = this.minBoundaryY) : b > this.maxBoundaryY ? \"scale\" === this.options.shrink ? (this.height = Math.max(this.indicatorHeight - 3 * (b - this.maxPosY), 8), this.indicatorStyle.height = this.height + \"px\", b = this.maxPosY + this.indicatorHeight - this.height) : b = this.maxBoundaryY : \"scale\" === this.options.shrink && this.height !== this.indicatorHeight && (this.height = this.indicatorHeight, this.indicatorStyle.height = this.height + \"px\")), this.x = a, this.y = b, this.scroller.options.useTransform ? this.indicatorStyle[f.style.transform] = \"translate(\" + a + \"px,\" + b + \"px)\" + this.scroller.translateZ : (this.indicatorStyle.left = a + \"px\", this.indicatorStyle.top = b + \"px\");\n        }, _pos: function _pos(a, b) {\n            0 > a ? a = 0 : a > this.maxPosX && (a = this.maxPosX), 0 > b ? b = 0 : b > this.maxPosY && (b = this.maxPosY), a = this.options.listenX ? Math.round(a / this.sizeRatioX) : this.scroller.x, b = this.options.listenY ? Math.round(b / this.sizeRatioY) : this.scroller.y, this.scroller.scrollTo(a, b);\n        }, fade: function fade(a, b) {\n            if (!b || this.visible) {\n                clearTimeout(this.fadeTimeout), this.fadeTimeout = null;\n                var c = a ? 250 : 500,\n                    d = a ? 0 : 300;\n                a = a ? \"1\" : \"0\", this.wrapperStyle[f.style.transitionDuration] = c + \"ms\", this.fadeTimeout = setTimeout(function (a) {\n                    this.wrapperStyle.opacity = a, this.visible = +a;\n                }.bind(this, a), d);\n            }\n        }\n    }, b.utils = f, a.IScroll = b;\n}(window), +function (a) {\n    \"use strict\";\n\n    function b(b) {\n        var c = Array.apply(null, arguments);\n        c.shift();\n        var e;\n        return this.each(function () {\n            var f = a(this),\n                g = a.extend({}, f.dataset(), \"object\" == (typeof b === \"undefined\" ? \"undefined\" : _typeof(b)) && b),\n                h = f.data(\"scroller\");\n            return h || f.data(\"scroller\", h = new d(this, g)), \"string\" == typeof b && \"function\" == typeof h[b] && (e = h[b].apply(h, c), void 0 !== e) ? !1 : void 0;\n        }), void 0 !== e ? e : this;\n    }\n\n    var c = { scrollTop: a.fn.scrollTop, scrollLeft: a.fn.scrollLeft };\n    !function () {\n        a.extend(a.fn, {\n            scrollTop: function scrollTop(a, b) {\n                if (this.length) {\n                    var d = this.data(\"scroller\");\n                    return d && d.scroller ? d.scrollTop(a, b) : c.scrollTop.apply(this, arguments);\n                }\n            }\n        }), a.extend(a.fn, {\n            scrollLeft: function scrollLeft(a, b) {\n                if (this.length) {\n                    var d = this.data(\"scroller\");\n                    return d && d.scroller ? d.scrollLeft(a, b) : c.scrollLeft.apply(this, arguments);\n                }\n            }\n        });\n    }();\n    var d = function d(b, c) {\n        var d = this.$pageContent = a(b);\n        this.options = a.extend({}, this._defaults, c);\n        var e = this.options.type,\n            f = \"js\" === e || \"auto\" === e && a.device.android && a.compareVersion(\"4.4.0\", a.device.osVersion) > -1 || \"auto\" === e && a.device.ios && a.compareVersion(\"6.0.0\", a.device.osVersion) > -1;\n        if (f) {\n            var g = d.find(\".content-inner\");\n            if (!g[0]) {\n                var h = d.children();\n                h.length < 1 ? d.children().wrapAll('<div class=\"content-inner\"></div>') : d.html('<div class=\"content-inner\">' + d.html() + \"</div>\");\n            }\n            if (d.hasClass(\"pull-to-refresh-content\")) {\n                var i = a(window).height() + (d.prev().hasClass(\".bar\") ? 1 : 61);\n                d.find(\".content-inner\").css(\"min-height\", i + \"px\");\n            }\n            var j = a(b).hasClass(\"pull-to-refresh-content\"),\n                k = 0 === d.find(\".fixed-tab\").length,\n                l = { probeType: 1, mouseWheel: !0, click: a.device.androidChrome, useTransform: k, scrollX: !0 };\n            j && (l.ptr = !0, l.ptrOffset = 44), this.scroller = new IScroll(b, l), this._bindEventToDomWhenJs(), a.initPullToRefresh = a._pullToRefreshJSScroll.initPullToRefresh, a.pullToRefreshDone = a._pullToRefreshJSScroll.pullToRefreshDone, a.pullToRefreshTrigger = a._pullToRefreshJSScroll.pullToRefreshTrigger, a.destroyToRefresh = a._pullToRefreshJSScroll.destroyToRefresh, d.addClass(\"javascript-scroll\"), k || d.find(\".content-inner\").css({\n                width: \"100%\",\n                position: \"absolute\"\n            });\n            var m = this.$pageContent[0].scrollTop;\n            m && (this.$pageContent[0].scrollTop = 0, this.scrollTop(m));\n        } else d.addClass(\"native-scroll\");\n    };\n    d.prototype = {\n        _defaults: { type: \"native\" }, _bindEventToDomWhenJs: function _bindEventToDomWhenJs() {\n            if (this.scroller) {\n                var a = this;\n                this.scroller.on(\"scrollStart\", function () {\n                    a.$pageContent.trigger(\"scrollstart\");\n                }), this.scroller.on(\"scroll\", function () {\n                    a.$pageContent.trigger(\"scroll\");\n                }), this.scroller.on(\"scrollEnd\", function () {\n                    a.$pageContent.trigger(\"scrollend\");\n                });\n            }\n        }, scrollTop: function scrollTop(a, b) {\n            return this.scroller ? void 0 === a ? -1 * this.scroller.getComputedPosition().y : (this.scroller.scrollTo(0, -1 * a, b), this) : this.$pageContent.scrollTop(a, b);\n        }, scrollLeft: function scrollLeft(a, b) {\n            return this.scroller ? void 0 === a ? -1 * this.scroller.getComputedPosition().x : (this.scroller.scrollTo(-1 * a, 0), this) : this.$pageContent.scrollTop(a, b);\n        }, on: function on(a, b) {\n            return this.scroller ? this.scroller.on(a, function () {\n                b.call(this.wrapper);\n            }) : this.$pageContent.on(a, b), this;\n        }, off: function off(a, b) {\n            return this.scroller ? this.scroller.off(a, b) : this.$pageContent.off(a, b), this;\n        }, refresh: function refresh() {\n            return this.scroller && this.scroller.refresh(), this;\n        }, scrollHeight: function scrollHeight() {\n            return this.scroller ? this.scroller.scrollerHeight : this.$pageContent[0].scrollHeight;\n        }\n    };\n    var e = a.fn.scroller;\n    a.fn.scroller = b, a.fn.scroller.Constructor = d, a.fn.scroller.noConflict = function () {\n        return a.fn.scroller = e, this;\n    }, a(function () {\n        a('[data-toggle=\"scroller\"]').scroller();\n    }), a.refreshScroller = function (b) {\n        b ? a(b).scroller(\"refresh\") : a(\".javascript-scroll\").each(function () {\n            a(this).scroller(\"refresh\");\n        });\n    }, a.initScroller = function (b) {\n        this.options = a.extend({}, \"object\" == (typeof b === \"undefined\" ? \"undefined\" : _typeof(b)) && b), a('[data-toggle=\"scroller\"],.content').scroller(b);\n    }, a.getScroller = function (b) {\n        return b = b.hasClass(\"content\") ? b : b.parents(\".content\"), b ? a(b).data(\"scroller\") : a(\".content.javascript-scroll\").data(\"scroller\");\n    }, a.detectScrollerType = function (b) {\n        return b ? a(b).data(\"scroller\") && a(b).data(\"scroller\").scroller ? \"js\" : \"native\" : void 0;\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = function b(_b2, c, d) {\n        var e = a(_b2);\n        if (2 === arguments.length && \"boolean\" == typeof c && (d = c), 0 === e.length) return !1;\n        if (e.hasClass(\"active\")) return d && e.trigger(\"show\"), !1;\n        var f = e.parent(\".tabs\");\n        if (0 === f.length) return !1;\n        var g = f.children(\".tab.active\").removeClass(\"active\");\n        if (e.addClass(\"active\"), e.trigger(\"show\"), c ? c = a(c) : (c = a(\"string\" == typeof _b2 ? '.tab-link[href=\"' + _b2 + '\"]' : '.tab-link[href=\"#' + e.attr(\"id\") + '\"]'), (!c || c && 0 === c.length) && a(\"[data-tab]\").each(function () {\n            e.is(a(this).attr(\"data-tab\")) && (c = a(this));\n        })), 0 !== c.length) {\n            var h;\n            if (g && g.length > 0) {\n                var i = g.attr(\"id\");\n                i && (h = a('.tab-link[href=\"#' + i + '\"]')), (!h || h && 0 === h.length) && a(\"[data-tab]\").each(function () {\n                    g.is(a(this).attr(\"data-tab\")) && (h = a(this));\n                });\n            }\n            return c && c.length > 0 && c.addClass(\"active\"), h && h.length > 0 && h.removeClass(\"active\"), c.trigger(\"active\"), !0;\n        }\n    },\n        c = a.showTab;\n    a.showTab = b, a.showTab.noConflict = function () {\n        return a.showTab = c, this;\n    }, a(document).on(\"click\", \".tab-link\", function (c) {\n        c.preventDefault();\n        var d = a(this);\n        b(d.data(\"tab\") || d.attr(\"href\"), d);\n    });\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    function b(b) {\n        var d = Array.apply(null, arguments);\n        d.shift(), this.each(function () {\n            var d = a(this),\n                e = a.extend({}, d.dataset(), \"object\" == (typeof b === \"undefined\" ? \"undefined\" : _typeof(b)) && b),\n                f = d.data(\"fixedtab\");\n            f || d.data(\"fixedtab\", f = new c(this, e));\n        });\n    }\n\n    a.initFixedTab = function () {\n        var b = a(\".fixed-tab\");\n        0 !== b.length && a(\".fixed-tab\").fixedTab();\n    };\n    var c = function c(b, _c2) {\n        var d = this.$pageContent = a(b),\n            e = d.clone(),\n            f = d[0].getBoundingClientRect().top;\n        e.css(\"visibility\", \"hidden\"), this.options = a.extend({}, this._defaults, {\n            fixedTop: f,\n            shadow: e,\n            offset: 0\n        }, _c2), this._bindEvents();\n    };\n    c.prototype = {\n        _defaults: { offset: 0 }, _bindEvents: function _bindEvents() {\n            this.$pageContent.parents(\".content\").on(\"scroll\", this._scrollHandler.bind(this)), this.$pageContent.on(\"active\", \".tab-link\", this._tabLinkHandler.bind(this));\n        }, _tabLinkHandler: function _tabLinkHandler(b) {\n            var c = a(b.target).parents(\".buttons-fixed\").length > 0,\n                d = this.options.fixedTop,\n                e = this.options.offset;\n            a.refreshScroller(), c && this.$pageContent.parents(\".content\").scrollTop(d - e);\n        }, _scrollHandler: function _scrollHandler(b) {\n            var c = a(b.target),\n                d = this.$pageContent,\n                e = this.options.shadow,\n                f = this.options.offset,\n                g = this.options.fixedTop,\n                h = c.scrollTop(),\n                i = h >= g - f;\n            i ? (e.insertAfter(d), d.addClass(\"buttons-fixed\").css(\"top\", f)) : (e.remove(), d.removeClass(\"buttons-fixed\").css(\"top\", 0));\n        }\n    }, a.fn.fixedTab = b, a.fn.fixedTab.Constructor = c, a(document).on(\"pageInit\", function () {\n        a.initFixedTab();\n    });\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = 0,\n        c = function c(_c3) {\n        function d() {\n            j.hasClass(\"refreshing\") || (-1 * i.scrollTop() >= 44 ? j.removeClass(\"pull-down\").addClass(\"pull-up\") : j.removeClass(\"pull-up\").addClass(\"pull-down\"));\n        }\n\n        function e() {\n            j.hasClass(\"refreshing\") || (j.removeClass(\"pull-down pull-up\"), j.addClass(\"refreshing transitioning\"), j.trigger(\"refresh\"), b = +new Date());\n        }\n\n        function f() {\n            i.off(\"scroll\", d), i.scroller.off(\"ptr\", e);\n        }\n\n        var g = a(_c3);\n        if (g.hasClass(\"pull-to-refresh-content\") || (g = g.find(\".pull-to-refresh-content\")), g && 0 !== g.length) {\n            var h = g.hasClass(\"content\") ? g : g.parents(\".content\"),\n                i = a.getScroller(h[0]);\n            if (i) {\n                var j = g;\n                i.on(\"scroll\", d), i.scroller.on(\"ptr\", e), g[0].destroyPullToRefresh = f;\n            }\n        }\n    },\n        d = function d(c) {\n        if (c = a(c), 0 === c.length && (c = a(\".pull-to-refresh-content.refreshing\")), 0 !== c.length) {\n            var d = +new Date() - b,\n                e = d > 1e3 ? 0 : 1e3 - d,\n                f = a.getScroller(c);\n            setTimeout(function () {\n                f.refresh(), c.removeClass(\"refreshing\"), c.transitionEnd(function () {\n                    c.removeClass(\"transitioning\");\n                });\n            }, e);\n        }\n    },\n        e = function e(b) {\n        if (b = a(b), 0 === b.length && (b = a(\".pull-to-refresh-content\")), !b.hasClass(\"refreshing\")) {\n            b.addClass(\"refreshing\");\n            var c = a.getScroller(b);\n            c.scrollTop(45, 200), b.trigger(\"refresh\");\n        }\n    },\n        f = function f(b) {\n        b = a(b);\n        var c = b.hasClass(\"pull-to-refresh-content\") ? b : b.find(\".pull-to-refresh-content\");\n        0 !== c.length && c[0].destroyPullToRefresh && c[0].destroyPullToRefresh();\n    };\n    a._pullToRefreshJSScroll = {\n        initPullToRefresh: c,\n        pullToRefreshDone: d,\n        pullToRefreshTrigger: e,\n        destroyPullToRefresh: f\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    a.initPullToRefresh = function (b) {\n        function c(b) {\n            if (h) {\n                if (!a.device.android) return;\n                if (\"targetTouches\" in b && b.targetTouches.length > 1) return;\n            }\n            i = !1, h = !0, j = void 0, p = void 0, s.x = \"touchstart\" === b.type ? b.targetTouches[0].pageX : b.pageX, s.y = \"touchstart\" === b.type ? b.targetTouches[0].pageY : b.pageY, l = new Date().getTime(), m = a(this);\n        }\n\n        function d(b) {\n            if (h) {\n                var c = \"touchmove\" === b.type ? b.targetTouches[0].pageX : b.pageX,\n                    d = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY;\n                if (\"undefined\" == typeof j && (j = !!(j || Math.abs(d - s.y) > Math.abs(c - s.x))), !j) return void (h = !1);\n                if (o = m[0].scrollTop, \"undefined\" == typeof p && 0 !== o && (p = !0), !i) {\n                    if (m.removeClass(\"transitioning\"), o > m[0].offsetHeight) return void (h = !1);\n                    r && (q = m.attr(\"data-ptr-distance\"), q.indexOf(\"%\") >= 0 && (q = m[0].offsetHeight * parseInt(q, 10) / 100)), v = m.hasClass(\"refreshing\") ? q : 0, u = m[0].scrollHeight === m[0].offsetHeight || !a.device.ios, u = !0;\n                }\n                return i = !0, k = d - s.y, k > 0 && 0 >= o || 0 > o ? (a.device.ios && parseInt(a.device.osVersion.split(\".\")[0], 10) > 7 && 0 === o && !p && (u = !0), u && (b.preventDefault(), n = Math.pow(k, .85) + v, m.transform(\"translate3d(0,\" + n + \"px,0)\")), u && Math.pow(k, .85) > q || !u && k >= 2 * q ? (t = !0, m.addClass(\"pull-up\").removeClass(\"pull-down\")) : (t = !1, m.removeClass(\"pull-up\").addClass(\"pull-down\")), void 0) : (m.removeClass(\"pull-up pull-down\"), void (t = !1));\n            }\n        }\n\n        function e() {\n            if (!h || !i) return h = !1, void (i = !1);\n            if (n && (m.addClass(\"transitioning\"), n = 0), m.transform(\"\"), t) {\n                if (m.hasClass(\"refreshing\")) return;\n                m.addClass(\"refreshing\"), m.trigger(\"refresh\");\n            } else m.removeClass(\"pull-down\");\n            h = !1, i = !1;\n        }\n\n        function f() {\n            g.off(a.touchEvents.start, c), g.off(a.touchEvents.move, d), g.off(a.touchEvents.end, e);\n        }\n\n        var g = a(b);\n        if (g.hasClass(\"pull-to-refresh-content\") || (g = g.find(\".pull-to-refresh-content\")), g && 0 !== g.length) {\n            var h,\n                i,\n                j,\n                k,\n                l,\n                m,\n                n,\n                o,\n                p,\n                q,\n                r,\n                s = {},\n                t = !1,\n                u = !1,\n                v = 0;\n            m = g, m.attr(\"data-ptr-distance\") ? r = !0 : q = 44, g.on(a.touchEvents.start, c), g.on(a.touchEvents.move, d), g.on(a.touchEvents.end, e), g[0].destroyPullToRefresh = f;\n        }\n    }, a.pullToRefreshDone = function (b) {\n        a(window).scrollTop(0), b = a(b), 0 === b.length && (b = a(\".pull-to-refresh-content.refreshing\")), b.removeClass(\"refreshing\").addClass(\"transitioning\"), b.transitionEnd(function () {\n            b.removeClass(\"transitioning pull-up pull-down\");\n        });\n    }, a.pullToRefreshTrigger = function (b) {\n        b = a(b), 0 === b.length && (b = a(\".pull-to-refresh-content\")), b.hasClass(\"refreshing\") || (b.addClass(\"transitioning refreshing\"), b.trigger(\"refresh\"));\n    }, a.destroyPullToRefresh = function (b) {\n        b = a(b);\n        var c = b.hasClass(\"pull-to-refresh-content\") ? b : b.find(\".pull-to-refresh-content\");\n        0 !== c.length && c[0].destroyPullToRefresh && c[0].destroyPullToRefresh();\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    function b() {\n        var b,\n            c = a(this),\n            d = a.getScroller(c),\n            e = d.scrollTop(),\n            f = d.scrollHeight(),\n            g = c[0].offsetHeight,\n            h = c[0].getAttribute(\"data-distance\"),\n            i = c.find(\".virtual-list\"),\n            j = c.hasClass(\"infinite-scroll-top\");\n        if (h || (h = 50), \"string\" == typeof h && h.indexOf(\"%\") >= 0 && (h = parseInt(h, 10) / 100 * g), h > g && (h = g), j) h > e && c.trigger(\"infinite\");else if (e + g >= f - h) {\n            if (i.length > 0 && (b = i[0].f7VirtualList, b && !b.reachEnd)) return;\n            c.trigger(\"infinite\");\n        }\n    }\n\n    a.attachInfiniteScroll = function (c) {\n        a.getScroller(c).on(\"scroll\", b);\n    }, a.detachInfiniteScroll = function (c) {\n        a.getScroller(c).off(\"scroll\", b);\n    }, a.initInfiniteScroll = function (b) {\n        function c() {\n            a.detachInfiniteScroll(d), b.off(\"pageBeforeRemove\", c);\n        }\n\n        b = a(b);\n        var d = b.hasClass(\"infinite-scroll\") ? b : b.find(\".infinite-scroll\");\n        0 !== d.length && (a.attachInfiniteScroll(d), b.forEach(function (b) {\n            if (a(b).hasClass(\"infinite-scroll-top\")) {\n                var c = b.scrollHeight - b.clientHeight;\n                a(b).scrollTop(c);\n            }\n        }), b.on(\"pageBeforeRemove\", c));\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    a(function () {\n        a(document).on(\"focus\", \".searchbar input\", function (b) {\n            var c = a(b.target);\n            c.parents(\".searchbar\").addClass(\"searchbar-active\");\n        }), a(document).on(\"click\", \".searchbar-cancel\", function (b) {\n            var c = a(b.target);\n            c.parents(\".searchbar\").removeClass(\"searchbar-active\");\n        }), a(document).on(\"blur\", \".searchbar input\", function (b) {\n            var c = a(b.target);\n            c.parents(\".searchbar\").removeClass(\"searchbar-active\");\n        });\n    });\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    a.allowPanelOpen = !0, a.openPanel = function (b) {\n        function c() {\n            f.transitionEnd(function (d) {\n                d.target === f[0] ? (b.hasClass(\"active\") ? b.trigger(\"opened\") : b.trigger(\"closed\"), a.allowPanelOpen = !0) : c();\n            });\n        }\n\n        if (!a.allowPanelOpen) return !1;\n        \"left\" !== b && \"right\" !== b || (b = \".panel-\" + b), b = b ? a(b) : a(\".panel\").eq(0);\n        var d = b.hasClass(\"panel-right\") ? \"right\" : \"left\";\n        if (0 === b.length || b.hasClass(\"active\")) return !1;\n        a.closePanel(), a.allowPanelOpen = !1;\n        var e = b.hasClass(\"panel-reveal\") ? \"reveal\" : \"cover\";\n        b.css({ display: \"block\" }).addClass(\"active\"), b.trigger(\"open\");\n        var f = (b[0].clientLeft, \"reveal\" === e ? a(a.getCurrentPage()) : b);\n        return c(), a(document.body).addClass(\"with-panel-\" + d + \"-\" + e), !0;\n    }, a.closePanel = function () {\n        var b = a(\".panel.active\");\n        if (0 === b.length) return !1;\n        var c = b.hasClass(\"panel-reveal\") ? \"reveal\" : \"cover\",\n            d = b.hasClass(\"panel-left\") ? \"left\" : \"right\";\n        b.removeClass(\"active\");\n        var e = \"reveal\" === c ? a(\".page\") : b;\n        b.trigger(\"close\"), a.allowPanelOpen = !1, e.transitionEnd(function () {\n            b.hasClass(\"active\") || (b.css({ display: \"\" }), b.trigger(\"closed\"), a(\"body\").removeClass(\"panel-closing\"), a.allowPanelOpen = !0);\n        }), a(\"body\").addClass(\"panel-closing\").removeClass(\"with-panel-\" + d + \"-\" + c);\n    }, a(document).on(\"click\", \".open-panel\", function (b) {\n        var c = a(b.target).data(\"panel\");\n        a.openPanel(c);\n    }), a(document).on(\"click\", \".close-panel, .panel-overlay\", function (b) {\n        a.closePanel();\n    }), a.initSwipePanels = function () {\n        function b(b) {\n            if (a.allowPanelOpen && (g || h) && !m && !(a(\".modal-in, .photo-browser-in\").length > 0) && (i || h || !(a(\".panel.active\").length > 0) || e.hasClass(\"active\"))) {\n                if (x.x = \"touchstart\" === b.type ? b.targetTouches[0].pageX : b.pageX, x.y = \"touchstart\" === b.type ? b.targetTouches[0].pageY : b.pageY, i || h) {\n                    if (a(\".panel.active\").length > 0) f = a(\".panel.active\").hasClass(\"panel-left\") ? \"left\" : \"right\";else {\n                        if (h) return;\n                        f = g;\n                    }\n                    if (!f) return;\n                }\n                if (e = a(\".panel.panel-\" + f), e[0]) {\n                    if (s = e.hasClass(\"active\"), j && !s) {\n                        if (\"left\" === f && x.x > j) return;\n                        if (\"right\" === f && x.x < window.innerWidth - j) return;\n                    }\n                    n = !1, m = !0, o = void 0, p = new Date().getTime(), v = void 0;\n                }\n            }\n        }\n\n        function c(b) {\n            if (m && e[0] && !b.f7PreventPanelSwipe) {\n                var c = \"touchmove\" === b.type ? b.targetTouches[0].pageX : b.pageX,\n                    d = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY;\n                if (\"undefined\" == typeof o && (o = !!(o || Math.abs(d - x.y) > Math.abs(c - x.x))), o) return void (m = !1);\n                if (!v && (v = c > x.x ? \"to-right\" : \"to-left\", \"left\" === f && \"to-left\" === v && !e.hasClass(\"active\") || \"right\" === f && \"to-right\" === v && !e.hasClass(\"active\"))) return void (m = !1);\n                if (l) {\n                    var g = new Date().getTime() - p;\n                    return 300 > g && (\"to-left\" === v && (\"right\" === f && a.openPanel(f), \"left\" === f && e.hasClass(\"active\") && a.closePanel()), \"to-right\" === v && (\"left\" === f && a.openPanel(f), \"right\" === f && e.hasClass(\"active\") && a.closePanel())), m = !1, console.log(3), void (n = !1);\n                }\n                n || (u = e.hasClass(\"panel-cover\") ? \"cover\" : \"reveal\", s || (e.show(), w.show()), t = e[0].offsetWidth, e.transition(0)), n = !0, b.preventDefault();\n                var h = s ? 0 : -k;\n                \"right\" === f && (h = -h), q = c - x.x + h, \"right\" === f ? (r = q - (s ? t : 0), r > 0 && (r = 0), -t > r && (r = -t)) : (r = q + (s ? t : 0), 0 > r && (r = 0), r > t && (r = t)), \"reveal\" === u ? (y.transform(\"translate3d(\" + r + \"px,0,0)\").transition(0), w.transform(\"translate3d(\" + r + \"px,0,0)\")) : e.transform(\"translate3d(\" + r + \"px,0,0)\").transition(0);\n            }\n        }\n\n        function d(b) {\n            if (!m || !n) return m = !1, void (n = !1);\n            m = !1, n = !1;\n            var c,\n                d = new Date().getTime() - p,\n                g = 0 === r || Math.abs(r) === t;\n            if (c = s ? r === -t ? \"reset\" : 300 > d && Math.abs(r) >= 0 || d >= 300 && Math.abs(r) <= t / 2 ? \"left\" === f && r === t ? \"reset\" : \"swap\" : \"reset\" : 0 === r ? \"reset\" : 300 > d && Math.abs(r) > 0 || d >= 300 && Math.abs(r) >= t / 2 ? \"swap\" : \"reset\", \"swap\" === c && (a.allowPanelOpen = !0, s ? (a.closePanel(), g && (e.css({ display: \"\" }), a(\"body\").removeClass(\"panel-closing\"))) : a.openPanel(f), g && (a.allowPanelOpen = !0)), \"reset\" === c) if (s) a.allowPanelOpen = !0, a.openPanel(f);else if (a.closePanel(), g) a.allowPanelOpen = !0, e.css({ display: \"\" });else {\n                var h = \"reveal\" === u ? y : e;\n                a(\"body\").addClass(\"panel-closing\"), h.transitionEnd(function () {\n                    a.allowPanelOpen = !0, e.css({ display: \"\" }), a(\"body\").removeClass(\"panel-closing\");\n                });\n            }\n            \"reveal\" === u && (y.transition(\"\"), y.transform(\"\")), e.transition(\"\").transform(\"\"), w.css({ display: \"\" }).transform(\"\");\n        }\n\n        var e,\n            f,\n            g = a.smConfig.swipePanel,\n            h = a.smConfig.swipePanelOnlyClose,\n            i = !0,\n            j = !1,\n            k = 2,\n            l = !1;\n        if (g || h) {\n            var m,\n                n,\n                o,\n                p,\n                q,\n                r,\n                s,\n                t,\n                u,\n                v,\n                w = a(\".panel-overlay\"),\n                x = {},\n                y = a(\".page\");\n            a(document).on(a.touchEvents.start, b), a(document).on(a.touchEvents.move, c), a(document).on(a.touchEvents.end, d);\n        }\n    }, a.initSwipePanels();\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    function b(a) {\n        for (var b = [\"external\", \"tab-link\", \"open-popup\", \"close-popup\", \"open-panel\", \"close-panel\"], c = b.length - 1; c >= 0; c--) {\n            if (a.hasClass(b[c])) return !0;\n        }var d = a.get(0),\n            e = d.getAttribute(\"href\"),\n            f = [\"http\", \"https\"];\n        return (/^(\\w+):/.test(e) && f.indexOf(RegExp.$1) < 0 ? !0 : !!d.hasAttribute(\"external\")\n        );\n    }\n\n    function c(b) {\n        var c = a.smConfig.routerFilter;\n        if (a.isFunction(c)) {\n            var d = c(b);\n            if (\"boolean\" == typeof d) return d;\n        }\n        return !0;\n    }\n\n    window.CustomEvent || (window.CustomEvent = function (a, b) {\n        b = b || { bubbles: !1, cancelable: !1, detail: void 0 };\n        var c = document.createEvent(\"CustomEvent\");\n        return c.initCustomEvent(a, b.bubbles, b.cancelable, b.detail), c;\n    }, window.CustomEvent.prototype = window.Event.prototype);\n    var d = {\n        pageLoadStart: \"pageLoadStart\",\n        pageLoadCancel: \"pageLoadCancel\",\n        pageLoadError: \"pageLoadError\",\n        pageLoadComplete: \"pageLoadComplete\",\n        pageAnimationStart: \"pageAnimationStart\",\n        pageAnimationEnd: \"pageAnimationEnd\",\n        beforePageRemove: \"beforePageRemove\",\n        pageRemoved: \"pageRemoved\",\n        beforePageSwitch: \"beforePageSwitch\",\n        pageInit: \"pageInitInternal\"\n    },\n        e = {\n        getUrlFragment: function getUrlFragment(a) {\n            var b = a.indexOf(\"#\");\n            return -1 === b ? \"\" : a.slice(b + 1);\n        }, getAbsoluteUrl: function getAbsoluteUrl(a) {\n            var b = document.createElement(\"a\");\n            b.setAttribute(\"href\", a);\n            var c = b.href;\n            return b = null, c;\n        }, getBaseUrl: function getBaseUrl(a) {\n            var b = a.indexOf(\"#\");\n            return -1 === b ? a.slice(0) : a.slice(0, b);\n        }, toUrlObject: function toUrlObject(a) {\n            var b = this.getAbsoluteUrl(a),\n                c = this.getBaseUrl(b),\n                d = this.getUrlFragment(a);\n            return { base: c, full: b, original: a, fragment: d };\n        }, supportStorage: function supportStorage() {\n            var a = \"sm.router.storage.ability\";\n            try {\n                return sessionStorage.setItem(a, a), sessionStorage.removeItem(a), !0;\n            } catch (b) {\n                return !1;\n            }\n        }\n    },\n        f = {\n        sectionGroupClass: \"page-group\",\n        curPageClass: \"page-current\",\n        visiblePageClass: \"page-visible\",\n        pageClass: \"page\"\n    },\n        g = { leftToRight: \"from-left-to-right\", rightToLeft: \"from-right-to-left\" },\n        h = window.history,\n        i = function i() {\n        this.sessionNames = {\n            currentState: \"sm.router.currentState\",\n            maxStateId: \"sm.router.maxStateId\"\n        }, this._init(), this.xhr = null, window.addEventListener(\"popstate\", this._onPopState.bind(this));\n    };\n    i.prototype._init = function () {\n        this.$view = a(\"body\"), this.cache = {};\n        var b = a(document),\n            c = location.href;\n        this._saveDocumentIntoCache(b, c);\n        var d,\n            g,\n            i = e.toUrlObject(c),\n            j = b.find(\".\" + f.pageClass),\n            k = b.find(\".\" + f.curPageClass),\n            l = k.eq(0);\n        if (i.fragment && (g = b.find(\"#\" + i.fragment)), g && g.length ? k = g.eq(0) : k.length || (k = j.eq(0)), k.attr(\"id\") || k.attr(\"id\", this._generateRandomId()), l.length && l.attr(\"id\") !== k.attr(\"id\") ? (l.removeClass(f.curPageClass), k.addClass(f.curPageClass)) : k.addClass(f.curPageClass), d = k.attr(\"id\"), null === h.state) {\n            var m = { id: this._getNextStateId(), url: e.toUrlObject(c), pageId: d };\n            h.replaceState(m, \"\", c), this._saveAsCurrentState(m), this._incMaxStateId();\n        }\n    }, i.prototype.load = function (b, c) {\n        void 0 === c && (c = !1), this._isTheSameDocument(location.href, b) ? this._switchToSection(e.getUrlFragment(b)) : (this._saveDocumentIntoCache(a(document), location.href), this._switchToDocument(b, c));\n    }, i.prototype.forward = function () {\n        h.forward();\n    }, i.prototype.back = function () {\n        h.back();\n    }, i.prototype.loadPage = i.prototype.load, i.prototype._switchToSection = function (b) {\n        if (b) {\n            var c = this._getCurrentSection(),\n                d = a(\"#\" + b);\n            c !== d && (this._animateSection(c, d, g.rightToLeft), this._pushNewState(\"#\" + b, b));\n        }\n    }, i.prototype._switchToDocument = function (a, b, c, d) {\n        var f = e.toUrlObject(a).base;\n        b && delete this.cache[f];\n        var g = this.cache[f],\n            h = this;\n        g ? this._doSwitchDocument(a, c, d) : this._loadDocument(a, {\n            success: function success(b) {\n                try {\n                    h._parseDocument(a, b), h._doSwitchDocument(a, c, d);\n                } catch (e) {\n                    location.href = a;\n                }\n            }, error: function error() {\n                location.href = a;\n            }\n        });\n    }, i.prototype._doSwitchDocument = function (b, c, g) {\n        \"undefined\" == typeof c && (c = !0);\n        var h,\n            i = e.toUrlObject(b),\n            j = this.$view.find(\".\" + f.sectionGroupClass),\n            k = a(a(\"<div></div>\").append(this.cache[i.base].$content).html()),\n            l = k.find(\".\" + f.pageClass),\n            m = k.find(\".\" + f.curPageClass);\n        i.fragment && (h = k.find(\"#\" + i.fragment)), h && h.length ? m = h.eq(0) : m.length || (m = l.eq(0)), m.attr(\"id\") || m.attr(\"id\", this._generateRandomId());\n        var n = this._getCurrentSection();\n        n.trigger(d.beforePageSwitch, [n.attr(\"id\"), n]), l.removeClass(f.curPageClass), m.addClass(f.curPageClass), this.$view.prepend(k), this._animateDocument(j, k, m, g), c && this._pushNewState(b, m.attr(\"id\"));\n    }, i.prototype._isTheSameDocument = function (a, b) {\n        return e.toUrlObject(a).base === e.toUrlObject(b).base;\n    }, i.prototype._loadDocument = function (b, c) {\n        this.xhr && this.xhr.readyState < 4 && (this.xhr.onreadystatechange = function () {}, this.xhr.abort(), this.dispatch(d.pageLoadCancel)), this.dispatch(d.pageLoadStart), c = c || {};\n        var e = this;\n        this.xhr = a.ajax({\n            url: b, success: a.proxy(function (b, d, e) {\n                var f = a(\"<html></html>\");\n                f.append(b), c.success && c.success.call(null, f, d, e);\n            }, this), error: function error(a, b, f) {\n                c.error && c.error.call(null, a, b, f), e.dispatch(d.pageLoadError);\n            }, complete: function complete(a, b) {\n                c.complete && c.complete.call(null, a, b), e.dispatch(d.pageLoadComplete);\n            }\n        });\n    }, i.prototype._parseDocument = function (a, b) {\n        var c = b.find(\".\" + f.sectionGroupClass);\n        if (!c.length) throw new Error(\"missing router view mark: \" + f.sectionGroupClass);\n        this._saveDocumentIntoCache(b, a);\n    }, i.prototype._saveDocumentIntoCache = function (b, c) {\n        var d = e.toUrlObject(c).base,\n            g = a(b);\n        this.cache[d] = { $doc: g, $content: g.find(\".\" + f.sectionGroupClass) };\n    }, i.prototype._getLastState = function () {\n        var a = sessionStorage.getItem(this.sessionNames.currentState);\n        try {\n            a = JSON.parse(a);\n        } catch (b) {\n            a = null;\n        }\n        return a;\n    }, i.prototype._saveAsCurrentState = function (a) {\n        sessionStorage.setItem(this.sessionNames.currentState, JSON.stringify(a));\n    }, i.prototype._getNextStateId = function () {\n        var a = sessionStorage.getItem(this.sessionNames.maxStateId);\n        return a ? parseInt(a, 10) + 1 : 1;\n    }, i.prototype._incMaxStateId = function () {\n        sessionStorage.setItem(this.sessionNames.maxStateId, this._getNextStateId());\n    }, i.prototype._animateDocument = function (b, c, e, g) {\n        var h = e.attr(\"id\"),\n            i = b.find(\".\" + f.curPageClass);\n        i.addClass(f.visiblePageClass).removeClass(f.curPageClass), e.trigger(d.pageAnimationStart, [h, e]), this._animateElement(b, c, g), b.animationEnd(function () {\n            i.removeClass(f.visiblePageClass), a(window).trigger(d.beforePageRemove, [b]), b.remove(), a(window).trigger(d.pageRemoved);\n        }), c.animationEnd(function () {\n            e.trigger(d.pageAnimationEnd, [h, e]), e.trigger(d.pageInit, [h, e]);\n        });\n    }, i.prototype._animateSection = function (a, b, c) {\n        var e = b.attr(\"id\");\n        a.trigger(d.beforePageSwitch, [a.attr(\"id\"), a]), a.removeClass(f.curPageClass), b.addClass(f.curPageClass), b.trigger(d.pageAnimationStart, [e, b]), this._animateElement(a, b, c), b.animationEnd(function () {\n            b.trigger(d.pageAnimationEnd, [e, b]), b.trigger(d.pageInit, [e, b]);\n        });\n    }, i.prototype._animateElement = function (a, b, c) {\n        \"undefined\" == typeof c && (c = g.rightToLeft);\n        var d,\n            e,\n            f = [\"page-from-center-to-left\", \"page-from-center-to-right\", \"page-from-right-to-center\", \"page-from-left-to-center\"].join(\" \");\n        switch (c) {\n            case g.rightToLeft:\n                d = \"page-from-center-to-left\", e = \"page-from-right-to-center\";\n                break;\n            case g.leftToRight:\n                d = \"page-from-center-to-right\", e = \"page-from-left-to-center\";\n                break;\n            default:\n                d = \"page-from-center-to-left\", e = \"page-from-right-to-center\";\n        }\n        a.removeClass(f).addClass(d), b.removeClass(f).addClass(e), a.animationEnd(function () {\n            a.removeClass(f);\n        }), b.animationEnd(function () {\n            b.removeClass(f);\n        });\n    }, i.prototype._getCurrentSection = function () {\n        return this.$view.find(\".\" + f.curPageClass).eq(0);\n    }, i.prototype._back = function (b, c) {\n        if (this._isTheSameDocument(b.url.full, c.url.full)) {\n            var d = a(\"#\" + b.pageId);\n            if (d.length) {\n                var e = this._getCurrentSection();\n                this._animateSection(e, d, g.leftToRight), this._saveAsCurrentState(b);\n            } else location.href = b.url.full;\n        } else this._saveDocumentIntoCache(a(document), c.url.full), this._switchToDocument(b.url.full, !1, !1, g.leftToRight), this._saveAsCurrentState(b);\n    }, i.prototype._forward = function (b, c) {\n        if (this._isTheSameDocument(b.url.full, c.url.full)) {\n            var d = a(\"#\" + b.pageId);\n            if (d.length) {\n                var e = this._getCurrentSection();\n                this._animateSection(e, d, g.rightToLeft), this._saveAsCurrentState(b);\n            } else location.href = b.url.full;\n        } else this._saveDocumentIntoCache(a(document), c.url.full), this._switchToDocument(b.url.full, !1, !1, g.rightToLeft), this._saveAsCurrentState(b);\n    }, i.prototype._onPopState = function (a) {\n        var b = a.state;\n        if (b && b.pageId) {\n            var c = this._getLastState();\n            return c ? void (b.id !== c.id && (b.id < c.id ? this._back(b, c) : this._forward(b, c))) : void (console.error && console.error(\"Missing last state when backward or forward\"));\n        }\n    }, i.prototype._pushNewState = function (a, b) {\n        var c = { id: this._getNextStateId(), pageId: b, url: e.toUrlObject(a) };\n        h.pushState(c, \"\", a), this._saveAsCurrentState(c), this._incMaxStateId();\n    }, i.prototype._generateRandomId = function () {\n        return \"page-\" + +new Date();\n    }, i.prototype.dispatch = function (a) {\n        var b = new CustomEvent(a, { bubbles: !0, cancelable: !0 });\n        window.dispatchEvent(b);\n    }, a(function () {\n        if (a.smConfig.router && e.supportStorage()) {\n            var d = a(\".\" + f.pageClass);\n            if (!d.length) {\n                var g = \"Disable router function because of no .page elements\";\n                return void (window.console && window.console.warn && console.warn(g));\n            }\n            var h = a.router = new i();\n            a(document).on(\"click\", \"a\", function (d) {\n                var e = a(d.currentTarget),\n                    f = c(e);\n                if (f && !b(e)) if (d.preventDefault(), e.hasClass(\"back\")) h.back();else {\n                    var g = e.attr(\"href\");\n                    if (!g || \"#\" === g) return;\n                    var i = \"true\" === e.attr(\"data-no-cache\");\n                    h.load(g, i);\n                }\n            });\n        }\n    });\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    a.lastPosition = function (b) {\n        function c(b, c) {\n            e.forEach(function (d, e) {\n                if (0 !== a(d).length) {\n                    var f = b,\n                        g = sessionStorage.getItem(f);\n                    c.find(d).scrollTop(parseInt(g));\n                }\n            });\n        }\n\n        function d(b, c) {\n            var d = b;\n            e.forEach(function (b, e) {\n                0 !== a(b).length && sessionStorage.setItem(d, c.find(b).scrollTop());\n            });\n        }\n\n        if (sessionStorage) {\n            var e = b.needMemoryClass || [];\n            a(window).off(\"beforePageSwitch\").on(\"beforePageSwitch\", function (a, b, c) {\n                d(b, c);\n            }), a(window).off(\"pageAnimationStart\").on(\"pageAnimationStart\", function (a, b, d) {\n                c(b, d);\n            });\n        }\n    };\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    var b = function b() {\n        var b = a(\".page-current\");\n        return b[0] || (b = a(\".page\").addClass(\"page-current\")), b;\n    };\n    a.initPage = function (c) {\n        var d = b();\n        d[0] || (d = a(document.body));\n        var e = d.hasClass(\"content\") ? d : d.find(\".content\");\n        e.scroller(), a.initPullToRefresh(e), a.initInfiniteScroll(e), a.initCalendar(e), a.initSwiper && a.initSwiper(e);\n    }, a.smConfig.showPageLoadingIndicator && (a(window).on(\"pageLoadStart\", function () {\n        a.showIndicator();\n    }), a(window).on(\"pageAnimationStart\", function () {\n        a.hideIndicator();\n    }), a(window).on(\"pageLoadCancel\", function () {\n        a.hideIndicator();\n    }), a(window).on(\"pageLoadComplete\", function () {\n        a.hideIndicator();\n    }), a(window).on(\"pageLoadError\", function () {\n        a.hideIndicator(), a.toast(\"加载失败\");\n    })), a(window).on(\"pageAnimationStart\", function (b, c, d) {\n        a.closeModal(), a.closePanel(), a(\"body\").removeClass(\"panel-closing\"), a.allowPanelOpen = !0;\n    }), a(window).on(\"pageInit\", function () {\n        a.hideIndicator(), a.lastPosition({ needMemoryClass: [\".content\"] });\n    }), window.addEventListener(\"pageshow\", function (a) {\n        a.persisted && location.reload();\n    }), a.init = function () {\n        var c = b(),\n            d = c[0].id;\n        a.initPage(), c.trigger(\"pageInit\", [d, c]);\n    }, a(function () {\n        FastClick.attach(document.body), a.smConfig.autoInit && a.init(), a(document).on(\"pageInitInternal\", function (b, c, d) {\n            a.init();\n        });\n    });\n}(Zepto), +function (a) {\n    \"use strict\";\n\n    if (a.device.ios) {\n        var b = function b(a) {\n            var b, c;\n            a = a || document.querySelector(a), a && a.addEventListener(\"touchstart\", function (d) {\n                b = d.touches[0].pageY, c = a.scrollTop, 0 >= c && (a.scrollTop = 1), c + a.offsetHeight >= a.scrollHeight && (a.scrollTop = a.scrollHeight - a.offsetHeight - 1);\n            }, !1);\n        },\n            c = function c() {\n            var c = a(\".page-current\").length > 0 ? \".page-current \" : \"\",\n                d = a(c + \".content\");\n            new b(d[0]);\n        };\n        a(document).on(a.touchEvents.move, \".page-current .bar\", function () {\n            event.preventDefault();\n        }), a(document).on(\"pageLoadComplete\", function () {\n            c();\n        }), a(document).on(\"pageAnimationEnd\", function () {\n            c();\n        }), c();\n    }\n}(Zepto);"

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)(__webpack_require__(12))

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "\"use strict\";\n\nvar _typeof = typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; };\n\n+function (a) {\n  \"use strict\";\n  var b = function b(c, d) {\n    function e() {\n      return \"horizontal\" === o.params.direction;\n    }function f() {\n      o.autoplayTimeoutId = setTimeout(function () {\n        o.params.loop ? (o.fixLoop(), o._slideNext()) : o.isEnd ? d.autoplayStopOnLast ? o.stopAutoplay() : o._slideTo(0) : o._slideNext();\n      }, o.params.autoplay);\n    }function g(b, c) {\n      var d = a(b.target);if (!d.is(c)) if (\"string\" == typeof c) d = d.parents(c);else if (c.nodeType) {\n        var e;return d.parents().each(function (a, b) {\n          b === c && (e = c);\n        }), e ? c : void 0;\n      }if (0 !== d.length) return d[0];\n    }function h(a, b) {\n      b = b || {};var c = window.MutationObserver || window.WebkitMutationObserver,\n          d = new c(function (a) {\n        a.forEach(function (a) {\n          o.onResize(), o.emit(\"onObserverUpdate\", o, a);\n        });\n      });d.observe(a, { attributes: \"undefined\" == typeof b.attributes ? !0 : b.attributes, childList: \"undefined\" == typeof b.childList ? !0 : b.childList, characterData: \"undefined\" == typeof b.characterData ? !0 : b.characterData }), o.observers.push(d);\n    }function i(b, c) {\n      b = a(b);var d, f, g;d = b.attr(\"data-swiper-parallax\") || \"0\", f = b.attr(\"data-swiper-parallax-x\"), g = b.attr(\"data-swiper-parallax-y\"), f || g ? (f = f || \"0\", g = g || \"0\") : e() ? (f = d, g = \"0\") : (g = d, f = \"0\"), f = f.indexOf(\"%\") >= 0 ? parseInt(f, 10) * c + \"%\" : f * c + \"px\", g = g.indexOf(\"%\") >= 0 ? parseInt(g, 10) * c + \"%\" : g * c + \"px\", b.transform(\"translate3d(\" + f + \", \" + g + \",0px)\");\n    }function j(a) {\n      return 0 !== a.indexOf(\"on\") && (a = a[0] !== a[0].toUpperCase() ? \"on\" + a[0].toUpperCase() + a.substring(1) : \"on\" + a), a;\n    }var k = this.defaults,\n        l = d && d.virtualTranslate;d = d || {};for (var m in k) {\n      if (\"undefined\" == typeof d[m]) d[m] = k[m];else if (\"object\" == _typeof(d[m])) for (var n in k[m]) {\n        \"undefined\" == typeof d[m][n] && (d[m][n] = k[m][n]);\n      }\n    }var o = this;if (o.params = d, o.classNames = [], o.$ = a, o.container = a(c), 0 !== o.container.length) {\n      if (o.container.length > 1) return void o.container.each(function () {\n        new a.Swiper(this, d);\n      });o.container[0].swiper = o, o.container.data(\"swiper\", o), o.classNames.push(\"swiper-container-\" + o.params.direction), o.params.freeMode && o.classNames.push(\"swiper-container-free-mode\"), o.support.flexbox || (o.classNames.push(\"swiper-container-no-flexbox\"), o.params.slidesPerColumn = 1), (o.params.parallax || o.params.watchSlidesVisibility) && (o.params.watchSlidesProgress = !0), [\"cube\", \"coverflow\"].indexOf(o.params.effect) >= 0 && (o.support.transforms3d ? (o.params.watchSlidesProgress = !0, o.classNames.push(\"swiper-container-3d\")) : o.params.effect = \"slide\"), \"slide\" !== o.params.effect && o.classNames.push(\"swiper-container-\" + o.params.effect), \"cube\" === o.params.effect && (o.params.resistanceRatio = 0, o.params.slidesPerView = 1, o.params.slidesPerColumn = 1, o.params.slidesPerGroup = 1, o.params.centeredSlides = !1, o.params.spaceBetween = 0, o.params.virtualTranslate = !0, o.params.setWrapperSize = !1), \"fade\" === o.params.effect && (o.params.slidesPerView = 1, o.params.slidesPerColumn = 1, o.params.slidesPerGroup = 1, o.params.watchSlidesProgress = !0, o.params.spaceBetween = 0, \"undefined\" == typeof l && (o.params.virtualTranslate = !0)), o.params.grabCursor && o.support.touch && (o.params.grabCursor = !1), o.wrapper = o.container.children(\".\" + o.params.wrapperClass), o.params.pagination && (o.paginationContainer = a(o.params.pagination), o.params.paginationClickable && o.paginationContainer.addClass(\"swiper-pagination-clickable\")), o.rtl = e() && (\"rtl\" === o.container[0].dir.toLowerCase() || \"rtl\" === o.container.css(\"direction\")), o.rtl && o.classNames.push(\"swiper-container-rtl\"), o.rtl && (o.wrongRTL = \"-webkit-box\" === o.wrapper.css(\"display\")), o.params.slidesPerColumn > 1 && o.classNames.push(\"swiper-container-multirow\"), o.device.android && o.classNames.push(\"swiper-container-android\"), o.container.addClass(o.classNames.join(\" \")), o.translate = 0, o.progress = 0, o.velocity = 0, o.lockSwipeToNext = function () {\n        o.params.allowSwipeToNext = !1;\n      }, o.lockSwipeToPrev = function () {\n        o.params.allowSwipeToPrev = !1;\n      }, o.lockSwipes = function () {\n        o.params.allowSwipeToNext = o.params.allowSwipeToPrev = !1;\n      }, o.unlockSwipeToNext = function () {\n        o.params.allowSwipeToNext = !0;\n      }, o.unlockSwipeToPrev = function () {\n        o.params.allowSwipeToPrev = !0;\n      }, o.unlockSwipes = function () {\n        o.params.allowSwipeToNext = o.params.allowSwipeToPrev = !0;\n      }, o.params.grabCursor && (o.container[0].style.cursor = \"move\", o.container[0].style.cursor = \"-webkit-grab\", o.container[0].style.cursor = \"-moz-grab\", o.container[0].style.cursor = \"grab\"), o.imagesToLoad = [], o.imagesLoaded = 0, o.loadImage = function (a, b, c, d) {\n        function e() {\n          d && d();\n        }var f;a.complete && c ? e() : b ? (f = new Image(), f.onload = e, f.onerror = e, f.src = b) : e();\n      }, o.preloadImages = function () {\n        function a() {\n          \"undefined\" != typeof o && null !== o && (void 0 !== o.imagesLoaded && o.imagesLoaded++, o.imagesLoaded === o.imagesToLoad.length && (o.params.updateOnImagesReady && o.update(), o.emit(\"onImagesReady\", o)));\n        }o.imagesToLoad = o.container.find(\"img\");for (var b = 0; b < o.imagesToLoad.length; b++) {\n          o.loadImage(o.imagesToLoad[b], o.imagesToLoad[b].currentSrc || o.imagesToLoad[b].getAttribute(\"src\"), !0, a);\n        }\n      }, o.autoplayTimeoutId = void 0, o.autoplaying = !1, o.autoplayPaused = !1, o.startAutoplay = function () {\n        return \"undefined\" != typeof o.autoplayTimeoutId ? !1 : o.params.autoplay ? o.autoplaying ? !1 : (o.autoplaying = !0, o.emit(\"onAutoplayStart\", o), void f()) : !1;\n      }, o.stopAutoplay = function () {\n        o.autoplayTimeoutId && (o.autoplayTimeoutId && clearTimeout(o.autoplayTimeoutId), o.autoplaying = !1, o.autoplayTimeoutId = void 0, o.emit(\"onAutoplayStop\", o));\n      }, o.pauseAutoplay = function (a) {\n        o.autoplayPaused || (o.autoplayTimeoutId && clearTimeout(o.autoplayTimeoutId), o.autoplayPaused = !0, 0 === a ? (o.autoplayPaused = !1, f()) : o.wrapper.transitionEnd(function () {\n          o.autoplayPaused = !1, o.autoplaying ? f() : o.stopAutoplay();\n        }));\n      }, o.minTranslate = function () {\n        return -o.snapGrid[0];\n      }, o.maxTranslate = function () {\n        return -o.snapGrid[o.snapGrid.length - 1];\n      }, o.updateContainerSize = function () {\n        o.width = o.container[0].clientWidth, o.height = o.container[0].clientHeight, o.size = e() ? o.width : o.height;\n      }, o.updateSlidesSize = function () {\n        o.slides = o.wrapper.children(\".\" + o.params.slideClass), o.snapGrid = [], o.slidesGrid = [], o.slidesSizesGrid = [];var a,\n            b = o.params.spaceBetween,\n            c = 0,\n            d = 0,\n            f = 0;\"string\" == typeof b && b.indexOf(\"%\") >= 0 && (b = parseFloat(b.replace(\"%\", \"\")) / 100 * o.size), o.virtualSize = -b, o.rtl ? o.slides.css({ marginLeft: \"\", marginTop: \"\" }) : o.slides.css({ marginRight: \"\", marginBottom: \"\" });var g;o.params.slidesPerColumn > 1 && (g = Math.floor(o.slides.length / o.params.slidesPerColumn) === o.slides.length / o.params.slidesPerColumn ? o.slides.length : Math.ceil(o.slides.length / o.params.slidesPerColumn) * o.params.slidesPerColumn);var h;for (a = 0; a < o.slides.length; a++) {\n          h = 0;var i = o.slides.eq(a);if (o.params.slidesPerColumn > 1) {\n            var j,\n                k,\n                l,\n                m,\n                n = o.params.slidesPerColumn;\"column\" === o.params.slidesPerColumnFill ? (k = Math.floor(a / n), l = a - k * n, j = k + l * g / n, i.css({ \"-webkit-box-ordinal-group\": j, \"-moz-box-ordinal-group\": j, \"-ms-flex-order\": j, \"-webkit-order\": j, order: j })) : (m = g / n, l = Math.floor(a / m), k = a - l * m), i.css({ \"margin-top\": 0 !== l && o.params.spaceBetween && o.params.spaceBetween + \"px\" }).attr(\"data-swiper-column\", k).attr(\"data-swiper-row\", l);\n          }\"none\" !== i.css(\"display\") && (\"auto\" === o.params.slidesPerView ? h = e() ? i.outerWidth(!0) : i.outerHeight(!0) : (h = (o.size - (o.params.slidesPerView - 1) * b) / o.params.slidesPerView, e() ? o.slides[a].style.width = h + \"px\" : o.slides[a].style.height = h + \"px\"), o.slides[a].swiperSlideSize = h, o.slidesSizesGrid.push(h), o.params.centeredSlides ? (c = c + h / 2 + d / 2 + b, 0 === a && (c = c - o.size / 2 - b), Math.abs(c) < .001 && (c = 0), f % o.params.slidesPerGroup === 0 && o.snapGrid.push(c), o.slidesGrid.push(c)) : (f % o.params.slidesPerGroup === 0 && o.snapGrid.push(c), o.slidesGrid.push(c), c = c + h + b), o.virtualSize += h + b, d = h, f++);\n        }o.virtualSize = Math.max(o.virtualSize, o.size);var p;if (o.rtl && o.wrongRTL && (\"slide\" === o.params.effect || \"coverflow\" === o.params.effect) && o.wrapper.css({ width: o.virtualSize + o.params.spaceBetween + \"px\" }), o.support.flexbox && !o.params.setWrapperSize || (e() ? o.wrapper.css({ width: o.virtualSize + o.params.spaceBetween + \"px\" }) : o.wrapper.css({ height: o.virtualSize + o.params.spaceBetween + \"px\" })), o.params.slidesPerColumn > 1 && (o.virtualSize = (h + o.params.spaceBetween) * g, o.virtualSize = Math.ceil(o.virtualSize / o.params.slidesPerColumn) - o.params.spaceBetween, o.wrapper.css({ width: o.virtualSize + o.params.spaceBetween + \"px\" }), o.params.centeredSlides)) {\n          for (p = [], a = 0; a < o.snapGrid.length; a++) {\n            o.snapGrid[a] < o.virtualSize + o.snapGrid[0] && p.push(o.snapGrid[a]);\n          }o.snapGrid = p;\n        }if (!o.params.centeredSlides) {\n          for (p = [], a = 0; a < o.snapGrid.length; a++) {\n            o.snapGrid[a] <= o.virtualSize - o.size && p.push(o.snapGrid[a]);\n          }o.snapGrid = p, Math.floor(o.virtualSize - o.size) > Math.floor(o.snapGrid[o.snapGrid.length - 1]) && o.snapGrid.push(o.virtualSize - o.size);\n        }0 === o.snapGrid.length && (o.snapGrid = [0]), 0 !== o.params.spaceBetween && (e() ? o.rtl ? o.slides.css({ marginLeft: b + \"px\" }) : o.slides.css({ marginRight: b + \"px\" }) : o.slides.css({ marginBottom: b + \"px\" })), o.params.watchSlidesProgress && o.updateSlidesOffset();\n      }, o.updateSlidesOffset = function () {\n        for (var a = 0; a < o.slides.length; a++) {\n          o.slides[a].swiperSlideOffset = e() ? o.slides[a].offsetLeft : o.slides[a].offsetTop;\n        }\n      }, o.updateSlidesProgress = function (a) {\n        if (\"undefined\" == typeof a && (a = o.translate || 0), 0 !== o.slides.length) {\n          \"undefined\" == typeof o.slides[0].swiperSlideOffset && o.updateSlidesOffset();var b = o.params.centeredSlides ? -a + o.size / 2 : -a;o.rtl && (b = o.params.centeredSlides ? a - o.size / 2 : a), o.slides.removeClass(o.params.slideVisibleClass);for (var c = 0; c < o.slides.length; c++) {\n            var d = o.slides[c],\n                e = o.params.centeredSlides === !0 ? d.swiperSlideSize / 2 : 0,\n                f = (b - d.swiperSlideOffset - e) / (d.swiperSlideSize + o.params.spaceBetween);if (o.params.watchSlidesVisibility) {\n              var g = -(b - d.swiperSlideOffset - e),\n                  h = g + o.slidesSizesGrid[c],\n                  i = g >= 0 && g < o.size || h > 0 && h <= o.size || 0 >= g && h >= o.size;i && o.slides.eq(c).addClass(o.params.slideVisibleClass);\n            }d.progress = o.rtl ? -f : f;\n          }\n        }\n      }, o.updateProgress = function (a) {\n        \"undefined\" == typeof a && (a = o.translate || 0);var b = o.maxTranslate() - o.minTranslate();0 === b ? (o.progress = 0, o.isBeginning = o.isEnd = !0) : (o.progress = (a - o.minTranslate()) / b, o.isBeginning = o.progress <= 0, o.isEnd = o.progress >= 1), o.isBeginning && o.emit(\"onReachBeginning\", o), o.isEnd && o.emit(\"onReachEnd\", o), o.params.watchSlidesProgress && o.updateSlidesProgress(a), o.emit(\"onProgress\", o, o.progress);\n      }, o.updateActiveIndex = function () {\n        var a,\n            b,\n            c,\n            d = o.rtl ? o.translate : -o.translate;for (b = 0; b < o.slidesGrid.length; b++) {\n          \"undefined\" != typeof o.slidesGrid[b + 1] ? d >= o.slidesGrid[b] && d < o.slidesGrid[b + 1] - (o.slidesGrid[b + 1] - o.slidesGrid[b]) / 2 ? a = b : d >= o.slidesGrid[b] && d < o.slidesGrid[b + 1] && (a = b + 1) : d >= o.slidesGrid[b] && (a = b);\n        }(0 > a || \"undefined\" == typeof a) && (a = 0), c = Math.floor(a / o.params.slidesPerGroup), c >= o.snapGrid.length && (c = o.snapGrid.length - 1), a !== o.activeIndex && (o.snapIndex = c, o.previousIndex = o.activeIndex, o.activeIndex = a, o.updateClasses());\n      }, o.updateClasses = function () {\n        o.slides.removeClass(o.params.slideActiveClass + \" \" + o.params.slideNextClass + \" \" + o.params.slidePrevClass);var b = o.slides.eq(o.activeIndex);if (b.addClass(o.params.slideActiveClass), b.next(\".\" + o.params.slideClass).addClass(o.params.slideNextClass), b.prev(\".\" + o.params.slideClass).addClass(o.params.slidePrevClass), o.bullets && o.bullets.length > 0) {\n          o.bullets.removeClass(o.params.bulletActiveClass);var c;o.params.loop ? (c = Math.ceil(o.activeIndex - o.loopedSlides) / o.params.slidesPerGroup, c > o.slides.length - 1 - 2 * o.loopedSlides && (c -= o.slides.length - 2 * o.loopedSlides), c > o.bullets.length - 1 && (c -= o.bullets.length)) : c = \"undefined\" != typeof o.snapIndex ? o.snapIndex : o.activeIndex || 0, o.paginationContainer.length > 1 ? o.bullets.each(function () {\n            a(this).index() === c && a(this).addClass(o.params.bulletActiveClass);\n          }) : o.bullets.eq(c).addClass(o.params.bulletActiveClass);\n        }o.params.loop || (o.params.prevButton && (o.isBeginning ? (a(o.params.prevButton).addClass(o.params.buttonDisabledClass), o.params.a11y && o.a11y && o.a11y.disable(a(o.params.prevButton))) : (a(o.params.prevButton).removeClass(o.params.buttonDisabledClass), o.params.a11y && o.a11y && o.a11y.enable(a(o.params.prevButton)))), o.params.nextButton && (o.isEnd ? (a(o.params.nextButton).addClass(o.params.buttonDisabledClass), o.params.a11y && o.a11y && o.a11y.disable(a(o.params.nextButton))) : (a(o.params.nextButton).removeClass(o.params.buttonDisabledClass), o.params.a11y && o.a11y && o.a11y.enable(a(o.params.nextButton)))));\n      }, o.updatePagination = function () {\n        if (o.params.pagination && o.paginationContainer && o.paginationContainer.length > 0) {\n          for (var a = \"\", b = o.params.loop ? Math.ceil((o.slides.length - 2 * o.loopedSlides) / o.params.slidesPerGroup) : o.snapGrid.length, c = 0; b > c; c++) {\n            a += o.params.paginationBulletRender ? o.params.paginationBulletRender(c, o.params.bulletClass) : '<span class=\"' + o.params.bulletClass + '\"></span>';\n          }o.paginationContainer.html(a), o.bullets = o.paginationContainer.find(\".\" + o.params.bulletClass);\n        }\n      }, o.update = function (a) {\n        function b() {\n          d = Math.min(Math.max(o.translate, o.maxTranslate()), o.minTranslate()), o.setWrapperTranslate(d), o.updateActiveIndex(), o.updateClasses();\n        }if (o.updateContainerSize(), o.updateSlidesSize(), o.updateProgress(), o.updatePagination(), o.updateClasses(), o.params.scrollbar && o.scrollbar && o.scrollbar.set(), a) {\n          var c, d;o.params.freeMode ? b() : (c = \"auto\" === o.params.slidesPerView && o.isEnd && !o.params.centeredSlides ? o.slideTo(o.slides.length - 1, 0, !1, !0) : o.slideTo(o.activeIndex, 0, !1, !0), c || b());\n        }\n      }, o.onResize = function () {\n        if (o.updateContainerSize(), o.updateSlidesSize(), o.updateProgress(), (\"auto\" === o.params.slidesPerView || o.params.freeMode) && o.updatePagination(), o.params.scrollbar && o.scrollbar && o.scrollbar.set(), o.params.freeMode) {\n          var a = Math.min(Math.max(o.translate, o.maxTranslate()), o.minTranslate());o.setWrapperTranslate(a), o.updateActiveIndex(), o.updateClasses();\n        } else o.updateClasses(), \"auto\" === o.params.slidesPerView && o.isEnd && !o.params.centeredSlides ? o.slideTo(o.slides.length - 1, 0, !1, !0) : o.slideTo(o.activeIndex, 0, !1, !0);\n      };var p = [\"mousedown\", \"mousemove\", \"mouseup\"];window.navigator.pointerEnabled ? p = [\"pointerdown\", \"pointermove\", \"pointerup\"] : window.navigator.msPointerEnabled && (p = [\"MSPointerDown\", \"MSPointerMove\", \"MSPointerUp\"]), o.touchEvents = { start: o.support.touch || !o.params.simulateTouch ? \"touchstart\" : p[0], move: o.support.touch || !o.params.simulateTouch ? \"touchmove\" : p[1], end: o.support.touch || !o.params.simulateTouch ? \"touchend\" : p[2] }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && (\"container\" === o.params.touchEventsTarget ? o.container : o.wrapper).addClass(\"swiper-wp8-\" + o.params.direction), o.initEvents = function (b) {\n        var c = b ? \"off\" : \"on\",\n            e = b ? \"removeEventListener\" : \"addEventListener\",\n            f = \"container\" === o.params.touchEventsTarget ? o.container[0] : o.wrapper[0],\n            g = o.support.touch ? f : document,\n            h = !!o.params.nested;o.browser.ie ? (f[e](o.touchEvents.start, o.onTouchStart, !1), g[e](o.touchEvents.move, o.onTouchMove, h), g[e](o.touchEvents.end, o.onTouchEnd, !1)) : (o.support.touch && (f[e](o.touchEvents.start, o.onTouchStart, !1), f[e](o.touchEvents.move, o.onTouchMove, h), f[e](o.touchEvents.end, o.onTouchEnd, !1)), !d.simulateTouch || o.device.ios || o.device.android || (f[e](\"mousedown\", o.onTouchStart, !1), g[e](\"mousemove\", o.onTouchMove, h), g[e](\"mouseup\", o.onTouchEnd, !1))), window[e](\"resize\", o.onResize), o.params.nextButton && (a(o.params.nextButton)[c](\"click\", o.onClickNext), o.params.a11y && o.a11y && a(o.params.nextButton)[c](\"keydown\", o.a11y.onEnterKey)), o.params.prevButton && (a(o.params.prevButton)[c](\"click\", o.onClickPrev), o.params.a11y && o.a11y && a(o.params.prevButton)[c](\"keydown\", o.a11y.onEnterKey)), o.params.pagination && o.params.paginationClickable && a(o.paginationContainer)[c](\"click\", \".\" + o.params.bulletClass, o.onClickIndex), (o.params.preventClicks || o.params.preventClicksPropagation) && f[e](\"click\", o.preventClicks, !0);\n      }, o.attachEvents = function () {\n        o.initEvents();\n      }, o.detachEvents = function () {\n        o.initEvents(!0);\n      }, o.allowClick = !0, o.preventClicks = function (a) {\n        o.allowClick || (o.params.preventClicks && a.preventDefault(), o.params.preventClicksPropagation && (a.stopPropagation(), a.stopImmediatePropagation()));\n      }, o.onClickNext = function (a) {\n        a.preventDefault(), o.slideNext();\n      }, o.onClickPrev = function (a) {\n        a.preventDefault(), o.slidePrev();\n      }, o.onClickIndex = function (b) {\n        b.preventDefault();var c = a(this).index() * o.params.slidesPerGroup;o.params.loop && (c += o.loopedSlides), o.slideTo(c);\n      }, o.updateClickedSlide = function (b) {\n        var c = g(b, \".\" + o.params.slideClass);if (!c) return o.clickedSlide = void 0, void (o.clickedIndex = void 0);if (o.clickedSlide = c, o.clickedIndex = a(c).index(), o.params.slideToClickedSlide && void 0 !== o.clickedIndex && o.clickedIndex !== o.activeIndex) {\n          var d,\n              e = o.clickedIndex;if (o.params.loop) {\n            if (d = a(o.clickedSlide).attr(\"data-swiper-slide-index\"), e > o.slides.length - o.params.slidesPerView) o.fixLoop(), e = o.wrapper.children(\".\" + o.params.slideClass + '[data-swiper-slide-index=\"' + d + '\"]').eq(0).index(), setTimeout(function () {\n              o.slideTo(e);\n            }, 0);else if (e < o.params.slidesPerView - 1) {\n              o.fixLoop();var f = o.wrapper.children(\".\" + o.params.slideClass + '[data-swiper-slide-index=\"' + d + '\"]');e = f.eq(f.length - 1).index(), setTimeout(function () {\n                o.slideTo(e);\n              }, 0);\n            } else o.slideTo(e);\n          } else o.slideTo(e);\n        }\n      };var q,\n          r,\n          s,\n          t,\n          u,\n          v,\n          w,\n          x,\n          y,\n          z = \"input, select, textarea, button\",\n          A = Date.now(),\n          B = [];o.animating = !1, o.touches = { startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0 };var C, D;o.onTouchStart = function (b) {\n        if (b.originalEvent && (b = b.originalEvent), C = \"touchstart\" === b.type, C || !(\"which\" in b) || 3 !== b.which) {\n          if (o.params.noSwiping && g(b, \".\" + o.params.noSwipingClass)) return void (o.allowClick = !0);if (!o.params.swipeHandler || g(b, o.params.swipeHandler)) {\n            if (q = !0, r = !1, t = void 0, D = void 0, o.touches.startX = o.touches.currentX = \"touchstart\" === b.type ? b.targetTouches[0].pageX : b.pageX, o.touches.startY = o.touches.currentY = \"touchstart\" === b.type ? b.targetTouches[0].pageY : b.pageY, s = Date.now(), o.allowClick = !0, o.updateContainerSize(), o.swipeDirection = void 0, o.params.threshold > 0 && (w = !1), \"touchstart\" !== b.type) {\n              var c = !0;a(b.target).is(z) && (c = !1), document.activeElement && a(document.activeElement).is(z) && document.activeElement.blur(), c && b.preventDefault();\n            }o.emit(\"onTouchStart\", o, b);\n          }\n        }\n      }, o.onTouchMove = function (b) {\n        if (b.originalEvent && (b = b.originalEvent), !(C && \"mousemove\" === b.type || b.preventedByNestedSwiper)) {\n          if (o.params.onlyExternal) return r = !0, void (o.allowClick = !1);if (C && document.activeElement && b.target === document.activeElement && a(b.target).is(z)) return r = !0, void (o.allowClick = !1);if (o.emit(\"onTouchMove\", o, b), !(b.targetTouches && b.targetTouches.length > 1)) {\n            if (o.touches.currentX = \"touchmove\" === b.type ? b.targetTouches[0].pageX : b.pageX, o.touches.currentY = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY, \"undefined\" == typeof t) {\n              var c = 180 * Math.atan2(Math.abs(o.touches.currentY - o.touches.startY), Math.abs(o.touches.currentX - o.touches.startX)) / Math.PI;t = e() ? c > o.params.touchAngle : 90 - c > o.params.touchAngle;\n            }if (t && o.emit(\"onTouchMoveOpposite\", o, b), \"undefined\" == typeof D && o.browser.ieTouch && (o.touches.currentX === o.touches.startX && o.touches.currentY === o.touches.startY || (D = !0)), q) {\n              if (t) return void (q = !1);if (D || !o.browser.ieTouch) {\n                o.allowClick = !1, o.emit(\"onSliderMove\", o, b), b.preventDefault(), o.params.touchMoveStopPropagation && !o.params.nested && b.stopPropagation(), r || (d.loop && o.fixLoop(), v = o.getWrapperTranslate(), o.setWrapperTransition(0), o.animating && o.wrapper.trigger(\"webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd\"), o.params.autoplay && o.autoplaying && (o.params.autoplayDisableOnInteraction ? o.stopAutoplay() : o.pauseAutoplay()), y = !1, o.params.grabCursor && (o.container[0].style.cursor = \"move\", o.container[0].style.cursor = \"-webkit-grabbing\", o.container[0].style.cursor = \"-moz-grabbin\", o.container[0].style.cursor = \"grabbing\")), r = !0;var f = o.touches.diff = e() ? o.touches.currentX - o.touches.startX : o.touches.currentY - o.touches.startY;f *= o.params.touchRatio, o.rtl && (f = -f), o.swipeDirection = f > 0 ? \"prev\" : \"next\", u = f + v;var g = !0;if (f > 0 && u > o.minTranslate() ? (g = !1, o.params.resistance && (u = o.minTranslate() - 1 + Math.pow(-o.minTranslate() + v + f, o.params.resistanceRatio))) : 0 > f && u < o.maxTranslate() && (g = !1, o.params.resistance && (u = o.maxTranslate() + 1 - Math.pow(o.maxTranslate() - v - f, o.params.resistanceRatio))), g && (b.preventedByNestedSwiper = !0), !o.params.allowSwipeToNext && \"next\" === o.swipeDirection && v > u && (u = v), !o.params.allowSwipeToPrev && \"prev\" === o.swipeDirection && u > v && (u = v), o.params.followFinger) {\n                  if (o.params.threshold > 0) {\n                    if (!(Math.abs(f) > o.params.threshold || w)) return void (u = v);if (!w) return w = !0, o.touches.startX = o.touches.currentX, o.touches.startY = o.touches.currentY, u = v, void (o.touches.diff = e() ? o.touches.currentX - o.touches.startX : o.touches.currentY - o.touches.startY);\n                  }(o.params.freeMode || o.params.watchSlidesProgress) && o.updateActiveIndex(), o.params.freeMode && (0 === B.length && B.push({ position: o.touches[e() ? \"startX\" : \"startY\"], time: s }), B.push({ position: o.touches[e() ? \"currentX\" : \"currentY\"], time: new Date().getTime() })), o.updateProgress(u), o.setWrapperTranslate(u);\n                }\n              }\n            }\n          }\n        }\n      }, o.onTouchEnd = function (b) {\n        if (b.originalEvent && (b = b.originalEvent), o.emit(\"onTouchEnd\", o, b), q) {\n          o.params.grabCursor && r && q && (o.container[0].style.cursor = \"move\", o.container[0].style.cursor = \"-webkit-grab\", o.container[0].style.cursor = \"-moz-grab\", o.container[0].style.cursor = \"grab\");var c = Date.now(),\n              d = c - s;if (o.allowClick && (o.updateClickedSlide(b), o.emit(\"onTap\", o, b), 300 > d && c - A > 300 && (x && clearTimeout(x), x = setTimeout(function () {\n            o && (o.params.paginationHide && o.paginationContainer.length > 0 && !a(b.target).hasClass(o.params.bulletClass) && o.paginationContainer.toggleClass(o.params.paginationHiddenClass), o.emit(\"onClick\", o, b));\n          }, 300)), 300 > d && 300 > c - A && (x && clearTimeout(x), o.emit(\"onDoubleTap\", o, b))), A = Date.now(), setTimeout(function () {\n            o && o.allowClick && (o.allowClick = !0);\n          }, 0), !q || !r || !o.swipeDirection || 0 === o.touches.diff || u === v) return void (q = r = !1);q = r = !1;var e;if (e = o.params.followFinger ? o.rtl ? o.translate : -o.translate : -u, o.params.freeMode) {\n            if (e < -o.minTranslate()) return void o.slideTo(o.activeIndex);if (e > -o.maxTranslate()) return void o.slideTo(o.slides.length - 1);if (o.params.freeModeMomentum) {\n              if (B.length > 1) {\n                var f = B.pop(),\n                    g = B.pop(),\n                    h = f.position - g.position,\n                    i = f.time - g.time;o.velocity = h / i, o.velocity = o.velocity / 2, Math.abs(o.velocity) < .02 && (o.velocity = 0), (i > 150 || new Date().getTime() - f.time > 300) && (o.velocity = 0);\n              } else o.velocity = 0;B.length = 0;var j = 1e3 * o.params.freeModeMomentumRatio,\n                  k = o.velocity * j,\n                  l = o.translate + k;o.rtl && (l = -l);var m,\n                  n = !1,\n                  p = 20 * Math.abs(o.velocity) * o.params.freeModeMomentumBounceRatio;l < o.maxTranslate() && (o.params.freeModeMomentumBounce ? (l + o.maxTranslate() < -p && (l = o.maxTranslate() - p), m = o.maxTranslate(), n = !0, y = !0) : l = o.maxTranslate()), l > o.minTranslate() && (o.params.freeModeMomentumBounce ? (l - o.minTranslate() > p && (l = o.minTranslate() + p), m = o.minTranslate(), n = !0, y = !0) : l = o.minTranslate()), 0 !== o.velocity && (j = o.rtl ? Math.abs((-l - o.translate) / o.velocity) : Math.abs((l - o.translate) / o.velocity)), o.params.freeModeMomentumBounce && n ? (o.updateProgress(m), o.setWrapperTransition(j), o.setWrapperTranslate(l), o.onTransitionStart(), o.animating = !0, o.wrapper.transitionEnd(function () {\n                y && (o.emit(\"onMomentumBounce\", o), o.setWrapperTransition(o.params.speed), o.setWrapperTranslate(m), o.wrapper.transitionEnd(function () {\n                  o.onTransitionEnd();\n                }));\n              })) : o.velocity ? (o.updateProgress(l), o.setWrapperTransition(j), o.setWrapperTranslate(l), o.onTransitionStart(), o.animating || (o.animating = !0, o.wrapper.transitionEnd(function () {\n                o.onTransitionEnd();\n              }))) : o.updateProgress(l), o.updateActiveIndex();\n            }return void ((!o.params.freeModeMomentum || d >= o.params.longSwipesMs) && (o.updateProgress(), o.updateActiveIndex()));\n          }var t,\n              w = 0,\n              z = o.slidesSizesGrid[0];for (t = 0; t < o.slidesGrid.length; t += o.params.slidesPerGroup) {\n            \"undefined\" != typeof o.slidesGrid[t + o.params.slidesPerGroup] ? e >= o.slidesGrid[t] && e < o.slidesGrid[t + o.params.slidesPerGroup] && (w = t, z = o.slidesGrid[t + o.params.slidesPerGroup] - o.slidesGrid[t]) : e >= o.slidesGrid[t] && (w = t, z = o.slidesGrid[o.slidesGrid.length - 1] - o.slidesGrid[o.slidesGrid.length - 2]);\n          }var C = (e - o.slidesGrid[w]) / z;if (d > o.params.longSwipesMs) {\n            if (!o.params.longSwipes) return void o.slideTo(o.activeIndex);\"next\" === o.swipeDirection && (C >= o.params.longSwipesRatio ? o.slideTo(w + o.params.slidesPerGroup) : o.slideTo(w)), \"prev\" === o.swipeDirection && (C > 1 - o.params.longSwipesRatio ? o.slideTo(w + o.params.slidesPerGroup) : o.slideTo(w));\n          } else {\n            if (!o.params.shortSwipes) return void o.slideTo(o.activeIndex);\"next\" === o.swipeDirection && o.slideTo(w + o.params.slidesPerGroup), \"prev\" === o.swipeDirection && o.slideTo(w);\n          }\n        }\n      }, o._slideTo = function (a, b) {\n        return o.slideTo(a, b, !0, !0);\n      }, o.slideTo = function (a, b, c, d) {\n        \"undefined\" == typeof c && (c = !0), \"undefined\" == typeof a && (a = 0), 0 > a && (a = 0), o.snapIndex = Math.floor(a / o.params.slidesPerGroup), o.snapIndex >= o.snapGrid.length && (o.snapIndex = o.snapGrid.length - 1);var e = -o.snapGrid[o.snapIndex];o.params.autoplay && o.autoplaying && (d || !o.params.autoplayDisableOnInteraction ? o.pauseAutoplay(b) : o.stopAutoplay()), o.updateProgress(e);for (var f = 0; f < o.slidesGrid.length; f++) {\n          -e >= o.slidesGrid[f] && (a = f);\n        }return \"undefined\" == typeof b && (b = o.params.speed), o.previousIndex = o.activeIndex || 0, o.activeIndex = a, e === o.translate ? (o.updateClasses(), !1) : (o.onTransitionStart(c), 0 === b ? (o.setWrapperTransition(0), o.setWrapperTranslate(e), o.onTransitionEnd(c)) : (o.setWrapperTransition(b), o.setWrapperTranslate(e), o.animating || (o.animating = !0, o.wrapper.transitionEnd(function () {\n          o.onTransitionEnd(c);\n        }))), o.updateClasses(), !0);\n      }, o.onTransitionStart = function (a) {\n        \"undefined\" == typeof a && (a = !0), o.lazy && o.lazy.onTransitionStart(), a && (o.emit(\"onTransitionStart\", o), o.activeIndex !== o.previousIndex && o.emit(\"onSlideChangeStart\", o));\n      }, o.onTransitionEnd = function (a) {\n        o.animating = !1, o.setWrapperTransition(0), \"undefined\" == typeof a && (a = !0), o.lazy && o.lazy.onTransitionEnd(), a && (o.emit(\"onTransitionEnd\", o), o.activeIndex !== o.previousIndex && o.emit(\"onSlideChangeEnd\", o)), o.params.hashnav && o.hashnav && o.hashnav.setHash();\n      }, o.slideNext = function (a, b, c) {\n        return o.params.loop ? o.animating ? !1 : (o.fixLoop(), o.slideTo(o.activeIndex + o.params.slidesPerGroup, b, a, c)) : o.slideTo(o.activeIndex + o.params.slidesPerGroup, b, a, c);\n      }, o._slideNext = function (a) {\n        return o.slideNext(!0, a, !0);\n      }, o.slidePrev = function (a, b, c) {\n        return o.params.loop ? o.animating ? !1 : (o.fixLoop(), o.slideTo(o.activeIndex - 1, b, a, c)) : o.slideTo(o.activeIndex - 1, b, a, c);\n      }, o._slidePrev = function (a) {\n        return o.slidePrev(!0, a, !0);\n      }, o.slideReset = function (a, b) {\n        return o.slideTo(o.activeIndex, b, a);\n      }, o.setWrapperTransition = function (a, b) {\n        o.wrapper.transition(a), \"slide\" !== o.params.effect && o.effects[o.params.effect] && o.effects[o.params.effect].setTransition(a), o.params.parallax && o.parallax && o.parallax.setTransition(a), o.params.scrollbar && o.scrollbar && o.scrollbar.setTransition(a), o.params.control && o.controller && o.controller.setTransition(a, b), o.emit(\"onSetTransition\", o, a);\n      }, o.setWrapperTranslate = function (a, b, c) {\n        var d = 0,\n            f = 0,\n            g = 0;e() ? d = o.rtl ? -a : a : f = a, o.params.virtualTranslate || (o.support.transforms3d ? o.wrapper.transform(\"translate3d(\" + d + \"px, \" + f + \"px, \" + g + \"px)\") : o.wrapper.transform(\"translate(\" + d + \"px, \" + f + \"px)\")), o.translate = e() ? d : f, b && o.updateActiveIndex(), \"slide\" !== o.params.effect && o.effects[o.params.effect] && o.effects[o.params.effect].setTranslate(o.translate), o.params.parallax && o.parallax && o.parallax.setTranslate(o.translate), o.params.scrollbar && o.scrollbar && o.scrollbar.setTranslate(o.translate), o.params.control && o.controller && o.controller.setTranslate(o.translate, c), o.emit(\"onSetTranslate\", o, o.translate);\n      }, o.getTranslate = function (a, b) {\n        var c, d, e, f;return \"undefined\" == typeof b && (b = \"x\"), o.params.virtualTranslate ? o.rtl ? -o.translate : o.translate : (e = window.getComputedStyle(a, null), window.WebKitCSSMatrix ? f = new WebKitCSSMatrix(\"none\" === e.webkitTransform ? \"\" : e.webkitTransform) : (f = e.MozTransform || e.OTransform || e.MsTransform || e.msTransform || e.transform || e.getPropertyValue(\"transform\").replace(\"translate(\", \"matrix(1, 0, 0, 1,\"), c = f.toString().split(\",\")), \"x\" === b && (d = window.WebKitCSSMatrix ? f.m41 : 16 === c.length ? parseFloat(c[12]) : parseFloat(c[4])), \"y\" === b && (d = window.WebKitCSSMatrix ? f.m42 : 16 === c.length ? parseFloat(c[13]) : parseFloat(c[5])), o.rtl && d && (d = -d), d || 0);\n      }, o.getWrapperTranslate = function (a) {\n        return \"undefined\" == typeof a && (a = e() ? \"x\" : \"y\"), o.getTranslate(o.wrapper[0], a);\n      }, o.observers = [], o.initObservers = function () {\n        if (o.params.observeParents) for (var a = o.container.parents(), b = 0; b < a.length; b++) {\n          h(a[b]);\n        }h(o.container[0], { childList: !1 }), h(o.wrapper[0], { attributes: !1 });\n      }, o.disconnectObservers = function () {\n        for (var a = 0; a < o.observers.length; a++) {\n          o.observers[a].disconnect();\n        }o.observers = [];\n      }, o.createLoop = function () {\n        o.wrapper.children(\".\" + o.params.slideClass + \".\" + o.params.slideDuplicateClass).remove();var b = o.wrapper.children(\".\" + o.params.slideClass);o.loopedSlides = parseInt(o.params.loopedSlides || o.params.slidesPerView, 10), o.loopedSlides = o.loopedSlides + o.params.loopAdditionalSlides, o.loopedSlides > b.length && (o.loopedSlides = b.length);var c,\n            d = [],\n            e = [];for (b.each(function (c, f) {\n          var g = a(this);c < o.loopedSlides && e.push(f), c < b.length && c >= b.length - o.loopedSlides && d.push(f), g.attr(\"data-swiper-slide-index\", c);\n        }), c = 0; c < e.length; c++) {\n          o.wrapper.append(a(e[c].cloneNode(!0)).addClass(o.params.slideDuplicateClass));\n        }for (c = d.length - 1; c >= 0; c--) {\n          o.wrapper.prepend(a(d[c].cloneNode(!0)).addClass(o.params.slideDuplicateClass));\n        }\n      }, o.destroyLoop = function () {\n        o.wrapper.children(\".\" + o.params.slideClass + \".\" + o.params.slideDuplicateClass).remove(), o.slides.removeAttr(\"data-swiper-slide-index\");\n      }, o.fixLoop = function () {\n        var a;o.activeIndex < o.loopedSlides ? (a = o.slides.length - 3 * o.loopedSlides + o.activeIndex, a += o.loopedSlides, o.slideTo(a, 0, !1, !0)) : (\"auto\" === o.params.slidesPerView && o.activeIndex >= 2 * o.loopedSlides || o.activeIndex > o.slides.length - 2 * o.params.slidesPerView) && (a = -o.slides.length + o.activeIndex + o.loopedSlides, a += o.loopedSlides, o.slideTo(a, 0, !1, !0));\n      }, o.appendSlide = function (a) {\n        if (o.params.loop && o.destroyLoop(), \"object\" == (typeof a === \"undefined\" ? \"undefined\" : _typeof(a)) && a.length) for (var b = 0; b < a.length; b++) {\n          a[b] && o.wrapper.append(a[b]);\n        } else o.wrapper.append(a);o.params.loop && o.createLoop(), o.params.observer && o.support.observer || o.update(!0);\n      }, o.prependSlide = function (a) {\n        o.params.loop && o.destroyLoop();var b = o.activeIndex + 1;if (\"object\" == (typeof a === \"undefined\" ? \"undefined\" : _typeof(a)) && a.length) {\n          for (var c = 0; c < a.length; c++) {\n            a[c] && o.wrapper.prepend(a[c]);\n          }b = o.activeIndex + a.length;\n        } else o.wrapper.prepend(a);o.params.loop && o.createLoop(), o.params.observer && o.support.observer || o.update(!0), o.slideTo(b, 0, !1);\n      }, o.removeSlide = function (a) {\n        o.params.loop && o.destroyLoop();var b,\n            c = o.activeIndex;if (\"object\" == (typeof a === \"undefined\" ? \"undefined\" : _typeof(a)) && a.length) {\n          for (var d = 0; d < a.length; d++) {\n            b = a[d], o.slides[b] && o.slides.eq(b).remove(), c > b && c--;\n          }c = Math.max(c, 0);\n        } else b = a, o.slides[b] && o.slides.eq(b).remove(), c > b && c--, c = Math.max(c, 0);o.params.observer && o.support.observer || o.update(!0), o.slideTo(c, 0, !1);\n      }, o.removeAllSlides = function () {\n        for (var a = [], b = 0; b < o.slides.length; b++) {\n          a.push(b);\n        }o.removeSlide(a);\n      }, o.effects = { fade: { fadeIndex: null, setTranslate: function setTranslate() {\n            for (var a = 0; a < o.slides.length; a++) {\n              var b = o.slides.eq(a),\n                  c = b[0].swiperSlideOffset,\n                  d = -c;o.params.virtualTranslate || (d -= o.translate);var f = 0;e() || (f = d, d = 0);var g = o.params.fade.crossFade ? Math.max(1 - Math.abs(b[0].progress), 0) : 1 + Math.min(Math.max(b[0].progress, -1), 0);g > 0 && 1 > g && (o.effects.fade.fadeIndex = a), b.css({ opacity: g }).transform(\"translate3d(\" + d + \"px, \" + f + \"px, 0px)\");\n            }\n          }, setTransition: function setTransition(a) {\n            if (o.slides.transition(a), o.params.virtualTranslate && 0 !== a) {\n              var b = null !== o.effects.fade.fadeIndex ? o.effects.fade.fadeIndex : o.activeIndex;o.slides.eq(b).transitionEnd(function () {\n                for (var a = [\"webkitTransitionEnd\", \"transitionend\", \"oTransitionEnd\", \"MSTransitionEnd\", \"msTransitionEnd\"], b = 0; b < a.length; b++) {\n                  o.wrapper.trigger(a[b]);\n                }\n              });\n            }\n          } }, cube: { setTranslate: function setTranslate() {\n            var b,\n                c = 0;o.params.cube.shadow && (e() ? (b = o.wrapper.find(\".swiper-cube-shadow\"), 0 === b.length && (b = a('<div class=\"swiper-cube-shadow\"></div>'), o.wrapper.append(b)), b.css({ height: o.width + \"px\" })) : (b = o.container.find(\".swiper-cube-shadow\"), 0 === b.length && (b = a('<div class=\"swiper-cube-shadow\"></div>'), o.container.append(b))));for (var d = 0; d < o.slides.length; d++) {\n              var f = o.slides.eq(d),\n                  g = 90 * d,\n                  h = Math.floor(g / 360);o.rtl && (g = -g, h = Math.floor(-g / 360));var i = Math.max(Math.min(f[0].progress, 1), -1),\n                  j = 0,\n                  k = 0,\n                  l = 0;d % 4 === 0 ? (j = 4 * -h * o.size, l = 0) : (d - 1) % 4 === 0 ? (j = 0, l = 4 * -h * o.size) : (d - 2) % 4 === 0 ? (j = o.size + 4 * h * o.size, l = o.size) : (d - 3) % 4 === 0 && (j = -o.size, l = 3 * o.size + 4 * o.size * h), o.rtl && (j = -j), e() || (k = j, j = 0);var m = \"rotateX(\" + (e() ? 0 : -g) + \"deg) rotateY(\" + (e() ? g : 0) + \"deg) translate3d(\" + j + \"px, \" + k + \"px, \" + l + \"px)\";if (1 >= i && i > -1 && (c = 90 * d + 90 * i, o.rtl && (c = 90 * -d - 90 * i)), f.transform(m), o.params.cube.slideShadows) {\n                var n = e() ? f.find(\".swiper-slide-shadow-left\") : f.find(\".swiper-slide-shadow-top\"),\n                    p = e() ? f.find(\".swiper-slide-shadow-right\") : f.find(\".swiper-slide-shadow-bottom\");0 === n.length && (n = a('<div class=\"swiper-slide-shadow-' + (e() ? \"left\" : \"top\") + '\"></div>'), f.append(n)), 0 === p.length && (p = a('<div class=\"swiper-slide-shadow-' + (e() ? \"right\" : \"bottom\") + '\"></div>'), f.append(p)), n.length && (n[0].style.opacity = -f[0].progress), p.length && (p[0].style.opacity = f[0].progress);\n              }\n            }if (o.wrapper.css({ \"-webkit-transform-origin\": \"50% 50% -\" + o.size / 2 + \"px\", \"-moz-transform-origin\": \"50% 50% -\" + o.size / 2 + \"px\", \"-ms-transform-origin\": \"50% 50% -\" + o.size / 2 + \"px\",\n              \"transform-origin\": \"50% 50% -\" + o.size / 2 + \"px\" }), o.params.cube.shadow) if (e()) b.transform(\"translate3d(0px, \" + (o.width / 2 + o.params.cube.shadowOffset) + \"px, \" + -o.width / 2 + \"px) rotateX(90deg) rotateZ(0deg) scale(\" + o.params.cube.shadowScale + \")\");else {\n              var q = Math.abs(c) - 90 * Math.floor(Math.abs(c) / 90),\n                  r = 1.5 - (Math.sin(2 * q * Math.PI / 360) / 2 + Math.cos(2 * q * Math.PI / 360) / 2),\n                  s = o.params.cube.shadowScale,\n                  t = o.params.cube.shadowScale / r,\n                  u = o.params.cube.shadowOffset;b.transform(\"scale3d(\" + s + \", 1, \" + t + \") translate3d(0px, \" + (o.height / 2 + u) + \"px, \" + -o.height / 2 / t + \"px) rotateX(-90deg)\");\n            }var v = o.isSafari || o.isUiWebView ? -o.size / 2 : 0;o.wrapper.transform(\"translate3d(0px,0,\" + v + \"px) rotateX(\" + (e() ? 0 : c) + \"deg) rotateY(\" + (e() ? -c : 0) + \"deg)\");\n          }, setTransition: function setTransition(a) {\n            o.slides.transition(a).find(\".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left\").transition(a), o.params.cube.shadow && !e() && o.container.find(\".swiper-cube-shadow\").transition(a);\n          } }, coverflow: { setTranslate: function setTranslate() {\n            for (var b = o.translate, c = e() ? -b + o.width / 2 : -b + o.height / 2, d = e() ? o.params.coverflow.rotate : -o.params.coverflow.rotate, f = o.params.coverflow.depth, g = 0, h = o.slides.length; h > g; g++) {\n              var i = o.slides.eq(g),\n                  j = o.slidesSizesGrid[g],\n                  k = i[0].swiperSlideOffset,\n                  l = (c - k - j / 2) / j * o.params.coverflow.modifier,\n                  m = e() ? d * l : 0,\n                  n = e() ? 0 : d * l,\n                  p = -f * Math.abs(l),\n                  q = e() ? 0 : o.params.coverflow.stretch * l,\n                  r = e() ? o.params.coverflow.stretch * l : 0;Math.abs(r) < .001 && (r = 0), Math.abs(q) < .001 && (q = 0), Math.abs(p) < .001 && (p = 0), Math.abs(m) < .001 && (m = 0), Math.abs(n) < .001 && (n = 0);var s = \"translate3d(\" + r + \"px,\" + q + \"px,\" + p + \"px)  rotateX(\" + n + \"deg) rotateY(\" + m + \"deg)\";if (i.transform(s), i[0].style.zIndex = -Math.abs(Math.round(l)) + 1, o.params.coverflow.slideShadows) {\n                var t = e() ? i.find(\".swiper-slide-shadow-left\") : i.find(\".swiper-slide-shadow-top\"),\n                    u = e() ? i.find(\".swiper-slide-shadow-right\") : i.find(\".swiper-slide-shadow-bottom\");0 === t.length && (t = a('<div class=\"swiper-slide-shadow-' + (e() ? \"left\" : \"top\") + '\"></div>'), i.append(t)), 0 === u.length && (u = a('<div class=\"swiper-slide-shadow-' + (e() ? \"right\" : \"bottom\") + '\"></div>'), i.append(u)), t.length && (t[0].style.opacity = l > 0 ? l : 0), u.length && (u[0].style.opacity = -l > 0 ? -l : 0);\n              }\n            }if (o.browser.ie) {\n              var v = o.wrapper[0].style;v.perspectiveOrigin = c + \"px 50%\";\n            }\n          }, setTransition: function setTransition(a) {\n            o.slides.transition(a).find(\".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left\").transition(a);\n          } } }, o.lazy = { initialImageLoaded: !1, loadImageInSlide: function loadImageInSlide(b) {\n          if (\"undefined\" != typeof b && 0 !== o.slides.length) {\n            var c = o.slides.eq(b),\n                d = c.find(\"img.swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)\");0 !== d.length && d.each(function () {\n              var b = a(this);b.addClass(\"swiper-lazy-loading\");var d = b.attr(\"data-src\");o.loadImage(b[0], d, !1, function () {\n                b.attr(\"src\", d), b.removeAttr(\"data-src\"), b.addClass(\"swiper-lazy-loaded\").removeClass(\"swiper-lazy-loading\"), c.find(\".swiper-lazy-preloader, .preloader\").remove(), o.emit(\"onLazyImageReady\", o, c[0], b[0]);\n              }), o.emit(\"onLazyImageLoad\", o, c[0], b[0]);\n            });\n          }\n        }, load: function load() {\n          if (o.params.watchSlidesVisibility) o.wrapper.children(\".\" + o.params.slideVisibleClass).each(function () {\n            o.lazy.loadImageInSlide(a(this).index());\n          });else if (o.params.slidesPerView > 1) for (var b = o.activeIndex; b < o.activeIndex + o.params.slidesPerView; b++) {\n            o.slides[b] && o.lazy.loadImageInSlide(b);\n          } else o.lazy.loadImageInSlide(o.activeIndex);if (o.params.lazyLoadingInPrevNext) {\n            var c = o.wrapper.children(\".\" + o.params.slideNextClass);c.length > 0 && o.lazy.loadImageInSlide(c.index());var d = o.wrapper.children(\".\" + o.params.slidePrevClass);d.length > 0 && o.lazy.loadImageInSlide(d.index());\n          }\n        }, onTransitionStart: function onTransitionStart() {\n          o.params.lazyLoading && (o.params.lazyLoadingOnTransitionStart || !o.params.lazyLoadingOnTransitionStart && !o.lazy.initialImageLoaded) && (o.lazy.initialImageLoaded = !0, o.lazy.load());\n        }, onTransitionEnd: function onTransitionEnd() {\n          o.params.lazyLoading && !o.params.lazyLoadingOnTransitionStart && o.lazy.load();\n        } }, o.scrollbar = { set: function set() {\n          if (o.params.scrollbar) {\n            var b = o.scrollbar;b.track = a(o.params.scrollbar), b.drag = b.track.find(\".swiper-scrollbar-drag\"), 0 === b.drag.length && (b.drag = a('<div class=\"swiper-scrollbar-drag\"></div>'), b.track.append(b.drag)), b.drag[0].style.width = \"\", b.drag[0].style.height = \"\", b.trackSize = e() ? b.track[0].offsetWidth : b.track[0].offsetHeight, b.divider = o.size / o.virtualSize, b.moveDivider = b.divider * (b.trackSize / o.size), b.dragSize = b.trackSize * b.divider, e() ? b.drag[0].style.width = b.dragSize + \"px\" : b.drag[0].style.height = b.dragSize + \"px\", b.divider >= 1 ? b.track[0].style.display = \"none\" : b.track[0].style.display = \"\", o.params.scrollbarHide && (b.track[0].style.opacity = 0);\n          }\n        }, setTranslate: function setTranslate() {\n          if (o.params.scrollbar) {\n            var a,\n                b = o.scrollbar,\n                c = b.dragSize;a = (b.trackSize - b.dragSize) * o.progress, o.rtl && e() ? (a = -a, a > 0 ? (c = b.dragSize - a, a = 0) : -a + b.dragSize > b.trackSize && (c = b.trackSize + a)) : 0 > a ? (c = b.dragSize + a, a = 0) : a + b.dragSize > b.trackSize && (c = b.trackSize - a), e() ? (o.support.transforms3d ? b.drag.transform(\"translate3d(\" + a + \"px, 0, 0)\") : b.drag.transform(\"translateX(\" + a + \"px)\"), b.drag[0].style.width = c + \"px\") : (o.support.transforms3d ? b.drag.transform(\"translate3d(0px, \" + a + \"px, 0)\") : b.drag.transform(\"translateY(\" + a + \"px)\"), b.drag[0].style.height = c + \"px\"), o.params.scrollbarHide && (clearTimeout(b.timeout), b.track[0].style.opacity = 1, b.timeout = setTimeout(function () {\n              b.track[0].style.opacity = 0, b.track.transition(400);\n            }, 1e3));\n          }\n        }, setTransition: function setTransition(a) {\n          o.params.scrollbar && o.scrollbar.drag.transition(a);\n        } }, o.controller = { setTranslate: function setTranslate(a, c) {\n          var d,\n              e,\n              f = o.params.control;if (o.isArray(f)) for (var g = 0; g < f.length; g++) {\n            f[g] !== c && f[g] instanceof b && (a = f[g].rtl && \"horizontal\" === f[g].params.direction ? -o.translate : o.translate, d = (f[g].maxTranslate() - f[g].minTranslate()) / (o.maxTranslate() - o.minTranslate()), e = (a - o.minTranslate()) * d + f[g].minTranslate(), o.params.controlInverse && (e = f[g].maxTranslate() - e), f[g].updateProgress(e), f[g].setWrapperTranslate(e, !1, o), f[g].updateActiveIndex());\n          } else f instanceof b && c !== f && (a = f.rtl && \"horizontal\" === f.params.direction ? -o.translate : o.translate, d = (f.maxTranslate() - f.minTranslate()) / (o.maxTranslate() - o.minTranslate()), e = (a - o.minTranslate()) * d + f.minTranslate(), o.params.controlInverse && (e = f.maxTranslate() - e), f.updateProgress(e), f.setWrapperTranslate(e, !1, o), f.updateActiveIndex());\n        }, setTransition: function setTransition(a, c) {\n          var d = o.params.control;if (o.isArray(d)) for (var e = 0; e < d.length; e++) {\n            d[e] !== c && d[e] instanceof b && d[e].setWrapperTransition(a, o);\n          } else d instanceof b && c !== d && d.setWrapperTransition(a, o);\n        } }, o.parallax = { setTranslate: function setTranslate() {\n          o.container.children(\"[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]\").each(function () {\n            i(this, o.progress);\n          }), o.slides.each(function () {\n            var b = a(this);b.find(\"[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]\").each(function () {\n              var a = Math.min(Math.max(b[0].progress, -1), 1);i(this, a);\n            });\n          });\n        }, setTransition: function setTransition(b) {\n          \"undefined\" == typeof b && (b = o.params.speed), o.container.find(\"[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]\").each(function () {\n            var c = a(this),\n                d = parseInt(c.attr(\"data-swiper-parallax-duration\"), 10) || b;0 === b && (d = 0), c.transition(d);\n          });\n        } }, o._plugins = [];for (var E in o.plugins) {\n        if (o.plugins.hasOwnProperty(E)) {\n          var F = o.plugins[E](o, o.params[E]);F && o._plugins.push(F);\n        }\n      }return o.callPlugins = function (a) {\n        for (var b = 0; b < o._plugins.length; b++) {\n          a in o._plugins[b] && o._plugins[b][a](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);\n        }\n      }, o.emitterEventListeners = {}, o.emit = function (a) {\n        o.params[a] && o.params[a](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);var b;if (o) {\n          if (o.emitterEventListeners[a]) for (b = 0; b < o.emitterEventListeners[a].length; b++) {\n            o.emitterEventListeners[a][b](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);\n          }o.callPlugins && o.callPlugins(a, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);\n        }\n      }, o.on = function (a, b) {\n        return a = j(a), o.emitterEventListeners[a] || (o.emitterEventListeners[a] = []), o.emitterEventListeners[a].push(b), o;\n      }, o.off = function (a, b) {\n        var c;if (a = j(a), \"undefined\" == typeof b) return o.emitterEventListeners[a] = [], o;if (o.emitterEventListeners[a] && 0 !== o.emitterEventListeners[a].length) {\n          for (c = 0; c < o.emitterEventListeners[a].length; c++) {\n            o.emitterEventListeners[a][c] === b && o.emitterEventListeners[a].splice(c, 1);\n          }return o;\n        }\n      }, o.once = function (a, b) {\n        a = j(a);var c = function c() {\n          b(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), o.off(a, c);\n        };return o.on(a, c), o;\n      }, o.a11y = { makeFocusable: function makeFocusable(a) {\n          return a[0].tabIndex = \"0\", a;\n        }, addRole: function addRole(a, b) {\n          return a.attr(\"role\", b), a;\n        }, addLabel: function addLabel(a, b) {\n          return a.attr(\"aria-label\", b), a;\n        }, disable: function disable(a) {\n          return a.attr(\"aria-disabled\", !0), a;\n        }, enable: function enable(a) {\n          return a.attr(\"aria-disabled\", !1), a;\n        }, onEnterKey: function onEnterKey(b) {\n          13 === b.keyCode && (a(b.target).is(o.params.nextButton) ? (o.onClickNext(b), o.isEnd ? o.a11y.notify(o.params.lastSlideMsg) : o.a11y.notify(o.params.nextSlideMsg)) : a(b.target).is(o.params.prevButton) && (o.onClickPrev(b), o.isBeginning ? o.a11y.notify(o.params.firstSlideMsg) : o.a11y.notify(o.params.prevSlideMsg)));\n        }, liveRegion: a('<span class=\"swiper-notification\" aria-live=\"assertive\" aria-atomic=\"true\"></span>'), notify: function notify(a) {\n          var b = o.a11y.liveRegion;0 !== b.length && (b.html(\"\"), b.html(a));\n        }, init: function init() {\n          if (o.params.nextButton) {\n            var b = a(o.params.nextButton);o.a11y.makeFocusable(b), o.a11y.addRole(b, \"button\"), o.a11y.addLabel(b, o.params.nextSlideMsg);\n          }if (o.params.prevButton) {\n            var c = a(o.params.prevButton);o.a11y.makeFocusable(c), o.a11y.addRole(c, \"button\"), o.a11y.addLabel(c, o.params.prevSlideMsg);\n          }a(o.container).append(o.a11y.liveRegion);\n        }, destroy: function destroy() {\n          o.a11y.liveRegion && o.a11y.liveRegion.length > 0 && o.a11y.liveRegion.remove();\n        } }, o.init = function () {\n        o.params.loop && o.createLoop(), o.updateContainerSize(), o.updateSlidesSize(), o.updatePagination(), o.params.scrollbar && o.scrollbar && o.scrollbar.set(), \"slide\" !== o.params.effect && o.effects[o.params.effect] && (o.params.loop || o.updateProgress(), o.effects[o.params.effect].setTranslate()), o.params.loop ? o.slideTo(o.params.initialSlide + o.loopedSlides, 0, o.params.runCallbacksOnInit) : (o.slideTo(o.params.initialSlide, 0, o.params.runCallbacksOnInit), 0 === o.params.initialSlide && (o.parallax && o.params.parallax && o.parallax.setTranslate(), o.lazy && o.params.lazyLoading && o.lazy.load())), o.attachEvents(), o.params.observer && o.support.observer && o.initObservers(), o.params.preloadImages && !o.params.lazyLoading && o.preloadImages(), o.params.autoplay && o.startAutoplay(), o.params.keyboardControl && o.enableKeyboardControl && o.enableKeyboardControl(), o.params.mousewheelControl && o.enableMousewheelControl && o.enableMousewheelControl(), o.params.hashnav && o.hashnav && o.hashnav.init(), o.params.a11y && o.a11y && o.a11y.init(), o.emit(\"onInit\", o);\n      }, o.cleanupStyles = function () {\n        o.container.removeClass(o.classNames.join(\" \")).removeAttr(\"style\"), o.wrapper.removeAttr(\"style\"), o.slides && o.slides.length && o.slides.removeClass([o.params.slideVisibleClass, o.params.slideActiveClass, o.params.slideNextClass, o.params.slidePrevClass].join(\" \")).removeAttr(\"style\").removeAttr(\"data-swiper-column\").removeAttr(\"data-swiper-row\"), o.paginationContainer && o.paginationContainer.length && o.paginationContainer.removeClass(o.params.paginationHiddenClass), o.bullets && o.bullets.length && o.bullets.removeClass(o.params.bulletActiveClass), o.params.prevButton && a(o.params.prevButton).removeClass(o.params.buttonDisabledClass), o.params.nextButton && a(o.params.nextButton).removeClass(o.params.buttonDisabledClass), o.params.scrollbar && o.scrollbar && (o.scrollbar.track && o.scrollbar.track.length && o.scrollbar.track.removeAttr(\"style\"), o.scrollbar.drag && o.scrollbar.drag.length && o.scrollbar.drag.removeAttr(\"style\"));\n      }, o.destroy = function (a, b) {\n        o.detachEvents(), o.stopAutoplay(), o.params.loop && o.destroyLoop(), b && o.cleanupStyles(), o.disconnectObservers(), o.params.keyboardControl && o.disableKeyboardControl && o.disableKeyboardControl(), o.params.mousewheelControl && o.disableMousewheelControl && o.disableMousewheelControl(), o.params.a11y && o.a11y && o.a11y.destroy(), o.emit(\"onDestroy\"), a !== !1 && (o = null);\n      }, o.init(), o;\n    }\n  };b.prototype = { defaults: { direction: \"horizontal\", touchEventsTarget: \"container\", initialSlide: 0, speed: 300, autoplay: !1, autoplayDisableOnInteraction: !0, freeMode: !1, freeModeMomentum: !0, freeModeMomentumRatio: 1, freeModeMomentumBounce: !0, freeModeMomentumBounceRatio: 1, setWrapperSize: !1, virtualTranslate: !1, effect: \"slide\", coverflow: { rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: !0 }, cube: { slideShadows: !0, shadow: !0, shadowOffset: 20, shadowScale: .94 }, fade: { crossFade: !1 }, parallax: !1, scrollbar: null, scrollbarHide: !0, keyboardControl: !1, mousewheelControl: !1, mousewheelForceToAxis: !1, hashnav: !1, spaceBetween: 0, slidesPerView: 1, slidesPerColumn: 1, slidesPerColumnFill: \"column\", slidesPerGroup: 1, centeredSlides: !1, touchRatio: 1, touchAngle: 45, simulateTouch: !0, shortSwipes: !0, longSwipes: !0, longSwipesRatio: .5, longSwipesMs: 300, followFinger: !0, onlyExternal: !1, threshold: 0, touchMoveStopPropagation: !0, pagination: null, paginationClickable: !1, paginationHide: !1, paginationBulletRender: null, resistance: !0, resistanceRatio: .85, nextButton: null, prevButton: null, watchSlidesProgress: !1, watchSlidesVisibility: !1, grabCursor: !1, preventClicks: !0, preventClicksPropagation: !0, slideToClickedSlide: !1, lazyLoading: !1, lazyLoadingInPrevNext: !1, lazyLoadingOnTransitionStart: !1, preloadImages: !0, updateOnImagesReady: !0, loop: !1, loopAdditionalSlides: 0, loopedSlides: null, control: void 0, controlInverse: !1, allowSwipeToPrev: !0, allowSwipeToNext: !0, swipeHandler: null, noSwiping: !0, noSwipingClass: \"swiper-no-swiping\", slideClass: \"swiper-slide\", slideActiveClass: \"swiper-slide-active\", slideVisibleClass: \"swiper-slide-visible\", slideDuplicateClass: \"swiper-slide-duplicate\", slideNextClass: \"swiper-slide-next\", slidePrevClass: \"swiper-slide-prev\", wrapperClass: \"swiper-wrapper\", bulletClass: \"swiper-pagination-bullet\", bulletActiveClass: \"swiper-pagination-bullet-active\", buttonDisabledClass: \"swiper-button-disabled\", paginationHiddenClass: \"swiper-pagination-hidden\", observer: !1, observeParents: !1, a11y: !1, prevSlideMessage: \"Previous slide\", nextSlideMessage: \"Next slide\", firstSlideMessage: \"This is the first slide\", lastSlideMessage: \"This is the last slide\", runCallbacksOnInit: !0 }, isSafari: function () {\n      var a = navigator.userAgent.toLowerCase();return a.indexOf(\"safari\") >= 0 && a.indexOf(\"chrome\") < 0 && a.indexOf(\"android\") < 0;\n    }(), isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent), isArray: function isArray(a) {\n      return \"[object Array]\" === Object.prototype.toString.apply(a);\n    }, browser: { ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled, ieTouch: window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1 || window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1 }, device: function () {\n      var a = navigator.userAgent,\n          b = a.match(/(Android);?[\\s\\/]+([\\d.]+)?/),\n          c = a.match(/(iPad).*OS\\s([\\d_]+)/),\n          d = !c && a.match(/(iPhone\\sOS)\\s([\\d_]+)/);return { ios: c || d || c, android: b };\n    }(), support: { touch: window.Modernizr && Modernizr.touch === !0 || function () {\n        return !!(\"ontouchstart\" in window || window.DocumentTouch && document instanceof DocumentTouch);\n      }(), transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function () {\n        var a = document.createElement(\"div\").style;return \"webkitPerspective\" in a || \"MozPerspective\" in a || \"OPerspective\" in a || \"MsPerspective\" in a || \"perspective\" in a;\n      }(), flexbox: function () {\n        for (var a = document.createElement(\"div\").style, b = \"alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient\".split(\" \"), c = 0; c < b.length; c++) {\n          if (b[c] in a) return !0;\n        }\n      }(), observer: function () {\n        return \"MutationObserver\" in window || \"WebkitMutationObserver\" in window;\n      }() }, plugins: {} }, a.Swiper = b;\n}(Zepto), +function (a) {\n  \"use strict\";\n  a.Swiper.prototype.defaults.pagination = \".page-current .swiper-pagination\", a.swiper = function (b, c) {\n    return new a.Swiper(b, c);\n  }, a.fn.swiper = function (b) {\n    return new a.Swiper(this, b);\n  }, a.initSwiper = function (b) {\n    function c(a) {\n      function b() {\n        a.destroy(), d.off(\"pageBeforeRemove\", b);\n      }d.on(\"pageBeforeRemove\", b);\n    }var d = a(b || document.body),\n        e = d.find(\".swiper-container\");if (0 !== e.length) for (var f = 0; f < e.length; f++) {\n      var g,\n          h = e.eq(f);if (h.data(\"swiper\")) h.data(\"swiper\").update(!0);else {\n        g = h.dataset();var i = a.swiper(h[0], g);c(i);\n      }\n    }\n  }, a.reinitSwiper = function (b) {\n    var c = a(b || \".page-current\"),\n        d = c.find(\".swiper-container\");if (0 !== d.length) for (var e = 0; e < d.length; e++) {\n      var f = d[0].swiper;f && f.update(!0);\n    }\n  };\n}(Zepto), +function (a) {\n  \"use strict\";\n  var b = function b(_b) {\n    var c,\n        d = this,\n        e = this.defaults;_b = _b || {};for (var f in e) {\n      \"undefined\" == typeof _b[f] && (_b[f] = e[f]);\n    }d.params = _b;var g = d.params.navbarTemplate || '<header class=\"bar bar-nav\"><a class=\"icon icon-left pull-left photo-browser-close-link' + (\"popup\" === d.params.type ? \" close-popup\" : \"\") + '\"></a><h1 class=\"title\"><div class=\"center sliding\"><span class=\"photo-browser-current\"></span> <span class=\"photo-browser-of\">' + d.params.ofText + '</span> <span class=\"photo-browser-total\"></span></div></h1></header>',\n        h = d.params.toolbarTemplate || '<nav class=\"bar bar-tab\"><a class=\"tab-item photo-browser-prev\" href=\"#\"><i class=\"icon icon-prev\"></i></a><a class=\"tab-item photo-browser-next\" href=\"#\"><i class=\"icon icon-next\"></i></a></nav>',\n        i = d.params.template || '<div class=\"photo-browser photo-browser-' + d.params.theme + '\">{{navbar}}{{toolbar}}<div data-page=\"photo-browser-slides\" class=\"content\">{{captions}}<div class=\"photo-browser-swiper-container swiper-container\"><div class=\"photo-browser-swiper-wrapper swiper-wrapper\">{{photos}}</div></div></div></div>',\n        j = d.params.lazyLoading ? d.params.photoLazyTemplate || '<div class=\"photo-browser-slide photo-browser-slide-lazy swiper-slide\"><div class=\"preloader' + (\"dark\" === d.params.theme ? \" preloader-white\" : \"\") + '\"></div><span class=\"photo-browser-zoom-container\"><img data-src=\"{{url}}\" class=\"swiper-lazy\"></span></div>' : d.params.photoTemplate || '<div class=\"photo-browser-slide swiper-slide\"><span class=\"photo-browser-zoom-container\"><img src=\"{{url}}\"></span></div>',\n        k = d.params.captionsTheme || d.params.theme,\n        l = d.params.captionsTemplate || '<div class=\"photo-browser-captions photo-browser-captions-' + k + '\">{{captions}}</div>',\n        m = d.params.captionTemplate || '<div class=\"photo-browser-caption\" data-caption-index=\"{{captionIndex}}\">{{caption}}</div>',\n        n = d.params.objectTemplate || '<div class=\"photo-browser-slide photo-browser-object-slide swiper-slide\">{{html}}</div>',\n        o = \"\",\n        p = \"\";for (c = 0; c < d.params.photos.length; c++) {\n      var q = d.params.photos[c],\n          r = \"\";\"string\" == typeof q || q instanceof String ? r = q.indexOf(\"<\") >= 0 || q.indexOf(\">\") >= 0 ? n.replace(/{{html}}/g, q) : j.replace(/{{url}}/g, q) : \"object\" == (typeof q === \"undefined\" ? \"undefined\" : _typeof(q)) && (q.hasOwnProperty(\"html\") && q.html.length > 0 ? r = n.replace(/{{html}}/g, q.html) : q.hasOwnProperty(\"url\") && q.url.length > 0 && (r = j.replace(/{{url}}/g, q.url)), q.hasOwnProperty(\"caption\") && q.caption.length > 0 ? p += m.replace(/{{caption}}/g, q.caption).replace(/{{captionIndex}}/g, c) : r = r.replace(/{{caption}}/g, \"\")), o += r;\n    }var s = i.replace(\"{{navbar}}\", d.params.navbar ? g : \"\").replace(\"{{noNavbar}}\", d.params.navbar ? \"\" : \"no-navbar\").replace(\"{{photos}}\", o).replace(\"{{captions}}\", l.replace(/{{captions}}/g, p)).replace(\"{{toolbar}}\", d.params.toolbar ? h : \"\");d.activeIndex = d.params.initialSlide, d.openIndex = d.activeIndex, d.opened = !1, d.open = function (b) {\n      return \"undefined\" == typeof b && (b = d.activeIndex), b = parseInt(b, 10), d.opened && d.swiper ? void d.swiper.slideTo(b) : (d.opened = !0, d.openIndex = b, \"standalone\" === d.params.type && a(d.params.container).append(s), \"popup\" === d.params.type && (d.popup = a.popup('<div class=\"popup photo-browser-popup\">' + s + \"</div>\"), a(d.popup).on(\"closed\", d.onPopupClose)), \"page\" === d.params.type ? (a(document).on(\"pageBeforeInit\", d.onPageBeforeInit), a(document).on(\"pageBeforeRemove\", d.onPageBeforeRemove), d.params.view || (d.params.view = a.mainView), void d.params.view.loadContent(s)) : (d.layout(d.openIndex), void (d.params.onOpen && d.params.onOpen(d))));\n    }, d.close = function () {\n      d.opened = !1, d.swiperContainer && 0 !== d.swiperContainer.length && (d.params.onClose && d.params.onClose(d), d.attachEvents(!0), \"standalone\" === d.params.type && d.container.removeClass(\"photo-browser-in\").addClass(\"photo-browser-out\").animationEnd(function () {\n        d.container.remove();\n      }), d.swiper.destroy(), d.swiper = d.swiperContainer = d.swiperWrapper = d.slides = t = u = v = void 0);\n    }, d.onPopupClose = function () {\n      d.close(), a(d.popup).off(\"pageBeforeInit\", d.onPopupClose);\n    }, d.onPageBeforeInit = function (b) {\n      \"photo-browser-slides\" === b.detail.page.name && d.layout(d.openIndex), a(document).off(\"pageBeforeInit\", d.onPageBeforeInit);\n    }, d.onPageBeforeRemove = function (b) {\n      \"photo-browser-slides\" === b.detail.page.name && d.close(), a(document).off(\"pageBeforeRemove\", d.onPageBeforeRemove);\n    }, d.onSliderTransitionStart = function (b) {\n      d.activeIndex = b.activeIndex;var c = b.activeIndex + 1,\n          e = b.slides.length;if (d.params.loop && (e -= 2, c -= b.loopedSlides, 1 > c && (c = e + c), c > e && (c -= e)), d.container.find(\".photo-browser-current\").text(c), d.container.find(\".photo-browser-total\").text(e), a(\".photo-browser-prev, .photo-browser-next\").removeClass(\"photo-browser-link-inactive\"), b.isBeginning && !d.params.loop && a(\".photo-browser-prev\").addClass(\"photo-browser-link-inactive\"), b.isEnd && !d.params.loop && a(\".photo-browser-next\").addClass(\"photo-browser-link-inactive\"), d.captions.length > 0) {\n        d.captionsContainer.find(\".photo-browser-caption-active\").removeClass(\"photo-browser-caption-active\");var f = d.params.loop ? b.slides.eq(b.activeIndex).attr(\"data-swiper-slide-index\") : d.activeIndex;d.captionsContainer.find('[data-caption-index=\"' + f + '\"]').addClass(\"photo-browser-caption-active\");\n      }var g = b.slides.eq(b.previousIndex).find(\"video\");g.length > 0 && \"pause\" in g[0] && g[0].pause(), d.params.onSlideChangeStart && d.params.onSlideChangeStart(b);\n    }, d.onSliderTransitionEnd = function (a) {\n      d.params.zoom && t && a.previousIndex !== a.activeIndex && (u.transform(\"translate3d(0,0,0) scale(1)\"), v.transform(\"translate3d(0,0,0)\"), t = u = v = void 0, w = x = 1), d.params.onSlideChangeEnd && d.params.onSlideChangeEnd(a);\n    }, d.layout = function (b) {\n      \"page\" === d.params.type ? d.container = a(\".photo-browser-swiper-container\").parents(\".view\") : d.container = a(\".photo-browser\"), \"standalone\" === d.params.type && d.container.addClass(\"photo-browser-in\"), d.swiperContainer = d.container.find(\".photo-browser-swiper-container\"), d.swiperWrapper = d.container.find(\".photo-browser-swiper-wrapper\"), d.slides = d.container.find(\".photo-browser-slide\"), d.captionsContainer = d.container.find(\".photo-browser-captions\"), d.captions = d.container.find(\".photo-browser-caption\");var c = { nextButton: d.params.nextButton || \".photo-browser-next\", prevButton: d.params.prevButton || \".photo-browser-prev\", indexButton: d.params.indexButton, initialSlide: b, spaceBetween: d.params.spaceBetween, speed: d.params.speed, loop: d.params.loop, lazyLoading: d.params.lazyLoading, lazyLoadingInPrevNext: d.params.lazyLoadingInPrevNext, lazyLoadingOnTransitionStart: d.params.lazyLoadingOnTransitionStart, preloadImages: !d.params.lazyLoading, onTap: function onTap(a, b) {\n          d.params.onTap && d.params.onTap(a, b);\n        }, onClick: function onClick(a, b) {\n          d.params.exposition && d.toggleExposition(), d.params.onClick && d.params.onClick(a, b);\n        }, onDoubleTap: function onDoubleTap(b, c) {\n          d.toggleZoom(a(c.target).parents(\".photo-browser-slide\")), d.params.onDoubleTap && d.params.onDoubleTap(b, c);\n        }, onTransitionStart: function onTransitionStart(a) {\n          d.onSliderTransitionStart(a);\n        }, onTransitionEnd: function onTransitionEnd(a) {\n          d.onSliderTransitionEnd(a);\n        }, onLazyImageLoad: function onLazyImageLoad(a, b, c) {\n          d.params.onLazyImageLoad && d.params.onLazyImageLoad(d, b, c);\n        }, onLazyImageReady: function onLazyImageReady(b, c, e) {\n          a(c).removeClass(\"photo-browser-slide-lazy\"), d.params.onLazyImageReady && d.params.onLazyImageReady(d, c, e);\n        } };d.params.swipeToClose && \"page\" !== d.params.type && (c.onTouchStart = d.swipeCloseTouchStart, c.onTouchMoveOpposite = d.swipeCloseTouchMove, c.onTouchEnd = d.swipeCloseTouchEnd), d.swiper = a.swiper(d.swiperContainer, c), 0 === b && d.onSliderTransitionStart(d.swiper), d.attachEvents();\n    }, d.attachEvents = function (a) {\n      var b = a ? \"off\" : \"on\";if (d.params.zoom) {\n        var c = d.params.loop ? d.swiper.slides : d.slides;c[b](\"gesturestart\", d.onSlideGestureStart), c[b](\"gesturechange\", d.onSlideGestureChange), c[b](\"gestureend\", d.onSlideGestureEnd), c[b](\"touchstart\", d.onSlideTouchStart), c[b](\"touchmove\", d.onSlideTouchMove), c[b](\"touchend\", d.onSlideTouchEnd);\n      }d.container.find(\".photo-browser-close-link\")[b](\"click\", d.close);\n    }, d.exposed = !1, d.toggleExposition = function () {\n      d.container && d.container.toggleClass(\"photo-browser-exposed\"), d.params.expositionHideCaptions && d.captionsContainer.toggleClass(\"photo-browser-captions-exposed\"), d.exposed = !d.exposed;\n    }, d.enableExposition = function () {\n      d.container && d.container.addClass(\"photo-browser-exposed\"), d.params.expositionHideCaptions && d.captionsContainer.addClass(\"photo-browser-captions-exposed\"), d.exposed = !0;\n    }, d.disableExposition = function () {\n      d.container && d.container.removeClass(\"photo-browser-exposed\"), d.params.expositionHideCaptions && d.captionsContainer.removeClass(\"photo-browser-captions-exposed\"), d.exposed = !1;\n    };var t,\n        u,\n        v,\n        w = 1,\n        x = 1,\n        y = !1;d.onSlideGestureStart = function () {\n      return t || (t = a(this), u = t.find(\"img, svg, canvas\"), v = u.parent(\".photo-browser-zoom-container\"), 0 !== v.length) ? (u.transition(0), void (y = !0)) : void (u = void 0);\n    }, d.onSlideGestureChange = function (a) {\n      u && 0 !== u.length && (w = a.scale * x, w > d.params.maxZoom && (w = d.params.maxZoom - 1 + Math.pow(w - d.params.maxZoom + 1, .5)), w < d.params.minZoom && (w = d.params.minZoom + 1 - Math.pow(d.params.minZoom - w + 1, .5)), u.transform(\"translate3d(0,0,0) scale(\" + w + \")\"));\n    }, d.onSlideGestureEnd = function () {\n      u && 0 !== u.length && (w = Math.max(Math.min(w, d.params.maxZoom), d.params.minZoom), u.transition(d.params.speed).transform(\"translate3d(0,0,0) scale(\" + w + \")\"), x = w, y = !1, 1 === w && (t = void 0));\n    }, d.toggleZoom = function () {\n      t || (t = d.swiper.slides.eq(d.swiper.activeIndex), u = t.find(\"img, svg, canvas\"), v = u.parent(\".photo-browser-zoom-container\")), u && 0 !== u.length && (v.transition(300).transform(\"translate3d(0,0,0)\"), w && 1 !== w ? (w = x = 1, u.transition(300).transform(\"translate3d(0,0,0) scale(1)\"), t = void 0) : (w = x = d.params.maxZoom, u.transition(300).transform(\"translate3d(0,0,0) scale(\" + w + \")\")));\n    };var z,\n        A,\n        B,\n        C,\n        D,\n        E,\n        F,\n        G,\n        H,\n        I,\n        J,\n        K,\n        L,\n        M,\n        N,\n        O,\n        P,\n        Q = {},\n        R = {};d.onSlideTouchStart = function (b) {\n      u && 0 !== u.length && (z || (\"android\" === a.device.os && b.preventDefault(), z = !0, Q.x = \"touchstart\" === b.type ? b.targetTouches[0].pageX : b.pageX, Q.y = \"touchstart\" === b.type ? b.targetTouches[0].pageY : b.pageY));\n    }, d.onSlideTouchMove = function (b) {\n      if (u && 0 !== u.length && (d.swiper.allowClick = !1, z && t)) {\n        A || (H = u[0].offsetWidth, I = u[0].offsetHeight, J = a.getTranslate(v[0], \"x\") || 0, K = a.getTranslate(v[0], \"y\") || 0, v.transition(0));var c = H * w,\n            e = I * w;if (!(c < d.swiper.width && e < d.swiper.height)) {\n          if (D = Math.min(d.swiper.width / 2 - c / 2, 0), F = -D, E = Math.min(d.swiper.height / 2 - e / 2, 0), G = -E, R.x = \"touchmove\" === b.type ? b.targetTouches[0].pageX : b.pageX, R.y = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY, !A && !y && (Math.floor(D) === Math.floor(J) && R.x < Q.x || Math.floor(F) === Math.floor(J) && R.x > Q.x)) return void (z = !1);b.preventDefault(), b.stopPropagation(), A = !0, B = R.x - Q.x + J, C = R.y - Q.y + K, D > B && (B = D + 1 - Math.pow(D - B + 1, .8)), B > F && (B = F - 1 + Math.pow(B - F + 1, .8)), E > C && (C = E + 1 - Math.pow(E - C + 1, .8)), C > G && (C = G - 1 + Math.pow(C - G + 1, .8)), L || (L = R.x), O || (O = R.y), M || (M = Date.now()), N = (R.x - L) / (Date.now() - M) / 2, P = (R.y - O) / (Date.now() - M) / 2, Math.abs(R.x - L) < 2 && (N = 0), Math.abs(R.y - O) < 2 && (P = 0), L = R.x, O = R.y, M = Date.now(), v.transform(\"translate3d(\" + B + \"px, \" + C + \"px,0)\");\n        }\n      }\n    }, d.onSlideTouchEnd = function () {\n      if (u && 0 !== u.length) {\n        if (!z || !A) return z = !1, void (A = !1);z = !1, A = !1;var a = 300,\n            b = 300,\n            c = N * a,\n            e = B + c,\n            f = P * b,\n            g = C + f;0 !== N && (a = Math.abs((e - B) / N)), 0 !== P && (b = Math.abs((g - C) / P));var h = Math.max(a, b);B = e, C = g;var i = H * w,\n            j = I * w;D = Math.min(d.swiper.width / 2 - i / 2, 0), F = -D, E = Math.min(d.swiper.height / 2 - j / 2, 0), G = -E, B = Math.max(Math.min(B, F), D), C = Math.max(Math.min(C, G), E), v.transition(h).transform(\"translate3d(\" + B + \"px, \" + C + \"px,0)\");\n      }\n    };var S,\n        T,\n        U,\n        V,\n        W,\n        X = !1,\n        Y = !0,\n        Z = !1;return d.swipeCloseTouchStart = function () {\n      Y && (X = !0);\n    }, d.swipeCloseTouchMove = function (a, b) {\n      if (X) {\n        Z || (Z = !0, T = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY, V = d.swiper.slides.eq(d.swiper.activeIndex), W = new Date().getTime()), b.preventDefault(), U = \"touchmove\" === b.type ? b.targetTouches[0].pageY : b.pageY, S = T - U;var c = 1 - Math.abs(S) / 300;V.transform(\"translate3d(0,\" + -S + \"px,0)\"), d.swiper.container.css(\"opacity\", c).transition(0);\n      }\n    }, d.swipeCloseTouchEnd = function () {\n      if (X = !1, !Z) return void (Z = !1);Z = !1, Y = !1;var b = Math.abs(S),\n          c = new Date().getTime() - W;return 300 > c && b > 20 || c >= 300 && b > 100 ? void setTimeout(function () {\n        \"standalone\" === d.params.type && d.close(), \"popup\" === d.params.type && a.closeModal(d.popup), d.params.onSwipeToClose && d.params.onSwipeToClose(d), Y = !0;\n      }, 0) : (0 !== b ? V.addClass(\"transitioning\").transitionEnd(function () {\n        Y = !0, V.removeClass(\"transitioning\");\n      }) : Y = !0, d.swiper.container.css(\"opacity\", \"\").transition(\"\"), void V.transform(\"\"));\n    }, d;\n  };b.prototype = { defaults: { photos: [], container: \"body\", initialSlide: 0, spaceBetween: 20, speed: 300, zoom: !0, maxZoom: 3, minZoom: 1, exposition: !0, expositionHideCaptions: !1, type: \"standalone\", navbar: !0, toolbar: !0, theme: \"light\", swipeToClose: !0, backLinkText: \"Close\", ofText: \"of\", loop: !1, lazyLoading: !1, lazyLoadingInPrevNext: !1, lazyLoadingOnTransitionStart: !1 } }, a.photoBrowser = function (c) {\n    return a.extend(c, a.photoBrowser.prototype.defaults), new b(c);\n  }, a.photoBrowser.prototype = { defaults: {} };\n}(Zepto);"

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)(__webpack_require__(14))

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "\"use strict\";\n\n(function ($) {\n    $.extend($, {\n        sDialog: function sDialog(options) {\n            var opts = $.extend({}, $.sDialog.defaults, options);\n\n            function _init() {\n                var mask_height = $(\"body\").height() > $(window).height() ? $(\"body\").height() : $(window).height();\n                var windowH = parseInt($(window).height());\n                var warpTop = windowH / 2;\n                var dTmpl = '<div class=\"simple-dialog-wrapper\">';\n                if (opts.lock) {\n                    dTmpl += '<div class=\"s-dialog-mask\" style=\"height:' + mask_height + 'px;\"></div>';\n                }\n                dTmpl += '<div style=\"left: 50%; top:' + warpTop + 'px\" class=\"s-dialog-wrapper s-dialog-skin-' + opts.skin + '\">' + '<div class=\"s-dialog-content\">' + opts.content + '</div>';\n                if (opts.okBtn || opts.cancelBtn) {\n                    dTmpl += '<div class=\"s-dialog-btn-wapper\">';\n                    if (opts.okBtn) {\n                        dTmpl += '<a href=\"javascript:void(0)\" class=\"s-dialog-btn-ok\">' + opts.okBtnText + '</a>';\n                    }\n                    if (opts.cancelBtn) {\n                        dTmpl += '<a href=\"javascript:void(0)\" class=\"s-dialog-btn-cancel\">' + opts.cancelBtnText + '</a>';\n                    }\n                    dTmpl += '</div>';\n                }\n                dTmpl += '</div>';\n                dTmpl += '</div>';\n                $(\"body\").append(dTmpl);\n                var d_wrapper = $(\".s-dialog-wrapper\");\n                var mLeft = -parseInt(d_wrapper.width()) / 2;\n                d_wrapper.css({\n                    \"margin-left\": mLeft\n                });\n\n                _bind();\n            }\n\n            function _bind() {\n                var okBtn = $(\".s-dialog-btn-ok\");\n                var cancelBtn = $(\".s-dialog-btn-cancel\");\n                okBtn.click(_okFn);\n                cancelBtn.click(_cancelFn);\n                if (!opts.okBtn && !opts.cancelBtn) {\n                    setTimeout(function () {\n                        _close();\n                    }, opts.autoTime);\n                }\n            }\n\n            function _okFn() {\n                opts.okFn();\n                _close();\n            }\n\n            function _cancelFn() {\n                opts.cancelFn();\n                _close();\n            }\n\n            function _close() {\n                $(\".simple-dialog-wrapper\").remove();\n            }\n            return this.each(function () {\n                _init();\n            })();\n        },\n        sValid: function sValid() {\n            var $this = $.sValid;\n            var sElement = $this.settings.sElement;\n            for (var i = 0; i < sElement.length; i++) {\n                var element = sElement[i];\n                var sEl = $(\"#\" + element).length > 0 ? $(\"#\" + element) : $(\".\" + element);\n                for (var j = 0; j < sEl.length; j++) {\n                    $this.check(element, sEl[j]);\n                }\n            }\n            $this.callBackData();\n            var cEid = $this.errorFiles.eId;\n            var cEmsg = $this.errorFiles.eMsg;\n            var cErules = $this.errorFiles.eRules;\n            var isVlided = false;\n            if (cEid.length > 0) {\n                isVlided = false;\n            } else {\n                isVlided = true;\n            }\n            $this.settings.callback.apply(this, [cEid, cEmsg, cErules]);\n            $this.destroyData();\n            return isVlided;\n        }\n    });\n\n    $.sDialog.defaults = {\n        autoTime: '2000',\n        \"skin\": 'block',\n        \"content\": \"我是一个弹出框\",\n        \"width\": 100,\n        \"height\": 100,\n        \"okBtn\": true,\n        \"cancelBtn\": true,\n        \"okBtnText\": \"确定\",\n        \"cancelBtnText\": \"取消\",\n        \"lock\": true,\n        \"okFn\": function okFn() {},\n        \"cancelFn\": function cancelFn() {} };\n\n    $.extend($.sValid, {\n        defaults: {\n            rules: {},\n            messages: {},\n            callback: function callback() {}\n        },\n        init: function init(options) {\n            var opt = $.extend({}, this.defaults, options);\n            var rules = opt.rules;\n            var messages = opt.messages;\n            var sElement = [];\n            $.map(rules, function (item, idx) {\n                sElement.push(idx);\n            });\n            this.settings = {};\n            this.settings[\"sElement\"] = sElement;\n            this.settings[\"sRules\"] = rules;\n            this.settings[\"sMessages\"] = messages;\n            this.settings['callback'] = opt.callback;\n        },\n        optional: function optional(element) {\n            var val = this.elementValue(element);\n            return !this.methods.required.call(this, val, element);\n        },\n        methods: {\n            required: function required(value, element) {\n                if (element.nodeName.toLowerCase() === \"select\") {\n                    var val = $(element).val();\n                    return val && val.length > 0;\n                }\n                return $.trim(value).length > 0;\n            },\n            maxlength: function maxlength(value, element, param) {\n                var length = $.trim(value).length;\n                return this.optional(element) || length <= param;\n            },\n            minlength: function minlength(value, element, param) {\n                var length = $.trim(value).length;\n                return this.optional(element) || length >= param;\n            },\n\n            number: function number(value, element, param) {\n                return this.optional(element) || /^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)?(?:\\.\\d+)?$/.test(value);\n            },\n            digits: function digits(value, element, param) {\n                return this.optional(element) || /^\\d+$/.test(value);\n            },\n            email: function email(value, element, param) {\n                return this.optional(element) || /^[a-z0-9-]{1,30}@[a-z0-9-]{1,65}(\\.[a-z0-9-]{1,65})*$/.test(value);\n            },\n            mobile: function mobile(value, element, param) {\n                return this.optional(element) || /^(13\\d|14[57]|15[^4\\D]|17[0135-8]|18\\d)\\d{8}$/.test(value);\n            },\n            equalTo: function equalTo(value, element, param) {\n                return this.optional(element) || value === $(param).val();\n            },\n            ncIdCartNo: function ncIdCartNo(value, element, param) {\n                return this.optional(element) || /^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$/.test(value);\n            },\n            name_length: function (_name_length) {\n                function name_length(_x, _x2, _x3) {\n                    return _name_length.apply(this, arguments);\n                }\n\n                name_length.toString = function () {\n                    return _name_length.toString();\n                };\n\n                return name_length;\n            }(function (value, element, param) {\n                value = value.trim();\n                name_length = value.replace(/[^\\u0000-\\u00ff]/g, \"aa\").length;\n                var pattern = new RegExp(\"[`~!@#$^&*()=|{}':;',\\\\[\\\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]\");\n                if (name_length >= param[0] && name_length <= param[1] && !pattern.test(value)) {\n                    return true;\n                } else {\n                    return false;\n                }\n            })\n\n        },\n\n        elementValue: function elementValue(element) {\n            var type = $(element).attr(\"type\");\n            var value = $(element).val();\n            if (typeof value === \"string\") {\n                return value.replace(/\\r/g, \"\");\n            }\n            return value;\n        },\n        rulesFormat: {\n            required: true,\n            email: true\n        },\n        errorFiles: {\n            eId: [],\n            eRules: {},\n            eMsg: {}\n        },\n        check: function check(element, mEl) {\n            var settingsRules = [];\n            var methods = this.methods;\n            var rules = this.settings[\"sRules\"];\n            var mVal = this.elementValue.call(this, mEl);\n            var mParam = [];\n            var errorFiles = this.errorFiles;\n            var errRules = [];\n\n            if (typeof rules[element] === \"string\") {\n                if ($.inArray(rules[element], settingsRules) < 0) {\n                    settingsRules.push(rules[element]);\n                }\n            } else {\n                $.each(rules[element], function (idx, item) {\n                    if ($.inArray(idx, settingsRules) < 0) {\n                        settingsRules.push(idx);\n                        if (idx == \"maxlength\" || idx == \"minlength\") {\n                            mParam.push(parseInt(item));\n                        } else {\n                            mParam.push(item);\n                        }\n                    }\n                });\n            }\n\n            for (var i = 0; i < settingsRules.length; i++) {\n                if (!methods[settingsRules[i]].call(this, mVal, mEl, mParam[i])) {\n                    errRules.push(settingsRules[i]);\n                    errorFiles['eRules'][element] = errRules;\n                    if ($.inArray(element, errorFiles['eId']) < 0) {\n                        errorFiles['eId'].push(element);\n                    }\n                }\n            }\n        },\n        callBackData: function callBackData() {\n            var errorFiles = this.errorFiles;\n            var errId = errorFiles.eId;\n            var eMsg = errorFiles.eMsg;\n            var eRules = errorFiles.eRules;\n            var sMessages = this.settings.sMessages;\n            for (var i = 0; i < errId.length; i++) {\n                if (typeof sMessages[errId[i]] === \"string\") {\n                    eMsg[errId[i] + \"_\" + eRules[errId[i]]] = sMessages[errId[i]];\n                } else {\n                    if ($.isArray(eRules[errId[i]])) {\n                        for (var j = 0; j < eRules[errId[i]].length; j++) {\n                            eMsg[errId[i] + \"_\" + eRules[errId[i]][j]] = sMessages[errId[i]][eRules[errId[i]][j]];\n                        }\n                    }\n                }\n            }\n        },\n        destroyData: function destroyData() {\n            this.errorFiles = {\n                eId: [],\n                eRules: {},\n                eMsg: {}\n            };\n        }\n    });\n})(Zepto);\n\n'use strict';\n(function ($, window) {\n    var loadStyles = function loadStyles(url) {\n        var hasSameStyle = false;\n        var links = $('link');\n        for (var i = 0; i < links.length; i++) {\n            if (links.eq(i).attr('href') == url) {\n                hasSameStyle = true;\n                return;\n            }\n        }\n\n        if (!hasSameStyle) {\n            var link = document.createElement(\"link\");\n            link.type = \"text/css\";\n            link.rel = \"stylesheet\";\n            link.href = url;\n            document.getElementsByTagName(\"head\")[0].appendChild(link);\n        }\n    };\n\n    loadStyles('/wap/css/animate.css');\n\n    $.fn.toast = function (options) {\n        var $this = $(this);\n        var _this = this;\n        return this.each(function () {\n            $(this).css({\n                position: 'relative'\n            });\n            var top = '';\n            var translateInfo = '';\n\n            var box = '';\n            var defaults = {\n                position: \"absolute\",\n                animateIn: \"fadeIn\",\n                animateOut: \"fadeOut\",\n                padding: \"10px 20px\",\n                background: \"rgba(7,17,27,0.66)\",\n                borderRadius: \"6px\",\n                duration: 3000,\n                animateDuration: 500,\n                fontSize: 14,\n                content: \"这是一个提示信息\",\n                color: \"#fff\",\n                top: \"80%\",\n                zIndex: 1000001,\n                isCenter: true,\n                closePrev: true };\n\n            var opt = $.extend(defaults, options || {});\n            var t = '';\n\n            top = opt.isCenter === true ? '50%' : opt.top;\n\n            defaults.isLowerIe9 = function () {\n                return !window.FormData;\n            };\n\n            defaults.createMessage = function () {\n                if (opt.closePrev) {\n                    $('.cpt-toast').remove();\n                }\n                box = $(\"<span class='animated \" + opt.animateIn + \" cpt-toast'></span>\").css({\n                    \"position\": opt.position,\n                    \"padding\": opt.padding,\n                    \"background\": opt.background,\n                    \"font-size\": opt.fontSize,\n                    \"-webkit-border-radius\": opt.borderRadius,\n                    \"-moz-border-radius\": opt.borderRadius,\n                    \"border-radius\": opt.borderRadius,\n                    \"color\": opt.color,\n                    \"top\": top,\n                    \"z-index\": opt.zIndex,\n                    \"-webkit-transform\": 'translate3d(-50%,-50%,0)',\n                    \"-moz-transform\": 'translate3d(-50%,-50%,0)',\n                    \"transform\": 'translate3d(-50%,-50%,0)',\n                    '-webkit-animation-duration': opt.animateDuration / 1000 + 's',\n                    '-moz-animation-duration': opt.animateDuration / 1000 + 's',\n                    'animation-duration': opt.animateDuration / 1000 + 's'\n                }).html(opt.content).appendTo($this);\n                defaults.colseMessage();\n            };\n\n            defaults.colseMessage = function () {\n                var isLowerIe9 = defaults.isLowerIe9();\n                if (!isLowerIe9) {\n                    t = setTimeout(function () {\n                        box.removeClass(opt.animateIn).addClass(opt.animateOut).on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {\n                            box.remove();\n                        });\n                    }, opt.duration);\n                } else {\n                    t = setTimeout(function () {\n                        box.remove();\n                    }, opt.duration);\n                }\n            };\n\n            defaults.createMessage();\n        });\n    };\n})(Zepto, window);\n\nvar showMessage = function showMessage(content, duration, isCenter, animateIn, animateOut, closeTime) {\n    var animateIn = animateIn || 'fadeIn';\n    var animateOut = animateOut || 'fadeOut';\n    var content = content || '这是一个提示信息';\n    var duration = duration || '3000';\n    var isCenter = isCenter || false;\n    var closeTime = closeTime || 3000;\n    $('body').toast({\n        position: 'fixed',\n        animateIn: animateIn,\n        animateOut: animateOut,\n        content: content,\n        duration: duration,\n        isCenter: isCenter\n    });\n    setTimeout(function () {\n        $('.cpt-toast').remove();\n    }, closeTime);\n};"

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)(__webpack_require__(16))

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "\"use strict\";\n\n$(function () {});"

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)(__webpack_require__(18))

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "'use strict';\n\nvar _typeof = typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; };\n\n(function ($) {\n    var closeBase = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDI0MUJBMDdCNjFEMTFFNzhCMUY4NUU1NTkwQkI1NEQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDI0MUJBMDhCNjFEMTFFNzhCMUY4NUU1NTkwQkI1NEQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MjQxQkEwNUI2MUQxMUU3OEIxRjg1RTU1OTBCQjU0RCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MjQxQkEwNkI2MUQxMUU3OEIxRjg1RTU1OTBCQjU0RCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt+VBHYAAAHpSURBVHjarJRLL0NREMfbhoSIIJ4t8Yq3CFqKSKW18Rkk1nY2voBPI7bWFnSBRFtFQsU7sRGPxCvEI9RvZG5yc13trWjy69zOmfl3zp05x51Op13//cmTr2g0OoWZhrlwOLzyFyE0ajDzsO1R3ySEYJnFyB8EfZgkjMOsIToDD/oswj05CJaqoFddQ9+ibPkUE4A7XYgR3O1AsAwTh2p1TaAVMyoV4WMVvoACSJDkzyBYi9mEFpPgkjy4rd0nuAKTgkp1+QnesnmHElOirj5idox1j7UCFm8w/XCprjgiA5Yux02CIbOgbaWmZK9uT6wE1esfnYMIf8IYgmvWXHem4Ue4DiNNzIdreIRmXQ4gmLTLc2c7UQhXYVahVV1XEEEw9VuOx8Eovlp+p00z7cq5Uu2yjFoh3MOLzqS8z06qPcypUm1UXAVdOhFtWqXkJYnpdCxKcJNcDODTqkap6gxE0K8nr0imw+7k2Q1/gwrKmX6HYWuXiWnVcSuGJx3+Y9tKCW43Ccr7G7QbG3xHmF641Yp3yQ38qBRnl1wkGvQMQZL3soxbo9wRUA4fuqvEt6guprQp8i/dLO47vPrkMBzohS+vq8PYftLU5ZBTQdO1GYQ3PXknhugibMCI3Vl2ICy32CCsw8KXAAMArgnOjDm5TZQAAAAASUVORK5CYII=';\n    $.fn.tipModal = function (method) {\n        var elem = $(this),\n            elemClass = 'tipModal',\n            effectIn = 'fadeIn',\n            effectOut = 'fadeOut',\n            animTime,\n            _options,\n            elemObj,\n            cancelBtn,\n            okBtn;\n        var methods = {\n            init: function init(params) {\n                var _defaults = {\n                    simple: true,\n                    html: '内容',\n                    showCloseBtn: false,\n                    showOkBtn: true,\n                    onCancelBtn: '',\n                    onOkBtn: '',\n                    onLoad: function onLoad() {},\n                    onClose: function onClose() {},\n                    autoClose: true,\n                    timeout: 3000,\n                    modalName: '',\n                    size: 'small',\n                    onDocumentClickClose: true,\n                    showTitle: false,\n                    showClose: true,\n                    css: {\n                        width: '70%'\n                    },\n                    noBackgroundScroll: true\n                };\n\n                _options = $.extend(_defaults, params);\n\n                $('.' + elemClass).remove();\n\n                var title = _options.showTitle && typeof _options.showTitle == 'boolean' ? '标题' : _options.showTitle,\n                    imgSrc = _options.showClose ? closeBase : '';\n\n                cancelBtn = '<div  class=\"tipModal_cancel tipModal_btn\">' + returnChange(_options.showCloseBtn && typeof _options.showCloseBtn === 'string', _options.showCloseBtn, '取消') + '</div>';\n\n                okBtn = '<div  class=\"tipModal_ok tipModal_btn\">' + returnChange(_options.showOkBtn && typeof _options.showOkBtn === 'string', _options.showOkBtn, '确认') + '</div>';\n\n                var tipModalContainer = $('<div class=\"tipModal\">\\\n                    <div class=\"tipModal_mask\"></div>\\\n                    <div class=\"tipModal_wrapper animated ' + effectIn + '\" style=\"width:' + _options.css.width + ';\">\\\n                    <div class=\"tipModal_header ' + returnClass(_options.showTitle, 'showtitle') + '\">\\\n                    <h3 class=\"tipModal_title ' + returnClass(!_options.showTitle) + '\">' + title + '</h3>\\\n                    <div class=\"tipModal_close ' + returnClass(!_options.showClose) + '\" >\\\n                    <img src=\"' + imgSrc + '\" alt=\"\">\\\n                    </div>\\\n                    </div>\\\n                    <div class=\"tipModal_content ' + returnClass(_options.simple, 'simple') + '\">\\\n                    ' + _options.html + '\\\n                    </div>\\\n                    <div class=\"tipModal_tools ' + returnClass(_options.showOkBtn && _options.showCloseBtn, 'allBtn') + ' \">\\\n                    ' + okBtn + '\\\n                    ' + cancelBtn + '\\\n                    </div>\\\n                    </div>\\\n                    </div>');\n\n                $('body').append(tipModalContainer);\n\n                elemObj = _options.modalName && $(_options.modalName) || $('.' + elemClass);\n\n                if (_options.onLoad && $.isFunction(_options.onLoad)) {\n                    _options.onLoad();\n                }\n\n                elemObj.on('destroyed', function () {\n                    if (_options.onClose && $.isFunction(_options.onClose)) {\n                        _options.onClose();\n                    }\n                });\n\n                if (_options.onDocumentClickClose) {\n                    elemObj.find('.tipModal_mask').click(function () {\n                        tipModalClose();\n                    });\n                }\n\n                elemObj.find('.tipModal_close').on('click', function (event) {\n                    tipModalClose();\n                });\n\n                elemObj.find('.tipModal_ok').on('click', function (event) {\n                    var ok;\n                    if (_options.onOkBtn && $.isFunction(_options.onOkBtn)) {\n                        ok = _options.onOkBtn(event, methods);\n                    } else {\n                        tipModalClose();\n                    }\n                });\n\n                elemObj.find('.tipModal_cancel').on('click', function (event) {\n                    var ok;\n                    if (_options.onCancelBtn && $.isFunction(_options.onCancelBtn)) {\n                        ok = _options.onCancelBtn(event, methods);\n                    } else {\n                        tipModalClose();\n                    }\n                });\n\n                $('html').on('keydown.' + elemClass + 'Event', function (event) {\n                    if (event.keyCode == 27) {\n                        tipModalClose();\n                    }\n                });\n            },\n            hide: function hide() {\n                tipModalClose();\n            }\n        };\n        elemObj = $('.' + elemClass);\n        $(document).on('click', '.tipModal_close', function () {\n            tipModalClose();\n        });\n\n        function tipModalClose() {\n            elemObj = $('.' + elemClass);\n            reverseEffect();\n            getAnimTime();\n            setTimeout(function () {\n                elemObj.remove();\n                $('html.' + elemClass + 'Open').off('.' + elemClass + 'Event').removeClass(elemClass + 'Open');\n            }, animTime);\n        }\n\n        function getAnimTime() {\n            if (!animTime) {\n                animTime = elemObj.find('.tipModal_wrapper').css('animation-duration');\n                if (animTime != undefined) {\n                    animTime = animTime.replace('s', '') * 1000;\n                } else {\n                    animTime = 0;\n                }\n            }\n        }\n\n        function reverseEffect() {\n            elemObj = $('.' + elemClass);\n            var animClassOld = elemObj.find('.tipModal_wrapper').attr('class'),\n                animClassNew = animClassOld.replace(effectIn, effectOut);\n            elemObj.removeClass(animClassOld).addClass(animClassNew);\n        }\n\n        if (methods[method]) {\n            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));\n        } else if ((typeof method === 'undefined' ? 'undefined' : _typeof(method)) === 'object' || !method) {\n            return methods.init.apply(this, arguments);\n        }\n\n        function returnClass(str, name) {\n            var cs = name || 'lm_hidden';\n            return str ? cs : '';\n        }\n\n        function returnChange(str, obj1, obj2) {\n            return str ? obj1 : obj2;\n        }\n\n        $.event.special.destroyed = {\n            remove: function remove(o) {\n                if (o.handler) {\n                    o.handler();\n                }\n            }\n        };\n    };\n})(Zepto);"

/***/ })
/******/ ]);