function wx_config(invite_code) {
    var url = location.href.split('#')[0];
    var jssdk_url = ApiUrl + "/index.php?act=jssdk&op=get_sign_package&url="+url;
    $.getJSON(jssdk_url, function (result) {
        var data = result.datas;  //alert(JSON.stringify(data));
        wx.config({
            debug: false,
            appId: data.appId,
            timestamp: data.timestamp,
            nonceStr: data.nonceStr,
            signature: data.signature,
            jsApiList: [
                "onMenuShareTimeline", //分享到朋友圈
                "onMenuShareAppMessage", //分享到朋友
            ]
        });

        //配置分享信息
        wx.ready(function () {
            var shareData = {
                 title: "艺术文化产业—名族魂，中国梦",
                  desc: "创新“事业合伙人模式”，共同打造一个互生互长的生态系统，使人人都能成为中华艺术的传播者、守护者，用5000元撬动6万亿艺术品市场!",
                  link: WapSiteUrl+'/tmpl/member/finance.html?invite_code='+invite_code,//$("#bind_uri").val(),
                imgUrl: WapSiteUrl+'/images/share/'+parseInt(Math.random()*4+1)+'_thumb.jpg'
            };
            wx.onMenuShareTimeline(shareData);
            wx.onMenuShareAppMessage(shareData);
        });
        // 错误提示
        wx.error(function(res){
//            alert(JSON.stringify(res));
        });
    });
}


