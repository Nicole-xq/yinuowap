var page = pagesize;
var curpage = 1;
var hasmore = true;
var footer = false;
var keyword = '';
var key = getQueryString('key');
var order = getQueryString('order');
var order2 = 1;
var order3 = 1;


$(function(){
    var special_id = getQueryString('special_id');
    loadSpecial(special_id);
})

function loadSpecial(special_id){
    $.ajax({
        url: ApiUrl + "/index.php?act=index&op=special&special_id=" + special_id,
        type: 'get',
        dataType: 'json',
        success: function(result) {
            $('title,h1').html(result.datas.special_desc);
            keyword = result.datas.special_desc;
            if (result.datas.type == 'search') {
                $('#page-modal').show();
                $('#header').hide();
                $('#auction_list').hide();
            } else {
                $('#page-modal').hide();
                $('#header').show();
                $('#auction_list').show();
            }
            var data = result.datas.list;
            var html = '';
             $.each(data, function(k, v) {
                $.each(v, function(kk, vv) {
                    switch (kk) {
                        case 'adv_list':
                        case 'home5':
                        case 'home6':
                        case 'home3':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'sem':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'store_top':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'home1':
                            vv.url = buildUrl(vv.type, vv.data);
                            break;
                        case 'home2':
                        case 'home4':
                            vv.square_url = buildUrl(vv.square_type, vv.square_data);
                            vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                            vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                            break;
                        case 'cla':
                        break;
                        case 'video_list':
                        case 'nav_list':
                    }
                    if (kk == 'sem') {
                        $("#main-container1").html(template.render(kk, vv));
                    } else if(kk == 'adv_list'){
                        $("#main-container3").html(template.render(kk, vv));
                    } else{
                        html += template.render(kk, vv);
                    }

                    return false;
                });
            });

            $("#main-container2").html(html);

            $('#auction_list').find('a').click(function(){
                $(this).addClass('actived').siblings().removeClass('actived');
            });
            get_list();
            $(window).scroll(function(){
                if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
                    get_list();
                }
            });

            updateEndTime();

            $('.adv_list').each(function() {
                if ($(this).find('.item').length < 2) {
                    return;
                }

                Swipe(this, {
                    startSlide: 2,
                    speed: 400,
                    auto: 3000,
                    continuous: true,
                    disableScroll: false,
                    stopPropagation: false,
                    callback: function(index, elem) {},
                    transitionEnd: function(index, elem) {}
                });
            });
        }
    });

}


function get_list() {
    $('.loading').remove();
    if (!hasmore) {
        return false;
    }
    hasmore = false;
    param = {};
    param.page = page;
    param.curpage = curpage;
    if (keyword != '') {
        param.keyword = keyword;
    }
    if (key != '') {
        param.key = key;
    }
    if (order != '') {
        param.order = order;
    }
    $.getJSON(ApiUrl + '/index.php?act=auction&op=auction_list' + window.location.search.replace('?','&'), param, function(result){
        if(!result) {
            result = [];
            result.datas = [];
            //result.datas.search_list = [];
            result.datas.auction_list = [];
        }
        var data = result.datas;
        window._bd_share_config={
            "common":{
                "bdText":data.auction_list,
                "bdPic":data.auction_list,
                "bdUrl":""
            },
            "share":{
                "bdSize":"24"
            },
            "selectShare":{
                "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
            }
        };
        // with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];

        $('.loading').remove();
        curpage++;
        var html = template.render('home_body', result.datas);
        $("#lists").append(html);
        hasmore = result.hasmore;

    });

}

function init_get_list(o, k) {
    if(k == 2){
        if(order2 == 1) {
            o = order2 = 2;
        }else{
            o = order2 = 1;
        }
    }else if(k == 3){
        if(order3 == 1) {
            o = order3 = 2;
        }else{
            o = order3 = 1;
        }
    }
    order = o;
    key = k;
    curpage = 1;
    hasmore = true;
    $("#lists").html('');
    get_list();
}

function get_special_list(id){
    $('#special_'+id).addClass('active').siblings().removeClass('active');
    $('#tab'+ id).addClass('active').siblings().removeClass('active');
}


//倒计时函数
function updateEndTime()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function(i){

        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("距结束&nbsp;<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
        }

    });
    setTimeout("updateEndTime()",1000);
}
