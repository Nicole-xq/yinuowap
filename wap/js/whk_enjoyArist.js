/**
 * Created by PhpStorm.
 * User:  whk
 * Date:2018.10.10
 */



$(function () {

    var store_id = getQueryString('store_id');//86
    // 推荐艺术家列表
    $.ajax({
        url: ApiUrl + '/index.php?act=store&op=special_store_brand_list',
        type: 'POST',
        dataType: 'json',
        data: {store_id: store_id, limit: 10, recommend: 0},
        async: false,
        success: function (result) {
            var data = result.datas;
            console.log(data)
            // if(!$("#lm_enjoy_artist").length||!$("#lm_enjoy_artist_con").length){return false;}
            // $("#lm_enjoy_artist").html(template.render('lm_enjoy_artist_con', data));
            $(".enjoyPreference").html(template.render('enjoyPreferenceContent', data));

            // var swiper = new Swiper('.lm_enjoy_artist_wrapper', {
            //     slidesPerView: 4,
            //     paginationClickable: true,
            // });
        }
    });


    //作品分类
    // $.ajax({
    //     url:ApiUrl+'/index.php?act=store&op=special_store_category_list',
    //     type:'POST',
    //     dataType:'json',
    //     data:{store_id:store_id},
    //     async:false,
    //     success:function (result) {
    //         var data = result.datas;
    //         if(!$("#lm_enjoy_typelist").length||!$("#lm_enjoy_typelist_con").length){return false;}
    //         $("#lm_enjoy_typelist").html(template.render('lm_enjoy_typelist_con', data));
    //     }
    // });
    //
    // //作品分类
    // $.ajax({
    //     url:ApiUrl+'/index.php?act=store&op=special_store_goods_recommend_list',
    //     type:'POST',
    //     dataType:'json',
    //     data:{store_id:store_id},
    //     success:function (result) {
    //         var data = result.datas;
    //         if(!$("#lm_enjoy_typelist_list").length||!$("#lm_enjoy_typelist_list_con").length){return false;}
    //         $("#lm_enjoy_typelist_list").html(template.render('lm_enjoy_typelist_list_con', data));
    //
    //     }
    // });
    //
    // if(isWeiXin){
    //     var shareData = {
    //         title: "艺诺尊享专区-升级合伙人专用及特惠区!",
    //         desc: "甄选大师之作，尊享超值特惠，海量福利等你来拿！",
    //         link: window.location.href,
    //         imgUrl: dataUrl+'/upload/logo.jpg',
    //     };
    //     wx_config(shareData);
    // }
    // //APP分享参数
    // appShareSet(shareData);


    //返回上一页
    $('.back').click(function () {
        window.history.go(-1);
    })

});
function whenError(a) {
    a.onerror = null;
    a.src = '../images/whk/bitamF.png';
    console.log('图片出错的时候调用默认的图片');
}

if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1) {
    $('header').hide();
    $("body").css('padding-top', '0');
    // $(".footer").css('bottom', '0!important');
}
