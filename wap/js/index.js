/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
var key = getCookie('key');
$(function() {
    $.ajax({
        url: ApiUrl + "/index.php?act=index",
        type: 'get',
        dataType: 'json',
        async:false,
        success: function(result) {
            var data = result.datas;
            console.log(data)
            var html = '';
            $.each(data, function(k, v) {
                $.each(v, function(kk, vv) {
                    switch (kk) {
                        case 'adv_list':
                        case 'home3':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'sem':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'store_top':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'home1':
                            vv.url = buildUrl(vv.type, vv.data);
                            break;
                        case 'home2':
                        case 'home4':
                            vv.square_url = buildUrl(vv.square_type, vv.square_data);
                            vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                            vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                            break;
                        case 'cla':
                            $.each(vv.item, function(k3,v3) {
                                $.each(v3.elem, function(k4,v4) {
                                    v3.elem[k4].e_link = buildUrl(v3.elem[k4].e_type,v3.elem[k4].e_data);
                                })
                                vv.item[k3] = v3;
                            })
                            break;
                        case 'video_list':
                        case 'nav_list':
                    }
                    if (kk == 'sem') {
                        $("#main-container1").html(template.render(kk, vv));
                    } else if(kk == 'adv_list'){
                        $("#main-container3").html(template.render(kk, vv));
                        var mySwiper = new Swiper('.shop_banner', {
                            loop: true,
                            autoplay : 5000,
                            pagination: '.index_banner_pagination',
                        })
                    } else if(kk == 'yinuo_store_id'){
                        return true;
                    }else{
                        html += template.render(kk, vv);
                    }
                    updateEndTime();
                    return false;
                });
            });
            $("#main-container2").html(html);
        }
    });
    if(isWeiXin){
        var shareData = {
            title: "艺诺商城-国画书法、西画雕塑、珠宝玉翠、紫砂陶瓷尽收眼底...",
            desc: "艺诺商城汇集各界艺术大师,精选海量艺术创作,让您享受足不出户”送上门”的艺术生活",
            link: window.location.href,
            imgUrl: dataUrl+'/upload/logo.jpg',
        };
        wx_config(shareData);
    }
    //APP分享参数
    appShareSet(shareData);
});