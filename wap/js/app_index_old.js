var key = getCookie('key');
var yinuo_store_id = '';
$(function() {

    var headerClone = $('#header').clone();
    $(window).scroll(function(){
        if ($(window).scrollTop() <= $('#main-container1').height()) {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('transparent').removeClass('');
            headerClone.prependTo('.nctouch-home-top');
        } else {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('').removeClass('transparent');
            headerClone.prependTo('body');
        }
    });
    $.ajax({
        url: ApiUrl + "/index.php?act=index&op=app_index&special_id=1",
        type: 'get',
        dataType: 'json',
        async:false,
        success: function(result) {
            var data = result.datas;
            var html = '';
            $.each(data, function(k, v) {
                $.each(v, function(kk, vv) {
                    switch (kk) {
                        case 'adv_list':
                        case 'home5':
                        case 'home3':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'store_top':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'home1':
                            vv.url = buildUrl(vv.type, vv.data);
                            break;

                        case 'home2':
                        case 'home4':
                            vv.square_url = buildUrl(vv.square_type, vv.square_data);
                            vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                            vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                            break;
                    }
                    if(kk == 'adv_list'){
                        $("#main-container1").html(template.render(kk, vv));
                    } else if(kk == 'yinuo_store_id'){
                        yinuo_store_id = vv;
                    }else{
                        html += template.render(kk, vv);
                    }

                return false;
                });
            });

            $("#main-container").html(html);
            updateEndTime();

            get_fav_store();
            $('.adv_list').each(function() {
                if ($(this).find('.item').length < 2) {
                    return;
                }

                Swipe(this, {
                    startSlide: 0,
                    speed: 400,
                    auto: 3000,
                    continuous: true,
                    disableScroll: false,
                    stopPropagation: false,
                    callback: function(index, elem) {},
                    transitionEnd: function(index, elem) {}
                });
            });

            $('.title-name h1').each(function(i,e){
                if($(e).text()==='尊享专区'){
                    $(e).html('<a style="float: none;position: static;" href="/wap/tmpl/store.html?store_id='+yinuo_store_id+'">尊享专区 <span class="qll_more">more >></span></a>');
                }else if($(e).text()==='艺诺商城'){
                    $(e).html('<a style="float: none;position: static;" href="/wap/index.html">艺诺商城 <span class="qll_more">more >></span></a>');
                }else if($(e).text()==='艺诺拍卖'){
                    $(e).html('<a style="float: none;position: static;" href="/wap/tmpl/auction/auction_index.html">艺诺拍卖 <span class="qll_more">more >></span></a>');
                }else if($(e).text()==='艺诺之星'){
                    $(e).html('<a style="float: none;position: static;" href="/wap/tmpl/auction/artist_list.html">艺诺之星 <span class="qll_more">more >></span></a>');
                }else if($(e).text()==='趣猜'){
                    $(e).html('<a style="float: none;position: static;" href="/wap/tmpl/qucai/qucai.html">趣猜 <span class="qll_more">more >></span></a>');
                }
            })

        }
    });

    $('.index_like_no').click(function(){
        var collect_num =  $(this).parent().find('.collect_num');
        var store_id = collect_num.attr('data-storeid');
        var f_result = favoriteStore(store_id);
        if (f_result){
            location.reload();
        }
    });

    $('.index_like_selected').click(function(){
        var collect_num =  $(this).parent().find('.collect_num');
        var store_id = collect_num.attr('data-storeid');
        var f_result = dropFavoriteStore(store_id);
        if (f_result){
            location.reload();
        }
    });
    if(isWeiXin){
        var shareData = {
            title: "艺诺网-全球首创艺术品交易平台",
            desc: "创新艺术品拍卖+金融收益+合伙人模式,全场保真、保值、保退换!",
            link:window.location.href,
            imgUrl: dataUrl+'/upload/logo.jpg',
        };
        wx_config(shareData);
    }

    //渲染list //艺术家
    var param = {'is_rec':1,'order':'artist_sort','curpage':1,'page':12};
    $.getJSON(ApiUrl + '/index.php?act=artist&op=artist_list' + window.location.search.replace('?', '&'), param, function (result) {
        if (!result) {
            result = [];
            result.datas = [];
            result.datas.artist_list = [];
        }
        var data = result.datas;
        var html = template.render('artist_list', data);
        $("#artist_list_html").html(html);
    });


});
function init_get_list(mb_class_id) {
    $('#nav_'+mb_class_id).addClass('selected').parent().siblings().find('a').removeClass('selected');

    loadAuction(mb_class_id);

}

function get_special_list(id){
    $('#special_'+id).addClass('active').siblings().removeClass('active');
    $('#tab'+ id).addClass('active').siblings().removeClass('active');
}

function get_fav_store(){
    var store_ids = '';
    if ($('.collect_num').length < 1) {
        return ;
    }
    $('.collect_num').each(function(){
        store_ids += $(this).attr('data-storeid')+',';
    });
    store_ids = (store_ids.substring(store_ids.length-1)==',')?store_ids.substring(0,store_ids.length-1):store_ids;
    $.ajax({
        url: ApiUrl + "/index.php?act=member_index&op=fav_store_list&key="+key+"&store_ids="+store_ids,
        type: 'get',
        dataType: 'json',
        async:false,
        success: function(result) {
            if (result.code == 400) {
                return false;
            }
            var store_ids = result.datas;
            $('.collect_num').each(function(){
                if ( $.inArray( $(this).attr('data-storeid'), store_ids ) >= 0) {
                    $(this).parent().parent().find('.index_like').removeClass('index_like_no').addClass('index_like_selected');
                }
            });
        }
    });
}

//倒计时函数
function updateEndTime()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function(i){

        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("距结束&nbsp;<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
        }

    });
    setTimeout("updateEndTime()",1000);
}

