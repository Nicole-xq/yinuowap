/**
 * Created by PhpStorm.
 * User:  whk
 * Date:
 */
$(function () {
    var store_id = getQueryString('store_id');//86
    var brand_id = getQueryString('brand_id');//14
    var descriptionHight;
    // 推荐艺术家列表
    $.ajax({
        url: ApiUrl + '/index.php?act=store&op=special_store_brand_goods_list',
        type: 'POST',
        dataType: 'json',
        data: {store_id: store_id, brand_id: brand_id},
        async: false,
        success: function (result) {
            var data = result.datas;
            console.log(data)
            console.log(data.brand_info.description)
            function escape2Html(str) {
                return $("<div/>").html(str).text();
            }
            if(data.brand_info.description){
                console.log(escape2Html(data.brand_info.description))
            }
            if (!$("#pub_works_list").length || !$("#pub_works_list_con").length) {
                return false;
            }
            // $("#pub_works_list").html(template.render('pub_works_list_con', data));
            var html = template.render('pub_works_list_con', data);
            $("#pub_works_list").html(html);
            $("#description").html(data.brand_info.description);

            //


            $(".title").html(template.render('backTitle', data));
            $(".enjoyPreference").html(template.render('enjoyPreferenceContent', data));
            ;
            if (data.brand_info.description == '') {
                $('.description').css('height', "0rem!important");
                $('.checkMore').css('display', "none");

            }
            descriptionHight = parseInt($('.description').get(0).offsetHeight);
            if(descriptionHight < 60){
                $('.moreW').css('display', 'none!important');
            }else {

            }
            $('.description').css('height', '3rem')
            $('.description').css('overflow', 'hidden')

            document.title = data.brand_info.brand_name + '艺术馆';
        }
    });

    //返回上一页
    $('.back').click(function () {
        window.history.go(-1);
    })


    //
    $('.checkMore .moreO').click(function () {
        $('.description').css('height', descriptionHight + 'px')
        $('.moreO').css('display', 'none!important');
        $('.moreT').css('display', 'block!important')
    })
    $('.checkMore .moreT').click(function () {
        $('.description').css('height', "3rem")
        $('.moreO').css('display', 'block!important');
        $('.moreT').css('display', 'none!important')
    })
    console.log(descriptionHight)


});