/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var store_id=getQueryString('store_id');//86
    var brand_id=getQueryString('brand_id');//14
    // 推荐艺术家列表
    $.ajax({
        url:ApiUrl+'/index.php?act=store&op=special_store_brand_goods_list',
        type:'POST',
        dataType:'json',
        data:{store_id:store_id,brand_id :brand_id},
        async:false,
        success:function (result) {
            var data = result.datas;
            if(!$("#pub_works_list").length||!$("#pub_works_list_con").length){return false;}
            $("#pub_works_list").html(template.render('pub_works_list_con', data));
            document.title=data.brand_info.brand_name+'艺术馆';
        }
    });
});