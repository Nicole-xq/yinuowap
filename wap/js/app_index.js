/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
var key = getCookie('key');
var timeout;
$(function () {
    var artist_param = {curpage: 1, reset: true, p: p};
    var artist_hasmore = true;
    var artist_swiper;
    var p;

    function artist_page() {

        return false;//首页改版，暂时不加载艺术家，说以后可能会上，暂留。(2017-10-26)
        if (artist_param.reset) {
            artist_param.curpage = 1;
            artist_hasmore = true;
        }
        $.ajax({
            url: ApiUrl + "/index.php?act=artist&op=index_artist&keyword=&artist_classify=&order=artist_vendue_id",
            type: 'get',
            dataType: 'json',
            data: artist_param,
            async: false,
            success: function (result) {
                if (!artist_hasmore) {
                    return false;
                }
                artist_hasmore = false;
                var data = result.datas;
                var html = '';
                html += template.render('appArts_con', data);
                if (artist_param.reset) {
                    artist_param.reset = false;
                    $("#appArts .swiper-wrapper").html(html);
                } else {
                    $("#appArts .swiper-wrapper").append(html);
                }
                artist_hasmore = result.hasmore;
                artist_param.p = data.p;
                //收藏店铺

                if (data.artist_list.length > 0) {
                    for (var i = 0; i < data.artist_list.length; i++) {
                        //显示收藏按钮
                        if (data.artist_list[i].is_favorate) {
                            $("#store_notcollect_" + data.artist_list[i].store_id).addClass('lm_hidden');
                            $("#store_collected_" + data.artist_list[i].store_id).removeClass('lm_hidden');
                        } else {
                            $("#store_notcollect_" + data.artist_list[i].store_id).removeClass('lm_hidden');
                            $("#store_collected_" + data.artist_list[i].store_id).addClass('lm_hidden');
                        }
                    }
                }

                $(".follow").live('click', function () {
                    var store_id = $(this).attr('nc_store_id');
                    //添加收藏
                    var f_result = favoriteStore(store_id);
                    if (f_result) {
                        $("#store_notcollect_" + store_id).addClass('lm_hidden');
                        $("#store_collected_" + store_id).removeClass('lm_hidden');
                    }
                    return false;
                });
                //取消店铺收藏
                $(".unfollow").live('click', function () {
                    var store_id = $(this).attr('nc_store_id');
                    //取消收藏
                    var f_result = dropFavoriteStore(store_id);
                    if (f_result) {
                        $("#store_collected_" + store_id).addClass('lm_hidden');
                        $("#store_notcollect_" + store_id).removeClass('lm_hidden');
                    }
                    return false;
                });
            }
        });
    }

    artist_page();
    // timeout = setTimeout(function () {
    artist_swiper = new Swiper('.app_arts_container', {
        freeMode: false,
        slidesPerView: 1,
        initialSlide: 1,
        width: window.innerWidth - 70,
        paginationClickable: true,
        spaceBetween: 15,
        slidesOffsetBefore: 35,
        onReachEnd: function () {
            artist_param.curpage++;
            artist_page();
            artist_swiper.update();
            if (artist_hasmore) {
                artist_swiper.setWrapperTranslate(0);
            }
            return false;
        }
    });
    // }, 300);//after 30 seconds

    $.ajax({
        url: ApiUrl + "/index.php?act=index&op=app_index&special_id=1&num=4",
        type: 'get',
        dataType: 'json',
        async: false,
        success: function (result) {
            var data = result.datas;
            var html = '';
            $.each(data, function (k, v) {
                $.each(v, function (kk, vv) {
                    switch (kk) {
                        case 'adv_list':
                        case 'home5':
                        case 'home3':
                            $.each(vv.item, function (k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'store_top':
                            $.each(vv.item, function (k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'home1':
                            break;

                        case 'home2':
                        case 'home4':
                            vv.square_url = buildUrl(vv.square_type, vv.square_data);
                            vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                            vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                            break;
                    }
                    if (kk == 'adv_list') {
                        $("#main-container1").html(template.render(kk, vv));
                    } else if (kk == 'yinuo_store_id') {
                        yinuo_store_id = vv;
                    } else {
                        html += template.render(kk, vv);
                    }

                    return false;
                });
            });
            $("#main-container").html(html);
            // $.ajax({
            //     url: ApiUrl + "/index.php?act=special&op=get_special_list&limit=1",
            //     type: 'get',
            //     dataType: 'json',
            //     success: function (result) {
            //         var data = result.datas;
            //         var html = '';
            //         html += template.render('profit_activity', data);
            //         $(".profit_activity_wrapper").html(html);
            //         //$(".app_recommend").after(html);
            //         lmCuteDown();
            //     }
            // });

            lmCuteDown();
            /*swiper 调用模块*/
            //banner图
            new Swiper('.app_banner', {
                paginationClickable: true,
                pagination: '.app_banner_pagination',
                autoplay: 5000
            });
            //尊享推荐模块
            new Swiper('.app_enjoy', {
                slidesPerView: 3,
                paginationClickable: true,
                spaceBetween: 9,
                slidesOffsetBefore: 9,
            });
            //趣猜模块
            new Swiper('.app_guess_container', {
                pagination: '.app_guess_pagination',
                slidesPerView: 2,
                paginationClickable: true,
                spaceBetween: 10
            });


        }
    });

    if (isWeiXin) {
        var shareData = {
            title: "艺诺网-打造全球最大艺术品保障交易平台",
            desc: "保真品，保增值，保退换。",
            link: window.location.href,
            imgUrl: dataUrl + '/upload/logo.jpg',
        };
        wx_config(shareData);
    }
    //APP分享参数
    appShareSet(shareData);
    // $(window).scroll(function(){
    //     var st = $(window).scrollTop();
    //     // console.log(st);
    //     if(st>47){
    //         $('.app_header').hide();
    //     }else{
    //         $('.app_header').show();
    //     }
    // });
    // if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
    //     if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1){
    //         $('.newPaimai').addClass('newPaimaiHide')
    //     }
    // }
    var str = navigator.userAgent;
    var index = str.lastIndexOf("\/");
    var str2 = str.substring(index + 1, str.length)
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1) {
            if (str2 == '6533.18.5 Appyinuo') {
                return;
            } else {
                $.ajax({
                    url: appUrl1 + '/api/version',
                    type: 'get',
                    dataType: 'json',
                    success: function (result) {
                        console.log(result.data.ios.new)
                        if (result.status_code == 200) {
                            if (result.data.ios.new < str2) {
                                $('.newPaimai').addClass('newPaimaiHide')
                            }
                        }
                    }
                });
            }
        }
    }


});