/**
 * Created by PhpStorm.
 * User:  whk
 * Date:2018.10.10
 */


var storeId;
$(function () {
    console.log(ApiUrl)
    var store_id = getQueryString('store_id');//86
    //轮播图
    $.ajax({
        // url:ApiUrl+'/index.php?act=store&op=special_store_brand_list',
        url: ApiUrl + '/index.php?act=store&op=special_store_slideshow',

        type: 'get',
        dataType: 'json',
        data: {store_id: store_id, limit: 10, recommend: 1},
        async: false,
        success: function (result) {
            var datas = result;
            console.log(result)

            // storeId = data.store_id;
            // if(!$("#lm_enjoy_artist").length||!$("#lm_enjoy_artist_con").length){return false;}
            // $("#lm_enjoy_artist").html(template.render('lm_enjoy_artist_con', data));
            $("#swiper-wrapper").html(template.render('swiperBannerContent', datas));
            // $(".supremacyFirst").html(template.render('supremacyFirstContent', data));
            // $(".works").html(template.render('works', data));
            // $(".enjoyPreference").html(template.render('enjoyPreferenceContent', data));


            // var swiper = new Swiper('.lm_enjoy_artist_wrapper', {
            //     slidesPerView: 4,
            //     paginationClickable: true,
            // });
        }
    });
    // 至尊首推
    $.ajax({
        // url:ApiUrl+'/index.php?act=store&op=special_store_brand_list',
        url: ApiUrl + '/index.php?act=store&op=special_store_brand_list',

        type: 'get',
        dataType: 'json',
        data: {store_id: store_id, limit: 10, recommend: 1},
        async: false,
        success: function (result) {
            var data = result.datas;
            console.log(data)

            storeId = data.store_id;
            // if(!$("#lm_enjoy_artist").length||!$("#lm_enjoy_artist_con").length){return false;}
            // $("#lm_enjoy_artist").html(template.render('lm_enjoy_artist_con', data));
            // $("#swiper-wrapper").html(template.render('swiperBannerContent', data));
            $(".supremacyFirst").html(template.render('supremacyFirstContent', data));
            // $(".works").html(template.render('works', data));
            // $(".enjoyPreference").html(template.render('enjoyPreferenceContent', data));


            // var swiper = new Swiper('.lm_enjoy_artist_wrapper', {
            //     slidesPerView: 4,
            //     paginationClickable: true,
            // });
        }
    });
    //尊享优选
    $.ajax({
        url: ApiUrl + '/index.php?act=store&op=get_quality_goods_list',
        type: 'get',
        dataType: 'json',
        // data:{store_id:store_id},
        async: false,
        success: function (result) {
            var data = result.datas;
            console.log(data)
            // if (!$("#lm_enjoy_typelist").length || !$("#lm_enjoy_typelist_con").length) {
            //     return false;
            // }
            // $("#lm_enjoy_typelist").html(template.render('lm_enjoy_typelist_con', data));
            $(".enjoyPreference").html(template.render('enjoyPreferenceContent', data));

        }
    });

    //推荐作品
    $.ajax({
        url: ApiUrl + '/index.php?act=store&op=special_store_goods_list',
        type: 'POST',
        dataType: 'json',
        data: {store_id: store_id,page:15,curpage:1},
        success: function (result) {
            var data = result.datas;
            // if (!$("#lm_enjoy_typelist_list").length || !$("#lm_enjoy_typelist_list_con").length) {
            //     return false;
            // }
            // $("#lm_enjoy_typelist_list").html(template.render('lm_enjoy_typelist_list_con', data));
            $(".recommendedWorks").html(template.render('recommendedWorksContent', data));

        }
    });

    if (isWeiXin) {
        var shareData = {
            title: "艺诺尊享专区-升级合伙人专用及特惠区!",
            desc: "甄选大师之作，尊享超值特惠，海量福利等你来拿！",
            link: window.location.href,
            imgUrl: dataUrl + '/upload/logo.jpg',
        };
        wx_config(shareData);
    }
    //APP分享参数
    appShareSet(shareData);

//    点击至尊首推的更多
    $('.supremacyMore').click(function () {
        window.location = WapSiteUrl + '/tmpl/enjoyArist.html';
    })
    //点击尊享艺术家
    $('.enjoyArtist').click(function () {
        window.location = WapSiteUrl + '/tmpl/enjoyArist.html';
    })
    //    点击推荐作品的更多
    $('.recommendedWorksMore').click(function () {
        // window.location = WapSiteUrl+ "/tmpl/store_goods.html?store_id=85"
    })
//    点击最底部查看更多
    $('.lookMoreA').click(function () {
        // alert('查看更多')
    })

    //
    $('.works').click(function () {
        window.location = WapSiteUrl + "/tmpl/store_goods.html?store_id=" + storeId
        console.log(storeId)
    })
    //
    $('.search').click(function () {
        window.location = WapSiteUrl + "/tmpl/store_search.html?store_id=" + storeId
        console.log(storeId)
    })

    $('.help').click(function () {
        window.location = WapSiteUrl + "/tmpl/help.html"
        console.log(storeId)
    })




});
