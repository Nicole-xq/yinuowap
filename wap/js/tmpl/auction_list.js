/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function(){
    var key = getCookie('key');
    var keyword = decodeURIComponent(getQueryString('keyword'));
    var hasmore=true;
    var param = {page: 10, curpage: 1, key: key,keyword:keyword};
    function initPage() {
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + '/index.php?act=auction&op=auction_list',
            data: param,
            dataType: 'json',
            type: 'get',
            success: function (result) {
                if(!result) {
                    result = [];
                    result.datas = [];
                    result.datas.search_list = [];
                    result.datas.auction_list = [];
                }
                var data = result.datas;

                window._bd_share_config={
                    "common":{
                        "bdText":"艺诺拍卖",
                        "bdPic":"",
                        "bdUrl":""
                    },
                    "share":{
                        "bdSize":"24"
                    },
                    "selectShare":{
                        "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
                    }
                };
                with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
                param.curpage++;
                var html = template.render('home_body', result.datas);
                $("#auction_list_con").append(html);
                hasmore = result.hasmore;
                $('.cutedown').each(function(){
                    var auction_id=$(this).attr('id');
                    takeCount(auction_id);
                });

                function pubFormSubmit(){
                    if($('#keyword').val() !== ''){
                        window.location.href = buildUrl('auction', $('#keyword').val());
                    }
                    else if ($('#keyword').text() !== '') {
                        window.location.href = buildUrl('auction', $('#keyword').text());
                    }else{
                        window.location.href='auction_list.html';
                    }
                }
                $('#keyword').focus(function () {
                   $(this).val('');
                   $(this).text('');
                });

                $('#search_btn').click(function () {
                    pubFormSubmit()
                    return false;
                });
                if (keyword != '') {
                    $('#keyword').val(keyword);
                }
                $('#searchFormLm').submit(function () {
                    pubFormSubmit();
                    return false;
                });
            }
        });


    }

    initPage();

    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            initPage();
        }
    });

    function takeCount(auction_id) {
        $("#" + auction_id).each(function(){
            var obj = $(this);
            var tms = obj.attr("data-date");
            if(obj.attr('data-date')){
                var displayTime;
                displayTime = setInterval(function () {
                    showTime();
                }, 1000);
                return false;
            }

            function showTime(){
                if (tms <= 0) {
                    clearInterval(displayTime);
                    return;
                }
                else if (tms>0) {
                    tms = parseInt(tms)-1;
                    var days = Math.floor(tms / (1 * 60 * 60 * 24));
                    var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
                    var minutes = Math.floor(tms / (1 * 60)) % 60;
                    var seconds = Math.floor(tms / 1) % 60;

                    if (days < 0) days = 0;
                    if (hours < 0) hours = 0;
                    if (minutes < 0) minutes = 0;
                    if (seconds < 0) seconds = 0;
                    obj.find(".day").html(days);
                    obj.find(".hours").html(hours);
                    obj.find(".minutes").html(minutes);
                    obj.find(".seconds").html(seconds);
                    obj.attr("data-date",tms);
                }
            }
            if (tms > 86400) {
                //时间戳如果是s的时候不是s则多除1000
                var displayTime;
                if (tms == -1) {
                    clearInterval(displayTime);
                    return;
                }
                displayTime = setInterval(function () {
                    showTime();
                }, 1000);
            } else if (tms < 86400) {
                var displayTime;
                displayTime = setInterval(function () {
                    showTime();
                }, 1000);
            }
        });
    }

    $('.auction_list_title li').click(function(){
        $(this).addClass('active').siblings('li').removeClass('active');
        var type=$(this).attr('data-type');
        param.curpage=1;
        hasmore=true;
        delete param['is_kai_pai'];
        if(type){
            param.is_kai_pai=type;
        }
        $("#auction_list_con").html('');
        initPage();
    });
});