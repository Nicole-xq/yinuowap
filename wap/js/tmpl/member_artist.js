var page = 10;
var curpage = 1;
var hasMore = true;
var reset = true;
$(function(){
    var key = getCookie('key');
    if(!key){
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
    }
    
    $.ajax({
        type:'post',
        url:ApiUrl+"/index.php?act=member_artist&op=artist_list&page="+page+"&curpage="+curpage,
        data:{key:key},
        dataType:'json',
        success:function(result){
            checkLogin(result.login);//检测是否登录了
            curpage++;
            hasMore = result.hasmore;
            var data = result;
            data.WapSiteUrl = WapSiteUrl;//页面地址
            data.ApiUrl = ApiUrl;
            data.key = getCookie('key');

            var html = template.render('artist_body', data);

            if (reset) {
                reset = false;
                $("#list-block").html(html);
            } else {
                $("#list-block").append(html);
            }

        }
    });
});