var page = pagesize;
var curpage = 1;
var hasMore = true;
var footer = false;
var reset = true;
var is_lock = true;
var orderKey = '';
var nowTime ;
var daojishiarr = [];
var timer;
	
$(function(){
	var key = getCookie('key');
	if(!key){
		window.location.href = WapSiteUrl+'/tmpl/member/login.html';
	}



//获取服务器时间

    $.ajax({
        type:'get',
        url:ApiUrl2+"/index.php?act=auction&op=timeFetch",
        dataType:'json',
        success:function(result1){

            //nowTime = Number(result1.datas)
            nowTime = result1.datas
            console.log(nowTime)

        }
    })

	function initPage(){
	    if (reset) {
	        curpage = 1;
	        hasMore = true;
	    }
        $('.loading').remove();
        if (!hasMore) {
            return false;
        }
        if ($('#list_all').hasClass('actived')) {
            //get_list('list_all');
            get_list('list_all');
        }
        if ($('#list_have').hasClass('actived')) {
            get_list('list_have');
        }
        if ($('#list_die').hasClass('actived')) {
            get_list('list_die');
        }

	}
	// 分类标签 获取数据
    $('#list_all').on('click', function () {
        if (is_lock) {
            console.log(666)
            //get_list('list_all');
            get_list('list_all');
        }
    });
    $('#list_have').on('click', function () {
        if (is_lock) {
            get_list('list_have');
        }
    });
    $('#list_die').on('click', function () {
        if (is_lock) {
            get_list('list_die');
        }
    });

    // 取消
    $('#order-list').on('click','.cancel-order', cancelOrder);
    // 收货
    $('#order-list').on('click','.sure-order',sureOrder);
    // 查看物流
    $('#order-list').on('click','.viewdelivery-order',viewOrderDelivery);
    // 立即支付
    $('#order-list').on('click','.check-payment',function() {
        var pay_sn = $(this).attr('data-paySn');
        var type = $(this).data('type');
        toPay(pay_sn,'member_buy_auction', 'pay', type);
        return false;
    });

    //取消订单
    function cancelOrder(){
        var order_id = $(this).attr("order_id");

        $.sDialog({
            content: '确定申请退款？',
            okFn: function() { cancelOrderId(order_id); }
        });
    }

    function cancelOrderId(order_id) {
        $.ajax({
            type:"post",
            url:ApiUrl+"/index.php?act=member_auction_order&op=order_cancel",
            data:{order_id:order_id,key:key},
            dataType:"json",
            success:function(result){
                if(result.datas == 0){
                    reset = true;
                    initPage();
                } else {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                }
            }
        });
    }

    //确认订单
    function sureOrder(){
        var order_id = $(this).attr("order_id");

        $.sDialog({
            content: '确定收到了货物吗？',
            okFn: function() { sureOrderId(order_id); }
        });
    }

    function sureOrderId(order_id) {
        $.ajax({
            type:"post",
            url:ApiUrl+"/index.php?act=member_auction_order&op=order_receive",
            data:{order_id:order_id,key:key},
            dataType:"json",
            success:function(result){
                if(result.datas == 0){
                    reset = true;
                    initPage();
                } else {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                }
            }
        });
    }

    // 查看物流
    function viewOrderDelivery() {
        var orderId = $(this).attr('order_id');
        var margin_id = $(this).attr('margin_id');
        location.href = WapSiteUrl + '/tmpl/member/auction_order_delivery.html?order_id=' + orderId + '&margin_id=' + margin_id;
    }
    
    $('#filtrate_ul').find('a').click(function(){
        $('#filtrate_ul').find('li').removeClass('selected');
        $(this).parent().addClass('selected').siblings().removeClass("selected");
        reset = true;
        window.scrollTo(0,0);
        initPage();
    });

    //初始化页面
    initPage();

    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            if (is_lock) {
                initPage();
            }
        }
    });

    if (isWeiXin()){
        $('header').hide();
        $("body").css('padding', '0 0 56px');
    }


});

/*
 * 获取列表信息
 * */
function get_list(type) {
    $('.loading').remove();
    var key = getCookie('key');

    // 选定标签未被激活时，切换标签，重新获取
    if (!$('#'+type).hasClass('actived')) {
        init_get_list(type);
    }
    if ((reset || curpage > 1) && hasMore ) {
        is_lock = false;
        $.ajax({
            type:'get',
            //url:ApiUrl+"/index.php?act=member_auction_order&op=order_list&page="+page+"&curpage="+curpage,
            url:ApiUrl2+"/index.php?act=member_auction_order&op=order_list&page="+curpage,
            data:{key:key, state_type:type,p_type:'auction'},
            dataType:'json',
            success:function(result){
                console.log(result);
                checkLogin(result.login);//检测是否登录了
                curpage++;
                hasMore = result.hasmore;
                if (!hasMore) {
                    get_footer();
                }
                var data = result;
                data.WapSiteUrl = WapSiteUrl;//页面地址
                data.ApiUrl = ApiUrl;
                data.auctionUrl = auctionUrl;
                data.key = getCookie('key');


                template.helper('$getLocalTime', function (nS) {
                    var d = new Date(parseInt(nS) * 1000);
                    var s = '';
                    s += d.getFullYear() + '-';
                    s += (d.getMonth() + 1) + '-';
                    s += d.getDate() + '    ';
                    s += d.getHours() + ':';
                    s += d.getMinutes() + ':';
                    s += d.getSeconds();
                    return s;
                });
                template.helper('p2f', function(s) {
                    return (parseFloat(s) || 0).toFixed(2);
                });
                template.helper('parseInt', function(s) {
                    return parseInt(s);
                });

                var pay = getQueryString("pay");
                var html = template.render('order-list-tmpl', data);


                if (reset) {
                    reset = false;
                    $("#order-list").html(html);
                } else {
                    $("#order-list").append(html);
                }
                is_lock = true;


                $(".time_72").each(function(i,e){
                    newTime = parseInt($(e).text())+259200;
                    var diff1 = newTime-nowTime;
                    daojishiarr.push(diff1)
                })
            }
        });
    }
}
/*
 * 更换标签页之前数据清理
 * */
function init_get_list(type) {
    curpage = 1;
    hasMore = true;
    reset = true;
    $('.title-name').children('a').removeClass('actived');
    $('#'+type).addClass('actived');
    $("#order-list").html('');
}

function show_time(daojishiarr){
    console.log(daojishiarr)
  if(daojishiarr.length == 0) {
    clearInterval(timer)
  }
    $(".time_72").each(function(i,e){
        daojishiarr[i] --;

        if( daojishiarr[i] <= 0 ) {
            daojishiarr = daojishiarr.splice(i,1)
            $(e).eq(i).siblings().text('取消支付')
            $(this).hide()
        }
        var diff = daojishiarr[i];
        console.log(diff)
        var h = Math.floor(diff / 3600) < 10 ? '0'+Math.floor(diff / 3600) : Math.floor(diff / 3600);
        var m = Math.floor((diff / 60 % 60)) < 10 ? '0' + Math.floor((diff / 60 % 60)) : Math.floor((diff / 60 % 60));
        var s = Math.floor((diff % 60)) < 10 ? '0' + Math.floor((diff % 60)) : Math.floor((diff % 60));

        $(e).text( h + ":" + m + ":" + s);

    })

}

function godaojishi(){
    clearInterval(timer)
   timer =  setInterval(function () {
        show_time(daojishiarr)
    },1000)
}



function get_footer() {
    if (!footer) {
        footer = true;
        $.ajax({
            url: WapSiteUrl+'/js/tmpl/footer.js',
            dataType: "script"
          });
    }
}

