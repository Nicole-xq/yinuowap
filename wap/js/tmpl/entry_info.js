$(function () {
    var key = getCookie('key');
    var id = getQueryString('id');
    var data = {key:key, id:id};

    if(navigator.userAgent.indexOf('Appyinuo') !== -1){
        var app = true;
    }else{
        var app = false;
    }

    $(document).ready(function() {
        $('body').on("click",'.main-top-icon1',function() {
            var A=$(this).attr("id");
            var B=A.split("like");
            var messageID=B[1];
            var C=parseInt($("#likeCount"+messageID).html());
            $(this).css("background-position","")
            var D=$(this).attr("rel");

            if(D === 'like')
            {
                $("#likeCount"+messageID).html(C+1);
                $(this).addClass("icon2").attr("rel","unlike");
            }
        });
    });

    function initPage(){
        $.getJSON(
            ApiUrl+"/index.php?act=vote_activity&op=get_entry_info", data,
            function (result) {
                var data = result.datas;
                var detail = data.detail;

                var img = "<img src='/data/upload/cms/vote_activity/"+ detail.image +"'>"
                $('.share-header').html(img);
                if(detail.short_content){
                    $('.huodong-explain').html(detail.short_content);
                    $('.main-center-top').show();
                }
                if(detail.vote_explain){
                    $('.vote-explain').html(detail.vote_explain);
                    $('.main-center-center').show();
                }

                if(result.code !== 200){
                    data = {vote_info:''}
                }
                html = template.render('vote-info-con', data);
                $("#vote-info").html(html);

                if (isWeiXin) {
                    var shareData = {
                        title: detail.title,
                        desc: detail.description,
                        link: window.location.href,
                        imgUrl: window.location.origin + '/data/upload/cms/vote_activity/' + detail.share_logo
                    };

                    wx_config(shareData);
                }
                //APP分享参数
                appShareSet(shareData);
            }
        )
    }
    initPage();

    if(app){
        $('#download_app').hide();
    }
});

function like(id) {

    if(!(navigator.userAgent.indexOf('Appyinuo') !== -1)){
        $.sDialog({content: '请下载App投票', okBtn: false, cancelBtn: false});
        return false;
    }
    var is_vote = true;
    var key = getCookie('key');
    var err_msg = '';
    var is_vote_more = true;

    if(!key){
        location.href = WapSiteUrl + '/tmpl/member/login.html';
    }
    // 限制同时间多次请求;
    if(!is_vote){
        return false;
    }

    // 是否可以投票
    if(!is_vote_more){
        $.sDialog({content: err_msg, okBtn: false, cancelBtn: false});
        return false;
    }

    var A = $('#like');
    var B = $('#likeCount');
    var C = parseInt(B.html());
    var data = {key:key, id:id};

    $.getJSON(
        ApiUrl + '/index.php?act=vote_activity&op=vote', data,
        function (result) {
            if(result.code === 200){
                if(result.datas.state > 0){
                    err_msg = result.datas.msg;
                    $.sDialog({content: err_msg, okBtn: false, cancelBtn: false});
                }else{
                    A.addClass("icon2");
                    B.html(C+1);
                }
                if(result.datas.state === 3){
                    is_vote_more = false;
                }
            }
            is_vote = true;
        }
    );

}