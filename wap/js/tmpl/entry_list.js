
function fresh() {
    if(location.href.indexOf("?reload=true")<0) {
        location.href+="?reload=true";
    }
}
setTimeout("fresh()",50)

var key = '1';
$(function () {
    var page = 10;
    var curpage = 1;
    var hasMore = true;
    var reset = false;
    var key = getCookie('key');
    var is_search = false;
    var is_vote = true;

    if(navigator.userAgent.indexOf('Appyinuo') !== -1){
        var app = true;
    }else{
        var app = false;
    }

    function initPage(){
        if (reset) {
            curpage = 1;
            hasMore = true;
        }
        if (!hasMore) {
            return false;
        }
        hasMore = false;

        var id = getQueryString('act_id');
        var data = {key:key, id:id};
        if(is_search){
            data.search = $('.search').val();
        }

        $.ajax({
            type:'get',
            // url:ApiUrl+"/index.php?act=vote_activity&op=get_entry_list&page="+page+"&curpage="+curpage,
            url:ApiUrl+"/index.php?act=vote_activity&op=get_entry_list",
            data:data,
            dataType:'json',
            success:function(result){
                curpage++;
                hasMore = result.datas.page.hasmore;
                var detail = result.datas.detail;
                var img = "<img src='/data/upload/cms/vote_activity/"+ detail.image +"'>"
                $('.header-top').html(img);
                if(detail.short_content){
                    $('.huodong-explain').html(detail.short_content);
                    $('.main-center-top').show();
                }
                if(detail.vote_explain){
                    $('.vote-explain').html(detail.vote_explain);
                    $('.main-center-center').show();
                }


                var html = template.render('vote_list_con', result.datas);
                if (reset) {
                    reset = false;
                    $("#vote_list_ul").html(html);
                } else {
                    $("#vote_list_ul").append(html);
                }

                if (isWeiXin) {
                    var shareData = {
                        title: detail.title,
                        desc: detail.description,
                        link: window.location.href,
                        imgUrl: window.location.origin + '/data/upload/cms/vote_activity/' + detail.share_logo
                    };

                    wx_config(shareData);
                }
                //APP分享参数
                appShareSet(shareData);
            }
        });
    }

//初始化页面
    initPage();
    /*$(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            initPage();
        }
    });*/

    $(document).on('click', '#search_b', function () {
        reset = true;
        is_search = true;
        initPage();
    });

    if(app){
        $('#download_app').hide();
    }
});

function like(id) {
    var is_vote = true;

    //验证APP
    if(!(navigator.userAgent.indexOf('Appyinuo') !== -1)){
        $.sDialog({content: '请下载App投票', okBtn: false, cancelBtn: false});
        setTimeout(function(){$('#go_download_app').click();},2000);
        return false;
    }
    var key = getCookie('key');

    if(!key){
        location.href = WapSiteUrl + '/tmpl/member/login.html';
    }

    if(is_vote){
        is_vote = false;
    }

    var A = $('.like_'+id);
    var B = $('.e_id_'+id);

    var data = {key:key, id:id};

    $.getJSON(
        ApiUrl + '/index.php?act=vote_activity&op=vote', data,
        function (result) {
            if(result.code === 200){
                if(result.datas.state > 0){
                    $.sDialog({content: result.datas.msg, okBtn: false, cancelBtn: false});
                }else{
                    A.addClass("icon2").attr('like','1');
                    var num = parseInt(B.html());
                    B.html(num+1);
                }
            }
            is_vote = true;
        }
    )
}