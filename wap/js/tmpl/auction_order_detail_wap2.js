/**
 * Created by tinachen on 2018/12/30.
 */
$(function(){
  var key = getCookie('key');
  if(!key){
    window.location.href = WapSiteUrl+'/tmpl/member/login.html';
  }
  template.helper('getLocalTime', function (nS) {
    var d = new Date(parseInt(nS) * 1000);
    var s = '';
    s += d.getFullYear() + '-';
    s += (d.getMonth() + 1) + '-';
    s += d.getDate() + '    ';
    s += d.getHours() + ':';
    s += d.getMinutes() + ':';
    s += d.getSeconds();
    return s;
  });

  // 收货

  $.ajax({
    type:'get',
    url: ApiUrl2 + '/index.php?act=member_auction_order&op=order_info',
    dataType:'json',
    data:{key:key,auction_order_id:getQueryString("auction_order_id")},
    success:function(result) {
      checkLogin(result.login);
      console.log(result)
      var data = result.datas;
      var html = template.render('order-info-tmpl', data);
      $("#order-info-container").html(html);

      $(".confirmOrder").click(sureOrder);

    }
  })

//  //确认订单
  function sureOrder(){
    var order_id = $(this).attr("order_id");
    console.log(66)
    $.sDialog({
      content: '确定收到了货物吗？',
      okFn: function() { sureOrderId(getQueryString("auction_order_id")); }
    });
  }

  function sureOrderId(order_id) {
    $.ajax({
      type:"post",
      url:ApiUrl+"/index.php?act=member_auction_order&op=order_receive",
      data:{order_id:order_id,key:key},
      dataType:"json",
      success:function(result){
        if(result.datas == 0){
          window.location.reload();
        }
      }
    });
  }


});

