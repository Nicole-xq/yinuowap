$(function () {
    var param = {page: 2, curpage: 1};
    var hasmore = true;
    var reset = true;

    function state_ing() {
        if (reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        //此处之所以手动设置为false是防止
        $.ajax({
            url: ApiUrl + '/index.php?act=guess&op=guess_list',
            type: 'GET',
            data: param,
            dataType: 'json',
            success: function (result) {
                param.curpage++;
                hasmore = result.hasmore;
                var data = result.datas;
                var html = template.render('guess_list', data);
                if (reset) {
                    reset = false;
                    $("#guess_list_html").html(html);
                } else {
                    $("#guess_list_html").append(html);
                }
                qllupdateEndTime();
            }
        });
    }

    state_ing();
    function state_end() {
        if (reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + '/index.php?act=guess&op=guess_list&close=1',
            type: 'GET',
            data: param,
            dataType: 'json',
            success: function (result) {
                param.curpage++;
                hasmore = result.hasmore;
                var data = result.datas;
                var html = template.render('guess_list', data);
                if (reset) {
                    reset = false;
                    $("#guess_list_html").html(html);
                } else {
                    $("#guess_list_html").append(html);
                }
                qllupdateEndTime();
            }
        });
    }

    function ready_tab() {
        var $qucai_list_tab = $('.qucai_list_tab'),
            $qucai_info_left_bottom = $('.qucai_info_left_bottom');
        $qucai_list_tab.find('li').click(function () {
            param.curpage = 1;
            $("#guess_list_html").html('');
            hasmore = true;
            var index = $(this).index();
            $(this).addClass('qucai_change').removeClass('qucai_change1');
            $(this).siblings('li').removeClass('qucai_change').addClass('qucai_change1');
            if (index === 0) {
                state_ing();
            } else if (index === 1) {
                state_end();
            }
        })
    }

    ready_tab();


    function qllupdateEndTime() {
        var date = new Date();
        var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

        $(".settime").each(function (i) {

            var endDate = this.getAttribute("endTime"); //结束时间字符串
            //转换为时间日期类型
            var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) {
                    return parseInt(a, 10) - 1;
                }).match(/\d+/g) + ')');

            var endTime = endDate * 1000; //结束时间毫秒数
            var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
            if (lag > 0) {
                var second = Math.floor(lag % 60);
                var minite = Math.floor((lag / 60) % 60);
                var hour = Math.floor((lag / 3600) % 24);
                var day = Math.floor((lag / 3600) / 24);
                $(this).html("距结束&nbsp;<em time_id='d'>" + day + "</em>日<em time_id='h'>" + hour + "</em>时<em time_id='m'>" + minite + "</em>分");
            }

        });
        setTimeout(function () {
            qllupdateEndTime();
        }, 1000);
    }


    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            if ($('.qucai_ing').hasClass('qucai_change')) {
                state_ing();
            } else if ($('.qucai_end').hasClass('qucai_change')) {
                state_end();
            }
        }
    });
});