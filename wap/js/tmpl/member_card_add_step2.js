$(function(){
    var key = getCookie('key');
    var withdrawals = getQueryString('withdrawals');
    var pd = getQueryString('pd');
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
    var card_name= decodeURI(getQueryString('card_name'));
    var card_number= getQueryString('card_number');
    var id_number= getQueryString('id_number');

    $.sValid.init({
        rules:{
            mobile: {
                required:true,
                minlength : 11,
                maxlength : 11,
                digits : true
            }
        },
        messages:{
            mobile: {
                required : "请填写手机号码",
                minlength: '请正确填写手机号',
                maxlength: '请正确填写手机号',
                digits : '请正确填写手机号'
            }
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });

    // 发送手机验证码
    $('#mobile_btn').click(function(){
        var mobile = $('#mobile').val();
        if(mobile != ''){
            send_sms(mobile,key);
        }else{
            $.sDialog({
                skin:"red",
                content:'请先填写手机号',
                okBtn:false,
                cancelBtn:false
            });
        }

    });
    var can_submit = true;
    $('#btnform').click(function(){
        if(can_submit == false){
            return;
        }
        if($.sValid()) {
            can_submit = false;
            var mobile = $('#mobile').val();
            var captcha= $('#captcha').val();
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=member_card&op=bankcard_step3",
                data:{key:key,true_name:card_name,bank_card:card_number,mobile:mobile,id_number:id_number,captcha:captcha},
                dataType:'json',
                success:function(result){
                    can_submit = true;
                    if(result.code == 200){
                        if (withdrawals && withdrawals == 1) {
                            console.log(result.datas)
                            //如果是从提现过来的
                            setTimeout(function(){
                                location.href = WapSiteUrl + '/tmpl/member/withdrawals.html?withdrawals=' + withdrawals + '&pd=' + pd +'&bankcard_id=' + result.datas
                            },1000);
                            return false;
                        }
                        setTimeout("location.href = WapSiteUrl+'/tmpl/member/member_card.html'",1000);
                    }else{
                        errorTipsShow('<p>' + result.datas.error + '</p>');
                    }
                }
            });
        }
    });
    if (isWeiXin()){
        $('header').hide();
        $("body").css('padding', '0');
    }
});

// 发送手机验证码
function send_sms(mobile,key) {
    $.getJSON(ApiUrl+'/index.php?act=member_card&op=bankcard_step2', {phone:mobile,key:key}, function(result){
        if(result.code == 200){
            $.sDialog({
                skin:"green",
                content:'发送成功',
                okBtn:false,
                cancelBtn:false
            });
            $('.send_captcha').hide();
            $('.code-countdown').show().find('em').html(result.datas.sms_time);
            var times_Countdown = setInterval(function(){
                var em = $('.code-countdown').find('em');
                var t = parseInt(em.html() - 1);
                if (t == 0) {
                    $('.send_captcha').show();
                    $('.code-countdown').hide();
                    clearInterval(times_Countdown);
                } else {
                    em.html(t);
                }
            },1000);
        }else{
            errorTipsShow('<p>' + result.datas.error + '<p>');
        }
    });
}