/**
 * Created by Li on 2017/6/1 0001.
 */
$(function () {
    var $title=$('.qll_tab_title'),
        $tabCon=$('.qll_tab_con'),
        apiKey=['order_commission','order_margin','member_join'],
        num=0;

    $title.find('li').removeClass('current');
    $title.find('li').eq(num).addClass('current');
    $tabCon.find('.qll_tab_section').hide();
    $tabCon.find('.qll_tab_section').eq(num).show();

    $title.find('li').click(function(){
        var index=$(this).index();
        initPage(apiKey[index],index);
        $title.find('li').removeClass('current');
        $title.find('li').eq(index).addClass('current');
        $tabCon.find('.qll_tab_section').hide();
        $tabCon.find('.qll_tab_section').eq(index).show();
        return false;
    });
    initPage(apiKey[num],num);

    function initPage(apiKey,nowNum) {
        // $("#transaction_con").html('');
        //数据渲染
        var key = getCookie('key');
        if (!key) {
            window.location.href = WapSiteUrl + '/tmpl/member/login.html';
            return;
        }
        $.showPreloader('加载中...');
        setTimeout(function () {
            $.hidePreloader();
        }, 500);
        //渲染list
        var load_class = new ncScrollLoad();
        load_class.loadInit({
            'url': ApiUrl + '/index.php?act=member_fund&op='+apiKey+'',
            'getparam': {'key': key,'type': 1},
            'tmplid': 'transaction_list',
            'containerobj': $("#transaction_con"),
            'iIntervalId': true,
            callback:function(){
                $tabCon.find('.qll_tab_section').eq(nowNum).show();
            }
        });
        //获取佣金总额
        $.getJSON(ApiUrl + '/index.php?act=member_fund&op=getMemberCommissionCount', {'key':key,'form_member_id':Request_pub("form_member_id")}, function(result){
            var data=result.datas.sum_commission_amount;
            $('#amount').html(result.datas.sum_commission_amount);
            $('#amount_1').html(result.datas.sum_commission_amount_1);
            $('#amount_2').html(result.datas.sum_commission_amount_2);
            // $('#sum_commission_amount_lm').html(data);
        });
    }
});
