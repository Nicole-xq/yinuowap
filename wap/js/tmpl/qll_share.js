function wx_config(invite_code) {
    var shareData = {
        title: "300w+艺术爱好者邀您一起走进艺术 ",
        desc: "分享好友，获取活动入场邀请函，领取活动大红包，让艺术照进生活！",
        link: WapSiteUrl+'/tmpl/member/member_qrcode.html?invite_code='+invite_code,//$("#bind_uri").val(),
        imgUrl: WapSiteUrl+'/images/lm/logo-2.png',
        // imgUrl: dataUrl+'/upload/logo.jpg'
    };
    appShareSet(shareData);
    var url = location.href.split('#')[0];
    var jssdk_url = ApiUrl + "/index.php?act=jssdk&op=get_sign_package&url="+url;
    $.getJSON(jssdk_url, function (result) {
        var data = result.datas;  //alert(JSON.stringify(data));
        wx.config({
            debug: false,
            appId: data.appId,
            timestamp: data.timestamp,
            nonceStr: data.nonceStr,
            signature: data.signature,
            jsApiList: [
                "onMenuShareTimeline", //分享到朋友圈
                "onMenuShareAppMessage", //分享到朋友
            ]
        });

        //配置分享信息
        wx.ready(function () {
            wx.onMenuShareTimeline(shareData);
            wx.onMenuShareAppMessage(shareData);
        });
        // 错误提示
        wx.error(function(res){
            //alert(JSON.stringify(res));
        });
    });
}


/**
 * app 分享功能
 * @param shareData
 */
var appShareStr;
function appShareSet(shareData){
    var pic = shareData.imgUrl;
    // shareData.imgUrl = pic.replace('http://','https://');
    appShareStr = JSON.stringify(shareData);
}
function appShareData(){
    var str = jsObj.shareData(appShareStr);
}
