/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    //获取微信openid
    var key = getCookie('key');
    if (key) {
        window.location.href = WapSiteUrl + '/tmpl/member/member.html';
        return;
    }
    var unionid = getQueryString('unionid');
    $("#unionid").val(unionid);

    $.sValid.init({
        rules: {
            username: {
                required: true,
                mobile: true
            },
            captcha: "required",
            password: {
                required: true,
                maxlength: 10,
                minlength: 6
            }
        },
        messages: {
            username: "手机号必须填写！",
            captcha: "请填写验证码",
            password: {
                required: "密码必填!",
                maxlength: "密码格式错误!",
                minlength: "密码格式错误!"
            }
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                $.map(eId, function (idx, item) {
                    $('#' + idx).parents('.login_form_input_panpel').addClass('error');
                    $('#' + idx).attr('placeholder', eMsg[idx + '_required']);
                });
            } else {
                $('#' + eId).parents('.login_form_input_panpel').removeClass('error');
            }
        }
    });

    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state', function(result){
        var ua = navigator.userAgent.toLowerCase();
        var allow_login = 0;
        if (result.datas.pc_qq == '1') {
            allow_login = 1;
            $('.qq').show();
        }
        if (result.datas.pc_sn == '1') {
            allow_login = 1;
            $('.weibo').show();
        }
        if ((ua.indexOf('micromessenger') > -1) && result.datas.connect_wap_wx == '1') {
            allow_login = 1;
            $('.wx').show();
        }
        if (allow_login) {
            $('.quick_login').show();
        }
    });

    //点击获取验证码
    var getCodeFlag = true;
    $('#getCode').click(function () {
        var val = $('#username').val();
        var validata = /^(1{1})+\d{10}$/.test(val);
        if (!getCodeFlag || !validata) {
            if (!validata) {
                $('#username').parents('.login_form_input_panpel').addClass('error');
                $('#username').attr('placeholder', '请填写手机号!');
            }
            return false;
        }
        getCodeFlag = false;
        var self = $(this),
            time = 60;

        setTimeout(function () {
            getCodeFlag = true;
        }, 3000);
        $.getJSON(ApiUrl + '/index.php?act=login&op=get_sms_captcha', {type: 1, phone: val}, function (result) {
            console.log(result)
            if (!result.datas.error) {
                lm_errorTipsShow('<p>发送短信中....</p>');
                var times_Countdown = setInterval(function () {
                    time--;
                    if (time == 0) {
                        self.html('<i class="iconfont icon-shouji"></i>再次获取');
                        clearInterval(times_Countdown);

                    } else {
                        self.html('<span style="color:#8c8c8c;">' + time + '秒后再次获取</span>')
                    }
                }, 1000);
            } else {
                lm_errorTipsShow('<p>' + result.datas.error + '</p>');
            }
        });
    });

// 提交数据
    $('#bindPhonebtn').click(function () {
        var username = $("#username").val();
        var captcha = $('#captcha').val();
        var pwd = $("#password").val();
        var unionid = $("input[name=unionid]").val();
        if ($.sValid()) {
            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=connect&op=wxRegister",
                data: {
                    user_info: {
                        nickname: username,
                        captcha: captcha,
                        member_mobile: username,
                        pwd: pwd,
                        member_mobile_bind: 1,
                        unionid: unionid
                    }
                },
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        addCookie('username', result.datas.username);
                        addCookie('key', result.datas.key);
                        lm_errorTipsShow('<p>绑定中....</p>');
                        location.href = WapSiteUrl + '/tmpl/member/member.html?get_register_award=get_register_award';
                    } else {
                        lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                    }
                }
            });
        }
        return false;
    });

});