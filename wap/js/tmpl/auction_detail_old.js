var auction_id = getQueryString("auction_id");

$(function () {
    var key = getCookie('key');

    get_detail(auction_id);

    function get_detail(auction_id) {
        //渲染页面
        $.ajax({
            url: ApiUrl + "/index.php?act=auction&op=auction_detail",
            type: "get",
            data: {auction_id: auction_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                window._bd_share_config={
                    "common":{
                        "bdText":data.auction_info.auction_name,
                        "bdPic":data.auction_info.auction_image_path,
                        "bdUrl":""
                    },
                    "share":{
                        "bdSize":"24"
                    },
                    "selectShare":{
                        "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
                    }
                };
                with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
                var tmp_param = template;
                tmp_param.isEscape = 0;
                var html = tmp_param.render('auction_detail', data);
                $("#auction_detail_html").html(html);

                /*拍品倒计时*/
                $("#saleCountDown").ncCountDown({
                    time: data.auction_info.auctioning_time,
                    unitDay:"天",
                    unitHour:"小时",
                    unitMinute:"分钟",
                    unitSecond:"秒",
                    end: function() {
                        //倒计时结束回调
                        //location.reload();
                    }
                });
                $("#qll_saleCountDown").ncCountDown({
                    time:data.auction_info.auction_remain_start_time,
                    unitDay:"天",
                    unitHour:"小时",
                    unitMinute:"分钟",
                    unitSecond:"秒",
                    end: function() {
                        //倒计时结束回调
                        location.reload();
                    }
                });
                for (var i = 0; i < data.recommend_list.special.length; i++) {
                    /*专场倒计时*/
                    $("#special_"+i).ncCountDown({
                        time: data.recommend_list.special[i].special_remain_date,
                        unitDay:"天",
                        unitHour:"小时",
                        unitMinute:"分钟",
                        unitSecond:"秒",
                        end: function() {
                            //倒计时结束回调
                            //location.reload();
                        }
                    });
                }

                //图片轮播
                var swiper = new Swiper('.swiper-container-banner', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    loop : true,
                    autoplay : 3000,
                });
                //商品视频
                if (data.auction_video_path) {
                    var player = new prismplayer({
                        id: "J_prismPlayer", // 容器id
                        source: data.auction_video_path, // 视频url 支持互联网可直接访问的视频地址
                        autoplay: true, // 自动播放
                        width: "100%", // 播放器宽度
                        height: "16rem", // 播放器高度
                        skinLayout: false
                    });
                }
                // 初始化插件
                var offer_num_value  = accAdd(data.auction_info.current_price, data.auction_info.auction_increase_range);
                var step = accAdd(data.auction_info.auction_increase_range, 0);

                $(".buy-num").val(offer_num_value.toFixed(2));
                //出价，减
                $(".minus").click(function () {
                    var buynum = $(".buy-num").val();
                    if (buynum >= offer_num_value + step) {
                        $(".buy-num").val(parseInt(buynum - step).toFixed(2));
                    }
                });
                //出价
                $(".add").click(function () {
                    var buynum = parseInt($(".buy-num").val());
                    if (buynum < 999999999) {
                        $(".buy-num").val(parseInt(buynum + step).toFixed(2));
                    }
                });

                //收藏
                $(".collect").click(function () {
                    if ($(this).hasClass('favorate')) {
                        if (dropFavoriteAuction(auction_id)) {
                            $(this).removeClass('favorate');
                            location.reload();
                        }
                    } else {
                        if (favoriteAuction(auction_id)) {
                            $(this).addClass('favorate');
                            location.reload();
                        }
                    }
                });
                // 联系客服
                $('.kefu').click(function () {
                    window.location.href = WapSiteUrl + '/tmpl/member/chat_info.html?auction_id=' + auction_id + '&t_id=' + data.auction_info.store_member_id;
                });

                //推荐专场倒计时
                for(var i=0; i<data.recommend_list.special.length;i++){
                    if(data.recommend_list.special[i].is_kaipai == 1){
                        takeCount(data.recommend_list.special[i].special_id);
                    }
                }
                if(isWeiXin){
                    var shareData = {
                        title: data.auction_info.auction_name+'-艺诺拍卖',
                        desc: window.location.href,
                        link: window.location.href,
                        imgUrl: data.auction_info.auction_image_mobile[0],
                     };
                     wx_config(shareData);
                 }
            }
        });

    }
        function init_lm_page(){
          $.ajax({
            url: ApiUrl + "/index.php?act=auction&op=auction_detail",
            type: "get",
            data: {auction_id: auction_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                if(!data.bid_log_list.length){return false;}
                $('.qll_price').html(data.bid_log_list[0].offer_num);
                var sum= parseFloat(data.bid_log_list[0].offer_num)+parseFloat(data.auction_info.auction_increase_range);
                // $('#buynum').val(sum.toFixed(2))
            }
          })
            // setTimeout(init_lm_page,1000);
        }
        init_lm_page();

});

/*
* 出价
* */
function bid() {
    var auction_id = $('#auction_id').val();
    var bid_num = parseInt($(".buy-num").val());
    var key = getCookie('key');
    is_login(key);
    $.ajax({
        type: 'POST',
        url: ApiUrl + "/index.php?act=auction&op=member_offer",
        data: {auction_id: auction_id, key: key, bid_num: bid_num},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                $.sDialog({skin: "green", content: '出价成功', okBtn: false, cancelBtn: false});
                setTimeout('location.reload()', 200);
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
}   
/*
* 验证是否登录
* */
function is_login(key) {
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return false;
    }
}

// 倒计时
function takeCount(special_id) {
    $("#special_"+ special_id).each(function(){
        var obj = $(this);
        var tms = obj.attr("count_down");
        if (tms>0) {
            tms = parseInt(tms)-1;
            var days = Math.floor(tms / (1 * 60 * 60 * 24));
            var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
            var minutes = Math.floor(tms / (1 * 60)) % 60;
            var seconds = Math.floor(tms / 1) % 60;

            if (days < 0) days = 0;
            if (hours < 0) hours = 0;
            if (minutes < 0) minutes = 0;
            if (seconds < 0) seconds = 0;
            obj.find("[time_id='d']").html(days);
            obj.find("[time_id='h']").html(hours);
            obj.find("[time_id='m']").html(minutes);
            obj.find("[time_id='s']").html(seconds);
            obj.attr("count_down",tms);
        }
    });
    setTimeout("takeCount("+ special_id + ")", 1000);
}

/*
* 设置提醒
* */
function set_remind() {
    var auction_id = $('#auction_id').val();
    var key = getCookie('key');
    is_login(key);
    $.ajax({
        type: 'POST',
        url: ApiUrl + "/index.php?act=auction&op=check_mobile",
        data: {key: key},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                $.sDialog({skin: "green", content: '请先绑定手机', okBtn: false, cancelBtn: false});
                setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                // window.location.href='../member/member_mobile_bind.html';
            } else {
                $.ajax({
                    type: 'POST',
                    url: ApiUrl + "/index.php?act=auction&op=set_remind",
                    data: {auction_id: auction_id, key: key},
                    dataType: "json",
                    success: function (result) {
                        if (result.datas == 0) {
                            $.sDialog({skin: "green", content: '设置提醒成功', okBtn: false, cancelBtn: false});
                            setTimeout('location.reload()', 200);
                        } else {
                            $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                        }
                    }
                });
            }
        }
    });

}

/*
* 取消提醒
* */
function cancel_remind(relation_id) {
    var auction_id = $('#auction_id').val();
    var key = getCookie('key');
    is_login(key);
    $.ajax({
        type: 'POST',
        url: ApiUrl + "/index.php?act=auction&op=cancel_remind",
        data: {auction_id: auction_id, key: key, relation_id: relation_id},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                $.sDialog({skin: "green", content: '取消提醒成功', okBtn: false, cancelBtn: false});
                setTimeout('location.reload()', 200);
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
}


//添加实时刷新拍卖者模块

function strToJson(str){
    return JSON.parse(str);
}

if(getCookie('count_rows_mob')){
    delCookie('count_rows_mob');
}
// auto url detection
var auctionUrl=ApiUrl+"/index.php?act=auction&op=get_bid_list&key=&auction_id="+Request_pub('auction_id');
function init_lm_page(){
    $.ajax({
        url: auctionUrl,
        cache: false,
        success: function(data){
            var result=strToJson(data).datas;
            if(!result.bid_log_list.length){
                  return false;
            }
            if(getCookie('count_rows_mob')&&getCookie('count_rows_mob')==result.count_rows){
            }else{
                $('#auction_content_lm').html('');
                var html='';
                html+='<tr>\
                    <td><span style="background: #800000">领先</span></td>\
                    <td>'+result.bid_log_list[0].member_name+'</td>\
                    <td>'+result.bid_log_list[0].created_at+'</td>\
                    <td>¥'+result.bid_log_list[0].offer_num+'</td>\
                </tr>';
                $.each(result.bid_log_list,function(i,e){
                    if(i>0){
                        html+='<tr>\
                            <td>\
                            <span style="background: #898989">出局</span>\
                            </td>\
                            <td>'+e.member_name+'</td>\
                            <td>'+e.created_at+'</td>\
                            <td>¥'+e.offer_num+'</td>\
                        </tr>';
                    }
                });


                for (var k=1;k<result.bid_log_list.length;k++){}
                $('#auction_content_lm').html(html);
                addCookie('count_rows_mob',result.count_rows);
            }
            // setTimeout(init_lm_page,1000);

        }
    });




}
init_lm_page();