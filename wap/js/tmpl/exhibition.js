/**
 *
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/03/06
 */
$(function () {

    var key = getCookie('key');
    var out_date_flag = false;
    var ex_id = getQueryString('ex_id');
    var invite_code = getQueryString('invite_code');
    var param = {key: key, ex_id: ex_id, invite_code:invite_code};

    $.ajax({
        url: ApiUrl + "/index.php?act=exhibition&op=get_exhibition_content",
        type: 'get',
        data: param,
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            var tmp_param = template;
            tmp_param.isEscape = false;
            var html = tmp_param.render('cms_activity', data);
            $("#cms_activity_wrapper").html(html);
            var detail = data.detail;

            $('.qrcode .ex_name').html(data.ex_inviter.name);
            $('.qrcode .ex_mobile').html(data.ex_inviter.mobile);
            $('.qrcode .entry_end_time').html(detail.entry_end_time);
            $('.qrcode').show();
            if(data.ex_inviter.wx_qrcode_ticket){
                var qrcode_url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" + data.ex_inviter.wx_qrcode_ticket;
                $('#qrcode').html('<img src="'+qrcode_url+'"/>');
            }else{
                $('#qrcode').html('<img src="/wap/images/boot/qrcode_default.png"/>');
            }
            out_date_flag=detail.out_date_flag;
            document.title=detail.title;
            if (isWeiXin) {
                var shareData = {
                    title: detail.title,
                    desc: detail.description,
                    link: window.location.href,
                    imgUrl: window.location.origin + '/data/upload/cms/exhibition/' + detail.share_logo
                };

                wx_config(shareData);
            }
            //APP分享参数
            appShareSet(shareData);
        }
    });

    $(".entry_into").on('click',function () {
        if (!out_date_flag){
            $.sDialog({content: '报名时间已过', okBtn: false, cancelBtn: false});
            return false;
        }
        if(key){
            $.getJSON(
                ApiUrl + '/index.php?act=exhibition&op=get_check_entry', param,
                function (result) {
                    if(result.code === 200){
                        $.sDialog({content: '您已报名成功', okBtn: false, cancelBtn: false});
                    }else{
                        window.location.href = WapSiteUrl + '/tmpl/member/exhibition_entry.html?ex_id='+ ex_id + '&invite_code=' + invite_code;
                    }
                }
            );
        }else{
            window.location.href = WapSiteUrl + '/tmpl/member/exhibition_entry.html?ex_id='+ ex_id + '&invite_code=' + invite_code;
        }
    });

    $(".register").on('click',function () {
        window.location.href = WapSiteUrl + '/tmpl/member/register.html?ex_id='+ ex_id + '&invite_code=' + invite_code;
    });
});