/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
var gs_id = getQueryString("gs_id");
var key = getCookie('key');

var per_count = 20;
var curpage = 1;
var reset = true;
var param = {page: per_count, curpage: curpage, gs_id: gs_id, key: key, hasmore: true, reset: true};

$(function () {
    get_listMore(gs_id);
    function get_listMore(gs_id) {
        if (param.reset) {
            param.curpage = 1;
            param.hasmore = true;
        }
        if (!param.hasmore) {
            return false;
        }
        param.hasmore = false;
        $.ajax({
            type: "get",
            url: ApiUrl + "/index.php?act=guess&op=guess_log",
            data: param,
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                var plLen = data.guess_count||0;
                $('#guess_detail_html .auction_detail_history .title').html(plLen+'条');
                var tmp_param = template;
                tmp_param.isEscape = 0;
                var html = '';
                html = tmp_param.render('guess_detail', data);
                if (param.reset) {
                    param.reset = false;
                    $("#guess_detail_html ul").html(html);
                } else {
                    $("#guess_detail_html ul").append(html);
                }
                param.curpage++;
                if (param.curpage > data.total) {
                    param.hasmore = false;
                }else{
                    param.hasmore = true;
                }
            }
        });
    }
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            setTimeout(function () {
                get_listMore(gs_id);
            }, 100);
        }
    });
});