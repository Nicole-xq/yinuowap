$(function () {
    var key = getCookie('key');
    var edit_name = getQueryString('edit_name');
    var _edit = false;
    if(edit_name){
        switch (edit_name){
            case 'member_truename':
                _edit = true;
                $('.title').html('昵称');
                break;
            case 'member_real_name':
                _edit = true;
                $('.title').html('姓名');
                break;
            case 'member_using_mobile':
                _edit = true;
                $('.title').html('手机号');
                break;
        }
    }
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
    }
    var obj = {
        save_url: ApiUrl + "/index.php?act=member_center&op=save_information",
        filePath: '',
        img_size: 1024 * 1024,
        newImg: new Image(),
        data: {key: key, edit_name: '', edit_value: ''},
        init: function () {
            //页面初始化
            $.ajax({
                type: "POST",
                url: ApiUrl + "/index.php?act=member_center&op=get_information",
                dataType: 'json',
                data: {key: key},
                success: function (result) {
                    if(_edit){
                        var datas = {'edit_name':edit_name, 'edit_value':result.datas[edit_name]};
                    }else{
                        var datas = result.datas;
                    }
                    var html = template.render('accountBox', datas);
                    $("#accountBoxHtml").html(html);
                }
            });
        },
        post: function () {
            obj.data.edit_name = $('.edit_value').attr('name');
            obj.data.edit_value = $('.edit_value').val();
            $.ajax({
                type: "POST",
                url: obj.save_url,
                dataType: 'json',
                data: obj.data,
                success: function (result) {
                    if (result.code == 200) {
                        window.location.href = WapSiteUrl + '/tmpl/member/member_account.html';
                    } else {
                        //待完善，错误提示
                        // alert(result.datas.error)
                        $.sDialog({
                            skin:"red",
                            content:result.datas.error,
                            okBtn:false,
                            cancelBtn:false
                        });
                    }
                }
            });
        },
        form: function (obj_form) {
            $.ajax({
                url: obj.save_url + '&key=' + key,
                type: 'POST',
                data: new FormData(obj_form),
                dataType: 'json',
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.code == 200) {
                        window.location.href = WapSiteUrl + '/tmpl/member/member_account.html';
                    } else {
                        //待完善，错误提示
                        // alert(result.datas.error)
                        $.sDialog({
                            skin:"red",
                            content:result.datas.error,
                            okBtn:false,
                            cancelBtn:false
                        });
                    }
                }
            });
        }
    };

    obj.init();

    //保存图片
    $(document).on('change', '.choose_avatar_inp', function () {
        var files = this.files, URL = window.URL || window.webkitURL;
        if (!files[0]) return;
        if (files[0].type.search(/image/) >= 0) {
            $('.member_avatar').html(obj.newImg);
            obj.newImg.src = URL.createObjectURL(files[0]);
        }
    });

    $(document).on('click', '.photo_avatar', function () {
        obj.form($("#uploadForm")[0]);
    });

    $(document).on('click', '.member_name_preservation', function () {
        obj.post();
    });

    $(document).on('click', '.member_name_content img', function () {
        $('.member_name_content textarea').val('');
    });
});