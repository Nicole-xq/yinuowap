/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function() {
    var key = getCookie('key');
    var artist_vendue_id = getQueryString("artist_vendue_id");
    var type = getQueryString("type");
    if(!artist_vendue_id){
        window.location.href = WapSiteUrl+'/index.html';
    }
    // $.showPreloader('加载中....');
    //加载详情
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=artist&op=artist_detail",
        data: {key:key,artist_vendue_id:artist_vendue_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            var tmp_param = template;
            tmp_param.isEscape = 0;

            var html = tmp_param.render('detail', data);
            $("#lm_art_home").html(html);
            document.title=result.datas.artist_details.artist_name+'艺术馆';
            // $.hidePreloader();
            //显示收藏按钮
            if (data.artist_details.is_favorate) {
                $("#store_notcollect").addClass('lm_hidden');
                $("#store_collected").removeClass('lm_hidden');
            }else{
                $("#store_notcollect").removeClass('lm_hidden');
                $("#store_collected").addClass('lm_hidden');
            }

            if(type && type == 'work'){
                window.location.href="#artist_works";
            }
            // 联系客服
            $('.lm_kefu').click(function () {
                window.location.href = WapSiteUrl + '/tmpl/member/chat_info.html?t_id=' + data.artist_details.member_id;
            });
            $('#lm_store_id').val(data.artist_details.store_id);
            init_page();
            console.log(data);
            if(isWeiXin){
                var artName=data.artist_details.artist_name;
                // var val='艺诺'+artName+'个人艺术馆，包括'+artName+'简介、作品欣赏、艺术资讯新闻、作品价格、展览，评论互动等信息，为您查询'+artName+'资料及了解艺术作品提供最有价值的参考。';
                var shareData = {
                    title: data.artist_details.artist_name+'艺术馆-艺诺网',
                    desc: data.artist_details.artist_job_title,
                    link: window.location.href,
                    imgUrl: data.artist_details.artist_image_path,
                };
                wx_config(shareData);
            }
            //APP分享参数
            appShareSet(shareData);
        }
    });
    $(".point").live('click',function(){
        $(".nomal").addClass("display-out");
        $(".point").addClass("display-none");
    });

    //收藏店铺
    $("#store_notcollect").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect").addClass('lm_hidden');
            $("#store_collected").removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0?t+1:1;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });
    //取消店铺收藏
    $("#store_collected").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected").addClass('lm_hidden');
            $("#store_notcollect").removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1?t-1:0;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });

    var curpage_imgList = 1,
        reset_imgList = true,
        total_pages=0,
        hasmore_imgList = true;
    var param = {page:curpage_imgList,total_pages:total_pages,reset:true,hasmore:true};

    function init_page(){
        if(!$('#imgList').length){return false;}
        if (param.reset) {
            param.page = 1;
            param.hasmore = true;
        }
        if (!param.hasmore) {
            return false;
        }
        param.hasmore = false;
        //获取图片列表
        var store_id=$('#lm_store_id').val();
        $.ajax({
            type: 'get',
            url: ApiUrl + "/index.php?act=artist&op=ajax_art_sale_goods_page",
            data: {store_id:store_id,per_count:4,page:param.page},
            dataType: 'json',
            async:false,
            success: function(result) {
                param.page++;
                var data=result.datas;
                var meta=result.datas.meta;
                param.total_pages=meta.pagination.total_pages;
                if(param.page>param.total_pages){
                    param.hasmore=false;
                } else {
                    param.hasmore = true;
                }
                var tmp_param = template;
                tmp_param.isEscape = 0;
                var html='';
                html=tmp_param.render('imgList', data);
                if (param.reset) {
                    param.reset = false;
                    $("#imgList_con").html(html);
                } else {
                    $("#imgList_con").append(html);
                    html='';
                }

                var total=meta.pagination.total;
                $('.lm_setting_info #fileNum').text(total);
            }
        });
    }

    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            setTimeout(function () {
            init_page();
            },0);
        }
    });

});


