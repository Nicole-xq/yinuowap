﻿var key = getCookie('key');
$(function (){
    $.ajax({
        url: ApiUrl + "/index.php?act=new_footer",
        type: 'get',
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            var item = data.item_info.item_data.item;
            var item_html = '';
            if(item.length > 0){
                for(var i=0;i<item.length;i++){
                    if(item[i].type != 'url'){
                        item_html += '<a class="tab-item external" href="'+buildUrl(item[i].type, item[i].data)+'">'
                            + '<span class="icon"><img src="'+item[i].image_path+'" alt=""/></span>'
                            + '<span class="tab-label">'+item[i].title+'</span>'
                            + '</a>';
                    }else{
                        item_html += '<a class="tab-item external" href="'+item[i].data+'">'
                            + '<span class="icon"><img src="'+item[i].image_path+'" alt=""/></span>'
                            + '<span class="tab-label">'+item[i].title+'</span>'
                            + '</a>';
                    }
                }
                var html = '<nav class="bar bar-tab footer">'
                    + item_html
                    + '</nav>';
                $("#footer").html(html);
            }
        }
    });
    $('#logoutbtn').click(function(){
        var username = getCookie('username');
        var key = getCookie('key');
        var client = 'wap';
        $.ajax({
            type:'get',
            url:ApiUrl+'/index.php?act=logout',
            data:{username:username,key:key,client:client},
            success:function(result){
                if(result){
                    delCookie('username');
                    delCookie('key');
                    //lm 17.5.19
                    delCookie('cart_count');
                    delCookie('invite_code');
                    location.href = WapSiteUrl;
                }
            }
        });
    });
    // 消息列表页判断有否登陆
    if (key == '' || key == undefined){
        $(".chat_list").attr('href',WapSiteUrl+'/tmpl/member/login.html');
    }
});