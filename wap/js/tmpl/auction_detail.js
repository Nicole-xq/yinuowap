/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    var auction_id = getQueryString("auction_id");
    var key = getCookie('key');
    function initPage() {
        $.ajax({
            url: ApiUrl + "/index.php?act=auction&op=auction_detail",
            type: "get",
            data: {auction_id: auction_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                var tmp_param = template;
                tmp_param.isEscape = 0;
                var html = tmp_param.render('auction_detail', data);
                $("#auction_detail_html").html(html);
                lmCuteDown();
                new Swiper('.auction_detail_banner', {
                    loop: true,
                    paginationClickable: true,
                    pagination: '.auction_detail_pagination',
                    autoplay: 5000,
                    height: 300,
                });
                // 初始化插件
                var offer_num_value  = accAdd(data.auction_info.current_price, data.auction_info.auction_increase_range);
                var step = accAdd(data.auction_info.auction_increase_range, 0);
                $(".buy_num").val(offer_num_value.toFixed(2));
                //出价，减
                $(".minus").click(function () {
                    var buynum = $(".buy_num").val();
                    if (buynum >= offer_num_value + step) {
                        $(".buy_num").val(parseInt(buynum - step).toFixed(2));
                    }
                });
                //出价,加
                $(".add").click(function () {
                    var buynum = parseInt($(".buy_num").val());
                    if (buynum < 999999999) {
                        $(".buy_num").val(parseInt(buynum + step).toFixed(2));
                    }
                });

                //出价
                $('.offer').click(function () {
                    if($('.auction_detail_mask').hasClass('lm_hidden')){
                        $('.auction_detail_mask').removeClass('lm_hidden');
                        $('.auction_detail_mask_close').click(function () {
                          $('.auction_detail_mask').addClass('lm_hidden');
                          return false;
                        });
                    }else{
                        bid();
                    }
                    return false;
                });

                /*
                 * 设置提醒
                 * */
                
                $('.remind').click(function () {
                    var _self = $(this);
                    is_login(key);
                    if(!$(_self).hasClass('active')){
                        $.ajax({
                            type: 'POST',
                            url: ApiUrl + "/index.php?act=auction&op=check_mobile",
                            data: {key: key},
                            dataType: "json",
                            success: function (result) {
                                if (result.datas == 0) {
                                    $.sDialog({skin: "green", content: '请先绑定手机', okBtn: false, cancelBtn: false});
                                    setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                                    // window.location.href='../member/member_mobile_bind.html';
                                } else {
                                    $.ajax({
                                        type: 'POST',
                                        url: ApiUrl + "/index.php?act=auction&op=set_remind",
                                        data: {auction_id: auction_id, key: key},
                                        dataType: "json",
                                        success: function (result) {
                                            if (result.datas == 0) {
                                                $.sDialog({skin: "green", content: '设置提醒成功', okBtn: false, cancelBtn: false});
                                                _self.addClass('active')
                                            } else {
                                                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }else{
                        var relation_id=data.relation_info.relation_id
                        $.ajax({
                            type: 'POST',
                            url: ApiUrl + "/index.php?act=auction&op=cancel_remind",
                            data: {auction_id: auction_id, key: key, relation_id: relation_id},
                            dataType: "json",
                            success: function (result) {
                                if (result.datas == 0) {
                                    $.sDialog({skin: "green", content: '取消提醒成功', okBtn: false, cancelBtn: false});
                                    _self.removeClass('active')
                                } else {
                                    $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                                }
                            }
                        });
                    }
                    return false;
                });

                //收藏
                $(".collect").click(function () {
                    if ($(this).hasClass('active')) {
                        if (dropFavoriteAuction(auction_id)) {
                            $(this).removeClass('active');
                        }
                    } else {
                        if (favoriteAuction(auction_id)) {
                            $(this).addClass('active');
                        }
                    }
                });

                // 联系客服
                // $('.kefu').click(function () {
                //     window.location.href = WapSiteUrl + '/tmpl/member/chat_info.html?auction_id=' + auction_id + '&t_id=' + data.auction_info.store_member_id;
                // });
                $(".openBigImg").click(function () {
                    window.location.href = WapSiteUrl + '/tmpl/big_img_swiper.html?auction_id='+auction_id;
                });

                if(isWeiXin) {
                    var shareData = {
                        title: data.auction_info.auction_name+'-'+data.auction_info.special_name+'-艺诺拍卖',
                        desc: data.auction_info.auction_name+'，保真品●保增值●保退换',
                        link: window.location.href,
                        imgUrl: data.auction_info.auction_image_path,
                    };
                    wx_config(shareData);

                }

                //APP分享参数
                appShareSet(shareData);
            }

        });
    }

    initPage();


    /*
     * 出价
     * */
    function bid() {
        var auction_id = $('#auction_id').val();
        var bid_num = parseInt($(".buy_num").val());
        var key = getCookie('key');
        is_login(key);
        $.ajax({
            type: 'POST',
            url: ApiUrl + "/index.php?act=auction&op=member_offer",
            data: {auction_id: auction_id, key: key, bid_num: bid_num},
            dataType: "json",
            success: function (result) {
                if (result.datas == 0) {
                    $.sDialog({skin: "green", content: '出价成功', okBtn: false, cancelBtn: false});
                    setTimeout('location.reload()', 200);
                } else {
                    $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                }
            }
        });
    }

    /*
     * 验证是否登录
     * */
    function is_login(key) {
        if (!key) {
            window.location.href = WapSiteUrl + '/tmpl/member/login.html';
            return false;
        }
    }






});