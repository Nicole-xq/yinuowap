/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    var value = 1;
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }

    var type = 0;
    //获取佣金总额
    // $.getJSON(ApiUrl + '/index.php?act=member_fund&op=getMemberCommissionCount', , function(result){
    //     var data=result.datas.sum_commission_amount;
    //     $('#amount').html(result.datas.sum_commission_amount);
    //     $('#amount_1').html(result.datas.sum_commission_amount_1);
    //     $('#amount_2').html(result.datas.sum_commission_amount_2);
    //     // $('#sum_commission_amount_lm').html(data);
    // });
    $.ajax({
        url: ApiUrl + "/index.php?act=member_fund&op=getMemberCommissionCount",
        type: "post",
        data: {'key': key, 'form_member_id': Request_pub("form_member_id")},
        dataType: "json",
        success: function (result) {
            var data = result.datas;
            var html = template.render('member_margin_top_con', data);
            $("#member_margin_top").html(html);
        }
    });

    /**
     * type类型对应关系:1消费金额3保证金收益4推荐会员5艺术家推荐 0 全部 7竞拍奖励 8竞拍成交
     * @type {{key: *, curpage: number, reset: boolean, type: number}}
     */
    var param = {key: key, reset: true, type: 0},
        hasmore = true,
        curpage = 1,
        page = 13;

    function initPage() {
        if (param.reset) {
            curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + "/index.php?act=member_fund&op=getMemberCommissionListByType&page=" + page + "&curpage=" + curpage + "",
            type: "get",
            data: param,
            dataType: "json",
            success: function (result) {
                curpage++;
                hasmore = result.datas.page.hasmore;
                var data = result.datas;
                console.log(result.datas)

                window.data = data;
                var tmp_param = template;
                tmp_param.helper('$formatDate', function (content) {
                    var date = new Date(),
                        year = content.slice(0, 4),
                        resultyear = date.getFullYear() == year ? '' : year + '年',
                        month = content.slice(4)+'月',
                        result = resultyear + month;
                    return result;
                });
                var html = tmp_param.render('member_margin_list_con', data);

                if (param.reset) {
                    param.reset = false;
                    $("#member_margin_list").html(html);
                } else {
                    $("#member_margin_list").append(html);
                }
                var res = [], seen;
                $('#member_margin_list .member_margin_select_date').each(function (i, e) {

                    if (seen === $(e).text() || res.indexOf($(e).text()) != -1) {

                        $(e).remove();
                    } else {

                        res.push($(e).text())
                    }
                    seen = $(e).text();
                });

                $('.member_margin_list_ul').each(function () {
                    if (!$(this).find('.member_margin_select_date').length) {
                        $(this).css({'padding-top': 0})
                    }
                });
            }
        });
    }

    initPage();
    // 下拉加载
    $(window).scroll(function () {
        var
            dom = $('.member_margin_select_head .pub_mask_select_val'),
            array = dom.data('data').split(','),
            ind = array.indexOf(dom.text());
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();

            if (parseInt(ind) === 1) {
                // initPage(ind);
            } else {
                // initPage();
            }
        }
    });
    // 调用公共选择
    pub_mask_select(function (i, e) {
        var array = i.data('data').split(','),
            ind = array.indexOf(i.text()),
            index = 0;
        switch (ind) {
            case 0:
                index = 0;
                break;
            case 1:
                index = 1;
                break;
            case 2:
                index = 3;
                break;
            case 3:
                index = 4;
                break;
            case 4:
                index = 5;
                break;
            case 5:
                index = 7;
                break;
            case 6:
                index = 8;
                break;
            default:
                index = 0;
        }
        param.type = index;
        curpage = 1;
        param.reset = true;
        initPage();
        // if (parseInt(index) === 1) {
        //     initPage(index);
        // } else {
        //     initPage();
        // }
    });


    var data = new Date(),
        year = data.getFullYear(),
        month = data.getMonth() + 1;

    var dataPickerVal = [year, month, '04', '9', '34'],
        defaultDataPickerVal = [year, month, '04', '9', '34'];

    // 日期格式话
    $("#datetime-picker").datetimePicker({
        toolbarTemplate: '<header class="bar bar-nav">\
        <button class="button button-link pull-right reset-picker">重置</button>\
        <button class="button button-link pull-right close-picker">确定</button>\
        <h1 class="title">请选择</h1>\
        </header>',
        value: dataPickerVal,
        onOpen: function (e) {
            $('.reset-picker').click(function () {
                e.value = defaultDataPickerVal;
                $("#datetime-picker").picker("close");
                delete param.start_time;
                //delete param.end_time;
                curpage = 1;
                param.reset = true;
                initPage();
            });
            $('.close-picker').click(function () {

                var val = ($('#datetime-picker').val()).split("-")[0]+'-'+($('#datetime-picker').val()).split("-")[1];

                param.start_time = val;
                //param.end_time = e.value[0] + '-' + (Number(e.value[1])+1);
                curpage = 1;
                param.reset = true;
                initPage();
            });
        }
    });

});