var page = 100;
var curpage = 1;
var hasmore = true;
var footer = false;

$(function() {
    hasmore = true;
    $('.loading').remove();
    if (!hasmore) {
        return false;
    }
    hasmore = false;
    param = {};
    param.page = page;
    param.curpage = curpage;
    curpage = 1;
    $.getJSON(ApiUrl + '/index.php?act=guess&op=guess_list'+ window.location.search.replace('?','&'), param, function(result){
        if(!result) {
            result = [];
            result.datas = [];
            result.datas.guess_list = [];
        }
        var data = result.datas;
        $('.loading').remove();
        curpage++;
        var html = template.render('guess_list', data);
        $("#guess_list_html").html(html);
        hasmore = result.hasmore;
        $.ajax({
            type: 'get',
            url: ApiUrl + '/index.php?act=guess&op=guess_list'+ window.location.search.replace('?','&')+'&close=1',
            data: param,
            dataType: 'json',
            success: function(result) {
                var data = result.datas;
                var html = template.render('guess_list', data);
                $("#guess_list_html").append(html);
            }
        });
        $.getJSON(ApiUrl + '/index.php?act=guess&op=guess_list'+ window.location.search.replace('?','&'), param, function(result){

        });
        updateEndTime();

    });
    if(isWeiXin){
        var shareData = {
            title: "艺诺趣猜-好玩好物由你来竞猜.",
            desc: "超值珍品尽在这里,猜中就抱走",
            link: window.location.href,
            imgUrl: dataUrl+'/upload/logo.jpg',
        };
        wx_config(shareData);
    }
    //APP分享参数
    appShareSet(shareData);
});


