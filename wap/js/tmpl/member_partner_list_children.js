/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    var hasmore = true;
    var lv = getQueryString('lv');
    var param = {key: key, curpage: 1, page: 20, reset: true,lv:lv};
    function initPage() {
        if (param.reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + "/index.php?act=member_partner&op=partner_list",
            type: "get",
            data: param,
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                param.curpage++;
                hasmore = result.hasmore;
                //添加计数
                data.count = countProperties(data.member_list);
                var html = template.render('member_ortner_list_children_con', data);
                if (param.reset) {
                    param.reset = false;
                    $(".member_ortner_list_children").html(html);
                } else {
                    $(".member_ortner_list_children").append(html);
                }
            }
        });
    }

    initPage();
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });

    if(lv == 1){
        document.title = '一级合伙人'
    }else if(lv == 2){
        document.title = '二级合伙人'
    }else{
        document.title = '优秀合伙人'
    }
});