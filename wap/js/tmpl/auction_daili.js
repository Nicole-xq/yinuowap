/**
 * Created by ADKi on 2017/3/13 0013.
 */

$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return false;
    }
    var auction_id = getQueryString("auction_id");

    //渲染页面
    $.ajax({
        url: ApiUrl + "/index.php?act=auction&op=get_daili",
        type: "get",
        data: {auction_id: auction_id, key: key},
        dataType: "json",
        success: function (result) {
            if (result.code == 400) {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                return false;
            }
            var data = result.datas;
            var html = template.render('auction_daili', data);
            $("#auction_daili_html").html(html);
        }
    });
});
function save_daili(auction_id) {
    var key = getCookie('key');
    var agent_price = $('#daili').val();
    var auction_id = $('#auction_id').val();
    var lowest_price = $('#lowest_price').val();

    if (agent_price < lowest_price) {
        $.sDialog({skin: "red", content: '出价不符合要求', okBtn: false, cancelBtn: false});
    }
    $.ajax({
        url: ApiUrl + "/index.php?act=auction&op=save_daili",
        type: "post",
        data: {auction_id: auction_id, key: key, agent_price:agent_price},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                $.sDialog({skin: "green", content: '设置代理价成功', okBtn: false, cancelBtn: false});
                setTimeout("history.go(-1)", 2000);
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
}