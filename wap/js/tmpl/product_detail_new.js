/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function(){
    var goods_id = getQueryString("goods_id");
    var map_list = [];
    var map_index_id = '';
    var store_id;
    var key = getCookie('key');

    function get_detail() {
        $.ajax({
            url: ApiUrl + "/index.php?act=goods&op=goods_detail",
            type: "get",
            data: {goods_id: goods_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                if (!data.error) {
                    //商品图片格式化数据
                    if (data.goods_image) {
                        var goods_image = data.goods_image.split(",");
                        data.goods_image = goods_image;
                    } else {
                        data.goods_image = [];
                    }
console.log(data);
                    //渲染模板
                    var html = template.render('product_detail', data);
                    document.title=data.goods_info.goods_name;
                    $("#product_detail_html").html(html);
                    new Swiper('.product_detail_banner', {
                        loop: true,
                        paginationClickable: true,
                        pagination: '.product_detail_banner_pagination',
                        autoplay : 5000,
                        height:$(this).width()
                    });
                }
            }
        });
    }
    get_detail();
});