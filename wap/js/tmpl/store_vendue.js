$(function() {
    var key = getCookie('key');
    var store_id = getQueryString("store_id");
    if(!store_id){
        window.location.href = WapSiteUrl+'/index.html';
    }
    
    //加载店铺详情
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=store_vendue&op=store_vendue_info",
        data: {key:key,store_id:store_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            window._bd_share_config={
                "common":{
                    "bdText":data.store_vendue_info.store_name,
                    "bdPic":data.store_vendue_info.store_avatar,
                    "bdUrl":""
                },
                "share":{
                    "bdSize":"24"
                },
                "selectShare":{
                    "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
                }
            };
            with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];

            var tmp_param = template;
            tmp_param.isEscape = 0;
            var html = tmp_param.render('store_vendue', data);
            $("#store_vendue_html").html(html);
            updateEndTime();

            //显示收藏按钮
            if (data.store_vendue_info.is_favorate) {
                $("#store_notcollect").hide();
                $("#store_collected").show();
            }else{
                $("#store_notcollect").show();
                $("#store_collected").hide();
            }



        }
    });


    $(".point").live('click',function(){
        $(".nomal").addClass("display-out");
        $(".point").addClass("display-none");
    });


    //收藏店铺
    $("#store_notcollect").live('click',function() {
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect").hide();
            $("#store_collected").show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0?t+1:1;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
            triggerClick();
        }
    });
    //取消店铺收藏
    $("#store_collected").live('click',function() {
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected").hide();
            $("#store_notcollect").show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1?t-1:0;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
            triggerClick();
        }
    });

   function triggerClick(){
        $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=store_vendue&op=store_vendue_info",
                data: {key:key,store_id:store_id},
                dataType: 'json',
                success: function(result) {
                 var data = result.datas;
                 $('#qll_store_collect').html(data.store_vendue_info.store_collect+'人关注')

             }
        });
   }

});
function takeCount() {
    setTimeout("takeCount()", 1000);
    $(".count-time").each(function(){
        var obj = $(this);
        var tms = obj.attr("count_down");
        if (tms>0) {
            tms = parseInt(tms)-1;
            var days = Math.floor(tms / (1 * 60 * 60 * 24));
            var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
            var minutes = Math.floor(tms / (1 * 60)) % 60;
            var seconds = Math.floor(tms / 1) % 60;

            if (days < 0) days = 0;
            if (hours < 0) hours = 0;
            if (minutes < 0) minutes = 0;
            if (seconds < 0) seconds = 0;
            obj.find("[time_id='d']").html(days);
            obj.find("[time_id='h']").html(hours);
            obj.find("[time_id='m']").html(minutes);
            obj.find("[time_id='s']").html(seconds);
            obj.attr("count_down",tms);
        }
    });
}

//倒计时函数
function updateEndTime()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function(i){

        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("距结束&nbsp;<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
        }

    });
    setTimeout("updateEndTime()",1000);
}

