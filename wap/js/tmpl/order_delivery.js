$(function() {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    var order_id = getQueryString("order_id");
    $.ajax({
        type: 'post',
        url: appUrl1 + "/api/order/getExpressInfo",
        data:{api_token:key,order_id:order_id},
        dataType:'json',
        success:function(result) {
            //检测是否登录了
            // checkLogin(result.login);

            // var data = result && result.datas;
            if (!result.data) {
                result = {};
                result.err = '暂无物流信息';
            }

            var html = template.render('order-delivery-tmpl', result);
            $("#order-delivery").html(html);
        }
    });

});