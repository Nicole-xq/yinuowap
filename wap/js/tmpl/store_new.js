$(function () {
    var key = getCookie('key');
    var store_id = getQueryString('store_id');
    var is_kai_pai = getQueryString('is_kai_pai');
    var goods_commend = getQueryString('goods_commend');
    var keyword = decodeURIComponent(getQueryString('keyword'));
    var param = {key: key,store_id: store_id,page: 20, curpage: 1,keyword:keyword,goods_commend:1};
    var param1 = {key: key,store_id: store_id,page: 5, curpage: 1,keyword:keyword,is_kai_pai:1};
    var hasmore = true;

    //店铺信息
    function storeVendue() {
        $.ajax({
            type: 'get',
            // url:ApiUrl + "/index.php?act=store_vendue&op=store_index",
            url:ApiUrl + "/index.php?act=store&op=store_info",
            // act=store&op=store_info&store_id=124
            data: param,
            dataType: 'json',
            success: function(result) {
                var data = result.datas;
                var html = template.render('qll_store_info', data);
                $("#qll_store_info_html").html(html);

                $('.qll_title_search,.qll_artlist_top_right').click(function () {
                    // $("#goods_search").attr('href','store_search.html?store_id='+store_id);
                    window.location.href='store_search.html?store_id='+store_id;
                });

                $(".qll_introduce").attr('href','store_intro.html?store_id='+store_id);
                $('.store_collect').click(function () {
                    if (!key) {
                        checkLogin(0);
                        return;
                    }
                    var store_id = $(this).attr('store_id');
                    var $self=$(this);
                    if ($(this).hasClass('active')) {
                        $.ajax({
                            url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_del',
                            type: "post",
                            dataType: 'json',
                            data: {key: key, store_id: store_id},
                            success: function (result) {
                                if (result.code === 200) {
                                    $self.removeClass('active');
                                }
                            }
                        })
                    }
                    else{
                        $.ajax({
                            url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_add',
                            type: "post",
                            dataType: 'json',
                            data: {key: key, store_id: store_id},
                            success: function (result) {
                                if (result.code === 200) {
                                    $self.addClass('active');
                                }
                            }
                        })
                    }

                });

                // 购物车中商品数量
                if (getCookie('cart_count') > 0) {
                    $('#cart_count').html('<sup>' + getCookie('cart_count') + '</sup>');
                }

                // 联系客服
                $('.qll_kefu').click(function () {
                    window.location.href = WapSiteUrl + '/tmpl/member/chat_info.html?t_id=' + result.datas.store_info.member_id;
                });
                if(isWeiXin){
                    var artName=data.store_info.store_name;
                    // var val='艺诺'+artName+'个人艺术馆，包括'+artName+'简介、作品欣赏、艺术资讯新闻、作品价格、展览，评论互动等信息，为您查询'+artName+'资料及了解艺术作品提供最有价值的参考。';
                    var shareData = {
                        title: data.store_info.store_name+'-艺诺艺术',
                        desc: data.store_info.store_name,
                        // desc: "艺诺艺术基金 在·现场 —— ART +国际美术馆驻留计划 巡展 | 上海站参展作品线上展览",
                        link: window.location.href,
                        imgUrl: data.store_info.store_logo,

                    };
                    wx_config(shareData);
                }
                //APP分享参数
                appShareSet(shareData);


            }
        });
    };
    storeVendue();
    //商品列表
    function initPage() {
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            type: 'get',
            url:ApiUrl + "/index.php?act=goods&op=goods_list",
            data: param,
            dataType: 'json',
            success: function(result) {
                param.curpage++;
                var data = result.datas;
                // console.log(result);
                // console.log(hasmore);
                var html = template.render('qll_works_list', data);
                $("#qll_works_list_html").append(html);
                Echo && Echo.init({
                    offset: 0,
                    throttle: 0
                });
                hasmore = result.hasmore;
                //收藏
                $(".qll_works_info i.collect").click(function () {
                    var goods_id = $(this).parents('li').attr('goods_id');
                    if ($(this).hasClass('active')) {
                        if (dropFavoriteGoods(goods_id, 'goods'))
                            $(this).removeClass('active');
                    } else {
                        if (favoriteGoods(goods_id))
                            $(this).addClass('active');
                    }
                    return false;
                });



                //tab切换
                $('.qll_nav_tab_con ul li').click(function(){
                    if($(this).hasClass('active')){
                        return false;
                    };
                    var index = $(this).index();
                    $(this).addClass('active').siblings().removeClass('active');
                    $('#qll_works_list_html').html('');
                    if(index===0){
                        $('.qll_recommend_works').show();
                        param.goods_commend=1;
                        param.curpage=1;
                        param.page=20;
                        hasmore = true;
                        initPage();
                        console.log(1);
                        return false;
                    }
                    else if(index===1){
                        $('.qll_recommend_works').hide();
                        param.goods_commend='';
                        param.curpage=1;
                        param.page=5;
                        hasmore = true;
                        initPage();

                    }
                    else if(index===2){
                        $('.qll_recommend_works').hide();
                        hasmore = true;
                        param1.curpage=1;
                        auctionList();
                    }

                    return false;
                });

            }
        });
    };
    initPage();
    function auctionList() {
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url:ApiUrl + '/index.php?act=auction&op=auction_list',
            type: 'get',
            data: param1,
            dataType: 'json',
            success: function(result) {
                var data = result.datas;
                param1.curpage++;
                var html = template.render('qll_auction_list', data);
                $("#qll_works_list_html").append(html);
                Echo && Echo.init({
                    offset: 0,
                    throttle: 0
                });
                hasmore = result.hasmore;

                //收藏
                $(".qll_works_info i.collect").click(function () {
                    var goods_id = $(this).parents('li').attr('goods_id');
                    if ($(this).hasClass('active')) {
                        if (dropFavoriteGoods(goods_id, 'goods'))
                            $(this).removeClass('active');
                    } else {
                        if (favoriteGoods(goods_id))
                            $(this).addClass('active');
                    }
                    return false;
                });
            }
        });
    };




    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            if($('.works_auction').hasClass('active')){
                auctionList();
            }else if($('.works_total').hasClass('active')){
                initPage();
            }else {
                return false;
            }
        }
    });
    var wh=$(window).height();
    $(window).scroll(function(){
        // var s=wh-$(window).scrollTop();
        var st = $(window).scrollTop();
        if(st>47){
            $('.qll_nav_tab_con').addClass('fixed');
        }else{
            $('.qll_nav_tab_con').removeClass('fixed');
        }
    });

});


