$(function () {
    var html = ['','/wap/tmpl/member/member_margin.html'];
    var test = window.location.pathname;
    var test_result =  html.indexOf(test);
    $(document).on('click', ".get_agreement_btn", function () {
        var agreement_type = $(this).attr('g_type');
        agreement_type && _get_agreement(agreement_type);
    });

    $('.agreement_bottom').click(function () {
        _clone();
    });

    function _clone() {
        test_result > 0 && $('.member_margin_select').addClass('pub_mask_select');
        $('body,html').css('height', 'auto');
        $('body,html').removeClass('pub_overFlow');
        $('.agreement_bottom').hide();
        $('.agreement_content').empty();
    }

    function _get_agreement(agreement_type) {
        $.ajax({
            type: 'get',
            url: ApiUrl+'/index.php?act=mb_agreement&type=' + agreement_type,
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                if(!data.content){
                    $("i[g_type='"+ agreement_type +"']").attr('g_type','');
                    return false;
                }
                changeCss();
                var html = template.render('withdrawalsAagreement', data);
                $("#withdrawalsAagreementHtml").html(html);
                $('.agreement_detail').html(data.content);
                $('.qll_close').click(function () {
                    _clone();
                });
            }
        });
    }

    var get_agreementCookie = getCookie('get_agreementCookie');
    var reg=/^.*(\/aucution_margin\.html).*$/;
    //get_agreementCookie为false则显示，否则隐藏，24小时cookie，不论是否登录
    if (!get_agreementCookie && reg.test(window.location.pathname)) {
        _get_agreement('underline_pay_prize');
        addCookie('get_agreementCookie', true, 24);
    }

    function changeCss() {
        test_result > 0 && $('.member_margin_select').removeClass('pub_mask_select');
        $('body,html').addClass('pub_overFlow');
        $('body,html').height($(window).height());
        $(".agreement_bottom").height(document.body.scrollHeight);
        $('.agreement_bottom').show();
    }
});