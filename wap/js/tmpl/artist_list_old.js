var page = pagesize;
var curpage = 1;
var hasmore = true;
var footer = false;
var reset = true;
var keyword = decodeURIComponent(getQueryString('keyword'));
var key = getCookie('key');
$(function () {

    function initPage() {
        param = {};
        if (reset) {
            curpage = 1;
            hasMore = true;
        }
        $('.loading').remove();
        if (!hasmore) {
            return false;
        }
        hasmore = false;

        param.page = page;
        param.curpage = curpage;
        if (keyword != '') {
            param.keyword = keyword;
        }
        if (key != '') {
            param.key = key;
        }
        $.getJSON(ApiUrl + '/index.php?act=artist&op=artist_list' + window.location.search.replace('?', '&'), param, function (result) {
            if (!result) {
                result = [];
                result.datas = [];
                result.datas.artist_list = [];
            }
            var data = result.datas;
            //$('.loading').remove();
            curpage++;
            hasmore = result.hasmore;
            var html = template.render('artist_list', data);
            console.log(111111111);
            if (reset) {
                reset = false;
                $("#artist_list_wrapper").html(html);
            } else {
                $("#artist_list_wrapper").append(html);
            }

            if (data.artist_list.length > 0) {
                for (var i = 0; i < data.artist_list.length; i++) {
                    //显示收藏按钮
                    if (data.artist_list[i].is_favorate) {
                        $("#store_notcollect_" + data.artist_list[i].store_id).hide();
                        $("#store_collected_" + data.artist_list[i].store_id).show();
                    } else {
                        $("#store_notcollect_" + data.artist_list[i].store_id).show();
                        $("#store_collected_" + data.artist_list[i].store_id).hide();
                    }
                }
            }
        });
    }


    //收藏店铺
    $(".notcollect").live('click', function () {
        var store_id = $(this).attr('nc_store_id');
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect_" + store_id).hide();
            $("#store_collected_" + store_id).show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0 ? t + 1 : 1;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
            // location.reload();
        }
    });
    //取消店铺收藏
    $(".collected").live('click', function () {
        var store_id = $(this).attr('nc_store_id');
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected_" + store_id).hide();
            $("#store_notcollect_" + store_id).show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1 ? t - 1 : 0;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });

    //初始化页面
    initPage();
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });

});

function get_footer() {
    if (!footer) {
        footer = true;
        $.ajax({
            url: WapSiteUrl + '/js/tmpl/new_footer.js',
            dataType: "script"
        });
    }
}
