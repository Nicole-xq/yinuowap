$(function () {
    function isNull(arg1) {
        return !arg1 && arg1 !== 0 && typeof arg1 !== "boolean" ? true : false;
    }

    if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        var usertype = getQueryString('usertype');
        addCookie('key', key);
        addCookie('username', username);
        addCookie('usertype', usertype);
        updateCookieCart(key);
        setCookieYinouvip('key',key);
        setCookieYinouvip('username',username);
        setCookieYinouvip('usertype',usertype);
        // alert(getQueryString('redirect_url'))
        if (getQueryString('redirect_url')) {
            // alert(decodeURIComponent(getQueryString('redirect_url')))
            location.href = decodeURIComponent(getQueryString('redirect_url'));
        } else {
            // var key_str = '&key='+getQueryString('key');
            // var reload_url = location.href.replace(key_str, '');
            location.href = '/wap/tmpl/member/member.html';
        }
    } else {
        var key = getCookie('key');
    }

    if (key) {
        $('.exhibition').show();
    }

    if (key) {
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=member_index",
            data: {key: key},
            dataType: 'json',
            //jsonp:'callback',
            success: function (result) {
                checkLogin(result.login);
                var html = '<a href="member_account.html" class="qll_member_avatar"><div class="qll_member_img" style="margin: 0;\n' +
                    '    padding: 2rem 0 0 1.5rem;"> <img class="lm_tx_top" src="' + result.datas.member_info.avatar + '"/><img class="member_grade" style="display: none" src="' + result.datas.member_info.type_logo + '" alt=""></div>'
                    + '<h1 style="  margin: -20px 0 -5px 124px;font-size: 0.82rem;color:#333;font-weight: bold;width: 10rem;text-align: left ">' + result.datas.member_info.user_name + '<img src="../../images/whk/whkSetUp.png" alt=""></h1></a>'
                    + '<div style="display: flex;display: -webkit-flex;justify-content: flex-start ;align-items:center;height: 1rem;margin: 0.5rem 0 0 5.6rem;"> <span style=" font-size: 0.55rem;color:#333;min-width: 1.5rem;max-width: 5rem;text-align: left;\n' +
                    '    margin-right: 0.5rem"> ' +result.datas.member_info.member_type_name+ '</span> <a style="font-size: 0.55rem;color:#333;text-align: left;" href="points.html">诺币</a> </div>'

                    + '<div class="clear"></div>'
                    + '<div class="row no-gutter lm_member_spans" style="background-color: transparent">'
                    // + '<div class="col-50">ID:&nbsp;'+result.datas.member_info.user_id+'</div>'
                    // + '<div class="col-50">会员等级:&nbsp;'+result.datas.member_info.level_name+'</div>'
                    // + '<div class="lm_member_span" >等级:&nbsp;' + result.datas.member_info.member_type_name + '</div>'
                    // + '<a href="favorites.html" class="lm_member_span" ><img src="../../images/qll/collect.png" alt=""><span>收藏</span><i>' + result.datas.member_info.favorites_goods + '</i></a>'
                    // + '<a href="favorites_store.html" class="lm_member_span" ><img src="../../images/qll/guanzhu.png" alt=""><span>关注</span><i>' + result.datas.member_info.favorites_store + '</i></a>'
                    // + '<div class="lm_member_span redpacket_member_lm" style="border-left: none;">关注服务号</div>'
                    + '</div>';
                $('#count_partner').html(result.datas.member_info.count_partner);
                $('#available_commis').html(result.datas.member_info.available_commis);
                $(".account-info").html(html);

                // if (!isNull(result.datas.top_member.top_member_name)) {
                //     $('span[nctype="top_member_name"]').text('您是 ' + result.datas.top_member.top_member_name + ' 推荐');
                // } else {
                //     $('span[nctype="top_member_name"]').remove();
                //     // $('span[nctype="top_member_name"]').text('');
                // }

                if (!isNull(result.datas.service_hotline)) {
                    $('#service_hotline').attr('href', 'tel:' + result.datas.service_hotline.value);
                } else {
                    $('#service_hotline').attr('href', 'tel:' + 0);
                }

                $(".redpacket_reward").html(result.datas.member_info.rpt_num);


                //点击关注服务号
                $('.redpacket_member_lm').on('click', function () {
                    var $wrapper = $('.lm_modal_member');
                    var $dom = $('.lm_con_member');
                    var $close = $('.lm_close_member');
                    $wrapper.show();
                    $dom.addClass('bounceInDown');
                    $close.on('click', function () {
                        $dom.removeClass('bounceInDown').addClass('bounceOutUp');
                        setTimeout(function () {
                            $wrapper.hide();
                        }, 1000)
                    });
                    return false;
                });
                //app登陆成功，添加别名
                loginSuccessClick({'member_id': result.datas.member_info.user_id});
                return false;
            }
        });
    } else {
        var html = '<div class="member-info">'
            + '<a href="login.html" class="default-avatar" style="display:block;"></a>'
            + '<a href="login.html" class="to-login">登录/注册</a>'
            + '<img style="width: 0.35rem;height: 0.64rem;margin-top: 0.2rem" src="../../images/whk/moreT.png" alt="">'

            + '</div>';
        //渲染页面
        $(".account-info").html(html);
        return false;
    }

    //滚动header固定到顶部
    $.scrollTransparent();


    //判断是否首次注册用户
    // if(LM_Request('get_register_award')){
    var $lm_dialog = $('.pub_lm_dialog');


    if (key) {
        $.ajax({
            type: 'get',
            url: ApiUrl + "/index.php?act=member_index&op=get_notice",
            data: {key: key},
            dataType: 'json',
            //jsonp:'callback',
            success: function (result) {
                if (result.datas.length > 0) {
                    $lm_dialog.removeClass('lm_hidden');
                    $('body,html').addClass('lm_over');
                    checkLogin(result.login);
                    var data = result.datas;
                    var html = '';
                    $.each(data, function (i, e) {
                        var url = e.rpacket_t_wap_link || '../product_list.html';
                        html += '<li>\
                               <a href=""+url+"">\
                                 <div class="pub_lm_dialog_left">\
                                   <h3 class="pub_lm_dialog_title">' + e.rpacket_title + '</h3>\
                                   <span class="pub_lm_dialog_date">有效期至:' + e.rpacket_end_date_text + '</span>\
                                 </div>\
                                 <div class="pub_lm_dialog_right">\
                                   <span class="pub_lm_dialog_price">¥<b>' + e.rpacket_price + '</b></span>\
                                   <span class="pub_lm_dialog_info">满¥' + parseInt(e.rpacket_limit) + '使用</span>\
                                 </div>\
                               </a>\
                             </li>';
                    });

                    $('.pub_lm_dialog_con_wrapper').html(html);
                    $('.pub_lm_dialog_close').click(function () {
                        // location.href='./member.html';
                        $lm_dialog.addClass('lm_hidden');
                        $('body,html').removeClass('lm_over');
                        return true;
                    });
                    $('.pub_lm_dialog_coolect').click(function () {
                        if ($('.pub_lm_dialog_ewm').hasClass('lm_hidden')) {
                            $('.pub_lm_dialog_ewm').removeClass('lm_hidden');
                        } else {
                            $('.pub_lm_dialog_ewm').addClass('lm_hidden');
                        }
                    });
                    return false;
                }
            }
        });
    }
    // }
    if (key) {
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=member_qrcode",
            data: {key: key},
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                var invite_code = data.invite_code;
                var usertype = data.usertype;
                if (usertype == 4) {
                    $('.lm_memberList_top').html("<h1>您没有邀请会员的权限</h1>");
                } else {
                    $('#member_a').attr('href', WapSiteUrl + '/tmpl/member/member_qrcode.html?invite_code=' + invite_code);
                }
            }
        });
    }

    if (key !== null) {
        $.getJSON(ApiUrl + '/index.php?act=member_chat&op=get_msg_count', {type: 3, key: key}, function (result) {
            if (result.datas > 0) {
                $('.other a i').show();
            }
        });
    }

});
if (isWeiXin) {
    var shareData = {
        title: "艺诺网-打造全球最大艺术品保障交易平台",
        desc: "保真品，保增值，保退换。",
        link: window.location.origin + window.location.pathname,
        imgUrl: dataUrl + '/upload/logo.jpg',
    };
    wx_config(shareData);
}
//APP分享参数
appShareSet(shareData);

function loginSuccessClick(member_info) {
    if (navigator.userAgent.indexOf('Appyinuo') !== -1) {
        //登陆成功，添加别名
        var str = jsObj.loginSuccess(JSON.stringify(member_info));
    }
}

