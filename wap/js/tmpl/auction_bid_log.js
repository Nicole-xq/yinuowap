/**
 * Created by ADKi on 2017/3/9 0009.
 */
$(function() {
    var key = getCookie('key');
    // 获取get参数
    var auction_id = getQueryString("auction_id");
    if (!auction_id) {
        window.location.href = WapSiteUrl + '/index.html';
    }

    //初始化页面数据
    $.ajax({
        type: 'get',
        url: ApiUrl + "/index.php?act=auction&op=get_bid_list&getAll=1",
        data: {key:key,auction_id:auction_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            var html = template.render('bid_list', data);
            $("#bid_list_html").html(html);
        }
    });
});