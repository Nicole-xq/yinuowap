/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function() {
    var key = getCookie('key');
    var artist_vendue_id = getQueryString("artist_vendue_id");
    var type = getQueryString("type");
    if(!artist_vendue_id){
        window.location.href = WapSiteUrl+'/index.html';
    }
    $.showPreloader('加载中....');
    //加载详情
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=artist&op=artist_detail",
        data: {key:key,artist_vendue_id:artist_vendue_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            var tmp_param = template;
            tmp_param.isEscape = 0;
            var html = tmp_param.render('detail', data);
            $("#lm_art_home").html(html);
            $.hidePreloader();
            console.log(data);

            //显示收藏按钮
            if (data.artist_details.is_favorate) {
                $("#store_notcollect").addClass('lm_hidden');
                $("#store_collected").removeClass('lm_hidden');
            }else{
                $("#store_notcollect").removeClass('lm_hidden');
                $("#store_collected").addClass('lm_hidden');
            }
            if(type && type == 'work'){
                window.location.href="#artist_works";
            }
        }

    });
    $(".point").live('click',function(){
        $(".nomal").addClass("display-out");
        $(".point").addClass("display-none");
    });

    //收藏店铺
    $("#store_notcollect").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect").addClass('lm_hidden');
            $("#store_collected").removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0?t+1:1;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });
    //取消店铺收藏
    $("#store_collected").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected").addClass('lm_hidden');
            $("#store_notcollect").removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1?t-1:0;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });



});
