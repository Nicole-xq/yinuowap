$(function () {
    var key = getCookie('key');
    var rpacket_state = getQueryString('rpacket_state');
    var param = {key: key, page: 10, curpage: 1, rpacket_state: 1};
    var hasmore = true;
    var reset = true;
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }

    //渲染list
    // var load_class = new ncScrollLoad();
    // load_class.loadInit({'url':ApiUrl + '/index.php?act=member_redpacket&op=redpacket_list','getparam':{'key':key},'tmplid':'redpacket_list','containerobj':$("#redpacket_list_html"),'iIntervalId':true});
    // load_class.loadInit({'url':ApiUrl + '/index.php?act=member_redpacket&op=redpacket_count','getparam':{'key':key},'tmplid':'redpacket_detail','containerobj':$("#redpacket_detail_html"),'iIntervalId':true});
    function redpacketList() {
        if (param.reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;

        $.ajax({
            url: ApiUrl + "/index.php?act=member_redpacket&op=redpacket_list",
            type: 'get',
            data: param,
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                param.curpage++;
                var redpacket_detail_html = template.render('redpacket_detail', data);
                var redpacket_list_html = template.render('redpacket_list', data);
                if (reset) {
                    $("#redpacket_detail_html").html(redpacket_detail_html);
                    $("#redpacket_list_html").html(redpacket_list_html);
                    reset = false;
                } else {
                    $("#redpacket_detail_html").html(redpacket_detail_html);
                    $("#redpacket_list_html").append(redpacket_list_html);
                }

                hasmore = result.hasmore;

            }
        });
    };
    redpacketList();

    function bodyScroll(event) {
        event.preventDefault();
    }


    var jinzhi = 0;


    $('.red_screen span,.red_screen img').click(function () {
        $('.red_screen_list').show();
        $('.red_screen_content').show();
        jinzhi = 0;
        document.addEventListener("touchmove", function (e) {
            if (jinzhi == 0) {
                e.preventDefault();
                e.stopPropagation();
            }
        }, false);
        // $(document.body).toggleClass("html-body-overflow");
    });
    $('.red_screen_list li').click(function () {
        //$("body").off("touchmove");

        $(this).css({'background': '#000', 'color': '#fff'}).siblings().css({'background': '#fff', 'color': '#000'});


        setTimeout(function () {
            $('.red_screen_list').hide();
            $('.red_screen_content').hide();
        }, 200);
        $("#redpacket_detail_html").html('');
        $("#redpacket_list_html").html('');
        hasmore = true;
        param.curpage = 1;
        var red_index = $(this).index();
        $('.red_screen span').html($(this).text());
        if (red_index === 0 || red_index === 1) {
            param.rpacket_state = '';
            redpacketList();
        } else {
            param.rpacket_state = $(this).index() - 1;
            redpacketList();
        }
        jinzhi = 1;
        //$("body").off("touchmove");
    });
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            redpacketList();
        }
    });
    if (isWeiXin()) {
        $('header').hide();
        $('body').css('padding', '0 0 56px;')
    }
});