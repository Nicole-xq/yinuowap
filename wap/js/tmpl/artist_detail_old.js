$(function() {
    var key = getCookie('key');
    var artist_vendue_id = getQueryString("artist_vendue_id");
    var type = getQueryString("type");
    if(!artist_vendue_id){
        window.location.href = WapSiteUrl+'/index.html';
    }
    
    //加载详情
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=artist&op=artist_detail",
        data: {key:key,artist_vendue_id:artist_vendue_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;

            window._bd_share_config={
                "common":{
                    "bdText":data.artist_details.artist_name,
                    "bdPic":data.artist_details.artist_image_path,
                    "bdUrl":""
                },
                "share":{
                    "bdSize":"24"
                },
                "selectShare":{
                    "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
                }
            };
            with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];

            var tmp_param = template;
            tmp_param.isEscape = 0;
            var html = tmp_param.render('detail', data);
            $("#artist_detail_html").html(html);

            //显示收藏按钮
            if (data.artist_details.is_favorate) {
                $("#store_notcollect").hide();
                $("#store_collected").show();
            }else{
                $("#store_notcollect").show();
                $("#store_collected").hide();
            }
            if(type && type == 'work'){
                window.location.href="#artist_works";
            }
        }

    });
    $(".point").live('click',function(){
        $(".nomal").addClass("display-out");
        $(".point").addClass("display-none");
    });

    //收藏店铺
    $("#store_notcollect").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect").hide();
            $("#store_collected").show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0?t+1:1;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });
    //取消店铺收藏
    $("#store_collected").live('click',function() {
        var store_id = $(this).attr('nc_store_id');
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected").hide();
            $("#store_notcollect").show();
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1?t-1:0;
            $('#store_favornum').html(favornum);
            $('#store_favornum_hide').val(favornum);
        }
    });



});
