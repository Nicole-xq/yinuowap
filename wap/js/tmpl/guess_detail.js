var gs_id = getQueryString("gs_id");
var key = getCookie('key');
var chujia = 1;
$(function () {

    if (!gs_id) {
        $.sDialog({
            content: '趣猜不存在！<br>请返回上一页继续操作。',
            okBtn: false,
            cancelBtnText: '返回',
            cancelFn: function () {
                history.back();
            }
        });
    }

    get_detail(gs_id);
    function get_detail(gs_id) {
        //渲染页面
        $.ajax({
            url: ApiUrl + "/index.php?act=guess&op=guess_detail",
            type: "get",
            data: {gs_id: gs_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                if (!data.error) {
                    //商品规格格式化数据
                    var guess = data.guess;
                    //渲染模板
                    var tmp_param = template;
                    tmp_param.isEscape = 0;
                    var html = tmp_param.render('guess_detail', guess);
                    document.title = data.guess.guess_info.gs_name;
                    $('#guess_state').attr('goods_id',data.guess.guess_info.goods_id);
                    $("#guess_detail_html").html(html);
                    updateEndTime();
                    // 渲染模板
                    var html = template.render('banner', data);
                    $("#banner_html").html(html);
                    if(data.banner.list.length>1){
                        var swiper = new Swiper('.swiper-container-banner', {
                            pagination: '.swiper-pagination',
                            paginationClickable: true,
                            loop : true,
                            autoplay : 3000,
                        });
                    }

                    if(data.is_guess == 1){
                        $('#guess_state').text('已出价');
                        $('#submit :first-child').removeClass('red-bg').addClass('gray-bg');
                    }

                    if(!guess.guess_info.guess_end_flag){
                        if(!guess.guess_info.winer||!key||data.is_guess == 0){

                            $('#guess_state').text('竞猜结束');
                            $('#submit :first-child').removeClass('red-bg').addClass('gray-bg');
                        }else if(guess.guess_info.winer.member_id == data.u_id){
                            $('#submit :first-child').removeClass('gray-bg').addClass('red-bg');
                            $('#guess_state').attr('lm_state',1).removeClass('red-bg').text('恭喜您已中奖，前去领奖');
                            chujia = 0;
                            // $('#guess_state').click(function(){
                            //     window.location.href = WapSiteUrl + '/tmpl/qucai/qucai_list.html';
                                // location.href=WapSiteUrl + '/tmpl/order/buy_step1.html?goods_id='+guess.guess_info.goods_id+'&buynum=1&gs_id='+guess.guess_info.gs_id;
                            // })
                        }else{
                            $('#guess_state').text('很遗憾，您未中奖');
                            $('#submit :first-child').removeClass('red-bg').addClass('gray-bg');
                        }

                        $('.state-button :first-child').removeClass('start').addClass('been');
                        // 竞猜按钮置灰

                        $('#new-address-valve').click(function () {
                            $.sDialog({
                                skin: "red",
                                content: "本场趣猜已经结束",
                                okBtn: false,
                                cancelBtn: false
                            });
                        });
                    }
                    // if(data.guess.guess_info.guess_offer_price){
                    //     $('#submit :first-child').removeClass('red-bg').addClass('gray-bg');
                    // }
                }
                $('.kefu').click(function () {
                    window.location.href = WapSiteUrl + '/tmpl/member/chat_info.html?gs_id=' + gs_id + '&t_id=' + result.datas.guess.guess_info.store_member_id;
                })
                if(isWeiXin){
                    var shareData = {
                        title:data.guess.guess_info.gs_name+ '-艺诺趣猜',
                        desc: data.guess.goods_jingle,
                        link: window.location.href,
                        imgUrl: data.banner.list[0],
                    };
                    wx_config(shareData);
                }
                //APP分享参数
                appShareSet(shareData);
                $('#lm_test').html($('#lm_test').html());
            }

        });
        $.ajax({
            url: ApiUrl + "/index.php?act=member_guess&op=if_fav_guess",
            type: "post",
            data: {gs_id: gs_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                if (!result.error && result.code != 400) {
                    if (data.fav_info.length != 0) {
                        $('.pd-collect').addClass('red-bg');
                        $('.pd-collect img').attr('src', '../../images/footer-04.png');
                    }
                    if (data.rel_info.length != 0 && data.rel_info.is_remind == 1) {
                        $('#new-address-valve').attr('data-remind',data.rel_info.is_remind);
                        $('#new-address-valve').addClass('red-bg');
                        $('#new-address-valve').find('span').text('取消');
                        $('#new-address-valve img').attr('src','../../images/footer-02a.png');
                        $('.rem-form').hide();
                        $('#del_remaind').show();
                    }
                }
            }
        })
    }

});
//收藏
$(".pd-collect").click(function () {
    if ($(this).hasClass('red-bg')) {
        if (dropFavoriteGuess(gs_id)) {
            $(this).removeClass('red-bg');
            $('.pd-collect img').attr('src', '../../images/footer-03.png');
        }
    } else {
        if (favoriteGuess(gs_id)) {
            $(this).addClass('red-bg');
            $('.pd-collect img').attr('src', '../../images/footer-04.png');
        }
    }
});
//竞猜出价
$("#submit").click(function () {
    if($('#guess_state').attr('lm_state')==1){
        var key = getCookie('key');
        var goods_id=$('#guess_state').attr('goods_id');
        var gs_id = getQueryString('gs_id');
        $.ajax({
            type:'post',
            url:ApiUrl + '/index.php?act=member_buy&op=buy_step1',
            data:{key:key, gs_id:gs_id,goods_id:goods_id,cart_id:goods_id+'|1'},
            dataType:'json',
            success:function(result){
                if(result.datas.error){
                    $.sDialog({
                        skin: "red",
                        content: result.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                } else {

                    var u = WapSiteUrl + '/tmpl/order/buy_step1.html?goods_id=' + goods_id + '&buynum=' + 1+'&gs_id='+gs_id;
                    location.href = u;
                }
            }
        });
        return false;
    }

    if ($('#submit :first-child').hasClass('red-bg')&chujia == 1) {
        var key = getCookie('key');
        var gs_id = getQueryString("gs_id");
        if (!key) {
            checkLogin(0);
            return;
        }
        window.location.href = WapSiteUrl + '/tmpl/qucai/xiangqing-qucaichujia.html?gs_id=' + gs_id;
    }
});

//倒计时函数
//倒计时函数
function updateEndTime() {
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function (i) {

        var endDate = this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        //var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate * 1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if (lag > 0) {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("<em time_id='d'>" + day + "</em>日<em time_id='h'>" + hour + "</em>时<em time_id='m'>" + minite + "</em>分<em time_id='s'>" + second + "</em>秒");
        }
        else {
            $(this).parent().html("已结束");
        }
    });
    setTimeout("updateEndTime()", 1000);
}

updateEndTime1();


//倒计时函数
function updateEndTime1()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime1").each(function(i){
        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {

            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
            if($(this).attr('data-time')=='start') {
                $('#guess_state').text('竞猜未开始');
                $('#new-address-valve').click(function () {
                            $.sDialog({
                                skin: "red",
                                content: "竞猜未开始",
                                okBtn: false,
                                cancelBtn: false
                            });
                        });
            }
            else if($(this).attr('data-time')=='end'){
                $('#new-address-valve').on('click',remind);
            }
        }
        else{

            if($(this).attr('data-time')=='start'){
                location.reload();
                $('.state-button :first-child').removeClass('been').addClass('start');
                $('#submit :first-child').removeClass('gray-bg').addClass('red-bg');
            }else if($(this).attr('data-time')=='end'){
                $(this).parent().html("趣猜已经结束啦！");
                $('#guess_state').text('竞猜结束');
                $('.state-button :first-child').removeClass('start').addClass('been');
                // 竞猜按钮置灰
                $('#submit :first-child').removeClass('red-bg').addClass('gray-bg');
                $('#new-address-valve').click(function () {
                    $.sDialog({
                        skin: "red",
                        content: "本场趣猜已经结束",
                        okBtn: false,
                        cancelBtn: false
                    });
                });
            }
        }
        // $(this).html("已经结束啦！");
    });
    setTimeout("updateEndTime1()",1000);
}

function remind(){
    $('#new-address-valve').off('click',remind);
    if (!key) {
        checkLogin(0);
        return;
    }
    var remind_status = $('#new-address-valve').attr('data-remind');
    if(remind_status == 1){
        $.ajax({
            type: "get",
            url: ApiUrl + "/index.php?act=member_guess&op=cancel_guess_remind",
            data: {gs_id: gs_id, key: key},
            dataType: "json",
            success: function (data) {
                checkLogin(data.login);
                if (data.code == 'ok') {
                    $.sDialog({
                        skin: "green",
                        content: data.message,
                        okBtn: false,
                        cancelBtn: false
                    });
                    $('#new-address-valve').removeClass('red-bg');
                    $('#new-address-valve').find('span').text('提醒');
                    $('#new-address-valve img').attr('src','../../images/footer-02a.png');
                    $('.rem-form').show();
                    $('#del_remaind').hide();
                    $('#go_back').click();
                    setTimeout(function () {
                        location.reload(true);
                    },2000);
                } else {
                    $.sDialog({
                        skin: "red",
                        content: data.message,
                        okBtn: false,
                        cancelBtn: false
                    });
                }
            }
        });
    }else{
        $.ajax({
            type: "POST",
            url: ApiUrl + "/index.php?act=member_guess&op=guess_remind",
            data: {gs_id: gs_id, key: key},
            dataType: "json",
            success: function (data) {
                checkLogin(data.login);
                if (data.code == 'ok') {
                    $.sDialog({skin: "green", content: data.message, okBtn: false, cancelBtn: false});
                    $('#new-address-valve').addClass('red-bg');
                    $('#new-address-valve').find('span').text('取消');
                    $('#new-address-valve img').attr('src','../../images/footer-02a.png');
                    $('.rem-form').hide();
                    $('#del_remaind').show();
                    $('#go_back').click();
                    setTimeout(function () {
                        location.reload(true);
                    },2000);
                }
                if(data.code == 'error1'){
                    $.sDialog({skin: "red", content: data.message, okBtn: false, cancelBtn: false});
                    setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                }
                if (data.code == 'error') {
                    $.sDialog({
                        skin: "red",
                        content: data.message,
                        okBtn: false,
                        cancelBtn: false,
                    });
                }
            },
        });
    }
}




