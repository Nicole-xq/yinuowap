var page = pagesize;
var curpage = 1;
var hasMore = true;
var footer = false;
var reset = true;
var key = getCookie('key');
$(function(){
    if(!key){
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
    }

    if (getQueryString('data-state') != '') {
        $('#filtrate_ul').find('li').has('a[data-state="' + getQueryString('data-state')  + '"]').addClass('selected').siblings().removeClass("selected");
    }

    $('#fixed_nav').waypoint(function() {
        $('#fixed_nav').toggleClass('fixed');
    }, {
        offset: '50'
    });

    function initPage(){
        if (reset) {
            curpage = 1;
            hasMore = true;
            $('#footer').html('');
        }
        $('.loading').remove();
        if (!hasMore) {
            return false;
        }
        hasMore = false;
        var state_type = $('#filtrate_ul').find('.selected').find('a').attr('data-state');
        $.ajax({
            type:'post',
            url:ApiUrl + '/index.php?act=member_guess&op=mb_gs_list&page='+page+'&curpage='+curpage,
            data:{key:key, state_type:state_type},
            dataType:'json',
            success:function(result){
                checkLogin(result.login);//检测是否登录了
                curpage++;
                hasMore = result.hasmore;
                var data = result.datas;
                if (!hasMore) {
                    get_footer();
                }
                if (result.datas.guess_list.length <= 0) {
                    $('#footer').addClass('posa');
                }
                var html = template.render('qucai-list', data);
                if (reset) {
                    reset = false;
                    $("#qucai-list-html").html(html);
                } else {
                    $("#qucai-list-html").append(html);
                }
            }
        });
        recommend_guess();
        
    }
   
    //初始化页面
    initPage(page,curpage);
    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            initPage();
        }
    });

    $('#filtrate_ul').find('a').click(function(){
        $('#filtrate_ul').find('li').removeClass('selected');
        $(this).parent().addClass('selected').siblings().removeClass("selected");
        reset = true;
        window.scrollTo(0,0);
        initPage();
    });
    if (isWeiXin()){
        $('header').hide();
        $("body").css('padding', '0');
        $("#fixed_nav").css('top', '0');
    }
});

function add_gs_cart(gs_id,goods_id){
     var key = getCookie('key');
     $.ajax({
            type:'post',
            url:ApiUrl + '/index.php?act=member_buy&op=buy_step1',
            data:{key:key, gs_id:gs_id,goods_id:goods_id,cart_id:goods_id+'|1'},
            dataType:'json',
            success:function(result){
               if(result.datas.error){
                   $.sDialog({
                       skin: "red",
                       content: result.datas.error,
                       okBtn: false,
                       cancelBtn: false
                   });
               } else {
                   var u = WapSiteUrl + '/tmpl/order/buy_step1.html?goods_id=' + goods_id + '&buynum=' + 1 + '&gs_id=' + gs_id;
                   location.href = u;
               }
            }
        });

}

function get_footer() {
    if (!footer) {
        footer = true;
        $.ajax({
            url: WapSiteUrl+'/js/tmpl/footer.js',
            dataType: "script"
          });
    }
}

function recommend_guess(){
    $.ajax({
            type:'get',
            url:ApiUrl + '/index.php?act=member_guess&op=more_guess',
            data:{key:key},
            dataType:'json',
            success:function(result){
                if (result.code == 200) {
                    var html = template.render('recommend_data',result.datas);
                    $('#recommend_html').html(html);
                }
            }
    });
}