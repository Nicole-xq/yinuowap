$(function(){


    var key = getCookie('key');
    if(!key){
        location.href = '/wap/tmpl/member/login.html';
    }else {
        //渲染list
        var load_class = new ncScrollLoad();
        load_class.loadInit({
            'url':ApiUrl + '/index.php?act=member_refund&op=get_refund_list',
            'getparam':{key :key },
            'tmplid':'refund-list-tmpl',
            'containerobj':$("#refund-list"),
            'iIntervalId':true,
            'data':{WapSiteUrl:WapSiteUrl}
        });
    }

});