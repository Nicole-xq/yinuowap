$(function(){
    //获取微信openid
    var key = getCookie('key');
    if (key) {
        window.location.href = WapSiteUrl+'/tmpl/member/member.html';
        return;
    }
    var unionid = getQueryString('unionid');
    $("#unionid").val(unionid);
    $("#goToLogin").attr('href',WapSiteUrl + '/tmpl/member/login.html?unionid='+unionid);

    //验证

    $.sValid.init({//注册验证
        rules:{
            username: {
                required:true,
                name_length:[4,20],
            },
            mobile: {
            	required:true,
                mobile:true
            },
            pwd:"required",
        },
        messages:{
            username: {
                required:"用户名必须填写！",
                name_length:"用户名格式错误！",
            },

            mobile: {
                required:"手机号必填",
                mobile:"手机格式不正确"
            },
            pwd:"密码必填!"
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });




    // 获取手机号验证码


    // 发送手机验证码
    $('#refister_mobile_btn').click(function(){
        var mobile = $('#mobile').val();
        if(mobile != ''){
            send_sms(mobile);
        }else{
            $.sDialog({
                skin:"red",
                content:'请先填写手机号',
                okBtn:false,
                cancelBtn:false
            });
        }

    });


    function send_sms(mobile) {
        $.getJSON(ApiUrl+'/index.php?act=login&op=get_sms_captcha', {type:1,phone:mobile}, function(result){
            if(!result.datas.error){
                $.sDialog({
                    skin:"green",
                    content:'发送成功',
                    okBtn:false,
                    cancelBtn:false
                });
                $('.send_captcha').hide();
                $('.code-countdown').show().find('em').html(result.datas.sms_time);
                var times_Countdown = setInterval(function(){
                    var em = $('.code-countdown').find('em');
                    var t = parseInt(em.html() - 1);
                    if (t == 0) {
                        $('.send_captcha').show();
                        $('.code-countdown').hide();
                        clearInterval(times_Countdown);
                    } else {
                        em.html(t);
                    }
                },1000);
            }else{
                errorTipsShow('<p>' + result.datas.error + '<p>');
            }
        });
    }

   // 提交数据
    $('#bindPhonebtn').click(function(){
        var username = $("input[name=username]").val();
        var mobile = $("input[name=mobile]").val();
        var captcha = $('#captcha').val();
        var pwd = $("input[name=pwd]").val();
        var unionid = $("input[name=unionid]").val();
        if($.sValid()){
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=connect&op=wxRegister",
                data:{user_info:{nickname:username,captcha:captcha,member_mobile:mobile,pwd:pwd,member_mobile_bind:1,unionid:unionid}},
                dataType:'json',
                success:function(result){
                    if(!result.datas.error){
                        addCookie('username',result.datas.username);
                        addCookie('key',result.datas.key);
                        location.href = WapSiteUrl + '/tmpl/member/member.html?get_register_award=get_register_award';
                    }else{
                        errorTipsShow("<p>"+result.datas.error+"</p>");
                    }
                }
            });
        }
   });


});