function wx_config(shareData) {

    var key = getCookie('key');
    console.log(key,"key");
    if (key){
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=member_qrcode",
            data: {key: key},
            dataType: 'json',
            success: function (result) {
               // console.log(result,"result");
                var data = result.datas;
                var link_flag = shareData.link.indexOf("?") >= 0 ? '&' : '?';
                // alert(link_flag)
                if (data.usertype != 4) {
                    shareData.link += link_flag+'invite_code='+data.invite_code;
                    // alert(shareData.link)

                    wx_config_children(shareData);
                }

               // console.log(shareData)
            },
            fail:function(){
               // console.log("fail");
            }
        })
    }else{
        wx_config_children(shareData);
    }
}



function wx_config_children(shareData) {
    //console.log("ssssssssss");
    shareData.imgUrl = shareData.imgUrl.replace('https', 'http');
    var url = location.href.split('#')[0];
    var jssdk_url = ApiUrl + "/index.php?act=jssdk&op=get_sign_package&url="+encodeURIComponent(url);
    console.log(jssdk_url,"jssdk_url");
    $.ajax(jssdk_url, function (result) {  
        var data = result.datas;
        //console.log(data,"data");
        wx.config({
            debug: true,
            appId: data.appId,
            timestamp: data.timestamp,
            nonceStr: data.nonceStr,
            signature: data.signature,
            jsApiList: [
                "onMenuShareTimeline", //分享到朋友圈
                "onMenuShareAppMessage", //分享到朋友
                'updateTimelineShareData',
                'updateAppMessageShareData',
            ]
        });

        shareData.imgUrl = shareData.imgUrl.replace('https', 'http');
        //配置分享信息
        wx.ready(function () {
            wx.onMenuShareTimeline(shareData);
            wx.onMenuShareAppMessage(shareData);
            wx.updateAppMessageShareData(shareData);
            wx.updateTimelineShareData(shareData);
        });
        // 错误提示
        wx.error(function(res){
            alert(JSON.stringify(res));
        });
    })
}

/**
 * app 分享功能
 * @param shareData
 */
var appShareStr;
function appShareSet(shareData){
    var pic = shareData.imgUrl;
    shareData.imgUrl = pic.replace('http://','https://');
    var tmp = JSON.stringify(shareData);
    appShareStr = tmp;
    // console.log(tmp);
    // if(isapp == 1){
    //     var str = jsObj.shareData(tmp);
    //     alert(str);
    // }else{
//老版app分享....待删除....
        var apiInterval = setInterval(function() {
            if (typeof (api) != "undefined" && api && api.appId) {
                window.clearInterval(apiInterval);
                api.sendEvent({
                    name: 'shareparams',
                    extra: {
                        title: shareData.title,
                        description: shareData.desc,
                        pic: shareData.imgUrl,
                        url: shareData.link
                    }
                });
            }
        }, 1000);
    // }
}
function appShareData(){
    var str = jsObj.shareData(appShareStr);
}

