var key = getCookie('key');
var auction_order_id = getQueryString('auction_order_id');
var margin_id = getQueryString('margin_id');
var pay_name = 'online';
var invoice_id = 0;
var vat_hash,freight_hash,pd_pay,password,fcode='',rcb_pay,rpt,payment_code;
var message = {};
// 其他变量
var area_info;
var goods_id;
var ifshow_offpay = 0;
$(function() {
    $('#list-address-valve').click(function () {
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=member_address&op=address_list",
            data: {key: key},
            dataType: 'json',
            async: false,
            success: function (result) {
                checkLogin(result.login);
                if (result.datas.address_list == null) {
                    return false;
                }
                var data = result.datas;
                data.address_id = address_id;
                var html = template.render('list-address-add-list-script', data);
                $("#list-address-add-list-ul").html(html);
            }
        });
    });
    $.animationLeft({
        valve: '#list-address-valve',
        wrapper: '#list-address-wrapper',
        scroll: '#list-address-scroll'
    });

    // 地区选择
    $('#list-address-add-list-ul').on('click', 'li', function () {
        $(this).addClass('selected').siblings().removeClass('selected');
        eval('address_info = ' + $(this).attr('data-param'));
        _init(address_info.address_id);
        $('#list-address-wrapper').find('.header-l > a').click();
    });

    // 地址新增
    $.animationLeft({
        valve: '#new-address-valve',
        wrapper: '#new-address-wrapper',
        scroll: ''
    });
    // 支付方式
    $.animationLeft({
        valve : '#select-payment-valve',
        wrapper : '#select-payment-wrapper',
        scroll : ''
    });
    // 地区选择
    $('#new-address-wrapper').on('click', '#varea_info', function () {
        $.areaSelected({
            success: function (data) {
                city_id = data.area_id_2 == 0 ? data.area_id_1 : data.area_id_2;
                area_id = data.area_id;
                area_info = data.area_info;
                $('#varea_info').val(data.area_info);
            }
        });
    });
    // 发票
    $.animationLeft({
        valve : '#invoice-valve',
        wrapper : '#invoice-wrapper',
        scroll : ''
    });
    
    
    template.helper('isEmpty', function(o) {
        var b = true;
        $.each(o, function(k, v) {
            b = false;
            return false;
        });
        return b;
    });
    
    template.helper('pf', function(o) {
        return parseFloat(o) || 0;
    });

    template.helper('p2f', function(o) {
        return (parseFloat(o) || 0).toFixed(2);
    });

    var _init = function (address_id) {
      console.log(1213)
        var totals = 0;
        // 购买第一步 提交
        $.ajax({//提交订单信息
            type:'get',
            url:ApiUrl2+'/index.php?act=member_buy_auction&op=show_auction_order',
            dataType:'json',
            data:{key:key,auction_order_id:auction_order_id,address_id:address_id,margin_id:margin_id},
            success:function(result){
                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                    return false;
                }
                // 商品数据
                result.datas.WapSiteUrl = WapSiteUrl;
                var html = template.render('goods_list', result.datas);
                $("#deposit").html(html);
                if (typeof(result.datas.ifshow_offpay) != 'undefined') {
                    ifshow_offpay = result.datas.ifshow_offpay;
                }
                store_id = result.datas.auction_order_info.store_id;
                // 默认地区相关
                if ($.isEmptyObject(result.datas.auction_order_info.address_info)) {
                    $.sDialog({
                        skin: "block",
                        content: '请添加地址',
                        okFn: function () {
                            $('#new-address-valve').click();
                        },
                        cancelFn: function () {
                            history.go(-1);
                        }
                    });
                    return false;
                }

                // 输入地址数据
                insertHtmlAddress(result.datas.auction_order_info.address_info, result.datas.address_api);
                if (result.datas.underline) {
                    $('#payment-underline').show();
                } else {
                    $('#payment-underline').hide();
                }

                if (typeof(result.datas.inv_info.inv_id) != 'undefined') {
                invoice_id = result.datas.inv_info.inv_id;
                }
                // 发票
                $('#invContent').html(result.datas.inv_info.content);
                vat_hash = result.datas.vat_hash;

                // 总价
                $('#storeTotal').html(result.datas.auction_order_info.order_amount);
                totals = parseFloat(result.datas.auction_order_info.order_amount);
                var margin_amount = parseFloat(result.datas.auction_order_info.margin_amount);
              console.log(margin_amount)
                var pay_totals = result.datas.auction_order_info.pay_amount;
                $('#ToBuyStep2').parent().addClass('ok');

                password = '';

                //如果保证金金额大于支付金额
                if(margin_amount >　totals){

                    $('#margin_amount').html(totals);
                    $('#margin_amount_memo').html('您剩余的保证金(<b>￥'+(margin_amount - totals)+'</b>)会保留在您的保证金帐号中');
                } else {

                    $('#margin_amount').html(margin_amount);
                }

                $('#onlineTotal').html(totals.toFixed(2));
                $('#totalPrice').html(pay_totals.toFixed(2));

              //up
              upOrder();


            }


        });
    }
  var _init2 = function (address_id) {
    console.log(1213)
    var totals = 0;
    // 购买第一步 提交
    $.ajax({//提交订单信息
      type:'get',
      url:ApiUrl2+'/index.php?act=member_buy_auction&op=show_auction_order',
      dataType:'json',
      data:{key:key,auction_order_id:auction_order_id,address_id:address_id,margin_id:margin_id},
      success:function(result){
        checkLogin(result.login);
        if (result.datas.error) {
          $.sDialog({
            skin:"red",
            content:result.datas.error,
            okBtn:false,
            cancelBtn:false
          });
          return false;
        }
        // 商品数据
        result.datas.WapSiteUrl = WapSiteUrl;
        var html = template.render('goods_list', result.datas);
        $("#deposit").html(html);
        if (typeof(result.datas.ifshow_offpay) != 'undefined') {
          ifshow_offpay = result.datas.ifshow_offpay;
        }
        store_id = result.datas.auction_order_info.store_id;
        // 默认地区相关
        if ($.isEmptyObject(result.datas.auction_order_info.address_info)) {
          $.sDialog({
            skin: "block",
            content: '请添加地址',
            okFn: function () {
              $('#new-address-valve').click();
            },
            cancelFn: function () {
              history.go(-1);
            }
          });
          return false;
        }

        // 输入地址数据
        insertHtmlAddress(result.datas.auction_order_info.address_info, result.datas.address_api);
        if (result.datas.underline) {
          $('#payment-underline').show();
        } else {
          $('#payment-underline').hide();
        }

        if (typeof(result.datas.inv_info.inv_id) != 'undefined') {
          invoice_id = result.datas.inv_info.inv_id;
        }
        // 发票
        $('#invContent').html(result.datas.inv_info.content);
        vat_hash = result.datas.vat_hash;

        // 总价
        $('#storeTotal').html(result.datas.auction_order_info.order_amount);
        totals = parseFloat(result.datas.auction_order_info.order_amount);
        var margin_amount = parseFloat(result.datas.auction_order_info.margin_amount);
        console.log(margin_amount)
        var pay_totals = result.datas.auction_order_info.pay_amount;
        $('#ToBuyStep2').parent().addClass('ok');

        password = '';

        //如果保证金金额大于支付金额
        if(margin_amount >　totals){

          $('#margin_amount').html(totals);
          $('#margin_amount_memo').html('您剩余的保证金(<b>￥'+(margin_amount - totals)+'</b>)会保留在您的保证金帐号中');
        } else {

          $('#margin_amount').html(margin_amount);
        }

        $('#onlineTotal').html(totals.toFixed(2));
        $('#totalPrice').html(pay_totals.toFixed(2));

        //up
        //upOrder();


      }


    });
  }
  var upOrder = function (){
    $.ajax({
      type: 'post',
      url: ApiUrl2 + "/index.php?act=member_buy_auction&op=update_auction_order",
      data: {key: key,vat_hash:vat_hash,pay_name:'online',auction_order_id:auction_order_id,},
      dataType: 'json',
      async: false,
      success: function (result) {
        _init2()
      }
    });
  }


  rcb_pay = 0;
    pd_pay = 0;

    // 初始化
    _init();

    // 插入地址数据到html
    var insertHtmlAddress = function (address_info, address_api) {
        address_id = address_info.address_id;
        $('#true_name').html(address_info.true_name);
        $('#mob_phone').html(address_info.mob_phone);
        $('#address').html(address_info.area_info + address_info.address);
        // area_id = address_info.area_id;
        // city_id = address_info.city_id;
        //
        // if (address_api.content) {
        //     for (var k in address_api.content) {
        //         $('#storeFreight' + k).html(parseFloat(address_api.content[k]).toFixed(2));
        //     }
        // }
        // offpay_hash = address_api.offpay_hash;
        // offpay_hash_batch = address_api.offpay_hash_batch;
        // if (address_api.allow_offpay == 1 && ifshow_offpay) {
        //     $('#payment-offline').show();
        // } else {
        //     $('#payment-offline').hide();
        //     pay_name = 'online';
        //     $('#select-payment-valve').find('.current-con').html('在线支付');
        // }
        // if (!$.isEmptyObject(address_api.no_send_tpl_ids)) {
        //     $('#ToBuyStep2').parent().removeClass('ok');
        //     for (var i = 0; i < address_api.no_send_tpl_ids.length; i++) {
        //         $('.transportId' + address_api.no_send_tpl_ids[i]).show();
        //     }
        // } else {
        //     $('#ToBuyStep2').parent().addClass('ok');
        // }
    }
    // 支付方式选择
    // 在线支付
    $('#payment-online').click(function(){
        pay_name = 'online';
        $('#select-payment-wrapper').find('.header-l > a').click();
        $('#select-payment-valve').find('.current-con').html('在线支付');
        $(this).addClass('sel').siblings().removeClass('sel');
    })
    // 线下支付
    $('#payment-underline').click(function(){
        pay_name = 'underline';
        $('#select-payment-wrapper').find('.header-l > a').click();
        $('#select-payment-valve').find('.current-con').html('线下支付');
        $(this).addClass('sel').siblings().removeClass('sel');
    })

    // 发票选择
    $('#invoice-noneed').click(function(){
        $(this).addClass('sel').siblings().removeClass('sel');
        $('#invoice_add,#invoice-list-scroll').hide();
        invoice_id = 0;
    });
    $('#invoice-need').click(function(){
        $(this).addClass('sel').siblings().removeClass('sel');
        $('#invoice-list-scroll').show();
        $.ajax({//获取发票内容
            type:'post',
            url:ApiUrl+'/index.php?act=member_invoice&op=invoice_content_list',
            data:{key:key},
            dataType:'json',
            success:function(result){
                checkLogin(result.login);
                var data = result.datas;
                var html = '';
                $.each(data.invoice_content_list,function(k,v){
                    html+='<option value="'+v+'">'+v+'</option>';
                });
                $('#inc_content').append(html);
            }
        });
        //获取发票列表
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_invoice&op=invoice_list',
            data:{key:key},
            dataType:'json',
            success:function(result){
                checkLogin(result.login);
                var html = template.render('invoice-list-script', result.datas);
                $('#invoice-list').html(html);
                var invoice_scroll = new IScroll('#invoice-list-scroll', { mouseWheel: true, click: true });
                if (result.datas.invoice_list.length > 0) {
                    invoice_id = result.datas.invoice_list[0].inv_id;
                }
                $('.del-invoice').click(function(){
                    var $this = $(this);
                    var inv_id = $(this).attr('inv_id');
                    $.ajax({
                        type:'post',
                        url:ApiUrl+'/index.php?act=member_invoice&op=invoice_del',
                        data:{key:key,inv_id:inv_id},
                        success:function(result){
                            if(result){
                                $this.parents('label').remove();
                            }
                            return false;
                        }
                    });
                });
            }
        });
    })
    // 发票类型选择
    $('input[name="inv_title_select"]').click(function(){
        if ($(this).val() == 'person') {
            $('#inv-title-li').hide();
        } else {
            $('#inv-title-li').show();
        }
    });
    $('#invoice-div').on('click', '#invoiceNew', function(){
        invoice_id = 0;
        $('#invoice_add').show();
        $('#invoice-list-scroll').hide();
    });
    $('#invoice-list').on('click', 'label', function(){
        invoice_id = $(this).find('input').val();
        $('#invoice_add').hide();
    });
    // 发票添加
    $('#invoice-div').find('.btn-l').click(function(){
        if ($('#invoice-need').hasClass('sel')) {
            if (invoice_id == 0) {
                var param = {};
                param.key = key;
                param.inv_title_select = $('input[name="inv_title_select"]:checked').val();
                param.inv_title = $("input[name=inv_title]").val();
                param.inv_content = $('select[name=inv_content]').val();
                if (param.inv_title_select == 'person') {
                    param.inv_title = '个人';
                }
                $.ajax({
                    type:'post',
                    async:false,
                    url:ApiUrl+'/index.php?act=member_invoice&op=invoice_add',
                    data:param,
                    dataType:'json',
                    success:function(result){
                        if(result.datas.inv_id>0){
                            invoice_id = result.datas.inv_id;
                            $('#invoiceNew').before('<label><i></i><input type="radio" name="invoice" value="'+invoice_id+
                            '"/><span id="inv_'+invoice_id+'">'+param.inv_title+'&nbsp;&nbsp;'+param.inv_content+
                            '</span><a class="del-invoice" href="javascript:void(0);" inv_id="'+invoice_id+'"></a></label>');
                        }
                    }
                });
                $('#invContent').html(param.inv_title + ' ' + param.inv_content);
            } else {
                $('#invContent').html($('#inv_'+invoice_id).html());
            }
        } else {
            $('#invContent').html('不需要发票');
        }
        $('#invoice-wrapper').find('.header-l > a').click();
    });
// 地址保存
    $.sValid.init({
        rules: {
            vtrue_name: "required",
            vmob_phone: {
                required: true,
                mobile: true
            },
            varea_info: "required",
            vaddress: "required"
        },
        messages: {
            vtrue_name: "姓名必填！",
            vmob_phone: {
                required: "手机号必填",
                mobile: "请填写正确的手机号码",
            },
            varea_info: "地区必填！",
            vaddress: "街道必填！"
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                var errorHtml = "";
                $.map(eMsg, function (idx, item) {
                    errorHtml += "<p>" + idx + "</p>";
                });
                errorTipsShow(errorHtml);
            } else {
                errorTipsHide();
            }
        }
    });
    $('#add_address_form').find('.btn').click(function () {
        if ($.sValid()) {
            var param = {};
            param.key = key;
            param.true_name = $('#vtrue_name').val();
            param.mob_phone = $('#vmob_phone').val();
            param.address = $('#vaddress').val();
            param.city_id = city_id;
            param.area_id = area_id;
            param.area_info = $('#varea_info').val();
            param.is_default = 0;

            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=member_address&op=address_add",
                data: param,
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        param.address_id = result.datas.address_id;
                        _init(param.address_id);
                        $('#new-address-wrapper,#list-address-wrapper').find('.header-l > a').click();
                    }
                }
            });
        }
    });
    
    // 支付
    var buy_step2 = 0;
    $('#ToBuyStep2').click(function(){
        if (buy_step2) {
            $.sDialog({
                skin:"red",
                content:'订单正在处理中，请勿重复点击！',
                okBtn:false,
                cancelBtn:false
            });
            return false;
        }
        buy_step2 = 1;
        var msg = '';
        for (var k in message) {
            msg += k + '|' + $('#storeMessage' + k).val() + ',';
        }
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_buy_auction&op=update_auction_order',
            data:{
                key:key,
                vat_hash:vat_hash,
                pay_name:pay_name,
                address_id:address_id,
                invoice_id:invoice_id,
                auction_order_id:auction_order_id,
                margin_id:margin_id,
                store_id:store_id,
                pd_pay:pd_pay,
                password:password,
                rcb_pay:rcb_pay,
                pay_message:msg
                },
            dataType:'json',
            success: function(result){
                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                    buy_step2 = 0;
                    return false;
                }
                if(result.datas.payment_code == 'underline'){
                    window.location.href = WapSiteUrl + '/tmpl/member/underline_pay_auction.html?pay_sn='+result.datas.pay_sn + '&type=auction';
                }else {
                    toPay(result.datas.pay_sn,'member_buy_auction','pay','auction');
                }
            }
        });
    });
});