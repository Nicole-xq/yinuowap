$(function() {
    var special_id = getQueryString('special_id');

    //加载店铺详情
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=special&op=special_details",
        data: {special_id:special_id},
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            window._bd_share_config={
                "common":{
                    "bdText":data.special_info.special_name,
                    "bdPic":data.special_info.adv_image_paath,
                    "bdUrl":""
                },
                "share":{
                    "bdSize":"24"
                },
                "selectShare":{
                    "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
                }
            };
            with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];

            var html = template.render('special_detail', data);
            $("#special_detail_html").html(html);
            updateEndTime();
            //for(i=0; i<data.special_goods_list.length;i++){
            //    if(data.special_goods_list[i].is_kaipai == 1){
            //        takeCount();
            //    }
            //}

            if(isWeiXin) {
            var shareData = {
                title: data.special_info.special_name+'-艺诺拍卖',
                desc: window.location.href,
                link: window.location.href,
                imgUrl: data.special_info.adv_image_path,
            };
            wx_config(shareData);
        }

        }
    });



});

function takeCount() {
    setTimeout("takeCount()", 1000);
    $(".count-time").each(function(){
        var obj = $(this);
        var tms = obj.attr("count_down");
        if (tms>0) {
            tms = parseInt(tms)-1;
            var days = Math.floor(tms / (1 * 60 * 60 * 24));
            var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
            var minutes = Math.floor(tms / (1 * 60)) % 60;
            var seconds = Math.floor(tms / 1) % 60;

            if (days < 0) days = 0;
            if (hours < 0) hours = 0;
            if (minutes < 0) minutes = 0;
            if (seconds < 0) seconds = 0;
            obj.find("[time_id='d']").html(days);
            obj.find("[time_id='h']").html(hours);
            obj.find("[time_id='m']").html(minutes);
            obj.find("[time_id='s']").html(seconds);
            obj.attr("count_down",tms);
        }
    });
}

//倒计时函数
function updateEndTime()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function(i){

        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("距结束&nbsp;<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
        }

    });
    setTimeout("updateEndTime()",1000);
}
