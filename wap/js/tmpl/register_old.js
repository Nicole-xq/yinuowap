$(function(){
    var key = getCookie('key');
    if (key) {
        window.location.href = WapSiteUrl+'/tmpl/member/member.html';
        return;
    }

    var unionid = getQueryString('unionid');
    $("#goToLogin").attr('href',WapSiteUrl+'/tmpl/member/login.html?unionid='+unionid);
    if (unionid == '' && isWeiXinLoad()){
        location.href = ApiUrl+'/index.php?act=connect&op=index&invite_code='+getQueryString('invite_code');
    }
    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state&t=connect_sms_reg', function(result){
        if (result.datas != '0') {
            $('.register-tab').show();
        }
    });

    
	$.sValid.init({//注册验证
        rules:{
        	username:{
        	    required:true,
                mobile:true
            },
            userpwd:"required",
            // password_confirm:"required",
            //email:{
            //	required:true,
            //	email:true
            //}
        },
        messages:{
            username:"用户名必须填写！",
            username:{
        	    required:"手机号必须填写！",
                mobile:'手机号错误！',
            },
            userpwd:"密码必填!",
            // password_confirm:"确认密码必填!",
            //email:{
            //	required:"邮件必填!",
            //	email:"邮件格式不正确"
            //}
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });
	$('#registerbtn').click(function(){
        //if (!$(this).parent().hasClass('ok')) {
        //    return false;
        //}
        if($('#agree_protocol').val() == 0){
            return false;
        }

        var username = $("input[name=username]").val();
        var captcha = $('#captcha').val();
        var pwd = $("input[name=pwd]").val();
        var client = 'wap';
        var source = getQueryString('source');
        if($.sValid()){
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=login&op=sms_register",
                data:{phone:username,captcha:captcha,password:pwd,client:client,source:source},
                dataType:'json',
                success:function(result){
                    if(!result.datas.error){
                        addCookie('username',result.datas.username);
                        addCookie('key',result.datas.key);
                        delCookie('invite_code');
                        location.href = WapSiteUrl + '/tmpl/member/member.html?get_register_award=get_register_award';
                    }else{
                        errorTipsShow("<p>"+result.datas.error+"</p>");
                    }
                }
            });
        } else {
        errorTipsShow('<p>' + result.datas.error + '<p>');
    }

	});


    // 发送手机验证码
    $('#refister_mobile_btn').click(function(){
        var mobile = $('#username').val();
        if(mobile.length != 11){
            $.sDialog({
                skin:"red",
                content:'请先填写手机号',
                okBtn:false,
                cancelBtn:false
            });
        }
        var pattern=new RegExp("^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\\d{8}$");
        if(!pattern.test(mobile)){
            $.sDialog({
                skin:"red",
                content:'手机号错误',
                okBtn:false,
                cancelBtn:false
            });
        }else {
            send_sms(mobile);
        }
        // if(mobile != ''){
        //     send_sms(mobile);
        // }else{
        //     $.sDialog({
        //         skin:"red",
        //         content:'请先填写手机号',
        //         okBtn:false,
        //         cancelBtn:false
        //     });
        // }

    });


    $('#checkbox').click(function(){
        if($('#checkbox').is(':checked')){
            $('#agree_protocol').val(1);
        }else{
            $('#agree_protocol').val(0);
        }
    });
});

// 发送手机验证码
function send_sms(mobile) {
    $.getJSON(ApiUrl+'/index.php?act=login&op=get_sms_captcha', {type:1,phone:mobile}, function(result){
        if(!result.datas.error){
            $.sDialog({
                skin:"green",
                content:'发送成功',
                okBtn:false,
                cancelBtn:false
            });
            $('.send_captcha').hide();
            $('.code-countdown').show().find('em').html(result.datas.sms_time);
            var times_Countdown = setInterval(function(){
                var em = $('.code-countdown').find('em');
                var t = parseInt(em.html() - 1);
                if (t == 0) {
                    $('.send_captcha').show();
                    $('.code-countdown').hide();
                    clearInterval(times_Countdown);
                } else {
                    em.html(t);
                }
            },1000);
        }else{
            errorTipsShow('<p>' + result.datas.error + '<p>');
        }
    });
}

$(function(){
    //如果是微信端的时候
    if(isWeiXinLoad()){
        $('.qll_bindwx').show();
        $('.login .account-img').hide();
    }else{
        $('.qll_bindwx').hide();
        $('.login .account-img').show();
    }
});