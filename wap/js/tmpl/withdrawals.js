/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var key = getCookie('key');


    var pd; //可用余额
    $.getJSON(ApiUrl + '/index.php?act=member_index&op=member_info', {'key': key}, function (result) {
        var data = result.datas;
        pd = data.available_predeposit;
        var bankcard_id = getQueryString('bankcard_id'); //如果是添加银行卡后的返回
        var is_pwd = false;
        if (!key) {
            window.location.href = WapSiteUrl + '/index.html';
        }
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=member_card",
            data: {key: key},
            dataType: 'json',
            success: function (result) {
                console.log(result)
                checkLogin(result.login);
                var data = result.datas;
                data.pd = pd;
                var html = template.render('withdrawals_con', data);
                $("#withdrawals").html(html);

                var client_h = document.documentElement.clientHeight;
                $(window).on("resize", function () {
                    var body_h = document.body.scrollHeight;
                    if (body_h < client_h) {
                        $('.lm_footer').css({'position': 'static'});
                    } else {
                        $('.lm_footer').css({'position': 'fixed'});
                    }
                });
                $('#password,#password1').on('input', function () {
                    var val = $(this).val();
                    if (val.length > 6) {
                        $(this).val($(this).val().substr(0, 6))
                    }
                });
                if (data.bankcard_list[0] && !bankcard_id) {
                    bankcard_id = data.bankcard_list[0].bankcard_id;
                }
                //点击打开银行卡选择
                $('.bank_select_list .bank_item').click(function () {
                    $('.bank_mask').addClass('active');
                    if (data.bankcard_list.length < 2) {
                        $('.bank_mask .bank_item').addClass('active');
                    }
                    return false;
                });

                $('.select_bank_add .bank_item,.select_bank_add .close').click(function () {
                    $('.bank_mask').removeClass('active');
                    return false;
                });

                function initBankInfo(bankcard_id) {
                    $.each(data.bankcard_list, function (i, e) {
                        if (e.bankcard_id == bankcard_id) {
                            $('.bank_select_list .bank_item').attr('bankcard_id', bankcard_id);
                            $('.bank_select_list .bank_item .bank_img img').attr('src', e.bank_image);
                            $('.bank_select_list .bank_item .bank_card').text('**** ***** **** ' + e.bank_card.toString().slice(-4));
                        }
                    });
                }

                initBankInfo(bankcard_id);
                //点击选择银行卡
                $('.bank_mask .bank_item').click(function () {
                    var self = $(this);
                    if (!self.hasClass('active')) {
                        $('.bank_mask .bank_item').removeClass('active');
                        self.addClass('active');
                    }
                    bankcard_id = $(this).attr('bankcard_id');
                    initBankInfo(bankcard_id);
                    $('.bank_mask').removeClass('active');
                    return false;
                });
                //输入取现金额时
                $('.drawals_price_val').on('input', function () {
                    if (parseFloat($(this).val()) > parseFloat(pd)) {
                        $(this).val(pd);
                    }
                    if (parseFloat($(this).val()) < 1) {
                        $(this).val(1);
                    }
                });
                //点击提现
                $('.withdrawals_btn').click(function () {
                    var money = $('.drawals_price_val').val();
                    if (!parseFloat(pd)) {
                        $(document).tipModal({
                            html: '余额不足',
                        });
                        return false;
                    }
                    var card_number, bank_name, bank_user;
                    if (!bankcard_id) {
                        $(document).tipModal({
                            html: '请选择银行卡',
                        });
                        return false;
                    }
                    if (!parseFloat(money)) {
                        $(document).tipModal({
                            html: '请输入正确提现金额',
                        });
                        return false;
                    }
                    if (parseFloat(money) < 100) {
                        $(document).tipModal({
                            html: '最低提现金额≥100元',
                        });
                        return false;
                    }


                    //如果存在已选中银行卡
                    $.each(data.bankcard_list, function (i, e) {
                        if (e.bankcard_id == bankcard_id) {
                            card_number = e.bank_card;
                            bank_name = e.bank_name;
                            bank_user = e.true_name;
                        }
                    });


                    $.ajax({
                        type: 'post',
                        url: ApiUrl + "/index.php?act=member_account&op=check_init_paypwd",
                        data: {
                            key: key,
                        },
                        dataType: 'json',
                        success: function (result) {
                            // 0代表已经设置支付密码
                            if (result.datas == 1) {
                                $('.drawals_modal').hide();
                                $('.input_bank_new').show()
                            } else {
                                $('.drawals_modal').hide();
                                $('.input_bank_pwd').show()
                                //支付密码请输入时
                                boxInput.init(function () {
                                    getValue();
                                });
                            }
                        }
                    });


                    function getValue() {
                        $('#payment_password').val(boxInput.getBoxInputValue());
                        password = $('#payment_password').val()
                        $.ajax({
                            type: 'post',
                            url: ApiUrl + "/index.php?act=member_index&op=pd_cash_add",
                            data: {
                                key: key,
                                money: money,
                                card_number: card_number,
                                bank_name: bank_name,
                                bank_user: bank_user,
                                password: password
                            },
                            dataType: 'json',
                            success: function (result) {
                                if (result.code == 200) {
                                    $('.drawals_modal').hide();
                                    $('.input_bank_success').show()

                                    function jump(count) {
                                        window.setTimeout(function () {
                                            count--;
                                            if (count > 0) {
                                                $('#num_djs').text(count + '秒');
                                                jump(count);
                                            } else {
                                                location.href = WapSiteUrl + '/tmpl/member/predepositlog_list.html'
                                            }
                                        }, 1000);
                                    }

                                    jump(3);

                                } else {
                                    $('.drawals_modal').hide();
                                    $('.input_bank_error').show()
                                    $('.agine_inp').click(function () {
                                        $('.drawals_modal').hide();
                                        $('.drawals_modal input').val('');
                                        $('.input_bank_pwd').show()
                                        //支付密码请输入时
                                        boxInput.init(function () {
                                            getValue();
                                        });
                                        return false;
                                    });
                                    // errorTipsShow('<p>' + result.datas.error + '</p>');
                                }
                            }
                        });
                    }

                    return false;
                });

                function validata_ls() {
                    var reg = /^[0-9]{6}$/;
                    var password = $.trim($("#password").val());
                    var password1 = $.trim($("#password1").val());
                    if (!reg.test(password)) {
                        return 1;
                    }else if(password1 != password){
                        return 2;
                    }
                    return 3;
                }


                // $('.withdrawals_btn').click();


                $('.input_form_sub').click(function () {
                    var password = $.trim($("#password").val());
                    var password1 = $.trim($("#password1").val());
                    if (validata_ls()===3) {
                        $.ajax({
                            type: 'post',
                            url: ApiUrl + "/index.php?act=member_account&op=modify_paypwd_step5",
                            data: {key: key, password: password, password1: password1},
                            dataType: 'json',
                            success: function (result) {
                                if (result.code == 200) {
                                    // $('.drawals_modal').hide();
                                    // $('.input_bank_success').show();
                                    $(document).tipModal({
                                        html: '支付密码设置成功',
                                    });
                                    $('.drawals_modal').hide();
                                    $('.drawals_modal input').val('');
                                } else {
                                    // $('.drawals_modal').hide();
                                    // $('.input_bank_error').show();
                                    $(document).tipModal({
                                        html: result.datas.error,
                                    });
                                    $('.drawals_modal').hide();
                                    $('.drawals_modal  input').val('');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 2000)
                                    // errorTipsShow('<p>' + result.datas.error + '</p>');
                                }
                            }
                        });
                    } else if(validata_ls()===1){
                        $(document).tipModal({
                            html: "密码格式输入错误",
                        });
                    } else if(validata_ls()===2){
                        $(document).tipModal({
                            html: "两次密码输入不一致",
                        });
                    }
                    return false;
                });

                //点击空白部分或者叉关闭modal
                $('.drawals_modal .drawals_modal_mask,.drawals_modal .close').on('click', function () {
                    $(this).parents('.drawals_modal').hide();
                    $('.drawals_modal input').val('');
                    return false;
                });
            }
        });

    });


    // 密码输入框，未优化
    var container = document.getElementById("inputBoxContainer");
    boxInput = {
        maxLength: "",
        realInput: "",
        bogusInput: "",
        bogusInputArr: "",
        callback: "",
        init: function (fun) {
            var that = this;
            this.callback = fun;
            that.realInput = container.children[0];
            that.bogusInput = container.children[1];
            that.bogusInputArr = that.bogusInput.children;
            that.maxLength = that.bogusInputArr[0].getAttribute("maxlength");
            that.realInput.oninput = function () {
                that.setValue();
            }
            that.realInput.onpropertychange = function () {
                that.setValue();
            }
        },
        setValue: function () {
            this.realInput.value = this.realInput.value.replace(/\D/g, "");
            var real_str = this.realInput.value;
            for (var i = 0; i < this.maxLength; i++) {
                this.bogusInputArr[i].value = real_str[i] ? real_str[i] : "";
            }
            if (real_str.length >= this.maxLength) {
                this.realInput.value = real_str.substring(0, 6);
                this.callback();
            }
        },
        getBoxInputValue: function () {
            var realValue = "";
            for (var i in this.bogusInputArr) {
                if (!this.bogusInputArr[i].value) {
                    break;
                }
                realValue += this.bogusInputArr[i].value;
            }
            return realValue;
        }
    }
});


// document.addEventListener('touchstart', function (event) {
//     // 判断默认行为是否可以被禁用
//     if (event.cancelable) {
//         // 判断默认行为是否已经被禁用
//         if (!event.defaultPrevented) {
//             event.preventDefault();
//         }
//     }
// }, false);