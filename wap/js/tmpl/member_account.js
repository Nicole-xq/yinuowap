$(function() {
    var key = getCookie('key');
    var data = {key:key,edit_name:'member_sex',edit_value:''};
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
  
    $.ajax({
        type:'post',
        url:ApiUrl+"/index.php?act=member_center&op=get_information",
        data:{key:key},
        dataType:'json',
        success:function(result){
            var datas = result.datas;
            var html = template.render('accountBox', datas);
            $("#accountBoxHtml").html(html);
            $('.choose_sex_btn').click(function () {
                $('.choose_sex').show();
                $('.choose_sex_wrap').show();
                var sexVal = $('.account_box_list_sex span').html();
                if(sexVal=='女'){
                    $('.sex_women .choose_before').hide();
                    $('.sex_women .choose_after').show();
                }else {
                    $('.sex_men .choose_before').hide();
                    $('.sex_men .choose_after').show();
                }
                setTimeout(function () {
                    $('.choose_sex').hide();
                    $('.choose_sex_wrap').hide();
                },3000);
            });
            $('.sex_btn').click(function () {
                data.edit_value = $(this).is('.sex_men')?1:2;
                //修改性别
                $.ajax({
                    type: "POST",
                    url: ApiUrl + "/index.php?act=member_center&op=save_information",
                    dataType:'json',
                    data: data,
                    success: function(data){}
                });
                $(this).find($('.choose_before')).toggle();
                $(this).find($('.choose_after')).toggle();
                $(this).siblings().find($('.choose_before')).show();
                $(this).siblings().find($('.choose_after')).hide();
                $('.account_box_list_sex span').html($(this).find('span').html());
                setTimeout(function () {
                    $('.choose_sex').hide();
                    $('.choose_sex_wrap').hide();
                },10);
            });
        }
    });
    // $.ajax({
    //     type:'get',
    //     url:ApiUrl+"/index.php?act=member_account&op=get_paypwd_info",
    //     data:{key:key},
    //     dataType:'json',
    //     success:function(result){
    //         if(result.code == 200){
    //         	if (!result.datas.state) {
    //         		$('#paypwd_tips').html('未设置');
    //         	}
    //         }else{
    //         }
    //     }
    // });

});