/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    if (getQueryString('invite_code')) {
        addCookie('source_staff_id', getQueryString('source_staff_id'));
        addCookie('invite_code', getQueryString('invite_code'));
        $("#invite_code").val(getQueryString('invite_code'))
        $("#invite_code").attr('disabled', 'disabled')
    }
    var referurl = document.referrer;//上级网址
    // alert(referurl+'=====上级网址')
    var key = getCookie('key');
    if (key) {
        window.location.href = WapSiteUrl + '/tmpl/member/member.html';
        return;
    }

    var unionid = getQueryString('unionid');
    $('.target_reg').attr('href', $('.target_reg').attr('href') + '?unionid=' + unionid)
    // alert(unionid + '----unionid')

    if (unionid == '' && isWeiXinLoad()) {
        // alert('yes')
        location.href = ApiUrl + '/index.php?act=connect&op=index&invite_code=' + getQueryString('invite_code') + '&ex_id=' + getQueryString('ex_id');
    }

    $.sValid.init({
        rules: {
            username: {
                required: true,
                mobile: true
            },
            captcha: "required",
            password: {
                required: true,
                maxlength: 18,
                minlength: 6
            }
        },
        messages: {
            username: "手机号必须填写！",
            captcha: "验证码必须填写！",
            password: {
                required: "密码必须是6-18位!",
                maxlength: "密码必须是6-18位!",
                minlength: "密码必须是6-18位!"
            }
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                $.map(eId, function (idx, item) {
                    $('#' + idx).parents('.login_form_input_panpel').addClass('error');
                    if ('password' == idx) {
                        lm_errorTipsShow('密码必须是6-18位!');
                    }
                    $('#' + idx).attr('placeholder', eMsg[idx + '_required']);
                });
            } else {
                $('#' + eId).parents('.login_form_input_panpel').removeClass('error');
            }
        }
    });

    //点击获取验证码
    var getCodeFlag = true;
    $('#getCode').click(function () {
        var val = $('#username').val();
        var validata = /^(1{1})+\d{10}$/.test(val);
        if (!getCodeFlag || !validata) {
            if (!validata) {
                $('#username').parents('.login_form_input_panpel').addClass('error');
                $('#username').attr('placeholder', '请填写手机号!');
            }
            return false;
        }
        getCodeFlag = false;
        var self = $(this),
            time = 60;

        setTimeout(function () {
            getCodeFlag = true;
        }, 3000);
        $.getJSON(ApiUrl + '/index.php?act=login&op=get_sms_captcha', { type: 1, phone: val }, function (result) {
            if (!result.datas.error) {
                lm_errorTipsShow('<p>发送短信中....</p>');
                var times_Countdown = setInterval(function () {
                    time--;
                    if (time == 0) {
                        self.html('再次获取');
                        clearInterval(times_Countdown);

                    } else {
                        self.html('<span style="color:#8c8c8c;">' + time + '秒后再次获取</span>')
                    }
                }, 1000);
            } else {
                lm_errorTipsShow('<p>' + result.datas.error + '</p>');
            }
        });
    });
    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state', function (result) {
        var ua = navigator.userAgent.toLowerCase();
        var allow_login = 0;
        if (result.datas.pc_qq == '1') {
            allow_login = 1;
            $('.qq').show();
        }
        if (result.datas.pc_sn == '1') {
            allow_login = 1;
            $('.weibo').show();
        }
        if ((ua.indexOf('micromessenger') > -1) && result.datas.connect_wap_wx == '1') {
            allow_login = 1;
            $('.wx').show();
        }
        if (allow_login) {
            $('.quick_login').show();
        }
    });
    //点击登录
    //设置开关，用于提交间歇
    var allow_submit = true;
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $("#registerbtn").click();
        }
    });



    $('#registerbtn').click(function () {
        var username = $("#username").val();
        var captcha = $('#captcha').val();
        var pwd = $("#password").val();
        if (!getCookie('wap')) {
            var client = 'wap';
        }
        else {
            var client = getCookie('wap');
        }

        var source = getQueryString('source');
        var city_id = $('#addressname').attr('data-areaid2');
        var province_id = $('#addressname').attr('data-areaid1');
        var area_id = $('#addressname').attr('data-areaid');
        if (!getQueryString('invite_code')) {
            delCookie('invite_code');
            var invite_code = $('#invite_code').val()
            addCookie('invite_code', invite_code);
        }

        // console.log(pwd)
        // return false;
        //恢复error
        $('.form .login_form_input_panpel.error').removeClass("error");
        if ($.sValid()) {
            if (allow_submit) {
                allow_submit = false;
                $('.login_form_btn').addClass('active')
                //此处设置按钮不可以点击与不可点击的样式
            } else {
                return false;
            }
            setTimeout(function () {
                allow_submit = true;
            }, 3000);
            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=login&op=sms_register",
                data: {
                    phone: username,
                    captcha: captcha,
                    password: pwd,
                    client: client,
                    source: source,
                    // member_provinceid:province_id,
                    // member_cityid:city_id,
                    // member_areaid:area_id,
                    // member_areainfo:area_info,
                    member_provinceid: '',
                    member_cityid: '',
                    member_areaid: '',
                    member_areainfo: '',
                    source_staff_id: getQueryString('source_staff_id'),
                    ex_id: getQueryString('ex_id')
                },
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        addCookie('username', result.datas.username);
                        addCookie('key', result.datas.key);
                        delCookie('invite_code');
                        lm_errorTipsShow('<p>注册中....</p>');

                        if (getCookie('currentUrl')) {
                            var goUrl=getCookie('currentUrl');
                            delCookie('currentUrl');
                            location.href =  goUrl;
                        }else{
                            location.href = WapSiteUrl + '/tmpl/member/member.html?get_register_award=get_register_award';
                        }
                      

                        
                    } else {
                        lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                    }
                }
            });
        }
        return false;
    });


    // 选择地区
    // $('#addressname').on('click', function(){
    //     this.blur();
    //     $.areaSelected({
    //         success : function(data){
    //             $('#addressname').val(data.area_info).attr({'data-areaid':data.area_id, 'data-areaid1':data.area_id_1,'data-areaid2':data.area_id_2});
    //         }
    //     });
    // });
});