$(function(){
    var key = getCookie('key');
    if(!key){
        if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
            window.webkit.messageHandlers.loginOut.postMessage("loginOut");
            location.href = WapSiteUrl + "/tmpl/member/login.html";
            return
        }else if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1) {
            jsObj.loginOut();
            location.href = WapSiteUrl + "/tmpl/member/login.html";
            return
        }
        window.location.href = WapSiteUrl+'/index.html';
    }

    $.ajax({
        type:'post',
        url: appUrl1 + "/api/bankcard",
        data:{api_token:key},
        dataType:'json',
        success:function(result){
            checkLogin(result.login);
            var data = result.data;
            var html = template.render('member_bankcard', data);
            $("#member_bankcard_html").html(html);
        }
    });

    $('#member_bankcard_html').on('click','#card_list .delete-card',function (){
        console.log(11111)
        var bankcard_id = $(this).attr("bankcard_id");
        console.log(bankcard_id)
        $.sDialog({
            content: '是否移除银行卡？',
            okFn: function() { deleteCardId(bankcard_id); }
        });
    });

    if (isWeiXin()){
        $('header').hide();
        $("body").css('padding', '0');
    }

    function deleteCardId(bankcard_id) {
        $.ajax({
            type:"post",
            url:appUrl1 + "/api/bankcard/delete",
            data:{bankcard_id:bankcard_id,api_token:key},
            dataType:"json",
            success:function(result){
                console.log(result)
                if (result.status_code == 200) {
                    $.ajax({
                        type:'post',
                        url: appUrl1 + "/api/bankcard",
                        data:{api_token:key},
                        dataType:'json',
                        success:function(result){
                            checkLogin(result.login);
                            var data = result.data;
                            var html = template.render('member_bankcard', data);
                            $("#member_bankcard_html").html(html);
                        }
                    });
                        $.sDialog({
                            skin:"red",
                            content:'删除成功',
                            okBtn:false,
                            cancelBtn:false
                        });
                }

            }
        });
    }
});