var page = 10;
var curpage = 1;
var hasmore = true;
var current_type = 1;
//var footer = false;
var reset = true;
$(function(){
    var key = getCookie('key');
    $('.lm_cardcontent_ul li').click(function(){
        var index=$(this).index();
        $('.lm_cardcontent_ul li').removeClass('active');
        $(this).addClass('active');
        current_type = $(this).attr('data-type');
        reset = true;
        hasmore = true;
        curpage = 1;
        initPage(current_type);
    });
    // if(!key){
    //     window.location.href = WapSiteUrl+'/tmpl/member/login.html';
    // }
    var upper_id = getQueryString('member_id');
    var is_childrens = 'no';
    if(getQueryString('is_childrens') == 'ok'){
        is_childrens = 'ok';
    }

    function initPage(type) {

        $('.loading').remove();
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_partner&op=partner_list&page="+page+"&curpage="+curpage,
            data:{key:key,upper_id:upper_id,is_childrens:is_childrens,lv:type},
            dataType:'json',
            success:function(result){
                checkLogin(result.login);//检测是否登录了
                curpage++;
                hasmore = result.hasmore;
                var data = result;
                $('#total').html(data.datas.total);
                $('#count_lv_1').html(data.datas.lv_total_1);
                $('#count_lv_2').html(data.datas.lv_total_2);
                data.WapSiteUrl = WapSiteUrl;//页面地址
                data.ApiUrl = ApiUrl;
                data.key = getCookie('key');
                // if(is_childrens == 'no'){
                data.is_childrens = is_childrens;
                // }
                if(is_childrens == 'ok'){
                    $('#upper_name').html(result.datas.user_name);
                    $('#count_money').html(result.datas.commission_amount||'0.00');
                    // $('#count_money').html();
                }
                // data.name = result.datas.user_name + '下面的三级伙伴';

                var html = template.render('partner_body', data);
                var title = '';
                if(result.datas.partner_level == 2){
                    title = '我的合伙人';
                }else if(result.datas.partner_level == 3){
                    title = '我的三级合伙人';
                }
                $('.title').html(title);

                if (reset) {
                    reset = false;
                    $("#list-block").html(html);
                } else {
                    $("#list-block").append(html);
                }

                 $('.remarks_lm').on('click',remarks_lmClick);

            }
        });
    }

    initPage(current_type);//初始化分页
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage(current_type);
        }
    });


    function remarks_lmClick(self){

        $('#remarks_lm_wrapper').addClass('nctouch-full-mask left');
        $('#remarks_lm_wrapper').removeClass('hide');
        var memberId=$(this).parents('li').attr('data-memberId');
        $('#remarks_lm_wrapper .lm_sub').click(function(){
            var remarksVal=$('input[name=lmremarks]').val().trim();
            var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");

            if(pattern.test(remarksVal)||remarksVal.length<0||remarksVal.length>10){
                $.sDialog({
                    skin: "red",
                    content: '请输入正确的备注！',
                    okBtn: false,
                    cancelBtn: false
                });
                return false;
            }
            $.ajax({
                type:'post',
                url:ApiUrl+'/index.php?act=member_partner&op=update_member_comment',
                data:{key:key,comment:remarksVal,member_id:memberId},
                dataType:'json',
                success:function(result){
                    if(result.datas=='success'){
                        $.sDialog({
                            skin: "green",
                            content: '修改成功',
                            okBtn: false,
                            cancelBtn: false
                        });
                        // location.reload();
                        $('#remarks_lm_wrapper').removeClass('nctouch-full-mask left');
                        $('#remarks_lm_wrapper').addClass('hide');
                    }
                }
            });
        });
    }

    $(document).on('click','#qll_header_close',function(){
        $('#remarks_lm_wrapper').removeClass('nctouch-full-mask left');
        $('#remarks_lm_wrapper').addClass('hide');
    });


});

