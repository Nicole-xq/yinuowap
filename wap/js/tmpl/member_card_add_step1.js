$(function () {
    var key = getCookie('key');
    var withdrawals = getQueryString('withdrawals');
    var pd = getQueryString('pd');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    if (withdrawals && withdrawals == 1) {
        $('#header .header-l a').attr('href','javascript:history.go(-1)')
    }

    $.sValid.init({
        rules: {
            card_name: "required",
            card_number: {
                required: true,
                maxlength: 19,
                minlength: 16,
            },
            id_number: {
                required : true,
                minlength: 4,
            }

        },
        messages: {
            card_name: "请填写持卡人姓名!",
            card_number: {
                required : "请填写卡号!",
                maxlength : "请填写正确卡号!",
                minlength : "请填写正确卡号!",
            },
            id_number: {
                required : "请选择发卡银行!"
            }
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                var errorHtml = "";
                $.map(eMsg, function (idx, item) {
                    errorHtml += "<p>" + idx + "</p>";
                });
                errorTipsShow(errorHtml);
            } else {
                errorTipsHide();
            }
        }
    });
    $.ajax({

        type: 'post',
        url: ApiUrl + "/index.php?act=member_card&op=get_member_info",
        data: {key: key},
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            if(data.true_name && data.id_number){
                $("#card_name").val(data.true_name).attr("readonly","readonly").removeAttr("id");
                // $("#id_number").val(data.id_number).attr("readonly","readonly").removeAttr("id");
            }
        }
    });

    var obj_member = {
        withdrawals : getQueryString('withdrawals'),
        pd : getQueryString('pd'),
        init : function () {
            obj_member.card_name   = $('#card_name').val();
            obj_member.id_number   = $('#id_number').val();
            obj_member.card_number = $('#card_number').val();
        },
        test_bind : function () {
            // $.post(
            //     ApiUrl + '/index.php?act=member_card&op=test_bind',
            //     {key:key,id_number:obj_member.id_number},
            //     function (result) {
            //         if(result.code != 200){
            //             errorTipsShow(result.datas.error);
            //             return false;
            //         }
            //         obj_member.member_card_add_step2();
            //     },
            //     'json'
            // );
            $.ajax({
                type:"post",
                url:appUrl1 + "/api/bankcard/create",
                data:{api_token:key,bank_name:obj_member.id_number,bank_card:obj_member.card_number,true_name: obj_member.card_name },
                dataType:"json",
                success:function(result){
                    console.log(result)
                    if(result.status_code != 200){
                        errorTipsShow(result.message);
                        return false;
                    }
                    if ( result.status_code == 200)  {
                        if (withdrawals && withdrawals == 1) {
                            //如果是从提现过来的
                            setTimeout(function(){
                                location.href = WapSiteUrl + '/tmpl/member/withdrawals.html?withdrawals=' + withdrawals + '&pd=' + obj_member.pd +'&bankcard_id=' + result.data
                            },1000);
                            return false;
                        }
                        setTimeout("location.href = WapSiteUrl+'/tmpl/member/member_card.html'",1000);
                    }
                }
            })
        }
        // member_card_add_step2 : function () {
            // if (withdrawals && withdrawals == 1) {
            //     //如果是从提现过来的
            //     location.href = WapSiteUrl + '/tmpl/member/member_card_add_step2.html?card_name=' + obj_member.card_name + '&card_number=' + obj_member.card_number + '&id_number=' + encodeURI(obj_member.id_number) + '&withdrawals=' + obj_member.withdrawals + '&pd=' + obj_member.pd;
            //     return false;
            // }
            // location.href = WapSiteUrl + '/tmpl/member/member_card_add_step2.html?card_name=' + obj_member.card_name + '&card_number=' + obj_member.card_number + '&id_number=' + encodeURI(obj_member.id_number);
        // }
    };

    $('#nextform').click(function () {
        if ($.sValid()) {
            obj_member.init();
            if ( $('#id_number').val().length > 0) {
                obj_member.test_bind()
            }
        }
    });

    if (isWeiXin()){
        $('header').hide();
        $("body").css('padding', '0');
    }

    $('#infoBank').on('click','ul .bankItem',function (event) {
            event.stopPropagation();
            var bank_name =  $(this).attr('bank_name')
            $('#id_number').val(bank_name);
            $('#infoBank').hide()
             $('#boxModel').hide()
    })

    document.onclick = function(event){
        event = event || window.event;
        var aaa = event.target ? event.target : event.srcElement;
        if (aaa.id !== "id_number") {
            $('#infoBank').hide();
            $('#boxModel').hide()

        }
    }

    $('#selectBank').click(function () {
        $.ajax({
            type: 'get',
            url: appUrl1 + "/api/bankcard/list",
            data: {api_token:key},
            dataType: 'json',
            success: function(result) {
                var info = result;
                console.log(1111,info)
                var html = template('tpl',info);
                document.getElementById('infoBank').innerHTML = html;
                $('#infoBank').show()
                $('#boxModel').show()
            }
        });
    });

});
