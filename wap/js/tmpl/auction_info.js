/**
 * Created by ADKi on 2017/3/9 0009.
 */
$(function() {
    var auction_id = getQueryString("auction_id");
    $.ajax({
        url: ApiUrl + "/index.php?act=auction&op=auction_body",
        data: {auction_id: auction_id},
        type: "get",
        success: function(result) {
            $(".fixed-tab-pannel").html(result);
        }
    });
});