/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

var invite_code = getQueryString('invite_code');
var key = getCookie('key');
var url = location.href;

if (!invite_code && key) {
    requestUrl();
}

var resultUrl = !invite_code ? WapSiteUrl + '/tmpl/qucai/guess_detail.html' : WapSiteUrl + '/tmpl/qucai/guess_detail.html?invite_code=' + invite_code;

var shareData = {
    title: "土豪老板又任性啦，继一亿红包后，这次猜中就拿走",
    desc: "竞猜者参与作品估值竞猜，猜价最接近者即可以免费获得该作品。",
    link: resultUrl,
    imgUrl: WapSiteUrl + '/images/lm/guess_detail/boos_bg_share.png'
    // imgUrl: '../../images/lm/guess_detail/boos_bg_share.png'
};

wx_config(shareData);
//APP分享参数
appShareSet(shareData);
function requestUrl(){
    $.ajax({
        url: ApiUrl + '/index.php?act=member_qrcode',
        type: 'get',
        data: {key: key, url: url},
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            window.location.href = data.go_url;
        }
    });
}