$(function() {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }

    var cash = getQueryString('cash');
    $('#available_commis').html(cash);

    $.sValid.init({
        rules:{
            money:"required",
            card_number:"required",
            bank_name : 'required',
            bank_user : 'required',
            password : 'required'
        },
        messages:{
            money:"请填写金额!",
            card_number:"请填写卡号!",
            bank_name :"请填写银行名称!",
            bank_user :"请填写开户姓名",
            password : '请填写支付密码'
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });

    $('#checkbox').click(function(){
        if($('#checkbox').is(':checked')){
            $('#agree_protocol').val(1);
        }else{
            $('#agree_protocol').val(0);
        }
    });

    $('#all_commis').click(function(){
        $('#money').val(cash);
    });


    $('#nextform').click(function(){
        if($('#agree_protocol').val() == 0){
            return false;
        }
        if($.sValid()){
            var money = $('#money').val();
            var card_number = $('#card_number').val();
            var bank_name= $('#bank_name').val();
            var bank_user = $('#bank_user').val();
            var password= $('#password').val();
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=member_index&op=commis_cash_add",
                data:{key:key,money:money,card_number:card_number,bank_name:bank_name,bank_user:bank_user,password:password},
                dataType:'json',
                success:function(result){
                    if(result.code == 200){
                        $.sDialog({
                            skin:"block",
                            content:'已提交申请',
                            okBtn:false,
                            cancelBtn:false
                        });
                        setTimeout("location.href = WapSiteUrl+'/tmpl/member/member.html'",1000);
                    }else{
                        errorTipsShow('<p>' + result.datas.error + '</p>');
                    }
                }
            });
        }

    });
});
