/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    var key = getCookie('key'),
        log_id = getQueryString('log_id');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    $.ajax({
        url: ApiUrl + "/index.php?act=member_index&op=member_margin_detail",
        type: "get",
        data: {key: key,log_id:log_id},
        dataType: "json",
        success: function (result) {
            var data = result.datas;
            var html = template.render('member_margin_info_con', data);
            $("#member_margin_info").html(html);
        }
    });
});