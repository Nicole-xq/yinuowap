/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function(){
    function initPage(){
        $.ajax({
            url: ApiUrl + "/index.php?act=auction&op=index&special_id=0",
            type: 'get',
            dataType: 'json',
            success:function (result) {
                var data = result.datas;
                var html = '';
                $.each(data, function(k, v) {
                    $.each(v, function(kk, vv) {
                        switch (kk) {
                            case 'adv_list':
                            case 'home3':
                            case 'home5':
                            case 'home6':
                            case 'sem' :
                                $.each(vv.item, function(k3, v3) {
                                    vv.item[k3].url = buildUrl(v3.type, v3.data);
                                });
                                break;

                            case 'home1':
                                vv.url = buildUrl(vv.type, vv.data);
                                break;

                            case 'home2':
                            case 'home4':
                                vv.square_url = buildUrl(vv.square_type, vv.square_data);
                                vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                                vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                                break;

                        }
                        if (kk == 'sem') {
                            $("#main-container2").html(template.render(kk, vv));
                        } else if(kk == 'adv_list'){
                            $("#main-container1").html(template.render(kk, vv));
                        } else{
                            html += template.render(kk, vv);
                        }

                        return false;

                    });
                });
                $("#main-container").html(html);
                $.ajax({
                    url: ApiUrl + "/index.php?act=special&op=get_special_list",
                    type: 'get',
                    dataType: 'json',
                    success: function (result) {
                        var data = result.datas;
                        var html = '';
                        html += template.render('profit_activity', data);
                        $(".auction_recommend").before(html);
                        lmCuteDown();
                    }
                });
                lmCuteDown();
                new Swiper('.auction_banner', {
                    loop: true,
                    paginationClickable: true,
                    pagination: '.auction_banner_pagination',
                    autoplay : 5000
                });

                var $title=$('.pub_tab_title'),
                    $con=$('.pub_tab_wrapper'),
                    ind=0;

                $title.find('li').click(function () {
                    ind=$(this).index();
                    curChange(ind);
                });
                curChange(ind);
                function curChange(index){
                    $title.find('li').removeClass('active');
                    $title.find('li').eq(index).addClass('active');
                    $con.find('.pub_tab_con').hide();
                    $con.find('.pub_tab_con').eq(index).show();
                }
            }

        });
    }
    initPage();

    if(isWeiXin){
        var shareData = {
            title: "艺诺拍卖会场,一元起拍,惊喜不断!",
            desc: "全新拍卖玩法,首创保证金收益,年收益高达10%,能拍能赚,超值所得.",
            link: window.location.href,
            imgUrl: dataUrl+'/upload/logo.jpg',
        };
        wx_config(shareData);
    }
    //APP分享参数
    appShareSet(shareData);
});

