var page = pagesize;
var curpage = 1;
var hasMore = true;
var reset = true;

$(function(){
	var key = getCookie('key');

	function initPage(){
	    if (reset) {
	        curpage = 1;
	        hasMore = true;
	    }
        $('.loading').remove();
        if (!hasMore) {
            return false;
        }
        hasMore = false;
	    var state_type = $('#filtrate_ul').find('.selected').find('a').attr('data-state');
	    var orderKey = $('#order_key').val();

        $.ajax({
			type:'post',
			url:ApiUrl+"/index.php?act=vote_activity&op=get_activity_list&page="+page+"&curpage="+curpage,
			data:{key:key, state_type:state_type, order_key : orderKey},
			dataType:'json',
			success:function(result){
				curpage++;
                hasMore = result.datas.page.hasmore;

				var html = template.render('list-main-con', result.datas);
				if (reset) {
				    reset = false;
                    $("#vote-list").html(html);
                } else {
                    $("#vote-list").append(html);
                }
			}
		});

	}

    //初始化页面
    initPage();
    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            initPage();
        }
    });
});