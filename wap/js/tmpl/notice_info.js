$(function () {
    var key = getCookie('key');
    var message_id = getQueryString('message_id');
    var reset = true;
    if(!key){
        location.href = 'login.html';
    }
    //通知详情页
    $.ajax({
        type:'get',
        url:ApiUrl+'/index.php?act=member_chat&op=message_detail',
        data:{key:key,message_id:message_id},
        dataType:'json',
        success:function (result) {
            var data = result.datas;
            var html = template.render('noticeDetialScript', result);
            var html1 = template.render('qllKefuScript', result);
            var message_body = data.message_body;
            if(reset){
                $("#noticeDetial").html(html);
                $("#qllKefu").html(html1);
                $('.message_body').html(message_body);
                reset = false;
            }
            //$('.message_body a').hide();
            $('.qll_del').click(function () {
                // 通知删除
                $.ajax({
                    type:'get',
                    url:ApiUrl+'/index.php?act=member_chat&op=message_delete',
                    data:{key:key,message_id:message_id},
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            window.parent.location.href='chat_list.html';
                        }
                    }
                });
            });

        }
    });



});