/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function ($) {
    var special_id = getQueryString('special_id');
    var key = getCookie('key');
    // http://hjh.test.yinuovip.com/mobile/index.php?act=special&op=special_details    post: special_id = 108 & key=key
    function initPage() {
        $.ajax({
            url: ApiUrl + "/index.php?act=special&op=special_details",
            type: 'post',
            data: {special_id: special_id, key: key},
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                window._bd_share_config = {
                    "common": {
                        "bdText": data.special_info.special_name,
                        "bdPic": data.special_info.adv_image_paath,
                        "bdUrl": ""
                    },
                    "share": {
                        "bdSize": "24"
                    },
                    "selectShare": {
                        "bdSelectMiniList": ["tsina", "qzone", "weixin", "sqq"]
                    }
                };
                with (document)0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion=' + ~(-new Date() / 36e5)];
                var html = template.render('special_detail', data);
                $("#special_detail_html").html(html);
                var html1 = template.render('qllProfit', data);
                $("#qllProfitHtml").html(html1);

                lmCuteDown();

                //点击收藏／取消收藏拍品
                $('.special_list_con .collect').click(function () {
                    var auction_id = $(this).parents('li').attr('auction_id');
                    if ($(this).hasClass('active')) {
                        if (dropFavoriteAuction(auction_id)) {
                            $(this).removeClass('active');
                            // location.reload();
                        }
                    } else {
                        if (favoriteAuction(auction_id)) {
                            $(this).addClass('active');
                            // location.reload();
                        }
                    }
                    return false;
                });
                //点击提醒／取消提醒
                $('.special_list_con .remin').click(function () {
                    if (!key) {
                        checkLogin(0);
                        return;
                    }
                    var $self = $(this);
                    var auction_id = $(this).parents('li').attr('auction_id');
                    var special_id = $(this).parents('li').attr('special_id');
                    var relation_id = $(this).parents('li').attr('relation_id');
                    if ($(this).hasClass('active')) {
                        $.ajax({
                            type: 'POST',
                            url: ApiUrl + "/index.php?act=auction&op=cancel_remind",
                            data: {auction_id: auction_id, key: key, relation_id: relation_id},
                            dataType: "json",
                            success: function (result) {
                                if (result.datas == 0) {
                                    $self.removeClass('active');
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: ApiUrl + "/index.php?act=auction&op=check_mobile",
                            data: {key: key},
                            dataType: "json",
                            success: function (result) {
                                if (result.datas == 0) {
                                    setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                                } else {
                                    $.ajax({
                                        type: 'POST',
                                        url: ApiUrl + "/index.php?act=auction&op=set_remind",
                                        data: {auction_id: auction_id, key: key},
                                        dataType: "json",
                                        success: function (result) {
                                            if (result.datas == 0) {
                                                $self.addClass('active');
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                    return false;
                });

                //点击关注专场
                $('.collectSpecial').click(function () {
                    return false;
                    if (!key) {
                        checkLogin(0);
                        return;
                    }
                    var store_id = $(this).attr('store_id');
                    var $self=$(this);
                    if ($(this).hasClass('active')) {
                        $.ajax({
                            url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_del',
                            type: "post",
                            dataType: 'json',
                            data: {key: key, store_id: store_id},
                            success: function (result) {
                                if (result.code === 200) {
                                    $self.removeClass('active');
                                }
                            }
                        })
                    }
                    else{
                        $.ajax({
                            url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_add',
                            type: "post",
                            dataType: 'json',
                            data: {key: key, store_id: store_id},
                            success: function (result) {
                                if (result.code === 200) {
                                    $self.addClass('active');
                                }
                            }
                        })
                    }

                });


                //点击大图展示或者小图展示
                $('.special_list_setting .big').click(function () {
                    $('.special_list_con').toggleClass('big_list');
                    $(this).toggleClass('active');
                });
                //点击收缩或者展开
                $('.special_list_setting .stop').click(function () {
                    var flag = $('.special_list_top').hasClass('lm_hidden');
                    if (!flag) {
                        //如果是展开状态
                        $('.special_list_top,.special_list_info .state,.special_list_setting,.spjg,.bzjsy').addClass('lm_hidden');
                        $('.special_list_con_constr').addClass('active');
                        $('.special_list_con').css('padding-top', '10.5rem');
                        $('.special_list_setting_open').removeClass('lm_hidden');
                    }
                });
                $('.special_list_setting_open').click(function () {
                    var flag = $('.special_list_top').hasClass('lm_hidden');
                    if (flag) {
                        //如果是收缩状态
                        $('.special_list_top,.special_list_info .state,.special_list_setting,.spjg,.bzjsy').removeClass('lm_hidden');
                        $('.special_list_con_constr').removeClass('active');
                        $('.special_list_con').css('padding-top', '0');
                        $('.special_list_setting_open').addClass('lm_hidden');
                    }
                });
                if(isWeiXin) {
                    var shareData = {
                        title: data.special_info.special_name+'-艺诺拍卖',
                        desc: '保真品●保增值●保退换',
                        link: window.location.href,
                        imgUrl: data.special_info.wap_image_path,
                    };
                    wx_config(shareData);
                };
                //APP分享参数
                appShareSet(shareData);
                var title_html=data.special_info.special_name;
                $('.special_details_title').html(title_html);
                $(function () {
                    $.ajax({
                        // url:'http://www.yinuovip.com/mobile/index.php?act=other&op=get_number',
                        url:ApiUrl + '/index.php?act=other&op=get_number',
                        type:'GET',
                        dataType:'json',
                        success:function (result) {
                            var str= result.datas;
                            var array=str.split('');
                            var html='';
                            for(var i=0;i<array.length;i++){
                                html+='<span>'+array[i]+'</span>';
                            }
                            $('.member_num').html(html);
                        }
                    })
                });
                $('.qll_join_us').click(function(){
                    var key = getCookie('key');
                    if (!key) {
                        checkLogin(0);
                        return false;
                    }else{

                    }

                });
            }
        });
    }

    initPage();

});