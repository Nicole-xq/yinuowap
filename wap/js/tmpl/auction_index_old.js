$(function(){
    var headerClone = $('#header').clone();
    $(window).scroll(function(){
        if ($(window).scrollTop() <= $('#main-container1').height()) {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('transparent').removeClass('');
            headerClone.prependTo('.nctouch-home-top');
        } else {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('').removeClass('transparent');
            headerClone.prependTo('body');
        }
    });

    //$.ajax({
    //    url: ApiUrl + "/index.php?act=auction&op=class_name",
    //    type: 'get',
    //    dataType: 'json',
    //    success: function(result) {
    //        var html = '';
    //        $.each(result, function(k, v) {
    //            $.each(v, function(kk, vv) {
    //
    //                html += template.render('class_name', result);
    //                return false;
    //
    //            });
    //        });
    //
    //        $("#class_id").html(html);
    //
    //
    //    }
    //});
    loadAuction();

    if(isWeiXin){
            var shareData = {
                title: "艺诺拍卖会场,一元起拍,惊喜不断!",
                desc: "全新拍卖玩法,首创保证金收益,年收益高达10%,能拍能赚,超值所得.",
                link: window.location.href,
                imgUrl: dataUrl+'/upload/logo.jpg',
            };
            wx_config(shareData);
        }
})

function loadAuction(){
    $.ajax({
        url: ApiUrl + "/index.php?act=auction&op=index&special_id=0",
        type: 'get',
        dataType: 'json',
        success: function(result) {
            //$('title,h1').html(result.datas.special_desc);
            var data = result.datas;
            var html = '';
                $.each(data, function(k, v) {
                    $.each(v, function(kk, vv) {
                        switch (kk) {
                            case 'adv_list':
                            case 'home3':
                            case 'home5':
                            case 'home6':
                            case 'sem' :
                                $.each(vv.item, function(k3, v3) {
                                    vv.item[k3].url = buildUrl(v3.type, v3.data);
                                });
                                break;

                            case 'home1':
                                vv.url = buildUrl(vv.type, vv.data);
                                break;

                            case 'home2':
                            case 'home4':
                                vv.square_url = buildUrl(vv.square_type, vv.square_data);
                                vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                                vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                                break;

                        }
                        if (kk == 'sem') {
                            $("#main-container2").html(template.render(kk, vv));
                        } else if(kk == 'adv_list'){
                            $("#main-container1").html(template.render(kk, vv));
                        } else{
                            html += template.render(kk, vv);
                        }

                        return false;

                    });
                });


            $("#main-container").html(html);
            updateEndTime();

            $('.adv_list').each(function() {
                if ($(this).find('.item').length < 2) {
                    return;
                }

                Swipe(this, {
                    startSlide: 2,
                    speed: 400,
                    auto: 3000,
                    continuous: true,
                    disableScroll: false,
                    stopPropagation: false,
                    callback: function(index, elem) {},
                    transitionEnd: function(index, elem) {}
                });
            });


        }
    });


}

function init_get_list(mb_class_id) {
        $('#nav_'+mb_class_id).addClass('selected').parent().siblings().find('a').removeClass('selected');

        loadAuction(mb_class_id);

}

function get_special_list(id){
    $('#special_'+id).addClass('active').siblings().removeClass('active');
    $('#tab'+ id).addClass('active').siblings().removeClass('active');
}


function takeCount() {
    setTimeout("takeCount()", 1000);
    $(".count-time").each(function(){
        var obj = $(this);
        var tms = obj.attr("count_down");
        if (tms>0) {
            tms = parseInt(tms)-1;
            var days = Math.floor(tms / (1 * 60 * 60 * 24));
            var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
            var minutes = Math.floor(tms / (1 * 60)) % 60;
            var seconds = Math.floor(tms / 1) % 60;

            if (days < 0) days = 0;
            if (hours < 0) hours = 0;
            if (minutes < 0) minutes = 0;
            if (seconds < 0) seconds = 0;
            obj.find("[time_id='d']").html(days);
            obj.find("[time_id='h']").html(hours);
            obj.find("[time_id='m']").html(minutes);
            obj.find("[time_id='s']").html(seconds);
            obj.attr("count_down",tms);
        }
    });
}

//倒计时函数
function updateEndTime()
{
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function(i){

        var endDate =this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

        var endTime = endDate*1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if(lag > 0)
        {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("距结束&nbsp;<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
        }

    });
    setTimeout("updateEndTime()",1000);
}

