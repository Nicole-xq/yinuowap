/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    $.ajax({
        url: ApiUrl + "/index.php?act=member_index&op=member_margin_info",
        type: "get",
        data: {key: key},
        dataType: "json",
        success: function (result) {
            var data = result.datas;
            var html = template.render('member_margin_top_con', data);
            $("#member_margin_top").html(html);
        }
    });

    /**
     * dis_commis_state:0 未结算 1 已结算 2 全部
     * @type {{key: *, page: number, curpage: number, reset: boolean, dis_commis_state: number}}
     */
    var param = {key: key, page: 10, curpage: 1, reset: true, dis_commis_state: 2},
        hasmore = true;

    function initPage(current) {
        if (param.reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + "/index.php?act=member_index&op=member_margin_list",
            type: "get",
            data: param,
            dataType: "json",
            success: function (result) {
                param.curpage++;
                hasmore = result.hasmore;
                var data = result.datas;
                if (parseInt(current) === 1) {
                    data.current = 1;
                }
                var html = template.render('member_margin_list_con', data);
                if (param.reset) {
                    param.reset = false;
                    $("#member_margin_list").html(html);
                } else {
                    $("#member_margin_list").append(html);
                }
                // 对比去重复
                var res = [], seen;
                $('#member_margin_list .member_margin_select_date').each(function (i, e) {
                    if (seen === $(e).text() || res.indexOf($(e).text()) != -1) {
                        $(e).remove();
                    } else {
                        res.push($(e).text())
                    }
                    seen = $(e).text();
                });
            }
        });
    }

    initPage();
    // 下拉加载
    $(window).scroll(function () {
        var
            dom = $('.member_margin_select_head .pub_mask_select_val'),
            array = dom.data('data').split(','),
            ind = array.indexOf(dom.text());
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            if (parseInt(ind) === 1) {
                initPage(ind);
            } else {
                initPage();
            }
        }
    });
    // 调用公共选择
    pub_mask_select(function (i, e) {
        var array = i.data('data').split(','),
            ind = array.indexOf(i.text()), index = 0;
        switch (ind) {
            case 0:
                index = 2;
                break;
            case 1:
                index = 0;
                break;
            case 2:
                index = 1;
                break;
            default:
                index = 0;
        }
        param.dis_commis_state = index;
        param.curpage = 1;
        param.reset = true;
        if (parseInt(ind) === 1) {
            initPage(ind);
        } else {
            initPage();
        }
    });
    var data = new Date(),
        year = data.getFullYear(),
        month = data.getMonth() + 1;

    var dataPickerVal = [year, month, '04', '9', '34'],
        defaultDataPickerVal = [year, month, '04', '9', '34'];
    // 日期格式话
    $("#datetime-picker").datetimePicker({
        toolbarTemplate: '<header class="bar bar-nav">\
        <button class="button button-link pull-right reset-picker">重置</button>\
        <button class="button button-link pull-right close-picker">确定</button>\
        <h1 class="title">请选择</h1>\
        </header>',
        value: dataPickerVal,
        onOpen: function (e) {
            $('.reset-picker').click(function(){
                e.value = defaultDataPickerVal;
                $("#datetime-picker").picker("close");
                delete param.commission_time;
                param.curpage = 1;
                param.reset = true;
                initPage();
            });
            $('.close-picker').click(function(){
                var val = e.value[0] + '-' + e.value[1];
                param.commission_time = val;
                param.curpage = 1;
                param.reset = true;
                initPage();
            });
        },
        onClose: function (obj) {
            // var val = obj.value[0] + '-' + obj.value[1];
            // param.commission_time = val;
            // param.curpage = 1;
            // param.reset = true;
            // initPage();
        }
    });

});