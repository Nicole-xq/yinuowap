var page = pagesize;
var curpage = 1;
var hasmore = true;
var footer = false;
var keyword = decodeURIComponent(getQueryString('keyword'));
var key = getQueryString('key');
var order = getQueryString('order');
var order2 = 1;
var order3 = 1;

$(function(){

    $('#header-nav').click(function(){
        if ($('#keyword').val() !== '') {
            window.location.href = buildUrl('auction',$('#keyword').val());
        }
    });
    $('#keyword').
    if (keyword != '') {
    	$('#keyword').html(keyword);
    }
    template.helper('$buildUrl',buildUrl);

    $('#nav_ul').find('a').click(function(){
        $(this).addClass('actived').siblings().removeClass('actived');
    });
    get_list();
    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            get_list();
        }
    });
    get_search();

});

function get_list() {
    $('.loading').remove();
    if (!hasmore) {
        return false;
    }
    hasmore = false;
    param = {};
    param.page = page;
    param.curpage = curpage;
    if (keyword != '') {
        param.keyword = keyword;
    }
    if (key != '') {
        param.key = key;
    }
    if (order != '') {
        param.order = order;
    }
    $.getJSON(ApiUrl + '/index.php?act=auction&op=auction_list' + window.location.search.replace('?','&'), param, function(result){
    	if(!result) {
    		result = [];
    		result.datas = [];
            result.datas.search_list = [];
    		result.datas.auction_list = [];
    	}
        var data = result.datas;

        window._bd_share_config={
            "common":{
                "bdText":"艺诺拍卖",
                "bdPic":"",
                "bdUrl":""
            },
            "share":{
                "bdSize":"24"
            },
            "selectShare":{
                "bdSelectMiniList":["tsina","qzone","weixin","sqq"]
            }
        };
        with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];

        $('.loading').remove();
        curpage++;
        var html = template.render('home_body', result.datas);
        $("#auction_list").append(html);
        hasmore = result.hasmore;

        for(i=0; i<data.auction_list.length;i++){
            if(data.auction_list[i].is_kaipai == 1){
                takeCount(data.auction_list[i].auction_id);
            }
        }

    });

}

function get_search() {
    $.getJSON(ApiUrl + '/index.php?act=auction&op=auction_list', function(result){
        var data = result.datas;
        var html = template.render('search_list', result.datas);
        $("#search_list_html").append(html);

    });
}

function init_get_list(o, k) {
    if(k == 2){
        if(order2 == 1) {
            o = order2 = 2;
        }else{
            o = order2 = 1;
        }
    }else if(k == 3){
        if(order3 == 1) {
            o = order3 = 2;
        }else{
            o = order3 = 1;
        }
    }
    order = o;
    key = k;
    curpage = 1;
    hasmore = true;
    $("#auction_list").html('');
    get_list();
}

function takeCount(auction_id) {
    $("#auction_" + auction_id).each(function(){
        var obj = $(this);
        var tms = obj.attr("count_down");
        if (tms>0) {
            tms = parseInt(tms)-1;
            var days = Math.floor(tms / (1 * 60 * 60 * 24));
            var hours = Math.floor(tms / (1 * 60 * 60)) % 24;
            var minutes = Math.floor(tms / (1 * 60)) % 60;
            var seconds = Math.floor(tms / 1) % 60;

            if (days < 0) days = 0;
            if (hours < 0) hours = 0;
            if (minutes < 0) minutes = 0;
            if (seconds < 0) seconds = 0;
            obj.find("[time_id='d']").html(days);
            obj.find("[time_id='h']").html(hours);
            obj.find("[time_id='m']").html(minutes);
            obj.find("[time_id='s']").html(seconds);
            obj.attr("count_down",tms);
        }
    });
    setTimeout("takeCount("+ auction_id + ")", 1000);
}