/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
var  echarts_color = ['#ea5c2c', '#78c05d', '#3399cc', '#f3a18b'] , echarts_data = [];
option = {
    baseOption: {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                center: ['50%', '50%'],
                radius: ['100%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '12',
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: echarts_data ,
                color: echarts_color,
            }
        ]
    },
};


$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    $.ajax({
        type: 'get',
        url: ApiUrl + "/index.php?act=member_partner&op=index",
        data: {key: key},
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            data.echarts_color = echarts_color;
            var html = template.render('member_partner_con', data);
            $('#member_partner').html(html);
            //饼图
            for(var key in data.commission_list){
                var array  = {}
                array.name  = data.commission_list[key].type_name;
                array.value  = data.commission_list[key].amount;
                echarts_data.push(array);
            }
            var myChart = echarts.init(document.getElementById('main'));
            myChart.title = '环形图';
            myChart.setOption(option);
        }
    });
});