/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    var hasmore = true;
    var param = {keyword: '', key: key, curpage: 1, page: 10, reset: true},
        keyword = '';

    function initPage() {
        if (param.reset) {
            param.curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + "/index.php?act=member_partner&op=partner_list",
            type: "get",
            data: param,
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                param.curpage++;
                hasmore = result.hasmore;
                var html = template.render('member_partner_list_con', data);
                var html_list = template.render('member_partner_list_list_con', data);
                if (param.reset) {
                    param.reset = false;
                    $("#member_partner_list").html(html);
                    $("#member_partner_list_list").html(html_list);
                } else {
                    $("#member_partner_list_list").append(html_list);
                }
                //元素去重复
                var res = [], seen;
                $('#member_partner_list_list .member_partner_list_con_date').each(function (i, e) {
                    if (seen === $(e).text() || res.indexOf($(e).text()) != -1) {
                        $(e).remove();
                    } else {
                        res.push($(e).text())
                    }
                    seen = $(e).text();
                });
                $.animationLeft({
                    valve: '#member_partner_search',
                    wrapper: '#member_partner_search_wrapper',
                    scroll: ''
                });
                $('.public_search_back').click(function () {
                    $('#member_partner_search_wrapper').removeClass('hide,left').addClass('right')
                });
            }
        });
    }

    initPage();
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });
    var previous = 0;
    $('#public_search_inp').on('input', function () {
        var now = +new Date();
        //节流
        if (now - previous > 2000) {
            previous = now;
            search();
        }
    });
    
    function search() {
        keyword = $('#public_search_inp').val();
        $.ajax({
            url: ApiUrl + "/index.php?act=member_partner&op=search_partner_list",
            type: "get",
            data: {keyword: keyword, key: key},
            dataType: "json",
            success: function (result) {
                var html = template.render('public_search_result_con', result);
                $(".public_search_result .public_search_result_panpel").html(html);
                $(".public_search_result .public_search_result_panpel li a").each(function (i, e) {
                    $(e).html($(e).text().replace(keyword, '<span style="color:red;">' + keyword + '</span>'))
                });
                $(".public_search_result .public_search_result_panpel li").each(function (i, e) {
                    if($(e).is(":empty")||$(e).find('a').is(":empty")){
                        $(e).remove();
                    }
                })
            }
        });
    }
});