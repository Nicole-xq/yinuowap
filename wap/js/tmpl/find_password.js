/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    $.sValid.init({//注册验证
        rules: {
            usermobile: {
                required: true,
                mobile: true
            },
            captcha: "required",
            password: {
                required: true,
                maxlength:10,
                minlength:6
            }
        },
        messages: {
            usermobile: {
                required: "请填写手机号！",
                mobile: "手机号码不正确"
            },
            captcha: "请填写验证码",
            password:{
                required: "密码必填!",
                maxlength:"密码格式错误!",
                minlength:"密码格式错误!"
            }
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                $.map(eId, function (idx, item) {
                    $('#' + idx).parents('.login_form_input_panpel').addClass('error');
                    $('#' + idx).attr('placeholder', eMsg[idx + '_required']);
                });
            } else {
                $('#' + eId).parents('.login_form_input_panpel').removeClass('error');
            }
        }
    });

    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state', function(result){
        var ua = navigator.userAgent.toLowerCase();
        var allow_login = 0;
        if (result.datas.pc_qq == '1') {
            allow_login = 1;
            $('.qq').show();
        }
        if (result.datas.pc_sn == '1') {
            allow_login = 1;
            $('.weibo').show();
        }
        if ((ua.indexOf('micromessenger') > -1) && result.datas.connect_wap_wx == '1') {
            allow_login = 1;
            $('.wx').show();
        }
        if (allow_login) {
            $('.quick_login').show();
        }
    });

    //点击获取验证码
    var getCodeFlag = true;
    $('#getCode').click(function () {
        var val = $('#usermobile').val();
        var validata=/^(1{1})+\d{10}$/.test(val);
        if(!getCodeFlag||!validata){
            if(!validata){
                $('#usermobile').parents('.login_form_input_panpel').addClass('error');
                $('#usermobile').attr('placeholder','请填写手机号!');
            }
            return false;
        }
        getCodeFlag = false;

        setTimeout(function(){
            getCodeFlag = true;
        },3000);
        var self = $(this),
            time = 60;

        $.getJSON(ApiUrl+'/index.php?act=connect&op=get_sms_captcha', {type:3,phone:val,sec_val:'',sec_key:''}, function(result){
            if(!result.datas.error){
                lm_errorTipsShow('<p>发送短信中....</p>');
                var times_Countdown = setInterval(function(){
                    time--;
                    if (time == 0) {
                        self.html('<i class="iconfont icon-shouji"></i>再次获取');
                        clearInterval(times_Countdown);
                    } else {
                        self.html('<span style="color:#8c8c8c;">'+time+'秒后再次获取</span>')
                    }
                },1000);
            }else{
                lm_errorTipsShow('<p>' + result.datas.error + '</p>');
            }
        });
    });

    //点击完成
    $('#completebtn').click(function () {
        var password = $("#password").val(),
            mobile = $("#usermobile").val(),
            captcha = $("#captcha").val();
        if ($.sValid()) {
            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=connect&op=find_password",
                data: {phone: mobile, captcha: captcha, password: password, client: 'wap'},
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        addCookie('username', result.datas.username);
                        addCookie('key', result.datas.key);
                        lm_errorTipsShow('<p>请求中....</p>');
                        location.href = WapSiteUrl + '/tmpl/member/member.html';
                    } else {
                        lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                    }
                }
            });
        }
        return false;
    });
});