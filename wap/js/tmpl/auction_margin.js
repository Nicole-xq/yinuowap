/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var key = getCookie('key');
    var address_id, vat_hash, voucher, pd_pay, password, fcode = '', rcb_pay, payment_code;
    var pay_name='online'; //支付方式
    var agreement = true; //是否勾选认同协议
    var auction_id = getQueryString('auction_id');
    var message = {};
    var order_sn = '';
    var once_notice;
    var is_new_order = 1;
    var buyer_id = getQueryString('buyer_id');
    var tl_check = false;
    var orderId;
    var whkAuctionId;
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return false;
    }

    $(document).on("click", '#whkEditMargin', function () {
        // init_page();
        var whkMarginId = getCookie('whkMarginId');
        console.log(whkMarginId)
        $.ajax({
            type:"post",
            url:ApiUrl+"/index.php?act=member_auction_order&op=order_cancel",
            data:{order_id:whkMarginId,key:key},
            dataType:"json",
            success:function(result){
                console.log(result)
                if(result.code == 200){
                    // location.reload();
                    // reset = true;
                    // initPage();
                    $('.whkAuction').css('display','block');
                    $('.whkMarginP').css('display','none');
                    location.reload();
                } else {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                }
            }
        });
    });
    setInterval(function(){
        if(tl_check){
            $.ajax({
                type:'get',
                url:ApiUrl + '/index.php?act=member_buy_auction&op=check_margin_order',
                data: {key: key, order_sn: order_sn,},
                dataType:'json',
                success:function(res){
                    console.log(result)
                    if(res.datas == 1){
                        window.location.href = WapSiteUrl + '/tmpl/member/auction_order_list.html';
                    }
                }
            })
        }
    },1000);
    function init_page() {
        $.ajax({
            type: 'get',
            url: ApiUrl + '/index.php?act=member_buy_auction&op=show_margin_order',
            data: {key: key, auction_id: auction_id, address_id: address_id,},
            dataType: 'json',
            success: function (result) {
                console.log(result)
                var whkAuctionId = result.datas.auction_info.auction_id;
                console.log(whkAuctionId)
                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin: "red",
                        content: result.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                    return false;
                }
                if(result.datas.e){
                    window.location.href = WapSiteUrl + '/tmpl/member/auction_order_list.html';
                }
                var margin_num_html = template.render('margin_num_html', result.datas.auction_info);
                $("#margin_num").html(margin_num_html);
                if(result.datas.margin_order != ''){

                    $('.whkAuction').css('display','none');
                    $('.whkMarginP').css('display','block');

                    is_new_order = 0;
                    order_sn = result.datas.margin_order.order_sn;
                    $(".buy_num").val(Number(result.datas.margin_order.margin_amount));
                    // $(".buy_num").attr('readonly',true);
                    once_notice = result.datas.auction_info.auction_type;
                    //出价，减
                    $(".minus").click(function () {

                        // $(document).tipModal({
                        //     html: '如需调整金额,请取消本订单后重新下单!',
                        // });

                    });
                    //出价
                    $(".add").click(function () {

                        // $(document).tipModal({
                        //     html: '如需调整金额,请<span id="whkGoorderList" style="color: #fabf13">取消</span>本订单后重新下单!<br/>',
                        // });
                    });
                    $(document).on("click", '#whkGoorderList', function () {
                        window.location.href = WapSiteUrl + '/tmpl/member/margin_order_list.html';
                    })
                    $('#pay_msg').html('已锁定!如需修改,请至个人中心保证金页面取消订单后重新下单!');
                    $('.chose_list .icon-yuandian').removeClass('icon-yuandian').addClass('icon-yuand');
                    if(result.datas.margin_order.payment_code == 'underline'){
                        $('#underline').find('i').removeClass('icon-yuand').addClass('icon-yuandian');
                    }else if(result.datas.margin_order.payment_code == 'tl_pay'){
                        $('#tl_pay').find('i').removeClass('icon-yuand').addClass('icon-yuandian');
                    }else{
                        $('#online').find('i').removeClass('icon-yuand').addClass('icon-yuandian');
                    }
                    $('.chose_list').removeClass('active');
                    $(this).addClass('active');
                    pay_name = result.datas.margin_order.payment_code;
                }else{
                    $(".buy_num").val(Number(result.datas.auction_info.auction_bond));
                    once_notice = result.datas.auction_info.auction_type;
                    //出价，减
                    $(".minus").click(function () {
                        var buynum = $(".buy_num").val();
                        if (buynum >= result.datas.auction_info.auction_bond + 1) {
                            $(".buy_num").val(parseInt(buynum - 1));
                        }
                    });
                    //出价
                    $(".add").click(function () {
                        var buynum = parseInt($(".buy_num").val());
                        var maxNum = 1000000000;
                        if (buynum < 999999999) {
                            $(".buy_num").val(parseInt(buynum + 1));
                        } else {
                            $(".buy_num").val(parseInt(maxNum));
                        }
                    });
                    //选择支付方式
                    $('.chose_list').click(function () {
                        $('.chose_list .icon-yuandian').removeClass('icon-yuandian').addClass('icon-yuand');
                        $(this).find('i').removeClass('icon-yuand').addClass('icon-yuandian');
                        $('.chose_list').removeClass('active');
                        $(this).addClass('active');
                        pay_name = $(this).attr('pay_name');
                        return false;
                    });
                }


                //agreement  点击是否同意协议
                $('.agreement .agreement_btn').click(function(){
                    $(this).toggleClass('active');
                    if($(this).hasClass('active')){
                        agreement=true;
                    }else{
                        agreement=false;
                    }
                    // return false;
                })
                //禁止输入非num类型
                $('input[type=tel]').keypress(function(e){
                    keyPress();
                });

                function keyPress() { var keyCode = event.keyCode; if (keyCode >= 48 && keyCode <= 57) { event.returnValue = true; }else { event.returnValue = false; } }

                password = '';
            }
        });
    }
    rcb_pay = 0;
    pd_pay = 0;
    // 初始化
    init_page();
    //

    // 支付，创建订单
    var buy_step2 = 0;
    $('#ToBuyStep2').click(function(){
        // alert('totototot')
        if(once_notice == '1'){

            $.ajax({
                type:'post',
                url:ApiUrl+'/index.php?act=member_buy_auction&op=check_auction_type',
                data:{
                    key:key,
                    auction_id:auction_id,
                },
                dataType:'json',
                success: function(result){
                    console.log(result)
                    once_notice = '0';
                    if(result.datas != '0'){
                        $(document).tipModal({
                            html: '您已经参与过拍品,本次提交不再赠送收益!',
                        });
                        return false;
                    }else{
                        create_order();
                        return false;
                    }
                }
            });
        }else{
            if(order_sn != ''){
                if(pay_name == 'tl_pay') {
                    if (!agreement) {
                        $(document).tipModal({
                            html: '请同意艺诺竞拍协议！',
                        });
                        return false;
                    }
                    // bootFn();
                    $('#qrcode').LMqcrode({
                        ecLevel: 'H',
                        size: 100,
                        mSize: 20,
                        imageId: 'lmbuffer',
                        text: order_sn,
                        label: "艺诺",
                    });
                    $('#tl_qrcode').show();
                    tl_check = true;
                    $('.boot_mask').click(function(){
                        $('#tl_qrcode').hide();
                    })
                    return false;
                }
                if(pay_name == 'underline'){
                    window.location.href = WapSiteUrl + '/tmpl/member/underline_pay_auction.html?pay_sn='+order_sn + '&type=margin';
                    return false;
                }
                toPay(order_sn,'member_buy_auction','pay', 'margin');

                $(document).on('click','#wrapperUsePDpy .power',function(){

                    $(this).parent('label').click();
                    return false;
                })
                   // $('#wrapperUsePDpy .power').click(function () {
                   //     $(this).parent('label').click();
                   //     return false;
                   // })
            }else{
                create_order();
            }
            return false;
        }


    });
    function create_order(){

        var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
        var lm_val=$('#buy_num').val().trim();

        // if(!lm_val||pattern.test(lm_val)||isNaN(lm_val)){
        //     // $.sDialog({
        //     //     skin:"red",
        //     //     content:'请输入正确的金额！',
        //     //     okBtn:false,
        //     //     cancelBtn:false
        //     // });
        //     $(document).tipModal({
        //         html: '请输入正确的金额!',
        //     });
        //     return false;
        // }
       if(!agreement){
           // $.sDialog({
           //     skin:"red",
           //     content:'请同意艺诺竞拍协议！',
           //     okBtn:false,
           //     cancelBtn:false
           // });
           $(document).tipModal({
                html: '请同意艺诺竞拍协议！',
            });
           return false;
       }
        if (buy_step2) {
            // $.sDialog({
            //     skin:"red",
            //     content:'订单正在处理中，请勿重复点击！',
            //     okBtn:false,
            //     cancelBtn:false
            // });
            $(document).tipModal({
                html: '订单正在处理中，请勿重复点击！',
            });
            return false;
        }
        buy_step2 = 0;

        var margin_amount=parseInt($('#buy_num').val());
        var margin_amount_min=parseInt($('#buy_num').attr('data-min'));

        if(margin_amount<margin_amount_min){
            // $.sDialog({
            //     skin:"red",
            //     content:'保证金最低金额为'+margin_amount_min+'元',
            //     okBtn:false,
            //     cancelBtn:false
            // });
            $(document).tipModal({
                html: '保证金最低金额为'+margin_amount_min+'元',
            });
            $('#buy_num').val(margin_amount_min.toFixed(2));
            return false;
        }

        alert(key)
        alert(auction_id)

        alert(pay_name)



        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_buy_auction&op=create_margin_order',
            data:{
                key:key,
                auction_id:auction_id,
                // address_id:address_id,
                pay_name:pay_name,
                is_margin:1,
                // is_anonymous:0,
                // pd_pay:pd_pay,
                // password:password,
                // rcb_pay:rcb_pay,
                // margin_amount:margin_amount
            },
            dataType:'json',
            success: function(result){
                console.log(result)

                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                    // location.reload();
                    buy_step2 = 0;
                    return false;
                }


                if(result.datas.payment_code == 'tl_pay'){
                    order_sn = result.datas.pay_sn;
                    $('#qrcode').LMqcrode({
                        ecLevel: 'H',
                        size: 100,
                        mSize: 20,
                        imageId: 'lmbuffer',
                        text: order_sn,
                        label: "艺诺",
                    });
                    $('#tl_qrcode').show();
                    tl_check = true;
                    $('.boot_mask').click(function(){
                        $('#tl_qrcode').hide();
                    })
                    return false;
                }else if(result.datas.payment_code == 'underline'){
                    window.location.href = WapSiteUrl + '/tmpl/member/underline_pay_auction.html?pay_sn='+result.datas.pay_sn + '&type=margin';
                } else {
                    init_page();
                    toPay(result.datas.pay_sn,'member_buy_auction','pay', 'margin');
                    // order_sn = result.datas.pay_sn;
                    // pay_name = result.datas.payment_code;

                    // $(document).on('click','#wrapperUsePDpy .power',function(){
                    //
                    //     $(this).parent('label').click();
                    //     return false;
                    // })
                   $('#wrapperUsePDpy .power').click(function () {
                       $(this).parent('label').click();
                       return false;
                   })
                }
                if(result.code == 200){
                    $('.whkAuction').css('display','none');
                    $('.whkMarginP').css('display','block');
                }

            }
        });
}

    // type=1,2,3分别对应点击次数，点赞次数，分享次数
    // id 为该引导页的编号
    // thumbs_up_flag   fasle : 今天没有点赞，true：已点赞
    //share_state：1不显示 2显示 是否显示分享和点赞
    function bootFn() {
        // $.ajax({
        //     url: ApiUrl + "/index.php?act=boot_page&op=getBootPage",
        //     type: 'get',
        //     dataType: 'json',
        //     async: false,
        //     success: function (result) {
        //         var data = result.datas,
        //             boot_id = data.id;
        //         $('.boot_modal').remove();
        //         if(!data.boot_image)return false;
                var html = '';
        //         var clasFlag = data.share_state == 1 ? 'lm_hidden' : '';
        //         var thumbs_up_flag = !data.thumbs_up_flag ? '' : 'active';

                html += '<div class="boot_modal">\
                    <div class="boot_mask"></div>\
                    <div class="boot_con" id="qrcode">\
                    <img  class="img" src="http://imgsrc.baidu.com/image/c0%3Dshijue1%2C0%2C0%2C294%2C40/sign=2738de2617950a7b6138468762b808ac/03087bf40ad162d999e3e70c1bdfa9ec8a13cd95.jpg" alt="通联支付">\
                    <div class="nativeShare_close"><img  class="img" src="/wap/images/lm/icon/nativeShare_close.png" alt=""></div>\
                    </div>\
                    </div>';
                $('body').addClass('pub_overFlow').append(html);
                $('html').addClass('pub_overFlow');
                $('body,html').height($(window).height());
                // var domHeight = $('.boot_con').outerHeight()<$(window).height()?$(window).height():$('.boot_con').outerHeight()+100;
                $('.boot_mask,.nativeShare_close').click(function () {
                    $('.boot_modal').remove();
                    $('body,html').removeClass('pub_overFlow');
                    $('body,html').css('height','auto');
                });
                $('.nativeShare_mask').click(function () {
                            $('#nativeShare').remove();
                        });
                // var browerName  = getBrowerVersion();
                // if(browerName=='MobileQQ' || browerName=='Baidu' || browerName=='QQBrowser' || browerName=='UC'){
                //     //点击分享 , 仅支持双端 uc 和 qq浏览器
                //     $('.boot_modal .boot_share').click(function () {
                //         //判断当为微信浏览器 || safari || 手机谷歌浏览器
                //         if(navigator.userAgent.indexOf('MicroMessenger') > -1 ){
                //             return false;
                //         }
                //         //注释，这里如果不是qq浏览器或者uc浏览器是无法通过此代码分享成功的，所以此处计算为点击次数，并非真正成功的数据
                //         $.post(ApiUrl + "/index.php?act=boot_page&op=recordNum&id=" + boot_id + "&type=3");
                //         //发送分享次数
                //         $('#nativeShare').remove();
                        $('body').append('<div id="nativeShare">' +
                            // '<div class="list_con">' +
                            // '<div class="label">分享到</div>' +
                            // '<div class="list clearfix">' +
                            // '<span data-app="weixin" class="nativeShare weixin" ><i></i>微信好友</span>' +
                            // '<span data-app="weixinFriend" class="nativeShare weixin_timeline"><i></i>朋友圈</span>' +
                            // '<span data-app="QQ" class="nativeShare qq"><i></i>QQ好友</span>' +
                            // '<span data-app="sinaWeibo" class="nativeShare weibo"><i></i>新浪微博</span>' +
                            // '</div>' +
                            // '</div>' +
                            '<div class="nativeShare_mask"></div>' +
                            '</div>');

                        $('.nativeShare_mask').click(function () {
                            $('#nativeShare').remove();
                        });
                //         // 先创建一个实例
                //         var nativeShare = new NativeShare();
                //         var shareData = {
                //             icon:  data.boot_image,// 图片
                //             link: data.wap_link,// 分享的网页链接
                //             title: data.title,// 标题
                //             desc: data.share_desc,// 描述
                //             from: data.wap_link, // 来源
                //             // 不要过于依赖以下两个回调，很多浏览器是不支持的
                //             success: function() {
                //                 // alert('success')
                //             },
                //             fail: function() {
                //                 // alert('fail')
                //             }
                //         }
                //         nativeShare.setShareData(shareData)
                //
                //         function call(command) {
                //             try {
                //                 nativeShare.call(command)
                //             } catch (err) {
                //                 // 如果不支持，你可以在这里做降级处理
                //                 // alert(err.message)
                //             }
                //         }
                //
                //         $('#nativeShare .weixin').click(function(){
                //             call('wechatFriend');
                //         });
                //         $('#nativeShare .weixin_timeline').click(function(){
                //             call('wechatTimeline');
                //         });
                //         $('#nativeShare .qq').click(function(){
                //             call('qqFriend');
                //         });
                //         $('#nativeShare .weibo').click(function(){
                //             call('weibo');
                //         });
                //         return false;
                //     });
                // }else{
                //
                //     $('.boot_setting').append('<img style="display: none;" class="suport_icon" src="images/lm/icon/suport_icon.png"/>');
                //     $('.suport_icon').click(function () {
                //         $('.suport_icon').hide();
                //     });
                //     $('.boot_share').click(function(){
                //         $('.suport_icon').toggle();
                //     })
                // }


            // }
        // });
    }



});

