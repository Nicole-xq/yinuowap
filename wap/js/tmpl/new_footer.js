var key = getCookie('key');
$(function () {
    $.ajax({
        url: ApiUrl + "/index.php?act=new_footer",
        type: 'get',
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            var item = data.item_info.item_data.item;
            var item_html = '';
            var $parent = $('<div class="lm_footer" >\
                </div>\
                ')
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    if (item[i].type != 'url') {
                        item_html += '<li>\
                            <a href="' + buildUrl(item[i].type, item[i].data) + '">\
                            <i class="iconfont"></i>\
                            <span class="text">' + item[i].title + '</span>\
                            </a>\
                            </li>';
                    } else {
                        item_html += '<li>\
                            <a href="' + item[i].data + '">\
                            <i class="iconfont"></i>\
                            <span class="text">' + item[i].title + '</span>\
                            </a>\
                            </li>';
                    }
                }

                // if(window.location.origin == "http://192.168.97.164" ) {
                //     alert(000)
                //     console.log(window.location)
                //
                //     $(".lm_footer").hide();
                //     return
                // }
                var html = '<div class="lm_footer" style="padding: 0!important;"><ul>'
                    + item_html
                    + '</ul></div>';
                var browser = {
                    versions: function () {
                        var u = navigator.userAgent, app = navigator.appVersion;
                        return {         //移动终端浏览器版本信息
                            trident: u.indexOf('Trident') > -1, //IE内核
                            presto: u.indexOf('Presto') > -1, //opera内核
                            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
                            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
                            iPad: u.indexOf('iPad') > -1, //是否iPad
                            webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
                        };
                    }()
                }

                if (browser.versions.mobile) {//判断是否是移动设备打开
                    var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
                    if (ua.match(/MicroMessenger/i) == "micromessenger") {
                        //在微信中打开
                        // alert("wexin");
                    }
                    if (ua.match(/WeiBo/i) == "weibo") {
                        //在新浪微博客户端打开
                        // alert("weibo");
                    }
                    if (ua.match(/QQ/i) == "qq") {
                        // alert("QQ");
                    }
                    if (/alipay/ig.test(ua)) {
                        // alert("支付宝");
                    }
                    if (browser.versions.ios) {
                        // alert("IOS浏览器打开"); //是否在IOS浏览器打开
                    }
                    if (browser.versions.android) {
                        // alert("安卓浏览器打开") //是否在安卓浏览器打开
                    }
                    if (browser.versions.Safari) {
                        // alert("Safari") //

                    }
                } else {
                    // alert("PC");
                }

                if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1) {
                    return
                }

                var hideKey = getCookie('hideKey');
                if (hideKey) {
                    return
                }
                $('body').append(html);
                setUrlCurActive();
            }
        }
    });
    $('#logoutbtn').click(function () {
        var username = getCookie('username');
        var key = getCookie('key');
        var client = 'wap';
        $.ajax({
            type: 'get',
            url: ApiUrl + '/index.php?act=logout',
            data: {username: username, key: key, client: client},
            success: function (result) {
                if (result) {
                    delCookie('username');
                    delCookie('key');
                    //lm 17.5.19
                    delCookie('cart_count');
                    delCookie('invite_code');
                   if (navigator.userAgent.indexOf('Appyinuo') !== -1 || navigator.userAgent.indexOf('YinuoAPP') !== -1) {

                       if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
                           window.webkit.messageHandlers.loginOut.postMessage("loginOut");
                           location.href = WapSiteUrl + "/tmpl/member/member.html";
                           return
                       } else{
                           jsObj.loginOut();
                           location.href = WapSiteUrl + "/tmpl/member/member.html";
                           return
                       }

                    }
                    location.href = WapSiteUrl+ '/index.html';
                }
            }
        });
    });
    // 消息列表页判断有否登陆
    if (key == '' || key == undefined) {
        $(".chat_list").attr('href', WapSiteUrl + '/tmpl/member/login.html');
    }

    function setUrlCurActive() {
        $('.lm_footer li a').each(function (i, e) {
            if ($(this).attr('href').indexOf(location.pathname) != -1) {
                $('.lm_footer li').removeClass('active');
                $(this).parents('li').addClass('active');
            }
        });
        if (location.pathname == '/wap/') {
            $('.lm_footer li').removeClass('active');
            $('.lm_footer li').eq(1).addClass('active');
        }
    }

    $('head').append('<link rel="stylesheet" type="text/css" href="/wap/css/lm/footer.css">');

    // if(top.location != location || (window.location.ancestorOrigins[0] == "http://192.168.97.164") ) {
    //     alert(000)
    //     console.log(window.location)
    //
    //     $(".lm_footer").hide();
    //
    // }
});

