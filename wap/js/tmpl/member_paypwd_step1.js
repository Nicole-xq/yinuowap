/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }
    $.ajax({
        type: 'get',
        url: ApiUrl + "/index.php?act=member_account&op=get_mobile_info&flag_whole=1",
        data: {key: key},
        dataType: 'json',
        success: function (result) {
            if (result.code == 200) {
                if (result.datas.state) {
                    $('#mobile').val(result.datas.mobile);
                } else {
                    location.href = WapSiteUrl + '/tmpl/member/member_mobile_bind.html';
                }
            }
        }
    });

    $.sValid.init({//注册验证
        rules: {
            captcha: "required",
            password: {
                required: true,
                maxlength:6,
                minlength:6,
                number:true
            }
        },
        messages: {
            captcha: "请填写验证码",
            password:{
                required: "密码必填!",
                maxlength:"密码格式错误!",
                minlength:"密码格式错误!",
                number:"密码格式错误!"
            }
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                $.map(eId, function (idx, item) {
                    $('#' + idx).parents('.login_form_input_panpel').addClass('error');
                    $('#' + idx).attr('placeholder', eMsg[idx + '_required']);
                });
            } else {
                $('#' + eId).parents('.login_form_input_panpel').removeClass('error');
            }
        }
    });

    //点击获取验证码
    var getCodeFlag = true;
    $('#getCode').click(function () {
        if (!getCodeFlag) {
            return false;
        }
        getCodeFlag = false;

        setTimeout(function () {
            getCodeFlag = true;
        }, 3000);
        var self = $(this),
            time = 60;
        $.getJSON(ApiUrl + '/index.php?act=member_account&op=modify_paypwd_step2', {key: key}, function (result) {
            if (!result.datas.error) {
                lm_errorTipsShow('<p>发送短信中....</p>');
                var times_Countdown = setInterval(function () {
                    time--;
                    if (time == 0) {
                        self.html('<i class="iconfont icon-shouji"></i>再次获取');
                        clearInterval(times_Countdown);
                    } else {
                        self.html('<span style="color:#8c8c8c;">' + time + '秒后再次获取</span>')
                    }
                }, 1000);
            } else {
                lm_errorTipsShow('<p>' + result.datas.error + '</p>');
            }
        });
    });

    //点击完成
    $('#completebtn').click(function () {
        var password = $("#password").val(),
            captcha = $("#captcha").val();
        if ($.sValid()) {
            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=member_account&op=new_modify_paypwd",
                data: {auth_code: captcha, password: password,key:key},
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        lm_errorTipsShow('<p>修改成功....</p>');
                        // location.href = WapSiteUrl + '/tmpl/member/member.html';
                        // window.history.go(-1)
                        // self.location=document.referrer;
                        window.location.href = document.referrer;
                    } else {
                        lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                    }
                }
            });
        }
        return false;
    });


});