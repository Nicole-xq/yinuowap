/**
 * Created by ADKi on 2017/4/7 0007.
 */
$(function() {
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
    var margin_num;
    $.getJSON(ApiUrl + '/index.php?act=member_index&op=member_margin_info', {'key':key}, function(result){
        var data = result.datas;
        margin_num = parseFloat(data.switch_margin).toFixed(2);
        $('#available_pd').html(margin_num);
    });

    $.sValid.init({
        rules:{
            money:"required",
        },
        messages:{
            money:"请填写金额!",
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });

    $('#checkbox').click(function(){
        if($('#checkbox').is(':checked')){
            $('#agree_protocol').val(1);
        }else{
            $('#agree_protocol').val(0);
        }
    });

    $('#all_pd').click(function(){
        $('#money').val(margin_num);
    });


    $('#nextform').click(function(){
        if($('#agree_protocol').val() == 0){
            return false;
        }
        if($.sValid()){
            var money = $('#money').val();
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=member_index&op=margin_cash_add",
                data:{key:key,money:money},
                dataType:'json',
                success:function(result){
                    if(result.code == 200){
                        $.sDialog({
                            skin:"block",
                            content:'提现完成',
                            okBtn:false,
                            cancelBtn:false
                        });
                        setTimeout("location.href = WapSiteUrl+'/tmpl/member/member.html'",1000);
                    }else{
                        errorTipsShow('<p>' + result.datas.error + '</p>');
                    }
                }
            });
        }

    });
});
