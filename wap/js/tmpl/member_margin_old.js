/**
 * Created by ADKi on 2017/4/7 0007.
 */
$(function(){
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
    //获取保证金金额
    $.getJSON(ApiUrl + '/index.php?act=member_index&op=member_margin_info', {'key':key}, function(result){
        var data = result.datas;
        var margin_num = parseFloat(data.available_margin).toFixed(2);

        var all_recharge = parseFloat(data.all_recharge).toFixed(2);
        var freeze_margin = parseFloat(data.freeze_margin).toFixed(2);
        var switch_margin = parseFloat(data.switch_margin).toFixed(2);
        $('#margin_num').html(margin_num);
        $('#switch_margin').html(switch_margin);
        $('#all_recharge').html(all_recharge);
        $('#freeze_margin').html(freeze_margin);
    });
    $('#draw_cash').click(function(){
        window.location.href="draw_margin.html";
    });
});