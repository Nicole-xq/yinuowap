$(function () {

    var key = getCookie('key');
    var id = getQueryString('act_id');
    var A = $('.vote_footer ul li:nth-child(3) a');
    var param = {key: key, id: id};

    if(key){
        $.ajax({
            url: ApiUrl + "/index.php?act=vote_activity&op=get_state",
            type: 'get',
            data: param,
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                var B = $('.vote_footer ul li:nth-child(3) i');
                var C = $('.vote_footer ul li:nth-child(3) span');

                switch (data.state){
                    case '0':
                        B.removeClass().addClass('iconfont icon-dkw_shenhezhong');
                        C.html('审核中');
                        break;
                    case '1':
                        // 弹框 报名成功，是否去分享
                        $.sDialog({content: '报名成功，去分享', okFn:function () {
                            location.href = WapSiteUrl + '/tmpl/member/entry_info.html?id=' + data.id;
                        }});
                        A.attr('href', WapSiteUrl + '/tmpl/member/entry_info.html?id=' + data.id);
                        B.removeClass().addClass('iconfont icon-toudichenggong');
                        C.html('审核通过');
                        break;
                    case '2':
                        A.attr('href', WapSiteUrl + '/tmpl/member/vote_activity_entry.html?act_id=' + id);
                        B.removeClass().addClass('iconfont icon-cha');
                        C.html('审核未通过');
                        break;
                    case '3':
                        A.attr('href', WapSiteUrl + '/tmpl/member/entry_info.html');
                        B.removeClass().addClass('iconfont icon-zhongjiangliebiao');
                        C.html('已获奖');
                        break;
                    case '4':
                        A.attr('href', WapSiteUrl + '/tmpl/member/entry_info.html');
                        B.removeClass().addClass('iconfont icon-jiedianjieshu');
                        C.html('活动结束');
                        break;
                    case '5':
                        B.removeClass().addClass('iconfont icon-jiedianjieshu');
                        C.html('活动结束');
                        break;
                    default:
                        A.attr('href', WapSiteUrl + '/tmpl/member/vote_activity_entry.html?act_id=' + id);
                }
            }
        });
    }

    $.ajax({
        url: ApiUrl + "/index.php?act=vote_activity&op=get_activity_info",
        type: 'get',
        data: param,
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            var html = template.render('cms_activity', data);
            $("#cms_activity_wrapper").html(html);
            var detail = data.detail;
            if(detail.short_content){
                $('.huodong-explain').html(detail.short_content);
                $('.main-center-top').show();
            }
            if(detail.vote_explain){
                $('.vote-explain').html(detail.vote_explain);
                $('.main-center-center').show();
            }

            document.title=detail.title;
            if (isWeiXin) {
                var shareData = {
                    title: detail.title,
                    desc: detail.description,
                    link: window.location.href,
                    imgUrl: window.location.origin + '/data/upload/cms/vote_activity/' + detail.share_logo
                };

                wx_config(shareData);
            }
            //APP分享参数
            appShareSet(shareData);
        }
    });

    $(A).click(function () {
        if(!key){
            location.href = 'login.html';
        }
    })

});