var key = getCookie('key');
var password,rcb_pay,pd_pay,payment_code,points_pay;
// 现在支付方式
function toPay(pay_sn,act,op) {
    $.ajax({
        type:'post',
        url:ApiUrl+'/index.php?act='+act+'&op='+op,
        data:{
            key:key,
            pay_sn:pay_sn
            },
        dataType:'json',
        success: function(result){
            checkLogin(result.login);
            if (result.datas.error) {
                $.sDialog({
                    skin:"red",
                    content:result.datas.error,
                    okBtn:false,
                    cancelBtn:false
                });
                return false;
            }
            // 从下到上动态显示隐藏内容
            $.animationUp({valve:'',scroll:''});
            // 需要支付金额
            $('#onlineTotal').html(result.datas.pay_info.pay_amount);

            // 是否设置支付密码
            if (!result.datas.pay_info.member_paypwd) {
                $('#wrapperPaymentPassword').find('.input-box-help').show();
            }

            // 支付密码标记
            var _use_password = false;
            if (parseFloat(result.datas.pay_info.payed_amount) <= 0) {
                if (parseFloat(result.datas.pay_info.member_available_pd) == 0 && parseFloat(result.datas.pay_info.member_available_rcb) == 0 && result.datas.pay_info.member_points_money == 0) {
                    $('#internalPay').hide();
                } else {
                    $('#internalPay').show();
                    // 充值卡
                    if (parseFloat(result.datas.pay_info.member_available_rcb) != 0) {
                        $('#wrapperUseRCBpay').show();
                        $('#availableRcBalance').html(parseFloat(result.datas.pay_info.member_available_rcb).toFixed(2));
                    } else {
                        $('#wrapperUseRCBpay').hide();
                    }

                    // 预存款
                    if (parseFloat(result.datas.pay_info.member_available_pd) != 0) {
                        $('#wrapperUsePDpy').show();
                        $('#availablePredeposit').html(parseFloat(result.datas.pay_info.member_available_pd).toFixed(2));
                    } else {
                        $('#wrapperUsePDpy').hide();
                    }

                    //诺币
                    if (result.datas.pay_info.member_points != 0 && act == 'member_buy' &&  result.datas.pay_info.order_type != 2) {
                        $('#wrapperPoints').show();
                        $('#availablePoints').html(result.datas.pay_info.member_points);
                        $('#availablePmoney').html(result.datas.pay_info.points_money);
                    } else {
                        $('#wrapperPoints').hide();
                    }

                }
            } else {
                $('#internalPay').hide();
            }

            password = '';
            $('#paymentPassword').on('change', function(){
                password = $(this).val();
            });

            rcb_pay = 0;
            $('#useRCBpay').click(function(){
                if ($(this).prop('checked')) {
                    _use_password = true;
                    $('#wrapperPaymentPassword').show();
                    rcb_pay = 1;
                } else {
                    if (pd_pay == 1) {
                        _use_password = true;
                        $('#wrapperPaymentPassword').show();
                    } else {
                        _use_password = false;
                        $('#wrapperPaymentPassword').hide();
                    }
                    rcb_pay = 0;
                }
            });

            pd_pay = 0;
            $('#usePDpy').click(function(){
                if ($(this).prop('checked')) {
                    _use_password = true;
                    $('#wrapperPaymentPassword').show();
                    pd_pay = 1;
                } else {
                    if (rcb_pay == 1) {
                        _use_password = true;
                        $('#wrapperPaymentPassword').show();
                    } else {
                        _use_password = false;
                        $('#wrapperPaymentPassword').hide();
                    }
                    pd_pay = 0;
                }
            });
            points_pay = 0;
            $('#usePoints').click(function(){
                if ($(this).prop('checked')) {
                    points_pay = 1;
                } else {
                    points_pay = 0;
                }
            });

            payment_code = '';
            if (!$.isEmptyObject(result.datas.pay_info.payment_list)) {
                var readytoWXPay = false;
                var readytoAliPay = false;
                // if(result.datas.pay_info.pay_amount < 5000){
                //     var readytoLklPay = false;
                //     var readytoAliPay = true;
                // }else{
                //     var readytoLklPay  = true;
                //     var readytoAliPay = false;
                // }
                var m = navigator.userAgent.match(/MicroMessenger\/(\d+)\./);
                if (parseInt(m && m[1] || 0) >= 5) {
                    // 微信内浏览器
                    readytoWXPay = true;
                    readytoAliPay = false;
                } else {
                    readytoAliPay = true;
                }
                if (isapp) {
                    readytoAliPay = true;
                    readytoWXPay = wxIsInstalled;
                }
                $('#alipay').parents('label').hide();
                $('#lklpay').parents('label').hide();
                // $('#wxpay_jsapi').parents('label').hide();
                // $('#wxpay').parents('label').hide();

                for (var i=0; i<countProperties(result.datas.pay_info.payment_list); i++) {
                    var _payment_code = result.datas.pay_info.payment_list[i].payment_code;
                    $('#'+_payment_code).attr('checked', false).parents('label').removeClass('checked');
                    if (_payment_code == 'alipay' && readytoAliPay) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            // $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'lklpay' && readytoLklPay) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            // $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'wxpay_jsapi' && readytoWXPay && !isapp) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            // $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'wxpay' && readytoWXPay && isapp) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            // $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                }
            }

            $('#alipay').parent().click(function(){
                payment_code = 'alipay';
                $(this).attr('checked', true).parents('label').addClass('checked').parents('label').siblings().removeClass('checked');
            });
            $('#lklpay').parent().click(function(){
                payment_code = 'lklpay';
                $(this).attr('checked', true).parents('label').addClass('checked').parents('label').siblings().removeClass('checked');

            });

            $('#wxpay_jsapi').parent().click(function(){
                payment_code = 'wxpay_jsapi';
                $(this).attr('checked', true).parents('label').addClass('checked').parents('label').siblings().removeClass('checked');

            });

            $('#wxpay').parent().click(function(){
                payment_code = 'wxpay';
                $(this).attr('checked', true).parents('label').addClass('checked').parents('label').siblings().removeClass('checked');

            });

            $('#toPay').click(function(){
                if (payment_code == '') {
                    $.sDialog({
                        skin:"red",
                        content:'请选择支付方式',
                        okBtn:false,
                        cancelBtn:false
                    });
                    return false;
                }
                if (_use_password) {
                    // 验证支付密码是否填写
                    if (password == '') {
                        $.sDialog({
                            skin:"red",
                            content:'请填写支付密码',
                            okBtn:false,
                            cancelBtn:false
                        });
                        return false;
                    }
                    // 验证支付密码是否正确
                    $.ajax({
                        type:'post',
                        url:ApiUrl+'/index.php?act=member_buy&op=check_pd_pwd',
                        dataType:'json',
                        data:{key:key,password:password},
                        success:function(result){
                            if (result.datas.error) {
                                $.sDialog({
                                    skin:"red",
                                    content:result.datas.error,
                                    okBtn:false,
                                    cancelBtn:false
                                });
                                return false;
                            }
                            goToPayment(pay_sn,'pd_pay_new');
                        }
                    });
                } else {
               	 goToPayment(pay_sn,'pd_pay_new');
                }
            });
        }
    });
}

function goToPayment(pay_sn,op) {
    if (isapp) {
        goToPaymentInApp(pay_sn,op)
    } else {
        if(op == 'pay_new'){
            location.href = ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=' + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code=' + payment_code + '&points_pay=' + points_pay;
        }else{


            location.href = ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=' + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code=' + payment_code;
        }
    }
}

var isapp = 0;
var aliPay, wxPay, wx,appType;
var wxIsInstalled = false;
function checkWxHandler() {
    var wx = api.require('wx');
    wx.isInstalled(function(ret, err){
        if(ret.installed){
            wxIsInstalled = true;

            $("#wxpay").parents("label").show();
            if (payment_code == "") {
                payment_code = "wxpay";
                $("#wxpay").attr("checked", true).parents("label").addClass("checked")
            }
        } else {
            wxIsInstalled = false;
        }
    });
}

function getNoncestr() {
    var timestamp = new Date().getTime();
    var Num = "";
    for (var i = 0; i < 6; i++) {
        Num += Math.floor(Math.random() * 10);
    }
    timestamp = timestamp + Num;
    return timestamp;
}

function goToPaymentInApp(a, e) {
    if (payment_code == 'alipay') {
        if(appType == 1){
            var tmp_url = ApiUrl + "/index.php?act=member_payment&op=alipay_native_pay_new&opex="+e;
        }else{
            var tmp_url = ApiUrl + "/index.php?act=member_payment&op=alipay_native_pay&opex="+e;
        }
        $.ajax({
            type: "post",
            url: tmp_url,
            dataType: "json",
            data: {
                key: key,
                pay_sn: a
            },
            success: function(p) {
                if (p.datas.error) {
                    $.sDialog({
                        skin: "red",
                        content: p.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                    return false
                }

                if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
                    var tmp_str = {sign:p.datas.signStr,url:WapSiteUrl + "/tmpl/member/predepositlog.html"}
                    tmp_str = JSON.stringify(tmp_str);
                    // alert(tmp_str)
                    window.webkit.messageHandlers.aliPay.postMessage(tmp_str);
                    return true
                }else if(appType == 1){
                    var tmp_str = {sign:p.datas.signStr,url:WapSiteUrl + "/tmpl/member/predepositlog.html"}
                    tmp_str = JSON.stringify(tmp_str);
                    var str = jsObj.aliPay(tmp_str);
		            // alert(str);
		            return true;
                }

                var aliPay = api.require('aliPay');
                aliPay.payOrder({
                    orderInfo: p.datas.signStr
                }, function(ret, err) {
                    if (ret.code == '9000') {
                        alert('支付操作完成！如果您的订单状态没有改变，请耐心等待支付网关的返回结果。');

                        openNewWin(WapSiteUrl + "/tmpl/member/predepositlog.html");
                    } else {
                        alert('Sorry，支付未完成或失败！');
                    }
                });
            }
        });
    } else if (payment_code == 'wxpay') {
        $.ajax({
            type: "post",
            url: ApiUrl + "/index.php?act=member_payment&op=wx_app_pay3&opex="+e,
            dataType: "json",
            data: {
                key: key,
                pay_sn: a
            },
            success: function(p) {
                if (p.datas.error) {
                    $.sDialog({
                        skin: "red",
                        content: p.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                    return false
                }

                if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
                    var json_str = JSON.stringify(p.datas);
                    var tmp_str = {sign:json_str,url:WapSiteUrl + "/tmpl/member/predepositlog.html"}
                    tmp_str = JSON.stringify(tmp_str);
                    window.webkit.messageHandlers.wxPay.postMessage(tmp_str);
                    return true;
                }else if(appType == 1){
                    var json_str = JSON.stringify(p.datas);
                    var tmp_str = {sign:json_str,url:WapSiteUrl + "/tmpl/member/predepositlog.html"}
                    tmp_str = JSON.stringify(tmp_str);
                    var str = jsObj.wxPay(tmp_str);
		            // alert(str);
		            return true;
                }
                var appid = p.datas.appid;
                var mch_id = p.datas.partnerid;
                var noncestr = p.datas.noncestr;
                var timestamp = p.datas.timestamp;
                var sign = p.datas.sign;
                var prepayid = p.datas.prepayid;

                var wxPay = api.require('wxPay');
                wxPay.payOrder({
                    apiKey: appid,
                    orderId: prepayid,
                    mchId: mch_id,
                    nonceStr: noncestr,
                    timeStamp: timestamp,
                    package: 'Sign=WXPay',
                    sign: sign
                }, function(ret, err) {
                    if (ret.status) {
                        alert('支付操作完成！如果您的订单状态没有改变，请耐心等待支付网关的返回结果。');

                        openNewWin(WapSiteUrl + "/tmpl/member/predepositlog.html");
                    } else {
                        alert('Sorry，支付未完成或失败！');
                    }
                });
            }
        });
    } else {
        window.location.href = ApiUrl + "/index.php?act=member_payment&op=" + e + "&key=" + key + "&pay_sn=" + a + "&password=" + password + "&rcb_pay=" + rcb_pay + "&pd_pay=" + pd_pay + "&payment_code=" + payment_code
    }
}

function openNewWin(url) {
    window.location.href = url;
}
