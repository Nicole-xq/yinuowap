/**
 * Created by ADKi on 2017/3/20 0020.
 */
var key = getCookie('key');
var password = '';
var rcb_pay = 0;
var pd_pay = 0;
var payment_code = 'alipay';
var points_amount = 0;
var real_total,points_money;
var is_check = 0;
// 现在支付方式
function toPay(pay_sn,act,op,type,show_pd) {
    //alert(act+ '------------act')
    // alert(op+ '------------op')

    $.ajax({
        type:'get',
        url:ApiUrl+'/index.php?act='+act+'&op='+op,
        data:{
            key:key,
            pay_sn:pay_sn,
            type: type,
        },
        dataType:'json',
        success: function(result){
            checkLogin(result.login);
            console.log(222222);
            console.log(result);
            addCookie('whkMarginId',result.datas.order_info.margin_id)

            if (result.datas.error) {
                $.sDialog({
                    skin:"red",
                    content:result.datas.error,
                    okBtn:false,
                    cancelBtn:false
                });
                return false;
            }
            // 如果保证金账户已经支付完成直接跳转
            if (result.datas.over_pay == 1) {
                goToPayment(pay_sn,type == 'margin' ? 'pay_margin' : 'pay_auction');
                return false;
            }

            // 从下到上动态显示隐藏内容
            $.animationUp({valve:'',scroll:''});

            // 需要支付金额
            $('#onlineTotal').html(result.datas.pay_info.pay_amount);
            real_total = result.datas.pay_info.pay_amount;
            // 是否设置支付密码
            if (!result.datas.pay_info.member_paypwd) {
                $('#wrapperPaymentPassword').find('.input-box-help').show();
            }

            // 支付密码标记
            var _use_password = false;
            if (parseFloat(result.datas.pay_info.payed_amount) <= 0) {
                if (parseFloat(result.datas.pay_info.member_available_pd) == 0 && parseFloat(result.datas.pay_info.member_available_rcb) == 0) {
                    $('#internalPay').hide();
                } else {
                    $('#internalPay').show();
                    // 充值卡
                    if (parseFloat(result.datas.pay_info.member_available_rcb) != 0 && (result.datas.order_info.api_pay_time == 0 || result.datas.order_info.api_pay_time == null)) {
                        $('#wrapperUseRCBpay').show();
                        $('#availableRcBalance').html(parseFloat(result.datas.pay_info.member_available_rcb).toFixed(2));
                    } else {
                        $('#wrapperUseRCBpay').hide();
                    }
                    // 预存款
                    if (parseFloat(result.datas.pay_info.member_available_pd) != 0 && result.datas.pay_info.hide_pd ==0 && (result.datas.order_info.api_pay_time == 0 || result.datas.order_info.api_pay_time == null)) {
                        $('#wrapperUsePDpy').show();
                        $('#availablePredeposit').html(parseFloat(result.datas.pay_info.member_available_pd).toFixed(2));
                    } else {
                        $('#wrapperUsePDpy').hide();
                    }

                    //诺币
                    if (result.datas.pay_info.member_points != 0 && (result.datas.order_info.api_pay_time == 0 || result.datas.order_info.api_pay_time == null)) {
                        $('#wrapperPoints').show();
                        points_money = result.datas.pay_info.points_money;
                        $('#availablePoints').html(result.datas.pay_info.points_money);
                        $('#availablePmoney').html(result.datas.pay_info.points_money);
                    } else {
                        $('#wrapperPoints').hide();
                    }
                }
            } else {
                $('#internalPay').hide();
            }

            password = '';
            $('#paymentPassword').on('change', function(){
                password = $(this).val();
            });

            rcb_pay = 0;
            $('#useRCBpay').click(function(){
                if ($(this).prop('checked')) {
                    _use_password = true;
                    $('#wrapperPaymentPassword').show();
                    rcb_pay = 1;
                } else {
                    if (pd_pay == 1) {
                        _use_password = true;
                        $('#wrapperPaymentPassword').show();
                    } else {
                        _use_password = false;
                        $('#wrapperPaymentPassword').hide();
                    }
                    rcb_pay = 0;
                }
            });

            pd_pay = 0;
            $('#usePDpy').click(function(){
                if ($(this).prop('checked')) {
                    _use_password = true;
                    $('#wrapperPaymentPassword').show();
                    pd_pay = 1;
                } else {
                    if (rcb_pay == 1) {
                        _use_password = true;
                        $('#wrapperPaymentPassword').show();
                    } else {
                        _use_password = false;
                        $('#wrapperPaymentPassword').hide();
                    }
                    pd_pay = 0;
                }
            });

            points_amount = 0;
            $('#usePoints').click(function(){
                if ($(this).prop('checked')) {
                    if(parseFloat(real_total) >= parseFloat(points_money)){
                        var total = real_total - points_money;
                    }else{
                        var total = 0;
                    }
                    $('#onlineTotal').html(total);
                    points_amount = 1;
                    points_pay = 1;
                } else {
                    $('#onlineTotal').html(real_total);
                    points_pay = 0;
                    points_amount = 0;
                }
            });

            payment_code = '';
            if (!$.isEmptyObject(result.datas.pay_info.payment_list)) {
                var readytoWXPay = false;
                var readytoAliPay = false;
                var readytoLklPay = false;
                // if(result.datas.pay_info.pay_amount < 1){
                //     var readytoLklPay = false;
                //     var readytoAliPay = true;
                // }else{
                //     var readytoLklPay  = true;
                //     var readytoAliPay = false;
                // }
                var m = navigator.userAgent.match(/MicroMessenger\/(\d+)\./);
                if (parseInt(m && m[1] || 0) >= 5) {
                    // 微信内浏览器
                    readytoWXPay = true;
                    readytoAliPay = false;
                } else {
                    readytoAliPay = true;
                }
                if (isapp) {
                    readytoAliPay = true;
                    readytoWXPay = wxIsInstalled;
                }
                $('#alipay').parents('label').hide();
                $('#lklpay').parents('label').hide();
                for (var i=0; i<result.datas.pay_info.payment_list.length; i++) {
                    var _payment_code = result.datas.pay_info.payment_list[i].payment_code;
                    $('#'+_payment_code).attr('checked', false).parents('label').removeClass('checked');
                    if (_payment_code == 'alipay' && readytoAliPay) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'lklpay' && readytoLklPay) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'wxpay_jsapi' && readytoWXPay && !isapp) {

                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                    if (_payment_code == 'wxpay' && readytoWXPay && isapp) {
                        $('#'+ _payment_code).parents('label').show();
                        if (payment_code == '') {
                            payment_code = _payment_code;
                            $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                        }
                    }
                }
            }

            $('#alipay').click(function(){
                payment_code = 'alipay';
            });
            $('#lklpay').click(function(){
                payment_code = 'lklpay';
            });

            $('#wxpay_jsapi').click(function(){
                payment_code = 'wxpay_jsapi';
            });

            $('#wxpay').click(function(){
                payment_code = 'wxpay';
            });

            $('#toPay').click(function(){
                if (payment_code == '') {
                    $.sDialog({
                        skin:"red",
                        content:'请选择支付方式',
                        okBtn:false,
                        cancelBtn:false
                    });
                    return false;
                }
                if (_use_password) {
                    console.log(password)
                    // 验证支付密码是否填写
                    if (password == '') {
                        $.sDialog({
                            skin:"red",
                            content:'请填写支付密码',
                            okBtn:false,
                            cancelBtn:false
                        });
                        return false;
                    }
                    if(is_check == 1){
                        return false;
                    }
                    is_check = 1;
                    // 验证支付密码是否正确
                    $.ajax({
                        type:'post',
                        url:ApiUrl+'/index.php?act=member_buy&op=check_pd_pwd',
                        dataType:'json',
                        data:{key:key,password:password},
                        success:function(result){
                            is_check = 0;
                            if (result.datas.error) {
                                $.sDialog({
                                    skin:"red",
                                    content:result.datas.error,
                                    okBtn:false,
                                    cancelBtn:false
                                });
                                return false;
                            }
                            goToPayment(pay_sn,type == 'margin' ? 'pay_margin' : 'pay_auction');
                        }
                    });
                } else {
                    goToPayment(pay_sn,type == 'margin' ? 'pay_margin' : 'pay_auction');
                }
            });
        }
    });
}

function goToPayment(pay_sn,op) {
    if (isapp) {
        goToPaymentInApp(pay_sn,op)
    } else {
        //alert(op+'---------op')
        //alert(key+'---------key')
        //
        //alert(pay_sn+'---------pay_sn')
        //alert(password+'---------password')
        //alert(rcb_pay+'---------rcb_pay')
        //alert(pd_pay+'---------pd_pay')
        //alert(points_amount+'---------points_amount')
        //alert(payment_code+'---------payment_code')


        //alert(ApiUrl+'/index.php?act=member_payment_auction&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password='
        //    + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&points_amount=' + points_amount + '&payment_code=' + payment_code);

        location.href = ApiUrl+'/index.php?act=member_payment_auction&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password='
            + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&points_amount=' + points_amount + '&payment_code=' + payment_code;
    }
}

var isapp = 0;
var aliPay, wxPay, wx,appType;
var wxIsInstalled = false;
function checkWxHandler() {
    var wx = api.require('wx');
    wx.isInstalled(function(ret, err){
        if(ret.installed){
            wxIsInstalled = true;

            $("#wxpay").parents("label").show();
            if (payment_code == "") {
                payment_code = "wxpay";
                $("#wxpay").attr("checked", true).parents("label").addClass("checked")
            }
        } else {
            wxIsInstalled = false;
        }
    });
}

function getNoncestr() {
    var timestamp = new Date().getTime();
    var Num = "";
    for (var i = 0; i < 6; i++) {
        Num += Math.floor(Math.random() * 10);
    }
    timestamp = timestamp + Num;
    return timestamp;
}

function goToPaymentInApp(a, e) {
    if (payment_code == 'alipay') {
        if(appType == 1){
            if(e == 'pay_margin'){
                var tmp_url = ApiUrl + "/index.php?act=member_payment&op=alipay_native_pay_new&margin=1";
            }else{
                var tmp_url = ApiUrl + "/index.php?act=member_payment_auction&op=alipay_native_pay_new&auction=1";
            }
        }else{
            var tmp_url = ApiUrl + "/index.php?act=member_payment&op=alipay_native_pay";
        }
        $.ajax({
            type: "post",
            url: tmp_url,
            dataType: "json",
            data: {
                key: key,
                pay_sn: a,
                rcb_pay:rcb_pay,
                pd_pay:pd_pay,
                password:password,
                points_amount:points_amount
            },
            success: function(p) {
                if (p.datas.error) {
                    $.sDialog({
                        skin: "red",
                        content: p.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                    return false
                }
                if(p.datas.code == '1000'||p.datas == 0){
                    if (/attach\=v$/.test(window.location.href)) {
                        openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                    } else {
                        openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                    }
                    return true;
                }
                if(appType == 1){
                    if(e == 'pay_margin'){
                        var tmp_url = WapSiteUrl + "/tmpl/member/margin_order_list.html";
                    }else{
                        var tmp_url = WapSiteUrl + "/tmpl/member/auction_order_list.html";
                    }
                    var tmp_str = {sign:p.datas.signStr,url:tmp_url}
                    tmp_str = JSON.stringify(tmp_str);
                    var str = jsObj.aliPay(tmp_str);
		            // alert(str);
		            return true;
                }
                var aliPay = api.require('aliPay');
                aliPay.payOrder({
                    orderInfo: p.datas.signStr
                }, function(ret, err) {
                    if (ret.code == '9000') {
                        alert('支付操作完成！如果您的订单状态没有改变，请耐心等待支付网关的返回结果。');
                        if (/attach\=v$/.test(location.href)) {
                            openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                        } else {
                            if(e == 'pay_margin'){
                                openNewWin(WapSiteUrl + "/tmpl/member/margin_order_list.html");
                            }else{
                                openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                            }
                        }
                    } else {
                        alert('Sorry，支付未完成或失败！');

                        if (/attach\=v$/.test(location.href)) {
                            openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                        } else {
                            if(e == 'pay_margin'){
                                openNewWin(WapSiteUrl + "/tmpl/member/margin_order_list.html");
                            }else{
                                openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                            }
                        }
                    }
                });
            }
        });
    } else if (payment_code == 'wxpay') {
        if(e == 'pay_margin'){
            var wx_url = ApiUrl + "/index.php?act=member_payment&op=wx_app_pay3&margin=1";
        }else{
            var wx_url = ApiUrl + "/index.php?act=member_payment_auction&op=wx_app_pay_auction3";
        }
        $.ajax({
            type: "post",
            url: wx_url,
            dataType: "json",
            data: {
                key: key,
                pay_sn: a,
                rcb_pay:rcb_pay,
                pd_pay:pd_pay,
                password:password
            },
            success: function(p) {
                if (p.datas.error) {
                    $.sDialog({
                        skin: "red",
                        content: p.datas.error,
                        okBtn: false,
                        cancelBtn: false
                    });
                    return false
                }
                if(p.datas == 0){
                    if (/attach\=v$/.test(window.location.href)) {
                        openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                    } else {
                        openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                    }
                    return true;
                }
                if(appType == 1){
                    var json_str = JSON.stringify(p.datas);
                    if(e == 'pay_margin'){
                        var tmp_url = WapSiteUrl + "/tmpl/member/margin_order_list.html";
                    }else{
                        var tmp_url = WapSiteUrl + "/tmpl/member/auction_order_list.html";
                    }
                    var tmp_str = {sign:json_str,url:tmp_url}
                    tmp_str = JSON.stringify(tmp_str);
                    var str = jsObj.wxPay(tmp_str);
		            // alert(str);
		            return true;
                }

                var appid = p.datas.appid;
                var mch_id = p.datas.partnerid;
                var noncestr = p.datas.noncestr;
                var timestamp = p.datas.timestamp;
                var sign = p.datas.sign;
                var prepayid = p.datas.prepayid;

                var wxPay = api.require('wxPay');
                wxPay.payOrder({
                    apiKey: appid,
                    orderId: prepayid,
                    mchId: mch_id,
                    nonceStr: noncestr,
                    timeStamp: timestamp,
                    package: 'Sign=WXPay',
                    sign: sign
                }, function(ret, err) {
                    if (ret.status) {
                        alert('支付操作完成！如果您的订单状态没有改变，请耐心等待支付网关的返回结果。');
                        if (/attach\=v$/.test(window.location.href)) {
                            openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                        } else {
                            if(e == 'pay_margin'){
                                openNewWin(WapSiteUrl + "/tmpl/member/margin_order_list.html");
                            }else{
                                openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                            }
                        }
                    } else {
                        alert('Sorry，支付未完成或失败！');

                        if (/attach\=v$/.test(window.location.href)) {
                            openNewWin(WapSiteUrl + "/tmpl/member/vr_order_list.html");
                        } else {
                            if(e == 'pay_margin'){
                                openNewWin(WapSiteUrl + "/tmpl/member/margin_order_list.html");
                            }else{
                                openNewWin(WapSiteUrl + "/tmpl/member/auction_order_list.html");
                            }
                        }
                    }
                });
            }
        });
    } else {
        //alert(e +'==========op')
        //alert(key +'==========key')
        //
        //alert(pay_sn + '-----pay_sn')
        //alert(password + '-----password')
        //alert(rcb_pay + '-----rcb_pay')
        //
        //alert(pd_pay + '-----pd_pay')
        //
        //alert(payment_code + '-----payment_code')
        //alert(points_pay + '-----points_pay')
        window.location.href = ApiUrl + "/index.php?act=member_payment&op=" + e + "&key=" + key + "&pay_sn=" + a + "&password=" + password + "&rcb_pay=" + rcb_pay + "&pd_pay=" + pd_pay + "&payment_code=" + payment_code
    }
}

function openNewWin(url) {
    window.location.href = url;
}
