/**
 * Created by ADKi on 2017/3/13 0013.
 */
var key = getCookie('key');
var address_id,vat_hash,voucher,pd_pay,password,fcode='',rcb_pay,payment_code;
var auction_id = getQueryString('auction_id');
var message = {};
var buyer_id = getQueryString('buyer_id');
$(function () {
    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return false;
    }
    // 初始化地址列表
    $('#list-address-valve').click(function(){
        $.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_address&op=address_list",
            data:{key:key},
            dataType:'json',
            async:false,
            success:function(result){
                checkLogin(result.login);
                if(result.datas.address_list==null){
                    return false;
                }
                var data = result.datas;
                // data.address_id = address_id;
                var html = template.render('list-address-add-list-script', data);
                $("#list-address-add-list-ul").html(html);

            }
        });
    });

    $.animationLeft({
        valve : '#list-address-valve',
        wrapper : '#list-address-wrapper',
        scroll : '#list-address-scroll'
    });

    // 地区选择
    $('#list-address-add-list-ul').on('click', 'li', function(){
        $(this).addClass('selected').siblings().removeClass('selected');
        eval('address_info = ' + $(this).attr('data-param'));
        // _init(address_info.address_id);
        $('#list-address-wrapper').find('.header-l > a').click();
    });

    // 地址新增
    $.animationLeft({
        valve : '#new-address-valve',
        wrapper : '#new-address-wrapper',
        scroll : ''
    });
    // 支付方式
    $.animationLeft({
        valve : '#select-payment-valve',
        wrapper : '#select-payment-wrapper',
        scroll : ''
    });
    // 地区选择
    $('#new-address-wrapper').on('click', '#varea_info', function(){
        $.areaSelected({
            success : function(data){
                city_id = data.area_id_2 == 0 ? data.area_id_1 : data.area_id_2;
                area_id = data.area_id;
                area_info = data.area_info;
                $('#varea_info').val(data.area_info);
            }
        });
    });

    template.helper('isEmpty', function(o) {
        var b = true;
        $.each(o, function(k, v) {
            b = false;
            return false;
        });
        return b;
    });

    template.helper('pf', function(o) {
        return parseFloat(o) || 0;
    });

    template.helper('p2f', function(o) {
        return (parseFloat(o) || 0).toFixed(2);
    });

    var _init = function () {

        $.ajax({
            type: 'get',
            url: ApiUrl+'/index.php?act=member_buy_auction&op=show_margin_order',
            data: {key: key,auction_id: auction_id, address_id:address_id,},
            dataType: 'json',
            success: function (result) {
                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                    return false;
                }

                var margin_num_html = template.render('margin_num_html', result.datas.auction_info);
                $("#margin_num").html(margin_num_html);
                $(".buy-num").val(parseInt(result.datas.auction_info.auction_bond));
//出价，减
                $(".minus").click(function () {
                    var buynum = $(".buy-num").val();
                    if (buynum >= result.datas.auction_info.auction_bond + 1) {
                        $(".buy-num").val(parseInt(buynum - 1));
                    }
                });
//出价
                $(".add").click(function () {
                    var buynum = parseInt($(".buy-num").val());
                    var maxNum=1000000000;
                    if (buynum < 999999999) {
                        $(".buy-num").val(parseInt(buynum + 1));
                    }else{
                        $(".buy-num").val(parseInt(maxNum));
                    }
                });
                $('input[type=number]').keypress(function(e){
                    keyPress();
                });

                function keyPress() { var keyCode = event.keyCode; if (keyCode >= 48 && keyCode <= 57) { event.returnValue = true; }else { event.returnValue = false; } }


                // 默认地区相关-不需要地址了
                /*if ($.isEmptyObject(result.datas.address_info)) {
                 $.sDialog({
                 skin:"block",
                 content:'请添加地址',
                 okFn: function() {
                 $('#new-address-valve').click();
                 },
                 cancelFn: function() {
                 history.go(-1);
                 }
                 });
                 return false;
                 }*/

                // 输入地址数据
                insertHtmlAddress(result.datas.address_info);
                console.log(result.datas.address_info);
                password = '';
            }
        });
    };

    rcb_pay = 0;
    pd_pay = 0;
    // 初始化
    _init();

    // 插入地址数据到html
    var insertHtmlAddress = function (address_info) {
        address_id = address_info.address_id;
        console.log(address_id);
        $('#true_name').html(address_info.true_name);
        $('#mob_phone').html(address_info.mob_phone);
        $('#address').html(address_info.area_info + address_info.address);
        area_id = address_info.area_id;
        city_id = address_info.city_id;

        $('#payment-offline').hide();
        pay_name = 'online';
        $('#select-payment-valve').find('.current-con').html('在线支付');

        $('#ToBuyStep2').parent().addClass('ok');

    }

    // 支付方式选择
    // 在线支付
    $('#payment-online').click(function(){
        pay_name = 'online';
        $('#select-payment-wrapper').find('.header-l > a').click();
        $('#select-payment-valve').find('.current-con').html('在线支付');
        $(this).addClass('sel').siblings().removeClass('sel');
    })
    // 线下支付
    $('#payment-underline').click(function(){
        pay_name = 'underline';
        $('#select-payment-wrapper').find('.header-l > a').click();
        $('#select-payment-valve').find('.current-con').html('线下支付');
        $(this).addClass('sel').siblings().removeClass('sel');
    })

    // 地址保存
    $.sValid.init({
        rules:{
            vtrue_name:"required",
            vmob_phone:"required",
            varea_info:"required",
            vaddress:"required"
        },
        messages:{
            vtrue_name:"姓名必填！",
            vmob_phone:"手机号必填！",
            varea_info:"地区必填！",
            vaddress:"街道必填！"
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                errorTipsShow(errorHtml);
            }else{
                errorTipsHide();
            }
        }
    });
    $('#add_address_form').find('.btn').click(function(){
        if($.sValid()){
            var param = {};
            param.key = key;
            param.true_name = $('#vtrue_name').val();
            param.mob_phone = $('#vmob_phone').val();
            param.address = $('#vaddress').val();
            param.city_id = city_id;
            param.area_id = area_id;
            param.area_info = $('#varea_info').val();
            param.is_default = 0;

            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=member_address&op=address_add",
                data:param,
                dataType:'json',
                success:function(result){
                    if (!result.datas.error) {
                        param.address_id = result.datas.address_id;
                        _init(param.address_id);
                        $('#new-address-wrapper,#list-address-wrapper').find('.header-l > a').click();
                    }
                }
            });
        }
    });

    // 支付，创建订单
    var buy_step2 = 0;
    $('#ToBuyStep2').click(function(){
        var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
        var lm_val=$('#buynum').val().trim();
        if(!lm_val||pattern.test(lm_val)||isNaN(lm_val)){
            $.sDialog({
                skin:"red",
                content:'请输入正确的金额！',
                okBtn:false,
                cancelBtn:false
            });
            return false;
        }

        if (buy_step2) {
            $.sDialog({
                skin:"red",
                content:'订单正在处理中，请勿重复点击！',
                okBtn:false,
                cancelBtn:false
            });
            return false;
        }
        buy_step2 = 0;
        var msg = '';
        for (var k in message) {
            msg += k + '|' + $('#storeMessage' + k).val() + ',';
        }
        var margin_amount=parseInt($('#buynum').val());
        var margin_amount_min=parseInt($('#buynum').attr('data-min'));

        if(margin_amount<margin_amount_min){
            $.sDialog({
                skin:"red",
                content:'保证金最低金额为'+margin_amount_min+'元',
                okBtn:false,
                cancelBtn:false
            });
            $('#buynum').val(margin_amount_min.toFixed(2));
            return false;
        }
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_buy_auction&op=create_margin_order',
            data:{
                key:key,
                auction_id:auction_id,
                address_id:address_id,
                pay_name:pay_name,
                is_margin:1,
                is_anonymous:0,
                pd_pay:pd_pay,
                password:password,
                rcb_pay:rcb_pay,
                margin_amount:margin_amount
            },
            dataType:'json',
            success: function(result){
                checkLogin(result.login);
                if (result.datas.error) {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                    buy_step2 = 0;
                    return false;
                }
                if(result.datas.payment_code == 'underline'){
                    window.location.href = WapSiteUrl + '/tmpl/member/underline_pay_auction.html?pay_sn='+result.datas.pay_sn + '&type=margin';
                } else {
                    toPay(result.datas.pay_sn,'member_buy_auction','pay', 'margin');
                }
            }
        });
    });
});



//修复firefox输入字符的bug
function lm_inputNum(self){
    if(self.value.length==1){
        self.value=self.value.replace(/[^1-9]/g,'')
    }else{
        self.value=self.value.replace(/\D/g,'')
    }
}


