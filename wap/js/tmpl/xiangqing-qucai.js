/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var gs_id = getQueryString("gs_id");
    var key = getCookie('key');
    if (!gs_id) {
        window.location.href = '../qucai/qucai.html'
    }
    get_detail();
    function get_detail() {
        //渲染页面
        $.ajax({
            url: ApiUrl + "/index.php?act=guess&op=guess_detail",
            type: "get",
            data: {gs_id: gs_id, key: key},
            dataType: "json",
            success: function (result) {
                var data = result.datas;
                if (!data.error) {
                    //商品规格格式化数据
                    //渲染模板
                    var tmp_param = template;
                    tmp_param.isEscape = 0;
                    var html = tmp_param.render('guess_detail', data);
                    document.title = data.guess.guess_info.gs_name;
                    $("#guess_detail_html").html(html);
                    lmCuteDown();
                    //banner图
                    new Swiper('.qucai_detail_banner', {
                        loop: true,
                        paginationClickable: true,
                        pagination: '.qucai_detail_pagination',
                        autoplay: 5000
                    });
                }
                if(parseInt(data.guess.guess_info.joined_times)<=0){
                    $('.qll_quiz_price').remove();
                }

                if(!$('.qucai_detail_bottom').length){
                    $('body').append('<div class="qucai_detail_bottom"  style="background: #666666;color:#fff;">竞猜已结束</div>');
                }

                $("#submit").click(function () {
                    var key = getCookie('key');
                    var goods_id = $(this).attr('goods_id');
                    var gs_id = getQueryString('gs_id');
                    $.ajax({
                        type: 'post',
                        url: ApiUrl + '/index.php?act=member_buy&op=buy_step1',
                        data: {key: key, gs_id: gs_id, goods_id: goods_id, cart_id: goods_id + '|1'},
                        dataType: 'json',
                        success: function (result) {
                            if (result.datas.error) {
                                $.sDialog({skin: "green", content: result.datas.error, okBtn: false, cancelBtn: false});
                                return false;
                            } else {
                                var u = WapSiteUrl + '/tmpl/order/buy_step1.html?goods_id=' + goods_id + '&buynum=' + 1 + '&gs_id=' + gs_id;
                                location.href = u;
                            }
                        }
                    });
                    return false;
                });

                $("#chujia").click(function () {
                    var key = getCookie('key');
                    var gs_id = getQueryString("gs_id");
                    gs_id = parseInt(gs_id);
                    if (!key) {
                        checkLogin(0);
                        return;
                    }
                    window.location.href = WapSiteUrl + '/tmpl/qucai/xiangqing-qucaichujia.html?gs_id=' + gs_id;
                    return false;
                });
                //设置提醒／取消提醒
                $('.remind').click(function () {
                    var _self = $(this);
                    var key = getCookie('key');
                    if (!key) {
                        checkLogin(0);
                        return;
                    }
                    if (_self.hasClass('active')) {
                        $.ajax({
                            type: 'get',
                            url: ApiUrl + "/index.php?act=member_guess&op=cancel_guess_remind",
                            data: {gs_id: gs_id, key: key},
                            dataType: "json",
                            success: function (result) {
                                console.log(result)
                                if (result.code == 'ok') {
                                    _self.removeClass('active');
                                    $.sDialog({skin: "green", content: '取消提醒成功', okBtn: false, cancelBtn: false});
                                } else {
                                    $.sDialog({
                                        skin: "red",
                                        content: result.code.error,
                                        okBtn: false,
                                        cancelBtn: false
                                    });
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: ApiUrl + "/index.php?act=auction&op=check_mobile",
                            data: {key: key},
                            dataType: "json",
                            success: function (result) {
                                if (result.datas == 0) {
                                    $.sDialog({skin: "green", content: '请先绑定手机', okBtn: false, cancelBtn: false});
                                    setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                                } else {
                                    $.ajax({
                                        type: 'POST',
                                        url: ApiUrl + "/index.php?act=member_guess&op=guess_remind",
                                        data: {gs_id: gs_id, key: key},
                                        dataType: "json",
                                        success: function (result) {
                                            console.log(result)
                                            if (result.code == 'ok') {
                                                _self.addClass('active');
                                                $.sDialog({
                                                    skin: "green",
                                                    content: '设置提醒成功',
                                                    okBtn: false,
                                                    cancelBtn: false
                                                });
                                            } else {
                                                $.sDialog({
                                                    skin: "red",
                                                    content: result.datas.error,
                                                    okBtn: false,
                                                    cancelBtn: false
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }

                });
                //收藏／取消收藏
                $(".collect").click(function () {
                    if ($(this).hasClass('active')) {
                        if (dropFavoriteGuess(gs_id)) {
                            $(this).removeClass('active');
                        }
                    } else {
                        if (favoriteGuess(gs_id)) {
                            $(this).addClass('active');
                        }
                    }
                });

                if (isWeiXin) {
                    var shareData = {
                        title: data.guess.guess_info.gs_name + '-艺诺趣猜',
                        desc: data.guess.goods_jingle,
                        link: window.location.href,
                        imgUrl: data.banner.list[0],
                    };
                    wx_config(shareData);
                }
                //APP分享参数
                appShareSet(shareData);
            }
        });
    }
});