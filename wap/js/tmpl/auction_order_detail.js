$(function(){
    var key = getCookie('key');
    if(!key){
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
    }
    template.helper('getLocalTime', function (nS) {
        var d = new Date(parseInt(nS) * 1000);
        var s = '';
        s += d.getFullYear() + '-';
        s += (d.getMonth() + 1) + '-';
        s += d.getDate() + '    ';
        s += d.getHours() + ':';
        s += d.getMinutes() + ':';
        s += d.getSeconds();
        return s;
    });

    $.getJSON(
        ApiUrl + '/index.php?act=member_auction_order&op=order_info',
        {
            key:key,
            margin_id:getQueryString('margin_id'),
            auction_order_id: getQueryString('auction_order_id')
        },
        function(result) {
        result.datas.order_info.WapSiteUrl = WapSiteUrl;
        //$('#order-info-container').html(template.render('order-info-tmpl',result.datas.order_info));
            console.log(result);
            // 取消
        $(".cancel-order").click(cancelOrder);
        // 收货
        $(".sure-order").click(sureOrder);
        // 查看物流
        $('.viewdelivery-order').click(viewOrderDelivery);

        $.ajax({
            type: 'get',
            url: ApiUrl + "/index.php?act=member_auction_order&op=get_current_deliver",
            data:{key:key,order_id:getQueryString("auction_order_id"), margin_id: getQueryString('margin_id')},
            dataType:'json',
            success:function(result) {
                //检测是否登录了
                console.log(result)
                checkLogin(result.login);

                var data = result && result.datas;
                if (data.deliver_info) {
                    $("#delivery_content").html(data.deliver_info.context);
                    $("#delivery_time").html(data.deliver_info.time);
                }
            }
        });
    });

    //取消订单
    function cancelOrder(){
        var order_id = $(this).attr("order_id");

        $.sDialog({
            content: '确定申请退款？',
            okFn: function() { cancelOrderId(order_id); }
        });
    }

    function cancelOrderId(order_id) {
        $.ajax({
            type:"post",
            url:ApiUrl+"/index.php?act=member_auction_order&op=order_cancel",
            data:{order_id:order_id,key:key},
            dataType:"json",
            success:function(result){
                console.log(result.datas);
                if(result.datas == 0){
                    reset = true;
                    location.reload();
                } else {
                    $.sDialog({
                        skin:"red",
                        content:result.datas.error,
                        okBtn:false,
                        cancelBtn:false
                    });
                }
            }
        });
    }

    //确认订单
    function sureOrder(){
        var order_id = $(this).attr("order_id");

        $.sDialog({
            content: '确定收到了货物吗？',
            okFn: function() { sureOrderId(order_id); }
        });
    }

    function sureOrderId(order_id) {
        $.ajax({
            type:"post",
            url:ApiUrl+"/index.php?act=member_auction_order&op=order_receive",
            data:{order_id:order_id,key:key},
            dataType:"json",
            success:function(result){
                if(result.datas == 0){
                    window.location.reload();
                }
            }
        });
    }

    // 查看物流
    function viewOrderDelivery() {
        var orderId = $(this).attr('order_id');
        var margin_id = $(this).attr('margin_id');
        location.href = WapSiteUrl + '/tmpl/member/auction_order_delivery.html?order_id=' + orderId + '&margin_id=' + margin_id;
    }
});