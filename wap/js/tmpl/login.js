/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function() {
    var referurl = document.referrer;//上级网址
    var key = getCookie('key');
    var wap=getQueryString("wap"); //新带的参数 618
    //alert(key)
    if (key) {
        if(referurl.search("192.168.97.172" || "http://whk.paimai.yinuovip.com/" || "http://paimai.yinuovip.com/") !== -1 ){
          location.href = referurl;
           return;
        }else if ( getQueryString('warnTitle') !== ' ' ){
            if (referurl.indexOf("?")) {
                location.href = referurl + '&warnTitle='+ getQueryString('warnTitle');
                return;
            }
            location.href = referurl + '?warnTitle='+ getQueryString('warnTitle');
            return;
        } else {
            window.location.href = WapSiteUrl + '/tmpl/member/member.html';
            return;
        }

    }
    else {
      // window.location.href="login.html"
    }
    var unionid = getQueryString('unionid');
    $("#unionid").val(unionid);
    if (unionid == '' && isWeiXinLoad()){
        if(referurl.search("192.168.97.172" || "http://whk.paimai.yinuovip.com/" || "http://paimai.yinuovip.com/") !== -1){
        }else {
            location.href = ApiUrl+'/index.php?act=connect&op=index&redirect_url='+referurl;

        }
    }
    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state', function(result){
        var allow_login = 0;
        if (result.datas.pc_qq == '1') {
            allow_login = 1;
            $('.qq').show();
        }
        if (result.datas.pc_sn == '1') {
            allow_login = 1;
            $('.weibo').show();
        }
        if ((navigator.userAgent.indexOf('Appyinuo') !== -1) && result.datas.connect_wap_wx == '1') {
            allow_login = 1;
            $('.wx_login').show();
        }
        if (allow_login) {
            $('.quick_login').show();
        }
    });

    $.sValid.init({
        rules:{
            username:"required",
            userpwd:"required"
        },
        messages:{
            username:"用户名必须填写！",
            userpwd:"密码必填!"
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                $.map(eId,function (idx,item){
                   $('#'+idx).parents('.login_form_input_panpel').addClass('error');
                   $('#'+idx).attr('placeholder',eMsg[idx+'_required']);
                });
            }else{
                $('#'+eId).parents('.login_form_input_panpel').removeClass('error');
            }
        }
    });

    //设置开关，用于提交间歇
    var allow_submit = true;
    $(document).keydown(function(event){
        if(event.keyCode==13){
            $("#loginbtn").click();
        }
    });
    //会员登陆
    $('#loginbtn').click(function() {
        var username = $('#username').val();
        var pwd = $('#userpwd').val();
        var unionid = $("#unionid").val();
        var client = 'wap';
        if($.sValid()){
            if (allow_submit) {
                allow_submit = false;
                $('.login_form_btn').addClass('active')
                //此处设置按钮不可以点击与不可点击的样式
            } else {
                return false;
            }
            setTimeout(function(){
                allow_submit = true;
            },3000);

            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=login",
                data:{username:username,password:pwd,client:client,unionid:unionid},
                dataType:'json',
                success:function(result){
                    console.log(result.datas)
                    if(!result.datas.error){
                        if(typeof(result.datas.key)=='undefined'){
                            return false;
                        }else{
                            var expireHours = 0;
                            // var expireHours = 0;
                            if ($('#checkbox').prop('checked')) {
                                expireHours = 188;
                            }
                            // 更新cookie购物车
                            updateCookieCart(result.datas.key);
                            addCookie('username',result.datas.username, expireHours);
                            addCookie('key',result.datas.key, expireHours);
                            addCookie('usertype',result.datas.usertype, expireHours);
                            setCookieYinouvip('username',result.datas.username);
                            setCookieYinouvip('key',result.datas.key);
                            setCookieYinouvip('usertype',result.datas.usertype);

                            location.href = referurl+wap;
                        }
                        //关闭报错
                        lm_errorTipsHide();
                    }else{
                        // errorTipsShow('<p>' + result.datas.error + '</p>');
                        lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                        //提示报错
                    }
                },
                error:function(xhr){
                    //需要模拟服务器错误等情况
                    //服务器错误、无响应、正在连接中等。
                    console.log(xhr)
                }
            });
        }
        return false;
    });
    //点击注册
    $("#register").click(function(){
        window.location.href="register.html"

    })
    if(event.keyCode==13){
        $("#loginbtn").click();
    }
    //第三方登录
    $('.weibo').click(function(){
        location.href = ApiUrl+'/index.php?act=connect&op=get_sina_oauth2';
    })
    $('.qq').click(function(){
        location.href = ApiUrl+'/index.php?act=connect&op=get_qq_oauth2';
    })
    $('.wx').click(function(){//此处APP支持第三方登录
        //location.href = ApiUrl+'/index.php?act=connect&op=index';
    })
});

//网页点击登陆按钮触发事件
function wxLoginClick(){
    var str = jsObj.wxLogin();
}
//微信联合登陆成功返回值

function wxLoginClickResponse(str){
    alert('wx')
    if (typeof(str) == "string"){
        str = JSON.parse(str);
    }
    var weixin_info = {'unionid':str.unionid,'nickname':str.nickname,openid:str.openid,headimgurl:str.headimgurl,sex:str.gender}
    if(JSON.stringify(weixin_info) == "{}"){
        return false;
    }
    $.ajax({
        type:'post',
        url:ApiUrl+"/index.php?act=connect&op=appAuthorizeLogin",
        data:weixin_info,
        dataType:'json',
        success:function(result){
            window.location.href = result.datas;
        }
    });
}

