var gs_id = getQueryString("gs_id");

$(function(){
    var key = getCookie('key');
    if (!key) {
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
    if (!gs_id){
        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
        return;
    }
    $.ajax({
        url: ApiUrl + "/index.php?act=member_guess&op=getGuessInfo",
        type: "post",
        data: {gs_id: gs_id, key: key},
        dataType: "json",
        success: function (result) {
            var data = result.datas;
            if (!data.error) {
                //渲染模板
                var html = template.render('content', data);
                $("#content_html").html(html);
            }
        }
    });

    $('#submit').click(function(){
        var guess_offer = $('#guess_offer').val();
        $.ajax({
            url: ApiUrl + "/index.php?act=member_guess&op=add_guess_offer",
            type: "post",
            data: {guess_offer:guess_offer,gs_id: gs_id, key: key},
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    $.sDialog({skin: "green", content: "出价成功", okBtn: false, cancelBtn: false});
                    return_val = true;
                    window.location.href = WapSiteUrl+'/tmpl/qucai/qucai_list.html';
                } else {
                    $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                }
            }
        });
    });

});
