/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

var page = pagesize;
var curpage = 1;
var hasmore = true;
var footer = false;
var reset = true;
var keyword = decodeURIComponent(getQueryString('keyword'));
var key = getCookie('key');

$(function () {
    var param = {keyword:'',artist_classify:'',order:"artist_sort",curpage:curpage,page:page,reset:true};
    var strArry=['国画','西画','雕塑','陶瓷','玉雕'];
    function initPage() {
        if (param.reset) {
            param.curpage = 1;
            hasmore = true;
        }

        // param.page = page;
        // param.curpage = curpage;
        // param.artist_classify = 1;//艺术家分类ID,一共有4中分类:{1:国画;2:西画;3:雕塑;4:陶瓷}
        // param.order = 'artist_click';//排序有{artist_vendue_id:最新排序;artist_click:热度排序}

        $('.loading').remove();

        if (!hasmore) {
            return false;
        }
        hasmore = false;
        if (keyword != '') {
            param.keyword = keyword;
        }
        if (key != '') {
            param.key = key;
        }

        $.getJSON(ApiUrl + '/index.php?act=artist&op=artist_list' + window.location.search.replace('?', '&'), param, function (result) {
            if (!result) {
                result = [];
                result.datas = [];
                result.datas.artist_list = [];
            }
            var data = result.datas;
            param.curpage++;
            hasmore = result.hasmore;
            var html = template.render('artist_list', data);
            if (param.reset) {
                param.reset = false;
                $("#artist_list_wrapper").html(html);
            } else {
                $("#artist_list_wrapper").append(html);
            }
            $(".image_lazy_load").nc_lazyload();
            if (data.artist_list.length > 0) {
                for (var i = 0; i < data.artist_list.length; i++) {
                    //显示收藏按钮
                    if (data.artist_list[i].is_favorate) {
                        $("#store_notcollect_" + data.artist_list[i].store_id).addClass('lm_hidden');
                        $("#store_collected_" + data.artist_list[i].store_id).removeClass('lm_hidden');
                    } else {
                        $("#store_notcollect_" + data.artist_list[i].store_id).removeClass('lm_hidden');
                        $("#store_collected_" + data.artist_list[i].store_id).addClass('lm_hidden');
                    }
                }
            }
        });
    }


    //收藏店铺
    $(".fllow").live('click', function () {

        var store_id = $(this).attr('nc_store_id');
        //添加收藏
        var f_result = favoriteStore(store_id);
        if (f_result) {
            $("#store_notcollect_" + store_id).addClass('lm_hidden');
            $("#store_collected_" + store_id).removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 0 ? t + 1 : 1;
        }
        $.showPreloader('加载中...');
        setTimeout(function () {
            $.hidePreloader();
        }, 500);
    });
    //取消店铺收藏
    $(".notfllow").live('click', function () {
        var store_id = $(this).attr('nc_store_id');
        //取消收藏
        var f_result = dropFavoriteStore(store_id);
        if (f_result) {
            $("#store_collected_" + store_id).addClass('lm_hidden');
            $("#store_notcollect_" + store_id).removeClass('lm_hidden');
            var t;
            var favornum = (t = parseInt($("#store_favornum_hide").val())) > 1 ? t - 1 : 0;
        }
        $.showPreloader('加载中...');
        setTimeout(function () {
            $.hidePreloader();
        }, 500);
    });

    initPage();
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });
    if ($('.lm_change_list') && $('.lm_change_list').length) {
        $('.lm_change_list').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('.artist_list_con').removeClass('lm_big_list');
            } else {
                $(this).addClass('active');
                $('.artist_list_con').addClass('lm_big_list');
            }
            return false;
        });
    }
    if (document.body.scrollTop === 0) {
        $('.lm_gotop').hide();
    }

    $(window).scroll(function (event) {
        if (document.body.scrollTop > 10) {
            $('.lm_gotop').show();
        } else {
            $('.lm_gotop').hide();
        }
    });
    $('.lm_gotop').click(function (e) {
        $('html,body').scrollTop(0);
        e.preventDefault();
    });

    $('.lm_page1 .lm_artlist_top_right .lm_all_category').click(function () {
        $('.lm_page1,#footer').addClass('lm_hidden');
        $('body').addClass('nopadding');
        $('.lm_screen').removeClass('lm_hidden');
        $('.lm_screen ul li').removeClass('active');
        if(param.artist_classify!==''){
            $('.lm_screen ul li').eq(param.artist_classify-1).addClass('active');
        }
        return false;
    });

    //筛选部分
    $('.lm_screen ul li').click(function () {
        var artist_classify=$(this).attr('artist_classify');
        param.artist_classify=artist_classify;
        $('.lm_screen ul li').removeClass('active');
        $(this).addClass('active');
        // $.UrlUpdateParams(window.location.href, "mid", 11111),
        return false;
    });

    $('.lm_screen_section .lm_art_close').click(function () {
        $('.lm_page1,#footer').removeClass('lm_hidden');
        $('body').removeClass('nopadding');
        $('.lm_screen').addClass('lm_hidden');
    });
    //点击提交时
    $('.lm_screen_result .lm_screen_setting_ok').click(function(){
        param.curpage=1;
        param.reset=true;
        initPage();
        $('.lm_screen_btn span').text(strArry[param.artist_classify-1]);
        $('.lm_page1,#footer').removeClass('lm_hidden');
        $('body').removeClass('nopadding');
        $('.lm_screen').addClass('lm_hidden');
        return false;
    });
    $('.lm_screen_result .lm_screen_setting_reset').click(function(){
        param.artist_classify='';
        param.curpage=1;
        param.reset=true;
        $('.lm_screen_btn span').text('筛选');
        $('.lm_page1,#footer').removeClass('lm_hidden');
        $('body').removeClass('nopadding');
        $('.lm_screen').addClass('lm_hidden');
        initPage();
        return false;
    });

    $('.lm_artlist_top_left').click(function () {
        $(this).toggleClass('active');
    });
    //关键词检索
    $('.qll_search_btn').click(function () {
        param.curpage=1;
        param.reset=true;
        param.keyword=$('.search_keyword').val();
        initPage();
        $('.search_keyword').blur();
        return false;
    });

    $('.lm_artlist_top_left .lm_category li').click(function () {
        $('.lm_artlist_top_left .lm_category ul li').removeClass('active');
       $('.lm_artlist_top_left span').text($(this).text());
       $(this).addClass('active');
        param.curpage=1;
        param.reset=true;
        param.order=$(this).attr('order');
        $('.lm_artlist_top_left').removeClass('active');
        initPage();
        return false;
    });
    if(isWeiXin){
                var shareData = {
                    title: "艺诺之星,邀您共赴艺术盛宴",
                    desc: "艺术不再为少数人所享用,它属于每一个人.而他们将为您实现.....",
                    link: window.location.href,
                    imgUrl: dataUrl+'/upload/logo.jpg',
                };
                wx_config(shareData);
            };
    //APP分享参数
    appShareSet(shareData);
    $('.qll_search_tip').focus(function () {
        $('.qll_search_btn i').css('color','#e1dac5');
    })
    $('.qll_search_tip').blur(function () {
        $('.qll_search_btn i').css('color','#000');
    })
});

