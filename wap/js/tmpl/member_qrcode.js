$(function (){
    var invite_code = getQueryString('invite_code');
    var key = getCookie('key');
    var bind_uri = WapSiteUrl + '/tmpl/member/register.html?invite_code=' + invite_code;
    // console.log(invite_code)
    if (invite_code) {
        // console.log(111)
        $('.qrcode-content_lm').html('<img src="' + ApiUrl + "/index.php?act=member_index&op=myInviteCodeImg&invite_code=" + invite_code + '"/>');
        $('#invite_code').html(invite_code)
        $('#bind_uri').val(bind_uri);
        wx_config(invite_code);
    } else {
        if (key) {
            // console.log(222)
            $.ajax({
                type: "get",
                url: ApiUrl + "/index.php?act=member_qrcode",
                data: {key: key},
                dataType: "json",
                success: function (data) {
                    // console.log(data)
                    location.href=WapSiteUrl + '/tmpl/member/member_qrcode.html?invite_code='+data.datas.invite_code;
                }
            })
        } else {
            window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        }
    }

})
