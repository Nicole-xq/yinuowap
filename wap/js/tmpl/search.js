/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function(){
    Array.prototype.unique = function()
    {
        var n = [];
        for(var i = 0; i < this.length; i++)
        {
            if (n.indexOf(this[i]) == -1) n.push(this[i]);
        }
        return n;
    }
    var keyword = decodeURIComponent(getQueryString('keyword'));
    if (keyword) {
        $('#keyword').val(keyword);writeClear($('#keyword'));
    }
    template.helper('$buildUrl',buildUrl);
    $.getJSON(ApiUrl + '/index.php?act=index&op=search_key_list', function(result) {
        var data = result.datas;
        console.log(data);
        data.WapSiteUrl = WapSiteUrl;
        $('#history_list').html(template.render('history_list_con',data));
    })

    $('#search_btn').click(function(){
        if ($('#keyword').val() == '') {
            window.location.href = buildUrl('keyword',getCookie('deft_key_value') ? getCookie('deft_key_value') : '');
        } else {
            window.location.href = buildUrl('keyword',$('#keyword').val());
        }
    });

    $('#qll_search').on('submit',function () {
        if ($('#keyword').val() == '') {
            window.location.href = buildUrl('keyword',getCookie('deft_key_value') ? getCookie('deft_key_value') : '');
            return false;
        } else {
            window.location.href = buildUrl('keyword',$('#keyword').val());
            return false;
        }
    });

    var $dom=$('.lm_search .lm_search_left'),
        $con=$('.lm_search .lm_category');
    $dom.click(function () {
       if($(this).hasClass('active')){
           $(this).removeClass('active');
       }else{
           $(this).addClass('active');
       }
    });
    $con.find('li').click(function () {
        $dom.find('.text').text($(this).text());
        var val=$(this).attr('data-val');
        if(val == 1){
            $('#search_btn').click(function(){
                if ($('#keyword').val() == '') {
                    window.location.href = buildUrl('auction',getCookie('deft_au_key_value') ? getCookie('deft_au_key_value') : '');
                } else {
                    window.location.href = buildUrl('auction',$('#keyword').val());
                }
            });
        }else if(val == 2){
            $('#search_btn').click(function(){
                if ($('#keyword').val() == '') {
                    window.location.href = buildUrl('keyword',getCookie('deft_key_value') ? getCookie('deft_key_value') : '');
                } else {
                    window.location.href = buildUrl('keyword',$('#keyword').val());
                }
            });
        }else if(val == 3){
            $('#search_btn').click(function(){
                if ($('#keyword').val() == '') {
                    window.location.href = buildUrl('artist',getCookie('deft_key_value') ? getCookie('deft_key_value') : '');
                } else {
                    window.location.href = buildUrl('artist',$('#keyword').val());
                }
            });
        }else{
            $('#search_btn').click(function(){
                if ($('#keyword').val() == '') {
                    window.location.href = buildUrl('keyword',getCookie('deft_key_value') ? getCookie('deft_key_value') : '');
                } else {
                    window.location.href = buildUrl('keyword',$('#keyword').val());
                }
            });
        }
    })
});