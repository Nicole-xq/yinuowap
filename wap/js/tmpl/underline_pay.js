var pay_sn = getQueryString("pay_sn");
$(function(){
	var key = getCookie('key');
	if(!key){
		window.location.href = WapSiteUrl+'/tmpl/member/login.html';
	}
    $.getJSON(ApiUrl + '/index.php?act=payment&op=get_admin_underline_config',{key:key}, function(result) {
    	result.datas.WapSiteUrl = WapSiteUrl;
        if (result.code == '200' && result.datas.payment_state == 1) {
            var payment_info = result.datas.payment_config;
            $('#bank_card').val(payment_info.number);
            $('#bank').val(payment_info.bank);
            $('#owner').val(payment_info.card_user);
        } else {
            window.location.href = WapSiteUrl;
        } 
        // 图片上传
        $('input[name="refund_pic"]').ajaxUploadImage({
            url : ApiUrl + "/index.php?act=member_underline&op=upload_pic",
            data:{key:key},
            start :  function(element){
                element.parent().after('<div class="upload-loading"><i></i></div>');
                element.parent().siblings('.pic-thumb').remove();
            },
            success : function(element, result){
                checkLogin(result.login);
                if (result.datas.error) {
                    element.parent().siblings('.upload-loading').remove();
                    $.sDialog({
                        skin:"red",
                        content:'图片尺寸过大！',
                        okBtn:false,
                        cancelBtn:false
                    });
                    return false;
                }
                element.parent().after('<div class="pic-thumb"><img src="'+result.datas.pic+'"/></div>');
                element.parent().siblings('.upload-loading').remove();
                element.parents('a').next().val(result.datas.file_name);
            }
        });

        $('.btn-l').click(function(){
            var _form_param = $('form').serializeArray();
            var param = {};
            param.key = key;
            param.pay_sn = pay_sn;
            param.pay_voucher = $('#voucher').val();
            $.ajax({//获取区域列表
                type:'post',
                url:ApiUrl+'/index.php?act=member_underline&op=pay_underline_order',
                data:param,
                dataType:'json',
                async:false,
                success:function(result){
                    checkLogin(result.login);
                    if (result.datas.error) {
                        $.sDialog({
                            skin:"red",
                            content:result.datas.error,
                            okBtn:false,
                            cancelBtn:false
                        });
                        return false;
                    }
                    $.sDialog({
                            skin:"green",
                            content:"凭证已上传，等待管理员审核",
                            okBtn:false,
                            cancelBtn:false
                    });
                    window.location.href = WapSiteUrl + '/tmpl/member/order_list.html';
                }
            });
        });
    });
});