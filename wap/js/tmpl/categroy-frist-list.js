$(function() {
    var myScroll;
    $("#header").on('click', '.header-inp', function(){
        location.href = WapSiteUrl + '/tmpl/search.html';
    });
    $.getJSON(ApiUrl+"/index.php?act=goods_class", function(result){
		var data = result.datas;
		data.WapSiteUrl = WapSiteUrl;
		var html = template.render('category-one', data);
		$("#categroy-cnt").html(html);


		myScroll = new IScroll('#categroy-cnt', { mouseWheel: true, click: true });




        get_brand_recommend();


        function click_pub(dom){
            $('.pre-loading').show();
            dom.parent().addClass('selected').siblings().removeClass("selected");
            var gc_id = dom.attr('date-id');
            $.getJSON(ApiUrl + '/index.php?act=goods_class&op=get_child_all', {gc_id:gc_id}, function(result){
                var data = result.datas;
                data.WapSiteUrl = WapSiteUrl;
                var html = template.render('category-two', data);
                $("#categroy-rgt").html(html);
                $('.pre-loading').hide();
                new IScroll('#categroy-rgt', { mouseWheel: true, click: true });
            });
            myScroll.scrollToElement(document.querySelector('.categroy-list li:nth-child(' + (dom.parent().index()+1) + ')'), 1000);

        }

        $('#categroy-cnt').on('click','.category', function(){
            click_pub($(this));
        });

        $('#categroy-cnt').on('click','.brand', function(){
            $('.pre-loading').show();
            get_brand_recommend();
        });
        //lm 17.5.22
        var index= document.location.search.replace('?lm_type_list=','');
        if(index){
            click_pub($('.categroy-list li').eq(index).find('a'));
        }
	});
	



});

function get_brand_recommend() {
    $('.category-item').removeClass('selected');
    $('.brand').parent().addClass('selected');
    $.getJSON(ApiUrl + '/index.php?act=brand&op=recommend_list', function(result){
        var data = result.datas;
        data.WapSiteUrl = WapSiteUrl;
        var html = template.render('brand-one', data);
        $("#categroy-rgt").html(html);
        $('.pre-loading').hide();
        new IScroll('#categroy-rgt', { mouseWheel: true, click: true });
    });
}

