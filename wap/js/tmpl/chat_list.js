$(function(){
    var key = getCookie('key');
    var reset = true;
    var hasmore = true;
    var param = {key:key,curpage:1,page:15};

    if(!key){
        location.href = 'login.html';
    }
    template.helper('isEmpty', function(o) {
        for (var i in o) {
            return false;
        }
        return true;
    });

    //通知列表页
    function notictList() {
        $.ajax({
            type:'get',
            url:ApiUrl+'/index.php?act=member_chat&op=message_list',
            data:param,
            dataType:'json',
            success:function (result) {
                if(!hasmore){
                    return false;
                }
                param.curpage++;
                var data = result.datas;
                console.log(result);
                var noticeReadHtml = template.render('noticeReadScript', data);
                var messageReadHtml = template.render('messageReadScript', data);
                var noticeNumHtml = template.render('qllNoticeNumScript', data);
                var noticeListHtml = template.render('noticeListScript', data);
                if(reset){
                    $("#qllNoticeNum").html(noticeNumHtml);
                    $("#noticeList").html(noticeListHtml);
                    $("#noticeRead").html(noticeReadHtml);
                    $("#messageRead").html(messageReadHtml);
                    reset=false;
                }else {
                    $("#noticeList").append(noticeListHtml);
                }
                hasmore = result.hasmore;


                $('.qll_notice_num li').click(function () {
                    if($(this).find('span').hasClass('active')){
                        return false;
                    }
                    var index = $(this).index();
                    $('.qll_notice_num li span').removeClass('active');
                    $(this).find('span').addClass('active');
                    if(index===0){
                        $('.nctouch-notice-list').show();
                        $('.nctouch-message-list').hide();
                    }else {
                        $('.nctouch-message-list').show();
                        $('.nctouch-notice-list').hide();
                        messageList();
                    }
                });
                // $('.nctouch-notice-list li a p').each(function(){
                //     var maxwidth=20;
                //     if($(this).text().length>maxwidth){
                //         $(this).text($(this).text().substring(0,maxwidth));
                //         $(this).html($(this).html()+'…');
                //     }
                // });

                $('.nctouch-notice-list .qll_all_read').click(function () {
                    qllDel(2);
                    // $(this).hide();
                });
                $('.nctouch-message-list .qll_all_read').click(function () {
                    qllDel(1);
                    // $(this).hide();
                });
                $('#noticeList li').click(function () {
                    location.reload();
                });
            }
        });
    }
    notictList();
    function qllDel(num) {
        $.ajax({
            type: 'get',
            url: ApiUrl+'/index.php?act=member_chat&op=setReadState',
            data: {key:key,type:num},
            dataType:'json',
            success:function (result) {
                // console.log(result);
                if (result.code == 200) {
                    location.reload();
                }

            }
        });
    }

    function messageList() {

        $.ajax({
            type: 'post',
            url: ApiUrl+'/index.php?act=member_chat&op=get_user_list',
            data: {key:key,recent:1},
            dataType:'json',
            success: function(result){
                checkLogin(result.login);
                var data = result.datas;
                //渲染模板
                $("#messageList").html(template.render('messageListScript', data));


                // $('.msg-list-del').click(function(){
                //     var t_id = $(this).attr('t_id');
                //     $.ajax({
                //         type: 'post',
                //         url: ApiUrl+'/index.php?act=member_chat&op=del_msg',
                //         data: {key:key,t_id:t_id},
                //         dataType:'json',
                //         success: function(result){
                //             if (result.code == 200) {
                //                 location.reload();
                //             } else {
                //                 $.sDialog({
                //                     skin:"red",
                //                     content:result.datas.error,
                //                     okBtn:false,
                //                     cancelBtn:false
                //                 });
                //                 return false;
                //             }
                //         }
                //     });
                // });

                $('.nctouch-message-list li p').each(function(){
                    var maxwidth=35;
                    if($(this).text().length>maxwidth){
                        $(this).text($(this).text().substring(0,maxwidth));
                        $(this).html($(this).html()+'…');
                    }
                });
            }
        });
    }
    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            notictList();
        }
    });


});