$(function () {
    var key = getCookie('key');
    var param = {page: 10, curpage: 1, order: '', key: key};
    var hasmore = true;
    var keyword = decodeURIComponent(getQueryString('keyword'));
    var sales_model = getQueryString('sales_model');
    var gc_id = getQueryString('gc_id');
    var a_id = getQueryString('a_id');
    var b_id = getQueryString('b_id');
    var own_shop = getQueryString('own_shop');
    var ci = getQueryString('ci');
    var myDate = new Date();
    var searchTimes = myDate.getTime();
    var price_from = getQueryString('price_from');
    var price_to = getQueryString('price_to');
    var type = getQueryString('type');
    if(type != '' && (type == 2 || type == 4)){
        param.key = type;
        if(type == 2){
            $('.pub_screen_top_left span').html('热门排序'+ '<i class="pub_arrow"></i>')
        }
    }

    function initPage() {
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        if (gc_id != '') {
            param.gc_id = gc_id;
        } else if (keyword != '') {
            param.keyword = keyword;
        } else if (b_id != '') {
            param.b_id = b_id;
        }else if (a_id != '') {
            param.a_id = a_id;
        }
        if(sales_model !=''){
            param.s_model = sales_model;
        }

        $.getJSON(ApiUrl + '/index.php?act=goods&op=goods_list' + window.location.search.replace('?', '&'), param, function (result) {
            if (!result) {
                result = [];
                result.datas = [];
                result.datas.goods_list = [];
            }
            param.curpage++;
            var data = result.datas;
            var html = template.render('shop_works_list', data);
            $("#shop_works_list_html").append(html);
            hasmore = result.hasmore;
            //收藏
            $("#shop_works_list_html .collect").click(function () {
                var goods_id = $(this).parents('li').attr('goods_id');
                if ($(this).hasClass('active')) {
                    if (dropFavoriteGoods(goods_id, 'goods'))
                        $(this).removeClass('active');
                } else {
                    if (favoriteGoods(goods_id))
                        $(this).addClass('active');
                }
                return false;
            });
        });
    }

    initPage();

    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });


    $('.pub_screen_top_left').click(function () {
        $('.pub_category').toggle();
    });

    $('.pub_screen_top_left .pub_category li').click(function () {
        var order = $(this).attr('data-order'),
            keyword = $(this).attr('data-key');
        var index = $(this).index();
        param.order = order;
        param.key = keyword;
        param.curpage = 1;
        hasmore = true;
        $("#shop_works_list_html").html('');
        initPage();
        // $('.pub_screen_top_left span').html($(this).find('a').text())
        if (index === 2 || index === 3) {
            $('.pub_screen_top_left span').html($(this).find('a').html())
        } else {
            $('.pub_screen_top_left span').html($(this).find('a').text() + '<i class="pub_arrow"></i>')
        }
    });


    $('.pub_change_list').click(function () {
        if ($('.shop_works_list').hasClass('shop_works_list_narrow')) {
            $(this).css('background-image', 'url(../images/lm/lm_artlist_change1.png)');
            $('.shop_works_list').removeClass('shop_works_list_narrow').addClass('shop_works_list_enlarge');
        } else if ($('.shop_works_list ').hasClass('shop_works_list_enlarge')) {
            $(this).css('background-image', 'url(../images/lm/lm_artlist_change.png)');
            $('.shop_works_list').removeClass('shop_works_list_enlarge').addClass('shop_works_list_narrow');
        }
    });

    // search_adv();

    $('#search_adv').click(function () {
        var $dom = $('.shop_works_screen');
        if ($dom.hasClass('lm_hidden')) {
            $dom.removeClass('lm_hidden');
            $('.shop_works_list').addClass('lm_hidden');
        }
    });
    $('.shop_works_screen_top a').click(function () {
        var $dom = $('.shop_works_screen');
        $dom.addClass('lm_hidden');
        $('.shop_works_list').removeClass('lm_hidden');
        return false;
    });

    var classParam = {key: key, op: 'get_class_all'};
    getGoodsClass();

    function getGoodsClass() {
        $.ajax({
            url: ApiUrl + '/index.php?act=goods_class&op=get_class_all',
            type: 'get',
            data: classParam,
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                var html = template.render('one', data);
                $("#one_wrap").html(html);
                $('.close_type').click(function () {
                    $('.shop_works_screen').addClass('lm_hidden');
                    $('.shop_works_list').removeClass('lm_hidden');
                });

                $('.bigType dd span:nth-child(3n)').after('<div class="list_line"></div>');
                $('.bigType dd span:last-of-type ~ div').remove();
                //大分类点击事件
                $('.bigType span').click(function () {
                    var index = $(this).attr('data-index');
                    var content = '';
                    $('.switch').parent('dl').removeClass('active');

                    $.each(data.class_list[index].child, function (i, e) {
                        content += '<span gc_id="' + e.gc_id + '"><i class="iconfont">' + e.gc_name + '</i></span>';
                    });

                    var self = $(this).find('i');
                    $('.smallType dd,#two_wrap').html('');
                    if (self.hasClass('active')) {
                        self.removeClass('active');
                        $('.smallType').addClass('lm_hidden');
                    } else {
                        $('.bigType span i').removeClass('active');
                        self.addClass('active');
                        $('.smallType').removeClass('lm_hidden');
                        $('.smallType dd').html(content);
                        $('.smallType dd span:nth-child(3n)').after('<div class="list_line"></div>');
                        $('.smallType dd span:last-of-type ~ div').remove();
                    }
                    if (!data.class_list[index].child || !data.class_list[index].child.length) {
                        $('.smallType').addClass('lm_hidden');
                        $('.smallType dd,#two_wrap').html('');
                        content = '';
                        return false;
                    }


                    //小分类点击事件
                    $('.smallType span').on('click', function () {
                        var gc_id = $(this).attr('gc_id');
                        var html = '';
                        var self = $(this).find('i');
                        html = '';
                        if (self.hasClass('active')) {
                            self.removeClass('active');
                            $("#two_wrap").html('');
                            return false;
                        } else {
                            $('.smallType span i').removeClass('active');
                            self.addClass('active');
                            $("#two_wrap").html('');
                        }
                        $.getJSON(ApiUrl + '/index.php?act=goods_class&op=get_type', {
                            key: key,
                            gc_id: gc_id
                        }, function (result) {
                            var data = result.datas;
                            html = template.render('two', result);
                            $("#two_wrap").html(html);
                            $('#two_wrap dd span:nth-child(3n)').after('<div class="list_line"></div>');
                            $('#two_wrap dd span:last-of-type ~ div').remove();
                            $('#two_wrap dl span').on('click', function () {
                                var self = $(this).find('i');
                                if (self.hasClass('active')) {
                                    self.removeClass('active');
                                } else {
                                    $(this).parents('dl').find('span i').removeClass('active');
                                    self.addClass('active');
                                }
                                return false;
                            });
                        });
                        return false;
                    });
                    return false;
                });

                if (gc_id) {
                    $.each(data.class_list, function (i, e) {
                        if (gc_id === e.gc_id) {
                            $('.bigType dd span').eq(i).click();
                        }else{
                            $.each(e.child,function (ind,el) {
                                if(gc_id === el.gc_id){
                                    $('.bigType dd span').eq(i).click();
                                    $('.smallType dd span').eq(ind).click();
                                    $('.smallType').addClass('active');
                                }
                            });
                        }
                    })
                }

                // $('.shop_works_screen_content  dt.switch').click(function(){
                $('.shop_works_screen_content').on('click','dt.switch',function () {
                    var $el = $(this).parent('dl');
                    if($el.hasClass('active')){
                        $el.removeClass('active');
                    }else{
                        $el.addClass('active');
                    }
                    return false;
                });

                $('#search_submit').click(function () {
                    param.curpage = 1;
                    param.reset = true;
                    hasmore = true;
                    var len = $('#one_wrap span i.active').length;
                    //如果有小分类则取小分类的gc_id
                    param.gc_id = len > 1 ? $('#one_wrap .smallType span i.active').parent('span').attr('gc_id') :
                        $('#one_wrap .bigType span i.active').parent('span').attr('gc_id');

                    var typeid = '';
                    $('#two_wrap span').each(function (i, e) {
                        if ($(this).find('i').hasClass('active')) {
                            typeid += $(e).attr('a_id') + '_';
                        }
                    });
                    param.a_id = typeid.substring(typeid.lastIndexOf('_'), -1);

                    window.location.href='product_list.html?gc_id='+param.gc_id+'&a_id='+param.a_id;

                    // $('#shop_works_list_html').html('');
                    // initPage();
                    // $('.shop_works_screen').addClass('lm_hidden');
                    // $('.shop_works_list').removeClass('lm_hidden');
                    //
                    return false;
                });
                $('#reset').click(function () {
                    param.curpage = 1;
                    param.reset = true;
                    param.gc_id = '';
                    param.a_id = '';
                    $('.shop_works_screen_content dd span i').removeClass('active');
                    $('.bigType span i').removeClass('active');
                    $('.smallType dd,#two_wrap').html('');
                    $('.smallType').addClass('lm_hidden');
                    return false;
                });
            }

        })
    }
});



