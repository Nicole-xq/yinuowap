/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    var cms_id = getQueryString('cms_id');
    $.ajax({
        url: ApiUrl + "/index.php?act=cms&op=index&cms_id="+cms_id,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            var data = result.datas;
            var tmp_param = template;
            tmp_param.isEscape = false;
            var html = tmp_param.render('cms_activity', data);
            $("#cms_activity_wrapper").html(html);
            var detail = data.detail;
            document.title=detail.special_title;
            if (isWeiXin) {
                var shareData = {
                    title: detail.special_title,
                    desc: detail.special_description,
                    link: window.location.href,
                    imgUrl: location.origin + detail.special_share_logo,
                };
                wx_config(shareData);
            }
            //APP分享参数
            appShareSet(shareData);
        }
    });
});