$(function () {
    var key = getCookie('key');
    var member_id = getQueryString('member_id');
    var hasmore = true;
    var reset = true;
    var url = function (act, op) {
        return ApiUrl + '/index.php?act=' + act + '&op=' + op;
    };
    var param = {key: key,member_id:member_id};
    var param1 = {key: key,form_member_id:member_id,page: 10, curpage: 1};

        $.ajax({
            url:url('member_partner', 'partner_info'),
            type:'post',
            dataType: 'json',
            data: param,
            success:function (result) {
                var data = result.datas;
                var html = template.render('partnerInfo', data);
                $("#partnerInfoHtml").html(html);

                $('.member_bonus').click(function () {
                    hasmore = true;
                    if($(this).hasClass('active')){
                        return false;
                    }
                    param1.curpage=1;
                    $("#bonusInfoDetailHtml").html('');
                    $("#bonusInfoHtml").html('');
                    var index = $(this).index();
                    if($(this).hasClass('active')){
                        return false;
                    }else {
                        $(this).addClass('active');
                        $(this).siblings().removeClass('active');
                    }
                    if(index==0){
                        $('.bonus_info_detail').show();
                        $('.partner_info_detail').hide();
                        //获取奖励金列表
                        bonus_list();
                    }else {
                        $('.bonus_info_detail').hide();
                        $('.partner_info_detail').show();
                        //获取合伙人列表
                        partner_list();
                    }
                });

                //获取奖励金总额
                if(data.commission_sum > 0){
                    bonus_list();
                }

            }
        });

    //奖励金列表
    function bonus_list() {
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.getJSON(url('member_fund','commissionlog'), param1, function(result){
            var data = result.datas;
            var html = template.render('bonusInfo', data);
            if (reset) {
                $("#bonusInfoHtml").html(html);
                reset = false;
            } else {
                $("#bonusInfoHtml").append(html);
            }
            hasmore = result.hasmore;
        });
        param1.curpage++;

    }

    //合伙人列表
    function partner_list() {
        if (!hasmore) {
            return false;
        }
        $.getJSON(url('member_partner','partner_list'), param1, function(result){
            var data = result.datas;
            var html = template.render('bonusInfoDetail', data);
            if (reset) {
                $("#bonusInfoDetailHtml").html(html);
                reset = false;
            } else {
                $("#bonusInfoDetailHtml").append(html);
            }
            hasmore = result.hasmore;
        });
        param1.curpage++;

    }



    //上拉加载
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            if($('.member_partner').hasClass('active')){
                partner_list();
            }else {
                bonus_list();
            }
        }
    });

});