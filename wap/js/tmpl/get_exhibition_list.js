/**
 *
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/03/06
 */

$(function () {
    var key = getCookie('key');

    if (!key) {
        window.location.href = WapSiteUrl + '/tmpl/member/login.html';
        return;
    }

    var param = {key: key},
        hasmore = true,
        curpage = 1,
        page = 13;

    function initPage() {
        if (param.reset) {
            curpage = 1;
            hasmore = true;
        }
        if (!hasmore) {
            return false;
        }
        hasmore = false;
        $.ajax({
            url: ApiUrl + "/index.php?act=exhibition&page=" + page + "&curpage=" + curpage + "",
            type: "post",
            data: param,
            dataType: "json",
            success: function (result) {
                curpage++;
                hasmore = result.datas.page.hasmore;
                var html = template.render('exhibition_list_con', result.datas);

                if (param.reset) {
                    param.reset = false;
                    $("#exhibition_list").html(html);
                } else {
                    $("#exhibition_list").append(html);
                }
            }
        });
    }

    initPage();
    // 下拉加载
    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height() > $(document).height() - 1)) {
            initPage();
        }
    });

});

function get_exhibition(ex_id,invite_code) {
    $.getJSON(
        ApiUrl + '/index.php?act=member_center&op=get_information&key=' + getCookie('key'),
        function (result) {
            if(result.datas.member_using_mobile && result.datas.member_real_name){
                 window.location.href = WapSiteUrl + '/tmpl/member/exhibition.html?ex_id='+ ex_id + '&invite_code=' + invite_code;
            } else {
                //去完善姓名手机号信息
                $(document).tipModal({
                    html: '去完善信息(姓名手机号)',
                    showCloseBtn:'取消',
                    onOkBtn: function () {
                        window.location.href = WapSiteUrl + '/tmpl/member/member_account.html';
                    },
                });
            }
    });
}

function go_exhibition_member_list(ex_id) {
    window.location.href = WapSiteUrl + '/tmpl/member/exhibition_member_list.html?ex_id='+ ex_id + '&key=' + getCookie('key');
}