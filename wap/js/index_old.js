var key = getCookie('key');
$(function() {

    var headerClone = $('#header').clone();
    $(window).scroll(function(){
        if ($(window).scrollTop() <= $('#main-container1').height()) {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('transparent').removeClass('');
            headerClone.prependTo('.nctouch-home-top');
        } else {
            headerClone = $('#header').clone();
            $('#header').remove();
            headerClone.addClass('').removeClass('transparent');
            headerClone.prependTo('body');
        }
    });
    $.ajax({
        url: ApiUrl + "/index.php?act=index",
        type: 'get',
        dataType: 'json',
        async:false,
        success: function(result) {
            var data = result.datas;
            var html = '';
            $.each(data, function(k, v) {
                $.each(v, function(kk, vv) {
                    switch (kk) {
                        case 'adv_list':
                        case 'home3':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'sem':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'store_top':
                            $.each(vv.item, function(k3, v3) {
                                vv.item[k3].url = buildUrl(v3.type, v3.data);
                            });
                            break;
                        case 'home1':
                            vv.url = buildUrl(vv.type, vv.data);
                            break;
                        case 'home2':
                        case 'home4':
                            vv.square_url = buildUrl(vv.square_type, vv.square_data);
                            vv.rectangle1_url = buildUrl(vv.rectangle1_type, vv.rectangle1_data);
                            vv.rectangle2_url = buildUrl(vv.rectangle2_type, vv.rectangle2_data);
                            break;
                        case 'cla':
                            $.each(vv.item, function(k3,v3) {
                                $.each(v3.elem, function(k4,v4) {
                                    v3.elem[k4].e_link = buildUrl(v3.elem[k4].e_type,v3.elem[k4].e_data);
                                })
                                vv.item[k3] = v3;
                            })
                        break;
                        case 'video_list':
                        case 'nav_list':
                    }
                    if (kk == 'sem') {
                        $("#main-container1").html(template.render(kk, vv));
                    } else if(kk == 'adv_list'){
                        $("#main-container3").html(template.render(kk, vv));
                    } else if(kk == 'yinuo_store_id'){
                        return true;
                    }else{
                        html += template.render(kk, vv);
                    }
                    updateEndTime();
                    return false;
                });
            });

            $("#main-container2").html(html);

            get_fav_store();
            $('.adv_list').each(function() {
                if ($(this).find('.item').length < 2) {
                    return;
                }

                Swipe(this, {
                    startSlide: 2,
                    speed: 400,
                    auto: 3000,
                    continuous: true,
                    disableScroll: false,
                    stopPropagation: false,
                    callback: function(index, elem) {},
                    transitionEnd: function(index, elem) {}
                });
            });

        }
    });

    $('.index_like_no').click(function(){
        var collect_num =  $(this).parent().find('.collect_num');
        var store_id = collect_num.attr('data-storeid');
        var f_result = favoriteStore(store_id);
        if (f_result){
            location.reload();
        }
    });

    $('.index_like_selected').click(function(){
        var collect_num =  $(this).parent().find('.collect_num');
        var store_id = collect_num.attr('data-storeid');
        var f_result = dropFavoriteStore(store_id);
        if (f_result){
            location.reload();
        }
    });
    if(isWeiXin){
        var shareData = {
            title: "艺诺商城-国画书法、西画雕塑、珠宝玉翠、紫砂陶瓷尽收眼底...",
            desc: "艺诺商城汇集各界艺术大师,精选海量艺术创作,让您享受足不出户”送上门”的艺术生活",
            link: window.location.href,
            imgUrl: dataUrl+'/upload/logo.jpg',
        };
        wx_config(shareData);
    }

});
function init_get_list(mb_class_id) {
        $('#nav_'+mb_class_id).addClass('selected').parent().siblings().find('a').removeClass('selected');

        loadAuction(mb_class_id);

}

function get_special_list(id){
    $('#special_'+id).addClass('active').siblings().removeClass('active');
    $('#tab'+ id).addClass('active').siblings().removeClass('active');
}

function get_fav_store(){
    var store_ids = '';
    if ($('.collect_num').length < 1) {
        return ;
    }
    $('.collect_num').each(function(){
        store_ids += $(this).attr('data-storeid')+',';
    });   
    store_ids = (store_ids.substring(store_ids.length-1)==',')?store_ids.substring(0,store_ids.length-1):store_ids;
    $.ajax({
        url: ApiUrl + "/index.php?act=member_index&op=fav_store_list&key="+key+"&store_ids="+store_ids,
        type: 'get',
        dataType: 'json',
        async:false,
        success: function(result) {
            if (result.code == 400) {
                return false;
            }
            var store_ids = result.datas;
            $('.collect_num').each(function(){
                if ( $.inArray( $(this).attr('data-storeid'), store_ids ) >= 0) {
                    $(this).parent().parent().find('.index_like').removeClass('index_like_no').addClass('index_like_selected');
                }
            });
        }
    });
}
