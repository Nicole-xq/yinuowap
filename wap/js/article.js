/**
 * Created by ADKi on 2017/4/21 0021.
 */
var ac_id = getQueryString("ac_id");
var article_position = getQueryString("article_position");
$(function(){
    $.getJSON(ApiUrl + '/index.php?act=document&op=get_article&ac_id=' + ac_id + '&article_position=' + article_position, function(result){
        $("#document").html(result.datas.article_content);
        var headTitle = result.datas.article_title;
        var html = '<div class="header-wrap">'
            +'<div class="header-l">'
            +'<a href="javascript:history.go(-1)">'
            +'<i class="back"></i>'
            +'</a></div>'
            +'<h1>'+headTitle+'</h1>'
            +'</div>';
        //渲染页面
        $("#header").html(html);
    });
});