/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */
$(function () {
    // type=1,2,3分别对应点击次数，点赞次数，分享次数
    // id 为该引导页的编号
    // thumbs_up_flag   fasle : 今天没有点赞，true：已点赞
    //share_state：1不显示 2显示 是否显示分享和点赞
    function bootFn() {
        $.ajax({
            url: ApiUrl + "/index.php?act=boot_page&op=getBootPage",
            type: 'get',
            dataType: 'json',
            async: false,
            success: function (result) {
                var data = result.datas,
                    boot_id = data.id;
                $('.boot_modal').remove();
                if(!data.boot_image)return false;
                var html = '';
                var clasFlag = data.share_state == 1 ? 'lm_hidden' : '';
                var thumbs_up_flag = !data.thumbs_up_flag ? '' : 'active';

                html += '<div class="boot_modal">\
                    <div class="boot_mask"></div>\
                    <div class="boot_con">\
                    <a href="' + data.wap_link + '" class="imgs" ><img  class="img" src="' + data.boot_image + '" alt="' + data.title + '"></a>\
                    <div class="nativeShare_close"><img  class="img" src="/wap/images/lm/icon/nativeShare_close.png" alt=""></div>\
                    <div class="boot_setting ' + clasFlag + '">\
                    <a href="javascript:void(0);" class="boot_share boot_icon"><i class="iconfont icon-fenxiang1"></i></a>\
                    <a href="javascript:void(0);" class="boot_thumbs boot_icon ' + thumbs_up_flag + '"><i class="iconfont icon-dianzan1"></i></a>\
                    </div>\
                    </div>\
                    </div>';
                $('body').addClass('pub_overFlow').append(html);
                $('html').addClass('pub_overFlow');
                $('body,html').height($(window).height());
                // var domHeight = $('.boot_con').outerHeight()<$(window).height()?$(window).height():$('.boot_con').outerHeight()+100;
                $('.boot_mask,.nativeShare_close').click(function () {
                    $('.boot_modal').remove();
                    $('body,html').removeClass('pub_overFlow');
                    $('body,html').css('height','auto');
                });
                var browerName  = getBrowerVersion();
                if(browerName=='MobileQQ' || browerName=='Baidu' || browerName=='QQBrowser' || browerName=='UC'){
                    //点击分享 , 仅支持双端 uc 和 qq浏览器
                    $('.boot_modal .boot_share').click(function () {
                        //判断当为微信浏览器 || safari || 手机谷歌浏览器
                        if(navigator.userAgent.indexOf('MicroMessenger') > -1 ){
                            return false;
                        }
                        //注释，这里如果不是qq浏览器或者uc浏览器是无法通过此代码分享成功的，所以此处计算为点击次数，并非真正成功的数据
                        $.post(ApiUrl + "/index.php?act=boot_page&op=recordNum&id=" + boot_id + "&type=3");
                        //发送分享次数
                        $('#nativeShare').remove();
                        $('body').append('<div id="nativeShare">' +
                            '<div class="list_con">' +
                            '<div class="label">分享到</div>' +
                            '<div class="list clearfix">' +
                            '<span data-app="weixin" class="nativeShare weixin" ><i></i>微信好友</span>' +
                            '<span data-app="weixinFriend" class="nativeShare weixin_timeline"><i></i>朋友圈</span>' +
                            '<span data-app="QQ" class="nativeShare qq"><i></i>QQ好友</span>' +
                            '<span data-app="sinaWeibo" class="nativeShare weibo"><i></i>新浪微博</span>' +
                            '</div>' +
                            '</div>' +
                            '<div class="nativeShare_mask"></div>' +
                            '</div>');

                        $('.nativeShare_mask').click(function () {
                            $('#nativeShare').remove();
                        });
                        // 先创建一个实例
                        var nativeShare = new NativeShare();
                        var shareData = {
                            icon:  data.boot_image,// 图片
                            link: data.wap_link,// 分享的网页链接
                            title: data.title,// 标题
                            desc: data.share_desc,// 描述
                            from: data.wap_link, // 来源
                            // 不要过于依赖以下两个回调，很多浏览器是不支持的
                            success: function() {
                                // alert('success')
                            },
                            fail: function() {
                                // alert('fail')
                            }
                        }
                        nativeShare.setShareData(shareData)

                        function call(command) {
                            try {
                                nativeShare.call(command)
                            } catch (err) {
                                // 如果不支持，你可以在这里做降级处理
                                // alert(err.message)
                            }
                        }

                        $('#nativeShare .weixin').click(function(){
                            call('wechatFriend');
                        });
                        $('#nativeShare .weixin_timeline').click(function(){
                            call('wechatTimeline');
                        });
                        $('#nativeShare .qq').click(function(){
                            call('qqFriend');
                        });
                        $('#nativeShare .weibo').click(function(){
                            call('weibo');
                        });
                        return false;
                    });
                }else{

                    $('.boot_setting').append('<img style="display: none;" class="suport_icon" src="images/lm/icon/suport_icon.png"/>');
                    $('.suport_icon').click(function () {
                        $('.suport_icon').hide();
                    });
                    $('.boot_share').click(function(){
                        $('.suport_icon').toggle();
                    })
                }

                //点赞功能
                $('.boot_modal .boot_thumbs').click(function () {
                    if (!$(this).hasClass('active')) {
                        var _this = $(this);
                        $.post(ApiUrl + "/index.php?act=boot_page&op=recordNum&id=" + boot_id + "&type=2", '', function (result) {
                            var data = $.parseJSON(result);
                            _this.addClass('active');
                        });
                    }
                    return false;
                });
                //统计点击链接的次数
                $('.boot_modal .boot_con a.imgs').click(function () {
                    $.post(ApiUrl + "/index.php?act=boot_page&op=recordNum&id=" + boot_id + "&type=1");
                    window.location.href=$(this).attr('href');
                    return false;
                })
            }
        });
    }
    var bootCookie = getCookie('bootCookie');
    //bootCookie为false则显示，否则隐藏，24小时cookie，不论是否登录
    if (!bootCookie) {
        bootFn();
        addCookie('bootCookie', true, 24)
    }
});


