/**
 * Created by PhpStorm.
 * User:  lm
 * Date:
 */

$(function () {
    var store_id=getQueryString('store_id');//86
    // 推荐艺术家列表
    $.ajax({
        url:ApiUrl+'/index.php?act=store&op=special_store_brand_list',
        type:'POST',
        dataType:'json',
        data:{store_id:store_id,limit :99},
        async:false,
        success:function (result) {
            var data = result.datas;
            if(!$("#lm_enjoy_artist").length||!$("#lm_enjoy_artist_con").length){return false;}
            $("#lm_enjoy_artist").html(template.render('lm_enjoy_artist_con', data));
            var swiper = new Swiper('.lm_enjoy_artist_wrapper', {
                slidesPerView: 4,
                paginationClickable: true,
            });
        }
    });
});