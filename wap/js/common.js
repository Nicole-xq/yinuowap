function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return r[2];
    return '';
}

function addCookie(name, value, expireHours) {
    var cookieString = name + "=" + escape(value) + "; path=/";
    //判断是否设置过期时间
    if (expireHours > 0) {
        var date = new Date();
        date.setTime(date.getTime() + expireHours * 3600 * 1000);
        cookieString = cookieString + ";expires=" + date.toGMTString();
    }
    document.cookie = cookieString + ";domain=.yinuovip.com";
}

// function setCookieYinouvip(c_name, value, expiredays) {
//     var exdate = new Date();
//     exdate.setDate(exdate.getDate() + expiredays);
//     // alert(exdate.getDate() + expiredays);
//     document.cookie =
//         c_name +
//         "=" +
//         escape(value) +
//         (expiredays == null ? "" : ";expires=" + exdate.toGMTString()) +
//         ";path=/;domain=.yinuovip.com";
// }

 function setCookieYinouvip(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    // alert(exdate.getDate() + expiredays);
    document.cookie =
        c_name +
        "=" +
        escape(value) +
        (expiredays == null ? "" : ";expires=" + exdate.toGMTString()) +
        ";path=/;domain=.yinuovip.com";
}
//获取参数
var wap=getQueryString("wap"); //获取新带的参数
if (wap != "undefined" && wap != null && wap != "") {
    setCookieYinouvip('wap', wap);
}
function getCookie(name) {
    var strcookie = document.cookie;
    var arrcookie = strcookie.split("; ");
    for (var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == name)return unescape(arr[1]);
    }
    return null;
}

function delCookie(name) {//删除cookie
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null) document.cookie = name + "=" + cval + "; path=/;expires=" + exp.toGMTString();
}




function SetCookieYinuoVip(name, value) {
    console.log(name, value)
    var str = name + "=" + escape(value) + ";domain=.yinuovip.com;path=/";
    var date = new Date();
    date.setTime(date.getTime()); //设置date为当前时间加一年

    str += ";expires=" + date.toGMTString();
    console.log('str',str)
    document.cookie = str;
}

// $.ajaxSettings = $.extend($.ajaxSettings, {
//
//     beforeSend: function(request) {
//         request.setRequestHeader("X-App-Ver", '2.6.0');
//     }
// });

if(window.jQuery || window.Zepto){
    if(window.jQuery) {
        jQuery.ajaxSetup({
            headers: {
                "X-App-Ver": '2.6.0'
            }
        })
    }else{
        Zepto.ajaxSettings = Zepto.extend(Zepto.ajaxSettings, {
            headers: {
                "X-App-Ver": '2.6.0'
            }
        });
    }

    //ajax预处理 后置处理
    $(document).bind("ajaxComplete", function(event, xhr, settings){
        if(settings.dataType === 'JSON' || settings.dataType === 'json'){
            if(xhr.status == 200 && xhr.responseText){
                try{
                    var reObj = JSON.parse(xhr.responseText);
                    //特殊code 没有权限 和token失效
                    if(reObj && (reObj.code==400 && reObj.error == '请登录' )){
                        SetCookieYinuoVip('key', '')
                    }
                }catch (e){console.error(e)}
            }
        }
    });
}



function checkLogin(state) {
    if (state == 0) {
        delCookie('key');
        SetCookieYinuoVip('key', '');
        window.location = WapSiteUrl + '/tmpl/member/login.html';
        return false;
    } else {
        return true;
    }
}






function contains(arr, str) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === str) {
            return true;
        }
    }
    return false;
}

function buildUrl(type, data) {
    switch (type) {
        case 'keyword':
            return WapSiteUrl + '/tmpl/product_list.html?keyword=' + encodeURIComponent(data);
        case 'special':
            return WapSiteUrl + '/special.html?special_id=' + data;
        case 'goods':
            return WapSiteUrl + '/tmpl/product_detail.html?goods_id=' + data;
        case 'pai':
            return WapSiteUrl + '/tmpl/auction/auction_detail.html?auction_id=' + data;
        case 'artist':
            return WapSiteUrl + '/tmpl/auction/artist_list.html?keyword=' + encodeURIComponent(data);
        case 'auction':
            return WapSiteUrl + '/tmpl/auction/auction_list.html?keyword=' + encodeURIComponent(data);
        case 'url':
            return data;
        case 'store':
            return WapSiteUrl + '/tmpl/store.html?store_id=' + encodeURIComponent(data);
        case 'guess':
            return WapSiteUrl + '/tmpl/qucai/xiangqing-qucai.html?gs_id=' + encodeURIComponent(data);
    }
    return WapSiteUrl;
}

function errorTipsShow(html) {
    $(".error-tips").html(html).show();
    setTimeout(function () {
        errorTipsHide();
    }, 3000);
}

function errorTipsHide() {
    $(".error-tips").html("").hide();
}77

function lm_errorTipsShow(str) {
    if (!$('.lm_errorTipsShow').length) {
        var html = '<div class="lm_errorTipsShow lm_hidden">' + str + '</div>';
        $('body').append(html);
        $('.lm_errorTipsShow').removeClass('lm_hidden');
    } else {
        $('.lm_errorTipsShow').html(str);
        $('.lm_errorTipsShow').removeClass('lm_hidden');
    }
    setTimeout(function () {
        lm_errorTipsHide();
        if ($('.login_form_btn') && $('.login_form_btn').length) {
            $('.login_form_btn').removeClass('active')
        }
    }, 3000);
    $('.lm_errorTipsShow').click(function () {
        lm_errorTipsHide();
        // $('.login_form_btn').removeClass('active')
    })
}

function lm_errorTipsHide() {
    $('.lm_errorTipsShow').html("").addClass('lm_hidden');
}


function writeClear(o) {
    if (o.val().length > 0) {
        o.parent().addClass('write');
    } else {
        o.parent().removeClass('write');
    }
    btnCheck(o.parents('form'));
}

function btnCheck(form) {
    var btn = true;
    form.find('input').each(function () {
        if ($(this).hasClass('no-follow')) {
            return;
        }
        if ($(this).val().length == 0) {
            btn = false;
        }
    });
    if (btn) {
        form.find('.btn').parent().addClass('ok');
    } else {
        form.find('.btn').parent().removeClass('ok');
    }
}

/**
 * 取得默认系统搜索关键词
 * @param cmd
 */
function getSearchName() {
    var keyword = decodeURIComponent(getQueryString('keyword'));

    if (keyword == '') {
        if (getCookie('deft_key_value') == null) {
            $.getJSON(ApiUrl + '/index.php?act=index&op=search_hot_info', function (result) {
                var data = result.datas.hot_info;
                if (typeof data.name != 'undefined') {
                    $('#keyword').attr('placeholder', data.name);
                    $('#keyword').html(data.name);
                    addCookie('', data.name, 1);
                    addCookie('deft_key_value', data.value, 1);
                } else {
                    addCookie('deft_key_name', '', 1);
                    addCookie('deft_key_value', '', 1);
                }
            })
        } else {
            $('#keyword').attr('placeholder', getCookie('deft_key_name'));
            $('#keyword').html(getCookie('deft_key_name'));
        }
    }


}

// 免费领代金券
function getFreeVoucher(tid) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=member_voucher&op=voucher_freeex",
        data: {tid: tid, key: key},
        dataType: 'json',
        success: function (result) {
            checkLogin(result.login);
            var msg = '领取成功';
            var skin = 'green';
            if (result.datas.error) {
                msg = '领取失败：' + result.datas.error;
                skin = 'red';
            }
            $.sDialog({
                skin: skin,
                content: msg,
                okBtn: false,
                cancelBtn: false
            });
        }
    });
}

// 登陆后更新购物车
function updateCookieCart(key) {
    var cartlist = decodeURIComponent(getCookie('goods_cart'));
    if (cartlist) {
        $.ajax({
            type: 'post',
            url: ApiUrl + '/index.php?act=member_cart&op=cart_batchadd',
            data: {key: key, cartlist: cartlist},
            dataType: 'json',
            async: false
        });
        delCookie('goods_cart');
    }
}

/**
 * 查询购物车中商品数量
 * @param key
 * @param expireHours
 */
function getCartCount(key, expireHours) {
    var cart_count = 0;
    if (getCookie('key') !== null && getCookie('cart_count') === null) {
        var key = getCookie('key');
        $.ajax({
            type: 'post',
            url: ApiUrl + '/index.php?act=member_cart&op=cart_count',
            data: {key: key},
            dataType: 'json',
            async: false,
            success: function (result) {
                if (typeof(result.datas.cart_count) != 'undefined') {
                    addCookie('cart_count', result.datas.cart_count, expireHours);
                    cart_count = result.datas.cart_count;
                }
            }
        });
    } else {
        cart_count = getCookie('cart_count');
    }
    if (cart_count > 0 && $('.nctouch-nav-menu').has('.cart').length > 0) {
        $('.nctouch-nav-menu').has('.cart').find('.cart').parents('li').find('sup').show();
        $('#header-nav').find('sup').show();
    }
}

/**
 * 查询是否有新消息
 */
function getChatCount() {
    if ($('#header').find('.message').length > 0 || $('#auction_header_msg').length > 0) {
        var key = getCookie('key');
        if (key !== null) {
            $.getJSON(ApiUrl + '/index.php?act=member_chat&op=get_msg_count', {key: key}, function (result) {
                if (result.datas > 0) {
                    $('#header').find('.message').parent().find('sup').show();
                    $('#header-nav').find('sup').show();
                    $('#auction_header_msg .auction_header_num').show();
                }
            });
        }
        $('#header').find('.message').parent().click(function () {
            window.location.href = WapSiteUrl + '/tmpl/member/chat_list.html';
        });
        $('#auction_header_msg').click(function () {
            window.location.href = WapSiteUrl + '/tmpl/member/chat_list.html';
        });
    }
}

$(function () {
    $('.input-del').click(function () {
        $(this).parent().removeClass('write').find('input').val('');
        btnCheck($(this).parents('form'));
    });

    // radio样式
    $('body').on('click', 'label', function () {
        if ($(this).has('input[type="radio"]').length > 0) {
            $(this).addClass('checked').siblings().removeAttr('class').find('input[type="radio"]').removeAttr('checked');
        } else if ($(this).has('[type="checkbox"]')) {
            if ($(this).find('input[type="checkbox"]').prop('checked')) {
                $(this).addClass('checked');
            } else {
                $(this).removeClass('checked');
            }
        }
    });
    // 滚动条通用js
    if ($('body').hasClass('scroller-body')) {
        new IScroll('.scroller-body', {mouseWheel: true, click: true});
    }

    // 右上侧小导航控件
    $('#header').on('click', '#header-nav', function () {
        if ($('.nctouch-nav-layout').hasClass('show')) {
            $('.nctouch-nav-layout').removeClass('show');
        } else {
            $('.nctouch-nav-layout').addClass('show');
        }
    });
    $('#header').on('click', '.nctouch-nav-layout', function () {
        $('.nctouch-nav-layout').removeClass('show');
    });
    $(document).scroll(function () {
        $('.nctouch-nav-layout').removeClass('show');
    });
    getSearchName();
    getCartCount();
    getChatCount();// 导航右侧消息


    //回到顶部
    $(document).scroll(function () {
        set();
    });
    $('.fix-block-r,footer').on('click', ".gotop", function () {
        btn = $(this)[0];
        this.timer = setInterval(function () {
            $(window).scrollTop(Math.floor($(window).scrollTop() * 0.8));
            if ($(window).scrollTop() == 0) clearInterval(btn.timer, set);
        }, 10);
    });

    function set() {
        $(window).scrollTop() == 0 ? $('#goTopBtn').addClass('hide') : $('#goTopBtn').removeClass('hide');
    }
});
(function ($) {
    $.extend($, {
        /**
         * 滚动header固定到顶部
         */
        scrollTransparent: function (options) {
            var defaults = {
                valve: '#header',          // 动作触发
                scrollHeight: 50
            }
            var options = $.extend({}, defaults, options);

            function _init() {
                $(window).scroll(function () {
                    if ($(window).scrollTop() <= options.scrollHeight) {
                        $(options.valve).addClass('transparent').removeClass('posf');
                    } else {
                        $(options.valve).addClass('posf').removeClass('transparent');
                    }
                });

            }

            return this.each(function () {
                _init();
            })();
        },

        /**
         * 选择地区
         *
         * @param $
         */
        areaSelected: function (options) {
            var defaults = {
                success: function (data) {
                }
            }
            var options = $.extend({}, defaults, options);
            var ASID = 0;
            var ASID_1 = 0;
            var ASID_2 = 0;
            var ASID_3 = 0;
            var ASNAME = '';
            var ASINFO = '';
            var ASDEEP = 1;
            var ASINIT = true;

            function _init() {
                if ($('#areaSelected').length > 0) {
                    $('#areaSelected').remove();
                }
                var html = '<div id="areaSelected">'
                    + '<div class="nctouch-full-mask left">'
                    + '<div class="nctouch-full-mask-bg"></div>'
                    + '<div class="nctouch-full-mask-block">'
                    + '<div class="header">'
                    + '<div class="header-wrap">'
                    + '<div class="header-l"><a href="javascript:void(0);"><i class="back"></i></a></div>'
                    + '<div class="header-title">'
                    + '<h1>选择地区</h1>'
                    + '</div>'
                    + '<div class="header-r"><a href="javascript:void(0);"><i class="close"></i></a></div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="nctouch-main-layout">'
                    + '<div class="nctouch-single-nav">'
                    + '<ul id="filtrate_ul" class="area">'
                    + '<li class="selected"><a href="javascript:void(0);">一级地区</a></li>'
                    + '<li><a href="javascript:void(0);" >二级地区</a></li>'
                    + '<li><a href="javascript:void(0);" >三级地区</a></li>'
                    + '</ul>'
                    + '</div>'
                    + '<div class="nctouch-main-layout-a"><ul class="nctouch-default-list"></ul></div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>';
                $('body').append(html);
                _getAreaList();
                _bindEvent();
                _close();
            }

            function _getAreaList() {
                $.ajax({//获取区域列表
                    type: 'get',
                    url: ApiUrl + '/index.php?act=area&op=area_list',
                    data: {area_id: ASID},
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        if (result.datas.area_list.length == 0) {
                            _finish();
                            return false;
                        }
                        if (ASINIT) {
                            ASINIT = false
                        } else {
                            ASDEEP++;
                        }
                        $('#areaSelected').find('#filtrate_ul').find('li').eq(ASDEEP - 1).addClass('selected').siblings().removeClass('selected');
                        checkLogin(result.login);
                        var data = result.datas;
                        var area_li = '';
                        for (var i = 0; i < data.area_list.length; i++) {
                            area_li += '<li><a href="javascript:void(0);" data-id="' + data.area_list[i].area_id + '" data-name="' + data.area_list[i].area_name + '"><h4>' + data.area_list[i].area_name + '</h4><span class="arrow-r"></span> </a></li>';
                        }
                        $('#areaSelected').find(".nctouch-default-list").html(area_li);
                        if (typeof(myScrollArea) == 'undefined') {
                            if (typeof(IScroll) == 'undefined') {
                                $.ajax({
                                    url: WapSiteUrl + '/js/iscroll.js',
                                    dataType: "script",
                                    async: false
                                });
                            }
                            myScrollArea = new IScroll('#areaSelected .nctouch-main-layout-a', {
                                mouseWheel: true,
                                click: true
                            });
                        } else {
                            myScrollArea.destroy();
                            myScrollArea = new IScroll('#areaSelected .nctouch-main-layout-a', {
                                mouseWheel: true,
                                click: true
                            });
                        }
                    }
                });
                return false;
            }

            function _bindEvent() {
                $('#areaSelected').find('.nctouch-default-list').off('click', 'li > a');
                $('#areaSelected').find('.nctouch-default-list').on('click', 'li > a', function () {
                    ASID = $(this).attr('data-id');
                    eval("ASID_" + ASDEEP + "=$(this).attr('data-id')");
                    ASNAME = $(this).attr('data-name');
                    if (ASINFO.indexOf(ASNAME) >= 0) return false;
                    ASINFO += ASNAME + ' ';
                    var _li = $('#areaSelected').find('#filtrate_ul').find('li').eq(ASDEEP);
                    _li.prev().find('a').attr({'data-id': ASID, 'data-name': ASNAME}).html(ASNAME);
                    if (ASDEEP == 3) {
                        _finish();
                        return false;
                    }
                    _getAreaList();
                });
                $('#areaSelected').find('#filtrate_ul').off('click', 'li > a');
                $('#areaSelected').find('#filtrate_ul').on('click', 'li > a', function () {
                    if ($(this).parent().index() >= $('#areaSelected').find('#filtrate_ul').find('.selected').index()) {
                        return false;
                    }
                    ASID = $(this).parent().prev().find('a').attr('data-id');
                    ASNAME = $(this).parent().prev().find('a').attr('data-name');
                    ASDEEP = $(this).parent().index();
                    ASINFO = '';
                    for (var i = 0; i < $('#areaSelected').find('#filtrate_ul').find('a').length; i++) {
                        if (i < ASDEEP) {
                            ASINFO += $('#areaSelected').find('#filtrate_ul').find('a').eq(i).attr('data-name') + ' ';
                        } else {
                            var text = '';
                            switch (i) {
                                case 0:
                                    text = '一级地区'
                                    break;
                                case 1:
                                    text = '二级地区'
                                    break;
                                case 2:
                                    text = '三级地区';
                                    break;
                            }
                            $('#areaSelected').find('#filtrate_ul').find('a').eq(i).html(text);
                        }
                    }
                    _getAreaList();
                });
            }

            function _finish() {
                var data = {
                    area_id: ASID,
                    area_id_1: ASID_1,
                    area_id_2: ASID_2,
                    area_id_3: ASID_3,
                    area_name: ASNAME,
                    area_info: ASINFO
                };
                options.success.call('success', data);
                if (!ASINIT) {
                    $('#areaSelected').find('.nctouch-full-mask').addClass('right').removeClass('left');
                }
                return false;
            }

            function _close() {
                $('#areaSelected').find('.header-l').off('click', 'a');
                $('#areaSelected').find('.header-l').on('click', 'a', function () {
                    $('#areaSelected').find('.nctouch-full-mask').addClass('right').removeClass('left');
                });
                return false;
            }

            return this.each(function () {
                return _init();
            })();
        },


        /**
         * 从右到左动态显示隐藏内容
         *
         */
        animationLeft: function (options) {

            var defaults = {
                valve: '.animation-left',          // 动作触发
                wrapper: '.nctouch-full-mask',    // 动作块
                scroll: ''     // 滚动块，为空不触发滚动
            }
            var options = $.extend({}, defaults, options);

            function _init() {
                $(options.valve).click(function () {
                    $(options.wrapper).removeClass('hide').removeClass('right').addClass('left');

                    if (options.scroll != '') {
                        if (typeof(myScrollAnimationLeft) == 'undefined') {
                            if (typeof(IScroll) == 'undefined') {
                                $.ajax({
                                    url: WapSiteUrl + '/js/iscroll.js',
                                    dataType: "script",
                                    async: false
                                });
                            }
                            myScrollAnimationLeft = new IScroll(options.scroll, {mouseWheel: true, click: true});
                        } else {
                            myScrollAnimationLeft.refresh();
                        }
                    }
                });
                $(options.wrapper).on('click', '.header-l > a', function () {
                    $(options.wrapper).addClass('right').removeClass('left');
                });

            }

            return this.each(function () {
                _init();
            })();
        },

        /**
         * 从下到上动态显示隐藏内容
         *
         */
        animationUp: function (options) {
            var defaults = {
                valve: '.animation-up',          // 动作触发，为空直接触发
                wrapper: '.nctouch-bottom-mask',    // 动作块
                scroll: '.nctouch-bottom-mask-rolling',     // 滚动块，为空不触发滚动
                start: function () {
                },       // 开始动作触发事件
                close: function () {
                }        // 关闭动作触发事件
            }
            var options = $.extend({}, defaults, options);

            function _animationUpRun() {
                options.start.call('start');
                $(options.wrapper).removeClass('down').addClass('up');

                if (options.scroll != '') {
                    if (typeof(myScrollAnimationUp) == 'undefined') {
                        if (typeof(IScroll) == 'undefined') {
                            $.ajax({
                                url: WapSiteUrl + '/js/iscroll.js',
                                dataType: "script",
                                async: false
                            });
                        }
                        myScrollAnimationUp = new IScroll(options.scroll, {mouseWheel: true, click: true});
                    } else {
                        myScrollAnimationUp.refresh();
                    }
                }
            }

            return this.each(function () {
                if (options.valve != '') {
                    $(options.valve).on('click', function () {
                        _animationUpRun();
                    });
                } else {
                    _animationUpRun();
                }
                $(options.wrapper).on('click', '.nctouch-bottom-mask-bg,.nctouch-bottom-mask-close', function () {
                    $(options.wrapper).addClass('down').removeClass('up');
                    options.close.call('close');
                });
            })();
        }
    });
})(Zepto);

/**
 * 异步上传图片
 */
$.fn.ajaxUploadImage = function (options) {
    var defaults = {
        url: '',
        data: {},
        start: function () {
        },     // 开始上传触发事件
        success: function () {
        }
    }
    var options = $.extend({}, defaults, options);
    var _uploadFile;

    function _checkFile() {
        //文件为空判断
        if (_uploadFile === null || _uploadFile === undefined) {
            alert("请选择您要上传的文件！");
            return false;
        }
//           
//          //检测文件类型
//          if(_uploadFile.type.indexOf('image') === -1) {
//              alert("请选择图片文件！");
//              return false;
//          }
//           
//          //计算文件大小
//          var size = Math.floor(_uploadFile.size/1024);
//          if (size > 5000) {
//              alert("上传文件不得超过5M!");
//              return false;
//          };
        return true;
    };
    return this.each(function () {
        $(this).on('change', function () {
            var _element = $(this);
            options.start.call('start', _element);
            _uploadFile = _element.prop('files')[0];
            if (!_checkFile) return false;
            try {
                //执行上传操作
                var xhr = new XMLHttpRequest();
                xhr.open("post", options.url, true);
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        returnDate = $.parseJSON(xhr.responseText);
                        options.success.call('success', _element, returnDate);
                    }
                    ;
                };
                //表单数据
                var fd = new FormData();
                for (k in options.data) {
                    fd.append(k, options.data[k]);
                }
                fd.append(_element.attr('name'), _uploadFile);
                //执行发送
                result = xhr.send(fd);
            } catch (e) {
                console.log(e);
                alert(e);
            }
        });
    });
}

function loadSeccode() {
    $("#codekey").val('');
    //加载验证码
    $.ajax({
        type: 'get',
        url: ApiUrl + "/index.php?act=seccode&op=makecodekey",
        async: false,
        dataType: 'json',
        success: function (result) {
            $("#codekey").val(result.datas.codekey);
        }
    });
    $("#codeimage").attr('src', ApiUrl + '/index.php?act=seccode&op=makecode&k=' + $("#codekey").val() + '&t=' + Math.random());
}

/**
 * 收藏店铺
 */
function favoriteStore(store_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (store_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_add',
        data: {key: key, store_id: store_id},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "收藏成功！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 取消收藏店铺
 */
function dropFavoriteStore(store_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (store_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites_store&op=favorites_del',
        data: {key: key, store_id: store_id},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "已取消收藏！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 收藏商品
 */
function favoriteGoods(goods_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (goods_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites&op=favorites_add',
        data: {key: key, goods_id: goods_id, fav_type: 'goods'},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "收藏成功！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 取消收藏商品
 */
function dropFavoriteGoods(goods_id, fav_type) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (goods_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites&op=favorites_del',
        data: {key: key, fav_id: goods_id, fav_type: fav_type},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "已取消收藏！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}


/**
 * 收藏拍品
 */
function favoriteAuction(auction_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (auction_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites&op=favorites_add',
        data: {key: key, auction_id: auction_id, fav_type: 'auction'},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "收藏成功！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 取消收藏拍品
 */
function dropFavoriteAuction(auction_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (auction_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_favorites&op=favorites_del',
        data: {key: key, fav_id: auction_id, fav_type: 'auction'},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "已取消收藏！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 收藏趣猜
 */
function favoriteGuess(gs_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (gs_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_guess&op=favorites_add',
        data: {key: key, gs_id: gs_id},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "收藏成功！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 取消收藏趣猜
 */
function dropFavoriteGuess(gs_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    if (gs_id <= 0) {
        $.sDialog({skin: "green", content: '参数错误', okBtn: false, cancelBtn: false});
        return false;
    }
    var return_val = false;
    $.ajax({
        type: 'post',
        url: ApiUrl + '/index.php?act=member_guess&op=favorites_del',
        data: {key: key, fav_id: gs_id},
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result.code == 200) {
                // $.sDialog({skin: "green", content: "已取消收藏！", okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
            }
        }
    });
    return return_val;
}

/**
 * 动态加载css文件
 * @param css_filename css文件路径
 */
function loadCss(css_filename) {
    var link = document.createElement('link');
    link.setAttribute('type', 'text/css');
    link.setAttribute('href', css_filename);
    link.setAttribute('href', css_filename);
    link.setAttribute('rel', 'stylesheet');
    css_id = document.getElementById('auto_css_id');
    if (css_id) {
        document.getElementsByTagName('head')[0].removeChild(css_id);
    }
    document.getElementsByTagName('head')[0].appendChild(link);
}

/**
 * 动态加载js文件
 * @param script_filename js文件路径
 */
function loadJs(script_filename) {
    var script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', script_filename);
    script.setAttribute('id', 'auto_script_id');
    script_id = document.getElementById('auto_script_id');
    if (script_id) {
        document.getElementsByTagName('head')[0].removeChild(script_id);
    }
    document.getElementsByTagName('head')[0].appendChild(script);
}

//倒计时函数
function updateEndTime() {
    var date = new Date();
    var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

    $(".settime").each(function (i) {

        var endDate = this.getAttribute("endTime"); //结束时间字符串
        //转换为时间日期类型
        var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) {
            return parseInt(a, 10) - 1;
        }).match(/\d+/g) + ')');

        var endTime = endDate * 1000; //结束时间毫秒数
        var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
        if (lag > 0) {
            var second = Math.floor(lag % 60);
            var minite = Math.floor((lag / 60) % 60);
            var hour = Math.floor((lag / 3600) % 24);
            var day = Math.floor((lag / 3600) / 24);
            $(this).html("<em time_id='d'>" + day + "</em>日<em time_id='h'>" + hour + "</em>时<em time_id='m'>" + minite + "</em>分<em time_id='s'>" + second + "</em>秒");
        }
        else
            $(this).html("已经结束啦！");
    });
    setTimeout("updateEndTime()", 1000);
}


/**
 ** 加法函数，用来得到精确的加法结果
 ** 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
 ** 调用：accAdd(arg1,arg2)
 ** 返回值：arg1加上arg2的精确结果
 **/
function accAdd(arg1, arg2) {
    var r1, r2, m, c;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    c = Math.abs(r1 - r2);
    m = Math.pow(10, Math.max(r1, r2));
    if (c > 0) {
        var cm = Math.pow(10, c);
        if (r1 > r2) {
            arg1 = Number(arg1.toString().replace(".", ""));
            arg2 = Number(arg2.toString().replace(".", "")) * cm;
        } else {
            arg1 = Number(arg1.toString().replace(".", "")) * cm;
            arg2 = Number(arg2.toString().replace(".", ""));
        }
    } else {
        arg1 = Number(arg1.toString().replace(".", ""));
        arg2 = Number(arg2.toString().replace(".", ""));
    }
    return (arg1 + arg2) / m;
}

//给Number类型增加一个add方法，调用起来更加方便。
Number.prototype.add = function (arg) {
    return accAdd(arg, this);
};

/**
 ** 减法函数，用来得到精确的减法结果
 ** 说明：javascript的减法结果会有误差，在两个浮点数相减的时候会比较明显。这个函数返回较为精确的减法结果。
 ** 调用：accSub(arg1,arg2)
 ** 返回值：arg1加上arg2的精确结果
 **/
function accSub(arg1, arg2) {
    var r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2)); //last modify by deeka //动态控制精度长度
    n = (r1 >= r2) ? r1 : r2;
    return ((arg1 * m - arg2 * m) / m).toFixed(n);
}

// 给Number类型增加一个mul方法，调用起来更加方便。
Number.prototype.sub = function (arg) {
    return accMul(arg, this);
};

/*百度分享js*/
/*window._bd_share_config = {
    "common": {
        "bdSnsKey": {},
        "bdText": "",
        "bdMini": "2",
        "bdMiniList": false,
        "bdPic": "",
        "bdStyle": "0",
        "bdSize": "16"
    },
    "share": {},
};
with (document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];*/

/*
 * 是否是微信浏览器
 * */
function isWeiXin() {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        return true;
    } else {
        return false;
    }
}


/*
 * 图片延迟加载
 */
(function ($) {
    $.fn.nc_lazyload = function () {
        var lazy_items = [];
        this.each(function () {
            if ($(this).attr("data-src") !== undefined) {
                var lazy_item = {
                    object: $(this),
                    url: $(this).attr("data-src")
                };
                lazy_items.push(lazy_item);
            }
        });

        var load_img = function () {
            var window_height = $(window).height();
            var scroll_top = $(window).scrollTop();

            $.each(lazy_items, function (i, lazy_item) {
                if (lazy_item.object) {
                    item_top = lazy_item.object.offset().top - scroll_top;
                    if (item_top >= 0 && item_top < window_height) {
                        if (lazy_item.url) {
                            lazy_item.object.attr("src", lazy_item.url);
                        }
                        lazy_item.object = null;
                    }
                }
            });
        };
        load_img();
        $(window).bind("scroll", load_img);
    };
})(Zepto);

// $(document).ready(function(){
//
// });


//获取url中的参数
// function getUrlParam(name) {
//     var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
//     var r = window.location.search.substr(1).match(reg); //匹配目标参数
//     if (r != null) return unescape(r[2]); return null; //返回参数值
// }
/*
 $.Request("act") = 1
 $.UrlUpdateParams(window.location.href, "mid", 11111)

 */

function Request_pub(m) {
    var sValue = location.search.match(new RegExp("[\?\&]" + m + "=([^\&]*)(\&?)", "i"));
    return sValue ? sValue[1] : sValue;
}

function UrlUpdateParams_pub(url, name, value) {
    var r = url;
    if (r != null && r != 'undefined' && r != "") {
        value = encodeURIComponent(value);
        var reg = new RegExp("(^|)" + name + "=([^&]*)(|$)");
        var tmp = name + "=" + value;
        if (url.match(reg) != null) {
            r = url.replace(eval(reg), tmp);
        }
        else {
            if (url.match("[\?]")) {
                r = url + "&" + tmp;
            } else {
                r = url + "?" + tmp;
            }
        }
    }
    return r;
}

function countProperties(obj) {
    var count = 0;
    for (var property in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, property)) {
            count++;
        }
    }
    return count;
}

// 第三方登录设置cookie
function ThirdLoginClick(str){

    if (typeof(str) == "string"){
        str = JSON.parse(str);
    }
    if(!getCookie('key')){
        addCookie("key",str.key);
    }
}

function NativeLoginClick(str){

    if (typeof(str) == "string"){
        str = JSON.parse(str);
    }

    if(!getCookie('key')){
        addCookie("key",str.key);
        setCookieYinouvip("key",str.key)
    }
}

function NativeLoginOutClick(){
    delCookie('key')
}

//判断是否是微信登录
function isWeiXinLoad() {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        return true;
    } else {
        return false;
    }
}

function LM_Request(m) {
    var sValue = location.search.match(new RegExp("[\?\&]" + m + "=([^\&]*)(\&?)", "i"));
    return sValue ? sValue[1] : sValue;
};

function LM_UrlUpdateParams(url, name, value) {
    var r = url;
    if (r != null && r != 'undefined' && r != "") {
        value = encodeURIComponent(value);
        var reg = new RegExp("(^|)" + name + "=([^&]*)(|$)");
        var tmp = name + "=" + value;
        if (url.match(reg) != null) {
            r = url.replace(eval(reg), tmp);
        }
        else {
            if (url.match("[\?]")) {
                r = url + "&" + tmp;
            } else {
                r = url + "?" + tmp;
            }
        }
    }
    return r;
}

//邀请码记入cookie
var invite_code = getQueryString('invite_code');
if (invite_code != '' && invite_code != null && invite_code != undefined) {
    addCookie('invite_code', invite_code, 24);
}

//百度统计
var _hmt = _hmt || [];
(function () {
    if (location.host !== 'www.yinuovip.com') {
        return false;
    }
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?710f078a02da6c22259010ad0803e16d";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();
if ($('header a[href="javascript:history.go(-1)"]').length) {
    var self = $('header a[href="javascript:history.go(-1)"]');
    self.click(function () {
        document.referrer === '' ?
            window.location.href = WapSiteUrl + '/app_index.html' :
            window.history.go(-1);
        return false;
    })
}


if ($('header a[href="javascript:history.go(-1)"]').length) {
    var self = $('header a[href="javascript:history.go(-1)"]');
    self.click(function () {
        document.referrer === '' ?
            window.location.href = WapSiteUrl + '/app_index.html' :
            window.history.go(-1);
        return false;
    })
}


function lmCuteDown() {
    // console.log($('.cutedown').length);
    $('.cutedown').each(function (i, e) {
        var date = parseInt($(this).attr('data-date')),
            three, two, one, sec,
            $data = $(this).find('.cutedown_con');

        function showTime(type) {
            if (date <= 0) {
                clearInterval(displayTime);
                addReload();
                return;
            }
            date -= 1;
            if (arguments.length === 2 && arguments[0].length === 3) {
                if (arguments[1] === 1) {
                    three = Math.floor(date / 60 / 60 / 24);
                    two = Math.floor(date / 60 / 60 % 24);
                    one = Math.floor(date / 60 % 60);
                } else {
                    three = Math.floor(date / 60 / 60 % 24);
                    two = Math.floor(date / 60 % 60);
                    one = Math.floor(date % 60);
                }
                if (three < 10) {
                    three = '0' + three
                }
                if (two < 10) {
                    two = '0' + two
                }
                if (one < 10) {
                    one = '0' + one
                }
                var html = '<span class="hour">' + three + '</span>' + arguments[0][0] + '\
                <span class="minute">' + two + '</span>' + arguments[0][1] + '\
                <span class="seconds">' + one + '</span>' + arguments[0][2] + '';
            } else {
                three = Math.floor(date / 60 / 60 / 24);
                two = Math.floor(date / 60 / 60 % 24);
                one = Math.floor(date / 60 % 60);
                sec = Math.floor(date % 60);
                if (three < 10) {
                    three = '0' + three
                }
                if (two < 10) {
                    two = '0' + two
                }
                if (one < 10) {
                    one = '0' + one
                }
                if (sec < 10) {
                    sec = '0' + sec
                }
                var html = '<span class="hour">' + three + '</span>天\
                <span class="minute">' + two + '</span>时\
                <span class="seconds">' + one + '</span>分\
                <span class="seconds">' + sec + '</span>秒';
            }
            $data.html(html);
            if (three == 0 && two == 0 && one == 0 && sec == 0) {
                addReload();
            }
        }

        if ($(this).attr('data-type') != 3) {
            if (date > 86400) {
                //时间戳如果是s的时候不是s则多除1000
                var displayTime;
                if (date == -1) {
                    clearInterval(displayTime);
                    addReload();
                    return;
                }
                displayTime = setInterval(function () {
                    showTime(['天', '时', '分'], 1);
                }, 1000);
            } else if (date < 86400) {
                var displayTime;
                displayTime = setInterval(function () {
                    showTime(['时', '分', '秒'], 2);
                }, 1000);
            }
        } else {
            var displayTime;
            displayTime = setInterval(function () {
                showTime(['天', '时', '分', '秒'], 1);
            }, 1000);
        }
    });
}


/*
 * 设置提醒
 * */
function set_remind_lm(auction_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    var return_val = false;
    $.ajax({
        type: 'POST',
        url: ApiUrl + "/index.php?act=auction&op=check_mobile",
        data: {key: key},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                // $.sDialog({skin: "green", content: '请先绑定手机', okBtn: false, cancelBtn: false});
                console.log('请绑定手机');
                setTimeout('window.location.href="../member/member_mobile_bind.html"', 500);
                // window.location.href='../member/member_mobile_bind.html';
            } else {
                $.ajax({
                    type: 'POST',
                    url: ApiUrl + "/index.php?act=auction&op=set_remind",
                    data: {auction_id: auction_id, key: key},
                    dataType: "json",
                    success: function (result) {
                        if (result.datas == 0) {
                            return_val = true;
                            console.log('ok', '1');
                            // $.sDialog({skin: "green", content: '设置提醒成功', okBtn: false, cancelBtn: false});
                        } else {
                            // $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                            console.log(result.datas.error);
                        }
                    }
                });
            }
        }
    });
    return return_val;
}

/*
 * 取消提醒
 * */
function cancel_remind_lm(auction_id, relation_id) {
    var key = getCookie('key');
    if (!key) {
        checkLogin(0);
        return;
    }
    var return_val = false;
    $.ajax({
        type: 'POST',
        url: ApiUrl + "/index.php?act=auction&op=cancel_remind",
        data: {auction_id: auction_id, key: key, relation_id: relation_id},
        dataType: "json",
        success: function (result) {
            if (result.datas == 0) {
                // $.sDialog({skin: "green", content: '取消提醒成功', okBtn: false, cancelBtn: false});
                return_val = true;
            } else {
                // $.sDialog({skin: "red", content: result.datas.error, okBtn: false, cancelBtn: false});
                console.log('ok', 3);
            }
        }
    });
    return return_val;
}

// bold 解决微信浏览器中window.reload失效
function addReload() {
    window.location.href = LM_UrlUpdateParams(window.location.href, 'reload_id', 10000 * Math.random());
}


//qqbower and uc
function qqBowerUcbower() {
    var bLevel = {
        qq: {forbid: 0, lower: 1, higher: 2},
        uc: {forbid: 0, allow: 1}
    };
    var UA = navigator.appVersion;
    var isqqBrowser = (UA.split("MQQBrowser/").length > 1) ? bLevel.qq.higher : bLevel.qq.forbid;
    var isucBrowser = (UA.split("UCBrowser/").length > 1) ? bLevel.uc.allow : bLevel.uc.forbid;
    var version = {
        uc: "",
        qq: ""
    };
    var platform_os;
    var ua = navigator.userAgent;
    if ((ua.indexOf("iPhone") > -1 || ua.indexOf("iPod") > -1)) {
        platform_os = "iPhone"
    } else {
        platform_os = "Android"
    }
    version.qq = isqqBrowser ? parseFloat(UA.split("MQQBrowser/")[1].split(".")[0] + "." + UA.split("MQQBrowser/")[1].split(".")[1]) : 0;
    version.uc = isucBrowser ? parseFloat(UA.split("UCBrowser/")[1].split(".")[0] + "." + UA.split("UCBrowser/")[1].split(".")[1]) : 0;
    if ((isqqBrowser && version.qq < 5.4 && platform_os == "iPhone") || (isqqBrowser && version.qq < 5.3 && platform_os == "Android")) {
        isqqBrowser = bLevel.qq.forbid
    } else {
        if (isqqBrowser && version.qq < 5.4 && platform_os == "Android") {
            isqqBrowser = bLevel.qq.lower
        } else {
            if (isucBrowser && ((version.uc < 10.2 && platform_os == "iPhone") || (version.uc < 9.7 && platform_os == "Android"))) {
                isucBrowser = bLevel.uc.forbid
            }
        }
    }
    return isqqBrowser || isucBrowser;
}


function getBrowerVersion() {
    var browserName = "Other";
    var ua = window.navigator.userAgent.toLowerCase();
    browserRegExp = {
        Sogou: /se\s2\.x/,
        SogouMobile: /sogoumobilebrowser|sogousearch/,
        Explorer2345: /2345explorer|2345chrome|mb2345browser/,
        Liebao: /lbbrowser/,
        Wechat: /micromessenger/,
        QQBrowser: /qqbrowser/,
        Shoujibaidu: /baiduboxapp|baiduhd/,
        Baidu: /bidubrowser|baidubrowser/,
        UC: /ubrowser|ucbrowser|ucweb/,
        MiuiBrowser: /miuibrowser/,
        SamsungBrowser: /samsungbrowser/,
        OPPO: /oppobrowser/,
        MobileQQ: /mobile.*qq/,
        toutiao: /newsarticle/,
        Firefox: /firefox/,
        Maxthon: /maxthon/,
        Se360: /360se/,
        Ee360: /360ee/,
        TheWorld: /theworld/,
        Weibo: /__weibo__/,
        NokiaBrowser: /nokiabrowser/,
        Opera: /opera|opr\/(\d+[\.\d]+)/,
        Edge: /edge/,
        IE: /trident|msie/,
        Safari: /macintosh.*safari|(iphone|ipad|ipod).*version.*mobile.*safari/,
        Chrome: /chrome|crios/,
        AndroidBrowser: /android.*safari|android.*release.*browser/
    };
    for (var i in browserRegExp) {
        if (browserRegExp[i].exec(ua)) {
            browserName = i;
            break;
        }
    }
    return browserName;
}

//登录注册模块公用
//当出现右侧红色叉叉后，点击可以清除input内容
$('.error i.iconfont').click(function () {
    $(this).parents('.login_form_input_panpel').val('');
});
//输入框输入时清除报错信息
$('.login_form .login_form_panpel .login_form_input').on('focus', function () {

    $(this).parents('.login_form_input_panpel').removeClass('error');
    $(this).attr('placeholder', '');
});

//密码输入框点击开启隐藏／显示
$('.login_form .login_form_panpel .chose_pwd').on('click', function () {
    var parents = $(this).parents('.login_form_input_panpel');
    if (parents.hasClass('hide_pwd')) {
        parents.removeClass('hide_pwd')
    } else {
        parents.addClass('hide_pwd')
    }
});

/**
 * 公用遮罩层选择器
 */
function pub_mask_select(func) {
    var $domTrigger = $('.pub_mask_select .pub_mask_select_trigger'),
        $dom = $('.pub_mask_select .pub_mask_select_trigger .pub_mask_select_val'),
        result;
    $domTrigger.on('click', function () {
        if ($('.pub_mask_select').hasClass('active')) {
            pub_mask_select_hide();
            return false;
        }
        if ($('.pub_mask_select_con').length) {
            return false;
        }
        if (!$('.pub_mask').length) {
            $('html').append('<div class="pub_mask"></div>');
        }
        var self = $(this).find('.pub_mask_select_val');
        var $html = $('<div class="pub_mask_select_con"><ul></ul></div>');
        if (self.data('data')) {
            var con = '';
            var item = self.data('data').split(',');
            for (var i = 0; i < item.length; i++) {
                if ($dom.text() == item[i]) con += '<li class="active">' + item[i] + '</li>';
                else con += '<li>' + item[i] + '</li>';
            }
            $html.find('ul').append(con);
        }
        self.parents('.pub_mask_select').addClass('active').append($html);
        $('.pub_mask_select_con li').click(function () {
            $('.pub_mask_select_con li').removeClass('active');
            $(this).addClass('active');
            pub_mask_select_hide();
            result = $(this).text();
            $dom.text(result);
            func(self, result);
            return false;
        });
        $('.pub_mask').click(function () {
            pub_mask_select_hide();
        });
        return false;
    });
}

/**
 * 公用遮罩曾选择器关闭
 */
function pub_mask_select_hide() {
    $('.pub_mask_select').removeClass('active');
    $('.pub_mask_select_con,.pub_mask').remove();
}

/**
 *
 * @param str
 * @param split
 * @returns {string}
 */

function changeSplit(str, split) {
    str += '';
    var split = split || "$1,";
    return str.slice(0, str.indexOf('.')).replace(/(\d)(?=(\d{3})+$)/g, split) + str.slice(str.indexOf('.'))
}

/**
 *
 * @param num1
 * @param num2
 * @returns {number}
 */
function numAdd(num1, num2) {
    var baseNum, baseNum1, baseNum2;
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
    return (num1 * baseNum + num2 * baseNum) / baseNum;
};

$('#iconfont_s').attr('href', 'http://at.alicdn.com/t/font_310664_b9scuds03tksatt9.css');


function is_show_header() {
    if (isapp && hide_header === true) {
        $('body header').hide();
        //$('body header');
        $('body header').next().css('margin-top', '0');
        ;
        $("body").css('padding-top', '0');
    }
}

window.onload = function () {
    if (navigator.userAgent.indexOf('Appyinuo') !== -1) {
        isapp = 1;
        appType = 1;
        wxIsInstalled = true;
        $("#wxpay").parents("label").show();
        is_show_header();
        if (payment_code == "") {
            payment_code = "wxpay";
            $("#wxpay").attr("checked", true).parents("label").addClass("checked")
        }
    }
    if (navigator.userAgent.indexOf('YinuoAPP') !== -1) {
        isapp = 1;
        appType = 2;
        is_show_header();
    }


}


function LoginClickResponse(str) {

    if (typeof(str) == "string") {
        str = JSON.parse(str);
    }

    var login_info = {"unionid": str.unionid, "username": str.mobile, "password": str.password, "client": str.client}
    window.member_id

    $.ajax({
        type: 'post',
        url: ApiUrl + "/index.php?act=login",
        data: {username: login_info.username, password: login_info.password, client: login_info.client, unionid: ''},
        dataType: 'json',
        success: function (result) {
            // console.log(result)
            if (!result.datas.error) {
                if (typeof(result.datas.key) == 'undefined') {
                    return false;
                } else {
                    var expireHours = 0;
                    // var expireHours = 0;
                    if ($('#checkbox').prop('checked')) {
                        expireHours = 188;
                    }
                    // 更新cookie购物车
                    updateCookieCart(result.datas.key);
                    addCookie('username', result.datas.username, expireHours);
                    addCookie('key', result.datas.key, expireHours);
                    addCookie('usertype', result.datas.usertype, expireHours);
                    // console.log(result.datas)

                    location.href = document.referrer;
                }
                //关闭报错
                console.log("LoginClickResponse-success")

                // lm_errorTipsHide();
            } else {
                // errorTipsShow('<p>' + result.datas.error + '</p>');
                // lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                console.log("LoginClickResponse-fail")

                //提示报错
            }
        },
        error: function (xhr) {
            //需要模拟服务器错误等情况
            //服务器错误、无响应、正在连接中等。
            console.log(xhr)
        }
    });
}



window.addEventListener('message', function (e) {

//     // LoginClickResponse({"username":15618147729,"password":111111,"client":'wap',"unionid":""})

    // 判断数据发送方是否是可靠的地址
    if (e.origin === 'http://wap.yinuovip.com' || e.origin === 'http://192.168.97.172' || e.origin === 'http://whk.wap2.yinuovip.com' ) {

        var hideKey = addCookie('hideKey','YinuoyishuForVue')
        // $(".lm_footer").hide();


        // console.log(e);
        // console.log(e.data.user_name);
        // console.log(e.data.password);
        // console.log(e.data.user_id);
        // console.log(e.data.api_token);
        // console.log(e.data.client);
        // LoginClickResponse({"username":e.data.user_name,"password":e.data.password,"client":e.data.client,"unionid":""})
        // LoginClickResponse({"username": 17612116058, "password": 111111, "client": "wap", "unionid": ''})

        // console.log(MessageEvent );

        // 回发数据

        // var login_info = {"unionid": '', "username": e.data.user_name, "password": e.data.password, "client": 'wap'}
        window.member_id
        $.ajax({
            type: 'post',
            url: ApiUrl + "/index.php?act=login",
            data: {username:e.data.user_name, password: e.data.password, client: 'wap', unionid: ''},
            dataType: 'json',
            success: function (result) {
                if (!result.datas.error) {
                    if (typeof(result.datas.key) == 'undefined') {
                        return false;
                    } else {
                        var expireHours = 0;
                        // var expireHours = 0;
                        if ($('#checkbox').prop('checked')) {
                            expireHours = 188;
                        }
                        // 更新cookie购物车
                        updateCookieCart(result.datas.key);
                        addCookie('username', result.datas.username, expireHours);
                        addCookie('key', result.datas.key, expireHours);
                        addCookie('usertype', result.datas.usertype, expireHours);
                        setCookieYinouvip('username',result.datas.username);
                        setCookieYinouvip('key',result.datas.key);
                        setCookieYinouvip('usertype',result.datas.usertype);
                        // console.log(result.datas)

                        // location.href = document.referrer;
                    }
                    //关闭报错
                    console.log("LoginClickResponse-success")
                    var is_reload = getCookie('is_reload');
                    // if(!is_reload){
                    //     location.reload();
                    // }
                    // addCookie('is_reload','1');

                    window.location.reload() + Date.parse(new Date())
                    return
                    // lm_errorTipsHide();
                } else {
                    // errorTipsShow('<p>' + result.datas.error + '</p>');
                    // lm_errorTipsShow('<p>' + result.datas.error + '....</p>');
                    console.log("LoginClickResponse-fail")

                    //提示报错
                }
            },
            error: function (xhr) {
                //需要模拟服务器错误等情况
                //服务器错误、无响应、正在连接中等。
                console.log(xhr)
            }
        });



        e.source.postMessage("success", e.origin);
    }

    // console.log(e.data)
    //
    // if(e.data.api_token == null || e.data.api_token == ''){
    //     window.location.href = 'http://192.168.97.164/user'
    //
    //     alert('no')
    // }else {
    //
    //     alert('ok')
    // }
}, false);












