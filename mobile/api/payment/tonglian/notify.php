<?php
	require_once 'AppConfig.php';
	require_once 'AppUtil.php';

	$params = array();
	foreach($_POST as $key=>$val) {//将参数遍历获取
		$params[$key] = $val;
	}
	if(count($params)<1){//如果参数为空,则不进行处理
		echo "error";
		exit();
	}
	if(AppUtil::ValidSign($params, AppConfig::APPKEY)){//验签成功
		//此处进行业务逻辑处理
		echo "success";
	}
	else{
		echo "erro";
	}

?>  
