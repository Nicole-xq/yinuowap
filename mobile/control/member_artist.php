<?php
/**
 * 特技会员签约艺人
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class member_artistControl extends mobileMemberControl {
    public function __construct(){
        parent::__construct();
    }
    //艺术家列表
    public function artist_listOp(){
        $condition = array();
        $condition['bind_member_id'] = $this->member_info['member_id'];
        $condition['is_artist'] = 1;
        $field = 'member.member_id,member.member_name,member.member_avatar,member.member_time,member.member_type';
        $member_model = Model('member');
        $member_list = $member_model->getArtistWithMemberList($condition, $field, $this->page);
        $member_ids = $this->_dataSort($member_list);
        $this->_makeCommissionData($member_list, $member_ids);
        $page_count = $member_model->gettotalpage();
        $list = array_values($member_list);
        output_data(array('member_list' => $list), mobile_page($page_count));
    }

    /**
     * 整理数据结构
     * @param $member_list
     * @return array
     */
    private function _dataSort(& $member_list){
        $member_ids = array();
        $tmp_list = array();
        $member_type_list= Logic('j_member')->getMemberTypeList();
        foreach($member_list as $key=>$member){
            array_push($member_ids,$member['member_id']);
            $tmp_list[$member['member_id']]['member_id'] = $member['member_id'];
            $tmp_list[$member['member_id']]['member_name'] = $member['member_name'];
            $tmp_list[$member['member_id']]['member_avatar'] = getMemberAvatarForID($member['member_id']);
            $tmp_list[$member['member_id']]['member_time'] = date('Y-m-d', $member['member_time']);
            $tmp_list[$member['member_id']]['commission_amount'] = ncPriceFormat(0);
            $tmp_list[$member['member_id']]['member_type_display'] = $member_type_list[$member['member_type']]['name'];
            unset($member_list[$key]);
        }
        $member_list = $tmp_list;
        return $member_ids;
    }

    /**
     * 处理艺术家佣金
     * @param $member_list
     * @param $member_ids
     */
    private function _makeCommissionData(& $member_list, $member_ids){
        if(!empty($member_list) && !empty($member_ids)){
            $condition = array();
            $condition['from_member_id'] = array('in',$member_ids);
            $condition['dis_member_id'] = $this->member_info['member_id'];
            $field = "from_member_id,sum('commission_amount') as commission_amount";
            $member_model = Model('member');
            $contri_list = (array)$member_model->table('member_commission')->field($field)->where($condition)->group('from_member_id')->select();
            foreach($contri_list as $val){
                $member_list[$val['from_member_id']]['commission_amount'] = ncPriceFormat($val['commission_amount']);
            }
        }
    }
}