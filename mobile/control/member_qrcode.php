<?php
/**
 * 我的二维码
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_qrcodeControl extends mobileMemberControl
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 我的二维码
	 */
	public function indexOp()
	{
	    $member_model = Model('member');
		$d_member = $member_model->getUpperMember(array('member_id'=>$this->member_info['member_id']));
        if(!empty($d_member)){
            if(empty($d_member['dis_code'])){
                /** @var j_memberLogic $logic_member */
                $logic_member = Logic('j_member');
                if($logic_member->createCode($this->member_info)){
                    $d_member = $member_model->getUpperMember(array('member_id'=>$this->member_info['member_id']));
                }
            }

            //$uri = MOBILE_SITE_URL."/index.php?act=member_qrcode&op=bind_code&key=".$this->member_info['token']."&invite_code=".$d_member['dis_code'];
            $uri = WAP_SITE_URL."/tmpl/member/register.html?invite_code=".$d_member['dis_code'];

            $output_data = [
                'bind_uri'   =>$uri,
                'usertype'   =>$d_member['member_type'],
                'invite_code'=>$d_member['dis_code'],
            ];
            $_GET['url'] ? $output_data['go_url'] = $_GET['url']."?invite_code=".$d_member['dis_code'] : '';
            output_data($output_data);
        }else{
            output_error('数据读取失败，请联系管理员');
        }
	}

    /**
     * 二维码绑定
     */
    public function bind_codeOp(){
        if($this->member_info['member_type'] == 4){
            output_data('艺术家会员不能建立合伙人关系');
        }
        $invite_code = trim($_POST['invite_code']);
        if($invite_code == ''){
            output_error('该会员无邀请权限');
        }
        $model_member = Model('member');
        $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>$invite_code));
        if(empty($check_member_dcode) || in_array($check_member_dcode['member_type'],array(0,4))){
            output_error('该会员无邀请权限');
        }

        $data = array('top_member'=>$check_member_dcode['member_id']);
        $condition = array('member_id'=>$this->member_info['member_id']);
        $res = $model_member->editMemberDistribute($data, $condition);
        if($res){
            output_data(1);
        }else{
            output_error('绑定失败');
        }
    }

}