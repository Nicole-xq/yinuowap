<?php
/**
 * jssdk  分享
 *
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class jssdkControl extends mobileHomeControl{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 分享API的基础数据调用验证
     */
    public function get_sign_packageOp() {
        //获取公众号基础配置信息
        $model_mb_payment = Model('mb_payment');
        $mb_payment_info = $model_mb_payment->getMbPaymentInfo(array('payment_id' => 3));
        $appId = $mb_payment_info['payment_config']['appId'];
        $appSecret = $mb_payment_info['payment_config']['appSecret'];
        //引入JSSDK类
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'wxpay_jsapi'.DS.'wx_jssdk.php';

        if (empty($appId) || empty($appSecret) || empty($_GET['url']) || !is_file($inc_file)){
            $signPackage = array();
        }else{
            require($inc_file);
            $sdkObj = new JSSDK($appId, $appSecret, str_replace('&amp;', '&', $_GET['url']));
            $signPackage = $sdkObj->getSignPackage();
        }

        output_data($signPackage);
    }

}



