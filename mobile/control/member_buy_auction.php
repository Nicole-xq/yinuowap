<?php
/**
 * 拍卖商品支付
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/20 0020
 * Time: 10:42
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_buy_auctionControl extends mobileMemberControl {

    protected $logic_auction_buy;

    public function __construct() {
        parent::__construct();
        /** @var auction_buyLogic logic_auction_buy */
        $this->logic_auction_buy = Logic('auction_buy');
    }

    // 订单 分类 支付

    public function payOp()
    {
        $type = $_GET['type'];
        $order_sn = $_GET['pay_sn'];
        switch ($type) {
            case 'margin':
                $result = $this->pay_margin($order_sn);
                break;
            case 'auction':
                $result = $this->pay_auction($order_sn);
                break;
        }
    }

    // 保证金支付，支付方式选择
    protected function pay_margin($order_sn)
    {
        //验证订单号
        if (!preg_match('/^\d{16}$/',$order_sn)){
            showMessage("无效的订单号",'index.php?act=auctions','html','error');
        }

        $model_margin_orders = Model('margin_orders');
        //查询订单信息
        $order_info = $model_margin_orders->getOrderInfo(array('order_sn' => $order_sn));

        if (empty($order_info)) {
            output_error('未找到需要支付的订单');
        }

        // 排除非自己订单
        if ($order_info['buyer_id'] != $this->member_info['member_id']) {
            output_error('未找到需要支付的订单');
        }

        // 排除线下支付，保证金账户支付
        if ($order_info['order_type'] != 1) {
            $logic_auction_order = Logic('auction_order');
            // 保证金账户支付
            $result = $logic_auction_order->MarginPay($this->member_info['member_id'], $order_info['order_sn']);
            // 更新订单内容
            $order_info = $result['data']['order_info'];
        }

        // 获取下单支付页面的信息
        $result = $this->getOrderPay($order_info, $this->member_info['member_id']);

        unset($order_info);
        list($over_pay, $order_info, $pay) = $result;

        $data = array(
            'order_info'=>$order_info,
            'pay_info' => $pay,
            'over_pay' => $over_pay,
        );
        output_data($data);
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //诺币支付金额(之前支付中止，余额被锁定)
        $pay['payd_points_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        // 是否已经被保证金余额支付完成
        $over_pay = 0;
        //显示支付金额和支付方式

        // 保证金总数
        $pay['pay_amount_online'] = $order_info['margin_amount'] - $order_info['account_margin_amount'];
        // 充值卡支付金额
        $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
        // 预存款支付金额
        $pay['payd_pd_amount'] = $order_info['pd_amount'];
        // 诺币支付
        $pay['payd_points_amount'] = $order_info['points_amount'];
        // 保证金账户支付
        $pay['account_margin_amount'] = $order_info['account_margin_amount'];

        // 未支付金额
        $pay['payd_diff_amount'] = $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'] - $order_info['account_margin_amount'];
        $order_info['payment_type'] = '在线支付';
        $pay['order_type'] = 0;

        //如果保证金金额为0，转到支付订单逻辑
        if ($pay['payd_diff_amount'] == 0) {
            $over_pay = 1;
        }

        //输出订单描述
        if (!empty($pay['payd_diff_amount'])) {
            $pay['order_remind'] = '下单成功，请尽快完成支付。';
        } else {
            $pay['order_remind'] = '保证金支付成功';
        }

        if ($pay['pay_amount_online'] >= 0) {
            //显示支付接口列表
            $payment_list = Model('mb_payment')->getMbPaymentOpenList();
            if(!empty($payment_list)) {
                foreach ($payment_list as $k => $value) {
                     if ($value['payment_code'] == 'wxpay') {
                         unset($payment_list[$k]);
                         continue;
                     }
                     if($pay['pay_amount_online'] < LKLPAY){
                        if ($value['payment_code'] == 'lklpay') {
                             unset($payment_list[$k]);
                             continue;
                         }
                     }else{
                        if ($value['payment_code'] == 'alipay') {
                             unset($payment_list[$k]);
                             continue;
                         }
                     }
                    unset($payment_list[$k]['payment_id']);
                    unset($payment_list[$k]['payment_config']);
                    unset($payment_list[$k]['payment_state']);
                    unset($payment_list[$k]['payment_state_text']);
                }
            }
        }

        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['member_points'] = $this->member_info['member_points'];
        $pay['points_money'] = ncPriceFormat($this->member_info['member_points']/100);
        $pay['pay_amount'] = ncPriceFormat($pay['payd_diff_amount']);
        $pay['hide_pd'] = $order_info['api_pay_time'] >0?1:0;
        //是否显示站内余额操作(如果以前没有使用站内余额支付过)
        $pay['payed_amount'] = ncPriceFormat($pay['payd_rcb_amount']+$pay['payd_pd_amount']+$pay['payd_points_amount']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        // 返回需要的数组
        return array(
            $over_pay,
            $order_info,
            $pay
        );
    }

    // 拍卖订单支付
    protected function pay_auction($order_sn)
    {
        $pay_message = $_POST['pay_message'];
        $auction_order_sn = $order_sn;
        $member_id = $this->member_info['member_id'];
        //验证订单号
        if (!preg_match('/^\d{16}$/',$auction_order_sn)){
            output_error("无效的订单号");
        }
        /** @var orderModel $model_order */
        $model_order = Model('order');
        $condition = array(
            'order_sn' => $auction_order_sn
        );
        $order_info = $model_order->getOrderInfo($condition);
        if (!empty($pay_message)) {
            $update = ['order_message' => $pay_message];
            $where = ['order_id' => $order_info['order_id']];
            if (!empty($where)) {
                //更新买家留言信息
                $model_order->table('order_common')->where($where)->update($update);
            }
        }
        $result = $this->getAuctionOrderPay($order_info, $member_id);

        unset($order_info);

        list($order_info, $pay) = $result;

        $data = array(
            'order_info' => $order_info,
            'pay_info' => $pay
        );
        output_data($data);
    }

    /**
     * 获取订单的支付信息
     * @param $order_info array 订单信息
     * @param $member_id int 会员ID
     * @return array
     * */
    public function getAuctionOrderPay($order_info, $member_id)
    {
        //定义输出数组
        $pay = array();
        //支付提示主信息
        $pay['order_remind'] = '';
        //重新计算支付金额
        $pay['pay_amount_online'] = 0;
        $pay['pay_amount_underline'] = 0;
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payd_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payd_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['payd_diff_amount'] = 0;
        //账户可用金额
        $pay['member_pd'] = 0;
        $pay['member_rcb'] = 0;
        $if_underline = false;
        //显示支付金额和支付方式
        if ($order_info['payment_code'] != 'underline') {
            // 需要支付尾款
            if($order_info['order_amount']  < $order_info['margin_amount']){//订单金额小于保证金金额
                $pay['pay_amount_online'] = 0;
            }else{
                $pay['pay_amount_online'] = $order_info['order_amount'] - $order_info['margin_amount'];
            }

            // 充值卡支付金额
            $pay['payd_rcb_amount'] = $order_info['rcb_amount'];
            // 预存款支付金额
            $pay['payd_pd_amount'] = $order_info['pd_amount'];
            // 诺币支付
            $pay['payd_points_amount'] = $order_info['points_amount'];
            // 未支付金额
            $pay['payd_diff_amount'] = $pay['pay_amount_online'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'];
            $order_info['payment_type'] = '在线支付';
            $pay['order_type'] = 0;
        } else {
            if ($if_underline != true) {
                $if_underline = true;
            }
            $pay['pay_amount_underline'] = $order_info['order_amount'] - $order_info['margin_amount'];
            $order_info['payment_type'] = '线下支付';
            $pay['order_type'] = 1;
        }

        //如果订单未支付金额为0，转到支付订单逻辑
        if ($order_info['order_amount'] - $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $pay['payd_points_amount'] == 0) {
            $logic_auction_pay = Logic('auction_pay');
            if($logic_auction_pay->marginPayOrder($order_info, 0)){
                output_data(array('over_pay'=>1));exit;
//                redirect('index.php');
            }
//            redirect('index.php?act=auction_payment&op=auction_order&order_sn='.$order_info['order_sn']);
        }

        //是否显示站内余额操作(如果以前没有使用站内余额支付过且非货到付款)
        $pay['if_show_pdrcb_select'] = ($pay['pay_amount_underline'] == 0 && $pay['payd_rcb_amount'] == 0 && $pay['payd_pd_amount'] == 0 && $pay['payd_points_amount']);

        //输出订单描述
        if (empty($pay['pay_amount_underline']) && !empty($pay['pay_amount_online'])) {
            $pay['order_remind'] = '请您在48小时内完成支付，逾期订单将视为违约。 ';
        } else {
            $pay['order_remind'] = '拍卖订单支付成功';
        }

        if ($pay['pay_amount_online'] >= 0) {
            //显示支付接口列表
            $payment_list = Model('mb_payment')->getMbPaymentOpenList();

            if(!empty($payment_list)) {
                foreach ($payment_list as $k => $value) {
                     if ($value['payment_code'] == 'wxpay') {
                         unset($payment_list[$k]);
                         continue;
                     }
                    unset($payment_list[$k]['payment_id']);
                    unset($payment_list[$k]['payment_config']);
                    unset($payment_list[$k]['payment_state']);
                    unset($payment_list[$k]['payment_state_text']);
                }
            }
        }

        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['member_points'] = $this->member_info['member_points'];
        $pay['points_money'] = ncPriceFormat($this->member_info['member_points']/100);
        $pay['pay_amount'] = ncPriceFormat($pay['payd_diff_amount']);
        //是否显示站内余额操作(如果以前没有使用站内余额支付过)
        $pay['payed_amount'] = ncPriceFormat($pay['payd_rcb_amount']+$pay['payd_pd_amount']+$pay['payd_points_amount']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        $pay['hide_pd'] = $order_info['api_pay_time'] >0?1:0;
        // 返回需要的数组
        return array(
            $order_info,
            $pay,
        );
    }


    /*
     * 显示保证金页面
     * */
    public function show_margin_orderOp()
    {
        $auction_id = intval($_GET['auction_id']);
        //获取界面 会员 店铺 拍品 收货地址 支付方式信息
        $result = $this->logic_auction_buy->show_margin_order($auction_id, $this->member_info['member_id']);
        $auction = Model('auctions')->getAuctionsInfo(array('auction_id'=>$auction_id));
        $e = false;
        if( ($auction['auction_end_time'] < time() && empty($auction['auction_end_time_t'])) || (!empty($auction['auction_end_time_t']) && $auction['auction_end_time_t'] < time())){
            $e = true;
        }
        if (intval($_GET['address_id']) > 0) {
            $result['address_info'] = Model('address')->getDefaultAddressInfo(array('address_id'=>intval($_GET['address_id']),'member_id'=>$this->member_info['member_id']));
        }
        $model_margin_order = Model('margin_orders');
        $margin_order = $model_margin_order->getOrderInfo(array('buyer_id'=>$this->member_info['member_id'],'order_state'=>array('in','0,2,3'),'auction_id'=>$_GET['auction_id']));

        $data = array(
            'e'=>$e,
            'address_info' => $result['address_info'],
            'is_auction_end_true_t' => $result['is_auction_end_true_t'],
            'auction_info' => $this->logic_auction_buy->auction_info,
            'margin_order' =>empty($margin_order)?'':$margin_order
        );
        output_data($data);
    }
    
    /*
     * 生成保证金订单
     * */
    public function create_margin_orderOp()
    {
        $auction_id = $_POST['auction_id'];
        $pay_name = $_POST['pay_name'];
        $address_id  = $_POST['address_id'];
        $is_margin = $_POST['is_margin'];
        $is_anonymous = $_POST['is_anonymous'];
        if ($is_margin != 1) {
            showDialog('支付保证金异常', 'reload', 'error');
        }
        $member_id = $this->member_info['member_id'];

        //创建订单，记录线上或者线下支付方式，检查是否存在会员和拍品关系，不存在则创建。
        $result = $this->logic_auction_buy->create_margin_order($auction_id, $member_id, $pay_name, $address_id, $is_anonymous, 2, $_POST);

        if(!$result['state']) {
            output_error($result['msg']);
        }
        output_data(array('pay_sn' => $result['data']['order_sn'],'payment_code'=>$result['data']['payment_code']));
    }
    
    /*
     * 获取拍卖订单信息
     * */
    public function show_auction_orderOp()
    {
        $auction_order_id = $_GET['auction_order_id'];
        $margin_id = $_GET['margin_id'];
        $member_id = $this->member_info['member_id'];
        // 获取支付尾款的必要信息
        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->show_auction_order($auction_order_id, $margin_id, $member_id);
        if (!$result['state']) {
            output_error($result['msg']);
        }


        $data = array(
            'vat_deny' => $result['data']['vat_deny'],
            'vat_hash' => $result['data']['vat_hash'],
            'inv_info' => $result['data']['inv_info'],
            'store_info' => $result['data']['store_info'],
            'auction_order_info' => $result['data']['auction_order_info'],
            'margin_order_info' => $result['data']['margin_order_info'],
            'auction_info' => $result['data']['auction_info'],
            'underline' => true,
        );

        output_data($data);
    }

    /*
     * 更新订单支付方式和发票
     * */
    public function update_auction_orderOp()
    {
        $logic_auction_buy = Logic('auction_buy');
        // 查询发票信息和相关记录进行更新
        $member_id = $this->member_info['member_id'];
        $result = $logic_auction_buy->update_auction_order($_POST, $member_id);

        if ($result['state']) {
            //转向到商城支付页面
            output_data(array('pay_sn' => $result['data']['order_sn'], 'payment_code' => $result['data']['payment_code']));
        } else {
            output_error($result['msg']);
        }
    }

    public function check_auction_typeOp(){
        $model_margin_order = Model('margin_orders');
        $member_id = $this->member_info['member_id'];
        $condition = array(
            'buyer_id'=>$member_id,
            'order_state'=>array('neq','4'),
            'auction_id'=>array('neq',$_POST['auction_id'])
        );
        $re = $model_margin_order->getOrderInfo($condition);
        $res = empty($re)?0:1;
        output_data($res);
    }

    public function check_margin_orderOp(){
        $order_sn = $_GET['order_sn'];
        $info = Model('margin_orders')->getOrderInfo(array('order_sn'=>$order_sn));
        if(!empty($info)&&$info['order_state'] == 1){
            output_data(1);
        }else{
            output_data(0);
        }
    }
}
