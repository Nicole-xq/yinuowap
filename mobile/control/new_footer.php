<?php
/**
 * 底部
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class new_footerControl extends mobileHomeControl
{


	public function __construct()
	{
		parent::__construct();

	}

	public function indexOp()
	{
		$model_special = Model('mb_special');
		$item_info = $model_special->getMbSpecialItemInfo(array('item_type' => 'nav_list'));

		$prefix = 's'.$item_info['special_id'];
		$item = $item_info['item_data']['item'];

		if(!empty($item)){
			foreach($item as $key => $value){
				$item[$key]['image_path'] = UPLOAD_SITE_URL.DS.ATTACH_MOBILE . DS . 'special' . DS . $prefix . DS . $value['image'];
			}
		}
		$item_info['item_data']['item'] = $item;
		output_data(array('item_info' => $item_info));
	}
}