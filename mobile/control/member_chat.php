<?php
/**
 * 会员聊天
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
use Shopnc\Tpl;
class member_chatControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * node连接参数
     */
    public function get_node_infoOp() {
        $output_data = array('node_chat' => C('node_chat'),'node_site_url' => NODE_SITE_URL,'resource_site_url' => RESOURCE_SITE_URL);
        $model_chat = Model('web_chat');
        $member_id = $this->member_info['member_id'];
        $member_info = $model_chat->getMember($member_id);
        $output_data['member_info'] = $member_info;
        $u_id = intval($_GET['u_id']);
        if ($u_id > 0) {
            //新需求，所有客服对接到艺诺店铺
            $u_id = Model('store')->getStoreDetail(['store_id'=>C('yinuo_store_id')],'member_id','member_id');
            $member_info = $model_chat->getMember($u_id);
            $output_data['user_info'] = $member_info;
        }
        $goods_id = intval($_GET['chat_goods_id']);
        if ($goods_id > 0) {
            $goods = $model_chat->getGoodsInfo($goods_id);
            if($goods['sales_model'] == 2){
                $goods['goods_price'] = '可议价';
                $goods['goods_marketprice'] = '可议价';
                $goods['goods_promotion_price'] = '可议价';
            }
            $output_data['chat_goods'] = $goods;
        }
        $auction_id = intval($_GET['chat_auction_id']);
        if ($auction_id > 0) {
            $auctions = $model_chat->getAuctionInfo($auction_id);
            $output_data['chat_auctions'] = $auctions;
        }
        output_data($output_data);
    }

    /**
     * 最近联系人
     */
    public function get_user_listOp() {
        $member_list = array();
        $model_chat = Model('web_chat');

        $member_id = $this->member_info['member_id'];
        $n = intval($_POST['n']);
        if ($n < 1) $n = 50;
        if(intval($_POST['recent']) != 1) {
            $member_list = $model_chat->getFriendList(array('friend_frommid'=> $member_id),$n,$member_list);
        }
        $add_time = date("Y-m-d");
        $add_time30 = strtotime($add_time)-60*60*24*30;
        $member_list = $model_chat->getRecentList(array('f_id'=> $member_id,'add_time'=>array('egt',$add_time30)),10,$member_list);
        $member_list = $model_chat->getRecentFromList(array('t_id'=> $member_id,'add_time'=>array('egt',$add_time30)),10,$member_list);
        $member_info = $model_chat->getMember($member_id);
        $node_info = array();
        $node_info['node_chat'] = C('node_chat');
        $node_info['node_site_url'] = NODE_SITE_URL;
        $node_info['chat_count_num'] = $this->getMsgCountByType(1);
        $node_info['message_count_num'] = $this->getMsgCountByType(2);
        output_data(array('node_info' => $node_info,'member_info' => $member_info,'list' => $member_list));
    }

    /**
     * 会员信息
     *
     */
    public function get_infoOp(){
        $val = '';
        $member = array();
        $model_chat = Model('web_chat');
        $types = array('member_id','member_name','store_id','member');
        $key = $_POST['t'];
        $member_id = intval($_POST['u_id']);
        if($member_id > 0 && trim($key) != '' && in_array($key,$types)){
            $member_info = $model_chat->getMember($member_id);
            output_data(array('member_info' => $member_info));
        } else {
            output_error('参数错误');
        }
    }

    /**
     * 发消息
     *
     */
    public function send_msgOp(){
        $member = array();
        $model_chat = Model('web_chat');
        $member_id = $this->member_info['member_id'];
        $member_name = $this->member_info['member_name'];
        $t_id = intval($_POST['t_id']);
        $t_name = trim($_POST['t_name']);
        $member = $model_chat->getMember($t_id);
        if ($t_name != $member['member_name']) output_error('接收消息会员账号错误');

        $msg = array();
        $msg['f_id'] = $member_id;
        $msg['f_name'] = $member_name;
        $msg['t_id'] = $t_id;
        $msg['t_name'] = $t_name;
        $msg['t_msg'] = trim($_POST['t_msg']);
        if ($msg['t_msg'] != '') $chat_msg = $model_chat->addMsg($msg);
        if ($chat_msg['m_id']) {
            $goods_id = intval($_POST['chat_goods_id']);
            if ($goods_id > 0) {
                $goods = $model_chat->getGoodsInfo($goods_id);
                $chat_msg['chat_goods'] = $goods;
            }
            output_data(array('msg' => $chat_msg));
        } else {
            output_error('发送失败，请稍后重新发送');
        }
    }

    /**
     * 删除最近联系人消息
     *
     */
    public function del_msgOp(){
        $model_chat = Model('web_chat');
        $member_id = $this->member_info['member_id'];
        $t_id = intval($_POST['t_id']);
        $condition = array();
        $condition['f_id'] = $member_id;
        $condition['t_id'] = $t_id;
        $model_chat->delChatMsg($condition);
        $condition = array();
        $condition['t_id'] = $member_id;
        $condition['f_id'] = $t_id;
        $model_chat->delChatMsg($condition);
        output_data(1);
    }

    /**
     * 商品图片和名称
     *
     */
    public function get_goods_infoOp(){
        $model_chat = Model('web_chat');
        $goods_id = intval($_POST['goods_id']);
        $goods = $model_chat->getGoodsInfo($goods_id);
        output_data(array('goods' => $goods));
    }

    /**
     * 未读消息查询
     *
     */
    public function get_msg_countOp(){
        $type = $_GET['type'] ? $_GET['type']: 1;
        output_data($this->getMsgCountByType($type));
    }

    /**
     * 全部标记为已读
     */
    public function setReadStateOp(){
        $result = false;
        if ($_GET['type'] == 1){
            $condition = ['t_id'=>$this->member_info['member_id']];
            $data = ['r_state'=>1];
            $result = Model('web_chat')->table('chat_msg')->where($condition)->update($data);
        }elseif ($_GET['type'] == 2){
            $condition = [
                'message_type'     =>1,
                'to_member_id'     =>$this->member_info['member_id'],
                'no_del_member_id' =>$this->member_info['member_id'],
                'no_read_member_id'=>$this->member_info['member_id'],
                'read_member_id'   =>'eq'
            ];
            $updateData = [
                'read_member_id'=>",".$this->member_info['member_id'].",",
            ];
            $messageModel = Model('message');
            $messageModel->updateCommonMessage($updateData,$condition);//新增阅读者
            $condition['read_member_id'] = 'neq';
            $messageModel->executeSQL($condition);//累加阅读者
            $result = true;
        }
        $result ? output_data($result) : output_error($result);
    }

    /*
     * 获取消息和站内信未读条数
     * @param int $type{1：消息；2：站内信；3：检测未读}
     */
    private function getMsgCountByType($type){
        $count_num = 0;
        if ($type == 1){
            $add_time30 = strtotime(date("Y-m-d"))-60*60*24*30;
            $model_chat = Model('web_chat');
            $member_list = $model_chat->getRecentList(array('f_id'=> $this->member_info['member_id'],'add_time'=>array('egt',$add_time30)),10);
            $member_list = $model_chat->getRecentFromList(array('t_id'=> $this->member_info['member_id'],'add_time'=>array('egt',$add_time30)),10,$member_list);
            foreach ($member_list as $value){
                if ($value['r_state'] == 2){
                    $count_num++;
                }
            }
        }elseif ($type == 2){
            $condition = [
                'message_type'     =>1,
                'to_member_id'     =>$this->member_info['member_id'],
                'no_del_member_id' =>$this->member_info['member_id'],
                'no_read_member_id'=>$this->member_info['member_id']
            ];
            $count_num = Model('message')->countMessage($condition);
        }elseif ($type == 3){
            $count_num = $this->getMsgCountByType(2);
            if($count_num < 1){
                $count_num = $this->getMsgCountByType(1);
            }
        }

        return $count_num;
    }

    /**
     * 聊天记录查询
     *
     */
    public function get_chat_logOp(){
        $member_id = $this->member_info['member_id'];
        $t_id = intval($_POST['t_id']);
        $add_time_to = date("Y-m-d");
        $time_from = array();
        $time_from['7'] = strtotime($add_time_to)-60*60*24*7;
        $time_from['15'] = strtotime($add_time_to)-60*60*24*15;
        $time_from['30'] = strtotime($add_time_to)-60*60*24*30;

        $key = $_POST['t'];
        if(trim($key) != '' && array_key_exists($key,$time_from)){
            $model_chat = Model('web_chat');
            $list = array();
            $condition_sql = " add_time >= '".$time_from[$key]."' ";
            $condition_sql .= " and ((f_id = '".$member_id."' and t_id = '".$t_id."') or (f_id = '".$t_id."' and t_id = '".$member_id."'))";
            $list = $model_chat->getLogList($condition_sql,$this->page);

            $total_page = $model_chat->gettotalpage();
            output_data(array('list' => $list), mobile_page($total_page));
        }
    }

    /**
     * 我的通知列表
     */
    public function message_listOp(){
        $model_message = Model('message');
        $messageType = isset($_REQUEST['messageType']) ? $_REQUEST['messageType'] : 0;
        if ($messageType && $messageType == 3) {
            $messageType = 3;
        } else {
            $messageType = 0;
        }
        $condition = [
            'from_member_id'=>0,
            'message_type'=>$messageType,
            'to_member_id'=>$this->member_info['member_id'],
            'no_del_member_id'=>$this->member_info['member_id'],
        ];
        $field = "message.message_id,message.message_time,message.message_title,message.message_open,message.message_body,";
        $field .= "message.read_member_id,message.message_pic,message.url_type,message.url_id";
        $message_list = $model_message->getMessageList($condition, $field, $this->page, 'message_id desc');
        $page_count = $model_message->gettotalpage();
        foreach ($message_list as &$value){
            $value['message_time'] = date('Y-m-d H:i:s',$value['message_time']);
            if (!empty($value['read_member_id'])){
                $tmp_read_arr = explode(',',$value['read_member_id']);
                if (in_array($this->member_info['member_id'],$tmp_read_arr)){
                    $value['message_open'] = '1';
                }
            }
        }
        unset($value);
        $count_info['chat_count_num'] = $this->getMsgCountByType(1);
        $count_info['message_count_num'] = $this->getMsgCountByType(2);
        output_data(['message_list' => $message_list,'count_info'=>$count_info], mobile_page($page_count));
    }

    /**
     * 我的通知列表
     */
    public function message_detailOp(){
        if (empty($_GET['message_id'])){
            output_error('无效参数');
        }
        $model_message = Model('message');
        $condition = [
            'from_member_id'=>0,
            'message_type'=>1,
            'to_member_id'=>$this->member_info['member_id'],
            'no_del_member_id'=>$this->member_info['member_id'],
            'message_id'=>intval($_GET['message_id'])
        ];
        $field = "message.message_id,message.message_time,message.message_body,message.read_member_id,message.mmt_code";
        $message_info = $model_message->getMessageInfo($condition, $field);
        if (!empty($message_info)){
            $message_info['message_time'] = date('Y-m-d',$message_info['message_time']);
            $site_phone = Model('setting')->getRowSetting('site_phone');
            $message_info['site_phone'] = $site_phone['value'];
            //消息模板logo
            $msg_info = Model('member_msg_tpl')->getMemberMsgTplInfo(['mmt_code'=>$message_info['mmt_code']], $field = 'mmt_message_logo');
            if ($msg_info['mmt_message_logo']){
                $message_info['mmt_message_logo'] = UPLOAD_SITE_URL.DS.ATTACH_MOBILE.'/boot/'.$msg_info['mmt_message_logo'];
            }
            //新增阅读者
            $tmp_read_id = explode(',',$message_info['read_member_id']);
            if (in_array($this->member_info['member_id'],$tmp_read_id)){
                output_data($message_info);
            }elseif (empty($tmp_read_id)){
                $tmp_read_str = ','.$this->member_info['member_id'].",";
            }elseif (!empty($tmp_read_id) && !in_array($this->member_info['member_id'],$tmp_read_id)){
                $tmp_read_id[] = $this->member_info['member_id'];
                $tmp_read_id = array_unique(array_filter($tmp_read_id));//去除相同
                sort($tmp_read_id);//排序
                $tmp_read_str = ",".implode(',',$tmp_read_id).",";
            }
            if ($tmp_read_str){
                $updateData = ['read_member_id'=>$tmp_read_str];
                $model_message->updateCommonMessage($updateData,$condition);
            }
        }
        output_data($message_info);
    }

    /**
     * 删除站内信
     */
    public function message_deleteOp() {
        $message_id = intval($_GET['message_id']);
        if (empty($message_id)){
            output_error('参数错误');
        }
        $param = [
            'message_id_in' =>$message_id,
            'message_type'  =>1,
            'from_member_id'=>0,
        ];
        $drop_state = Model('message')->dropBatchMessage($param,$this->member_info['member_id']);
        if ($drop_state){
            output_data($drop_state);
        }else {
            output_error('操作失败');
        }
    }
}
