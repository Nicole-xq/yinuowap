<?php
/**
 * 落地页
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class landingControl extends mobileHomeControl
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 趣猜列表
     */
    public function indexOp()
    {
        $id = $_GET['id'];
        $id or output_error('参数错误');
        $landing_info = Model('landing_page')->getLandingPageInfo(['id' => $id, 'state' => 2], '*', 'id desc');
        $landing_info or output_error('参数错误');

        if ($landing_info) {
            $landing_info['banner_image'] = UPLOAD_SITE_URL . DS . ATTACH_MOBILE . '/boot/' . $landing_info['banner_image'];
            $landing_info['footer_image'] = UPLOAD_SITE_URL . DS . ATTACH_MOBILE . '/boot/' . $landing_info['footer_image'];
        }

        $where = 'landing_page_id=' . $landing_info['id'];
        $list = Model('')->table('landing_relation_goods')->where($where)->select();

        $goods_id = '';
        $introduce = array();
        foreach ($list as $item) {
            $goods_id .= ',' . $item['goods_id'];
            $introduce[$item['goods_id']] = $item['introduce'];
        }
        $goods_id = trim($goods_id, ',');
        $condition = array('goods_id' => ['in', $goods_id],);
        $field = 'goods_id,goods_name,goods_price,goods_promotion_price,goods_image,goods_storage,store_id';
        $limit = $landing_info['show_goods_num'];
        $model_goods = Model('goods');
        $condition['goods_storage'] = ['gt', 0];
        $goods_list = $model_goods->getGoodsList($condition, $field, '', '', $limit);
        $goods_count = count($goods_list);
        if ($goods_count < count($list) && $limit2 = $limit - $goods_count) {
            $condition['goods_storage'] = ['eq', 0];
            $data = $model_goods->getGoodsList($condition, $field, '', '', $limit2);
            $goods_list = array_merge($goods_list, $data);
        }

        $landing_info['storage_count'] = 0;
        $data = array();
        foreach ($goods_list as $item) {
            $landing_info['storage_count'] += $item['goods_storage'];
            $tmp = array();
            $tmp['goods_id'] = $item['goods_id'];
            $tmp['goods_name'] = $item['goods_name'];
            $tmp['goods_price'] = $item['goods_price'];
            $tmp['goods_promotion_price'] = $item['goods_promotion_price'];
            $tmp['goods_image'] = cthumb($item['goods_image'], 360, $item['store_id']);
            $tmp['goods_storage'] = $item['goods_storage'];
            $tmp['introduce'] = $introduce[$item['goods_id']];
            $data[] = $tmp;
        }

        output_data(array('landing_info' => $landing_info, 'goods_list' => $data));
    }
}
