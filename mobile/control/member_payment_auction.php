<?php
/**
 * 支付
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/3/23 0023
 * Time: 14:33
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_payment_auctionControl extends mobileMemberControl {
    private $payment_code;
    private $payment_config;

    public function __construct() {
        parent::__construct();

        if($_GET['op'] != 'payment_list' && !$_POST['payment_code'] && !$_POST['op'] != 'pay_underline_order') {
            $payment_code = 'alipay';

            if(in_array($_GET['op'], array('wx_app_pay', 'wx_app_pay3', 'wx_app_vr_pay', 'wx_app_vr_pay3'), true)) {
                $payment_code = 'wxpay';
            }
            else if (in_array($_GET['op'],array('alipay_native_pay','alipay_native_vr_pay'),true)) {
                $payment_code = 'alipay_native';
            }
            else if (isset($_GET['payment_code'])) {
                $payment_code = $_GET['payment_code'];
            }

            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $payment_code;
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                output_error('支付方式未开启');
            }

            $this->payment_code = $payment_code;
            $this->payment_config = $mb_payment_info['payment_config'];

        }
    }

    /*
     * wap使用支付保证金订单
     * */
    public function pay_marginOp()
    {
        @header("Content-type: text/html; charset=".CHARSET);
        $order_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{16}$/',$order_sn)){
            exit('支付单号错误');
        }
        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi','lklpay'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];
        } else {
            exit('支付方式提交错误');
        }
        // 站内支付完成诺币支付和预存款、充值卡支付
        $pay_info = $this->_get_margin_order_info($order_sn,$_GET);
        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }

        //第三方API支付
        $this->_api_pay($pay_info, 0);

    }

    /*
     * APP支付保证金第一步，站内支付
     * */
    public function pay_margin_appOp()
    {
        $order_sn = $_GET['pay_sn'];
        if (!preg_match('/^\d{16}$/',$order_sn)){
            output_error('支付单号错误');
        }
        // 站内支付完成诺币支付和预存款、充值卡支付
        $pay_info = $this->_get_margin_order_info($order_sn,$_GET);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }
        output_data(
            array(
                'pay_sn' => $pay_info['order_sn'],
                'payment_code'=>$pay_info['payment_code']
            )
        );
    }

    /**
     * 处理订单支付流程
     * @param $order_sn int 订单号
     * @param $rcb_pd_pay array GET信息
     * @return array $order_info
     * */
    protected function _get_margin_order_info($order_sn,$rcb_pd_pay = array())
    {
        //获取订单信息
        $logic_auction_order = Logic('auction_order');
        $logic_auction_pay = Logic('auction_pay');
        $model_auctions = Model('auctions');
        $model_auction_special = Model('auction_special');
        //验证订单是否有效
        $result = $logic_auction_order->VerifyMarginOrderPay($order_sn);
        if(!$result['state']) {
            return array('error' => $result['msg']);
        }
        $order_info = $result['data'];
        // 检查是否已用保证金余额支付完成
        $pay_amount = ncPriceFormat($order_info['margin_amount'] - $order_info['account_margin_amount']);

        // 如果已经支付完成 更新订单
        if ($pay_amount == 0 && !empty($rcb_pd_pay)) {
            $order_info = $logic_auction_pay->completeMarginOrder($order_info['order_sn']);
            if ($rcb_pd_pay['is_app'] == 1) {
                output_data(0);
            } else {
                $this->sendAuctionsMarginMessage($order_info);
                redirect(WAP_SITE_URL.'/tmpl/member/auction_order_list.html');
            }
        }

        //消耗诺币（积分） 记录日志
        if(!empty($rcb_pd_pay) && $rcb_pd_pay['points_amount'] > 0){
            // 诺币支付，如果完全支付更细会员和拍品关系表
            $result = $logic_auction_pay->NuobiPay($order_sn, $this->member_info['member_id'], $this->member_info['member_name'], 0);
            $order_info = $result['data'];
        }
        //站内余额支付
        if ($rcb_pd_pay['pd_pay'] == 1) {
            $order_info = $this->_pd_pay($order_info,$rcb_pd_pay, 0);
            $margin_info = Model('margin_orders')->getOrderInfo(array('margin_id'=>$order_info['margin_id']));
            $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);

            $day_num = Logic('auction')->getInterestDay(TIMESTAMP,$auction_info['auction_end_time']);//计息天数
            $condition = array(
                    'buyer_id'=>$order_info['buyer_id'],
                    'order_state'=>array('neq','4'),
                    'margin_id'=>array('neq',$order_info['margin_id'])
                );
                $tmp_re = Model('margin_orders')->getOrderInfo($condition);
            if(TIMESTAMP < strtotime($auction_info['interest_last_date']) && $order_info['order_state'] == 1 && ($auction_info['auction_type'] != 1 || empty($tmp_re))){
                $member_distribute_logic = Logic('j_member_distribute');
                //添加利息待结算记录
                //$member_distribute_logic->margin_refund($margin_info,$day_num,$auction_info['auction_bond_rate']);
                //添加佣金待结算记录
                $result_3 = $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);

                //添加区域代理佣金待结算记录
                $member_distribute_logic->agent_margin_refund($margin_info, $day_num,0);
            }
//            dd();
        }
//        echo 444;exit;
        //计算本次需要在线支付的订单总金额
        $pay_amount = 0;
        if ($order_info['order_type'] == 0) {
            $pay_amount = floatval(ncPriceFormat($order_info['margin_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['account_margin_amount']));
        }

        if ($pay_amount == 0 && !empty($rcb_pd_pay)) {
            if ($rcb_pd_pay['is_app'] == 1) {
                output_data(0);
            } else {
                $this->sendAuctionsMarginMessage($order_info);
                redirect(WAP_SITE_URL.'/tmpl/member/auction_order_list.html');
            }
        }

        $order_info['api_pay_amount'] = ncPriceFormat($pay_amount);

        $result = Model('margin_orders')->editOrder(array('api_pay_time'=>TIMESTAMP,'payment_code'=>$this->payment_code),array('margin_id'=>$order_info['margin_id']));

        if(!$result) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }
        return $order_info;
    }

    /**
     * 发送客户系统通知
     * @param $condition
     * @param $order_info
     */
    private function sendAuctionsMarginMessage($order_info) {
        $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $order_info['auction_id'];
        $url = filterUrl($url);
        $url = sinaShortenUrl($url);
        /** @var margin_ordersModel $model_margin_order */
        $model_margin_order = Model('margin_orders');
        //已交保证金金额
        $getMarginSum=$model_margin_order->getMarginSum(
            [
                'auction_id' => $order_info['auction_id'],
                'buyer_id' => $order_info['buyer_id'],
                'order_state' => 1
            ],
            'margin_amount'
        );
        $margin_count = $getMarginSum ?: 0;
        $param = [
            'code'     =>'aucution_margin_pay',
            'member_id'=>intval($order_info['buyer_id']),
            'number'   =>['mobile'=>intval($order_info['buyer_phone'])],
            'param'    =>[
                'wx_first'=>'保证金支付成功',
                'wx_remark'=>'请及时关注拍卖动态',
                'auction_name'=>$order_info['auction_name'],
                'margin_amount'=>$order_info['margin_amount'],
                'money'=>$order_info['margin_amount'],
                'margin_count' => $margin_count,
                'url' => $url,
                'wx_url' => $url
            ]
        ];
        QueueClient::push('sendMemberMsg', $param);
    }

    /*
     * wap使用支付拍卖订单
     * */
    public function pay_auctionOp()
    {
        @header("Content-type: text/html; charset=".CHARSET);
        // 订单号
        $order_sn = $_GET['pay_sn'];
        // 需要支付保证金为0时直接跳转的参数
        if (is_null($order_sn)) {
            $order_sn = $_GET['order_sn'];
        }

        if(!preg_match('/^\d{16}$/',$order_sn)){
            exit('参数错误');
        }

        if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi'))) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_GET['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                exit('支付方式未开启');
            }

            $this->payment_code = $_GET['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];
        } else {
            exit('支付方式提交错误');
        }

        // 完成站内支付，和诺币支付
        $pay_info = $this->_get_auction_order_info($order_sn, $_GET);
        if(isset($pay_info['error'])) {
            exit($pay_info['error']);
        }
        //转到第三方API支付，和线下支付
        $this->_api_pay($pay_info, 1);
    }

    /**
     * APP支付保证金订单
     * */
    public function pay_auction_appOp()
    {
        $order_sn = $_GET['pay_sn'];
        if(!preg_match('/^\d{16}$/',$order_sn)){
            output_error('参数错误');
        }
        // 完成站内支付，和诺币支付
        $pay_info = $this->_get_auction_order_info($order_sn, $_GET);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }
        output_data(
            array(
                'pay_sn' => $pay_info['auction_order_sn'],
                'payment_code'=>$pay_info['payment_code']
            )
        );
    }

    /**
     * 获取拍卖订单信息，完成站内支付，和诺币支付
     * @param $order_sn int 订单编号
     * @param $rcb_pd_pay array GET数据
     * @return array 最新的订单信息
     * */
    protected function _get_auction_order_info($order_sn,$rcb_pd_pay = array())
    {
        //获取订单信息
        /** @var auction_orderLogic $logic_auction_order */
        /** @var auction_payLogic $logic_auction_pay */
        $logic_auction_order = Logic('auction_order');
        $logic_auction_pay = Logic('auction_pay');
        //验证订单是否有效
        $result = $logic_auction_order->VerifyAuctionOrderPay($order_sn);

        if(!$result['state']) {
            output_error($result['msg']);
        }
        $order_info = $result['data'];
        //如果金额为0，或者已经被保证账户支付完成的,转到支付成功页

        $pay_money = $order_info['order_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $order_info['points_amount'] - $order_info['api_pay_amount'];
        //     1             1
        if($pay_money < $order_info['margin_amount']){
            //如果保证金金额大于订单金额
            /** @var auction_payLogic $logic_auction_order */
            $logic_auction_pay = Logic('auction_pay');
            if($logic_auction_pay->marginPayOrder($order_info, $pay_money)){
                if ($rcb_pd_pay['is_app'] == 1) {
                    output_data(0);
                } else {
                    redirect(WAP_SITE_URL.'/tmpl/member/auction_order_list.html');
                }
            }else{
                return array('error' => '更新订单信息发生错误，请重新支付');
            }

        }
        $pay_money = $pay_money - $order_info['margin_amount'];
        if ($pay_money == 0 && !empty($rcb_pd_pay)) {
            if ($rcb_pd_pay['is_app'] == 1) {
                output_data(0);
            } else {
                redirect(WAP_SITE_URL.'/tmpl/member/auction_order_list.html');
            }
        }

        //消耗诺币（积分） 记录日志
        if(!empty($rcb_pd_pay) && $rcb_pd_pay['points_amount'] > 0){
            // 诺币支付，如果完全支付订单表 20 支付完成状态
            $result = $logic_auction_pay->NuobiPay($order_sn, $this->member_info['member_id'], $this->member_info['member_name'], 1);
            $order_info = $result['data'];
        }
        if (!empty($rcb_pd_pay)) {
            //站内余额支付
            $order_info = $this->_pd_pay($order_info,$rcb_pd_pay, 1);
        }
        //计算本次需要在线支付金额
        $api_pay_amount = 0;
        if ($order_info['payment_code'] != 'underline') {
            $api_pay_amount = floatval(ncPriceFormat($order_info['order_amount'] - $order_info['margin_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'] - $order_info['points_amount']));
        } else {
            $api_pay_amount = ncPriceFormat($order_info['order_amount'] - $order_info['margin_amount']);
        }

        //如果所需支付金额为0，转到支付成功页
        if (empty($api_pay_amount)) {
            if ($rcb_pd_pay['is_app'] == 1) {
                output_data(0);
            } else {
                redirect(WAP_SITE_URL.'/tmpl/member/auction_order_list.html');
            }
        }

        // 线上支付更新API time
        if ($order_info['payment_code'] != 'underline') {
            $result = Model('order')->where(array('order_id'=>$order_info['order_id']))->update(array('api_pay_time'=>TIMESTAMP));
        } else {
            $result = true;
        }

        if(!$result) {
            return array('error' => '更新订单信息发生错误，请重新支付');
        }
        $order_info['api_pay_amount'] = ncPriceFormat($api_pay_amount);
        return $order_info;
    }

    /**
     * 站内余额支付(充值卡、预存款支付) 实物订单
     * @param $order_info array 订单信息
     * @param $post array POST数据
     * @param $type int 0 保证金 1 尾款
     * @return array order_info
     */
    private function _pd_pay($order_info, $post, $type = 0) {
        if (empty($post['password'])) {
            return $order_info;
        }
        $model_member = Model('member');
        $buyer_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($buyer_info['member_paypwd'] == '' || $buyer_info['member_paypwd'] != md5($post['password'])) {
            return $order_info;
        }

        if ($buyer_info['available_rc_balance'] == 0) {
            $post['rcb_pay'] = null;
        }
        if ($buyer_info['available_predeposit'] == 0) {
            $post['pd_pay'] = null;
        }
        if (floatval($order_info['rcb_amount']) > 0 || floatval($order_info['pd_amount']) > 0) {
            return $order_info;
        }

        try {
            $model_member->beginTransaction();
            $logic_auction_pay = Logic('auction_pay');
            //使用充值卡支付
            if (!empty($post['rcb_pay'])) {
                $order_info = $logic_auction_pay->rcbPay($order_info, $post, $buyer_info, $type);
            }

            //使用预存款支付
            if (!empty($post['pd_pay'])) {
                $order_info = $logic_auction_pay->pdPay($order_info, $post, $buyer_info, $type);
            }

            $model_member->commit();
        } catch (Exception $e) {
            $model_member->rollback();
            exit($e->getMessage());
        }

        return $order_info;
    }

    /**
     * 第三方在线支付接口
     * @param $type int 类型 0 保证金 1
     */
    private function _api_pay($order_pay_info, $type = 0) {
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';

        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }

        require_once($inc_file);
        if ($type == 0) {
            $order_sn = $order_pay_info['order_sn'];
            $order_pay_info['pay_sn'] = $order_sn;
            // 支付接口判断订单类型
            $order_pay_info['is_margin'] = true;
            $order_pay_info['subject'] = '保证金订单：'.$order_sn;
            $order_id = $order_pay_info['margin_id'];
        } elseif ($type == 1) {
            $order_sn = $order_pay_info['order_sn'];
            $order_pay_info['pay_sn'] = $order_sn;
            // 支付接口判断订单类型
            $order_pay_info['is_margin'] = false;
            $order_pay_info['subject'] = '拍品尾款订单：'.$order_sn;
            $order_id = $order_pay_info['order_id'];
        }

        $param = $this->payment_config;

        // wxpay_jsapi
        if ($this->payment_code == 'wxpay_jsapi') {
            $param['orderSn'] = $order_pay_info['pay_sn'];
            $param['orderFee'] = (int) (ncPriceFormat(100 * $order_pay_info['api_pay_amount']));
            $param['orderInfo'] = $order_pay_info['pay_sn'] . '订单';
            $param['orderAttach'] = ($type == 0 ? 'm' : 'a');
            $type == 0 ? $param['limit_pay'] = 'no_credit': '';//微信wap支付保证金禁用信用卡
            $api = new wxpay_jsapi();
            $api->setConfigs($param);
            try {
                echo $api->paymentHtml($this,$order_pay_info['auction_id']);
            } catch (Exception $ex) {
                if (C('debug')) {
                    header('Content-type: text/plain; charset=utf-8');
                    echo $ex, PHP_EOL;
                } else {
                    Tpl::output('msg', $ex->getMessage());
                    Tpl::showpage('payment_result');
                }
            }
            exit;
        }

        $param['order_sn'] = $order_pay_info['pay_sn'];
        $param['order_amount'] = $order_pay_info['api_pay_amount'];
        $param['order_type'] = ($type == 0 ? 'm' : 'a');
        $param['order_sn'] = $order_pay_info['pay_sn'];
        $param['order_amount'] = $order_pay_info['api_pay_amount'];
//        echo $this->payment_code;exit;
        if($this->payment_code == 'lklpay'){
            $member_id = $order_pay_info['buyer_id'];
            $lkl_order_sn = Logic('buy_1')->makePaySn($member_id);
            $lkl_order = Logic('order')->createLklOrder($order_pay_info['pay_sn'],$lkl_order_sn);
//            print_R($order_pay_info);exit;
            $param['subject'] = $order_pay_info['subject'];
            $param['body'] = $order_pay_info['pay_sn'];
            $param['lkl_order'] = $lkl_order_sn;
            $param['member_id'] = $member_id;
//            print_R($param);exit;
            $payment_api = new \Paymax\example\lklpay();
        }else{
            $payment_api = new $this->payment_code();
        }
//        $payment_api = new $this->payment_code();
        $return = $payment_api->submit($param);
        echo $return;
        exit;
    }

    /*
     * 上传支付凭证提交到管理后台
     * */
    public function pay_underline_orderOp()
    {
        // 2.获取订单信息验证是否是新订单
        if (intval($_POST['is_margin']) == 1) {
            // 保证金
            $model_margin_orders = Model('margin_orders');
            $result = $model_margin_orders->getOrderInfo(array('order_sn' => $_POST['pay_sn']));
            $state = $result['order_state'] == 1;
            $type = 0;
            $order_sn = $result['order_sn'];
        } else {
            // 尾款
            $model_order = Model('order');
            $result = $model_order->getOrderInfo(array('order_sn' => $_POST['pay_sn']));
            $state = $result['order_state'] != 10;
            $type = 1;
            $order_sn = $result['order_sn'];
        }

        if (!$result) {
            output_error('订单不存在');
        }
        if ($state) {
            output_error('订单状态异常');
        }

        // 3.上传图片
        if(empty($_POST['pay_voucher'])) {
            output_error('请上传付款凭证');
        }
        $logic_auction_order = Logic('auction_order');
        // 4.修改订单状态
        $result = $logic_auction_order->changeOrderStateInConfirm($order_sn, $_POST['pay_voucher'], $type);
        if ($result['state']) {
            output_data(0);
        } else {
            output_error('操作失败，请重试');
        }
    }

    /*
     * 微信APP保证金订单支付
     * */
    public function wx_app_pay_margin3Op()
    {
        $pay_sn = $_POST['pay_sn'];

        $pay_info = $this->_get_margin_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = '保证金订单：'.$pay_sn;
        $param['amount'] = $pay_info['api_pay_amount'] * 100;

        $data = $this->_get_wx_pay_info3($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
    }

    /*
     * 微信APP支付拍卖订单
     * */
    public function wx_app_pay_auction3Op()
    {
        $pay_sn = $_POST['pay_sn'];
        $rcb_pd_pay = $_POST;
        $rcb_pd_pay['is_app'] = 1;
        $pay_info = $this->_get_auction_order_info($pay_sn,$rcb_pd_pay);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $param = array();
        $param['pay_sn'] = $pay_sn;
        $param['subject'] = '拍卖订单：'.$pay_sn;
        $param['amount'] = $pay_info['api_pay_amount'] * 100;
        $model_mb_payment = Model('mb_payment');
        $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo(array('payment_code'=>'wxpay'));
        $this->payment_config = $mb_payment_info['payment_config'];
        $data = $this->_get_wx_pay_info3($param);
        if(isset($data['error'])) {
            output_error($data['error']);
        }
        output_data($data);
    }

    /**
     * 获取微信支付参数
     * @param $pay_param array 支付参数信息
     * @return array
     * */
    protected function _get_wx_pay_info3($pay_param)
    {
        $noncestr = md5(rand());

        $param = array();
        $param['appid'] = $this->payment_config['wxpay_appid'];
        $param['mch_id'] = $this->payment_config['wxpay_partnerid'];
        $param['nonce_str'] = $noncestr;
        $param['body'] = $pay_param['subject'];
        $param['out_trade_no'] = $pay_param['pay_sn'];
        $param['total_fee'] = $pay_param['amount'];
        $param['spbill_create_ip'] = get_server_ip();
        $param['notify_url'] = MOBILE_SITE_URL . '/api/payment/wxpay3/notify_url.php';
        $param['trade_type'] = 'APP';

        $sign = $this->_get_wx_pay_sign3($param);
        $param['sign'] = $sign;

        $post_data = '<xml>';
        foreach ($param as $key => $value) {
            $post_data .= '<' . $key .'>' . $value . '</' . $key . '>';
        }
        $post_data .= '</xml>';

        $prepay_result = http_postdata('https://api.mch.weixin.qq.com/pay/unifiedorder', $post_data);
        $prepay_result = simplexml_load_string($prepay_result);
        if($prepay_result->return_code != 'SUCCESS') {
            return array('error' => '支付失败code:1002');
        }

        // 生成正式支付参数
        $data = array();
        $data['appid'] = $this->payment_config['wxpay_appid'];
        $data['noncestr'] = $noncestr;
        //微信修改接口参数，否则IOS报解析失败
        //$data['package'] = 'prepay_id=' . $prepay_result->prepay_id;
        $data['package'] = 'Sign=WXPay';
        $data['partnerid'] = $this->payment_config['wxpay_partnerid'];
        $data['prepayid'] = (string)$prepay_result->prepay_id;
        $data['timestamp'] = TIMESTAMP;
        $sign = $this->_get_wx_pay_sign3($data);
        $data['sign'] = $sign;
        return $data;
    }

    /**
     * 获取微信支付签名
     * @param $param array 支付接口参数
     * @return string sign
     * */
    private function _get_wx_pay_sign3($param) {
        ksort($param);
        foreach ($param as $key => $val) {
            $string .= $key . '=' . $val . '&';
        }
        $string .= 'key=' . $this->payment_config['wxpay_partnerkey'];
        return strtoupper(md5($string));
    }

    /**
     * 取得支付宝APP支付 订单信息 保证金订单 pay_sn就是order_sn
     */
    public function alipay_native_pay_marginOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        $pay_info = $this->_get_margin_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $pay_info['order_type'] = 'm';
        $pay_info['subject'] = '保证金订单：'.$pay_sn;
        $pay_info['pay_sn'] = $pay_sn;
        $payment_api = new $this->payment_code();
        $payment_api->init($this->payment_config,$pay_info);
        $prestr = 'partner="'.$payment_api->param['partner']
            .'"&seller_id="'.$payment_api->param['seller_id']
            .'"&out_trade_no="'.$payment_api->param['out_trade_no']
            .'"&subject="'.$payment_api->param['subject'].'"&body="m"&total_fee="'
            .$payment_api->param['total_fee'].'"&notify_url="'
            .$payment_api->param['notify_url'].'"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1"';
        $mysign = $payment_api->mySign($prestr);
        output_data(array('signStr'=>$prestr.'&sign_type="RSA"&sign="'.urlencode($mysign).'"'));
    }

    /**
     * 取得支付宝APP支付 订单信息 拍卖订单 pay_sn就是auction_order_sn
     */
    public function alipay_native_pay_articleOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        $pay_info = $this->_get_auction_order_info($pay_sn);
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }

        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $pay_info['order_type'] = 'a';
        $pay_info['subject'] = '拍卖订单：'.$pay_sn;
        $pay_info['pay_sn'] = $pay_sn;
        $payment_api = new $this->payment_code();
        $payment_api->init($this->payment_config,$pay_info);
        $prestr = 'partner="'.$payment_api->param['partner']
            .'"&seller_id="'.$payment_api->param['seller_id']
            .'"&out_trade_no="'.$payment_api->param['out_trade_no']
            .'"&subject="'.$payment_api->param['subject'].'"&body="a"&total_fee="'
            .$payment_api->param['total_fee'].'"&notify_url="'
            .$payment_api->param['notify_url'].'"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1"';
        $mysign = $payment_api->mySign($prestr);
        output_data(array('signStr'=>$prestr.'&sign_type="RSA"&sign="'.urlencode($mysign).'"'));
    }

    public function alipay_native_pay_newOp() {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d+$/',$pay_sn)){
            output_error('支付单号错误');
        }
        $rcb_pd_pay = $_POST;
        $rcb_pd_pay['is_app'] = 1;
        $pay_info = $this->_get_auction_order_info($pay_sn,$rcb_pd_pay);
//        $param = array();
//        $param['pay_sn'] = $pay_sn;
//        $param['subject'] = '拍卖订单：'.$pay_sn;
//        $param['amount'] = $pay_info['api_pay_amount'] * 100;
        if(isset($pay_info['error'])) {
            output_error($pay_info['error']);
        }
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'AopClient.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
        $aop = new AopClient;
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        $aop->appId = 2017063007608325;
        $aop->rsaPrivateKey = 'MIICXAIBAAKBgQCuVM6xnG1mmjBh8k5weX7vP4NUwZCvGHO16P2HsETBChXvQwvPXe3aQYitNL7kU0S9X7VBV+3LxPF6jqJ2NyPE6JlTLafEeQddL3vznXlGv4ijgxiG78E9mOO6kleLxiEQtihQQhQHu3dRZrhPV0VUtjdU/f9FOszwkwhu2R5odQIDAQABAoGAFDSoMFXdKEM+0FtxVAABAmHuKui0iFbhMnhqyktX7LfqiuzOdZ6BbwahfNHcPkKYeQqay5QRb8jH5Fib4+/IKV11WCvC5kGM1bjqO8QpByd+RAulMnmEX7M/l/lb3KDJ6bEeZgM7F95gu2owNJXTqbyUJ0mBgawJ+SbMr3qSqhECQQDdkomUjfM0yZ5J0kYNYPLjG3e4aUTnIWh9TE47ae9awC9MoVCwU8O47yqQHqao0DoMw+qq/QvR0Zo3T4AJ1Ej3AkEAyWsqFUpHv1nx7ig9UnYKxxQ2pZIRaVIB4G9G1LWZTAR0HaCkHV/oj4VCpSN5S6KMkPyneJfjfEVxBjE9SviK8wJABho8GdBTC3gmGOhmr4WlCuY9xOF5WVhNNW49lVtUkU5LvzOOMl0MPfKwXGnLs0iQ4Lsgonb3tV6tfap930dufwJBALNsLxTAEqG2cfkBB39Jf9hPfU6Ii9ISJ3HSLnqVOnWpEfbCfu9b3ELdJr0MmKRzrFwLdPPL+e1dvo0Rl9QNC1kCQDF8LVsaC3Tjs+xtGh9a3k1lkXiVmLYflgt0350W/hhy7I62SpBhBQgPJk2ajAM9r5ELvXwnfamw546VtqLdSis=';
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA";
        $aop->alipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB';
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        require(BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'request'.DS.'AlipayTradeAppPayRequest.php');
        $request = new AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $tmp = array(
            'body'=>'r',
            'timeout_express'=>'30m',
            'subject'=>'拍卖订单：'.$pay_sn,
            'out_trade_no'=>$pay_sn,
            'total_amount'=>$pay_info['api_pay_amount'],
            'product_code'=>'QUICK_MSECURITY_PAY'
        );
        $bizcontent = json_encode($tmp);
//        $bizcontent = "{\"body\":\"r\","
//                        . "\"subject\": \"{$pay_info['subject']}\","
//                        . "\"out_trade_no\": \"{$pay_info['pay_sn']}\","
//                        . "\"timeout_express\": \"30m\","
//                        . "\"total_amount\": \"{$pay_info['api_pay_amount']}\","
//                        . "\"product_code\":\"QUICK_MSECURITY_PAY\""
//                        . "}";
        $request->setNotifyUrl(MOBILE_SITE_URL.'/api/payment/alipay_aopclient/notify_url.php');
        $request->setBizContent($bizcontent);
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
//        echo htmlspecialchars($response);//就是orderString 可以直接给客户端请求，无需再做处理。
        output_data(array('signStr'=>$response,'code'=>$pay_info['api_pay_amount'] > 0?0:1000));
    }
}