<?php
/**
 * 我的订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_orderControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 订单列表
     * 拍卖订单不显示未支付的订单
     */
    public function order_listOp() {
        $model_order = Model('order');
        $condition = array();
        $condition['buyer_id|seller_id'] = $this->member_info['member_id'];
        $condition['order_type'] = array('in',array(1,3,4));

        if ($_POST['state_type'] != '') {
            $condition['order_state'] = str_replace(
                array('state_new','state_send','state_noeval'),
                array(ORDER_STATE_NEW,ORDER_STATE_SEND,ORDER_STATE_SUCCESS), $_POST['state_type']);
        }
        if ($_POST['state_type'] == 'state_new') {
            $condition['chain_code'] = 0;
        }
        if ($_POST['state_type'] == 'state_noeval') {
            $condition['evaluation_state'] = 0;
            $condition['order_state'] = ORDER_STATE_SUCCESS;
        }
        if ($_POST['state_type'] == 'state_notakes') {
            $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
            $condition['chain_code'] = array('gt',0);
        }
        if ($_REQUEST['state_type'] == 'state_deliver') {
            unset($condition['buyer_id|seller_id']);
            $condition['seller_id'] = $this->member_info['member_id'];
            $condition['order_state'] = ORDER_STATE_SEND;
            $condition['chain_code'] = 0;
        }

        if (preg_match('/^\d{10,20}$/',$_POST['order_key'])) {
            $condition['order_sn'] = $_POST['order_key'];
        } elseif ($_POST['order_key'] != '') {
            $condition['order_id'] = array('in',$this->_getOrderIdByKeyword($_POST['order_key'] , $this->member_info['member_id']));
        }
        //$condition['auction_id'] = 0;
        //$condition['__RAW'] = " if (`order_type`=4, `order_state` <> " . ORDER_STATE_NEW . ",1=1) ";
        $order_list_array = $model_order->getNormalOrderList($condition, $this->page, '*', 'order_id desc','', array('order_goods'));
        $page_count = $model_order->gettotalpage();
        $model_refund_return = Model('refund_return');
//        print_R($order_list_array);exit;
        $order_list_array = $model_refund_return->getGoodsRefundList($order_list_array,1);//订单商品的退款退货显示
        $ownShopIds = Model('store')->getOwnShopIds();

        $order_group_list = array();
        $order_pay_sn_array = array();
        foreach ($order_list_array as $value) {
            $value['zengpin_list'] = array();
            //显示取消订单
            $value['if_cancel'] = $model_order->getOrderOperateState('buyer_cancel',$value);
            //显示退款取消订单
            $value['if_refund_cancel'] = $model_order->getOrderOperateState('refund_cancel',$value);
            //显示收货
            $value['if_receive'] = $model_order->getOrderOperateState('receive',$value);
            //显示锁定中
            $value['if_lock'] = $model_order->getOrderOperateState('lock',$value);
            //显示物流跟踪
            $value['if_deliver'] = $model_order->getOrderOperateState('deliver',$value);
            //显示评价
            $value['if_evaluation'] = $model_order->getOrderOperateState('evaluation',$value);
            //显示追加评价
            $value['if_evaluation_again'] = $model_order->getOrderOperateState('evaluation_again',$value);
            //显示删除订单(放入回收站)
            $value['if_delete'] = $model_order->getOrderOperateState('delete',$value);

            $value['ownshop'] = in_array($value['store_id'], $ownShopIds);
            //拍卖订单 从保证金里取商品信息
            if(!empty($value['auction_id']) && empty($value['extend_order_goods'])){
                $value['extend_order_goods'] = [];
                $marginOrderInfo = Model("margin_orders")->getOrderInfo([
                    'auction_order_id'=>$value['order_id']
                    ]);
                if($marginOrderInfo){
                    //myDump([$value['auction_image'], cthumb($marginOrderInfo['auction_image'], 360)]);
                    $value['extend_order_goods'][] = [
                        'goods_image_url' => cthumb($marginOrderInfo['auction_image'], 360),
                        'refund' => $marginOrderInfo['refund_state']== margin_ordersModel::REFUND_STATE_REFUNDED ? true : false,
                        'goods_name' => $marginOrderInfo['auction_name'],
                        'goods_num' => 1,
                        'goods_price' => $value['goods_amount']
                    ];
                    $value['store_name'] = getMainConfig('shop.auctionShopName', "拍卖");
                }
                $value['store_url']=AUCTION_URL;
            }else{
                if($value['artist_id']>0){
                    //如果有艺术家id 跳转到艺术家页面
                    $value['store_url'] = YSJ_URL . '/ynh/getUserIdByArtistId?artistId=' . $value["artist_id"] . '&api_token=' . $_REQUEST['key'];
                }
                foreach ($value['extend_order_goods'] as $k => $goods_info) {
                    $value['extend_order_goods'][$k]['goods_image_url'] = cthumb($goods_info['goods_image'], 240, $value['store_id']);
                    $value['extend_order_goods'][$k]['refund'] = $value['extend_order_goods'][$k]['refund'] ? true : false;
                    unset($value['extend_order_goods'][$k]['rec_id']);
                    unset($value['extend_order_goods'][$k]['order_id']);
                    unset($value['extend_order_goods'][$k]['goods_pay_price']);
                    unset($value['extend_order_goods'][$k]['store_id']);
                    unset($value['extend_order_goods'][$k]['buyer_id']);
                    unset($value['extend_order_goods'][$k]['promotions_id']);
                    unset($value['extend_order_goods'][$k]['commis_rate']);
                    unset($value['extend_order_goods'][$k]['gc_id']);
                    unset($value['extend_order_goods'][$k]['goods_contractid']);
                    unset($value['extend_order_goods'][$k]['goods_image']);
                    if ($value['extend_order_goods'][$k]['goods_type'] == 5) {
                        $value['zengpin_list'][] = $value['extend_order_goods'][$k];
                        unset($value['extend_order_goods'][$k]);
                    }
                }
            }
            if (!empty($value['extend_order_goods'])) {
                $value['extend_order_goods'] = array_values($value['extend_order_goods']);
            }
/*            if ($value['order_type'] == 4) {//拼团订单
                $model_pintuan = Model('p_pintuan');
                $_info = $model_pintuan->getOrderInfo(array('order_id'=> $value['order_id']));
                $value['pintuan_info'] = $_info;
            }*/

            $order_group_list[$value['pay_sn']]['order_list'][] = $value;

            //如果有在线支付且未付款的订单则显示合并付款链接
            if ($value['order_state'] == ORDER_STATE_NEW) {
                $order_group_list[$value['pay_sn']]['pay_amount'] += $value['order_amount'] - $value['rcb_amount'] - $value['pd_amount'] - $value['points_amount'];
                if ($value['order_type'] == 4 && $value['pintuan_info']['end_time'] < TIMESTAMP) $order_group_list[$value['pay_sn']]['pay_amount'] = 0;//拼团订单过期后不能继续支付
            }
            $order_group_list[$value['pay_sn']]['add_time'] = $value['add_time'];

            //记录一下pay_sn，后面需要查询支付单表
            $order_pay_sn_array[] = $value['pay_sn'];
        }
        $new_order_group_list = array();
        foreach ($order_group_list as $key => $value) {
            $value['pay_sn'] = strval($key);
            $new_order_group_list[] = $value;
        }


        output_data(array('order_group_list' => $new_order_group_list), mobile_page($page_count));
    }

    private function _getOrderIdByKeyword($keyword,$buyer_id) {
        $goods_list = Model('order')->getOrderGoodsList(array('buyer_id'=>$buyer_id,'goods_name'=>array('like','%'.$keyword.'%')),'order_id',100,null,'', null,'order_id');
        return array_keys($goods_list);
    }

    /**
     * 取消订单
     */
    public function order_cancelOp() {
        $model_order = Model('order');
        $logic_order = Logic('order');
        $order_id = intval($_POST['order_id']);

        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['order_type'] = array('in',array(1,3,4));
        $order_info = $model_order->getOrderInfo($condition);
        $if_allow = $model_order->getOrderOperateState('buyer_cancel',$order_info);
        if (!$if_allow) {
            output_error('无权操作');
        }
        if (TIMESTAMP - ORDER_CANCEL_TIMES < $order_info['api_pay_time'] && $order_info['payment_code'] != "underline") {
            $_hour = ceil(($order_info['api_pay_time']+ORDER_CANCEL_TIMES-TIMESTAMP)/3600);
            output_error('该订单曾尝试使用第三方支付平台支付，须在'.$_hour.'小时以后才可取消');
        }
        $result = $logic_order->changeOrderStateCancel($order_info,'buyer', $this->member_info['member_name'], '其它原因');
        if(!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data('1');
        }
    }

    /**
     * 取消订单
     */
    public function order_deleteOp() {
        $model_order = Model('order');
        $logic_order = Logic('order');
        $order_id = intval($_POST['order_id']);
    
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['order_type'] = array('in',array(1,3,4));
        $order_info = $model_order->getOrderInfo($condition);
        $if_allow = $model_order->getOrderOperateState('delete',$order_info);
        if (!$if_allow) {
            output_error('无权操作');
        }

        $result = $logic_order->changeOrderStateRecycle($order_info,'buyer','delete');
        if(!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data('1');
        }
    }

    /**
     * 订单确认收货
     */
    public function order_receiveOp() {
        $model_order = Model('order');
        $logic_order = Logic('order');
        $order_id = intval($_POST['order_id']);

        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['order_type'] = ['in','1,4'];
        $order_info = $model_order->getOrderInfo($condition);
        $if_allow = $model_order->getOrderOperateState('receive',$order_info);
        if (!$if_allow) {
            output_error('无权操作');
        }

        $result = $logic_order->changeOrderStateReceive($order_info,'buyer', $this->member_info['member_name'],'签收了货物');
        if(!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data('1');
        }
    }

    /**
     * 物流跟踪
     */
    public function search_deliverOp(){
        $order_id   = intval($_POST['order_id']);
        if ($order_id <= 0) {
            output_error('订单不存在');
        }

        $model_order    = Model('order');
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $order_info = $model_order->getOrderInfo($condition,array('order_common','order_goods'));
        if (empty($order_info) || !in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS))) {
            output_error('订单不存在');
        }

        $express = rkcache('express',true);
        $e_code = $express[$order_info['extend_order_common']['shipping_express_id']]['e_code'];
        $e_name = $express[$order_info['extend_order_common']['shipping_express_id']]['e_name'];
        $deliver_info = $this->_get_express($e_code, $order_info['shipping_code']);
        output_data(array('express_name' => $e_name, 'shipping_code' => $order_info['shipping_code'], 'deliver_info' => $deliver_info));
    }


    /**
     * 物流跟踪
     */
    public function getExpressInfoOp(){
        $order_id   = intval($_POST['order_id']);
        $buyer_id   = $this->member_info['member_id'];
        if ($order_id <= 0) {
            output_error('订单不存在');
        }

        $model_order    = Model('order');
        $condition['order_id'] = $order_id;
        // $condition['buyer_id'] = $buyer_id;
        $order_info = $model_order->getOrderInfo($condition,array('order_common','order_goods'));
        if (empty($order_info)) {
            output_error('该订单不存在 !');
        }

        if (!in_array($order_info['order_state'], array(ORDER_STATE_SEND, ORDER_STATE_SUCCESS))) {
            output_error('该订单暂无物流信息 !');
        }
        $express = rkcache('express',true);
        $orderCommon = $order_info['extend_order_common'];
        $data['shipment'] = [];
        $shipment = [
            [
                'desc' => '已下单',
                'type_no' => '',
                'time' => ''
            ],
            [
                'desc' => '您提交了订单, 请等待商家系统确认',
                'type_no' => '',
                'time' => date('Y-m-d H:i:s', $order_info['add_time'])
            ]
        ];
        //TODO 这里都是兼容中转仓状态的
        //发往中转仓 0.初始状态 1.已发出 2.已收到 3.异常
        switch ($orderCommon['transit_shipment_status'])
        {
                case '0':
                    if (!empty($order_info['shipping_code'])) {
                        array_unshift($shipment,
                            [
                                'desc' => '物流信息: ' . $express[$orderCommon['shipping_express_id']]['e_name'],
                                'type_no' => $order_info['shipping_code'],
                                'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                            ],
                            [
                                'desc' => '商家已发货',
                                'type_no' => '',
                                'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                            ]
                        );
                    } else {
                        array_unshift($shipment, [
                            'desc' => '商家正在发货',
                            'type_no' => '',
                            'time' => date('Y-m-d H:i:s', $order_info['add_time'])
                        ]);
                        if (isset($orderCommon['transit_shipment_express_number'])) {
                            $transit_shipment_no = $orderCommon['transit_shipment_express_number'];
                            if (!empty($transit_shipment_no)) {
                                array_unshift($shipment, [
                                    'desc' => '商家已发货至上海中转仓',
                                    'type_no' => '',
                                    'time' => ''
                                ],
                                [
                                    'desc' => '物流信息: ' . $orderCommon['transit_shipment_express_company'],
                                    'type_no' => $transit_shipment_no,
                                    'time' => $orderCommon['transit_shipment_delivery_time']
                                ]);
                            }
                        }
                    }
                break;
            //已发货到中转仓
            case '1':
                array_unshift($shipment,
                    [
                        'desc' => '商家已发货至上海中转仓',
                        'type_no' => '',
                        'time' => ''
                    ],
                    [
                        'desc' => '物流信息: ' . $orderCommon['transit_shipment_express_company'],
                        'type_no' => $orderCommon['transit_shipment_express_number'],
                        'time' => $orderCommon['transit_shipment_delivery_time']
                    ],
                    [
                        'desc' => '商家正在发货',
                        'type_no' => '',
                        'time' => date('Y-m-d H:i:s', $order_info['add_time'])
                    ]
                );
                //中转仓是否发货
                if (!empty($orderCommon['shipping_time'])) {
                    array_unshift($shipment,
                        [
                            'desc' => '物流信息: ' . $express[$orderCommon['shipping_express_id']]['e_name'],
                            'type_no' => $order_info['shipping_code'],
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ],
                        [
                            'desc' => '上海中转仓已发货',
                            'type_no' => '',
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ],
                        [
                            'desc' => '上海中转仓已收货',
                            'type_no' => '',
                            'time' => $orderCommon['transit_shipment_receiving_time']
                        ]);
                }
                break;
            case '2':
                array_unshift($shipment,
                    [
                        'desc' => '上海中转仓已收货',
                        'type_no' => '',
                        'time' => $orderCommon['transit_shipment_receiving_time']
                    ],
                    [
                        'desc' => '商家已发货至上海中转仓',
                        'type_no' => '',
                        'time' => ''
                    ],
                    [
                        'desc' => '物流信息: ' . $orderCommon['transit_shipment_express_company'],
                        'type_no' => $orderCommon['transit_shipment_express_number'],
                        'time' => $orderCommon['transit_shipment_delivery_time']
                    ],
                    [
                        'desc' => '商家正在发货',
                        'type_no' => '',
                        'time' => $orderCommon['transit_shipment_delivery_time']
                    ]
                );
                //中转仓是否发货
                if (!empty($orderCommon['shipping_time'])) {
                    array_unshift($shipment,
                        [
                            'desc' => '物流信息: ' . $express[$orderCommon['shipping_express_id']]['e_name'],
                            'type_no' => $order_info['shipping_code'],
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ],
                        [
                            'desc' => '上海中转仓已发货',
                            'type_no' => '',
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ]);
                }
                break;
            default:
                if (!empty($order_info['shipping_code'])) {
                    array_unshift($shipment,
                        [
                            'desc' => '物流信息: ' . $express[$orderCommon['shipping_express_id']]['e_name'],
                            'type_no' => $order_info['shipping_code'],
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ],
                        [
                            'desc' => '商家已发货',
                            'type_no' => '',
                            'time' => date('Y-m-d H:i:s', $orderCommon['shipping_time'])
                        ]
                    );
                }
                break;
        }
        output_data($shipment);
    }

    /**
     * 取得当前的物流最新信息
     */
    public function get_current_deliverOp(){
        $order_id   = intval($_POST['order_id']);
        if ($order_id <= 0) {
            output_error('订单不存在');
        }
    
        $model_order    = Model('order');
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $order_info = $model_order->getOrderInfo($condition,array('order_common','order_goods'));
        if (empty($order_info) || !in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS))) {
            output_error('订单不存在');
        }
    
        $express = rkcache('express',true);
        $e_code = $express[$order_info['extend_order_common']['shipping_express_id']]['e_code'];
        $e_name = $express[$order_info['extend_order_common']['shipping_express_id']]['e_name'];
        $content = Model('express')->get_express($e_code, $order_info['shipping_code']);
        if (empty($content)) {
            output_error('物流信息查询失败');
        } else {
            foreach ($content as $k=>$v) {
                if ($v['time'] == '') continue;
                output_data(array('deliver_info'=>$content[0]));
            }
            output_error('物流信息查询失败');
        }
    }

    /**
     * 从第三方取快递信息
     *
     */
    public function _get_express($e_code, $shipping_code){

        $content = Model('express')->get_express($e_code, $shipping_code);
        if (empty($content)) {
            output_error('物流信息查询失败');
        }
        $output = array();
        foreach ($content as $k=>$v) {
            if ($v['time'] == '') continue;
            $output[]= $v['time'].'&nbsp;&nbsp;'.$v['context'];
        }

        return $output;
    }

    public function order_infoOp() {
        /** @var orderLogic $logic_order */
        $logic_order = logic('order');
        $orderId = $_GET['order_id'];
        $memberId = $this->member_info['member_id'];
        if (empty($memberId)) {
            output_error('请登录 !');
        }
        $result = $logic_order->getMemberOrderInfo($orderId, 'unset');
        if (!$result['state']) {
            output_error($result['msg']);
        }
        $condition = [];
        $condition = ['member_id' => $memberId];
        $condition['artist_id'] = ['gt', '0'];
        $store_id = Model('store')->getStoreDetail($condition, '*', 'store_id');
        $type = 0;
        if ($store_id && $store_id == $result['data']['order_info']['store_id']) {
            //商家查看订单
            $type = 1;
        } elseif ($memberId != $result['data']['order_info']['buyer_id']) {
            output_error('订单查询出错 !');
        }
        $data = array();
        $orderCommon = $result['data']['order_info']['extend_order_common'];
        $data['order_id'] = $result['data']['order_info']['order_id'];
        $data['order_sn'] = $result['data']['order_info']['order_sn'];
        $data['is_points'] = $result['data']['order_info']['is_points'];
        $data['points_amount'] = $result['data']['order_info']['points_amount'];
        $data['rpt_amount'] = $result['data']['order_info']['rpt_amount'];
        $data['store_id'] = $result['data']['order_info']['store_id'];
        $data['store_name'] = $result['data']['order_info']['store_name'];
        if($result['data']['order_info']['order_type'] == \App\Models\ShopncOrder::ORDER_TYPE_AUCTION ){
            $data['store_url']=AUCTION_URL;
            $data['store_name'] = getMainConfig('shop.auctionShopName', "拍卖");
        }
        $data['add_time'] = date('Y-m-d H:i:s',$result['data']['order_info']['add_time']);
        $data['payment_time'] = $result['data']['order_info']['payment_time'] ? date('Y-m-d H:i:s',$result['data']['order_info']['payment_time']) : '';
        $data['shipping_time'] = $orderCommon['shipping_time'] ? date('Y-m-d H:i:s',$result['data']['order_info']['extend_order_common']['shipping_time']) : '';
        $data['finnshed_time'] = $result['data']['order_info']['finnshed_time'] ? date('Y-m-d H:i:s',$result['data']['order_info']['finnshed_time']): '';
        $data['order_amount'] = ncPriceFormat($result['data']['order_info']['order_amount']);
        $data['shipping_fee'] = ncPriceFormat($result['data']['order_info']['shipping_fee']);
        $data['real_pay_amount'] = ncPriceFormat($result['data']['order_info']['order_amount']);
//         $data['evaluation_state'] = $result['data']['order_info']['evaluation_state'];
//         $data['evaluation_again_state'] = $result['data']['order_info']['evaluation_again_state'];
//         $data['refund_state'] = $result['data']['order_info']['refund_state'];
        $data['order_state'] = $result['data']['order_info']['order_state'];
        $data['state_desc'] = $result['data']['order_info']['state_desc'];
        $data['payment_name'] = $result['data']['order_info']['payment_name'];
        $data['payment_code'] = $result['data']['order_info']['payment_code'];
        if($result['data']['order_info']['artist_id']>0) {
            $data['store_url'] = YSJ_URL . '/ynh/getUserIdByArtistId?artistId=' . $result['data']['order_info']['artist_id'] . '&api_token=' . $_REQUEST['key'];
        }
        $data['pay_voucher'] = UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/pay_voucher/'.$result['data']['order_info']['pay_voucher'];
        $data['order_message'] = $orderCommon['order_message'];
        $data['reciver_phone'] = !empty($result['data']['order_info']['extend_order_common']['reciver_info']['phone']) ? $result['data']['order_info']['extend_order_common']['reciver_info']['phone'] :  $result['data']['order_info']['buyer_phone'];
        $data['reciver_name'] = $orderCommon['reciver_name'];
        $data['reciver_addr'] = $orderCommon['reciver_info']['address'];
        $shipment = [
            'desc' => '您提交了订单, 请等待商家系统确认',
            'time' => $data['add_time']
        ];
        //物流信息以及状态显示
        //订单处于已发货状态或者已收货展示最新物流信息
        if (in_array($data['order_state'], array(ORDER_STATE_SEND, ORDER_STATE_SUCCESS))) {
            //发往中转仓 0.初始状态 1.已发出 2.已收到 3.异常
            switch ($orderCommon['transit_shipment_status']) {
                case '0':
                    $shipment = [
                        'desc' => '已下单',
                        'type_no' => '',
                        'time' => $data['add_time']
                    ];
                    if (isset($orderCommon['transit_shipment_express_number'])) {
                        $transit_shipment_no = $orderCommon['transit_shipment_express_number'];
                        if (!empty($transit_shipment_no)) {
                            $shipment = [
                                'desc' => '商家已发货至上海中转仓',
                                'type_no' => $transit_shipment_no,
                                'time' => $orderCommon['transit_shipment_delivery_time']
                            ];
                        }
                    }
                    break;
                //已发货到中转仓
                case '1':
                    $shipment = [
                        'desc' => '商家正在发货',
                        'type_no' => '',
                        'time' => $orderCommon['transit_shipment_delivery_time']
                    ];
                    break;
                case '2':
                    $shipment = [
                        'desc' => '上海中转仓已收货',
                        'type_no' => '',
                        'time' => $orderCommon['transit_shipment_receiving_time'] ?: $orderCommon['shipping_time']
                    ];
                    //中转仓是否发货
                    if (!empty($orderCommon['shipping_time'])) {
                        $shipment = [
                            'desc' => '上海中转仓已发货',
                            'type_no' => $data['shipping_code'],
                            'time' => $orderCommon['shipping_time']
                        ];
                    }
                    break;
                case '3':
                    $shipment = [
                        'desc' => '物流信息异常',
                        'type_no' => '',
                        'time' => ''
                    ];
                    break;
                default:
                    if (!empty($data['shipping_code'])) {
                        $shipment = [
                            'desc' => '商家已发货',
                            'type_no' => $data['shipping_code'],
                            'time' => $orderCommon['shipping_time']
                        ];
                    } else {
                        $shipment = [
                            'desc' => '您提交了订单, 请等待商家系统确认',
                            'type_no' => '',
                            'time' => $data['add_time']
                        ];
                    }
                    break;
            }
        }
        $data['shipment'] = $shipment;
        $data['store_member_id'] = $result['data']['order_info']['extend_store']['member_id'];
        $data['store_phone'] = $result['data']['order_info']['extend_store']['store_phone'];
        $data['order_tips'] = $result['data']['order_info']['order_state'] == ORDER_STATE_NEW ? '请于'.ORDER_AUTO_CANCEL_TIME.'小时内完成付款，逾期未付订单自动关闭' : '';
        $_tmp = $result['data']['order_info']['extend_order_common']['invoice_info'];
        $_invonce = '';
        if (is_array($_tmp) && count($_tmp) > 0) {
            foreach ($_tmp as $_k => $_v) {
                $_invonce .= $_k.'：'.$_v.' ';
            }
        }
        $_tmp = $result['data']['order_info']['extend_order_common']['promotion_info'];
        $data['promotion'] = array();
        if(!empty($_tmp)){
            $pinfo = unserialize($_tmp);
            if (is_array($pinfo) && $pinfo){
                foreach ($pinfo as $pk => $pv){
                    if (!is_array($pv) || !is_string($pv[1]) || is_array($pv[1])) {
                        $pinfo = array();
                        break;
                    }
                    $pinfo[$pk][1] = strip_tags($pv[1]);
                }
                $data['promotion'] = $pinfo;
            }
        }
        
        $data['invoice'] = rtrim($_invonce);
        $data['if_deliver'] = $result['data']['order_info']['if_deliver'];
        $data['if_buyer_cancel'] = $result['data']['order_info']['if_buyer_cancel'];
        $data['if_refund_cancel'] = $result['data']['order_info']['if_refund_cancel'];
        $data['if_receive'] = $result['data']['order_info']['if_receive'];
        $data['if_evaluation'] = $result['data']['order_info']['if_evaluation'];
        $data['if_lock'] = $result['data']['order_info']['if_lock'];
        //当前用户是否为卖家, 卖家待发货可进行发货
        $data['if_seller'] = $type;
        $order_type = $result['data']['order_info']['order_type'];
        $data['order_type'] = $order_type;
        if ($order_type == 4) {//拼团订单
            $model_pintuan = Model('p_pintuan');
            $_info = $model_pintuan->getOrderInfo(array('order_id'=> $data['order_id']));
            $data['pintuan_info'] = $_info;
        }
        $data['goods_list'] = array();
        foreach ($result['data']['order_info']['goods_list'] as $_k => $_v) {
            $data['goods_list'][$_k]['rec_id'] = $_v['rec_id'];
            $data['goods_list'][$_k]['goods_id'] = $_v['goods_id'];
            $data['goods_list'][$_k]['goods_name'] = $_v['goods_name'];
            $data['goods_list'][$_k]['goods_price'] = ncPriceFormat($_v['goods_price']);
            $data['goods_list'][$_k]['goods_num'] = $_v['goods_num'];
            $data['goods_list'][$_k]['goods_spec'] = $_v['goods_spec'];
            $data['goods_list'][$_k]['image_url'] = $_v['image_240_url'];
            $data['goods_list'][$_k]['refund'] = $_v['refund'];
            $data['goods_list'][$_k]['is_special'] = $_v['is_special'];

            if($_v['artwork_id']>0){
                //如果有艺术家id 跳转到艺术家页面
                $data['goods_list'][$_k]['goods_url'] = WAP_YSJ_URL . '/workDetails?id=' . $_v['artwork_id'] . '&api_token=' . $_REQUEST['key'];
            }
        }
        $data['zengpin_list'] = array();
        foreach ($result['data']['order_info']['zengpin_list'] as $_k => $_v) {
            $data['zengpin_list'][$_k]['goods_name'] = $_v['goods_name'];
            $data['zengpin_list'][$_k]['goods_num'] = $_v['goods_num'];
        }

        $ownShopIds = Model('store')->getOwnShopIds();
        $data['ownshop'] = in_array($data['store_id'], $ownShopIds);

        output_data(array('order_info'=>$data));
    }

}
