<?php
/**
 * 前台登录 退出操作
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class loginControl extends mobileHomeControl {

    protected $openid;
    protected $unionid;
    protected $error = '';

    public function __construct(){
        parent::__construct();
    }

    /**
     * 登录
     */
    public function indexOp(){
        $request = $_REQUEST;
        $type = $request['type'];
        if ($type == 'mp') {
            //小程序登录
            $result = $this->mp_login($request);
        } else {
            $result = $this->passwordLogin($request);
        }
        if (isset($result['error'])) {
            output_error($result['error']);
        }
        output_data($result);
    }


    /**
     * 默认账号密码登录
     * @param $request
     * @return array
     * @Date: 2019/4/29 0029
     * @author: Mr.Liu
     */
    public function passwordLogin($request)
    {
        if(empty($request['username']) || empty($request['password']) || !in_array($request['client'], $this->client_type_array)) {
            return ['error' => '登录失败'];
        }

        $model_member = Model('member');
        $login_info = array();
        $login_info['user_name'] = $request['username'];
        $login_info['password'] = $request['password'];
        $member_info = $model_member->login($login_info);
        if(isset($member_info['error'])) {
            return ['error' => $member_info['error']];
        } else {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $request['client']);
            if($token) {
                $this->bindWeiXin($member_info['member_id'],cookie('weixin_info'));
                return array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token, 'usertype'=>$member_info['member_type']);
            } else {
                return ['error' => '登录失败'];
            }
        }
    }

    public function mp_login($request)
    {
        if (empty($request['iv']) || empty($request['code']) || empty($request['encryptedData'])) {
            return ['error' => '缺少必要参数 !'];
        }
        //判断用户是否需要登录  1不需要 0需要
        $status = 0;
        //获取微信小程序授权用户信息
        $wxUserInfo = $this->getWxUserInfo($request);
        //判断用户是否第一次登陆
        $model_member = Model('member');
        if (empty($this->unionid)) {
            return ['error' => 'unionid获取失败 !'];
        }
        $login_info['weixin_unionid'] = $this->unionid;
        $member_info = $model_member->where($login_info)->find();
        if (empty($member_info)) {
            $ucenter = $model_member->loginUCenter($login_info);
            if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
                $data = $ucenter->data;
                $condition = ['user_name' => $data->name];
                $member_info = $model_member->login($condition);
                if (empty($member_info)) {
                    $register_info = $this->_addUser($data);
                    $register_info['dis_code'] = trim($request['dis_code']);
                    $register_info['registered_source'] = isset($request['client']) ? $request['client'] : '';
                    $register_info['source_staff_id'] = isset($request['source_staff_id']) ? $request['source_staff_id'] : 0;
                    $member_info = $model_member->register($register_info);
                    if(!isset($member_info['error'])) {
                        $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
                        if($token) {
                            return [
                                'username' => $member_info['member_name'],
                                'userid' => $member_info['member_id'],
                                'key' => $token,
                                'status' => $status
                            ];
                        } else {
                            return ['error' => '注册失败 !'];
                        }
                    } else {
                        return ['error' => $member_info['error']];
                    }
                } else {
                    $update = array();
                    $update['user_areainfo'] = $data->areainfo;
                    $update['weixin_unionid'] = $data->weixin_unionid;
                    $update['weixin_open_id'] = $data->weixin_open_id;
                    $result = $model_member->where($condition)->update($update);
                }
                if ($result) {
                    $member_info = $model_member->where($login_info)->find(); // 验证登录信息
                }
                $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
                if ($token) {
                    $status = 1;
                    return [
                        'username' => $member_info['member_name'],
                        'userid' => $member_info['member_id'],
                        'key' => $token,
                        'status' => $status
                    ];
                } else {
                    return ['error' => '注册失败 !'];
                }
            } else {
                $wxUserInfo['status'] = $status;
                return $wxUserInfo;
            }
        } else {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if ($token) {
                $status = 1;
                return [
                    'username' => $member_info['member_name'],
                    'userid' => $member_info['member_id'],
                    'key' => $token,
                    'status' => $status
                ];
            } else {
                return ['error' => '注册失败 !'];
            }
        }
    }

    // 同步账号信息
    private function _addUser($ucenter_info)
    {
        //添加账号
        $datetime = date('Y-m-d H:i:s', time());
        $member = array();
        $member['user_key'] = $ucenter_info->key;
        $member['user_name'] = $ucenter_info->name;
        $member['user_passwd'] = $ucenter_info->password;
        $member['user_mobile'] = $ucenter_info->mobile;
        $member['user_mobile_bind'] = empty($ucenter_info->mobile) ? 0 : 1;
        $member['user_avatar'] = $ucenter_info->avatar;
        $member['user_nickname'] = $ucenter_info->nickname;
        $member['user_areaid'] = isset($ucenter_info->areaid) ? $ucenter_info->areaid : '';
        $member['user_cityid'] = isset($ucenter_info->cityid) ? $ucenter_info->cityid : '';
        $member['user_provinceid'] = isset($ucenter_info->provinceid) ? $ucenter_info->provinceid : '';
        $member['user_areainfo'] = isset($ucenter_info->areainfo) ? $ucenter_info->areainfo : '';
        $member['weixin_unionid'] = $ucenter_info->weixin_unionid ?: '';
        $member['weixin_open_id'] = $ucenter_info->weixin_open_id ?: '';
        $member['weixin_info'] = $ucenter_info->weixin_info ?: '';
        $member['sina_unionid'] = $ucenter_info->sina_unionid ?: '';
        $member['sina_info'] = $ucenter_info->sina_info ?: '';
        $member['qq_unionid'] = $ucenter_info->qq_unionid ?: '';
        $member['qq_info'] = $ucenter_info->qq_info ?: '';
        $member['created_at'] = $datetime;
        $member['updated_at'] = $datetime;
        return $member;
    }

    public function getWxUserInfo($request)
    {
        $iv = $request->iv;
        $code = $request->code;
        $encryptedData = $request->encryptedData;
        $appId = C('APP_ID', '');
        $appSecret = C('APP_SECRET', '');
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=';
        $url .= $appId . '&secret=' . $appSecret . '&js_code=' . $code . '&grant_type=authorization_code';
        //获取session_key 用于解密数据
        $grantInfo = file_get_contents($url);
        $grantInfo = json_decode($grantInfo, true);
        if (empty($grantInfo['session_key'])) {
            $this->error = 200228;
            return false;
        }
        $sessionKey = $grantInfo['session_key'];
        $this->openid = $grantInfo['openid'];
        $this->unionid = $grantInfo['unionid'];
        /** @var memberLogic $mpLogic */
        $mpLogic = Logic('member');
        $errCode = $mpLogic->decryptData($appId, $appSecret, $encryptedData, $iv, $data);

        if ($errCode != 0) {
            //解密后的用户信息
            $this->error = $errCode;
            return false;
        }
        if (!is_array($data)) {
            $data = json_decode($data, true);
        }
        $data['session_key'] = $sessionKey;
        return $data;
    }

    /**
     * 登录生成token
     */
    private function _get_token($member_id, $member_name, $client) {
        $model_mb_user_token = Model('mb_user_token');

        //重新登录后以前的令牌失效
        //暂时停用
        //$condition = array();
        //$condition['member_id'] = $member_id;
        //$condition['client_type'] = $client;
        //$model_mb_user_token->delMbUserToken($condition);

        //生成新的token
        $mb_user_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_user_token_info['member_id'] = $member_id;
        $mb_user_token_info['member_name'] = $member_name;
        $mb_user_token_info['token'] = $token;
        $mb_user_token_info['login_time'] = TIMESTAMP;
        $mb_user_token_info['client_type'] = $client;

        $result = $model_mb_user_token->addMbUserToken($mb_user_token_info);

        if($result) {
            return $token;
        } else {
            return null;
        }

    }

    /**
     * 注册
     */
    public function registerOp(){
        $model_member   = Model('member');
//        if(trim($_POST['dis_code']) != ''){
//            $return = $this->check_dcode(trim($_POST['dis_code']));
//            if($return == false){
//                output_error('邀请码不存在');
//            }
//        }

        $register_info = array();
        $register_info['username'] = $_POST['username'];
        $register_info['password'] = $_POST['password'];
        //注册来源员工ID
        $register_info['source_staff_id'] = $_POST['source_staff_id'];
        $register_info['password_confirm'] = $_POST['password_confirm'];
//        $register_info['email'] = $_POST['email'];
        $register_info['dis_code'] = trim($_POST['dis_code']);
        $register_info['registered_source'] = isset($_POST['client']) ? $_POST['client'] : '';
        $register_info['source_staff_id'] = isset($_POST['source_staff_id']) ? $_POST['source_staff_id'] : 0;
        $member_info = $model_member->register($register_info);
        if(!isset($member_info['error'])) {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if($token) {
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token));
            } else {
                output_error('注册失败');
            }
        } else {
            output_error($member_info['error']);
        }

    }

    public function createLevelRelationOp()
    {
        $dis_code = $_POST['dis_code'];
        $memberId = (int)$this->getMemberIdIfExists();
        if (empty($memberId)) {
            output_error('请登录 !');
        }
        if (empty($dis_code)) {
            Logic('register')->getAward($memberId);
            output_data('success');
        } else {
            $memberModel = Model('member');
            //处理邀请码
            $member_decode = $memberModel->getUpperMember(array('dis_code' => $dis_code));
            $memberInfo = $memberModel->table('member')->where(['member_id' => $memberId])->find();
            $memberInfo['top_member'] = $memberInfo['upper_member_id'] = $member_decode['member_id'];
            $memberModel->invitationRegister($memberInfo);
            Logic('register')->getAward($memberId);
            output_data('success');
        }
    }


    /*
     * 提供app 使用
     */
    public function getInviteCodeByKeyOp()
    {
        $user_key = $_POST['user_key'];
        if (empty($user_key)) {
            output_data(0);
        }
        $memberInfo = Model('member')->where(['member_key' => $user_key])->find();
        if (empty($memberInfo)) {
            output_data(0);
        }
        $inviteCode = Logic('j_member')->getMemberInviteCode($memberInfo['member_id']);
        output_data($inviteCode);
    }


    /**
     * 获取手机短信验证码
     */
    public function get_sms_captchaOp(){

        $http_referer = $_SERVER['HTTP_REFERER'];
        $sign = $_GET['sign'];
        $ynys_check = md5($_GET['phone'].'ynys') == $sign?true:false;
        if((empty($http_referer) || !strpos($http_referer, 'yinuovip'))&&($sign == '' || ($sign != '' && !$ynys_check))){

            exit();
        }

        $phone = $_GET['phone'];
        $log_type = $_GET['type'];//短信类型:1为注册,2为登录,3为找回密码
        $state_data = array(
            'state' => false,
            'msg' => '验证码或手机号码不正确'
        );

        if (strlen($phone) == 11){
            $logic_connect_api = Logic('connect_api');
            $state_data = $logic_connect_api->sendCaptcha($phone, $log_type);
        }
        $this->connect_output_data($state_data);
    }
    /**
     * 验证手机验证码
     */
    public function check_sms_captchaOp(){
        $phone = $_GET['phone'];
        $log_type = $_GET['type'];
        $state_data = $this->checkSmsCaptcha($phone, $log_type);
        $this->connect_output_data($state_data, 1);
    }

    /**
     * 手机注册
     */
    public function sms_registerOp(){
        $phone = $_POST['phone'];
        $password = $_POST['password'];
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array(
                "input" => $phone,
                "require" => "true",
                "message" => "请填写用户名"
            ),

            array(
                "input" => $password,
                "require" => "true",
                "message" => "密码不能为空"
            ),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            $state['state'] = false;
            $state['msg'] = $error;
            $this->connect_output_data($state);
        }

        $dis_code = '';

        if(!empty($_COOKIE['invite_code']) && $_COOKIE['invite_code'] != 'null'){
            $invite_code = trim($_COOKIE['invite_code']);
            $state['state'] = false;
            $model_member = Model('member');
            $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>$invite_code));
            if(empty($check_member_dcode) /*|| in_array($check_member_dcode['member_type'],array(4))*/){
                $state['msg'] = '分享链接错误，无法注册绑定上级，请检查后再注册。';
                $invite_code = '';
                //$this->connect_output_data($state);
            }
            $dis_code = $invite_code;
        }
        $regArr = [
            'source'=>$_POST['source'],
            'captcha'=>$_POST['captcha'],
            'password'=>$password,
            'client'=>$_POST['client'],
            'member_areaid'=>$_POST['member_areaid'],
            'member_cityid'=>$_POST['member_cityid'],
            'member_provinceid'=>$_POST['member_provinceid'],
            'member_areainfo'=>$_POST['member_areainfo']
        ];
        if (isset($_POST['source_staff_id']) && !empty($_POST['source_staff_id'])) {
            $regArr['source_staff_id'] = $_POST['source_staff_id'];
        }
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsRegister($phone, $dis_code, $regArr);

        //绑定微信
        $this->bindWeiXin($state_data['userid'],cookie('weixin_info'));
        if ($_POST['ex_id'] && $invite_code){
            Logic('register')->exhibition_reg($state_data['userid'],$check_member_dcode['member_id'],$_POST['ex_id']);
        }

        $this->connect_output_data($state_data);
    }

    /**
     * 绑定微信
     * @param int $member_id
     * @param array $weixin_info
     * @return bool
     */
    private function bindWeiXin($member_id, $weixin_info){
        $user_info = unserialize($weixin_info);
        if (empty($user_info['unionid'])){
            return false;
        }
        $model_member = Model('member');
        $condition = ['member_id'=>$member_id];
        $member_info = $model_member->getMemberInfo($condition, $field = 'weixin_unionid');
        if (!$member_info['weixin_unionid'] || ($member_info['weixin_unionid'] == trim($user_info['unionid']))){
            $update_data = [
                'weixin_unionid'=>trim($user_info['unionid']),
                'member_sex' => $user_info['sex'],
                'member_avatar' => $user_info['headimgurl'],
                'weixin_open_id' => trim($user_info['openid']),
                'weixin_info' => serialize([
                    'nickname'=>$user_info['nickname'],
                    'openid'=>$user_info['openid'],
                ])
            ];
            if(empty($member_info['member_truename'])){
                $update_data['member_truename'] = $user_info['nickname'];
            }
            return $model_member->editMember(['member_id'=>$member_id],$update_data);
        }
        return false;
    }

    /**
     * 格式化输出数据
     */
    public function connect_output_data($state_data, $type = 0){
        if($state_data['state']){
            unset($state_data['state']);
            unset($state_data['msg']);
            if ($type == 1){
                $state_data = 1;
            }
            output_data($state_data);
        } else {
            output_error($state_data['msg']);
        }
    }


    /**
     * 邀请码检测
     *
     * @param
     * @return
     */
    private function check_dcode($dcode) {
        $model_member = Model('member');
        $check_member_dcode = $model_member->getUpperMember(array('dis_code'=>$dcode));
        if(is_array($check_member_dcode) && count($check_member_dcode)>0 || $dcode == '') {
            return true;
        } else {
            return false;
        }
    }
}
