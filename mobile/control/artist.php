<?php
/**
 * 艺术家
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class artistControl extends mobileHomeControl
{
	public function __construct()
	{
		parent::__construct();
	}

    public function index_artistOp(){
        $model_artist = Model('artist_vendue');
        $_P = $_GET['p']?$_GET['p']:0;
        $condition['store_apply_state'] = 40;
        $condition['is_rec'] = 1;
        $order = 'artist_sort ASC,artist_vendue_id DESC';
        $page_count = $model_artist->getArtist_vendueList([], 0, '', 'count(artist_vendue_id) as count', 0);
        $limit = $_P.",3";
        $_P += 2;
        $artist_list = array();
        $hasmore = true;
        while(count($artist_list)<3 && $page_count>$_P){
            $list = $model_artist->getArtist_StoreList($condition,0,$order,'*',$limit);
            $limit = ++$_P.',1';
            if(empty($list)){
                $hasmore = false;
                break;
            }
            $artist_list = array_merge($artist_list,$this->_list($list));
        };
        $page_count[0]['count'] <= $_P && $hasmore = false;
        output_data(array('artist_list' => array_values($artist_list),'p'=>$_P),array('hasmore'=>$hasmore));
    }

	public function artist_listOp(){
		$model_artist = Model('artist_vendue');
		if (!empty($_GET['keyword'])) {
			$condition['artist_name'] = array('like', '%' . $_GET['keyword'] . '%');
		}
        if (!empty($_GET['artist_classify'])) { //分类的
            $condition['artist_classify'] = $_GET['artist_classify'];
        }
        $_GET['is_rec'] ? $condition['is_rec'] = $_GET['is_rec'] : '';
		$order = $_GET['order'] ? $_GET['order'].' DESC' : '';//按照最新和热度排序 artist_vendue_id、artist_click
        $_GET['order'] == 'artist_sort' ? $order = "artist_sort ASC" : '';
		$condition['store_apply_state'] = 40;
		$list = $model_artist->getArtist_StoreList($condition,$this->page,$order);
        $artist_list = $this->_list($list);
        $page_count = $model_artist->gettotalpage();
		output_data(array('artist_list' => array_values($artist_list)),mobile_page($page_count));
	}

	private function _list($artist_list = ''){
	    if(empty($artist_list)){
	        return array();
        }
        $model_store = Model('store');
		foreach($artist_list as $key => $value){
			// 如果已登录 判断该店铺是否已被收藏
			if ($memberId = $this->getMemberIdIfExists()) {
				$c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($value['store_id'], $memberId);
				$artist_list[$key]['is_favorate'] = $c > 0;
			} else {
				$artist_list[$key]['is_favorate'] = false;
			}

			//收藏
			$store_info = $model_store->getStoreInfo(array('store_id' => $value['store_id']));
			$artist_list[$key]['store_collect'] = $store_info['store_collect'];

			$artist_list[$key]['artist_image_path'] = UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$value['artist_image'];

			$artist_works_list = array();
			//作品集
			$artist_works = unserialize($value['artist_works']);
			if(!empty($artist_works)){
				foreach ($artist_works as $k => $works){
					$works['work_pic_path'] = UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$works['work_pic'];
					$artist_works_list[] = $works;
				}
			}else{
				$artist_works_list = array();
			}

			$artist_list[$key]['artist_works'] = $artist_works_list;

			$artist_works_count = count($artist_works);
			$artist_list[$key]['artist_works_count'] = $artist_works_count;

            //获取城市和地区
            $model_area = Model('area');
            $city_info = $model_area->getAreaInfo(array('area_id'=>$value['city_id']),'area_name');
            $area_info = $model_area->getAreaInfo(array('area_id'=>$value['area_id']),'area_name');
            $artist_list[$key]['area_name'] = $city_info['area_name'];
            $artist_list[$key]['city_name'] = $area_info['area_name'];

            // 每个艺术家来3张售卖的全景图
            $condition = array('store_id'=>$value['store_id']);
            $image_list_info = $this->get_artist_goods_image($condition);

            if(empty($image_list_info['image_list'])){
                unset($artist_list[$key]);
			    continue;
            }
            $artist_list[$key]['image_list_info'] = $image_list_info['image_list'];
            $artist_list[$key]['sales_goods_count'] = $image_list_info['sales_goods_count'];
		}
        return $artist_list;
	}

	public function artist_detailOp(){
		if(!$_POST['artist_vendue_id']){
			output_error('非法参数');
		}
		$artist_vendue_id = $_POST['artist_vendue_id'];

		$model_artist = Model('artist_vendue');
		$artist_vendue_info = $model_artist->getArtist_StoreInfo(array('artist_vendue_id' => $artist_vendue_id));
		$artist_represent = unserialize($artist_vendue_info['artist_represent']);
		if(!empty($artist_represent)){
			foreach($artist_represent as $value){
				$artist_represent_list[] = $value;
			}
		}else{
			$artist_represent_list = array();
		}

		$artist_vendue_info['artist_represent'] = $artist_represent_list;
		$artist_awards = unserialize($artist_vendue_info['artist_awards']);
		if(!empty($artist_awards)){
			foreach($artist_awards as $val){
				$artist_awards_list[] = $val;
			}
		}else{
			$artist_awards_list = array();
		}

		$artist_vendue_info['artist_awards'] = $artist_awards_list;
		$artist_works = unserialize($artist_vendue_info['artist_works']);
		if(!empty($artist_works)){
			foreach($artist_works as $works){
				$works['work_pic_path'] = UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$works['work_pic'];
				$artist_works_list[] = $works;
			}
		}else{
			$artist_works_list = array();
		}
		//地区
        $model_area = Model('area');
        $city_info = $model_area->getAreaInfo(array('area_id'=>$artist_vendue_info['city_id']),'area_name');
        $area_info = $model_area->getAreaInfo(array('area_id'=>$artist_vendue_info['area_id']),'area_name');
        $artist_vendue_info['area_name'] = $city_info['area_name'];
        $artist_vendue_info['city_name'] = $area_info['area_name'];

		$artist_vendue_info['artist_works_count'] = count($artist_works);
		$artist_vendue_info['artist_works'] = $artist_works_list;
		$artist_vendue_info['artist_image_path'] = UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$artist_vendue_info['artist_image'];

		// 如果已登录 判断该店铺是否已被收藏
		if ($memberId = $this->getMemberIdIfExists()) {
			$c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($artist_vendue_info['store_id'], $memberId);
			$artist_vendue_info['is_favorate'] = $c > 0;
		} else {
			$artist_vendue_info['is_favorate'] = false;
		}

		$model_store = Model('store');
		$store_info = $model_store->getStoreInfo(array('store_id' => $artist_vendue_info['store_id']));
		$artist_vendue_info['store_collect'] = $store_info['store_collect'];
		$data = array();
		$data['artist_click'] = $artist_vendue_info['artist_click'] +1;
		$model_artist->editArtist_vendue($data,array('artist_vendue_id'=>$artist_vendue_id));

		output_data(array('artist_details' => $artist_vendue_info));

	}

	//艺术家作品拍卖
    public function ajax_art_sale_goods_pageOp(){
        $condition = array();
        $condition['store_id'] = $_GET['store_id'];
        $data = $this->_ngetGoodsList($condition, $_GET['page'], $_GET['per_count']);
        output_data($data);
    }


    /**
     * 新获取商品列表
     * @param unknown $condition
     * @param unknown $order
     */
    private function _ngetGoodsList($condition, $page = 1, $num = 4, $order = 'goods_id desc') {
        /** @var goodsModel $model_goods */
        $model_goods = Model('goods');
        $fieldstr = "goods_id,goods_commonid,goods_name,goods_jingle,store_id,store_name,goods_price,goods_promotion_price,goods_marketprice,goods_storage,goods_image,goods_freight,goods_salenum,color_id,evaluation_good_star,evaluation_count,goods_promotion_type,goods_collect,goods_click";
        $count = $model_goods->getGoodsOnlineCount($condition,"distinct goods_commonid");

        $limit = ($page - 1)*$num.','.$num;
        $recommended_goods_list = $model_goods->getGoodsOnlineList($condition, $fieldstr, 0, $order, $limit, 'goods_commonid', false);

        $format_data = array();
        foreach($recommended_goods_list as $value){

            $result2 = $model_goods->getGoodsCommonInfoByID($value['goods_commonid']);

            $type = '';
            foreach(unserialize($result2['goods_attr']) as $val){
                $val= array_values($val);
                if($val[0] == '材质'){
                    $type = $val[1];
                }
            }

            $size = '';
            $year = '';
            foreach(unserialize($result2['goods_custom']) as $val){
                if($val['name'] == '尺寸'){
                    $size = $val['value'];
                }

                if($val['name'] == '年代'){
                    $year = $val['value'];
                }

            }
            // 商品多图
            $image_more = $model_goods->getGoodsImageByKey($value['goods_commonid'] . '|' . $value['color_id']);
            $image_list = array();
            if (!empty($image_more)) {
                array_splice($image_more, 5);
                foreach ($image_more as $val) {
                    $image_list[] = array( 'small' => cthumb($val['goods_image'] , 60 , $value['store_id']) , 'mid' => cthumb($val['goods_image'] , 360 , $value['store_id']) , 'big' => cthumb($val['goods_image'] , 1280 , $value['store_id']) );
                }
            } else {
                $image_list[] = array( 'small' => thumb($value,60) , 'mid' => thumb($value , 360) ,'big' => thumb($value , 1280) );
            }

            $row = array(
                'id' => $value['goods_id'],
                'title' => $value['goods_name'],
                'price' => $value['goods_promotion_price'],
                'cover' => cthumb($value['goods_image'], 360, $value['store_id']),  //图片
                'state' => '商城在售',
                'href' => urlShop('goods', 'index', array('goods_id' => $value['goods_id'])),
                'created_at' => $year, //创作时间
                'type' => $type,   //材料
                'size' => $size,   //尺寸
                'image_list' => $image_list,
                'vote_count' => $value['goods_click'],     //点击量
                'view_count' => $value['goods_collect'],     //收藏量
                'goods_sale_count' => $count < 10 ? $count : 10,     //售卖商品数
            );
            $format_data[] = $row;
        }
        $data = array(
            'data' => $format_data,
            'meta' => array(
                'pagination' => array(
                    'total' => $count,
                    'per_page' => $num,
                    'current_page' => $page,
                    'total_pages' => ceil($count/$num),
                )
            )
        );

        return $data;
    }

    /**
     * 获取艺术3张全景图
     * @param array $condition
     */
    private function get_artist_goods_image($condition) {
        $model_goods = Model('goods');
        $fieldstr = "goods_id,store_id,goods_image,color_id,goods_commonid";
        $condition['goods_storage'] = ['gt',"0"];
        $count = $model_goods->getGoodsOnlineCount($condition,"distinct goods_commonid");

        $order = 'artist_sort asc,goods_id desc';
        $recommended_goods_list = $model_goods->getGoodsOnlineList($condition, $fieldstr, 0, $order, 3, 'goods_commonid', false);
        $image_list = array();
        foreach($recommended_goods_list as $value){
            $image_list[] = array(
                'small' => thumb($value,60) ,
                'mid' => thumb($value , 360),
                'big' => thumb($value , 1280),
                'goods_id' => $value['goods_id']
            );
        }
        return  array('image_list'=>$image_list,'sales_goods_count'=>$count);
    }


    /**
     * 获取艺术家分类
     */
    public function get_artist_classifyOp() {
        $model_artist_classify = Model('artist_classify');
        $condition = array();
        $artist_classify = $model_artist_classify->getArtist_classifyList($condition, $page = null, $order = '', $field = '*', $limit = '');
        output_data($artist_classify);
    }
}