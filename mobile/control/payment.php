<?php
/**
 * 支付回调
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class paymentControl extends mobileHomeControl{

    private $payment_code;
    private $distribute_str = array(
        '童生',
        '秀才',
        '举人',
        '贡士',
        '进士'
    );
    public function __construct() {
        parent::__construct();

        $this->payment_code = $_GET['payment_code'];
    }

    /**
     * 支付回调
     */
    public function returnOp() {
        unset($_GET['act']);
        unset($_GET['op']);
        unset($_GET['payment_code']);
        $payment_api = $this->_get_payment_api();

        $payment_config = $this->_get_payment_config();

        $callback_info = $payment_api->getReturnInfo($payment_config);

        if($callback_info) {

            //验证成功
            $result = $this->_update_order($callback_info['out_trade_no'], $callback_info['trade_no']);
            if($result['state']) {
                Tpl::output('result', 'success');
                Tpl::output('message', '支付成功');
            } else {
                Tpl::output('result', 'fail');
                Tpl::output('message', '支付失败');
            }
            Tpl::output('order_type', $result['order_type']);
            $str = iconv('UTF-8', 'gbk', print_r($result, true));
            file_put_contents(BASE_DATA_PATH . '/log/jrl.txt', $str);
        } else {
            //验证失败
            Tpl::output('result', 'fail');
            Tpl::output('message', '支付失败');
        }

        Tpl::showpage('payment_message');
    }

    /**
     * 支付提醒
     */
    public function notifyOp() {
        yLog()->debug("payment_notify", [$_REQUEST]);
        if($this->payment_code == 'tlpay'){
            $special = false;
            $success = 'success';
            $error = 'error';
            require_once BASE_PATH.DS.'api'.DS.'payment'.DS.'tonglian'.DS.'AppConfig.php';
            require_once BASE_PATH.DS.'api'.DS.'payment'.DS.'tonglian'.DS.'AppUtil.php';

            $params = array();
            foreach($_POST as $key=>$val) {//将参数遍历获取
                $params[$key] = $val;
            }
            if(isset($params['special_sign'])){
                $success = json_encode($success);
                $error = json_encode($error);
                $special = true;
                $str = iconv("GB2312", "UTF-8", $params['order_sn']);
                $tmp = md5(md5($str).'special_code');
                if($tmp == $params['special_sign']){
                    $special = true;
                }
            }
//            file_put_contents(BASE_DATA_PATH.'/log/tl_test.log',"------notify------\r\n",FILE_APPEND);
//            file_put_contents(BASE_DATA_PATH.'/log/tl_test.log',json_encode($params)."\r\n",FILE_APPEND);
//            echo 'error';exit;
            if(count($params)<1){//如果参数为空,则不进行处理
                echo $error;
                exit();
            }
            if($params['trxstatus'] != '0000'){
                file_put_contents(BASE_DATA_PATH.'/log/tl_pay.log',"------fail-------------\r\n",FILE_APPEND);
                file_put_contents(BASE_DATA_PATH.'/log/tl_pay.log',json_encode($params)."\r\n",FILE_APPEND);
                echo $error;exit;
            }
            if($special||AppUtil::ValidSign($params, AppConfig::APPKEY)){//验签成功
                $trade_no = $params['trxid'];
                if(substr($params['bizseq'],0,9) == 'underline'){
                    $order_sn = substr($params['bizseq'],9);
                    try{
                        Model('underline_partner_order')->beginTransaction();

                        $order_info = Model('underline_partner_order')->getOrderInfo(array('order_sn'=>$order_sn));
                        if($order_info['order_status'] == 1){
                            throw new Exception('不可重复操作');
                        }
                        $number = $params['amount']/100;
                        if($number > 0){
                            $update = array(
                                'td_amount' => array('exp','td_amount+'.$number)
                            );
                            $re = Model('member')->editMember(array('member_id' => $order_info['member_id']), $update);
                            if(!$re){
                                throw new Exception('用户信息更新失败');
                            }
                        }else{
                            throw new Exception('支付金额异常');
                        }
                        $data = array(
                            'trade_no'=>$trade_no,
                            'order_status'=>1,
                            'finish_time'=>date('Y-m-d H:i:s'),
                            'msg'=>$params['msg']
                        );
                        $condition = array(
                            'order_sn'=>$order_sn,
                            'order_status'=>0
                        );
                        $re1 = Model('underline_partner_order')->updateInfo($data,$condition);
                        if(!$re1){
                            throw new Exception('订单更新失败');
                        }
                        $re2 = Logic('j_member_distribute')->upgrade_lv($order_info['member_id'],$order_info['type']);
                        if(!$re2){
                            throw new Exception('用户升级失败');
                        }
                        $tmp_data = array(
                            'buyer_id'=>$order_info['member_id'],
                            'order_id'=>$order_info['id'],
                            'order_sn'=>$order_sn,
                            'order_goods_id'=>0,
                            'goods_id'=>0,
                            'goods_name'=>'线下升级'.$this->distribute_str[$order_info['type']].'商品返佣',
                            'goods_image'=>'',
                            'goods_pay_amount'=>$order_info['amount'],
                            'goods_commission'=>$order_info['amount']
                        );
                        $this->_commissionHandle($tmp_data);
                        $re3 = $this->handleUnrefundOrder($order_info['id']);
                        if(!$re3){
                            throw new Exception('商品返佣失败');
                        }
                        $param = array();
                        $member = Model('member')->getMemberInfo(array('member_id'=>$order_info['member_id']));
                        if($member['member_mobile']!=''){
                            $param['code'] = 'member_leaveup';
                            $param['member_id'] = $member['member_id'];
                            $param['number']['mobile'] = $member['member_mobile'];
                            $param['param'] = array(
                                'name' => $this->distribute_str[$order_info['type']]
                            );
                            QueueClient::push('sendMemberMsg', $param);
                        }
                        if($special){
                            $param = array(
                                'admin_id'=>$params['aid'],
                                'admin_name'=>$params['aname'],
                                'content'=>"管理员ID:{$params['aid']}手动更改线下合伙人[member_id:{$member['member_id']}]订单状态[{$order_sn}]",
                                'cdate'=>date('Y-m-d H:i:s')
                            );
                            Model('admin_underline_order_log')->insert($param);
                        }
                        Model('underline_partner_order')->commit();
                    }catch(Exception $e){
                        $err = $e->getMessage();
                        file_put_contents(BASE_DATA_PATH.'/log/tl_pay.log',"------error-------------\r\n",FILE_APPEND);
                        file_put_contents(BASE_DATA_PATH.'/log/tl_pay.log',json_encode($params)."\r\n",FILE_APPEND);
                        file_put_contents(BASE_DATA_PATH.'/log/tl_pay.log',"{$err}\r\n",FILE_APPEND);
                        Model('underline_partner_order')->rollback();
                        echo $error;exit;
                    }
                    //线下
                }else{
                    file_put_contents(BASE_DATA_PATH.'/log/tl_test.log',"------online------\r\n",FILE_APPEND);
                    $result = $this->_update_order($params['bizseq'], $trade_no,$params['amount']/100);
                    if($result['state']) {
                        echo 'success';die;
                    }else{
                        echo 'error';exit;
                    }

                }

                //此处进行业务逻辑处理
                echo $success;exit;
            }
            else{
                file_put_contents(BASE_DATA_PATH.'/log/tl_test.log',"------sign_fail------\r\n",FILE_APPEND);
                echo $error;exit;
            }
        }
        if($this->payment_code == 'lklpay'){
            $time = time();
            $d = json_decode(file_get_contents('php://input'),true);
            $data = $d['data'];
            if($data['status'] == 'SUCCEED'){
                $lkl_order_sn = $data['order_no'];
                Model('lkl_order')->updateInfo($lkl_order_sn,array('status'=>1));
                $internalSn = $data['body'];
                $externalSn = $data['transaction_no'];
                $updateSuccess = $this->_update_order($internalSn, $externalSn,$data['amount']);
                if($updateSuccess['state']) {
                    echo 'success';die;
                }
            }
            //如果失败不再接收通知
            echo 'success';die;
            exit;
        }
        // wxpay_jsapi
        if ($this->payment_code == 'wxpay_jsapi') {
            $api = $this->_get_payment_api();
            $params = $this->_get_payment_config();
            $api->setConfigs($params);

            list($result, $output) = $api->notify();
            file_put_contents(BASE_DATA_PATH . '/log/jrl.txt', json_encode($result)."\r\n", FILE_APPEND);
            if ($result) {
                $internalSn = $result['out_trade_no'] . '_' . $result['attach'];
                $externalSn = $result['transaction_id'];
                $api_pay = $result['total_fee']/100;
                $updateSuccess = $this->_update_order($internalSn, $externalSn,$api_pay);

                if (!$updateSuccess["state"]) {
                    yLog()->error("payment_notify_wxpay_jsapi_error", [$updateSuccess]);
                    // @todo
                    // 直接退出 等待下次通知
                    exit;
                }
            }

            echo $output;
            exit;
        }
        // wxpay_mini 或者 yinuovip_mini
        if ($this->payment_code == 'wxpay_mini' || $this->payment_code == 'yinuovip_mini') {
            $api = $this->_get_payment_api();
            $params = $this->_get_payment_config();
            $api->setConfigs($params);

            list($result, $output) = $api->notify();
            file_put_contents(BASE_DATA_PATH . '/log/jrl.txt', json_encode($result)."\r\n", FILE_APPEND);
            if ($result) {
                $internalSn = $result['out_trade_no'] . '_' . $result['attach'];
                $externalSn = $result['transaction_id'];
                $api_pay = $result['total_fee']/100;
                $updateSuccess = $this->_update_order($internalSn, $externalSn,$api_pay);

                if (!$updateSuccess["state"]) {
                    yLog()->error("payment_notify_wxpay_mini_error", [$updateSuccess]);
                    // @todo
                    // 直接退出 等待下次通知
                    exit;
                }
            }

            echo $output;
            exit;
        }
        try {
            // 恢复框架编码的post值
            $_POST['notify_data'] = html_entity_decode($_POST['notify_data']);
            $payment_api = $this->_get_payment_api();

            $payment_config = $this->_get_payment_config();

            $callback_info = $payment_api->getNotifyInfo($payment_config);
//            $str = iconv('UTF-8', 'gbk', print_r($callback_info, true));
            //验证失败
            if(!$callback_info) {
                yLog()->error("payment_notify_wxNotify_error", [$_REQUEST]);
                echo "fail";die;
            }
            yLog()->debug("payment_notify_wxNotifyInfo", [$callback_info]);
            //验证成功
            $result = $this->_update_order($callback_info['out_trade_no'], $callback_info['trade_no'],$callback_info['pay_amount']);
            //更新失败
            if(empty($result['state'])) {
                yLog()->error("payment_notify_update_order_error", [$result, $_REQUEST]);
                echo "fail";die;
            }
        }catch (\Exception $e){
            yLog()->error("payment_notify_Exc_error", [\Yinuo\Lib\Helpers\LogHelper::getEInfo($e), $_REQUEST]);
            //验证失败
            echo "fail";die;
        }
        echo 'success';die;
    }

    /**
     * 支付宝移动支付
     */
    public function notify_alipay_nativeOp() {
        $this->payment_code = 'alipay_native';
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';

        if(is_file($inc_file)) {
            require($inc_file);
        }
        $payment_config = $this->_get_payment_config();
        $payment_api = new $this->payment_code();
        $payment_api->payment_config = $payment_config;
        $payment_api->alipay_config['partner'] = $payment_config['alipay_partner'];
        file_put_contents(BASE_DATA_PATH . '/log/jrl.txt', json_encode($_POST)."\r\n",FILE_APPEND);
        //商户订单号
        $out_trade_no = $_POST['out_trade_no'];

        if ($payment_api->verify_notify()) {

            //支付宝交易号
            $trade_no = $_POST['trade_no'];

            //交易状态
            $trade_status = $_POST['trade_status'];

            $pay_amount = $_POST['buyer_pay_amount'];

            if($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
                $result = $this->_update_order($out_trade_no, $trade_no,$pay_amount);
                if(!$result['state']) {
                    logResult("订单状态更新失败".$out_trade_no);
                }
            }
            exit("success");
        } else {
            logResult("verifyNotify验证失败".$out_trade_no);
            exit("fail");
        }
    }


    /**
     * 支付宝移动支付
     */
    public function notify_alipay_aopOp() {
        yLog()->debug("notify_alipay_aopOp", $_REQUEST);
        try{
            $_POST['fund_bill_list'] = htmlspecialchars_decode($_POST['fund_bill_list']);
            $this->payment_code = 'AopClient';
            $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_aopclient'.DS.'aop'.DS.'AopClient.php';

            if(is_file($inc_file)) {
                require($inc_file);
            }
            $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_native'.DS.'alipay_native.php';
            require($inc_file);
            new alipay_native();
            $payment_config = $this->_get_payment_config();
            $payment_api = new $this->payment_code();
            $payment_api->payment_config = $payment_config;
            $payment_api->alipay_config['partner'] = $payment_config['alipay_partner'];
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            $payment_api->alipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB';
            if ($payment_api->rsaCheckV1($_POST,null,'RSA')) {
                //支付宝交易号
                $trade_no = $_POST['trade_no'];

                //交易状态
                $trade_status = $_POST['trade_status'];
                $pay_amount = $_POST['buyer_pay_amount'];
                if($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
                    $result = $this->_update_order($out_trade_no, $trade_no,$pay_amount);
                    if(!$result['state']) {
                        yLog()->error("notify_alipay_aopOp_order_status_update_error", [$_REQUEST, $result]);
                        logResult("订单状态更新失败".$out_trade_no);
                        exit("fail");
                    }
                }
                exit("success");
            } else {
                yLog()->error("notify_alipay_aopOp_rsaCheckV1_error", [$_REQUEST]);
                logResult("rsaCheckV1验证失败".$out_trade_no);
                exit("fail");
            }
        }catch (\Exception $e){
            yLog()->error("notify_alipay_aopOp_exception_error", [$_REQUEST, \App\Lib\Helpers\LogHelper::getEInfo($e)]);
            exit("fail");
        }
    }

    /**
     * 拉卡拉支付结果回调
     */
    public function notify_lklpayOp(){

    }

    /**
     * 获取支付接口实例
     */
    private function _get_payment_api()
    {
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';

        if (is_file($inc_file)) {
            require($inc_file);
        }
        return new $this->payment_code();
    }

    /**
     * 获取支付接口信息
     */
    private function _get_payment_config() {
        $model_mb_payment = Model('mb_payment');

        if ($this->payment_code == 'wxpay_mini') {
            $payment_info['payment_config'] = C('mini_payment_info');
        } else {
            //读取接口配置信息
            $condition = array();
            if ($this->payment_code == 'wxpay3') {
                $condition['payment_code'] = 'wxpay';
            } else {
                $condition['payment_code'] = $this->payment_code;
            }
            $payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
        }

        return $payment_info['payment_config'];
    }

    /**
     * 更新订单状态
     */
    private function _update_order($out_trade_no, $trade_no,$pay_amount) {
        /** @var orderModel $model_order */
        /** @var paymentLogic $logic_payment */
        $model_order = Model('order');
        $logic_payment = Logic('payment');
        $tmp = explode('_', $out_trade_no);
        $out_trade_no = $tmp[0];
        if (!empty($tmp[1])) {
            $order_type = $tmp[1];
        } else {
            $order_pay_info = $model_order->getOrderPayInfo(array('pay_sn'=> $out_trade_no));
            if (!empty($order_pay_info)) {
                $order_type = 'r';
            }
            /** @var vr_orderModel $vr_order_model */
            $vr_order_model = Model('vr_order');
            $order_pay_info = $vr_order_model->getOrderInfo(array('order_sn' => $out_trade_no));
            if (!empty($order_pay_info)) {
                $order_type = 'r';
            }
            /** @var margin_ordersModel $margin_orders_model */
            $margin_orders_model = Model('margin_orders');
            $order_pay_info = $margin_orders_model->getOrderInfo(array('order_sn' => $out_trade_no));
            if (!empty($order_pay_info)) {
                $order_type = 'm';
            }
            $order_pay_info = $model_order->getOrderInfo(array('order_sn' => $out_trade_no,'auction_id'=>array('neq','1')));
            if (!empty($order_pay_info)) {
                $order_type = 'a';
            }
            /** @var predepositModel $predeposit_model */
            $predeposit_model = Model('predeposit');
            $order_pay_info = $predeposit_model->getPdRechargeInfo(array('pdr_sn'=>$out_trade_no));
            if(!empty($order_pay_info)){
                $order_type = 'p';
            }
        }
        // wxpay_jsapi
        $paymentCode = $this->payment_code;
        if ($paymentCode == 'wxpay_jsapi') {
            $paymentCode = 'wx_jsapi';
        } elseif ($paymentCode == 'wxpay3') {
            $paymentCode = 'wxpay';
        } elseif ($paymentCode == 'alipay_native') {
            $paymentCode = 'ali_native';
        } elseif ($paymentCode == 'wxpay_mini') {
            $paymentCode = 'wx_mini';
        }

        if ($order_type == 'r') {
            $result = $logic_payment->getRealOrderInfo($out_trade_no);
            if (intval($result['data']['api_pay_state'])) {
                return array('state'=>true);
            }
            $order_list = $result['data']['order_list'];
            $api_pay_amount = 0;
            if (!empty($order_list)) {
                foreach ($order_list as $order_info) {
                    $api_pay_amount += $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount']-$order_info['points_amount'];
                }
                $api_pay_amount = ncPriceFormat($api_pay_amount);
            }
            //非正式环境强制相等
            if (!isNcProduction()){
                $pay_amount = $api_pay_amount;
            }
            if($pay_amount != $api_pay_amount) {
                $tmp = array(
                    'trade_no'=>$trade_no,
                    'order_sn'=>$out_trade_no,
                    'api_price'=>$api_pay_amount,
                    'api_price_t'=>$pay_amount,
                    'cdate'=>date('Y-m-d H:i:s')
                );
                Model()->table('payment_error_log')->insert($tmp);
                Model('orders')->editOrder(array('order_state'=>50),array('order_sn'=>$out_trade_no));
                return array('state'=>true);
            }
            $result = $logic_payment->updateRealOrder($out_trade_no, $paymentCode, $order_list, $trade_no, $pay_amount);
            //TODO 成交返佣
            //TODO  待确定 之前的竞拍出价是否添加返佣
            /** @var member_commissionModel $member_commission */
            $member_commission = Model('member_commission');
            $auctionModel = Model('auctions');
            $auction_info = $auctionModel->where(['auction_id' => $order_list[0]['auction_id']])->find();
            if ($auction_info && $auction_info['auction_type'] != $auctionModel::AUCTION_TYPE_NEWBIE) {
                $member_commission->transactionForCommission($order_list[0]);
            }

            $log_buyer_id = $order_list[0]['buyer_id'];
            $log_buyer_name = $order_list[0]['buyer_name'];
            $log_desc = '实物订单使用'.orderPaymentName($paymentCode).'成功支付，支付单号：'.$out_trade_no;

        } elseif ($order_type == 'v') {
            $result = $logic_payment->getVrOrderInfo($out_trade_no);
            $order_info = $result['data'];
            if (!in_array($result['data']['order_state'],array(ORDER_STATE_NEW,ORDER_STATE_CANCEL))) {
                return array('state'=>true);
            }
            $result = $logic_payment->updateVrOrder($out_trade_no, $paymentCode, $result['data'], $trade_no);

            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'];
            $log_buyer_id = $order_info['buyer_id'];
            $log_buyer_name = $order_info['buyer_name'];
            $log_desc = '虚拟订单使用'.orderPaymentName($paymentCode).'成功支付，支付单号：'.$out_trade_no;
        } elseif ($order_type == 'm') {
            // 获取订单信息
            $result = $logic_payment->getMgOrderInfo($out_trade_no);
            $order_info = $result['data'];
            // 判断订单状态是否已经支付
            if (intval($order_info['api_pay_state'])) {
                return array('state'=>true);
            }
            // 计算第三方支付的金额
            $api_pay_amount = bcsub($order_info['margin_amount'],
                bcadd(bcadd(bcadd(bcadd($order_info['pd_amount'], $order_info['rcb_amount'], 2), $order_info['points_amount'], 2) , $order_info['account_margin_amount'], 2), $order_info['bag_amount'], 2), 2);
            $api_pay_amount = ncPriceFormat($api_pay_amount);
            //非正式环境强制相等
            if (!isNcProduction()){
                $pay_amount = $api_pay_amount;
            }
            if($pay_amount != $api_pay_amount){
                $tmp = array(
                    'trade_no'=>$trade_no,
                    'order_sn'=>$out_trade_no,
                    'api_price'=>$api_pay_amount,
                    'api_price_t'=>$pay_amount,
                    'cdate'=>date('Y-m-d H:i:s')
                );

                Model('margin_api_pai_error_log')->table('payment_error_log')->insert($tmp);
                /** @var margin_ordersModel $margin_orders */
                $margin_orders = Model('margin_orders');
                $margin_orders->editOrder(array('order_state'=>5),array('order_sn'=>$out_trade_no));
//                file_put_contents(BASE_DATA_PATH . '/log/wt_1212.log',json_encode($tmp)."\r\n",FILE_APPEND);
//                file_put_contents(BASE_DATA_PATH . '/log/wt_1212.log',"666666666\r\n",FILE_APPEND);
                //margin_list 插入记录 list记录表  结算编号
                $auction_id = isset($order_info['auction_id']) ?: 0;
                if ($auction_id) {
                    /** @var margin_listModel $margin_list_model */
                    $margin_list_model = Model('margin_list');
                    $condition = [
                        'buyer_id' => $order_info['buyer_id'],
                        'auction_id' => $order_info['auction_id']
                    ];
                    if (!empty($condition)) {
                        $margin_list_data = $margin_list_model->where($condition)->find();
                        if ($margin_list_data) {
                            //$update = ['auction_click' => array('exp', 'auction_click + '.$click)];
                            $update = [
                                'all_money' => ['exp', 'all_money + ' . $order_info['margin_amount']],
                                'pay_num' => ['exp', 'pay_num + ' . 1],
                                'last_pay_time' => TIMESTAMP,
                                'last_payment_code' => $order_info['payment_code']
                            ];
                            $margin_list_model->where($condition)->update($update);
                        } else {
                            $list_insert = [
                                'list_sn' => $order_info['order_sn'],
                                'buyer_id' => $order_info['buyer_id'],
                                'buyer_name' => $order_info['buyer_name'],
                                'all_money' => $order_info['margin_amount'],
                                'pay_num' => 1,
                                'last_payment_code' => $order_info['payment_code'],
                                'last_pay_time' => TIMESTAMP,
                                'auction_id' => $order_info['auction_id'],
                                'statue' => 1,
                                'create_time' => TIMESTAMP,
                            ];
                            $margin_list_model->insert($list_insert);
                        }
                    }
                }
                return array('state'=>true);
//                dd();
            }
            /** @var paymentLogic $logic_payment */
            $result = $logic_payment->updateMgOrder($order_info['payment_code'], $order_info, $trade_no);

            $log_buyer_id = $order_info['buyer_id'];
            $log_buyer_name = $order_info['buyer_name'];
            $log_desc = '保证金订单使用'.orderPaymentName($order_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;

        } elseif ($order_type == 'a') {
            $result = $logic_payment->getAtOrderInfo($out_trade_no);
            $order_info = $result['data'];
            //订单存在被系统自动取消的可能性
            if (!in_array($order_info['order_state'],array(ORDER_STATE_NEW,ORDER_STATE_CANCEL))) {
                return array('state'=>true);
            }
            /** @var member_commissionModel $member_commission */
            $member_commission = Model('member_commission');
            //TODO 成交返佣处理
            $auctionModel = Model('auctions');
            $auction_info = $auctionModel->where(['auction_id' => $order_info['auction_id']])->find();
            if ($auction_info && $auction_info['auction_type'] != $auctionModel::AUCTION_TYPE_NEWBIE) {
                $member_commission->transactionForCommission($order_info);
            }
            $result = $logic_payment->updateAtOrder($order_info['payment_code'], $order_info, $trade_no);
            // 计算api支付金额
            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'] - $order_info['points_amount'] - $order_info['margin_amount'];

            $log_buyer_id = $order_info['buyer_id'];
            $log_buyer_name = $order_info['buyer_name'];
            $log_desc = '拍卖订单使用'.orderPaymentName($order_info['payment_code']).'成功支付，支付单号：'.$out_trade_no;
        }elseif ($order_type == 'p'){
            $result = $logic_payment->getPdOrderInfo($out_trade_no);
            $order_info = $result['data'];
            if (intval($result['data']['pdr_payment_state'])) {
                return array('state'=>true, 'order_type' => $order_type);
            }
            $payment_info = array('payment_code'=>$this->payment_code,'payment_name'=>orderPaymentName($this->payment_code));
            $result = $logic_payment->updatePdOrder($out_trade_no,$trade_no,$payment_info,$order_info);
            $result['order_type'] = $order_type;
        }
        if ($result['state'] && $order_type != 'p') {
            if($order_type == 'm'){
                $margin_info = Model('margin_orders')->getOrderInfo(array('margin_id'=>$order_info['margin_id']));
                $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);
                $day_num = Logic('auction')->getInterestDay($margin_info['payment_time'],$auction_info['auction_end_time']);//计息天数
                $condition = array(
                    'buyer_id'=>$order_info['buyer_id'],
                    'order_state'=>array('neq','4'),
                    'margin_id'=>array('neq',$order_info['margin_id'])
                );
                $tmp_re = Model('margin_orders')->getOrderInfo($condition);
                if(TIMESTAMP < strtotime($auction_info['interest_last_date'])&&($auction_info['auction_type'] != 1 || empty($tmp_re))){
                    $member_distribute_logic = Logic('j_member_distribute');
                    //添加利息待结算记录
                    //$re = $member_distribute_logic->margin_refund($margin_info,$day_num,$auction_info['auction_bond_rate']);
                    //添加佣金待结算记录
                    $result_3 = $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);
                    //添加区域代理佣金待结算记录
                    $member_distribute_logic->agent_margin_refund($margin_info, $day_num,0);
                }
            }
            //记录消费日志
            QueueClient::push('addConsume', array('member_id'=>$log_buyer_id,'member_name'=>$log_buyer_name,
                'consume_amount'=>ncPriceFormat($api_pay_amount),'consume_time'=>TIMESTAMP,'consume_remark'=>$log_desc));
        }

        return $result;
    }

    /**
     * 线下支付配置数据
     * @return [type] [description]
     */
    public function get_admin_underline_configOp(){
        $model_payment = Model('payment');
        $underline_data = $model_payment->getPaymentInfo(array('payment_code' => 'underline'));
        $underline_data['payment_config'] = unserialize($underline_data['payment_config']);
        output_data($underline_data);
    }

    public function getOrderInfoOp(){
        require_once BASE_PATH.DS.'api'.DS.'payment'.DS.'tonglian'.DS.'AppConfig.php';
        require_once BASE_PATH.DS.'api'.DS.'payment'.DS.'tonglian'.DS.'AppUtil.php';
        require_once BASE_PATH.DS.'api'.DS.'payment'.DS.'tonglian'.DS.'RspModel.php';
        $params = array();
        foreach($_POST as $key=>$val) {//将参数遍历获取
            $params[$key] = $val;
        }
        $rsp = new RspModel();
        if(AppUtil::ValidSign($params, AppConfig::APPKEY)){//验签成功,进行业务处理返回
            $tmp = substr($_POST['bizseq'],0,9);
            $data = array();
            file_put_contents('../tl_test.log',"------success-{$tmp}------\r\n",FILE_APPEND);
            if($tmp == "underline"){
                file_put_contents('../tl_test.log',"------success-88888888------\r\n",FILE_APPEND);
                //线下合伙人升级订单
                $order_sn = substr($_POST['bizseq'],9);
                $re = Model('underline_partner_order')->getOrderInfo(array('order_sn'=>$order_sn));
                if(empty($re)){
                    $rsp->init("9999", "订单号不存在");//验证签名失败
                    $rsp->sign();//返回加签
                    echo json_encode($rsp);exit;
                }
                $data['amount'] = $re['amount']*100;
                $data['bizseq'] = $_POST['bizseq'];
            }else{
                //保证金订单
                $order_sn = $_POST['bizseq'];
                $order_info = Model('margin_orders')->getOrderInfo(array('order_sn'=>$order_sn,'order_state'=>0));
                if(empty($order_info)){
                    $rsp->init("9999", "该订单不存在");//验证签名失败
                    $rsp->sign();//返回加签
                    echo json_encode($rsp);exit;
                }
                $data['amount'] = $order_info['margin_amount']*100;
                $data['bizseq'] = $_POST['bizseq'];
            }
            file_put_contents('../tl_test.log',json_encode($data)."------success-------\r\n",FILE_APPEND);
            file_put_contents('../tl_test.log',json_encode($_POST)."\r\n",FILE_APPEND);
    //		$re = Model('underline_partner_order')->getInfoByOrderSn(123);
    //		print_R($_);exit;
            $rsp->init("0000", "查询成功");//查询成功
            $rsp->amount = "{$data['amount']}";//1分钱测试交易
            $rsp->bizseq = "{$data['bizseq']}";
    		$rsp->trxreserve = "05#####{$order_sn}######";
    		//业务类型(05)#收费类型#订购人姓名#订购人地址#联系电话#跟踪订单号#证件类型#证件号#备注#保留字段#保留字段#保留字段#保留字段
            $rsp->sign();//返回加签
        }
        else{
            file_put_contents('../tl_test.log',"------fail-------\r\n",FILE_APPEND);
            $rsp->init("9999", "验证签名失败");//验证签名失败
            $rsp->sign();//返回加签
        }
        file_put_contents('../tl_test.log',json_encode($data)."------end-------\r\n",FILE_APPEND);
        file_put_contents('../tl_test.log',json_encode($rsp)."\r\n",FILE_APPEND);
        echo json_encode($rsp);
    }

    public function _commissionHandle($order_info = array())
    {

        if(!empty($order_info)){
            $data = array();
            $data['order_id'] = $order_info['order_id'];
            $data['order_sn'] = $order_info['order_sn'];
            $data['order_goods_id'] = $order_info['order_goods_id'];
            $data['goods_id'] = $order_info['goods_id'];
            $data['goods_name'] = $order_info['goods_name'];
            $data['goods_image'] = $order_info['goods_image'];
            $data['goods_pay_amount'] = $order_info['goods_pay_amount'];
            $data['goods_commission'] = $order_info['goods_commission'];
            $order_goods_commis[] = $data;
            $member_distribute_logic = Logic('j_member_distribute');

            //获取分销上级
            $member_relation_info = $member_distribute_logic->get_top_member($order_info['buyer_id']);

            $distribute_list = $member_distribute_logic->get_distribution_info($member_relation_info['member_id']);
            if(!empty($distribute_list)){
                foreach($distribute_list as $v){
                    if($v['goods_num'] > 0){
                        $member_relation = array(
                            'from_id' => $member_relation_info['member_id'],
                            'from_name' => $member_relation_info['member_name'],
                            'to_id' => $v['member_id'],
                            'to_name' => $v['member_name'],
                            'top_lv' => $v['level']
                        );
                        Logic('order')->_CommisHandle($member_relation,$order_goods_commis,$v['goods_num']);
                    }
                }
            }

            //区域代理佣金记录
            $agent_distribute_info = $member_distribute_logic->get_agent_info($order_info['buyer_id']);
            if(!empty($agent_distribute_info)){
                foreach($agent_distribute_info as $v){
                    if($v['goods_num'] > 0){
                        $member_distribute_logic->agent_goods_commission($v,$order_goods_commis,1);
                    }
                }
            }

        }else{
            throw new Exception('订单信息为空');
        }
    }

    private function handleUnrefundOrder($order_id = 0){
        $res = false;
        if($order_id > 0){
            $deal_list = (array)Model()->table('member_commission')->where(array('order_id' => $order_id,'goods_id'=>0,'commission_type'=>1,'dis_commis_state'=>0))->select();

            /** @var memberModel  $member_model */
            $member_model = Model('member');
            $member_model->beginTransaction();

            $flag = 0;
            try{
                foreach($deal_list as $deal){
                    if(!$this->add_pd_log($deal)){ //订单预存款支付余额并添加日志
                        throw new Exception('数据库存储错误!');
                    }

                    if(!Model()->table('member_commission')->where(array('log_id' => $deal['log_id']))->update(array('dis_commis_state' => 1, 'commission_time' => time()))){
                        throw new Exception('数据库存储错误!');
                    }
                    if(!Model('agent_order')->where(array('order_id' => $order_id))->update(array('commission_status' => 1, 'finish_time' => date('Y-m-d H:i:s')))){
                        throw new Exception('数据库存储错误!');
                    }
                    $flag++;
                }

                if($flag == count($deal_list)){
                    if(!Model()->table('orders')->where(array('order_id' => $order_id))->update(array('commission_state' => 1))){
                        throw new Exception('数据库存储错误!');
                    }
                }
                $res = true;
                $member_model->commit();
            } catch (Exception $e){
                $member_model->rollback();
            }

        }

        return $res;
    }

    public function add_pd_log($deal){
        //预存款存储记录
        /** @var predepositModel $model_pd */
        $model_pd = Model('predeposit');

        $data_pd['amount'] = $deal['commission_amount'];
        $data_pd['msg'] = '订单('.$deal['order_id'].')商品('.$deal['goods_name'].')佣金处理';
        $data_pd['member_id'] = $deal['dis_member_id'];
        $data_pd['member_name'] = $deal['dis_member_name'];
        $data_pd['commission_type'] = $deal['commission_type'];

        //记录预存款日志
        if(!$model_pd->changePd('member_distribute_buy_good', $data_pd)){
            return false;
        }

        return true;
    }
}
