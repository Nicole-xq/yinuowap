<?php
/**
 * 购买
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_buyControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 购物车、直接购买第一步:选择收获地址和配置方式
     */
    public function buy_step1Op() {
        $logic_buy = Logic('buy');
        $buy_list = $logic_buy->getBuyInfo(
            $this->member_info['member_id'],
            $_POST['cart_id'],
            $_POST['ifcart'],
            $this->member_info['store_id'],
            $_POST['address_id'],
            $_POST['dis_id']
            );

        //得到会员等级
        output_data($buy_list);
    }

    /**
     * 购物车、直接购买第二步:保存订单入库，产生订单号，开始选择支付方式
     *
     */
    public function buy_step2Op() {
        try{
            $orderPayInfo = Logic('buy')->createOrder(
                $this->member_info['member_id'],
                $_POST['ifcart'],
                $_POST['cart_id'],
                $_POST['address_id'],
                $_POST['vat_hash'],
                $_POST['offpay_hash'],
                $_POST['offpay_hash_batch'],
                $_POST['pay_name'],
                $_POST['invoice_id'],
                $_POST['rpt'],
                $_POST['pd_pay'],
                $_POST['rcb_pay'],
                $_POST['points_pay'],
                $_POST['password'],
                $_POST['fcode'],
                $_POST['voucher'],
                $_POST['pay_message'],
                2
            );
            //修改作品库的作品的售卖状态  修改为交易中
            output_data($orderPayInfo);
        }catch (\App\Exceptions\ResponseException $e){
            output_error($e->getMessage());
        }
    }

    /**
     * 验证密码
     */
    public function check_passwordOp() {
        if(empty($_POST['password'])) {
            output_error('参数错误');
        }

        $model_member = Model('member');

        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if($member_info['member_paypwd'] == md5($_POST['password'])) {
            output_data('1');
        } else {
            output_error('密码错误');
        }
    }

    /**
     * 更换收货地址
     */
    public function change_addressOp() {
        $logic_buy = Logic('buy');
        if (empty($_POST['city_id'])) {
            $_POST['city_id'] = $_POST['area_id'];
        }

        $data = $logic_buy->changeAddr($_POST['freight_hash'], $_POST['city_id'], $_POST['area_id'], $this->member_info['member_id']);
        if(!empty($data) && $data['state'] == 'success' ) {
            output_data($data);
        } else {
            output_error('地址修改失败');
        }
    }

    /**
     * 实物订单支付(新接口)
     */
    public function payOp() {
        $pay_sn = $_POST['pay_sn'];
        output_data(Logic("buy")->getPayInfo(
            $this->member_info['member_id'],
            $pay_sn
        ));
    }

    /**
     * AJAX验证支付密码
     */
    public function check_pd_pwdOp(){
        if (empty($_POST['password'])) {
            output_error('支付密码格式不正确');
        }
        $buyer_info = Model('member')->getMemberInfoByID($this->member_info['member_id'],'member_paypwd');
        if ($buyer_info['member_paypwd'] != '') {
            if ($buyer_info['member_paypwd'] === md5($_POST['password'])) {
                output_data('1');
            }
        }
        output_error('支付密码验证失败');
    }

    /**
     * F码验证
     */
    public function check_fcodeOp() {
        $goods_id = intval($_POST['goods_id']);
        if ($goods_id <= 0) {
            output_error('商品ID格式不正确');
        }
        if ($_POST['fcode'] == '') {
            output_error('F码格式不正确');
        }
        $result = logic('buy')->checkFcode($goods_id, $_POST['fcode']);
        if ($result['state']) {
            output_data('1');
        } else {
            output_error('F码验证失败');
        }
    }
}
