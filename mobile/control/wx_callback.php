<?php
/**
 *
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/03/28
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class wx_callbackControl extends mobileHomeControl
{
    const TOKEN = 'yinuovip';
    private $FromUserName;
    private $ToUserName;
    private $CreateTime;
    private $Event;
    private $EventKey;
    private $Ticket;
    private $dis_code = 'aaa';
    private $open_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function indexOp()
    {
        if (!isset($_GET["echostr"])) {
            $this->responseMsg();
        } else {
            $this->valid();
        }
    }

    /**
     * 自定义菜单
     * */
    public function create_menuOp()
    {
        /** @var mb_paymentModel $model_mb_payment */
        $model_mb_payment = Model('mb_payment');
        $mb_payment_info = $model_mb_payment->getMbPaymentInfo(array('payment_id' => 3));
        $app_id = $mb_payment_info['payment_config']['appId'];
        $app_secret = $mb_payment_info['payment_config']['appSecret'];
        $inc_file = BASE_ROOT_PATH . '/mobile/api/payment/wxpay_jsapi/wx_jssdk.php';

        $post_data = '{
            "button": [
                {
                    "name": "艺诺在线", 
                    "sub_button": [
                        {
                            "type": "view", 
                            "name": "艺诺拍卖", 
                            "url": "http://www.yinuovip.com/wap/tmpl/auction/auction_index.html"
                        }, 
                        {
                            "type": "view", 
                            "name": "艺诺首页", 
                            "url": "http://www.yinuovip.com/wap/app_index.html"
                        }, 
                        {
                            "type": "view", 
                            "name": "尊享专区", 
                            "url": "http://www.yinuovip.com/wap/tmpl/store.html?store_id=84"
                        }, 
                        {
                            "type": "view", 
                            "name": "艺诺商城", 
                            "url": "http://www.yinuovip.com/wap/index.html"
                        }, 
                        {
                            "type": "view", 
                            "name": "趣猜有奖", 
                            "url": "http://www.yinuovip.com/wap/tmpl/qucai/qucai.html"
                        }
                    ]
                }, 
                {
                    "name": "艺诺艺术", 
                    "sub_button": [
                        {
                            "type": "view", 
                            "name": "最新艺讯", 
                            "url": "http://wap.yinuovip.com/artNews"
                        }, 
                        {
                            "type": "view", 
                            "name": "热门约展", 
                            "url": "http://wap.yinuovip.com/artExhibition"
                        }, 
                        {
                            "type": "view", 
                            "name": "艺家艺言", 
                            "url": "http://wap.yinuovip.com/home"
                        }, 
                        {
                            "name": "艺诺之星", 
                            "type": "view", 
                            "url": "http://www.yinuovip.com/wap/tmpl/auction/artist_list.html"
                        }
                    ]
                }, 
                {
                    "name": "我的服务", 
                    "sub_button": [
                        {
                            "name": "个人中心", 
                            "type":"view",
                            "url":"http://www.yinuovip.com/wap/tmpl/member/member.html"
                        },
                        {
                            "name": "app下载", 
                            "type":"view",
                            "url":"http://www.yinuovip.com/wap/tmpl/app_download.html"
                        },
                        {
                            "name": "联系客服", 
                            "type": "click",
                            "key": "ContactUs", 
                        },
                    ]
                }
            ]
        }';

        if (!is_file($inc_file)) {
            $send_result_data = [201];
        } else {
            require_once($inc_file);
            $sdkObj = new JSSDK($app_id, $app_secret);
            $send_result_data = $sdkObj->createMenu($post_data);
        }

        return $send_result_data;
    }

    private function _initializationData($object)
    {
        $jsonStr = json_encode($object);
        $postArray = json_decode($jsonStr, true);
        $this->FromUserName = $postArray['FromUserName'];
        $this->ToUserName = $postArray['ToUserName'];
        $this->CreateTime = $postArray['CreateTime'];
        $this->Event = $postArray['Event'];
        $this->EventKey = $postArray['EventKey'];
        $this->Ticket = $postArray['Ticket'];
        $this->open_id = $postArray['FromUserName'];
    }

    private function responseMsg()
    {
        //get post data, May be due to the different environments
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

        $resultStr = "";
        //extract post data
        if (!empty($postStr)) {
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $RX_TYPE = trim($postObj->MsgType);
            switch ($RX_TYPE) {
                case "text":
                    $resultStr = $this->handleText($postObj);
                    break;
                case "event":
                    self::_initializationData($postObj);
                    $resultStr = $this->handleEvent();
                    break;
                default:
                    break;
            }
            file_put_contents(BASE_DATA_PATH.'/log/wt.log',"---------------end--------------\r\n",FILE_APPEND);
            echo $resultStr;exit;
        } else {
            echo $resultStr;
            exit;
        }
    }

    private function handleText($postObj)
    {
        $fromUsername = $postObj->FromUserName;
        $toUsername = $postObj->ToUserName;
        $keyword = trim($postObj->Content);
        $time = time();
        file_put_contents(BASE_DATA_PATH.'/log/wt.log',"-----------------------------\r\n",FILE_APPEND);
        file_put_contents(BASE_DATA_PATH.'/log/wt.log',json_encode($postObj)."\r\n",FILE_APPEND);
        switch($keyword){
            case '转载':
                $msgType = 'news';
                $title = '艺诺艺术公众号 | 文章转载及合作须知';
                $pic = 'https://mmbiz.qpic.cn/mmbiz_gif/rICpwfU2Ye9Xzu1q6hI0GKS4NAicvgic9K7YUBfPibHdYdutUsPWlKtvXGSTibPUohqN8Ng48UnzibGMDf207DYAAGw/640?wx_fmt=gif&tp=webp&wxfrom=5&wx_lazy=1';
                $description = '我们希望一起努力创造一个原创友好的环境。';
                $url = 'https://mp.weixin.qq.com/s/AU9f6JhY0e8nnziZerDg5w';
                $textTpl = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[%s]]></MsgType><ArticleCount>1</ArticleCount><Articles><item><Title><![CDATA[%s]]></Title> <Description><![CDATA[%s]]></Description><PicUrl><![CDATA[%s]]></PicUrl><Url><![CDATA[%s]]></Url></item></Articles></xml>";
                $resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType,$title,$description,$pic,$url);
                break;
            default :
                $textTpl = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[%s]]></MsgType><Content><![CDATA[%s]]></Content><FuncFlag>0</FuncFlag></xml>";
                $msgType = "text";
                $contentStr = "欢迎关注艺诺网！";
                $resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                break;
        }
        file_put_contents(BASE_DATA_PATH.'/log/wt.log',$resultStr."\r\n",FILE_APPEND);
        return $resultStr;
//        if (!empty($keyword)) {
//            echo $resultStr;
//        } else {
//            echo "Input something...";
//        }
    }

    private function handleEvent()
    {
        $contentStr = "";
        switch ($this->Event) {
            case "subscribe":
                $this->subscribe();
                $url = WAP_SITE_URL . '/tmpl/member/register.html?invite_code=' . $this->dis_code;
                $contentStr = "您好，欢迎关注艺诺网！";
                $contentStr .= "\r\n    艺诺网打造全球最大艺术品保障交易平台";
                $contentStr .= "\r\n    艺诺网通过“保真品、保增值、保退换”的保障交易模式，让艺术品交易安全简易，让艺术品收藏者不再顾虑重重，而是安心收藏；让艺术家不再疲于奔波，而是安心创作！";
                $contentStr .= "\r\n<a href='{$url}'>★注册即送288元红包，新手福利注册享受，赶紧注册吧★</a>";
                break;
            case "unsubscribe":
                $this->subscribe();
                break;
            case "SCAN":
                /*已关注时的事件推送*/
                return '';
            case "CLICK":
                if ($this->EventKey == 'ContactUs') {
                    $contentStr = '艺诺400客服电话400-109-2866 工作时间：周一至周五（9：00-18：00）';
                }
                break;
            default :
//                $contentStr = "Unknow Event: " . $object->Event;
                break;
        }
        $resultStr = $this->responseText($contentStr);
        return $resultStr;
    }

    /**
     * 关注或取消操作
     * */
    private function subscribe()
    {
        /** @var extension_logModel $model_extension_log */
        $model_extension_log = Model('extension_log');

        $log_info = $model_extension_log->getOne(['open_id' => $this->open_id]);
        if (empty($log_info)) {
            $this->dis_code = substr($this->EventKey, '8');
        }else{
            $this->dis_code = $log_info['extension_code'];
        }

        $extension_info = Model('extension')->getOne(['extension_code' => $this->dis_code]);
        if (empty($extension_info)) return '';

        $save_data = ['modify_time' => TIMESTAMP];
        switch ($this->Event) {
            case 'subscribe':
                $subscribe_type = 1;
                if(empty($log_info)){
                    $save_data['total_follow'] = ['exp', 'total_follow+1'];
                    $save_data['month_follow'] = ['exp', 'month_follow+1'];
                }
                break;
            case 'unsubscribe':
                if(empty($log_info)) return '';
                $subscribe_type = 0;
                if(empty($log_info['first_unsubscribe_time'])){
                    $save_data['total_follow'] = ['exp', 'total_follow-1'];
                    $save_data['month_cancel'] = ['exp', 'month_cancel+1'];
                }
                break;
        }

        $Exception = [];
        try {
            $model_extension_log->beginTransaction();

            // 更新推广日志表
            $save_data1 = array(
                'subscribe_type' => $subscribe_type,
                'extension_code' => $this->dis_code,
                'open_id' => $this->open_id,
                $this->Event . '_time' => $this->CreateTime,
            );

            if ($log_info) {
                if (empty($log_info['first_unsubscribe_time'])) {
                    $save_data1['first_unsubscribe_time'] = TIMESTAMP;
                }
                $save_data1['modify_time'] = TIMESTAMP;
                $result = $model_extension_log->modify($save_data1, ['open_id' => $this->open_id]);
            } else {
                $save_data1['add_time'] = TIMESTAMP;
                $result = $model_extension_log->save($save_data1);
            }

            $Exception['shopnc_extension_log'] = $save_data1;
            if (!$result) {
                throw new Exception("推广日志表，保存失败。");
            }

            // 更新行为记录表
            $save_data2 = array(
                'open_id' => $this->open_id,
                'extension_code' => $this->dis_code,
                'record' => $this->Event,
                'add_time' => TIMESTAMP,
            );

            $Exception['shopnc_extension_log_act'] = $save_data2;
            $result = $model_extension_log->table('extension_log_act')->insert($save_data2);
            if (!$result) {
                throw new Exception("行为记录表，保存失败。");
            }

            if(isset($save_data['total_follow'])){
                // 更新推广表
                $times = strtotime(date('Y-m', TIMESTAMP));
                if (!empty($extension_info['modify_time']) && $times > $extension_info['modify_time']) {
                    $save_data['month_follow'] = 0;
                    $save_data['month_cancel'] = 0;
                }

                $Exception['shopnc_extension'] = $save_data;
                $result = $model_extension_log->table('extension')->where(['extension_code' => $this->dis_code])->update($save_data);
                if (!$result) {
                    throw new Exception("推广表，保存失败。");
                }
            }
            $model_extension_log->commit();
        } catch (Exception $e) {
            $model_extension_log->rollback();
            $string = $e->getMessage();

            $filename = BASE_DATA_PATH . DS . 'log/wx_event/' . date('Ymd', TIMESTAMP) . '.log';
            file_put_contents($filename, $string . "\r\n" . print_r($Exception, true), FILE_APPEND);
        }

        return true;
    }

    /**
     * 相应文本
     * */
    private function responseText($content, $flag = 0)
    {
        $textTpl = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[%s]]></Content><FuncFlag>%d</FuncFlag></xml>";
        $resultStr = sprintf($textTpl, $this->FromUserName, $this->ToUserName, time(), $content, $flag);
        return $resultStr;
    }

    /**
     * 验证签名是否有效
     * */
    private function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

        $token = self::TOKEN;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验
     * */
    private function valid()
    {
        $echoStr = $_GET["echostr"];
        if ($this->checkSignature()) {
            echo $echoStr;
            //$this->responseMsg();
            exit;
        }
    }

}