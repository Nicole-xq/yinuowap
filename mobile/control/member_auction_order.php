<?php
/**
 * 我的订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_auction_orderControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 订单列表
     */
    public function order_listOp() {
        $type = $_GET['state_type'];
        $member_id = $this->member_info['member_id'];
        $logic_auction_order = Logic('auction_order');
        if ($member_id == 0) {
            output_error('会员不存在');
        }
        $margin_state = array();
        if($_GET['p_type'] == 'auction'){
            $margin_state = array(0,1,2,3,5);
        }
        $is_have = 0;
        // 获取订单列表
        switch ($type) {
            case 'list_all'://所有保证金订单
                $result = $logic_auction_order->getMemberAuctionList($member_id, '', '','');
                break;
            case 'list_ing'://参拍
                $result = $logic_auction_order->getMemberAuctionList($member_id, '', '','',array(1,3));
//            dd();
                break;
            case 'list_have'://已拍下
                $result = $logic_auction_order->getMemberAuctionList($member_id, 0, 1,null,$margin_state);
                $is_have = 1;
                break;
            case 'list_die'://未拍中
                $result = $logic_auction_order->getMemberAuctionList($member_id, 1, ['neq',1],null,$margin_state);
                break;
        }
        $page_count = $result['totalpage'];
        output_data(array('order_group_list' => $result['margin_order_list'], 'is_have' => $is_have), mobile_page($page_count));
    }

    /*
    * 取消保证金订单
    * */
    public function order_cancelOp()
    {
        $margin_id = $_POST['order_id'];
        $model_margin_orders = Model('margin_orders');
        $logic_auction_order = Logic('auction_order');
        //获取订单详细
        $condition = array();
        $condition['margin_id'] = $margin_id;
        $order_info = $model_margin_orders->getOrderInfo($condition);
        // 取消订单
        $result =  $logic_auction_order->changeOrderStateCancel($order_info,'admin', $this->admin_info['name'],'',true);
//        $order_id = $_POST['order_id'];
//
//        $model_auction_order = Model('auction_order');
//        $order_info = $model_auction_order->getInfo(array('auction_order_id' => $order_id));
//
//        $logic_auction_order = Logic('auction_order');
//        // 会员中心修改尾款订单状态
//        $result = $logic_auction_order->MemberChangeOrderState($order_info, $_POST, 0);
//        if (TIMESTAMP - ORDER_CANCEL_TIMES < $order_info['api_pay_time'] && $order_info['payment_code'] != "underline") {
//            $_hour = ceil(($order_info['api_pay_time']+ORDER_CANCEL_TIMES-TIMESTAMP)/3600);
//            return output_error('该订单曾尝试使用第三方支付平台支付，须在'.$_hour.'小时以后才可申请退款');
//        }
        if (!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 订单确认收货
     */
    public function order_receiveOp() {
        $order_id = $_POST['order_id'];

        $model_order = Model('order');
        $order_info = $model_order->getOrderInfo(array('order_id' => $order_id));
        $logic_auction_order = Logic('auction_order');
        // 会员中心修改尾款订单状态
        $result = $logic_auction_order->MemberChangeOrderState($order_info, $_POST, 1);
        if (!$result['state']) {
            output_error($result['msg']);
        }

        // 添加会员积分，经验，会员返佣
        $result = $logic_auction_order->changeOrderStateReceive($order_info, 'buyer', $_SESSION['member_name'], '签收了货物');

        if(!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 物流跟踪
     */
    public function search_deliverOp(){
        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['order_id']);
        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            output_error($result['msg']);
        }
        $express = rkcache('express',true);
        $e_code = $express[$result['data']['order_info']['express_id']]['e_code'];

        $deliver_info = $this->_get_express($e_code, $result['data']['order_info']['invoice_no']);

        output_data(array('express_name' => $result['data']['order_info']['express_info']['e_name'], 'shipping_code' => $result['data']['order_info']['invoice_no'], 'deliver_info' => $deliver_info));
    }

    /**
     * 取得当前的物流最新信息
     */
    public function get_current_deliverOp(){
        $auction_order_id   = intval($_GET['order_id']);
        $margin_id = intval($_GET['margin_id']);
        if ($auction_order_id <= 0 || $margin_id <= 0) {
            output_error('订单不存在');
        }

        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        $order_info = $result['data']['order_info'];

        if (empty($order_info) || !in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS))) {
            output_error('订单不存在');
        }
    
        $express = rkcache('express',true);
        $e_code = $express[$order_info['express_id']]['e_code'];
        $e_name = $express[$order_info['express_id']]['e_name'];

        $content = Model('express')->get_express($e_code, $order_info['invoice_no']);
        if (empty($content)) {
            output_error('物流信息查询失败');
        } else {
            foreach ($content as $k=>$v) {
                if ($v['time'] == '') continue;
                output_data(array('deliver_info'=>$content[0]));
            }
            output_error('物流信息查询失败');
        }
    }

    /**
     * 从第三方取快递信息
     *
     */
    public function _get_express($e_code, $shipping_code){

        /** @var expressModel $express */
        $express = Model('express');
        $content = $express->get_express($e_code, $shipping_code);
        if (empty($content)) {
            output_error('物流信息查询失败');
        }
        $output = array();
        foreach ($content as $k=>$v) {
            if ($v['time'] == '') continue;
            $output[]= $v['time'].'&nbsp;&nbsp;'.$v['context'];
        }

        return $output;
    }

    /*
     * 订单详情
     * */
    public function order_infoOp()
    {
        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['auction_order_id']);
        // 获取订单详情
        /** @var auction_orderLogic $login_auction_order */
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            showDialog($result['msg'], '', 'error', '', 5);
        }
        $order_info = $result['data']['order_info'];
        $order_info['state_desc'] = orderState($order_info);

        $_tmp = $order_info['invoice_info'];
        $_invonce = '';
        if (is_array($_tmp) && count($_tmp) > 0) {
            foreach ($_tmp as $_k => $_v) {
                $_invonce .= $_k.'：'.$_v.' ';
            }
        }
        $order_info['invoice'] = rtrim($_invonce);
        if (empty($order_info['payment_time'])) {
            $order_info['payment_time'] = 0;
        }
        if (empty($order_info['ship_time'])) {
            $order_info['ship_time'] = 0;
        }
        if (empty($order_info['finnshed_time'])) {
            $order_info['finnshed_time'] = 0;
        }
        $ownShopIds = Model('store')->getOwnShopIds();
        $data['ownshop'] = in_array($order_info['store_id'], $ownShopIds);

        output_data(array('order_info'=>$order_info));
    }
}
