<?php
/**
 * 我的资金相关信息
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_fundControl extends mobileMemberControl {
    public function __construct(){
        parent::__construct();
    }
    /**
     * 预存款日志列表
     */
    public function predepositlogOp(){
        /** @var predepositModel $model_predeposit */
        $model_predeposit = Model('predeposit');
        $where = array();
        $where['lg_member_id'] = $this->member_info['member_id'];
        $where['lg_av_amount'] = array('neq',0);
        $list = $model_predeposit->getPdLogList($where, $this->page, '*', 'lg_id desc');
        $page_count = $model_predeposit->gettotalpage();
        if ($list) {
            foreach($list as $k=>$v){
                $v['lg_add_time_text'] = @date('Y-m-d H:i:s',$v['lg_add_time']);
                $list[$k] = $v;
            }
        }
        output_data(array('list' => $list), mobile_page($page_count));
    }

    public function predepositDetailOp()
    {
        $lgId = $_GET['lgId'];
        if (empty($lgId)) {
            \App\Exceptions\ApiResponseException::throwFailMsg('日志详情主键不能为空!');
        }
        /** @var predepositModel $model_predeposit */
        $model_predeposit = Model('predeposit');
        $where = array();
        $where['lg_member_id'] = $this->member_info['member_id'];
        $where['lg_av_amount'] = array('neq',0);
        $where['lg_id'] = $lgId;
        $pdLogDetail = $model_predeposit->getPdLogDetail($where);
        $pdLogDetail['lg_add_time_text'] = @date('Y-m-d H:i:s', $pdLogDetail['lg_add_time']);
        $orderSn = preg_replace('/[^\d]*/', '', $pdLogDetail['lg_desc']);
        $pdLogDetail['orderSn'] = $orderSn;
        $pdLogDetail['payment_name'] = '余额';
        if ($orderSn) {
            switch ($pdLogDetail['lg_type'])
            {
                case 'recharge':
                    $paymentInfo = Model('')->table('pd_recharge')->where(['pdr_sn' => $orderSn])->find();
                    if ($paymentInfo) {
                        $pdLogDetail['payment_name'] = $paymentInfo['pdr_payment_name'];
                    }
                break;
                default:
                    break;
            }
        }
        $lgType = explode("，", $pdLogDetail['lg_desc']);
        if ($lgType) {
            $pdLogDetail['lg_type'] = $lgType[0];
        }
        unset($pdLogDetail['lg_admin_name'], $pdLogDetail['lg_source'], $pdLogDetail['lg_add_time']);
        unset($pdLogDetail['lg_freeze_amount'], $pdLogDetail['lg_edit_time']);
        output_data($pdLogDetail);
    }
    /**
     * 充值卡余额变更日志
     */
    public function rcblogOp()
    {
        $model_rcb_log = Model('rcb_log');
        $where = array();
        $where['member_id'] = $this->member_info['member_id'];
        $where['available_amount'] = array('neq',0);
        $log_list = $model_rcb_log->getRechargeCardBalanceLogList($where, $this->page, '', 'id desc');
        $page_count = $model_rcb_log->gettotalpage();
        if ($log_list) {
            foreach($log_list as $k=>$v){
                $v['add_time_text'] = @date('Y-m-d H:i:s',$v['add_time']);
                $log_list[$k] = $v;
            }
        }
        output_data(array('log_list' => $log_list), mobile_page($page_count));
    }
    /**
     * 充值明细
     */
    public function pdrechargelistOp(){
        $where = array();
        $where['pdr_member_id'] = $this->member_info['member_id'];
        $model_pd = Model('predeposit');
        $list = $model_pd->getPdRechargeList($where, $this->page,'*','pdr_id desc');
        $page_count = $model_pd->gettotalpage();
        if ($list) {
            foreach($list as $k=>$v){
                $v['pdr_add_time_text'] = @date('Y-m-d H:i:s',$v['pdr_add_time']);
                $v['pdr_payment_state_text'] = $v['pdr_payment_state']==1?'已支付':'未支付';
                $list[$k] = $v;
            }
        }
        output_data(array('list' => $list), mobile_page($page_count));
    }
    /**
     * 提现记录
     */
    public function pdcashlistOp(){
        $where = array();
        $where['pdc_member_id'] =  $this->member_info['member_id'];
        $model_pd = Model('predeposit');
        $list = $model_pd->getPdCashList($where, $this->page, '*', 'pdc_id desc');
        $page_count = $model_pd->gettotalpage();
        if ($list) {
            foreach($list as $k=>$v){
                $v['pdc_add_time_text'] = @date('Y-m-d H:i:s',$v['pdc_add_time']);
                $v['pdc_payment_time_text'] = @date('Y-m-d H:i:s',$v['pdc_payment_time']);
                $v['pdc_payment_state_text'] = $v['pdc_payment_state']==1?'已支付':'未支付';
                $list[$k] = $v;
            }
        }
        output_data(array('list' => $list), mobile_page($page_count));
    }
    /**
     * 充值卡充值
     */
    public function rechargecard_addOp()
    {
        $param = $_POST;
        $rc_sn = trim($param["rc_sn"]);
        if (!$rc_sn) {
            output_error('请输入平台充值卡号');
        }
        if (!Model('apiseccode')->checkApiSeccode($param["codekey"],$param['captcha'])) {
            output_error('验证码错误');
        }
        try {
            Model('predeposit')->addRechargeCard($rc_sn, array('member_id'=>$this->member_info['member_id'],'member_name'=>$this->member_info['member_name']));
            output_data('1');
        } catch (Exception $e) {
            output_error($e->getMessage());
        }
    }
    /**
     * 预存款提现记录详细
     */
    public function pdcashinfoOp(){
        $param = $_GET;
        $pdc_id = intval($param["pdc_id"]);
        if ($pdc_id <= 0){
            output_error('参数错误');
        }
        $where = array();
        $where['pdc_member_id'] =  $this->member_info['member_id'];
        $where['pdc_id'] = $pdc_id;
        $info = Model('predeposit')->getPdCashInfo($where);
        if (!$info){
            output_error('参数错误');
        }
        $info['pdc_add_time_text'] = $info['pdc_add_time']?@date('Y-m-d H:i:s',$info['pdc_add_time']):'';
        $info['pdc_payment_time_text'] = $info['pdc_payment_time']?@date('Y-m-d H:i:s',$info['pdc_payment_time']):'';
        $info['pdc_payment_state_text'] = $info['pdc_payment_state']==1?'已支付':'未支付';
        output_data(array('info' => $info));
    }

    /*
     * 保证金日志列表
     * */
    public function margin_log_listOp()
    {
        $model_margin_log = Model('margin_log');
        $log_list = $model_margin_log->getLogList(array('member_id' => $this->member_info['member_id']), 10);
        foreach ($log_list as $key => $log) {
            $log_list[$key]['add_time'] = @date('Y-m-d H:i:s',$log['add_time']);
        }
        $page_count = $model_margin_log->gettotalpage();
        output_data(
            array('list' => $log_list),
            mobile_page($page_count)
        );
    }

    /*
     * 保证金收益列表
     * */
    public function get_cumulative_margin_listOp()
    {
        $model_predeposit = Model('predeposit');
        $pd_log_list = $model_predeposit->get_cumulative_margin_list($this->member_info['member_id']);
        foreach ($pd_log_list as $key => $log) {
            $pd_log_list[$key]['lg_add_time'] = @date('Y-m-d H:i:s',$log['lg_add_time']);
        }
        $page_count = $model_predeposit->gettotalpage();
        output_data(
            array('list' => $pd_log_list),
            mobile_page($page_count)
        );
    }

    /**
     * 佣金日志列表
     */
    public function commissionlogOp(){
        $model_commission = Model('member_commission');
        $where = array();
        $where['dis_member_id'] = $this->member_info['member_id'];
        $where['dis_commis_state'] = 1;
        if($_REQUEST['form_member_id']){
            $where['commission_amount'] = array('gt',0);
            $where['from_member_id'] = $_REQUEST['form_member_id'];
        }
        $list = $model_commission->getCommissionList($where, '*',$this->page, 'log_id desc');
        $page_count = $model_commission->gettotalpage();
        if ($list) {
            foreach($list as $k=>$v){
                $v['lg_add_time_text'] = @date('Y-m-d H:i:s',$v['commission_time']);
                $v['lg_desc'] = '获得来自'.$v['from_member_name'].'的返佣金额';
                $v['commission_type_name'] = $this->commission_type($v['commission_type']);
                $list[$k] = $v;
            }
        }
        output_data(array('list' => $list), mobile_page($page_count));
    }

    //数据处理
    private function commission_type($commission_type){
        switch ($commission_type){
            case 1:
                $type_name = '商品返佣';
                break;
            case 2:
                $type_name = '拍卖';
                break;
            case 3:
                $type_name = '保证金返佣';
                break;
            case 4:
                $type_name = '合伙人升级';
                break;
            case 5:
                $type_name = '艺术家入驻';
                break;
            case 6:
                $type_name = '保证金收益利息';
                break;
            default:
                $type_name = '';
                break;
        }
        return $type_name;
    }

    /**
     * 获取下级会员交易返佣
     */
    public function order_commissionOp(){
        $data = array();
        $form_member_id = $_REQUEST['form_member_id'];
        $type = $_REQUEST['type'];
        if(empty($form_member_id) && empty($type)){
            goto END;
        }

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        $where['dis_member_id'] = $this->member_info['member_id'];
        if($type != 1){
            $where['from_member_id'] = $form_member_id;
        }
        $this->page = 100;
        $list = $model_commission->getByTypeCommission(1, $where, '*',$this->page, 'log_id desc');

        if(empty($list)){
            goto  END;
        }

        //获取订单信息
        $order_id = array();
        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $order_id[$value['order_id']] = $value['order_id'];
            $value['label_status'] = $model_commission->status_setting[$value['dis_commis_state']]['label'];
        }

        /** @var orderModel $model_order */
        $model_order = Model('order');
        $data['orders_list'] = $model_order->getCommissionOrderList($order_id);

        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(1, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        $data['sum_commission_amount'] = $sum_commission_amount;
        $sum_commission_amount_1 = $model_commission->getMemberCommissionCount(1, $where['dis_member_id'], $form_member_id,array('dis_commis_state'=>0));
        $sum_commission_amount_1 = empty($sum_commission_amount_1) ? 0 : $sum_commission_amount_1;
        $data['sum_commission_amount_1'] = $sum_commission_amount_1;


        END:
        $data['commission_list'] = $list;
        output_data($data);

    }


    /**
     * 获取下级会员保证金返佣
     */
    public function order_marginOp(){
        $data = array();
        $form_member_id = $_REQUEST['form_member_id'];
        $type = $_REQUEST['type'];
        if(empty($form_member_id) && empty($type)){
            goto END;
        }

        $list = Logic('j_commission_show')->get_form_margin_commission($this->member_info['member_id'], $form_member_id, 100);

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        if($type != 1){
            $where['from_member_id'] = $form_member_id;
        }
        $where['dis_member_id'] = $this->member_info['member_id'];
        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(2, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        $data['sum_commission_amount'] = $sum_commission_amount;

        END:
        $data['list'] = $list;

        output_data($data);

    }

    /**
     * 获取下级会员推荐返佣列表
     */
    public function member_joinOp(){
        $data = array();
        $form_member_id = $_REQUEST['form_member_id'];
        $type = $_REQUEST['type'];

        if(empty($form_member_id)&&empty($type)){
            goto END;
        }

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $where = array();
        if($type != 1){
            $where['from_member_id'] = $form_member_id;
        }
        $where['dis_member_id'] = $this->member_info['member_id'];
        $this->page = 100;
        $list = $model_commission->getByTypeCommission(3, $where, 'log_id,goods_name,commission_amount,add_time,dis_commis_rate,dis_commis_state',$this->page, 'log_id desc');

        if(empty($list)){
            goto  END;
        }

        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $value['label_status'] = $model_commission->status_setting[$value['dis_commis_state']]['label'];
        }

        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount(3, $where['dis_member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        $data['sum_commission_amount'] = $sum_commission_amount;

        END:
        $data['list'] = $list;

        output_data($data);
    }

    /**
     * 获取下级会员返佣总金额
     */
    public function getMemberCommissionCountOp(){
        $data = array();
        $form_member_id = $_REQUEST['form_member_id'];

//        if(empty($form_member_id)){
//            goto END;
//        }

        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        //查询历史累计金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        $data['sum_commission_amount'] = number_format($sum_commission_amount, 2);
        $sum_commission_amount_1 = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $form_member_id,array('top_lv'=>1));
        $sum_commission_amount_1 = empty($sum_commission_amount_1) ? 0 : $sum_commission_amount_1;
        $data['sum_commission_amount_1'] = number_format($sum_commission_amount_1, 2);
        $sum_commission_amount_2 = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $form_member_id,array('top_lv'=>2));
        $sum_commission_amount_2 = empty($sum_commission_amount_2) ? 0 : $sum_commission_amount_2;
        $data['sum_commission_amount_2'] = number_format($sum_commission_amount_2, 2);
//        END:
        output_data($data);
    }

    /**
     * 根据时间和类型查询奖励金明细commission_type
     */
    public function getMemberCommissionListByTypeOp(){
        $start_time = $_GET['start_time'];
        $type = $_GET['type'];
        $member_id = $this->member_info['member_id'];
        //消费金额、保证金收益、推荐会员、艺术家推荐、竞拍奖励、竞拍成交
        $condition = array(
            'dis_member_id'=>$this->member_info['member_id'],
            'dis_commis_state'=>1,
            'commission_type'=>array('neq','6')
        );
        if (!empty($auction_id)) {
            $condition['order_goods_id'] = $auction_id;
            $condition['commission_type'] = $type;
            /** @var auctionsModel $auction_model */
            $auction_model = Model('auctions');
            $auction_info = $auction_model->getAuctionsInfoByID($auction_id);
        }
        if($type != 0){
            $condition['commission_type'] = $type == 1?array('in','1,2'):$type;
            $condition['commission_type'] = $type == 3 ? array('in','3,9') : $type;
        }
        if($start_time) {
            $date_info = get_the_month($start_time);
            list($start_time, $end_time) = $date_info;
            if (!empty($start_time) && !empty($end_time)) {
                $condition['commission_time'] = array(
                    'between',
                    strtotime($start_time . ' 00:00:00')."," . strtotime($end_time . ' 23:59:59'));
            }
        }
        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $list = $model_commission->getCommissionList($condition,'*',$this->page,'commission_time desc');
        if(!empty($list)){
            foreach($list as &$v){
                $v['commission_time_str'] = date('Ym',$v['commission_time']);
                //TODO 默认头像处理
                $v['from_member_avatar'] = getMemberAvatarForID($v['from_member_id']) ?: '默认头像';
                $v['commission_date'] = date('m月d日 H:i:s',$v['commission_time']);
                if ($member_id == $v['from_member_id']) {
                    $v['level'] = 0;
                } else {
                    $model_member_distribute = Model('member_distribute');
                    $condition = ['member_id' => $v['from_member_id']];
                    $distribute_info = $model_member_distribute->where($condition)->find();
                    if ($distribute_info['top_member'] == $this->member_info['member_id']) {
                        $v['level'] = 1;
                    } else {
                        $v['level'] = 2;
                    }
                }
            }
        }
        if (!empty($auction_id)) {
            $data['auction_id'] = $auction_id;
            $data['auction_name'] = isset($auction_info['auction_name']) ? $auction_info['auction_name'] : '拍品名称';
        }
        $page_count = $model_commission->gettotalpage();
        $data = array(
            'commission_list'=>$list,
            'page'=>mobile_page($page_count)
        );
        output_data($data);
//        if(empty($list)){
//            goto  END;
//        }

        //获取订单信息
        $order_id = array();
        foreach($list as &$value){
            $value['label_date'] = date('Y.m.d', $value['add_time']);
            $order_id[$value['order_id']] = $value['order_id'];
            $value['label_status'] = $model_commission->status_setting[$value['dis_commis_state']]['label'];
        }
    }

    /**
     * 获取分销返佣详情
     */
    public function getMemberCommissionDetailOp(){
        $log_id = intval($_REQUEST['log_id']);
        if(empty($log_id)){
           output_error('参数有误');
        }
        $condition = ['log_id'=>$log_id];
        $field = 'goods_name,goods_pay_amount,commission_amount,goods_image,order_sn,commission_type,add_time,commission_time,from_member_id';
        $commission_info = Model('member_commission')->getCommissionInfo($condition, $field);
        switch ($commission_info['commission_type']){
            case 1://交易
                $commission_info['goods_image'] = cthumb($commission_info['goods_image'], 360);
                break;
            case 3://保证金
                $margin_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$commission_info['order_sn']],'margin_id,auction_image');
                $commission_info['goods_image'] = cthumb($margin_info['auction_image'],360);
                break;
            case 4://合伙人
            case 5://艺术家
                $from_member_info = Model('member')->getMemberInfo(['member_id'=>$commission_info['from_member_id']], 'member_name,member_mobile,member_type,member_avatar,member_time');
                $from_member_info['member_time'] = date('Y-m-d',$from_member_info['member_time']);
                $from_member_info['member_mobile'] = $from_member_info['member_mobile'] ? tellHide($from_member_info['member_mobile']) : '';
                $from_member_info['type_logo'] = getMemberVendueForID($from_member_info['member_type']);
                if(substr($from_member_info['member_avatar'], 0, 4) == 'http'){
                    $from_member_info['member_avatar'] = $from_member_info['member_avatar'];
                } else {
                    $from_member_info['member_avatar'] = getMemberAvatarForID($commission_info['from_member_id']);
                }
                $commission_info = array_merge($commission_info, $from_member_info);
                break;
            default :

        }
        $commission_info['add_time'] = date("Y-m-d",$commission_info['add_time']);
        $commission_info['commission_time'] = date("Y-m-d",$commission_info['commission_time']);

        output_data($commission_info);
    }

}