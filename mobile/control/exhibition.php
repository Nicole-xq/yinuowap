<?php
/**
 * 会销活动
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/02/26
 * Time: 14:21
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class exhibitionControl extends mobileHomeControl {
    public function __construct(){
        parent::__construct();
    }

    public function indexOp(){
        $this->get_exhibition_listOp();
    }

    /**
     * 会销活动列表
     * */
    public function get_exhibition_listOp(){
        $member_id = $this->getMemberIdIfExists();
        if(empty($member_id)){
            output_error('请先登录');
        }

        $model_exhibition = Model('exhibition_setting');
        $list = $model_exhibition->getList(['state'=>2],$this->page,'add_time desc,id desc','id,add_time,title');
        $page_count = $model_exhibition->gettotalpage();
        if ($list) {
            foreach($list as $k=>$v){
                $v['add_time'] = @date('Y-m-d H:i',$v['add_time']);
                $list[$k] = $v;
            }
        }
        $invite_code = Model('member')->getUpperMember(array('member_id'=>$member_id),'dis_code');

        output_data(array('exhibition_list' => $list, 'invite_code'=>$invite_code['dis_code'], 'page'=>mobile_page($page_count)));
    }

    /**
     * 会销活动详情
     * */
    public function get_exhibition_contentOp(){
        $id = $_GET['ex_id'];
        if (empty($id)) {
            output_error('请求过程中，参数跑丢了');
        }
        $info = Model('exhibition_setting')->getOne(array('id' => $id));
        $info['special_mobile_content'] = html_entity_decode($info['content']);
        $info['special_content'] = html_entity_decode($info['content']);
        $info['out_date_flag'] = $info['entry_end_time'] > time() ? true : false;
        $info['entry_end_time'] = $info['entry_end_time'] ? date('y-m-d', $info['entry_end_time']) : '';
        $info['special_share_logo'] = '/data/upload/cms/special/' . $info['share_logo'];

        $ex_inviter = ['name' => '艺诺客服', 'mobile' => '400-135-2688', 'wx_qrcode_ticket' => false];
        $dis_code = $_GET['invite_code'];
        if (!empty($dis_code)) {
            /** @var memberModel $model_member */
            $model_member = Model('member');
            $condition = array('dis_code' => $dis_code);
            $field = 'member.member_real_name,member.member_using_mobile,member_distribute.wx_qrcode_ticket';
            $inviter_data = $model_member->getMemberDistributeWithMemberList($condition, $field, '', '', 1);
            if(!empty($inviter_data)){
                $inviter_info = $inviter_data[0];

                if (empty($inviter_info['wx_qrcode_url']) || (!empty($inviter_info['qr_expire_seconds']) && $inviter_info['qr_expire_seconds'] < TIMESTAMP)) {
                    /** @var wx_qrcode_createLogic $logic_wx_qrcode */
                    $logic_wx_qrcode = Logic('wx_qrcode_create');
                    $data = $logic_wx_qrcode->getWxQrcode($dis_code);
                    $ticket_info = json_decode($data[1], true);
                    $save_data = array(
                        'wx_qrcode_ticket' => $ticket_info['ticket'],
                        'wx_qrcode_url' => $ticket_info['url'],
                        'qr_expire_seconds' => TIMESTAMP + 2592000,
                    );
                    $model_member->editMemberDistribute($save_data, $condition);

                    $inviter_info['wx_qrcode_ticket'] = $ticket_info['ticket'];
                }

                $ex_inviter = [
                    'name' => $inviter_info['member_real_name'],
                    'mobile' => $inviter_info['member_using_mobile'],
                    'wx_qrcode_ticket' => $inviter_info['wx_qrcode_ticket'],
                ];
            }
        }

        output_data(array('detail' => $info, 'ex_inviter' => $ex_inviter));
    }

    /**
     * 会销活动报名
     * */
    public function exhibition_entry_addOp(){
        $checked = $this->_check();
        if($checked['code']){
            output_error($checked['msg']);
        }
        $add_data = array();
        $add_data['ex_id'] = $_POST['ex_id'];
        $add_data['member_real_name'] = $_POST['ex_name'];
        $add_data['member_using_mobile'] = $_POST['ex_mobile'];
        $add_data['content'] = $_POST['content'];
        $add_data['add_time'] = TIMESTAMP;
        $add_data['modify_time'] = TIMESTAMP;

        $member_id = $this->getMemberIdIfExists();
        if($member_id > 0){
            $add_data['member_id'] = $member_id;
        }

        $invite_code = !empty($_POST['invite_code']) ? $_POST['invite_code'] : (!empty($_GET['invite_code']) ? $_GET['invite_code'] : '');
        if($invite_code){
            $inviter_info = Model('member')->getMemberDistributeWithMemberList(array('dis_code'=>$invite_code));

            if($inviter_info && $member_id != $inviter_info[0]['member_id']){
                $add_data['member_top'] = $inviter_info[0]['member_id'];
                $add_data['member_top_name'] = $inviter_info[0]['member_real_name'];
                $add_data['member_top_mobile'] = $inviter_info[0]['member_using_mobile'];
            }
        }

        $result = Model('exhibition_setting')->table('exhibition_entry')->insert($add_data);

        if($result){
            output_data('报名成功');
        }
        output_error('报名失败，系统错误');

    }

    /**
     * 验证是否报名
     * */
    public function get_check_entryOp(){
        $checked = $this->_check(true);
        if($checked['code']){
            output_data($checked['msg']);
        }
        output_error($checked['msg']);
    }

    private function _check($is_member_id = false){
        $ex_id = $_REQUEST['ex_id'];
        if(empty($ex_id)){
            return ['msg'=>'参数错误','code'=>1];
        }

        $model_exhibition = Model('exhibition_entry');

        $member_id = $this->getMemberIdIfExists();
        if($member_id > 0){
            $condition = ['member_id' => $member_id, 'ex_id' => $ex_id];
            $result = $model_exhibition->isExist($condition);
            if($result){
                return ['msg'=>'您已报名成功','code'=>1];
            }
        }
        //仅验证用户ID
        if($is_member_id){
            return ['msg'=>'你未参加报名','code'=>0];
        }

        $ex_name = $_REQUEST['ex_name'];
        $ex_mobile = $_REQUEST['ex_mobile'];
        if(empty($_POST['ex_name'])){
            return ['msg'=>'请填写名字','code'=>1];
        }
        if(empty($_POST['ex_mobile'])){
            return ['msg'=>'请填写手机号','code'=>1];
        }

        $condition = array('ex_id' => $ex_id, 'member_real_name' => $ex_name, 'member_using_mobile' => $ex_mobile,);
        $result = $model_exhibition->isExist($condition);
        if($result){
            return ['msg'=>'您已报名成功','code'=>1];
        }
        return ['msg'=>'你未参加报名','code'=>0];
    }

    /**
     * 邀请报名会员列表
     */
    public function exhibition_member_listOp() {
        $member_id = $this->getMemberIdIfExists();
        if(empty($member_id)){
            output_error('请先登录');
        }
        $ex_id = intval($_GET['ex_id']);
        $model_exhibition = Model('exhibition_entry');
        $condition = [
            'member_top'=>$member_id,
            'ex_id'=>$ex_id
        ];
        $result = $model_exhibition->getList($condition, 0, $order='', $field='*', $limit='');
        foreach($result as &$value){
            $value['add_time'] = date('Y-m-d',$value['add_time']);
        }

        output_data(['exhibition_list'=>$result]);
    }

}