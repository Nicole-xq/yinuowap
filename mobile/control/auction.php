<?php
/**
 * 拍卖
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class auctionControl extends mobileHomeControl
{
	public function __construct()
	{
		parent::__construct();
	}

	public function indexOp(){
		$special_id = $_GET['special_id'];
		$model_mb_class = Model('mb_class');
		$model_auctions = Model('auctions');
		$model_guess = Model('p_guess');
		$list1 = $model_mb_class->getMbSpecialItemUsableListByID($special_id);
		$a = array();
		$b= 0;
		foreach($list1 as $key => $val){
			if($val['special']){
				if($b == 0){
					$k = $key;
				}
				unset($key);
				if(!in_array($val['special'],$a)){
					$a[] = $val['special'];
					$list[$k]['special']['special'][] = $val['special'];
				}
				else{
					$list[$k]['special']['special'][] = array_merge(array($a),array($val['special']));
				}
				$b += 1;
				if(!empty($list[$k]['special']['special'])){
					foreach($list[$k]['special']['special'] as $kk=>$vv){
						foreach($vv['item'] as $k1=>$v1){
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_end'] = $v1['special_end_time'] - TIMESTAMP >0 ?$v1['special_end_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_rate'] = $v1['special_rate_time'] - TIMESTAMP >0 ?$v1['special_rate_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_remain_time_start'] = $v1['special_start_time'] - TIMESTAMP >0 ?$v1['special_start_time'] - TIMESTAMP : 0;
                            $list[$k]['special']['special'][$kk]['item'][$k1]['special_start_date'] = date('m月d日 H:i',$v1['special_start_time']);
							$list[$k]['special']['special'][$kk]['item'][$k1]['special_end_date'] = date('m月d日 H:i',$v1['special_end_time']);
							$list[$k]['special']['special'][$kk]['item'][$k1]['special_end_time'] = $v1['special_end_time'];
							if(time() >= $v1['special_start_time'] && $v1['special_end_time']> time()) {
								$list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 1;
							}elseif($v1['special_end_time']<= time() || $v1['special_preview_start'] > time()){
								$list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 2;
                                unset($list[$k]['special']['special'][$kk]['item'][$k1]);
                                continue;
							}elseif(($v1['special_preview_start'] < time()) && ($v1['special_start_time'] > time()) && ($v1['special_rate']>0 && $v1['special_rate_time']>time())){//保证金年收益率不为零时，筛选出最后计息时间点到拍卖开始的专场进行展示
                                unset($list[$k]['special']['special'][$kk]['item'][$k1]);
                                continue;
                            }elseif($v1['special_start_time'] > time()){
								$list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 0;
							}
							if($v1['special_rate_time'] > time()){
                                $list[$k]['special']['special'][$kk]['item'][$k1]['is_kaipai'] = 3;
                            }
                            $list[$k]['special']['special'][$kk]['item'][$k1]['day_num'] = Logic('auction')->getInterestDay($v1['special_rate_time'],$v1['special_end_time']);//计息天数
                            $v1['store_label'] = Model("store")->getStoreDetail(['store_id'=>$v1['store_id']],'store_label','store_label');
                            $list[$k]['special']['special'][$kk]['item'][$k1]['store_logo'] = getStoreLogo($v1['store_label'],'store_logo');
                            $auction_list = $model_auctions->getAuctionList(['special_id'=>$v1['special_id']]);
                            $all_bid_number = 0;
							$jian_num = 0;
                            $all_price = 0;
                            $special_click = 0;
							if(!empty($auction_list)){
								foreach($auction_list as $id=>$auction_info){
									$all_bid_number += $auction_info['bid_number'];
                                    $special_click += $auction_info['auction_click'];
									if($auction_info['is_liupai'] == 0 && $auction_info['auction_end_true_t'] <=time()){
										$jian_num += 1;
                                        $all_price += $auction_info['current_price'];
									}
								}
							}

							$list[$k]['special']['special'][$kk]['item'][$k1]['all_bid_number'] = $all_bid_number;
							$list[$k]['special']['special'][$kk]['item'][$k1]['special_click'] = $special_click;
							$list[$k]['special']['special'][$kk]['item'][$k1]['jian_num'] = $jian_num;
							$list[$k]['special']['special'][$kk]['item'][$k1]['all_price'] = $all_price;
						}
					}
				}

			}
			if($val['qucai']){
				$list[$key]['qucai'] = $list1[$key]['qucai'];
				foreach($list[$key]['qucai']['item'] as $k=>$v){
					$qucai_info = $model_guess->getOpenGuessInfo(array('gs_id'=>$v['gs_id']));
					$list[$key]['qucai']['item'][$k]['joined_times'] = $qucai_info['joined_times'];
					$list[$key]['qucai']['item'][$k]['state'] = $qucai_info['gs_state'];

				}
			}
			if($val['home5']){
				$list[$key]['home5'] = $list1[$key]['home5'];
			}
			if($val['home6']){
				$list[$key]['home6'] = $list1[$key]['home6'];
			}
			if($val['sem']){
				$list[$key]['sem'] = $list1[$key]['sem'];
			}

			if($val['auctions']){
                $list[$key]['auctions']['title'] = $list1[$key]['auctions']['title'];
                $break = 0;
                $limit = $list1[$key]['auctions']['limit'] > 0 ? $list1[$key]['auctions']['limit'] : 4;
                foreach($list1[$key]['auctions']['item'] as $k=>$v){
                    $auction_info = $model_auctions->getAuctionsInfoByID($v['auction_id']);
                    if($auction_info['auction_end_true_t'] != ''){
                        $v['auction_remin_date'] = $auction_info['auction_end_true_t'] - TIMESTAMP >0 ?$auction_info['auction_end_true_t'] - TIMESTAMP : 0;
                    }else{
                        $v['auction_remin_date'] = $auction_info['auction_end_time'] - TIMESTAMP >0 ?$auction_info['auction_end_time'] - TIMESTAMP : 0;
                    }
                    if($v['auction_remin_date'] < 1) continue;
                    if($limit-- <= 0) break;

                    if(time() >= $auction_info['auction_start_time']){
                        if($auction_info['auction_start_time'] <= time() && ($auction_info['auction_end_time'] >= time() || $auction_info['auction_end_true_t'] >= time())){
                            $v['is_kaipai'] = 1;
                        }elseif($auction_info['auction_end_true_t'] < time()){
                            $v['is_kaipai'] = 2;
                        }

                    }else{
                        $v['is_kaipai'] = 0;
                    }

                    $v['current_price'] = $auction_info['current_price'] != 0.00 ?intval($auction_info['current_price']):intval($auction_info['auction_start_price']);
                    $v['auction_start_date'] = date('Y-m-d H:i',$auction_info['auction_start_time']);
                    $v['auction_end_date'] = date('Y-m-d H:i',$auction_info['auction_end_time']);
                    $v['auction_end_time'] = $auction_info['auction_end_time'];
                    $v['auction_end_true_t'] = $auction_info['auction_end_true_t'];
                    $v['bid_number'] = $auction_info['bid_number'];
                    $v['is_liupai'] = $auction_info['is_liupai'];
                    $v['auction_click'] = $auction_info['auction_click'];
                    $list[$key]['auctions']['item'][] = $v;
                }
			}
			if($val['adv_list']){
				$list[$key]['adv_list'] = $list1[$key]['adv_list'];
			}


		}
		$data1 = $list;
		$data = array();
		foreach ($data1 as $value) {
			$data[] = $value;
		}
		$this->_output_special($data, $_GET['type'], $special_id);

	}

	public function auction_listOp(){
		$model_setting = Model('auction_setting');
		$search_info = $model_setting->getRowSetting('auction_rec_search');
		if ($search_info !== false) {
			$search_list = @unserialize($search_info['value']);
		}
		if (!$search_list && !is_array($search_list)) {
			$search_list = array();
		}

		$model_auctions = Model('auctions');
        $condition = ['is_liupai'=>0 , 'state'=>0];
		if (!empty($_GET['keyword'])) {
			$condition['auction_name'] = array('like', '%' . $_GET['keyword'] . '%');
		}
        intval($_GET['store_id']) > 0 ? $condition['store_id'] = intval($_GET['store_id']) : '';
		//排序方式
		$order = $this->_auction_list_order(4, $_GET['order']);
        if ($_GET['is_kai_pai'] == 1){          //   正在拍
            $condition['auction_start_time'] = ['elt',time()];
            $condition['auction_end_time'] = ['gt',time()];
        }elseif ($_GET['is_kai_pai'] == 3){     //   预展
            $condition['auction_preview_start'] = ['elt',time()];
            $condition['auction_start_time'] = ['gt',time()];
        }else{
            $condition['auction_preview_start'] = ['elt',time()];
            $condition['auction_end_time'] = ['gt',time()];
        }
		$count = $model_auctions->getAuctionsCount($condition);
		$auctions_list = $model_auctions->getAuctionList($condition,'*','',$order,0,$this->page,$count);
        $auctions_ids_arr = [];
		foreach($auctions_list as $k => $auctions_info){
            $auctions_ids_arr[] = $auctions_info['auction_id'];
            if(time() >= $auctions_info['auction_start_time']){
                if($auctions_info['auction_start_time'] <= time() && ($auctions_info['auction_end_time'] >= time() || $auctions_info['auction_end_true_t'] >= time())){
                    $is_kaipai = 1;
                }elseif($auctions_info['auction_end_true_t'] < time()){
                    $is_kaipai = 2;
                }
                $auctions_list[$k]['auction_remain_date'] = $auctions_info['auction_end_time'] - time() > 0 ? $auctions_info['auction_end_time'] - time() : 0;
            }else{
                $auctions_list[$k]['auction_remain_date'] = $auctions_info['auction_start_time'] - time() > 0 ? $auctions_info['auction_start_time'] - time() : 0;
                $is_kaipai = 0;
            }
            $auctions_list[$k]['is_kaipai'] = $is_kaipai;
			$auctions_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'],360);
			$auctions_list[$k]['auction_start_date'] = date('m月d日 H:i',$auctions_info['auction_start_time']);
			$auctions_list[$k]['auction_end_date'] = date('m月d日 H:i',$auctions_info['auction_end_time']);
			$auctions_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00 ? number_format($auctions_info['current_price'],0) : number_format($auctions_info['auction_start_price'],0);
		}
		$page_count = $model_auctions->gettotalpage();
        $member_id = (int) $this->getMemberIdIfExists();
        if ($member_id > 0){
            $condition = [
                'fav_id' => ['in',$auctions_ids_arr],
                'member_id' =>  $member_id
            ];
            $auctions_favorite_list = Model('favorites')->getAuctionsFavoritesList($condition, 'log_id , fav_id');
            $auctions_favorite_list = array_under_reset($auctions_favorite_list,'fav_id');
            foreach($auctions_list as &$item){
                $item['is_favorate'] = $auctions_favorite_list[$item['auction_id']] ? true : false;
            }
            unset($item);
        }
		output_data(array('search_list' => $search_list , 'auction_list' => $auctions_list ),mobile_page($page_count));
	}

	/**
	 * 拍卖列表排序方式
	 */
	private function _auction_list_order($key, $order) {
		$result = 'auction_id desc';
		if (!empty($key)) {

			$sequence = 'desc';
			if($order == 1) {
				$sequence = 'asc';
			}

			switch ($key) {
				//最新
				case '1' :
					$result = 'auction_start_time' . ' ' . $sequence;
					break;
				//浏览量
				case '2' :
					$result = 'bid_number' . ' ' . $sequence;
					break;
				//价格
				case '3' :
					$result = 'current_price' . ' ' . $sequence;
					break;
                //最
                case '4' :
                    $result = 'auction_end_time' . ' ' . $sequence;
                    break;
			}
		}
		return $result;
	}

	/*
	 * 拍卖详情
	 */
	public function auction_detailOp(){
        // is_kaipai 是否开拍 1是（正在进行） 0否 2是（已结束）

		if(intval($_GET['auction_id']) <= 0) {
			output_error('非法参数');
		}

		$auction_id = intval($_GET['auction_id']);

		$model_auction = Model('auctions');
        $logic_auction = Logic('auction');
        $model_goods = Model('goods');

        $auction_detail = $model_auction->getAuctionDetail($auction_id);
        $auction_info = $auction_detail['auction_info'];
		if(empty($auction_info)){
			output_error('拍品不存在或未审核');
		}
        if(time() >= $auction_info['auction_start_time']){
            if($auction_info['auction_start_time'] <= time() && (($auction_info['auction_end_time'] >= time()&&empty($auction_info['auction_end_true_t'])) || (!empty($auction_info['auction_end_true_t'])&&$auction_info['auction_end_true_t'] >= time()))){
                $is_kaipai = 1;
            }elseif($auction_info['auction_end_true_t'] < time()){
                $is_kaipai = 2;
            }
        }else{
            $is_kaipai = 0;
        }
        $auction_info['auction_image_path'] = cthumb($auction_info['auction_image'],360);
        $goods_info = Logic('goods')->getGoodsCommonInfo($auction_info['goods_id'],$auction_info['auction_image_path']); 
        $auction_info['is_kaipai'] = $is_kaipai;
		$auction_info['auction_video_path'] = goodsVideoPath($auction_info['auction_video'],$auction_info['store_id']);
        $auction_info = array_merge($auction_info,$goods_info); 
		$auction_info['current_price'] = $auction_info['current_price'] != 0.00 ? $auction_info['current_price'] : $auction_info['auction_start_price'];
		$model_store = Model('store');
		$store_info = $model_store->getStoreInfo(array('store_id' => $auction_info['store_id']));
		$auction_info['store_avatar'] = $store_info['store_avatar']
			? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_info['store_avatar']
			: UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
		$auction_info['delayed_time'] = $store_info['delayed_time'];
		$auction_info['auction_start_date'] = date('m月d日 H:i',$auction_info['auction_start_time']);
        $auction_info['auction_remain_start_time'] = $auction_info['auction_start_time'] - time() >0 ? $auction_info['auction_start_time'] - time() : 0;
        $auction_info['auction_end_date'] = date('m月d日 H:i',$auction_info['auction_end_time']);
		$auction_info['auction_remain_date'] = $auction_info['auction_end_time'] - time() >0 ? $auction_info['auction_end_time'] - time() : 0;
        $auction_info['store_member_id'] = $store_info['member_id'];
        if ($auction_info['auction_preview_start'] <= time() && ($auction_info['auction_end_true_t'] >= time() || (empty($auction_info['auction_end_true_t']) && $auction_info['auction_end_time'] >= time()))){
            $auction_info['submit_bond_flag'] = true;
        }
		// 出价记录 
        $result = $logic_auction->getBidList($auction_id, 5);
        $bid_log_list = $result['bid_log_list'];
		foreach($bid_log_list as $key => &$value){
		    //$value['member_name'] = substr($value['member_name'],0,1).'***'.substr($value['member_name'],-1,1);
		    $value['member_name'] = mb_substr($value['member_name'],0,1, 'utf-8').'***'.mb_substr($value['member_name'],-1,1, 'utf-8');
			$bid_log_list[$key]['created_at'] = date('Y-m-d H:i:s' , $value['created_at']);
		}
unset($value);
		//拍品推荐
		$where = array();
//		$where['recommended'] = 1;
		$where['store_id'] = $auction_info['store_id'];
		$where['auction_id'] = array('neq' , $auction_info['auction_id']);
		$where['special_id'] = $auction_info['special_id'];
//		$where['state'] = array('in',array(0,1));
        $where['state'] = 0;
		$auction_recommended_list_tmp = $model_auction->getAuctionList($where);
		$auction_recommended_list = array();
		$tmp = array();
		if(!empty($auction_recommended_list_tmp)){
		    if(count($auction_recommended_list_tmp)<=4){
		        $auction_recommended_list = $auction_recommended_list_tmp;
            }else{
		        while(count($tmp) < 4){
		            $num = rand(0,count($auction_recommended_list_tmp)-1);
		            if(!in_array($num,$tmp)){
		                $tmp[] = $num;
                    }
                }
                foreach($tmp as $v){
		            $auction_recommended_list[] = $auction_recommended_list_tmp[$v];
                }
            }
        }
        if(!empty($auction_recommended_list)){

            foreach($auction_recommended_list as $k => $auctions_info){
                $auction_recommended_list[$k]['auction_image_path'] = cthumb($auctions_info['auction_image'],360);
                if(time() >= $auctions_info['auction_start_time']){
                    if($auctions_info['auction_start_time'] <= time() && ($auctions_info['auction_end_time'] >= time() || $auctions_info['auction_end_true_t'] >= time())){
                        $is_kaipai = 1;
                    }elseif($auctions_info['auction_end_true_t'] < time()){
                        $is_kaipai = 2;
                    }
                }else{
                    $is_kaipai = 0;
                }
                $auction_recommended_list[$k]['current_price'] = $auctions_info['current_price'] != 0.00 ? $auctions_info['current_price'] : $auctions_info['auction_start_price'];
                $auction_recommended_list[$k]['is_kaipai'] = $is_kaipai;
            }
        }

		//专场推荐
		$model_special = Model('auction_special');
		$special_list = $model_special->getSpecialOpenList(array('store_id' => $store_info['store_id'],'is_rec' => 1),'*','special_id asc',3);
		foreach($special_list as $key => $special_info){
			$special_list[$key]['adv_image_path'] = getVendueLogo($special_info['wap_image']);
			$special_list[$key]['special_start_date'] = date('Y年m月d日 H:i',$special_info['special_start_time']);
			$special_list[$key]['special_end_date'] = date('Y年m月d日 H:i',$special_info['special_end_time']);
			$goods_list = $model_special->getSpecialGoodsLists(array('special_id'=>$special_info['special_id'],'auction_sp_state'=>1),'bid_number');
			$all_bid_number = 0;
			foreach($goods_list as $k=>$v){
				$all_bid_number += $v['bid_number'];
			}
			$special_list[$key]['all_bid_number'] = $all_bid_number;
			$special_list[$key]['special_remain_date'] = $special_info['special_end_time'] - time() >0 ? $special_info['special_end_time'] - time() : 0;
			if(time() >= $special_info['special_start_time']){
				if($special_info['special_state'] == 12){
					$is_kaipai = 1;
				}elseif($special_info['special_state'] == 13){
					$is_kaipai = 2;
				}
			}else{
				$is_kaipai = 0;
			}
			$special_list[$key]['is_kaipai'] = $is_kaipai;
		}
        $special_info =  $model_special->getSpecialInfo(['special_id'=>$auction_info['special_id']], $field = "special_id,special_name");
        $auction_info['special_name'] = $special_info['special_name'];
        // 整理拍品属性
        $goods_detail = $model_goods->getGoodsDetail($auction_info['goods_id']);
        foreach ($goods_detail['goods_info']['goods_attr'] as $val){
            $auction_info['auction_attr'][] = array_values($val);
        }
//        print_R($goods_detail['goods_info']['goods_attr']);exit;
//        $auction_info['auction_attr'] = $goods_detail['goods_info']['goods_attr'];
//        if (!empty($auction_info['auction_attr'])) {
//            $auction_info['auction_attr'] = array_merge($auction_info['auction_attr']);
//            foreach ($auction_info['auction_attr'] as $key => $value) {
//                $value = array_values($value);
//                foreach ($value as $key1 => $attr) {
//                    if ($key1 == 0) {
//                        $auction_info['auction_attr'][$key]['name'] = $attr;
//                    } else {
//                        $auction_info['auction_attr'][$key]['value'] = $attr;
//                    }
//                }
//            }
//        }
        $auction_info['interest_last_time'] = strtotime($auction_info['interest_last_date']);
        //计息时间到期标识
        $auction_info['interest_flag'] =  strtotime($auction_info['interest_last_date']) > time() ? true : false;
        $auction_info['interest_time_str'] = strtotime($auction_info['interest_last_date'])-time() > 0?strtotime($auction_info['interest_last_date'])-time():0;
        //利息标识
        $auction_info['bond_flag'] = $auction_info['auction_bond_rate'] > 0?true:false;
        $auction_info['auction_start_time_str'] = $auction_info['auction_start_time'] - time() > 0?$auction_info['auction_start_time'] - time():0;
        $auction_info['auction_end_time_str'] = empty($auction_info['auction_end_true_t'])?($auction_info['auction_end_time'] - time() > 0?$auction_info['auction_end_time'] - time():0):($auction_info['auction_end_true_t'] - time() > 0?$auction_info['auction_end_true_t'] - time():0);
        //计息天数
        $auction_info['bond_day'] = Logic('auction')->getInterestDay(strtotime($auction_info['interest_last_date']),$auction_info['auction_end_time']);
        //商品属性
//        $goods_attr = Model('goods')->getGoodsCommonInfo(array('goods_commonid'=>$auction_info['goods_common_id']),'goods_attr');
//        $goods_attr = unserialize($goods_attr['goods_attr']);

//        $spec_val = unserialize($spec_info['goods_spec']);
        $spec_arr = array();
        if(!empty($goods_attr)){
            foreach($goods_attr as $v){
                $spec_arr[] = array(
                    'name'=>$v['name'],
                    'val'=>$v[0]
                );
            }
        }
        $auction_info['goods_attr'] = $spec_arr;
        if (!empty($auction_info['auction_custom'])) {
            $auction_info['auction_custom'] = array_merge($auction_info['auction_custom']);
        }

		// 整理拍品详情
        $auction_info['auction_mobile_body'] = $this->auction_body($auction_info);
		// 如果已登录 判断该商品是否已被收藏&&添加浏览记录，检查会员和拍品的关系
		if ($this->getMemberIdIfExists() != 0) {
            $member_id = $this->getMemberIdIfExists();
			$c = (int)Model('favorites')->getGoodsFavoritesCountByAuctionId($auction_id, $member_id);
			$auction_info['is_favorate'] = $c > 0;
            $model_relation = Model('auction_member_relation');
            //$model_member = Model('member');
            //$member_info = $model_member->getMemberInfoByID($member_id);
            $relation_info = $model_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
		} else {
            $relation_info = array();
        }
        // 读取服务保障
        $article = Model('article')->getArticleOne(9, 15);

		output_data(array(
		        'auction_info' => $auction_info,
                'bid_log_list' => $bid_log_list,
                'recommend_list' => array('auction' => $auction_recommended_list , 'special' => $special_list),
                'relation_info' => $relation_info,
                'article' => $article,
            )
        );
	}
    
	/**
	 * 输出专题
	 */
	private function _output_special($data, $type = 'json', $special_id = 0) {
		$model_class = Model('mb_class');
		if($_GET['type'] == 'html') {
			$html_path = $model_class->getMbSpecialHtmlPath($special_id);
			if(!is_file($html_path)) {
				ob_start();
				Tpl::output('list', $data);
				Tpl::showpage('mb_auction');
				file_put_contents($html_path, ob_get_clean());
			}
			header('Location: ' . $model_class->getMbSpecialHtmlUrl($special_id));
			die;
		} else {
			output_data($data);
		}
	}
    
	/**
	 * 热门搜索列表
	 */
	public function search_hot_infoOp() {
		if (C('auction_rec_search') != '') {
			$rec_search_list = @unserialize(C('auction_rec_search'));
		}
		$rec_search_list = is_array($rec_search_list) ? $rec_search_list : array();
		$result = $rec_search_list[array_rand($rec_search_list)];
		output_data(array('hot_info'=>$result ? $result : array()));
	}
	
	/*
	 * 出价记录详细
	 * */
    public function get_bid_listOp()
    {
        $auction_id = intval($_GET['auction_id']);
        if($auction_id <= 0){
            output_error('拍品ID异常');
        }
        $page = $_GET['getAll'] ? '' : 5;
        $logic_auction = Logic('auction');
        $bid_log_list = $logic_auction->getBidList($auction_id, '',$page);
        $bid_log_list_b = $bid_log_list['bid_log_list'];
        foreach($bid_log_list_b as $key => &$value){
            //$value['member_name'] = substr($value['member_name'],0,1).'***'.substr($value['member_name'],-1,1);
            $value['member_name'] = mb_substr($value['member_name'],0,1, 'utf-8').'***'.mb_substr($value['member_name'],-1,1, 'utf-8');
            $bid_log_list_b[$key]['created_at'] = date('Y-m-d H:i:s' , $value['created_at']);
        }
        unset($value);
        $return = array(
            'bid_log_list' => $bid_log_list_b,
            'count_rows' => $bid_log_list['count_rows'],
        );
        output_data($return);
	}

	/*
	 * 出价
	 * */
    public function member_offerOp()
    {
        $auction_id = intval($_POST['auction_id']);
        $offer_num = intval($_POST['bid_num']);
        $member_id = $this->getMemberIdIfExists();
        if ($member_id == 0) {
            output_error('不存在的会员');
        }
        $model_margin_order = Model('margin_orders');
        $margin_info = $model_margin_order->getOrderInfo(array('auction_id'=>$auction_id,'buyer_id'=>$member_id,'order_state'=>1));
        if(empty($margin_info)){
            output_error('未支付保证金');
        }

        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($member_id);
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
        // 验证出价
        if ($offer_num < $auction_info['auction_increase_range'] + $auction_info['current_price']) {
            //LM 17.5.19
            output_error('请输入正确的价格');
//            output_error('出价过低，不符合要求');
        }
        // 验证拍卖是否已经结束
        if (empty($auction_info['auction_end_true_t'])) {
            if (time() > $auction_info['auction_end_time']) {
                output_error('拍卖已结束');
            }
        } else {
            if (time() > $auction_info['auction_end_true_t']) {
                output_error('拍卖已结束');
            }
        }
        // 出价写入关系表
        $model_auction_member_relation = Model('auction_member_relation');
        $update = array(
            'is_offer' => 1,
            'offer_num' => $offer_num
        );
        $condition = array(
            'auction_id' => $auction_id,
            'member_id' => $member_id
        );
        $result = $model_auction_member_relation->setRelationInfo($update, $condition);
        if (!$result) {
            output_error('更新关系表失败');
        }

        $relation_info = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);

        // 写入出价日志
        $model_bid_log = Model('bid_log');
        $param = array(
            'member_id' => $member_id,
            'auction_id' => $auction_id,
            'created_at' => time(),
            'offer_num' => $offer_num,
            'member_name' => $member_info['member_name'],
            'is_anonymous' => $relation_info['is_anonymous'],
        );
        $result = $model_bid_log->addBid($param);
        if (!$result) {
            output_error('写入出价日志失败');
        }

        // 更新拍品最新价格
        $update = array(
            'current_price' => $offer_num,
            'bid_number' => array('exp', 'bid_number+1'),
            'current_price_time' => time(),
        );
        $result = $model_auctions->editAuctions($update, array('auction_id' => $auction_id));
        if (!$result) {
            output_error('更新拍品价格失败');
        }
        Logic('auction_agent')->updateCurrentPrice($auction_id);
        output_data(0);
	}

	// 拍品详情
    protected function auction_body($auction_info)
    {
        if ($auction_info['auction_mobile_body'] == '') {
            $auction_info['auction_mobile_body'] = $auction_info['auction_body'];
        }
        return $auction_info['auction_mobile_body'];
    }
    
    /*
     * 设置代理价，获取当前价格信息
     * */
    public function get_dailiOp()
    {
        $auction_id = $_GET['auction_id'];
        $auction_info = Model('auctions')->getAuctionsInfoByID($auction_id);
        $member_id = $this->getMemberIdIfExists();
        // 验证拍卖是否已经结束
        if (empty($auction_info['auction_end_true_t'])) {
            if (time() > $auction_info['auction_end_time']) {
                output_error('拍卖已结束');
            }
        } else {
            if (time() > $auction_info['auction_end_true_t']) {
                output_error('拍卖已结束');
            }
        }
        // 关系表
        $model_relation = Model('auction_member_relation');
        $relation_info = $model_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);

        $data = array(
            'lowest_price' => $relation_info['agent_num'] > $auction_info['current_price'] + $auction_info['auction_increase_range'] * 2 ? $relation_info['agent_num'] : $auction_info['current_price'] + $auction_info['auction_increase_range'] * 2,
            'is_agent' => $relation_info['is_agent'],
            'agent_num' => $relation_info['agent_num'],
            'auction_id' => $auction_id,
        );
        output_data($data);
    }

    /*
     * 保存代理价
     * */
    public function save_dailiOp()
    {
        $member_id = $this->getMemberIdIfExists();
        $result = Logic('auction_agent')->setAgentPrice($_POST, $member_id);
        if ($result['state'] == 'succ') {
            output_data(0);
        } else {
            output_error($result['msg']);
        }
    }

    /*
     * 设置提醒
     * */
    public function set_remindOp()
    {
        $auction_id = intval($_POST['auction_id']);
        $member_id = $this->getMemberIdIfExists();
        $is_remind = 1;
        $is_remind_app = 0;

        $model_member = Model('member');
        /** @var auctionsModel $model_auctions */
        /** @var memberModel $model_member */
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfo(array('auction_id' => $auction_id));
        $member_info = $model_member->getMemberInfoByID($member_id);
        //建立订阅关系
        $model_auction_member_relation = Model('auction_member_relation');
        //查询是否已经存在
        /** @var auction_member_relationModel $model_auction_member_relation */
        $relation = $model_auction_member_relation->getRelationInfoByMemberAuctionID($member_id, $auction_id);
        $insert['auction_id'] = $auction_id;
        $insert['member_id'] = $member_id;
        $insert['is_remind'] = $is_remind;
        $insert['member_mobile'] = $member_info['member_mobile'];
        $insert['is_remind_app'] = $is_remind_app;
        $insert['member_name'] = $member_info['member_name'];
        $insert['remind_time'] = $auction_info['auction_end_time'] - AUCTION_OVER_REMIND;
        $insert['auction_name'] = $auction_info['auction_name'];
        if (empty($relation)) {
            $model_auction_member_relation->addRelation($insert);
        } else {
            $model_auction_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }
        // 设置提醒人数 +1
        $model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('auction_id' => $auction_id));

        output_data(0);
    }


    /*
     * 取消提醒
     * */
    public function cancel_remindOp()
    {
        $relation_id = intval($_POST['relation_id']);
        $auction_id = intval($_POST['auction_id']);
        if (empty($relation_id)) {
            output_error('无效的订阅ID');
        }
        $model_auction_member_relation = Model('auction_member_relation');
        $model_auctions = Model('auctions');
        $update = array(
            'is_remind' => 0,
            'is_remind_app' => 0,
        );
        $result_1 = $model_auction_member_relation->setRelationInfo($update, array('relation_id' => $relation_id));

        // 设置提醒人数 -1
        $result_2 = $model_auctions->editAuctions(array('set_reminders_num' => array('exp', 'set_reminders_num-1')), array('auction_id' => $auction_id));
        if ($result_1 && $result_2) {
            output_data(0);
        } else {
            output_error('拍品取消订阅失败');
        }
    }

    public function check_mobileOp(){
        $member_id = $this->getMemberIdIfExists();
        $member_info = Model('member')->getMemberInfoByID($member_id);
//        print_R($member_info);exit;
        if($member_info['member_mobile'] != ''){
            output_data('1');
        }else{
            output_data('0');
        }
    }
    /**
     * 获去拍品图片
     * @return array
     */
    public function get_auction_imgOp(){
        $auction_id = intval($_GET['auction_id']);

        $auction_info = Model('auctions')->getAuctionsInfo(['auction_id'=>$auction_id], 'goods_id,auction_image,store_id');
        $model_goods = Model('goods');
        $goods_info = $model_goods->getGoodsInfo(['goods_id'=>$auction_info['goods_id']], 'goods_commonid,color_id,store_id');
        $image_more = $model_goods->getGoodsImageByKey($goods_info['goods_commonid'] . '|' . $goods_info['color_id']);


        $image_list = array();
        foreach ($image_more as $val) {
            $image_list[] = array( '_small' => cthumb($val['goods_image'] , 60 , $goods_info['store_id']) , '_mid' => cthumb($val['goods_image'] , 360 , $goods_info['store_id']) , '_big' => cthumb($val['goods_image'] , 1280 , $goods_info['store_id']) );
        }
        if (empty($image_more)) {
            $image_list[] = array( '_small' => cthumb($auction_info['auction_image'],60) , '_mid' => cthumb($auction_info['auction_image'] , 360) ,'_big' => cthumb($auction_info['auction_image'] , 1280) );
        }
        output_data(['img_list'=>$image_list]);
    }

}