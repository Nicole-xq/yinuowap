<?php
/**
 * 优惠券
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_couponControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
    }
    /*
     * 获取优惠券
     */
    public function coupon_infoOp(){
        $t_id = intval($_POST['tid']);
        if ($t_id <= 0){
            output_error('无效地址');
        }

        $model_voucher = Model('voucher');
        $gettype_array = $model_voucher->getVoucherGettypeArray();
        $templatestate_arr = $model_voucher->getTemplateState();
        $where = [
            'voucher_t_id' => $t_id,
            'voucher_t_gettype' => $gettype_array['free']['sign'],
            'voucher_t_state' => $templatestate_arr['usable'][0],
            'voucher_t_end_date' => ['gt',time()]
        ];
        $template_info = $model_voucher->getVoucherTemplateInfo($where);

        if (empty($template_info)){
            output_error('无效地址');
        }
        if ($template_info['voucher_t_total']<=$template_info['voucher_t_giveout']){//代金券不存在或者已兑换完
            output_error('代金券已兑换完');
        }
        $template_info['voucher_t_end_date'] = date('Y-m-d',$template_info['voucher_t_end_date']);
        output_data(array('coupon_info' => $template_info));
    }

    /**
     * 领取免费代金券
     */
    public function get_free_couponOp() {
        $t_id = intval($_POST['tid']);
        if($t_id <= 0){
            output_error('代金券信息错误');
        }
        $model_voucher = Model('voucher');
        //验证是否可领取代金券
        $data = $model_voucher->getCanChangeTemplateInfo($t_id, $this->member_info['member_id'], $this->member_info['store_id']);
        if ($data['state'] == false){
            output_error($data['msg']);
        }
        try {
            $model_voucher->beginTransaction();
            //添加代金券信息
            $data = $model_voucher->exchangeVoucher($data['info'], $this->member_info['member_id'], $this->member_info['member_name']);
            if ($data['state'] == false) {
                throw new Exception($data['msg']);
            }
            $model_voucher->commit();
            output_data('1');
        } catch (Exception $e) {
            $model_voucher->rollback();
            output_error($e->getMessage());
        }
    }


}
