<?php
/**
 * 投票活动
 *
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class vote_activityControl extends mobileHomeControl {
    private $member_id;

    public function __construct(){
        parent::__construct();
        $this->member_id = $this->getMemberIdIfExists();
    }

    public function indexOp(){
        $this->get_activity_listOp();
    }

    /**
     * 活动列表
     * */
    public function get_activity_listOp(){
        $model_activity = Model('vote_activity');

        $list = $model_activity->getList($this->page,'','add_time desc,id desc','id,title,description,end_time,image');
        $page_count = $model_activity->gettotalpage();

        output_data(array('vote_list' => $list, 'page'=>mobile_page($page_count)));
    }

    /**
     * 活动详情
     * */
    public function get_activity_infoOp(){
        $id = trim($_GET['id']);
        if(empty($id)) output_error('缺少参数');

        $detail = $this->activity_info($id);

        output_data(array('detail' => $detail));
    }

    private function activity_info($id){
        $data = Model('vote_activity')->getInfo(['id'=>$id]);
        $data['content'] = html_entity_decode($data['content']);

        return $data;
    }

    /**
     * 作品列表
     * */
    public function get_entry_listOp(){
        $id = trim($_GET['id']);
        $member_id = $this->member_id;
        if(empty($id)) output_error('缺少参数');

        $model_activity_entry = Model('vote_activity_entry');
        $model_entry_log = Model('vote_entry_log');

        $condition = array('activity_id' => $id, 'state' => ['in', '1,3,4']);
        if(isset($_GET['search']) && !empty($_GET['search'])){
            $condition['entry_no|name'] = array(array('like','%'.$_GET['search'].'%'));
        }
        $vote_list = $model_activity_entry->getList($condition, '','entry_no,add_time desc,id desc','*');
        $page_count = $model_activity_entry->gettotalpage();

        $log_list = $model_entry_log->getList(['activity_id'=>$id, 'member_id'=>$member_id],'entry_id');
        $log_list_id = array_column($log_list,'entry_id');

        foreach ($vote_list as $key => $value){
            if($member_id > 0 && in_array($value['id'], $log_list_id)){
                $vote_list[$key]['like'] = 1;
            }else{
                $vote_list[$key]['like'] = 0;
            }
        }

        $detail = $this->activity_info($id);

        output_data(array('entry_list' => $vote_list, 'detail'=> $detail, 'page'=>mobile_page($page_count)));
    }

    /**
     * 作品详情
     * */
    public function get_entry_infoOp(){
        $id = trim($_GET['id']);
        $member_id = $this->member_id;
        if(empty($id)) output_error('缺少参数');

        $model_activity_entry = Model('vote_activity_entry');
        $model_vote = Model('vote_entry_log');

        $vote_info = $model_activity_entry->getInfo(['id'=>$id]);
        $vote_info['like'] = 0;
        if($member_id > 0){
            $log = $model_vote->getInfo(['entry_id'=>$id, 'member_id'=>$member_id]);
            $log and $vote_info['like'] = 1;
        }

        $detail = $this->activity_info($vote_info['activity_id']);
        unset($detail['content']);

        output_data(array('vote_info' => $vote_info, 'detail' => $detail));
    }

    /**
     * 审核状态
     * */
    public function get_stateOp(){
        $model_activity_entry = Model('vote_activity_entry');
        $id = trim($_GET['id']);
        $member_id = $this->member_id;
        if($id > 0) {
            $vote_info = $model_activity_entry->getInfo(['activity_id'=>$id,'member_id'=>$member_id]);
            if($vote_info){
                output_data(['id'=>$vote_info['id'], 'state'=>$vote_info['state']]);
            }else{
                $activity_info = $this->activity_info($id);
                if (strtotime($activity_info['end_time']) <= TIMESTAMP) {
                    output_data(['id'=>0, 'state'=>'5']);
                }
            }
        }

        output_data(['id'=>0, 'state'=>-1]);
    }

    /*
     * 作品投票
     * */
    public function voteOp(){
        $id = $_GET['id'];
        $member_id = $this->member_id;
        empty($member_id) and output_data(['state'=>1,'msg'=>'请先登录']);

        $model_entry_log = Model('vote_entry_log');
        $model_activity_entry = Model('vote_activity_entry');

        $entry_info = $model_activity_entry->getInfo(['id'=>$id]);
        empty($entry_info) and output_data(['state'=>2,'msg'=>'您投票的作品不存在']);

        // 获取活动详情
        $activity_info = $this->activity_info($entry_info['activity_id']);

        if(strtotime($activity_info['end_time']) <= TIMESTAMP){
            output_data(['state'=>3,'msg'=>'活动已结束']);
        }

        $condition = array('activity_id'=>$entry_info['activity_id'], 'member_id'=>$member_id);
        // 给予条件，当天或周期
        $msg = '您本期';
        if($activity_info['vote_type'] == 0){
            $start_time = strtotime(date("Y-m-d"),time());
            $condition['add_time'] = ['gt',$start_time];
            $msg = '您今天';
        }

        $log_list = $model_entry_log->getList($condition);
        $entry_id_arr = [];
        $count_member_num = 0;
        $sum_vote_num = 0;
        foreach ($log_list as $value) {
            if(!in_array($value['entry_id'],$entry_id_arr)){
                $entry_id_arr[] = $value['entry_id'];
                $count_member_num++;
            }
            $sum_vote_num += $value['vote_num'];
        }

        // 验证(当天||周期)活动单人票数上限
        if($sum_vote_num >= $activity_info['vote_num']){
            output_data(['state'=>3,'msg'=>$msg.'投票已达上限了']);
        }

        !in_array($id,$entry_id_arr) and $count_member_num++;
        // 验证(当天||周期)活动投票人数上限
        if($count_member_num > $activity_info['vote_member_num']){
            output_data(['state'=>3,'msg'=>$msg.'投票作品已达到上限']);
        }

        try{
            $model_entry_log->beginTransaction();

            // 添加投票
            if(in_array($id,$entry_id_arr)){
                $data = array('vote_num' => ['exp','vote_num+1'],'modify_time' => TIMESTAMP,);
                $result = $model_entry_log->modify($data,['entry_id'=>$id]);
            }else{
                $data = array(
                    'member_id' => $member_id,
                    'entry_id' => $entry_info['id'],
                    'activity_id' => $entry_info['activity_id'],
                    'vote_num' => 1,
                    'add_time' => TIMESTAMP,
                );
                $result = $model_entry_log->save($data);
            }
            if(empty($result)){
                throw new Exception();
            }
            $data = array('vote_num' => ['exp','vote_num+1']);
            $result = $model_activity_entry->modify($data,['id'=>$entry_info['id']]);
            if(empty($result)){
                throw new Exception();
            }
            $model_entry_log->commit();
            output_data(['state'=>0,'msg'=>'投票成功']);
        }catch (Exception $e){
            $model_entry_log->rollback();
            output_data(['state'=>4,'msg'=>'投票失败']);
        }
    }

    public function save_informationOp(){
        $act_id = $_POST['act_id'];
        if(empty($act_id) || empty($this->member_id)){
            output_error('参数错误');
        }
        $auther = $_POST['auther'];
        if (mb_strlen($auther, 'UTF8') > 30) {
            output_error('作者名字不能超过30个字');
        }
        $name = $_POST['name'];
        if (mb_strlen($name, 'UTF8') > 30) {
            output_error('作品名称不能超过30个字');
        }

        $description = $_POST['description'];
        if (mb_strlen($description, 'UTF8') > 300) {
            output_error('描述不能超过300个字');
        }

        $activity_info = $this->activity_info($act_id);
        if(strtotime($activity_info['end_time']) <= TIMESTAMP){
            output_error('活动已结束');
        }
        $model_activity_entry = Model('vote_activity_entry');

        $condition = array('member_id' => $this->member_id,'activity_id'=>$act_id);
        $entry_info = $model_activity_entry->getInfo($condition);
        if(!empty($entry_info)){
            switch ($entry_info['state']){
                case 0:
                    output_error('您的作品正在审核中，请勿重复提交');
                    break;
                case 1:
                    output_error('您的作品已经通过审核！');
                    break;
                case 3:
                    output_error('活动已经结束！');
                    break;
                case 4:
                    output_error('活动已经结束！');
                    break;
            }
        }

        $update = array();
        //上传头像
        if ($_FILES['entry_image']['name']) {
            $upload = new UploadFile();
            $upload->set('max_size', 1024 * 5);
            $upload->set('default_dir', '/cms/vote_activity_entry');
            $upload->set('file_name', 'vote_entry_' . $act_id . '_' . $this->member_id . '_'. TIMESTAMP . '.jpg');
            $result = $upload->upfile('entry_image');
            if ($result) {
                $update['image'] = $upload->file_name;
            } else {
                output_error($upload->error);
            }
        }

        $update['member_id'] = $this->member_id;
        $update['activity_id'] = $act_id;
        $update['auther'] = $auther;
        $update['name'] = $name;
        $update['description'] = $description;
        $update['vote_num'] = 0;
        $update['state'] = 0;
        $update['add_time'] = date('Y-m-d H:i:s', time());

        if ($entry_info) {
            $res = $model_activity_entry->modify($update, $condition);
        } else {
            $res = $model_activity_entry->save($update);
        }

        if ($res) {
            $image_file = BASE_UPLOAD_PATH . '/cms/vote_activity/' .$entry_info['image'];
            if(isset($image_file) && is_file($image_file)) {
                @unlink($image_file);
            }
            output_data('保存成功！');
        } else {
            output_error('保存失败！');
        }

    }

}