<?php
/**
 * 我的商城
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_indexControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 我的商城
     */
    public function indexOp() {
        $isSeller = isset($_REQUEST['is_seller']) ? $_REQUEST['is_seller'] : '';
        $member_info = array();
        $memberId = $this->member_info['member_id'];
        $member_info['user_id'] = $this->member_info['member_id'];
        $member_info['user_name'] = $this->member_info['member_name'];
        $member_info['type_logo'] = getMemberVendueForID($this->member_info['member_type']);
        $member_info['user_name'] = $this->member_info['member_truename'] ? $this->member_info['member_truename'] : $this->member_info['member_name'];
        if(substr($this->member_info['member_avatar'], 0, 4) == 'http'){
            $member_info['avatar'] = $this->member_info['member_avatar'] ;
        } else {
            $member_info['avatar'] = getMemberAvatarForID($this->member_info['member_id']).'?'.TIMESTAMP;
        }
        $member = Model('member')->getMemberInfoByID($this->member_info['member_id']);
        $member_gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
        $member_info['level'] = $member_gradeinfo['level'];
        $member_info['level_name'] = $member_gradeinfo['level_name'];
        $member_info['favorites_store'] = Model('favorites')->getStoreFavoritesCountByMemberId($this->member_info['member_id']);
        $member_info['favorites_goods'] = Model('favorites')->getFavoritesCount($this->member_info['member_id']);
//        print_R($member_info);exit;
        // 交易提醒
        $model_order = Model('order');
        if (empty($isSeller)) {
            $member_info['order_nopay_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'NewCount');
            $member_info['order_pay_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'PayCount');
            $member_info['order_noreceipt_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'SendCount');
            $member_info['order_notakes_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'TakesCount');
            $member_info['order_noeval_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'EvalCount');
        } else {
            $store_id = 0;
            $storeInfo = Model('')->table('store')->where(['member_id' => $memberId])->find();
            if (!empty($storeInfo) && isset($storeInfo['store_id'])) {
                $store_id = $storeInfo['store_id'];
            }
            if (!empty($store_id)) {
                //待支付
                $member_info['order_nopay_count'] = $model_order->getOrderCountByID('store_id', $store_id, 'NewCount');
                //待发货
                $member_info['order_pay_count'] = $model_order->getOrderCountByID('store_id', $store_id, 'PayCount');
                //待收货
                $member_info['order_noreceipt_count'] = $model_order->getOrderCountByID('store_id', $store_id, 'SendCount');
                //待自提
                $member_info['order_notakes_count'] = $model_order->getOrderCountByID('store_id', $store_id, 'TakesCount');
                //待评价
                $member_info['order_noeval_count'] = $model_order->getOrderCountByID('store_id', $store_id, 'EvalCount');
            } else {
                $member_info['order_nopay_count'] = $member_info['order_noreceipt_count']
                    = $member_info['order_notakes_count'] = $member_info['order_noeval_count'] = 0;
            }
        }
        $member_info['member_type_name'] = $this->member_info['member_type_name'];
        $member_info['count_price'] = $member['available_predeposit'] + $member['freeze_margin'];//总资产
        $member_info['freeze_margin'] = $member['freeze_margin'];//保证金
        $member_info['available_predeposit'] = $member['available_predeposit'];//余额
        $member_info['member_points'] = $member['member_points'];//余额
        //$member_info['favourite_count'] = Model('favorites')->getFavoritesCount($this->member_info['member_id']);
        //$member_info['store_favourite_count'] = Model('favorites')->getStoreFavoritesCount($this->member_info['member_id']);
        // 售前退款
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_state'] = array('lt', 3);
        $member_info['return'] = Model('refund_return')->getRefundReturnCount($condition);
        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        //查询历史累计奖励金金额
        $sum_commission_amount = $model_commission->getMemberCommissionCount('', $this->member_info['member_id'], $form_member_id);
        $sum_commission_amount = empty($sum_commission_amount) ? 0 : $sum_commission_amount;
        $member_info['commission_amount'] = number_format($sum_commission_amount, 2);


        $model_commission = Model('member_commission');
        $member_id = $this->member_info['member_id'];
        $condition = [
            'dis_member_id' => $member_id, //当前受益人
            'commission_type' => 6, //保证金收益
            'dis_commis_state' => 1, // 1:已结算  0: 未结算
        ];
        //这个是自己的保证金收益已经结算的金额
        $all_margin = $model_commission->where($condition)->sum('commission_amount');
        //这个是自己的保证金收益已缴纳的保证金本金
        $margin_order = Model('margin_orders');
        $where = [
            'margin_orders.buyer_id' => $member_id,
            'margin_orders.order_state' => 1,     //已支付
            'margin_orders.refund_state' => 0,    //未退款
            'margin_orders.default_state' => 0,    //不违约
            'auctions.state' => 0    //未结束的专场
        ];
        //新代码上线之前的保证金统计
        $on = 'margin_orders.auction_id=auctions.auction_id';
        $freeze_margin = $margin_order->table('margin_orders,auctions')
            ->join('inner')->on($on)->where($where)
            ->sum('margin_orders.margin_amount');
        $member_info['freeze_margin_amount'] = number_format($freeze_margin, 2);
        $member_info['is_distri'] = $this->member_info['distri_state'] == 2 ? 1 : 0;
        $model_dis_goods = Model('dis_goods');
        $dis_goods_count = $model_dis_goods->getDistriGoodsCount(array('member_id' => $this->member_info['member_id']));//查看该会员分销商品数量
        if(C('live.open')){
            if($dis_goods_count > 0){
                $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
                $member_info['is_movie_msg'] = '';
            }else{
                $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
                $member_info['is_movie_msg'] = '您还没有选择分销商品，请先选择。';
            }
        }else{
            $member_info['is_movie'] = intval(C('live.open'));//该会员是否开启直播
            $member_info['is_movie_msg'] = '';
        }
        $member_info['count_partner'] = $this->member_info['distribute_lv_1'] + $this->member_info['distribute_lv_2'];
        //下级用户头像 限制显示五条
        $member_info['partner_avatar'] = logic('j_member_distribute')->getLowerLevelMemberAvatarByMemberId($memberId);
        $available_info = Model('member')->getMemberInfoByID($this->member_info['member_id'],'available_commis,freeze_commis');
        $member_info['available_commis'] = $available_info['available_commis'];

        //红包个数
        $member_info['rpt_num'] = Model('redpacket')->own_available_rpt($this->member_info['member_id']);

        $top_member = Model()->table('member_distribute')->field('top_member_name,dis_code')->where(array('member_id'=>$this->member_info['member_id']))->find();
        $service_hotline = Model()->table('setting')->where(array('name' => 'service_hotline'))->find();

        output_data(array('member_info' => $member_info,'top_member'=>$top_member, 'service_hotline' => $service_hotline));
    }
    public function myAvatarOp() {
        $invite_code = isset($_REQUEST['invite_code']) ? $_REQUEST['invite_code'] : null;
        if($invite_code){
            $top_member = Model()->table('member_distribute')->field('member_id')->where(array('dis_code'=>$invite_code))->find();
            if(empty($top_member["member_id"])){
                exit("not exist invite code");
            }
            $memberInfo =  Model('member')->getMemberInfoByID($top_member["member_id"]);
        }else{
            $memberInfo = $this->member_info;
        }
        if(substr($memberInfo['member_avatar'], 0, 4) == 'http'){
            $memberInfo['avatar'] = $memberInfo['member_avatar'] ;
        } else {
            $memberInfo['avatar'] = getMemberAvatarForID($memberInfo['member_id']).'?'.TIMESTAMP;
        }
/*        if(strstr($imgUrl,'wscgs.sxga.gov.cn')){
            $url = $imgUrl;
        }else{
            $url = "http://www.sxol.com/Images/index20120814/Logo.gif";
        }*/
        if(strpos($memberInfo['avatar'], $_SERVER["HTTP_HOST"]) !== false){
            redirect($memberInfo['avatar']);
            exit();
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$memberInfo['avatar']);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $img=curl_exec($ch);
        curl_close($ch);
        header("Content-type: image/jpg");
        echo $img;
    }
    public function myInviteCodeImgOp() {
        $invite_code = isset($_REQUEST['invite_code']) ? $_REQUEST['invite_code'] : null;
        if(!$invite_code){
            $top_member = Model()->table('member_distribute')->field('top_member_name,dis_code')->where(array('member_id'=>$this->member_info['member_id']))->find();
            if(empty($top_member["dis_code"])){
                exit("not exist invite code");
            }
            $invite_code = $top_member["dis_code"];
        }
        $bind_uri = "http://".$_SERVER["HTTP_HOST"].'/wap/tmpl/member/register.html?invite_code='.$invite_code;
        // var qr_img = create_qrcode(bind_uri);
        $imgUrl = "http://qrcode.yinuovip.com?type=member_qrcode&text=".$bind_uri;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $imgUrl);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $img=curl_exec($ch);
        curl_close($ch);
        header("Content-type: image/png");
        echo $img;
    }

    
    /**
     * 我的资产
     */
    public function my_assetOp() {
        $param = $_GET;
        $fields_arr = array('point','predepoit','available_rc_balance','redpacket','voucher','available_commis');
        $fields_str = trim($param['fields']);
        if ($fields_str) {
            $fields_arr = explode(',',$fields_str);
        }
        $member_info = array();
        if (in_array('point',$fields_arr)) {
            $member_info['point'] = $this->member_info['member_points'];
        }
        if (in_array('predepoit',$fields_arr)) {
            $member_info['predepoit'] = $this->member_info['available_predeposit'];
        }
        if (in_array('available_rc_balance',$fields_arr)) {
            $member_info['available_rc_balance'] = $this->member_info['available_rc_balance'];
        }
        if (in_array('redpacket',$fields_arr)) {
            $member_info['redpacket'] = Model('redpacket')->getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        }
        if (in_array('voucher',$fields_arr)) {
            $member_info['voucher'] = Model('voucher')->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        }
        if (in_array('available_commis',$fields_arr)) {
            $member = Model('member')->getMemberInfoByID($this->member_info['member_id'],'available_commis,freeze_commis');
            $member_info['available_commis'] = $member['available_commis'];
        }
        output_data($member_info);
    }
    
    /**
     * 我的会员等级
     */
    public function member_gradeOp() {
        $gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
        $_info = array();
        $_info['level'] = $gradeinfo['level'];
        $_info['level_name'] = $gradeinfo['level_name'];
        output_data($_info);
    }

    /**
     * 首页店铺的收藏状态
     * @return [type] [description]
     */
    public function fav_store_listOp() {
        $store_ids = $_GET['store_ids'];
        if (empty($store_ids)) {
            output_error('参数错误');
        }
        $store_ids = explode(',',$store_ids);
        $store_ids = array_unique($store_ids);
        $model_fav = Model('favorites');
        $store_id_list = $model_fav->getStoreFavoritesList(array('fav_id' => array('in',$store_ids),'member_id' => $this->member_info['member_id']));
        $my_fav_stores = array();
        foreach ($store_id_list as $key => $value) {
            $my_fav_stores[] = $value['fav_id'];
        }
        if (empty($my_fav_stores)) {
            output_error('没有匹配到的店铺');
        }
        output_data($my_fav_stores);
    }


    //账户信息
    public function member_infoOp(){
        $model_member = Model('member');
        /** @var predepositModel $model_pd */
        $model_pd = Model('predeposit');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        $recharge_amount = 0.00;
        $recharge_list = $model_pd->getPdRechargeList(array('pdr_member_id'=>$this->member_info['member_id'],'pdr_payment_state'=>1));
        if(!empty($recharge_list)){
            foreach($recharge_list as $k1=>$v1){
                $recharge_amount += $v1['pdr_amount'];
            }
        }
        $member_info['all_recharge'] = ncPriceFormat($recharge_amount);
        if($member_info['available_commis'] == ''){
            $member_info['available_commis'] = 0.00;
        }
        $member_info['all_commis'] =  $member_info['available_commis'];
        output_data($member_info);
    }

    /**
     * 申请佣金提现
     */
    public function commis_cash_addOp(){

            $pdc_amount = abs(floatval($_POST['money']));
            $model_pd = Model('predeposit');
            $model_member = Model('member');
            $member_info = $model_member->table('member')->where(array('member_id'=> $this->member_info['member_id']))->master(true)->lock(true)->find();;//锁定当前会员记录
            //验证支付密码
            if (md5($_POST['password']) != $member_info['member_paypwd']) {
                output_error('支付密码错误');
            }
            $cash_amount = 0.00;
            $cash_list = $model_pd->getPdCashList(array('pdc_member_id'=>$this->member_info['member_id'],'is_commis'=>1));
            if(!empty($cash_list)){
                foreach($cash_list as $k=>$v){
                    $cash_amount += $v['pdc_amount'];
                }
            }

            if($member_info['available_predeposit'] > ($member_info['available_commis']-$cash_amount)){
                $member_info['effect_commis'] = $member_info['available_commis']-$cash_amount;
            }else{
                $member_info['effect_commis'] = $member_info['available_predeposit'];
            }

            //验证金额是否足够
            if (floatval($member_info['effect_commis']) < $pdc_amount){
                output_error('佣金金额不足');
            }
            try {
                $model_pd->beginTransaction();
                $pdc_sn = $model_pd->makeSn();
                $data = array();
                $data['pdc_sn'] = $pdc_sn;
                $data['pdc_member_id'] = $this->member_info['member_id'];
                $data['pdc_member_name'] = $this->member_info['member_name'];
                $data['pdc_amount'] = $pdc_amount;
                $data['pdc_bank_name'] = $_POST['bank_name'];
                $data['pdc_bank_no'] = $_POST['card_number'];
                $data['pdc_bank_user'] = $_POST['bank_user'];
                $data['pdc_add_time'] = TIMESTAMP;
                $data['pdc_payment_state'] = 0;
                $data['is_commis'] = 1;
                $insert = $model_pd->addPdCash($data);
                if (!$insert) {
                    output_error('提现信息添加失败');
                }
                //冻结可用预存款
                $data = array();
                $data['member_id'] = $member_info['member_id'];
                $data['member_name'] = $member_info['member_name'];
                $data['amount'] = $pdc_amount;
                $data['order_sn'] = $pdc_sn;
                $model_pd->changePd('cash_apply_commis',$data);
                $model_pd->commit();
                output_data('1');
            } catch (Exception $e) {
                $model_pd->rollback();
                output_error($e->getMessage());
            }

    }

    /**
     * 申请余额提现
     */
    public function pd_cash_addOp(){

        $pdc_amount = abs(floatval($_POST['money']));
        //验证提现金额不小于100
        if ($pdc_amount < 100) {
            output_error('最低提现金额≥100元');
        }
        $model_pd = Model('predeposit');
        $model_member = Model('member');
        $member_info = $model_member->table('member')->where(array('member_id'=> $this->member_info['member_id']))->master(true)->lock(true)->find();;//锁定当前会员记录
        //验证支付密码
        if (md5($_POST['password']) != $member_info['member_paypwd']) {
            output_error('支付密码错误');
        }

        //验证金额是否足够
        if (floatval($member_info['available_predeposit']) < $pdc_amount){
            output_error('预存款金额不足');
        }

        $bank_card = $_POST['card_number'];
        $bankcard = Model('bankcard')->getbankCardInfo(['bank_card' => $bank_card]);
        if (empty($bankcard)) {
            output_error('没有找到此银行卡');
        }
        try {
            $model_pd->beginTransaction();
            $pdc_sn = $model_pd->makeSn();
            $data = array();
            $data['pdc_sn'] = $pdc_sn;
            $data['pdc_member_id'] = $this->member_info['member_id'];
            $data['pdc_member_name'] = $this->member_info['member_name'];
            $data['pdc_amount'] = $pdc_amount;
            $data['pdc_bank_name'] = $bankcard['bank_name'];
            $data['pdc_bank_no'] = $bank_card;
            $data['pdc_bank_user'] = $bankcard['true_name'];
            $data['pdc_add_time'] = TIMESTAMP;
            $data['pdc_payment_state'] = 0;
            $insert = $model_pd->addPdCash($data);
            if (!$insert) {
                output_error('提现信息添加失败');
            }
            //冻结可用预存款
            $data = array();
            $data['member_id'] = $member_info['member_id'];
            $data['member_name'] = $member_info['member_name'];
            $data['amount'] = $pdc_amount;
            $data['order_sn'] = $pdc_sn;
            $model_pd->changePd('cash_apply',$data);
            $model_pd->commit();
            output_data('1');
        } catch (Exception $e) {
            $model_pd->rollback();
            output_error($e->getMessage());
        }

    }

    /*
     * 获取保证金收益
     * */
    public function member_margin_infoOp()
    {
//        /** @var memberModel $model_member */
//        /** @var predepositModel $model_predeposit */
//        $model_member = Model('member');
//        $model_predeposit = Model('predeposit');
//        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
//        $all_margin = $model_predeposit->get_cumulative_margin($this->member_info['member_id']);
        /** @var member_commissionModel $model_commission */
        $model_commission = Model('member_commission');
        $member_id = $this->member_info['member_id'];
        $condition = [
            'dis_member_id' => $member_id, //当前受益人
            'commission_type' => 6, //保证金收益
            'dis_commis_state' => 1, // 1:已结算  0: 未结算
        ];
        //这个是自己的保证金收益已经结算的金额
        $all_margin = $model_commission->where($condition)->sum('commission_amount');
        //这个是自己的保证金收益已缴纳的保证金本金
        $margin_order = Model('margin_orders');
        $where = [
            'margin_orders.buyer_id' => $member_id,
            'margin_orders.order_state' => 1,     //已支付
            'margin_orders.refund_state' => 0,    //未退款
            'margin_orders.default_state' => 0,    //不违约
            'auctions.state' => 0    //未结束的专场
        ];
        //新代码上线之前的保证金统计
        $on = 'margin_orders.auction_id=auctions.auction_id';
        $freeze_margin = $margin_order->table('margin_orders,auctions')
            ->join('inner')->on($on)->where($where)
            ->sum('margin_orders.margin_amount');
        $data = array(
            //'available_margin' => $member_info['available_margin']+$member_info['freeze_margin'],
            //'switch_margin' => $member_info['available_margin'],
            //'freeze_margin' => $member_info['freeze_margin'],
            'freeze_margin' => $freeze_margin ?: 0,
            'all_recharge' => $all_margin ?: 0
        );
        output_data($data);
    }

    /*
     * 获取会员保证金列表信息
     * */
    public function member_margin_listOp()
    {
        //查询红包
        $where = [
            'dis_member_id'=>$this->member_info['member_id'],
            'commission_type'=>6,
            ];
        if (isset($_GET['commission_time'])){
            $month_day = date('t', strtotime($_GET['commission_time']));
            $month_first = strtotime($_GET['commission_time'].'-01');
            $month_last =  strtotime($_GET['commission_time'].'-'.$month_day.' 23:59:59');
            $where['add_time'] = ['between',$month_first.','.$month_last];
        }
        if (strval($_GET['dis_commis_state']) !='' && in_array(strval($_GET['dis_commis_state']),[0,1])){
            $where['dis_commis_state'] = intval($_GET['dis_commis_state']);
        }
        $field = "log_id,goods_name,add_time,commission_amount,dis_member_id,commission_time,order_sn,dis_commis_state";
        $model_margin = Model('member_commission');
        $margin_list = $model_margin->getCommissionList($where, $field, $this->page, 'add_time desc');

        $page_count = $model_margin->gettotalpage();
        $new_data_list = [];
        $cur_year = date("Y",time());
        foreach ($margin_list as $value){
            $month_key = date('m',$value['add_time']).'月';
            $year_key = date('Y',$value['add_time']);
            $month_key = ($cur_year == $year_key) ? $month_key : $year_key.'年'.$month_key;
            $value['commission_time'] = date('Y.m.d',$value['add_time']);
            $auction_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$value['order_sn']], 'auction_name');;
            $value['auction_name'] = $auction_info['auction_name'];
            $new_data_list[$month_key][] = $value;
        }
        output_data(array('margin_list' => $new_data_list), mobile_page($page_count));
    }

    /*
 * 获取会员保证金列表信息
 * */
    public function member_margin_detailOp()
    {
        if (!intval($_GET['log_id'])){
            output_error('参数有误');
        }

        $field = "log_id,goods_name,goods_pay_amount,commission_amount,order_id,order_sn,dis_commis_state,commission_time";
        $where = ['log_id'=>intval($_GET['log_id'])];
        $commission_info = Model('member_commission')->getCommissionInfo($where, $field);

        $commission_info['redemption_amount'] = ncPriceFormat($commission_info['goods_pay_amount'] + $commission_info['commission_amount']);

        //保证金订单
        $field = 'payment_code,created_at,payment_time,auction_id';
        $margin_info = Model('margin_orders')->getOrderInfo(['order_sn'=>$commission_info['order_sn']], $field);
        $margin_info['add_time'] = date('Y.m.d H:i:s',$margin_info['created_at']);
        $margin_info['bond_start_data'] = date('Y.m.d',strtotime('+1 day',$margin_info['created_at']));
        $margin_info['payment_name'] = orderPaymentName($margin_info['payment_code']);

        //拍卖的相关信息
        $field = 'auction_end_time,auction_bond_rate,auction_name';
        $auction_info = Model('auctions')->getAuctionsInfo(['auction_id'=>$margin_info['auction_id']], $field);
        $auction_info['auction_bond_rate'] = $auction_info['auction_bond_rate']."%";
        $auction_info['bond_end_data'] = date('Y.m.d',$auction_info['auction_end_time']);
        $auction_info['pay_date_number'] = Logic('auction')->getInterestDay($margin_info['payment_time'],$auction_info['auction_end_time']);
        $auction_info['auction_end_time'] = date('Y.m.d', $auction_info['auction_end_time']);

        output_data(array_merge($commission_info,$margin_info,$auction_info));
    }
    /*
     * 保证金提现
     * */
    public function margin_cash_addOp()
    {
        $num = ncPriceFormat(floatval($_POST['money']));

        $model_member = Model('member');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($num <= 0) {
            output_error('提现金额异常');
        }
        if ($member_info['available_margin'] < $num) {
            output_error('提现金额大于余额');
        }

        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->margin_to_pd($num, $this->member_info['member_id'], $this->member_info['member_name']);
        if (!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data(0);
        }
    }

    /**
     * 获取注册红包信息
     */
    public function get_register_awardOp(){
        $model_redpacket = Model('redpacket');
//        //更新红包过期状态
//        $model_redpacket->updateRedpacketExpire($_SESSION['member_id']);
        //查询红包
        $where = array();
        $where['rpacket_owner_id'] = $this->member_info['member_id'];
        $where['rpacket_state'] = 1;
        $list = $model_redpacket->getRedpacketList($where, '*', 0, 10, 'rpacket_active_date desc');
        output_data($list);
    }

    /**
     * 获取红包通知信息
     */
    public function get_noticeOp(){
        $list = Logic('notice')->get_redpacket_notice($this->member_info['member_id']);
        Model('redpacket_notice')->update_notice_status(array('member_id'=>$this->member_info['member_id']));
        output_data($list);
    }

}
