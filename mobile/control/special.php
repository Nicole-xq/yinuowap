<?php
/**
 * 专场详情
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class specialControl extends mobileHomeControl
{
	public function __construct()
	{
		parent::__construct();
	}

	public function special_detailsOp(){
		if(!$_POST['special_id']){
			output_error('参数错误');
		}
		$special_id = $_POST['special_id'];
		$model_special = Model('auction_special');
		$model_auctions = Model('auctions');
		$special_info = $model_special->getSpecialInfo(array('special_id' => $special_id));
		if($special_info['special_state'] == 10 || $special_info['special_state'] == 30){
			output_error('未审核通过的专场');
		}
        $special_info['wap_image_path'] = getVendueLogo($special_info['wap_image']);
        $special_info['special_rate_date'] = date("Y-m-d",$special_info['special_rate_time']);
        $special_info['day_num'] = Logic('auction')->getInterestDay($special_info['special_rate_time'],$special_info['special_end_time']);//计息天数
        $special_info['special_remain_time_end'] = $special_info['special_end_time'] - TIMESTAMP >0 ? $special_info['special_end_time'] - TIMESTAMP : 0;
        $special_info['special_remain_time_start'] = $special_info['special_start_time'] - TIMESTAMP >0 ? $special_info['special_start_time'] - TIMESTAMP : 0;
        $special_info['special_remain_time_rate'] = $special_info['special_rate_time'] - TIMESTAMP >0 ? $special_info['special_rate_time'] - TIMESTAMP : 0;
        if(time() >= $special_info['special_start_time'] && $special_info['special_end_time']> time()) {
            $special_info['is_kaipai'] = 1;
        }elseif($special_info['special_end_time']<= time()){
            $special_info['is_kaipai'] = 2;
        }elseif($special_info['special_start_time'] > time()){
            $special_info['is_kaipai'] = 0;
        }
        if($special_info['special_rate_time'] > time()){
            $special_info['is_kaipai'] = 3;
        }
        // 如果已登录 判断该店铺是否已被关注
        $memberId = $this->getMemberIdIfExists();
        if ($memberId) {
            $c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($special_info['store_id'], $memberId);
            $special_info['is_favorate'] = $c > 0;
        } else {
            $special_info['is_favorate'] = false;
        }
		$auction_list = $model_auctions->getAuctionList(array('special_id' => $special_id));
        $all_bid_number = 0;
        $piece_num = 0;
        $num_of_applicants = 0;//报名人数
        $all_price = 0;
        $special_click = 0;
        $collect = 0;
		if(!empty($auction_list)){
			foreach($auction_list as $key => &$value){
				$auction_id = $value['auction_id'];
                $value['current_price'] = $value['current_price'] != 0.00 ? $value['current_price'] : $value['auction_start_price'];
                $value['auction_image_path'] = cthumb($value['auction_image'],360);

                $all_bid_number += $value['bid_number'];
                $num_of_applicants += $value['num_of_applicants'];
                $collect += $value['auction_collect'];
                $special_click += $value['auction_click'];
                if($value['is_liupai'] == 0 && $value['auction_end_true_t'] <=time()){
                    $piece_num += 1;
                    $all_price += $value['current_price'];
                }
                $cur_goods_info = Model('goods')->getGoodsInfo(['goods_id'=>$value['goods_id']], 'goods_marketprice');
                $value['goods_marketprice'] = $cur_goods_info['goods_marketprice'];
                //收藏  & 提醒
                if ($memberId) {
                    $c = (int)Model('favorites')->getGoodsFavoritesCountByAuctionId($auction_id, $memberId);
                    $value['is_favorate'] = $c > 0;
                    $relation_info = Model('auction_member_relation')->getRelationInfo(['member_id'=>$memberId,'auction_id'=>$auction_id],'is_remind,relation_id');
                    $value['is_remind'] = $relation_info['is_remind'] ? true : false ;
                    $value['relation_id'] = $relation_info['relation_id'];
                }
			}
			unset($value);
		}
        $special_info['all_bid_number'] = $all_bid_number;
        $special_info['num_of_applicants'] = $num_of_applicants;
        $special_info['special_click'] = $special_click;
        $special_info['collect'] = $collect;
        $special_info['jian_num'] = $piece_num;
        $special_info['all_price'] = $all_price;

		output_data(array('special_info' => $special_info , 'special_goods_list' => $auction_list));
	}

    /**
     * 专场列表
     */
	public function get_special_listOp(){
	    /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        $condition = 'special_rate > 0 and special_rate_time > '.time().' and special_end_time > special_rate_time';
        $limit = '';
        if((int) $_GET['limit']) $limit = (int) $_GET['limit'];
        $special_list = $model_special->getSpecialOpenList($condition, '*,(special_end_time-special_rate_time) as day_num', 'special_type desc,day_num,special_id desc',$limit);
        $store_list = [];
        foreach($special_list as $key => &$special_info){
            $special_list[$key]['day_num'] = ceil($special_info['day_num']/(3600*24));
            $special_list[$key]['wap_image_path'] = getVendueLogo($special_info['wap_image']);
            $special_list[$key]['special_remain_rate_time'] = $special_info['special_rate_time']-TIMESTAMP;

            if (!$store_list[$special_info['store_id']]){
                $store_label = Model("store")->getStoreDetail(['store_id'=>$special_info['store_id']],'store_label','store_label');
                $store_list[$special_info['store_id']]['store_label'] = $store_label;
                $store_list[$special_info['store_id']]['store_logo'] = getStoreLogo($store_label,'store_logo');
            }
            $special_list[$key]['store_logo'] = $store_list[$special_info['store_id']]['store_logo'];

            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$special_info['special_id']],'auction_id,auction_click,bid_number');
            $special_click = 0;
            foreach($special_auction_list as $k=>$v){
                $special_click += $v['auction_click'];
            }
            $special_list[$key]['special_click'] = $special_click;
        }

        output_data(['list'=>$special_list]);
    }

}