<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .success { width: 100%; text-align: center; padding: 200px 0 10px 0; color: green; }
    .fail { width: 100%; text-align: center; padding: 200px 0 10px 0; color: red; }
    .return { width: 100%; text-align: center; }
</style>
<script>window.demo.checkPaymentAndroid("<?php echo $output['result'];?>");</script>
<div class="<?php echo $output['result'];?>" >
<?php echo $output['message'];?>
</div>
<div class="return" >
    <?php if($output['order_type'] == 'p'): ?>
    <a href="<?php echo WAP_SITE_URL;?>/tmpl/member/member.html"><img src="<?php echo WAP_SITE_URL;?>/images/pay_ok.png"></a>
    <?php else: ?>
    <a href="<?php echo WAP_SITE_URL;?>/tmpl/member/order_list.html"><img src="<?php echo WAP_SITE_URL;?>/images/pay_ok.png"></a>
    <?php endif; ?>
</div>
