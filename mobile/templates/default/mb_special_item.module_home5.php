<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<div class="content-box-01">
        <?php if(!empty($vv['title'])) {?>
            <div class="title-name">
                <h1><?php echo $vv['title']; ?></h1>
                <i class="all-block"></i>
            </div>
        <?php } ?>
        
        <div class="row no-gutter">
            <?php $i = 0;?>            
            <?php foreach ((array) $vv['item'] as $item) { ?>
                <a nctype="btn_item" href="javascript:;" data-type="<?php echo $item['type']; ?>" data-data="<?php echo $item['data']; ?>" <?php if($i==0 || $i==1):?>class="col-50"<?php else:?>class="col-25"<?php endif;?>><img src="<?php echo $item['image']; ?>" alt=""></a>
            <?php $i++;} ?>
        </div>
        <div class="clear"></div>
    </div>