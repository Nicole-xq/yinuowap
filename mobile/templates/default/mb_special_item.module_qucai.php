<?php defined('InShopNC') or exit('Access Invalid!'); ?>
<div class="content-box-04">
   <?php if(!empty($vv['title'])) {?>
        <div class="title-name">
            <h1><?php echo $vv['title']; ?></h1>
            <i class="all-block"></i>
        </div>
    <?php } ?>
     <?php foreach ((array) $vv['item'] as $item) { ?>
    <div class="row no-gutter">
            <a nctype="btn_item" href="javascript:;" data-type="guess" data-data="<?php echo $item['gs_id']; ?>" class="col-100">
                <img src="<?php echo $item['gs_img'];?>" alt="">
                <div>
                    <div class="content-detail fl">
                        <h1><?php echo $item['gs_name']; ?></h1>
                        <span>竞猜区间&nbsp;&nbsp;¥<?php echo $item['gs_min_price']; ?>-<?php echo $item['gs_max_price']; ?></span>
                        <span>距离结束&nbsp;&nbsp;
                        <span class="time-remain settime" endtime="<?php echo $item['end_time']; ?>" style="display: inline;color: #800000 !important;">距结束&nbsp;<em time_id="d">0</em>日<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒</span>
                    </span></div>
                    <div class="fr"><b><?php echo $item['joined_times'];?></b>次竞猜</div>
                </div>
            </a>
        </div>
     <?php } ?>
        <div class="clear"></div>
    </div>
