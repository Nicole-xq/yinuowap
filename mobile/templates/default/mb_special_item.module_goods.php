<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div class="content-box-02">
 <?php if(!empty($vv['title'])) {?>
    <div class="title-name">
        <h1><?php echo $vv['title']; ?></h1>
        <i class="all-block"></i>
    </div>
<?php } ?>

    <div class="clear"></div>
    <div class="row no-gutter"> 
        
         <?php foreach ((array) $vv['item'] as $item) { ?>
        <a nctype="btn_item" href="javascript:;" data-type="goods" data-data="<?php echo $item['goods_id']; ?>" class="col-50">
            <img src="<?php echo $item['goods_image']; ?>" alt="">
            <div class="content-detail">
                <h1 class="w100"><?php echo $item['goods_name']; ?></h1>
                <span class="theme-color"><b>¥<?php echo $item['goods_promotion_price']; ?></b></span>
            </div>
        </a>
    <?php } ?>        
    </div>
    <div class="clear"></div>
    <a href="./tmpl/product_list.html" class="index-more" style="margin-bottom:56px">查看更多&nbsp;&gt;</a>
</div>