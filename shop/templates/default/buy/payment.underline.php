<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
 <link href="<?php echo SHOP_TEMPLATES_URL?>/css/seller_center.css" rel="stylesheet" type="text/css">

<style>
body{background: #fff}
.wxpayment { border-top: 2px solid #4b5b78; padding: 12px 30px 0; border: 1px solid #eee; color: #777; }
.p-w-bd::after,
.pay-weixin::after { clear: both; content: ""; display: table; }
.p-w-hd { font-family: "Microsoft Yahei"; font-size: 18px; margin-bottom: 20px; }
.p-w-bd { margin-bottom: 30px; padding-left: 130px; }
.pw-box-hd img { border: 1px solid #ddd; }
.p-w-box { float: left; width: 300px; }
.payment-change .pc-wrap { display: block; height: 56px; line-height: 56px; padding: 0 20px; transition: all 0.1s ease 0s; }
.payment-change .pc-wrap .pc-w-arrow-left { float: left; margin-right: 15px; }
.payment-change .pc-wrap .pc-w-arrow-left,
.payment-change .pc-wrap .pc-w-arrow-right { color: #2fa1dd; float: right; font-family: "宋体"; font-size: 22px; font-style: normal; text-align: center; width: 20px; }
.pw-box-ft { background: url("<?php echo SHOP_SITE_URL?>/templates/default/images/payment/icon-red.png") no-repeat scroll 50px 8px #ff7674; height: 44px; padding: 8px 0 8px 125px; }
.p-w-sidebar { background: url("<?php echo SHOP_SITE_URL?>/templates/default/images/payment/phone-bg.png") no-repeat scroll 50px 0 rgba(0, 0, 0, 0); float: left; height: 421px; margin-top: -20px; padding-left: 50px; width: 379px; }
.pw-box-ft p { color: #fff; font-size: 14px; font-weight: 700; line-height: 22px; margin: 0; }
.payment-change .pc-wrap .pc-w-arrow-left { float: left; margin-right: 15px; }
.payment-change .pc-wrap strong { color: #2ea7e7; cursor: pointer; float: left; font-size: 14px; margin-right: 30px; }
</style>
<div class="ncc-main">
  <div class="ncc-title">
    <h3><?php echo '线下支付';?></h3>
    <?php if (!isset($output['order_list'][0]['pdr_id'])): ?>
      <h5>订单详情内容可通过查看<a href="index.php?act=member_order" target="_blank">我的订单</a>进行核对处理。</h5>
    <?php endif;?>
  </div>
  <div class="ncc-receipt-info">
    <div class="ncc-receipt-info-title">
      <h3><?php echo (!(isset($output['order_list'][0]['pdr_id']))) ? "订单" : "充值单";?>提交成功，请您尽快提交支付凭证。 应付金额：<strong><?php echo ncPriceFormat($output['api_pay_amount']);?></strong>元 </h3>
    </div>
  </div>
      <table class="ncc-table-style">
        <thead>
          <tr>
            <th class="w50"></th>
            <th class="w200 tl"><?php echo (!(isset($output['order_list'][0]['pdr_id']))) ? "订单号" : "充值单号";?></th>
            <th class="tl">金额(元)</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($output['order_list']) > 1) { ?>
          <tr>
            <th colspan="20">由于您的商品由不同商家发出，此单将分为<?php echo count($output['order_list']);?>个不同子订单配送！</th>
          </tr>
          <?php } ?>
          <?php foreach ($output['order_list'] as $key => $order_info) { ?>
          <tr>
            <td></td>
            <td class="tl"><?php echo isset($order_info['pdr_sn']) ? $order_info['pdr_sn'] : $order_info['order_sn']; ?></td>
            <td class="tl"><?php echo isset($order_info['pdr_amount']) ? $order_info['pdr_amount'] : $order_info['order_amount'];?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div style="padding-left:82px">
          <p>银行卡号：<?php echo $output['payment_info']['number'];?></p>
          <p><br/></p>
          <p>银行：<?php echo $output['payment_info']['bank'];?></p>
          <p><br/></p>
          <p>开户人：<?php echo $output['payment_info']['card_user'];?></p>
          <p><br/></p>
      </div>
  <div class="wxpayment"> 
     <form action="index.php?act=payment&op=pay_underline_order" id="paymentForm" method="post">
      <input type='hidden' name='pay_sn' value="<?php echo $output['pay_sn'];?>"/>
         <input type="hidden" name="order_type" value="<?=$output['order_type'];?>">
      <input type='hidden' name='pay_voucher' value="" id="pay_voucher"/>
      <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb">
               <img id="pay_voucher_img" nctype="goods_image" src="<?php echo thumb('', 240);?>"/> 
              </div>
              <span></span>
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="pay_voucher_upload">
                  </span>
                  <p><i class="icon-upload-alt"></i>图片上传</p>
                  </a> </div>
              </div>
            </div>
          </div>
      <div style="margin:20px 0">
        <input type="submit" value="提交凭证"/>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
      $('input[name="pay_voucher_upload"]').fileupload({
        dataType: 'json',
        url: '<?php echo urlShop('payment', 'ajax_upload_image');?>',
        formData: '',
        add: function (e,data) {
            data.submit();
        },
        done: function (e,data) {
            if (!data.result){
              alert('上传失败，请尝试上传小图或更换图片格式');return;
            }
            if(data.result.state) {
              $('#pay_voucher_img').attr('src',data.result.pic_url);
              $('#pay_voucher').val(data.result.pic_name);
            } else {
              alert(data.result.message);
            }
        },
        fail: function(){
          alert('上传失败，请尝试上传小图或更换图片格式');
        }
    });
    $('#paymentForm').validate({
        errorPlacement: function(error, element){
            element.nextAll('span').first().after(error);
        },
        rules : {
            pay_voucher: {
                required: true
            },
        },
        messages : {
            pay_voucher: {
                required: '请上传支付凭证'
            }
        },
         showErrors : function(errorMap, errorList) {  
          var msg = "";  
          $.each(errorList, function(i, v) {  
            msg += (v.message + "\r\n");  
          });  
          if (msg != "")  
           alert(msg);  
          }  
      });
  });
</script>