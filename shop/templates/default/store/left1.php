<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
	.auction-detail-left .slide-pic ul li .title{bottom:22px}
</style>
<div class="auction-detail-left">   
<div class="box-01">
	  <div class="title shop-name">
		  <h4 class="fl"><?php echo $output['store_info']['store_name']?></h4><?php if(!empty($output['store_info']['store_qq'])){?>
    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $output['store_info']['store_qq'];?>&site=qq&menu=yes" title="QQ: <?php echo $output['store_info']['store_qq'];?>"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo $output['store_info']['store_qq'];?>:52" style=" vertical-align: middle;"/></a>
    <?php }?>
    <?php if(!empty($output['store_info']['store_ww'])){?>
    <a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&amp;uid=<?php echo $output['store_info']['store_ww'];?>&site=cntaobao&s=1&charset=<?php echo CHARSET;?>" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=<?php echo $output['store_info']['store_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" alt="<?php echo $lang['nc_message_me'];?>" style=" vertical-align: middle;"/></a>
    <?php }?>
	  </div>
		<div class="clear"></div>
		<div class="shop-info">
			<h1 class="fl"><?php echo $output['store_info']['store_credit_average'];?></h1>
			<p class="fl">
				<span>商品评价：<?php echo $output['store_info']['store_credit']['store_desccredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
				<span>服务态度：<?php echo $output['store_info']['store_credit']['store_servicecredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
				<span>物流速度：<?php echo $output['store_info']['store_credit']['store_deliverycredit']['credit'];?>&nbsp;<i class="fa fa-long-arrow-up" aria-hidden="true"></i></span>
			</p>
		</div>
		<div class="clear"></div>
		<div class="shop-other">
			<a href="<?php echo urlShop('show_store', 'index', array('store_id' => $output['store_info']['store_id']), $output['store_info']['store_domain']);?>"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;进店逛逛</a>
			<a href="javascript:collect_store('<?php echo $output['store_info']['store_id'];?>','count','store_collect')" id="if_store_fav" <?php if($output['if_store_favorites']):?> class="focus_on"<?php endif;?><i class="fa fa-star" aria-hidden="true"></i>&nbsp;<font id="store_fav_desc"><?php if($output['if_store_favorites']):?>已关注<?php else:?>关注店铺<?php endif;?></font></a>
		</div>
	</div>
<div class="clear"></div>
<div class="box-01">
	  <div class="title shop-name">
		  <h4 style="font-size: 14px; color: #505050">店内搜索</h4></div>
		  <div class="clear"></div>
		   <form id="" name="searchShop" method="get" action="index.php" >
		     <input type="hidden" name="act" value="show_store" />
        	 <input type="hidden" name="op" value="goods_all" />
        	 <input type="hidden" name="store_id" value="<?php echo $output['store_info']['store_id'];?>" />
			  <label for=""><span>关键词:</span>&nbsp;&nbsp;<input type="text" class="text w120" name="inkeyword" value="<?php echo $_GET['inkeyword'];?>" placeholder="搜索店内商品" autocomplete="off" maxlength="100"></label>
			  <label for=""><span>&nbsp;&nbsp;&nbsp;价格:</span>&nbsp;&nbsp;<input type="text" name="price_from" placeholder="" autocomplete="off" maxlength="100" style="width: 50px">&nbsp;到&nbsp;<input type="text" name="price_to" placeholder="" autocomplete="off" maxlength="100" style="width: 50px"></label>
			  <input type="submit" value="搜 索" onclick="javascript:document.searchShop.submit();">
		  </form>
	</div>
<div class="clear"></div>
<div class="buyers-help">
	<div class="title shop-name">
		  <h4 style="font-size: 14px; color: #505050">店内分类</h4></a>
	  </div>
	<ul id="accordion" class="accordion fl">
		<?php foreach($output['goods_class_list'] as $val):?>
		<li>
			<div class="link"><?php echo $val['stc_name'];?><i class="fa fa-angle-down" aria-hidden="true"></i></div>
			<ul class="submenu">
			<?php foreach($val['children'] as $value):?>
				<li><a href="<?php echo urlShop('show_store', 'goods_all', array('store_id' => $value['store_id'], 'stc_id' => $value['stc_id']));?>"><?php echo $value['stc_name'];?></a></li>
			<?php endforeach;?>
			</ul>
		</li>
	<?php endforeach; ?>
	</ul>
</div>
<div class="clear"></div>
<div id="vertical-slide">
	<h1><span>精品推荐</span></h1>
	<div class="clear"></div>
	<div class="slide-pic" id="slidePic">
			<div class="vertical-slide-pic">
				<ul>
				 <?php if(is_array($output['hot_sales']) && !empty($output['hot_sales'])){?>
        			<?php foreach($output['hot_sales'] as $val){?>
					<li>
					  <a href="<?php echo urlShop('goods', 'index',array('goods_id'=>$val['goods_id']));?>">
					  	  <img src="<?php echo thumb($val, 240);?>" alt="<?php echo $val['goods_name']?>"/>
						  <div class="title">
						  <div class="info"><span class="fr"><b class="red-word"><?php if($val['sales_model'] == 2){echo '可议价';} else {echo '￥',ncPriceFormat($val['goods_promotion_price']);} ?></b></span></div>
						  </div>
					  </a>
					</li>
       				 <?php }?>
     			 <?php }?>
			</ul>
		    </div>
			<a class="gray" id="prev" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
			<a id="next" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		  </div>
	<div class="clear"></div>
</div>
</div>
<!--商品轮播js-->
<script type="text/javascript">
		jQuery(function() {
			if (!$('#slidePic')[0]) return;
			var i = 0,
				p = $('#slidePic ul'),
				pList = $('#slidePic ul li'),
				len = pList.length;
			var elePrev = $('#prev'),
				eleNext = $('#next');
			var w = 196,
				num = 4;
			p.css('width', w * len);
			if (len <= num) eleNext.addClass('gray');
			function prev() {
				if (elePrev.hasClass('gray')) {
					return
				}
				p.animate({
					marginTop: -(--i) * w
				}, 500);
				if (i < len - num) {
					eleNext.removeClass('gray')
				}
				if (i == 0) {
					elePrev.addClass('gray')
				}
			}
			function next() {
				if (eleNext.hasClass('gray')) {
					return
				}
				p.animate({
					marginTop: -(++i) * w
				}, 500);
				if (i != 0) {
					elePrev.removeClass('gray')
				}
				if (i == len - num) {
					eleNext.addClass('gray')
				}
			}
			elePrev.bind('click', prev);
			eleNext.bind('click', next);
		});
</script>
<script>
$(function() {
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;
		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}
	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();
		$next.slideToggle();
		$this.parent().toggleClass('open');
		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	
	var accordion = new Accordion($('#accordion'), false);
});
</script>