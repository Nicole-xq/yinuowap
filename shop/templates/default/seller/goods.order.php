<?php defined('InShopNC') or exit('Access Invalid!'); ?>

<div style="margin-left: 5%;width: 90%;height: 150px;">
    <hr>
    <br>
    <table>
        <tr>
            <td><b>设定当前位置</b>&nbsp;&nbsp;</td>
            <td><input id="artist_sort" type="text" value="<?=$output['goods_info']['artist_sort']?>" name="goods_sort" style="width: 80px"></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><span class="artist_sort_notice">位置范围1~10，0或空取消位置</span></b></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" onclick="save_artist_sort()" ></td>
            <td></td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    function save_artist_sort() {
        var sort = $('#artist_sort').val();
        var commentid = "<?=$output['goods_info']['commonid']?>";
        var save_url = 'index.php?act=store_goods_online&op=edit_save_artist_sort&commentid=' + commentid + '&sort=' + sort ;
        if (0 <= sort && sort <= 10) {
            $.getJSON('index.php?act=store_goods_online&op=check_sort&commentid=' + commentid + '&sort=' + sort , function (data) {
                if(data.code){
                    save_url += '&commentid2=' + data.commonid;
                    showDialog('该位置已有商品，确定替换吗？', 'confirm', '', function(){
                        ajaxget(save_url);
                        setTimeout(function () {$(".dialog_close_button", parent.document).click();}, 2000);
                    });
                } else {
                    ajaxget(save_url);
                    setTimeout(function () {$(".dialog_close_button", parent.document).click();}, 2000);
                }
            });
        } else {
            $('.artist_sort_notice').css('color','red');
        }
    }
</script>