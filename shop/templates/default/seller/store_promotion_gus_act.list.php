<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a class="ncbtn ncbtn-mint" href="<?php echo urlShop('store_promotion_guess', 'guess_act_add');?>"><i class="icon-plus-sign"></i>添加活动</a>
</div>
<form method="get">
  <table class="search-form">
    <input type="hidden" name="act" value="store_promotion_guess" />
    <input type="hidden" name="op" value="guess_act_list" />
    <tr>
      <td>&nbsp;</td>
      <th>状态</th>
      <td class="w100"><select name="state">
          <?php foreach ((array) $output['gusActStates'] as $key=>$val) { ?>
          <option value="<?php echo $key;?>" <?php if(intval($key) === intval($_GET['state'])) echo 'selected';?>><?php echo $val;?></option>
          <?php } ?>
        </select></td>
      <th class="w110">活动名称</th>
      <td class="w160"><input type="text" class="text w150" name="gs_name" value="<?php echo $_GET['cou_name'];?>"/></td>
      <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label></td>
    </tr>
  </table>
</form>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w30"></th>
      <th class="tl">活动名称</th>
      <th class="w120">活动图片</th>
      <th class="w180">开始时间</th>
      <th class="w180">结束时间</th>
      <th class="w150"><?php echo $lang['nc_handle'];?></th>
    </tr>
  </thead>
  <?php if(!empty($output['list']) && is_array($output['list'])){?>
  <?php foreach($output['list'] as $key=>$val){?>
  <tbody id="cou_list">
    <tr class="bd-line">
      <td></td>
      <td class="tl"><dl class="goods-name">
          <dt><?php echo $val['name'];?></dt>
        </dl></td>
        <?php 
          if (empty($val['gs_act_img'])) {
            $img = UPLOAD_SITE_URL.DS.'shop/common/default_goods_image_240.gif';
          } else {
            $img = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess'.DS.$val['gs_act_img'];
          }
        ?>
      <td> <a href="javascript:;" class="icon-picture" onmouseout="toolTip()" onmouseover="toolTip('<img src=<?php echo $img;?>>')">
<i class="fa fa-picture-o"></i></a></td>
      <td class="goods-time"><?php echo date("Y-m-d H:i",$val['tstart']);?></td>
      <td class="goods-time"><?php echo date("Y-m-d H:i",$val['tend']);?></td>
      <td class="nscs-table-handle tr">
      <?php if($val['state'] == 20 || $val['state'] == 32):?>
          <span>
              <a href="index.php?act=store_promotion_guess&op=guess_list&gus_act_id=<?php echo $val['id'];?>" class="btn-bluejeans">
                  <i class="icon-cog"></i>
                  <p>管理</p>
              </a>
          </span>
      <?php endif;?>
      <?php if($val['tstart'] > time() || $val['state'] == 10):?>
          <span>
              <a href="javascript:;" nctype="btn_del_gs_act" data-gs-act-id=<?php echo $val['id'];?> class="btn-grapefruit">
                  <i class="icon-trash"></i>
                  <p><?php echo $lang['nc_delete'];?></p>
              </a>
          </span>
      <?php else:?>
      <span></span>
      <?php endif;?>
      </td>
  </tr>
  <?php } ?>
  <?php } else { ?>
  <tr id="cou_list_norecord">
      <td class="norecord" colspan="20"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
  </tr>
  <?php }?>
  </tbody>
  <tfoot>
    <?php if(!empty($output['list']) && is_array($output['list'])){?>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
    <?php } ?>
  </tfoot>
</table>
<form id="submit_form" action="" method="post" >
  <input type="hidden" id="gs_act_id" name="gs_act_id" value="">
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('[nctype="btn_del_gs_act"]').on('click', function() {
            if(confirm('<?php echo $lang['nc_ensure_del'];?>')) {
                var action = "<?php echo urlShop('store_promotion_guess', 'guess_act_del');?>";
                var guess_id = $(this).attr('data-gs-act-id');
                $('#submit_form').attr('action', action);
                $('#gs_act_id').val(guess_id);
                ajaxpost('submit_form', '', '', 'onerror');
            }
        });
    });
</script>
