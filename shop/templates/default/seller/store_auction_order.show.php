<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="ncsc-oredr-show">
    <div class="ncsc-order-info">
        <div class="ncsc-order-details">
            <div class="title"><?php echo $lang['store_show_order_info'];?></div>
            <div class="content">
                <dl>
                    <dt><?php echo $lang['store_show_order_receiver'].$lang['nc_colon'];?></dt>
                    <dd>
                        <?php echo $output['order_info']['margin_info']['reciver_name'];?>&nbsp;
                        <?php echo @$output['order_info']['margin_info']['buyer_phone'];?>&nbsp;
                        <?php echo @$output['order_info']['margin_info']['area_info'];?>
                        <?php echo @$output['order_info']['margin_info']['address'];?>
                    </dd>
                </dl>
                <?php if($output['order_info']['payment_name']) { ?>
                    <dl>
                        <dt><?php echo $lang['store_order_pay_method'].$lang['nc_colon'];?></dt>
                        <dd> <?php echo $output['order_info']['payment_name']; ?>
                        </dd>
                    </dl>
                <?php } ?>
                <dl>
                    <dt>发&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;票：</dt>
                    <dd>
                        <?php foreach ((array)$output['order_info']['invoice_info'] as $key => $value){?>
                            <span><?php echo $key;?> (<strong><?php echo $value;?></strong>)</span>
                        <?php } ?>
                    </dd>
                </dl>

                <dl class="line">
                    <dt><?php echo $lang['store_order_order_sn'].$lang['nc_colon'];?></dt>
                    <dd><?php echo $output['order_info']['auction_order_sn']; ?><a href="javascript:void(0);">更多<i class="icon-angle-down"></i>
                            <div class="more"><span class="arrow"></span>
                                <ul>
                                    <li><span><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']); ?></span>买家下单</li>
                                    <?php if (is_array($output['order_log_list'])) { ?>
                                        <?php foreach($output['order_log_list'] as $log_info) { ?>
                                            <li><span><?php echo date('Y-m-d H:i:s',$log_info['log_time']);?></span><?php echo $log_info['log_role'];?> <?php echo $log_info['log_msg'];?></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </a>
                    </dd>
                </dl>
                <dl>
                    <dt></dt>
                    <dd></dd>
                </dl>
            </div>
        </div>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_CANCEL) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-off orange"></i>订单状态：</dt>
                    <dd>交易关闭</dd>
                </dl>
                <ul>
                    <li>
                        <?php if ($output['order_info']['close_reason']) { ?>
                            <?php echo $output['order_info']['close_reason'];?> 于 <?php echo date('Y-m-d H:i:s',$output['order_info']['close_time']);?> <?php echo $output['order_info']['close_info']['log_msg'];?>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        <?php } ?>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_NEW) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-ok-circle green"></i>订单状态：</dt>
                    <dd>订单已经提交，等待买家付款</dd>
                </dl>
                <ul>
                    <li>1. 买家尚未对该订单进行支付。</li>
                    <?php if (!$output['order_info']['api_pay_time']) { ?>
                        <li>2. 如果买家未对该笔订单进行支付操作，系统将于
                            <time><?php echo date('Y-m-d H:i:s',$output['order_info']['order_cancel_day']);?></time>
                            自动关闭该订单。</li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_PAY) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-ok-circle green"></i>订单状态：</dt>
                    <dd>
                        订单已提交，等待发货
                    </dd>
                </dl>
                <ul>
                    <li>1. 买家已使用“<?php echo orderPaymentName($output['order_info']['payment_code']);?>”方式成功对订单进行支付，支付单号 “<?php echo $output['order_info']['auction_order_sn'];?>”。</li>
                    <li>2. 订单已提交商家进行备货发货准备。</li>
                    <li>3. 如果商家在买家支付成功三天后仍未发货将视为商家违约，商城将扣除商家拍卖保证金。</li>
                </ul>
            </div>
        <?php } ?>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_SEND) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-ok-circle green"></i>订单状态：</dt>
                    <dd>已发货</dd>
                </dl>
                <ul>
                    <li>1. 商品已发出；
                        <?php if ($output['order_info']['invoice_no'] != '') { ?>
                            物流公司：<?php echo $output['order_info']['express_info']['e_name']?>；单号：<?php echo $output['order_info']['invoice_no'];?>。
                            <?php if ($output['order_info']['if_deliver']) { ?>
                                查看 <a href="#order-step" class="blue">“物流跟踪”</a> 情况。
                            <?php } ?>
                        <?php } else { ?>
                            无需要物流。
                        <?php } ?>
                    </li>
                </ul>
            </div>
        <?php } ?>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_SUCCESS) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-ok-circle green"></i>订单状态：</dt>
                    <dd>已经收货。</dd>
                </dl>
                <ul>
                    <li>1. 买家收货完成，交易结束。</li>
                </ul>
            </div>
        <?php } ?>
        <?php if ($output['order_info']['order_state'] == ORDER_STATE_UPLOAD_EVIDENCE) { ?>
            <div class="ncsc-order-condition">
                <dl>
                    <dt><i class="icon-ok-circle green"></i>订单状态：</dt>
                    <dd>等待平台审核凭证</dd>
                </dl>
                <div>
                    <img style="height:150px" src="<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_voucher'.DS.$output['order_info']['pay_voucher'];?>">
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if ($output['order_info']['order_state'] != ORDER_STATE_CANCEL) { ?>
        <div id="order-step" class="ncsc-order-step">
            <dl class="step-first <?php if ($output['order_info']['order_state'] != ORDER_STATE_CANCEL) echo 'current';?>">
                <dt>提交订单</dt>
                <dd class="bg"></dd>
                <dd class="date" title="<?php echo $lang['store_order_add_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['add_time']); ?></dd>
            </dl>
            <?php if ($output['order_info']['payment_code'] != 'offline') { ?>
                <dl class="<?php if(intval($output['order_info']['payment_time']) && $output['order_info']['order_pay_state'] !== false) echo 'current'; ?>">
                    <dt>支付订单</dt>
                    <dd class="bg"> </dd>
                    <dd class="date" title="<?php echo $lang['store_show_order_pay_time'];?>"><?php echo intval(date("His",$output['order_info']['payment_time'])) ? date("Y-m-d H:i:s",$output['order_info']['payment_time']) : date("Y-m-d",$output['order_info']['payment_time']); ?></dd>
                </dl>
            <?php } ?>
            <dl class="<?php if($output['order_info']['ship_time']) echo 'current'; ?>">
                <dt>商家发货</dt>
                <dd class="bg"> </dd>
                <dd class="date" title="<?php echo $lang['store_show_order_send_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['ship_time']); ?></dd>
            </dl>
            <dl class="<?php if(intval($output['order_info']['finnshed_time'])) { ?>current<?php } ?>">
                <dt>确认收货</dt>
                <dd class="bg"> </dd>
                <dd class="date" title="<?php echo $lang['store_show_order_finish_time'];?>"><?php echo date("Y-m-d H:i:s",$output['order_info']['finnshed_time']); ?></dd>
            </dl>
        </div>
    <?php } ?>
    <div class="ncsc-order-contnet">
        <table class="ncsc-default-table order">
            <thead>
            <tr>
                <th class="w10">&nbsp;</th>
                <th colspan="2"><?php echo $lang['store_show_order_goods_name'];?></th>
                <th class="w120"><?php echo $lang['store_show_order_price'];?></th>
                <th class="w200"><strong>实付 * 佣金比 = 应付佣金(元)</strong></th>
                <th class="w100">交易操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($output['order_info']['invoice_no'])) { ?>
                <tr>
                    <th colspan="6" style="border-right: 0;">
                        <div class="order-deliver">
                            <span>物流公司：
                                <a target="_blank" href="<?php echo $output['order_info']['express_info']['e_url'];?>"><?php echo $output['order_info']['express_info']['e_name'];?></a>
                            </span>
                            <span>
                                <?php echo $lang['store_order_shipping_no'].$lang['nc_colon'];?> <?php echo $output['order_info']['invoice_no']; ?>
                            </span>
                            <span>
                                <a href="javascript:void(0);" id="show_shipping">物流跟踪<i class="icon-angle-down"></i>
              <div class="more">
                  <span class="arrow">

                  </span>
                <ul id="shipping_ul">
                  <li>加载中...</li>
                </ul>
              </div>
              </a></span> </div></th>
                    <th colspan="3" style=" border-left: 0;">

                    </th>
                </tr>
            <?php } ?>

                <tr class="bd-line">
                    <td>&nbsp;</td>
                    <td class="w50">
                        <div class="pic-thumb">
                            <a target="_blank" href="<?php echo $output['order_info']['extend_order_goods']['auction_url'];?>">
                                <img src="<?php echo $output['order_info']['extend_order_goods']['image_60_url']; ?>" />
                            </a>
                        </div>
                    </td>
                    <td class="tl">
                        <dl class="goods-name">
                            <dt>
                                <a target="_blank" href="<?php echo $output['order_info']['extend_order_goods']['auction_url']; ?>">
                                    <?php echo $output['order_info']['auction_info']['auction_name']; ?>
                                </a>
                            </dt>
                        </dl>
                    </td>
                    <td><?php echo ncPriceFormat($output['order_info']['order_amount']); ?>
                        <p class="green">
                            <?php if ($output['order_info']['refund_state'] == 2) {?>
                                <?php echo $output['order_info']['order_amount'];?><span>退</span>
                            <?php } ?>
                        </p>
                    </td>

                    <td class="commis bdl bdr">
                        <?php if ($output['order_info']['commis_rate'] != 200) { ?>
                            <?php echo $output['order_info']['order_amount']; ?> * <?php echo $output['order_info']['commis_rate']; ?>% = <b><?php echo ncPriceFormat($output['order_info']['order_amount']*$output['order_info']['commis_rate']/100); ?></b>
                        <?php } ?></td>

                    <!-- S 合并TD -->
                        <td class="bdl bdr" rowspan="1">
                            <?php echo $output['order_info']['state_desc']; ?>
                            <?php if ($output['order_info']['if_lock']) { ?>
                                <p>退款中</p>
                            <?php } ?>

                            <!-- 发货 -->
                            <?php if ($output['order_info']['if_store_send']) { ?>

                                <p><a href="javascript:void(0)"
                                      class="ncbtn ncbtn-mint mt10"
                                      nc_type="dialog"
                                      dialog_title="发货信息"
                                      dialog_id="auction_send"
                                      dialog_width="480"
                                      uri="<?php echo urlShop('store_auction_order', 'send', array('order_id' => $output['order_info']['auction_order_id']));?>"
                                      title="<?php echo $lang['store_order_send'];?>">
                                        <i class="icon-truck"></i><?php echo $lang['store_order_send'];?>
                                    </a>
                                </p>
                            <?php } ?>

                            <!-- 审核退款 -->
                            <?php if ($output['order_info']['if_store_cancel']) { ?>

                                <p>
                                    <a href="javascript:void(0)"
                                       class="ncbtn ncbtn-grapefruit mt10"
                                       nc_type="dialog"
                                       dialog_title="审核退款"
                                       dialog_id="auction_cancel"
                                       dialog_width="480"
                                       uri="<?php echo urlShop('store_auction_order', 'cancel', array('order_id' => $output['order_info']['auction_order_id'], 'order_sn' => $output['order_info']['auction_order_sn']));?>"
                                       title="确认退款"
                                    >
                                        <i class="icon-edit"></i>审核退款
                                    </a>
                                </p>
                            <?php } ?>
                        </td>
                    <!-- E 合并TD -->
                </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="20"><dl class="freight">
                        <dd>
                            <?php if($output['order_info']['refund_state'] > 0) { ?>
                                (<?php echo $lang['store_order_refund'];?>:<?php echo $lang['currency'].$output['order_info']['order_amount'];?>)
                            <?php } ?>
                        </dd>
                    </dl>
                    <dl class="sum">
                        <dt><?php echo $lang['store_order_sum'].$lang['nc_colon'];?></dt>
                        <dd><em><?php echo $output['order_info']['order_amount']; ?></em>元</dd>
                    </dl></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#show_shipping').on('hover',function(){
            var_send = '<?php echo date("Y-m-d H:i:s",$output['order_info']['extend_order_common']['shipping_time']); ?>&nbsp;&nbsp;<?php echo $lang['member_show_seller_has_send'];?><br/>';
            $.getJSON('index.php?act=seller_center&op=get_express&e_code=<?php echo $output['order_info']['express_info']['e_code']?>&shipping_code=<?php echo $output['order_info']['invoice_no']?>&t=<?php echo random(7);?>',function(data){
                if(data){
                    data = var_send+data;
                    $('#shipping_ul').html(data);
                    $('#show_shipping').unbind('hover');
                }else{
                    $('#shipping_ul').html(var_send);
                    $('#show_shipping').unbind('hover');
                }
            });
        });
    });
</script>
