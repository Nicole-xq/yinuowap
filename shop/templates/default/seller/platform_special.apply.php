<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
    <?php include template('layout/submenu');?>
    <div class="text-intro">专题名称：<?php echo $output['special_info']['special_name'];?></div>
</div>
<form method="GET">
    <input type="hidden" value="<?php echo intval($_GET['special_id']);?>" name="special_id"/>
    <table class="ncsc-default-table" >
        <thead>
        <tr>
            <th class="w50"></th>
            <th class="tl" colspan="2">拍品名称</th>
            <th class="w90">起拍价</th>
            <th class="w120">审核状态</th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($output['list']) and is_array($output['list'])){?>
            <?php foreach ($output['list'] as $k=>$v){ ?>
                <tr class="bd-line">
                    <td><div class="pic-thumb"><a href="<?php echo urlVendue('auctions', 'index', array('id' => $v['auction_id']));?>" target="_blank"><img src="<?php echo cthumb($v['auction_image'], 60,$_SESSION['store_id']);?>"></a></div></td>
                    <td class="tl" colspan="2"><dl class="goods-name">
                            <dt><a target="_blank" href="<?php echo urlVendue('auctions', 'index', array('id' => $v['auction_id']));?>"><?php echo $v['auction_name'];?></a></dt>
                            <dd><?php echo $v['gc_name'];?></dd>
                        </dl></td>
                    <td>￥<?php echo ncPriceFormat($v['auction_start_price']);?></td>
                    <td><?php echo $v['bid_number']?></td>
                    <td><?php if($v['auction_sp_state']=='1'){?>
                            已通过
                        <?php }elseif(in_array($v['auction_sp_state'],array('0','3'))){?>
                            审核中
                        <?php }
                        ?></td>
                </tr>
            <?php }?>
        <?php }else{?>
            <tr>
                <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
        <?php }?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="20"></td>
        </tr>
        </tfoot>
    </table>
</form>

<div class="div-goods-select">
    <form method="GET">
        <input type="hidden" name="act" value="store_auction_special"/>
        <input type="hidden" name="op" value="special_apply"/>
        <input type="hidden" name="special_id" value="<?php echo $_GET['special_id'];?>"/>
        <table class="search-form">
            <tr>
                <th class="w250"><strong>选择参加专场的拍品，勾选并提交平台审核</strong></th>
                <td class="w160"><input type="text" class="text w150" name="name" value="<?php echo $output['search']['name'];?>" placeholder="搜索拍品名称"/></td>
                <td class="w70 tc"><label class="submit-border">
                        <input type="submit" class="submit" value="搜索"/>
                    </label></td><td></td>
            </tr>
        </table>
    </form>
    <form method="POST" id="apply_form" onsubmit="ajaxpost('apply_form','','','onerror');" action="index.php?act=store_auction_special&op=special_apply_save">
        <input type="hidden" name="special_id" value="<?php echo $_GET['special_id'];?>"/>
        <?php if(!empty($output['goods_list']) and is_array($output['goods_list'])){?>
            <div class="search-result">
                <ul class="goods-list">
                    <?php foreach ($output['goods_list'] as $goods){?>
                        <li>
                            <div class="goods-thumb"><a href="" target="_blank"><img alt="<?php echo $goods['auction_name'];?>" title="<?php echo $goods['auction_name'];?>" src="<?php echo cthumb($goods['auction_image'], 240, $_SESSION['store_id']);?>"/></a></div>
                            <dl class="goods-info">
                                <dt>
                                    <input type="checkbox" value="<?php echo $goods['auction_id'];?>" class="vm" name="item_id[]"/>
                                    <label><a href="<?php echo urlVendue('auctions', 'index', array('id' => $goods['auction_id']));?>" target="_blank"><?php echo $goods['auction_name'];?></a></label>
                                </dt>
                                <dd>当前价：￥<?php if($goods['current_price'] != 0.00){echo ncPriceFormat($goods['current_price']);}else{echo ncPriceFormat($goods['auction_start_price']);};?></dd>
                            </dl>
                        </li>
                    <?php }?>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="pagination"><?php echo $output['show_page'];?></div>
        <?php }else{?>
            <div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];//您尚未发布任何商品?></span></div>
        <?php }?>
        <?php if(!empty($output['goods_list']) and is_array($output['goods_list'])){?>
            <div class="bottom tc p10">
                <input type="submit" class="submit" style="display: inline; *display: inline; zoom: 1;" value="选择完毕，参与专场"/>
            </div>
        <?php }?>
    </form>
</div>
