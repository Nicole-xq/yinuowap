<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<div class="item-publish">
  <form method="post" enctype="multipart/form-data" id="auction_goods_form" action="<?php echo urlShop('store_goods_online', 'save_auctions');?>">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="commonid" value="<?php echo $output['goods']['goods_commonid'];?>" />
    <input type="hidden" name="id" value="<?php echo $output['auctions_info']['id'];?>" />
    <input type="hidden" name="store_id" value="<?php echo $output['goods']['store_id'];?>" />
    <input type="hidden" name="store_name" value="<?php echo $output['goods']['store_name'];?>" />
    <div class="ncsc-form-goods">
      <?php if ($output['goods_list']) { ?>
      <dl>
        <dt><i class="required">*</i>选择规格</dt>
        <dd>
          <?php foreach ($output['goods_list'] as $k=>$val){?>
              <ul class="ncsc-form-radio-list">
                  <li>
                     <label>
                     <input name="goods_id" value="<?php echo $val['goods_id'];?>" type="radio" <?php if (($output['auctions_info']['goods_id'] == $val['goods_id']) || (count($output['goods_list'])==1)){ ?> checked="checked" <?php } ?> />
                     <?php echo $val['spec_name'];?>
                     </label>
                  </li>
              </ul>
          <?php } ?>
          <p class="hint">确认商品规格</p>
        </dd>
      </dl>
      <?php }else {?>
        <dl>
            <dt><i class="required">*</i>商品名称</dt>
            <dd>
                <?php echo $output['goods']['goods_name'];?>
                <input type="hidden" name="goods_id" value="<?php echo $output['goods']['goods_id']?>" />
                <span></span>
                <p class="hint"></p>
            </dd>
        </dl>
      <?php }?>

      <dl>
        <dt><i class="required">*</i>拍卖标题</dt>
        <dd>
          <?php if ($output['auctions_info']){?>
            <input name="auction_name" type="text" class="text w400" value="<?php echo $output['auctions_info']['auction_name']; ?>" />
          <?php } else {?>
             <input name="auction_name" type="text" class="text w400" value="<?php echo $output['goods']['goods_name']; ?>" />
          <?php } ?>
            <span></span>
          <p class="hint">拍卖标题名称长度至少3个字符，最长50个汉字</p>
        </dd>
      </dl>
      <dl>
        <dt>拍卖首图<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb"> <img nctype="auction_image" src="<?php echo cthumb($output['auctions_info']['auction_image'], 240);?>"/> </div>
              <input type="hidden" name="image_path" id="image_path" nctype="auction_image" value="<?php if ($output['auctions_info']['auction_image']){echo $output['auctions_info']['auction_image'];}else{echo $output['goods']['goods_image'];} ?>" />
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="auction_image" id="auction_image">
                  </span>
                  <p><i class="icon-upload-alt"></i>上传</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </dd>
      </dl>
      <dl>
        <dt nc_type="no_spec"><i class="required">*</i>起拍价<?php echo $lang['nc_colon'];?></dt>
        <dd nc_type="no_spec">
            <input name="auction_start_price" value="<?php echo ncPriceFormat($output['auctions_info']['auction_start_price']); ?>" type="text"  class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
            <p class="hint">必填项，大于0<br>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>保留价<?php echo $lang['nc_colon'];?></dt>
        <dd>
            <input name="auction_reserve_price" value="<?php echo ncPriceFormat($output['auctions_info']['auction_reserve_price']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
            <p class="hint">必填项，大于0</p>
        </dd>
        </dl>
      <dl>
        <dt><i class="required">*</i>加浮价<?php echo $lang['nc_colon'];?></dt>
        <dd>
            <input name="auction_increase_range" value="<?php echo ncPriceFormat($output['auctions_info']['auction_increase_range']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
            <p class="hint">必填项，大于0</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>保证金<?php echo $lang['nc_colon'];?></dt>
        <dd>
            <input name="auction_bond" value="<?php echo ncPriceFormat($output['auctions_info']['auction_bond']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
            <p class="hint">必填项，大于0</p>
        </dd>
      </dl>
    </div>
    <div class="bottom tc hr32">
      <label class="submit-border">
        <input type="submit" nctype="formSubmit" class="submit" value="提交" />
      </label>
    </div>
  </form>
</div>
<script type="text/javascript">

$(function(){

    /* 商品图片ajax上传 */
    $('#auction_image').fileupload({
        dataType: 'json',
        url: SITEURL + '/index.php?act=store_auction_add&op=image_upload&upload_type=uploadedfile',
        formData: {name:'auction_image'},
        add: function (e,data) {
            $('img[nctype="auction_image"]').attr('src', SHOP_TEMPLATES_URL + '/images/loading.gif');
            data.submit();
        },
        done: function (e,data) {
            var param = data.result;
            if (typeof(param.error) != 'undefined') {
                alert(param.error);
                $('img[nctype="auction_image"]').attr('src',DEFAULT_GOODS_IMAGE);
            } else {
                $('input[nctype="auction_image"]').val(param.name);
                $('img[nctype="auction_image"]').attr('src',param.thumb_name);
            }
        }
    });

    // 防止重复提交
    var __formSubmit = false;
    $('input[nctype="formSubmit"]').click(function(){
        if (__formSubmit) {
            return false;
        }
        if($('#auction_goods_form').valid()){
            __formSubmit = true;
        }
    });

    $('#auction_goods_form').validate({
        errorPlacement: function(error, element){
            __formSubmit = false;
            $(element).nextAll('span').append(error);
        },
        rules : {
            auction_name : {
                required    : true,
                minlength   : 3,
                maxlength   : 50
            },
            auction_start_price : {
                required    : true,
                number      : true,
                min         : 0.01,
                max         : 9999999
            },
            auction_reserve_price : {
                required    : true,
                number      : true,
                min         : 0.01,
                max         : 9999999
            },
            auction_increase_range : {
                required    : true,
                number      : true,
                min         : 0.01,
                max         : 9999999
            },
            auction_bond  : {
                required    : true,
                number      : true,
                min         : 0.01,
                max         : 999999999
            },
            goods_id : {
                required    : true
            }
        },
        messages : {
            auction_name  : {
                required    : '<i class="icon-exclamation-sign"></i>请填写拍卖标题',
                minlength   : '<i class="icon-exclamation-sign"></i>请填写拍卖标题',
                maxlength   : '<i class="icon-exclamation-sign"></i>请填写拍卖标题'
            },
            auction_start_price : {
                required    : '<i class="icon-exclamation-sign"></i>请填写正确起拍价',
                number      : '<i class="icon-exclamation-sign"></i>请正确填写数字',
                min         : '<i class="icon-exclamation-sign"></i>请填写正确起拍价',
                max         : '<i class="icon-exclamation-sign"></i>请填写正确起拍价'
            },
            auction_reserve_price : {
                required    : '<i class="icon-exclamation-sign"></i>请填写保留价',
                number      : '<i class="icon-exclamation-sign"></i>请填写正确的价格',
                min         : '<i class="icon-exclamation-sign"></i>请填写正确的价格',
                max         : '<i class="icon-exclamation-sign"></i>请填写正确的价格'
            },
            auction_increase_range : {
                required    : '<i class="icon-exclamation-sign"></i>请填写加浮价',
                number      : '<i class="icon-exclamation-sign"></i>请填写正确的价格',
                min         : '<i class="icon-exclamation-sign"></i>请填写正确的价格',
                max         : '<i class="icon-exclamation-sign"></i>请填写正确的价格'
            },
            auction_bond : {
                required    : '<i class="icon-exclamation-sign"></i>请填写保证金',
                number      : '<i class="icon-exclamation-sign"></i>请填写正确的金额',
                min         : '<i class="icon-exclamation-sign"></i>请填写正确的金额',
                max         : '<i class="icon-exclamation-sign"></i>请填写正确的金额>'
            },
            goods_id : {
				required	: '<i class="icon-exclamation-sign"></i>请选择规格'
			}
        }
    });
});
</script>
