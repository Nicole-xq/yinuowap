<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<form method="get" action="index.php">
  <table class="search-form">
    <input type="hidden" name="act" value="store_auction_list" />
    <input type="hidden" name="op" value="index" />
    <tr>
      <td>&nbsp;</td>
      <th>
        <select name="search_type">
          <option value="0" <?php if ($_GET['type'] == 0) {?>selected="selected"<?php }?>>拍品名称</option>
          <option value="1" <?php if ($_GET['type'] == 1) {?>selected="selected"<?php }?>>拍品ID</option>
        </select>
      </th>
      <td class="w160"><input type="text" class="text" name="keyword" value="<?php echo $_GET['keyword']; ?>"/></td>
      <td class="tc w70"><label class="submit-border"><input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label></td>
    </tr>
  </table>
</form>
<table class="ncsc-default-table">
  <thead>
    <tr nc_type="table_header">
      <th class="w30"></th>
      <th class="w180">拍品名称</th>
      <th class="w180">关联专场</th>
      <th class="w180">拍品状态</th>
<!--      <th class="w180">拍卖结果</th>-->
      <th class="w180">操作</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['auction_list'])) { ?>
    <?php foreach ($output['auction_list'] as $val) { ?>
    <tr>
      <th class="tc"></th>
      <th colspan="20">ID：<?php echo $val['auction_id'];?></th>
    </tr>
    <tr>
      <td>
        <div class="pic-thumb">
          <a href="<?php echo urlAuction('auctions', 'index', array('id' => $val['auction_id']));?>" target="_blank"><img src="<?php echo cthumb($val['auction_image'], 60);?>"/></a>
        </div>
      </td>
      <td class="tl"><dl class="goods-name">
          <dt style="max-width: 450px !important;">
            <a href="<?php echo urlAuction('auctions', 'index', array('id' => $val['auction_id']));?>" target="_blank"><?php echo $val['auction_name']; ?></a></dt>
<!--          <dd>拍品编号：--><?php //echo $val['auction_number'];?><!--</dd>-->
          </dd>
        </dl>
      </td>
      <td><span><?=$val['special_name'];?></span></td>
      <td><span><?=$val['auction_state'];?></span></td>
      <td class="">
            <span class="tip" title="该拍品正在开始拍卖，不允许编辑，如有特殊需求请联系管理员">
                    <a href="<?php echo urlShop('store_auction_list', 'auction_detail',array('auction_id'=>$val['auction_id']));?>" class="">
                        <i class="icon-edit"></i>
                        <p>查看拍品信息</p>
                    </a>
              </span>
        </td>
    </tr>
    <tr style="display:none;"><td colspan="20"><div class="ncsc-goods-sku ps-container"></div></td></tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
    </tr>
    <?php } ?>
  </tbody>
    <?php  if (!empty($output['auction_list'])) { ?>
  <tfoot>
    <tr>
      <td colspan="20"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
    </tr>
  </tfoot>
  <?php } ?>
</table>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_goods_list.js"></script> 
<script>
$(function(){
    //Ajax提示
    $('.tip').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 5,
        allowTipHover: false
    });
});
</script>