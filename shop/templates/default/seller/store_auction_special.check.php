<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>
<form method="get" action="index.php">
    <table class="search-form">
        <input type="hidden" name="act" value="store_auction_special" />
        <input type="hidden" name="op" value="special_check" />
        <tr>
            <td>&nbsp;</td>
            <th>
                <select name="search_type">
                    <option value="0" <?php if ($_GET['type'] == 0) {?>selected="selected"<?php }?>>专场名称</option>
                    <option value="1" <?php if ($_GET['type'] == 1) {?>selected="selected"<?php }?>>专场ID</option>
                </select>
            </th>
            <td class="w160"><input type="text" class="text" name="keyword" value="<?php echo $_GET['keyword']; ?>"/></td>
            <td class="tc w70"><label class="submit-border"><input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label></td>
        </tr>
    </table>
</form>
<table class="ncsc-default-table">
    <thead>
    <tr nc_type="table_header">
        <th class="w60">专场ID</th>
        <th class="w180">专场名称</th>
        <th class="w180">审核状态</th>
        <th class="w180">专场开始时间</th>
        <th class="w180">专场结束时间</th>
        <th class="w180">预展开始时间</th>
        <th class="w120">操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($output['special_list'])) { ?>
        <?php foreach ($output['special_list'] as $val) { ?>
            <tr>
                <td><span><?php echo $val['special_id'];?></span></td>
                <td><span><?php echo $val['special_name'] ?></span></td>
                <td><span>未审核</span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_start_time']) ?></span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_end_time']) ?></span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_preview_start']) ?></span></td>
                <td class="nscs-table-handle"><span class="tip" title="查看">
                    <a href="index.php?act=store_auction_special&op=special_detail&special_id=<?php echo $val['special_id'];?>" class="btn-darkgray">
                        <i class="icon-edit"></i>
                        <p>查看</p>
                    </a>
              </span></td>

            </tr>
            <tr style="display:none;"><td colspan="20"><div class="ncsc-goods-sku ps-container"></div></td></tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
        </tr>
    <?php } ?>
    </tbody>
    <?php  if (!empty($output['special_list'])) { ?>
        <tfoot>
        <tr>
            <td colspan="20"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
        </tr>
        </tfoot>
    <?php } ?>
</table>
