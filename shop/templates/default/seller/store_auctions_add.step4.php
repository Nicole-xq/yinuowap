
<ul class="add-goods-step">
  <li><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>选择拍品分类</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-edit"></i>
    <h6>STEP.2</h6>
    <h2>填写拍品详情</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-camera-retro "></i>
    <h6>STEP.3</h6>
    <h2>上传拍品图片</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li class="current"><i class="icon icon-ok-circle"></i>
    <h6>STEP.4</h6>
    <h2>拍品发布成功</h2>
  </li>
</ul>
<div class="alert alert-block hr32">
  <h2><i class="icon-ok-circle mr10"></i>恭喜您，拍品发布成功！
      <?php if (C('goods_verify')) {?>等待管理员审核商品！<?php }?></h2>
  <div class="hr16"></div>

  <strong>
    <a class="ml30" href="<?php echo urlAuction('auctions', 'index', array('id'=>$output['auction_id']));?>">去店铺查看拍品详情&gt;&gt;</a>
    <a class="ml30" href="<?php echo urlShop('store_auction_list', 'edit_auction', array('auction_id' => $_GET['auction_id'], 'ref_url' => urlShop('store_auction_list', 'index')));?>">重新编辑刚发布的拍品&gt;&gt;</a>
  </strong>  
  <div class="hr16"></div>
  <h4 class="ml10"><?php echo $lang['store_goods_step3_more_actions'];?></h4>
  <ul class="ml30">
    <li>1. 继续 &quot; <a href="<?php echo urlShop('store_auction_add', 'index');?>">发布新拍品</a>&quot;</li>
    <li>2. 进入 " 商家中心" 管理 &quot;<a href="<?php echo urlShop('store_auction_list', 'index');?>">出售中的拍品</a>&quot;</li>
  </ul>
</div>
