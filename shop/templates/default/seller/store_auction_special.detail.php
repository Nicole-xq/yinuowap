<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style type="text/css">

    .ncsc-form-default .ncsc-upload-thumb.store-logo p {
        width: 638px;
        height: 158px;
    }
    .ncsc-form-default .ncsc-upload-thumb.store-logo {
        width: 638px;
        height: 158px;
    }

</style>

<div class="ncsc-form-default">
    <form method="post" id="special_form" action="">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncsc-form-goods">
            <dl>
                <dt><i class="required">*</i>专场名称：</dt>
                <dd><?php echo $output['special_info']['special_name']?></dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>送拍机构：</dt>
                <dd><?php echo $output['special_info']['delivery_mechanism']?></dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>专场缩略图：</dt>
                <dd>
                 <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['special_info']['special_image'];?>" />
                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>专场广告图： </dt>
                <dd>
                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['special_info']['adv_image'];?>" />
                </dd>
            </dl>

            <dl>
                <dt><i class="required">*</i>专场开始时间：</dt>
                <dd><?php echo date('Y-m-d H:i:s',$output['special_info']['special_start_time'])?></dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>专场结束时间：</dt>
                <dd><?php echo date('Y-m-d H:i:s',$output['special_info']['special_end_time'])?></dd>
            </dl>
          <?php if(!empty($output['special_info']['special_preview_start'])){?>
            <dl>
                <dt><i class="required">*</i>预展开始时间：</dt>
                <dd><?php echo date('Y-m-d H:i:s',$output['special_info']['special_preview_start'])?></dd>
            </dl>
          <?php }?>
            <dl>
                <dt><i class="required">*</i>添加拍品：</dt>
                <dd>
                    <p>
                        <input id="bundling_goods" type="hidden" value="" name="bundling_goods">
                        <span></span></p>
                    <table class="ncsc-default-table mb15">
                        <thead>
                        <tr>
                            <th class="tl" colspan="2">拍品名称</th>
                            <th class="w30">起拍价</th>
                            <th class="w30">加浮价</th>
                            <th class="w30">保留价</th>
                            <th class="w30">保证金</th>
                            <?php if($output['special_info']['special_end_time']< time()){?>
                            <th class="w30">状态</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody nctype="bundling_data"  class="bd-line tip" title="<?php echo $lang['bundling_add_goods_explain'];?>">
                        <tr style="display:none;">
                            <td colspan="20" class="norecord"><div class="no-promotion"><i class="zh"></i><span>还未选择拍品。</span></div></td>
                        </tr>
                        <?php if(!empty($output['auction_special_list'])){?>
                        <?php foreach($output['auction_special_list'] as $val){?>
                        <tr id="bundling_tr_<?php echo $val['goods_auction_id']?>" class="off-shelf">
                            <input type="hidden" value="<?php echo $val['special_auction_id'];?>" name="goods[<?php echo $val['goods_auction_id'];?>][bundling_goods_id]" />
                            <input type="hidden" value="<?php echo $val['auction_id'];?>" name="goods[<?php echo $val['goods_auction_id'];?>][gid]" nctype="goods_id">
                            <td class="w50"><div class="shelf-state"><div class="pic-thumb"><img src="<?php echo cthumb($val['auction_image'], 60, $_SESSION['store_id']);?>" ncname="<?php echo $output['auction_special_list'][$val['goods_auction_id']]['auction_image'];?>" nctype="bundling_data_img">
                                    </div></div>
                            </td>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;"><?php echo $val['auction_name'];?></dt>
                              </dl>
                            </td>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;">¥<?php echo $val['auction_start_price'];?></dt>
                              </dl>
                            </td>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;">¥<?php echo $val['auction_increase_range'];?></dt>
                              </dl>
                            </td>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;">¥<?php echo $val['auction_reserve_price'];?></dt>
                              </dl>
                            </td>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;">¥<?php echo $val['auction_bond'];?></dt>
                              </dl>
                            </td>
                            <?php if($output['special_info']['special_end_time']< time()){?>
                            <td class="tl">
                              <dl class="goods-name">
                                <dt style="width: 100px;"><?php echo $val['order_info']['state'];?>&nbsp;&nbsp;&nbsp;<?php echo $val['order_info'] == 1?'已支付':'未支付';?>
                                    <?php if($val['order_info']['is_pay'] == 1){?>
                                <p><a href="index.php?act=store_order&op=show_order&order_id=<?=$val['order_info']['order_id'];?>">订单详情</a></p>
                                  <?php }?>
                                </dt>
                              </dl>
                            </td>
                            <?php }?>

                        </tr>
            <?php }?>
            <?php }?>
                        </tbody>
                    </table>

                </dd>
            </dl>
            <?php if($output['special_info']['special_state'] != 10){?>
                <dl>
                    <dt>审核意见</dt>
                    <dd><?php echo $output['special_info']['special_check_message']?></dd>
                </dl>
            <?php }?>

        </div>

    </form>

</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_bundling.js"></script>
