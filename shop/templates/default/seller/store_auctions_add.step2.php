<?php defined('InShopNC') or exit('Access Invalid!');?>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common_select.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"></script>
<!--[if lt IE 8]>
  <script src="<?php echo RESOURCE_SITE_URL;?>/js/json2.js"></script>
<![endif]-->
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/store_auctions_add.step2.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style type="text/css">
#fixedNavBar { filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#CCFFFFFF', endColorstr='#CCFFFFFF');background:rgba(255,255,255,0.8); width: 90px; margin-left: 510px; border-radius: 4px; position: fixed; z-index: 999; top: 172px; left: 50%;}
#fixedNavBar h3 { font-size: 12px; line-height: 24px; text-align: center; margin-top: 4px;}
#fixedNavBar ul { width: 80px; margin: 0 auto 5px auto;}
#fixedNavBar li { margin-top: 5px;}
#fixedNavBar li a { font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px; background-color: #F5F5F5; color: #999; text-align: center; display: block;  height: 20px; border-radius: 10px;}
#fixedNavBar li a:hover { color: #FFF; text-decoration: none; background-color: #27a9e3;}
</style>

<div id="fixedNavBar">
<h3>页面导航</h3>
  <ul>
    <li><a id="demo1Btn" href="#demo1" class="demoBtn">基本信息</a></li>
    <li><a id="demo2Btn" href="#demo2" class="demoBtn">详情描述</a></li>
    <li><a id="demo3Btn" href="#demo3" class="demoBtn">物流信息</a></li>
  </ul>
</div>
<?php if ($output['edit_auctions_sign']) {?>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<?php } else {?>
<ul class="add-goods-step">
  <li><i class="icon icon-list-alt"></i>
    <h6>STEP.1</h6>
    <h2>选择拍卖分类</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li class="current"><i class="icon icon-edit"></i>
    <h6>STEP.2</h6>
    <h2>填写拍卖详情</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-camera-retro "></i>
    <h6>STEP.3</h6>
    <h2>上传拍卖图片</h2>
    <i class="arrow icon-angle-right"></i> </li>
  <li><i class="icon icon-ok-circle"></i>
    <h6>STEP.4</h6>
    <h2>拍卖发布成功</h2>
  </li>
</ul>
<?php }?>
<div class="item-publish">
  <form method="post" enctype="multipart/form-data" id="auction_form" action="<?php if ($output['edit_auctions_sign']) { echo urlShop('store_auction_list', 'edit_save_auction');} else { echo urlShop('store_auction_add', 'save_auction');}?>">
    <input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="auction_id" value="<?php echo $output['auction']['auction_id'];?>" />
    <input type="hidden" name="type_id" value="<?php echo $output['goods_class']['type_id'];?>" />
    <input type="hidden" name="ref_url" value="<?php echo $_GET['ref_url'] ? $_GET['ref_url'] : getReferer();?>" />
    <div class="ncsc-form-goods">
      <h3 id="demo1"><?php echo $lang['store_goods_index_goods_base_info']?></h3>
      <dl>
        <dt>拍品发布</dt>
        <dd id="gcategory"> <?php echo $output['goods_class']['gc_tag_name'];?> <a class="ncbtn" href="<?php if ($output['edit_auctions_sign']) { echo urlShop('store_auction_list', 'edit_class', array('auction_id' => $output['auction']['auction_id'], 'ref_url' => getReferer())); } else { echo urlShop('store_auction_add', 'add_step_one'); }?>"><?php echo $lang['nc_edit'];?></a>
          <input type="hidden" id="cate_id" name="cate_id" value="<?php echo $output['goods_class']['gc_id'];?>" class="text" />
          <input type="hidden" name="cate_name" value="<?php echo $output['goods_class']['gc_tag_name'];?>" class="text"/>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>拍品名称：</dt>
        <dd>
          <input name="auction_name" type="text" class="text w400" value="<?php echo $output['auction']['auction_name']; ?>" />
          <span></span>
          <p class="hint">拍品标题名称长度至少3个字符，最长50个汉字</p>
        </dd>
      </dl>
        <dl>
            <dt><i class="required">*</i>商品预展：</dt>
            <dd>
                <input id="auction_preview_start" name="auction_preview_start" value="<?php echo $output['auction']['auction_preview_start'] ? date('Y-m-d H:i', $output['auction']['auction_preview_start']) :''; ?>" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
                <p class="hint">

                </p>
            </dd>

        </dl>
        <dl>
            <dt><i class="required">*</i>拍卖时间：</dt>
            <dd>
                <ul class="ncsc-form-radio-list">
                    <li>
                        <label>
                            开始时间
                        </label>
                        <input id="auction_start_time" name="auction_start_time" value="<?php echo $output['auction']['auction_start_time'] ? date('Y-m-d H:i', $output['auction']['auction_start_time']):''; ?>" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
                    </li>
                    <li>
                        <label>
                            结束时间
                        </label>
                        <input id="auction_end_time" name="auction_end_time" value="<?php echo $output['auction']['auction_end_time'] ? date('Y-m-d H:i', $output['auction']['auction_end_time']) : ''; ?>" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
                    </li>
                </ul>
            </dd>
        </dl>
      <dl>
        <dt nc_type="no_spec"><i class="required">*</i>起拍价格：</dt>
        <dd nc_type="no_spec">
          <input name="auction_start_price" value="<?php echo ncPriceFormat($output['auction']['auction_start_price']); ?>" type="text"  class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
          <p class="hint">起拍价最少为1元</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>加价幅度<?php echo $lang['nc_colon'];?></dt>
        <dd>
          <input name="auction_increase_range" value="<?php echo ncPriceFormat($output['auction']['auction_increase_range']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
          <p class="hint">加价幅度最少为1元</p>
        </dd>
      </dl>
        <dl>
            <dt><i class="required">*</i>保证金<?php echo $lang['nc_colon'];?></dt>
            <dd>
                <input name="auction_bond" value="<?php echo ncPriceFormat($output['auction']['auction_bond']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
                <p class="hint">保证金可设置为0元</p>
            </dd>
        </dl>
        <dl>
            <dt><i class="required">*</i>保留价<?php echo $lang['nc_colon'];?></dt>
            <dd>
                <input name="auction_reserve_price" value="<?php echo ncPriceFormat($output['auction']['auction_reserve_price']); ?>" type="text" class="text w60" /><em class="add-on"><i class="icon-renminbi"></i></em> <span></span>
                <p class="hint">保留价前台不显示，作用于拍卖结束时，如未到保留价时，进行机器人出价设置</p>
            </dd>
        </dl>
      <dl>
        <dt nc_type="no_spec">拍品编号：</dt>
        <dd nc_type="no_spec">
          <p>
            <input name="auction_number" value="<?php echo $output['auction']['auction_number'];?>" type="text" class="text" />
          </p>
          <p class="hint">拍品编号为商家自行设定的内部管理编号，最多20个字符</p>
        </dd>
      </dl>
        <dl>
            <dt nc_type="no_spec">送拍机构：</dt>
            <dd nc_type="no_spec">
                <p>
                    <input name="delivery_mechanism" value="<?php echo $output['auction']['delivery_mechanism'];?>" type="text" class="text" />
                </p>
                <p class="hint">送拍机构默认为商家店铺名称，商家可在此处自行编辑</p>
            </dd>
        </dl>
      <dl>
        <dt>商品视频：</dt>
        <dd>
          <div class="upload-thumb">
          <?php if(!empty($output['auction']['auction_video'])) { ?>
          <video src="<?php echo goodsVideoPath($output['auction']['auction_video'],$output['auction']['store_id']);  ?>" width="100" height="100">
            <img width="240" height="240" src="<?php echo upload_site_url.'/'.ATTACH_COMMON.'/'.'default_video.gif';?>">
          </video>
          <?php } ?>
          </div>
          <input id="file" name="auction_video_file" type="file" class="type-file-file">
          <input type="hidden" name="auction_video" value="<?php echo $output['auction']['auction_video']; ?>">
          <p class="hint">上传商品视频；支持mp4格式上传，建议使用
            <font color="red">大小不超过10M的视频</font>，上传后的视频将会自动保存在视频空间的默认分类中。</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo $lang['store_goods_album_goods_pic'].$lang['nc_colon'];?></dt>
        <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb"> <img nctype="auction_image" src="<?php echo cthumb($output['auction']['auction_image'], 240);?>"/> </div>
              <input type="hidden" name="image_path" id="image_path" nctype="auction_image" value="<?php echo $output['auction']['auction_image']?>" />
              <span></span>
              <p class="hint"><?php echo $lang['store_goods_step2_description_one'];?><?php printf($lang['store_goods_step2_description_two'],intval(C('image_max_filesize'))/1024);?></p>
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input type="file" hidefocus="true" size="1" class="input-file" name="auction_image" id="auction_image">
                  </span>
                  <p><i class="icon-upload-alt"></i>图片上传</p>
                  </a> </div>
                <a class="ncbtn mt5" nctype="show_image" href="<?php echo urlShop('store_album', 'pic_list', array('item'=>'goods'));?>"><i class="icon-picture"></i>从图片空间选择</a> <a href="javascript:void(0);" nctype="del_goods_demo" class="ncbtn mt5" style="display: none;"><i class="icon-circle-arrow-up"></i>关闭相册</a></div>
            </div>
          </div>
          <div id="demo"></div>
        </dd>
      </dl>
      <h3 id="demo2"><?php echo $lang['store_goods_index_goods_detail_info']?></h3>
      <?php if(is_array($output['attr_list']) && !empty($output['attr_list'])){?>
      <dl>
        <dt><?php echo $lang['store_goods_index_goods_attr'].$lang['nc_colon']; ?></dt>
        <dd>
          <?php foreach ($output['attr_list'] as $k=>$val){?>
          <span class="property">
          <label class="mr5"><?php echo $val['attr_name']?></label>
          <input type="hidden" name="attr[<?php echo $k;?>][name]" value="<?php echo $val['attr_name']?>" />
          <?php if(is_array($val) && !empty($val)){?>
          <select name="" attr="attr[<?php echo $k;?>][__NC__]" nc_type="attr_select">
            <option value='不限' nc_type='0'>不限</option>
            <?php foreach ($val['value'] as $v){?>
            <option value="<?php echo $v['attr_value_name']?>" <?php if(isset($output['attr_checked']) && in_array($v['attr_value_id'], $output['attr_checked'])){?>selected="selected"<?php }?> nc_type="<?php echo $v['attr_value_id'];?>"><?php echo $v['attr_value_name'];?></option>
            <?php }?>
          </select>
          <?php }?>
          </span>
          <?php }?>
        </dd>
      </dl>
      <?php }?>
      <?php if (!empty($output['custom_list'])) { ?>
      <dl>
        <dt>自定义属性：</dt>
        <dd>
          <?php foreach ($output['custom_list'] as $val) {?>
          <span class="property">
            <label class="mr5"><?php echo $val['custom_name'];?></label>
            <input type="hidden" name="custom[<?php echo $val['custom_id'];?>][name]" value="<?php echo $val['custom_name'];?>" />
            <input class="text w60" type="text" name="custom[<?php echo $val['custom_id'];?>][value]" value="<?php if ($output['auction']['auction_custom'][$val['custom_id']]['value'] != '') {echo $output['auction']['auction_custom'][$val['custom_id']]['value'];}?>" />
          </span>
          <?php }?>
        </dd>
      </dl>
      <?php }?>
      <dl>
        <dt><?php echo $lang['store_goods_index_goods_desc'].$lang['nc_colon'];?></dt>
        <dd id="ncProductDetails">
          <div class="tabs">
            <ul class="ui-tabs-nav">
              <li class="ui-tabs-selected"><a href="#panel-1"><i class="icon-desktop"></i> 电脑端</a></li>
              <li class="selected"><a href="#panel-2"><i class="icon-mobile-phone"></i>手机端</a></li>
            </ul>
            <div id="panel-1" class="ui-tabs-panel">
              <?php showEditor('auction_body',$output['auction']['auction_body'],'100%','480px','visibility:hidden;',"false",$output['editor_multimedia']);?>
              <div class="hr8">
                <div class="ncsc-upload-btn">
                    <a href="javascript:void(0);"><span>
                      <input type="file" hidefocus="true" size="1" class="input-file" name="add_album" id="add_album" multiple>
                      </span>
                      <p><i class="icon-upload-alt" data_type="0" nctype="add_album_i"></i>图片上传</p>
                      </a>
                </div>
                    <a class="ncbtn mt5" nctype="show_desc" href="index.php?act=store_album&op=pic_list&item=des">
                        <i class="icon-picture"></i><?php echo $lang['store_goods_album_insert_users_photo'];?>
                    </a>
                      <a href="javascript:void(0);" nctype="del_desc" class="ncbtn mt5" style="display: none;">
                          <i class=" icon-circle-arrow-up"></i>关闭相册
                      </a>
              </div>
              <p id="des_demo"></p>
            </div>
            <div id="panel-2" class="ui-tabs-panel ui-tabs-hide">
              <div class="ncsc-mobile-editor">
                <div class="pannel">
                  <div class="size-tip"><span nctype="img_count_tip">图片总数不得超过<em>20</em>张</span><i>|</i><span nctype="txt_count_tip">文字不得超过<em>5000</em>字</span></div>
                  <div class="control-panel" nctype="mobile_pannel">
                    <?php if (!empty($output['auction']['mb_body'])) {?>
                    <?php foreach ($output['auction']['mb_body'] as $val) {?>
                    <?php if ($val['type'] == 'text') {?>
                    <div class="module m-text">
                      <div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_edit" href="javascript:void(0);">编辑</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>
                      <div class="content">
                        <div class="text-div"><?php echo $val['value'];?></div>
                      </div>
                      <div class="cover"></div>
                    </div>
                    <?php }?>
                    <?php if ($val['type'] == 'image') {?>
                    <div class="module m-image">
                      <div class="tools"><a nctype="mp_up" href="javascript:void(0);">上移</a><a nctype="mp_down" href="javascript:void(0);">下移</a><a nctype="mp_rpl" href="javascript:void(0);">替换</a><a nctype="mp_del" href="javascript:void(0);">删除</a></div>
                      <div class="content">
                        <div class="image-div"><img src="<?php echo $val['value'];?>"></div>
                      </div>
                      <div class="cover"></div>
                    </div>
                    <?php }?>
                    <?php }?>
                    <?php }?>
                  </div>
                  <div class="add-btn">
                    <ul class="btn-wrap">
                      <li><a href="javascript:void(0);" nctype="mb_add_img"><i class="icon-picture"></i>
                        <p>图片</p>
                        </a></li>
                      <li><a href="javascript:void(0);" nctype="mb_add_txt"><i class="icon-font"></i>
                        <p>文字</p>
                        </a></li>
                    </ul>
                  </div>
                </div>
                <div class="explain">
                  <dl>
                    <dt>1、基本要求：</dt>
                    <dd>（1）手机详情总体大小：图片+文字，图片不超过20张，文字不超过5000字；</dd>
                    <dd>建议：所有图片都是本宝贝相关的图片。</dd>
                  </dl><dl>
                    <dt>2、图片大小要求：</dt>
                    <dd>（1）建议使用宽度480 ~ 620像素、高度小于等于960像素的图片；</dd>
                    <dd>（2）格式为：JPG\JEPG\GIF\PNG；</dd>
                    <dd>举例：可以上传一张宽度为480，高度为960像素，格式为JPG的图片。</dd>
                  </dl><dl>
                    <dt>3、文字要求：</dt>
                    <dd>（1）每次插入文字不能超过500个字，标点、特殊字符按照一个字计算；</dd>
                    <dd>（2）请手动输入文字，不要复制粘贴网页上的文字，防止出现乱码；</dd>
                    <dd>（3）以下特殊字符“<”、“>”、“"”、“'”、“\”会被替换为空。</dd>
                    <dd>建议：不要添加太多的文字，这样看起来更清晰。</dd>
                  </dl>
                </div>
              </div>
              <div class="ncsc-mobile-edit-area" nctype="mobile_editor_area">
                <div nctype="mea_img" class="ncsc-mea-img" style="display: none;"></div>
                <div class="ncsc-mea-text" nctype="mea_txt" style="display: none;">
                  <p id="meat_content_count" class="text-tip"></p>
                  <textarea class="textarea valid" nctype="meat_content"></textarea>
                  <div class="button"><a class="ncbtn ncbtn-bluejeansjeansjeans" nctype="meat_submit" href="javascript:void(0);">确认</a><a class="ncbtn ml10" nctype="meat_cancel" href="javascript:void(0);">取消</a></div>
                  <a class="text-close" nctype="meat_cancel" href="javascript:void(0);">X</a>
                </div>
              </div>
              <input name="auction_mobile_body" autocomplete="off" type="hidden" value='<?php echo $output['auction']['auction_mobile_body'];?>'>
            </div>
          </div>
        </dd>
      </dl>
      <!-- 商品物流信息 S -->
      <h3 id="demo3"><?php echo $lang['store_goods_index_goods_transport']?></h3>
      <dl>
        <dt><?php echo $lang['store_goods_index_goods_szd'].$lang['nc_colon']?></dt>
        <dd>
          <input type="hidden" value="<?php echo $output['auction']['areaid_2'] ? $output['auction']['areaid_2'] : $output['auction']['areaid_1'];?>" name="region" id="region">
          <input type="hidden" value="<?php echo $output['auction']['areaid_1'];?>" name="province_id" id="_area_1">
          <input type="hidden" value="<?php echo $output['auction']['areaid_2'];?>" name="city_id" id="_area_2">
          </p>
        </dd>
      </dl>
      <!-- 商品物流信息 E -->
    </div>
    <div class="bottom tc hr32">
      <label class="submit-border">
        <input type="submit" nctype="formSubmit" class="submit" value="<?php if ($output['edit_auctions_sign']) {echo '提交';} else {?><?php echo $lang['store_goods_add_next'];?>，上传商品图片<?php }?>" />
      </label>
    </div>
  </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script type="text/javascript">
var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
var DEFAULT_GOODS_IMAGE = "<?php echo thumb(array(), 60);?>";
var SHOP_RESOURCE_SITE_URL = "<?php echo SHOP_RESOURCE_SITE_URL;?>";

$(function(){

    // 防止重复提交
    var __formSubmit = false;
    $('input[nctype="formSubmit"]').click(function(){
        if (__formSubmit) {
            return false;
        }
        if($('#auction_form').valid()){
            __formSubmit = true;
        }
    });
	
    $.validator.addMethod('checkPrice', function(value,element){
    	_g_price = parseFloat($('input[name="g_price"]').val());
        _g_marketprice = parseFloat($('input[name="g_marketprice"]').val());
        if (_g_marketprice <= 0) {
            return true;
        }
        if (_g_price > _g_marketprice) {
            return false;
        }else {
            return true;
        }
    }, '');

    //拍卖预展时间不能先于现在
    $.validator.addMethod('greaterThanNowDate', function(value,element){
        var date1 = new Date();
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    }, '');

    //拍卖时间不能先于预展时间
    $.validator.addMethod('greaterThanPreviewStart', function(value,element){
        var start_date = $("#auction_preview_start").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    }, '');

    //拍卖结束时间不能先于拍卖时间
    $.validator.addMethod('greaterThanStartTime', function(value,element){
        var start_date = $("#auction_start_time").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    }, '');

    $('#auction_form').validate({
        errorPlacement: function(error, element){
            __formSubmit = false;
            $(element).nextAll('span').append(error);
        },
        <?php if ($output['edit_auctions_sign']) {?>
        submitHandler:function(form){
            ajaxpost('auction_form', '', '', 'onerror');
        },
        <?php }?>
        onfocusout : false,
        rules : {
            auction_name : {
                required    : true,
                minlength   : 3,
                maxlength   : 50
            },
            auction_start_price : {
                required    : true,
                number      : true,
                min         : 1,
                max         : 999999999
            },
            auction_increase_range : {
                required    : true,
                number      : true,
                min         : 1,
                max         : 999999999
            },
            auction_bond : {
                required    : true,
                number      : true,
                min         : 0,
                max         : 999999999
            },
            auction_reserve_price : {
                required    : true,
                number      : true,
                min         : 0,
                max         : 999999999
            },
            auction_number : {
                maxlength   : 20
            },
            image_path : {
                required    : true
            },
            auction_preview_start: {
                required : true,
                greaterThanNowDate: true
            },
            auction_start_time: {
                required : true,
                greaterThanPreviewStart: true
            },
            auction_end_time: {
                required : true,
                greaterThanStartTime: true
            }
        },
        messages : {
            auction_name  : {
                required    : '<i class="icon-exclamation-sign"></i>请输入拍品名',
                minlength   : '<i class="icon-exclamation-sign"></i>拍品标题名称长度至少3个字符，最长50个汉字',
                maxlength   : '<i class="icon-exclamation-sign"></i>拍品标题名称长度至少3个字符，最长50个汉字'
            },
            auction_start_price : {
                required    : '<i class="icon-exclamation-sign"></i>请填写起拍价',
                number      : '<i class="icon-exclamation-sign"></i>请填写数字',
                min         : '<i class="icon-exclamation-sign"></i>最小值为1',
                max         : '<i class="icon-exclamation-sign"></i>最大值为999999999',
            },
            auction_increase_range : {
                required    : '<i class="icon-exclamation-sign"></i>请填写加价幅度',
                number      : '<i class="icon-exclamation-sign"></i>请填写数字',
                min         : '<i class="icon-exclamation-sign"></i>最小值为1',
                max         : '<i class="icon-exclamation-sign"></i>最大值为999999999',
            },
            auction_bond : {
                required    : '<i class="icon-exclamation-sign"></i>请填写保证金',
                number      : '<i class="icon-exclamation-sign"></i>请填写数字',
                min         : '<i class="icon-exclamation-sign"></i>最小值为0',
                max         : '<i class="icon-exclamation-sign"></i>最大值为999999999',
            },
            auction_reserve_price : {
                required    : '<i class="icon-exclamation-sign"></i>请填写保留价',
                number      : '<i class="icon-exclamation-sign"></i>请填写数字',
                min         : '<i class="icon-exclamation-sign"></i>最小值为0',
                max         : '<i class="icon-exclamation-sign"></i>最大值为999999999',
            },
            auction_number : {
                maxlength   : '<i class="icon-exclamation-sign"></i>最大长度为20',
            },
            image_path : {
                required    : '<i class="icon-exclamation-sign"></i>请设置商品主图'
            },
            auction_preview_start: {
                required : '<i class="icon-exclamation-sign"></i>预展开始时间不能为空',
                greaterThanNowDate: '<i class="icon-exclamation-sign"></i>不能早于现在'
            },
            auction_start_time: {
                required    : '<i class="icon-exclamation-sign"></i>拍卖开始时间不能为空',
                greaterThanPreviewStart: '<i class="icon-exclamation-sign"></i>不能早于商品预展时间'
            },
            auction_end_time: {
                required    : '<i class="icon-exclamation-sign"></i>拍卖结束时间不能为空',
                greaterThanStartTime: '<i class="icon-exclamation-sign"></i>不能早于商品拍卖时间'
            }
        }
    });
    <?php if (isset($output['auction'])) {?>
	setTimeout("setArea(<?php echo $output['auction']['areaid_1'];?>, <?php echo $output['auction']['areaid_2'];?>)", 1000);
	<?php }?>

  // 上传类型
  $('input[class="type-file-file"]').change(function(){
    var filepath=$(this).val();
    var extStart=filepath.lastIndexOf(".");
    var ext=filepath.substring(extStart,filepath.length).toUpperCase();
    if(ext!=".mp4"&&ext!=".MP4"){
      alert("视频限于mp4格式");
      $(this).attr('value','');
      return false;
    }
  });
});
</script> 
<script src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/scrolld.js"></script>
<script type="text/javascript">$("[id*='Btn']").stop(true).on('click', function (e) {e.preventDefault();$(this).scrolld();})</script>
