<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style type="text/css">

    .ncsc-form-default .ncsc-upload-thumb.store-logo p {
        width: 200px;
        height: 60px;
    }
    .explain {
        font-size: 14px;
        font-weight: 600;
        line-height: 22px;
        color: #000;
        clear: both;
        background-color: #F5F5F5;
        padding: 5px 0 5px 12px;
        border-bottom: solid 1px #E7E7E7;
    }
</style>
<script type="text/javascript">
    var ref_area_id_1 = "<?php echo $output['store_vendue_info']['city_id']?>";
    var ref_area_id_2 = "<?php echo $output['store_vendue_info']['area_id']?>";
    var store_vendue_info = "<?php echo $output['store_vendue_info']?>";

</script>

<?php if(empty($output['store_vendue_info']) || $output['store_vendue_info']['store_apply_state'] == 30){?>
    <?php if($output['store_vendue_info']['store_apply_state'] == 30){?>
    <div class="explain"><i></i><?php echo $output['check_message'];?></div>
        <?php }?>
<div class="ncsc-form-default">
    <form method="post" enctype="multipart/form-data" id="vendue_form" action=" <?php echo urlShop('store_auction', 'save_store_vendue');?>">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncsc-form-goods">
            <h3 id="demo1">申请拍卖功能</h3>
            <dl>
                <dt><i class="required">*</i>所在地：</dt>
                <dd>
                    <input name="region" type="hidden" id="region" value="">
                    <input type="hidden" name="city_id" id="_area_2" />
                    <input type="hidden" name="area_id" id="_area" />
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>机构类型：</dt>
                <dd>
                    <select name="vendue_type">
                        <option value="合作机构" <?php if($output['store_vendue_info']['oz_type'] == '合作机构'){?>selected="selected"<?php }?>>合作机构</option>
                        <option value="艺术家" <?php if($output['store_vendue_info']['oz_type'] == '艺术家'){?>selected="selected"<?php }?>>艺术家</option>
                    </select>

                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>

            <dl nctype="after" >
                <dt>联系方式（在线IM）：</dt>
                <dd>
                    <select name="store_vendue_contact">
                    <?php if (is_array($output['seller_list']) && !empty($output['seller_list'])) { ?>
                        <?php foreach ($output['seller_list'] as $key => $val) { ?>
                            <option value="<?php echo $val['member_id'];?>" <?php if($output['store_vendue_info']['store_vendue_contact'] == $val['member_id']){?>selected="selected"<?php }?>><?php echo $val['seller_name'];?></option>
                        <?php } ?>
                    <?php } ?>
                    </select>

                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>简介：</dt>
                <dd>
                    <?php showEditor('vendue_intro',$output['store_vendue_info']['store_vendue_intro'],'100%','480px','visibility:hidden;',"false","false");?>
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>

        </div>

<div class="bottom tc hr32">
    <label class="submit-border">
        <input type="submit" nctype="formSubmit" class="submit" value="提交" />
    </label>
</div>
</form>

</div>
<?php }else{?>
    <div class="explain"><i></i><?php echo $output['check_message'];?></div>
    <div class="ncsc-form-default">
            <form method="post" id="pay_form" enctype="multipart/form-data" action=" <?php echo urlShop('store_auction', 'save_pay_store');?>">
                <input type="hidden" name="form_submit" value="ok" />
                <div class="ncsc-form-goods">
                    <h3 id="demo1">拍卖功能信息</h3>

                    <dl>
                        <dt>所在地：</dt>
                        <dd><?php echo $output['city_info']['area_name']?>&nbsp;&nbsp;<?php echo $output['area_info']['area_name']?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>机构类型：</dt>
                        <dd>
                            <?php echo $output['store_vendue_info']['oz_type']?>
                        </dd>
                    </dl>

                    <dl nctype="after" >
                        <dt>联系方式(在线IM)：</dt>
                        <dd><?php echo $output['seller_info']['seller_name']?></dd>
                    </dl>
                    <dl>
                        <dt><i class="required">*</i>简介：</dt>
                        <dd><?php echo $output['store_vendue_info']['store_vendue_intro']?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>拍卖保障金：</dt>
                        <dd>
                            <?php echo ncPriceFormat($output['store_info']['vendue_bail'])?>
                        </dd>
                    </dl>
                    <?php if($output['store_vendue_info']['store_apply_state'] == 20) {?>
                        <dl>
                            <dt>上传付款凭证：</dt>
                            <dd><input name="paying_money_certificate" type="file" />
                                <span></span></dd>
                        </dl>
                        <dl>
                            <dt>备注：</dt>
                            <dd><textarea name="paying_money_certif_exp" rows="10" cols="30"></textarea>
                                <span></span></dd>
                        </dl>
                    <?php }?>
                    <?php if($output['store_vendue_info']['store_apply_state'] == 40 || $output['store_vendue_info']['store_apply_state'] == 11){?>
                        <dl>
                            <dt>上传付款凭证：</dt>
                            <dd><img src="<?php echo getVendueLogo($output['store_vendue_info']['paying_money_certificate'])?>" />
                                <span></span></dd>
                        </dl>
                        <dl>
                            <dt>备注：</dt>
                            <dd><?php echo $output['store_vendue_info']['paying_money_certif_exp']?>
                                <span></span></dd>
                        </dl>
                    <?php }?>


                </div>
                <?php if($output['store_vendue_info']['store_apply_state'] == 20) {?>
                    <div class="bottom tc hr32">
                        <label class="submit-border">
                            <input type="submit" nctype="formSubmit" class="submit" value="提交" />
                        </label>
                    </div>
                <?php }?>
                <?php if($output['store_vendue_info']['store_apply_state'] == 40){?>
                    <a style="font-size: 14px;margin:20px auto;line-height: 36px;color: #FFF;background-color: #48CFAE;display:block;width:100px;height: 36px;
text-align:center;border-radius: 3px;border: none 0;" href="index.php?act=store_auction&op=edit_store">编辑</a>
                <?php }?>

            </form>
    </div>
<?php } ?>
<script type="text/javascript">
    var SITEURL = "<?php echo SHOP_SITE_URL; ?>";
    $(function() {
        if (typeof ref_area_id_1 != '' && store_vendue_info != '') {
            $('#region').val('<?php echo $output['city_info']['area_name']?>&nbsp;&nbsp;<?php echo $output['area_info']['area_name']?>');
            $('#_area').val(ref_area_id_1);
            $('#_area_2').val(ref_area_id_2);
            var $newArea = $("<select name='area_id_1' id='area_id' style='display:none'></select>");
            $("#region").before($newArea);
            $newArea.append("<option value='"+ref_area_id_1+"'></option>");
        }
        $("#region").nc_region();
        $('input[nc_type="change_store_label"]').change(function () {
            var src = getFullPath($(this)[0]);
            $('div[nctype="store_label"]').find('p').html('<img src="' + src + '">');
        });

        // 防止重复提交

        $('input[nctype="formSubmit"]').click(function () {

            if ($('#vendue_form').valid()) {
                $('#vendue_form').submit();
            }
        });

        $('#vendue_form').validate({
            errorPlacement: function (error, element) {
                $(element).nextAll('span').append(error);
            },

            rules: {
                region: {
                    checklast: true
                },
                vendue_type: {
                    required: true
                },
                vendue_intro: {
                    required: true
                }

            },
            messages: {
                region: {
                    checklast: '<i class="icon-exclamation-sign"></i>请将地区选择完整'
                },
                vendue_type: {
                    required: '<i class="icon-exclamation-sign"></i>机构类型不能为空'
                },
                vendue_intro: {
                    required: '<i class="icon-exclamation-sign"></i>简介不能为空'
                }

            }
        });

        $('#pay_form').validate({
            errorPlacement: function(error, element){
                element.nextAll('span').first().after(error);
            },
            rules : {
                paying_money_certificate: {
                    required: true
                },
                paying_money_certif_exp: {
                    maxlength: 100
                }
            },
            messages : {
                paying_money_certificate: {
                    required: '请选择上传付款凭证'
                },
                paying_money_certif_exp: {
                    maxlength: jQuery.validator.format("最多{0}个字")
                }
            }
        });

    });


</script>
