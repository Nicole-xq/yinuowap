<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/4 0004
 * Time: 18:16
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');
?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/member.css" rel="stylesheet" type="text/css">
<div class="account-center">
    <div class="common-list box">
        <div class="wrap">
            <div class="tabmenu">
                <?php include template('layout/submenu');?>
            </div>
            <div class="clear"></div>
            <table width="1025" border="0" cellspacing="0" cellpadding="0">
            <?php if (!empty($output['margin_order_list'])) { ?>
                <?php foreach ($output['margin_order_list'] as $key => $margin_order)  { ?>
                    <tr style="height: 36px;background: #f5f5f5">
                        <td>订单号<?php echo $margin_order['order_sn'] ?></td>
                        <td>开拍时间</td>
                        <td>当前出价</td>
                        <td>我的最高出价</td>
                        <td>保证金</td>
                        <td>状态</td>
                        <td>操作</td>
                    </tr>

                    <tr style="height: 90px;">
                        <td>
                            <a href="<?php echo $margin_order['auction_url'] ?>">
                                <img src="<?php echo $margin_order['image_60_url'] ?>" width="58" height="58" class="all-block fl" />

                                <p class="all-block fl"><?php echo $margin_order['auction_info']['auction_name'] ?></p>
                            </a>
                        </td>
                        <td><?php echo date('Y-m-d H:i:s', $margin_order['auction_info']['auction_start_time']) ?></td>
                        <td>&yen;<?php echo $margin_order['auction_info']['current_price'] ?></td>
                        <td>&yen;<?php echo $margin_order['relation']['offer_num'] ? $margin_order['relation']['offer_num'] : 0.00 ?></td>
                        <td>&yen;<?php echo $margin_order['margin_amount'] ?></td>
                        <td>
                            <?php if ($margin_order['order_state'] == 1) { ?>
                                已报名
                            <?php } elseif($margin_order['order_state'] == 4) { ?>
                                已取消
                            <?php } elseif($margin_order['order_state'] == 2) { ?>
                                待审核
                            <?php } else { ?>
                                未支付
                            <?php } ?>
                        </td>
                        <td>

                            <?php if ($margin_order['auction_info']['current_price'] <= $margin_order['relation']['offer_num']) { ?>
                                <span>已出价</span>
                            <?php } else { ?>
                                <?php if ($margin_order['order_state'] == 1) {?>
                                    <a href="<?php echo $margin_order['auction_url'] ?>" target="_blank"><span>出价</span></a>
                                <?php } elseif($margin_order['order_state'] == 4) { ?>
                                    <a href="<?php echo $margin_order['auction_url'] ?>" target="_blank"><span>查看</span></a>
                                <?php } elseif($margin_order['order_state'] == 2) { ?>
                                    <span>等待审核</span>
                                <?php } else { ?>
                                    <a href="<?php echo urlAuction('auction_buy', 'pay_order', array('order_sn' => $margin_order['order_sn']))?>" target="_blank">
                                        <span>支付保证金</span>
                                    </a>
                                <?php } ?>
                            <?php } ?>

                        </td>
                    </tr>
                <?php } ?>
                    <tfoot>
                    <tr>
                        <td colspan="19"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
                    </tr>
                    </tfoot>
                <?php } else { ?>
                    <div class="warning-option"><i>&nbsp;</i><span>暂无符合条件的数据记录</span></div>
                <?php } ?>
            </table>
        </div>
    </div>
</div>


