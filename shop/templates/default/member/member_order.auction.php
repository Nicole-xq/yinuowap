<?php defined('InShopNC') or exit('Access Invalid!');?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />

<div class="wrap">
    <div class="tabmenu">
        <?php include template('layout/submenu');?>
    </div>
    <form method="get" action="index.php" target="_self">
        <table class="ncm-search-table">
            <input type="hidden" name="act" value="member_auction" />
            <input type="hidden" name= "op" value="have_auction" />
            <tr>
                <td>&nbsp;</td>
                <td class="w240 tr"><input type="text" class="text w200" placeholder="输入保证金订单号进行搜索" name="keyword" value="<?php echo $_GET['keyword']; ?>"></td>
                <td class="w70 tc"><label class="submit-border">
                        <input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>"/>
                    </label></td>
            </tr>
        </table>
    </form>
    <table class="ncm-default-table order">
        <thead>
        <tr>
            <th class="w10"></th>
            <th colspan="2">商品</th>
            <th class="w90">单价（元）</th>
            <th class="w40">数量</th>
            <th class="w110">订单金额</th>
            <th class="w90">交易状态</th>
            <th class="w120">交易操作</th>
        </tr>
        </thead>
        <?php if ($output['margin_order_list']) { ?>
            <?php foreach ($output['margin_order_list'] as $order_pay_sn => $order_info) { ?>
                <tbody<?php if (!empty($order_info['pay_amount'])) {?> class="pay" <?php }?>>
                    <?php if ($order_info['pay_amount'] < 0) {?>
                        <tr>
                            <td colspan="19" class="sep-row"></td>
                        </tr>
                    <?php } ?>
                    <?php if ($order_info['auction_order']['order_state'] == 10) {?>
                        <tr>
                            <td colspan="19" class="sep-row"></td>
                        </tr>
                        <tr>
                            <td colspan="19" class="pay-td">
                                <span class="ml15">在线支付金额：
                                    <em>￥<?php echo ncPriceFormat($order_info['pay_amount']);?></em>
                                </span>
                                <a class="ncbtn ncbtn-bittersweet fr mr15" href="<?php echo urlAuction('auction_buy', 'show_auction_order', array('auction_order_id' => $order_info['auction_order']['order_id'], 'margin_id' => $order_info['margin_id']))?>">
                                    <i class="icon-shield"></i>订单支付
                                </a>
                            </td>
                        </tr>
                    <?php } else {?>
                        <tr>
                            <td colspan="19" class="sep-row"></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th colspan="19"> <span class="ml10">
          <!-- order_sn -->
                                拍卖订单号：
                                <?php echo $order_info['auction_order']['order_sn']; ?>
                                - 保证金订单号：
                                <?php echo $order_info['order_sn']; ?>
                                <?php if ($order_info['order_from'] == 2){?>
                                    <i class="icon-mobile-phone"></i>
                                <?php }?>
          </span>
                            <!-- order_time -->
                            <span><?php echo $lang['member_order_time'].$lang['nc_colon'];?><?php echo date("Y-m-d H:i:s",$order_info['auction_order']['add_time']); ?></span>

                            <!-- store_name -->
                            <span><a href="<?php echo urlShop('show_store','index',array('store_id'=>$order_info['store_id']), $order_info['extend_store']['store_domain']);?>" title="<?php echo $order_info['store_name'];?>"><?php echo $order_info['store_name']; ?></a></span>

                            <!-- QQ -->
                            <span member_id="<?php echo $order_info['extend_store']['member_id'];?>">
          <?php if(!empty($order_info['extend_store']['store_qq'])){?>
              <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $order_info['extend_store']['store_qq'];?>&site=qq&menu=yes" title="QQ: <?php echo $order_info['extend_store']['store_qq'];?>"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo $order_info['extend_store']['store_qq'];?>:52" style=" vertical-align: middle;"/></a>
          <?php }?>

                                <!-- wang wang -->
                                <?php if(!empty($order_info['extend_store']['store_ww'])){?>
                                    <a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&uid=<?php echo $order_info['extend_store']['store_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>"  class="vm" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=<?php echo $order_info['extend_store']['store_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" alt="Wang Wang"  style=" vertical-align: middle;"/></a>
                                <?php }?>
          </span>

                        </th>
                    </tr>

                    <!-- S 商品列表 -->

                    <?php if (!empty($order_info['auction_info'])) { ?>
                            <?php $i++; $auction_info = $order_info['auction_info']?>
                            <tr>
                                <td class="bdl"></td>
                                <td class="w70">
                                    <div class="ncm-goods-thumb">
                                        <a href="<?php echo $order_info['auction_url'];?>" target="_blank"><img src="<?php echo $order_info['image_60_url'];?>" onMouseOver="toolTip('<img src=<?php echo $order_info['image_240_url'];?>>')" onMouseOut="toolTip()"/></a></div>
                                </td>
                                <td class="tl">
                                    <dl class="goods-name">
                                        <dt><a href="<?php echo $order_info['auction_url'];?>" target="_blank"><?php echo $auction_info['auction_name']; ?></a><span class="rec"></span></dt>
                                    </dl></td>
                                <td><?php echo ncPriceFormat($auction_info['current_price']);?>
                                    <p class="green">
                                        <?php if ($auction_info['refund_state'] == 1) {?>
                                            <?php echo $auction_info['current_price'];?>(退)
                                        <?php } ?>
                                    </p></td>
                                <td>1</td>
                                <!-- S 合并TD -->
                                <td class="bdl" rowspan="1">
                                    <p class="">
                                        <strong>
                                            <?php echo $order_info['auction_order']['order_amount']; ?>
                                        </strong>
                                    </p>
                                    <p title="<?php echo $lang['member_order_pay_method'].$lang['nc_colon'];?><?php echo $order_info['auction_order']['payment_name']; ?>">
                                        <?php echo $order_info['auction_order']['payment_name']; ?>
                                    </p>
                                </td>

                                <td class="bdl" rowspan="1">
                                    <p>
                                        <?php echo $order_info['auction_order']['state_desc']; ?>
                                    </p>

                                    <!-- 订单查看 -->
                                    <p>
                                        <a href="index.php?act=member_auction&op=show_order&margin_id=<?php echo $order_info['margin_id']; ?>&auction_order_id=<?php echo $order_info['auction_order']['auction_order_id']; ?>" target="_blank">
                                            <?php echo $lang['member_order_view_order'];?>
                                        </a>
                                    </p>

                                    <!-- 物流跟踪 -->

                                    <?php if ($order_info['if_deliver']){ ?>
                                        <p>
                                            <a href='index.php?act=member_auction&op=search_deliver&margin_id=<?php echo $order_info['margin_id']; ?>&auction_order_id=<?php echo $order_info['auction_order']['auction_order_id']; ?>' target="_blank">
                                                <?php echo $lang['member_order_show_deliver']?>
                                            </a>
                                        </p>
                                    <?php } ?>
                                </td>

                                <td class="bdl bdr" style="display: none" rowspan="<?php echo $order_info['goods_count'];?>">
                                    <!-- 锁定-->
                                    <?php if ($order_info['if_lock']) { ?>
                                        <p>退款中</p>
                                    <?php } ?>

                                    <!-- 退款取消订单 -->

                                    <?php if ($order_info['if_refund_cancel']){ ?>
                                        <p>
                                            <a href="javascript:void(0)"
                                              class="ncbtn"
                                              nc_type="dialog"
                                              dialog_id="buyer_order_confirm_order"
                                              dialog_width="480"
                                              dialog_title="取消订单"
                                              uri="index.php?act=member_auction&op=change_state&state_type=order_cancel&order_sn=<?php echo $order_info['auction_order']['auction_order_sn']; ?>&order_id=<?php echo $order_info['auction_order']['auction_order_id']; ?>"
                                              id="order<?php echo $order_info['auction_order']['auction_order_id']; ?>_action_confirm"
                                            >
                                                <i class="icon-legal"></i>订单退款
                                            </a>
                                        </p>
                                    <?php } ?>

                                    <!-- 收货 -->

                                    <?php if ($order_info['if_receive']) { ?>
                                        <p>
                                            <a href="javascript:void(0)"
                                               class="ncbtn"
                                               nc_type="dialog"
                                               dialog_id="buyer_order_confirm_order"
                                               dialog_width="480"
                                               dialog_title="<?php echo $lang['member_order_ensure_order'];?>"
                                               uri="index.php?act=member_auction&op=change_state&state_type=order_receive&order_sn=<?php echo $order_info['auction_order']['auction_order_sn']; ?>&order_id=<?php echo $order_info['auction_order']['auction_order_id']; ?>"
                                               id="order<?php echo $order_info['auction_order']['auction_order_id']; ?>_action_confirm"
                                            >
                                                <?php echo $lang['member_order_ensure_order'];?>
                                            </a>
                                        </p>
                                    <?php } ?>
                                </td>
                                <!-- E 合并TD -->
                            </tr>
                    <?php } ?>
                    <!-- E 商品列表 -->
                </tbody>
            <?php } ?>
        <?php } else { ?>
            <tbody>
            <tr>
                <td colspan="20" class="norecord"><div class="warning-option"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></div></td>
            </tr>
            </tbody>
        <?php } ?>
        <?php if($output['margin_order_list']) { ?>
            <tfoot>
            <tr>
                <td colspan="19"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
            </tr>
            </tfoot>
        <?php } ?>
    </table>
</div>
<script charset="utf-8" type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" ></script>
<script charset="utf-8" type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/sns.js" ></script>
<script type="text/javascript">
    $(function(){
        $('#query_start_date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#query_end_date').datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
