<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />

<!--签署入驻协议-->
    <div class="mingcheng">商家资料提交</div>
    <!-- 个人资料提交 -->
    <div class="lm_upload">
        <div class="lm_container_ful lm_shopUpload">
            <form class="lm_upload_form" id="form_company_info" action="index.php?act=store_joinin&op=step2" method="post">
                <fieldset>
                    <div class="shop_title">联系方式</div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="companyname"><i class="lm_req">*</i>公司名称：</label>
                        <div class="zm_inp">
                            <input id="companyname" name="company_name" type="text" placeholder="填写公司名称">
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="companytel"><i class="lm_req">*</i>公司电话：</label>
                        <div class="zm_inp">
                            <input id="companytel" name="company_phone" type="text" placeholder="填写公司电话">
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="lm_area"><i class="lm_req">*</i>公司地址：</label>
                        <div class="zm_inp">
                            <input id="company_address" name="company_address" type="hidden" value=""/>
                            <input type="hidden" value="" name="province_id" id="province_id">
                            <input type="hidden" name="area_id_1" id="_area_1" value="">
                            <span></span>
                        </div>
                    </div>

                    <div class="zm_form-group">
                        <label class="zm_label" for="infoaddress"></label>
                        <div class="zm_inp">
                            <input id="infoaddress" name="company_address_detail" type="text" placeholder="详细地址">
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="operatename"><i class="lm_req">*</i>运营联系人：</label>
                        <div class="zm_inp">
                            <input id="operatename" name="contacts_name" type="text" placeholder="填写本人真实姓名">
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="operatetel"><i class="lm_req">*</i>运营联系人手机号：</label>
                        <div class="zm_inp">
                            <input id="operatetel" name="contacts_phone" type="text" placeholder="填写13位手机号码">
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="email"><i class="lm_req">*</i>邮箱：</label>
                        <div class="zm_inp">
                            <input id="email" name="contacts_email" type="text" placeholder="填写常用邮箱">
                            <span></span>
                        </div>
                    </div>
                    <div class="shop_title">企业信息</div>
                    <div class="zm_form-group">
                        <label class="zm_label"></label>
                        <div class="zm_inp lm_change_shop">
                            <label for="lm_default">
                                <input id="lm_default" checked name="type" value="1" type="radio">普通执照
                            </label>
                            <label for="lm_three">
                                <input id="lm_three"  name="type" value="2" type="radio">三证合一
                            </label>
                            <input type="hidden" name="card_type" id="card_type" value="1"/>
                            <span></span>
                      </div>
                    </div>
                    <div class="zm_form-group lm_defult_div">
                        <label class="zm_label" for="lm_yyzz"><i class="lm_req">*</i>营业执照：</label>
                        <div class="zm_inp">
                            <input id="lm_yyzz" name="business_licence_number" type="text" placeholder="填写营业执照">
                            <span></span>
                            <div class="sign_webupload signUp_1 lm_yy">
                                <div class="upload_btn">
                                    <div class="btn_up lm_upload_btn">
                                        <input name="business_licence_number_elc" type="file" class="w60" />
                                        <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                        <span>上传营业执照</span>
                                        <input name="business_licence_number_elc1" type="hidden"/>
                                        <span></span>
                                    </div>
                                    <ul class="lm_upload_imgs">
                                        <li><img src="" alt=""></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="zm_form-group lm_three_div">
                        <label class="zm_label" for="lm_xydm"><i class="lm_req">*</i>统一信用代码：</label>
                        <div class="zm_inp">
                            <input id="lm_xydm" name="tax_registration_certificate" type="text" placeholder="填写统一信用代码"><!--税务登记证号-->
                            <span></span>
                            <div class="sign_webupload signUp_2 lm_yy">
                                <div class="upload_btn ">

                                    <div class="btn_up lm_upload_btn">
                                        <input name="all_cards_elc" type="file" class="w60" />
                                        <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                        <span>上传</span>
                                        <input name="all_cards_elc1" type="hidden"/><span></span>
                                    </div>
                                    <ul class="lm_upload_imgs">
                                        <li><img src="" alt=""></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" for="lm_area_1"><i class="lm_req">*</i>所在地：</label>
                        <div class="zm_inp">
                            <input id="business_licence_address" name="business_licence_address" type="hidden" />
                            <span></span>
                        </div>
                    </div>
                    <div class="zm_form-group">
                        <label class="zm_label" ><i class="lm_req">*</i>有效期：</label>
                        <div class="zm_inp">
                            <input class='lm_begin_date' type="text" id="business_licence_start" name="business_licence_start" placeholder="起始时间">
                            <span class='middle_hr'></span>
                            <input class='lm_end_date' type="text" id="business_licence_end" name="business_licence_end" placeholder="结束时间">
                        </div>
                    </div>

                    <div class="zm_form-group">
                        <label class="zm_label" for="lm_fdfw"><i class="lm_req">*</i>法定范围：</label>
                        <div class="zm_inp">
                            <textarea class="lm_textarea" name="business_sphere" id='lm_fdfw' rows="3"></textarea>
                            <span></span>
                        </div>
                    </div>

                    <div class="zm_form-group lm_defult_div">
                        <label class="zm_label" for="lm_zzjg"><i class="lm_req">*</i>组织机构代码：</label>
                        <div class="zm_inp">
                            <input id="lm_zzjg" name="organization_code" type="text" placeholder="填写组织机构代码">
                            <span></span>
                            <div class="sign_webupload signUp_3 lm_yy">

                                <div class="btn_up lm_upload_btn">
                                    <input name="organization_code_electronic" type="file" class="w60" />
                                    <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                    <span>上传图片</span>
                                    <input name="organization_code_electronic1" type="hidden"/><span></span>
                                </div>
                                <ul class="lm_upload_imgs">
                                    <li><img src="" alt=""></li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="zm_form-group lm_defult_div">
                        <label class="zm_label" for="lm_nsr"><i class="lm_req"></i>一般纳税人：</label>
                        <div class="zm_inp">
                            <div class="sign_webupload signUp_4 lm_yy">

                                <div class="btn_up lm_upload_btn">
                                    <input name="general_taxpayer" type="file" class="w60" />
                                    <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                    <span>上传图片</span>
                                    <input name="general_taxpayer1" type="hidden"/>
                                    <span></span>
                                </div>
                                <ul class="lm_upload_imgs">
                                    <li><img src="" alt=""></li>
                                </ul>

                            </div>
                            <div class="clear"></div>
                            <div>提示：仅支持JPG\GIF\PNG格式图片，大小请控制在1M之内。</div>
                        </div>
                    </div>

                    <p>
                        <input class="submit" type="submit" value="Submit">
                    </p>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="anniu_shuang">
        <a class="old" href="<?php echo urlShop('store_joinin', 'step0');?>">上一步，入驻协议</a>
        <a class="new" id="btn_apply_company_next" href="javascript:;" id='lm_shop_btn'>下一步，财务帐号</a>
    </div>


<script>
    $(function () {
        // 普通证件和三证合一的切换
        var $dom = $('.lm_change_shop');
        var $form = $('#form_company_info');

        function pubStart(self) {
            if ($(self).prop('checked') && $(self).attr('id') === 'lm_three') {
                $form.find('.lm_three_div').show();
                $form.find('.lm_defult_div').hide();
            } else {
                $form.find('.lm_three_div').hide();
                $form.find('.lm_defult_div').show();
            }
        }

        $dom.find('input').on('click', function () {
            pubStart(this)
        })
        $dom.find('input').each(function () {
            pubStart(this)
        })
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        <?php foreach (array('business_licence_number_elc','organization_code_electronic','general_taxpayer','all_cards_elc') as $input_id) { ?>
        $('input[name="<?php echo $input_id;?>"]').fileupload({
            dataType: 'json',
            url: '<?php echo urlShop('store_joinin', 'ajax_upload_image');?>',
            formData: '',
            add: function (e,data) {
                data.submit();
            },
            done: function (e,data) {
                if (!data.result){
                    alert('上传失败，请尝试上传小图或更换图片格式');return;
                }
                if(data.result.state) {
                    $('input[name="<?php echo $input_id;?>"]').nextAll().remove('img');
                    $(this).parent().next().find('img').attr('src', data.result.pic_url);
                    //$('input[name="<?php echo $input_id;?>"]').after('<img height="60" src="'+data.result.pic_url+'">');
                    $('input[name="<?php echo $input_id;?>1"]').val(data.result.pic_name);
                } else {
                    alert(data.result.message);
                }
            },
            fail: function(){
                alert('上传失败，请尝试上传小图或更换图片格式');
            }
        });
        <?php } ?>
        $('#company_address').nc_region();
        $('#business_licence_address').nc_region();
        $('#business_licence_address1').nc_region();

        $('#business_licence_start').datepicker();
        $('#business_licence_end').datepicker();

        $('#business_licence_start1').datepicker();
        $('#business_licence_end1').datepicker();

        $('#btn_apply_agreement_next').on('click', function() {
            if($('#input_apply_agreement').prop('checked')) {
                $('#apply_agreement').hide();
                $('#apply_company_info').show();
            } else {
                alert('请阅读并同意协议');
            }
        });

        $('#form_company_info').validate({
            errorPlacement: function (error, element) {
                if(element.hasClass('lm_begin_date')){
                    error.insertAfter(element.nextAll('input'));
                    return;
                }

                if (element.is(':radio') || element.is(':checkbox') || element.is('select') || element.hasClass('lmWebUploadHidden')) { //如果是radio或checkbox
                   // var eid = element.attr('name'); //获取元素的name属性
                    error.appendTo(element.parent()); //将错误信息添加当前元素的父结点后面
                } else if (element.is(':hidden')) {
                    error.appendTo(element.parent());
                } else {
                    error.insertAfter(element);
                }
                //element.nextAll('span').first().after(error);    //原始

            },
            rules: {
                company_name: {
                    required: true,
                    maxlength: 50
                },
                company_address: {
                    required: true,
                    maxlength: 50
                },
                company_address_detail: {
                    required: true,
                    maxlength: 50
                },
                company_phone: {
                    required: true,
                    maxlength: 20
                },
                company_employee_count: {
                    required: true,
                    digits: true
                },
                company_registered_capital: {
                    required: true,
                    digits: true
                },
                contacts_name: {
                    required: true,
                    maxlength: 20
                },
                contacts_phone: {
                    required: true,
                    maxlength: 20
                },
                contacts_email: {
                    required: true,
                    email: true
                },
                business_licence_number: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 20
                },
                business_licence_address: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 50
                },
                business_licence_start: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}}
                },
                business_licence_end: {
                    required: function (e) {

                        if(!$(e).prevAll(':input').prop('value').length > 0){
                            return false;
                        }

                        if ($('#card_type').val() == 1) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                business_sphere: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 500
                },
                business_licence_number_elc1: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}}
                },
                organization_code: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 20
                },
                organization_code_electronic1: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}}
                },
                business_licence_number1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 20
                },
                business_licence_address1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 50
                },
                business_licence_start1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}}
                },
                business_licence_end1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}}
                },
                business_sphere1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 500
                },
                all_cards_elc1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}}
                },
                organization_code1: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 20
                },
                tax_registration_certificate: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 20
                },
                taxpayer_id: {
                    required: function () {if ($('#card_type').val() == 2) {return true;} else {return false;}},
                    maxlength: 20
                }
            },
            messages: {
                company_name: {
                    required: '请输入公司名称',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_address: {
                    required: '请选择区域地址',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_address_detail: {
                    required: '请输入公司详细地址',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_phone: {
                    required: '请输入公司电话',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_employee_count: {
                    required: '请输入员工总数',
                    digits: '必须为数字'
                },
                company_registered_capital: {
                    required: '请输入注册资金',
                    digits: '必须为数字'
                },
                contacts_name: {
                    required: '请输入联系人姓名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                contacts_phone: {
                    required: '请输入联系人电话',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                contacts_email: {
                    required: '请输入常用邮箱地址',
                    email: '请填写正确的邮箱地址'
                },
                business_licence_number: {
                    required: '请输入营业执照号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                business_licence_address: {
                    required: '请选择营业执照所在地',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                business_licence_start: {
                    required: '请选择生效日期'
                },
                business_licence_end: {
                    required: '请选择结束日期'
                },
                business_sphere: {
                    required: '请填写营业执照法定经营范围',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                business_licence_number_elc1: {
                    required: '请上传照片'
                },
                organization_code: {
                    required: '请填写组织机构代码',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                organization_code_electronic1: {
                    //required: '请选择上传组织机构代码证电子版文件'
                    required: '请上传文件'
                },
                business_licence_number1: {
                    required: '请输入营业执照号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                business_licence_address1: {
                    required: '请选择营业执照所在地',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                business_licence_start1: {
                    required: '请选择生效日期'
                },
                business_licence_end1: {
                    required: '请选择结束日期'
                },
                business_sphere1: {
                    required: '请填写营业执照法定经营范围',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                all_cards_elc1: {
                    required: '请选择上传企业三证合一电子版文件'
                },
                organization_code1: {
                    required: '请填写组织机构代码',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                tax_registration_certificate: {
                    required: '请填写税务登记证号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                taxpayer_id: {
                    required: '请填写纳税人识别号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                }
            }
        });

        $('#btn_apply_company_next').on('click', function() {
            if($('#form_company_info').valid()) {
                $('#province_id').val($("#company_address").fetch('area_id_1'));
                $('#form_company_info').submit();
            }
        });
        $('input[name="type"]').click(function(){
            if($('input[name="type"]:checked ').val() == 1){
                $('.three').css('display','block');
                $('.one').css('display','none');
                $('#card_type').val(1);
            }else if($('input[name="type"]:checked ').val() == 2){
                $('.three').css('display','none');
                $('.one').css('display','block');
                $('#card_type').val(2);
            }
        });
        if($('#card_type').val()== 1){
            $('.three').css('display','block');
            $('.one').css('display','none');
        }else{
            $('.three').css('display','none');
            $('.one').css('display','block');
        }

    });
</script>
