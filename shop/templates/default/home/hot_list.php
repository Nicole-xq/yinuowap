<div class="hot-list fr">
			<div class="hot-goods fr">
				<img src="<?php echo SHOP_TEMPLATES_URL;?>/images/new-index/hot-goods-title.jpg"  alt=""/>
				<ul>
				<?php foreach($output['auctions_item'] as $val):?>
					<li>
					<a href="<?php echo urlvendue('auctions','index',array('id'=>$val['auction_id']));?>" title="">
						<img src="<?php echo UPLOAD_SITE_URL."/".ATTACH_GOODS."/".$val['store_id']."/".$val['auction_image'];?>"  alt=""/>
						<div class="title"><?php echo $val['auction_name']?></div>
						<div class="info"><span class="fl">当前价&nbsp;<b>￥<?php echo $val['current_price'] > 0 ? $val['current_price'] : $val['auction_start_price']; ?>元</b></span><span class="fr"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;<b><?php echo $val['bid_number'] ? $val['bid_number'] : 0;?></b>&nbsp;次</span></div>
					</a>
					</li>
				<?php endforeach;?>
				</ul>
			</div>
</div>