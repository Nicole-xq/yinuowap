<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript">
$(document).ready(function(){
    $('#guess_member').find('.demo').ajaxContent({
      event:'click', //mouseover
      loaderType:"img",
      loadingMsg:"<?php echo SHOP_TEMPLATES_URL;?>/images/transparent.gif",
      target:'#guess_member'
    });

});
</script>

 <div class="content" id="ncGoodsIntro">
      <table width="960" border="0" cellspacing="0" cellpadding="0" class="auction-record">
      <tr style="background: #eeeeee">
      <td>状态</td>
      <td>竞猜人</td>
      <td>价格</td>
      <td>时间</td>
      </tr>
      <?php if(!empty($output['gs_member_list']) && is_array($output['gs_member_list'])):?>
      <?php foreach($output['gs_member_list'] as $key=>$member):?>
          <tr>
          <td><span style="background: <?php if(in_array($member['gs_state'], array(2,3))){echo "#800000";}else{echo "#898989";}?>"><?php if(in_array($member['gs_state'],array(2,3))){echo "中奖";}else{echo "参与";} ?></span></td>
          <td><?php echo hideStar($member['member_name']);?></td>
          <td><?php echo $member['gs_offer_price'];?></td>
          <td><?php echo date('Y-m-d H:i:s', $member['partake_time']);?></td>
          </tr>
      <?php endforeach;?>
      <?php else:?>
         <tbody>
          <tr>
            <td colspan="10" class="ncs-norecord"><?php echo $lang['no_record'];?></td>
          </tr>
        </tbody>
      <?php endif;?>
      </table>
      <?php if(!empty($output['gs_member_list']) && is_array($output['gs_member_list'])):?>
     <div class="pagination"> <?php echo $output['show_page'];?> </div> 
      <?php endif;?>
    </div>

