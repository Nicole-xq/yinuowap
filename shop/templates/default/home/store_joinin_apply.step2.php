<?php defined('InShopNC') or exit('Access Invalid!');?>

<!-- 公司资质 -->
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>

<!--签署入驻协议-->
<div class="mingcheng">财务账号</div>
<!-- 个人资料提交 -->
<div class="lm_upload">
    <div class="lm_container_ful">
        <form  id="form_credentials_info" class="lm_upload_form" action="index.php?act=store_joinin&op=step3" method="post" >
            <fieldset>
                <div class="zm_form-group">
                    <label class="zm_label" for="bankname"><i class="lm_req">*</i>银行开户名：</label>
                    <div class="zm_inp">
                        <input id="bankname" name="bank_account_name" type="text" placeholder="填写本人真实姓名">
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="bankcard"><i class="lm_req">*</i>银行账号：</label>
                    <div class="zm_inp">
                        <input id="bankcard" name="bank_account_number" type="text" placeholder="填写银行账号">
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="lm_area"><i class="lm_req">*</i>开户银行支行名称：</label>
                    <div class="zm_inp">
                        <input id="bank_address" name="bank_address" type="hidden" />
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="infoaddress"></label>
                    <div class="zm_inp">
                        <input id="infoaddress" name="bank_name" type="text" placeholder="填写开户银行支行名称">
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="zhbankname"><i class="lm_req">*</i>支行联行号：</label>
                    <div class="zm_inp">
                        <input id="zhbankname" name="bank_code" type="text" placeholder="填写支行联行号">
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="lm_zzjg"><i class="lm_req">*</i>开户银行许可证：</label>
                    <div class="zm_inp">

                        <div class="btn_up lm_upload_btn">
                            <input name="bank_licence_electronic" type="file" class="w60" />
                            <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                            <span>上传图片</span>
                            <input name="bank_licence_electronic1" type="hidden"/>
                            <span></span>
                        </div>
                        <ul class="lm_upload_imgs">
                            <li><img src="" alt=""></li>
                        </ul>

                        <div class="clear"></div>
                        <div>提示：仅支持JPG\GIF\PNG格式图片，大小请控制在1M之内。</div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="anniu_shuang">
    <a class="old" href="<?php echo urlShop('store_joinin', 'step1');?>">上一步，资料填写</a>
    <a class="new" id="btn_apply_credentials_next" href="javascript:;" id='lm_bankshop_btn'>下一步，选择入驻类目</a>
</div>



<script type="text/javascript">
    var card_type= <?php echo $output['joinin_info']['card_type']?>;
    $(document).ready(function(){
        <?php foreach (array('tax_registration_certif_elc','bank_licence_electronic') as $input_id) { ?>
        $('input[name="<?php echo $input_id;?>"]').fileupload({
            dataType: 'json',
            url: '<?php echo urlShop('store_joinin', 'ajax_upload_image');?>',
            formData: '',
            add: function (e,data) {
                data.submit();
            },
            done: function (e,data) {
                if (!data.result){
                    alert('上传失败，请尝试上传小图或更换图片格式');return;
                }
                if(data.result.state) {
                    $('input[name="<?php echo $input_id;?>"]').nextAll().remove('img');
                    $(this).parent().next().find('img').attr('src', data.result.pic_url);
                    //$('input[name="<?php echo $input_id;?>"]').after('<img height="60" src="'+data.result.pic_url+'">');
                    $('input[name="<?php echo $input_id;?>1"]').val(data.result.pic_name);
                } else {
                    alert(data.result.message);
                }
            },
            fail: function(){
                alert('上传失败，请尝试上传小图或更换图片格式');
            }
        });
        <?php } ?>
        var use_settlement_account = true;
        $("#bank_address").nc_region();
        $("#settlement_bank_address").nc_region();

        $("#is_settlement_account").on("click", function() {
            if($(this).prop("checked")) {
                use_settlement_account = false;
                $("#div_settlement").hide();
                $("#settlement_bank_account_name").val("");
                $("#settlement_bank_account_number").val("");
                $("#settlement_bank_name").val("");
                $("#settlement_bank_code").val("");
                $("#settlement_bank_address").val("");
            } else {
                use_settlement_account = true;
                $("#div_settlement").show();
            }
        });

        $('#form_credentials_info').validate({
            errorPlacement: function(error, element){
                element.nextAll('span').first().after(error);
            },
            rules : {
                bank_account_name: {
                    required: true,
                    maxlength: 50
                },
                bank_account_number: {
                    required: true,
                    maxlength: 20
                },
                bank_name: {
                    required: true,
                    maxlength: 50
                },
                bank_code: {
                    required: true,
                    maxlength: 20
                },
                bank_address: {
                    required: true
                },
                bank_licence_electronic1: {
                    required: true
                },
                settlement_bank_account_name: {
                    required: function() { return use_settlement_account; },
                    maxlength: 50
                },
                settlement_bank_account_number: {
                    required: function() { return use_settlement_account; },
                    maxlength: 20
                },
                settlement_bank_name: {
                    required: function() { return use_settlement_account; },
                    maxlength: 50
                },
                settlement_bank_code: {
                    required: function() { return use_settlement_account; },
                    maxlength: 20
                },
                settlement_bank_address: {
                    required: function() { return use_settlement_account; }
                },
                tax_registration_certificate: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 20
                },
                taxpayer_id: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}},
                    maxlength: 20
                },
                tax_registration_certif_elc1: {
                    required: function () {if ($('#card_type').val() == 1) {return true;} else {return false;}}
                }

            },
            messages : {
                bank_account_name: {
                    required: '请填写银行开户名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_account_number: {
                    required: '请填写公司银行账号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_name: {
                    required: '请填写开户银行支行名称',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_code: {
                    required: '请填写支行联行号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_address: {
                    required: '请选择开户银行所在地'
                },
                bank_licence_electronic1: {
                    required: '请选择上传开户银行许可证电子版文件'
                },
                settlement_bank_account_name: {
                    required: '请填写银行开户名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                settlement_bank_account_number: {
                    required: '请填写公司银行账号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                settlement_bank_name: {
                    required: '请填写开户银行支行名称',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                settlement_bank_code: {
                    required: '请填写支行联行号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                settlement_bank_address: {
                    required: '请选择开户银行所在地'
                },
                tax_registration_certificate: {
                    required: '请填写税务登记证号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                taxpayer_id: {
                    required: '请填写纳税人识别号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                tax_registration_certif_elc1: {
                    required: '请选择上传税务登记证号电子版文件'
                }
            }
        });

        $('#btn_apply_credentials_next').on('click', function() {
            if($('#form_credentials_info').valid()) {
                $('#form_credentials_info').submit();
            }
        });
        if(card_type == 1){
            $('.tax').css('display','block');
        }else{
            $('.tax').css('display','none');
        }

    });
</script>