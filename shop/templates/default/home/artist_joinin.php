<?php defined('InShopNC') or exit('Access Invalid!');?>

<style>
    .ruzhu_banner_wk {
        background: #0d0d0d;
        margin: 0 auto;
        height: 400px
    }

    .ruzhu_banner_wk .ruzhu_nr {
        width: 1200px;
        height: 400px;
        margin: 0 auto
    }

    .ruzhu_banner_wk .ruzhu_nr .ruzhu_banner {
        width: 900px;
        float: left
    }

    #ruzhutab {
        float: left;
        width: 300px;
        padding: 33px 0;
    }

    #ruzhutab li {
        height: 90px;
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_tab_2.gif) left no-repeat #dfdfdf
    }

    #ruzhutab li a {
        display: block;
        width: 300px;
        height: 65px;
        padding-top: 25px;
        padding-left: 100px;
        font-size: 18px;
        line-height: 40px;
        color: #777777
    }

    #ruzhutab li a:hover {
        color: #000
    }

    #ruzhutab .tab {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_tab_1.gif) left no-repeat #e3b631
    }

    #ruzhutab .tab span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_04.png)
    }

    #ruzhutab .tab a {
        color: #000
    }

    #ruzhutab span {
        display: block;
        width: 40px;
        height: 40px;
        float: left
    }

    #ruzhutab p {
        float: left
    }

    .shenqing span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_01.png);
    }

    .shenqing:hover span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_04.png)
    }

    .chaxun span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_02.png);
    }

    .chaxun a {
        padding-left: 80px !important;
    }

    .chaxun:hover span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_05.png)
    }

    .zixun span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_03.png);
    }

    .zixun:hover span {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/tab_icon_06.png)
    }

    .ruzhu_liuchen {
        margin: 30px auto;
        width: 1200px;
        overflow: hidden
    }

    .ruzhu_liuchen li {
        float: left
    }

    .ruzhu_liuchen .mr12 {
        margin-right: 12px;
    }

    .mb32 {
        margin-bottom: 32px;
    }
</style>

<!--banner-->
<div class="ruzhu_banner_wk">
  <div class="ruzhu_nr">
    <div class="ruzhu_banner"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_banner.gif" /></div>
    <div id="ruzhutab">
      <ul>
        <li class="tab mb32 shenqing"><a href="<?php echo urlShop('artist_joinin', 'index');?>" target="_blank"><span></span><p>申请入驻</p></a></li>
        <li class="mb32 chaxun"><a href="<?php echo urlShop('artist_joinin', 'index');?>" target="_blank"><span></span>申请进度查询</a></li>
        <li class="zixun"><a href="#" target="_blank"><span></span>在线咨询</a></li>
      </ul>
    </div>
  </div>
</div>


<!--流程-->
<div class="ruzhu_liuchen">
  <ul>
    <li class="mr12"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_liuchen_1.gif"/></li>
    <li class="mr12"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_liuchen_2.gif"/></li>
    <li class="mr12"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_liuchen_3.gif"/></li>
    <li class="mr12"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_liuchen_4.gif"/></li>
    <li><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/ruzhu_liuchen_5.gif"/></li>
  </ul>
</div>

<script>
    $('#ruzhutab li').mouseover(function(){
        $('#ruzhutab .tab').removeClass('tab');
        $(this).addClass('tab');
    });
</script>

<div class="main mt30">
    <h2 class="index-title">入驻指南</h2>
    <div class="joinin-info">
        <ul class="tabs-nav">
            <?php if(!empty($output['help_list']) && is_array($output['help_list'])){ $i = 0;?>
                <?php foreach($output['help_list'] as $key => $val){ $i++;?>
                    <li class="<?php echo $i==1 ? 'tabs-selected':'';?>">
                        <h3><?php echo $val['help_title'];?></h3>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
        <?php if(!empty($output['help_list']) && is_array($output['help_list'])){ $i = 0;?>
            <?php foreach($output['help_list'] as $key => $val){ $i++;?>
                <div class="tabs-panel <?php echo $i==1 ? '':'tabs-hide';?>"><?php echo $val['help_info'];?></div>
            <?php } ?>
        <?php } ?>
    </div>
</div>



<script>
    $(document).ready(function(){
        $("#login_form ").validate({
            errorPlacement: function(error, element){
                var error_td = element.parent('dd');
                error_td.find('label').hide();
                error_td.append(error);
            },
            rules: {
                user_name: "required",
                password: "required"
                <?php if(C('captcha_status_login') == '1') { ?>
                ,captcha : {
                    required : true,
                    minlength: 4,
                    remote   : {
                        url : 'index.php?act=seccode&op=check&nchash=<?php echo getNchash();?>',
                        type: 'get',
                        data:{
                            captcha : function(){
                                return $('#captcha').val();
                            }
                        }
                    }
                }
                <?php } ?>
            },
            messages: {
                user_name: "用户名不能为空",
                password: "密码不能为空"
                <?php if(C('captcha_status_login') == '1') { ?>
                ,captcha : {
                    required : '验证码不能为空',
                    minlength: '验证码不能为空',
                    remote	 : '验证码错误'
                }
                <?php } ?>
            }
        });
    });
</script>
<?php if( $pic_n > 1) { ?>
    <script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<?php }else { ?>
    <script>
        $(document).ready(function(){
            $(".tabs-nav > li > h3").bind('mouseover', (function(e) {
                if (e.target == this) {
                    var tabs = $(this).parent().parent().children("li");
                    var panels = $(this).parent().parent().parent().children(".tabs-panel");
                    var index = $.inArray(this, $(this).parent().parent().find("h3"));
                    if (panels.eq(index)[0]) {
                        tabs.removeClass("tabs-selected").eq(index).addClass("tabs-selected");
                        panels.addClass("tabs-hide").eq(index).removeClass("tabs-hide");
                    }
                }
            }));
        });
    </script>
<?php } ?>
