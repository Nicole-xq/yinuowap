<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<style type="text/css">
	.category { display: block !important; }
	.home-focus-layout .right-sidebar{filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#B2000000', endColorstr='#B2000000');background:rgba(0,0,0,0.75);width: 120px; border:none; margin-left: 480px;}
	.home-focus-layout .right-sidebar .top-account img,.home-focus-layout .right-sidebar .activity img{margin:12px auto; width:68px; height:68px; overflow: hidden; border-radius: 100px;}
	.home-focus-layout .right-sidebar .top-account p{text-align:center; font-size: 14px; color: #fff;width:120px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
	.home-focus-layout .right-sidebar .top-account a,.home-focus-layout .right-sidebar .activity a{color:#fff; text-align: center;display:block}
	.home-focus-layout .right-sidebar .login{padding:2px 14px; background: #800000;width:54px;border-radius: 100px;margin: 8px auto 6px !important}
	.home-focus-layout .right-sidebar .activity h1{background:#000;text-align: center; color: #fff;padding:6px 0;margin:0 0 6px;width:122px;transform:translateX(-1px);font-size: 14px}
	.home-focus-layout .right-sidebar .activity a{margin:4px 0;font-size: 13px}
	#ncToolbar{display:none;}
	.right-sidebar .activity{margin:12px 0}
	.activity-name{display:block;width: 80px;text-align: center;margin:12px auto 0;font-size: 14px;line-height: 24px}
	#new-tab .tab_menu{width:881px}
	.hot-list-top{width:317px;height:40px;background:#fff;border: #e5e5e5 solid 1px;border-left:none;margin-top:20px}
	.hot-list{margin:5px 0 0}
</style>
<div class="clear"></div>
<!-- HomeFocusLayout Begin-->
<div class="home-focus-layout"> <?php echo $output['web_html']['index_pic'];?>
  <div class="right-sidebar">
  	<div class="top-account">
  		<?php if($_SESSION['is_login'] === '1'):?>
		<a href="<?php echo urlShop('member','home');?>" title=""><img src="<?php echo getMemberAvatar($_SESSION['avatar']);?>"  alt="" class="block"/></a>
		<p><?php echo $_SESSION['member_name'];?></p>
	<?php else :?>
		<a href="<?php echo urlLogin('login');?>" title="" class="login">请登录</a>
		<a href="<?php echo urlLogin('login','register');?>" title="">免费注册&nbsp;&raquo;</a>
	<?php endif;?>
	</div>
	<!--<div class="activity">
		<h1>推荐活动</h1>
		<?php if(C('rec_act_link1') && C('rec_act_img')):?>
		<a href="<?php echo C('rec_act_link1');?>" title="<?php echo C('rec_act_name1');?>"><img src="<?php echo UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.C('rec_act_img');?>"  alt="" class="block"/></a>
		<?php endif;?>
		<?php if(C('rec_act_link2') && C('rec_act_name2')):?>
		<a href="<?php echo C('rec_act_link2');?>" title="<?php echo C('rec_act_name2');?>">推荐活动</a>
		<?php endif;?>
		<?php if(C('rec_act_link3') && C('rec_act_name3')):?>
		<a href="<?php echo C('rec_act_link3');?>" title="<?php echo C('rec_act_name3');?>">推荐活动</a>
		<?php endif;?>
		<?php if(C('rec_act_link4') && C('rec_act_name4')):?>
		<a href="<?php echo C('rec_act_link4');?>" title="<?php echo C('rec_act_name4');?>">推荐活动</a>
		<?php endif;?>
	</div>-->
	<div class="activity">
		<h1>我要入驻</h1>
		<a href="<?php echo urlShop('store_joinin', 'step0');?>" title="">入驻协议</a>
		<a href="<?php echo urlShop('show_help','index');?>" title="">入驻帮助</a>
		<a href="<?php echo urlShop('show_joinin', 'index');?>" title="">卖家入驻</a>
	</div>
	<div class="activity">
		<h1>推荐活动</h1>
		<a href="<?php echo C('rec_act_link1');?>" title="<?php echo C('rec_act_name1');?>">
		<img src="<?php echo C('rec_act_img') ? UPLOAD_SITE_URL.'/'.'shop/pay_guess/index'.DS.C('rec_act_img') : UPLOAD_SITE_URL.DS.'shop\/common\/default_goods_image_240.gif';?>"  alt="<?php echo C('rec_act_name1');?>" class="block" style="margin-top:15px;border:#232326 solid 1px" />
		<span class="activity-name"><?php echo C('rec_act_name1');?></span>
		</a>
		<a href="<?php echo C('rec_act_link4');?>" style="margin-top:12px;text-align:center;width:100%;display:inline-block;color:#cc2929;font-size:14px">查看更多活动</a>
	</div>
  </div>
</div>
<!--HomeFocusLayout End-->
<div class="wrapper">
  <!--首页新添加开始-->
  <div class="artist-profile middle-box">
		<div class="artist-profile-word fl">
		  <h1>艺术家们都来啦！</h1>
			<h2>你还在犹豫什么？</h2>
			<a href="/shop/index.php?act=show_joinin&op=index" title="" target="_blank">立即入驻艺诺网&nbsp;&gt;&gt;</a>
		</div>
		<ul class="artist-list fl">
			<?php if(!empty($output['artist_list'])){?>
			<?php foreach($output['artist_list'] as $k=>$v){?>
			<li>
				<a href="<?php echo urlVendue('index','artist_detail',array('artist_id'=>$v['artist_vendue_id']))?>" title="" target="_blank">
					<span class="123">
						<p><?php echo $v['artist_name']?></p>
						<p><?php echo $v['artist_job_title']?></p>
						<i class="icon-double-angle-down"></i>
					</span>
					<img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$v['artist_image']?>"  alt=""/>
				</a>
			</li>
				<?php }?>
			<?php }?>

		</ul>
		<a href="<?php echo urlVendue('index','artist_list')?>" title="" target="_blank" class="artist-more">查看更多艺术家&nbsp;&gt;&gt;</a>
		<div class="clear"></div>
	</div>
  <div class="middle-box">
		<div class="big-title">
			<h1>拍卖推荐</h1>
			<h2>品质行货，特惠商品抱回家!</h2>
		</div>
		<div id="new-tab" class="fl">
			<ul class="tab_menu">
				<li class="selected">正在进行</li>
				<li>即将开始</li>
				<li>已经结束</li>
			</ul>
			<div class="tab_box">
				<div>
					<?php if(!empty($output['start_special_list'])){?>
							<?php foreach($output['start_special_list'] as $k=>$v){?>
					<a href="<?php echo urlVendue('special_details','index',array('special_id'=>$v['special_id']))?>" target="_blank" title="">
							<img src="<?php echo getVendueLogo($v['special_image'])?>"  alt=""/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $v['special_name']?></h1>
								<?php if($v['delivery_mechanism'] != ''){?>
									<span>送拍机构&nbsp;&nbsp;<b><?php echo $v['delivery_mechanism']?></b></span>
									<?php }?>
								</div>
								<div class="offer fr black-bg">
									<b><?php echo $v['all_bid_number']?></b>
									<b>次出价</b>
								</div>
								<div class="clear"></div>
								<dd class="time">
									<span class="sell">距&nbsp;结&nbsp;束&nbsp;</span>
									<span class="time-remain" count_down="<?php echo $v['special_end_time']-TIMESTAMP; ?>">
									<em time_id="d">0</em>天<em time_id="h">0</em>时 <em time_id="m">0</em>分<em time_id="s">0</em>秒</span>
								</dd>
							</div>
						</a>
								<?php }?>
					<?php }else{?>
						<div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂无推荐进行的专场信息</span></div>
					<?php }?>
					<a href="<?php echo urlVendue('index','index')?>" title="" class="tab-more">未来七天共&nbsp;<b><?php echo $output['special_count']?></b>&nbsp;场</a>
				</div>
				<div class="hide">
					<?php if(!empty($output['nostart_special_list'])){?>
							<?php foreach($output['nostart_special_list'] as $k=>$v){?>
				    <a href="<?php echo urlVendue('special_details','index',array('special_id'=>$v['special_id']))?>" target="_blank" title="">
							<img src="<?php echo getVendueLogo($v['special_image'])?>"  alt=""/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $v['special_name']?></h1>
									<?php if($v['delivery_mechanism'] != ''){?>
										<span>送拍机构&nbsp;&nbsp;<b><?php echo $v['delivery_mechanism']?></b></span>
									<?php }?>
								</div>
								<div class="offer fr green-bg">
									<b><?php echo $v['special_click']?></b>
									<b>次围观</b>
								</div>
								<div class="clear"></div>
								<dd class="time">
									<span class="sell">开始时间&nbsp;</span>
									<span class="time-remain"><?php echo date('Y-m-d H:i',$v['special_start_time'])?></span>
								</dd>
							</div>
						</a>
								<?php }?>
					<?php }else{?>
						<div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂无推荐即将开始的专场信息</span></div>
					<?php }?>
				</div>
				<div class="hide">
					<?php if(!empty($output['end_special_list'])){?>
							<?php foreach($output['end_special_list'] as $k=>$v){?>
					<a href="<?php echo urlVendue('special_details','index',array('special_id'=>$v['special_id']))?>" target="_blank" title="">
							<img src="<?php echo getVendueLogo($v['special_image'])?>"  alt=""/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $v['special_name']?></h1>
								<?php if($v['delivery_mechanism'] != ''){?>
									<span>送拍机构&nbsp;&nbsp;<b><?php echo $v['delivery_mechanism']?></b></span>
									<?php }?>
								</div>
								<div class="offer fr gray-bg">
									<b><?php echo $v['jian_num']?></b>
									<b>件成交</b>
								</div>
								<div class="clear"></div>
								<dd class="time">
									<span class="sell">结束时间&nbsp;</span>
									<span class="time-remain"><?php echo date('Y-m-d H:i',$v['special_end_time'])?></span>
								</dd>
							</div>
						</a>
								<?php }?>
					<?php }else{?>
						<div class="no-data"><img src="templates/default/images/no-data.png" width="250" height="auto" alt=""><span class="all-block">暂无推荐结束的专场信息</span></div>
					<?php }?>

				</div>
			</div>
		</div>
		<div class="fr hot-list-top"></div>
		<?php include template('home/hot_list');?>
		<div class="clear"></div>
	</div>
  <div class="mt10">
     <div class="mt10">
     <?php echo loadadv(45);?>
	  </div>
	  <div class="clear"></div>
  </div>
  <div class="middle-box">
		<div class="big-title">
			<h1>趣猜推荐</h1>
			<h2>品质行货，特惠商品抱回家!</h2>
		</div>
		<div class="i-guess">
		<?php foreach($output['rec_guess_list'] as $k => $v):?>
			<a href="<?php echo urlShop('show_guess','index',array('gs_act_id' => $v['id']));?>" title="<?php echo $v['name'];?>"><img src="<?php echo  UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess'.DS.$v['gs_act_img'];?>" width="600" height="250"  alt="<?php echo $v['name'];?>"/></a>
		<?php endforeach;?>
	    </div>
		<div class="clear"></div>
  </div>
  <!--首页新添加结束-->
</div>
<!--StandardLayout Begin-->
<!--首页楼层开始-->
 <div class="middle-box index-floor">
	<div class="big-title">
			<h1>精品推荐</h1>
			<h2>品质行货，特惠商品抱回家!</h2>
		</div>
		<?php echo $output['web_html']['index'];?>
</div>
 <div class="middle-box">
	<div class="big-title">
		<h1>艺诺资讯</h1>
		<h2>品质行货，特惠商品抱回家!</h2>
	</div>
	<div class="index-news">
		<div class="master fl">
			<h1>名师新作</h1>
			<ul>
				<li>
					<a href="<?php echo C('famous1_link');?>" title="">
						<h2><?php echo C('famous1_title');?></h2>
						<img src="<?php echo UPLOAD_SITE_URL.DS.'shop/pay_guess/index'.DS.C('famous1_img');?>" alt="">
						<p>藏品作者：<?php echo C('famous1_author');?></p>
						<p>材料工艺：<?php echo C('famous1_name');?></p>
						<p>尺寸规格：<?php echo C('famous1_spec');?></p>
						<p>题材：<?php echo C('famous1_theme');?></p>
						<p>风格：<?php echo C('famous1_manner');?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo C('famous2_link');?>" title="">
						<h2><?php echo C('famous2_title');?></h2>
						<img src="<?php echo UPLOAD_SITE_URL.DS.'shop/pay_guess/index'.DS.C('famous2_img');?>" alt="">
						<p>藏品作者：<?php echo C('famous2_author');?></p>
						<p>材料工艺：<?php echo C('famous2_name');?></p>
						<p>尺寸规格：<?php echo C('famous2_spec');?></p>
						<p>题材：<?php echo C('famous2_theme');?></p>
						<p>风格：<?php echo C('famous2_manner');?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo C('famous3_link');?>" title="">
						<h2><?php echo C('famous3_title');?></h2>
						<img src="<?php echo UPLOAD_SITE_URL.DS.'shop/pay_guess/index'.DS.C('famous3_img');?>" alt="">
						<p>藏品作者：<?php echo C('famous3_author');?></p>
						<p>材料工艺：<?php echo C('famous3_name');?></p>
						<p>尺寸规格：<?php echo C('famous3_spec');?></p>
						<p>题材：<?php echo C('famous3_theme');?></p>
						<p>风格：<?php echo C('famous3_manner');?></p>
					</a>
				</li>
			</ul>
		</div>
		<div class="market fl">
			<h1>市场信息</h1>
			<div>
				<div class="home-sale-layout">
					<div class="left-layout">
						<ul class="tabs-nav">
						<?php $i=0;foreach ($output['info_class_list'] as $key => $value):?>
							<li class="<?php if($i==0){ echo 'tabs-selected';}?>"><i class="arrow"></i>
							<h3><?php echo $value['class_name'];?></h3>
							<?php $i++;?>
						<?php endforeach;?>
						</ul>
						<?php $j=0;foreach ($output['info_list'] as $key => $value):?>
						<div class="tabs-panel sale-goods-list <?php if($j!=0){echo ' tabs-hide';}?>">
							<ul>
							<?php $i=0;foreach($value as $k => $v):?>
								<?php if ($i == 0): ?>
								<li>
									<a href="<?php echo urlCMS('article','article_detail',array('article_id' => $v['article_id']))?>" title="">
										<h3><?php echo $v['article_title'];?></h3>
										<p><?php echo $v['article_abstract'];?></p>
									</a>
								</li>
							<?php else: ?>
								<li><a href="<?php echo urlCMS('article','article_detail',array('article_id' => $v['article_id']))?>" title="">&#8226;&nbsp;<?php echo $v['article_title'];?></a></li>
							<?php endif; ?>
							<?php $i++;?>
							<?php endforeach;?>
							</ul>
						</div>
						<?php $j++;?>
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!--首页楼层结束-->
<!--StandardLayout End-->
<!--<div class="wrapper">
  <div class="mt10"><?php echo loadadv(9,'html');?></div>
</div>-->
<!--首页拍卖推荐Tab-->
<script type="text/javascript">
$(document).ready(function(){
	var $tab_li = $('#new-tab ul li');
	$tab_li.click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
		var index = $tab_li.index(this);
		$('div.tab_box > div').eq(index).show().siblings().hide();
	});
});

</script>
<!--首页拍卖推荐Tab 结束-->