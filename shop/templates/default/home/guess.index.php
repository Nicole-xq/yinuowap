
<?php defined('InShopNC') or exit('Access Invalid!');?>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/index.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
<style type="text/css">
    .home-focus-layout .right-sidebar{filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#B2000000', endColorstr='#B2000000');background:rgba(0,0,0,0.7);width: 120px; border:none; margin-left: 480px;}
    .home-focus-layout .right-sidebar .top-account img,.home-focus-layout .right-sidebar .activity img{margin: 10px auto; width:68px; height:68px; overflow: hidden; border-radius: 100px;}
    .home-focus-layout .right-sidebar .top-account p{text-align:center; font-size: 14px; color: #fff}
    .home-focus-layout .right-sidebar .top-account a,.home-focus-layout .right-sidebar .activity a{color:#fff; text-align: center;display:block}
    .home-focus-layout .right-sidebar .login{padding:0 14px; background: #800000;width:54px;border-radius: 100px;margin: 10px auto !important}
    .home-focus-layout .right-sidebar .activity h1{filter:progid:DXImageTransform.Microsoft.gradient(enabled='true',startColorstr='#B2434343', endColorstr='#B2434343');background:rgba(67,67,67,0.7);text-align: center; color: #fff;padding: 4px 0;margin: 8px 0 6px;}
    .home-focus-layout .right-sidebar .activity a{margin:2px 0}
    .full-screen-slides li a {
        display: block;
        width: 100% !important;
        height: 480px !important;
        text-indent:0px !important;
        margin-left: 0;
        position: inherit;
        z-index: 2;
        left: 0;
    }
    .full-screen-slides li a img{
        width:100% !important;
        height:480px !important;
    }
	.public-nav-layout .all-category .category{height:479px;}
	.auction-index #new-tab .tab_box .new-tab-list .time .time-remain em{transform: translateY(2px);font-size:24px !important}
	.auction-index #new-tab .tab_box .new-tab-list h1{font-size:20px;color:#000}
	.auction-index #new-tab .tab_box .new-tab-list span{font-size:12px !important;color: #000 !important}
	.auction-index #new-tab .tab_box .new-tab-list span b{font-size:16px;}
	.auction-index #new-tab .tab_box .new-tab-list .time .sell{font-size:13px !important}
	.auction-index #new-tab .tab_box .new-tab-list .time{bottom:-20px !important}
</style>
<div class="home-focus-layout">
    <?php if (!empty($output['list'])) { ?>
        <div class="ncg-slides-banner">
            <ul id="fullScreenSlides" class="full-screen-slides">
                <?php foreach($output['list'] as $p) { ?>
                    <li><a href="<?php echo $p['url'];?>" target="_blank"><img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_AUCTION.'/'.$p['pic'];?>"></a></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    </div>
<div class="clear"></div>
<div class="nch-breadcrumb-layout">
    <div class="nch-breadcrumb wrapper"><i class="icon-home"></i>
    	<span><a href="<?php echo urlShop('index','index');?>">首页</a></span><span class="arrow">></span>
        <span><a href="<?php echo urlShop('show_guess','index');?>">趣猜</a></span><span class="arrow">
    </div>
  </div>
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/fudai.css" rel="stylesheet" type="text/css">
<div class="wrapper auction-index">
  <!--日期选择-->
  <!--首页新添加开始-->
  <div class="middle-box">
		<div id="new-tab" class="fl">
			<div class="tab_box">
				<div>
				<?php foreach($output['guess_list'] as $key => $val):?>
					<a href="<?php echo urlShop('show_guess', 'guess_detail', array('gs_id' => $val['gs_id']));?>" target="_blank" title="<?php echo $val['gs_name'];?>">
							<img src="<?php echo UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$val['gs_img'];?>"  alt="<?php echo $val['gs_name'];?>"/>
							<div class="new-tab-list fr">
								<div class="new-tab-list-title fl">
									<h1><?php echo $val['gs_name'];?></h1>
									<span class="all-block">竞猜区间:&nbsp;<b class="red-word">&yen;<?php echo $val['gs_min_price'].'-'.$val['gs_max_price'];?></b></span>
									<span class="all-block">中猜奖励:
										<b class="red-word">
										<?php if(!empty($val['points'])):?>
										<?php echo $val['points'];?>诺币
										<?php endif;?>
										<?php echo   round($val['gs_discount_price']/$val['gs_cost_price']*10,2);?>折购买
										</b>
									</span>
								</div>
								<div class="offer fr red-bg">
									<b><?php echo $val['joined_times'] ? $val['joined_times'] : 0;?></b>
									<b>次竞猜</b>
								</div>
								<div class="clear"></div>
								<dd class="time">
									<span class="sell">距&nbsp;结&nbsp;束&nbsp;</span>
									<span class="time-remain settime" endtime="<?php echo date("Y-m-d H:i:s",$val['end_time'])?>">
									<em time_id="d">0</em>日<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒
									</span>
								</dd>
							</div>
						</a>
				<?php endforeach;?>
				</div>
			</div>
		</div>
		<div class="hot-list fr">
			<div class="guess-hero fr">
				<h1><span>竞猜英雄榜</span></h1>
				<ul>
					<?php foreach($output['winers'] as $key => $val):?>
					<li>
						<img class="fl" src="<?php echo getMemberAvatarForID($val['member_id']);?>" width="62" height="62" alt=""/>
						<div class="fl">
							<h2 class="red-word"><?php echo $val['member_name']; ?></h2>
							<h2>上榜次数:<?php echo $val['times'];?>次</h2>
							<h2>最近中奖日期:<?php echo date('Y-m-d', $val['last_time']);?></h2>
						</div>
					</li>
					<?php endforeach;?>
				</ul>
			</div>
			<?php include template('home/hot_list');?>	
		</div>
		<div class="clear"></div>
	</div>
  <!--首页新添加结束-->
</div>
<script language="javascript">
$(function(){
updateEndTime();
});
//倒计时函数
function updateEndTime()
{
 var date = new Date();
 var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数
 
 $(".settime").each(function(i){
 
 var endDate =this.getAttribute("endTime"); //结束时间字符串
 //转换为时间日期类型
 var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

 var endTime = endDate1.getTime(); //结束时间毫秒数
 
 var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
  if(lag > 0)
  {
   var second = Math.floor(lag % 60);    
   var minite = Math.floor((lag / 60) % 60);
   var hour = Math.floor((lag / 3600) % 24);
   var day = Math.floor((lag / 3600) / 24);
   $(this).html("<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
  }
  else
   $(this).html("已经结束啦！");
 });
 setTimeout("updateEndTime()",1000);
}
</script>