<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/qll_profit.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SHOP_RESOURCE_SITE_URL; ?>/js/home_index.js" charset="utf-8"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/lm/js/lm_home.js"></script>
<div class="qll_profit_banner">
    <img src="<?php echo SHOP_TEMPLATES_URL;?>/images/qll/profit_banner.jpg" alt="" class="profit_img">
    <a href="/shop/activity/bond_profit_activity.html" target="_blank" class="profit_btn"><img src="<?php echo SHOP_TEMPLATES_URL;?>/images/qll/profit_btn.png" alt=""></a>
</div>
<div class="profit_content">
    <?php if(!empty($output['list'])){?>
        <ul class="qll_home_mod1 lm_clear">

        <?php foreach ($output['list'] as $key => $val) {
            if($key=='small'){?>
                <h1 class="profit_title">30天年化收益</h1>
            <?php } if($key=='middle'){?>
                <h1 class="profit_title">90天年化收益</h1>
            <?php } if($key=='big'){?>
                <h1 class="profit_title">180天年化收益</h1>
            <?php }
            $list = $output['list'][$key];
            if(!empty($list)){
                foreach ($list as $k => $v){?>
                            <li class="lm_clear ">
                                <div class="qll_home_mod1_left">
                                    <a target="_blank" href="<?php echo urlVendue('special_details','index',array('special_id'=>$v['special_id']))?>">
                                        <img src="<?=$v['wap_image_path']?>" alt="">
                                    </a>
                                    <span class="qll_finance_ad">
                                    <span>
                                        保证金年收益率<strong><?=$v['special_rate']?></strong>%
                                        计息期<?=$v['day_num']?>天
                                        <a class="qll_bzjsy" href="<?=C('cms_site_url')?>/index.php?act=special&op=special_detail&special_id=43" target="_blank">什么是保证金送收益?</a>
                                    </span>
                              </span>
                                </div>
                                <a class="qll_home_mod1_right" href="">
                                    <div class="qll_counttop">
                                        <h2 class="qll_title"><?=$v['special_name']?></h2>
                                        <span class="qll_address"><?=$v['store_name']?></span>
                                        <div class="qll_info lm_clear">
                                            <span class="qll_count">拍品数 <?=$v['auction_num']?>件</span>
                                            <span class="vote_count">
                                                <i><img src="<?php echo SHOP_TEMPLATES_URL;?>/lm/images/lm_eye.png" alt=""></i>
                                                <?=$v['special_click']?>人次
                                            </span>
                                        </div>
                                    </div>

                                    <div class="qll_countdown lm_countdown" data-date="<?=$v['special_remain_rate_time']?>">
                                        <span class="qll_timestate">距赠送收益截止</span>
                                        <div class="lm_date">
                                            <?//=date('Y-m-d H:i:s',$v['special_rate_time'])?>
                                            <?php// $val['special_rate_time'] ?>
                                            <div class="lm_three lm_date_info">09</div><span>天</span>
                                            <div class="lm_two   lm_date_info">11</div><span>时</span>
                                            <div class="lm_one   lm_date_info">42</div><span>分</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
        <?php }
            }
        }?>
        </ul>

    <?php }?>

</div>
<script type="text/javascript">
    function lmCuteDown() {
        // console.log($('.cutedown').length);
        $('.lm_countdown').each(function (i, e) {
            var date = parseInt($(this).attr('data-date')),
                three, two, one, sec,
                $data = $(this).find('.lm_date');
            function showTime(type) {
                if (date <= 0) {
                    clearInterval(displayTime);
                    return;
                }
                date -= 1;
                if (arguments.length === 2 && arguments[0].length === 3) {
                    if (arguments[1] === 1) {
                        three = Math.floor(date / 60 / 60 / 24);
                        two = Math.floor(date / 60 / 60 % 24);
                        one = Math.floor(date / 60 % 60);
                    } else {
                        three = Math.floor(date / 60 / 60 % 24);
                        two = Math.floor(date / 60 % 60);
                        one = Math.floor(date % 60);
                    }
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>' + arguments[0][0] + '</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>' + arguments[0][1] + '</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>' + arguments[0][2] + '</span>';
                } else {
                    three = Math.floor(date / 60 / 60 / 24);
                    two = Math.floor(date / 60 / 60 % 24);
                    one = Math.floor(date / 60 % 60);
                    sec = Math.floor(date % 60);
                    if (three < 10) {
                        three = '0' + three
                    }
                    if (two < 10) {
                        two = '0' + two
                    }
                    if (one < 10) {
                        one = '0' + one
                    }
                    if (sec < 10) {
                        sec = '0' + sec
                    }
                    var html = '<div class="lm_three lm_date_info">' + three + '</div><span>天</span>\
                <div class="lm_two   lm_date_info">' + two + '</div><span>时</span>\
                <div class="lm_one   lm_date_info">' + one + '</div><span>分</span>\
                <div class="lm_zeo   lm_date_info">' + sec + '</div><span>秒</span>';
                }
                $data.html(html);
                // console.log(three);
                // console.log(two);
                // console.log(one);
                if(three == 0 && two == 0 && one == 0){
                    // console.log(2222);
                    location.reload();
                }
            }

            if ($(this).attr('data-type') != 3) {
                if (date > 86400) {
                    //时间戳如果是s的时候不是s则多除1000
                    var displayTime;
                    if (date == -1) {
                        clearInterval(displayTime);
                        return;
                    }
                    displayTime = setInterval(function () {
                        showTime(['天', '时', '分'], 1);
                    }, 1000);
                } else if (date < 86400) {
                    var displayTime;
                    displayTime = setInterval(function () {
                        showTime(['时', '分', '秒'], 2);
                    }, 1000);
                }
            }else{
                var displayTime;
                displayTime = setInterval(function () {
                    showTime(['天', '时', '分', '秒'], 1);
                }, 1000);
            }
        });
    }

    lmCuteDown();
</script>