<?php defined('InShopNC') or exit('Access Invalid!');?>

<style>
    .yindao_wk {
        margin: 0 auto;
        height: 60px;
        background: #f6f6f6;
    }

    .yindao_wk .yindao_bg {
        background: url(<?php echo SHOP_TEMPLATES_URL;?>/images/joinin/yingdao.png);
        height: 60px;
        width: 1200px;
        margin: 0 auto
    }

    .yindao_wk .yindao_1 {
        background-position: 0 0px
    }

    .yindao_wk .yindao_2 {
        background-position: 0 -60px
    }

    .yindao_wk .yindao_3 {
        background-position: 0 -120px
    }

    .yindao_wk .yindao_4 {
        background-position: 0 -180px
    }

    .yindao_wk .yindao_5 {
        background-position: 0 -240px
    }

    .yindao_wk .yindao_6 {
        background-position: 0 -300px
    }

    .anniu_dange {
        overflow: hidden;
        height: 40px;
        line-height: 40px;
        margin: 20px auto;
        text-align: center
    }

    .anniu_dange a {
        background: #fac734;
        display: inline-block;
        padding: 0 15px;
        margin: 0 auto;
        font-size: 14px;
        color: #2a2a2a
    }

    .anniu_dange a:hover {
        background: #f3bd22;
        color: #000
    }

    .anniu_shuang {
        overflow: hidden;
        height: 40px;
        line-height: 40px;
        margin: 20px auto;
        text-align: center
    }

    .anniu_shuang a {
        background: #fac734;
        display: inline-block;
        padding: 0 15px;
        margin: 0 30px;
        font-size: 14px;
        color: #2a2a2a
    }

    .anniu_shuang a:hover {
        background: #f3bd22;
        color: #000
    }

    .anniu_shuang a.old {
        background: #d2d2d2
    }

    .anniu_shuang a.old:hover {
        background: #bababa
    }

    .ruzhudanxuan {
        text-align: center;
        line-height: 100px;
        color: #000;
        font-size: 14px;
        margin: 0 20px
    }

    .xieyi {
        width: 1200px;
        margin: 0 auto;
        height: 496px
    }

    .xieyi textarea {
        width: 1180px;
        height: 496px;
        font-size: 14px;
        line-height: 24px;
        color: #777777;
        padding: 10px
    }

    .mingcheng {
        margin: 0 auto;
        text-align: center;
        font-size: 16px;
        line-height: 100px;
        color: #777777
    }

    .ruzhudanxuan {
        text-align: center;
        line-height: 100px;
        color: #000;
        font-size: 14px;
        margin: 0 20px
    }
</style>

<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/lm-style.css" rel="stylesheet" type="text/css">

<!--状态区域-->
<div class="yindao_wk">
    <div class="yindao_bg yindao_<?php  echo $output['yindao_style']; ?>"></div>
</div>

 <?php require($output['join_type'].'.'.$output['sub_step'].'.php'); ?>
