<?php defined('InShopNC') or exit('Access Invalid!');?>

<!-- 公司资质 -->
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>

<!--签署入驻协议-->
<div class="mingcheng">财务账号</div>
<!-- 个人资料提交 -->
<div class="lm_upload">
    <div class="lm_container_ful">
        <form class="lm_upload_form" id="form_credentials_info" method="post" action="index.php?act=artist_joinin&op=step3">
            <fieldset>
                <div class="zm_form-group">
                    <label class="zm_label" for="bankname"><i class="lm_req">*</i>银行开户名：</label>

                    <div class="zm_inp">
                        <input id="bankname" name="bank_account_name" type="text" placeholder="填写本人真实姓名">
                        <span></span>
                    </div>
                </div>


                <div class="zm_form-group">
                    <label class="zm_label" for="bankcard"><i class="lm_req">*</i>银行账号：</label>

                    <div class="zm_inp">
                        <input id="bankcard" name="bank_account_number" type="text" placeholder="填写银行账号">
                        <span></span>
                    </div>
                </div>

                <div class="zm_form-group">
                    <label class="zm_label" for="lm_area"><i class="lm_req">*</i>开户银行支行名称：</label>

                    <div class="zm_inp">
                        <input id="bank_address" name="bank_address" type="hidden" />
                        <span></span>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="infoaddress"></label>

                    <div class="zm_inp">
                        <input id="infoaddress" name="bank_name" type="text" placeholder="填写开户银行支行名称">
                        <span></span>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="anniu_shuang">
    <a class="old" href="<?php echo urlShop('artist_joinin', 'step1');?>">上一步，资料填写</a>
    <a class="new" href="index.php?act=artist_joinin&op=step3" id='lm_bank_btn'>后期填写，跳过步骤</a>
    <a class="old btn" id="btn_apply_credentials_next" href="javascript:;">下一步，提交信息</a>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        var use_settlement_account = true;
        $("#bank_address").nc_region();
        $("#settlement_bank_address").nc_region();

        $("#is_settlement_account").on("click", function() {
            if($(this).prop("checked")) {
                use_settlement_account = false;
                $("#div_settlement").hide();
                $("#settlement_bank_account_name").val("");
                $("#settlement_bank_account_number").val("");
                $("#settlement_bank_name").val("");
                $("#settlement_bank_code").val("");
                $("#settlement_bank_address").val("");
            } else {
                use_settlement_account = true;
                $("#div_settlement").show();
            }
        });

        $('#form_credentials_info').validate({
            errorPlacement: function(error, element){
                element.nextAll('span').first().after(error);
            },
            rules : {
                bank_account_name: {
                    required: true,
                    maxlength: 50
                },
                bank_account_number: {
                    required: true,
                    maxlength: 20
                },
                bank_name: {
                    required: true,
                    maxlength: 50
                },
                bank_address: {
                    required: true
                }
            },
            messages : {
                bank_account_name: {
                    required: '请填写银行开户名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_account_number: {
                    required: '请填写银行账号',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_name: {
                    required: '请填写开户银行支行名称',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                bank_address: {
                    required: '请选择开户银行所在地'
                }
            }
        });

        $('#btn_apply_credentials_next').on('click', function() {
            if($('#form_credentials_info').valid()) {
                $('#form_credentials_info').submit();
            }
        });


    });
</script>