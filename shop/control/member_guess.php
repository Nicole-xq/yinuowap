<?php
/**
 * 会员中心——买家评价
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class member_guessControl extends BaseMemberControl{
    public function __construct(){
        parent::__construct() ;
        Tpl::output('pj_act','member_guessControl');
    }

    /**
     * 趣猜列表
     */
    public function listOp(){
        // 1.实例化数据模型
        $model_guess_offer = Model('guess_offer');
        // 2.加好限制条件
        $condition = array();
        $condition['member_id'] = $_SESSION['member_id'];
        switch ($_GET['limit_type']) {
            case 'unchoiced':
                 $condition['finished_time'] = array('lt',time());
                break;
            case 'choiced':
                $condition['gs_state'] = array('in',array(2,3));
                break;
            default:
                $condition['gs_state'] = 0;
                break;
        }
        // 3.获取数据和分页数据
        $guess_list = $model_guess_offer->get_guess_offer_list($condition, 10);
        $guess_list_data = array();
        if (!empty($guess_list)) {
                foreach ($guess_list as $key => $value) {
                    $guess_list_data[$key] = $value;  
                    $guess_list_data[$key]['show_img'] = $this->get_gs_img($value['gs_id']);
                }
        }
        // 4.加载模板
        Tpl::output('member_guess_list',$guess_list_data);
        Tpl::output('show_page',$model_guess_offer->showpage());
        Tpl::showpage('member_guess.list');
    }

     /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240);
    }   

}

