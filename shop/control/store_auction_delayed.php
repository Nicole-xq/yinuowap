<?php
/**
 * 延时周期
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/1/10 0010
 * Time: 16:21
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit ('Access Invalid!');

class store_auction_delayedControl extends BaseSellerControl
{
    protected $model_store;

    public function __construct()
    {
        parent::__construct ();
        $this->model_store = Model('store');
        if ($_SESSION['open_auction'] == 0) {
            showDialog('该店铺尚未开通发布拍品功能','index.php?act=store_auction&op=index','error');
        }
    }

    public function indexOp()
    {
        $store_info = $this->model_store->getStoreInfoByID($_SESSION['store_id']);
        Tpl::output('delayed_time', $store_info['delayed_time']);
        Tpl::showpage('store_auctions_delayed');
    }

    public function delayed_saveOp()
    {
        if ($_POST['form_submit'] == 'ok') {
            $update = array();
            $update['delayed_time'] = intval($_POST['delayed_time']);
            $this->model_store->editStore($update, array('store_id' => $_SESSION['store_id']));
            showDialog('延时时间设置成功','index.php?act=store_auction_delayed&op=index','succ');
        }
    }
}