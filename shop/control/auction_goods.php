<?php
/**
 * 拍卖池管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit ('Access Invalid!');
class auction_goodsControl extends BaseSellerControl {
    public function __construct() {
        parent::__construct ();
        Language::read ('member_store_goods_index');
    }

    /**
     * 拍卖池列表
     */
    public function indexOp()
    {
        $where = ['del_flag'=>0,'store_id'=>$_SESSION['store_id']];
        trim($_GET['keyword']) != '' ? $where['auction_name'] = ['like', '%' . trim($_GET['keyword']) . '%'] : '';
        (isset($_GET['auction_state']) && in_array($_GET['auction_state'],[0,1,2,3])) ? $where['auction_state'] = intval($_GET['auction_state']) : '';
        intval($_GET['special_id']) == 1 ? $where['cur_special_id'] = ['gt',0] : '';
        (intval($_GET['special_id']) == 0 && $_GET['special_id'] !='') ? $where['cur_special_id'] = 0 : '';

        //获取拍品池的数据
        $goods_auctions_model = Model('goods_auctions');
        $goods_auction_list = $goods_auctions_model->getGoodsAuctionList($where);
        $show_page = $goods_auctions_model->showpage();

        //拍品状态
        $auction_status = $goods_auctions_model->getAuctionStatus();

        $goods_id_arr = [];
        $special_id_arr = [];
        foreach ($goods_auction_list as $key=>$value){
            $goods_id_arr[] = $value['goods_id'];
            array_push($special_id_arr,$value['last_special_id'],$value['cur_special_id']);
            $goods_auction_list[$key]['auction_state_display'] = $auction_status[$value['auction_state']];
        }
        //商品信息
        $goods_model = Model("goods");
        $goods_where = ['goods_id'=>['in',implode(',',$goods_id_arr)]];
        $goods_field = 'goods_id,goods_price,goods_marketprice,goods_spec';
        $goods_list = $goods_model->getGoodsList($goods_where, $goods_field);
        $goods_list = array_under_reset($goods_list, 'goods_id', $type = 1);
        foreach ($goods_list as &$val){
            $goods_spec_arr = unserialize($val['goods_spec']);
            foreach ($goods_spec_arr as $spec_item){
                $val['spec_name'] .= $spec_item.' ';
            }
        }
        unset($val);

        //会场信息
        $special_model = Model('auction_special');
        $special_where = ['special_id'=>['in',array_unique($special_id_arr)]];
        $special_field = 'special_id,special_name,special_start_time,special_end_time';
        $special_list = $special_model->getSpecialList($special_where, $special_field, $order = 'special_id desc', $page = 20);
        $special_list = array_under_reset($special_list,'special_id');

        //拍品关联专场状态
        $link_special_status = $goods_auctions_model->getLinkSpecialStatus();

        //映射
        Tpl::output('show_page', $show_page);
        Tpl::output('auction_list', $goods_auction_list);
        Tpl::output('auction_status', $auction_status);
        Tpl::output('link_special_status', $link_special_status);
        Tpl::output('goods_list', $goods_list);
        Tpl::output('special_list', $special_list);

        Tpl::showpage('auction_goods_list');
    }


    /**
     * 删除拍品
     */
    public function drop_auction_goodsOp() {
        $id = intval($_GET['id']);
        $goods_id = intval($_GET['goods_id']);
        if (empty($id) || empty($goods_id)){
            showDialog("拍品删除失败", '', 'error');
        }
        $goods_auctions_model = Model('goods_auctions');
        $result = $goods_auctions_model->updateGoodsAuctions(['del_flag'=>1], ['id'=>$id]);
        if ($result) {
            // 撤销商品转拍品标识
            Model('goods')->editGoodsById(['is_add_auctions'=>0], [$goods_id]);
            showDialog("拍品删除成功", 'reload', 'succ');
        } else {
            showDialog("拍品删除失败", '', 'error');
        }
    }




}
