<?php
/**
 * 拍品专场
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/27 0027
 * Time: 11:39
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit ('Access Invalid!');

class store_auction_specialControl extends BaseSellerControl
{
    protected $special_state = array(
        '0'=>'未审核',
        '10'=>'审核中',
        '20'=>'审核成功',
        '30'=>'审核失败',
        '11'=>'预展开始',
        '12'=>'专场开始',
        '13'=>'专场结束'
    );
    public function __construct()
    {
        parent::__construct ();
        Language::read ('member_store_goods_index');
        if ($_SESSION['open_auction'] == 0) {
            showDialog('该店铺尚未开通发布拍品功能','index.php?act=store_auction&op=index','error');
        }
    }

    public function indexOp(){
        $special_id = $_GET['special_id'];
        $auction_special_list = array();
        if($special_id){
            $model_auction_special = Model('auction_special');
            $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
            Tpl::output('special_info', $special_info);
            $auction_special_list = $model_auction_special->getSpecialGoodsLists(array('special_id'=>$special_id));
        }
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];

        //权限组对应分类权限判断
        if (!$_SESSION['seller_gc_limits'] && $_SESSION['seller_group_id']) {
            $gc_list = Model('seller_group_bclass')->getSellerGroupBclasList(array('group_id'=>$_SESSION['seller_group_id']),'','','gc_id','gc_id');
            $where['gc_id'] = array('in',array_keys($gc_list));
        }


        $this->profile_menu('special_list', 'special_add');
        Tpl::output('special_id',$special_id);
        Tpl::output('selected_auction_list',$auction_special_list);
        Tpl::showpage('store_auction_special.add');
    }

    /**
     * 添加拍品
     */
    public function add_auctionsOp() {
        /**
         * 实例化模型
         */
        $model_goods_auctions = Model('goods_auctions');
        // where条件
        $where = array ();
        $where['store_id'] = $_SESSION['store_id'];

        if (trim($_GET['keyword']) != '') {
            $where['auction_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
        }
        $arr = array(0);
        if($_GET['special_id']){
            $arr[] = $_GET['special_id'];
        }
        $where['cur_special_id'] = array('in',$arr);
//        print_R($where);exit;
        $auctions_list = $model_goods_auctions->getGoodsAuctionList($where, '*',8,'','');
//        print_R($auctions_list);exit;
        Tpl::output('show_page', $model_goods_auctions->showpage(2));
        Tpl::output('auctions_list', $auctions_list);
        Tpl::showpage('store_special.auctions', 'null_layout');
    }

    public function save_specialOp(){
        $special_id = $_POST['special_id'];
        $data = array();
        $data['special_name'] = $_POST['special_name'];
        $data['store_id'] = $_SESSION['store_id'];
        $data['store_name'] = $_SESSION['store_name'];
        $model_store_vendue = Model('store_vendue');
        $store_vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $data['store_vendue_id'] = $store_vendue_info['store_vendue_id'];
        $data['special_add_time'] = TIMESTAMP;
        $data['special_start_time'] = strtotime($_POST['special_start_time']);
        $data['special_end_time'] = strtotime($_POST['special_end_time']);
//        $data['special_preview_start'] = strtotime($_POST['special_preview_start']);
        $upload = new UploadFile();
         //上传专场缩略图
        if (!empty($_FILES['special_image_file']['name'])){
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('special_image_file');
            if ($result){
                $_POST['special_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        if (!empty($_FILES['adv_image_file']['name'])){
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('adv_image_file');
            if ($result){
                $_POST['adv_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        if (!empty($_FILES['wap_image_file']['name'])){
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('wap_image_file');
            if ($result){
                $_POST['wap_image'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }

        $model_auction_special = Model('auction_special');
        $auction_special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
        if($_POST['special_id']){
            $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : $auction_special_info['special_image'];
            $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : $auction_special_info['adv_image'];
            $data['wap_image'] = !empty($_POST['wap_image']) ? $_POST['wap_image'] : $auction_special_info['wap_image'];
        }else{
            $data['special_image'] = !empty($_POST['special_image']) ? $_POST['special_image'] : '';
            $data['adv_image'] = !empty($_POST['adv_image']) ? $_POST['adv_image'] : '';
            $data['wap_image'] = !empty($_POST['wap_image']) ? $_POST['wap_image'] : '';
        }
        $data['special_sponsor'] = 1;
        $data['delivery_mechanism'] = $_POST['delivery_mechanism'];
        $data['special_state'] = 0;
        $data['auction_num'] = isset($_POST['goods'])?count($_POST['goods']):0;
//        print_R($data);exit;
        if($special_id){
            $return = $model_auction_special->editSpecial($data, array('special_id'=>$special_id));
        }else{
            $return = $model_auction_special->addSpecial($data, true);
            $special_id = $return;
        }
//        dd();
        if (!$return) {
            showDialog('申请失败', '', '', 'error');
        }

        // 插入套餐商品
        $model_goods_auctions = Model('goods_auctions');
        $data_goods = array();
//        $model_auction_special->delSpecialGoods(array('special_id' => intval($_POST['special_id'])));
        if (!empty($_POST['goods']) && is_array($_POST['goods'])) {
            foreach ($_POST['goods'] as $key => $val) {
                // 验证是否为本店铺商品
                $goods_info = $model_goods_auctions->getGoodsAuctionInfo(array('id'=>$val['gid']));
                $model_goods_auctions->updateGoodsAuctions(array('cur_special_id'=>$special_id),array('id'=>$val['gid']));
                if (empty($goods_info) || $goods_info['store_id'] != $_SESSION['store_id']) {
                    continue;
                }
                $data = array();
                $data['special_id'] = isset($_POST['special_id'])&&intval($_POST['special_id'])!=0 ? intval($_POST['special_id']) : $return;
                $data['goods_auction_id'] = $goods_info['id'];
                $data['auction_name'] = $goods_info['auction_name'];
                $data['auction_image'] = $goods_info['auction_image'];
                $data['auction_start_price'] = ncPriceFormat($goods_info['auction_start_price']);
//                $data['current_price'] = ncPriceFormat($goods_info['current_price']);
//                $data['bid_number'] = $goods_info['bid_number'];
                $data['store_id'] = $goods_info['store_id'];
                $data['store_name'] = $goods_info['store_name'];
                $data_goods[] = $data;
            }
        }
        //如果为更新专场数据,则清空原有专场拍品,重新绑定关系
        if(isset($_POST['special_id'])){
            $model_auction_special->delSpecialGoods(array('special_id'=>$special_id));
        }
        // 插入数据
        $return = $model_auction_special->addSpecialGoodsAll($data_goods);
        if(isset($_POST['special_id'])){
            $this->recordSellerLog('更新专场，名称：'.$data['special_name'] . ' id：'.$return);
        }else{
            $this->recordSellerLog('添加专场，名称：'.$data['special_name'] . ' id：'.$return);
        }
        showDialog('保存成功,请提交审核', urlShop('store_auction_special', 'special_list'), 'succ');
    }

    //等待审核专场
    public function special_checkOp(){
        $model_auction_special = Model('auction_special');
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['special_state'] = 10;
        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $condition['special_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $condition['special_id'] = intval($_GET['keyword']);
                    break;
            }
        }
        $count = $model_auction_special->getSpecialCount($condition);
        $special_list = $model_auction_special->getSpecialList($condition,'', '','',0,$count);
        Tpl::output('show_page', $model_auction_special->showpage());
        $this->profile_menu('special_list', 'special_verify');
        Tpl::output('special_list', $special_list);
        Tpl::showpage('store_auction_special.check');
    }

    //已审核专场
    public function special_check_okOp(){
        $model_auction_special = Model('auction_special');
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['special_state'] = array('neq',10);
        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $condition['special_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $condition['special_id'] = intval($_GET['keyword']);
                    break;
            }
        }
        $count = $model_auction_special->getSpecialCount($condition);
        $special_list = $model_auction_special->getSpecialList($condition,'', '','',0,$count);
        Tpl::output('show_page', $model_auction_special->showpage());
        $this->profile_menu('special_list', 'special_lockup');
        Tpl::output('special_list', $special_list);
        Tpl::showpage('store_auction_special.check_ok');
    }

    public function special_detailOp(){
        $special_id = $_GET['special_id'];
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
        Tpl::output('special_info', $special_info);
        $auction_special_list = $model_auction_special->getSpecialGoodsLists(array('special_id'=>$special_id));
        if($special_info['special_end_time'] < time() ){
            foreach($auction_special_list as &$v){
                $v['order_info'] = array(
                    'state'=>'流拍'
                );
                $order_info = Model('order')->getOrderInfo(array('auction_id'=>$v['auction_id']));
                if($order_info){
                    $v['order_info']['state'] = '成交';
                    if($order_info['payment_time']){
                        $v['order_info']['order_id'] = $order_info['order_id'];
                        $v['order_info']['is_pay'] = 1;
                    }else{
                        $v['order_info']['is_pay'] = 0;
                    }
                }
            }
        }
//        print_R($auction_special_list);exit;
        Tpl::output('auction_special_list', $auction_special_list);
        Tpl::showpage('store_auction_special.detail');
    }

    //删除专场
    public function delete_specialOp(){
        /**
         * 参数验证
         */
        $blids = trim($_GET['special_id']);
        if (empty($blids)) {
            showDialog(L('para_error'), '', 'error');
        }

        $return = Model('auction_special')->delSpecial($blids, $_SESSION['store_id']);
        if ($return) {
            $model_goods_auctions = Model('goods_auctions');
            $model_goods_auctions->updateGoodsAuctions(array('cur_special_id'=>0),array('cur_special_id'=>$blids));
            $this->recordSellerLog('删除专场，id：'.$blids);
            showDialog('删除专场成功', 'reload', 'succ');
        } else {
            showDialog('删除专场失败', '', 'error');
        }

    }


    //平台专场
    public function pt_specialOp(){
        $model_auction_special = Model('auction_special');
        $condition['is_open'] = 1;
        $condition['special_sponsor'] = 0;
        $condition['special_preview_start'] = array('gt',TIMESTAMP);
        $count = $model_auction_special->getSpecialCount($condition);
        $special_list = $model_auction_special->getSpecialList($condition,'', '','',0,$count);
        Tpl::output('show_page', $model_auction_special->showpage());
        Tpl::output('special_list', $special_list);
        $this->profile_menu('special_list', 'platform special');
        Tpl::showpage('platform_special.list');
    }

    /**
     * 报名专场
     */
    public function special_applyOp(){
        //根据专场编号查询专场信息
        $special_id = intval($_GET['special_id']);
        if($special_id <= 0){
            showMessage(Language::get('para_error'),'index.php?act=store_auction_special&op=pt_special','html','error');
        }
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
        //专场没有关闭并且在预展之前
        if(empty($special_info) || $special_info['is_open'] != '1' || $special_info['special_preview_start']<time() ){
            showMessage('该专场并不存在','index.php?act=store_auction_special&op=pt_special','html','error');
        }
        Tpl::output('special_info',$special_info);

        //查询活动细节信息
        $list   = $model_auction_special->getSpecialGoodsList(array('special_id'=>$special_id,'store_id'=>$_SESSION['store_id'],'auction_sp_state'=>array('in',array(0,1,3))));
        //构造通过与审核中商品的编号数组,以便在下方待选列表中,不显示这些内容
        $item_ids   = array();
        if(is_array($list) and !empty($list)){
            foreach($list as $k=>$v){
                $item_ids[] = $v['auction_id'];
            }
        }
        Tpl::output('list',$list);

        //根据查询条件查询商品列表
        $condition  = array();
        if(trim($_GET['name'])!=''){
            $condition['auction_name'] = array('like' ,'%'.trim($_GET['name']).'%');
        }
        $condition['store_id']      = $_SESSION['store_id'];
        if (!empty($item_ids)){
            $condition['auction_id']  = array('not in', $item_ids);
        }
        $model_auctions    = Model('auctions');
        $condition['state'] = array('in',array(0,1));
        $goods_list = $model_auctions->getAuctionList($condition,'*', 10);
        Tpl::output('goods_list',$goods_list);
        Tpl::output('show_page',$model_auctions->showpage());
        Tpl::output('search',$_GET);
        /**
         * 页面输出
         */
        $this->profile_menu('special_list', 'platform special');
        Tpl::showpage('platform_special.apply');
    }

    /**
     * 专场申请保存
     */
    public function special_apply_saveOp(){
        //判断页面参数
        if(empty($_POST['item_id'])){
            showDialog('请手动选择内容后再保存','index.php?act=store_auction_special&op=pt_special');
        }
        $special_id = intval($_POST['special_id']);
        if($special_id <= 0){
            showDialog(Language::get('para_error'),'index.php?act=store_auction_special&op=pt_special');
        }
        $model_auction_special = Model('auction_special');
        $special_info = $model_auction_special->getSpecialInfo(array('special_id'=>$special_id));
        //专场没有关闭并且在预展之前
        if(empty($special_info) || $special_info['is_open'] != '1' || $special_info['special_preview_start']<time() ){
            showMessage('该专场并不存在','index.php?act=store_auction_special&op=pt_special','html','error');
        }
        $list   = $model_auction_special->getSpecialGoodsList(array('store_id'=>$_SESSION['store_id'],'special_id'=>$special_id));
        $ids    = array();//已经存在的活动内容编号
        $ids_state2 = array();//已经存在的被拒绝的活动编号
        if(is_array($list) and !empty($list)){
            foreach ($list as $ad){
                $ids[]  = $ad['auction_id'];
                if($ad['auction_sp_state']=='2'){
                    $ids_state2[]   = $ad['auction_id'];
                }
            }
        }
        //根据查询条件查询商品列表
        foreach ($_POST['item_id'] as $item_id){
            $item_id = intval($item_id);
            if(!in_array($item_id,$ids)){
                $input  = array();
                $input['special_id']   = $special_id;
                $auctions  = Model('auctions');
                $item   = $auctions->getAuctionsInfoByID($item_id, 'auction_name,store_id,store_name,bid_number,current_price,auction_start_price,auction_image');
                if(empty($item) || $item['store_id'] != $_SESSION['store_id']){
                    continue;
                }
                $input['auction_name'] = $item['auction_name'];
                $input['auction_id']   = $item_id;
                $input['auction_image'] = $item['auction_image'];
                $input['auction_start_price'] = $item['auction_start_price'];
                $input['store_id']  = $item['store_id'];
                $input['store_name']= $item['store_name'];
                $input['auction_sp_state']= 0;
                $model_auction_special->addSpecialGoods($input);
            }elseif(in_array($item_id,$ids_state2)){
                $input  = array();
                $input['auction_sp_state']= '0';//将重新审核状态去除
                $model_auction_special->editSpecialGoods($input,array('auction_id'=>$item_id,'special_id'=>$special_id,'store_id'=>$_SESSION['store_id']));
            }
        }
        showDialog('参与申请已提交','reload','succ');
    }










    /**
     * 子页面上方小导航
     *
     * @param string $menu_type 导航类型
     * @param string $menu_key 当前导航的menu_key
     * @param boolean $allow_promotion
     * @return
     */
    private function profile_menu($menu_type,$menu_key, $allow_promotion = array()) {
        $menu_array = array();
        switch ($menu_type) {
            case 'special_list':
                $menu_array = array(
                    array('menu_key' => 'special_add',    'menu_name' => "申请专场",   'menu_url' => urlShop('store_auction_special', 'index')),
                    array('menu_key' => 'special_verify', 'menu_name' => "等待审核专场",     'menu_url' => urlShop('store_auction_special', 'special_check')),
                    array('menu_key' => 'special_lockup',     'menu_name' => "已审核专场",    'menu_url' => urlShop('store_auction_special', 'special_check_ok')),
                    array('menu_key' => 'platform special',     'menu_name' => "平台专场",    'menu_url' => urlShop('store_auction_special', 'pt_special')),

                );
                break;

        }
        Tpl::output ( 'member_menu', $menu_array );
        Tpl::output ( 'menu_key', $menu_key );
    }

    /**
     * 专场列表
     */
    public function special_listOp(){
        $model_auction_special = Model('auction_special');
        $condition['store_id'] = $_SESSION['store_id'];
//        $condition['special_state'] = 10;
        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $condition['special_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $condition['special_id'] = intval($_GET['keyword']);
                    break;
            }
        }
        $count = $model_auction_special->getSpecialCount($condition);
        $special_list = $model_auction_special->getSpecialList($condition,'', 'special_id desc',10,5,$count);
        foreach($special_list as $k=>&$v){
            $v['auction_state'] = '';
            $v['sell_num'] = 0;//TODO
            $v['start_time'] = date('Y-m-d H:i:s',$v['special_start_time']);
            $v['end_time'] = date('Y-m-d H:i:s',$v['special_end_time']);
            switch($v['special_state']){
                case 0://未审核
                    $v['check_state'] = $this->special_state[$v['special_state']];
                    break;
                case 10://审核中
                    $v['check_state'] = $this->special_state[$v['special_state']];
                    break;
                case 20://审核成功
                    $v['preview_time'] = date('Y-m-d H:i:s',$v['special_preview_start']);
                    $v['check_state'] = $this->special_state[$v['special_state']];
                    if(time()<$v['special_preview_start']){
                        $v['auction_state'] = '等待预展';
                    }elseif(time() < $v['special_start_time']){
                        $v['auction_state'] = '预展中';
                    }elseif(time() < $v['special_end_time']){
                        $v['auction_state'] = '拍卖中';
                    }else{
                        $v['auction_state'] = '拍卖结束';
                    }
                    break;
                case 30://审核失败
                    $v['check_state'] = $this->special_state[$v['special_state']];
                    break;
                default:
                    $v['preview_time'] = date('Y-m-d H:i:s',$v['special_preview_start']);
                    $v['check_state'] = '审核成功';
                    if(time()<$v['special_preview_start']){
                        $v['auction_state'] = '等待预展';
                    }elseif(time() < $v['special_start_time']){
                        $v['auction_state'] = '预展中';
                    }elseif(time() < $v['special_end_time']){
                        $v['auction_state'] = '拍卖中';
                    }else{
                        $v['auction_state'] = '拍卖结束';
                    }
                    break;
            }
        }
        Tpl::output('show_page', $model_auction_special->showpage());
        $this->profile_menu('special_list', 'special_verify');
        Tpl::output('special_list', $special_list);
        Tpl::showpage('store_auction_special.list');
    }

    /**
     * 提交审核
     */
    public function submit_auditOp(){
        $special_id = trim($_GET['special_id']);
        $special_model = Model('auction_special');
        $param = array('special_state'=>$special_model::STATE1);
        $condition = array(
            'special_id'=>$special_id,
            'store_id'=>$_SESSION['store_id'],
            'special_state'=>array('in',array($special_model::STATE0,$special_model::STATE3))
        );
//        print_R($param);
//        print_R($condition);
        $re = $special_model->editSpecial($param,$condition);
//        dd();
//        print_R($re);exit;
        if($re){
            showDialog('您的专场已经提交审核，请耐心等待。','reload','succ');
        }else{
            showDialog('申请审核失败','reload','error');
        }
    }

}