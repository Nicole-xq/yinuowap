<?php
/**
 * 拍卖订单
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/21 0021
 * Time: 16:56
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit ('Access Invalid!');

class store_auction_orderControl extends BaseSellerControl
{
    public function __construct()
    {
        parent::__construct ();
        Language::read ('member_store_goods_index');
        Language::read('member_store_index');
        Language::read('member_store_index,deliver');
        Language::read('member_member_index');
        if ($_SESSION['open_auction'] == 0) {
            showDialog('该店铺尚未开通发布拍品功能','index.php?act=store_auction&op=index','error');
        }
    }

    public function indexOp()
    {
        $auction_order_sn = intval($_GET['auction_order_sn']);
        if (!$_GET['state_type']) {
            $_GET['state_type'] = 'store_order';
        }
        $logic_auction_order = Logic('auction_order');
        // 获取订单列表
        $order_list = $logic_auction_order->getStoreAuctionOrderList($_SESSION['store_id'], $auction_order_sn, $_GET['buyer_name'], $_GET['state_type'], '*');
        Tpl::output('order_list',$order_list['data']['order_list']);
        Tpl::output('show_page',$order_list['data']['showPage']);
        self::profile_menu('list',$_GET['state_type']);

        Tpl::showpage('auction_order.index');
    }

    /*
         * 发货页面
         * */
    public function sendOp()
    {
        $order_id = intval($_GET['order_id']);
        $is_submit = false;
        if (empty($order_id)) {
            $order_id = $_POST['order_id'];
            $is_submit = true;
        }
        if ($order_id <= 0){
            showMessage(Language::get('wrong_argument'),'','html','error');
        }

        // 拍卖尾款订单详情
        $model_auction_order = Model('auction_order');
        $condition = array();
        $condition['auction_order_id'] = $order_id;
        $condition['store_id'] = $_SESSION['store_id'];
        $order_info = $model_auction_order->getInfo($condition);

        if (empty($order_info)) {
            showMessage('订单不存在','','html','error');
        }

        $if_allow_send = intval(!in_array($order_info['order_state'],array(ORDER_STATE_PAY,ORDER_STATE_SEND)));

        if ($if_allow_send) {
            showMessage(Language::get('wrong_argument'),'','html','error');
        }

        // 如果表单提交存储信息
        if ($is_submit){
            $logic_auction_order = Logic('auction_order');
            $result = $logic_auction_order->changeOrderSend($order_info, 'seller', $_SESSION['seller_name'], $_POST);
            if (!$result['state']) {
                showMessage($result['msg'],'','html','error');
            } else {
                showDialog($result['msg'],'reload','succ');
            }
        }

        Tpl::output('order_info',$order_info);
        $express_list  = rkcache('express',true);
        Tpl::output('express_list',$express_list);
        Tpl::showpage('auction_deliver.send', 'null_layout');
    }

    /**
     * 同意退款
     * */
    public function cancelOp()
    {
        $order_id = intval($_GET['order_id']);
        $model_auction_order = Model('auction_order');
        $order_info = $model_auction_order->getInfo(array('auction_order_id' => $order_id));
        if (chksubmit()) {
            $update = array(
                'close_reason' => '商家同意退款',
                'refund_state' => 3,
                'seller_time' => time(),
            );
            $model_auction_order->setOrder(array('auction_order_id' => $order_id), $update);

            showDialog('退款通过，等待平台退款', 'reload', 'succ');
        }
        Tpl::output('order_info', $order_info);
        Tpl::showpage('auction_deliver.cancel','null_layout');
    }

    /**
     * 卖家订单详情
     *
     */
    public function show_orderOp()
    {
        $auction_order_id = intval($_GET['auction_order_id']);
        $margin_id = intval($_GET['margin_id']);

        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            showDialog($result['msg'], '', 'error');
        }

        // 查询订单佣金比例

        Tpl::output('order_info',$result['data']['order_info']);
        Tpl::showpage('store_auction_order.show');
    }

    /**
     * 物流跟踪
     */
    public function search_deliverOp(){
        Language::read('member_member_index');
        $lang   = Language::getLangContent();

        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['auction_order_id']);
        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            showDialog($result['msg'], '', 'error', '', 5);
        }
        Tpl::output('order_info',$result['data']['order_info']);

        Tpl::output('store_info',$result['data']['order_info']['extend_store']);

        Tpl::output('e_code',$result['data']['order_info']['express_info']['e_code']);
        Tpl::output('e_name',$result['data']['order_info']['express_info']['e_name']);
        Tpl::output('e_url',$result['data']['order_info']['express_info']['e_url']);
        Tpl::output('shipping_code',$result['data']['order_info']['invoice_no']);

        self::profile_menu('search','search');
        Tpl::output('left_show','order_view');
        Tpl::showpage('store_auction_deliver.detail');
    }

    /**
     * 用户中心右边，小导航
     *
     * @param string    $menu_type  导航类型
     * @param string    $menu_key   当前导航的menu_key
     * @return
     */
    private function profile_menu($menu_type='',$menu_key='') {
        Language::read('member_layout');
        switch ($menu_type) {
            case 'list':
                $menu_array = array(
                    array(
                        'menu_key'=>'auction_order',
                        'menu_name'=>'所有订单',
                        'menu_url'=>'index.php?act=store_auction_order'),
                    array(
                        'menu_key'=>'state_new',
                        'menu_name'=>'待付款',
                        'menu_url'=>'index.php?act=store_auction_order&op=index&state_type=state_new'),
                    array(
                        'menu_key'=>'state_pay',
                        'menu_name'=>'待发货',
                        'menu_url'=>'index.php?act=store_auction_order&op=store_order&state_type=state_pay'),
                    array(
                        'menu_key'=>'state_send',
                        'menu_name'=>'已发货',
                        'menu_url'=>'index.php?act=store_auction_order&op=index&state_type=state_send'),
                    array(
                        'menu_key'=>'state_success',
                        'menu_name'=>'已完成',
                        'menu_url'=>'index.php?act=store_auction_order&op=index&state_type=state_success'),
                    array(
                        'menu_key'=>'state_cancel',
                        'menu_name'=>'已取消',
                        'menu_url'=>'index.php?act=store_auction_order&op=index&state_type=state_cancel'),
                );
                break;
        }
        Tpl::output('member_menu',$menu_array);
        Tpl::output('menu_key',$menu_key);
    }
}