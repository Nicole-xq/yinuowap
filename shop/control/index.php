<?php
/**
 * 默认展示页面
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');
class indexControl extends BaseHomeControl{
    public function index1Op(){
        Language::read('home_index_index');
        Tpl::output('index_sign','index');

        Tpl::output('index_home',true);

        //团购专区
        Language::read('member_groupbuy');
        $model_groupbuy = Model('groupbuy');
        $group_list = $model_groupbuy->getGroupbuyCommendedList(4);
        Tpl::output('group_list', $group_list);
        //限时折扣
        $model_xianshi_goods = Model('p_xianshi_goods');
        $xianshi_item = $model_xianshi_goods->getXianshiGoodsCommendList(4);
        Tpl::output('xianshi_item', $xianshi_item);
        // 拍卖推荐
        $model_auctions = Model('auctions');
        $auctions_item = $model_auctions->getHotAuctionGoodsList(20);
        Tpl::output('auctions_item', $auctions_item);

        //艺术家
        $model_artist = Model('artist_vendue');
        $artist_list = $model_artist->getArtist_vendueList(array('is_rec'=>1),'','artist_sort asc,artist_vendue_id desc','',5);
        Tpl::output('artist_list', $artist_list);
        //趣猜精品
        $model_guess = Model('p_act_guess');
        $rec_guess_list = $model_guess->getRecGuessList(4);
        Tpl::output('rec_guess_list', $rec_guess_list);
        //专场推荐
        $model_special = Model('auction_special');
        $start_special_list = $model_special->getSpecialList(array('special_start_time'=>array('elt',TIMESTAMP),'special_end_time'=>array('gt',TIMESTAMP),'is_rec'=>1,'is_open'=>1),'','special_end_time asc');
        foreach($start_special_list as $k=>$v){
            $remain_time = $v['special_end_time'] - $v['special_start_time'];

                $special_goods_list= $model_special->getSpecialGoodsList(array('special_id'=>$v['special_id'],'auction_sp_state'=>1));
                $all_bid_number = 0;
                foreach($special_goods_list as $key=>$val){
                    $model_auctions = Model('auctions');
                    $auction_info = $model_auctions->getAuctionsInfo(array('auction_id'=>$val['auction_id']));
                    $all_bid_number += $auction_info['bid_number'];
                }
                $start_special_list1[$remain_time]['special_id'] = $v['special_id'];
                $start_special_list1[$remain_time]['special_name'] = $v['special_name'];
                $start_special_list1[$remain_time]['special_state'] = $v['special_state'];
                $start_special_list1[$remain_time]['special_click'] = $v['special_click'];
                $start_special_list1[$remain_time]['special_image'] = $v['special_image'];
                $start_special_list1[$remain_time]['special_end_time'] = $v['special_end_time'];
                $start_special_list1[$remain_time]['special_start_time'] = $v['special_start_time'];
                $start_special_list1[$remain_time]['delivery_mechanism'] = $v['delivery_mechanism'];
                $start_special_list1[$remain_time]['all_bid_number'] = $all_bid_number;

        }
        if (!empty($start_special_list1)) {
            ksort($start_special_list1);
        }
        Tpl::output('start_special_list', $start_special_list1);
        $end_special_list = $model_special->getSpecialList(array('special_end_time'=>array('elt',TIMESTAMP),'is_rec'=>1,'is_open'=>1),'','special_end_time desc');
        foreach($end_special_list as $k=>$v){
            $special_goods_list= $model_special->getSpecialGoodsList(array('special_id'=>$v['special_id'],'auction_sp_state'=>1));
            $all_bid_number = 0;
            $jian_num = 0;
            foreach($special_goods_list as $key=>$val){
                $model_auctions = Model('auctions');
                $auction_info = $model_auctions->getAuctionsInfo(array('auction_id'=>$val['auction_id']));
                $all_bid_number += $auction_info['bid_number'];
                if($auction_info['is_liupai'] == 0){
                    $jian_num += 1;
                }
            }
            $end_special_list[$k]['all_bid_number'] = $all_bid_number;
            $end_special_list[$k]['jian_num'] = $jian_num;
        }
        Tpl::output('end_special_list', $end_special_list);
        $nostart_special_list = $model_special->getSpecialList(array('special_preview_start'=>array('elt',TIMESTAMP),'special_start_time'=>array('gt',TIMESTAMP),'is_rec'=>1,'is_open'=>1),'','special_start_time asc');
        Tpl::output('nostart_special_list', $nostart_special_list);
        //未来7天的总专场数
        $start_time1 = strtotime(date('Y-m-d',time()))+24*3600;
        $end_time1 = $start_time1+7*24*3600;
        $data['is_open'] = 1;
        $data['special_state'] = array('in',array(20,11,12,13));
        $data['special_start_time'] = array('between',array($start_time1,$end_time1));
        $special_count = $model_special->getSpecialCount($data);
        Tpl::output('special_count', $special_count);

        //资讯列表  
        $info_list = Model('cms_article')->getList1();
        $info_class_key = array_keys($info_list);
        Tpl::output('info_list',$info_list);
        $info_class = Model('cms_article_class')->getList1(array('class_id' => array('gt',0)));
        $info_class_list = array();
        foreach ($info_class as $key => $value) {
           if (in_array($value['class_id'], $info_class_key))$info_class_list[$key] = $value; 
        }
        // 专场列表    

        Tpl::output('info_class_list',$info_class_list);
        //板块信息
        $model_web_config = Model('web_config');
        $web_html = $model_web_config->getWebHtml('index');
        Tpl::output('web_html',$web_html);
        $web_index_sale = $model_web_config->getWebHtml('index_sale');
        Tpl::output('web_sale',$web_index_sale);
        Model('seo')->type('index')->show();
        Tpl::showpage('index');
    }

    /**测试图片上传
    public function testOp(){
        $autoload =  BASE_DATA_PATH.'/api/oss2/autoload.php';
        require_once($autoload);
        $ossClient = new OSS\OssClient(C('oss.access_id'), C('oss.access_key'), C('oss.api_url'), false);


        //$ossClient->uploadFile(C('oss.bucket'), '2_05473074980795509.jpg', BASE_PATH.'/templates/default/images/2014grate.png');

        //最后删除上传的$object
        $ossClient->deleteObject(C('oss.bucket'), 'example.jpg');
        die();
    }
     * */

    public function indexOp(){
        Language::read('home_index_index');
        Tpl::output('index_sign','index');

        Tpl::output('index_home',true);
        //团购专区
        Language::read('member_groupbuy');
        $model_groupbuy = Model('groupbuy');
        $group_list = $model_groupbuy->getGroupbuyCommendedList(4);
        Tpl::output('group_list', $group_list);
        //限时折扣
        $model_xianshi_goods = Model('p_xianshi_goods');
        $xianshi_item = $model_xianshi_goods->getXianshiGoodsCommendList(4);
        Tpl::output('xianshi_item', $xianshi_item);
        // 拍卖推荐
        $model_auctions = Model('auctions');
        $auctions_item = $model_auctions->getHotAuctionGoodsList(20);
        Tpl::output('auctions_item', $auctions_item);

        //艺术家
        $model_artist = Model('artist_vendue');
        $artist_list = $model_artist->getArtist_vendueList(array('is_rec'=>1),'','artist_sort asc,artist_vendue_id desc','',5);
        Tpl::output('artist_list', $artist_list);
        //趣猜精品
        $model_guess = Model('p_act_guess');
        $model_p_guess = Model('p_guess');
        $rec_guess_list = $model_guess->getRecGuessList(3);

        foreach($rec_guess_list as &$guess_value){
            $guess_value['number'] = $model_p_guess->get_guess_number_by_id($guess_value['id']);
        }

        Tpl::output('rec_guess_list', $rec_guess_list);


        //专场推荐
        $model_special = Model('auction_special');
        //$start_special_list = $model_special->getSpecialList(array('special_start_time'=>array('gt',TIMESTAMP),'is_rec'=>1,'is_open'=>1),'','special_end_time asc');
        $start_special_list = $model_special->getSpecialList(array('special_end_time'=>array('gt',TIMESTAMP),'is_rec'=>1,'is_open'=>1),'','special_end_time asc', 6);

        $sort_array = array();
        foreach($start_special_list as $k=>$v){

            if($v['special_start_time'] < TIMESTAMP){//已经开始
                $remain_time = 100000000 + $v['special_end_time']-TIMESTAMP;
                $start_special_list1[$v['special_id']]['end_timestamp'] = $v['special_end_time']-TIMESTAMP;
            }else{//尚未开始
                $remain_time = 1000000000 + $v['special_start_time'] - TIMESTAMP;
                $start_special_list1[$v['special_id']]['end_timestamp'] = $v['special_start_time'] - TIMESTAMP;
            }
            if($v['special_rate'] > 0 && $v['special_rate_time'] > TIMESTAMP){
                $remain_time = $v['special_rate_time']-TIMESTAMP;
            }
            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$v['special_id']],'auction_id,auction_click,bid_number');
            $special_click = 0;
            $all_bid_number = 0;
            foreach($special_auction_list as $item){
                $special_click += $item['auction_click'];
                $all_bid_number += $item['bid_number'];
            }
            //拍品数量
            $auction_count= $model_special->getSpecialGoodsCount(array('special_id' => $v['special_id'] , 'auction_sp_state' => 1));

            $sort_array[$v['special_id']] =  $remain_time;
            $start_special_list1[$v['special_id']]['auction_count'] = $auction_count;
            $start_special_list1[$v['special_id']]['special_id'] = $v['special_id'];
            $start_special_list1[$v['special_id']]['special_name'] = $v['special_name'];
            $start_special_list1[$v['special_id']]['special_state'] = $v['special_state'];
            $start_special_list1[$v['special_id']]['special_click'] = $special_click;
            $start_special_list1[$v['special_id']]['special_image'] = $v['special_image'];
            $start_special_list1[$v['special_id']]['special_end_time'] = $v['special_end_time'];
            $start_special_list1[$v['special_id']]['special_start_time'] = $v['special_start_time'];
            $start_special_list1[$v['special_id']]['delivery_mechanism'] = $v['delivery_mechanism'];
            $start_special_list1[$v['special_id']]['all_bid_number'] = $all_bid_number;
            $start_special_list1[$v['special_id']]['special_rate'] = $v['special_rate'];
        }

        if (!empty($start_special_list1)) {
            array_multisort($sort_array, $start_special_list1);
        }
        Tpl::output('start_special_list', $start_special_list1);

        $condition = [
            'special_rate_time'=>['gt',time()],
            'special_rate'=>['gt',0],
        ];
        $order = 'special_rate_time asc';
        $special_list = $model_special->getSpecialOpenList($condition,'',$order);
        $store_list = [];
        $i = 1;
        foreach($special_list as $key => &$special_info){
            $special_info['day_num'] = Logic('auction')->getInterestDay($special_info['special_rate_time'],$special_info['special_end_time']);//计息天数
            if (!in_array($special_info['day_num'],[30,31,90,91,180,181])){
                continue;
            }
            $special_info['wap_image_path'] = getVendueLogo($special_info['wap_image']);
            $special_info['special_remain_rate_time'] = $special_info['special_rate_time']-TIMESTAMP;
            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$special_info['special_id']],'auction_id,auction_click,bid_number');
            $special_click = 0;
            foreach($special_auction_list as $k=>$v){
                $special_click += $v['auction_click'];
            }
            $special_info['special_click'] = $special_click;
            $store_list[] = $special_info;
            if($i++>=3){
                break;
            }
        }
        Tpl::output('store_list', $store_list);

        //国画西画
        $gh_goods = $this->get_home_cat_goods(75, 4);
        Tpl::output('gh_goods', $gh_goods);

        //珠宝翡翠
        $gh_goods = $this->get_home_cat_goods(1, 4);
        Tpl::output('zb_goods', $gh_goods);

        //紫砂陶瓷
        $gh_goods = $this->get_home_cat_goods(121, 4);
        Tpl::output('zs_goods', $gh_goods);


        //资讯列表
        $info_list = Model('cms_article')->limit(9)->field('article_class_id,article_attachment_path,article_id,article_image,article_title')->where(array('article_state' => 3))->order('article_modify_time desc')->select();
        $info_cat_ids = array();
        foreach($info_list as &$info){
            $info['article_image'] = current(unserialize($info['article_image']));
            $info_cat_ids[$info['article_class_id']] = $info['article_class_id'];
        }

        $info_cat = Model('cms_article_class')->where(array('class_id' => array('in', $info_cat_ids)))->select();

        $format_info_cat = array();
        foreach($info_cat as $value){
            $format_info_cat[$value['class_id']] = $value;
        }

        Tpl::output('info_class_list',$format_info_cat);
        Tpl::output('info_list',$info_list);

        //关注
        $a['artist_vendue_id'] = array('in',array(1,2,3,4,5,6,7,8,9,10,12,15));
        $model_artist_vendue = Model('artist_vendue')->getArtist_StoreList($a,'','','artist_vendue_id,attention_num');
        $follow = array_column($model_artist_vendue,'attention_num','artist_vendue_id');
        Tpl::output('follow',$follow);

        //板块信息
        $model_web_config = Model('web_config');
        $web_html = $model_web_config->getWebHtml('index');
        Tpl::output('web_html',$web_html);
        $web_index_sale = $model_web_config->getWebHtml('index_sale');
        Tpl::output('web_sale',$web_index_sale);
        Model('seo')->type('index')->show();

        Tpl::showpage('index1');
    }

    public function get_home_cat_goods($cat_id, $goods_num){
        //国画西画
        $goods = Model('goods')->getGoodsOnlineList(array('gc_id_1' => $cat_id), '*', 0, 'goods_edittime desc', $goods_num);
        $this->store_ids = array();
        $store_model = Model('store');
        foreach($goods as &$value){
            if($value['is_add_auctions'] == 1){
                $tmp_re = Model('auctions')->getAuctionsInfo(array('goods_id'=>$value['goods_id'],'goods_common_id'=>$value['goods_commonid'],'auction_end_time'=>array('gt',time())));
                $value['auction_flag'] = empty($tmp_re)?0:1;
            }
            $store_info = $store_model->field('store_name,store_id,store_avatar')->where(array('store_id' => $value['store_id']))->find();
            $value['store_info'] = $store_info;
        }

        return $goods;
    }



    //json输出商品分类
    public function josn_classOp() {
        /**
         * 实例化商品分类模型
         */
        $model_class        = Model('goods_class');
        $goods_class        = $model_class->getGoodsClassListByParentId(intval($_GET['gc_id']));
        $array              = array();
        if(is_array($goods_class) and count($goods_class)>0) {
            foreach ($goods_class as $val) {
                $array[$val['gc_id']] = array(
                    'gc_id'=>$val['gc_id'],
                    'gc_name'=>htmlspecialchars($val['gc_name']),
                    'gc_parent_id'=>$val['gc_parent_id'],
                    'commis_rate'=>$val['commis_rate'],
                    'gc_sort'=>$val['gc_sort'],
                    'auction_commis_rate' => $val['auction_commis_rate'],
                );
            }
        }
        /**
         * 转码
         */
        if (strtoupper(CHARSET) == 'GBK'){
            $array = Language::getUTF8(array_values($array));//网站GBK使用编码时,转换为UTF-8,防止json输出汉字问题
        } else {
            $array = array_values($array);
        }
        echo $_GET['callback'].'('.json_encode($array).')';
    }

    /**
     * json输出地址数组 原data/resource/js/area_array.js
     */
    public function json_areaOp()
    {
        $_GET['src'] = $_GET['src'] != 'db' ? 'cache' : 'db';
        echo $_GET['callback'].'('.json_encode(Model('area')->getAreaArrayForJson($_GET['src'])).')';
    }

    /**
     * 根据ID返回所有父级地区名称
     */
    public function json_area_showOp()
    {
        $area_info['text'] = Model('area')->getTopAreaName(intval($_GET['area_id']));
        echo $_GET['callback'].'('.json_encode($area_info).')';
    }

    //判断是否登录
    public function loginOp(){
        echo ($_SESSION['is_login'] == '1')? '1':'0';
    }

    /**
     * 头部最近浏览的商品
     */
    public function viewed_infoOp(){
        $info = array();
        if ($_SESSION['is_login'] == '1') {
            $member_id = $_SESSION['member_id'];
            $info['m_id'] = $member_id;
            if (C('voucher_allow') == 1) {
                $time_to = time();//当前日期
                $info['voucher'] = Model()->table('voucher')->where(array('voucher_owner_id'=> $member_id,'voucher_state'=> 1,
                'voucher_start_date'=> array('elt',$time_to),'voucher_end_date'=> array('egt',$time_to)))->count();
            }
            $time_to = strtotime(date('Y-m-d'));//当前日期
            $time_from = date('Y-m-d',($time_to-60*60*24*7));//7天前
            $info['consult'] = Model()->table('consult')->where(array('member_id'=> $member_id,
            'consult_reply_time'=> array(array('gt',strtotime($time_from)),array('lt',$time_to+60*60*24),'and')))->count();
        }
        $goods_list = Model('goods_browse')->getViewedGoodsList($_SESSION['member_id'],5);
        if(is_array($goods_list) && !empty($goods_list)) {
            $viewed_goods = array();
            foreach ($goods_list as $key => $val) {
                $goods_id = $val['goods_id'];
                $val['url'] = urlShop('goods', 'index', array('goods_id' => $goods_id));
                $val['goods_image'] = thumb($val, 60);
                $viewed_goods[$goods_id] = $val;
            }
            $info['viewed_goods'] = $viewed_goods;
        }
        if (strtoupper(CHARSET) == 'GBK'){
            $info = Language::getUTF8($info);
        }
        echo json_encode($info);
    }
    /**
     * 查询每月的周数组
     */
    public function getweekofmonthOp(){
        import('function.datehelper');
        $year = $_GET['y'];
        $month = $_GET['m'];
        $week_arr = getMonthWeekArr($year, $month);
        echo json_encode($week_arr);
        die;
    }
}
