<?php
/**
 * 卖家资讯管理
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class store_informControl extends BaseSellerControl {
    public function __construct() {
        parent::__construct();
    }

    /**
     * 订单列表
     *
     */
    public function indexOp() {
        $model_store_vendue = Model('store_vendue');
        $store_vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id'],'store_apply_state'=>40));
        if(empty($store_vendue_info)){
            showMessage('请先开通拍卖功能','index.php?act=store_auction&op=index','','error');
        }else{
            $model_store_inform = Model('store_inform');
            $store_inform_info = $model_store_inform->getStore_informInfo(array('store_id'=>$_SESSION['store_id']));
            $store_inform_info['inform_list'] = unserialize($store_inform_info['inform_url']);

            Tpl::output('store_inform_info', $store_inform_info);
            Tpl::showpage('store_inform.add');
        }


    }


    /**
     * 商品列表
     */
    public function goods_listOp() {
        $model_goods = Model('goods');

        $condition = array();
        if($_GET['search_type'] == 'goods_url') {
            $condition['goods_id'] = intval($_GET['search_keyword']);
        }
        $goods_list = $model_goods->getGoodsOnlineList($condition, 'goods_id,goods_name,store_id,goods_image,goods_price', 10);
        Tpl::output('show_page', $model_goods->showpage(2));
        Tpl::output('goods_list', $goods_list);
        Tpl::showpage('api_goods_list', 'null_layout');
    }



    /**
     * 图片商品添加
     */
    public function inform_info_by_urlOp() {
        $url = urldecode($_GET['url']);
        if(empty($url)) {
            self::return_json('资讯不存在', 'false');
        }
        $model_inform_info = Model('inform_info_by_url');
        $result = $model_inform_info->get_inform_info_by_url($url);
        if($result) {
            self::echo_json($result);
        } else {
            self::return_json('资讯不存在', 'false');
        }
    }

    public function store_inform_saveOp(){
        $model_store_vendue = Model('store_vendue');
        $store_vendue_info = $model_store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        //插入文章
        $param = array();
        $param['store_id'] = $_SESSION['store_id'];
        $param['store_vendue_id'] = $store_vendue_info['store_vendue_id'];

        //文章商品
        if(!empty($_POST['article_goods_url'])) {
            $article_goods_list = array();
            for ($i = 0,$count = count($_POST['article_goods_url']); $i < $count; $i++) {
                $article_goods = array();
                $article_goods['url'] = $_POST['article_goods_url'][$i];
                $article_goods['title'] = $_POST['article_goods_title'][$i];
                $article_goods['image'] = $_POST['article_goods_image'][$i];
                $article_goods['abstract'] = $_POST['article_goods_abstract'][$i];
                $article_goods['time'] = $_POST['article_goods_time'][$i];
                $article_goods['id'] = $_POST['article_id'][$i];
                $article_goods_list[] = $article_goods;

            }

            if(!empty($article_goods_list)) {
                $param['inform_url'] = serialize($article_goods_list);
            } else {
                $param['inform_url'] = '';
            }
        }else{
            $param['inform_url'] = '';
        }

        $model_store_inform = Model('store_inform');

        if(!empty($_POST['inform_id'])) {
            $inform_id = intval($_POST['inform_id']);
            $inform_auth = $this->check_inform_auth($inform_id);
            if($inform_auth) {
                $model_store_inform->editStore_inform($param,array('inform_id'=>$inform_id));
            } else {
                showMessage('保存失败','','','error');
            }
        } else {
            $inform_id = $model_store_inform->addStore_inform($param);
        }



        if($inform_id) {
            showMessage('保存成功','index.php?act=store_inform&op=index');
        } else {
            showMessage('保存失败','index.php?act=store_inform&op=index','','error');
        }
    }

    private function check_inform_auth($inform_id) {
        $model_store_inform = Model('store_inform');
        if($inform_id > 0) {
            $inform_detail = $model_store_inform->getStore_informInfo(array('inform_id'=>$inform_id));
            if(!empty($inform_detail)) {
                return $inform_detail;
            }
        }
        return false;
    }








}
