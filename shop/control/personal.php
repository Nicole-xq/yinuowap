<?php
/**
 * 个人主页
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class personalControl extends BaseHomeControl{

    public function indexOp(){
        //刘传明
        Tpl::showpage('sign_activity');
    }
}
