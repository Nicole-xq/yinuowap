<?php
/**
 * 商品图片统一调用函数
 *
 *
 *
 * @package    function
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @author     ShopNC Team
 * @since      File available since Release v1.1
 */

defined('InShopNC') or exit('Access Invalid!');

/**
 * 取得商品缩略图的完整URL路径，接收商品信息数组，返回所需的商品缩略图的完整URL
 *
 * @param array $goods 商品信息数组
 * @param string $type 缩略图类型  值为60,240,360,1280
 * @return string
 */
function thumb($goods = array(), $type = ''){
    $type_array = explode(',_', ltrim(GOODS_IMAGES_EXT, '_'));
    if (!in_array($type, $type_array)) {
        $type = '240';
    }
    if (empty($goods)){
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
    }
    if (array_key_exists('apic_cover', $goods)) {
        $goods['goods_image'] = $goods['apic_cover'];
    }
    if (empty($goods['goods_image'])) {
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
    }
    $search_array = explode(',', GOODS_IMAGES_EXT);
    $file = str_ireplace($search_array,'',$goods['goods_image']);
    $fname = basename($file);
    //取店铺ID
    if (preg_match('/^(\d+_)/',$fname)){
        $store_id = substr($fname,0,strpos($fname,'_'));
    }else{
        $store_id = $goods['store_id'];
    }

    if (!C('oss.open')) {
        $file = $type == '' ? $file : str_ireplace('.', '_' . $type . '.', $file);
        if (!file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$store_id.'/'.$file)){
            return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
        }
        $thumb_host = UPLOAD_SITE_URL.'/'.ATTACH_GOODS;
        return $thumb_host.'/'.$store_id.'/'.$file;        
    } else {
        return oss::getImgUrl( '/' . ATTACH_GOODS . '/' . $store_id . '/' . $file,$type);
    }


}
/**
 * 取得商品缩略图的完整URL路径，接收图片名称与店铺ID
 *
 * @param string $file 图片名称
 * @param string $type 缩略图尺寸类型，值为60,240,360,1280
 * @param mixed $store_id 店铺ID 如果传入，则返回图片完整URL,如果为假，返回系统默认图
 * @return string
 */
function cthumb($file, $type = '', $store_id = false) {
    if (substr($file, 0, 4) == 'http') {
        return $file;
    }
    $type_array = explode(',_', ltrim(GOODS_IMAGES_EXT, '_'));
    if (!in_array($type, $type_array)) {
        $type = '240';
    }
    if (empty($file)) {
        return UPLOAD_SITE_URL . '/' . defaultGoodsImage ( $type );
    }
    $search_array = explode(',', GOODS_IMAGES_EXT);
    $file = str_ireplace($search_array,'',$file);
    $fname = basename($file);
    //取店铺ID
    if (preg_match('/^(\d+_)/',$fname)){
        $store_id = substr($fname,0,strpos($fname,'_'));
    }
    if (!C('oss.open')) {
        // 本地存储时，增加判断文件是否存在，用默认图代替
        if ( !file_exists(BASE_UPLOAD_PATH . '/' . ATTACH_GOODS . '/' . $store_id . '/' . ($type == '' ? $file : str_ireplace('.', '_' . $type . '.', $file)) )) {
            return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
        }
        $thumb_host = UPLOAD_SITE_URL . '/' . ATTACH_GOODS;
        return $thumb_host . '/' . $store_id . '/' . ($type == '' ? $file : str_ireplace('.', '_' . $type . '.', $file));        
    } else {
        return oss::getImgUrl( '/' . ATTACH_GOODS . '/' . $store_id . '/' . $file,$type);
    }

}

/**
 * 取得拍品缩略图的完整URL路径，接收拍品信息数组，返回所需的拍品缩略图的完整URL
 *
 * @param array $goods 拍品信息数组
 * @param string $type 缩略图类型  值为60,240,360,1280
 * @return string
 */
function athumb($goods = array(), $type = ''){
    $type_array = explode(',_', ltrim(GOODS_IMAGES_EXT, '_'));
    if (!in_array($type, $type_array)) {
        $type = '240';
    }
    if (empty($goods)){
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
    }
    if (array_key_exists('apic_cover', $goods)) {
        $goods['auction_image'] = $goods['apic_cover'];
    }
    if (empty($goods['auction_image'])) {
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
    }
    $search_array = explode(',', GOODS_IMAGES_EXT);
    $file = str_ireplace($search_array,'',$goods['auction_image']);
    $fname = basename($file);
    //取店铺ID
    if (preg_match('/^(\d+_)/',$fname)){
        $store_id = substr($fname,0,strpos($fname,'_'));
    }else{
        $store_id = $goods['store_id'];
    }

    if (!C('oss.open')) {
        $file = $type == '' ? $file : str_ireplace('.', '_' . $type . '.', $file);
        if (!file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$store_id.'/'.$file)){
            return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
        }
        $thumb_host = UPLOAD_SITE_URL.'/'.ATTACH_GOODS;
        return $thumb_host.'/'.$store_id.'/'.$file;
    } else {
        //return C('oss.img_url'). '/' . ATTACH_GOODS . '/' . $store_id . '/' . $file . '@product-' . $type;
        return oss::getImgUrl( '/' . ATTACH_GOODS . '/' . $store_id . '/' . $file,$type);
    }


}
/**
 * 商品视频连接
 */
function goodsVideoPath($video_file, $store_id = false){
    if(empty($store_id)){
        //取店铺ID
        if (preg_match('/^(\d+_)/',$video_file)){
            $store_id = substr($video_file,0,strpos($video_file,'_'));
        }
    }
    if(!C('oss.open')){
        if(file_exists(BASE_UPLOAD_PATH . '/' . ATTACH_GOODS . '/' . $store_id . '/' . 'goods_video' . '/' . $video_file )){
            return UPLOAD_SITE_URL . '/' . ATTACH_GOODS . '/' . $store_id . '/' . 'goods_video' . '/' . $video_file ;
        }
    }else{
        return oss::getImgUrl( '/' . ATTACH_GOODS . '/' . $store_id . '/' . $file,$type);
    }
    
}
/**
 * 商品二维码
 * @param array $goods_info
 * @return string
 */
function goodsQRCode($goods_info) {
    if (!file_exists(BASE_UPLOAD_PATH. '/' . ATTACH_STORE . '/' . $goods_info['store_id'] . '/' . $goods_info['goods_id'] . '.png' )) {
        return UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.'default_qrcode.png';
    }
    return UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.$goods_info['store_id'].DS.$goods_info['goods_id'].'.png';
}

/**
 * 拍品二维码
 * @param array $goods_info
 * @return string
 */
function auctionsQRCode($auction_info) {
    if (!file_exists(BASE_UPLOAD_PATH. '/' . ATTACH_STORE . '/' . $auction_info['store_id'] . '/' . $auction_info['auction_id'] . '.png' )) {
        return UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.'default_qrcode.png';
    }
    return UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.$auction_info['store_id'].DS.$auction_info['auction_id'].'.png';
}
/**
 * 取得团购缩略图的完整URL路径
 *
 * @param string $imgurl 商品名称
 * @param string $type 缩略图类型  值为small,mid,max
 * @return string
 */
function gthumb($image_name = '', $type = ''){
    if (!in_array($type, array('small','mid','max'))) $type = 'small';
    if (empty($image_name)){
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }
    list($base_name, $ext) = explode('.', $image_name);
    list($store_id) = explode('_', $base_name);
    $file_path = ATTACH_GROUPBUY.DS.$store_id.DS.$base_name.'_'.$type.'.'.$ext;
    if(!file_exists(BASE_UPLOAD_PATH.DS.$file_path)) {
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }
    return UPLOAD_SITE_URL.DS.$file_path;

}

/**
 * 取得买家缩略图的完整URL路径
 *
 * @param string $imgurl 商品名称
 * @param string $type 缩略图类型  值为240,1024
 * @return string
 */
function snsThumb($image_name = '', $type = ''){
    if (!in_array($type, array('240','1024'))) $type = '240';
    if (empty($image_name)){
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }

    if(strpos($image_name, '/')) {
        $image = explode('/', $image_name);
        $image = end($image);
    } else {
        $image = $image_name;
    }

    list($member_id) = explode('_', $image);
    $file_path = ATTACH_MALBUM.DS.$member_id.DS.str_ireplace('.', '_'.$type.'.', $image_name);
    if(!file_exists(BASE_UPLOAD_PATH.DS.$file_path)) {
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }
    return UPLOAD_SITE_URL.DS.$file_path;
}

/**
 * 取得积分商品缩略图的完整URL路径
 *
 * @param string $imgurl 商品名称
 * @param string $type 缩略图类型  值为small
 * @return string
 */
function pointprodThumb($image_name = '', $type = ''){
    if (!in_array($type, array('small','mid'))) $type = '';
    if (empty($image_name)){
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }

    if($type) {
        $file_path = ATTACH_POINTPROD.DS.str_ireplace('.', '_'.$type.'.', $image_name);
    } else {
        $file_path = ATTACH_POINTPROD.DS.$image_name;
    }
    if(!file_exists(BASE_UPLOAD_PATH.DS.$file_path)) {
        return UPLOAD_SITE_URL.'/'.defaultGoodsImage('240');
    }
    return UPLOAD_SITE_URL.DS.$file_path;
}

/**
 * 取得品牌图片
 *
 * @param string $image_name
 * @return string
 */
function brandImage($image_name = '') {
    if ($image_name != '') {
        return UPLOAD_SITE_URL.'/'.ATTACH_BRAND.'/'.$image_name;
    }
    return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.'/default_brand_image.gif';
}

/**
 * 取得推荐分类图片
 * @param $image_name
 * @return string
 */
function categoryImage($image_name){
    return UPLOAD_SITE_URL.'/'.ATTACH_SCATEGORY.'/'.$image_name;
}
/**
 * 取得合作商家图片
 *
 * @param string $image_name
 * @return string
 */
function co_storeImage($image_name = '') {
    if ($image_name != '') {
        return UPLOAD_SITE_URL.'/'.ATTACH_PAINTING.'/'.$image_name;
    }

}

/**
* 取得订单状态文字输出形式
*
* @param array $order_info 订单数组
* @return string $order_state 描述输出
*/
function orderState($order_info) {
    switch ($order_info['order_state']) {
        case ORDER_STATE_CANCEL:
            $order_state = '已取消';
        break;
        case ORDER_STATE_NEW:
            if ($order_info['chain_code']) {
                $order_state = '门店付款自提';
            } else {
                $order_state = '待付款';
            }
        break;
        case ORDER_STATE_PAY:
            if ($order_info['chain_code']) {
                $order_state = '待自提';
            } else {
                if (isset($order_info['extend_order_common']['transit_shipment_status']) && $order_info['extend_order_common']['transit_shipment_status']==1) {
                    //扩展表为中转仓待收货
                    $order_state = '待发货(中转仓待收货)';
                } else {
                    $order_state = '待发货';
                }
            }
        break;
        case ORDER_STATE_SEND:
            $order_state = '待收货';
        break;
        case ORDER_STATE_SUCCESS:
            $order_state = '交易完成';
        break;
        case ORDER_STATE_UPLOAD_EVIDENCE:
            $order_state = '待确认';
        break;
    }
    return $order_state;
}

/**
 * 取得订单支付类型文字输出形式
 *
 * @param array $payment_code
 * @return string
 */
function orderPaymentName($payment_code) {
    return str_replace(
            array('offline','online','AopClient','alipay_native','ali_native','alipay','tenpay','chinabank','predeposit','wxpay_jsapi','wxpay','wx_jsapi','wx_saoma','chain','underline','lklpay','tl_pay', 'wxpay_mini'),
            array('货到付款','在线付款','支付宝移动支付','支付宝移动支付','支付宝移动支付','支付宝','财付通','网银在线','站内余额支付','微信支付','微信支付[客户端]','微信支付','微信支付[扫码]','门店支付','线下支付','拉卡拉支付','小程序支付'),
            $payment_code);
}

/**
 * 取得订单商品销售类型文字输出形式
 *
 * @param array $goods_type
 * @return string 描述输出
 */
function orderGoodsType($goods_type) {
    return str_replace(array(
        '11','10','1','2','3','4','5',
        '8', '9'
    ), array(
        '趣猜','拼团','','团购','限时折扣','优惠套装','赠品',
        '', '换购'
    ), $goods_type);
}

/**
 * 取得结算文字输出形式
 *
 * @param array $bill_state
 * @return string 描述输出
 */
function billState($bill_state) {
    return str_replace(
            array('1','2','3','4'),
            array('已出账','商家已确认','平台已审核','结算完成'),
            $bill_state);
}
//新浪长链接变成短链接
define('SINA_APPKEY', '696183059');
function curlQuery($url) {
    //设置附加HTTP头
    $addHead = array(
        "Content-type: application/json"
    );
    //初始化curl，当然，你也可以用fsockopen代替
    $curl_obj = curl_init();
    //设置网址
    curl_setopt($curl_obj, CURLOPT_URL, $url);
    //附加Head内容
    curl_setopt($curl_obj, CURLOPT_HTTPHEADER, $addHead);
    //是否输出返回头信息
    curl_setopt($curl_obj, CURLOPT_HEADER, 0);
    //将curl_exec的结果返回
    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, 1);
    //设置超时时间
    curl_setopt($curl_obj, CURLOPT_TIMEOUT, 15);
    //执行
    $result = curl_exec($curl_obj);
    //关闭curl回话
    curl_close($curl_obj);
    return $result;
}
//简单处理下url，sina对于没有协议(http://)开头的和不规范的地址会返回错误
function filterUrl($url = '') {
    $url = trim(strtolower($url));
    $url = trim(preg_replace('/^http:\/\//', '', $url));
    if ($url == '')
        return false;
    else
        return urlencode('http://' . $url);
}
//根据长网址获取短网址
function sinaShortenUrl($long_url) {
    //拼接请求地址，此地址你可以在官方的文档中查看到
    $url = 'http://api.t.sina.com.cn/short_url/shorten.json?source=' . SINA_APPKEY . '&url_long=' . $long_url;
    //获取请求结果
    $result = curlQuery($url);
    //下面这行注释用于调试，
    //print_r($result);exit();
    //解析json
    $json = json_decode($result);
    //异常情况返回false
    if (isset($json->error) || !isset($json[0]->url_short) || $json[0]->url_short == '')
        return false;
    else
        return $json[0]->url_short;
}
//根据短网址获取长网址，此函数重用了不少sinaShortenUrl中的代码，以方便你阅读对比，你可以自行合并两个函数
function sinaExpandUrl($short_url) {
    //拼接请求地址，此地址你可以在官方的文档中查看到
    $url = 'http://api.t.sina.com.cn/short_url/expand.json?source=' . SINA_APPKEY . '&url_short=' . $short_url;
    //获取请求结果
    $result = curlQuery($url);
    //下面这行注释用于调试
    //print_r($result);exit();
    //解析json
    $json = json_decode($result);
    //异常情况返回false
    if (isset($json->error) || !isset($json[0]->url_long) || $json[0]->url_long == '')
        return false;
    else
        return $json[0]->url_long;
}
