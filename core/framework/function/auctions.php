<?php
/**
 * 拍品使用函数
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/6 0006
 * Time: 14:15
 * @author DukeAnn
 */

defined('InShopNC') or exit('Access Invalid!');

/**
 * 取得订单状态文字输出形式
 *
 * @param array $order_info 订单数组
 * @return string $order_state 描述输出
 */
function orderMarginState($order_info) {
    switch ($order_info['order_state']) {
        case 4:
            $order_state = '已取消';
            break;
        case 0:
            if ($order_info['order_type'] == 1) {
                $order_state = '请提交支付凭证';
            } else {
                $order_state = '待付款';
            }
            break;
        case 1:
            $order_state = '交易完成';
            break;
        case 2:
            $order_state = '已提交支付凭证';
            break;
        case 3:
            $order_state = '线上支付余下金额';
            break;

    }
    return $order_state;
}