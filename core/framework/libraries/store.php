<?php
/**
 * 手机短信类
 *
 *
 *
 * @package    library
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @author     ShopNC Team
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
class Store {


    var $second_number_setting = array(
        0 => 2,     //商家/机构
        1 => 1      //艺术家
    );


    /**生成商家入驻编号
     * @param $info         商家入驻信息
     * @return bool|string  生成是否成功|生成的编号
     */
    public function createSerialNumber($info){
        $first_number = 'Y';
        $second_number = $this->second_number_setting[$info['joinin_type']];
        $date_number = date('Ymd');

        /** @var store_card_recordModel $store_card_record_model */
        $store_card_record_model = Model('store_card_record');

        /** @var store_joininModel $model_store_joinin */
        $model_store_joinin = Model('store_joinin');

        $store_card_record_model->beginTransaction();
        $i = 0;
        $do_create = true;
        do{
            echo $i;

            $last_number = $store_card_record_model->getNumber($first_number, $info['joinin_type']);

            $number = $first_number.$second_number.$date_number.str_pad($last_number, 3, 0, STR_PAD_LEFT);

            if($model_store_joinin->where(array('member_id' => $info['member_id']))->update(array('record_number' => $number))){
                $do_create = false;
            }

            $i++;
        }while($do_create && $i < 5);

        if($do_create){
            return false;
        }

        $store_card_record_model->commit();
        return $number;
    }


    /**根据商家入驻资料进行店铺开通
     * @param $joinin_detail    商家入驻资料
     * @return bool             是否开通成功
     */
    public function openStore($joinin_detail){
        $model_seller = Model('seller');
        $model_store    = Model('store');




        //开店
        $shop_array = array();
        $shop_array['member_id'] = $joinin_detail['member_id'];
        $shop_array['member_name'] = $joinin_detail['member_name'];
        $shop_array['seller_name'] = $joinin_detail['seller_name'];
        $shop_array['grade_id'] = $joinin_detail['sg_id'];
        $shop_array['store_name'] = $joinin_detail['store_name'];
        $shop_array['sc_id'] = $joinin_detail['sc_id'];
        $shop_array['store_company_name'] = $joinin_detail['company_name'];
        $shop_array['province_id'] = $joinin_detail['company_province_id'];
        $shop_array['area_info'] = $joinin_detail['company_address'];
        $shop_array['store_address'] = $joinin_detail['company_address_detail'];
        $shop_array['store_zip'] = '';
        $shop_array['store_zy'] = '';
        $shop_array['store_state'] = 1;
        $shop_array['store_time'] = time();
        $shop_array['store_end_time'] = strtotime(date('Y-m-d 23:59:59', strtotime('+1 day')) . " +" . intval($joinin_detail['joinin_year']) . " year");
        $shop_array['store_type'] = $joinin_detail['joinin_type'];
        $shop_array['record_number'] = $joinin_detail['record_number'];

        $store_id = $model_store->addStore($shop_array);

        if($store_id){
            //写入商家账号
            $seller_array = array();
            $seller_array['seller_name'] = $joinin_detail['seller_name'];
            $seller_array['member_id'] = $joinin_detail['member_id'];
            $seller_array['seller_group_id'] = 0;
            $seller_array['store_id'] = $store_id;
            $seller_array['is_admin'] = 1;
            $state = $model_seller->addSeller($seller_array);
        }

        if($state){

            /** @var j_member_distributeLogic $member_distribute_logic */
            $member_distribute_logic = Logic('j_member_distribute');
            //三级分销返佣
            $member_distribute_logic->openStore($joinin_detail);
            //区域代理返佣记录
            $member_distribute_logic->open_store_for_agent($joinin_detail);

            //处理艺术家入驻会员类型
            if($joinin_detail['joinin_type']){
                Model()->table('member')->where(array('member_id' => $joinin_detail['member_id']))->update(array('is_artist' => 1));
            }

            // 添加相册默认
            $album_model = Model('album');
            $album_arr = array();
            $album_arr['aclass_name'] = Language::get('store_save_defaultalbumclass_name');
            $album_arr['store_id'] = $store_id;
            $album_arr['aclass_des'] = '';
            $album_arr['aclass_sort'] = '255';
            $album_arr['aclass_cover'] = '';
            $album_arr['upload_time'] = time();
            $album_arr['is_default'] = '1';
            $album_model->addClass($album_arr);

            $model = Model();
            //插入店铺扩展表
            $model->table('store_extend')->insert(array('store_id' => $store_id));
            $msg = Language::get('store_save_create_success');

            //插入店铺绑定分类表
            $store_bind_class_array = array();
            $store_bind_class = unserialize($joinin_detail['store_class_ids']);
            $store_bind_commis_rates = explode(',', $joinin_detail['store_class_commis_rates']);
            for($i = 0, $length = count($store_bind_class); $i < $length; $i++){
                list($class1, $class2, $class3) = explode(',', $store_bind_class[$i]);
                $store_bind_class_array[] = array(
                    'store_id' => $store_id,
                    'commis_rate' => $store_bind_commis_rates[$i],
                    'class_1' => $class1,
                    'class_2' => $class2,
                    'class_3' => $class3,
                    'state' => 1
                );
            }
            $model_store_bind_class = Model('store_bind_class');
            $model_store_bind_class->addStoreBindClassAll($store_bind_class_array);
            return true;
        }else {
            return false;
        }
    }


}
