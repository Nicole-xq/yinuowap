<?php
/**
 *
 *
 *
 * @package    library
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @author     ShopNC Team
 * @since      File available since Release v1.1
 */
class sendMemberMsg {
    private $code = '';
    private $member_id = 0;
    private $member_info = array();
    private $mobile = '';
    private $email = '';

    /**
     * 设置
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key,$value){
        $this->$key = $value;
    }

    public function send($param = array()) {
        $msg_tpl = rkcache('member_msg_tpl', true);        
        if (($this->code !='global_msg_tpl' && !isset($msg_tpl[$this->code])) || $this->member_id <= 0) {
            $msg_tpl[$this->code]=Model("member_msg_tpl")->getMemberMsgTplInfo(array('mmt_code'=>$this->code));
            if (empty($msg_tpl)) {
                return false;
            }
        }

        $tpl_info = $param['global_send_id'] ? $this->getGlobalInfo($param['global_send_id']) : $msg_tpl[$this->code];

        $setting_info = Model('member_msg_setting')->getMemberMsgSettingInfo(array('mmt_code' => $this->code, 'member_id' => $this->member_id), 'is_receive');
        if (empty($setting_info) || $setting_info['is_receive']) {
            // 发送站内信
            if ($tpl_info['mmt_message_switch']) {
                $message_content = ncReplaceText($tpl_info['mmt_message_content'],$param);
                $message_subject = ncReplaceText($tpl_info['mmt_message_subject'],$param);
                $this->sendMessage($message_content, $message_subject,$tpl_info);
            }
            $this->getMemberInfo();
            // 发送短消息
            if ($tpl_info['mmt_short_switch']) {
                if (!empty($this->mobile)) $this->member_info['member_mobile'] = $this->mobile;
                if ($this->member_info['member_mobile_bind'] && !empty($this->member_info['member_mobile'])) {
                    $param['site_name'] = C('site_name');
                    $message = ncReplaceText($tpl_info['mmt_short_content'],$param);
                    $this->sendShort($this->member_info['member_mobile'], $message);
                }
            }
            // 发送邮件
            if ($tpl_info['mmt_mail_switch']) {
                if (!empty($this->email)) $this->member_info['member_email'] = $this->email;
                if ($this->member_info['member_email_bind'] && !empty($this->member_info['member_email'])) {
                    $param['site_name'] = C('site_name');
                    $param['mail_send_time'] = date('Y-m-d H:i:s');
                    $subject = ncReplaceText($tpl_info['mmt_mail_subject'],$param);
                    $message = ncReplaceText($tpl_info['mmt_mail_content'],$param);
                    $this->sendMail($this->member_info['member_email'], $subject, $message);
                }
            }
            // 发送微信模板消息
            if ($tpl_info['mmt_wx_switch']) {
                /** @var wx_template_messageLogic $wx_template_message */
                $wx_template_message = Logic('wx_template_message');
                $wx_template_message->sendWxMessage($tpl_info['mmt_wx_subject'],$this->member_info,$param);
            }
            // 推送APP模板消息
            if ($tpl_info['mmt_app_switch']) {
                $tpl_info['mmt_app_subject'] = ncReplaceText($tpl_info['mmt_app_subject'],$param);
                $tpl_info['mmt_app_content'] = ncReplaceText($tpl_info['mmt_app_content'],$param);
                $tpl_info['mmt_app_link'] = ncReplaceText($tpl_info['mmt_app_link'],$param);
                Logic('app_push_message')->sendAppMessage($tpl_info,$this->member_info,$param);
            }
        }
    }

    /**
     * 会员详细信息
     */
    private function getMemberInfo() {
        if (empty($this->member_info)) {
            $this->member_info = Model('member')->getMemberInfoByID($this->member_id);
        }
    }

    /**
     * 全局推送信息
     * @param $send_id
     */
    private function getGlobalInfo($send_id) {
       $global_info = Model('global_msg_tpl')->getMemberMsgTplInfo(['id'=>$send_id], $field = '*');
       $global_info['mmt_short_content'] = '【{$site_name}】'.$global_info['mmt_short_content'];
       return $global_info;
    }

    /**
     * 发送站内信
     * @param unknown $message_content
     * @param unknown $message_subject
     * @param unknown $tpl_info
     */
    private function sendMessage($message_content, $message_subject, $tpl_info) {
        //添加短消息
        $model_message = Model('message');
        $insert_arr = array();
        $insert_arr['from_member_id'] = 0;
        $insert_arr['member_id'] = $this->member_id;
        $insert_arr['msg_title'] = $message_subject;
        $insert_arr['msg_content'] = $message_content;
        $insert_arr['mmt_code'] = $tpl_info['mmt_code'];
        $insert_arr['message_type'] = 1;
        $model_message->saveMessage($insert_arr);
    }

    /**
     * 发送短消息
     * @param unknown $number
     * @param unknown $message
     */
    private function sendShort($number, $message) {
        $sms = new Sms();
        $sms->send($number, $message);
    }

    /**
     * 发送邮件
     * @param unknown $number
     * @param unknown $subject
     * @param unknown $message
     */
    private function sendMail($number, $subject, $message) {
        // 计划任务代码
        $insert = array();
        $insert['mail'] = $number;
        $insert['subject'] = $subject;
        $insert['contnet'] = $message;
        Model('mail_cron')->addMailCron($insert);
    }
}
