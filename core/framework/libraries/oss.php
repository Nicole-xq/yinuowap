<?php
/**
 * aliyun oss
 *
 * @package    library
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @author     ShopNC Team
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');
final class oss {
    /**
     * @var  OSS\OssClient
     */
    private static $oss_sdk_service;
    private static $bucket;
    private static function _init() {
        $autoload =  BASE_DATA_PATH.'/api/oss2/autoload.php';
        require_once($autoload);
        self::$oss_sdk_service = new OSS\OssClient(C('oss.access_id'), C('oss.access_key'), C('oss.api_url'), false);
        self::$bucket = C('oss.bucket');
    }

    //格式化返回结果
    private static function _format($response) {
        echo '|-----------------------Start---------------------------------------------------------------------------------------------------'."\n";
        echo '|-Status:' . $response->status . "\n";
        echo '|-Body:' ."\n";
        echo $response->body . "\n";
        echo "|-Header:\n";
        print_r ( $response->header );
        echo '-----------------------End-----------------------------------------------------------------------------------------------------'."\n\n";
    }

    /**
     * 
     * @param unknown $src_file
     * @param unknown $new_file
     */
    public static function upload($src_file,$new_file) {
        self::_init();
        try{
            $response = self::$oss_sdk_service->uploadFile(self::$bucket,$new_file,$src_file);
            if ($response->status == '200') {
                return true;
            } else {
                return false;
            }
//             self::_format($response);exit;
        } catch (Exception $ex){
            return false;
            //die($ex->getMessage());
        }
    }

    public static function del($img_list = array()) {
        self::_init();
        try{
            $options = array(
                    'quiet' => false,
                    //ALIOSS::OSS_CONTENT_TYPE => 'text/xml',
            );
            $response = self::$oss_sdk_service->deleteObjects(self::$bucket,$img_list,$options);
            if ($response->status == '200') {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
            return false;
            //die($ex->getMessage());
        }
    }

    public static function getImgUrl($path, $type = '240'){
        $img_path = C('oss.img_url').$path;
        if($type != '1280'){
            //$str = '?x-oss-process=image/resize,m_lfit,w_%d,limit_0/auto-orient,0/quality,q_90';
            //$img_path .= sprintf($str, $type);
            //设置样式
            $img_path .= '?x-oss-process=style/'.$type;
        }
        return $img_path;
    }
}
