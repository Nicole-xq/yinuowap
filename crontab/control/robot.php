<?php
/**
 * 机器人脚本--预计3分钟执行一次
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

/**
 * Class robotControl
 * @deprecated 此脚本已弃用
 */
class robotControl extends BaseCronControl {

    public $multiple = 1;
    /**
     * 默认方法
     */
    public function indexOp() {
        die;
        //0 - 4 s 脚本时间错开
        sleep(mt_rand(0, 20));
        //拍品绑定机器人
        $this->_bind_robot_auction();

        $this->_robot_chujia();
    }

    /**
     * 创建机器人
     */
    private function _create_robot(){
        $str_1 = '176';
        $num = 0;
        for($a = 0;$a< 5000;$a++){
            $str_2 = '';
            for($i = 0;$i <8;$i++){
                $str_2 .= rand(0,9);
            }
            $re = Model('robot')->getInfo(array('robot_name'=>$str_1.$str_2));
            if(!$re){
                Model('robot')->add(array('robot_name'=>$str_1.$str_2,'update_time'=>time()));
                $num++;
            }
        }
        echo 'success:'.$num;
    }

    /**
     * 绑定拍品出价机器人
     */
    private function _bind_robot_auction(){
        $condition = array(
            'auction_start_time'=>array('lt',time()),
            'auction_end_time'=>array('gt',time()),
            'auction_type'=>['neq', auctionsModel::AUCTION_TYPE_PICKER]
        );
        $auction_list = Model('auctions')->getAuctionList($condition);
        //print_r($auction_list);
        if(!empty($auction_list)){
            foreach($auction_list as $v){
                $re = Model('robot')->getInfo(array('auction_id'=>$v['auction_id']));
                if(!$re){
                    $robot_tmp_list = Model('robot')->getList(array('auction_id'=>0));
                    if(count($robot_tmp_list) < 5){
                        $this->_create_robot();
                    }
//                    echo 2;
                    Model('robot')->beginTransaction();
                    $rand = rand(3,5);
                    $res = Model('robot')->updateInfo(array('auction_id'=>0),array('auction_id'=>$v['auction_id']),$rand);
                    $robot_list = Model('robot')->getList(array('auction_id'=>$v['auction_id']));
                    $re_num = Model('auctions')->editAuctions(array('num_of_applicants'=>$v['num_of_applicants'] + $rand),array('auction_id'=>$v['auction_id']));
                    if($re_num && $rand == count($robot_list)){
                        Model('robot')->commit();
                    }else{
                        Model('robot')->rollback();
                    }
                }
            }
        }
    }

    private function _robot_chujia(){
        //$sleep_time = rand(1,110);
        //sleep($sleep_time);
        $condition = array(
            'auction_start_time'=>array('lt',time()),
            'auction_end_time'=>array('gt',time()),
            'auction_reserve_price'=>array('gt','`current_price`'),
            'auction_type'=>array('neq', auctionsModel::AUCTION_TYPE_PICKER),
        );
        $auction_list = Model('auctions')->getAuctionList($condition);
        //print_r($auction_list);
        foreach($auction_list as $auction){
            //最新出价信息
            /** @var bid_logModel $bid_log_model */
            $bid_log_model = Model('bid_log');
            $re = $bid_log_model->getBidList(array('auction_id'=>$auction['auction_id']),'member_id,member_name,created_at','','offer_num desc',1);
            //print_r($re);
            //是真人的话机器人出价
            /*if ($re && $re[0]['member_id']!=0) {
                $this->_common($auction['auction_id']);

                continue;
            }*/
            $check_time=time()-27*60;
            if ($re && $re[0]['created_at']<=$check_time) {
                $this->_common($auction['auction_id']);
                continue;
            }
            //机器人第一次出价
            if (empty($re) && $auction['auction_start_time']<=$check_time) {                
                $this->_common($auction['auction_id']);
                continue;
            }
            //最后出价是机器人
            $check_time2=time()-25*60;
            if ($re && $auction['current_price_time']<=$check_time2 && $auction['current_price_time']>0) {                
                $this->_common($auction['auction_id']);
                continue;
            }
            //时间频率 1-5
            $endTime =  mt_rand(300,540);
            //最后5分钟的出价格
            if(time() >= ($auction['auction_end_time']-$endTime)){
                $this->_common($auction['auction_id']);
                continue;
            }
        }
    }
    private function _common($auction_id,$multiple = 1){
        $model_auctions = Model('auctions');
        $auction = $model_auctions->getAuctionsInfo(array('auction_id'=>$auction_id));
        //非最终加价
        if($auction['current_price']<$auction['auction_reserve_price']){
            //最少加一元 20190426世伟需求
            $add_price=$auction['auction_increase_range'] <= 0 ? 1 : $auction['auction_increase_range'];
            $current_price = $auction['current_price'] + $add_price;
        }else{
            //已经到达保留价格
            return true;
        }

        $uSleepCount = 0;
        $lockKey = \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_AUCTION_MEMBER_OFFER.$auction_id;
        do{
            $myRandomValue = \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->lock($lockKey, 8);
            if($myRandomValue === false){
                //0.01 - 0.5 s
                $uSleepMicroseconds = mt_rand(10000, 500000);
                usleep($uSleepMicroseconds);
            }
            $uSleepCount ++;
        }while($myRandomValue === false && $uSleepCount < 3);
        //没有获得锁
        if($myRandomValue === false){
            return false;
        }

        $auction = $model_auctions->getAuctionsInfo(array('auction_id'=>$auction_id));

        if($current_price <= $auction['auction_start_price']){
            //最少加一元 最少加一元 20190426世伟需求
            $current_price = $auction['auction_start_price'] + ($auction['auction_increase_range'] <= 0 ? 1 : $auction['auction_increase_range']);
        }
        if($current_price <  $auction['auction_reserve_price']){
            $current_price += $auction['auction_increase_range'] * mt_rand(0, 1);
        }
        try {
            $model_auctions = Model('auctions');
            $model_auctions->beginTransaction();
            $model_robot = Model('robot');
            $model_relation = Model('auction_member_relation');
            // 获取到当前价最高的出价人
            $condition = array(
                'auction_id'=>$auction['auction_id']
            );
            /** @var bid_logModel $bid_log_model */
            $bid_log_model = Model('bid_log');
            $re = $bid_log_model->getBidList(array('auction_id'=>$auction['auction_id']),'member_id,member_name','','offer_num desc',1);
            $member_id = $re[0]['member_id'];
            if(!empty($re)&& $re[0]['member_id'] == 0){
                $condition['robot_name'] = array('neq',$re[0]['member_name']);
            }
            $robot_list = $model_robot->getList($condition,'','','');
            if(empty($robot_list)){
                throw new Exception('未绑定机器人');
            }
            $rand_tmp = rand(0,count($robot_list)-1);
            $robot = $robot_list[$rand_tmp];
            // 添加出价日志
            $commission_amount=$add_price*0.1;
            $param = array(
                'member_id' => 0,
                'auction_id' => $auction['auction_id'],
                'created_at' => time(),
                'offer_num' => $current_price,
                'member_name' => $robot['robot_name'],
                'is_anonymous' => 0,
                'commission_amount'=>$commission_amount
            );
            //提醒前一个用户竞价被超越
            /** @var member_msg_tplModel $member_msg_tpl */
            $member_msg_tpl = Model('member_msg_tpl');
            //显示当前最高出价人, 方便片品结束生成订单显示
            /** @var bid_logModel $bidder_member_model */
            if (!empty($member_id)) {
                $sendParam = [
                    'member_id' => $member_id,
                    'mobile' => $robot['robot_name'],
                    'current_price' => $current_price
                ];
                $member_msg_tpl->sendBeyondPriceMessage($sendParam, $auction);
            }
            $model_bid_log = Model('bid_log');
            if(!$model_bid_log->addBid($param)){
                throw new Exception('数据库更新失败!');
            }
            $auction_config=Model("")->table("auction_config")->where(array('id'=>1))->find();
            $update_relation = array(
                'is_offer'=>1,
                'offer_num'=>$current_price
            );            
            if(!$model_relation->setRelationInfo($update_relation, array('member_id'=>0,'auction_id'=>$auction['auction_id'],'member_mobile'=>$robot['id']))){
                throw new Exception('数据库更新失败!');
            }
            //更新牌品数据
            $update = array(
                'current_price' => $current_price,
                'bid_number' => array('exp', 'bid_number + 1'),
                'current_price_time' => time(),
                'num_of_applicants' => array('exp', 'num_of_applicants+1'),
                'real_participants' => array('exp', 'real_participants + 1')
            );
            $check_time4=time()+$auction_config['add_time']*60;
            if ($auction['auction_end_time']<$check_time4) {
                echo "更新延长时间";
                $update['auction_end_time']=$check_time4;
                $update['auction_end_true_t']=$check_time4;
            }
            print_r($update);
            if(!$model_auctions->editAuctions($update, array('auction_id' => $auction['auction_id']))){
                throw new Exception('数据库更新失败!');
            }
            $model_auctions->commit();
            //解锁
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
        }catch (Exception $e){
            //解锁
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
            $model_auctions->rollback();
        }
    }
}