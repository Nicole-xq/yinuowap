<?php
/**
 * 任务计划 - 天执行的任务
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class timeout_remindControl extends BaseCronControl
{
    /**
     * 默认方法
     */
    public function indexOp()
    {
        //用户缴纳保证金违约提前提醒 Mr.liu  二十四小时一次  四十八小时一次  四十八小时以内提醒
        $this->_margin_timeout_remind();
    }

    /**
     * 用户缴纳保证金违约提前提醒
     * Mr.liu
     * 二十四小时一次  四十八小时一次
     * 四十八小时以内提醒
     */
    private function _margin_timeout_remind()
    {
        //提醒时间两天
        $time_start = mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"));
        //拍卖订单
        $condition = array();
        $condition['order_state'] = ORDER_STATE_NEW;
        $condition['order_type'] = 4;
        $condition['add_time'] = array('egt', $time_start);
        $_break = false;
        //分批，每批处理100个订单，最多处理1W个订单
        for ($i = 1; $i < 101; $i++) {
            if ($_break) {
                break;
            }
            $begin = ($i - 1) * 100;
            $limit = $begin . ',' . 100;
            $model = Model();
            $on = 'orders.auction_id = auctions.auction_id';
            $order_list = $model->table('orders,auctions')
                ->join('inner,left')->on($on)
                ->where($condition)->limit($limit)
                ->select();
            if (empty($order_list)) {
                break;
            }
            foreach ($order_list as $order_info) {
                $url = AUCTION_NEW_URL . '/auctionCommodityDetails?auction_id=' . $order_info['auction_id'];
                $url = filterUrl($url);
                $url = sinaShortenUrl($url);
                $param = array();
                $param['code'] = 'margin_timeout_remind';
                $param['member_id'] = $order_info['buyer_id'];
                $param['param'] = array(
                    'auction_name' => $order_info['auction_name'] ?: '拍卖品',
                    'order_sn' => $order_info['order_sn'],
                    //提醒时间  格式: 2017年7月30 10:30至2017年8月1 10:30
                    'remind_time' => date('Y年m月d日, H:i', $order_info['add_time'])
                        . '至' . date('Y年m月d日, H:i', $order_info['add_time'] + 24 * 3600),
                    'auction_url' => $url,
                    'url' => $url
                );
                /** @var member_msg_tplModel $member_msg_tpl */
                $member_msg_tpl =Model('member_msg_tpl');
                $member_msg_tpl->sendWXTplMsg($param);
            }
        }
    }
}