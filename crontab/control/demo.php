<?php
/**
 * 临时脚本
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class demoControl extends BaseCronControl {

    public $margin_id_arr = array(1128);
    /**
     * 默认方法
     */
    public function indexOp() {
        exit;
        $this->demo($this->margin_id_arr);

    }


    /**
     *
     */
    private function demo($margin_id_arr){
        foreach($margin_id_arr as $margin_id){
            $margin_info = Model('margin_orders')->getOrderInfo(array('margin_id'=>$margin_id));
            $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);
            $day_num = Logic('auction')->getInterestDay($margin_info['payment_time'],$auction_info['auction_end_time']);//计息天数

            $member_distribute_logic = Logic('j_member_distribute');
            //添加利息待结算记录
            //$member_distribute_logic->margin_refund($margin_info,$day_num,$auction_info['auction_bond_rate']);
            //添加佣金待结算记录
            $result_3 = $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num,0);
        }
        exit;
    }

    /**
     * 保证金订单本金和利息未返--修复脚本
     */
    private function demo2($margin_id_arr){
        foreach($margin_id_arr as $margin_id){
            $margin_info = Model('margin_orders')->getOrderInfo(array('margin_id'=>$margin_id));
            $auction_info = Model('auctions')->getAuctionsInfoByID($margin_info['auction_id']);
            $day_num = Logic('auction')->getInterestDay($margin_info['payment_time'],$auction_info['auction_end_time']);//计息天数

            /** @var j_member_distributeLogic $member_distribute_logic */
            $member_distribute_logic = Logic('j_member_distribute');
            //计算保证金利息
            //$member_distribute_logic->pay_margin_refund($margin_info, $day_num, $auction_info['auction_bond_rate']);
            //计算保证金返佣
            $member_distribute_logic->pay_top_margin_refund($margin_info, $day_num);

            Logic('auction_order')->MarginLog($margin_info['buyer_id'], $margin_info['buyer_name'], 2, $margin_info['margin_amount'], $margin_info['order_sn']);
            $model_pd = Model('predeposit');
            $data_pd['member_id'] = $margin_info['buyer_id'];
            $data_pd['member_name'] = $margin_info['buyer_name'];
            $data_pd['amount'] = $margin_info['margin_amount'];
            $data_pd['order_sn'] = $margin_info['order_sn'];
            $model_pd->changePd('margin_to_pd',$data_pd);
        }
    }


}
