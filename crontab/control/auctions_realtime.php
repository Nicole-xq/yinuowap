<?php
/**
* 任务计划 - 分钟执行的任务
* 执行频率1-2分钟
*
*
*
* @copyright  Copyright (c) 2007-2017 ShopNC Inc. (http://www.shopnc.net)
* @license    http://www.shopnc.net
* @link       http://www.shopnc.net
* @since      File available since Release v1.1
*/
defined('InShopNC') or exit('Access Invalid!');

class auctions_realtimeControl extends BaseCronControl {
    public function indexOp() {
        //拍品结束脚本
        $this->_auctions();
        //专场结束脚本
        $this->_special_end();
        //成交返佣
        $this->_bill_commission();
        //保证金利息返佣
        //$this->_pay_rate_commisson();
        //保证金订单超时取消
        $this->margin_order_cannel();
        /*$this->_auctions_liupai();
        $this->_auctions_end();
        $this->_auctions_order();*/

        //$this->_bill_commission();
        $this->pay_margin_default_state();
        $this->pay_margin_default_state_2();
        $this->check_margin_order_lock();
        $this->orders_cannel();
    }
    //专场结束
    private function _special_end(){
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        /** @var auctionsModel $model_auctions */
        $model_auctions = Model('auctions');
        $condition['special_end_time'] = 0;
        $condition['special_state'] = 20;
        $condition['store_id']=0;
        $condition['is_open']=1;
        $special_list = $model_special->getSpecialList($condition, '*', 'special_id desc', 0);
        if (!empty($special_list)) {
            foreach ($special_list as $key => $item) {
                $map['special_id']=$item['special_id'];
                $map['state']=0;
                $num=$model_auctions->where($map)->count();
                if (!$num) {
                    $update = array('special_end_time' => TIMESTAMP);
                     // 更新成结束状态
                    $model_special->editSpecial($update, array('special_id'=>$item['special_id']));
                    echo "专场ID".$item['special_id']."关闭<br/>";
                }
            }
        }
    }
    /**
     * 拍品状态出来
     */

    private function _auctions(){
        $model_auctions = Model('auctions');
        $condition['is_liupai'] = 0;
        $condition['state'] = 0;
        $condition['auction_start_time'] = ['gt', 0];
        $condition['auction_end_time'] = array('elt',TIMESTAMP);
        //$condition['auction_end_true_t'] = array('between','0,' . TIMESTAMP);
        $list = $model_auctions->getAuctionList($condition);
        $model_auctions->beginTransaction();
        try {            
            if(!empty($list) && is_array($list)) {
                foreach($list as $k => $v) {                    
                    $map['auction_id']=$v['auction_id'];
                    $map['member_id']=array('neq',0);
                    //防止null的问题
                    $map['offer_num']=array('egt', ($v['auction_reserve_price'] <=0 ? 0 : $v['auction_reserve_price']) );
                    $num=Model("bid_log")->where($map)->count();
                    if ($num>0) {
                        //修改状态
                        $update = array();
                        $update['is_liupai'] = 0;
                        $update['state'] = 1;
                        $id = Model("")->table('auctions')->where(array('auction_id' => $v['auction_id']))->lock(true)->update($update);
                        //echo $id."<br/>";
                        echo "拍品ID".$v['auction_id']."拍卖成功<br/>";
                        //有真人出价生产订单
                        $this->_auctions_end($v['auction_id']);
                        //退保证金
                        $this->_update_margin_order($v['auction_id']);
                        //保证金利息
                        $this->_pay_rate_commisson($v['auction_id']);
                    }else{
                        //没有真人出价流拍,修改状态
                        $update = array();
                        $update['is_liupai'] = 1;
                        $update['state'] = 1;
                        Model("")->table('auctions')->where(array('auction_id' => $v['auction_id']))->lock(true)->update($update);
                        echo "拍品ID".$v['auction_id']."流拍<br/>";
                        //退保证金
                        $this->_update_margin_order($v['auction_id']);
                        //保证金利息
                        $this->_pay_rate_commisson($v['auction_id']);
                    }
                }
            }
            $model_auctions->commit();
        } catch (Exception $e) {
            $model_auctions->rollback();
        }
    }


    /**
     * 结束处理
     */
    private function _auctions_end($auction_id) {
        $model_auctions = Model('auctions');
        $condition['auction_id'] = $auction_id;
        $list = $model_auctions->table('auctions')->where($condition)->find();
        try {
            $model_auctions->beginTransaction();
            if(!empty($list)) {
                $map['auction_id']=$auction_id;
                $map['member_id']=array('neq',0);
                $bid_log=Model("bid_log")->where($map)->order("offer_num DESC")->find();
                //查出谁最后出的最高价
                if ($bid_log) {
                    $order=Model('')->table('orders')->where(array('auction_id'=>$auction_id))->find();
                    if (empty($order)) {
                        $this->_auctions_order($list,$bid_log);
                    }                    
                }
            }
            $model_auctions->commit();            
        }catch (Exception $e){
            $model_auctions->rollback();
            yLog()->error("拍卖结束处理异常", [$auction_id, \Yinuo\Lib\Helpers\LogHelper::getEInfo($e)]);
        }
    }

    /**
     * 订单处理
     */
    private function _auctions_order($auction,$bid_log) {        
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $model_order = Model('order');
        //锁定拍中人该拍品的保证金
        $check = $model_margin_orders->table('margin_orders')->where(array('buyer_id' => $bid_log['member_id'],'auction_id'=>$auction['auction_id'],'order_state'=> 1))->update(array('lock_state'=> 1));
        $margin_order=$model_margin_orders->table('margin_orders')->where(array('buyer_id' => $bid_log['member_id'],'auction_id'=>$auction['auction_id'],'order_state'=> 1))->order("margin_id DESC")->find();
        $member = Model("member")->getMemberInfo(array('member_id'=>$bid_log['member_id']));
        if($check){
            $sellerId = 0;
            //获取拍品的卖家ID
            if (!empty($auction['goods_id'])) {
                $sellerId = logic('auction')->getSellerIdByGoodsId($auction['goods_id']);
            }
            $model_auctions->beginTransaction();
            try {
                //echo "string";
                $auction_id = $auction['auction_id'];
                $auction_info = $auction;
                $pay_sn = Logic('buy_1')->makePaySn($bid_log['member_id']);
                $order_pay = array();
                $order_pay['pay_sn'] = $pay_sn;
                $order_pay['buyer_id'] = $bid_log['member_id'];
                $order_pay_id = $model_order->addOrderPay($order_pay);
                if(!$order_pay_id){
                    throw new Exception('订单保存失败[未生成支付单]');
                }
                $order_info=Model("")->table("orders")->where(array('buyer_id'=>$bid_log['member_id'],'auction_id'=>$auction_id,'order_type'=>4))->find();
                if (!empty($order_info)) {
                    throw new Exception('已经存在订单,无需生成');
                }
                $order = array();
                $order['order_sn'] = Logic('buy_1')->makeOrderSn($order_pay_id);
                $order['pay_sn'] = $pay_sn;
                $order['store_id'] = $auction_info['store_id'];
                $order['store_name'] = $auction_info['store_name'];
                $order['buyer_id'] = $bid_log['member_id'];
                $order['seller_id'] = $sellerId;       //获取卖家ID
                $order['buyer_name'] = $bid_log['member_name'];
                $order['add_time'] = TIMESTAMP;
                $order['payment_code'] = 'online';
                $order['order_state'] = 10;
                $order['order_amount'] = $auction_info['current_price'];
                $order['goods_amount'] = $auction_info['current_price'];
                $order['auction_cost'] = $auction_info['auction_cost'];
                //订单总奖励金(成交返佣加竞价返佣)
                $bid_comssion = Model('')->table('member_commission')->where(['order_goods_id' => $auction_id])->sum('commission_amount');
                //$total_commission_amount = bcadd($order_info['pd_amount'], $bid_comssion, 2);
                //成交返佣在支付完成之后生成
                //奖励金结算金额=（订单金额-商品成本价-返佣总金额（竞价返佣+成交返佣））*60%
                $operate_commission = bcmul(bcsub($auction_info['current_price'], bcadd($auction_info['auction_cost'], $bid_comssion, 2), 2), 0.6, 2);
                $order['commission_amount'] = $operate_commission;
                $order['source_staff_id'] = $member['source_staff_id'];
                $distribute_info = Model('')->table('member_distribute')->where(['member_id' => $bid_log['member_id']])->find();
                if (!empty($distribute_info)) {
                    $top_member = !empty($distribute_info['top_member']) ? $distribute_info['top_member'] : 0;
                } else {
                    $top_member = 0;
                }
                $order['top_member_id'] = $top_member;
                if (empty($margin_order['order_from'])) {
                    $margin_order['order_from']=0;
                }
                $order['order_from'] = $margin_order['order_from'];

                $order['order_type'] = 4;
                $order['auction_id'] = $auction_id;
                //$order['margin_amount'] = $bid_log['offer_num'];
                //print_r($order);
                $order_id = Model("")->table("orders")->insert($order);
                if (!$order_id) {
                    throw new Exception('订单创建失败');
                }
                $re = $model_margin_orders->table('margin_orders')->where(array('auction_id' => $auction_id,'buyer_id'=>$bid_log['member_id'],'order_state'=> 1))->update(array('auction_order_id'=> $order_id));
                if(!$re){
                    throw new Exception('保证金订单更新订单号失败');
                }
                $url = AUCTION_NEW_URL."/auctioncommoditydetails?auction_id=".$auction['auction_id'];
                $url = filterUrl($url);
                $short = sinaShortenUrl($url);
                $param = array();
                $param['code'] = 'auction_auction';
                $param['member_id'] = $member['member_id'];
                $param['number']['mobile'] = $member['member_mobile'];
                $param['param'] = array(
                    'auction_name' => $auction_info['auction_name'],
                    'money' => $auction_info['current_price'],
                    'url' => $short,
                    'wx_url' => $short
                );
                QueueClient::push('sendMemberMsg', $param);
                $model_auctions->commit();
            }catch (Exception $e){
                $model_auctions->rollback();
                throw $e;
            }
        }
    }

    /**
     * 保证金处理
     */
    private function margin_refund($auction_id,$auction_info) {
        $model_margin_orders = Model('margin_orders');
        $model_member = Model('member');
        $model_predeposit = Model('predeposit');
        $list = $model_margin_orders->table('margin_orders')->where(
            array('auction_id' => $auction_id,'order_state'=> 1,'lock_state'=>0,'refund_state'=> 0)
        )->select();
        if(!empty($list) && is_array($list)) {

            /** @var j_member_distributeLogic $member_distribute_logic */
            $member_distribute_logic = Logic('j_member_distribute');
            $_time3 = 3600*24*3;//拍卖结束前3天
            $end_time = $auction_info['auction_end_time'];//结束时间
            $auction_bond_rate = $auction_info['auction_bond_rate'];
            foreach($list as $k => $v) {
                $margin_amount = $v['margin_amount'];
                $tmp_re = array();
                if($auction_info['auction_type'] == 1){
                    $condition = array(
                        'buyer_id'=>$v['buyer_id'],
                        'order_state'=>array('neq','4'),
                        'margin_id'=>array('neq',$v['margin_id'])
                    );
                    $tmp_re = Model('margin_orders')->getOrderInfo($condition);
                }
                if ($margin_amount>0&&($auction_info['auction_type'] != 1 || empty($tmp_re))) {
                    $payment_time = $v['payment_time'];//支付(付款)时间
                    if(strtotime($auction_info['interest_last_date']) > $payment_time){
                        $day_num = Logic('auction')->getInterestDay($payment_time,$auction_info['auction_start_time']);//计息天数
                    }else{
                        $day_num = 0;
                    }
                    //计算保证金返佣
                    $member_distribute_logic->pay_top_margin_refund($v, $day_num);
                    //添加区域代理佣金待结算记录
                    $member_distribute_logic->agent_margin_refund($v, $day_num);
                }
                if ($v['lock_state'] == 0 ) {//买家未中拍返还保证金
                    Logic('auction_order')->MarginLog($v['buyer_id'], $v['buyer_name'], 2, $v['margin_amount'], $v['order_sn']);
                    $model_pd = Model('predeposit');
                    $data_pd['member_id'] = $v['buyer_id'];
                    $data_pd['member_name'] = $v['buyer_name'];
                    $data_pd['amount'] = $v['margin_amount'];
                    $data_pd['order_sn'] = $v['order_sn'];
                    $model_pd->changePd('margin_to_pd',$data_pd);
                }
                if ($v['lock_state'] == 0 ){
                    $update_margin = array(
                        'refund_state'=> 1,
                        'pay_date_number'=> $day_num,
                        'finnshed_time'=> TIMESTAMP
                    );
                    $model_margin_orders->table('margin_orders')->where(array('margin_id' => $v['margin_id']))->update($update_margin);
                }
            }
        }
    }

    /**
     * 返佣结算
     */
    private function _bill_commission(){
        $condition = array();
        $condition['commission_state'] = 0;
        $condition['order_state'] = 40;
        $condition['finnshed_time'] = array('lt',time() + 120);
        $condition['order_type'] = 4;
        $condition['auction_id'] = array('neq','0');
        $auction_order = (array)Model()->table('orders')->field('order_id')->key('order_id')->where($condition)->limit(1000)->select();

        foreach($auction_order as $order){
            $this->handleOrder($order['order_id']);
        }
    }

    //处理单个订单返佣
    private function handleOrder($order_id = 0){
        if($order_id > 0){
            //保存余额变更日志
            $pdModel = Model('predeposit');
            $deal_list = (array)Model()->table('member_commission')->where(array('order_id' => $order_id,'commission_type'=>8,'dis_commis_state'=>0))->select();
            $member_model = Model('member');
            $flag = 0;
            foreach($deal_list as $deal){
                $update_member = array();
                $update_member['available_commis'] = array('exp','available_commis+'.$deal['commission_amount']);
                $update_member['freeze_commis'] = array('exp','freeze_commis-'.$deal['commission_amount']);
                $update_member['available_predeposit'] = array('exp','available_predeposit+'.$deal['commission_amount']);
                $res = $member_model->editMember(array('member_id'=>$deal['dis_member_id']),$update_member);
                if($res){
                    echo "订单ID".$order_id."的保证金订单的成交奖励ID".$deal['log_id']."<br/>";
                    $flag++;
                    Model()->table('member_commission')->where(array('log_id'=>$deal['log_id']))->update(array('dis_commis_state'=>1,'commission_time'=>time()));
                }
                //做日志使用
                $priceInfo = Model('')->table('member')
                    ->field('available_predeposit')
                    ->where(['member_id' => $deal['dis_member_id']])->find();
                if (isset($priceInfo['available_predeposit'])) {
                    $deal['lg_available_amount'] = $priceInfo['available_predeposit'];
                    $pdModel->insertPdLog($deal);
                }
            }
            if($flag == count($deal_list)){
                Model()->table('orders')->where(array('order_id'=>$order_id))->update(array('commission_state'=>1));
            }
        }
    }
    /**
     * 保证金利息
     */
    private function _pay_rate_commisson($auction_id){//保存余额变更日志
        $pdModel = Model('predeposit');
        $deal_list = Model()->table('member_commission')->where(array('order_goods_id' => $auction_id,'commission_type'=>array("in","6,9"),'dis_commis_state'=>0))->limit(1000)->select();
        foreach ($deal_list as $key => $deal) {
            $member_model=Model('member');
            $update_member = array();
            $update_member['available_commis'] = array('exp','available_commis+'.$deal['commission_amount']);
            $update_member['freeze_commis'] = array('exp','freeze_commis-'.$deal['commission_amount']);
            $update_member['available_predeposit'] = array('exp','available_predeposit+'.$deal['commission_amount']);
            $res = $member_model->editMember(array('member_id'=>$deal['dis_member_id']),$update_member);
            Model()->table('member_commission')->where(array('log_id'=>$deal['log_id']))->update(array('dis_commis_state'=>1,'commission_time'=>time()));
            //做日志使用
            $priceInfo = Model('')->table('member')
                ->field('available_predeposit')
                ->where(['member_id' => $deal['dis_member_id']])->find();
            if (isset($priceInfo['available_predeposit'])) {
                $deal['lg_available_amount'] = $priceInfo['available_predeposit'];
                $pdModel->insertPdLog($deal);
            }
            echo $deal['log_id']."返佣";
        }
    }

    /**
     * 保证金订单取消
     */
    private function _update_margin_order($auction_id){
        $model_margin_order = Model('margin_orders');
        //未支付的直接取消
        $model_margin_order->editOrder(array('order_state'=>4),array('order_state'=>0,'auction_id'=>$auction_id));

        $list = $model_margin_order->getOrderList(array('order_state'=>array("in","1"),'auction_id'=>$auction_id,'lock_state'=>0,'refund_state'=>0));
        //print_r($list);
        if(!empty($list)){
            foreach($list as $order_info){
                $member_id_array[$order_info['buyer_id']]=$order_info['buyer_id'];
                try{
                    $model_margin_order->beginTransaction();
                    $model_pd = Model('predeposit');
                    //解冻充值卡
                    $rcb_amount = floatval($order_info['rcb_amount']);
                    if ($rcb_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $order_info['buyer_id'];
                        $data_pd['member_name'] = $order_info['buyer_name'];
                        $data_pd['amount'] = $rcb_amount;
                        $data_pd['order_sn'] = $order_info['order_sn'];
                        $model_pd->changeRcb('order_cancel', $data_pd);
                    }

                    //解冻预存款
                    $pd_amount = floatval($order_info['pd_amount']);
                    if ($pd_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $order_info['buyer_id'];
                        $data_pd['member_name'] = $order_info['buyer_name'];
                        $data_pd['amount'] = $pd_amount;
                        $data_pd['order_sn'] = $order_info['order_sn'];
                        $model_pd->changePd('order_cancel', $data_pd);
                    }
                    //解冻积分
                    $points_amount = floatval($order_info['points_amount']);
                    if ($points_amount > 0) {
                        $nuobi_points = $points_amount * 100;
                        Model('points')->savePointsLog('cancel', array('pl_memberid' => $order_info['buyer_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                    }
                    //线上支付退款
                    $api_pay_amount = floatval($order_info['api_pay_amount']);
                    if ($api_pay_amount>0) {
                        $order_info['refund_money']=$order_info['api_pay_amount'];                        
                        $this->_wxpay($order_info);
                    }

                    $re = $model_margin_order->editOrder(array('refund_state'=>1),array('margin_id'=>$order_info['margin_id']));
                    $log_data['member_id']=$order_info['buyer_id'];
                    $log_data['member_name']=$order_info['buyer_name'];
                    $log_data['type']="order_refund";
                    $log_data['available_amount']=$rcb_amount;
                    $log_data['member_name']=$api_pay_amount;
                    $log_data['description']="微信在线退款:".$api_pay_amount."；解冻预存款：".$pd_amount."；解冻充值卡：".$rcb_amount;
                    $log_data['auction_id']=$order_info['auction_id'];
                    Model("margin_log")->addLog($log_data);
                    $url = AUCTION_OLD_URL."/wap/tmpl/member/margin_details_wap2.html?auction_id=".$order_info['auction_id']; 
                    $url = filterUrl($url); 
                    $short = sinaShortenUrl($url); 
                    $param = array();
                    $param['code'] = 'release_margin';
                    $param['member_id'] = $order_info['buyer_id'];
                    $param['number']['mobile'] = $order_info['buyer_phone'];
                    $param['param'] = array(
                        'auction_name' => $order_info['auction_name'],
                        'money' => $order_info['margin_amount'],
                        'url' => $short,
                        'wx_url' => $short
                    );
                    QueueClient::push('sendMemberMsg', $param);
                    if(!$re){
                        throw new Exception('失败');
                    }
                    $model_margin_order->commit();
                }catch (Exception $e){
                    $model_margin_order->rollback();
                }
            }
            $condition['auction_id'] = $auction_id;
            $auction_info = Model("")->table('auctions')->where($condition)->find();
            foreach ($member_id_array as  $memberid) {                
                $member = Model("member")->getMemberInfo(array('member_id'=>$memberid));
                $url = AUCTION_OLD_URL."/wap/tmpl/member/margin_details_wap2.html?auction_id=".$auction_id; 
                $url = filterUrl($url); 
                $short = sinaShortenUrl($url); 
                $param = array();
                $param['code'] = 'lost_auction';
                $param['member_id'] = $member['member_id'];
                $param['number']['mobile'] = $member['member_mobile'];
                $param['param'] = array(
                    'auction_name' => $auction_info['auction_name'],
                    'url' => $short,
                    'wx_url' => $short
                );
                QueueClient::push('sendMemberMsg', $param);
            }
        }
    }
    /**
     * 微信退款
     *
     */
    public function _wxpay($order_info, $apiRefundMsgSuccess = '微信成功退款:') {
        $result = array('state'=>'false','msg'=>'参数错误，微信退款失败');
        $model_margin_order=Model("margin_orders");
        $order=$order_info;
        if(!empty($order) && in_array($order['payment_code'],array('wxpay','wxpay_jsapi','wx_saoma','wxpay_mini','yinuovip_mini'))) {
            $condition['payment_code'] = $order['payment_code'];
            $model_payment = Model('mb_payment');
            $payment_info = $model_payment->getMbPaymentInfo($condition);//接口参数
            $payment_info = $payment_info['payment_config'];
            $payment_config = $payment_info;
            $order['payment_config'] = $payment_config;
            $refund_amount = $order['refund_money'];//本次在线退款总金额
            if ($refund_amount > 0) {
                $wxpay = $order['payment_config'];
                define('WXPAY_APPID', $wxpay['appId']);
                define('WXPAY_MCHID', $wxpay['partnerId']);
                define('WXPAY_KEY', $wxpay['apiKey']);
                $total_fee = $order['api_pay_amount']*100;//微信订单实际支付总金额(在线支付金额,单位为分)
                $refund_fee = $refund_amount*100;//本次微信退款总金额(单位为分)
                $api_file = BASE_PATH.DS.'api'.DS.'refund'.DS.'wxpay'.DS.'WxPay.Api.php';
                include_once $api_file;
                $miniApi_file = BASE_PATH.DS.'api'.DS.'refund'.DS.'wxpay'.DS.'WxMiniPay.Api.php';
                include_once $miniApi_file;
                $input = new WxPayRefund();
                $input->SetOut_trade_no($order['order_sn']);//商户ssss订单号
                $input->SetTotal_fee($total_fee);
                $input->SetRefund_fee($refund_fee);
                $input->SetOut_refund_no(date('YmdHis').$order['margin_id']);//退款批次号
                $input->SetOp_user_id(WxPayConfig::MCHID);
                if ($order['payment_code'] == 'wxpay_mini' || $order['payment_code'] == 'yinuovip_mini') {
                    $data = WxMiniPayApi::refund($input);
                } else {
                    $data = WxPayApi::refund($input);
                }
                if(!empty($data) && $data['return_code'] == 'SUCCESS') {//请求结果
                    if($data['result_code'] == 'SUCCESS') {//业务结果
                        $result2['refund_state']=1;
                        $result2['api_refund_state'] = 1;
                        $result2['api_refund_msg'] = $apiRefundMsgSuccess.$refund_amount;
                        $re = $model_margin_order->editOrder($result2,array('margin_id'=>$order_info['margin_id']));

                        $consume_array = array();
                        $consume_array['member_id'] = $order['buyer_id'];
                        $consume_array['member_name'] = $order['buyer_name'];
                        $consume_array['consume_amount'] = $order_info['api_pay_amount'];
                        $consume_array['consume_time'] = time();
                        $consume_array['consume_remark'] = '微信在线退款成功（到账有延迟），退款退货单号：'.date('YmdHis').$order['margin_id'];
                        QueueClient::push('addConsume', $consume_array);
                        unset($result);
                        $result['msg'] = '微信成功退款'.$refund_amount;//错误描述
                    } else {
                        $result3['refund_state']=1;
                        $result3['api_refund_state'] = 2;
                        $result3['api_refund_msg'] = '微信退款错误,'.$data['err_code_des'];//错误描述s;
                        $re = $model_margin_order->editOrder($result3,array('margin_id'=>$order_info['margin_id']));
                        $result['msg'] = '微信退款错误,'.$data['err_code_des'];//错误描述
                    }
                } else {
                    yLog()->error('微信退款失败', $order_info);
                    $result['msg'] = '微信接口错误,'.$data['return_msg'];//返回信息
                }
            }
        }
        print_r($result);
        //exit(json_encode($result));
    }

    /**
     * 保证金15分钟未支付取消订单
     */
    private function margin_order_cannel(){
        $model_margin_order = Model('margin_orders');
        //未支付创建超过15分钟
        $map['order_state']=array("in","0,3");
        $check_time=time()-15*60;
        $map['created_at']=array('elt',$check_time);
        $list=$model_margin_order->where($map)->limit(1000)->select();
        foreach ($list as $key => $value) {
            $rec=$model_margin_order->editOrder(array('order_state'=>4,'updated_at'=>time()),array('margin_id'=>$value['margin_id']));
            //根据需求判断是否变更
            if ($rec) {
                $model_pd = Model('predeposit');
                //解冻充值卡
                $rcb_amount = floatval($value['rcb_amount']);
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $value['buyer_id'];
                    $data_pd['member_name'] = $value['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $value['order_sn'];
                    $model_pd->changeRcb('order_cancel', $data_pd);
                }

                //解冻预存款
                $pd_amount = floatval($value['pd_amount']);
                if ($pd_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $value['buyer_id'];
                    $data_pd['member_name'] = $value['buyer_name'];
                    $data_pd['amount'] = $pd_amount;
                    $data_pd['order_sn'] = $value['order_sn'];
                    $model_pd->changePd('order_cancel', $data_pd, $value);
                }
                //解冻积分
                $points_amount = floatval($value['points_amount']);
                if ($points_amount > 0) {
                    $nuobi_points = $points_amount * 100;
                    Model('points')->savePointsLog('cancel', array('pl_memberid' => $value['buyer_id'], 'pl_membername' => $value['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $value['order_sn'], 'order_id' => $value['order_id']), true);
                }
            }
            echo "取消保证金订单".$value['margin_id']."判断时间".$check_time."<br/>";
        }
    }
    /**
     * 实物订单15分钟未支付取消订单
     */
    private function orders_cannel(){
        $time=time()-5*60;
        $map['api_pay_time']=array(array('neq',0),array('elt',$time));
        $map['order_state']=10;
        $list=Model("")->table("orders")->where($map)->limit(1000)->select();
        $model_pd = Model('predeposit');
        $model_order = Model('order');
        foreach ($list as $orders) {
            $pay_sn = Logic('buy_1')->makePaySn($orders['buyer_id']);
            $order_pay = array();
            $order_pay['pay_sn'] = $pay_sn;
            $order_id_store = [];
            $order_pay['buyer_id'] = $orders['buyer_id'];
            /** @var orderModel $model_order */
            $order_pay_id = $model_order->addOrderPay($order_pay);
            //解冻充值卡
            $pd_amount = $orders['pd_amount'];
            if ($pd_amount > 0) {
                $data_pd = array();
                $data_pd['member_id'] = $orders['buyer_id'];
                $data_pd['member_name'] = $orders['buyer_name'];
                $data_pd['amount'] = $pd_amount;
                $data_pd['order_sn'] = $orders['order_sn'];
                //print_r($data_pd);
                $res = $model_pd->changePd('order_cancel2', $data_pd);
                //print_r($res);
            }
            $update=array();
            $update['order_sn'] = Logic('buy_1')->makeOrderSn($order_pay_id);
            $update['pay_sn'] = $pay_sn;
            $update['api_pay_time']=0;
            $update['pd_amount']=0;
            $update['order_state']=10;
            $rec=Model("")->table("orders")->where(array('order_id'=>$orders['order_id']))->update($update);
            echo $rec."实物订单退款<br/>";
            $model_margin_order = Model('margin_orders');
            $result2['refund_state']=0;
            $re = $model_margin_order->editOrder($result2,array('auction_order_id'=>$orders['order_id']));
            echo $re;
        }
    }

    /**
     * 保证金抵扣后退款
     */
    private function pay_margin_default_state(){
        $model_margin_order = Model('margin_orders');
        $map['default_state']=2;
        $map['order_state']=1;
        $map['lock_state']=1;
        $map['refund_state']=['neq', 1];
        $list=Model("")->table("margin_orders")->where($map)->limit(1000)->select();
        //print_r($list);
        foreach ($list as $key => $value) {
            if ($value['rufund_money']>0 && $value['refund_state']==2) {
                //print_r($value);
                //print_r($value);
                //部分退款
                $model_pd = Model('predeposit');
                //解冻充值卡
                $rcb_amount = $value['rufund_money'] - $value['api_pay_amount'];
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $value['buyer_id'];
                    $data_pd['member_name'] = $value['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $value['order_sn'];
                    //print_r($data_pd);
                    $res = $model_pd->changePd('order_cancel2', $data_pd);
                    //print_r($res);
                }else{
                   $rcb_amount=0; 
                }
                if ($value['payment_code']=="wxpay_jsapi" || $value['payment_code']=="wxpay_mini" || $value['payment_code']=="yinuovip_mini") {
                    $refund_money=$value['rufund_money']-$rcb_amount;
                    if ($refund_money>0) {
                        echo $refund_money."元退款<br/>";
                        $value['refund_money']=$refund_money;
                        $this->_wxpay($value);
                    }                    
                }
                $result2['refund_state']=1;
                $re = $model_margin_order->editOrder($result2,array('margin_id'=>$value['margin_id']));
            }else{                
                //全额退
                if($value['refund_state']==0){
                    $model_pd = Model('predeposit');
                    //解冻充值卡
                    $rcb_amount = floatval($value['rcb_amount']);
                    if ($rcb_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $value['buyer_id'];
                        $data_pd['member_name'] = $value['buyer_name'];
                        $data_pd['amount'] = $rcb_amount;
                        $data_pd['order_sn'] = $value['order_sn'];
                        $model_pd->changeRcb('order_cancel2', $data_pd);
                    }

                    //解冻预存款
                    $pd_amount = floatval($value['pd_amount']);
                    if ($pd_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $value['buyer_id'];
                        $data_pd['member_name'] = $value['buyer_name'];
                        $data_pd['amount'] = $pd_amount;
                        $data_pd['order_sn'] = $value['order_sn'];
                        $model_pd->changePd('order_cancel2', $data_pd);
                    }
                    //解冻积分
                    $points_amount = floatval($value['points_amount']);
                    if ($points_amount > 0) {
                        $nuobi_points = $points_amount * 100;
                        Model('points')->savePointsLog('cancel', array('pl_memberid' => $value['buyer_id'], 'pl_membername' => $value['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $value['order_sn'], 'order_id' => $value['order_id']), true);
                    }
                    if ($value['payment_code']=="wxpay_jsapi" || $value['payment_code']=="wxpay_mini" || $value['payment_code']=="yinuovip_mini") {
                        $value['refund_money']=$value['api_pay_amount'];
                        $this->_wxpay($value);
                    }
                    $result2['refund_state']=1;
                    $re = $model_margin_order->editOrder($result2,array('margin_id'=>$value['margin_id']));
                }
            }
        }
    }
    /**
     * 保证金违约后退款
     */
    private function pay_margin_default_state_2(){
        $model_margin_order = Model('margin_orders');
        $map['default_state']=1;
        $map['order_state']=1;
        $map['lock_state']=1;
        $map['refund_state']=array('in','0,2');
        $list=Model("")->table("margin_orders")->where($map)->limit(1000)->select();
        //print_r($list);
        foreach ($list as $key => $value) {
            if ($value['rufund_money']>0 && $value['refund_state']==2) {
                //部分退款
                $model_pd = Model('predeposit');
                //解冻充值卡
                $rcb_amount = $value['rufund_money'] - $value['api_pay_amount'];
                if ($rcb_amount > 0) {
                    $data_pd = array();
                    $data_pd['member_id'] = $value['buyer_id'];
                    $data_pd['member_name'] = $value['buyer_name'];
                    $data_pd['amount'] = $rcb_amount;
                    $data_pd['order_sn'] = $value['order_sn'];
                    $model_pd->changePd('order_cancel2', $data_pd);
                }else{
                   $rcb_amount=0; 
                }
                if ($value['payment_code']=="wxpay_jsapi" || $value['payment_code']=="wxpay_mini" || $value['payment_code']=="yinuovip_mini") {
                    $refund_money=$value['rufund_money']-$rcb_amount;;
                    if ($refund_money>0) {
                        $value['refund_money']=$refund_money;
                        $this->_wxpay($value);
                    }                    
                }
                $result2['refund_state']=1;
                $re = $model_margin_order->editOrder($result2,array('margin_id'=>$value['margin_id']));

            }else{
                if($value['refund_state']==0){
                    //全部退款
                    $model_pd = Model('predeposit');
                    //解冻充值卡
                    $rcb_amount = $value['pd_amount'];
                    if ($rcb_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $value['buyer_id'];
                        $data_pd['member_name'] = $value['buyer_name'];
                        $data_pd['amount'] = $rcb_amount;
                        $data_pd['order_sn'] = $value['order_sn'];
                        $model_pd->changePd('order_cancel2', $data_pd);
                    }
                    if ($value['payment_code']=="wxpay_jsapi" || $value['payment_code']=="wxpay_mini" || $value['payment_code']=="yinuovip_mini") {
                        //$refund_money=$value['rufund_money']-$rcb_amount;;
                        if ($value['api_pay_amount']>0) {
                            $value['refund_money']=$value['api_pay_amount'];
                            $this->_wxpay($value);
                        }                    
                    }
                    $result2['refund_state']=1;
                    $re = $model_margin_order->editOrder($result2,array('margin_id'=>$value['margin_id']));
                }
            }
        }
    }

    /**
     * 保证金 违约状态订单 微信退款
     */
    public function wxBreachRefundOp(){
        if(empty($_SERVER['argv'][3])){
            die("缺少参数");
        }
        $model_margin_order = Model('margin_orders');
        $map['default_state']=1;
        $map['order_state']=1;
        $map['lock_state']=1;
        $map['refund_state']=array('in','0,2');
        $map['order_sn'] = $_SERVER['argv'][3];
        $map['payment_code'] = 'wxpay_jsapi';
        $apiRefundMsgSuccess = '手动保证金微信违约退款:';
        if(!empty($_SERVER['argv'][4])){
            $apiRefundMsgSuccess = $_SERVER['argv'][4];
        }
        $value=Model("")->table("margin_orders")->where($map)->find();
        if (empty($value) || $value['api_pay_amount']<0) {
            die("此单无需退款");
        }
        $model_margin_order->beginTransaction();
        $value['refund_money']=$value['api_pay_amount'];
        try{
        echo "-----------开始退款order_sn:{$value['order_sn']}.------------\n";
        $this->_wxpay($value, $apiRefundMsgSuccess);
        echo "-----------结束退款order_sn:{$value['order_sn']}.------------\n";
        }catch (\Exception $e){
            $model_margin_order->rollback();
            die($e->getMessage());
        }
        $model_margin_order->commit();
        die("成功");
    }
    /**
     * 拍卖违约,保证金扣除
     */
    private function check_margin_order_lock(){
        $model_order = Model('order');
        $model_margin_order = Model('margin_orders');
        $order_map['order_state']=10;
        $order_map['refund_state']=0;
        $order_map['add_time']=array('elt',time()-3*24*60*60);
        $list=$model_order->table("orders")->where($order_map)->limit(1000)->select();
        print_r($order_map);
        print_r($list);
        $logic_auction_order = Logic('auction_order');
        if (!empty($list)) {
            foreach($list as $order){
                $model_margin_order->beginTransaction();
                try{                    
                    $margin_order_list=$model_margin_order->where(array('auction_id'=>$order['auction_id'],'buyer_id'=>$order['buyer_id'],'refund_state'=>0,'lock_state'=>1))->order("api_pay_amount DESC,margin_amount DESC")->select(); 
                    //print_r($margin_order_list);
                    $pay_money=floatval($order['order_amount']/2);
                    $member_model=Model('member');
                    $update_member['default_amount'] = array('exp','default_amount+'.$pay_money);
                    $member_model->editMember(array('member_id'=>$order['buyer_id']),$update_member);
                    foreach ($margin_order_list as $key => $value) {
                        $return_amount = $value['margin_amount']-$pay_money;
                        $pay_money=$pay_money-$value['margin_amount'];
                        if ($pay_money<0) {
                            $update_margin_order=array();
                            //部分退款
                            $update_margin_order['rufund_money']=$return_amount;//涉及部分退款的金额                         
                            $update_margin_order['updated_at']=time();
                            //需要部分退款
                            $update_margin_order['refund_state']=2;
                            $update_margin_order['default_state']=1;
                            $update_margin_order['update_margin_order'];
                            $re2=Model("margin_orders")->where(array('margin_id'=>$value['margin_id']))->update($update_margin_order);

                            $logic_auction_order->MarginLog($order['buyer_id'], $order['buyer_name'], 2, $return_amount, $order['order_sn']);
                            //退出循环
                            break;
                        }else{
                            $update_margin_order=array();
                            //全额抵扣
                            $update_margin_order['refund_state']=2;
                            $update_margin_order['default_state']=1;
                            $update_margin_order['updated_at']=time();
                            $re2=Model("margin_orders")->where(array('margin_id'=>$value['margin_id']))->update($update_margin_order);
                            $logic_auction_order->MarginLog($order['buyer_id'], $order['buyer_name'], 2, $value['margin_amount'], $order['order_sn']);
                        }
                        echo $pay_money;
                    }
                    //解放剩余保证金
                    $re1 = $model_margin_order->editOrder(array('lock_state'=>1,'default_state'=>1),array('auction_id'=>$order['auction_id'],'buyer_id'=>$order['buyer_id'],'refund_state'=>0,'lock_state'=>1));
                    $update_order['order_state']=0;
                    $model_order->table("orders")->where($order_map)->update($update_order);
                    $model_margin_order->commit();
                    $url = AUCTION_OLD_URL."/wap/tmpl/member/margin_details_wap2.html?auction_id=".$order['auction_id']; 
                    $url = filterUrl($url); 
                    $short = sinaShortenUrl($url); 
                    $param = array();
                    $param['code'] = 'default_margin';
                    $param['member_id'] = $order['buyer_id'];
                    $param['number']['mobile'] = $order['buyer_phone'];
                    $param['param'] = array(
                        'auction_name' => $order['auction_name'],
                        'money' => floatval($order['order_amount']/2),
                        'url' => $short,
                        'wx_url' => $short
                    );
                    if ($return_amount > 0) {
                        //退款金额大于零, 部分退款
                        $param['param']['type'] = 1;
                        $param['param']['return_amount'] = $return_amount;
                    } else {
                        //全额退款
                        $param['param']['type'] = 2;
                    }
                    QueueClient::push('sendMemberMsg', $param);
                }catch(Exception $e){
                    $model_margin_order->rollback();
                    file_put_contents(BASE_DATA_PATH.'/log/auction_default.log',"margin_id:{$order['margin_id']},error:{$e->getMessage()}\r\n",FILE_APPEND);
                }
            }

        }
    }

}
