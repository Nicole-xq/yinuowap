<?php
/**
* 任务计划 - 分钟执行的任务
* 执行频率1-2分钟
*
*
*
* @copyright  Copyright (c) 2007-2017 ShopNC Inc. (http://www.shopnc.net)
* @license    http://www.shopnc.net
* @link       http://www.shopnc.net
* @since      File available since Release v1.1
*/
defined('InShopNC') or exit('Access Invalid!');

/**
 * Class auctions_realtime2Control
 * @deprecated 已经弃用
 */
class auctions_realtime2Control extends BaseCronControl {
    public function indexOp() {
        //拍卖商品状态计划任务
        $this->_auctions_liupai();
        $this->_auctions_end();
        $this->_auctions_order();
        //订单返佣处理
        $this->_bill_commission();

    }

    /**
     * 流拍处理
     */
    private function _auctions_liupai() {
        die;
        //echo TIMESTAMP;
        $model_auctions = Model('auctions');
        $model_goods_auctions = Model('goods_auctions');
        $condition = array();
        //$condition['auction_id'] = array('lt'=>'4226');
        $condition['is_liupai'] = 0;
        $condition['bid_number'] = 0;
        $condition['state'] = 0;
        $condition['auction_end_time'] = array('elt',TIMESTAMP);
        $list = $model_auctions->getAuctionList($condition);
        print_r($list);
        try {
            $model_auctions->beginTransaction();
            if(!empty($list) && is_array($list)) {
                foreach($list as $k => $v) {
                    $auction_id = $v['auction_id'];
                    if ($auction_id<4226) { 
                        $model_goods_auctions->updateGoodsAuctions(array('cur_special_id'=>0,'last_special_id'=>$v['special_id'],'auction_state'=>0),array('goods_id'=>$v['goods_id']));
                        $this->margin_refund($auction_id,$v);
                        $this->_update_margin_order($auction_id);
                        $update = array();
                        $update['is_liupai'] = 1;
                        $update['state'] = 1;
                        $model_auctions->table('auctions')->where(array('auction_id' => $auction_id))->update($update);
                    }
                }
            }
            $model_auctions->commit();
        }catch (Exception $e){
            $model_auctions->rollback();
        }
    }

    /**
     * 结束处理
     */
    private function _auctions_end() {
        die;
        $model_goods_auctions = Model('goods_auctions');
        $model_auctions = Model('auctions');
        $condition_sql = "auction_id < 4226 and state = 0 and bid_number > 0 and (auction_end_time <= '".TIMESTAMP."' or auction_end_true_t <= '".TIMESTAMP."')";
        $list = $model_auctions->table('auctions')->where($condition_sql)->select();
        print_r($list);
        try {
            $send_param_array = array();
            $model_auctions->beginTransaction();
            if(!empty($list) && is_array($list)) {
                $model_relation = Model('auction_member_relation');
                $model_margin_orders = Model('margin_orders');
                $model_bid_log = Model('bid_log');
                foreach($list as $k => $v) {
                    $auction_id = $v['auction_id'];
                    Model('robot')->updateInfo(array('auction_id'=>$auction_id),array('auction_id'=>0));
                    $end_time = $v['auction_end_time'];
                    $current_price = $v['current_price'];
                    $reserve_price = $v['auction_reserve_price'];
                    if ($v['auction_end_true_t']) $end_time = $v['auction_end_true_t'];
                    if ($end_time<=TIMESTAMP) {
                        $send_messages_member_id = 0;
                        if ($current_price<$reserve_price) {
                            $condition = array();
                            $condition['auction_id'] = $auction_id;
                            $condition['is_agent'] = 1;
                            $condition['agent_num'] = array('egt',$current_price);
                            $agent = $model_relation->table('auction_member_relation')->where($condition)->order('agent_num desc,relation_id asc')->find();
                            if(!empty($agent) && is_array($agent) && $agent['member_id'] > 0) {
                                $send_messages_member_id = $agent['member_id'];
                                $update = array(
                                    'is_offer' => 1,
                                    'offer_num' => $reserve_price
                                );
                                $result = $model_relation->setRelationInfo($update, array('relation_id' => $agent['relation_id']));
                                if ($result) {
                                    $model_margin_orders->table('margin_orders')->where(array('auction_id'=> $auction_id,'buyer_id'=> $agent['member_id']))->update(array('lock_state'=> 1,'finnshed_time'=> TIMESTAMP));
                                    $update = array(
                                        'current_price' => $reserve_price,
                                        'bid_number' => array('exp', 'bid_number+1'),
                                        'current_price_time' => $end_time
                                    );
                                    $result = $model_auctions->editAuctions($update, array('auction_id' => $auction_id));
                                    $param = array(
                                        'member_id' => $agent['member_id'],
                                        'auction_id' => $auction_id,
                                        'created_at' => $end_time,
                                        'offer_num' => $reserve_price,
                                        'member_name' => $agent['member_name'],
                                        'is_anonymous' => $agent['is_anonymous']
                                    );
                                    $model_bid_log->addBid($param);
                                }
                            }
                        } else {
                            $member_relation = $model_relation->table('auction_member_relation')->where(array('auction_id'=> $auction_id,'is_offer'=> 1))->order('offer_num desc')->find();
                            $current_member_id = $member_relation['member_id'];
                            if(!empty($member_relation) && $current_member_id) {
                                $model_margin_orders->table('margin_orders')->where(array('auction_id'=> $auction_id,'buyer_id'=> $member_relation['member_id']))->update(array('lock_state'=> 1,'finnshed_time'=> TIMESTAMP));
                                $send_messages_member_id = $current_member_id;
                            }
                        }
                        $condition = array('auction_id' => $auction_id, 'is_offer' => 1);
                        $member_relation_list = $model_relation->getRelationList($condition,'*','','offer_num desc');
                        foreach ($member_relation_list as $key => $val) {
                            $send_desc = '未能竞拍成功';
                            if($send_messages_member_id > 0 && $val['member_id'] == $send_messages_member_id){
                                $send_desc = '竞拍成功';
                            }
                            $send_param_array[] = array(
                                'code'      => 'auction_end_notice',
                                'member_id' => $val['member_id'],
                                'param'     => array(
                                    'auction_name'  => $v['auction_name'],
                                    'desc'          => $send_desc,
                                    'end_time'      => date('Y-m-d H:i:s',$end_time),
                                    'auction_bond'  => $val['bond_num'],
                                ),
                                'number'     => array('mobile'=>$val['member_mobile']),
                            );
                        }
                        $this->margin_refund($auction_id,$v);
                        $this->_update_margin_order($auction_id);
                        $model_auctions->editAuctions(array('auction_end_true_t' => $end_time,'state'=> 1), array('auction_id' => $auction_id));
                        $model_goods_auctions->updateGoodsAuctions(array('cur_special_id'=>0,'last_special_id'=>$v['special_id'],'auction_state'=>0),array('goods_id'=>$v['goods_id']));
                    }
                }
            }
            $model_auctions->commit();
            if(!empty($send_param_array)){
                foreach ($send_param_array as $key => $val){
                    QueueClient::push('sendMemberMsg', $val);
                }
            }
        }catch (Exception $e){
            $model_auctions->rollback();
        }
    }

    /**
     * 订单处理
     */
    private function _auctions_order() {
        die;
        $model_auctions = Model('auctions');
        $model_margin_orders = Model('margin_orders');
        $model_order = Model('order');
        $model_goods_auction = Model('goods_auction');
//        $model_auction_order = Model('auction_order');
//        $store_bind_class = Model('store_bind_class');
        $list = $model_margin_orders->table('margin_orders')->where(array('order_state'=> 1,'lock_state'=> 1,'auction_order_id'=> 0))->select();
//      
        print_r($list);
        try {
            $model_auctions->beginTransaction();
            if(!empty($list) && is_array($list)){
                foreach($list as $k=>$v){
                    $auction_id = $v['auction_id'];
                    if($auction_id<4226){
                        $auction_info = $model_auctions->table('auctions')->where(array('auction_id'=> $auction_id))->find();
                        $pay_sn = Logic('buy_1')->makePaySn($v['buyer_id']);
                        $order_pay = array();
                        $order_pay['pay_sn'] = $pay_sn;
                        $order_id_store = [];
                        $order_pay['buyer_id'] = $v['buyer_id'];
                        /** @var orderModel $model_order */
                        $order_pay_id = $model_order->addOrderPay($order_pay);
                        if(!$order_pay_id){
                            throw new Exception('订单保存失败[未生成支付单]');
                        }

                        $promotion_sum = 0;

                        $order = array();
                        $order_common = array();
                        $order_goods = array();
                        $order['order_sn'] = Logic('buy_1')->makeOrderSn($order_pay_id);
                        $order['pay_sn'] = $pay_sn;
                        $order['store_id'] = $v['store_id'];
                        $order['store_name'] = $v['store_name'];
                        $order['buyer_id'] = $v['buyer_id'];
                        $order['buyer_name'] = $v['buyer_name'];
                        $order['add_time'] = TIMESTAMP;
                        $order['payment_code'] = 'online';
                        $order['order_state'] = 10;
                        $order['order_amount'] = $auction_info['current_price'];
                        $order['goods_amount'] = $auction_info['current_price'];
                        $order['order_from'] = $v['order_from'];
                        $order['order_type'] = 4;
                        $order['auction_id'] = $auction_id;
                        $order['margin_amount'] = $v['margin_amount'];
                        $order_id = $model_order->addOrder($order);
                        if ($order_id){
                            $order_goods = array(
                                'order_id'=>$order_id,
                                'goods_id'=>$auction_info['goods_id'],
                                'goods_name'=>$auction_info['auction_name'],
                                'goods_price'=>$auction_info['current_price'],
                                'goods_image'=>$auction_info['auction_image'],
                                'goods_pay_price'=>$auction_info['current_price'],
                                'store_id'=>$v['store_id'],
                                'buyer_id'=>$v['buyer_id'],
                                'goods_commonid'=>$auction_info['goods_common_id'],
                                'add_time'=>time(),
                                'is_special'=>1
                            );
                            $insert = $model_order->addOrderGoods(array($order_goods));
                            if (!$insert) {
                                throw new Exception('订单保存失败[未生成商品数据]');
                            }
                            $re = $model_margin_orders->table('margin_orders')->where(array('margin_id' => $v['margin_id']))->update(array('auction_order_id'=> $order_id));
                            if(!$re){
                                echo 4;
                                throw new Exception('订单创建失败');
                            }
                        }else{
                            echo 5;
                            throw new Exception('订单创建失败');
                        }
                    }
                }
            }
            $model_auctions->commit();
        }catch (Exception $e){
            $model_auctions->rollback();
        }
    }

    /**
     * 保证金处理
     */
    private function margin_refund($auction_id,$auction_info) {
        if ($auction_id<4226) {
            $model_margin_orders = Model('margin_orders');
            $model_member = Model('member');
            $model_predeposit = Model('predeposit');
            $list = $model_margin_orders->table('margin_orders')->where(array('auction_id' => $auction_id,'order_state'=> 1,'refund_state'=> 0))->select();
            if(!empty($list) && is_array($list)) {

                /** @var j_member_distributeLogic $member_distribute_logic */
                $member_distribute_logic = Logic('j_member_distribute');
                $_time3 = 3600*24*3;//拍卖结束前3天
                $end_time = $auction_info['auction_end_time'];//结束时间
                $auction_bond_rate = $auction_info['auction_bond_rate'];
                foreach($list as $k => $v) {
                    $margin_amount = $v['margin_amount'];
                    $tmp_re = array();
                    if($auction_info['auction_type'] == 1){
                        $condition = array(
                            'buyer_id'=>$v['buyer_id'],
                            'order_state'=>array('neq','4'),
                            'margin_id'=>array('neq',$v['margin_id'])
                        );
                        $tmp_re = Model('margin_orders')->getOrderInfo($condition);
                    }
                    if ($margin_amount>0&&($auction_info['auction_type'] != 1 || empty($tmp_re))) {
                        $payment_time = $v['payment_time'];//支付(付款)时间
                        if(strtotime($auction_info['interest_last_date']) > $payment_time){
                            $day_num = Logic('auction')->getInterestDay($payment_time,$auction_info['auction_end_time']);//计息天数
                        }else{
                            $day_num = 0;
                        }


                        //计算保证金利息
                        $member_distribute_logic->pay_margin_refund($v, $day_num, $auction_bond_rate);
                        //计算保证金返佣
                        $member_distribute_logic->pay_top_margin_refund_bak($v, $day_num);
                        //添加区域代理佣金待结算记录
                        $member_distribute_logic->agent_margin_refund($v, $day_num);
                    }
                    if ($v['lock_state'] == 0 ) {//买家未中拍返还保证金
                        Logic('auction_order')->MarginLog($v['buyer_id'], $v['buyer_name'], 2, $v['margin_amount'], $v['order_sn']);
                        $model_pd = Model('predeposit');
                        $data_pd['member_id'] = $v['buyer_id'];
                        $data_pd['member_name'] = $v['buyer_name'];
                        $data_pd['amount'] = $v['margin_amount'];
                        $data_pd['order_sn'] = $v['order_sn'];
                        $model_pd->changePd('margin_to_pd',$data_pd);
                    }
                    if ($v['lock_state'] == 0 ){
                        $update_margin = array(
                            'refund_state'=> 1,
                            'pay_date_number'=> $day_num,
                            'finnshed_time'=> TIMESTAMP
                        );
                        $model_margin_orders->table('margin_orders')->where(array('margin_id' => $v['margin_id']))->update($update_margin);
                    }
                }
            }
        }
    }

    /**
     * 返佣结算
     */
    private function _bill_commission(){
        die;
        $condition = array();
        $condition['commission_state'] = 0;
        $condition['order_state'] = 40;
        $condition['finnshed_time'] = array('lt',time() - 120);
        $condition['order_type'] = 4;
//        $condition['auction_id'] = array('neq','0');
        $condition['auction_id'] = array(array('neq','0'),array('lt','4226'));
        $auction_order = (array)Model()->table('orders')->field('order_id')->key('order_id')->where($condition)->limit(1000)->select();

        foreach($auction_order as $order){
            $this->handleOrder($order['order_id']);
        }
    }

    //处理单个订单返佣
    private function handleOrder($order_id = 0){
        if($order_id > 0){
            //保存余额变更日志
            $pdModel = Model('predeposit');
            $deal_list = (array)Model()->table('member_commission')->where(array('order_id' => $order_id,'commission_type'=>2,'dis_commis_state'=>0))->select();
            $member_model = Model('member');
            $flag = 0;
            foreach($deal_list as $deal){
                $update_member = array();
                $update_member['available_commis'] = array('exp','available_commis+'.$deal['commission_amount']);
                $update_member['freeze_commis'] = array('exp','freeze_commis-'.$deal['commission_amount']);
                $update_member['available_predeposit'] = array('exp','available_predeposit+'.$deal['commission_amount']);
                $res = $member_model->editMember(array('member_id'=>$deal['dis_member_id']),$update_member);
                if($res){
                    $flag++;
                    Model()->table('member_commission')->where(array('log_id'=>$deal['log_id']))->update(array('dis_commis_state'=>1,'commission_time'=>time()));
                }
                //做日志使用
                $deal['lg_available_amount'] = !empty($update_member)?$update_member['available_predeposit']:'0.00';
                $pdModel->insertPdLog($deal);
            }
            if($flag == count($deal_list)){
                Model()->table('orders')->where(array('order_id'=>$order_id))->update(array('commission_state'=>1));
            }
        }
    }

    /*
     * 拍品状态执行计划任务
     * */

    /**
     * 拍卖预展
     * */
    private function _cron_11()
    {
        $model_auction = Model('auctions');
        $condition = array(
            'auction_lock' => 0,
            'state' => 1,
            'auction_preview_start' => array(
                'lt', time()
            )
        );
        $update = array('state' => 3);
        // 更新成预展状态
        $model_auction->where($condition)->update($update);
    }

    /**
     * 拍卖开始
     * */
    private function _cron_12()
    {
        $model_auction = Model('auctions');
        $condition = array(
            'auction_lock' => 0,
            'state' => 3,
            'auction_start_time' => array(
                'lt', time()
            )
        );
        // 更新成预展状态
        $update = array('state' => 4);
        // 更新成预展状态
        $model_auction->where($condition)->update($update);
    }

    /**
     * 保证金订单取消
     */
    private function _update_margin_order($auction_id){
        if ($auction_id<4226) {
            $model_margin_order = Model('margin_orders');
            $model_margin_order->editOrder(array('order_state'=>4),array('order_state'=>0,'auction_id'=>$auction_id));

            $list = $model_margin_order->getOrderList(array('order_state'=>3,'auction_id'=>$auction_id));
            if(!empty($list)){
                foreach($list as $order_info){
                    try{
                        $model_margin_order->beginTransaction();
                        $model_pd = Model('predeposit');
                        //解冻充值卡
                        $rcb_amount = floatval($order_info['rcb_amount']);
                        if ($rcb_amount > 0) {
                            $data_pd = array();
                            $data_pd['member_id'] = $order_info['buyer_id'];
                            $data_pd['member_name'] = $order_info['buyer_name'];
                            $data_pd['amount'] = $rcb_amount;
                            $data_pd['order_sn'] = $order_info['order_sn'];
                            $model_pd->changeRcb('order_cancel', $data_pd);
                        }

                        //解冻预存款
                        $pd_amount = floatval($order_info['pd_amount']);
                        if ($pd_amount > 0) {
                            $data_pd = array();
                            $data_pd['member_id'] = $order_info['buyer_id'];
                            $data_pd['member_name'] = $order_info['buyer_name'];
                            $data_pd['amount'] = $pd_amount;
                            $data_pd['order_sn'] = $order_info['order_sn'];
                            $model_pd->changePd('order_cancel', $data_pd);
                        }
                        //解冻积分
                        $points_amount = floatval($order_info['points_amount']);
                        if ($points_amount > 0) {
                            $nuobi_points = $points_amount * 100;
                            Model('points')->savePointsLog('cancel', array('pl_memberid' => $_SESSION['member_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                        }
                        $re = $model_margin_order->editOrder(array('order_state'=>4),array('margin_id'=>$order_info['margin_id']));
                        if(!$re){
                            throw new Exception('失败');
                        }
                        $model_margin_order->commit();
                    }catch (Exception $e){
                        $model_margin_order->rollback();
                    }
                }
            }
        }
    }

}
