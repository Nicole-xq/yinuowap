<?php
/**
* 任务计划 - 分钟执行的任务
* 执行频率1-2分钟
*
*
*
* @copyright  Copyright (c) 2007-2017 ShopNC Inc. (http://www.shopnc.net)
* @license    http://www.shopnc.net
* @link       http://www.shopnc.net
* @since      File available since Release v1.1
*/
defined('InShopNC') or exit('Access Invalid!');

class scriptsControl extends BaseCronControl {
    /**
     * 保证金 违约状态订单 微信退款
     */
    public function wxBreachRefundOp(){
        if(empty($_SERVER['argv'][3]) || empty($_SERVER['argv'][4]) || empty($_SERVER['argv'][5])){
            die("缺少参数");
        }
        $apiRefundMsgSuccess = '手动微信退款';
        if(!empty($_SERVER['argv'][6])){
            $apiRefundMsgSuccess .= ":".$_SERVER['argv'][6];
        }
        $trade_no = $_SERVER['argv'][3];
        $total_fee = $_SERVER['argv'][4];
        $refund_fee = $_SERVER['argv'][5];
        $model_payment = Model('mb_payment');
        $payment_info = $model_payment->getMbPaymentInfo(['payment_code' => 'wxpay_jsapi']);//接口参数
        $payment_config = $payment_info['payment_config'];
        define('WXPAY_APPID', $payment_config['appId']);
        define('WXPAY_MCHID', $payment_config['partnerId']);
        define('WXPAY_KEY', $payment_config['apiKey']);
        try{
            echo "-----------开始退款:" . implode($_SERVER['argv'], ",") . ".------------\n";
            $api_file = BASE_PATH.DS.'api'.DS.'refund'.DS.'wxpay'.DS.'WxPay.Api.php';
            include_once $api_file;
            $input = new WxPayRefund();
            $input->SetOut_trade_no($trade_no);//商户ssss订单号
            $input->SetTotal_fee($total_fee);
            $input->SetRefund_fee($refund_fee);
            $input->SetOut_refund_no(date('YmdHis').$trade_no);//退款批次号
            $input->SetOp_user_id(WxPayConfig::MCHID);
            $input->setRefund_desc($apiRefundMsgSuccess);
            $data = WxPayApi::refund($input);
            echo "-----------结束退款:" . json_encode($data, JSON_UNESCAPED_UNICODE) ."------------\n";
        }catch (\Exception $e){
            die($e->getMessage());
        }
    }
}
