<?php
/**
 * 机器人出价服务
 * 拍品开始时启动
 */
defined('InShopNC') or exit('Access Invalid!');

class workerRobotControl extends BaseCronControl {
    private $auctionId;
    private $lockRandomValue;
    /**
     * 默认方法
     */
    public function indexOp() {
        $this->auctionId = $_SERVER['argv'][2];
        $this->lockRandomValue = $_SERVER['argv'][3];
        if(empty($this->auctionId) || empty($this->lockRandomValue)){
            return;
        }
//        register_shutdown_function()
        //初始化
        $this->_init();
        //机器人循环出价
        $this->_robot_chujia();
    }

    /**
     * 初始化拍品
     */
    private function _init(){
        $rand = rand(3,5);
        //随机增加个数
        Model('auctions')->editAuctions(array('num_of_applicants'=>['exp', '`num_of_applicants`+' . $rand]),array('auction_id'=>$this->auctionId));
        //设置好级别信息

    }

    private function _robot_chujia(){
        $maxRunTime = \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_ROBOT_RUNTIME_EXPIRES;
        $startTime = time();
        while (true) {
            //超时
            if((time() - $startTime) >= $maxRunTime){
                $this->shutdown();
            }
            $auction = Model('auctions')->where(["auction_id" => $this->auctionId])
                ->field('auction_id, current_price, auction_increase_range, auction_end_time, auction_end_true_t, auction_name, auction_reserve_price')
                ->find();

            //已结束
            if(!$auction || ($auction['auction_end_true_t'] > 0 && $auction['auction_end_true_t'] <= time())){
                $this->shutdown();
            }
            //已到保留价
            if($auction['current_price']>=$auction['auction_reserve_price']){
                $this->shutdown();
            }
            //出价
            $this->_common($auction);
            //时间频率 0.1 - 5 s
//            $uSleepMicroseconds = mt_rand(100000, 5000000);
            $uSleepMicroseconds = mt_rand(2000000, 40000000); // 2 - 40 s
            usleep($uSleepMicroseconds);
        }
    }
    //退出
    public function shutdown(){
        //解运行时锁
        \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock(\Yinuo\Entity\Constant\RedisKeyConstant::LOCK_ROBOT_RUNTIME.$this->auctionId, $this->lockRandomValue);
        exit();
    }
    //获得机器人
    private function getRobot(){
        //随机头像个数
        $avatarImgNum = 50;
        $robotMobile = '176';
        for($i = 0;$i <8;$i++){
            $robotMobile .= rand(0,9);
        }
        $typeNames = [
            'wdl','zl','sy','hy','dz'
        ];
        $avatar = "http://cnd.static.yinuovip.com/shop/robotAvatar/20190518_"  . crc32($robotMobile) % $avatarImgNum . ".png";
        return [
            'robotMobile' => $robotMobile,
            'avatar' => $avatar,
            //0.1
            'memberRate' => $this->getMemberRateConfig($typeNames[array_rand($typeNames)])
        ];
    }
    //获得基本配置信息 0.1
    private function getMemberRateConfig($type){
        static $configs = [];
        if (isset($configs[$type])){
            return $configs[$type];
        }
        $member_rate_config = Model('member_rate_config');
        //给自己返佣比率

        $mine_where = ['type' => $type];
        $mine_bid_rate_info = $member_rate_config->where($mine_where)->field("bid_rate")->find();
        $mine_bid_rate = isset($mine_bid_rate_info['bid_rate']) ? $mine_bid_rate_info['bid_rate']/100 : 0;
        return $configs[$type] = $mine_bid_rate;
    }
    //出价
    private function _common($auction){
        $uSleepCount = 0;
        $lockKey = \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_AUCTION_MEMBER_OFFER.$auction['auction_id'];
        do{
            $myRandomValue = \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->lock($lockKey, 8);
            if($myRandomValue === false){
                //0.01 - 0.5 s
                $uSleepMicroseconds = mt_rand(10000, 500000);
                usleep($uSleepMicroseconds);
            }
            $uSleepCount ++;
        }while($myRandomValue === false && $uSleepCount < 3);
        //没有获得锁
        if($myRandomValue === false){
            return false;
        }
        $auction = Model('auctions')->getAuctionsInfo(array('auction_id'=>$auction['auction_id']),
            "auction_id, current_price, auction_increase_range, auction_end_time,
             auction_name, auction_start_time, auction_start_price");
        $current_price = !empty($auction['current_price']) ? $auction['current_price'] : $auction['auction_start_price'];
        $auction_increase_range = $auction['auction_increase_range'] <= 0 ? 1 : $auction['auction_increase_range'];
        //该拍品机器人第二次出价为 1到2 倍加附价
        $robot_bid_num = Model('')->table('bid_log')
            ->where(['auction_id' => $auction['auction_id'], 'member_id' => 0])
            ->count();
        if ($robot_bid_num == 1) {
            $auction_increase_range = $auction_increase_range * mt_rand(1, 2);
        } else {
            $auction_increase_range = $auction_increase_range * mt_rand(1, 3);
        }
        $current_price += $auction_increase_range;

        // 获取到当前价最高的出价人
        $bid_log_model = Model('bid_log');
        $re = $bid_log_model->getBidList(array('auction_id'=>$auction['auction_id']),'member_id,member_name','','offer_num desc',1);
        //上一个出价用户
        $lastMemberId = $re[0]['member_id'];
        //获得机器人
        $robot = $this->getRobot();
        //出价时间
        $bidCreatedAt = time();
        //第一口出价时间控制在$auction['auction_start_time'] + 1上
        if($bidCreatedAt <= $auction['auction_start_time']){
            //第一口出价时间控制在$auction['auction_start_time'] + 1上
            sleep($auction['auction_start_time'] - $bidCreatedAt + 1);
            $bidCreatedAt = time();
        }
        // 添加出价日志
        $commission_amount= $auction_increase_range * $robot['memberRate'];
        $param = array(
            'member_id' => 0,
            'auction_id' => $auction['auction_id'],
            'created_at' => $bidCreatedAt,
            'offer_num' => $current_price,
            'member_name' => $robot['robotMobile'],
            'is_anonymous' => 0,
            'commission_amount'=>$commission_amount,
            'member_avatar'=>$robot['avatar'],
        );
        $model_auctions = Model('auctions');
        try {
            //开启事务,开始写记录
            $model_auctions->beginTransaction();
            //记入出价记录
            if(!Model('bid_log')->addBid($param)){
                throw new Exception('数据库更新失败!');
            }
            $auction_config=Model("")->table("auction_config")->where(array('id'=>1))->find();
            //更新拍品数据
            $update = array(
                'current_price' => $current_price,
                'bid_number' => array('exp', 'bid_number + 1'),
                'current_price_time' => time(),
                'num_of_applicants' => array('exp', 'num_of_applicants+1'),
                'real_participants' => array('exp', 'real_participants + 1')
            );
            $check_time4=time()+$auction_config['add_time']*60;
            if ($auction['auction_end_time']<$check_time4) {
                $update['auction_end_time']=$check_time4;
                $update['auction_end_true_t']=$check_time4;
            }
            if(!$model_auctions->editAuctions($update, array('auction_id' => $auction['auction_id']))){
                throw new Exception('数据库更新失败!');
            }
            $model_auctions->commit();
            //解锁
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
        }catch (Exception $e){
            //解锁
            \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->unLock($lockKey, $myRandomValue);
            $model_auctions->rollback();
            yLog()->error("robot_bid_exception", [$auction, \Yinuo\Lib\Helpers\LogHelper::getEInfo($e)]);
            //操作失败
            return false;
        }
        //操作成功 提醒前一个用户竞价被超越
        if (!empty($lastMemberId)) {
            $sendParam = [
                'member_id' => $lastMemberId,
                'mobile' => $robot['robotMobile'],
                'current_price' => $current_price
            ];
            Model('member_msg_tpl')->sendBeyondPriceMessage($sendParam, $auction);
        }
        return true;
    }
}