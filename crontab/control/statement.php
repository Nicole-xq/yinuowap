<?php
/**
 * 区域代理对账单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class statementControl extends BaseCronControl {
    public $assessment_config;
    /**
     * 默认方法
     */
    public function indexOp() {
        $this->_get_assessment_config();
        $this->_create_statement();

    }


    /**
     *创建对账单
     */
    private function _create_statement(){
        $model_agent = Model('area_agent');
        $model_agent_order = Model('agent_order');
        $model_agent_statement = Model('agent_statement');
        $agent_arr = $model_agent->getAreaAgentList(array('agent_state'=>1));
        $month_start = date('Y-m-01 00:00:00',strtotime('-1month'));
        $month_end = date('Y-m-01 00:00:00');
        foreach($agent_arr as $agent){
            $key = $agent['area_type'].'_'.$agent['commission_lv'];
            $condition1 = array(
                'agent_id'=>$agent['agent_id'],
                'commission_type'=>array('in','1,2'),
                'commission_status'=>1,
                'finish_time'=>array('between',"{$month_start},{$month_end}")
            );
            $condition2 = array(
                'agent_id'=>$agent['agent_id'],
                'commission_type'=>3,
                'commission_status'=>1,
                'finish_time'=>array('between',"{$month_start},{$month_end}")
            );
            $condition3 = array(
                'agent_id'=>$agent['agent_id'],
                'commission_type'=>5,
                'commission_status'=>1,
                'finish_time'=>array('between',"{$month_start},{$month_end}")
            );
            $condition4 = array(
                'agent_id'=>$agent['agent_id'],
                'commission_type'=>4,
                'commission_status'=>1,
                'finish_time'=>array('between',"{$month_start},{$month_end}")
            );
            $goods = $model_agent_order->where($condition1)->sum('order_price');
            $margin = $model_agent_order->where($condition2)->sum('order_price');
            $artist = $model_agent_order->where($condition3)->count();
            $partner = $model_agent_order->where($condition4)->count();
            $param = array(
                'last_bzj_content'=>$goods >=0?$goods:0,
                'last_goods_content'=>$margin >= 0?$margin:0,
                'last_artist_content'=>$artist
            );
            $rate_arr = $this->_get_rate($key,$param);
            $goods_amount = $model_agent_order->where($condition1)->sum('commission_amount');
            $margin_amount = $model_agent_order->where($condition2)->sum('commission_amount');
            $artist_amount = $model_agent_order->where($condition3)->sum('commission_amount');
            $partner_amount = $model_agent_order->where($condition4)->sum('commission_amount');
            $total_amount = ceil($goods_amount*($rate_arr['last_goods_content']/100)) + ceil($margin_amount*($rate_arr['last_bzj_content']/100)) + ceil($artist_amount*($rate_arr['last_artist_content']/100)) + $partner_amount;
            $no = "MS".date('Ym');
            for($i = 0;$i<5-strlen($agent['agent_id']);$i++){
                $no .= 0;
            }
            $no .= $agent['agent_id'];
            $insert_data = array(
                'agent_id'=>$agent['agent_id'],
                'statement_no'=>$no,
                'title'=>'《'.date('Y年m月',strtotime('-1month')).'》月度对账单',
                'goods_amount'=>$goods_amount !=''?$goods_amount:'0',
                'goods_rate'=>$rate_arr['last_goods_content'],
                'margin_amount'=>$margin_amount !=''?$margin_amount:'0',
                'margin_rate'=>$rate_arr['last_bzj_content'],
                'artist_amount'=>$artist_amount !=''?$artist_amount:'0',
                'artist_rate'=>$rate_arr['last_artist_content'],
                'partner_amount'=>$partner_amount !=''?$partner_amount:'0',
                'partner_rate'=>100,
                'total_amount'=>$total_amount,
                'cdate'=>date('Y-m-d H:i:s')
            );
            $model_agent_statement->addStatement($insert_data);
        }
        exit;
    }

    private function _get_assessment_config(){
        $model_agent_assessment = Model('agent_assessment');
        $list = $model_agent_assessment->getAgentAssessmentList();
        $data = array();
        if(!empty($list)){
            foreach($list as $v){
                $key = $v['agent_type'].'_'.$v['commission_lv'];
                $data[$key] = array(
                    'last_bzj_content'=>unserialize($v['last_bzj_content']),
                    'last_goods_content'=>unserialize($v['last_goods_content']),
                    'last_artist_content'=>unserialize($v['last_artist_content'])
                );
            }
        }
        $this->assessment_config = $data;
//        print_R($data);exit;
    }

    /***
     * 获取区域代理考核标准
     * @param $key
     * @param $data
     * @return mixed
     */
    private function _get_rate($key,$data){
        foreach($data as $k=>&$v){
            $v = 100;
            if($this->assessment_config[$key]){
                foreach($this->assessment_config[$key][$k] as $val){
                    if($v >= $val['index']){
                        $v = $val['rate'];
                        break;
                    }
                }
            }
        }
        unset($v);
        return $data;
    }


}
