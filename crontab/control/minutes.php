<?php
/**
 * 任务计划 - 分钟执行的任务
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
defined('InShopNC') or exit('Access Invalid!');

class minutesControl extends BaseCronControl {

    /**
     * 默认方法
     */
    public function indexOp() {
        $this->_cron_common();
        $this->_web_index_update();
        //更改趣猜状态
        $this->_set_guess_state();
        //趣猜结束前提醒
        $this->_send_gs_remind_msg();
        $this->_cron_mail_send();
        $this->_pintuan_timeout_refund();
        /*//更改专场状态-预展
        $this->_update_special_state_preview();
        //更改专场状态-开始
        $this->_update_special_state_start();
        //更改专场状态-结束
        $this->_update_special_state_end();*/
        // 结束前提醒
        //$this->_send_auciton_over_remind();
        //专场开拍前提醒
        $this->_special_start_notice();
        //专场结束前提醒
        //$this->_special_end_notice();
        //保证金订单取消
//        $this->_update_margin_order();
        //骗人的数字,总和
        $this->_dou_shi_pian_ren_de();
        //订单待支付提醒
        $this->_order_waiting_payment_notice();

        //拍卖违约订单处理(扣除保证金)
        //$this->check_margin_order_lock();
        //保证金待支付提醒
        //$this->_bond_waitting_payment_notice();//时间待配置
    }

    /**
     * 更新首页的商品价格信息
     */
    private function _web_index_update(){
         Model('web_config')->updateWebGoods();
    }

    /**
     * 拼团退款
     */
    private function _pintuan_timeout_refund(){
         Model('p_pintuan')->orderRefund();
    }

    /**
     * 发送邮件消息
     */
    private function _cron_mail_send() {
        //每次发送数量
        $_num = 50;
        $model_storemsgcron = Model('mail_cron');
        $cron_array = $model_storemsgcron->getMailCronList(array(), $_num);
        if (!empty($cron_array)) {
            $email = \Shopnc\Lib::messager();
            $mail_array = array();
            foreach ($cron_array as $val) {
                try {
                    $email->send($val['mail'],$val['subject'],$val['contnet']);
                    $return = true;
                } catch (\Shopnc\Lib\Messager\Exception $ex) {
                    $return = false;
                }
                if ($return) {
                    // 记录需要删除的id
                    $mail_array[] = $val['mail_id'];
                }
            }
            // 删除已发送的记录
            $model_storemsgcron->delMailCron(array('mail_id' => array('in', $mail_array)));
        }
    }

    /**
     * 更新趣猜信息
     */ 
    private function _set_guess_state() {
        // 1.获取符合条件的趣猜列表趣猜id
        $model_p_guess = Model('p_guess');
        $model_guess_offer = Model('guess_offer');
        $condition['end_time'] = array('lt', time()); //TMark
        $condition['gs_state'] = 1;
        $list = $model_p_guess->getGuessOpenList($condition);
        foreach ($list as $key => $value) {
            $model_guess_offer->get_winer(array('gs_id' => $value['gs_id']),$value['gs_right_price'],$value);
        }
    }

    /** 
     * 发送用户提醒信息
     * @return [type] [description]
     */
    private function _send_gs_remind_msg() {
        // 1.找到提醒时间小于当前时间的记录信息
        $remind_list = Model('guess_member_relation')->getRelationList(array('is_remind'=>1,'remind_time' => array('lt',time())));
        // 2.寻获遍历信息调用信息发送接口
        $relation_id = array();
        if (!empty($remind_list)) {
            $gs_id = array_column($remind_list,'gs_id');
            $p_guess = Model('p_guess')->getGuessList(array('gs_id'=>array('in',$gs_id)), '', $order = 'gs_id ', 0);
            $guess_list = array_column($p_guess,'gs_name','gs_id');
            foreach ($remind_list as $key => $value) {
                $relation_id[] = $value['relation_id'];
                $param = array();
                $param['code'] = 'guess_remind';
                $param['member_id'] = $value['member_id'];
                $param['number']['mobile'] = $value['member_mobile'];
                $param['param'] = array(
                    'gs_name' => $guess_list[$value['gs_id']],
                    'gs_url' => urlShop('show_guess', 'guess_detail',array('gs_id' => $value['gs_id']))
                );
                QueueClient::push('sendMemberMsg', $param);
            }
            Model('guess_member_relation')->delRelationInfo(array('relation_id' => array('in', $relation_id)));
        }
    }

    /**
     * 执行通用任务
     */
    private function _cron_common(){

        //查找待执行任务
        $model_cron = Model('cron');
        $cron = $model_cron->getCronList(array('exetime'=>array('elt',TIMESTAMP)));
        if (!is_array($cron)) return ;
        $cron_array = array(); $cronid = array();
        foreach ($cron as $v) {
            $cron_array[$v['type']][$v['exeid']] = $v;
        }
        foreach ($cron_array as $k=>$v) {
            // 如果方法不存是，直接删除id
            if (!method_exists($this,'_cron_'.$k)) {
                $tmp = current($v);
                $cronid[] = $tmp['id'];continue;
            }
            $result = call_user_func_array(array($this,'_cron_'.$k),array($v));
            if (is_array($result)){
                $cronid = array_merge($cronid,$result);
            }
        }
        //删除执行完成的cron信息
        if (!empty($cronid) && is_array($cronid)){
            $model_cron->delCron(array('id'=>array('in',$cronid)));
        }
    }


    /**
     * 上架
     *
     * @param array $cron
     */
    private function _cron_1($cron = array()){
        $condition = array('goods_commonid' => array('in',array_keys($cron)));
        $update = Model('goods')->editProducesOnline($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 根据商品id更新商品促销价格
     *
     * @param array $cron
     */
    private function _cron_2($cron = array()){
        $condition = array('goods_id' => array('in',array_keys($cron)));
        $update = Model('goods')->editGoodsPromotionPrice($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 优惠套装过期
     *
     * @param array $cron
     */
    private function _cron_3($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_bundling')->editBundlingQuotaClose($condition);
        if ($update) {
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 推荐展位过期
     *
     * @param array $cron
     */
    private function _cron_4($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_booth')->editBoothClose($condition);
        if ($update) {
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 团购开始更新商品促销价格
     *
     * @param array $cron
     */
    private function _cron_5($cron = array()) {
        $condition = array();
        $condition['goods_commonid'] = array('in', array_keys($cron));
        $condition['start_time'] = array('lt', TIMESTAMP);
        $condition['end_time'] = array('gt', TIMESTAMP);
        $groupbuy = Model('groupbuy')->getGroupbuyList($condition);
        foreach ($groupbuy as $val) {
            Model('goods')->editGoods(array('goods_promotion_price' => $val['groupbuy_price'], 'goods_promotion_type' => 1), array('goods_commonid' => $val['goods_commonid']));
        }
        //返回执行成功的cronid
        $cronid = array();
        foreach ($cron as $v) {
            $cronid[] = $v['id'];
        }
        return $cronid;
    }

    /**
     * 团购过期
     *
     * @param array $cron
     */
    private function _cron_6($cron = array()) {
        $condition = array('goods_commonid' => array('in', array_keys($cron)));
        //团购活动过期
        $update = Model('groupbuy')->editExpireGroupbuy($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 限时折扣过期
     *
     * @param array $cron
     */
    private function _cron_7($cron = array()) {
        $condition = array('xianshi_id' => array('in', array_keys($cron)));
        //限时折扣过期
        $update = Model('p_xianshi')->editExpireXianshi($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 加价购过期
     *
     * @param array $cron
     */
    private function _cron_8($cron = array()) {
        $condition = array('id' => array('in', array_keys($cron)));
        // 过期
        $update = Model('p_cou')->editExpireCou($condition);
        if ($update){
            // 返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 更新店铺（新增）商品消费者保障服务开启状态（如果商品在店铺开启保障服务之后增加则需要执行该任务更新其服务状态）
     * @param array $cron
     */
    private function _cron_9($cron = array()) {
        //查询商品详情
        $model_goods = Model('goods');
        $where = array();
        $where['goods_commonid'] =  array('in', array_keys($cron));
        $goods_list = $model_goods->getGoodsList($where, 'goods_id,goods_commonid,store_id');
        $cronid = array();
		if (!$goods_list) {
            // 返回执行成功的cronid
            foreach ($cron as $k=>$v) {
                $cronid[] = $v['id'];
            }
            return $cronid;
        }
        $store_goods_list = array();
        foreach($goods_list as $k=>$v){
            $store_goods_list[$v['store_id']][$v['goods_id']] = $v;
        }
        //查询店铺的保障服务
        $where = array();
        $where['ct_storeid'] = array('in', array_keys($store_goods_list));
        $model_contract = Model('contract');
        $c_list = $model_contract->getContractList($where);
		if (!$c_list) {
            foreach ($cron as $k=>$v) {
				$cronid[] = $v['id'];
            }
            return $cronid;
        }
        $goods_contractstate_arr = $model_contract->getGoodsContractState();
        $c_list_tmp = array();
        foreach ($c_list as $k=>$v) {
            if ($v['ct_joinstate_key'] == 'added' && $v['ct_closestate_key'] == 'open') {
                $c_list_tmp[$v['ct_storeid']][$v['ct_itemid']] = $goods_contractstate_arr['open']['sign'];
            }else{
                $c_list_tmp[$v['ct_storeid']][$v['ct_itemid']] = $goods_contractstate_arr['close']['sign'];
            }
        }

        //整理更新数据
        $goods_commonidarr = array();
        foreach ($c_list_tmp as $s_k=>$s_v) {
            $update_arr = array();
            foreach ($s_v as $item_k=>$item_v) {
                $update_arr["contract_$item_k"] = $item_v;
            }
            $result = $model_goods->editGoodsById($update_arr, array_keys($store_goods_list[$s_k]));
            if ($result){
                foreach ($store_goods_list[$s_k] as $g_k=>$g_v) {
                    $goods_commonidarr[] = $g_v['goods_commonid'];
                }
                array_unique($goods_commonidarr);
            }
        }

        if ($goods_commonidarr){
            // 返回执行成功的cronid
            foreach ($cron as $k=>$v) {
                if (in_array($k, $goods_commonidarr)) {
                    $cronid[] = $v['id'];
                }
            }
        }
        if ($cronid){
            // 返回执行成功的cronid
            return $cronid;
        } else {
            return false;
        }
    }

    /**
     * 手机专享过期
     *
     * @param array $cron
     */
    private function _cron_10($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_sole')->editSoleClose($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    //更改专场状态-预展
    private function _update_special_state_preview(){
        $model_special = Model('auction_special');
        $condition['special_preview_start'] = array('elt',TIMESTAMP);
        $condition['special_state'] = 20;
        $special_list = $model_special->getSpecialList($condition);
        $special_id_arr = array();
        if (!empty($special_list)) {
            foreach ($special_list as $key => $item) {
                $special_id_arr[] = $item['special_id'];
            }
        }
        // 更新成预展状态
        $update = array('special_state' => 11);
        $model_special->editSpecial($update, array('special_id'=>array('in',$special_id_arr)));
    }
    //更改专场状态-开始
    private function _update_special_state_start(){
        $model_special = Model('auction_special');
        $condition['special_start_time'] = array('elt',TIMESTAMP);
        $condition['special_state'] = 11;
        $special_list = $model_special->getSpecialList($condition);
        $special_id_arr = array();
        if (!empty($special_list)) {
            foreach ($special_list as $key => $item) {
                $special_id_arr[] = $item['special_id'];
            }
        }
        // 更新成开始状态
        $update = array('special_state' => 12);
        $model_special->editSpecial($update, array('special_id'=>array('in',$special_id_arr)));
    }

    //更改专场状态-结束
    private function _update_special_state_end(){
        $model_special = Model('auction_special');
        $condition['special_end_time'] = array('elt',TIMESTAMP);
        $condition['special_state'] = 12;
        $special_list = $model_special->getSpecialList($condition);
        $special_id_arr = array();
        if (!empty($special_list)) {
            foreach ($special_list as $key => $item) {
                $special_id_arr[] = $item['special_id'];
            }
        }
        // 更新成结束状态
        $update = array('special_state' => 13);
        $model_special->editSpecial($update, array('special_id'=>array('in',$special_id_arr)));
    }

    /*
     * 发送结束前提醒
     * */
    private function _send_auciton_over_remind()
    {
        $model_auction_member_relation = Model('auction_member_relation');
        // 提前五分钟
        $time = time() + 5 * 60;
        $condition = array(
            'is_remind' => 1,
            'remind_time' => array('lt', $time)
        );
        $list = $model_auction_member_relation->getRelationList($condition);
        if (!empty($list)) {
            foreach ($list as $value) {
                $param['code'] = 'auction_notic';
                $param['member_id'] = $value['member_id'];
                $param['param'] = array(
                    'auction_url' => urlAuction('auctions', 'index', array('id' => $value['auction_id'])),
                    'indate' => date('Y-m-d H:i:s', ($value['remind_time'] + AUCTION_OVER_REMIND)),
                    'auction_name' => $value['auction_name']
                );
                if (!empty($value['member_mobile'])) {
                    $param['number']['mobile'] = $value['member_mobile'];
                }
                QueueClient::push('sendMemberMsg', $param);
            }

            // 将发送过短信的设置成未订阅
            $model_auction_member_relation->setRelationInfo(array('is_remind' => 0), $condition);
        }
    }


    //专场开拍前提醒
    private function _special_start_notice()
    {

        $model_special_notice = Model('special_notice');
        $condition = array();
        $condition['sn_type'] = 1;
        $condition['sn_remind_time'] = array('elt',time());
        $condition['is_send'] = 0;
        $notice_list = $model_special_notice->getSpecialNoticeList($condition);

            if(!empty($notice_list)){
                foreach ($notice_list as $val) {
                    $model_special_notice->editSpecialNotice(array('is_send'=>1),array('sn_id'=>$val['sn_id']));
                    $url = AUCTION_NEW_URL."/profitDetails?special_id=".$val['special_id']; 
                    $url = filterUrl($url); 
                    $short = sinaShortenUrl($url); 
                    $param = array();
                    $param['code'] = 'special_start_notice';
                    $param['member_id'] = $val['member_id'];
                    $param['param'] = array(
                        'special_name' => $val['special_name'],
                        'special_url' => $short
                    );
                    $param['number'] = array('mobile' => $val['sn_mobile'], 'email' => $val['sn_email']);
                    QueueClient::push('sendMemberMsg', $param);
                }

            }
    }

    //专场结束前提醒
    private function _special_end_notice()
    {

        $model_special_notice = Model('special_notice');

        $notice_list = $model_special_notice->getSpecialNoticeList(array('sn_type'=>2,'sn_remind_time'=>array('elt',time()),'is_send'=>0));

        if(!empty($notice_list)){
            foreach ($notice_list as $val) {
                $model_special_notice->editSpecialNotice(array('is_send'=>1),array('sn_id'=>$val['sn_id']));
                $param = array();
                $param['code'] = 'special_end_notice';
                $param['member_id'] = $val['member_id'];
                $param['param'] = array(
                    'special_name' => $val['special_name'],
                    'special_url' => urlVendue('special_details', 'index', array('special_id' => $val['special_id']))
                );
                $param['number'] = array('mobile' => $val['sn_mobile'], 'email' => $val['sn_email']);
                QueueClient::push('sendMemberMsg', $param);
            }

        }

    }

    /**
     * 保证金订单取消
     * 超过30分钟,系统自动关闭订单
     */
    private function _update_margin_order(){
        $model_margin_order = Model('margin_orders');
        //$model_margin_order->editOrder(array('order_state'=>4),array('order_state'=>0,'created_at'=>array('lt',time()-3600)));

        $list = $model_margin_order->getOrderList(array('order_state'=>array('in','0,3'),'created_at'=>array('lt',time()-AUCTION_BOND_ORDER_AUTO_CANCEL_TIME)));
        if(!empty($list)){
            foreach($list as $order_info){
                try{
                    $model_margin_order->beginTransaction();
                    $model_pd = Model('predeposit');
                    //解冻充值卡
                    $rcb_amount = floatval($order_info['rcb_amount']);
                    if ($rcb_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $order_info['buyer_id'];
                        $data_pd['member_name'] = $order_info['buyer_name'];
                        $data_pd['amount'] = $rcb_amount;
                        $data_pd['order_sn'] = $order_info['order_sn'];
                        $model_pd->changeRcb('order_cancel', $data_pd);
                    }

                    //解冻预存款
                    $pd_amount = floatval($order_info['pd_amount']);
                    if ($pd_amount > 0) {
                        $data_pd = array();
                        $data_pd['member_id'] = $order_info['buyer_id'];
                        $data_pd['member_name'] = $order_info['buyer_name'];
                        $data_pd['amount'] = $pd_amount;
                        $data_pd['order_sn'] = $order_info['order_sn'];
                        $model_pd->changePd('order_cancel', $data_pd);
                    }
                    //解冻积分
                    $points_amount = floatval($order_info['points_amount']);
                    if ($points_amount > 0) {
                        $nuobi_points = $points_amount * 100;
                        Model('points')->savePointsLog('cancel', array('pl_memberid' => $_SESSION['member_id'], 'pl_membername' => $order_info['buyer_name'], 'pl_points' => $nuobi_points, 'order_sn' => $order_info['order_sn'], 'order_id' => $order_info['order_id']), true);
                    }
                    $re = $model_margin_order->editOrder(array('order_state'=>4),array('margin_id'=>$order_info['margin_id']));
                    if(!$re){
                        throw new Exception('失败');
                    }
                    $model_margin_order->commit();
                }catch (Exception $e){
                    $model_margin_order->rollback();
                }
            }
        }
    }

    /**
     * 骗人的数字
     */
    private function _dou_shi_pian_ren_de(){
        $str = file_get_contents(BASE_DATA_PATH . '/log/number.txt');
        $arr = array(0,1,2,2,3,4);
        $str += $arr[rand(0,5)];
        file_put_contents(BASE_DATA_PATH . '/log/number.txt', $str);
    }

    /**
     * (普通|拍卖|趣猜)商品订单待支付(创建订单15分钟后)提醒
     */
    private function _order_waiting_payment_notice(){
        /** @var orderModel $model_order */
        $model_order = Model('order');

        $start = TIMESTAMP-ORDER_CANCEL_TIMES;
        $end = TIMESTAMP-900;
        $condition = array(
            'chain_code' => 0,
            'notice_state' => 0,
            'order_state' => ORDER_STATE_NEW,
            'add_time' => array('between',"$start,$end"),
        );
        $order_list = $model_order->getNormalOrderList($condition,'','','','',['order_goods']);

        if($order_list){
            $orders_id = array();
            foreach ($order_list as $item) {
                $orders_id[] =  $item['order_id'];
                $goods_name = '';
                foreach ($item['extend_order_goods'] as $goods_item) {
                    $goods_name .= ','.$goods_item['goods_name'];
                }
                $param = array();
                $param['code'] = 'waiting_payment_notice';
                $param['member_id'] = $item['buyer_id'];
                $param['number']['mobile'] = $item['buyer_phone'];
                $param['param'] = array(
                    'goods_name' => substr($goods_name,1),
                    'goods_price' => $item['goods']['goods_price'],
                    'add_time' => date('Y-m-d H:i:s', $item['add_time']),
                    'delete_time' => date('Y.m.d H:i:s', ($item['add_time'] + ORDER_AUTO_CANCEL_TIME*3600)),
                );
                QueueClient::push('sendMemberMsg', $param);
            }
            if(!empty($orders_id)){
                $model_order->editOrder(['notice_state'=>1],['order_id'=>['in',$orders_id]]);
            }
        }
    }

    /**
     * 保证金订单待支付提醒
     * */
    private function _bond_waitting_payment_notice(){
        $model_margin_order = Model('margin_orders');

        $start = TIMESTAMP-AUCTION_BOND_ORDER_AUTO_CANCEL_TIME;
        $end = TIMESTAMP-900;
        $condition = array(
            'order_state'=>0,
            'notice_state'=>0,
            'created_at'=>array('between',"$start,$end"),
        );
        $margin_order_list = $model_margin_order->getOrderList($condition);

        if($margin_order_list){
            $margin_id = array();
            foreach ($margin_order_list as $item) {
                $margin_id[] = $item['margin_id'];
                $param = array();
                $param['code'] = 'waiting_payment_notice';
                $param['member_id'] = $item['buyer_id'];
                $param['number']['mobile'] = $item['buyer_phone'];
                $param['param'] = array(
                    'goods_name' => $item['auction_name'],
                    'goods_price' => $item['margin_amount'],
                    'add_time' => date('Y-m-d H:i:s', $item['created_at']),
                );
                QueueClient::push('sendMemberMsg', $param);
            }
            $margin_id = array_column($margin_order_list, 'margin_id');
            if(!empty($margin_id)){
                $model_margin_order->editOrder(['notice_state'=>1],['margin_id'=>['in',$margin_id]]);
            }
        }
    }

    

}
