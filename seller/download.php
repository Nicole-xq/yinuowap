<?php
/**
 * app下载
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
define('APP_ID','shop');
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));

require __DIR__ . '/../shopnc.php';

define('SELLER_SITE_URL', C('seller_site_url'));
define('APP_SITE_URL', SELLER_SITE_URL);
define('TPL_NAME',TPL_SELLER_NAME);
define('SELLER_RESOURCE_SITE_URL',SELLER_SITE_URL.DS.'resource');
define('SELLER_TEMPLATES_URL',SELLER_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);

$_GET['act']='download';

Shopnc\Core::runApplication();
