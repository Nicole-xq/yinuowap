<?php
/**
 * 收益专场
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class auction_specialControl extends BaseHomeControl{

    public function indexOp(){

        $model_special = Model('auction_special');
        $condition = [
            'special_rate_time'=>['gt',time()],
            'special_rate'=>['gt',0],
        ];
        $order = 'special_rate_time asc';
        $special_list = $model_special->getSpecialOpenList($condition,'',$order);
        $special_list_small = [];
        $special_list_middle = [];
        $special_list_big = [];
        $i = 1;
        foreach($special_list as $key => &$special_info){
            $special_info['day_num'] = Logic('auction')->getInterestDay($special_info['special_rate_time'],$special_info['special_end_time']);//计息天数
            if (!in_array($special_info['day_num'],[30,31,90,91,180,181])){
                continue;
            }
            $special_info['wap_image_path'] = getVendueLogo($special_info['wap_image']);
            $special_info['special_remain_rate_time'] = $special_info['special_rate_time']-TIMESTAMP;
            $special_auction_list = Model('auctions')->getAuctionList(['special_id'=>$special_info['special_id']],'auction_id,auction_click,bid_number');
            $special_click = 0;
            foreach($special_auction_list as $k=>$v){
                $special_click += $v['auction_click'];
            }
            $special_info['special_click'] = $special_click;
            if (in_array($special_info['day_num'],[30,31])){
                $special_list_small[] = $special_info;
            }elseif (in_array($special_info['day_num'],[90,91])){
                $special_list_middle[] = $special_info;
            }elseif (in_array($special_info['day_num'],[180,181])){
                $special_list_big[] = $special_info;
            }
        }
        unset($special_info);

        Tpl::output('list', array('small'=>$special_list_small, 'middle'=>$special_list_middle, 'big'=>$special_list_big));
        Tpl::showpage('profit_activity');
    }
}