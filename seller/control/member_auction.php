<?php
/**
 * 我的拍卖
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/8 0008
 * Time: 17:14
 * @author DukeAnn
 */

use Shopnc\Tpl;

defined('InShopNC') or exit('Access Invalid!');

class member_auctionControl extends BaseMemberControl
{
    public function __construct() {
        parent::__construct();
        Language::read('member_member_index');
    }

    public function indexOp()
    {
        return $this->auctioning_listOp();
    }

    /**
     * 参拍列表
     * */
    public function auctioning_listOp()
    {
        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->getMemberAuctionList($_SESSION['member_id'], '', '');
        $margin_order_list =$result['margin_order_list'];

        Tpl::output('margin_order_list',$margin_order_list);
        Tpl::output('show_page',$result['showPage']);
        $this->profile_menu('auctioning_list');
        Tpl::showpage('member_order.margin');
    }

    /**
     * 已拍下
     * */
    public function have_auctionOp()
    {
        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->getMemberAuctionList($_SESSION['member_id'], 0, 1, $_GET['keyword']);
//        print_R($result);exit;
        $margin_order_list =$result['margin_order_list'];
        Tpl::output('margin_order_list',$margin_order_list);
        Tpl::output('show_page',$result['showPage']);
        $this->profile_menu('have_auction');
        Tpl::showpage('member_order.auction');
    }

    /**
     * 未拍中
     * */
    public function auctioned_listOp()
    {
        $logic_auction_order = Logic('auction_order');
        $result = $logic_auction_order->getMemberAuctionList($_SESSION['member_id'], '', ['neq',1]);
        $margin_order_list =$result['margin_order_list'];

        Tpl::output('margin_order_list',$margin_order_list);
        Tpl::output('show_page',$result['showPage']);
        $this->profile_menu('auctioned_list');
        Tpl::showpage('member_order.margin_old');
    }

    /*
     * 买家修改订单状态
     * */
    public function change_stateOp()
    {
        $state_type = $_GET['state_type'];
        $order_id   = intval($_GET['order_id']);

        $model_auction_order = Model('auction_order');
        $order_info = $model_auction_order->getInfo(array('auction_order_id' => $order_id));

        if($state_type == 'order_cancel') {
            // 取消订单
            $result = $this->_order_cancel($order_info, $_POST);
        } else if ($state_type == 'order_receive') {
            // 确认收货
            $result = $this->_order_receive($order_info, $_POST);
        } else {
            exit();
        }
        if(!$result['state']) {
            showDialog($result['msg'],'','error','',5);
        } else {
            showDialog($result['msg'],'reload','js');
        }
    }

    /**
     * 取消订单
     */
    private function _order_cancel($order_info, $post) {
        if (!chksubmit()) {
            Tpl::output('order_info', $order_info);
            Tpl::showpage('member_auction.cancel','null_layout');
            exit();
        } else {
            $logic_auction_order = Logic('auction_order');
            if (TIMESTAMP - ORDER_CANCEL_TIMES < $order_info['api_pay_time'] && $order_info['payment_code'] != "underline") {
                $_hour = ceil(($order_info['api_pay_time']+ORDER_CANCEL_TIMES-TIMESTAMP)/3600);
                return callback(false,'该订单曾尝试使用第三方支付平台支付，须在'.$_hour.'小时以后才可申请退款');
            }
            // 会员中心修改尾款订单状态
            $result = $logic_auction_order->MemberChangeOrderState($order_info, $post, 0);
            if (!$result['state']) {
                showDialog($result['msg'], '', 'error');
            } else {
                showDialog($result['msg'], 'reload', 'succ');
            }
        }
    }

    /**
     * 收货
     */
    private function _order_receive($order_info, $post) {
        if (!chksubmit()) {
            Tpl::output('order_info', $order_info);
            Tpl::showpage('member_auction.receive','null_layout');
            exit();
        } else {
            $logic_auction_order = Logic('auction_order');
            // 会员中心修改尾款订单状态
            $result = $logic_auction_order->MemberChangeOrderState($order_info, $post, 1);
            if (!$result['state']) {
                showDialog($result['msg'], '', 'error');
            }
            // 添加会员积分，经验，会员返佣
            $result = $logic_auction_order->changeOrderStateReceive($order_info, 'buyer', $_SESSION['member_name'], '签收了货物');
            if (!$result['state']) {
                showDialog($result['msg'], '', 'error');
            } else {
                showDialog($result['msg'], 'reload', 'succ');
            }
        }
    }

    /*
     * 已拍下订单详情
     * */
    public function show_orderOp()
    {
        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['auction_order_id']);
        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            showDialog($result['msg'], '', 'error', '', 5);
        }
        Tpl::output('order_info',$result['data']['order_info']);
        Tpl::showpage('member_auction.show');
    }

    /**
     * 物流跟踪
     */
    public function search_deliverOp(){
        Language::read('member_member_index');
        $lang   = Language::getLangContent();

        $margin_id = intval($_GET['margin_id']);
        $auction_order_id = intval($_GET['auction_order_id']);
        // 获取订单详情
        $login_auction_order = Logic('auction_order');
        $result = $login_auction_order->getMemberAuctionOrderInfo($auction_order_id, $margin_id);
        if (!$result['state']) {
            showDialog($result['msg'], '', 'error', '', 5);
        }
        Tpl::output('order_info',$result['data']['order_info']);

        Tpl::output('store_info',$result['data']['order_info']['extend_store']);

        Tpl::output('e_code',$result['data']['order_info']['express_info']['e_code']);
        Tpl::output('e_name',$result['data']['order_info']['express_info']['e_name']);
        Tpl::output('e_url',$result['data']['order_info']['express_info']['e_url']);
        Tpl::output('shipping_code',$result['data']['order_info']['invoice_no']);

        self::profile_menu('search','search');
        Tpl::output('left_show','order_view');
        Tpl::showpage('member_auction_deliver.detail');
    }

    /**
     * 用户中心右边，小导航
     *
     * @param string    $menu_key   当前导航的menu_key
     * @return mixed
     */
    private function profile_menu($menu_key='')
    {
        Language::read('member_layout');
        $menu_array = array(
            array(
                'menu_key'=>'auctioning_list',
                'menu_name'=>'参拍的',
                'menu_url'=>'index.php?act=member_auction&op=auctioning_list'
            ),
            array(
                'menu_key'=>'have_auction',
                'menu_name'=>'已拍下',
                'menu_url'=>'index.php?act=member_auction&op=have_auction'
            ),
            array(
                'menu_key'=>'auctioned_list',
                'menu_name'=>'未拍中',
                'menu_url'=>'index.php?act=member_auction&op=auctioned_list'
            ),
        );
        Tpl::output('member_menu',$menu_array);
        Tpl::output('menu_key',$menu_key);
    }

}
