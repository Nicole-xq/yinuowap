<?php
/**
 * 前台趣猜
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');

class show_guessControl extends BaseHomeControl {

    public function __construct() {
        parent::__construct();
        Tpl::output('index_sign','guess');
        //读取语言包
         Language::read('member_groupbuy,home_cart_index');

        //分类导航
        $nav_link = array(
            0=>array(
                'title'=>Language::get('homepage'),
                'link'=>SELLER_SITE_URL,
            ),
            1=>array(
                'title'=>Language::get('nc_groupbuy')
            )
        );
    }

    /**
     * 趣猜活动首页
     */
    public function indexOp()
    {
        // 热拍商品
        $model_auctions = Model('auctions');
        $auctions_item = $model_auctions->getHotAuctionGoodsList();
        Tpl::output('auctions_item', $auctions_item);
      
        // 活动龙虎榜
        $model_guess_offer = Model('guess_offer');
        $winers = $model_guess_offer->get_winer_list();
        foreach($winers as &$winer){
            $winer['member_name'] = hideStar($winer['member_name']);
        }
        Tpl::output('winers', $winers);
      
        // 活动内的趣猜列表
        $model_guess = Model('p_guess');
        $condition = array();
        if ($_GET['gs_act_id']) {
            $condition['gs_act_id'] = $_GET['gs_act_id'];
        }
        $condition['end_time'] = array('gt', time());
        $guess_list = $model_guess->getGuessOpenList($condition);
        if (empty($guess_list)){
             Tpl::output('hidden_nctoolbar', 1);
             Tpl::output('second',3);
             Tpl::output('msg','该趣猜活动不存在或未开启');
             Tpl::output('url','index.php?act=index');
             Tpl::showpage('count');exit;
        }
        $list = unserialize(C('guess_advpic'));
        Tpl::output('list', $list);
        Tpl::output('guess_list', $guess_list);
        Tpl::output('current', 'online');
        Tpl::showpage('guess.index');
    }

    /**
     * 趣猜详情页
     * @return [type] [description]
     */
    public function guess_detailOp()
    {
        $gs_id = $_GET['gs_id'];
        if (empty($gs_id)) {
            showMessage('参数有误请重试', '', 'html', 'error');
        }
        $time = time();
        $model_p_guess = Model('p_guess');
        $guess_info = $model_p_guess->getOpenGuessInfo(array('gs_id'=>$gs_id));
        if (empty($guess_info)) {
            showMessage('趣猜不存在或者已经关闭', '', 'html', 'error');
        }
        $guess_info['guess_remaining_end_time'] =  $guess_info['end_time'] >time() ? $guess_info['end_time'] - time() : 0;
        $guess_info['guess_remaining_start_time'] =  $guess_info['start_time'] >time() ? $guess_info['start_time'] - time() : 0;
        Tpl::output('guess_info', $guess_info);

        //往期趣猜
        $pass_guess_info = $model_p_guess->getGuessInfo(array('gs_state' => 2),'end_time desc');
        $model_guess_offer = Model('guess_offer');
        if ($pass_guess_info) {
            $pass_guess_info['img'] = $this->get_gs_img($pass_guess_info['gs_id']);
            //检测有无中奖
            $winer = $model_guess_offer->get_guess_offer(['gs_id' => $pass_guess_info['gs_id'],'gs_state' => ['in',[2,3]]],'id');
            $pass_guess_info['be_win'] = $winer ? true : false;
        }
        Tpl::output('pass_guess_info',$pass_guess_info);

        //更多趣猜
        $more_guess_array = $model_p_guess->getGuessOpenList(array('end_time' => array('gt',time())),'*',$order = 'end_time asc',6);
        $more_guess = array();
        foreach ($more_guess_array as $key => $value) {
            $value['img'] = $this->get_gs_img($value['gs_id']);
            $more_guess[$key] = $value;
        }
        Tpl::output('more_guess',$more_guess);

        // 获取 goods ids
        $gs_goods_list = $model_p_guess->get_guess_goods_list(array('gs_id' => $gs_id),array('goods_id,goods_image'));
        $goods_id_list = array();
        $goods_img_list = array();
        foreach ($gs_goods_list as $key => $value) {
            $goods_id_list[$key] = $value['goods_id'];
            $good_img_list[$key] = $value['goods_image'];
        }
        // 获取图片相关信息
        $image_infos = $this->deal_gs_img_data($good_img_list,$guess_info['store_id']);
        Tpl::output('show_img', $good_img_list[0]);
        Tpl::output('image_list',$image_infos['image_list']);
        Tpl::output('image_json',json_encode($image_infos['image_list']));
        Tpl::output('goods_image',$image_infos['goods_image']);

        // 获取 goods_list
        $model_goods = Model('goods');
        $goods_list =  $model_goods->getGoodsList(array('goods_id' => array('in',$goods_id_list)),'goods_commonid');
        $goods_commonid_list = array();
        foreach ($goods_list as $key => $value) {
            $goods_commonid_list[]= $value['goods_commonid'];
        }
        $goods_commonid_list = array_unique($goods_commonid_list);
        $goods_common_list = $model_goods->getGoodsCommonList(array('goods_commonid' => array('in',$goods_commonid_list)),'goods_body');
        $guess_body = '';
        foreach ($goods_common_list as $key => $value) {
            $guess_body .= $value['goods_body'];
        }
        Tpl::output('guess_body', $guess_body);

        // 如果趣猜结束获取获取中奖者信息
        if ($guess_info['gs_state'] == 2) {
            $winer = $model_guess_offer->get_guess_offer(array('gs_id' => $gs_id,'gs_state' => array('in',array(2,3))));
            Tpl::output('winer_info', $winer);
            // 如果是中奖者并且距结束时间还不到半个小时
            if (($winer['member_id'] == $_SESSION['member_id']) && (($winer['finished_time'] + C('guess_add_cart_limit_time')) > $time) && ($winer['gs_state'] == 2)) {
                Tpl::output('if_winer','true');
            } elseif(isset($_SESSION['member_id'])) {
                $unchosen = $model_guess_offer->get_guess_offer(array('gs_id' => $gs_id,'gs_state' => 1,'member_id' => $_SESSION['member_id'])); 
                if (!empty($unchosen)){
                    Tpl::output('if_winer','false');
                }
            }
        }

        $model_favorites = Model('favorites');
        if ($_SESSION['member_id']) {
            //1.获取用户对商品的收藏状态
            $if_favorites = $model_favorites->getOneFavorites(array('member_id' => $_SESSION['member_id'],'fav_type' => 'guess','fav_id' => $gs_id));
            Tpl::output('if_favorites', $if_favorites);
            //2. 当前用户的中奖状态
            $my_guess_offer = $model_guess_offer->get_guess_offer(array('gs_id' => $gs_id,'member_id' => $_SESSION['member_id']));
            Tpl::output('my_guess_offer',$my_guess_offer);
            //3.获取用户对店铺的收藏状态
            $if_store_favorites = $model_favorites->getOneFavorites(array('member_id' => $_SESSION['member_id'],'fav_type' => 'store','fav_id' =>  $guess_info['store_id']));
            Tpl::output('if_store_favorites', $if_store_favorites);
        }

        // 最高竞猜价和最低竞猜价
        $guess_offer_min = $model_guess_offer->get_guess_offer_min(array('gs_id' => $gs_id));
        Tpl::output('offer_min', $guess_offer_min);
        $guess_offer_max = $model_guess_offer->get_guess_offer_max(array('gs_id' => $gs_id));
        Tpl::output('offer_max', $guess_offer_max);

        // 增加增加围观次数
        $model_p_guess->addViews(array('gs_id' => $gs_id));
        
        // 获取趣猜详情页需要的文章
        $condition = array();
        $condition['ac_id'] = '8';
        $guess_articles = Model('article')->getArticleList($condition);
        $guess_articles_arr = array();
        foreach ($guess_articles as $key => $value) {
            $guess_articles_arr[$value['article_position']] = $value;
        }
        Tpl::output('guess_articles', $guess_articles_arr);
        
        // 获取用户提醒信息
        $model_guess_member_relation = Model('guess_member_relation');
        $guess_member_relation_info = $model_guess_member_relation->getRelationInfo(array('gs_id' => $gs_id,'member_id' => $_SESSION['member_id']));
        Tpl::output('guess_member_relation', $guess_member_relation_info); 
        Tpl::showpage('guess.detail');
    }

    /**
     * 添加趣猜报价
     */
    public function add_guess_offerOp()
    {
        if (empty($_SESSION['member_id'])){
            echo  json_encode(array('state' => '0', 'data' => '请登录'));exit;
        }
        $insert['gs_id'] = $_POST['gs_id'];
        $insert['gs_name'] = $_POST['gs_name'];
        $insert['member_id'] = $_SESSION['member_id'];
        $insert['member_name'] = $_SESSION['member_name'];
        $insert['gs_offer_price'] = $_POST['guess_offer'];
        $insert['end_time'] = $_POST['end_time'];
        $insert['partake_time'] = time();
        $model_guess_offer = Model('guess_offer');
        $guess_offer_data = $model_guess_offer->get_guess_offer(array('member_id' => $_SESSION['member_id'], 'gs_id' => $insert['gs_id']));
        if ($guess_offer_data) {
            echo  json_encode(array('state' => '0', 'data' => '您已经参与过该趣猜'));exit;
        }
        // 添加趣猜
        $result = $model_guess_offer->add_guess_offer($insert);
        if($result) {
            // 更新趣猜报名人数
            $model_p_guess = Model('p_guess');
            $model_p_guess->update_join_num(array('gs_id' => $_POST['gs_id']));
            echo  json_encode(array('state' => '1','data' => array('price' => $_POST['guess_offer'])));exit;
        } else {
            echo  json_encode(array('state' => '0','data' => '添加失败'));exit;
        }
    }

    /**
     * 销售记录
     */
    public function guesslogOp() {
        $gs_id = intval($_GET['gs_id']);
        $model_guess_offer = Model('guess_offer');
        $gs_member_list = $model_guess_offer->get_guess_offer_list(array('gs_id'=>$gs_id),10);
        Tpl::output('show_page',$model_guess_offer->showpage());
        Tpl::output('gs_member_list',$gs_member_list);
        Tpl::showpage('goods.guesslog','null_layout');
    }

    /**
     * 获取趣猜代表商品的图片
     * @return [type] [description]
     */
    private function get_gs_img($gs_id){
         $model_guess = Model('p_guess');
         $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => $gs_id), 'min(goods_image) as goods_image', 'gs_appoint desc', 'gs_id');
         $ggoods_array = current($ggoods_array);
         return cthumb($ggoods_array['goods_image'], 240, $_SESSION['store_id']);
    }   

    /**
     * 处理要展示的图片
     * @param  array  $image_list [description]
     * @return [type]             [description]
     */
    private function deal_gs_img_data($img_list = array(),$store_id = 0) {
        $image_list = array();
        $goods_image = array();
        foreach ($img_list as $key => $value) {   
            $image_list[] = array( '_small' => cthumb($value , 60 ,$store_id) , '_mid' => cthumb($value , 360 ,$store_id) , '_big' => cthumb($value , 1280 ,$store_id) );
            $goods_image[] = "{ title : '', levelA : '".cthumb($value, 60,$store_id)."', levelB : '".cthumb($value, 360,$store_id)."', levelC : '".cthumb($value, 360,$store_id)."', levelD : '".cthumb($value, 1280,$store_id)."'}";
        }
        $result['image_list'] = $image_list;
        $result['goods_image'] = $goods_image;
        return $result;
    }


    /**
     * 发送短信验证码
     */
    public function send_modify_mobileOp()
    {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["mobile"], "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号码'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            exit(json_encode(array('state'=>'false','msg'=>$error)));
        }
        $gs_mb_relation_info = Model('guess_member_relation')->getRelationInfo(array('gs_id' => $_POST['gs_id'],'member_mobile' => $_POST['mobile'],'is_remind' => 1));
        if ($gs_mb_relation_info) {
            exit(json_encode(array('state'=>'false','msg'=>'该手机已设置了趣猜提醒')));
        }
        $model_member = Model('member');

        //发送频率验证
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id'=>$_SESSION['member_id']));
        if (!empty($member_common_info['send_mb_time'])) {
            if (TIMESTAMP - $member_common_info['send_mb_time'] < 58) {
                exit(json_encode(array('state'=>'false','msg'=>'请60秒以后再次发送短信')));
            }
        }

        // 生成验证码
        $verify_code = rand(1000,9999);

        //组装发送内容
        $model_tpl = Model('mail_templates');
        $tpl_info = $model_tpl->getTplInfo(array('code'=>'guess_mobile'));
        $param = array();
        $param['site_name'] = C('site_name');
        $param['send_time'] = date('Y-m-d H:i',TIMESTAMP);
        $param['verify_code'] = $verify_code;
        $message    = ncReplaceText($tpl_info['content'],$param);
        
        // 发送
        $sms = new Sms();
        $result = $sms->send($_POST["mobile"],$message);

        // 发送成功回调
        if ($result) {
            $data = array();
            $data['auth_code'] = $verify_code;
            $data['send_acode_time'] = TIMESTAMP;
            $data['send_mb_time'] = TIMESTAMP;
            $update = $model_member->editMemberCommon($data,array('member_id'=>$_SESSION['member_id']));
            if (!$update) {
                exit(json_encode(array('state'=>'false','msg'=>'系统发生错误，如有疑问请与管理员联系')));
            }
            exit(json_encode(array('state'=>'true','msg'=>'发送成功')));
        } else {
            exit(json_encode(array('state'=>'false','msg'=>'发送失败')));
        }
    }


    /**
     * 设置结束前提醒
     * */
    public function guess_remindOp()
    {
        $mobile = $_POST['mobile'];
        $vcode = $_POST['vcode'];
        $is_remind = 1;
        $gs_id = $_POST['gs_id'];

        if (empty($_SESSION['member_id'])){
            json_api('error', '请登录');
        }
        if (empty($gs_id)) {
            json_api('error', '您的参数有误');
        }
        $model_member = Model('member');
        $model_guess = Model('p_guess');
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$mobile, "require"=>"true", 'validator'=>'mobile',"message"=>'请正确填写手机号'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            json_api('error', $error);
        }
       
        //如果是获取验证码用户进行手机验证
        $condition = array();
        $condition['member_id'] = $_SESSION['member_id'];
        $condition['auth_code'] = intval($vcode);
        $member_common_info = $model_member->getMemberCommonInfo($condition,'send_acode_time');
        if (!$member_common_info) {
            json_api('error', '手机验证码错误，请重新输入');
        }
        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
            json_api('error','手机验证码已过期，请重新获取验证码');
        }
        $guess_info = $model_guess->getGuessInfo(array('gs_id' => $gs_id));
        if (empty($guess_info)) {
             json_api('error', '您的参数有误');
        }
        
        //建立订阅关系
        $model_guess_member_relation = Model('guess_member_relation');
        
        //查询是否已经存在
        $relation = $model_guess_member_relation->getRelationInfoByMemberGuessID($_SESSION['member_id'], $gs_id);
        $insert['gs_id'] = $gs_id;
        $insert['member_id'] = $_SESSION['member_id'];
        $insert['is_remind'] = $is_remind;
        $insert['member_mobile'] = $mobile;
        $insert['remind_time'] = $guess_info['end_time'] - 30*60;
        $insert['member_name'] = $_SESSION['member_name'];
        if (empty($relation)) {
            $res = $model_guess_member_relation->addRelation($insert);
        } else {
            $res = $model_guess_member_relation->setRelationInfo($insert, array('relation_id' => $relation['relation_id']));
        }

        // 设置提醒人数 +1
        if (!$res) {
            json_api('error','订阅失败');
        } else {
            $model_guess->editGuess(array('set_reminders_num' => array('exp', 'set_reminders_num+1')), array('gs_id' => $gs_id));
            json_api('ok', '订阅提醒成功');
        }
    }

    /*
     * 取消提醒
     *
     * */
    public function cancel_guess_remindOp() 
    {
        $relation_id = intval($_POST['relation_id']);
        $gs_id = intval($_POST['gs_id']);
        if (empty($relation_id)) {
            showDialog('无效的订阅ID','','error');
        }
        $model_guess_member_relation = Model('guess_member_relation');
        $model_guess = Model('p_guess');
        $update = array(
            'is_remind' => 0,
            'is_remind_app' => 0,
        );
        $result_1 = $model_guess_member_relation->setRelationInfo($update, array('relation_id' => $relation_id));

        // 设置提醒人数 -1
        $result_2 = $model_guess->editGuess(array('set_reminders_num' => array('exp', 'set_reminders_num-1')), array('gs_id' => $gs_id));
        if ($result_1&&$result_2) {
            json_api('ok','取消订阅成功');
        } else {
            json_api('error','取消订阅失败');
        }
    }
}