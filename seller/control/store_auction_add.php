<?php
/**
 * 拍卖商品
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/22 0022
 * Time: 10:10
 * @author ADKi
 */
use Shopnc\Tpl;

defined('InShopNC') or exit ('Access Invalid!');

class store_auction_addControl extends BaseSellerControl
{
    protected $model_auctions;
    protected $model_goods_class;
    protected $model_staple;
    protected $model_store_bind_class;
    public function __construct() {
        parent::__construct();
        Language::read('member_store_goods_index');
        $this->model_auctions = Model('auctions');
        $this->model_goods_class = Model('goods_class');
        $this->model_staple = Model('goods_class_staple');
        $this->model_store_bind_class = Model('store_bind_class');
        if ($_SESSION['open_auction'] == 0) {
            showDialog('该店铺尚未开通发布拍品功能','index.php?act=store_auction&op=index','error');
        }
    }

    public function indexOp()
    {
        $this->add_step_oneOp();
    }

    /**
     * 添加拍卖品
     */
    public function add_step_oneOp() {
        // 商品分类
        $goods_class = $this->model_goods_class->getGoodsClass($_SESSION['store_id'],0,1,$_SESSION['seller_group_id'],$_SESSION['seller_gc_limits']);

        //常用分类
        $param_array = array();
        $param_array['member_id'] = $_SESSION['member_id'];
        $staple_array = $this->model_staple->getStapleList($param_array);

        Tpl::output('staple_array', $staple_array);
        Tpl::output('goods_class', $goods_class);
        Tpl::showpage('store_auctions_add.step1');
    }

    /**
     * 添加拍品
     */
    public function add_step_twoOp() {

        // 现暂时改为从匿名“自营店铺专属等级”中判断
        $editor_multimedia = false;
        if ($this->store_grade['sg_function'] == 'editor_multimedia') {
            $editor_multimedia = true;
        }
        Tpl::output('editor_multimedia', $editor_multimedia);

        $gc_id = intval($_GET['class_id']);

        // 验证商品分类是否存在且商品分类是否为最后一级
        $data = $this->model_goods_class->getGoodsClassForCacheModel();
        if (!isset($data[$gc_id]) || isset($data[$gc_id]['child']) || isset($data[$gc_id]['childchild'])) {
            showDialog(L('store_goods_index_again_choose_category1'));
        }

        // 如果不是自营店铺或者自营店铺未绑定全部商品类目，读取绑定分类
        if (!checkPlatformStoreBindingAllGoodsClass()) {
            $where['class_1|class_2|class_3'] = $gc_id;
            $where['store_id'] = $_SESSION['store_id'];
            //店铺分佣比例
            $rs = $this->model_store_bind_class->getStoreBindClassInfo($where);
            if (empty($rs)) {
                showMessage(L('store_goods_index_again_choose_category2'));
            }
        }

        //权限组对应分类权限判断
        if (!$_SESSION['seller_gc_limits']&& $_SESSION['seller_group_id']) {
            $rs = Model('seller_group_bclass')->getSellerGroupBclassInfo(array('group_id'=>$_SESSION['seller_group_id'],'gc_id'=>$gc_id));
            if (!$rs) {
                showMessage('您所在的组无权操作该分类下的商品', '', 'html', 'error');
            }
        }

        // 更新常用分类信息
        $goods_class = $this->model_goods_class->getGoodsClassLineForTag($gc_id);
        Tpl::output('goods_class', $goods_class);
        $this->model_staple->autoIncrementStaple($goods_class, $_SESSION['member_id']);

        // 获取类型相关数据
        $typeinfo = Model('type')->getAttr($goods_class['type_id'], $_SESSION['store_id'], $gc_id);
        list($spec_json, $spec_list, $attr_list, $brand_list) = $typeinfo;
        Tpl::output('attr_list', $attr_list);

        // 自定义属性
        $custom_list = Model('type_custom')->getTypeCustomList(array('type_id' => $goods_class['type_id']));
        Tpl::output('custom_list', $custom_list);

        Tpl::showpage('store_auctions_add.step2');
    }

    /**
     * 保存拍品（商品发布第二步使用）
     */
    public function save_auctionOp() {
        if (!empty($_FILES['auction_video_file']['name'])) {
            //获取视频数据，提交时候保存视频
            $datas = $this->_getGoodsVideo($_FILES);
            if ($datas['status'] == 'success') {
                $_POST['auction_video'] = $datas['auction_video'];
            } elseif ($datas['status'] == 'fail') {
                showMessage($datas['error'], '', 'html', 'error');
            }
        }

        $logic_auction = Logic('auction');

        $result =  $logic_auction->saveAuctions(
            $_POST,
            $_SESSION['store_id'],
            $_SESSION['store_name'],
            $this->store_info['store_state'],
            $_SESSION['seller_id'],
            $_SESSION['seller_name'],
            $_SESSION['bind_all_gc']
        );
        if(!$result['state']) {
            showMessage(L('error') . $result['msg'], urlSeller('seller_center'), 'html', 'error');
        }

        redirect(urlSeller('store_auction_add', 'add_step_three', array('auction_id' => $result['data'])));
    }

    /**
     * 第三步添加颜色图片
     */
    public function add_step_threeOp() {
        $auction_id = intval($_GET['auction_id']);
        if ($auction_id <= 0) {
            showMessage(L('wrong_argument'), urlSeller('seller_center'), 'html', 'error');
        }

        $model_auction = Model('auctions');
        $img_array = $model_auction->getAuctionList(array('auction_id' => $auction_id), 'auction_image');

        // 整理，更具id查询颜色名称
        if (!empty($img_array)) {
            $image_array = array();
            foreach ($img_array as $key => $val) {
                $image_array[0]['auction_image'] = $val['auction_image'];
                $image_array[0]['is_default'] = 1;
            }
            Tpl::output('img', $image_array);
        }

        Tpl::output('auction_id', $auction_id);
        Tpl::showpage('store_auctions_add.step3');
    }

    /**
     * 保存商品颜色图片
     */
    public function save_imageOp(){
        if (chksubmit()) {
            $auction_id = intval($_POST['auction_id']);
            if ($auction_id <= 0 || empty($_POST['img'])) {
                showMessage(L('wrong_argument'));
            }
            // 保存
            $insert_array = array();

            $k = 0;
            foreach ($_POST['img'] as $key => $value) {

                if ($value['name'] == '') {
                    continue;
                }
                // 商品默认主图
                $update_array = array();        // 更新商品主图
                $update_where = array();
                $update_array['auction_image']    = $value['name'];
                $update_where['auction_id'] = $auction_id;
                if ($k == 0 || $value['default'] == 1) {
                    $k++;
                    $update_array['auction_image']    = $value['name'];
                    $update_where['auction_id'] = $auction_id;
                    // 更新拍品主图
                    $this->model_auctions->editAuctions($update_array, $update_where);
                }
                $tmp_insert = array();
                $tmp_insert['auction_id']   = $auction_id;
                $tmp_insert['store_id']         = $_SESSION['store_id'];
                $tmp_insert['auction_image']      = $value['name'];
                $tmp_insert['auction_image_sort'] = ($value['default'] == 1) ? 0 : intval($value['sort']);
                $tmp_insert['is_default']       = $value['default'];
                $insert_array[] = $tmp_insert;

            }

            $rs = $this->model_auctions->addAuctionsImagesAll($insert_array);
            if ($rs) {
                redirect(urlSeller('store_auction_add', 'add_step_four', array('auction_id' => $auction_id)));
            } else {
                showMessage(L('nc_common_save_fail'));
            }
        }
    }

    /**
     * 商品发布第四步
     */
    public function add_step_fourOp() {
        // 单条商品信息
        $auctions_info = $this->model_auctions->getAuctionsInfo(array('auction_id' => $_GET['auction_id']));

        // 自动发布动态
        $data_array = array();
        $data_array['auction_id'] = $auctions_info['auction_id'];
        $data_array['store_id'] = $auctions_info['store_id'];
        $data_array['auction_name'] = $auctions_info['auction_name'];
        $data_array['auction_image'] = $auctions_info['auction_image'];
        $data_array['goods_price'] = $auctions_info['goods_price'];

        $this->storeAutoShare($data_array, 'new');

        Tpl::output('auction_id', $auctions_info['auction_id']);
        Tpl::showpage('store_auctions_add.step4');
    }

    //获取视频数据,加入店铺的专辑内
    public function _getGoodsVideo($file){
        $data = array();
        $store_id = $_SESSION['store_id'];
        if (!empty($file['auction_video_file']['name'])) {
            $upload = new UploadVideoFile();
            $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . 'goods_video'   );
            $upload->set('fprefix', $store_id);
            $upload->set('max_size' , 10240);
            $result = $upload->upfile('auction_video_file',true);
            if ($result) {
                $data['status'] = 'success';
                $data['auction_video'] = $upload->file_name;
            } else {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
        }

        // 判断图片数量是否超限
        $model_album = Model('video_album');
        $album_limit = $this->store_grade['sg_album_limit'];
        if ($album_limit > 0) {
            $album_count = $model_album->getCount(array('store_id' => $store_id));
            if ($album_count >= $album_limit) {
                return callback(false, '您上传图片数达到上限，请升级您的店铺或跟管理员联系');
            }
        }

        $class_info = $model_album->getOne(array('store_id' => $store_id, 'is_default' => 1), 'video_album_class');

        $video_path = $upload->file_name;

        // 存入视频
        $video = explode('.', $_FILES["auction_video_file"]["name"]);
        $insert_array = array();
        $insert_array['video_name'] = $video['0'];
        $insert_array['video_tag'] = '';
        $insert_array['video_class_id'] = $class_info['video_class_id'];
        $insert_array['video_cover'] = $video_path;
        $insert_array['video_size'] = intval($_FILES['auction_video_file']['size']);
        $insert_array['upload_time'] = TIMESTAMP;
        $insert_array['store_id'] = $store_id;
        Model('upload_video_album')->add($insert_array);

        return $data;
    }

    /**
     * 上传图片
     */
    public function image_uploadOp() {
        $logic_goods = Logic('goods');

        $result =  $logic_goods->uploadGoodsImage(
            $_POST['name'],
            $_SESSION['store_id'],
            $this->store_grade['sg_album_limit']
        );

        if(!$result['state']) {
            echo json_encode(array('error' => $result['msg']));die;
        }

        echo json_encode($result['data']);die;
    }

    /**
     * ajax获取商品分类的子级数据
     */
    public function ajax_goods_classOp() {
        $gc_id = intval($_GET['gc_id']);
        $deep = intval($_GET['deep']);
        if ($gc_id <= 0 || $deep <= 0 || $deep >= 4) {
            exit();
        }

        $list = $this->model_goods_class->getGoodsClass($_SESSION['store_id'], $gc_id, $deep,$_SESSION['seller_group_id'],$_SESSION['seller_gc_limits']);
        if (empty($list)) {
            exit();
        }
        /**
         * 转码
         */
        if (strtoupper ( CHARSET ) == 'GBK') {
            $list = Language::getUTF8 ( $list );
        }
        echo json_encode($list);
    }
    /**
     * ajax删除常用分类
     */
    public function ajax_stapledelOp() {
        Language::read ( 'member_store_goods_index' );
        $staple_id = intval($_GET ['staple_id']);
        if ($staple_id < 1) {
            echo json_encode ( array (
                'done' => false,
                'msg' => Language::get ( 'wrong_argument' )
            ) );
            die ();
        }
        /**
         * 实例化模型
         */

        $result = $this->model_staple->delStaple(array('staple_id' => $staple_id, 'member_id' => $_SESSION['member_id']));
        if ($result) {
            echo json_encode ( array (
                'done' => true
            ) );
            die ();
        } else {
            echo json_encode ( array (
                'done' => false,
                'msg' => ''
            ) );
            die ();
        }
    }
    /**
     * ajax选择常用商品分类
     */
    public function ajax_show_commOp() {
        $staple_id = intval($_GET['stapleid']);

        /**
         * 查询相应的商品分类id
         */
        $staple_info = $this->model_staple->getStapleInfo(array('staple_id' => intval($staple_id)), 'gc_id_1,gc_id_2,gc_id_3');
        if (empty ( $staple_info ) || ! is_array ( $staple_info )) {
            echo json_encode ( array (
                'done' => false,
                'msg' => ''
            ) );
            die ();
        }

        $list_array = array ();
        $list_array['gc_id'] = 0;
        $list_array['type_id'] = $staple_info['type_id'];
        $list_array['done'] = true;
        $list_array['one'] = '';
        $list_array['two'] = '';
        $list_array['three'] = '';

        $gc_id_1 = intval ( $staple_info['gc_id_1'] );
        $gc_id_2 = intval ( $staple_info['gc_id_2'] );
        $gc_id_3 = intval ( $staple_info['gc_id_3'] );

        /**
         * 查询同级分类列表
         */
        $model_goods_class = Model ( 'goods_class' );
        // 1级
        if ($gc_id_1 > 0) {
            $list_array['gc_id'] = $gc_id_1;
            $class_list = $model_goods_class->getGoodsClass($_SESSION['store_id'],0,1,$_SESSION['seller_group_id'],$_SESSION['seller_gc_limits']);
            if (empty ( $class_list ) || ! is_array ( $class_list )) {
                echo json_encode ( array (
                    'done' => false,
                    'msg' => ''
                ) );
                die ();
            }
            foreach ( $class_list as $val ) {
                if ($val ['gc_id'] == $gc_id_1) {
                    $list_array ['one'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:1, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="classDivClick" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                } else {
                    $list_array ['one'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:1, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                }
            }
        }
        // 2级
        if ($gc_id_2 > 0) {
            $list_array['gc_id'] = $gc_id_2;
            $class_list = $model_goods_class->getGoodsClass($_SESSION['store_id'], $gc_id_1, 2,$_SESSION['seller_group_id'],$_SESSION['seller_gc_limits']);
            if (empty ( $class_list ) || ! is_array ( $class_list )) {
                echo json_encode ( array (
                    'done' => false,
                    'msg' => ''
                ) );
                die ();
            }
            foreach ( $class_list as $val ) {
                if ($val ['gc_id'] == $gc_id_2) {
                    $list_array ['two'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:2, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="classDivClick" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                } else {
                    $list_array ['two'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:2, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                }
            }
        }
        // 3级
        if ($gc_id_3 > 0) {
            $list_array['gc_id'] = $gc_id_3;
            $class_list = $model_goods_class->getGoodsClass($_SESSION['store_id'], $gc_id_2, 3,$_SESSION['seller_group_id'],$_SESSION['seller_gc_limits']);
            if (empty ( $class_list ) || ! is_array ( $class_list )) {
                echo json_encode ( array (
                    'done' => false,
                    'msg' => ''
                ) );
                die ();
            }
            foreach ( $class_list as $val ) {
                if ($val ['gc_id'] == $gc_id_3) {
                    $list_array ['three'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:3, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="classDivClick" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                } else {
                    $list_array ['three'] .= '<li class="" onclick="selClass($(this));" data-param="{gcid:' . $val ['gc_id'] . ', deep:3, tid:' . $val ['type_id'] . '}" nctype="selClass"> <a class="" href="javascript:void(0)"><span class="has_leaf"><i class="icon-double-angle-right"></i>' . $val ['gc_name'] . '</span></a> </li>';
                }
            }
        }
        // 转码
        if (strtoupper ( CHARSET ) == 'GBK') {
            $list_array = Language::getUTF8 ( $list_array );
        }
        echo json_encode ( $list_array );
        die ();
    }
}
