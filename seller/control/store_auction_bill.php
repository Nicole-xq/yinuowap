<?php
/**
 * 拍卖订单结算
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class store_auction_billControl extends BaseSellerControl {
    /**
     * 每次导出多少条记录
     * @var unknown
     */
    const EXPORT_SIZE = 1000;

    public function __construct() {
        parent::__construct() ;
    }

    /**
     * 结算列表
     *
     */
    public function indexOp() {
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_store_id'] = $_SESSION['store_id'];
        if ($_GET['ob_id'] != '') {
            $condition['ob_id'] = intval($_GET['ob_id']);
        }
        if (is_numeric($_GET['bill_state'])) {
            $condition['ob_state'] = intval($_GET['bill_state']);
        }
        $bill_list = $model_bill->getOrderBillList($condition,'*',12,'ob_state asc,ob_id desc');
        Tpl::output('bill_list',$bill_list);
        Tpl::output('show_page',$model_bill->showpage());

        $model_store_ext = Model('store_extend');
        $ext_info = $model_store_ext->getStoreExtendInfo(array('store_id'=>$_SESSION['store_id']));
        Tpl::output('bill_cycle',$ext_info['bill_cycle'] ? $ext_info['bill_cycle'].'天' : '1个月');

        $this->profile_menu('list','list');
        Tpl::showpage('store_auction_bill.index');
    }

    /**
     * 查看结算单详细
     *
     */
    public function show_billOp(){
		$ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $model_order = Model('auction_order');
		$condition = array();
		$condition['ob_id'] = $ob_id;
		$condition['ob_store_id'] = $_SESSION['store_id'];
        $bill_info = $model_bill->getOrderBillInfo($condition);
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }

        $condition = array();
        $condition['order_state'] = ORDER_STATE_SUCCESS;
        $condition['store_id'] = $bill_info['ob_store_id'];
        if (preg_match('/^\d{8,20}$/',$_GET['query_order_no'])) {
            //取order_id
            $order_info = $model_order->getInfo(array('auction_order_sn'=>$_GET['query_order_no']),'auction_order_id');
            $condition['auction_order_id'] = $order_info['auction_order_id'];
        }

        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']) : null;
        if ($if_start_date || $if_end_date) {
            $condition['finnshed_time'] = array('time',array($start_unixtime,$end_unixtime));
        } else {
            $condition['finnshed_time'] = array('between',"{$bill_info['ob_start_date']},{$bill_info['ob_end_date']}");
        }
        $order_list = $model_order->getList($condition);
        Tpl::output('order_list',$order_list);
        Tpl::output('show_page',$model_order->showpage());
        Tpl::output('bill_info',$bill_info);
        Tpl::showpage('store_auction_order_bill.show');
    }

    /**
     * 打印结算单
     *
     */
    public function bill_printOp(){
		$ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_id'] = $ob_id;
        $condition['ob_state'] = BILL_STATE_SUCCESS;
		$condition['ob_store_id'] = $_SESSION['store_id'];
        $bill_info = $model_bill->getOrderBillInfo($condition);
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }

        Tpl::output('bill_info',$bill_info);
        Tpl::showpage('store_auction_order_bill.print','null_layout');
    }

    /**
     * 店铺确认出账单
     *
     */
    public function confirm_billOp(){
		$ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showDialog('参数错误','','error');
        }
        $model_bill = Model('auction_order_bill');
        $condition = array();
        $condition['ob_id'] = $ob_id;
        $condition['ob_store_id'] = $_SESSION['store_id'];
        $condition['ob_state'] = BILL_STATE_CREATE;
        $update = $model_bill->editOrderBill(array('ob_state'=>BILL_STATE_STORE_COFIRM),$condition);
        if ($update){
            showDialog('确认成功，请等待系统审核','','succ');
        }else{
            showDialog(L('nc_common_op_fail'),'reload','error');
        }
    }

    /**
     * 导出结算订单明细CSV
     *
     */
    public function export_orderOp(){
		$ob_id = intval($_GET['ob_id']);
        if ($ob_id <= 0) {
            showMessage('参数错误','','html','error');
        }

        $model_bill = Model('auction_order_bill');
		$condition = array();
		$condition['ob_store_id'] = $_SESSION['store_id'];
		$condition['ob_id'] = $ob_id;
        $bill_info = $model_bill->getOrderBillInfo($condition);
        if (!$bill_info){
            showMessage('参数错误','','html','error');
        }

        $model_order = Model('auction_order');
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['order_state'] = ORDER_STATE_SUCCESS;
        if (preg_match('/^\d{8,20}$/',$_GET['query_order_no'])) {
            //取order_id
            $order_info = $model_order->getInfo(array('auction_order_sn'=>$_GET['query_order_no']),'auction_order_id');
            $condition['auction_order_id'] = $order_info['auction_order_id'];
        }
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_GET['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_GET['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_GET['query_end_date']) : null;
        if ($if_start_date || $if_end_date) {
            $condition['finnshed_time'] = array('time',array($start_unixtime,$end_unixtime));
        } else {
            $condition['finnshed_time'] = array('between',"{$bill_info['ob_start_date']},{$bill_info['ob_end_date']}");
        }
        if (!is_numeric($_GET['curpage'])){
            $count = $model_order->getOrderCount($condition);
            $array = array();
            if ($count > self::EXPORT_SIZE ){
                //显示下载链接
                $page = ceil($count/self::EXPORT_SIZE);
                for ($i=1;$i<=$page;$i++){
                    $limit1 = ($i-1)*self::EXPORT_SIZE + 1;
                    $limit2 = $i*self::EXPORT_SIZE > $count ? $count : $i*self::EXPORT_SIZE;
                    $array[$i] = $limit1.' ~ '.$limit2 ;
                }
                Tpl::output('list',$array);
                Tpl::output('murl','index.php?act=store_auction_bill&op=show_bill&ob_id='.$_GET['ob_id']);
                Tpl::showpage('store_export.excel');
                exit();
            }else{
                //如果数量小，直接下载
                $data = $model_order->getList($condition,self::EXPORT_SIZE);
            }
        }else{
            //下载
            $limit1 = ($_GET['curpage']-1) * self::EXPORT_SIZE;
            $limit2 = self::EXPORT_SIZE;
            $data = $model_order->getList($condition,self::EXPORT_SIZE,'*','auction_order_id desc',"{$limit1},{$limit2}");
        }
        $export_data = array();
        $export_data[0] = array('订单编号','下单时间','成交时间','订单金额','佣金金额','买家','买家编号','商品');
        $order_totals = 0;
        $commis_totals = 0;
        $k = 0;
        foreach ($data as $v) {
            $export_data[$k+1][] = $v['auction_order_sn'];
            $export_data[$k+1][] = date('Y-m-d',$v['add_time']);
            $export_data[$k+1][] = date('Y-m-d',$v['finnshed_time']);
            $order_totals += $export_data[$k+1][] = $v['order_amount'];
            $commis_totals += $export_data[$k+1][] = ncPriceFormat($v['order_amount'] * $v['commis_rate']/100);
            $export_data[$k+1][] = $v['buyer_name'];
            $export_data[$k+1][] = $v['buyer_id'];
            $export_data[$k+1][] = $v['auction_name'];
            $k++;
        }
        $count = count($export_data);
        $export_data[$count][] = '合计';
        $export_data[$count][] = '';
        $export_data[$count][] = '';
        $export_data[$count][] = $order_totals;
        $export_data[$count][] = $commis_totals;
        $csv = new Csv();
        $export_data = $csv->charset($export_data,CHARSET,'gbk');
        $csv->filename = 'auction-order-list';
        $csv->export($export_data);
    }

    /**
     * 用户中心右边，小导航
     *
     * @param string    $menu_type  导航类型
     * @param string    $menu_key   当前导航的menu_key
     * @return
     */
    private function profile_menu($menu_type,$menu_key='') {
        $menu_array = array();
        switch ($menu_type) {
            case 'list':
                $menu_array = array(
                1=>array('menu_key'=>'list','menu_name'=>'拍卖订单结算', 'menu_url'=>'index.php?act=store_auction_bill'),
                );
                break;
        }
        Tpl::output('member_menu',$menu_array);
        Tpl::output('menu_key',$menu_key);
    }
}
