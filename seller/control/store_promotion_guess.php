<?php
/**
 * 用户中心-商品趣猜
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class store_promotion_guessControl extends BaseSellerControl {

    public function __construct() {
        parent::__construct();
        /**
         * 读取语言包
         */
        Language::read('member_layout,member_store_promotion_bundling');
    }

    public function indexOp() {
        $this->guess_act_listOp();
    }

    /**
     * 发布的活动列表
     */
    public function guess_listOp() {
        $model_guess = Model('p_guess');

            // 查询活动
            $where = array();
            $where['store_id'] = $_SESSION['store_id'];
            $where['gs_act_id'] = $_GET['gus_act_id'];
            if ($_GET['guess_name'] != '') {
                $where['gs_name'] = array('like', '%' . trim($_GET['guess_name']) . '%');
            }
            if (is_numeric($_GET['state'])) {
                $where['gs_state'] = $_GET['state'];
            }
            $guess_published = $model_guess->getguessCount(array('store_id' => $_SESSION['store_id'],'gs_act_id' => $_GET['gus_act_id']));
            $guess_list = $model_guess->getGuessList($where, '*', 'gs_id desc', 10, 0, $guess_published);
            $guess_list = array_under_reset($guess_list, 'gs_id');
            Tpl::output('show_page',$model_guess->showpage(2));
            if (!empty($guess_list)) {
                $gsid_array = array_keys($guess_list);
                $ggoods_array = $model_guess->getGuessGoodsList(array('gs_id' => array('in', $gsid_array)), 'min(gs_id) as gs_id,min(goods_id) as goods_id,min(goods_image) as goods_image,count(*) as count', 'gs_appoint desc', 'gs_id');
                $ggoods_array = array_under_reset($ggoods_array, 'gs_id');
                foreach ($guess_list as $key => $val) {
                    $guess_list[$key]['goods_id'] = $ggoods_array[$val['gs_id']]['goods_id'];
                    $guess_list[$key]['count'] = $ggoods_array[$val['gs_id']]['count'];
                    $guess_list[$key]['img'] = cthumb($ggoods_array[$val['gs_id']]['goods_image'], 60, $_SESSION['store_id']);
                }
            }
            Tpl::output('list', $guess_list);

            // 状态数组
            $state_array = array(0=>'已关闭',1=>'开启中',2=>'已结束');
            Tpl::output('state_array', $state_array);
            $this->profile_menu('guess_list', 'guess_list');
            Tpl::showpage('store_promotion_guess.list');
    }


    /**
     * 趣猜添加
     */
    public function guess_addOp() {
        /**
         * 实例化模型
         */
        $model_guess = Model('p_guess');
        // 验证套餐数量
        if (chksubmit()) {
            // 插入趣猜
            $data = array();
            if (isset($_POST['guess_id'])) {
                $data['gs_id'] = intval($_POST['guess_id']);
            }
            $data['gs_name'] = $_POST['guess_name'];
            $data['delivery_mechanism'] = $_POST['delivery_mechanism'];
            $data['gs_act_id'] = $_POST['gus_act_id'];
            $data['store_id'] = $_SESSION['store_id'];
            $data['store_name'] = $_SESSION['store_name'];
            $data['gs_img'] = $_POST['gs_img'];
            $data['gs_cost_price'] = $_POST['pub_cost_price'];
            $data['gs_discount_price'] = $_POST['discount_price'];
            $data['gs_estimate_price'] = $_POST['estimate_price'];
            $data['gs_min_price'] = $_POST['min_price'];
            $data['gs_max_price'] = $_POST['max_price'];
            $data['gs_right_price'] = $_POST['right_price'];
            $data['gs_freight_choose'] = $_POST['guess_freight_choose'];
            $data['gs_freight'] = $_POST['guess_freight_choose'] ?  0 : $_POST['guess_freight'];
            $data['gs_state'] = intval($_POST['state']);
            $data['points'] = intval($_POST['points']);
            $data['verify_state'] = 0;
            if (empty($data['gs_act_id'])) {
                showDialog('活动id不能为空', '', '', 'error');
            }   
            $model_act_guess = Model('p_act_guess');
            $act_guess_info = $model_act_guess->getGusActInfoByID($data['gs_act_id'],$_SESSION['store_id']);
            if (empty($act_guess_info)) {
                showDialog('活动参数有误', '', '', 'error');
            }
            if (($_POST['right_price'] < $_POST['min_price']) || ($_POST['right_price'] > $_POST['max_price'])) {
                showDialog('请正确填写竞猜区间', '', '', 'error');
            }
            $points_val = floatval($_POST['discount_price'] * $_POST['points_ratio']);
            if (($_POST['points'] > $_POST['points_max']) || ($_POST['points'] > $points_val)) {
                showDialog('请填正确填写赠送诺币', '', '', 'error');
            }
            $data['start_time'] = $act_guess_info['tstart'];
            $data['end_time'] = $act_guess_info['tend'];
            $return = $model_guess->addGuess($data, true);
            if (!$return) {
                showDialog(L('nc_common_op_fail'), '', '', 'error');
            }

            // 插入套餐商品
            $model_goods = Model('goods');
            $data_goods = array();
            $appoint_goodsid = false;
            $model_guess->delGuessGoods(array('gs_id' => intval($_POST['guess_id'])));
            if (!empty($_POST['goods']) && is_array($_POST['goods'])) {
                $commonid_array = array();
                foreach ($_POST['goods'] as $key => $val) {
                    // 验证是否为本店铺商品
                    $goods_info = $model_goods->getGoodsInfoByID($val['gid'], 'goods_id,goods_commonid,goods_name,goods_image,store_id');
                    if (empty($goods_info) || $goods_info['store_id'] != $_SESSION['store_id']) {
                        continue;
                    }
                    $commonid_array[] = $goods_info['goods_commonid'];
                    $data = array();
                    $data['gs_id'] = isset($_POST['guess_id']) ? intval($_POST['guess_id']) : $return;
                    $data['goods_id'] = $goods_info['goods_id'];
                    $data['goods_name'] = $goods_info['goods_name'];
                    $data['goods_image'] = $goods_info['goods_image'];
                    $data['gs_goods_price'] = ncPriceFormat($val['price']);
                    $data['gs_appoint'] = intval($val['appoint']);
                    if (!$appoint_goodsid && intval($val['appoint']) == 1) {
                        $appoint_goodsid = intval($val['gid']);
                    }
                    $data_goods[] = $data;
                }
            }
            // 插入数据
            $return = $model_guess->addGuessGoodsAll($data_goods);
            $result = Logic('goods')->goodsUnShow($commonid_array, $this->store_info['store_id'], $_SESSION['seller_id'], $_SESSION['seller_name']);
//            print_R($result);exit;
            // if (!isset($_POST['guess_id']) && !$appoint_goodsid) {
            //     // 自动发布动态
            //     // bl_id,bl_name,image_path,bl_discount_price,bl_freight_choose,bl_freight,store_id
            //     $data_array = array();
            //     $data_array['gs_id'] = $return;
            //     $data_array['goods_id'] = $appoint_goodsid;
            //     $data_array['gs_name'] = $data['gs_name'];
            //     $data_array['gs_img'] = empty($_POST['image_path']) ? '' : $_POST['image_path'][0];
            //     $data_array['gs_discount_price'] = $data['gs_discount_price'];
            //     $data_array['gs_freight_choose'] = $data['gs_freight_choose'];
            //     $data_array['gs_freight'] = $data['gs_freight'];
            //     $data_array['store_id'] = $_SESSION['store_id'];
            //     $this->storeAutoShare($data_array, 'bundling');
            // }

            $this->recordSellerLog('添加商品趣猜，名称：'.$data['gs_name'] . ' id：'.$return);
            showDialog(L('nc_common_op_succ'), urlSeller('store_promotion_guess', 'guess_list', array('gus_act_id' =>$_POST['gus_act_id'])), 'succ');
        }

        // 是否能使用编辑器
        $editor_multimedia = true;
        Tpl::output('editor_multimedia', $editor_multimedia);

        if (intval($_GET['guess_id']) > 0) {
            $guess_info = $model_guess->getGuessInfo(array('gs_id' => intval($_GET['guess_id']), 'store_id' => $_SESSION['store_id']));
            if ($guess_info['start_time'] < time()) {
                Tpl::output('can_edit',false);
            }else{
                Tpl::output('can_edit',true);
            }
                Tpl::output('guess_info', $guess_info);
            if (empty($guess_info['store_id'])) {
                showMessage(L( 'wrong_argument'), urlSeller('store_promotion_guess', 'guess_list'), '', 'error' );
            }
            $g_goods_list = $model_guess->getGuessGoodsList(array('gs_id' => intval($_GET['guess_id'])));
            if (!empty($g_goods_list)) {
                $goodsid_array = array();
                foreach ($g_goods_list as $val) {
                    $goodsid_array[] = $val['goods_id'];
                }
                $goods_list = Model('goods')->getGoodsList(array('goods_id' => array('in', $goodsid_array)), 'goods_id,goods_price,goods_image,goods_name');
                Tpl::output('goods_list', array_under_reset($goods_list, 'goods_id'));
            }
            Tpl::output('g_goods_list', $g_goods_list);
            // 输出导航
            self::profile_menu('guess_edit', 'guess_edit');
        } else {
            // 输出导航
            self::profile_menu('guess_add', 'guess_add');
        }
        Tpl::showpage('store_promotion_guess.add');
    }

    /**
     * 删除趣猜
     */
    public function drop_guessOp(){
        /**
         * 参数验证
         */
        $gsids = trim($_GET['guess_id']);
        if (empty($gsids)) {
            showDialog(L('para_error'), '', 'error');
        }
        $return = Model('p_guess')->delGuess($gsids, $_SESSION['store_id']);
        if ($return) {
            $this->recordSellerLog('删除商品趣猜，套餐id：'.$gsids);
            showDialog(L('bundling_delete_success'), 'reload', 'succ');
        } else {
            showDialog(L('bundling_delete_fail'), '', 'error');
        }
    }

    // 趣猜单场图片上传
    public function ajax_upload_image0Op() {
        $pic_name = '';
        $upload = new UploadFile();
        $file = current($_FILES);
        $uploaddir = ATTACH_PATH.DS.'pay_guess/single'.DS;
        $upload->set('max_size',C('image_max_filesize'));
        $upload->set('default_dir',$uploaddir);
        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
        if (!empty($file['tmp_name'])){
            $result = $upload->upfile(key($_FILES));
            if ($result){
                echo json_encode(array('state'=>true,'pic_name'=>$upload->file_name,'pic_url'=>UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$upload->file_name));
            } else {
                echo json_encode(array('state'=>false,'message'=>$upload->error));
            }
        }
    }

    /**
     * 用户中心右边，小导航
     *
     * @param string    $menu_type  导航类型
     * @param string    $menu_key   当前导航的menu_key
     * @return
     */
    private function profile_menu($menu_type,$menu_key='') {
        $menu_array = array();
        $menu_array[1] = array('menu_key' => 'guess_act_list','menu_name' => '活动列表', 'menu_url'=>urlSeller('store_promotion_guess', 'guess_act_list'));
        switch ($menu_type) {
            case 'guess_list':
                $menu_array[] = array('menu_key'=>'guess_list', 'menu_name'=> '趣猜列表', 'menu_url'=>'');
                break;
            case 'guess_add':
                $menu_array[] = array('menu_key'=>'guess_add', 'menu_name'=> '添加趣猜', 'menu_url'=>urlSeller('store_promotion_guess', 'guess_add'));
                    // 1=>array('menu_key'=>'guess_list', 'menu_name'=>Language::get('bundling_list'), 'menu_url'=>urlSeller('store_promotion_guess', 'guess_list')),
                break;
            case 'guess_edit':
                $menu_array[] =array('menu_key'=>'guess_edit', 'menu_name'=>Language::get('bundling_edit'), 'menu_url'=>urlSeller('store_promotion_guess', 'guess_edit'));
            break;
            case 'guess_act_add':
                $menu_array[] =array('menu_key'=>'guess_act_add', 'menu_name'=> '添加活动', 'menu_url'=>urlSeller('store_promotion_guess', 'guess_act_add'));
            break;
        }
        Tpl::output('member_menu',$menu_array);
        Tpl::output('menu_key',$menu_key);
    }


    /**
     * 发布的趣猜活动列表
     */
    public function guess_act_listOp()
    {
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        // if (($state = (int) $_GET['state']) > 0) {
        //     $condition['state'] = $state;
        // }
        if (strlen($gs_name = trim($_GET['gs_name']))) {
            $condition['name'] = array('like', '%' . $gs_name . '%');
        }

        $model_act_guess = Model('p_act_guess');
        Tpl::output('list', $model_act_guess->getGusActList($condition, 10, 'id desc'));
        Tpl::output('show_page', $model_act_guess->showpage());
        Tpl::output('gusActStates', $model_act_guess->getGusActStates());

        self::profile_menu('guess_act_list','guess_act_list');
        Tpl::showpage('store_promotion_gus_act.list');
    }

    /**
     * 添加趣猜活动
     */
    public function guess_act_addOp()
    {
        self::profile_menu('guess_act_add','guess_act_add');
        Tpl::showpage('store_promotion_gus_act.add');
    }

    // 趣猜活动图片上传
    public function ajax_upload_imageOp() {
        $pic_name = '';
        $upload = new UploadFile();
        $file = current($_FILES);
        $uploaddir = ATTACH_PATH.DS.'pay_guess'.DS;
        $upload->set('max_size',C('image_max_filesize'));
        $upload->set('default_dir',$uploaddir);
        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
        if (!empty($file['tmp_name'])){
            $result = $upload->upfile(key($_FILES));
            if ($result){
                echo json_encode(array('state'=>true,'pic_name'=>$upload->file_name,'pic_url'=>UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess'.DS.$upload->file_name));
            } else {
                echo json_encode(array('state'=>false,'message'=>$upload->error));
            }
        }
    }

    /**
     * 保存添加的趣猜活动
     */
    public function guess_act_saveOp()
    {
        // 验证输入
        $gus_act_name = trim($_POST['gus_act_name']);
        $tstart = strtotime($_POST['tstart']);
        $tend = strtotime($_POST['tend']);
        $gs_act_img = $_POST['gs_act_img'];

        if (empty($gus_act_name)) {
            showDialog('活动名称不能为空');
        }

        if ($tstart >= $tend) {
            showDialog('开始时间不能晚于结束时间');
        }

        if (empty($gs_act_img)) {
            showDialog('活动图片不能为空');
        }

        // 生成活动
        $actGuessModel = Model('p_act_guess');
        $param = array();
        $param['name'] = $gus_act_name;
        $param['gs_act_img'] = $gs_act_img;
        $param['tstart'] = $tstart;
        $param['tend'] = $tend;
        $param['store_id'] = $_SESSION['store_id'];
        $param['store_name'] = $_SESSION['store_name'];

        $result = $actGuessModel->addGuess($param);
        if ($result) {
            $this->recordSellerLog('添加加价购活动，活动名称：' . $gus_act_name . '，活动编号：' . $result);

            // 添加计划任务
            //Model('cron')->addCron(array('exetime' => $param['tend'], 'exeid' => $result, 'type' => 8), true);

            $editUrl = 'index.php?act=store_promotion_guess&op=guess_act_list';
            showDialog('趣猜活动添加成功', $editUrl, 'succ', '', 3);
        } else {
            showDialog('趣猜活动添加失败');
        }
    }
    
    /**
     * 趣猜活动删除
     */
    public function guess_act_delOp()
    {
        // 1.获取趣猜活动信息
        $gs_act_id = intval($_POST['gs_act_id']);
        $gs_act_info = Model('p_act_guess')->getGusActInfoByID($gs_act_id,$_SESSION['store_id']);
        if (empty($gs_act_info)) {
            showDialog('参数错误');
        }
        // 2.获取趣猜活动下属趣猜的gs_ids
        $gs_infos = Model('p_guess')->getGuessList1(array('gs_act_id' => $gs_act_id));
        $gs_del = true;
        if (!empty($gs_infos)) {
            $gs_ids = array();
            foreach ($gs_infos as $key => $val) {
                $gs_ids[] = $val['gs_id'];
            }
            $gs_ids_str = implode(',',$gs_ids);
            $gs_del = Model('p_guess')->delGuess($gs_ids_str);
        }
        // 3.删除趣猜活动
        $act_gs_del = Model('p_act_guess')->delGuessAct(array('id' => $gs_act_id));
        // 4.删除趣猜活动下所属的趣猜

        if ($gs_del && $act_gs_del) {
            $this->recordSellerLog('删除趣猜活动，活动名称：' . $act_guess_info['name'] . '活动编号：' . $gs_act_id);
            showDialog(L('nc_common_op_succ'), 'index.php?act=store_promotion_guess&op=guess_act_list', 'succ');
        } else {
            showDialog(L('nc_common_op_fail'));
        }
    }
}
