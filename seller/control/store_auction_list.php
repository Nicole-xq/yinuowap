<?php
/**
 * 拍品列表
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2016/12/27 0027
 * Time: 11:39
 * @author ADKi
 */

use Shopnc\Tpl;

defined('InShopNC') or exit ('Access Invalid!');

class store_auction_listControl extends BaseSellerControl
{
    public function __construct()
    {
        parent::__construct ();
        Language::read ('member_store_goods_index');
        if ($_SESSION['open_auction'] == 0) {
            showDialog('该店铺尚未开通发布拍品功能','index.php?act=store_auction&op=index','error');
        }
    }

    public function indexOp()
    {
        $this->auction_listOp();
//        $this->auctions_storageOp();
    }

    //已发布的拍品
    public function auctions_storageOp()
    {
        $model_auctions = Model('auctions');
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];

        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $where['auction_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $where['auction_id'] = intval($_GET['keyword']);
                    break;
            }
        }

        //权限组对应分类权限判断
        if (!$_SESSION['seller_gc_limits'] && $_SESSION['seller_group_id']) {
            $gc_list = Model('seller_group_bclass')->getSellerGroupBclasList(array('group_id'=>$_SESSION['seller_group_id']),'','','gc_id','gc_id');
            $where['gc_id'] = array('in',array_keys($gc_list));
        }

        switch ($_GET['type']) {
            // 审核失败的拍品
            case 'lock_up':
                $this->profile_menu('auction_list', 'auctions_lockup');
                $auction_list = $model_auctions->getAuctionsLockUpList($where);
                break;
            // 等待审核的拍品
            case 'wait_verify':
                $this->profile_menu('auction_list', 'auctions_verify');
                $auction_list = $model_auctions->getAuctionsWaitVerifyList($where);
                break;
            case 'success':
                $this->profile_menu('auction_list', 'auctions_success');
                $auction_list = $model_auctions->getAuctionsSuccessList($where);
                break;
            case 'fail_bid':
                $this->profile_menu('auction_list', 'auctions_fail_bid');
                $auction_list = $model_auctions->getAuctionsFailBidList($where);
                break;
            case 'preview':
                $this->profile_menu('auction_list', 'auctions_preview');
                $auction_list = $model_auctions->getAuctionPreviewList($where);
                break;
            case 'the_auction':
                $this->profile_menu('auction_list', 'auctions_the_auction');
                $auction_list = $model_auctions->getAuctionsTheAuctionList($where);
                break;
            // 申请通过的拍品
            default:
                $this->profile_menu('auction_list', 'auctions_storage');
                $auction_list = $model_auctions->getAuctionsOfflineList($where);
                break;
        }

        Tpl::output('show_page', $model_auctions->showpage());
        Tpl::output('auction_list', $auction_list);

        // 商品分类
        $store_goods_class = Model('store_goods_class')->getClassTree(array('store_id' => $_SESSION['store_id'], 'stc_state' => '1'));
        Tpl::output('store_goods_class', $store_goods_class);

        Tpl::showpage('store_auctions_list.offline');
    }

    /**
     * 编辑拍品页面
     */
    public function edit_auctionOp() {
        $auction_id = $_GET['auction_id'];
        if ($auction_id <= 0) {
            showMessage(L('wrong_argument'), '', 'html', 'error');
        }
        $model_auctions = Model('auctions');
        $auction_info = $model_auctions->getAuctionsInfoByID($auction_id);
        if (empty($auction_info) || $auction_info['store_id'] != $_SESSION['store_id'] || $auction_info['auction_lock'] == 1) {
            showMessage(L('wrong_argument'), '', 'html', 'error');
        }
        $where = array('auction_id' => $auction_id, 'store_id' => $_SESSION['store_id']);
        //权限组对应分类权限判断
        if (!$_SESSION['seller_gc_limits'] && $_SESSION['seller_group_id']) {
            $gc_list = Model('seller_group_bclass')->getSellerGroupBclasList(array('group_id'=>$_SESSION['seller_group_id']),'','','gc_id','gc_id');
            if (!in_array($auction_info['gc_id'],array_keys($gc_list))) {
                showMessage('您所在的组无权操作该分类下的商品','', 'html', 'error');
            }
        }
        $auction_info['auction_custom'] = unserialize($auction_info['auction_custom']);
        if ($auction_info['auction_mobile_body'] != '') {
            $auction_info['mb_body'] = unserialize($auction_info['auction_mobile_body']);
            if (is_array($auction_info['mb_body'])) {
                $mobile_body = '[';
                foreach ($auction_info['mb_body'] as $val ) {
                    $mobile_body .= '{"type":"' . $val['type'] . '","value":"' . $val['value'] . '"},';
                }
                $mobile_body = rtrim($mobile_body, ',') . ']';
            }
            $auction_info['auction_mobile_body'] = $mobile_body;
        }
        Tpl::output('auction', $auction_info);

        if (intval($_GET['class_id']) > 0) {
            $auction_info['gc_id'] = intval($_GET['class_id']);
        }
        $goods_class = Model('goods_class')->getGoodsClassLineForTag($auction_info['gc_id']);
        Tpl::output('goods_class', $goods_class);

        $model_type = Model('type');
        // 获取类型相关数据
        $typeinfo = $model_type->getAttr($goods_class['type_id'], $_SESSION['store_id'], $auction_info['gc_id']);
        list($spec_json, $spec_list, $attr_list, $brand_list) = $typeinfo;
        Tpl::output('attr_list', $attr_list);

        // 取得拍品输入的属性
        $auction_array = $model_auctions->getAuctionList($where);

        if (is_array($auction_array) && !empty($auction_array)) {

            // 取得已选择了哪些商品的属性
            $attr_checked_l = $model_type->typeRelatedList ( 'auctions_attr_index', array (
                'auction_id' => intval ( $auction_array[0]['auction_id'] )
            ), 'attr_value_id' );
            if (is_array ( $attr_checked_l ) && ! empty ( $attr_checked_l )) {
                $attr_checked = array ();
                foreach ( $attr_checked_l as $val ) {
                    $attr_checked [] = $val ['attr_value_id'];
                }
            }
            Tpl::output ( 'attr_checked', $attr_checked );

        }
        // 自定义属性
        $custom_list = Model('type_custom')->getTypeCustomList(array('type_id' => $goods_class['type_id']));
        $custom_list = array_under_reset($custom_list, 'custom_id');
        Tpl::output('custom_list', $custom_list);

        // 是否能使用编辑器
        if(checkPlatformStore()){ // 平台店铺可以使用编辑器
            $editor_multimedia = true;
        } else {    // 三方店铺需要
            $editor_multimedia = false;
            if ($this->store_grade['sg_function'] == 'editor_multimedia') {
                $editor_multimedia = true;
            }
        }
        Tpl::output ( 'editor_multimedia', $editor_multimedia );

        $menu_promotion = array(
            'lock' => $auction_info['goods_lock'] == 1 ? true : false,
        );
        $this->profile_menu('edit_detail','edit_detail', $menu_promotion);
        Tpl::output('edit_auctions_sign', true);
        Tpl::showpage('store_auctions_add.step2');
    }

    /**
     * 编辑商品保存
     */
    public function edit_save_auctionOp() {
        if(!empty($_FILES['auction_video_file']['name'])){
            //获取视频数据，提交时候保存视频
            $datas = $this->_getGoodsVideo($_FILES);
            if($datas['status'] == 'success'){
                $_POST['auction_video'] = $datas['auction_video'];
            }elseif($datas['status'] == 'fail'){
                showDialog($datas['error'],'','error');
            }
        }

        $logic_auction = Logic('auction');

        $result =  $logic_auction->updateAuctions(
            $_POST,
            $_SESSION['store_id'],
            $_SESSION['store_name'],
            $this->store_info['store_state'],
            $_SESSION['seller_id'],
            $_SESSION['seller_name'],
            $_SESSION['bind_all_gc']
        );

        if ($result['state']) {
            //提交事务
            showDialog(L('nc_common_op_succ'), $_POST['ref_url'], 'succ');
        } else {
            //回滚事务
            showDialog($result['msg'], urlSeller('store_goods_online', 'index'));
        }
    }

    //获取视频数据,加入店铺的专辑内
    public function _getGoodsVideo($file){
        $data = array();
        $store_id = $_SESSION['store_id'];
        if (!empty($file['auction_video_file']['name'])) {
            $upload = new UploadVideoFile();
            $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . 'goods_video'   );
            $upload->set('fprefix', $store_id);
            $upload->set('max_size' , 10240);
            $result = $upload->upfile('auction_video_file',true);
            if ($result) {
                $data['status'] = 'success';
                $data['auction_video'] = $upload->file_name;
            } else {
                $data['status'] = 'fail';
                $data['error'] = $upload->error;
            }
        }

        // 判断图片数量是否超限
        $model_album = Model('video_album');
        $album_limit = $this->store_grade['sg_album_limit'];
        if ($album_limit > 0) {
            $album_count = $model_album->getCount(array('store_id' => $store_id));
            if ($album_count >= $album_limit) {
                return callback(false, '您上传图片数达到上限，请升级您的店铺或跟管理员联系');
            }
        }

        $class_info = $model_album->getOne(array('store_id' => $store_id, 'is_default' => 1), 'video_album_class');

        $video_path = $upload->file_name;

        // 存入视频
        $video = explode('.', $_FILES["auction_video_file"]["name"]);
        $insert_array = array();
        $insert_array['video_name'] = $video['0'];
        $insert_array['video_tag'] = '';
        $insert_array['video_class_id'] = $class_info['video_class_id'];
        $insert_array['video_cover'] = $video_path;
        $insert_array['video_size'] = intval($_FILES['auction_video_file']['size']);
        $insert_array['upload_time'] = TIMESTAMP;
        $insert_array['store_id'] = $store_id;
        Model('upload_video_album')->add($insert_array);

        return $data;
    }

    /**
     * 编辑图片
     */
    public function edit_imageOp() {
        $auction_id = intval($_GET['auction_id']);
        if ($auction_id <= 0) {
            showMessage(L('wrong_argument'), urlSeller('seller_center'), 'html', 'error');
        }
        $model_auction = Model('auctions');
        $auction_info = $model_auction->getAuctionsInfoByID($auction_id);

        if ($auction_info['store_id'] != $_SESSION['store_id'] || $auction_info['goods_lock'] == 1) {
            showMessage(L('wrong_argument'), urlSeller('seller_center'), 'html', 'error');
        }

        $img_array = $model_auction->getAuctionsImageList(array('auction_id' => $auction_id));

        Tpl::output('img', $img_array);

        Tpl::output('auction_id', $auction_id);

        $menu_promotion = array(
            'lock' => $auction_info['goods_lock'] == 1 ? true : false,
        );
        $this->profile_menu('edit_detail', 'edit_image', $menu_promotion);
        Tpl::output('edit_auctions_sign', true);
        Tpl::showpage('store_auctions_add.step3');
    }

    /**
     * 保存商品图片
     */
    public function edit_save_imageOp() {
        if (chksubmit()) {
            $auction_id = intval($_POST['auction_id']);
            $rs = Logic('auction')->editSaveImage($_POST['img'], $auction_id, $_SESSION['store_id'], $_SESSION['seller_id'], $_SESSION['seller_name']);
            if ($rs['state']) {
                showDialog(L('nc_common_op_succ'), $_POST['ref_url'], 'succ');
            } else {
                showDialog(L('nc_common_save_fail'), urlSeller('store_goods_online', 'index'));
            }
        }
    }

    /**
     * 编辑分类
     */
    public function edit_classOp() {
        // 实例化商品分类模型
        $model_goodsclass = Model('goods_class');
        // 商品分类
        $goods_class = $model_goodsclass->getGoodsClass($_SESSION['store_id']);

        // 常用商品分类
        $model_staple = Model('goods_class_staple');
        $param_array = array();
        $param_array['member_id'] = $_SESSION['member_id'];
        $staple_array = $model_staple->getStapleList($param_array);

        Tpl::output('staple_array', $staple_array);
        Tpl::output('goods_class', $goods_class);

        Tpl::output('auction_id', $_GET['auction_id']);
        $this->profile_menu('edit_class', 'edit_class');
        Tpl::output('edit_auctions_sign', true);
        Tpl::showpage('store_auctions_add.step1');
    }

    /**
     * 删除商品
     */
    public function drop_auctionOp() {
        $auction_id = intval($_GET['auction_id']);
        $auction_id_array = explode(',', $auction_id);
        $result = Logic('auction')->auctionDrop($auction_id_array, $_SESSION['store_id'], $_SESSION['seller_id'], $_SESSION['seller_name']);
        if ($result['state']) {
            // 添加操作日志
            $this->recordSellerLog('删除拍品，ID：'.$auction_id);
            showDialog("拍品删除成功", 'reload', 'succ');
        } else {
            showDialog("拍品删除失败", '', 'error');
        }
    }


    /**
     * 子页面上方小导航
     *
     * @param string $menu_type 导航类型
     * @param string $menu_key 当前导航的menu_key
     * @param boolean $allow_promotion
     * @return
     */
    private function profile_menu($menu_type,$menu_key, $allow_promotion = array()) {
        $menu_array = array();
        switch ($menu_type) {
            case 'auction_list':
                $menu_array = array(
                    array('menu_key' => 'auctions_storage',    'menu_name' => "发布成功",   'menu_url' => urlSeller('store_auction_list', 'index')),
                    array('menu_key' => 'auctions_lockup',     'menu_name' => "审核失败",     'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'lock_up'))),
                    array('menu_key' => 'auctions_verify',     'menu_name' => "等待审核",    'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'wait_verify'))),
                    array('menu_key' => 'auctions_success',     'menu_name' => "拍卖成功",    'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'success'))),
                    array('menu_key' => 'auctions_fail_bid',     'menu_name' => "拍卖流拍",    'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'fail_bid'))),
                    array('menu_key' => 'auctions_preview',     'menu_name' => "正在预展",    'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'preview'))),
                    array('menu_key' => 'auctions_the_auction',     'menu_name' => "正在拍卖",    'menu_url' => urlSeller('store_auction_list', 'index', array('type' => 'the_auction'))),
                );
                break;
            case 'edit_detail':
                if ($allow_promotion['lock'] === false) {
                    $menu_array = array(
                        array('menu_key' => 'edit_detail',  'menu_name' => '编辑拍品', 'menu_url' => urlSeller('store_auction_list', 'edit_auction', array('auction_id' => $_GET['auction_id'], 'ref_url' => $_GET['ref_url']))),
                        array('menu_key' => 'edit_image',   'menu_name' => '编辑图片', 'menu_url' => urlSeller('store_auction_list', 'edit_image', array('auction_id' => $_GET['auction_id'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer())))),
                    );
                }
                break;
            case 'edit_class':
                $menu_array = array(
                    array('menu_key' => 'edit_class',   'menu_name' => '选择分类', 'menu_url' => urlSeller('store_auction_list', 'edit_class', array('auction_id' => $_GET['auction_id'], 'ref_url' => $_GET['ref_url']))),
                    array('menu_key' => 'edit_detail',  'menu_name' => '编辑拍品', 'menu_url' => urlSeller('store_auction_list', 'edit_auction', array('auction_id' => $_GET['auction_id'], 'ref_url' => $_GET['ref_url']))),
                    array('menu_key' => 'edit_image',   'menu_name' => '编辑图片', 'menu_url' => urlSeller('store_auction_list', 'edit_image', array('auction_id' => $_GET['auction_id'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer())))),
                );
                break;
        }
        Tpl::output ( 'member_menu', $menu_array );
        Tpl::output ( 'menu_key', $menu_key );
    }

    /**
     * 拍品列表
     */
    public function auction_listOp(){
        $model_auctions = Model('auctions');
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];

        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $where['auction_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $where['auction_id'] = intval($_GET['keyword']);
                    break;
            }
        }

        //权限组对应分类权限判断
        if (!$_SESSION['seller_gc_limits'] && $_SESSION['seller_group_id']) {
            $gc_list = Model('seller_group_bclass')->getSellerGroupBclasList(array('group_id'=>$_SESSION['seller_group_id']),'','','gc_id','gc_id');
            $where['gc_id'] = array('in',array_keys($gc_list));
        }
        $auction_list = $model_auctions->getAuctionList($where,'','','auction_id desc',5,5,'');
        if(!empty($auction_list)){
            foreach($auction_list as &$v){
                $v['auction_state'] = $this->get_auction_state($v);
                $special_info = Model('auction_special')->getSpecialInfo(array('special_id'=>$v['special_id']));
                $v['special_name'] = '';
                if($special_info){
                    $v['special_name'] = $special_info['special_name'];
                }
            }
        }
        Tpl::output('show_page', $model_auctions->showpage());
        Tpl::output('auction_list', $auction_list);

        // 商品分类
        $store_goods_class = Model('store_goods_class')->getClassTree(array('store_id' => $_SESSION['store_id'], 'stc_state' => '1'));
        Tpl::output('store_goods_class', $store_goods_class);

        Tpl::showpage('store_auctions_list.offline');
    }

    public function auction_detailOp(){
        $auction_id = $_GET['auction_id'];
        $model_auction = Model('auctions');
        $auction_info = $model_auction->getAuctionsInfo(array('auction_id'=>$auction_id));
        $special_info = Model('auction_special')->getSpecialInfo(array('special_id'=>$auction_info['special_id']));
        $auction_info['special_name'] = $special_info['special_name'];
        $auction_info['auction_state'] = $this->get_auction_state($auction_info);
        $auction_info['order_info'] = Model('order')->getOrderInfo(array('auction_id'=>$auction_id),array(),'buyer_name,payment_time,add_time');
//        print_R($auction_info);exit;
//        echo date('Y-m-d H:i:s',$auction_info['order_info']['add_time']);exit;
        Tpl::output('auction_info',$auction_info);
        Tpl::showPage('auction_detail');
    }

    /**
     * 拍品状态
     * @param $info
     * @return string
     */
    private function get_auction_state($info){
        if(time() < $info['auction_preview_start']){
            $re = '等待预展';
        }elseif(time() < $info['auction_start_time']){
            $re = '预展中';
        }elseif(time() < $info['auction_end_time']){
            $re = '拍卖中';
        }else{
            $re = '拍卖结束';
        }
        return $re;
    }

}