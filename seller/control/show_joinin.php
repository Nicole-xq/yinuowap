<?php
/**
 * 店铺开店
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit('Access Invalid!');
class show_joininControl extends BaseHomeControl {
    public function __construct() {
        parent::__construct();
        Tpl::setLayout('joinin_layout');
        $this->check_joinin_state();
    }
    /**
     * 店铺开店页
     *
     */
    public function indexOp() {
        Language::read("home_login_index");
        $code_info = C('store_joinin_pic');
        $info['pic'] = array();
        if(!empty($code_info)) {
            $info = unserialize($code_info);
        }
        Tpl::output('pic_list',$info['pic']);//首页图片
        Tpl::output('show_txt',$info['show_txt']);//贴心提示
        $model_help = Model('help');
        $condition['type_id'] = '1';//入驻指南
        $help_list = $model_help->getHelpList($condition,'',4);//显示4个
        Tpl::output('help_list',$help_list);
        // Tpl::output('article_list','');//底部不显示文章分类
        Tpl::output('show_sign','joinin');
        Tpl::output('html_title',C('site_name').' - '.'入驻');
        Tpl::showpage('store_joinin');
    }


    public function joinOp(){
        Tpl::output('agreement', $document_info['doc_content']);
        Tpl::output('step', '');
        Tpl::output('yindao_style', '1');
        Tpl::output('sub_step', 'step');
        Tpl::output('join_type', 'join');
        Tpl::showpage('joinin_apply');
        exit;
    }


    public function finshOp(){

        $model_store_joinin = Model('store_joinin');
        $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$_SESSION['member_id']));
        Tpl::output('yindao_style', '6');
        Tpl::output('joinin_detail',$joinin_detail);

        $type = null;
        switch (intval($joinin_detail['joinin_state'])) {
            case STORE_JOIN_STATE_NEW:      //等待审核
                $type = 1;
                break;
            case STORE_JOIN_STATE_VERIFY_FAIL:  //审核失败
                $type = 2;
                break;
            case STORE_JOIN_STATE_VERIFY_SUCCESS: //初审成功
                $type = 3;
                break;
            case STORE_JOIN_STATE_PAY_FAIL:     //付款审核失败
                $type = 4;
                break;
            case STORE_JOIN_STATE_FINAL:     //开店成功
                $type = 5;
                break;
            case STORE_JOIN_STATE_PAY:     //完成付款
                $type = 6;
                break;


        }

        Tpl::output('type', $type);
        Tpl::output('step', $step);
        Tpl::output('sub_step', 'finsh');
        Tpl::output('join_type', 'join');
        Tpl::showpage('joinin_apply');
        exit();
    }


    public function check_joinin_state(){

        $model_store_joinin = Model('store_joinin');
        $joinin_detail = $model_store_joinin->getOne(array('member_id'=>$_SESSION['member_id']));
        if(!empty($joinin_detail)) {
            switch (intval($joinin_detail['joinin_state'])) {
                case STORE_JOIN_STATE_NEW:          //等待审核
                case STORE_JOIN_STATE_VERIFY_FAIL:  //审核失败
                case STORE_JOIN_STATE_VERIFY_SUCCESS: //初审成功
                case STORE_JOIN_STATE_PAY_FAIL:     //付款审核失败
                case STORE_JOIN_STATE_PAY:          //完成付款
                case STORE_JOIN_STATE_FINAL:        //开店成功
//                    $control_name = '';
//                    if($joinin_detail['joinin_type'] == 1){
//                        $control_name = 'artist_joinin';
//                    }else{
//                        $control_name = 'store_joinin';
//                    }

                    $this->finshOp();

                    break;
            }
        }
    }

}
