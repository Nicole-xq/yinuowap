<?php
/**
 * 拍卖
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2016 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */
use Shopnc\Tpl;


defined('InShopNC') or exit ('Access Invalid!');
class store_auctionControl extends BaseSellerControl {
    public function __construct() {
        parent::__construct();
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfo(array('store_id'=>$_SESSION['store_id']));
    }

    //申请拍卖
    public function indexOp() {
        if(C('auction_isuse') == 1) {
            $model_store = Model('store');
            $store_info = $model_store->getStoreInfo(array('store_id' => $_SESSION['store_id']));
            if ($store_info['store_type'] == 0) {
                $store_vendue = Model('store_vendue');
                $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id' => $_SESSION['store_id']));
                Tpl::output('store_vendue_info', $store_vendue_info);
                if ($store_vendue_info['store_apply_state'] == 10) {
                    $message = '申请拍卖已经提交，请等待管理员审核';
                } elseif ($store_vendue_info['store_apply_state'] == 20) {
                    $message = '审核成功，支付拍卖保证金后开启拍卖功能';
                } elseif ($store_vendue_info['store_apply_state'] == 30) {
                    $message = '审核失败:' . $store_vendue_info['check_message'];
                } elseif ($store_vendue_info['store_apply_state'] == 40) {
                    $message = '开启拍卖成功';
                }elseif ($store_vendue_info['store_apply_state'] == 11) {
                    $message = '付款完成，等待平台确认';
                }
                Tpl::output('check_message', $message);
                $model_store = Model('store');
                $store_info = $model_store->getStoreInfo(array('store_id' => $_SESSION['store_id']));
                $model_store_class = Model('store_class');
                $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$store_info['sc_id']));
                $store_info['vendue_bail'] = $sc_info['vendue_bail'];
                Tpl::output('store_info', $store_info);
                $model_area = Model('area');
                $city_info = $model_area->getAreaInfo(array('area_id' => $store_vendue_info['city_id']));
                $area_info = $model_area->getAreaInfo(array('area_id' => $store_vendue_info['area_id']));
                Tpl::output('city_info', $city_info);
                Tpl::output('area_info', $area_info);
                $model_seller = Model('seller');
                $seller_info = $model_seller->getSellerInfo(array('member_id' => $store_vendue_info['store_vendue_contact']));
                Tpl::output('seller_info', $seller_info);
                $seller_list = $model_seller->getSellerList(array('store_id' => $store_info['store_id']), '', 'seller_id asc');//账号列表
                Tpl::output('seller_list', $seller_list);
                Tpl::showpage('store_auction.add');
            } elseif ($store_info['store_type'] == 1) {
                $artist_vendue = Model('artist_vendue');
                $artist_vendue_info = $artist_vendue->getArtist_StoreInfo(array('store_id' => $_SESSION['store_id']));
                if (!empty($artist_vendue_info)) {
                    $artist_vendue_info['artist_represent'] = unserialize($artist_vendue_info['artist_represent']);
                    $artist_vendue_info['artist_awards'] = unserialize($artist_vendue_info['artist_awards']);
                    $artist_vendue_info['artist_works'] = unserialize($artist_vendue_info['artist_works']);
                }

                if ($artist_vendue_info['store_apply_state'] == 10) {
                    $message = '申请拍卖已经提交，请等待管理员审核';
                } elseif ($artist_vendue_info['store_apply_state'] == 20) {
                    $message = '审核成功，支付拍卖保证金后开启拍卖功能';
                } elseif ($artist_vendue_info['store_apply_state'] == 30) {
                    $message = '审核失败:' . $artist_vendue_info['check_message'];
                } elseif ($artist_vendue_info['store_apply_state'] == 40) {
                    $message = '开启拍卖成功';
                }elseif ($artist_vendue_info['store_apply_state'] == 11) {
                    $message = '付款完成，等待平台确认';
                }
                Tpl::output('check_message', $message);
                Tpl::output('artist_vendue_info', $artist_vendue_info);
                $model_area = Model('area');
                $city_info = $model_area->getAreaInfo(array('area_id' => $artist_vendue_info['city_id']));
                $area_info = $model_area->getAreaInfo(array('area_id' => $artist_vendue_info['area_id']));
                Tpl::output('city_info', $city_info);
                Tpl::output('area_info', $area_info);
                $model_seller = Model('seller');
                $seller_info = $model_seller->getSellerInfo(array('member_id' => $artist_vendue_info['store_vendue_contact']));
                Tpl::output('seller_info', $seller_info);
                $seller_list = $model_seller->getSellerList(array('store_id' => $store_info['store_id']), '', 'seller_id asc');//账号列表
                Tpl::output('seller_list', $seller_list);
                $model_store_class = Model('store_class');
                $sc_info = $model_store_class->getStoreClassInfo(array('sc_id'=>$store_info['sc_id']));
                $store_info['vendue_bail'] = $sc_info['vendue_bail'];
                Tpl::output('store_info', $store_info);
                Tpl::showpage('artist_auction.add');
            }
        }else{
            showDialog('对不起，平台没有开启拍卖功能','index.php?act=seller_center&op=index','error');
        }


    }

    //保存艺术家信息
    public function save_artist_vendueOp(){
        $model_artist_vendue = Model('artist_vendue');
        $insert_array = array();
        $data_array = array();
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $data_array['oz_type'] = $_POST['vendue_type'];
        $data_array['city_id'] = $_POST['city_id'];
        $data_array['area_id'] = $_POST['area_id'];
        $data_array['store_id'] = $_SESSION['store_id'];
        $data_array['store_apply_state'] = 10;
        $data_array['store_vendue_contact'] = $_POST['store_vendue_contact'];
        $data_array['add_time'] = TIMESTAMP;
        $data_array['store_name'] = $_SESSION['store_name'];
        $data_array['is_pay'] = 0;
        $data_array['check_message'] = '';
        if(empty($store_vendue_info)){
            $result1 = $store_vendue->addStore_vendue($data_array);
        }else{
            $result1 = $store_vendue->editStore_vendue($data_array,array('store_vendue_id'=>$store_vendue_info['store_vendue_id']));
        }
        $store_vendue_info1 = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $artist_vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('store_vendue_id'=>$store_vendue_info1['store_vendue_id']));
        $upload = new UploadFile();
        if(!empty($_POST['work'])){
            foreach($_POST['work'] as $k=>$v){
                if (!empty($_FILES["work_image_$k"]['name'])){
                    $upload->set('default_dir', ATTACH_VENDUE);
                    $upload->set('thumb_ext',   '');
                    $upload->set('file_name','');
                    $upload->set('ifremove',false);
                    $result = $upload->upfile("work_image_$k");
                    if ($result){
                        $_POST['work'][$k]['work_pic'] = $upload->file_name;
                    }else {
                        showDialog($upload->error);
                    }
                }
            }
        }

        if (!empty($_FILES['artist_image_file']['name'])){
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('artist_image_file');
            if ($result){
                $_POST['vendue_logo'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        $insert_array['artist_name'] = $_POST['artist_name'];
        $insert_array['artist_job_title'] = $_POST['artist_job_title'];
        $insert_array['artist_resume'] = html_entity_decode($_POST['artist_resume']);
        $insert_array['artist_represent'] = serialize($_POST['repres']);
        $insert_array['artist_awards'] = serialize($_POST['awards']);
        $insert_array['artist_works'] = serialize($_POST['work']);
        if(empty($artist_vendue_info)){
            $insert_array['artist_image'] = !empty($_POST['vendue_logo']) ? $_POST['vendue_logo'] : '';
        }else{
            $insert_array['artist_image'] = !empty($_POST['vendue_logo']) ? $_POST['vendue_logo'] : $artist_vendue_info['artist_image'];
        }
        $insert_array['store_vendue_id'] = $store_vendue_info1['store_vendue_id'];
        if(empty($artist_vendue_info)){
            $insert_array['member_id'] = $this->store_info['member_id'];
            $result = $model_artist_vendue->addArtist_vendue($insert_array);
        }else{
            $result = $model_artist_vendue->editArtist_vendue($insert_array,array('artist_vendue_id'=>$artist_vendue_info['artist_vendue_id']));
        }
        if($result){
            showDialog('申请成功，等待审核','','succ');
        }

    }

    /**
     * 商家申请拍卖
     */
    public function save_store_vendueOp() {
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $insert_array = array();

        $insert_array['oz_type'] = $_POST['vendue_type'];
        $insert_array['store_vendue_intro'] = html_entity_decode($_POST['vendue_intro']);
        $insert_array['city_id'] = $_POST['city_id'];
        $insert_array['area_id'] = $_POST['area_id'];
        $insert_array['store_id'] = $_SESSION['store_id'];
        $insert_array['store_apply_state'] = 10;
        $insert_array['store_vendue_contact'] = $_POST['store_vendue_contact'];
        $insert_array['add_time'] = TIMESTAMP;
        $insert_array['store_name'] = $_SESSION['store_name'];
        $insert_array['is_pay'] = 0;
        $insert_array['check_message'] = '';

        if(empty($store_vendue_info)){
            $result = $store_vendue->addStore_vendue($insert_array);
        }else{
            $result = $store_vendue->editStore_vendue($insert_array,array('store_vendue_id'=>$store_vendue_info['store_vendue_id']));
        }

        if($result){
            showDialog('申请成功，等待审核','','succ');
        }

    }

    public function save_pay_storeOp(){
        $param = array();
        $model_store_vendue = Model('store_vendue');
        $param['paying_money_certificate'] = $this->upload_image('paying_money_certificate');
        $param['paying_money_certif_exp'] = $_POST['paying_money_certif_exp'];
        $param['store_apply_state'] = 11;
        if(empty($param['paying_money_certificate'])) {
            showMessage('请上传付款凭证','','','error');
        }

        $result = $model_store_vendue->editStore_vendue($param,array('store_id'=>$_SESSION['store_id']));
        if($result){
            showDialog('提交成功，等待平台确认','','succ');
        }
    }

    private function upload_image($file) {
        $pic_name = '';
        $upload = new UploadFile();
        $uploaddir = ATTACH_PATH.DS.'vendue'.DS;
        $upload->set('default_dir',$uploaddir);
        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
        if (!empty($_FILES[$file]['name'])){
            $result = $upload->upfile($file);
            if ($result){
                $pic_name = $upload->file_name;
                $upload->file_name = '';
            }
        }
        return $pic_name;
    }


    //编辑艺术家信息
    public function edit_artistOp(){
        $model_artist_vendue = Model('artist_vendue');
        $artist_info = $model_artist_vendue->getArtist_StoreInfo(array('store_id' => $_SESSION['store_id']));
        if (!empty($artist_info)) {
            $artist_info['artist_represent'] = unserialize($artist_info['artist_represent']);
            $artist_info['artist_awards'] = unserialize($artist_info['artist_awards']);
            $artist_info['artist_works'] = unserialize($artist_info['artist_works']);
        }
        Tpl::output('artist_vendue_info', $artist_info);
        $model_area = Model('area');
        $city_info = $model_area->getAreaInfo(array('area_id' => $artist_info['city_id']));
        $area_info = $model_area->getAreaInfo(array('area_id' => $artist_info['area_id']));
        Tpl::output('city_info', $city_info);
        Tpl::output('area_info', $area_info);
        $model_seller = Model('seller');
        $seller_info = $model_seller->getSellerInfo(array('member_id' => $artist_info['store_vendue_contact']));
        Tpl::output('seller_info', $seller_info);
        $seller_list = $model_seller->getSellerList(array('store_id' => $_SESSION['store_id']), '', 'seller_id asc');//账号列表
        Tpl::output('seller_list', $seller_list);
        Tpl::showpage('artist_auction.edit');
    }

    //保存艺术家信息
    public function save_artist_editOp(){
        $model_artist_vendue = Model('artist_vendue');
        $insert_array = array();
        $data_array = array();
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $data_array['oz_type'] = $_POST['vendue_type'];
        $data_array['city_id'] = $_POST['city_id'];
        $data_array['area_id'] = $_POST['area_id'];
        $data_array['store_id'] = $_SESSION['store_id'];
        $data_array['store_vendue_contact'] = $_POST['store_vendue_contact'];
//        $data_array['add_time'] = TIMESTAMP;
        $data_array['store_name'] = $_SESSION['store_name'];

        $result1 = $store_vendue->editStore_vendue($data_array,array('store_vendue_id'=>$store_vendue_info['store_vendue_id']));

        $store_vendue_info1 = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $artist_vendue_info = $model_artist_vendue->getArtist_vendueInfo(array('store_vendue_id'=>$store_vendue_info1['store_vendue_id']));
        $upload = new UploadFile();
        if(!empty($_POST['work'])){
            foreach($_POST['work'] as $k=>$v){
                if (!empty($_FILES["work_image_$k"]['name'])){
                    $upload->set('default_dir', ATTACH_VENDUE);
                    $upload->set('thumb_ext',   '');
                    $upload->set('file_name','');
                    $upload->set('ifremove',false);
                    $result = $upload->upfile("work_image_$k");
                    if ($result){
                        $_POST['work'][$k]['work_pic'] = $upload->file_name;
                    }else {
                        showDialog($upload->error);
                    }
                }
            }
        }

        if (!empty($_FILES['artist_image_file']['name'])){
            $upload->set('default_dir', ATTACH_VENDUE);
            $upload->set('thumb_ext',   '');
            $upload->set('file_name','');
            $upload->set('ifremove',false);
            $result = $upload->upfile('artist_image_file');
            if ($result){
                $_POST['vendue_logo'] = $upload->file_name;
            }else {
                showDialog($upload->error);
            }
        }
        $insert_array['artist_name'] = $_POST['artist_name'];
        $insert_array['artist_job_title'] = $_POST['artist_job_title'];
        $insert_array['artist_resume'] = html_entity_decode($_POST['artist_resume']);
        $insert_array['artist_represent'] = serialize($_POST['repres']);
        $insert_array['artist_awards'] = serialize($_POST['awards']);
        $insert_array['artist_works'] = serialize($_POST['work']);
        if(empty($artist_vendue_info)){
            $insert_array['artist_image'] = !empty($_POST['vendue_logo']) ? $_POST['vendue_logo'] : '';
        }else{
            $insert_array['artist_image'] = !empty($_POST['vendue_logo']) ? $_POST['vendue_logo'] : $artist_vendue_info['artist_image'];
        }
        $insert_array['store_vendue_id'] = $store_vendue_info1['store_vendue_id'];
        if(empty($artist_vendue_info)){
            $insert_array['member_id'] = $this->store_info['member_id'];
            $result = $model_artist_vendue->addArtist_vendue($insert_array);
        }else{
            $result = $model_artist_vendue->editArtist_vendue($insert_array,array('artist_vendue_id'=>$artist_vendue_info['artist_vendue_id']));
        }
        if($result){
            showDialog('编辑成功','','succ');
        }

    }

    //编辑商家信息
    public function edit_storeOp(){
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id' => $_SESSION['store_id']));
        Tpl::output('store_vendue_info', $store_vendue_info);
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfo(array('store_id' => $_SESSION['store_id']));
        Tpl::output('store_info', $store_info);
        $model_area = Model('area');
        $city_info = $model_area->getAreaInfo(array('area_id' => $store_vendue_info['city_id']));
        $area_info = $model_area->getAreaInfo(array('area_id' => $store_vendue_info['area_id']));
        Tpl::output('city_info', $city_info);
        Tpl::output('area_info', $area_info);
        $model_seller = Model('seller');
        $seller_info = $model_seller->getSellerInfo(array('member_id' => $store_vendue_info['store_vendue_contact']));
        Tpl::output('seller_info', $seller_info);
        $seller_list = $model_seller->getSellerList(array('store_id' => $store_info['store_id']), '', 'seller_id asc');//账号列表
        Tpl::output('seller_list', $seller_list);
        Tpl::showpage('store_auction.edit');
    }

    /**
     * 商家申请拍卖
     */
    public function save_store_editOp() {
        $store_vendue = Model('store_vendue');
        $store_vendue_info = $store_vendue->getStore_vendueInfo(array('store_id'=>$_SESSION['store_id']));
        $insert_array = array();

        $insert_array['oz_type'] = $_POST['vendue_type'];
        $insert_array['store_vendue_intro'] = html_entity_decode($_POST['vendue_intro']);
        $insert_array['city_id'] = $_POST['city_id'];
        $insert_array['area_id'] = $_POST['area_id'];
        $insert_array['store_id'] = $_SESSION['store_id'];
        $insert_array['store_vendue_contact'] = $_POST['store_vendue_contact'];
        $insert_array['store_name'] = $_SESSION['store_name'];

        $result = $store_vendue->editStore_vendue($insert_array,array('store_vendue_id'=>$store_vendue_info['store_vendue_id']));

        if($result){
            showDialog('编辑成功','','succ');
        }

    }











}
