<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>
<form method="get" action="index.php" target="_self">
    <table class="search-form">
        <input type="hidden" name="act" value="store_order" />
        <input type="hidden" name="op" value="index" />
        <?php if ($_GET['state_type']) { ?>
            <input type="hidden" name="state_type" value="<?php echo $_GET['state_type']; ?>" />
        <?php } ?>
        <tr>
            <td>&nbsp;</td>
            <th><?php echo $lang['store_order_buyer'];?></th>
            <td class="w100"><input type="text" class="text w80" name="buyer_name" value="<?php echo $_GET['buyer_name']; ?>" /></td>
            <th><?php echo $lang['store_order_order_sn'];?></th>
            <td class="w160"><input type="text" class="text w150" name="order_sn" value="<?php echo $_GET['order_sn']; ?>" /></td>
            <td class="w70 tc"><label class="submit-border">
                    <input type="submit" class="submit" value="<?php echo $lang['store_order_search'];?>" />
                </label></td>
        </tr>
    </table>
</form>
<table class="ncsc-default-table order">
    <thead>
    <tr>
        <th class="w10"></th>
        <th colspan="2"><?php echo $lang['store_order_goods_detail'];?></th>
        <th class="w100">成交价</th>
        <th class="w100"><?php echo $lang['store_order_buyer'];?></th>
        <th class="w100"><?php echo $lang['store_order_sum'];?></th>
        <th class="w90">交易状态</th>
        <th class="w120">交易操作</th>
    </tr>
    </thead>
    <?php if (is_array($output['order_list']) and !empty($output['order_list'])) { ?>
    <?php foreach($output['order_list'] as $order_id => $order) { ?>
    <tbody>
    <tr>
        <td colspan="20" class="sep-row"></td>
    </tr>
    <tr>
        <th colspan="20">
            <span class="ml10">
                <?php echo $lang['store_order_order_sn'].$lang['nc_colon'];?>
                <em><?php echo $order['auction_order_sn']; ?></em>
                <?php if ($order['order_from'] == 2){?>
                    <i class="icon-mobile-phone"></i>
                <?php }?>
            </span>
            <span>
                <?php echo $lang['store_order_add_time'].$lang['nc_colon'];?>
                <em class="goods-time"><?php echo date("Y-m-d H:i:s",$order['add_time']); ?></em>
            </span>
        </th>
    </tr>
    <tr>
        <td class="bdl"></td>
        <td class="w70">
            <div class="ncsc-goods-thumb">
                <a href="<?php echo $order['extend_order_goods']['auction_url'];?>" target="_blank">
                    <img src="<?php echo $order['extend_order_goods']['image_60_url'];?>" onMouseOver="toolTip('<img src=<?php echo $order['extend_order_goods']['image_240_url'];?>>')" onMouseOut="toolTip()"/>
                </a>
            </div>
        </td>
        <td class="tl">
            <dl class="goods-name">
                <dt>
                    <a target="_blank" href="<?php echo $order['extend_order_goods']['auction_url'];?>"><?php echo $order['auction_info']['auction_name']; ?></a>
                </dt>
            </dl>
        </td>
        <td>
            <p><?php echo ncPriceFormat($order['auction_info']['current_price']); ?></p>
        <!-- S 合并TD -->
            <td class="bdl" rowspan="1"><div class="buyer"><?php echo $order['buyer_name'];?>
                    <p member_id="<?php echo $order['buyer_id'];?>">
                        <?php if(!empty($order['extend_member']['member_qq'])){?>
                            <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $order['extend_member']['member_qq'];?>&site=qq&menu=yes" title="QQ: <?php echo $order['extend_member']['member_qq'];?>"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo $order['extend_member']['member_qq'];?>:52" style=" vertical-align: middle;"/></a>
                        <?php }?>
                        <?php if(!empty($order['extend_member']['member_ww'])){?>
                            <a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&uid=<?php echo $order['extend_member']['member_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=<?php echo $order['extend_member']['member_ww'];?>&site=cntaobao&s=2&charset=<?php echo CHARSET;?>" alt="Wang Wang" style=" vertical-align: middle;" /></a>
                        <?php }?>
                    </p>
                    <div class="buyer-info"> <em></em>
                        <div class="con">
                            <h3><i></i><span><?php echo $lang['store_order_buyer_info'];?></span></h3>
                            <dl>
                                <dt><?php echo $lang['store_order_receiver'].$lang['nc_colon'];?></dt>
                                <dd><?php echo $order['extend_order_common']['reciver_name'];?></dd>
                            </dl>
                            <dl>
                                <dt><?php echo $lang['store_order_phone'].$lang['nc_colon'];?></dt>
                                <dd><?php echo $order['extend_order_common']['phone'];?></dd>
                            </dl>
                            <dl>
                                <dt>地址<?php echo $lang['nc_colon'];?></dt>
                                <dd><?php echo $order['extend_order_common']['address'];?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </td>
            <td class="bdl" rowspan="1">
                <p class="ncsc-order-amount"><?php echo $order['order_amount']; ?></p>
                <p class="goods-pay" title="<?php echo $lang['store_order_pay_method'].$lang['nc_colon'];?><?php echo $order['payment_name']; ?>"><?php echo $order['payment_name']; ?></p>
            </td>
            <td class="bdl bdr" rowspan="1">
                <p><?php echo $order['state_desc']; ?>
                    <?php if($order['evaluation_time']) { ?>
                        <br/>
                        <?php echo $lang['store_order_evaluated'];?>
                    <?php } ?>
                </p>

                <!-- 订单查看 -->

                <p><a href="index.php?act=store_auction_order&op=show_order&auction_order_id=<?php echo $order['auction_order_id'];?>&margin_id=<?php echo $order['margin_id'];?>" target="_blank"><?php echo $lang['store_order_view_order'];?></a></p>

                <!-- 物流跟踪 -->

                <p>
                    <?php if ($order['if_deliver']) { ?>
                        <a href='index.php?act=store_auction_order&op=search_deliver&auction_order_id=<?php echo $order['auction_order_id'];?>&margin_id=<?php echo $order['margin_id'];?>'><?php echo $lang['store_order_show_deliver'];?></a>
                    <?php } ?>
                </p>
            </td>

                <!-- 发货 -->
            <td class="bdl bdr" rowspan="1">
                <?php if ($order['if_store_send']) { ?>

                    <p><a href="javascript:void(0)"
                          class="ncbtn ncbtn-mint mt10"
                          nc_type="dialog"
                          dialog_title="发货信息"
                          dialog_id="auction_send"
                          dialog_width="480"
                          uri="<?php echo urlSeller('store_auction_order', 'send', array('order_id' => $order['auction_order_id']));?>"
                          title="<?php echo $lang['store_order_send'];?>">
                            <i class="icon-truck"></i><?php echo $lang['store_order_send'];?>
                        </a>
                    </p>
                <?php } ?>

                <!-- 审核退款 -->
                <?php if ($order['if_store_cancel']) { ?>

                    <p>
                        <a href="javascript:void(0)"
                          class="ncbtn ncbtn-grapefruit mt10"
                          nc_type="dialog"
                          dialog_title="审核退款"
                          dialog_id="auction_cancel"
                          dialog_width="480"
                          uri="<?php echo urlSeller('store_auction_order', 'cancel', array('order_id' => $order['auction_order_id'], 'order_sn' => $order['auction_order_sn']));?>"
                          title="确认退款"
                        >
                            <i class="icon-edit"></i>审核退款
                        </a>
                    </p>
                <?php } ?>

                <?php if ($order['if_system_refund']) { ?>
                    <p>平台审核退款中</p>
                <?php } ?>

                <!-- 锁定 -->

                <?php if ($order['if_lock']) {?>
                    <p><?php echo '退款中';?></p>
                <?php }?>
            </td>
        <!-- E 合并TD -->
    </tr>

    <?php } } else { ?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <?php if (is_array($output['order_list']) and !empty($output['order_list'])) { ?>
        <tr>
            <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
        </tr>
    <?php } ?>
    </tfoot>
</table>
<script charset="utf-8" type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript">
    $(function(){
        $('#query_start_date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#query_end_date').datepicker({dateFormat: 'yy-mm-dd'});
        $('.checkall_s').click(function(){
            var if_check = $(this).attr('checked');
            $('.checkitem').each(function(){
                if(!this.disabled)
                {
                    $(this).attr('checked', if_check);
                }
            });
            $('.checkall_s').attr('checked', if_check);
        });
        $('#skip_off').click(function(){
            url = location.href.replace(/&skip_off=\d*/g,'');
            window.location.href = url + '&skip_off=' + ($('#skip_off').attr('checked') ? '1' : '0');
        });
    });
</script>
