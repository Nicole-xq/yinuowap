<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
    .article-goods-list li, .goods-search-list li, .article-link-list li, .article-select-box .article-search-list li {
        display: block;
        clear: both;
        padding: 6px;
        margin: 0;
        border: dashed #E7E7E7;
        border-width: 0 0 1px 0;
        box-shadow: none;
    }
    .goods-search-list li dl, .article-goods-list li dl {
        display: inline-block;
        height: 40px;
        position: relative;
        z-index: 1;
    }
    .goods-search-list li dt, .article-goods-list li dt {
        line-height: 20px;
        font-weight: 600;
        margin-left: 48px;
    }
    .goods-search-list li dd.image, .article-goods-list li dd.image {
        background-color: #FFF;
        width: 38px;
        height: 38px;
        border: solid 1px #D8D8D8;
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
    }
    .goods-search-list li dd.image img, .article-goods-list li dd.image img {
        max-width: 38px;
        max-height: 38px;
    }
    fieldset, img {
        border: medium none;
    }

    .article-goods-list li:hover, .article-link-list li:hover {
        background-color: #FFECD9;
        cursor: pointer;
    }
    .article-goods-list li i, .article-link-list li i, .goods-search-list li i, .article-search-list li i {
        line-height: 20px;
        color: #F30;
        background: url(../images/cms_member_pic.png) no-repeat -490px -35px;
        display: none;
        float: right;
        padding-left: 16px;
    }
    .goods-search-list li i, .article-select-box .article-search-list li i { color: #1E82EF; background-position: -490px 6px;}
    .article-goods-list li:hover i, .article-link-list li:hover i, .goods-search-list li:hover i, .article-select-box .article-search-list li:hover i { display: block;}
	input[type="button"]{ font-size: 12px; line-height: 30px; font-weight: bold; color: #FFF; background-color: #48CFAE; display: block; height: 30px; padding: 0 20px; border-radius: 3px; border: none 0; cursor: pointer;margin-left: 20px }
	input[type="button"]:hover{ text-decoration: none; color: #FFF; background-color: #36BC9B;}
</style>
<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>
<div class="ncsc-form-default">
    <form method="post"  action="index.php?act=store_inform&op=store_inform_save" enctype="multipart/form-data" id="inform_form">
        <input type="hidden" name="form_submit" value="ok" />
        <input id="inform_id" name="inform_id" type="hidden" value="<?php if(!empty($output['store_inform_info'])) echo $output['store_inform_info']['inform_id'];?>" />
        <dl>
            <dt>资讯链接：</dt>
            <dd>
                <ul id="article_goods_list" class="article-goods-list">
                    <?php if(!empty($output['store_inform_info']['inform_list'])) { ?>
                    <?php foreach($output['store_inform_info']['inform_list'] as $value) { ?>
                    <li nctype="btn_goods_select">
                        <dl>
            <dt class="name"> <a target="_blank" href="<?php echo $value['url'];?>"><?php echo $value['title'];?></a> </dt>
            <dd nctype="btn_goods_select" class="image"> <img src="<?php echo $value['image'];?>" title="<?php echo $value['title'];?>"> </dd>
        </dl>
        <i>选择删除相关商品</i>
        <input type="hidden" value="<?php echo $value['url'];?>" name="article_goods_url[]">
        <input type="hidden" value="<?php echo $value['title'];?>" name="article_goods_title[]">
        <input type="hidden" value="<?php echo $value['image'];?>" name="article_goods_image[]">
        <input type="hidden" value="<?php echo $value['time'];?>" name="article_goods_time[]">
        <input type="hidden" value="<?php echo $value['id'];?>" name="article_id[]">
        <input type="hidden" value="<?php echo $value['abstract'];?>" name="article_goods_abstract[]">


        </li>
        <?php } ?>
        <?php } ?>
        </ul>

        <input id="goods_search_keyword" class="text w380 fl" name="goods_search_keyword" type="text" />
        <input id="btn_goods_search" class="btn-type-s fl" type="button" value="添加" />
        <div id="div_goods_select"> </div>
<!--                <input class="text w400" name="inform_link" type="text"  value="" />-->
                <p class="hint"></p>
            </dd>
        </dl>

        <div class="bottom">
            <label class="submit-border"><input type="submit" class="submit" id="submitBtn" value="提交" /></label>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_goods_search").click(function(){
                var search_keyword = $("#goods_search_keyword").val();
                $("#goods_search_keyword").val("");
                if(search_keyword != "") {
                        var url = encodeURIComponent(search_keyword);
                        $.getJSON("index.php?act=store_inform&op=inform_info_by_url", { url: url}, function(data){
                            if(data.result == "true") {
                                var temp = '<li nctype="btn_goods_select"><dl>';
                                temp += '<dt class="name"><a href="'+data.url+'" target="_blank">'+data.title+'</a></dt>';
                                temp += '<dd class="image"><img title="'+data.title+'" src="'+data.image+'" /></dd>';
                                temp += '</dl><i>选择删除相关商品</i>';
                                temp += '<input name="article_goods_url[]" value="'+data.url+'" type="hidden" />';
                                temp += '<input name="article_goods_title[]" value="'+data.title+'" type="hidden" />';
                                temp += '<input name="article_goods_image[]" value="'+data.image+'" type="hidden" />';
                                temp += '<input name="article_goods_time[]" value="'+data.time+'" type="hidden" />';
                                temp += '<input name="article_id[]" value="'+data.id+'" type="hidden" />';
                                temp += '<input name="article_goods_abstract[]" value="'+data.abstract+'" type="hidden" />';


                                temp += '</li>';
                                $("#article_goods_list").append(temp);
                            } else {
                                alert(data.message);
                            }
                        });

                }

        });
        //商品添加
//        $("#goods_search_list [nctype='btn_goods_select']").live("click",function(){
//                var temp = '<li nctype="btn_goods_select">'+$(this).html();
//                temp += '<input name="article_goods_url[]" value="'+$(this).attr("goods_url")+'" type="hidden" />';
//                temp += '<input name="article_goods_title[]" value="'+$(this).attr("goods_title")+'" type="hidden" />';
//                temp += '<input name="article_goods_image[]" value="'+$(this).attr("goods_image")+'" type="hidden" />';
//                temp += '<input name="article_goods_abstract[]" value="'+$(this).attr("goods_abstract")+'" type="hidden" />';
//                temp += '<input name="article_goods_time[]" value="'+$(this).attr("goods_time")+'" type="hidden" />';
//                temp += '<input name="article_id[]" value="'+$(this).attr("goods_")+'" type="hidden" />';
//                temp += '</li>';
//                $("#article_goods_list").append(temp);
//
//        });

        //商品删除
        $("#article_goods_list [nctype='btn_goods_select']").live("click",function(){
            $(this).remove();
        });

    });
</script>
