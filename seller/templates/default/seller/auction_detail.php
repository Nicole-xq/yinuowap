<?php defined('InShopNC') or exit('Access Invalid!');?>


<div class="ncsc-form-default">
    <dl>
      <dt>标题:</dt>
      <dd>
        <p><?php echo $output['auction_info']['auction_name']; ?></p>
        </dd>
    </dl>
<!--    <dl>-->
<!--      <dt>--><?php //echo $lang['store_setting_store_zy'].$lang['nc_colon']; ?><!--</dt>-->
<!--      <dd>-->
<!--          <textarea name="store_zy" rows="2" class="textarea w400"  maxlength="50" >--><?php //echo $output['store_info']['store_zy'];?><!--</textarea>-->
<!--        <p class="hint">--><?php //echo $lang['store_create_store_zy_hint'];?><!--</p>-->
<!--      </dd>-->
<!--    </dl>-->
    <dl>
      <dt>拍品首图:</dt>
      <dd>
        <div class="ncsc-upload-thumb store-logo" nctype="store_label">
          <p><?php if(empty($output['auction_info']['auction_image'])){ ?>
          <i class="icon-picture"></i>
          <?php } else {?>
          <img src="<?php echo cthumb($output['auction_info']['auction_image'], 240);?>" />
          <?php }?></p>
        </div>
      </dd>
    </dl>
    <dl>
      <dt>关联专场:</dt>
      <dd>
            <p><?=$output['auction_info']['special_name'];?></p>
            <p>预展时间:<?=date('Y-m-d H:i:s',$output['auction_info']['auction_preview_start']);?></p>
            <p>起拍时间:<?=date('Y-m-d H:i:s',$output['auction_info']['auction_start_time']);?></p>
            <p>结束时间:<?=date('Y-m-d H:i:s',$output['auction_info']['auction_end_time']);?></p>
      </dd>
    </dl>
      <dl class="setup store-logo">
          <dt>拍卖价格:</dt>
          <dd>
              <p>起拍价:<?=$output['auction_info']['auction_start_price'];?></p>
              <p>加浮价:<?=$output['auction_info']['auction_increase_range'];?></p>
              <p>保留价:<?=$output['auction_info']['auction_reserve_price'];?></p>
              <p>保证金:<?=$output['auction_info']['auction_bond'];?></p>
          </dd>
      </dl>
      <dl>
          <dt>拍卖状态:</dt>
          <dd>
        <?= $output['auction_info']['auction_state'];?>
          </dd>
      </dl>
      <dl>
          <dt>拍卖结果:</dt>
          <dd>
        <?= $output['auction_info']['is_liupai'] == 1?'流拍':'成交';?>
          </dd>
      </dl>
      <?php if($output['auction_info']['is_liupai'] == 0&&$output['auction_info']['state'] == 1){?>
      <dl>
          <dt>成交价:</dt>
          <dd>
              <?= $output['auction_info']['current_price'];?>
          </dd>
      </dl>
      <?php }?>
      <dl>
      <dt>出价次数:</dt>
      <dd>
        <p>
          <?= $output['auction_info']['bids_number'];?>
        </p>
      </dd>
    </dl>
    <dl>
        <dt>成交信息:</dt>
        <dd>
            <?php if(!empty($output['auction_info']['order_info'])){?>
                <p class="hint">成交日期:<?=date('Y-m-d H:i:s',$output['auction_info']['order_info']['add_time']);?></p>
                <p class="hint">成交人:<?=$output['auction_info']['order_info']['buyer_name'];?></p>
                <p class="hint">付款状态:<?=$output['auction_info']['order_info']['payment_time']?'已付款':'未付款';?></p>
            <?php }else{?>
                <p class="hint">无订单信息</p>
            <?php }?>
      </dd>
    </dl>
    <div class="bottom">
        <a href="<?php echo urlSeller('store_auction_list', 'index');?>" class="ncbtn ncbtn-mint">返回</a>
      </div>
</div>

