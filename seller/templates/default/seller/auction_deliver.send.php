<div class="eject_con">
    <div id="warning" class="alert alert-error"></div>
    <form id="send_form" method="post" target="_parent" action="index.php?act=store_auction_order&op=send">
        <input type="hidden" name="order_id" value="<?php echo $output['order_info']['auction_order_id']; ?>" />
        <dl>
            <dt><i class="required">*</i>订单号</dt>
            <dd>
                <p><?php echo $output['order_info']['auction_order_sn']; ?></p>
            </dd>
        </dl>
        <dl>
            <dt><i class="required">*</i>快递单号</dt>
            <dd>
                <input class="text w200" type="text" name="invoice_no" id="invoice_no" value="<?php echo $output['order_info']['invoice_no']; ?>" />
            </dd>
        </dl>
        <dl>
            <dt>快递公司</dt>
            <dd>
                <select name="express_id" id="express_id">
                    <option value="0"><?php echo $lang['store_create_please_choose'];?></option>
                    <?php if(!empty($output['express_list']) && is_array($output['express_list'])){ ?>
                        <?php foreach ($output['express_list'] as $val) { ?>
                            <option value="<?php echo $val['id']; ?>" <?php if ($val['id'] == $output['order_info']['express_id']) { ?>selected="selected"<?php } ?>><?php echo $val['e_name']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </dd>
        </dl>
        <div class="bottom">
            <label class="submit-border"><input type="submit" class="submit" value="发货" /></label>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#send_form').validate({
            errorLabelContainer: $('#warning'),
            invalidHandler: function(form, validator) {
                $('#warning').show();
            },
            submitHandler:function(form){
                ajaxpost('send_form', '', '', 'onerror')
            },
            rules : {
                invoice_no : {
                    required : true
                },
                express_id : {
                    number   : true,
                    min: 1
                }
            },
            messages : {
                invoice_no : {
                    required : '<i class="icon-exclamation-sign"></i>请填写快递单号'

                },
                express_id  : {
                    number   : '<i class="icon-exclamation-sign"></i>请选择快递公司',
                    min : '<i class="icon-exclamation-sign"></i>请选择快递公司'
                }
            }
        });
    });
</script>
