<?php defined('InShopNC') or exit('Access Invalid!');?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<style>
.pic_list .small_pic ul li {
	height: 100px;
}
.ui-sortable-helper {
	border: dashed 1px #F93;
	box-shadow: 2px 2px 2px rgba(153,153,153, 0.25);
	filter: alpha(opacity=75);
	-moz-opacity: 0.75;
	opacity: .75;
	cursor: ns-resize;
}
.ui-sortable-helper td {
	background-color: #FFC !important;
}
.ajaxload {
	display: block;
	width: 16px;
	height: 16px;
	margin: 100px 300px;
}
.ncsc-goods-default-pic .goodspic-uplaod .upload-thumb {
    width: 520px;
    height: 220px;
}
.ncsc-goods-default-pic .goodspic-uplaod .upload-thumb img {
    max-width: 520px;
    max-height: 220px;
}
</style>
<input id="level2_flag" type="hidden" value="false" />
<input id="level3_flag" type="hidden" value="false" />
<div class="wrap">
  <div class="tabmenu">
    <?php include template('layout/submenu');?>
  </div>
  <div class="ncsc-form-default"> 
    <!-- 说明 -->
    
    <form id="add_form" method="post" action="index.php?act=store_promotion_guess&op=guess_add">
      <input type="hidden" name="form_submit" value="ok" />
      <input type="hidden" name="points_max"  />
      <input type="hidden" name="points_ratio" />
      <input type="hidden" name="gus_act_id" value="<?php if(empty($output['guess_info']['gs_act_id'])) {echo $_GET['gus_act_id'];} else {echo $output['guess_info']['gs_act_id'];}?>" />
      <?php if (!empty($output['guess_info'])){?>
      <input type="hidden" name="guess_id" value="<?php echo $output['guess_info']['gs_id'];?>" />
      <?php }?>
      <input type="hidden" name="pub_cost_price" id="pub_cost_price" value="<?php echo $output['guess_info']['gs_cose_price'];?>"/>
      <dl>
        <dt><i class="required">*</i><?php echo '趣猜名称'.$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="guess_name" name="guess_name" type="text" maxlength="25" class="w400 text" value="<?php echo $output['guess_info']['gs_name'];?>"  <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>
            <span></span> </p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo '送拍机构'.$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="delivery_mechanism" name="delivery_mechanism" type="text" maxlength="25" class="w400 text" value="<?php echo $output['guess_info']['delivery_mechanism'];?>"  <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>
            <span></span> </p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo '中奖折扣价'.$lang['nc_colon'];?></dt>
        <dd>
          <input id="discount_price" name="discount_price" type="text" readonly style="background:#E7E7E7 none;" class="text w60 mr5" value="<?php echo $output['guess_info']['gs_discount_price'];?>" />
          <?php echo $lang['currency_zh'];?> <span></span>
          <p class="hint mt10"><?php echo $lang['bundling_cost_price'];?><span nctype="cost_price" class="price mr5 ml5"><?php echo $output['guess_info']['gs_cost_price'];?></span>（已选商品总计）</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i><?php echo $lang['bundling_goods'].$lang['nc_colon'];?></dt>
        <dd>
          <p>
            <input id="bundling_goods" type="hidden" value="" name="bundling_goods">
            <span></span></p>
          <table class="ncsc-default-table mb15">
            <thead>
              <tr>
                <th class="w70">趣猜商品</th>
                <th class="tl" colspan="2">商品名称</th>
                <th class="w90"><?php echo $lang['bundling_cost_price'];?></th>
                <th class="w90">中奖折扣价</th>
                <th class="w90"><?php echo $lang['nc_common_button_operate'];?></th>
              </tr>
            </thead>
            <tbody nctype="bundling_data"  class="bd-line tip" title="<?php echo $lang['bundling_add_goods_explain'];?>">
              <tr style="display:none;">
                <td colspan="20" class="norecord"><div class="no-promotion"><i class="zh"></i><span>趣猜套装还未选择添加商品。</span></div></td>
              </tr>
              <?php if(!empty($output['g_goods_list'])){?>
              <?php foreach($output['g_goods_list'] as $val){?>
              <?php if (isset($output['goods_list'][$val['goods_id']])) {?>
              <tr id="bundling_tr_<?php echo $val['goods_id']?>" class="off-shelf">
                <input type="hidden" value="<?php echo $val['g_goods_id'];?>" name="goods[<?php echo $val['goods_id'];?>][bundling_goods_id]" />
                <input type="hidden" value="<?php echo $val['goods_id'];?>" name="goods[<?php echo $val['goods_id'];?>][gid]" nctype="goods_id">
                <td class="w70"><input type="checkbox" name="goods[<?php echo $val['goods_id'];?>][appoint]" value="1" <?php if ($val['gs_appoint'] == 1) {?>checked="checked"<?php }?> <?php if ($output['can_edit'] === false){echo 'disabled';}?>></td>
                <td class="w50"><div class="shelf-state"><div class="pic-thumb"><img src="<?php echo cthumb($output['goods_list'][$val['goods_id']]['goods_image'], 60, $_SESSION['store_id']);?>" ncname="<?php echo $output['goods_list'][$val['goods_id']]['goods_image'];?>" nctype="bundling_data_img">
                    </div></div>
                </td>
                <td class="tl"><dl class="goods-name">
                    <dt style="width: 300px;"><?php echo $output['goods_list'][$val['goods_id']]['goods_name'];?></dt>
                  </dl></td>
                <td class="goods-price w90" nctype="bundling_data_price"><?php echo ncPriceFormat($output['goods_list'][$val['goods_id']]['goods_price']);?></td>
                <td class="w90"><?php echo $val['goods_store_price'];?>
                  <input nctype="price" type="text" value="<?php echo ncPriceFormat($val['gs_goods_price']);?>" name="goods[<?php echo $val['goods_id'];?>][price]" class="text w70" <?php if ($output['can_edit'] === false){echo 'disabled';}?>></td>
                <td class="nscs-table-handle w90"><span><a onclick="bundling_operate_delete($('#bundling_tr_<?php echo $val['goods_id']?>'), <?php echo $val['goods_id']?>)" href="JavaScript:void(0);" class="btn-bittersweet"><i class="icon-ban-circle"></i>
                  <p><?php echo $lang['bundling_goods_remove'];?></p>
                  </a></span></td>
              </tr>
              <?php }?>
              <?php }?>
              <?php }?>
            </tbody>
          </table>
          <?php if ($output['can_edit'] !== false):?>
          <a id="bundling_add_goods" href="index.php?act=store_promotion_bundling&op=bundling_add_goods" class="ncbtn ncbtn-aqua"><?php echo $lang['bundling_goods_add'];?></a>
        <?php endif;?>
          <div class="div-goods-select-box">
            <div id="bundling_add_goods_ajaxContent"></div>
            <a id="bundling_add_goods_delete" class="close" href="javascript:void(0);" style="display: none; right: -10px;">X</a></div>
        </dd>
      </dl>
      <dl>
      <dt><i class="required">*</i>活动图片：</dt>
      <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb">
              <?php if($output['guess_info']['gs_img']){
                      $img = UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess/single'.DS.$output['guess_info']['gs_img'];
                    } else {
                      $img = UPLOAD_SITE_URL.DS.'shop/common/default_goods_image_240.gif';
                    }
                ?>
               <img id="gs_img" nctype="goods_image" src="<?php echo $img;?>"> 
              </div>
              <input id="arg_gs_img" type="hidden" name="gs_img" value=""/>
              <span></span>
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input hidefocus="true" size="1" class="input-file" name="gs_img_upload" type="file">
                  </span>
                  <?php if ($output['can_edit'] !== false):?>
                  <p><i class="icon-upload-alt"></i>图片上传</p>
                  <?php endif;?>
                  </a> </div>
              </div>
            </div>
          </div>
        <p class="hint">趣猜图片，用于趣猜列表展示，请选用520*220的图片</p>
        <p class="hint"></p>
      </dd>
    </dl>
      <dl>
        <dt><i class="required">*</i>市场估值：</dt>
        <dd>
          <p>
            <input name="estimate_price" type="text" maxlength="25" class="text w60 mr5" value="<?php echo $output['guess_info']['gs_estimate_price'];?>" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>元
            <span></span> </p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>竞猜区间：</dt>
        <dd>
          <p>
            <input name="min_price" type="text" maxlength="25" class="text w60 mr5" value="<?php echo $output['guess_info']['gs_min_price'];?>" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/> ~ &nbsp;<input name="max_price" type="text" maxlength="25" class="text w60 mr5" value="<?php echo $output['guess_info']['gs_max_price'];?>" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>元
            <span></span> </p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>竞猜价：</dt>
        <dd>
          <p>
            <input name="right_price" type="text" maxlength="25" class="text w60 mr5" value="<?php echo $output['guess_info']['gs_right_price'];?>" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>元
            <span></span> </p>
        </dd>
      </dl>
      <dl>
      <dl>
        <dt>赠送诺币：</dt>
        <dd>
          <p>
            <input name="points" type="text" maxlength="25" class="text w60 mr5" value="<?php echo $output['guess_info']['points'];?>" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>
            <span></span> </p>
        </dd>
      </dl>
      <dl>
        <dt><?php echo $lang['bundling_add_freight_method'].$lang['nc_colon'];?></dt>
        <dd>
        <ul class="ncsc-form-radio-list">
          <li><label for="whops_seller"><input id="whops_seller" type="radio" name="guess_freight_choose" <?php if(!isset($output['guess_info']) || $output['guess_info']['gs_freight_choose'] == '1'){ ?>checked="checked"<?php }?> value="1" <?php if ($output['can_edit'] === false){echo 'disabled';}?>/><?php echo $lang['bundling_add_freight_method_seller'];?></label></li>
          <li><label for="whops_buyer"><input id="whops_buyer" type="radio" name="guess_freight_choose" <?php if(isset($output['guess_info']) && $output['guess_info']['gs_freight_choose'] == '0'){ ?>checked="checked"<?php }?> value="0" <?php if ($output['can_edit'] === false){echo 'disabled';}?> /><?php echo $lang['bundling_add_freight_method_buyer'];?></label>
          <div id="whops_buyer_box" class="transport_tpl" style="<?php if(!isset($output['guess_info']) || $output['guess_info']['gs_freight_choose'] == '1'){ ?>display:none;<?php }?>"><input class="w50 text" type="text" name="guess_freight" value="<?php echo $output['guess_info']['gs_freight'];?>" /><em class="add-on"><i class="icon-renminbi"></i></em>
          </div>
          </li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt><?php echo $lang['bundling_status'].$lang['nc_colon'];?></dt>
        <dd>
          <ul class="ncsc-form-radio-list">
            <li><label for="bundling_status_1">
              <input type="radio" name="state" value="1" id="bundling_status_1" <?php if(!isset($output['guess_info']) || $output['guess_info']['gs_state'] == 1) echo 'checked="checked"'; ?> <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>
              <?php echo $lang['bundling_status_1'];?></label></li>
            <li><label for="bundling_status_0">
              <input type="radio" name="state" value="0" id="bundling_status_0" <?php if(isset($output['guess_info']) && $output['guess_info']['gs_state'] == 0) echo 'checked="checked"'; ?> <?php if ($output['can_edit'] === false){echo 'disabled';}?>/>
              <?php echo $lang['bundling_status_0'];?></label></li>
          </ul>
        </dd>
      </dl>
      <div class="bottom">
      <?php if ($output['can_edit'] !== false):?>
          <label class="submit-border"><input id="submit_button" type="submit" value="<?php echo $lang['nc_submit'];?>"  class="submit"></label>        
      <?php endif;?>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script> 
<script src="<?php echo SELLER_RESOURCE_SITE_URL;?>/js/store_bundling.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script> 
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script type="text/javascript">

$('input[name="gs_img_upload"]').fileupload({
    dataType: 'json',
    url: '<?php echo urlSeller('store_promotion_guess', 'ajax_upload_image0');?>',
    formData: '',
    add: function (e,data) {
        data.submit();
    },
    done: function (e,data) {
        if (!data.result){
          alert('上传失败，请尝试上传小图或更换图片格式');return;
        }
        if(data.result.state) {
          $('#gs_img').attr('src',data.result.pic_url);
          $('#arg_gs_img').val(data.result.pic_name);
        } else {
          alert(data.result.message);
        }
    },
    fail: function(){
      alert('上传失败，请尝试上传小图或更换图片格式');
    }
});

var DEFAULT_GOODS_IMAGE = '<?php echo defaultGoodsImage(60);?>';
var points_max = 1000;//赠送诺币规则：首先当中奖折扣价的10%大于1000时，此时赠送诺币最高1000；否则，按照中奖折扣价的10%计算作为此时诺币最高值；
var points_ratio = 0.1;
$(function(){
    jQuery.validator.addMethod('bundling_goods', function(value, element){
    	return $('tbody[nctype="bundling_data"] > tr').length >1?true:false;
    });
    jQuery.validator.addMethod('interval', function(value, element){
        if ($('input[name="min_price"]').val() && $('input[name="max_price"]').val()) {
            return (parseInt($('input[name="min_price"]').val()) < parseInt($('input[name="max_price"]').val()))?true:false;
        }
    });
    jQuery.validator.addMethod('check_value', function(value, element){
        var right_price = parseInt($('input[name="right_price"]').val());
        var min_price = parseInt($('input[name="min_price"]').val());
        var max_price = parseInt($('input[name="max_price"]').val());
        return ((right_price < min_price) || (right_price > max_price)) ? false : true;
    });
    jQuery.validator.addMethod('set_value', function(value, element){
        var points_val = parseInt($('input[name="discount_price"]').val()) * points_ratio;
        if (value > points_max) {
            $('input[name="points"]').val(points_max);
        }
        if(value > points_val) {
            $('input[name="points"]').val(points_val);
        }
        $('input[name="points_ratio"]').val(points_ratio);
        $('input[name="points_max"]').val(points_max);
        return true;
    });
	//Ajax提示
    $('.tip').poshytip({
    	className: 'tip-yellowsimple',
    	showTimeout: 1,
    	alignTo: 'target',
    	alignX: 'left',
    	alignY: 'top',
    	offsetX: 5,
    	offsetY: -78,
    	allowTipHover: false
    });
    $('.tip2').poshytip({
    	className: 'tip-yellowsimple',
    	showTimeout: 1,
    	alignTo: 'target',
    	alignX: 'right',
    	alignY: 'center',
    	offsetX: 5,
    	offsetY: 0,
    	allowTipHover: false
    });
    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.nextAll('span:first');
            error_td.append(error);
        },
        rules : {
            guess_name : {
                required : true
            },
        delivery_mechanism: {
                required : true
            },
            bundling_goods : {
				bundling_goods : true
	        },
            discount_price : {
				      required : true,
				      number : true,
				      min : 0.01
            },
            estimate_price : {
                required : true,
                number : true,
                min : 0.01
            },
            min_price : {
                required : true,
                number : true,
                min : 0.01,
            },
            max_price : {
                required : true,
                number : true,
                min : 0.01,
                interval : true
            },
            right_price : {
                required : true,
                number : true,
                min : 0.01,
                check_value : true
            },
            gs_img : {
              required : true
            },
            points : {
                required : true,
                number : true,
                set_value : true
            }
        },
        messages : {
            guess_name : {
                required : '<i class="icon-exclamation-sign"></i>请填写趣猜名称'
            },
            delivery_mechanism : {
                required : '<i class="icon-exclamation-sign"></i>请填写送拍机构'
            },
            bundling_goods : {
            	 bundling_goods : '<i class="icon-exclamation-sign"></i>请选择1件及以上商品'
            },
            discount_price : {
				        required : '<i class="icon-exclamation-sign"></i><?php echo $lang['bundling_add_price_error_null'];?>',
				        number : '<i class="icon-exclamation-sign"></i><?php echo $lang['bundling_add_price_error_not_num'];?>',
				        min : '<i class="icon-exclamation-sign"></i><?php echo $lang['bundling_add_price_error_null'];?>'
            },
            estimate_price : {
                required : '<i class="icon-exclamation-sign"></i>请填写市场估值',
                number : '<i class="icon-exclamation-sign"></i>市场估值需要填写数字',
                min : '<i class="icon-exclamation-sign"></i>市场估值价格值最小为0.01'
            },
            min_price : {
                required : '<i class="icon-exclamation-sign"></i>请填写趣猜最小值',
                number : '<i class="icon-exclamation-sign"></i>趣猜最小值需要填写数字',
                min : '<i class="icon-exclamation-sign"></i>趣猜最小值价格值最小为0.01',
            },
            max_price : {
               required : '<i class="icon-exclamation-sign"></i>请填写趣猜最大值',
                number : '<i class="icon-exclamation-sign"></i>趣猜最大值需要填写数字',
                min : '<i class="icon-exclamation-sign"></i>趣猜最大值价格值最小为0.01',
                interval : '<i class="icon-exclamation-sign"></i>趣猜最小值价格不得大于最大值价格'
            },
            right_price : {
                required : '<i class="icon-exclamation-sign"></i>请填写竞猜价',
                number : '<i class="icon-exclamation-sign"></i>竞猜价需要填写数字',
                min : '<i class="icon-exclamation-sign"></i>竞猜价价格值最小为0.01',
                check_value : '<i class="icon-exclamation-sign"></i>请填写正确的竞猜价'
            },
             gs_img : {
                required : '<i class="icon-exclamation-sign"></i>请上传趣猜图片'
            },
            points : {
                required : '<i class="icon-exclamation-sign"></i>请填写赠送诺币',
                number : '<i class="icon-exclamation-sign"></i>请填写数字',
                set_value : '<i class="icon-exclamation-sign"></i>请填写正确数字'
            },
        }
    });

	$('input[name="guess_freight_choose"]').click(function(){
		if($(this).val() == '0'){
			$('#whops_buyer_box').show();
		}else{
			$('#whops_buyer_box').hide();
		}
	});

    check_bundling_data_length();
    <?php if(!empty($output['bundling_info'])){?>
    count_cost_price_sum(); // 计算商品原价
    count_price_sum();
    <?php }?>

    $('tbody[nctype="bundling_data"]').on('change', 'input[nctype="price"]', function(){
        count_price_sum();
    });
});
count_cost_price_sum(); // 计算商品原价
/* 删除商品 */
function bundling_operate_delete(o, id){
	o.remove();
	check_bundling_data_length();
	$('li[nctype="'+id+'"]').children(':last').html('<a href="JavaScript:void(0);" onclick="bundling_goods_add($(this))" class="ncbtn-mini ncbtn-mint"><i class="icon-plus"></i>添加到趣猜</a>');
	count_cost_price_sum();
	count_price_sum();
}

function check_bundling_data_length(){
	if ($('tbody[nctype="bundling_data"] tr').length == 1) {
	    $('tbody[nctype="bundling_data"]').children(':first').show();
	}
}
</script>