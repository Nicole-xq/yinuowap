<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>

<style type="text/css">
    .redb{border: 1px dashed red!important;}
    .ncsc-form-default .ncsc-upload-thumb.store-logo p {
        width: 200px;
        height: 60px;
    }
    .ncsc-form-goods textarea{width:188px}
    .explain {
        font-size: 14px;
        font-weight: 600;
        line-height: 22px;
        color: #000;
        clear: both;
        background-color: #F5F5F5;
        padding: 5px 0 5px 12px;
        border-bottom: solid 1px #E7E7E7;
    }
    .name {
        width: 247px !important;
    }
    .tool {
        width: 136px !important;
    }
    .number {
        width: 160px !important;
    }
    .number1 {
        width: 190px !important;
    }
    .del{/*float:right;*/ margin-left:11px;}
    .fr {
        float: left !important;
        display: inline;
        width:50%;
    }
    .fr span {
        text-align: right;
        font-size: 14px;
        margin-right: 20px;
    }
    .fr p {
        margin: 0 0 4px;
        line-height: 21px;
        height:84px;
    }
    .fr h2 {
        font-size: 24px;
        text-align: center;
        color: #000;
        margin: 0 0 12px;
    }
    .works-list {
        width: 100%;
        border: #f3f3f3 solid 1px;
        float: left;
        margin: 0 0 12px 10px;
    }
</style>
<script type="text/javascript">
    var ref_area_id_1 = "<?php echo $output['artist_vendue_info']['city_id']?>";
    var ref_area_id_2 = "<?php echo $output['artist_vendue_info']['area_id']?>";
    var store_vendue_info = "<?php echo $output['artist_vendue_info']?>";

</script>

<div class="ncsc-form-default">
    <form method="post" enctype="multipart/form-data" id="vendue_form" action=" <?php echo urlSeller('store_auction', 'save_artist_edit');?>">
        <input type="hidden" name="form_submit" value="ok" />
        <div class="ncsc-form-goods">
            <h3 id="demo1">编辑拍卖信息</h3>
            <dl>
                <dt><i class="required">*</i>所在地：</dt>
                <dd>
                    <input name="region" type="hidden" id="region" value="">
                    <input type="hidden" name="city_id" id="_area_2" />
                    <input type="hidden" name="area_id" id="_area" />
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>机构类型：</dt>
                <dd>
                    <select name="vendue_type">
                        <option value="合作机构" <?php if($output['artist_vendue_info']['oz_type'] == '合作机构'){?>selected="selected"<?php }?>>合作机构</option>
                        <option value="艺术家" <?php if($output['artist_vendue_info']['oz_type'] == '艺术家'){?>selected="selected"<?php }?>>艺术家</option>
                    </select>

                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>
            <dl nctype="after" >
                <dt>联系方式（在线IM）：</dt>
                <dd>
                    <select name="store_vendue_contact">
                        <?php if (is_array($output['seller_list']) && !empty($output['seller_list'])) { ?>
                            <?php foreach ($output['seller_list'] as $key => $val) { ?>
                                <option value="<?php echo $val['member_id'];?>" <?php if($output['store_vendue_info']['store_vendue_contact'] == $val['member_id']){?>selected="selected"<?php }?>><?php echo $val['seller_name'];?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>

                </dd>
            </dl>

            <dl>
                <dt><i class="required">*</i>艺术家名称：</dt>
                <dd>
                    <input name="artist_name" type="text" class="text w400" value="<?php echo $output['artist_vendue_info']['artist_name']?>" />
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>艺术家职称：</dt>
                <dd>
                    <input name="artist_job_title" type="text" class="text w400" value="<?php echo $output['artist_vendue_info']['artist_job_title']?>" />
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>艺术家主图：</dt>
                <dd>
                    <div class="upload-thumb">
                            <?php if(!empty($output['artist_vendue_info']['artist_image'])) { ?>
                                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['artist_vendue_info']['artist_image'];?>" />
                            <?php } ?>
                        </div>
                        <input  name="artist_image_file" id="artist_image_file" type="file" class="type-file-file">
                        <input type="hidden" name="vendue_logo" id="vendue_logo" value="<?php echo $output['artist_vendue_info']['artist_image']; ?>">

                    <span></span>
                    <p class="hint">建议上传宽为140px，高为140px的图片</p>

                </dd>
            </dl>
            <dl>
                <dt><i class="required">*</i>个人简历：</dt>
                <dd>
                    <?php showEditor('artist_resume',$output['artist_vendue_info']['artist_resume'],'100%','480px','visibility:hidden;',"false","false");?>
                    <span></span>
                    <p class="hint"></p>
                </dd>
            </dl>

          <!--
            <dl nctype="represent">
                <dt>艺术家年代表：</dt>
                <dd>
                    <div class="ncs-message-title"><span class="name">年份</span><span class="number1">代表情况</span></div>
                    <?php if(!empty($output['artist_vendue_info']['artist_represent'])){?>
                        <?php foreach($output['artist_vendue_info']['artist_represent'] as $k=>$v){?>
                        <div class="ncs-message-list"><span class="name tip" title="">
            <input type="text" class="text w80" value="<?php echo $v['repres_time']?>" name="repres[<?php echo $k?>][repres_time]" maxlength="4" />年
          </span><span class="number1 tip" title="">
          <input type="text" class="text w300" value="<?php echo $v['repres_intro']?>" name="repres[<?php echo $k?>][repres_intro]" maxlength="200" />
          </span><span class="del"><a nctype="del_repres" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
            <?php }?>
                    <?php }else{?>
                    <div class="ncs-message-list"><span class="name tip" title="">
            <input type="text" class="text w80" value="" name="repres[1][repres_time]" maxlength="4" />年
          </span><span class="number1 tip" title="">
          <input type="text" class="text w300" value="" name="repres[1][repres_intro]" maxlength="200" />
          </span><span class="del"><a nctype="del_repres" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
                <?php }?>
                    <p><span><a href="javascript:void(0);" onclick="add_repres();" class="ncbtn ncbtn-aqua mt10"><i class="icon-plus"></i>新增</a></span></p>

                </dd>
            </dl>
            -->
            <dl nctype="awards">
                <dt>获奖情况：</dt>
                <dd>
                    <div class="ncs-message-title"><span class="name">获奖年份</span><span class="number1">获奖描述(200字以内)</span></div>
                    <?php if(!empty($output['artist_vendue_info']['artist_awards'])){?>
                    <?php foreach($output['artist_vendue_info']['artist_awards'] as $k=>$v){?>
                        <div class="ncs-message-list"><span class="name tip" title="">
            <input type="text" class="text w80" value="<?php echo $v['awards_time']?>" name="awards[<?php echo $k?>][awards_time]" maxlength="4" />年
          </span><span class="number1 tip" title="">
          <textarea name="awards[<?php echo $k?>][awards_intro]" type="text" class="text w180" maxlength="200"><?php echo $v['awards_intro']?></textarea>
          </span><span class="del"><a nctype="del_awards" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
                        <?php }?>
                    <?php }else{?>
                    <div class="ncs-message-list"><span class="name tip" title="">
            <input type="text" class="text w80" value="" name="awards[1][awards_time]" maxlength="4" />年
          </span><span class="number1 tip" title="">
          <textarea name="awards[1][awards_intro]" type="text" class="text w180" maxlength="200"></textarea>
          </span><span class="del"><a nctype="del_awards" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
                    <?php }?>
                    <p><span><a href="javascript:void(0);" onclick="add_awards();" class="ncbtn ncbtn-aqua mt10"><i class="icon-plus"></i>新增</a></span></p>

                </dd>
            </dl>
            <dl nctype="work">
                <dt>获奖作品(照片和简介)：</dt>
                <dd>
                    <div class="ncs-message-title"><span class="name">获奖图片</span><span class="tool">获奖名称(9字以内)</span><span class="number">获奖时间</span><span class="number1">获奖简介(60字以内)</span></div>
                    <?php if(!empty($output['artist_vendue_info']['artist_works'])){?>
                    <?php foreach($output['artist_vendue_info']['artist_works'] as $k=>$v){?>
                            <div class="ncs-message-list"><span class="name tip" title="">
                            <?php if($v['work_pic'] != ''){?>
                                    <img src="<?php echo getVendueLogo($v['work_pic']);?>" style="width:230px; height:168px;" alt="" />
                                    <?php }?>
         <input type="file" class="type-file-file" id="work_image_<?php echo $k?>" data-param="<?php echo $k?>" name="work_image_<?php echo $k?>" size="30" hidefocus="true"  nc_type="upload_activity_banner" title="点击按钮选择文件并提交表单后上传生效">
                            <input type="hidden" name="work[<?php echo $k?>][work_pic]" data-id="<?php echo $k?>" value="<?php echo $v['work_pic']?>"/>
          </span><span class="tool tip" title="">
            <input type="text" class="text w120" value="<?php echo $v['work_name']?>" name="work[<?php echo $k?>][work_name]" maxlength="9" />
          </span><span class="number tip" title="">
           <input type="text" id="work[<?php echo $k?>][time]" name="work[<?php echo $k?>][work_time]" nctype="work_time" class="input-txt" value="<?php echo $v['work_time']?>"/>
          </span><span class="number1 tip" title="">
          <textarea name="work[<?php echo $k?>][work_intro]" type="text" class="text w160" maxlength="200"><?php echo $v['work_intro']?></textarea>
          </span><span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
                        <?php }?>
                    <?php }else{?>
                    <div class="ncs-message-list"><span class="name tip" title="">
         <input type="file" class="type-file-file" id="work_image_1" data-param="1" name="work_image_1" size="30" hidefocus="true"  nc_type="upload_activity_banner" title="点击按钮选择文件并提交表单后上传生效">
                            <input type="hidden" name="work[1][work_pic]" data-id="1" val=""/>
          </span><span class="tool tip" title="">
            <input type="text" class="text w120" value="" name="work[1][work_name]" maxlength="9" />
          </span><span class="number tip" title="">
           <input type="text" id="work[1][time]" name="work[1][work_time]" nctype="work_time" class="input-txt"/>
          </span><span class="number1 tip" title="">
          <textarea name="work[1][work_intro]" type="text" class="text w180" maxlength="200"></textarea>
          </span><span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span></div>
          <?php }?>
                    <p><span><a href="javascript:void(0);" onclick="add_work();" class="ncbtn ncbtn-aqua mt10"><i class="icon-plus"></i>新增</a></span></p>
                </dd>
            </dl>


        </div>

        <div class="bottom tc hr32">
            <label class="submit-border">
                <input type="submit" nctype="formSubmit" class="submit" id="submitBtn" value="提交" />
            </label>
        </div>
    </form>

</div>

<script type="text/javascript">
    var SITEURL = "<?php echo SELLER_SITE_URL; ?>";
    $(document).ready(function(){
        $('#vendue_form').find('input[nctype="work_time"]').datepicker({dateFormat: 'yy.mm'});
        if (typeof ref_area_id_1 != '' && store_vendue_info != '') {
            $('#region').val('<?php echo $output['city_info']['area_name']?>&nbsp;&nbsp;<?php echo $output['area_info']['area_name']?>');
            $('#_area').val(ref_area_id_1);
            $('#_area_2').val(ref_area_id_2);
            var $newArea = $("<select name='area_id_1' id='area_id' style='display:none'></select>");
            $("#region").before($newArea);
            $newArea.append("<option value='"+ref_area_id_1+"'></option>");
        }
        $("#region").nc_region();

        $("#artist_image_file").change(function(){
            $("#vendue_logo").val($(this).val());
        });

        // 防止重复提交
        var __formSubmit = false;
        $('input[nctype="formSubmit"]').click(function () {
            var flag = true;

            $('textarea[name="artist_resume"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });
            /* 隐藏必填判断
            $('input[name*="repres_time"]').each(function(){
                if(Number($(this).val())<=0 || isNaN(Number($(this).val()))){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });
            $('input[name*="repres_intro"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });

            $('input[name*="awards_time"]').each(function(){
                if(Number($(this).val())<=0 || isNaN(Number($(this).val()))){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });
            $('textarea[name*="awards_intro"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });

            $('input[name*="work_pic"]').each(function(){
                var data_id = $(this).attr('data-id');
                if(!$(this).val()){
                    $('#work_image_' + data_id).addClass('redb');
                    flag = false;
                }else{
                    $('#work_image_' + data_id).removeClass('redb');
                }
            });
            $('input[name*="work_name"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });

            $('input[name*="work_time"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    $(this).removeClass('redb');
                }
            });
            $('textarea[name*="work_intro"]').each(function(){
                if(!$(this).val()){
                    $(this).addClass('redb');
                    flag = false;
                }else{
                    if($(this).val().length >60){
                        $(this).addClass('redb');
                        flag = false;
                    }else{
                        $(this).removeClass('redb');
                    }

                }
            });
            */
            if (__formSubmit || !flag ) {
                return false;
            }

            if ($('#vendue_form').valid() && flag) {
                __formSubmit = true;
            }
        });



        $('#vendue_form').validate({
            errorPlacement: function (error, element) {
                __formSubmit = false;
                $(element).nextAll('span').append(error);
            },

            rules: {
                artist_name: {
                    required: true
                },
                artist_job_title: {
                    required: true
                },
                region :{
                    checklast: true
                },
                vendue_logo:{
                    required: true
                }


            },
            messages: {
                artist_name: {
                    required: '<i class="icon-exclamation-sign"></i>艺术家名称不能为空'

                },
                artist_job_title: {
                    required: '<i class="icon-exclamation-sign"></i>艺术家职称不能为空'
                },
                region :{
                    checklast: '<i class="icon-exclamation-sign"></i>请将地区选择完整'
                },
                vendue_logo:{
                    required: '<i class="icon-exclamation-sign"></i>艺术家主图不能为空'
                }

            }
        });

        $('#pay_form').validate({
            errorPlacement: function(error, element){
                element.nextAll('span').first().after(error);
            },
            rules : {
                paying_money_certificate: {
                    required: true
                },
                paying_money_certif_exp: {
                    maxlength: 100
                }
            },
            messages : {
                paying_money_certificate: {
                    required: '请选择上传付款凭证'
                },
                paying_money_certif_exp: {
                    maxlength: jQuery.validator.format("最多{0}个字")
                }
            }
        });

    });

    function add_work(){
        obj = $('dl[nctype="work"]').children('dd').find('p');
        len = $('dl[nctype="work"]').children('dd').find('div').length;
        key = 'k'+len+Math.floor(Math.random()*100);
        var add_html = '';
        add_html += '<div class="ncs-message-list">';
        add_html += '<span class="name tip" title="">';
        add_html += '<input type="file" class="type-file-file" id="work_image_'+ key+'" data-param= '+key+' name="work_image_'+ key+'" size="30" hidefocus="true"  nc_type="upload_activity_banner" title="点击按钮选择文件并提交表单后上传生效">' +
            '<input type="hidden" name="work'+'['+key+'][work_pic]" data-id= '+key+' val="" /></span>';
        add_html += '<span class="tool tip" title="">';
        add_html += '<input type="text" class="text w120" value="" name="work'+'['+key+'][work_name]" maxlength="9"/></span>';
        add_html += '<span class="number tip" title=""><input type="text" id="work'+'['+key+'][time]" name="work'+'['+key+'][work_time]" nctype="work_time" class="input-txt"/></span>';
        add_html += '<span class="number1 tip" title=""><textarea name="work'+'['+key+'][work_intro]" type="text" class="text w180" maxlength="200"></textarea></span>';
        add_html += '<span class="del"><a nctype="del" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span>';
        add_html += '</div>';
        obj.before(add_html);
        $('#vendue_form').find('input[nctype="work_time"]').datepicker({dateFormat: 'yy.mm'});
        $('input[nc_type="upload_activity_banner"]').change(function(){
            var kk= $(this).attr('data-param');
            $('input[name="work'+'['+kk+'][work_pic]"]').val($(this).val());
        });

// 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("<?php echo $lang['default_img_wrong'];?>");
                $(this).attr('value','');
                return false;
            }
        });
    }
    $(function(){
        $('#vendue_form').find('a[nctype="del"]').live('click', function(){
            $(this).parents('div:first').remove();
        });

    });

    function add_awards(){
        obj = $('dl[nctype="awards"]').children('dd').find('p');
        len = $('dl[nctype="awards"]').children('dd').find('div').length;
        key = 'k'+len+Math.floor(Math.random()*100);
        var add_html = '';
        add_html += '<div class="ncs-message-list">';
        add_html += '<span class="name tip" title="">';
        add_html += '<input type="text" class="text w80" value="" name="awards'+'['+key+'][awards_time]" />年</span>';
        add_html += '<span class="number1 tip" title=""><textarea name="awards'+'['+key+'][awards_intro]" type="text" class="text w180" maxlength="200"></textarea></span>';
        add_html += '<span class="del"><a nctype="del_awards" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span>';
        add_html += '</div>';
        obj.before(add_html);
    }
    $(function(){
        $('#vendue_form').find('a[nctype="del_awards"]').live('click', function(){
            $(this).parents('div:first').remove();
        });

    });

    function add_repres(){
        obj = $('dl[nctype="represent"]').children('dd').find('p');
        len = $('dl[nctype="represent"]').children('dd').find('div').length;
        key = 'k'+len+Math.floor(Math.random()*100);
        var add_html = '';
        add_html += '<div class="ncs-message-list">';
        add_html += '<span class="name tip" title="">';
        add_html += '<input type="text" class="text w80" value="" name="repres'+'['+key+'][repres_time]" />年</span>';
        add_html += '<span class="number1 tip" title=""><input type="text" class="text w300" value="" name="repres'+'['+key+'][repres_intro]" maxlength="200"/></span>';
        add_html += '<span class="del"><a nctype="del_repres" href="javascript:void(0);" class="ncbtn"><i class="icon-trash"></i><?php echo $lang['nc_delete'];?></a></span>';
        add_html += '</div>';
        obj.before(add_html);
    }
    $(function(){
        $('#vendue_form').find('a[nctype="del_repres"]').live('click', function(){
            $(this).parents('div:first').remove();
        });

    });


    $(function(){
        $('input[nc_type="upload_activity_banner"]').change(function(){
            var kk= $(this).attr('data-param');
            $('input[name="work'+'['+kk+'][work_pic]"]').val($(this).val());
        });

// 上传图片类型
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("<?php echo $lang['default_img_wrong'];?>");
                $(this).attr('value','');
                return false;
            }
        });
    });




</script>
