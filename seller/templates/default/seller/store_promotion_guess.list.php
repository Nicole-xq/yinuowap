<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
  <a class="ncbtn ncbtn-mint"  href="index.php?act=store_promotion_guess&op=guess_add&gus_act_id=<?php echo $_GET['gus_act_id']?>" ><i class="icon-plus-sign"></i>添加趣猜</a>
</div>
<?php if(empty($output['list'])) { ?>
<!-- 没有添加活动 -->
<table class="ncsc-default-table ncsc-promotion-buy">
  <tbody>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
    </tr>
  </tbody>
</table>
<?php } else { ?>
<table class="ncsc-default-table">
  <thead>
    <tr>
      <th class="w10"></th>
      <th class="w50"></th>
      <th class="tl">趣猜名称</th>
      <th class="w110"><?php echo '趣猜优惠价';?></th>
      <th class="w50"><?php echo $lang['bundling_list_goods_count'];?></th>
      <th class="w180">审核状态</th>
      <th class="w90"><?php echo $lang['nc_state'];?></th>
      <th class="w120">管理员备注</th>
      <th class="w110"><?php echo $lang['nc_handle'];?></th>
    </tr>
    <tr>
      <td class="w30 tc"><input type="checkbox" id="all" class="checkall"/></td>
      <td colspan="20"><label for="all" ><?php echo $lang['nc_select_all'];?></label>
        <a href="javascript:void(0);" class="ncbtn-mini" nc_type="batchbutton" uri="index.php?act=store_promotion_guess&op=drop_guess" name="guess_id" confirm="<?php echo $lang['nc_ensure_del'];?>"><i class="icon-trash"></i><?php echo $lang['nc_del'];?></a></td>
    </tr>
  </thead>
  <?php foreach($output['list'] as $key=>$val){?>
  <tbody>
    <tr class="bd-line">
      <td><input type="checkbox" class="checkitem tc" value="<?php echo $val['gs_id'];?>"/></td>
      <td><div class="pic-thumb"><a <?php if ($val['goods_id'] != '') {echo 'href="' . urlSeller('goods', 'index', array('goods_id' => $val['goods_id'])) . '" target="_blank"';} else { echo 'href="javascript:void(0);"';}?> target="black"><img src="<?php echo $val['img'];?>"/></a></div></td>
      <td class="tl"><dl class="goods-name">
          <dt><a <?php if ($val['goods_id'] != '') {echo 'href="' . urlSeller('goods', 'index', array('goods_id' => $val['goods_id'])) . '" target="_blank"';} else { echo 'href="javascript:void(0);"';}?>><?php echo $val['gs_name'];?></a></dt>
        </dl></td>
      <td class="goods-price"><?php echo $val['gs_discount_price'];?></td>
      <td class=""><?php echo $val['count'];?></td>
      <td class=""><?php switch ($val['verify_state']) {
        case 0:
          echo "待审核";
          break;
        case 1:
          echo "已通过";
          break;
        case 2:
          echo "未通过";
          break;
      }?></td>
      <td><?php echo $output['state_array'][$val['gs_state']];?></td>
      <td><?php echo $val['verify_desc'];?></td>
      <td class="nscs-table-handle"><span><a href="index.php?act=store_promotion_guess&op=guess_add&guess_id=<?php echo $val['gs_id'];?>" class="btn-bluejeans"><i class="icon-cog"></i>
        <p>
          <?php if($val['start_time'] < time()):?>
          <?php echo $lang['bundling_edit'];?>
          <?php else:?>
            查看
          <?php endif;?>
          </p>
        </a></span> 
        <?php if($val['start_time'] > time()):?>
        <span><a class="btn-grapefruit" href='javascript:void(0);' onclick="ajax_get_confirm('<?php echo $lang['nc_ensure_del'];?>', '<?php echo urlSeller('store_promotion_guess', 'drop_guess', array('guess_id' => $val['gs_id']));?>');"><i class="icon-trash"></i>
        <p><?php echo $lang['nc_del'];?></p>
        </a></span>
        <?php endif;?>
        </td>
    </tr>
  </tbody>
  <?php }?>
  <tfoot>
    <tr>
      <th class="tc"><input type="checkbox" id="all" class="checkall"/></th>
      <th colspan="20"><label for="all" ><?php echo $lang['nc_select_all'];?></label>
        <a href="javascript:void(0);" class="ncbtn-mini" nc_type="batchbutton" uri="index.php?act=store_promotion_guess&op=drop_guess" name="guess_id" confirm="<?php echo $lang['nc_ensure_del'];?>"><i class="icon-trash"></i><?php echo $lang['nc_del'];?></a></th>
    </tr>
    <tr>
      <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
    </tr>
  </tfoot>
</table>
<?php } ?>
