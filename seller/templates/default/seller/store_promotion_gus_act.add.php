<?php defined('InShopNC') or exit('Access Invalid!');?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>

<div class="ncsc-form-default">
    <form id="add_form" action="index.php?act=store_promotion_guess&op=guess_act_save" method="post">
    <dl>
      <dt><i class="required">*</i>活动名称<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="gus_act_name" name="gus_act_name" type="text" class="text w400" value="" />
          <span></span>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>活动图片<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <div class="ncsc-goods-default-pic">
            <div class="goodspic-uplaod">
              <div class="upload-thumb">
               <img id="gs_act_img" nctype="goods_image" src="http://localhost/yinuo/www/data/upload/shop/common/default_goods_image_240.gif"> 
              </div>
              <input type="hidden" id="act_img" name="gs_act_img" value=""/><span></span>
              <div class="handle">
                <div class="ncsc-upload-btn">
                  <a href="javascript:void(0);"><span>
                  <input hidefocus="true" size="1" class="input-file" name="gs_img_upload" type="file">
                  </span>
                  <p><i class="icon-upload-alt"></i>图片上传</p>
                  </a> </div>
              </div>
            </div>
          </div>
        <p class="hint">活动图片，用于首页精品趣猜展示，请选用600*250的图片</p>
        <p class="hint"></p>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>开始时间<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="tstart" name="tstart" type="text" class="text w130" value="" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
        <p class="hint">开始时间发布之后不能修改</p>
        <p class="hint">
        </p>
      </dd>

    </dl>
    <dl>

      <dt><i class="required">*</i>结束时间<?php echo $lang['nc_colon'];?></dt>
      <dd>
          <input id="tend" name="tend" type="text" class="text w130" value="" /><em class="add-on"><i class="icon-calendar"></i></em><span></span>
        <p class="hint">结束时间发布之后不能修改</p>
        <p class="hint">
        </p>
      </dd>

    </dl>

    <div class="bottom">
      <label class="submit-border"><input id="submit_button" type="submit" class="submit" value="<?php echo $lang['nc_submit'];?>"></label>
    </div>
  </form>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script>
$(function() {

    $('#tstart').datetimepicker({
        controlType: 'select'
    });

    $('#tend').datetimepicker({
        controlType: 'select'
    });
    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#tstart").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };

    $('input[name="gs_img_upload"]').fileupload({
        dataType: 'json',
        url: '<?php echo urlSeller('store_promotion_guess', 'ajax_upload_image');?>',
        formData: '',
        add: function (e,data) {
            data.submit();
        },
        done: function (e,data) {
            if (!data.result){
              alert('上传失败，请尝试上传小图或更换图片格式');return;
            }
            if(data.result.state) {
              $('#gs_act_img').attr('src',data.result.pic_url);
              $('#act_img').val(data.result.pic_name);
            } else {
              alert(data.result.message);
            }
        },
        fail: function(){
          alert('上传失败，请尝试上传小图或更换图片格式');
        }
    });


    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            $(element).nextAll('span').append(error);
        },
        onfocusout: false,
        // submitHandler:function(form){
        //     ajaxpost('add_form', '', '', 'onerror');
        // },
        rules : {
            gs_act_img : {
                required:true
            },
            gus_act_name : {
                required : true
            },
            tstart : {
                required : true
            },
            tend : {
                required : true,
                greaterThanStartDate : true
            }
        },
        messages : {
            gs_act_img : {
                required : '<i class="icon-exclamation-sign"></i>活动图片不能为空'
            },
            gus_act_name : {
                required : '<i class="icon-exclamation-sign"></i>活动名称不能为空'
            },
            tstart : {
                required : '<i class="icon-exclamation-sign"></i>开始时间不能为空',
            },
            tend : {
                required : '<i class="icon-exclamation-sign"></i>结束时间不能为空',
                greaterThanStartDate : '<i class="icon-exclamation-sign"></i>结束时间必须晚于开始时间'
            }
        }
    });

    // ajax添加商品
    $('#cou-sku-choose-btn').ajaxContent({
        event: 'click',
        loaderType: "img",
        loadingMsg: SELLER_TEMPLATES_URL + "/images/loading.gif",
        target: '#cou-sku-options'
    }).click(function() {
        $(this).hide();
        $('#cou-sku-close-btn').show();
    });
    $('#cou-sku-close-btn').click(function() {
        $(this).hide();
        $('#cou-sku-options').html('');
        $('#cou-sku-choose-btn').show();
    });

    $('#cou-sku-item-results [data-cou-sku-remove-button]').live('click', function() {
        $(this).parents('tr').remove();

        // 未参加活动的商品显示“设置为活动商品”按钮
        var id = $(this).attr('data-cou-sku-remove-button');
        $("div[data-cou-sku-switch-disabled='"+id+"']").hide();
        $("div[data-cou-sku-switch-enabled='"+id+"']").show();
    });

    var nextId = (function() {
        var i = 10000;
        return function() {
            return ++i;
        };
    })();

    // ajax添加规则
    $('#cou-level-add-button').click(function() {
        var id = nextId();
        var h = $('#cou-level-newly').html();
        h = h.replace(/__level/g, id);
        $('#cou-level-container').append(h);
    });

    // 规则移除按钮
    $('[data-cou-level-remove]').live('click', function() {
        var id = $(this).attr('data-cou-level-remove');
        $("tr[data-cou-level-item='"+id+"']").remove();
    });

    var couLevelSkuChooseTriggered = function(id, url) {
        $("[data-cou-level-sku-choose-container='"+id+"']").load(
            url || 'index.php?act=store_promotion_cou&op=cou_level_sku&level='+id,
            function() {
                $("[data-cou-level-selected-sku]").each(function() {
                    var sku = $(this).attr('data-cou-level-selected-sku');
                    setCouLevelSkuAddButton(sku, 0);
                });
            }
        );
    };

    // 选择换购商品按钮
    $('[data-cou-level-sku-choose-button]').live('click', function() {
        var id = $(this).attr('data-cou-level-sku-choose-button');
        $("[data-cou-level-sku-choose-button='"+id+"']").hide();
        $("[data-cou-level-sku-close-button='"+id+"']").show();
        couLevelSkuChooseTriggered(id);
    });

    $('[data-cou-level-sku-choose-container] a.demo').live('click', function() {
        var id = $(this).parents('[data-cou-level-sku-choose-container]').attr('data-cou-level-sku-choose-container');
        var url = this.href;
        couLevelSkuChooseTriggered(id, url);
        return false;
    });

    $('[data-cou-level-sku-choose-container] a[nctype="search_a"]').live('click', function() {
        var id = $(this).parents('[data-cou-level-sku-choose-container]').attr('data-cou-level-sku-choose-container');
        var url = this.href;
        url += '&stc_id=' + $('#cou_level_sku_stc_id_'+id).val();
        url += '&keyword=' + encodeURIComponent($('#cou_level_sku_keyword_'+id).val());
        couLevelSkuChooseTriggered(id, url);
        return false;
    });

    // 关闭选择换购商品选择框
    $('[data-cou-level-sku-close-button]').live('click', function() {
        var id = $(this).attr('data-cou-level-sku-close-button');
        $(this).hide();
        $("[data-cou-level-sku-choose-button='"+id+"']").show();
        $("[data-cou-level-sku-choose-container='"+id+"']").html('');
    });

    var setCouLevelSkuAddButton = function(sku, b) {
        if (b) {
            $("[data-cou-level-sku-switch-enabled='"+sku+"']").show();
            $("[data-cou-level-sku-switch-disabled='"+sku+"']").hide();
        } else {
            $("[data-cou-level-sku-switch-enabled='"+sku+"']").hide();
            $("[data-cou-level-sku-switch-disabled='"+sku+"']").show();
        }
    };

    window.couLevelSkuInSearch = {};

    // 设置为换购商品
    $("[data-cou-level-sku-add-button]").live('click', function() {
        var sku = $(this).attr('data-cou-level-sku-add-button');
        var id = $(this).attr('data-level');

        var h = $('#cou-level-sku-newly').html();
        h = h.replace(/__level/g, id);
        h = h.replace(/__(\w+)/g, function($m, $1) {
            return window.couLevelSkuInSearch[sku][$1];
        });

        $('#cou-level-sku-container-'+id).append(h);
        setCouLevelSkuAddButton(sku, 0);
    });

    // 移除已选换购商品按钮
    $("[data-cou-level-sku-remove]").live('click', function() {
        var sku = $(this).attr('data-cou-level-sku-remove');
        $("[data-cou-level-selected-sku='"+sku+"']").remove();
        setCouLevelSkuAddButton(sku, 1);
    });

});
</script>
