<?php defined('InShopNC') or exit('Access Invalid!');?>

<!--<div class="tabmenu">-->
<!--    --><?php //include template('layout/submenu');?>
<!--</div>-->

<form method="get" action="index.php">
    <table class="search-form">
        <input type="hidden" name="act" value="store_auction_special" />
        <input type="hidden" name="op" value="special_check" />
        <tr>
            <td>&nbsp;</td>
            <th>
                <select name="search_type">
                    <option value="0" <?php if ($_GET['type'] == 0) {?>selected="selected"<?php }?>>专场名称</option>
                    <option value="1" <?php if ($_GET['type'] == 1) {?>selected="selected"<?php }?>>专场ID</option>
                </select>
            </th>
            <td class="w160"><input type="text" class="text" name="keyword" value="<?php echo $_GET['keyword']; ?>"/></td>
            <td class="tc w70"><label class="submit-border"><input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label></td>
            <td><a class="submit" href="index.php?act=store_auction_special&op=index">添加专场</a></td>
        </tr>
    </table>
</form>

<table class="ncsc-default-table">
    <thead>
    <tr nc_type="table_header">
        <th class="w180">专场信息</th>
        <th class="w30">拍品件数</th>
        <th class="w80">审核状态</th>
        <th class="w60">拍卖状态</th>
        <th class="w30">售出</th>
        <th class="w130">操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($output['special_list'])) { ?>
        <?php foreach ($output['special_list'] as $val) { ?>
            <tr>
                <td>
                    <span><?=$val['special_name'];?></span>
                    <?php if($val['preview_time']){?>
                    <p>预展时间:<?=$val['preview_time'];?></p>
                    <?php } ?>
                    <p>起拍时间:<?=$val['start_time'];?></p>
                    <p>结束时间:<?=$val['end_time'];?></p>
                </td>
                <td><span><?=$val['auction_num'].'件';?></span></td>
                <td><span><?=$val['check_state'];?></span>
                  <?php if($val['special_state'] == 30){?>
                    <p>备注:<?=$val['special_check_message'];?></p>
                  <?php }?>
                </td>
                <td><span><?=$val['auction_state'];?></span></td>
                <td><span><?=$val['sell_num'].'件';?></span></td>
                <td class="nscs-table-handle">
                    <span class="tip" title="查看">
                        <a href="index.php?act=store_auction_special&op=special_detail&special_id=<?php echo $val['special_id'];?>" class="btn-darkgray">
                            <i class="icon-edit"></i>
                            <p>查看</p>
                        </a>
                    </span>
                    <?php if(in_array($val['special_state'],array(0,30))){?>
                        <span class="tip" title="编辑">
                            <a href="index.php?act=store_auction_special&op=index&special_id=<?php echo $val['special_id'];?>" class="btn-darkgray">
                                <i class="icon-edit"></i>
                                <p>编辑</p>
                            </a>
                        </span>
                        <span class="tip" title="提交审核">
                            <a href="javascript:void(0);" onclick="ajax_get_confirm('确认提交？', '<?php echo urlSeller('store_auction_special', 'submit_audit', array('special_id' => $val['special_id']));?>');" class="btn-darkgray">
                                <i class="icon-edit"></i>
                                <p>提交审核</p>
                            </a>
                        </span>
                        <span class="tip" title="删除">
                            <a href="javascript:void(0);" onclick="ajax_get_confirm('您确定删除此专场吗？删除后不可回复，确认删除专场信息，返回取消删除', '<?php echo urlSeller('store_auction_special', 'delete_special', array('special_id' => $val['special_id']));?>');" class="btn-darkgray">
                                <i class="icon-edit"></i>
                                <p>删除</p>
                            </a>
                        </span>
                    <?php }?>

                </td>

            </tr>
            <tr style="display:none;"><td colspan="20"><div class="ncsc-goods-sku ps-container"></div></td></tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
        </tr>
    <?php } ?>
    </tbody>
    <?php  if (!empty($output['special_list'])) { ?>
        <tfoot>
        <tr>
            <td colspan="20"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
        </tr>
        </tfoot>
    <?php } ?>
</table>

