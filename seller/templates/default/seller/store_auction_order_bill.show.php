<?php defined('InShopNC') or exit('Access Invalid!');?>
<style>
.bill-alert-block {
    padding-bottom: 14px;
    padding-top: 14px;
}
.bill_alert {
    background-color: #F9FAFC;
    border: 1px solid #F1F1F1;
    margin-bottom: 20px;
    padding: 8px 35px 8px 14px;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	line-height:30px;
}
</style>
  <div class="bill_alert bill-alert-block mt10">
    <div style="width:800px"><h3 style="float:left">本期结算</h3><div style="float:right;">
    <?php if ($output['bill_info']['ob_state'] == BILL_STATE_CREATE){?>
    <a class="ncbtn mt5" onclick="ajax_get_confirm('一旦确认将无法恢复，系统将自动进入结算环节,<BR/>确认系统出账单计算无误吗?', 'index.php?act=store_auction_bill&op=confirm_bill&ob_id=<?php echo $_GET['ob_id'];?>');" href="javascript:void(0)">本期结算无误，我要确认</a>
    <?php } elseif ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS) {?>
    <a class="ncbtn mt5" target="_blank" href="index.php?act=store_auction_bill&op=bill_print&ob_id=<?php echo $_GET['ob_id'];?>">打印结算单</a>
    <?php } ?>
    </div>
    <div style="clear:both"></div>
    </div>
    <ul>
      <li>结算单号：<?php echo $output['bill_info']['ob_id'];?>&emsp;
      <?php echo date('Y-m-d',$output['bill_info']['ob_start_date']);?> &nbsp;至&nbsp; <?php echo date('Y-m-d',$output['bill_info']['ob_end_date']);?></li>
      <li>出账时间：<?php echo date('Y-m-d',$output['bill_info']['ob_create_date']);?></li>
      <li>本期应收：<?php echo ncPriceFormat($output['bill_info']['ob_result_totals']);?> = <?php echo ncPriceFormat($output['bill_info']['ob_order_totals']);?> (订单金额) - <?php echo ncPriceFormat($output['bill_info']['ob_commis_totals']);?> (佣金金额)</li>
      <li>结算状态：<?php echo billState($output['bill_info']['ob_state']);?>
      <?php if ($output['bill_info']['ob_state'] == BILL_STATE_SUCCESS){?>
      	，结算日期：<?php echo date('Y-m-d',$output['bill_info']['ob_pay_date']);?>
      <?php }?>
      </li>
    </ul>
  </div>
  <div class="tabmenu">
  <form method="get" id="formSearch">
    <table class="search-form">
      <input type="hidden" id='act' name='act' value='store_auction_bill' />
      <input type="hidden" id='op' name='op' value='show_bill' />
      <input type="hidden" name='ob_id' value='<?php echo $_GET['ob_id'];?>' />
      <input type="hidden" name='type' value='<?php echo $_GET['type'];?>' />
      <tr>
        <td>&nbsp;</td>
        <th>订单编号</th>
        <td class="w180"><input type="text" class="text"  value="<?php echo $_GET['query_order_no'];?>" name="query_order_no" /></td>
        <th>成交时间</th>
        <td class="w180">
        	<input type="text" class="text w70" name="query_start_date" id="query_start_date" value="<?php echo $_GET['query_start_date']; ?>"/>
          &#8211;
          <input type="text" class="text w70" name="query_end_date" id="query_end_date" value="<?php echo $_GET['query_end_date']; ?>"/>
        </td>
        <td class="tc w200">
        <label class="submit-border"><input type="button" id="ncsubmit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label>
        <label class="submit-border"><input type="button" id="ncexport" class="submit" value="导出" /></label>
        </td>
    </table>
  </form>
<table class="ncsc-default-table">
    <thead>
      <tr>
        <th class="w10"></th>
        <th>订单编号</th>
        <th>下单时间</th>
        <th>成交时间</th>
        <th>订单金额</th>
        <th>佣金金额</th>
        <th><?php echo $lang['nc_handle'];?></th>
      </tr>
    </thead>
    <tbody>
      <?php if (is_array($output['order_list']) && !empty($output['order_list'])) { ?>
      <?php foreach($output['order_list'] as $order_info) { ?>
      <tr class="bd-line">
        <td></td>
        <td class="w90"><?php echo $order_info['auction_order_sn'];?></td>
        <td><?php echo date("Y-m-d",$order_info['add_time']);?></td>
        <td><?php echo date("Y-m-d",$order_info['finnshed_time']);?></td>
        <td><?php echo ncPriceFormat($order_info['order_amount']);?></td>
        <td><?php echo ncPriceFormat($order_info['order_amount'] * $order_info['commis_rate']/100);?></td>
        <td>
       	<a target="_blank" href="index.php?act=store_auction_order&op=show_order&order_id=<?php echo $order_info['auction_order_id'];?>"><?php echo $lang['nc_view'];?></a>
        </td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td colspan="20" class="norecord"><i>&nbsp;</i><span><?php echo $lang['no_record'];?></span></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php if (is_array($output['order_list']) && !empty($output['order_list'])) { ?>
      <tr>
        <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
<script type="text/javascript">
$(function(){
    $('#ncexport').click(function(){
    	$('input[name="op"]').val('export_order');
    	$('#formSearch').submit();
    });
    $('#ncsubmit').click(function(){
    	$('input[name="op"]').val('show_bill');
    	$('#formSearch').submit();
    });
});
</script>
  </div>
<link type="text/css" rel="stylesheet" href="<?php echo RESOURCE_SITE_URL."/js/jquery-ui/themes/ui-lightness/jquery.ui.css";?>"/>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8" ></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('#query_start_date').datepicker();
	$('#query_end_date').datepicker();
});
</script>