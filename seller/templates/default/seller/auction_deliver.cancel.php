<div class="eject_con"><!-- onsubmit="ajaxpost('confirm_order_form','','','onerror')" -->
    <div id="warning"></div>
    <?php if ($output['order_info']) {?>
        <form action="index.php?act=store_auction_order&op=cancel&order_id=<?php echo $output['order_info']['auction_order_id']; ?>" method="post" id="confirm_order_form" onsubmit="ajaxpost('confirm_order_form','','','onerror')" >
            <input type="hidden" name="form_submit" value="ok" />
            <h3 class="orange" style="padding: 20px 0 0 88px;">确定退款给买家?</h3>
            <dl>
                <dt>订单号：</dt>
                <dd><?php echo trim($_GET['order_sn']); ?>
                    <p class="hint">确定通过审核吗？</p>
                </dd>
            </dl>
            <div class="bottom">
                <label class="submit-border">
                    <input type="submit" class="submit" id="confirm_yes" value="<?php echo $lang['nc_ok'];?>" />
                </label>
                <a class="ncbtn ml5" href="javascript:DialogManager.close('auction_cancel');">取消</a> </div>
        </form>
    <?php } else { ?>
        <p style="line-height:80px;text-align:center">该订单并不存在，请检查参数是否正确!</p>
    <?php } ?>
</div>