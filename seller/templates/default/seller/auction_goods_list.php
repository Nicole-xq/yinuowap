<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
  <?php include template('layout/submenu');?>
</div>
<form method="get" action="">
  <table class="search-form">
    <input type="hidden" name="act" value="auction_goods" />
    <input type="hidden" name="op" value="index" />
    <tr>
      <td>
         <select name="auction_state">
             <?php foreach ((array) $output['auction_status'] as $key=>$val) { ?>
                 <option value="<?php echo $key;?>" <?php if(isset($_GET['auction_state']) && (intval($key) === intval($_GET['auction_state']))) echo 'selected';?>><?php echo $val;?></option>
             <?php } ?>
         </select>
         <select name="special_id">
             <?php foreach ((array) $output['link_special_status'] as $key=>$val) { ?>
                 <option value="<?php echo $key;?>" <?php if((isset($_GET['auction_state']) && intval($key) === intval($_GET['special_id']))) echo 'selected';?>><?php echo $val;?></option>
             <?php } ?>
         </select>
          <input type="text" class="text" name="keyword" value="<?php echo $_GET['keyword']; ?>" placeholder="请输入拍品名称" />
      </td>
      <td class="tc w70"><label class="submit-border"><input type="submit" class="submit" value="<?php echo $lang['nc_search'];?>" /></label></td>
    </tr>
  </table>
</form>
<table class="ncsc-default-table">
  <thead>
    <tr nc_type="table_header">
      <th class="w30"></th>
      <th class="w180">拍品名称</th>
      <th class="w150">价格</th>
      <th class="w150">拍卖价格</th>
      <th class="w300">正在关联专场</th>
      <th class="w300">最近关联专场</th>
      <th class="w100">状态</th>
      <th class="w100">上拍次数</th>
      <th class="w200">操作</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($output['auction_list'])) { ?>
    <?php foreach ($output['auction_list'] as $val) { ?>
    <tr>
      <th class="tc"></th>
      <th colspan="20">ID：<?php echo $val['id'];?></th>
    </tr>
    <tr>
      <td>
        <div class="ncsc-goods-thumb">
         <a href="<?php echo urlSeller('goods', 'index', array('goods_id' => $val['goods_id']));?>" target="_blank">
             <img src="<?php echo cthumb($val['auction_image'], 60);?>"/>
         </a>
        </div>
      </td>
      <td class="tc">
          <dl>
          <dd style="max-width: 450px !important;">
            <a href="<?php echo urlSeller('goods', 'index', array('goods_id' => $val['goods_id']));?>" target="_blank">
             <?php echo $val['auction_name']; ?>
            </a>
          </dd>
          <?php if ($output['goods_list'][$val['goods_id']]){?>
          <dd>规格：<?php echo $output['goods_list'][$val['goods_id']]['spec_name'] ;?></dd>
          <?php } ?>
        </dl>
      </td>
      <td>
          <dl>
              <dd>市场价：<?php echo ncPriceFormat($output['goods_list'][$val['goods_id']]['goods_price']); ?></dd>
              <dd>销售价：<?php echo ncPriceFormat($output['goods_list'][$val['goods_id']]['goods_marketprice']);?></dd>
          </dl>
      </td>
      <td class="tl">
          <dl>
              <dd>起拍价：<?php echo ncPriceFormat($val['auction_start_price']); ?></dd>
              <dd>加浮价：<?php echo ncPriceFormat($val['auction_increase_range']);?></dd>
              <dd>保留价：<?php echo ncPriceFormat($val['auction_reserve_price']);?></dd>
              <dd>保证金：<?php echo ncPriceFormat($val['auction_bond']);?></dd>
          </dl>
      </td>
      <td>
          <?php if ($output['special_list'][$val['cur_special_id']]){ ?>
          <dl>
              <dd><?php echo $output['special_list'][$val['cur_special_id']]['special_name']?></dd>
              <dd>起拍时间：<?php echo date('Y-m-d', $output['special_list'][$val['cur_special_id']]['special_start_time']);?></dd>
              <dd>结束时间：<?php echo date('Y-m-d', $output['special_list'][$val['cur_special_id']]['special_end_time']);?></dd>
          </dl>
          <?php } ?>
      </td>
      <td>
        <?php if ($output['special_list'][$val['last_special_id']]){ ?>
        <dl>
            <dd><?php echo $output['special_list'][$val['last_special_id']]['special_name']?></dd>
            <dd>起拍时间：<?php echo date('Y-m-d', $output['special_list'][$val['last_special_id']]['special_start_time']);?></dd>
            <dd>结束时间：<?php echo date('Y-m-d', $output['special_list'][$val['last_special_id']]['special_end_time']);?></dd>
        </dl>
        <?php } ?>
      </td>
      <td>
        <dl>
            <dd><?php echo $val['auction_state_display'];?></dd>
       </dl>
      </td>
      <td>
        <dl>
            <dd><?php echo $val['auction_sum'];?></dd>
        </dl>
      </td>
      <td class="nscs-table-handle">
        <?php if(in_array($val['auction_state'],[0,3])) { ?>
            <span>
                <a href="<?php echo urlSeller('store_goods_online', 'add_auctions', array('id' => $val['id'],'commonid'=>$val['goods_common_id']));?>" class="btn-grapefruit">
                    <i class="icon-edit"></i>
                    <p>编辑</p>
                </a>
            </span>
            <span>
                <a href="javascript:void(0);" onclick="ajax_get_confirm('确认删除', '<?php echo urlSeller('auction_goods', 'drop_auction_goods', array('id' => $val['id'],'goods_id'=>$val['goods_id']));?>');" class="btn-grapefruit">
                    <i class="icon-trash"></i>
                    <p>删除</p>
                </a>
            </span>
        <?php } ?>
      </td>
    </tr>
    <tr style="display:none;"><td colspan="20"><div class="ncsc-goods-sku ps-container"></div></td></tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
    </tr>
    <?php } ?>
  </tbody>
    <?php  if (!empty($output['auction_list'])) { ?>
  <tfoot>
    <tr>
      <td colspan="20"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
    </tr>
  </tfoot>
  <?php } ?>
</table>
<script>
$(function(){

});
</script>