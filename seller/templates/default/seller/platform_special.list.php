<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>

<table class="ncsc-default-table">
    <thead>
    <tr nc_type="table_header">
        <th class="w60">专场ID</th>
        <th class="w180">专场名称</th>
        <th class="w180">专场开始时间</th>
        <th class="w180">专场结束时间</th>
        <th class="w180">预展开始时间</th>
        <th class="w120">操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($output['special_list'])) { ?>
        <?php foreach ($output['special_list'] as $val) { ?>
            <tr>
                <td><span><?php echo $val['special_id'];?></span></td>
                <td><span><?php echo $val['special_name'] ?></span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_start_time']) ?></span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_end_time']) ?></span></td>
                <td><span><?php echo date('Y-m-d H:i:s', $val['special_preview_start']) ?></span></td>
                <td class="nscs-table-handle"><span class="tip" title="查看">
                    <a id="a_<?php echo $val['special_id'];?>" href="index.php?act=store_auction_special&op=special_apply&special_id=<?php echo $val['special_id'];?>"  class="btn-mint">
                        <i class="icon-edit"></i>
                        <p>参与活动</p>
                    </a>
              </span></td>

            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span><?php echo $lang['no_record'];?></span></div></td>
        </tr>
    <?php } ?>
    </tbody>
    <?php  if (!empty($output['special_list'])) { ?>
        <tfoot>
        <tr>
            <td colspan="20"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
        </tr>
        </tfoot>
    <?php } ?>
</table>
