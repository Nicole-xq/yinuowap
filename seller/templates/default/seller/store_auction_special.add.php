<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<style type="text/css">

    .ncsc-form-default .ncsc-upload-thumb.store-logo p {
         width: 638px;
         height: 158px;
     }
    .ncsc-form-default .ncsc-upload-thumb.store-logo {
        width: 638px;
        height: 158px;
    }

</style>
<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>

    <div class="ncsc-form-default">
        <form method="post" enctype="multipart/form-data" id="special_form" action="index.php?act=store_auction_special&op=save_special">
            <input type="hidden" name="form_submit" value="ok" />
            <input type="hidden" name="special_id" value="<?=$output['special_id'];?>">
            <div class="ncsc-form-goods">
                <dl>
                    <dt><i class="required">*</i>专场名称：</dt>
                    <dd>
                        <input name="special_name" type="text" class="text w400" value="<?=$output['special_info']['special_name'];?>" />
                        <span></span>
                        <p class="hint"></p>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="required">*</i>送拍机构：</dt>
                    <dd>
                        <input name="delivery_mechanism" type="text" class="text w200" value="<?=$output['special_info']['delivery_mechanism'];?>" />
                        <span></span>
                        <p class="hint"></p>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="required">*</i>专场缩略图：</dt>
                    <dd>
                        <div class="upload-thumb">
                            <?php if(!empty($output['special_info']['special_image'])) { ?>
                                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['special_info']['special_image'];?>" />
                            <?php } ?>
                        </div>
                        <input  name="special_image_file" id="special_image_file" type="file" class="type-file-file">
                        <input type="hidden" name="special_image" id="special_image" value="<?php echo $output['special_info']['special_image']; ?>">
                        <span></span>
                        <p class="hint">建议上传图片大小的尺寸为520px*220px</p>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="required">*</i>专场广告图： </dt>
                    <dd>
                        <div class="upload-thumb">
                            <?php if(!empty($output['special_info']['adv_image'])) { ?>
                                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['special_info']['adv_image'];?>" />
                            <?php } ?>
                        </div>
                        <input  name="adv_image_file" id="adv_image_file" type="file" class="type-file-file">
                        <input type="hidden" name="adv_image" id="adv_image" value="<?php echo $output['special_info']['adv_image']; ?>">
                        <span></span>
                        <p class="hint">建议上传图片大小的尺寸为1920px*300px</p>
                    </dd>
                </dl>

                <dl>
                    <dt><i class="required">*</i>wap广告图： </dt>
                    <dd>
                        <div class="upload-thumb">
                            <?php if(!empty($output['special_info']['wap_image'])) { ?>
                                <img src="<?php echo UPLOAD_SITE_URL.'/'.ATTACH_VENDUE.'/'.$output['special_info']['wap_image'];?>" />
                            <?php } ?>
                        </div>
                        <input  name="wap_image_file" id="wap_image_file" type="file" class="type-file-file">
                        <input type="hidden" name="wap_image" id="wap_image" value="<?php echo $output['special_info']['wap_image']; ?>">
                        <span></span>
                        <p class="hint">建议上传图片大小的尺寸为1920px*300px</p>
                    </dd>
                </dl>

                <dl>
                    <dt><i class="required">*</i>专场开始时间：</dt>
                    <dd>
                        <input id="special_start_time" name="special_start_time" value="<?php if($output['special_info']){ echo date('Y-m-d H:i',$output['special_info']['special_start_time']);}?>" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em>
                        <span></span>
                        <p class="hint"></p>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="required">*</i>专场结束时间：</dt>
                    <dd>
                        <input id="special_end_time" name="special_end_time" value="<?php if($output['special_info']){ echo date('Y-m-d H:i',$output['special_info']['special_end_time']);}?>" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em>
                        <span></span>
                        <p class="hint"></p>
                    </dd>
                </dl>
<!--                <dl>-->
<!--                    <dt><i class="required">*</i>预展开始时间：</dt>-->
<!--                    <dd>-->
<!--                        <input id="special_preview_start" name="special_preview_start" value="" type="text" class="text w130" /><em class="add-on"><i class="icon-calendar"></i></em>-->
<!--                        <span></span>-->
<!--                        <p class="hint"></p>-->
<!--                    </dd>-->
<!--                </dl>-->
                <dl>
                    <dt><i class="required">*</i>添加拍品：</dt>
                    <dd>
                        <p>
                            <input id="bundling_goods" type="hidden" value="" name="bundling_goods">
                            <span></span></p>
                        <table class="ncsc-default-table mb15">
                            <thead>
                            <tr>
                                <th class="w70">指定拍品</th>
                                <th class="tl" colspan="2">拍品名称</th>
                                <th class="w90">起拍价</th>
                                <th class="w90"><?php echo $lang['nc_common_button_operate'];?></th>
                            </tr>
                            </thead>
                            <tbody nctype="bundling_data"  class="bd-line tip" title="<?php echo $lang['bundling_add_goods_explain'];?>">
                            <tr style="display:none;">
                                <td colspan="20" class="norecord"><div class="no-promotion"><i class="zh"></i><span>还未选择拍品。</span></div></td>
                            </tr>

                </tbody>
                </table>
                <a id="bundling_add_goods" href="index.php?act=store_auction_special&op=add_auctions&special_id=<?=$output['special_id'];?>" class="ncbtn ncbtn-aqua" style="color:#fff">添加拍品</a>
                <div class="div-goods-select-box">
                    <div id="bundling_add_goods_ajaxContent"></div>
                    <a id="bundling_add_goods_delete" class="close" href="javascript:void(0);" style="display: none; right: -10px;">X</a></div>
                </dd>
                </dl>


            </div>


            <div class="bottom tc hr32">
                <label class="submit-border">
                    <input type="submit" nctype="formSubmit" class="submit" value="提交" />
                </label>
            </div>
        </form>

    </div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/common.js"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"  />
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.poshytip.min.js"></script>
<script src="<?php echo SELLER_RESOURCE_SITE_URL;?>/js/store_bundling.js"></script>

<script type="text/javascript">
  var goods_list;
    $(document).ready(function(){
        goods_list = <?=json_encode($output['selected_auction_list']);?>;
        $.each(goods_list,function(id,v){
            // 隐藏第一个tr
        $('tbody[nctype="bundling_data"]').children(':first').hide();
        // 插入数据
        $('<tr id="bundling_tr_' + v.goods_auction_id + '"></tr>')
            .append('<input type="hidden" nctype="goods_id" name="goods[g_' + v.goods_auction_id + '][gid]" value="' + v.goods_auction_id + '">')
            .append('<td class="w70"><input type="checkbox" name="goods[g_' + v.goods_auction_id + '][appoint]" value="1" checked="checked"></td>')
            .append('<td class="w50 "><div class="pic-thumb"><img nctype="bundling_data_img" ncname="' + v.auction_image + '" src="' + v.auction_image + '" onload="javascript:DrawImage(this,60,60)"></span></div></td>')
            .append('<td class="tl"><dl class="goods-name"><dt style="width: 300px;">' + v.auction_name + '</dt></dl></td>')
            .append('<td class="w90 goods-price" nctype="bundling_data_price">' + v.auction_start_price + '</td>')
            .append('<td class="nscs-table-handle w90"><span><a href="javascript:void(0);" onclick="bundling_operate_delete($(\'#bundling_tr_' + v.auction_id + '\'), ' + v.auction_id + ')" class="btn-bittersweet"><i class="icon-ban-circle"></i><p>移除</p></a></span></td>')
            .fadeIn().appendTo('tbody[nctype="bundling_data"]');

        $('li[nctype="' + v.auction_id + '"]').children(':last').html('<a href="JavaScript:void(0);" class="ncbtn-mini ncbtn-bittersweet" onclick="bundling_operate_delete($(\'#bundling_tr_' + v.auction_id + '\'), ' + v.auction_id + ')"><i class="icon-ban-circle"></i>移除</a>');
        })
        $("#adv_image_file").change(function(){
            $("#adv_image").val($(this).val());
        });
        $("#special_image_file").change(function(){
            $("#special_image").val($(this).val());
        });
        jQuery.validator.addMethod('bundling_goods', function(value, element){
            return true;
            return $('tbody[nctype="bundling_data"] > tr').length >19?true:false;
        });
        $('#special_start_time').datetimepicker({
            controlType: 'select'
        });

        $('#special_end_time').datetimepicker({
            controlType: 'select'
        });
        $('#special_preview_start').datetimepicker({
            controlType: 'select'
        });


        //Ajax提示
        $('.tip').poshytip({
            className: 'tip-yellowsimple',
            showTimeout: 1,
            alignTo: 'target',
            alignX: 'left',
            alignY: 'top',
            offsetX: 5,
            offsetY: -78,
            allowTipHover: false
        });
        $('.tip2').poshytip({
            className: 'tip-yellowsimple',
            showTimeout: 1,
            alignTo: 'target',
            alignX: 'right',
            alignY: 'center',
            offsetX: 5,
            offsetY: 0,
            allowTipHover: false
        });



        //图片上传
        $('input[class="type-file-file"]').change(function(){
            var filepath=$(this).val();
            var extStart=filepath.lastIndexOf(".");
            var ext=filepath.substring(extStart,filepath.length).toUpperCase();
            if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
                alert("图片限于png,gif,jpeg,jpg格式");
                $(this).attr('value','');
                return false;
            }
        });


        //专场预展开始时间不能先于现在
        $.validator.addMethod('greaterThanNowDate', function(value,element){
            var date1 = new Date();
            var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
            return date1 < date2;
        }, '');


        //专场结束时间不能先于专场开始时间
        $.validator.addMethod('greaterThanStartTime', function(value,element){
            var start_date = $("#special_start_time").val();
            var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
            var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
            return date1 < date2;
        }, '');



        //页面输入内容验证
        $("#special_form").validate({
            errorPlacement: function(error, element){
                var error_td = element.nextAll('span:first');
                error_td.append(error);
            },
            onfocusout: false,
            submitHandler:function(form){
                ajaxpost('special_form', '', '', 'onerror');
            },
            rules : {
                special_name: {
                    required : true,
                    minlength   : 5,
                    maxlength   : 20
                },
                delivery_mechanism: {
                    required : true
                },
                special_preview_start: {
                    required : true,
                    greaterThanNowDate: true
                },
                special_start_time: {
                    required : true,
                    greaterThanPreviewEnd: true
                },
                special_end_time: {
                    required : true,
                    greaterThanStartTime: true
                },
                special_image: {
                    required : true
                },
                adv_image: {
                    required : true
                },
                bundling_goods: {
                    bundling_goods : true
                }

            },
            messages : {
                special_name: {
                    required : '<i class="icon-exclamation-sign"></i>专场名称不能为空',
                    minlength   : '<i class="icon-exclamation-sign"></i>专场名称最少是5个汉字',
                    maxlength   : '<i class="icon-exclamation-sign"></i>专场名称最多是20个汉字'
                },
                delivery_mechanism: {
                    required : '<i class="icon-exclamation-sign"></i>送拍机构不能为空'
                },
                special_preview_start: {
                    required : '<i class="icon-exclamation-sign"></i>预展开始时间不能为空',
                    greaterThanNowDate: '<i class="icon-exclamation-sign"></i>不能早于现在'
                },
                special_start_time: {
                    required    : '<i class="icon-exclamation-sign"></i>专场开始时间不能为空',
                    greaterThanPreviewEnd: '<i class="icon-exclamation-sign"></i>应为专场预展结束时间'
                },
                special_end_time: {
                    required    : '<i class="icon-exclamation-sign"></i>专场结束时间不能为空',
                    greaterThanStartTime: '<i class="icon-exclamation-sign"></i>不能早于专场开始时间'
                },
                special_image: {
                    required : '<i class="icon-exclamation-sign"></i>请设置专场缩略图'
                },
                adv_image: {
                    required : '<i class="icon-exclamation-sign"></i>请设置专场广告图'
                },
                bundling_goods: {
                    bundling_goods : '<i class="icon-exclamation-sign"></i>请至少选择20件拍品'
                }

            }
        });

    });

    /* 删除商品 */
    function bundling_operate_delete(o, id){
        o.remove();
        check_bundling_data_length();
        $('li[nctype="'+id+'"]').children(':last').html('<a href="JavaScript:void(0);" onclick="bundling_goods_add($(this))" class="ncbtn-mini ncbtn-mint"><i class="icon-plus"></i>添加此拍品</a>');
    }
    function check_bundling_data_length(){
        if ($('tbody[nctype="bundling_data"] tr').length == 1) {
            $('tbody[nctype="bundling_data"]').children(':first').show();
        }
    }


</script>
