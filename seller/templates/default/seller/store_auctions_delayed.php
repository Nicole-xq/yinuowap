<?php defined('InShopNC') or exit('Access Invalid!');?>

<div class="ncsc-form-default">
    <form method="post"  action="index.php?act=store_auction_delayed&op=delayed_save" id="my_store_form">
        <input type="hidden" name="form_submit" value="ok" />
        <dl>
            <dt>设置延时时间：</dt>
            <dd>
                <input class="w200 text" name="delayed_time" type="text"  id="delayed_time" value="<?php echo $output['delayed_time'];?>" />（分钟）
            </dd>
        </dl>
        <div class="bottom">
            <label class="submit-border"><input type="submit" class="submit" value="提交" /></label>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
    var SITEURL = "<?php echo SELLER_SITE_URL; ?>";
    $(function(){
        $('#my_store_form').validate({
            submitHandler:function(form){
                ajaxpost('my_store_form', '', '', 'onerror')
            },
            rules : {
                delayed_time: {
                    required:true,
                    digits:true
                }
            },
            messages : {
                delayed_time: {
                    required: '请输入延迟时间',
                    digits: '时间必须是整数，单位分钟'
                }
            }
        });
    });
</script>
