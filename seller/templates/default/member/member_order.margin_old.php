<?php
/**
 * Created by PhpStorm.
 * User: ADKi
 * Date: 2017/2/4 0004
 * Time: 18:16
 * @author DukeAnn
 */
defined('InShopNC') or exit('Access Invalid!');
?>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link href="<?php echo SHOP_TEMPLATES_URL;?>/css/member.css" rel="stylesheet" type="text/css">
<div class="account-center">
    <div class="common-list box">
        <div class="wrap">
            <div class="tabmenu">
                <?php include template('layout/submenu');?>
            </div>
            <div class="clear"></div>
            <table width="1025" border="0" cellspacing="0" cellpadding="0">
            <?php if (!empty($output['margin_order_list'])) { ?>
                <?php foreach ($output['margin_order_list'] as $key => $margin_order)  { ?>
                    <tr style="height: 36px;background: #f5f5f5">
                        <td>订单号:<?php echo $margin_order['order_sn'] ?></td>
                        <td>结束时间</td>
                        <td>成交价</td>
                        <td>我的最高出价</td>
                        <td>保证金</td>
                        <td>状态</td>
                    </tr>
                        <tr style="height: 90px;">
                            <td>
                                <a href="<?php echo $margin_order['auction_url'] ?>">
                                    <img src="<?php echo $margin_order['image_60_url'] ?>" width="58" height="58" class="all-block fl" />

                                    <p class="all-block fl"><?php echo $margin_order['auction_info']['auction_name'] ?></p>
                                </a>
                            </td>
                            <td><?php echo date('Y-m-d H:i:s', $margin_order['auction_info']['auction_end_true_t']) ?></td>
                            <td>&yen;<?php echo $margin_order['auction_info']['current_price'] ?></td>
                            <td>&yen;<?php echo $margin_order['relation']['offer_num'] ? $margin_order['relation']['offer_num'] : 0.00 ?></td>
                            <td>&yen;<?php echo $margin_order['margin_amount'] ?></td>
                            <td>
                                已退保证金
                            </td>
                        </tr>
                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td colspan="19"><div class="pagination"> <?php echo $output['show_page']; ?> </div></td>
                    </tr>
                    </tfoot>
                <?php } else { ?>
                    <div class="warning-option"><i>&nbsp;</i><span>暂无符合条件的数据记录</span></div>
                <?php } ?>
            </table>
        </div>
    </div>
</div>


