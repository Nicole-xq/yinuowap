<?php defined('InShopNC') or exit('Access Invalid!');?>
<div class="tabmenu">
    <?php include template('layout/submenu');?>
</div>
<?php if(!empty($output['favorites_list']) && is_array($output['favorites_list'])){ $i = 0; ?>
    <div class="favorite-goods-list">
        <ul>
            <?php foreach($output['favorites_list'] as $key=>$favorites){$i++;?>
                <li class="favorite-pic-list" >
                    <div class="favorite-goods-thumb">
                        <a href="<?php echo urlShop('show_guess','guess_detail',array('gs_id' => $favorites['fav_id']));?>" target="_blank" title="<?php echo $favorites['goods_name'];?>">
                            <img src="<?php echo $favorites['goods_image'];?>" width="154" />
                        </a>
                    </div>
                    <div class="handle">
                        <a href="javascript:void(0)" onclick="ajax_get_confirm('<?php echo $lang['nc_ensure_del'];?>', 'index.php?act=member_favorite_goods&op=delfavorites&type=guess&fav_id=<?php echo $favorites['fav_id'];?>');" class="fr ml5" title="<?php echo $lang['nc_del'];?>"><i class="icon-trash"></i>
                        </a>
                    </div>
                    <dl class="favorite-goods-info">
                        <dt>
                            <a href="<?php echo urlShop('show_guess','guess_detail',array('gs_id' => $favorites['fav_id']));?>" target="_blank" title="<?php echo $favorites['goods_name'];?>"><?php echo $favorites['goods_name'];?></a>
                        </dt>
                    </dl>
                </li>
            <?php }?>
        </ul>
    </div>
<?php }?>
