<?php defined('InShopNC') or exit('Access Invalid!');?>

  <?php if(!empty($output['favorites_list']) && is_array($output['favorites_list'])){ $i = 0; ?>
    <div class="account-center">
    <div class="common-list box">
  <div class="favorite-goods-list">
    <ul class="collection">
      <?php foreach($output['favorites_list'] as $key=>$favorites){$i++;?>
      <li class="favorite-pic-list<?php if (!isset($favorites['goods']) || !$favorites['goods']['state']) {?> disable<?php }?>" >

        <a href="<?php echo urlShop('goods','index',array('goods_id' => $favorites['fav_id']));?>"><img src="<?php echo thumb($favorites['goods'], 240);?>" class="all-block" width="120" height="120"></a>

         <?php if (!isset($favorites['goods']) || !$favorites['goods']['state']) {?>
          <dd class="goods-price">
            <h1>商品已失效     
            </h1>
          </dd>
          <p><?php echo $favorites['goods']['goods_name'];?></p>
          <?php } else {?>
          <dd class="goods-price">
            <?php if ($favorites['goods']['goods_promotion_price'] < $favorites['log_price']) {?>
             <h1>&yen;<?php echo ncPriceFormat($favorites['goods']['log_price']);?></h1>
            <?php } else {?>
             <h1>&yen;<?php echo ncPriceFormat($favorites['goods']['goods_promotion_price']);?></h1>
            <?php }?>
              <p><?php echo $favorites['goods']['goods_name'];?></p>
          </dd>
          <?php }?>
        <div>
          <a href="javascript:void(0)"  nc_type="sharegoods" data-param='{"gid":"<?php echo $favorites['goods']['goods_id'];?>"}'>
          <i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;分享</a>
          <a href="javascript:void(0)" onclick="ajax_get_confirm('<?php echo $lang['nc_ensure_del'];?>', 'index.php?act=member_favorite_goods&op=delfavorites&type=goods&fav_id=<?php echo $favorites['fav_id'];?>');" title="<?php echo $lang['nc_del'];?>">
          <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;删除</a>
        </div>
      </li>
      <?php }?>
    </ul>
    </div>
  </div>
  </div>
  <?php }?>
