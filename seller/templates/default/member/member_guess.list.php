<div class="account-center">
<div class="common-list box">
		<div class="tabmenu">
			<ul class="tab pngFix">
				<li class="<?php if(!isset($_GET['limit_type']) || !in_array($_GET['limit_type'],array('choiced','unchoiced'))){echo "active";} else {echo "normal";};?>"><a href="<?php echo urlShop('member_guess','list');?>">竞猜的</a></li>
				<li class="<?php if($_GET['limit_type'] == 'choiced'){echo "active";} else {echo "normal";};?>"><a href="<?php echo urlShop('member_guess','list',array('limit_type' => 'choiced'));?>">已中奖</a></li>
				<li class="<?php if($_GET['limit_type'] == 'unchoiced'){echo "active";} else {echo "normal";};?>"><a href="<?php echo urlShop('member_guess','list',array('limit_type' => 'unchoiced'));?>">已结束</a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<table width="1025" cellspacing="0" cellpadding="0" border="0">
			<tbody>
			<tr style="height: 36px;background: #f5f5f5">
				<td>趣猜商品</td>
				<td>结束时间</td>
				<td>当前出价</td>
				<td>状态</td>
				<td>操作</td>
			</tr>
			<?php if (!empty($output['member_guess_list'])):?>
			<?php foreach($output['member_guess_list'] as $val):?>
			<tr style="height: 90px;">
				<td><a href="<?php echo urlShop('show_guess','guess_detail',array('gs_id'=>$val['gs_id']));?>"><img src="<?php echo $val['show_img'];?>" class="all-block fl" width="58" height="58"><p class="all-block fl">
				<?php echo $val['gs_name'];?>
				</p></a></td>
				<td><?php echo date('Y-m-d H:i:s',$val['end_time']);?></td>
				<td>¥<?php echo $val['gs_offer_price'];?></td>
				<?php if ($val['gs_state'] == 0):?>
				<td>已报名</td>
				<?php elseif($val['gs_state'] == 1):?>
				<td>未中奖</td>
				<?php elseif($val['gs_state'] == 2):?>
				<td>已中奖</td>
				<?php elseif($val['gs_state'] == 3):?>
				<td>已下单</td>
				<?php endif;?>
				<?php if($_GET['limit_type'] != 'choiced'):?>
				<td><a href="<?php echo urlShop('show_guess','guess_detail',array('gs_id'=>$val['gs_id']));?>" target="_blank"><span>查看</span></a></td>
				<?php //elseif(($_GET['limit_type'] == 'choiced') && (($val['finished_time'] + 30*60) < time())):?>
				<?php elseif(($_GET['limit_type'] == 'choiced') && $val['end_time']+C('guess_add_cart_limit_time') >= time() && $val['gs_state'] == 2):?>
				<td><a href="javascript:;" class="add_gs_cart" gs_id="<?php echo $val['gs_id'];?>"><span>添加购物车</span></a></td>
				<?php else:?>
            <?php if($val['gs_state'] == 2):?>
              <td>超过付款时限</td>
            <?php else:?>
              <td></td>
            <?php endif;?>
				<?php endif;?>
			</tr>
			<?php endforeach;?>
			<?php else:?>
			      <tr>
			        <td colspan="20" class="norecord">暂无符合条件的数据</div></td>
			      </tr>
			<?php endif;?>
		</tbody></table>
			<?php if($output['member_guess_list']) { ?>
			        <div class="pagination"> <?php echo $output['show_page']; ?> </div>
    		<?php } ?>
			<div class="clear"></div>
	</div>
	</div>
	<script>
    $(function(){
        $('.add_gs_cart').click(function(){
            addgscart($(this).attr('gs_id'));
         });	
    });
    
    /* add one bundling to cart */ 
    function addgscart(gs_id)
    {
    	<?php if ($_SESSION['is_login'] !== '1'){?>
    	   login_dialog();
        <?php } else {?>
            var url = 'index.php?act=cart&op=add';
            $.getJSON(url, {'gs_id':gs_id}, function(data){
            	if(data != null){
            		if (data.state)
                    {
                        // 头部加载购物车信息
                        load_cart_information();
						alert('已添加到购物车，请尽快下单支付');
                    }
                    else
                    {
                        showDialog(data.msg, 'error','','','','','','','','',2);
                    }
            	}
            });
        <?php } ?>
    }
    </script> 