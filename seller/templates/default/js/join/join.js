/**
 * Created by Administrator on 2017/4/19 0019.
 */


// 选择入驻类型页面的选择逻辑
$(function() {
    function result(dom) {
        var $self = dom.find('.lm_radio');
        var pt_price = $self.attr('pt_price');
        var pt_sale = $self.attr('pt_sale');
        var price = $self.attr('price'),
            scale = $self.attr('sale'),
            result = (price - scale) + (pt_price - pt_sale);
        $('.lm_result .bzj').html('保证金：¥' + price + '，优惠：¥' + scale + '');
        $('.lm_result .fwf').html('平台服务费：¥' + pt_price + '，优惠：¥' + pt_sale + '');
        $('.lm_result .sum_result').html('合计：¥' + result + '');
    }

    $('.settled input[type="checkbox"]').on('click', function () {
        var $dom = $(this).parents('.zm_form-group');
        if ($(this).hasClass('lm_radio')) {
            $(this).attr('checked', true);
            $(this).parents('.zm_form-group').addClass('active');
            $(this).parents('.zm_form-group').siblings('.zm_form-group').find('input[type="checkbox"]').removeAttr('checked');
            $(this).parents('.zm_form-group').siblings('.zm_form-group').removeClass('active');


        } else {
            $dom.addClass('active');
            $dom.find('.lm_radio').attr('checked', true);
            $dom.siblings('.zm_form-group').removeClass('active');
            $dom.siblings('.zm_form-group').find('.checkboxs input[type="checkbox"]').removeAttr('checked');
        }
        result($dom);
    });
})




