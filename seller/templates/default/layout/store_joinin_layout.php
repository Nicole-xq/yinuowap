<?php defined('InShopNC') or exit('Access Invalid!');?>
<!doctype html>
<html>
<head>
<title><?php echo $output['html_title'];?></title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>">
<meta name="keywords" content="<?php echo $output['seo_keywords']; ?>" />
<meta name="description" content="<?php echo $output['seo_description']; ?>" />
<meta name="author" content="ShopNC">
<meta name="copyright" content="ShopNC Inc. All Rights Reserved">
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/store_joinin_new.css" rel="stylesheet" type="text/css">
<style type="text/css">
body { _behavior: url(<?php echo SELLER_TEMPLATES_URL;?>/css/csshover.htc);}</style>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/html5shiv.js"></script>
      <script src="<?php echo RESOURCE_SITE_URL;?>/js/respond.min.js"></script>
<![endif]-->
<script>
var COOKIE_PRE = '<?php echo COOKIE_PRE;?>';var _CHARSET = '<?php echo strtolower(CHARSET);?>';var SITEURL = '<?php echo SELLER_SITE_URL;?>';var MEMBER_SITE_URL = '<?php echo MEMBER_SITE_URL;?>';var SELLER_SITE_URL = '<?php echo SELLER_SITE_URL;?>';var RESOURCE_SITE_URL = '<?php echo RESOURCE_SITE_URL;?>';var SELLER_TEMPLATES_URL = '<?php echo SELLER_TEMPLATES_URL;?>';
</script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/dialog/dialog.js" id="dialog_js" charset="utf-8"></script>

<style>
    .ruzhu_ding {
        margin: 0 auto;
        width: 1200px;
        height: 110px;
    }

    .ruzhu_ding .ruzhu_logo {
        float: left;
        width: 310px;
        height: 60px;
        background: url(<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/ruzhu_logo.gif);
        margin-top: 25px
    }

    .ruzhu_ding .ruzhu_bangzhu {
        height: 60px;
        padding: 25px 65px;
        width: 640px;
        float: left
    }

    .ruzhu_ding .ruzhu_bangzhu ul {
        width: 160px;
        margin: 0 auto;
        overflow: hidden;
        float: left
    }

    .ruzhu_ding .ruzhu_bangzhu ul li {
        float: left;
        padding-left: 8px
    }

    .ruzhu_ding .ruzhu_bangzhu a {
        line-height: 60px;
        font-size: 20px;
        color: #777777;
    }

    .ruzhu_ding .ruzhu_bangzhu a:hover {
        line-height: 60px;
        font-size: 20px;
        color: #000
    }

    .ruzhu_ding .ruzhu_geren_in, .ruzhu_geren_on {
        float: right;
        padding-top: 25px;
        font-size: 14px;
        color: #777777;
    }

    .ruzhu_ding .ruzhu_geren_in li {
        line-height: 24px;
        padding: 3px 0 0 0
    }

    .ruzhu_ding .ruzhu_geren_in span {
        margin-left: 5px
    }

    .ruzhu_ding .ruzhu_geren_in a {
        margin: 0 5px;
        color: #777777
    }

    .ruzhu_ding .ruzhu_geren_in a:hover {
        color: #000
    }

    .ruzhu_ding .ruzhu_geren_on {
        line-height: 60px;
    }

    .ruzhu_ding .ruzhu_geren_on a {
        margin: 0 5px;
        color: #777777
    }

    .ruzhu_ding .ruzhu_geren_on a:hover {
        color: #000
    }

</style>

</head>
<body>

<!--顶部-->
<div class="ruzhu_ding">
 <div class="ruzhu_logo"> </div>
 <div class="ruzhu_bangzhu">
   <ul>
     <li><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/ruzhu_icon_ding_1.gif"/></li>
     <li><a href="#" target="_blank">招商方向</a></li>
   </ul>
   <ul>
     <li><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/ruzhu_icon_ding_2.gif"/></li>
     <li><a href="#" target="_blank">招商标准</a></li>
   </ul>
   <ul>
     <li><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/ruzhu_icon_ding_3.gif"/></li>
     <li><a href="#" target="_blank">资质要求</a></li>
   </ul>
   <ul>
     <li><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/ruzhu_icon_ding_4.gif"/></li>
     <li><a href="#" target="_blank">资费标准</a></li>
   </ul>
 </div>

<!--个人中心-->
<?php if($_SESSION['is_login'] == '1'){?>
<ul class="ruzhu_geren_in">
    <li><span><?php echo $_SESSION['member_name'];?></span></li>
    <li><a href="<?php echo urlSeller('member','home');?>">个人中心</a> | <a href="<?php echo urlLogin('login','logout');?>">退出</a></li>
</ul>
<?php }else{ ?>
<ul class="ruzhu_geren_on" >
    <li>
        <a href="<?php echo urlLogin('login','register');?>" target="_blank"><?php echo $lang['nc_register'];?></a>
        <a href="<?php echo urlLogin('login');?>" target="_blank"><?php echo $lang['nc_login'];?></a>
    </li>
</ul>
<?php } ?>

</div>
<?php require_once($tpl_file);?>
<?php require_once template('footer');?>
</body>
</html>
