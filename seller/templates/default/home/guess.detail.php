<div class="nch-breadcrumb-layout">
    <div class="nch-breadcrumb wrapper"><i class="icon-home"></i>
    	<span><a href="<?php echo urlSeller('index','index');?>">首页</a></span><span class="arrow">></span>
        <span><a href="<?php echo urlSeller('show_guess','index');?>">趣猜</a></span><span class="arrow">></span>
        <span><a href="<?php echo urlSeller('show_guess','guess_detail',array('gs_id' => $output['guess_info']['gs_id']));?>"><?php echo $output['guess_info']['gs_name'];?></a></span>
    </div>
  </div>
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/home_goods.css" rel="stylesheet" type="text/css">
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/index-min.css" rel="stylesheet" type="text/css">
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/zoom.css" rel="stylesheet" type="text/css">
<script>
    var tb_line=true;
</script>
<style type="text/css">
.ncs-goods-picture .levelB,
.ncs-goods-picture .levelC { cursor: url(<?php echo SELLER_TEMPLATES_URL;?>/images/shop/zoom.cur), pointer;
}
.ncs-goods-picture .levelD { cursor: url(<?php echo SELLER_TEMPLATES_URL;?>/images/shop/hand.cur), move\9;
}
.nch-sidebar-all-viewed { display: block; height: 20px; text-align: center; padding: 9px 0; }

.player{position: absolute; overflow: hidden; opacity: 1; height: 320px; width: 324px; left: 16px; top: 20px; z-index:100}
.btns .ncbtn,.ncs-cosult-askbtn .ncbtn{ color:#fff}
.ncs-handle{ padding-top: 18px}
#imageMenu ul li{margin-right: 8px;}
.ncs-lal{background:#fcfcfc}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {-webkit-appearance: none;margin: 0;}
.qll_inp{display: inline-block;height: 30px;text-align: center; -moz-appearance:textfield; }
.qll_count span{display: block;height: 17px;line-height:17px;text-align: center;width: 22px; }
.qll_count img{width: 22px;height: 17px;}
.qll_count{display: inline-block;vertical-align: -9px;;margin-left: -5px;}
</style>

<div class="wrapper pr">
  <input type="hidden" id="lockcompare" value="unlock" />
  <div class="ncs-detail ownshop">
    <!-- S 商品图片 -->
    <div class="preview fl">
      <div id="vertical" class="bigImg">
        <img src="<?php echo cthumb($output['show_img'],'1280',$guess_info['store_id']) ?>" width="360" height="360" nc-data-index="0" alt="" id="midimg" class="content-img"  style="display:block;" />
        <div style="display:none;" id="winSelector"></div>
      </div>
      <div class="smallImg">
        <div id="imageMenu">
          <ul>
            <?php if (!empty($output['image_list'])) {?>
              <?php foreach($output['image_list'] as $key => $li) { ?>
                <li class="list-img list-img-hover" ><img id="nc_small" nc-data-index="<?php echo $key; ?>" src="<?php echo $li['_small'] ?>" width="68" height="68" alt=""/></li>
              <?php } ?>
            <?php } ?>
          </ul>
        </div>
      </div>
      <!--smallImg end-->
      <div id="bigView" style="display:none;"><img width="1280" height="1280" alt="" src="" /></div>
    </div>
    <div id="goodcover"></div>
    <input type="hidden" value='<?php echo $output['image_json']; ?>' id="image_json">
    <!-- S 商品基本信息 -->

     <!--新增拍卖部分开始-->
    <link href="<?php echo SELLER_TEMPLATES_URL;?>/css/fudai.css" rel="stylesheet" type="text/css">
    <div class="ncs-goods-summary">
    	<input type="hidden" name="gs_id" id="gs_id" value="<?php echo $output['guess_info']['gs_id'];?>"/>
		<div class="auction-middle-box">
			<div class="title">
				<div class="fl">
					<h1 id="gs_name"><?php echo $output['guess_info']['gs_name'];?></h1>
					<!--距离结束-->
					<dd class="time">
						<?php if($output['guess_info']['gs_state'] == 2){?>
						<span class="now fl">已结束</span>
						<span class="time-remain fl" style="line-height:2;font-size:15px;">
							结束时间：<?php echo date('m月d日 H:i',$output['guess_info']['end_time']);?>
						</span>
						<?php }elseif($output['guess_info']['start_time'] <=time()){?>
						<span class="now fl" style="transform:translateY(5px);">距离结束</span>
						<span data-time="end" class="time-remain fl settime"  style="line-height:2;font-size:15px;" endTime="<?php echo date('Y-m-d H:i:s',$output['guess_info']['end_time']);?>">
							<em time_id="d">0</em>日<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒
						</span>

                        <?php }elseif($output['guess_info']['start_time'] > time()){?>
                        <span class="now fl" style="transform:translateY(5px);">距离开始</span>
                        <span data-time="start" class="time-remain fl settime"  style="line-height:2;font-size:15px;" endTime="<?php echo date('Y-m-d H:i:s',$output['guess_info']['start_time']);?>">
                            <em time_id="d">0</em>日<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒
                        </span>

						<?php }?>
						<div class="clear"></div>
					</dd>
				</div>
				<?php if ($output['guess_info']['gs_state'] != 2) :?>
				<a href="javascript:void(0);" class="fr cd-popup-trigger1"><div class="clock-01"></div><span class="all-block" id="rm_desc"><?php if(empty($output['guess_member_relation']) || !$output['guess_member_relation']['is_remind']):?>结束前提醒<?php else:?>已定阅<?php endif;?></span></a>
				<?php endif;?>
				<div class="clear"></div>
			</div>
			<?php if ($output['guess_info']['gs_state'] == 1) :?>
			<div class="auction-middle-box">
				<div class="normal">
					<h1><span>竞猜价格</span><b><span id="gs_min_price" style="font-size:28px;color:#000">&yen;<?php echo $output['guess_info']['gs_min_price'];?></span>&nbsp;<span style="font-size: 34px;color: #000;">-</span>&nbsp;<span id="gs_max_price" style="font-size:28px;color:#000">&yen;<?php echo $output['guess_info']['gs_max_price'];?></span></b></h1>
                    <?php if ($output['guess_info']['start_time']<time()) {?>
                        <label class="all-block" for="" style="height: 42px;margin: 0 0 20px 0"><span style="line-height: 42px">竞猜出价</span>
                        <?php if(!$output['my_guess_offer']):?>
                          <input class="qll_inp buy-num" type="number" name="guess_offer" placeholder="" autocomplete="off" maxlength="10" id="guess_offer" style="width: 180px;height: 30px;margin-left: 20px;padding-left: 10px" data-min="<?php echo $output['guess_info']['gs_min_price'];?>" >
                          <div class="qll_count">
                               <span class="add"><a href="javascript:void(0);"><img src="<?php echo SELLER_TEMPLATES_URL;?>/lm/images/qll_add.png" alt=""/></a></span>
                               <span class="minus"><a href="javascript:void(0);"><img src="<?php echo SELLER_TEMPLATES_URL;?>/lm/images/qll_minus.png" alt=""/></a></span>
                          </div>
                        <?php else:?>
                            &nbsp;&nbsp;&nbsp;<b style="font-size:28px;color:#000">￥<?php echo $output['my_guess_offer']['gs_offer_price'];?></b>
                        <?php endif;?>
                        </label>
                    <?php }?>
					<?php if(!$output['my_guess_offer']):?>
					<a href="javascript:void(0);" id="btn-guess" class="all-block">
						<img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/btn-01.png" alt=""/>
					</a>
					<?php endif;?>
				</div>
				<div class="clear"></div>
			</div>
			<?php endif;?>
			<div id="mask-dialogBox"></div>
			<?php if ($output['guess_info']['gs_state'] == 2) :?>
			<div class="lottery">
				<?php if (!empty($output['winer_info']['member_name']) && (in_array($output['winer_info']['gs_state'],array(2,3)))):?>
					<h1>中奖者:&nbsp;&nbsp;<?php echo $output['winer_info']['member_name'];?></h1>
				<?php //elseif(!empty($output['winer_info']['member_name']) && ($output['winer_info']['gs_state'] == 2) && ($output['guess_info']['end_time'] + 30*60) > time()):?>
					<!-- <h1>即将开奖</h1> -->
				<?php else:?>
					<h1>本场趣猜无人中奖</h1>
				<?php endif;?>
				<?php if(!empty($output['guess_info']['points'])):?>
					<h2>诺币奖励:&nbsp;&nbsp;<?php echo $output['guess_info']['points'];?>诺币</h2>
				<?php endif;?>
				<h2><span>市场估价:&nbsp;&nbsp;&yen;<?php echo $output['guess_info']['gs_estimate_price'];?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>购买折扣:&nbsp;&nbsp;<?php echo round(($output['guess_info']['gs_discount_price']/$output['guess_info']['gs_cost_price'])*10,2);?>折</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>福购价:&nbsp;&nbsp;&yen;<?php echo $output['guess_info']['gs_discount_price'];?></span></h2>
				<div class="clear"></div>
			</div>
			<?php endif;?>
			<!--弹窗部分-->
			<!--服务提醒-->
		 	<div class="cd-popup1">
				<div class="cd-popup-container1">
					<div class="cd-popup-top"><h1>提醒设置</h1></div>
					<div class="cd-buttons">
						<div class="box-01">
							<div class="fl"><h1>提醒时间</h1><img class="all-block" src="<?php echo SELLER_TEMPLATES_URL;?>/images/paimai/clock-black.png" width="34" height="34" alt=""/></div>
							<span class="all-block fr">结束前30分钟<b class="gold-word"><?php echo date('m-d H:i',($output['guess_info']['end_time'] - 30*60));?></b></span>
							<span class="all-block fr">结束时间<b class="gold-word"><?php echo date('m-d H:i',$output['guess_info']['end_time']);?></b></span>
						</div>
						<div class="clear"></div>
						<?php if($output['guess_member_relation'] && ($output['guess_member_relation']['is_remind'])): ?>
						<div class="box-02">
							<span>拍卖会将根据您设置的<?php if($output['guess_member_relation']['is_remind']):?>手机(<?php echo $output['guess_member_relation']['member_mobile'];?>)<?php endif;?>提醒您。</span>
							<a href="javascript:void(0);" class="all-block" id="change_relation1">更改设置&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
						</div>
						<div class="clear"></div>
						<?php endif;?>
						<div class="box-02" id="relation_set" style="<?php if($output['guess_member_relation'] && ($output['guess_member_relation']['is_remind'] || $output['guess_member_relation']['is_remind_app'])){ echo "display:none";}?>">
							<div class="fl" id="relation_form1" style="<?php if(!($output['guess_member_relation'] && ($output['guess_member_relation']['is_remind'] || $output['guess_member_relation']['is_remind_app']))){echo "display:none";};?>">
								<span>提醒方式&nbsp;&nbsp;<b>(您可以在我的拍卖-设置中找到哦)</b></span>
								<form>
									<div class="all-block">
									<input type="checkbox" tabindex="3" <?php if($output['guess_member_relation']['is_remind'])echo "checked";?> style="outline:#6d6d6d none 0;margin-top: -2px" disabled>&nbsp;&nbsp;<label for="">手机短信：<?php echo $output['guess_member_relation']['member_mobile'];?></label>&nbsp;&nbsp;<a href="javascript:void(0);" id="change_relation2">修改</a>
									</div>
								</form>
							</div>
							<div class="fl" id="relation_form2" style="<?php if(($output['guess_member_relation'] && ($output['guess_member_relation']['is_remind'] || $output['guess_member_relation']['is_remind_app']))){echo "display:none";};?>">
								<span>提醒方式&nbsp;&nbsp;</span>
								<form>
									<div class="all-block">
										<input type="hidden" name="remind_time" id="remind_time" value="<?php echo ($output['guess_info']['end_time'] - 30*60);?>"/>
										<input type="hidden" name="gs_id" id="gs_id" value="<?php echo $output['guess_info']['gs_id'];?>"/>
										<div class="phone-number">
											<label for="">手机号码:&nbsp;&nbsp;<input type="text" name="phone" placeholder="" autocomplete="off" maxlength="11" id="member_mobile"></label>
											<label for="">手机短信:&nbsp;&nbsp;<input type="text" name="phone" placeholder="" autocomplete="off" maxlength="6"style="width: 70px;" id="vcode"><a href="#" id="get_vcode">获取验证码</a></label>
											<input type="hidden" name="is_vcode" value="1" id="is_vcode">
										</div>
									</div>
								</form>
							</div>
							<div class="qrcode">
								<img src="<?php echo UPLOAD_SITE_URL.DS.ATTACH_COMMON.DS.C('mobile_app');?>" width="96" height="96" alt=""/>
								<span class="all-block" style="font-size: 12px;">手机出价快人一步</span>
							</div>
						</div>
						<div class="clear"></div>
						<div class="botton">
							<input id="mb_gs_relation_submit" type="submit" value="确 定" class="fl"><input type="submit" value="取 消" class="cd-popup-close fr">
						</div>
						<div class="clear"></div>
					</div>
					<a href="#0" class="cd-popup-close">X</a>
				</div>
			</div>
			<script language="javascript">
				$(function(){
					updateEndTime();
				});
				//倒计时函数
				function updateEndTime()
				{
				 var date = new Date();
				 var time = date.getTime();  //当前时间距1970年1月1日之间的毫秒数

				 $(".settime").each(function(i){

				 var endDate =this.getAttribute("endTime"); //结束时间字符串
				 //转换为时间日期类型
				 var endDate1 = eval('new Date(' + endDate.replace(/\d+(?=-[^-]+$)/, function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');

				 var endTime = endDate1.getTime(); //结束时间毫秒数

				 var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
				  if(lag > 0)
				  {
				   var second = Math.floor(lag % 60);
				   var minite = Math.floor((lag / 60) % 60);
				   var hour = Math.floor((lag / 3600) % 24);
				   var day = Math.floor((lag / 3600) / 24);
				   $(this).html("<em time_id='d'>"+day+"</em>日<em time_id='h'>"+hour+"</em>时<em time_id='m'>"+minite+"</em>分<em time_id='s'>"+second+"</em>秒");
				  }
				  else
          {
              if($(this).attr('data-time')=='start'){
                  location.reload();
              }else if($(this).attr('data-time')=='end'){
                  $(this).html("趣猜已经结束啦！");
              }
          }
				 });
				 setTimeout("updateEndTime()",1000);
				}
			</script>
			<script type="text/javascript">
				$('#change_relation1').toggle(function(){$("#relation_set").show();},function(){
					$("#relation_set").hide();
				});
				$('#change_relation2').click(function(){
					$('#relation_form1').hide();
					$('#relation_form2').show();
				});
				$('#mb_gs_relation_submit').click(function(){
					if ($('#relation_form2').css('display') == 'none') {
							$('.cd-popup1').click();return false;
					}

					var remind_time = $("#remind_time").val();
					var member_mobile = $("#member_mobile").val();
					var vcode = $("#vcode").val();
					var gs_id = $("#gs_id").val();
					if (!vcode) {
						alert('请填写验证码');
    					return false;
					}
					var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
					if(!myreg.test(member_mobile)){
					    alert('请输入有效的手机号码');
    					return false;
					}
					$.ajax({
				        type:"POST",
				        data:{remind_time:remind_time,mobile:member_mobile,vcode:vcode,gs_id:gs_id},
				        url: SELLER_SITE_URL+"/index.php?act=show_guess&op=guess_remind",
				        dataType: "json",
			        	success: function(data){
			        		if (data.code=="error") {
			        			alert(data.message);return false;
			        		}
			        		if (data.code=="ok") {
			        			$('.cd-popup1').click();
			        			location.reload();return false;
			        			// $('#rm_desc').html('已定阅');
			        			return false;
			        		}
				        }
				      });
				});
			</script>
			<script type="text/javascript">
				/*弹框JS内容*/
				jQuery(document).ready(function($){
					//打开窗口
					$('.cd-popup-trigger1').on('click', function(event){
						<?php if(empty($_SESSION['member_id'])):?>
							login_dialog();return false;
						<?php endif;?>
						<?php if( $output['guess_info']['end_time'] < time()):?>
							alert("活动已经结束");return false;
						<?php endif;?>
						<?php if($output['guess_member_relation'] &&  $output['guess_member_relation']['is_remind'] == 1):?>
							var gs_id = $('#gs_id').val();
							var relation_id = <?php print_r($output['guess_member_relation']['relation_id']);?>;
							$.ajax({
							type:"POST",
				        	data:{relation_id:relation_id,gs_id:gs_id},
				        	url: SELLER_SITE_URL+"/index.php?act=show_guess&op=cancel_guess_remind",
				        	dataType: "json",
			        		success: function(data){
			        			if (data.code=="error") {
			        				alert(data.msg);return false;
			        			}
			        			if (data.code=="ok") {
			        				location.reload();return false;
			        				// $('#rm_desc').html('结束前提醒');return false
			        			}

		        			}
				    	});return false;
						<?php endif;?>
						event.preventDefault();
						$('.cd-popup1').addClass('is-visible1');
					});
					//关闭窗口
					$('.cd-popup1').on('click', function(event){
						if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup1') ) {
							event.preventDefault();
							$(this).removeClass('is-visible1');
						}
					});
					//ESC关闭
					$(document).keyup(function(event){
						if(event.which=='27'){
							$('.cd-popup1').removeClass('is-visible1');
						}
					});
					//获取验证码
					$('#get_vcode').click(function(){
						var member_mobile = $("#member_mobile").val();
						var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
						var gs_id = $("#gs_id").val();
						if(!myreg.test(member_mobile)){
						    alert('请输入有效的手机号码');
	    					return false;
						}
						$.ajax({
							type:"POST",
				        	data:{mobile:member_mobile,gs_id:gs_id},
				        	url: SELLER_SITE_URL+"/index.php?act=show_guess&op=send_modify_mobile",
				        	dataType: "json",
			        		success: function(data){
			        			if (data.state == 'false') {
			        				alert(data.msg);
			        			}
			        			if (data.state == 'true') {
			        				alert(data.msg+'请等待');
			        			}
		        			}
				    	});
					});
				});
			</script>
			<script type="text/javascript">
			$('#btn-guess').click(function(){

			<?php if ($_SESSION['is_login'] !== '1'){?>
				login_dialog();return;
			<?php }?>

    		<?php if ($_SESSION['store_id'] == $output['guess_info']['store_id']) { ?>
    			alert('不能参与自己店铺的趣猜');return;
    		<?php } ?>

				var gs_id = $('#gs_id').val();
				var gs_name = $('#gs_name').text();
				var guess_offer = $('#guess_offer').val();
				var gs_max_price = $('#gs_max_price').text().replace('¥','');
				var gs_min_price = $('#gs_min_price').text().replace('¥','');
				var end_time = <?php echo $output['guess_info']['end_time'];?>;
				if (guess_offer == '') {
					$('#mask-dialogBox').dialogBox({
								hasClose: true,
								autoHide: true,
								hasMask: false,
								time: 6000,
								content: '<img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-04.png" width="260" height="190" alt=""/><span class="dialog-box-close" style="top:0"></span><h2 class="guess-price">竞猜价不能为空</h2>'
					});return;
				}
				if ((parseInt(gs_max_price) < parseInt(guess_offer)) || (parseInt(guess_offer) < parseInt(gs_min_price))) {
					$('#mask-dialogBox').dialogBox({
								hasClose: true,
								autoHide: true,
								hasMask: false,
								time: 6000,
								content: '<img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-04.png" width="260" height="190" alt=""/><span class="dialog-box-close" style="top:0"></span><h2 class="guess-price">竞猜价不能超出竞猜范围</h2>'
					});return;
				}
				$.ajax({
					type: "POST",
					url: SELLER_SITE_URL+'/index.php?act=show_guess&op=add_guess_offer',
					data: {gs_id:gs_id,guess_offer:guess_offer,gs_name:gs_name,end_time:end_time},
					dataType: "json",
					async:false,
					success: function(data) {
						if (data.state == 0 && data.data == "请登录") {
							login_dialog();return;
						}
						if (data.state == 1) {
							$('#mask-dialogBox').dialogBox({
								hasClose: true,
								autoHide: true,
								hasMask: false,
								time: 4000,
								content: '<img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-01.png" width="260" height="190" alt=""/><span class="dialog-box-close" style="top:0"></span><h2 class="guess-price">您的猜价:&nbsp;&nbsp;<b>&yen;'+data.data.price+'</b></h2>'
							});
							getGsMember();
							setTimeout("location.reload();",5000);
							return;
						}
						if (data.state == 0) {
							$('#mask-dialogBox').dialogBox({
								hasClose: true,
								autoHide: true,
								hasMask: false,
								time: 6000,
								content: '<img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-04.png" width="260" height="190" alt=""/><span class="dialog-box-close" style="top:0"></span><h2 class="guess-price">'+data.data+'</h2>'
							});return;
						}
					}
				});
			});
			</script>
			<!--新弹窗部分-->
			<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.dialogBox.js"></script>
			<script>
				<?php if (isset($output['if_winer']) && ($output['if_winer'] == 'true')):?>
						$('#mask-dialogBox').dialogBox({
						hasClose: true,
						autoHide: true,
						hasMask: true,
						time: 6000,
						content: '<a href="<?php echo urlSeller('member_guess','list',array('limit_type' => 'choiced'));?>" style="display:inline-block"><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-02.png" width="500" height="494" alt=""/></a><span class="dialog-box-close"></span>'
						});
				<?php endif;?>
				<?php if (isset($output['if_winer']) && ($output['if_winer'] == 'false')):?>
						$('#mask-dialogBox').dialogBox({
						hasClose: true,
						autoHide: true,
						hasMask: true,
						time: 6000,
						content: '<a href="<?php echo urlSeller('member_guess','list',array('limit_type' => 'all'));?>" style="display:inline-block"><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/fudai/dialog-03.png" width="500" height="480" alt=""/></a><span class="dialog-box-close"></span>'
						});
				<?php endif;?>
			</script>
			<div class="info">
				<div>
					<span><b><?php echo $output['guess_info']['joined_times'];?></b>&nbsp;人报名</span>
					<span><b><?php echo $output['guess_info']['set_reminders_num'];?></b>&nbsp;人设置提醒</span>
					<span><b><?php echo $output['guess_info']['views'];?></b>&nbsp;次围观</span>
				</div>
				<ul>
					<li>最低竞猜价格:&nbsp;&nbsp;&yen;<?php if(!empty($output['offer_min']['gs_offer_price'])){echo $output['offer_min']['gs_offer_price'];}else{echo 0;}?></li>
					<li>最高竞猜价格:&nbsp;&nbsp;&yen;<?php if(!empty($output['offer_max']['gs_offer_price'])){echo $output['offer_max']['gs_offer_price'];}else{echo 0;}?></li>
					<li>送拍机构:&nbsp;&nbsp;<?php echo $output['guess_info']['delivery_mechanism'];?></li>
					<li style="min-width: 340px;">特色服务:<span class="gold-word"> 无</span>
					</li>

				</ul>
				<div class="clear"></div>
				<div class="share">
					<a href="javascript:collect_guess('<?php echo $output['guess_info']['gs_id'];?>','count','guess_collect');" id="if_fav"<?php if($output['if_favorites']):?>class="selected"<?php endif;?>><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<font id="fav_desc"><?php if($output['if_favorites']):?>已关注<?php else:?>关注<?php endif;?></font></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<!--<div class="bdsharebuttonbox" style="display:inline-block"><a href="#" class="fa fa-share-alt" data-cmd="more" style="height:20px;line-height:21px; padding-left:0;margin:0px;background:none">&nbsp;分享</a></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
-->				</div>
			</div>
		</div>
    </div>
    <!--新增拍卖部分结束-->

    <!--S 店铺信息-->
    <div style="position: absolute; z-index: 2; top: -1px; right: -1px;">
      <!--店铺基本信息 S-->

<div class="ncs-info">
  <div class="title">
    <h4>yinuo</h4>
  </div>
  <div class="content">
              </div>
</div>
<script>
$(function(){
	var store_id = "1";
	var goods_id = "100009";
	var act = "goods";
	var op  = "index";
	$.getJSON("index.php?act=show_store&op=ajax_flowstat_record",{store_id:store_id,goods_id:goods_id,act_param:act,op_param:op});
});
</script>
      <!--S 出价记录 -->
      <div class="ncs-lal" style="height:500px">
        <div class="title" style="text-align:center;background:#fff !important;border-bottom:#ededed solid 1px !important">往期竞猜</div>
        <?php if($output['pass_guess_info']):?>
        <div class="content guess" style="width:240px !important">
			<h1><?php echo $output['pass_guess_info']['gs_name'];?></h1>
			<img src="<?php echo $output['pass_guess_info']['img'];?>" width="240" height="228" alt=""/>
			<h2 style="margin-top:15px">市场估价:&nbsp;¥<?php echo $output['pass_guess_info']['gs_estimate_price'];?></h2>
			<h2>出价次数:&nbsp;<?php echo $output['pass_guess_info']['joined_times'];?>次</h2>
			<h2>竞猜价格:&nbsp;¥<?php echo $output['pass_guess_info']['gs_right_price'];?></h2>
			<h2>中奖详情:&nbsp;<?php echo ($output['pass_guess_info']['be_win']) ? '中奖' : '未中奖';?></h2>
			<a href="<?php echo urlSeller('show_guess','guess_detail',array('gs_id'=>$output['pass_guess_info']['gs_id']));?>" class="fr">查看详情</a>
        </div>
    	<?php endif;?>
      </div>
      <!--E 出价记录 -- >
    </div>
    <!--E 店铺信息 -->
  </div>
  <div class="clear"></div>
</div>
<div id="content" class="ncs-goods-layout expanded" >
  <!--竞拍流程-->
  <a class="bidding-process">
	  <h1 class="fl">竞拍流程</h1>
	  <span class="fl all-block"><b>趣猜报价</b><b>等待开奖</b><b>开奖胜出</b><b>支付订单</b><b>完成收款</b></span>
  </a>
  <div class="clear"></div>
  <div class="ncs-goods-main" id="main-nav-holder">
    <div class="tabbar pngFix" id="main-nav">
      <div class="ncs-goods-title-nav">
        <ul id="categorymenu">
          <li class="current"><a id="guessDetail" href="#content">商品详情</a></li>
          <li><a id="guessLog" href="#content">竞猜记录</a></li>
          <li><a id="sellerPromise" href="#content">卖家承诺</a></li>
          <li><a id="guessRule" href="#content">竞猜规则</a></li>
        </ul>
        <div class="switch-bar"><a href="javascript:void(0)" id="fold">&nbsp;</a></div>
      </div>
    </div>
    <div class="hd ncs-intro" id="guessDetailInfo"><?php echo $output['guess_body'];?></div>
    <div class="hd ncs-intro guess" id="guess_member" style="display:none">
    </div>
    <script type="text/javascript">
    	function getGsMember(){
    		$.ajax({
    			type:"GET",
    			url:'<?php echo urlSeller('show_guess','guesslog',array('gs_id' => $output['guess_info']['gs_id']));?>',
    			dataType:'html',
    			success:function(data){
    				$('#guess_member').html(data);
    			}
    		});
    	}
    	$(function(){
    		getGsMember();
    	});
    </script>
    <div class="hd ncs-intro" id="sellerProInfo" style="display:none"><?php echo($output['guess_articles'][22]['article_content']);?></div>
    <div class="hd ncs-intro" id="guellRulelInfo" style="display:none"><?php echo($output['guess_articles'][21]['article_content']);?></div>
  </div>
  <!--拍卖详细左边部分-->
  <div class="auction-detail-left ncs-sidebar">
	  <div id="vertical-slide">
		  <h1><span>更多趣猜</span></h1>
	  	  <div class="clear"></div>
		  <div class="slide-pic" id="slidePic">
			<div class="vertical-slide-pic">
				<ul>
				<?php if($output['more_guess']):?>
				<?php foreach ($output['more_guess'] as $key => $value): ?>
				  <li>
				  <a href="<?php echo  urlSeller('show_guess','guess_detail',array('gs_id'=>$value['gs_id']));?>">
				  	  <img src="<?php echo $value['img'];?>" alt=""/>
					  <div class="title">
					  <span class="title-name"><?php echo $value['gs_name'];?></span>
					  <div class="info"><span class="fl">范围&nbsp;<b class="red-word"><?php echo $value['gs_min_price'];?></b> ~ <b class="red-word"><?php echo $value['gs_max_price'];?></b></span><div></div></div></div>
				  </a>
				  </li>
				<?php endforeach; ?>
				<?php endif;?>
			</ul>
		    </div>
			<a class="gray" id="prev" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
			<a id="next" hidefocus="" href="javascript:void(0);"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		  </div>
		  <div class="clear"></div>
		</div>
	  <!--商品轮播js-->
	  <script type="text/javascript">
		jQuery(function() {
			if (!$('#slidePic')[0]) return;
			var i = 0,
				p = $('#slidePic ul'),
				pList = $('#slidePic ul li'),
				len = pList.length;
			var elePrev = $('#prev'),
				eleNext = $('#next');
			var w = 224,
				num = 5;
			p.css('width', w * len);
			if (len <= num) eleNext.addClass('gray');
			function prev() {
				if (elePrev.hasClass('gray')) {
					return
				}
				p.animate({
					marginTop: -(--i) * w
				}, 500);
				if (i < len - num) {
					eleNext.removeClass('gray')
				}
				if (i == 0) {
					elePrev.addClass('gray')
				}
			}
			function next() {
				if (eleNext.hasClass('gray')) {
					return
				}
				p.animate({
					marginTop: -(++i) * w
				}, 500);
				if (i != 0) {
					elePrev.removeClass('gray')
				}
				if (i == len - num) {
					eleNext.addClass('gray')
				}
			}
			elePrev.bind('click', prev);
			eleNext.bind('click', next);
		});
		</script>
 </div>
  <div class="clear"></div>
</div>
</div>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.charCount.js"  type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/sns.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.F_slider.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/waypoints.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/prism-min.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.raty/jquery.raty.min.js" type="text/javascript"></script>
<script src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/custom.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo RESOURCE_SITE_URL;?>/js/jquery.nyroModal/styles/nyroModal.css" rel="stylesheet" type="text/css" id="cssfile2" />
<script type="text/javascript">
/** 辅助浏览 **/
jQuery(function($){
  // 产品图片
  var count = $("#imageMenu li").length - 5; /* 显示 6 个 li标签内容 */
  var interval = $("#imageMenu li:first").width();
  var curIndex = 0;

  $('.scrollbutton').click(function(){
    if( $(this).hasClass('disabled') ) return false;

    if ($(this).hasClass('smallImgUp')) --curIndex;
    else ++curIndex;

    $('.scrollbutton').removeClass('disabled');
    if (curIndex == 0) $('.smallImgUp').addClass('disabled');
    if (curIndex == count-1) $('.smallImgDown').addClass('disabled');

    $("#imageMenu ul").stop(false, true).animate({"marginLeft" : -curIndex*interval + "px"}, 800);
  });
  // 解决 ie6 select框 问题
  $.fn.decorateIframe = function(options) {
    if ($.browser.msie && $.browser.version < 7) {
      var opts = $.extend({}, $.fn.decorateIframe.defaults, options);
      $(this).each(function() {
        var $myThis = $(this);
        //创建一个IFRAME
        var divIframe = $("<iframe />");
        divIframe.attr("id", opts.iframeId);
        divIframe.css("position", "absolute");
        divIframe.css("display", "none");
        divIframe.css("display", "block");
        divIframe.css("z-index", opts.iframeZIndex);
        divIframe.css("border");
        divIframe.css("top", "0");
        divIframe.css("left", "0");
        if (opts.width == 0) {
          divIframe.css("width", $myThis.width() + parseInt($myThis.css("padding")) * 2 + "px");
        }
        if (opts.height == 0) {
          divIframe.css("height", $myThis.height() + parseInt($myThis.css("padding")) * 2 + "px");
        }
        divIframe.css("filter", "mask(color=#fff)");
        $myThis.append(divIframe);
      });
    }
  }
  $.fn.decorateIframe.defaults = {
    iframeId: "decorateIframe1",
    iframeZIndex: -1,
    width: 0,
    height: 0
  }
  //放大镜视窗
  $("#bigView").decorateIframe();
  //点击到中图
  var midChangeHandler = null;

  $("#imageMenu li img").bind("click", function(){
    if ($(this).attr("id") != "onlickImg") {
      var list_key = $(this).attr("nc-data-index");
      var image_json = $('#image_json').val();
      var list_arr = new Array();
      var list_arr = JSON.parse(image_json);
      var image = list_arr[list_key]['_big'];
      midChange(list_key,image);
      $("#imageMenu li").removeAttr("id");
      $(this).parent().attr("id", "onlickImg");
    }
  }).bind("mouseover", function(){
    if ($(this).attr("id") != "onlickImg") {
      var list_key = $(this).attr("nc-data-index");
      var image_json = $('#image_json').val();
      var list_arr = new Array();
      var list_arr = JSON.parse(image_json);
      var image = list_arr[list_key]['_big'];
      midChange(list_key,image);
      $("#imageMenu li").removeAttr("id");
      $(this).parent().attr("id", "onlickImg");
    }
  }).bind("mouseout", function(){
    if($(this).attr("id") != "onlickImg"){
      $(this).removeAttr("style");
    }
  });
  function midChange(key,src) {
    $("#midimg").attr("src", src).load(function() {
      $(".content-img").attr("nc-data-index",key);
      changeViewImg(key);
    });
  }
  //大视窗看图
  function mouseover(e) {
    if ($("#winSelector").css("display") == "none") {
      $("#winSelector,#bigView").show();
    }
    $("#winSelector").css(fixedPosition(e));
    e.stopPropagation();
  }
  function mouseOut(e) {
    if ($("#winSelector").css("display") != "none") {
      $("#winSelector,#bigView").hide();
    }
    e.stopPropagation();
  }
  $("#midimg").mouseover(mouseover); //中图事件
  $("#midimg,#winSelector").mousemove(mouseover).mouseout(mouseOut); //选择器事件

  var $divWidth = $("#winSelector").width(); //选择器宽度
  var $divHeight = $("#winSelector").height(); //选择器高度
  var $imgWidth = $("#midimg").width(); //中图宽度
  var $imgHeight = $("#midimg").height(); //中图高度
  var $viewImgWidth = $viewImgHeight = $height = null; //IE加载后才能得到 大图宽度 大图高度 大图视窗高度

  function changeViewImg(key) {
    var image_json = $('#image_json').val();
    var list_arr = new Array();
    var list_arr = JSON.parse(image_json);
    var image = list_arr[key]['_big'];
    $("#bigView img").attr("src", image);
  }
  changeViewImg(0);
  $("#bigView").scrollLeft(0).scrollTop(0);
  function fixedPosition(e) {
    if (e == null) {
      return;
    }
    var $imgLeft = $("#midimg").offset().left; //中图左边距
    var $imgTop = $("#midimg").offset().top; //中图上边距
    X = e.pageX - $imgLeft - $divWidth / 2; //selector顶点坐标 X
    Y = e.pageY - $imgTop - $divHeight / 2; //selector顶点坐标 Y
    X = X < 0 ? 0 : X;
    Y = Y < 0 ? 0 : Y;
    X = X + $divWidth > $imgWidth ? $imgWidth - $divWidth : X;
    Y = Y + $divHeight > $imgHeight ? $imgHeight - $divHeight : Y;

    if ($viewImgWidth == null) {
      $viewImgWidth = $("#bigView img").outerWidth();
      $viewImgHeight = $("#bigView img").height();
      if ($viewImgWidth < 200 || $viewImgHeight < 200) {
        $viewImgWidth = $viewImgHeight = 800;
      }

    }
    var scrollX = X * $viewImgWidth / $imgWidth;
    var scrollY = Y * $viewImgHeight / $imgHeight;
    $("#bigView img").css({ "left": scrollX * -1, "top": scrollY * -1 });
//    $("#bigView").css({ "top": 0, "left": $(".preview").offset().left + $(".preview").width() - 132 });

    return { left: X, top: Y };
  }

  $(".list-img").mousemove(function(){
    $(".content-video").hide();
      });
  $(".list-video-img").mousemove(function(){
    $(".content-video").show();
      });


  $('#imageMenu li').hover(function(){
       $(this).siblings().removeClass('list-img-hover');
       $(this).addClass('list-img-hover');
  });


});


    //收藏分享处下拉操作
    jQuery.divselect = function(divselectid,inputselectid) {
      var inputselect = $(inputselectid);
      $(divselectid).mouseover(function(){
          var ul = $(divselectid+" ul");
          ul.slideDown("fast");
          if(ul.css("display")=="none"){
              ul.slideDown("fast");
          }
      });
      $(divselectid).live('mouseleave',function(){
          $(divselectid+" ul").hide();
      });
    };
$(function(){
    // 分享收藏下拉操作
    $.divselect("#handle-l");
    $.divselect("#handle-r");
});

$(function(){
/** goods.php **/
	// 商品内容部分折叠收起侧边栏控制
	$('#fold').click(function(){
  		$('.ncs-goods-layout').toggleClass('expanded');
	});
	// 商品内容介绍Tab样式切换控制
	$('#categorymenu').find("li").click(function(){
		$('#categorymenu').find("li").removeClass('current');
		$(this).addClass('current');
	});
	// $('.hd').hide();
	// $('#guessDetailInfo').show();
	// 商品详情默认情况下显示全部
	$('#guessDetail').click(function(){
		$('.hd').css('display','none');
		$('#guessDetailInfo').css('display','');
	});
	$('#guessLog').click(function(){
		$('.hd').css('display','none');
		$('#guess_member').css('display','');
	});
	// 点击地图隐藏其他以及其标题栏
	$('#sellerPromise').click(function(){
		$('.hd').css('display','none');
		$('#sellerProInfo').css('display','');
	});
	// 点击评价隐藏其他以及其标题栏
	// 点击咨询隐藏其他以及其标题
	$('#guessRule').click(function(){
		$('.hd').css('display','none');
		$('#guellRulelInfo').css('display','');
	});

	//商品排行Tab切换
	$(".ncs-top-tab > li > a").mouseover(function(e) {
		if (e.target == this) {
			var tabs = $(this).parent().parent().children("li");
			var panels = $(this).parent().parent().parent().children(".ncs-top-panel");
			var index = $.inArray(this, $(this).parent().parent().find("a"));
			if (panels.eq(index)[0]) {
				tabs.removeClass("current ").eq(index).addClass("current ");
				panels.addClass("hide").eq(index).removeClass("hide");
			}
		}
	});
	//信用评价动态评分打分人次Tab切换
	$(".ncs-rate-tab > li > a").mouseover(function(e) {
		if (e.target == this) {
			var tabs = $(this).parent().parent().children("li");
			var panels = $(this).parent().parent().parent().children(".ncs-rate-panel");
			var index = $.inArray(this, $(this).parent().parent().find("a"));
			if (panels.eq(index)[0]) {
				tabs.removeClass("current ").eq(index).addClass("current ");
				panels.addClass("hide").eq(index).removeClass("hide");
			}
		}
	});

//触及显示缩略图
	$('.goods-pic > .thumb').hover(
		function(){
			$(this).next().css('display','block');
		},
		function(){
			$(this).next().css('display','none');
		}
	);
// 保证金可加减
        var minRecord=parseInt($(".buy-num").attr('data-min'));
        $(".buy-num").val(parseInt(minRecord));
        //出价，减
        $(".minus").click(function () {
            var buynum = parseInt($(".buy-num").val());
                            console.log(minRecord);
            if (buynum >= minRecord +1) {
                $(".buy-num").val(parseInt(buynum - 1));
            }
        });
        //出价
        $(".add").click(function () {
            var buynum = parseInt($(".buy-num").val());
            var maxNum=1000000000;
            if (buynum < 999999999) {
                $(".buy-num").val(parseInt(buynum + 1));
            }else{
                $(".buy-num").val(parseInt(maxNum));
            }
        });
});
</script>
<?php echo getChat($layout);?>
</style>