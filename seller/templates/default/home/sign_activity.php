<!doctype html>
<html class="fullScreen">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>艺诺网首席文化顾问——刘传铭先生</title>
  <meta name="viewport"
        content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
  <meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
  <meta content="no-cache,must-revalidate" http-equiv="Cache-Control">
  <meta content="no-cache" http-equiv="pragma">
  <meta content="0" http-equiv="expires">
  <meta content="telephone=no, address=no" name="format-detection">
  <meta name="HandheldFriendly" content="true">
  <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
  <!-- uc强制竖屏 -->
  <meta name="screen-orientation" content="portrait">
  <!-- QQ强制竖屏 -->
  <meta name="x5-orientation" content="portrait">
  <!-- UC强制全屏 -->
  <meta name="full-screen" content="yes">
  <!-- QQ强制全屏 -->
  <meta name="x5-fullscreen" content="true">
  <!-- UC应用模式 -->
  <meta name="browsermode" content="application">
  <!-- QQ应用模式 -->
  <meta name="x5-page-mode" content="app">
  <!-- windows phone 点击无高光 -->
  <meta name="msapplication-tap-highlight" content="no">
  <!-- 适应移动端end -->
  <!-- apple devices fullscreen -->
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
  <!-- Mobile Devices -->
  <meta name="msapplication-TileColor" content="#800000"/>
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-touch-fullscreen" content="yes"/>
  <meta name="msapplication-tap-highlight" content="no"/>
  <meta name="msapplication-tap-highlight" content="no"/>
  <link rel="stylesheet" href="/wap/dist/css/main.css">
  <style>
    .head-search-bar .input-text{
      height:38px;
    }
    #ncToolbar{
      display: none;
    }
    .lm_home_nav{
      background: #fff;
    }
    .header-wrap{
      margin-top: 0;
    }
  </style>
</head>
 <body style="background: #f9f8f6;">
<div class="liuchuanming_con">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_01.jpg" alt="">
  <div class="liuchuanming_viedo_con">
    <img class="img" src="/shop/activity/image/sign_activity/sign_activity_02.jpg" alt="">
    <div class="liuchuanming_viedo_wrapper">
        <div class="liuchuanming_viedo">
          <iframe height=320 width=534 src='http://player.youku.com/embed/XMzAyNTc5NjM0OA=='frameborder=0'allowfullscreen'></iframe>
        </div>
      <div class="liuchuanming_text">
        <img src="/shop/activity/image/sign_activity/sign_activity_text.png" alt="">
      </div>
    </div>
  </div>
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_03.jpg" alt="">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_04.jpg" alt="">
  <a target='_blank' href="http://news.china.com/focus/sczlklhyd/11182931/20160911/23524645.html">
    <img class="img" src="/shop/activity/image/sign_activity/sign_activity_05.jpg" alt="">
  </a>
  <a target='_blank' href="http://history.sina.com.cn/sino/xs/2014-11-06/1734106968.shtml">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_06.jpg" alt="">
  </a>
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_07.jpg" alt="">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_08.jpg" alt="">
  <a target='_blank' href="http://art.people.com.cn/n1/2016/1114/c356129-28859456.html">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_09.jpg" alt="">
  </a>
  <a target='_blank' href="http://www.arts168.com/display.aspx?id=615">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_10.jpg" alt="">
  </a>
  <a target='_blank' href="http://book.ifeng.com/yeneizixun/detail_2014_07/21/101739_0.shtml">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_11.jpg" alt="">
  </a>
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_12.jpg" alt="">
  <img class="img" src="/shop/activity/image/sign_activity/sign_activity_13.jpg" alt="">
</div>
<script type="text/javascript" src="/data/resource/js/jquery.js"></script>
<script type="text/javascript" src="/wap/js/config.js"></script>
<script type="text/javascript" src="<?php echo SELLER_RESOURCE_SITE_URL; ?>/js/home_index.js" charset="utf-8"></script>
<script>
    $('.site-logo').find('img').attr('src','/data/upload/shop/common/logo.png');
    if($('.liuchuanming_viedo_wrapper').outerWidth()>=1200){
        $('iframe').attr('width',734)
        $('iframe').attr('height',428)
    }
</script>
</body>
</html>
