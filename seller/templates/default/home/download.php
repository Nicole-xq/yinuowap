<!doctype html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>下载</title>
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
    <meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
    <meta content="no-cache,must-revalidate" http-equiv="Cache-Control">
    <meta content="no-cache" http-equiv="pragma">
    <meta content="0" http-equiv="expires">
    <meta content="telephone=no, address=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <!-- uc强制竖屏 -->
    <meta name="screen-orientation" content="portrait">
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait">
    <!-- UC强制全屏 -->
    <meta name="full-screen" content="yes">
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="true">
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app">
    <!-- windows phone 点击无高光 -->
    <meta name="msapplication-tap-highlight" content="no">
    <!-- 适应移动端end -->
    <!-- apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <!-- Mobile Devices -->
    <meta name="msapplication-TileColor" content="#800000"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-touch-fullscreen" content="yes"/>
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="stylesheet" href="<?=SELLER_TEMPLATES_URL?>/css/normalize.css">
    <link rel="stylesheet" href="<?=SELLER_TEMPLATES_URL?>/css/style.css">
    <!--<link rel="stylesheet" href="../../wap/css/lm/activity.css">-->
</head>
<body style="background: #fff">
    <div class="download_img">
        <img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_01.jpg" alt="">
        <img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_02.jpg" alt="">
        <img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_03.jpg" alt="">
        <img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_04.jpg" alt="">
        <div class="download_btn">
            <div class="download_btn_top">
                <a href="http://a.app.qq.com/o/simple.jsp?pkgname=com.yinuovip.mob&from=singlemessage&isappinstalled=0" class="andr_download"><img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn.jpg" alt=""></a>
                <a href="https://itunes.apple.com/cn/app/%E8%89%BA%E8%AF%BA%E7%BD%91/id1274965309?mt=8"><img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn1.jpg" alt=""></a>
            </div>
                <div class="andr_download andr_download_hide">
                    <a class="andr_download_left"><img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn2.png" alt=""></a>
                    <div class="andr_download_right">
                        <a href=""><img class="qll_img_width" src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn3.png" alt=""></a>
                        <a href=""><img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn4.png" alt=""></a>
                        <a href=""><img src="<?=SELLER_TEMPLATES_URL?>/images/download/download_btn5.png" alt=""></a>
                    </div>
                </div>
        </div>
    </div>


<script type="text/javascript" src="../../../wap/js/config.js"></script>
<script type="text/javascript" src="../../../data/resource/js/jquery.js"></script>
<!--<script type="text/javascript">-->
    <!--$('.andr_download').click(function () {-->
        <!--$('.andr_download').toggleClass('andr_download_hide');-->
    <!--});-->
<!--</script>-->

</body>
</html>





