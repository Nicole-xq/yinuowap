<?php defined('InShopNC') or exit('Access Invalid!');?>


<style type="text/css" media="screen">
    .xieyi_lm {
        width: 1200px;
        margin: 0 auto;
        height: 496px;
        overflow-x: hidden;
        border: #eee solid 1px;
    }

    .xieyi_lm .xieyi_con {
        padding: 25px;
    }

    .xieyi_lm .xieyi_con .title {
        font-size: 16px;
        color: #777;
        margin: 0 0 26px 0;
    }

    .xieyi_lm .xieyi_con .text {
        font-size: 14px;
        line-height: 1.75;
    }

    .mb_32 {
        margin-bottom: 32px
    }
</style>

<!--签署入驻协议-->
<div class="mingcheng">注册协议</div>
<div class="xieyi_lm">
    <!--  <textarea>《艺诺网规则协议》 第一章 概述                          </textarea> -->
    <div class="xieyi_con">
        <?php echo $output['agreement'];?>
    </div>
</div>
<div class="ruzhudanxuan">
    <label for='agree'><input id="input_apply_agreement" name="input_apply_agreement" type="radio" name="agree" value="单选"> 我已阅读并同意以上协议</label>
</div>
<div class="anniu_shuang">
    <a class="new" id='btn_apply_agreement_next' href="javascript:;">下一步，个人资料填写</a>
</div>


<script type="text/javascript">
$(document).ready(function(){
    $('#btn_apply_agreement_next').on('click', function() {
        if($('#input_apply_agreement').prop('checked')) {
            window.location.href = "index.php?act=store_joinin&op=step1";
        } else {
            alert('请阅读并同意协议');
        }
    });
});
</script>