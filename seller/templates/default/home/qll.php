<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/qll_profit.css" rel="stylesheet" type="text/css">

<div class="qll_profit_banner">
    <img src="<?php echo SELLER_TEMPLATES_URL;?>/images/qll/profit_banner.jpg" alt="" class="profit_img">
    <a href=""  class="profit_btn"><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/qll/profit_btn.png" alt=""></a>
</div>
<div class="profit_content">
    <h1 class="profit_title">30天年化收益</h1>
    <ul class="qll_home_mod1 lm_clear">

        <li class="lm_clear ">
            <div class="qll_home_mod1_left">
                <a target="_blank" href="">
                    <img src="" alt="">
                </a>
                <span class="qll_finance_ad">
                    <span>
                        保证金年收益率<strong>6.00</strong>%
                        计息期30天
                        <a class="qll_bzjsy" href="<?=C('cms_site_url')?>/index.php?act=special&op=special_detail&special_id=43" target="_blank">什么是保证金送收益?</a>
                    </span>
              </span>
            </div>
            <a class="qll_home_mod1_right" href="">
                <div class="qll_counttop">
                    <h2 class="qll_title">邓刚侗乡风情个人画展专场</h2>
                    <span class="qll_address">邓刚艺术馆</span>
                    <div class="qll_info lm_clear">
                        <span class="qll_count">拍品数 30件</span>
                        <span class="vote_count"><i><img src="<?php echo SELLER_TEMPLATES_URL;?>/lm/images/lm_eye.png" alt=""></i>36482人次</span>
                    </div>
                </div>

                <div class="qll_countdown" data-date="827905">
                    <span class="qll_timestate">距赠送收益截止</span>
                    <div class="qll_date">
                        <div class="qll_three qll_date_info">09</div><span>天</span>
                        <div class="qll_two   qll_date_info">11</div><span>时</span>
                        <div class="qll_one   qll_date_info">42</div><span>分</span>
                    </div>
                </div>
            </a>
        </li>

    </ul>
</div>
