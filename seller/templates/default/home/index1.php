<link href="<?php echo SELLER_TEMPLATES_URL; ?>/css/index.css" rel="stylesheet" type="text/css">
<link href="<?php echo SELLER_TEMPLATES_URL;?>/css/qll_profit.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SELLER_RESOURCE_SITE_URL; ?>/js/home_index.js" charset="utf-8"></script>
<style type="text/css">
  .category {
    display: block !important;
  }

  .home-focus-layout .right-sidebar {
    filter: progid:DXImageTransform.Microsoft.gradient(enabled='true', startColorstr='#B2000000', endColorstr='#B2000000');
    background: rgba(0, 0, 0, 0.75);
    width: 120px;
    border: none;
    margin-left: 480px;
  }

  .home-focus-layout .right-sidebar .top-account img,
  .home-focus-layout .right-sidebar .activity img {
    margin: 12px auto;
    width: 68px;
    height: 68px;
    overflow: hidden;
    border-radius: 100px;
  }

  .home-focus-layout .right-sidebar .top-account p {
    text-align: center;
    font-size: 14px;
    color: #fff;
    width: 120px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  .home-focus-layout .right-sidebar .top-account a,
  .home-focus-layout .right-sidebar .activity a {
    color: #fff;
    text-align: center;
    display: block
  }

  .home-focus-layout .right-sidebar .login {
    padding: 2px 14px;
    background: #800000;
    width: 54px;
    border-radius: 100px;
    margin: 8px auto 6px !important
  }

  .home-focus-layout .right-sidebar .activity h1 {
    background: #000;
    text-align: center;
    color: #fff;
    padding: 6px 0;
    margin: 0 0 6px;
    width: 122px;
    transform: translateX(-1px);
    font-size: 14px
  }

  .home-focus-layout .right-sidebar .activity a {
    margin: 4px 0;
    font-size: 13px
  }

  #ncToolbar {
    display: none;
  }

  .right-sidebar .activity {
    margin: 12px 0
  }

  .activity-name {
    display: block;
    width: 80px;
    text-align: center;
    margin: 12px auto 0;
    font-size: 14px;
    line-height: 24px
  }

  #new-tab .tab_menu {
    width: 881px
  }

  .hot-list-top {
    width: 317px;
    height: 40px;
    background: #fff;
    border: #e5e5e5 solid 1px;
    border-left: none;
    margin-top: 20px
  }

  .hot-list {
    margin: 5px 0 0
  }
 .lm_bzjsy{
   font-size: 14px;
   color: #fff;
     margin-left: 10px;
 }
  .auction_flag{
      width: 4rem;
      height: 1.5rem;
      line-height: 1.5rem;
      text-align: center;
      display: inline-block;
      border-radius: 2.5rem;
      background: #fabf13;
      font-size: 1rem;
      opacity: 0.8;
      color: #000;
      vertical-align:3px;
  }
</style>
<!--OLD SILDER-->
<div class="lm_home_top_silder">
    <?php echo $output['web_html']['index_pic']; ?>
</div>
<!--OLD SILDER END-->
<section class="lm_home_pub ">
    <div class="lm_home_top mtb15">
        <h2 class="lm_home_titile qll_home_titile">
            <img src="<?php echo SELLER_TEMPLATES_URL; ?>/images/qll/profit_title_1.png" alt="">
        </h2>
    </div>
    <div class="lm_home_con">
        <ul class="qll_profit lm_clear">
            <?php if(!empty($output['store_list'])){ ?>
                <?php foreach ($output['store_list'] as $key=>$val) {?>
                    <li>
                        <div class="qll_profit_img">
                            <a href="<?php echo urlVendue('special_details','index',array('special_id'=>$val['special_id']))?>">
                                <img src="<?=$val['wap_image_path']?>" alt="">
                            </a>
                            <span class="qll_finance_ad">
                        <span>
                            保证金年收益率<strong><?=$val['special_rate']?></strong>%
                            计息期<?=$val['day_num']?>天
                        </span>
                    </span>
                        </div>
                        <div class="profit_activity_info">
                            <div class="profit_activity_info_top lm_clear">
                                <h2><?=$val['special_name']?></h2>
                                <div class="profit_activity_details">
                                    <span class="qll_address"><?=$val['store_name']?></span>
                                    <span class="qll_count">拍品数 <?=$val['auction_num']?>件</span>
                                    <span class="vote_count"><i><img src="<?php echo SELLER_TEMPLATES_URL;?>/lm/images/lm_eye.png" alt=""></i><?=$val['special_click']?>人次</span>
                                </div>
                            </div>
                            <div class="profit_activity_info_bottom lm_countdown" data-date="<?=$val['special_remain_rate_time']?>">
                                <span class="qll_timestate">距赠送收益截止</span>
                                <?php// date('Y-m-d H:i:s',$val['special_rate_time'])?>
                                <?php// $val['special_remain_rate_time'] ?>
                                <div class="lm_date">
                                    <div class="lm_three lm_date_info">09</div><span>天</span>
                                    <div class="lm_two   lm_date_info">11</div><span>时</span>
                                    <div class="lm_one   lm_date_info">42</div><span>分</span>
                                </div>
                            </div>
                        </div>
                    </li>

                <?php }}?>
        </ul>
        <a style="margin-top: 20px;display: block;text-align: right;font-size: 16px;color: #e90525;" href="http://www.yinuovip.com/shop/activity/download.html">关注微信公众号或下载APP体验新手标（奖励金年化率高达10%）<span style="color: #000;">>></span></a>
    </div>
</section>
<!--lm auction-->
<section class="lm_home_pub ">
  <div class="lm_home_top mtb15">
    <h2 class="lm_home_titile "><img
          src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_pub1.png"
          alt=""></h2>
  </div>
  <div class="lm_home_con">
    <ul class="lm_home_mod1 lm_clear">
        <?php $special_num = 1; foreach($output['start_special_list'] as $row):?>

            <?php $show_class = ''; if($special_num > 3){$show_class = 'lm_home_mod1_small';} $special_num++;?>
            <?php if($row['special_start_time'] < TIMESTAMP ){ $show_class .= ' lm_state_over'; } ?>
          <li class="lm_clear <?php echo $show_class; ?> ">
            <div class="lm_home_mod1_left">
              <a target="_blank" href="<?php echo urlVendue('special_details','index',array('special_id'=>$row['special_id']))?>">
                <img src="<?php echo getVendueLogo($row['special_image'])?>" alt="">
              </a>
                <?php if ($row['special_rate'] >0){?>
              <span class="lm_finance_ad"><span>保证金年收益率<strong><?php echo $row['special_rate']?></strong>%
                      <a class="lm_bzjsy" href="<?=C('cms_site_url')?>/index.php?act=special&op=special_detail&special_id=43" target="_blank">什么是保证金送收益?</a></span>
              </span>
                <?php } ?>
            </div>
            <a class="lm_home_mod1_right" href=<?php echo urlVendue('special_details','index',array('special_id'=>$row['special_id']))?>">
                    <h2 class="lm_title"><?php echo $row['special_name'];?></h2>
            <span class="lm_address"><?php echo $row['delivery_mechanism'];?></span>
            <div class="lm_info lm_clear">
              <span class="lm_count">拍品数 <?php echo $row['auction_count'];?>件</span>
              <?php if($row['special_start_time'] < TIMESTAMP ){ ?>
              <div class="lm_middle_info">
                  <span class="lm_price_state">已出价</span>
                  <span class="lm_people_count"><?php echo $row['all_bid_number'];?>人次</span>
              </div>
              <?php } ?>
              <span class="vote_count"><i><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_eye.png" alt=""></i><?php echo $row['special_click'];?>人次</span>
            </div>
            <div class="lm_countdown" data-date="<?php echo $row['end_timestamp'];?>" >
              <span class="lm_timestate"><?php if($row['special_start_time'] < TIMESTAMP ):?>距结束<?php else:?>距开始<?php endif;?></span>
              <div class="lm_date" >
                <div class="lm_three lm_date_info">0</div>
                <span>天</span>
                <div class="lm_two   lm_date_info">0</div>
                <span>时</span>
                <div class="lm_one   lm_date_info">0</div>
                <span>分</span>
              </div>
            </div>
            </a>
          </li>
        <?php endforeach;?>
    </ul>
    <span class="lm_notfound_arts">找不到您要的专场？</span>
    <a target="_blank" href="<?php echo urlAuction(); ?>" class="lm_more">浏览所有拍卖专场</a>
  </div>
</section>
<!--lm auction end-->
<section class="lm_home_pub lm_home_mod2" id="lm_pin_container">
  <div class="lm_home_top">
    <h2 class="lm_home_titile"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_pub2.png" alt=""></h2>
  </div>
  <div class="lm_home_con ">
    <section class="lm_home_mod2_one">
      <h2 class="lm_title"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_mod2_title1.png" alt="">国画西画
      </h2>
      <ul class="lm_product_list lm_clear">

          <?php foreach($output['gh_goods'] as $value):?>
            <li>
              <a  target="_blank" href="<?php echo urlSeller('goods', 'index', array('goods_id' => $value['goods_id']));?>" class="lm_img">
                <img src="<?php echo cthumb($value['goods_image'], 360, $value['store_id']); ?>" alt="">
              </a>
              <div class="lm_product_info">
                <h2 class="lm_product_info_title"><a href="#"><?php echo $value['goods_name']; ?></a></h2>
                <div class="lm_product_info_bottom">
                  <div class="lm_product_info_user">
                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>">
                      <img src="<?php echo getStoreLogo($value['store_info']['store_avatar']); ?>" alt="">
                    </a>
                    <span class="lm_text">
                                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>"><?php echo $value['store_info']['store_name']; ?></a>
                                </span>
                  </div>
                    <?php if($value['auction_flag'] == 1) { ?>
                        <span class="auction_flag">拍卖中</span>
                    <?php }else if($value['sales_model'] == 2) { ?>
                        <span class="auction_flag">可议价</span>
                    <?php }else{?>
                        <span class="lm_product_info_price">￥<?php echo $value['goods_promotion_price']; ?></span>
                    <?php }?>
                  <span class="lm_product_info_cell">
                                <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png" alt=""><?php echo $value['goods_collect']; ?>
                            </span>
                </div>
              </div>
            </li>
          <?php endforeach;?>

      </ul>
    </section>
    <section class="lm_home_mod2_two">
      <h2 class="lm_title"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_mod2_title2.png" alt="">珠宝玉翠
      </h2>
      <ul class="lm_product_list lm_clear">

          <?php foreach($output['zb_goods'] as $value):?>
            <li>
              <a  target="_blank" href="<?php echo urlSeller('goods', 'index', array('goods_id' => $value['goods_id']));?>" class="lm_img">
                <img src="<?php echo cthumb($value['goods_image'], 360, $value['store_id']); ?>" alt="">
              </a>
              <div class="lm_product_info">
                <h2 class="lm_product_info_title"><a href="#"><?php echo $value['goods_name']; ?></a></h2>
                <div class="lm_product_info_bottom">
                  <div class="lm_product_info_user">
                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>">
                      <img src="<?php echo getStoreLogo($value['store_info']['store_avatar']); ?>" alt="">
                    </a>
                    <span class="lm_text">
                                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>"><?php echo $value['store_info']['store_name']; ?></a>
                                </span>
                  </div>
                    <?php if($value['auction_flag'] == 1) { ?>
                        <span class="auction_flag">拍卖中</span>
                    <?php }else if($value['sales_model'] == 2) { ?>
                        <span class="auction_flag">可议价</span>
                    <?php }else{?>
                        <span class="lm_product_info_price">￥<?php echo $value['goods_promotion_price']; ?></span>
                    <?php }?>
                  <span class="lm_product_info_cell">
                                <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png" alt=""><?php echo $value['goods_collect']; ?>
                            </span>
                </div>
              </div>
            </li>
          <?php endforeach;?>

      </ul>
    </section>
    <section class="lm_home_mod2_three">
      <h2 class="lm_title"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_mod2_title3.png" alt="">紫砂陶瓷
      </h2>
      <ul class="lm_product_list lm_clear">

          <?php foreach($output['zs_goods'] as $value):?>
            <li>
              <a  target="_blank" href="<?php echo urlSeller('goods', 'index', array('goods_id' => $value['goods_id']));?>" class="lm_img">
                <img src="<?php echo cthumb($value['goods_image'], 360, $value['store_id']); ?>" alt="">
              </a>
              <div class="lm_product_info">
                <h2 class="lm_product_info_title"><a href="#"><?php echo $value['goods_name']; ?></a></h2>
                <div class="lm_product_info_bottom">
                  <div class="lm_product_info_user">
                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>">
                      <img src="<?php echo getStoreLogo($value['store_info']['store_avatar']); ?>" alt="">
                    </a>
                    <span class="lm_text">
                                    <a target="_blank" href="<?php echo urlSeller('show_store', 'index', array('store_id' => $value['store_id']));?>"><?php echo $value['store_info']['store_name']; ?></a>
                                </span>
                  </div>
                    <?php if($value['auction_flag'] == 1) { ?>
                        <span class="auction_flag">拍卖中</span>
                    <?php }else if($value['sales_model'] == 2) { ?>
                        <span class="auction_flag">可议价</span>
                    <?php }else{?>
                        <span class="lm_product_info_price">￥<?php echo $value['goods_promotion_price']; ?></span>
                    <?php }?>
                  <span class="lm_product_info_cell">
                                <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png" alt=""><?php echo $value['goods_collect']; ?>
                            </span>
                </div>
              </div>
            </li>
          <?php endforeach;?>

      </ul>
    </section>
    <div class="lm_home_mod2_left">
      <ul>
        <li><span>国画</span><span>西画</span></li>
        <li><span>珠宝</span><span>玉翠</span></li>
        <li><span>紫砂</span><span>陶瓷</span></li>
      </ul>
    </div>
    <span class="lm_notfound_arts">没有您感兴趣的藏品？</span>
    <a target="_blank" href="<?php echo urlSeller('search', 'index'); ?>" class="lm_more">浏览所有商城藏品</a>
  </div>
</section>
<!--第二块结束-->
<section class="lm_home_pub lm_home_mod3">
  <div class="lm_home_top">
    <h2 class="lm_home_titile"><img
          src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_pub3.png"
          alt=""></h2>
  </div>
  <div class="lm_home_con ">
    <ul class="lm_clear">

        <?php foreach($output['rec_guess_list'] as $k => $v):?>
          <li>
            <a target="_blank" href="<?php echo urlSeller('show_guess','index',array('gs_act_id' => $v['id']));?>">
              <div class="lm_toptitle"><?php echo $v['name']; ?></div>
              <div class="lm_home_mod3_content">
                <img src="<?php echo  UPLOAD_SITE_URL.DS.ATTACH_PATH.DS.'pay_guess'.DS.$v['gs_act_img'];?>" alt="">
                <div class="lm_participate_info">
                  <span><?php echo  $v['number'];?></span>人参与
                </div>
                <a href="#" class="lm_tips"></a>
              </div>
              <div class="lm_countdown" data-date="<?php echo  $v['tend'] - TIMESTAMP;?>">
                <div class="lm_date">
                  <div class="lm_three lm_date_info">05</div>
                  <span>时</span>
                  <div class="lm_two   lm_date_info">05</div>
                  <span>分</span>
                  <div class="lm_one   lm_date_info">05</div>
                  <span>秒</span>
                </div>
              </div>
            </a>
          </li>
        <?php endforeach;?>

    </ul>
    <a target="_blank" href="<?php echo urlSeller('show_guess', 'index'); ?>" class="lm_more">更多趣猜专场等着您</a>
  </div>
</section>
<!--第三块结束-->
<section class="lm_home_pub lm_home_mod4">
  <div class="lm_home_top">
    <h2 class="lm_home_titile"><img
          src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_pub4.png"
          alt=""></h2>
  </div>
  <div class="lm_home_con">
    <div class="lm_home_left">
      <ul>
        <li><span>国画</span><span>西画</span></li>
        <li><span>珠宝</span><span>玉翠</span></li>
        <li><span>紫砂</span><span>陶瓷</span></li>
      </ul>
    </div>
    <div class="lm_home_center">
      <div class="lm_home_center_tab">
        <div class="lm_home_center_title">最新推荐</div>
        <div class="lm_home_double">
          <div class="lm_home_double_con">
            <div class="lm_home_double_wrapper">
              <ul class="lm_home_recommend">
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=15">
                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05467030181089883.png"
                           alt="潘柬芝">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <a href="index.php?act=index&amp;op=artist_detail&amp;artist_id=15">
                          <h3 class="lm_home_title">潘柬芝</h3>
                          <span class="lm_home_path">黑龙江大学艺术学院</span>
                          <span class="lm_home_cell"><img
                                src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                                alt="">
                              <?php echo $output['follow'][15]; ?></span>
                          <span class="lm_home_address"><img
                                src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                                alt="">
                                深圳</span>
                        </a>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=9">
                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05461945178121754.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">杨茂东</h3>
                        <span class="lm_home_path">中央美院</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][9]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                北京</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=4">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05442965306833373.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">陆振远</h3>
                        <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][4]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                北京</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=7">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443552031814077.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">哑翁</h3>
                        <span class="lm_home_path">山东大学</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][7]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                山东</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=6">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443110993492125.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">赵长国</h3>
                        <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][6]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                山东</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=10">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05463441377329194.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">王祥彬</h3>
                        <span class="lm_home_path">黑龙江大学</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][10]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                深圳</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=5">
                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443760025386525.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">一帥</h3>
                        <span class="lm_home_path">博爱比安奇设计学院</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][5]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                越南</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=1">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443013181379883.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">邓刚</h3>
                        <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][1]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                湖南</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=3">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05442948975729514.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">张家玮</h3>
                        <span class="lm_home_path">中国美术学院</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][3]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                杭州</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=2">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05442929408466239.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">邓辉</h3>
                        <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][2]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                北京</span>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
              <ul class="lm_home_recommend">
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=8">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443613505125132.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">李仁渤</h3>
                        <span class="lm_home_path">清华美院</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][8]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                北京</span>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=12">

                    <div class="lm_home_img">
                      <img src="http://www.yinuovip.com/data/upload/shop/vendue/05464598434185751.jpg"
                           alt="">
                    </div>
                    <div class="lm_home_info">
                      <div class="lm_home_info_con">
                        <h3 class="lm_home_title">王威</h3>
                        <span class="lm_home_path">福州大学</span>
                        <span class="lm_home_cell"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                              alt="">
                            <?php echo $output['follow'][12]; ?></span>
                        <span class="lm_home_address"><img
                              src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                              alt="">
                                江苏</span>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <span class="lm_next"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_next.png"
                                     alt=""></span>
        </div>
        <div class="lm_home_center_title">最新注册</div>
        <div class="lm_new_registed_list lm_pub_lag">
          <div class="pub_lag_con">
            <ul class="lm_home_recommend">
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=8">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443613505125132.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">李仁渤</h3>
                      <span class="lm_home_path">清华美院</span>
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][8]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                北京</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=12">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05464598434185751.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">王威</h3>
                      <span class="lm_home_path">福州大学</span>
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][12]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                江苏</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=6">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443110993492125.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">赵长国</h3>
                      <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][6]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                山东</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=10">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05463441377329194.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">王祥彬</h3>
                      <span class="lm_home_path">黑龙江大学</span>
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][10]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                深圳</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=5">
                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443760025386525.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">一帥</h3>
                      <span class="lm_home_path">博爱比安奇设计学院</span>
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][5]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                越南</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=1">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05443013181379883.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">邓刚</h3>
                      <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][1]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                湖南</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=3">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05442948975729514.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">张家玮</h3>
                      <span class="lm_home_path">中国美术学院</span>
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][3]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                杭州</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="/auction/index.php?act=index&amp;op=artist_detail&amp;artist_id=2">

                  <div class="lm_home_img">
                    <img src="http://www.yinuovip.com/data/upload/shop/vendue/05442929408466239.jpg"
                         alt="">
                  </div>
                  <div class="lm_home_info">
                    <div class="lm_home_info_con">
                      <h3 class="lm_home_title">邓辉</h3>
                      <!--                                            <span class="lm_home_path">江西雕塑学院</span>-->
                      <span class="lm_home_cell"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_heart.png"
                            alt="">
                          <?php echo $output['follow'][2]; ?></span>
                      <span class="lm_home_address"><img
                            src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_address.png"
                            alt="">
                                北京</span>
                    </div>
                  </div>
                </a>
              </li>

            </ul>
          </div>
          <span class="lm_next"><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_next.png"
                                     alt=""></span>
        </div>
      </div>
    </div>
    <div class="lm_home_right">
      <div class="lm_home_right_title">相关新闻</div>
      <div class="lm_home_big_news">
          <?php $first_article = current($output['info_list']); unset($output['info_list'][0]);?>
        <a target="_blank" href="<?php echo urlCMS('article', 'article_detail', array('article_id' => $first_article['article_id'])); ?>"><img src="<?php echo getCMSArticleImageUrl($first_article['article_attachment_path'], $first_article['article_image'], 'max');?>" alt=""></a>
        <a target="_blank" href="<?php echo urlCMS('article', 'article_detail', array('article_id' => $first_article['article_id'])); ?>" class="lm_title"><?php echo $first_article['article_title'];?></a>
      </div>

      <ul class="lm_home_news">
          <?php foreach($output['info_list'] as $value):?>
            <li><a target="_blank" href="<?php echo urlCMS('article', 'article_detail', array('article_id' => $value['article_id'])); ?>">
                【<?php echo $output['info_class_list'][$value['article_class_id']]['class_name'];?>】<?php echo $value['article_title'];?>
              </a></li>
          <?php endforeach;?>
      </ul>
    </div>
  </div>
</section>
<!--第四块结束-->
<section class="lm_home_pub lm_home_mod5">
  <div class="lm_home_top">
    <h2 class="lm_home_titile">
      <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_pub5.png" alt="">
    </h2>
  </div>
  <div class="lm_home_con">
    <ul>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_1.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_2.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_3.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_4.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_5.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_6.png" alt=""></li>
      <li><img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_footer_7.png" alt=""></li>
    </ul>
  </div>
</section>
<!--第五块结束-->
<section class="lm_home_footer_info">
  <div class="lm_home_con">
    <ul>
      <li>
        <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_footer1.png" alt="">
        <h3 class="title">如实描述承诺</h3>
        <p class="text">所有艺术品根据实物进行如实描述</p>
      </li>
      <li>
        <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_footer2.png" alt="">
        <h3 class="title">商家保证金保障</h3>
        <p class="text">保障金存入艺诺会保障各商家的资金安全</p>
      </li>
      <li>
        <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_footer3.png" alt="">
        <h3 class="title">全场正品保障</h3>
        <p class="text">所有艺术品都是正品，假一赔十。</p>
      </li>
      <li>
        <img src="<?php echo SELLER_TEMPLATES_URL; ?>/lm/images/lm_home_footer4.png" alt="">
        <h3 class="title">拍卖保证金收益</h3>
        <p class="text">购买拍品时保证金会在拍卖期间得丰厚的收益</p>
      </li>
    </ul>
  </div>
</section>
<!--start cur_local -->
<div class="nch-breadcrumb-layout">
    <?php if (!empty($output['nav_link_list']) && is_array($output['nav_link_list'])) { ?>
      <div class="nch-breadcrumb wrapper"><i class="icon-home"></i>
          <?php foreach ($output['nav_link_list'] as $nav_link) { ?>
              <?php if (!empty($nav_link['link'])) { ?>
              <span><a href="<?php echo $nav_link['link']; ?>"><?php echo $nav_link['title']; ?></a></span><span
                  class="arrow">></span>
              <?php } else { ?>
              <span><?php echo $nav_link['title']; ?></span>
              <?php } ?>
          <?php } ?>
      </div>
    <?php } ?>
</div>
<!--end  cur_local -->
