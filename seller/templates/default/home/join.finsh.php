<!-- 内容区 -->
<div class="lm_upload">
    <div class="lm_container_ful lm_rz_success_con">
        <h3 class="title">
            <?php if($output['type'] == 2 ):?>
            您的入驻申请未通过，其原因：<?php echo $output['joinin_detail']['joinin_message']; ?>。请再次编辑申请资料。
            <?php elseif($output['type'] == 3):?>
            您的入驻申请已经通过，总计需要缴纳<?php echo $output['joinin_detail']['paying_amount']; ?>元，请前往个人中心充值
            <?php elseif($output['type'] == 5):?>
            您的入驻申请已经通过，请前往商家管理中心。
            <?php elseif($output['type'] == 1):?>
            入驻申请已经提交成功，1-5个工作日，我们工作人员会主动联系您核实信息
            <?php endif;?>
        </h3>
        <div class="lm_rz_success">
             <p class="text">您申请的入驻编号：<?php echo $output['joinin_detail']['record_number']; ?></p>
             <p class="text">联系电话：400-135-2688</p>
        </div>
    </div>
</div>
<div class="anniu_shuang">
    <?php if($output['type'] == 5): ?>
    <a class="new" href="<?php echo urlSeller('seller_login', 'show_login'); ?>">前往商家管理中心</a>
    <?php else:?>
    <a class="new" href="settled.html">联系在线客服</a>
    <?php endif;?>
</div>


