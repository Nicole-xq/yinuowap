<?php defined('InShopNC') or exit('Access Invalid!');?>

<script type="text/javascript" src="<?php echo SELLER_TEMPLATES_URL;?>/js/join/join.js"></script>

<!--签署入驻协议-->
<div class="mingcheng">选择入驻类目</div>
<!-- 个人资料提交 -->
<div class="lm_upload">
    <div class="lm_container_ful">
        <form  class="lm_upload_form" id="form_store_info" action="index.php?act=artist_joinin&op=step4" method="post" >
            <fieldset>

                <div class="zm_form-group">
                    <label class="zm_label" for="bankname"><i class="lm_req">*</i>艺术家/经理人账号：</label>

                    <div class="zm_inp">
                        <input id="seller_name" name="seller_name" type="text" placeholder="">
                        <span></span>
                        <p class="emphasis">此账号为日后登录并管理艺术家中心时使用，注册后不可修改，请牢记。</p>
                    </div>
                </div>

                <div class="zm_form-group">
                    <label class="zm_label" for="bankname"><i class="lm_req">*</i>店铺名称：</label>

                    <div class="zm_inp">
                        <input id="store_name" name="store_name" type="text" placeholder="">
                        <span></span>
                        <p class="emphasis">店铺名称注册后不可修改，请认真填写。</p>
                    </div>
                </div>

                <div class="zm_form-group">
                    <label class="zm_label" for="bankname"><i class="lm_req">*</i>店铺等级：</label>

                    <div class="zm_inp">
                        <select name="sg_id" id="sg_id" style="width: 222px;">
                            <option value="">请选择</option>
                            <?php if(!empty($output['grade_list']) && is_array($output['grade_list'])){ ?>
                                <?php foreach($output['grade_list'] as $k => $v){ ?>
                                    <?php $goods_limit = empty($v['sg_goods_limit'])?'不限':$v['sg_goods_limit'];?>
                                    <?php $explain = '商品数：'.$goods_limit.' 模板数：'.$v['sg_template_number'].' 收费标准：'.$v['sg_price'].' 元/年 附加功能：'.$v['function_str'];?>
                                    <option value="<?php echo $v['sg_id'];?>"><?php echo $v['sg_name'];?> (<?php echo $explain;?>)</option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <span></span>
                    </div>
                </div>

                <div class="settled">

                    <?php foreach($output['gc_list'] as $category): ?>
                    <div class="zm_form-group">
                        <label class="zm_label"></label>
                        <div class="zm_inp" >
                            <label for="check_<?php echo $category['gc_id'];?>">
                            <input type="checkbox" class='lm_radio' id="check_<?php echo $category['gc_id'];?>"
                                   price="<?php echo $output['store_class'][$category['gc_name']]['sc_bail'];?>"
                                   sale="<?php echo $output['store_class'][$category['gc_name']]['sc_sale'];?>"
                                   pt_price="<?php echo $output['store_class'][$category['gc_name']]['sc_service_price'];?>"
                                   pt_sale="<?php echo $output['store_class'][$category['gc_name']]['sc_service_price_sale'];?>"
                                   value="<?php echo $category['gc_id'];?>" ><?php echo $category['gc_name'];?>
                            </label>

                            <div class="checkboxs">
                            <?php foreach($category['class2'] as $child_class): ?>
                                <label><input type="checkbox" name="store_class_ids[]" id='check1C1' value="<?php echo $child_class['gc_id'];?>"><?php echo $child_class['gc_name'];?></label>
                            <?php endforeach; ?>
                            </div>

                        </div>
                    </div>
                    <?php endforeach; ?>


                </div>

                <p>
                    <input class="submit" type="submit" value="Submit">
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div class="anniu_shuang">
    <a class="old" href="<?php echo urlSeller('artist_joinin', 'step2');?>">上一步，财务账号</a>
    <a class="new"  id="btn_apply_store_next" href="javascript:;"  href="rz_success.html" id='lm_bank_btn'>提交申请</a>
</div>


<div class="lm_hr mr30"></div>
<div class="lm_result">
    <div class="bzj">保证金：¥0，优惠：¥0</div>
    <div class="fwf">平台服务费：¥0，优惠：¥0</div>
    <div class="sum_result">合计：¥0<span></span></div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        jQuery.validator.addMethod("seller_name_exist", function(value, element, params) {
            var result = true;
            $.ajax({
                type:"GET",
                url:'<?php echo urlSeller('artist_joinin', 'check_seller_name_exist');?>',
                async:false,
                data:{seller_name: $('#seller_name').val()},
                success: function(data){
                    if(data == 'true') {
                        $.validator.messages.seller_name_exist = "卖家账号已存在";
                        result = false;
                    }
                }
            });
            return result;
        }, '');

        $('#form_store_info').validate({
            errorPlacement: function(error, element){
                element.nextAll('span').first().after(error);
            },
            rules : {
                seller_name: {
                    required: true,
                    maxlength: 50,
                    seller_name_exist: true
                },
                store_name: {
                    required: true,
                    maxlength: 50,
                    remote : '<?php echo urlSeller('artist_joinin', 'checkname');?>'
                },
                sg_id: {
                    required: true
                }
            },
            messages : {
                seller_name: {
                    required: '请填写卖家用户名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                store_name: {
                    required: '请填写店铺名称',
                    maxlength: jQuery.validator.format("最多{0}个字"),
                    remote : '店铺名称已存在'
                },
                sg_id: {
                    required: '请选择店铺等级'
                }
            }
        });

        $('#btn_select_category').on('click', function() {
            $('#gcategory').show();
            $('#btn_select_category').hide();
            $('#gcategory_class1').val(0).nextAll("select").remove();
        });

        $('#btn_add_category').on('click', function() {
            var tr_category = '<tr class="store-class-item">';
            var category_id = '';
            var category_name = '';
            var class_count = 0;
            var validation = true;
            var i = 1;
            $('#gcategory').find('select').each(function() {
                if(parseInt($(this).val(), 10) > 0) {
                    var name = $(this).find('option:selected').text();
                    tr_category += '<td>';
                    tr_category += name;
                    if ($('#gcategory > select').size() == i) {
                        //最后一级显示分佣比例
                        tr_category += ' (分佣比例：' + $(this).find('option:selected').attr('data-explain') + '%)';
                    }
                    tr_category += '</td>';
                    category_id += $(this).val() + ',';
                    category_name += name + ',';
                    class_count++;
                } else {
                    validation = false;
                }
                i++;
            });
            if(validation) {
                for(; class_count < 2; class_count++) {
                    tr_category += '<td></td>';
                }
                tr_category += '<td><a nctype="btn_drop_category" href="javascript:;">删除</a></td>';
                tr_category += '<input name="store_class_ids[]" type="hidden" value="' + category_id + '" />';
                tr_category += '<input name="store_class_names[]" type="hidden" value="' + category_name + '" />';
                tr_category += '</tr>';
                $('#table_category').append(tr_category);
                $('#gcategory').hide();
                $('#btn_select_category').show();
                select_store_class_count();
            } else {
                showError('请选择分类');
            }
        });

        $('#table_category').on('click', '[nctype="btn_drop_category"]', function() {
            $(this).parent('td').parent('tr').remove();
            select_store_class_count();
        });

        // 统计已经选择的经营类目
        function select_store_class_count() {
            var store_class_count = $('#table_category').find('.store-class-item').length;
            $('#store_class').val(store_class_count);
        }

        $('#btn_cancel_category').on('click', function() {
            $('#gcategory').hide();
            $('#btn_select_category').show();
        });

        $('#btn_apply_store_next').on('click', function() {
            $('#form_store_info').submit();
        });
    });
</script>
