<?php defined('InShopNC') or exit('Access Invalid!');?>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<!-- 公司信息 -->

<style>
    .webuploader-container {
        position: relative;
    }

    .webuploader-element-invisible {
        position: absolute !important;
        clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
        clip: rect(1px, 1px, 1px, 1px);
    }

    .webuploader-pick {
        position: relative;
        display: inline-block;
        cursor: pointer;
        background: #00b7ee;
        padding: 10px 15px;
        color: #fff;
        text-align: center;
        border-radius: 3px;
        overflow: hidden;
    }

    .webuploader-pick-hover {
        background: #00a2d4;
    }

    .webuploader-pick-disable {
        opacity: 0.6;
        pointer-events: none;
    }


</style>

<!--签署入驻协议-->
<div class="mingcheng">个人资料提交</div>
<!-- 个人资料提交 -->
<div class="lm_upload">
    <div class="lm_container_ful">
        <form id="form_company_info" class="lm_upload_form" action="index.php?act=artist_joinin&op=step2" method="post">
            <fieldset>
                <div class="zm_form-group">
                    <label class="zm_label" for="artname"><i class="lm_req">*</i>艺术家/经理人姓名：</label>
                    <div class="zm_inp">
                        <input id="artname" name="company_name" type="text" placeholder="填写本人真实姓名">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="arttel"><i class="lm_req">*</i>艺术家/经理人手机号：</label>
                    <div class="zm_inp">
                        <input id="arttel" name="company_phone" type="text" placeholder="填写13位手机号码">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for=""></label>
                    <label class="zm_inp" for="artOperat">
                        <input type="checkbox" id="artOperat" value="true" name="is_same">艺术家/经理人和运营联系方式相同
                    </label>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="operatename"><i class="lm_req">*</i>运营联系人：</label>
                    <div class="zm_inp">
                        <input id="operatename" name="contacts_name" type="text" placeholder="填写本人真实姓名">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="operatetel"><i class="lm_req">*</i>运营联系人手机号：</label>
                    <div class="zm_inp">
                        <input id="operatetel" name="contacts_phone" type="text" placeholder="填写13位手机号码">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="lm_area"><i class="lm_req">*</i>地址：</label>
                    <div class="zm_inp">
                        <input id="company_address" name="company_address" type="hidden" value=""/>
                        <input type="hidden" name="area_id_1" id="_area_1" value="">
                        <input type="hidden" value="" name="company_province_id" id="province_id">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="infoaddress"></label>
                    <div class="zm_inp">
                        <input id="infoaddress" name="company_address_detail" type="text" placeholder="详细地址">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="email"><i class="lm_req"></i>邮箱：</label>
                    <div class="zm_inp">
                        <input id="email" name="contacts_email" type="text" placeholder="填写常用邮箱">

                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label" for="card"><i class="lm_req">*</i>身份证号：</label>
                    <div class="zm_inp">
                        <input id="card" name="id_number" type="text" placeholder="填写本人身份证号">

                        <div class="upload_list">
                            <div class="upload_sign_con">
                                <div class="sign_webupload signUp_1">
                                    <div class="upload_btn">
                                        <div class="btn_up lm_upload_btn">
                                            <input name="id_number_positive_elc" type="file" class="w60" />
                                            <i><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                            <span>上传正面</span>
                                            <input name="id_number_positive_elc1" type="hidden"/>
                                        </div>
                                        <ul class="lm_upload_imgs">
                                            <li><img src="" alt=""></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="upload_sign_con">
                                <div class="sign_webupload signUp_2">
                                    <div class="upload_btn">
                                        <div class="btn_up lm_upload_btn">
                                            <input name="id_number_negative_elc" type="file" class="w60" />
                                            <i><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                            <span>上传反面</span>
                                            <input name="id_number_negative_elc1" type="hidden"/>
                                        </div>
                                        <ul class="lm_upload_imgs">
                                            <li><img src="" alt=""></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="zm_form-group">
                    <label class="zm_label">资质证明：</label>
                    <div class="zm_inp">
                        <div class="upload_list marno">
                                <div class="sign_webupload signUp_3">
                                    <div class="upload_btn">
                                        <div class="btn_up lm_upload_btn">
                                            <input name="other_qualifications_elc" type="file" class="w60" />
                                            <i><img src="<?php echo SELLER_TEMPLATES_URL;?>/images/joinin/upload_icon.png" alt=""></i>
                                            <span>上传反面</span>

                                        </div>
                                        <ul class="lm_upload_imgs">
                                            <li></li>
                                        </ul>
                                        <span class="tip">可上传多张</span>
                                    </div>
                                    <p class="tips">提示：仅支持JPG\GIF\PNG格式图片，大小请控制在1M之内。</p>
                                </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="anniu_shuang">
    <a class="old" href="<?php echo urlSeller('artist_joinin', 'step0');?>">上一步，入驻协议</a> <a class="new" id="btn_apply_company_next" href="javascript:;" id='lm_info_btn'>下一步，财务帐号</a>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        <?php foreach (array('id_number_positive_elc','id_number_negative_elc','member_certificate_elc') as $input_id) { ?>
        $('input[name="<?php echo $input_id;?>"]').fileupload({
            dataType: 'json',
            url: '<?php echo urlSeller('artist_joinin', 'ajax_upload_image');?>',
            formData: '',
            add: function (e,data) {
                data.submit();
            },
            done: function (e,data) {
                if (!data.result){
                    alert('上传失败，请尝试上传小图或更换图片格式');return;
                }
                if(data.result.state) {
                    $('input[name="<?php echo $input_id;?>"]').nextAll().remove('img');
                    $(this).parent().next().find('img').attr('src', data.result.pic_url);
                    //$('input[name="<?php echo $input_id;?>"]').after('<img height="60" src="'+data.result.pic_url+'">');
                    $('input[name="<?php echo $input_id;?>1"]').val(data.result.pic_name);
                } else {
                    alert(data.result.message);
                }
            },
            fail: function(){
                alert('上传失败，请尝试上传小图或更换图片格式');
            }
        });
        <?php } ?>



        $('input[name="other_qualifications_elc"]').fileupload({
            dataType: 'json',
            url: '<?php echo urlSeller('artist_joinin', 'ajax_upload_image');?>',
            formData: '',
            add: function (e,data) {
                data.submit();
            },
            done: function (e,data) {
                if (!data.result){
                    alert('上传失败，请尝试上传小图或更换图片格式');return;
                }
                if(data.result.state) {
//                    $('input[name="other_qualifications_elc"]').nextAll().remove('img');
//                    $(this).parent().next().find('img').attr('src', data.result.pic_url);
//                    $('input[name="other_qualifications_elc1"]').val(data.result.pic_name);
                    str = '<img src="'+data.result.pic_url+'" alt="">';
                    $(this).parents('.upload_btn').find('li').prepend(str);
                    input_str = '<input name="other_qualifications_elc1[]" value="'+data.result.pic_name+'" type="hidden"/>';
                    $(input_str).insertAfter($(this));
                } else {
                    alert(data.result.message);
                }
            },
            fail: function(){
                alert('上传失败，请尝试上传小图或更换图片格式');
            }
        })

        $('#btn_apply_agreement_next').on('click', function() {
            if($('#input_apply_agreement').prop('checked')) {
                $('#apply_agreement').hide();
                $('#apply_company_info').show();
            } else {
                alert('请阅读并同意协议');
            }
        });

        $.validator.addMethod('checkId', function(value,element){
            //15位数身份证正则表达式
            var arg1 = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;
            //18位数身份证正则表达式
            var arg2 = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[A-Z])$/;
            if (value.match(arg1) == null && value.match(arg2) == null) {
                return false;
            } else {
                return true;
            }
        }, '');

        $('#company_address').nc_region();

        $.validator.addMethod('new_required', function(value,element,param){
            if($('#form_company_info :checkbox').prop('checked')){
                rs =  true;
            }else{
                rs =  $.trim(value).length > 0;
            }
            return rs;

        }, '');

        $('#form_company_info').validate({
            errorPlacement: function (error, element) {
                if (element.is(':radio') || element.is(':checkbox') || element.is('select') || element.hasClass('lmWebUploadHidden')) { //如果是radio或checkbox
                   // var eid = element.attr('name'); //获取元素的name属性
                    error.appendTo(element.parent()); //将错误信息添加当前元素的父结点后面
                } else if (element.is(':hidden')) {
                    error.appendTo(element.parent());
                } else {
                    error.insertAfter(element);
                }
                //element.nextAll('span').first().after(error);    //原始
            },
            rules: {
                company_name: {
                    required: true,
                    maxlength: 50
                },
                company_address_detail: {
                    required: true,
                    maxlength: 50
                },
                company_address: {
                    required: true,
                    maxlength: 50
                },
                contacts_name: {
                    new_required: true,
                    maxlength: 20
                },
                contacts_phone: {
                    new_required: true,
                    mobile : true
                },
                company_phone: {
                    required: true,
                    mobile : true
                },
                id_number :{
                    required: true,
                    checkId : true
                },
                id_number_positive_elc1: {
                    required:  true
                },
                id_number_negative_elc1: {
                    required: true
                }
            },
            messages: {
                company_name: {
                    required: '请输入艺术家姓名',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_address_detail: {
                    required: '详细地址不能为空',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                company_phone: {
                    required: '艺术家手机号不能为空',
                    mobile: '请输入正确的联系手机'
                },
                company_address: {
                    required: '请选择区域地址',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                contacts_name: {
                    new_required: '运营联系人不能为空',
                    maxlength: jQuery.validator.format("最多{0}个字")
                },
                contacts_phone: {
                    new_required: '运营联系人手机号不能为空',
                    mobile : '请输入正确的联系手机'
                },
                contacts_email: {
                    required: '请输入常用邮箱地址',
                    email: '请填写正确的邮箱地址'
                },
                id_number :{
                    required: '身份证号不能为空',
                    checkId : '请输入正确的艺术家身份证号码'
                },
                id_number_positive_elc1: {
                    //required: '请选择上传艺术家身份证正面照片'
                    required: '请选择上传照片'
                },
                id_number_negative_elc1: {
                    //required: '请选择上传艺术家身份证反面照片'
                    required: '请选择上传照片'
                }
            }
        });

        $('#btn_apply_company_next').on('click', function() {
            if($('#form_company_info').valid()) {
                $('#company_province_id').val($("#company_address").fetch('area_id_1'));
                $('#form_company_info').submit();
            }
        });

        var $dom = $('#form_company_info');
        var operatename = $dom.find('#operatename').parents('.zm_form-group');
        var operatetel = $dom.find('#operatetel').parents('.zm_form-group');

        $('#artOperat').on('click', function(){
            if ($(this).prop('checked')) {
                operatename.hide();
                operatetel.hide();
            } else {
                operatename.show();
                operatetel.show();
            }
        });


    });
</script>
