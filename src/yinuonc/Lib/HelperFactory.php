<?php 
namespace Yinuo\Lib;

use Yinuo\Lib\Helpers\HttpClientHelper;
use Yinuo\Lib\Helpers\IpHelper;
use Yinuo\Lib\Helpers\ModelHelper;
use Yinuo\Lib\Helpers\RedisHelper;
use Yinuo\Lib\Helpers\VerifyHelper;
use Yinuo\Lib\TraitCode\InstanceCode;

/**
 * Class HelperFactory
 * @package Yinuo\Lib
 * @property VerifyHelper Verify
 * @property ModelHelper Model
 * @property HttpClientHelper HttpClient
 * @property RedisHelper Redis
 * @property IpHelper Ip
 */
class HelperFactory
{
    use InstanceCode;

    const BASE_NAMESPACE = "Yinuo\\Lib\\Helpers\\";

    private $_service = [];

    //不是单例的配置
    static $notSingle = [
    ];
    public function __get($name)
    {
        $className = self::BASE_NAMESPACE . $name .'Helper';
        if(in_array($className, self::$notSingle)){
            return new $className;
        }
        if(!isset($this->_service[$className])){
            if (!class_exists($className)){
                throw new \ErrorException('class: "' . $className . '" not exist');
            }
            $this->_service[$className] = new $className;
        }
        return $this->_service[$className];
    }
}