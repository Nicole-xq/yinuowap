<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2017/9/22 0022
 * Time: 下午 16:38
 */

namespace Yinuo\Lib\TraitCode;
/**
 * 单例支持
 * Class InstanceCode
 * @package Yinuo\Lib\TraitCode
 */
trait InstanceCode
{
    private static $_instance = NULL;

    /**
     * @return static
     */
    public static function instance(){
        if(!self::$_instance){
            self::$_instance = new static();
        }
        return self::$_instance;
    }
}