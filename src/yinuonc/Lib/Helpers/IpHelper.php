<?php
namespace Yinuo\Lib\Helpers;

use Yinuo\Lib\HelperFactory;

class IpHelper extends HelperBase{
    const API_URL_TAOBAO = 'http://ip.taobao.com/service/getIpInfo.php';
    const API_URL_SINA = 'http://int.dpool.sina.com.cn/iplookup/iplookup.php';

    /**
     * 获得ip地址的一些相关信息
     * http://ip.taobao.com/service/getIpInfo.php?ip=203.156.231.167
     * http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=203.156.231.167
     * @param $ip
     * @return array
     */
    public function getIpInfo($ip){
        $outIpInfo = [];
        $ipInfoTaobao = $this->getIpInfoByTaobao($ip);
        if($ipInfoTaobao){
            $outIpInfo['country'] = $ipInfoTaobao['data']['country'];
            $outIpInfo['region'] = $ipInfoTaobao['data']['region'];
            $outIpInfo['city'] = $ipInfoTaobao['data']['city'];
        }else{
            $ipInfoSina = $this->getIpInfoBySina($ip);
            if($ipInfoSina){
                $outIpInfo['country'] = $ipInfoSina['country'];
                $outIpInfo['region'] = $ipInfoSina['province'];
                $outIpInfo['city'] = $ipInfoSina['city'];
            }
        }
        $outIpInfo = $outIpInfo ? $this->_processPosition($outIpInfo) : $outIpInfo;
        if(!$outIpInfo){
            \Log::error('IpHelper_getIpInfo_fail', [$ip, $outIpInfo]);
        }
        return $outIpInfo;
    }

    /**
     * @param $ip
     * @return array|NULL
     * array (size=2)
     * 'code' => int 0
     * 'data' =>
         * array (size=13)
         * 'country' => string '中国' (length=6)
         * 'country_id' => string 'CN' (length=2)
         * 'area' => string '华东' (length=6)
         * 'area_id' => string '300000' (length=6)
         * 'region' => string '上海市' (length=9)
         * 'region_id' => string '310000' (length=6)
         * 'city' => string '上海市' (length=9)
         * 'city_id' => string '310100' (length=6)
         * 'county' => string '' (length=0)
         * 'county_id' => string '-1' (length=2)
         * 'isp' => string '电信' (length=6)
         * 'isp_id' => string '100017' (length=6)
         * 'ip' => string '203.156.231.167' (length=15)
     */
    protected function getIpInfoByTaobao($ip){
        $responseJson = HelperFactory::instance()->HttpClient->getJson(self::API_URL_TAOBAO, ['ip'=>$ip], 60);
        if(!$responseJson){
            return NULL;
        }
        if(!isset($responseJson['code']) || !isset($responseJson['data']) || $responseJson['code'] != 0){
            \Log::warning('IpHelper_getIpInfoByTaobao_result_abnormal', [$ip, $responseJson]);
            return NULL;
        }
        return $responseJson;
    }

    /**
     * @param $ip
     * @return array|NULL
     * array (size=10)
         * 'ret' => int 1
         * 'start' => int -1
         * 'end' => int -1
         * 'country' => string '中国' (length=6)
         * 'province' => string '上海' (length=6)
         * 'city' => string '上海' (length=6)
         * 'district' => string '' (length=0)
         * 'isp' => string '' (length=0)
         * 'type' => string '' (length=0)
         * 'desc' => string '' (length=0)
     */
    protected function getIpInfoBySina($ip){
        $responseJson = HelperFactory::instance()->HttpClient->getJson(self::API_URL_SINA, ['format'=>'json', 'ip'=>$ip], 60);
        if(!$responseJson){
            return NULL;
        }
        if(!isset($responseJson['ret']) || $responseJson['ret'] != 1){
            YNApp()->log->warning('IpHelper_getIpInfoBySina_result_abnormal', [$ip, $responseJson]);
            return NULL;
        }
        return $responseJson;
    }
    protected function _processPosition($outIpInfo){
        $regionFootRm = ['省', '市'];
        $cityFootRm = ['市'];
        if($outIpInfo){
            //删除 省 区域 的最后一个字
            if($outIpInfo['region']){
                foreach ($regionFootRm as $item) {
                    if(mb_strpos($outIpInfo['region'], $item) === (mb_strlen($outIpInfo['region'])-1)){
                        $outIpInfo['region'] = mb_substr($outIpInfo['region'], 0, -1);
                        break;
                    }
                }
            }
            //删除 市 的最后一个字
            if($outIpInfo['city']){
                foreach ($cityFootRm as $item) {
                    if(mb_strpos($outIpInfo['city'], $item) === (mb_strlen($outIpInfo['city'])-1)){
                        $outIpInfo['city'] = mb_substr($outIpInfo['city'], 0, -1);
                        break;
                    }
                }
            }
        }
        return $outIpInfo;
    }

}