<?php 
namespace Yinuo\Lib\Helpers;

class VerifyHelper extends HelperBase{
    /**
     * Validate that an attribute is an integer.
     *
     * @param  mixed   $value
     * @return bool
     */
    public function isInteger($value)
    {
        return filter_var($value, FILTER_VALIDATE_INT) !== false;
    }
    /**
     * Validate that an attribute is an positive integer.
     *
     * @param  mixed   $value
     * @return bool
     */
    public function isPositiveInteger($value)
    {
        return $this->isInteger($value) && $value > 0;
    }

    /**
     * Validate that an attribute is a valid IP.
     *
     * @param  mixed   $value
     * @return bool
     */
    public function isIp($value)
    {
        return filter_var($value, FILTER_VALIDATE_IP) !== false;
    }
    /**
     * Validate that an attribute is a valid e-mail address.
     *
     * @param  mixed   $value
     * @return bool
     */
    public function isEmail($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }
    /**
     * Determine if the given path is a valid URL.
     *
     * @param  string  $path
     * @return bool
     */
    public function isValidUrl($path)
    {
        if (! preg_match('~^(#|//|https?://|mailto:|tel:)~', $path)) {
            return filter_var($path, FILTER_VALIDATE_URL) !== false;
        }

        return true;
    }
}