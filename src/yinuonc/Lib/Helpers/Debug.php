<?php
namespace Yinuo\Lib\Helpers;
/**
 * @Desc 调试文件
 * @author: huangguoliang
 * $Time:2014-11-11
 **/
error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR);
ini_set('display_errors', 'On');
$_DEBUG = array();
class Debug{
    public static function init(){}
    /**
     * 调试函数
     * @author 黄国梁
     * @param $exception
     * @param bool $isExit
     * @param bool $isPre
     */
    public static function showExceptionBacktrace(\Exception $exception, $isExit = true, $isPre = true){
        ob_start();
        $debugB = $exception->getTrace();
        echo '<br /><b>Error:'.$exception->getMessage()."(" . $exception->getCode() . "); File:".$exception->getFile()."(".$exception->getLine().");Type:". get_class($exception) ."</b>";
        echo "<div style='background-color: #bbb;'>";
        echo "<p><b>追踪信息:</b></p>";
        foreach ($debugB as $k=>$v) {
            echo empty($v['file']) ? "" : "<p><b>{$k}:</b>调用文件:<b><a>{$v['file']}</a></b>,行数:{$v['line']}</p>";
        }
        echo "</div>";
            $outStr =  ob_get_contents();
            ob_end_clean();
    if($isPre){
        echo "<pre style='border-bottom: 2px dotted #000;background-color: #DDD;'>".$outStr."</pre>";
            }else{
        echo $outStr;
    }
    if($isExit)
        exit;
    }

    /**
     * 获得栈的明细信息
     * @param $debugB
     * @return string
     */
    public static function getDebugBacktraceMsg($debugB){
        if(empty($debugB) || !is_array($debugB)){
            return "";
        }
        $msg = "追踪信息:\r\n";
        foreach ($debugB as $k=>$v) {
            $msg .= empty($v['file']) ? "" : "{$k}:调用文件:{$v['file']},行数:{$v['line']}\r\n";
        }
        $msg .= "调用文件:{$debugB[0]['file']},行数:{$debugB[0]['line']}\r\n";
        return $msg;
    }
    /**
     * 调试函数
     * @author 黄国梁
     * @param $exception
     * @param bool $isExit
     */
    public static function cliShowExceptionBacktrace(\Exception $exception, $isExit = true){
        $debugB = $exception->getTrace();
        echo 'Error:'.$exception->getMessage()."(" . $exception->getCode() . "); File:".$exception->getFile()."(".$exception->getLine().");Type:". get_class($exception) ."\r\n ";//PHP_EOL
        echo "追踪信息:\r\n";
        foreach ($debugB as $k=>$v) {
            echo empty($v['file']) ? "" : "{$k}:调用文件:{$v['file']},行数:{$v['line']}\r\n ";
        }
        if($isExit)
            exit;
    }
    /**
     * 调试函数
     * @author 黄国梁
     * @param mixed  $valS
     * @param bool $isExit
     * @param bool $isPre
     */
     public static function dump($valS, $isExit = true, $isPre = true){
        ob_start();
        $debugB = debug_backtrace();
        var_dump($valS);
        echo "<div style='background-color: #bbb;'>";
        echo "<p><b>追踪信息:</b></p>";
        if($isExit){
            foreach ($debugB as $k=>$v) {
                echo empty($v['file']) ? "" : "<p><b>{$k}:</b>调用文件:<b><a>{$v['file']}</a></b>,行数:{$v['line']}</p>";
            }
        }else{
            echo "<p><b>0:</b>调用文件:{$debugB[0]['file']},行数:{$debugB[0]['line']}</p>";
        }
        echo "</div>";
        $outStr =  ob_get_contents();
        ob_end_clean();
        if($isPre){
            echo "<pre style='border-bottom: 2px dotted #000;background-color: #DDD;'>".$outStr."</pre>";
        }else{
            echo $outStr;
        }
        if($isExit)
            exit;
    }
    /**
     * 调试函数
     * @author 黄国梁
     * @param mixed  $valS
     * @param bool $isExit
     */
    public static function cliDump($valS, $isExit = true){
        ob_start();
        $debugB = debug_backtrace();
        var_dump($valS);
        echo "追踪信息:\r\n";
        if($isExit){
            foreach ($debugB as $k=>$v) {
                echo empty($v['file']) ? "" : "{$k}:调用文件:{$v['file']},行数:{$v['line']}\r\n";
            }
        }else{
            echo "调用文件:{$debugB[0]['file']},行数:{$debugB[0]['line']}\r\n";
        }
        $outStr =  ob_get_contents();
        ob_end_clean();
        echo $outStr;
       // console()->info($outStr);
        if($isExit)
            exit;
    }
    /**
     * 写到文件调试函数
     * @author 黄国梁
     */
     public static function fileDump($val){
        $ROOTDIR = defined('CACHE_PATH') ? CACHE_PATH : (dirname(__FILE__). DIRECTORY_SEPARATOR);
        file_put_contents($ROOTDIR.'debug.txt',print_r($val, true), FILE_APPEND);
    }
}
?>