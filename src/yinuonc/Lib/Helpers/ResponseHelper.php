<?php 
namespace Yinuo\Lib\Helpers;

/*use App\Services\Constant\ApiResponseCode;
use App\Services\Constant\ResponseCode;
use Illuminate\Http\JsonResponse;*/

class ResponseHelper extends HelperBase{
    /**
     * 把响应附上跨域支持
     * @param JsonResponse $response
     * @return JsonResponse
     */
    public static function appendCrossResponse(JsonResponse &$response){
        $referer = \Request::header('referer');
        if($referer){
            $origins = config('access_control_allows.origins');
            $is_match = false;
            $match_origin = '';
            foreach ($origins as $origin) {
                if(strpos($referer, $origin) === 0){
                    $is_match = true;
                    $match_origin = $origin;
                    break;
                }
            }
            if($is_match){
                $response->header('Access-Control-Allow-Origin', $match_origin);
                $response->header('Access-Control-Allow-Headers', config('access_control_allows.headers'));
//                    $response->header('Access-Control-Allow-Methods', config('access_control_allows.methods'));
                $response->header('Access-Control-Allow-Credentials', config('access_control_allows.credentials'));
            }
        }
        return $response;
    }
    public static function json($code, $data = NULL, $status = ''){
        $responseData = ['code'=>$code, 'status'=>$status ? $status : ResponseCode::getMsg($code)];
        if($data !== NULL){
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
    public static function jsonApi($code, $data = NULL, $status = ''){
        $responseData = ['code'=>$code, 'msg'=>$status ? $status : ApiResponseCode::getMsg($code)];
        if($data !== NULL){
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
}