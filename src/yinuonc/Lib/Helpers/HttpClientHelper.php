<?php 
namespace Yinuo\Lib\Helpers;

//use Galaxy\Framework\Contracts\Service\Log;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Yinuo\Lib\HelperFactory;
use Yinuo\Lib\Helpers\HttpClent\Exceptions\ResponseException;

class HttpClientHelper extends HelperBase{
    //get的默认请求超时时间
    const GET_DEFAULT_TIME_OUT = 120;
    const POST_DEFAULT_TIME_OUT = 120;
    /**
     * @param $url
     * @param array $param
     * @param null $timeout
     * @return array | NULL
     */
    public function getJson($url, $param = [], $timeout = NULL){
        try{
            $response = $this->_get($url, [
                'query' => $param,
                'timeout' => $timeout,
                //这里补充 json 请求头
                'headers' => [
                    'Accept'     => 'application/json',
                ]
            ]);
        }catch (\Exception $e){
            //Log::error('HttpClient_getJson_Exception', [func_get_args(), LogHelper::getEInfo($e)]);
            return NULL;
        }
        if($response->getStatusCode() !== 200){
            //Log::error('HttpClient_getJson_statusCode_not_200', [func_get_args(), $response->getStatusCode()]);
            return NULL;
        }
        $jsonCon = $response->getBody();
        $json = $jsonCon ? json_decode($jsonCon, true) : NULL;
        if(!$json){
            //Log::warning('HttpClient_getJson_JsonCon_Error', [func_get_args(), $jsonCon]);
            return NULL;
        }
        return $json;
    }

    /**
     * post json body
     * @param $url
     * @param $body
     * @param int $timeout
     * @return string
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postJsonBody($url, $body, $timeout = self::POST_DEFAULT_TIME_OUT)
    {
        $response = $this->postBody($url, json_encode($body), [
            'Accept'  => 'application/json',
            'Content-Type' => 'application/json'
        ], $timeout);
        if($response->getStatusCode()!=200){
            throw new ResponseException('StatusCode not 200 -' . $response->getStatusCode());
        }
        return $response->getBody()->getContents();
    }


    /**
     * post body
     * @param $url
     * @param $body
     * @param array $headers
     * @param int $timeout
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postBody($url, $body, $headers=[], $timeout = self::POST_DEFAULT_TIME_OUT){
        $reqOptions = [
            'body' => $body,
            'headers' => $headers,
        ];
        if ($timeout !== NULL) {
            $reqOptions['timeout'] = $timeout;
        }
        return $this->_getHttpClient()->request('POST', $url, $reqOptions);
    }

    /**
     * 异步post json
     * @param $url
     * @param $body array|mixed
     * @param callable|null $onFulfilled
     * @param callable|null $onRejected
     * @param int $timeout
     * @return bool
     */
    public function asyncPostJsonBody(
        $url,
        $body,
        callable $onFulfilled = null,
        callable $onRejected = null,
        $timeout = self::POST_DEFAULT_TIME_OUT){
        return $this->asyncPostBody(
            $url,
            json_encode($body),
            [
                'Accept'  => 'application/json',
                'Content-Type' => 'application/json'
            ],
            $onFulfilled,
            $onRejected,
            $timeout
        );
    }

    /**
     * 异步post body
     * @param $url
     * @param $body
     * @param array $headers
     * @param callable|null $onFulfilled
     *  function (ResponseInterface $res) {
     * echo $res->getStatusCode() . "\n";
     * }
     * @param callable|null $onRejected
     *  function (RequestException $e) {
     * echo $e->getMessage() . "\n";
     * echo $e->getRequest()->getMethod();
     * }
     * @param int $timeout
     * @return bool
     */
    public function asyncPostBody(
        $url,
        $body,
        $headers=[],
        callable $onFulfilled = null,
        callable $onRejected = null,
        $timeout = self::POST_DEFAULT_TIME_OUT
    ){
        $reqOptions = [
            'body' => $body,
            'headers' => $headers,
        ];
        if ($timeout !== NULL) {
            $reqOptions['timeout'] = $timeout;
        }
        $promise = $this->_getHttpClient()->requestAsync('POST', $url, $reqOptions);
        $promise->then(
            $onFulfilled,
            $onRejected
        );
        return true;
    }
    /**
     * @param array ...$param
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function _get(...$param){
        $param[1] = $param[1] ? $param[1] : [];
        $param[1]['timeout'] = $this->_getGetTimeOut(array_get($param, 'timeout'));
        return $this->_getHttpClient()
            ->get(...$param);
    }
    protected function _getHttpClient(...$param){
        return new Client(...$param);
    }
    protected function _getGetTimeOut($time_out = NULL){
        if(is_int($time_out) && $time_out >= 0){
            return $time_out;
        }
        return self::GET_DEFAULT_TIME_OUT;
    }
}