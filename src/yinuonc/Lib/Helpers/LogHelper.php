<?php 
namespace Yinuo\Lib\Helpers;

class LogHelper extends HelperBase{
    /**
     * 获得简单的 方便log函数使用的 数组异常信息
     * @param \Exception $e
     * @param mixed $arg
     * @return array
     */
    public static function getEInfo(\Exception $e, $arg = null){
        $baseError = [$e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()];
        if($arg !== null && is_array($arg)){
            $baseError = array_merge($baseError, $arg);
        }
        return $baseError;
    }
}