<?php 
namespace Yinuo\Lib\Helpers;

class ModelHelper extends HelperBase{
    /**
     * 绑定where到$query
//     * @param \Illuminate\Database\Query\Builder|array $query
     * @param array $where [
     * ['id', '=', 1],
     *
     * 'id'=>1,
     *
     * function ($query){
     * $query->where('id',1);
     * }
     * ,
     *
     * 'orWhere'=>function ($query){
     * $query->where('id',1);
     * },
     *
     * 'orWhere'=>['id', '=', 1]
     * ]
     * @return bool
     */
    public function whereBindQuery(&$query, $where){
        if(!is_array($query)){
            $queryAll = [$query];
        }else{
            $queryAll = &$query;
        }
        foreach ($where as $key => $item) {
            if(is_array($item)){
                array_walk($queryAll, function (&$query) use ($item, $key){
                    if($key === 'orWhere'){
                        $query->orWhere([$item]);
                    }else{
                        $query->where([$item]);
                    }
                });

            }elseif ($item instanceof \Closure){
                array_walk($queryAll, function (&$query) use ($item, $key){
                    if($key === 'orWhere'){
                        $query->orWhere($item);
                    }else{
                        $query->where($item);
                    }
                });
            }else{
                array_walk($queryAll, function (&$query) use ($item, $key){
                    $query->where($key, $item);
                });
            }
        }
        return true;
    }

    /**
     * 处理 select 参数 $start = 0, $size = 10, $where = false, $columns = null
     * @param null $default 覆盖 sysDefault值
     * @param null $custom 自定义
     * @return array
     */
    public function selectArg(&$custom, $default = null){
        //$start = 0, $size = 10, $where = false, $columns = null
        $sysDefault = [
            'start' => 0,
            'size' => 10,
            'where' => null,
            'columns' => ['*'],
        ];
        $default = is_array($default) ? $default : [];
        $custom = is_array($custom) ? $custom : [];
        $custom = array_merge($sysDefault, $default, $custom);
    }
}