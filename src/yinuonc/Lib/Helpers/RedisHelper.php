<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/4/16 0016
 * Time: 下午 16:43
 */

namespace Yinuo\Lib\Helpers;


use Predis\Client;
use Predis\ClientInterface;
use Yinuo\Lib\Redis\RedisLock;

/**
 * 请用 HelperFactory::instance()->Redis 使用
 * Class RedisHelper
 * @package Yinuo\Lib\Helpers
 */
class RedisHelper
{
    /**
     * @var ClientInterface
     */
    private $redis;

    /**
     * 单例
     * @var RedisLock
     */
    private $redisLock;

    public function __construct()
    {
        $single_server = $this->getConfig();
        $this->redis = new Client($single_server);
        if($single_server["password"]){
            $this->redis->auth($single_server["password"]);
        }
    }

    /**
     * 单例
     * @return RedisLock
     */
    public function getRedisLock()
    {
        if(!$this->redisLock){
            $this->redisLock = new RedisLock($this->redis);
        }
        return $this->redisLock;
    }

    /**
     * 单例
     * @return Client|ClientInterface
     */
    public function getRedis()
    {
        return $this->redis;
    }

    private function getConfig(){
        return [
            'host' => getMainConfig("REDIS_HOST"),
            'password' => getMainConfig("REDIS_PASSWORD"),
            'port' => getMainConfig("REDIS_PORT"),
            'database' => getMainConfig("REDIS_DATABASE"),
        ];
    }
}