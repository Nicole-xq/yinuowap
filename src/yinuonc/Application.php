<?php
namespace Yinuo;
use Monolog\Formatter\LineFormatter;
use Yinuo\Exceptions\YnHandler;
use Yinuo\Providers\Config\MainConfig;
use Yinuo\Providers\LogProvider\LogProvider;


/**
 *
 * Class Application
 * @package App
 * @property LogProvider log
 * @property MainConfig mainConfig
 */
class Application
{
    //服务数组
    private $providers = [];
    private static $instance;
    /**
     * @var YnHandler
     */
    private $ynHandler;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->ynHandler = new YnHandler();
    }


    /**
     * //返回全局实例
     * @return Application
     */
    public static function one()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public function __get($name)
    {
        if(!array_key_exists($name, $this->providers)){
            throw new \Exception("不存在服务: ".$name);
        }
        return $this->providers[$name];
    }

    /**
     * 设置一个单例
     * @param $abstract
     * @param $provider
     * @return bool
     * @throws \Exception
     */
    public function singleton($abstract, $provider){
        if(array_key_exists($abstract, $provider)){
            throw new \Exception("重复注册单例, 已存在单例: ".$abstract);
        }
        $this->providers[$abstract] = $provider;
        return true;
    }

    public function init(){
        //配置注入 转移
        $config = \Shopnc\Core::getConfigs();
        YNApp()->singleton("mainConfig", \Yinuo\Providers\MainConfigProvider::getInstance($config));

        // the default date format is "Y-m-d H:i:s"
        $dateFormat = "Y-m-d H:i:s";
        // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
        $output = "[%datetime% %extra.url% %extra.ip%] %extra.uid% > %level_name% > %message% %context% {%extra.file%:%extra.line%}\n";
        // finally, create a formatter
        $formatter = new LineFormatter($output, $dateFormat);
        //log配置
        $logConfig = new \Yinuo\Providers\LogProvider\LogConfig(\Yinuo\Providers\LogProvider\LogProvider::INFO,
            BASE_ROOT_PATH.getMainConfig("logDir"), $formatter);

        //debug处理
        define("APP_DEBUG", YNApp()->mainConfig->getKey("debug", false));
        if(APP_DEBUG){
            $logConfig->setLevel(\Yinuo\Providers\LogProvider\LogProvider::DEBUG);
        }
        //注入log
        YNApp()->singleton("log", \Yinuo\Providers\LogProvider\LogProvider::getInstance($logConfig));
        $this->setErrorReporting(APP_DEBUG);
        //非laravel下设置异常处理器
        if(!defined("IS_LARAVEL") || !IS_LARAVEL){
            set_error_handler([$this, 'handleError']);
            set_exception_handler([$this, 'handleException']);
            register_shutdown_function([$this, 'handleShutdown']);
        }

    }
    public function setErrorReporting($appDebug){
        if($appDebug){
            error_reporting(E_ERROR | E_PARSE);// 这边先关闭E_WARNING
            ini_set("display_errors", "1");
        }else{
            error_reporting(E_ERROR | E_PARSE);//| E_WARNING
            ini_set("display_errors", "0");
        }
    }


    /**
     * Convert PHP errors to ErrorException instances.
     *
     * @param  int  $level
     * @param  string  $message
     * @param  string  $file
     * @param  int  $line
     * @param  array  $context
     * @return void
     *
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (error_reporting() & $level) {
            $this->handleException(new \ErrorException($message, 0, $level, $file, $line), $level != E_WARNING);
        }
    }

    /**
     * Handle an uncaught exception from the application.
     *
     * Note: Most exceptions can be handled via the try / catch block in
     * the HTTP and Console kernels. But, fatal error exceptions must
     * be handled differently since they are not normal exceptions.
     *
     * @param  \Throwable $e
     * @param bool $isRender
     * @return void
     */
    public function handleException($e, $isRender = true)
    {
        if (! $e instanceof \Exception) {
            if ($e instanceof \ParseError) {
                $message = 'Parse error: '.$e->getMessage();
                $severity = E_PARSE;
            } elseif ($e instanceof \TypeError) {
                $message = 'Type error: '.$e->getMessage();
                $severity = E_RECOVERABLE_ERROR;
            } else {
                $message = $e->getMessage();
                $severity = E_ERROR;
            }
            $e = new \ErrorException($message, $e->getCode(), $severity, $e->getFile(), $e->getLine());
        }

        $this->ynHandler->report($e);
        if($isRender){
            $this->ynHandler->render($e);
        }
    }
    /**
     * Handle the PHP shutdown event.
     *
     * @return void
     */
    public function handleShutdown()
    {
        if (! is_null($error = error_get_last()) && $this->isFatal($error['type'])) {
            $this->handleException(new \ErrorException("Shutdown error_get_last:".$error['message'], $error['type'], 0, $error['file'], $error['line']));
        }
    }
    /**
     * Determine if the error type is fatal.
     *
     * @param  int  $type
     * @return bool
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_COMPILE_ERROR, E_CORE_ERROR, E_ERROR, E_PARSE]);
    }
}
