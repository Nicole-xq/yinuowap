# 后台cms

* 生成环境 :`composer install or update --no-dev` "跳过 require-dev 字段中列出的包"
* 开发环境 :`composer install or update`
* `mv .env.example .env`
* 配置 .env
* 密钥生成 `php artisan key:generate`
* 数据库sql 迁移\(填充数据\) `php artisan db:seed`
* 数据库迁移 `php artisan migrate`
* Remove the compiled class file `php artisan clear-compiled`



