<?php

namespace Yinuo\Exceptions;

use App\Exceptions\ResponseException;
use Exception;
use Yinuo\Lib\Helpers\LogHelper;

/**
 * 只有在非laravel 下才会进入
 * Class YnHandler
 * @package Yinuo\Exceptions
 */
class YnHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \App\Exceptions\ResponseException::class,
    ];
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $shouldReport = true;
        foreach ($this->dontReport as $v){
            if($exception instanceof $v){
                $shouldReport = false;
                break;
            }
        }
        if ($shouldReport){
            YLog()->error("exception_handler", ['env'=>getMainConfig('APP_ENV'), LogHelper::getEInfo($exception)]);
            if(!isNcProduction()){
                YLog()->error("exception_handler_showExceptionTrace", [showExceptionTrace($exception, false, true, false)]);
            }
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render(Exception $exception)
    {
        $data = NULL;
        if(!isNcProduction()){
            $message = $exception instanceof ResponseException ? $exception->getMessage() : showExceptionTrace($exception, false, true, false);
        }else{
            //这里由于历史原因 异常都是message返回
            $message = $exception->getMessage();
/*            if($exception instanceof ResponseException ){
                $message = $exception->getMessage();
            }else{
                $message = "发生了未知错误,您可以联系客服处理!";
            }*/
        }
        //错误返回500
        if(function_exists("output_error")){
            return output_error($message);
        }else{
            die($message);
        }
    }
}
