<?php

namespace Yinuo\Entity\Constant;
/**
 * redis key常量集合
 * Class RedisConstant
 */
class RedisKeyConstant{
    const LOCK_ROBOT_RUNTIME = "lock_robot_runtime:"; //机器人运行时: auction_id
    const LOCK_ROBOT_RUNTIME_EXPIRES = 21600; //机器人运行时锁 超时最大时间 + 最大运行时间 6小时
    const LOCK_AUCTION_MEMBER_OFFER = "auction_member_offerOp:"; //出价锁
    const LOCK_PAY_PAY_SN = "lock_pay_pay_sn:"; //余额支付锁


    const TOKEN_INFO = "token_info:"; //token信息缓存
    const TOKEN_INFO_EXPIRES = 7200; //token信息缓存过期时间 秒(2小时)
}