<?php

if (!function_exists("YNApp")) {
    /**
     * @return \Yinuo\Application|\Yinuo\Providers\ProvidersInterface
     */
    function YNApp($provider = null) {
        if (!is_null($provider)){
            return \Yinuo\Application::one()->$provider;
        }
        return \Yinuo\Application::one();
    }
}

if (!function_exists("getMainConfig")) {
    function getMainConfig($key, $default = \Yinuo\Providers\Config\MainConfig::DEFAULT_NULL){
        return YNApp()->mainConfig->getKey($key, $default);
    }
}

if (!function_exists("yLog")) {
    /**
     * 日志工具 </br>
     * 使用方法:
     *  1. log($a); log($a, $b) --- 快速 debug
     *  2. log()->error("xxx", $array); ........
     * @param array $context
     * @return \Yinuo\Providers\LogProvider\LogProvider
     */
    function yLog(...$context){
        if(empty($context)){
            return YNApp("log");
        }
        //直接使用debug
        $debugB = debug_backtrace();
        $msg = "yLogDebug:". !empty($debugB[0]) ? ($debugB[0]['file']."(".$debugB[0]['line'].")"):"";
        YNApp()->log->debug($msg, $context);
    }
}
/**
 * 是否为生产环境
 * @return bool
 */
function isNcProduction(){
    return getMainConfig('APP_ENV', 'local') == 'production';
}