<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/2/26 0026
 * Time: 下午 20:33
 */

namespace Yinuo\Providers;


interface ProvidersInterface
{
    static function getInstance($config);
}