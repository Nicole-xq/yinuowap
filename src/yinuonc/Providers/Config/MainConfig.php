<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/2/27 0027
 * Time: 上午 10:10
 */

namespace Yinuo\Providers\Config;


/**
 * Class AppConfig
 * 系统配置信息
 * @package Yinuo\Providers\Config
 */
class MainConfig implements ConfigInterface
{
    const DEFAULT_NULL = NULL;
    private $configs = [];

    function __construct($configs)
    {
        $this->configs = $configs;
    }

    /**
     * 取得系统配置信息
     * @param $key
     * @param $default mixed 不传default 返回null
     * @return mixed
     */
    public function getKey($key, $default = MainConfig::DEFAULT_NULL)
    {
        if (strpos($key,'.')){
            $key = explode('.',$key);
            if(($default!==MainConfig::DEFAULT_NULL && !isset($this->configs[$key[0]]))){
                return $default;
            }
            $value = $this->configs[$key[0]];
            if($default!==MainConfig::DEFAULT_NULL && (!isset($value[$key[1]]))){
                return $default;
            }
            if (isset($key[2])){
                return ($default!==MainConfig::DEFAULT_NULL && !isset($value[$key[1]][$key[2]])) ? $default : $value[$key[1]][$key[2]];
            }else{
                return  $value[$key[1]];
            }
        }else{
            return ($default!==MainConfig::DEFAULT_NULL && !isset($this->configs[$key])) ? $default : $this->configs[$key];
        }
    }
    public function setKey($key, $val){
        $this->configs[$key] = $val;
    }

    public function getConfigs(){
        return $this->configs;
    }
}