<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/2/26 0026
 * Time: 下午 20:39
 */

namespace Yinuo\Providers\Config;


interface ConfigInterface
{
    /**
     * @param $key
     * @param $default mixed 不传default 并且key不存在就返回异常
     * @return mixed
     */
    function getKey($key, $default);
}