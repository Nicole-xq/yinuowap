<?php

namespace Yinuo\Providers\LogProvider;


use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\UidProcessor;
use Monolog\Processor\WebProcessor;
use Yinuo\Providers\ProvidersInterface;

class LogProvider extends Logger implements ProvidersInterface
{
    /**
     * @var LogConfig
     */
    private $logConfig;

    /**
     * todo 这边必须和app.php  configureMonologUsing 同步,否者会不一致
     * @param $config LogConfig
     * @return LogProvider
     * @throws \Exception
     */
    public static function getInstance($config)
    {
        // create a log channel
        $log = new LogProvider('my_logger');
        $log->setLogConfig($config);
        //将记录记录到任何PHP流中，将其用于日志文件。
        $streamHandler = new StreamHandler($config->getLogPath(), $config->getLevel());
        //new RotatingFileHandler(); 每天一个文件，会自动删除比$maxFiles老的文件，这只是一个
        $streamHandler->setFormatter($config->getFormatter());
        $log->pushHandler($streamHandler);

        //日志到钉钉
        $dingdingHandler = new \Yinuo\Providers\LogProvider\DingDingHandler(\Monolog\Logger::ERROR,
            isNcProduction() ? $config->getDingDingProToken() : $config->getDingDingTestToken(),
            true,
            $config->getDingDingStartTime(),
            $config->getDingDingEndTime()
        );
        $dingdingHandler->setFormatter($config->getFormatter());
        $log->pushHandler($dingdingHandler);
        //为日志记录添加唯一标识符。
        $uidProcessor = new UidProcessor();
        //添加发起日志调用的行/文件/类/方法。
        $introspectionProcessor = new IntrospectionProcessor(Logger::ERROR);
        //将当前请求URI，请求方法和客户端IP添加到日志记录中。
        $webProcessor = new \Monolog\Processor\WebProcessor(null, ["url", "ip"]);
        $log->pushProcessor($uidProcessor);
        $log->pushProcessor($webProcessor);


        if(!isNcProduction()){
/*            $chromePhpHandler = new ChromePHPHandler($config->getLevel());
            $log->pushHandler($chromePhpHandler);*/
            $phpConsole = new MyPHPConsoleHandler([], null, $config->getLevel());
            $introspectionProcessor = new IntrospectionProcessor($config->getLevel());
            $log->pushHandler($phpConsole);
            //$log->pushHandler(new BrowserConsoleHandler([], null, $config->getLevel()));
        }
        $log->pushProcessor($introspectionProcessor);
        return $log;
    }

    /**
     * @return LogConfig
     */
    public function getLogConfig()
    {
        return $this->logConfig;
    }

    public function setLogConfig($logConfig){
        $this->logConfig = $logConfig;
    }

}
