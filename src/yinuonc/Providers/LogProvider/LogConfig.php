<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/2/26 0026
 * Time: 下午 20:40
 */

namespace Yinuo\Providers\LogProvider;


use Monolog\Formatter\LineFormatter;
use Yinuo\Providers\Config\ConfigInterface;
use Yinuo\Providers\LogProvider\LogProvider;

class LogConfig implements ConfigInterface
{
    private $configs = [
        "level" => LogProvider::INFO,
        "logPath" => "/data/log/yinuo_shopnc_system.log"
    ];
    private $allowKeys = ["level", "path"];

    /**
     * @var LineFormatter
     */
    private $formatter;

    private $dingDingProToken = "da8c1f40faeb5ac500d935d0e1d78fde365842e1511b36587de9258ca8cf91a8";
    private $dingDingTestToken = "f484c39671cace0b7fa34830c1a8c8d80fe2d2279bdae3400d56d031b9c82ed1";
    //3.30 3:30:30
    private $dingDingStartTime = null;
    //4.7 23:59:59
    private $dingDingEndTime = null;

    /**
     * LogConfig constructor.
     * @param $level
     * @param $logPath
     * @param LineFormatter $formatter
     */
    public function __construct($level, $logPath, $formatter = null)
    {
        $this->setLevel($level);
        $this->setLogPath($logPath);
        if(!$formatter){
            $dateFormat = "Y-m-d H:i:s";
            // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
            $output = "[%datetime% %extra.uid%] > %level_name% > %message% %context% %extra%\n";
            // finally, create a formatter
            $this->formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat);
        }else{
            $this->formatter = $formatter;
        }
    }


    /**
     * @param $key
     * @param $default mixed 不传default 并且key不存在就返回异常
     * @return mixed
     */
    function getKey($key, $default)
    {
        if(isset($this->configs[$key])){
            return $this->configs[$key];
        }else{
            if(func_num_args() == 1) {
                throw new \InvalidArgumentException("配置项'" . $key . "'不存在;");
            }else{
                return $default;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->configs['level'];
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->configs['level'] = $level;
    }

    /**
     * @return mixed
     */
    public function getLogPath()
    {
        return $this->configs['logPath'];
    }

    /**
     * @param mixed $logPath
     */
    public function setLogPath($logPath)
    {
        $this->configs['logPath'] = $logPath;
    }

    /**
     * @return LineFormatter
     */
    public function getFormatter()
    {
        return $this->formatter;
    }

    /**
     * @param LineFormatter $formatter
     */
    public function setFormatter($formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @return string
     */
    public function getDingDingProToken()
    {
        return $this->dingDingProToken;
    }

    /**
     * @return string
     */
    public function getDingDingTestToken()
    {
        return $this->dingDingTestToken;
    }

    /**
     * @return null
     */
    public function getDingDingStartTime()
    {
        return $this->dingDingStartTime;
    }

    /**
     * @return null
     */
    public function getDingDingEndTime()
    {
        return $this->dingDingEndTime;
    }



}