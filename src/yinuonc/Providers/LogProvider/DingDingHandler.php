<?php

/*
 * This file is part of the Monolog package.
 *
 * (c) Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Yinuo\Providers\LogProvider;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Yinuo\Lib\HelperFactory;
use Yinuo\Lib\Helpers\LogHelper;

class DingDingHandler extends AbstractProcessingHandler
{
    private $baseUrl = "https://oapi.dingtalk.com/robot/send";
    private $accessToken = null;
    private $startTime = null;
    private $endTime = null;

    /**
     * @param  int $level
     * @param $accessToken
     * @param  bool $bubble
     * @param null $startTime
     * @param null $endTime
     */
    public function __construct($level = Logger::ERROR, $accessToken, $bubble = true, $startTime = null, $endTime = null)
    {
        $this->accessToken = $accessToken;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        if($this->startTime){
            if(time() < $this->startTime){
                return;
            }
        }
        if($this->endTime){
            if (time() > $this->endTime){
                return;
            }
        }
        $this->handleToDingDingContent($record['formatted']);
    }

    private function handleToDingDingContent($content)
    {
        $url = $this->baseUrl."?access_token=".$this->accessToken;
        $body = [
            'msgtype' => "text",
            'text'    => [
                'content' => $content
            ]
        ];
        try {
            HelperFactory::instance()->HttpClient->postJsonBody($url, $body);
        } catch (\Exception $e) {
            //错误了但是不能打日志会死循环
        }
    }
}
