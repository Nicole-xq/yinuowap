<?php

namespace Yinuo\Providers;


use Yinuo\Providers\Config\MainConfig;

class MainConfigProvider implements ProvidersInterface
{
    public static function getInstance($configs)
    {
        // create a log channel
        $mainConfig = new MainConfig($configs);
        return $mainConfig;
    }
}
