$('.navbar').mouseover(function () {
    $(this).stop().animate({opacity:1});
}).mouseout(function () {
    $(this).stop().animate({opacity:0});
});



// 获取验证码 ---------------------
$(document).on('focusout','#email', function () {
    let my = $(this), userName = $.trim(my.val()), captchaEl = $('.captcha')
    if(userName && !captchaEl.is(':visible')){
        $.get('/signon/loginIsCaptcha', {userName: userName}, function (ret) {
            if(ret==1){
                captchaEl.show()
                captchaEl.find('#captcha').attr('required', 'required')
            }
        })
    }
})

// 点击切换验证码
$(document).on('click','.captcha-img', function () {
    let my = $(this), src = my.attr('src');
    my.attr('src', src.substr(0, src.indexOf('?')+1)+Math.random());
})