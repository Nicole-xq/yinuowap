@extends('layouts.app')
@push('styles')
<link href="{{ asset('css/login_reg.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">登录</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('submitLogin') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="appid" value="{{$appid or ''}}">
                        <input type="hidden" name="redirect_url" value="{{$redirect_url or ''}}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control" name="email" placeholder="邮箱/手机号" value="{{ old('email') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {{--<label for="password" class="col-md-4 control-label">密码</label>--}}

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" placeholder="密码" required>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }} captcha" @if (!$isNeedCaptcha) hidden @endif>
                            {{--<label for="password" class="col-md-4 control-label">密码</label>--}}

                            <div class="col-md-12">
                                <input id="captcha" type="text" class="form-control captcha-input" name="captcha" placeholder="验证码" minlength="5" maxlength="5" @if ($isNeedCaptcha) required @endif><img title="点击更换" class="captcha-img" src="{{captcha_src()}}" alt="captcha">
                            </div>
                        </div>
{{--                        <div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 记住我--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn-login">
                                    登录
                                </button>
                                @if ( $errors->first())
                                    <div class="has-error">
                                    @if ($errors->has('email') && strpos($errors->first('email'), '邮箱') !== false)
                                        <a href="{{route('emailReverify',old('email'))}}">
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        </a>
                                    @else
                                        <span class="help-block">
                                        <strong>{{ $errors->first() }}</strong>
                                    </span>
                                    @endif
                                    </div>
                                @endif
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    忘记密码?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/login_reg.js') }}"></script>
@endpush