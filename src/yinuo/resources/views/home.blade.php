@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">欢迎</div>

                <div class="panel-body">
                    欢迎 {{ Auth::user()->name }}，这是ycc-admin主页
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
