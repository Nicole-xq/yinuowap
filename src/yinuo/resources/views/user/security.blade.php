<form class="form-horizontal" role="form" style="padding:15px;">
    <div class="form-group">
        <label for="real_name" class="col-sm-2 control-label">原有密码</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="original_pwd"
                   placeholder="请输入原有密码">
        </div>
    </div>
    <div class="form-group">
        <label for="idcard_num" class="col-sm-2 control-label">新密码</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="new_pwd"
                   placeholder="请输入新密码">
        </div>
    </div>
    <div class="form-group">
        <label for="nation" class="col-sm-2 control-label">确认新密码</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="confirm_pwd"
                   placeholder="请确认新密码">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="reset" class="btn btn-default" id="cancel">取消</button>
            <button type="submit" class="btn btn-info" id="save-security">保存</button>
        </div>
    </div>
</form>