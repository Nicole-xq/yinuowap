<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
    </head>
    <body>

        <div style="margin:100px;">
            <span class="glyphicon glyphicon-home"></span>
            <a href="/adminIndex">返回后台</a>
            <br>
            <br>
            <ul class="nav nav-tabs lab">
                <li role="presentation" class="active"><a href="#0">个人信息</a></li>
                <li role="presentation"><a href="#1">登录日志</a></li>
                <li role="presentation"><a href="#2">密码修改</a></li>
            </ul>
            <div class="box-div">
                <div class="icon">
                    @include('user.userInfo')
                </div>
                <div class="icon" style="display:none;">
                    @include('user.loginLog')
                </div>
                <div class="icon" style="display:none;">
                    @include('user.security')
                </div>
            </div>
        </div>
    </body>
    <script>
        $('body').on('click', '.lab li', function () {
            //先把所有的remove掉，再给发生click事件的绑定上class
            $('.lab li').removeClass('active');
            $(this).addClass('active');
            var index = $('.lab li').index($(this));
//            alert(index);
            $('.icon').each(function (i) {
                if(i == index){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            })
        });

        //性别默认值
        $("input[name='sex']").each(function () {
            if($(this).val() == '{{$user['sex'] or 0}}'){
                $(this).attr('checked', 'checked');
            }
        });
        //保存用户信息
        $('#save-info').on('click', function () {
            var sex = $("input[name='sex']:checked").val();
            if(sex == undefined){
                sex = 0;
            }
            var real_name = $('#real_name').val();
            var idcard_num = $('#idcard_num').val();
            var nation = $('#nation').val();
            var homeplace = $('#homeplace').val();
            var position = $('#position').val();
            var id = '{{$user['id']}}';
            var user_id = '{{$user['user_id']}}';
            if(sex == 0 && real_name == '' && idcard_num == '' && nation == '' && homeplace == '' && position == ''){
                alert('未编辑数据');
                return false;
            }
            if(user_id == ''){
                alert('请重新刷新页面');
                return false;
            }
            var data = {'user_info':{'real_name':real_name, 'idcard_num':idcard_num, 'nation':nation, 'homeplace':homeplace, 'position':position, 'sex':sex, 'id':id, 'user_id':user_id}};
            console.log(data);
            $.ajax({
                url:'{{route('updatePersonal')}}',
                type:'POST',
                data:data,
                success:function (res) {
                    if(res.code == 1000){
                        alert('保存成功');
                    }else{
                        alert('网络问题，请稍后重试');
                    }
                }
            });
            return false;
        })
        //保存密码
        $('#save-security').on('click', function () {
            var original_pwd = $('#original_pwd').val();
            var new_pwd = $('#new_pwd').val();
            var confirm_pwd = $('#confirm_pwd').val();
            if(original_pwd == '' || new_pwd == '' || confirm_pwd == ''){
                alert('请输入完整');
                return false;
            }
            if(new_pwd != confirm_pwd){
                alert('两次输入密码不一致');
                return false;
            }
            var pwdReg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
            var pwdRes = pwdReg.test(new_pwd);
            if(!pwdRes){
                alert('密码需由6~20位数字、字母组合');
                return false;
            }
            var data = {'original_pwd': original_pwd, 'new_pwd':new_pwd, 'confirm_pwd':confirm_pwd};
            console.log(data);
            $.ajax({
                url:'{{route('resetPwd')}}',
                type:'POST',
                data:data,
                success:function (res) {
                    if(res.code == 1000){
                        alert('密码修改成功');
                    }else{
                        alert(res.status);
                    }
                }
            });
        })

    </script>
<script>
    (function ($) {
        //确认状态
        $(document).on('click', '[data-but="confirm_status"]:not(.disabled)', function () {
            if(!confirm('确认修改?')){
                return;
            }
            var my = $(this);
            var confirm_status = my.data('confirm_status');
            var logId = my.data('log_id');
            $.post('personal/loginLogConfirm', {confirm_status:confirm_status, logId:logId}, function (res) {
                if(res.code!=1000){
                    alert(res.status);
                    return;
                }
                my.siblings('[data-but="confirm_status"].disabled').removeClass('disabled');
                my.parents('.unusual').eq(0)
                    .removeClass('unusual-confirm_status-0 unusual-confirm_status-1 unusual-confirm_status-2 unusual-confirm_status-3').addClass('unusual-confirm_status-'+confirm_status);
                my.addClass('disabled');
            })
        })
        //触发标签
        if(location.hash){
            var nowLi = location.hash.substr(1);
            $('body .lab li:eq(' + nowLi + ')').size() > 0 && $('body .lab li:eq(' + nowLi + ')').trigger('click');
        }
    })(jQuery)
</script>
</html>