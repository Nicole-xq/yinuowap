<style>
    .unusual{
        color: red;
    }
    /*确认状态 0.未确认 1.确认自己登录 2.无法确认 3.确认非自己登录*/
    .unusual-confirm_status-0{
        font-weight: bold;
    }
    .unusual-confirm_status-1{
        color: #333;
    }
    .unusual-confirm_status-2{
    }
    .unusual-confirm_status-3{
        font-weight: bold;
    }
    .disabled{
        background-color: #a4e7fb !important;
    }
</style>
<table class="table table-hover" style="margin-top:15px;">
    <caption style="margin-bottom:25px;">登录日志记录了您在本账号中进行的登录操作。防止异常登录的出现，便于您的核查。</caption>
    <thead>
    <tr>
        <th>登录IP</th>
        <th>登录时间</th>
        <th>登录地址</th>
        <th>是否异常</th>
        <th>确认状态</th>
    </tr>
    </thead>
    <tbody>
    @foreach($log as $low)
        <tr @if($low['is_unusual']) class="unusual unusual-confirm_status-{{ $low['confirm_status'] }}" @endif>
            <td>{{$low['ip']}}</td>
            <td>{{$low['diff_time']}}</td>
            <td>{{ $low['country'] . '&nbsp;&nbsp;' .$low['region'] . '&nbsp;&nbsp;' .$low['city'] }}</td>
            <td>@if($low['is_unusual'])异常登录@else正常登录@endif</td>
            <td>
                <div class="btn-group" data-toggle="buttons-checkbox">
                @if($low['is_unusual'])
                        @foreach($confirm_status_texts as $key=>$text)@if($key != 0)
                            <button class="btn btn-sm btn-info @if($key == $low['confirm_status']) disabled @endif"
                                    data-but="confirm_status" data-confirm_status="{{$key}}" data-log_id="{{$low['id']}}" >{{$text}}</button>@endif
                        @endforeach
                    @endif
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>