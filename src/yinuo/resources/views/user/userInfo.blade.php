<form class="form-horizontal" role="form" style="padding:15px;">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">系统用户名</label>
        <div class="col-sm-10">
            <div style="padding:8px;">{{$user['name'] or ''}}</div>
        </div>
    </div>
    <div class="form-group">
        <label for="real_name" class="col-sm-2 control-label">真实姓名</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="real_name"
                   placeholder="请输入名字" value="{{$user['real_name'] or ''}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right"><font><font class=""> 性别 </font></font></label>
        <div class="col-sm-10">
            <label class="control-label no-padding-right" for="male">
                <input type="radio" name="sex" value="1" id="male">男
            </label>&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label no-padding-right" for="female">
                <input type="radio" name="sex" value="2" id="female">女
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="idcard_num" class="col-sm-2 control-label">身份证号</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="idcard_num"
                   placeholder="请输入身份证号" value="{{$user['idcard_num'] or ''}}">
        </div>
    </div>
    <div class="form-group">
        <label for="nation" class="col-sm-2 control-label">民族</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nation"
                   placeholder="请输入民族" value="{{$user['nation'] or ''}}">
        </div>
    </div>
    <div class="form-group">
        <label for="homeplace" class="col-sm-2 control-label">籍贯</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="homeplace"
                   placeholder="例：广东省广州市" value="{{$user['homeplace'] or ''}}">
        </div>
    </div>
    <div class="form-group">
        <label for="position" class="col-sm-2 control-label">职位</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="position"
                   placeholder="请输入职位" value="{{$user['position'] or ''}}">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info" id="save-info">保存</button>
        </div>
    </div>
</form>