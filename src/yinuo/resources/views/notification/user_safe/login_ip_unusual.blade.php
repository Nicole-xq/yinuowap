@extends('notification.layouts.app')

@section('content')
<div>
    <div>
        <div>
            <div>
                <div style="color: #ff0000;">{{ $title }}</div>
                <div>
                    <p>登录IP: {{ $ip }}</p>
                    <p>登录地址: {{ $country . '&nbsp;&nbsp;' .$region . '&nbsp;&nbsp;' .$city }}</p>
                    <p>登录时间: {{ date('Y-m-d H:i:s', $login_time) }}</p>
                    <p><a href="{{\App\Lib\Helpers\AppHelper::getFullUrl('signon/login',['redirect_url'=> '/personal#1']) }}" target="_blank">查看|处理</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
