<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=ProgId content=Excel.Sheet>
    <meta name=Generator content="Microsoft Excel 12">
</head>

<body>
<!--[if !excel]>　　<![endif]-->
<!--下列信息由 Microsoft Office Excel 的“发布为网页”向导生成。-->
<!--如果同一条目从 Excel 中重新发布，则所有位于 DIV 标记之间的信息均将被替换。-->
<!----------------------------->
<!--“从 EXCEL 发布网页”向导开始-->
<!----------------------------->

<div id="Book1_23007" align=center x:publishsource="Excel">

    <table border="1" bordercolor="#000000" cellpadding=0 cellspacing=0 width=2160 style='border-collapse:
 collapse;table-layout:fixed;width:1620pt'>
        <col width=72 span=30 style='width:54pt'>
        <tr height=27 valign=middle style='height:20.25pt'>
            <td colspan=<?php echo $dateInfo['maxDay']+3; ?> height=27 class=xl6323007 align=center width=2160
                style='height:20.25pt;width:1620pt'><font class="font523007" size="5"><b><?php echo $dateInfo['nowMonth']; ?>月统计</b></font></td>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 rowspan=2 height=36 class=xl6523007 align=center
                style='border-right:.5pt solid black;height:27.0pt'>用户数据</td>
            <?php foreach ($dateInfo['monthDays'] as $value):?>
                <td class=xl6723007 align=center style='border-top:none;border-left:none'><?php echo $value; ?>日</td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <?php foreach ($dateInfo['monthDays'] as $value):?>
                <td class=xl6723007 align=center style='border-top:none;border-left:none'>周<?php echo $dateInfo['weekStr'][date("w", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]; ?></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7023007 bgcolor="#FFFF00" align=center
                style='border-right:.5pt solid black;height:13.5pt'>PV</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"> </font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7323007 bgcolor="#FFFF00" align=center
                style='border-right:.5pt solid black;height:13.5pt'>UV</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"> </font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7323007 bgcolor="#FFFF00" align=center
                style='border-right:.5pt solid black;height:13.5pt'>IP</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"> </font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7323007 bgcolor="#FFFF00" align=center
                style='border-right:.5pt solid black;height:13.5pt'>跳出率</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"> </font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7323007 bgcolor="#FFFF00" align=center
                style='border-right:.5pt solid black;height:13.5pt'>访问时长</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"> </font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl6723007 align=center style='height:13.5pt'>活跃总数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["活跃总数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl7523007 align=center width=216
                style='height:13.5pt;width:162pt'>老用户</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["老用户"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=3 height=18 class=xl6723007 align=center style='height:13.5pt'>新增用户</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["新增用户"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=30 rowspan=2 height=36 class=xl7623007 align=center
                style='border-right:.5pt solid black;border-bottom:.5pt solid black;
  height:27.0pt'>拍卖统计</td>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=4 height=72 class=xl8223007 align=center width=72
                style='height:54.0pt;border-top:none;width:54pt'>保证金</td>
            <td colspan=2 class=xl6723007 align=center style='border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["保证金"]["总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl6723007 align=center style='height:13.5pt;
  border-left:none'>微信</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["保证金"]["微信"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl6723007 align=center style='height:13.5pt;
  border-left:none'>余额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["保证金"]["余额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl6723007 align=center style='height:13.5pt;
  border-left:none'>退回金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["保证金"]["退回金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=5 height=90 class=xl6723007 align=center style='height:67.5pt'>竞价频率</td>
            <td colspan=2 class=xl6723007 align=center style='border-left:none'>竞价次数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["竞价频率"]["竞价次数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl8523007 align=center style='border-right:
  .5pt solid black;height:13.5pt;border-left:none'>竞价人数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["竞价频率"]["竞价人数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl8523007 align=center style='border-right:
  .5pt solid black;height:13.5pt;border-left:none'>最高竞价</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["竞价频率"]["最高竞价"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl8723007 align=center width=144
                style='border-right:.5pt solid black;height:13.5pt;border-left:none;
  width:108pt'>最低竞价</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["竞价频率"]["最低竞价"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl8923007 align=center width=144
                style='height:13.5pt;border-left:none;width:108pt'>平均用户竞价次数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["竞价频率"]["平均用户竞价次数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=12 height=216 class=xl9023007 align=center style='border-bottom:
  .5pt solid black;height:162.0pt;border-top:none'>返佣</td>
            <td colspan=2 class=xl6723007 align=center style='border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=5 height=90 class=xl7523007 align=center width=72
                style='height:67.5pt;border-top:none;width:54pt'>自身竞价</td>
            <td class=xl9223007 align=center style='border-top:none;border-left:none'><font
                        color="#000000">10%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["自身竞价10"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">12%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["自身竞价12"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">15%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["自身竞价15"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">18%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["自身竞价18"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">20%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["自身竞价20"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=5 height=90 class=xl7523007 align=center width=72
                style='height:67.5pt;border-top:none;width:54pt'>下级返佣</td>
            <td class=xl9223007 align=center style='border-top:none;border-left:none'><font
                        color="#000000">12%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["下级返佣12"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">14%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["下级返佣14"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">18%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["下级返佣18"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">20%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["下级返佣20"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9223007 align=center style='height:13.5pt;border-top:
  none;border-left:none'><font color="#000000">22%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["下级返佣22"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl9423007 style='height:13.5pt;border-top:none;
  border-left:none'>成交返佣</td>
            <td class=xl9223007 align=center style='border-top:none;border-left:none'><font
                        color="#000000">20%</font></td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["返佣"]["成交返佣20"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td rowspan=2 height=36 class=xl9023007 align=center style='border-bottom:
  .5pt solid black;height:27.0pt;border-top:none'>成交价</td>
            <td colspan=2 class=xl6723007 align=center style='border-left:none'>最高</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["成交价"]["最高"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 height=18 class=xl6723007 align=center style='height:13.5pt;
  border-left:none'>最低</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["成交价"]["最低"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=30 rowspan=2 height=36 class=xl7623007 align=center
                style='border-right:.5pt solid black;border-bottom:.5pt solid black;
  height:27.0pt'>销售统计</td>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 rowspan=7 height=126 class=xl6723007 align=center
                style='height:94.5pt'>总交易额</td>
            <td class=xl6723007 align=center style='border-top:none;border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>商城</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["商城"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>拍卖</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["拍卖"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>客单价</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["客单价"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>黄山交易金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["黄山交易金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["黄山拍卖笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 rowspan=2 height=36 class=xl7523007 align=center width=144
                style='height:27.0pt;width:108pt'>拍卖今日订单已付</td>
            <td class=xl6723007 align=center style='border-top:none;border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["拍卖今日订单已付总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["拍卖今日订单已付笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 rowspan=2 height=36 class=xl7523007 align=center width=144
                style='height:27.0pt;width:108pt'>拍卖今日订单待付</td>
            <td class=xl6723007 align=center style='border-top:none;border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["拍卖今日订单待付总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["总交易额"]["拍卖今日订单待付笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 rowspan=3 height=54 class=xl6723007 align=center
                style='height:40.5pt'>3日违约</td>
            <td class=xl6723007 align=center style='border-top:none;border-left:none'>订单总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["3日数据"]["3日违约订单总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["3日数据"]["3日违约笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>扣除总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["3日数据"]["3日违约扣除总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td colspan=2 rowspan=2 height=36 class=xl6723007 align=center
                style='height:27.0pt'>3日已付</td>
            <td class=xl6723007 align=center style='border-top:none;border-left:none'>总金额</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["3日数据"]["3日已付总金额"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <tr height=18 valign=middle style='height:13.5pt'>
            <td height=18 class=xl6723007 align=center style='height:13.5pt;border-top:
  none;border-left:none'>笔数</td>
            <?php foreach ($dateInfo['monthDays'] as $value):
                ?>
                <td class=xl7223007 style='border-top:none;border-left:none'><font
                            color="#000000"><?php  if($nowData = $data[(int)date("Ymd", strtotime($dateInfo['nowYear']."-". $dateInfo['nowMonth']."-".$value))]) echo json_decode($nowData['context'], true)["3日数据"]["3日已付笔数"]; ?></font></td>
            <?php endforeach;?>
        </tr>
        <![if supportMisalignedColumns]>
        <tr height=0 style='display:none'>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
            <td width=72 style='width:54pt'></td>
        </tr>
        <![endif]>
    </table>

</div>


<!----------------------------->
<!--“从 EXCEL 发布网页”向导结束-->
<!----------------------------->
</body>

</html>
