<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '用户名或密码错误',
    'throttle' => '登录操作频繁,请在 :seconds 秒后重试.',
    //增加邮箱验证
    'unverified'=>'邮箱未激活，请激活邮箱',
];
