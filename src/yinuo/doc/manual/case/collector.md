### 催收管理

~~~
系统自动派单，系统会自动在工作台分配一个订单给催收员，处理过的订单会进入催收员自己跟进中、还款中、成功订单菜单中。
~~~

> 工作台

~~~
催收员在待处理中接收系统派单，写催记作为处理订单的标识。

~~~

![](assets/collector_daichuli.png)
![](assets/collector_daichuli_1.png)


> 添加联系人

~~~
如果联系人不在联系人信息列表中，点击添加联系人按钮添加联系人
~~~
![](assets/collector_daichuli_2.png)


> 写催记

~~~
点击联系人添加催记
~~~
![](assets/collector_daichuli_3.png)

~~~
如果催收结果为还款（部分还款、全额还款），则还需录入还款凭证
~~~
![](assets/collector_daichuli_4.png)

~~~
点击『查看所有催收记录』查看案件所有催收的记录，
点击有边的『查看详情』可以查看催记详细记录。
点击『查看联系人信息』返回
~~~
![](assets/collector_daichuli_5.png)

> 申请结案

~~~
点击页面底部 『申请结案』,申请后订单会流转到组长审核，审核完成后订单催收完成。如果被组长退回，则需要当前催收员继续催收。
~~~
![](assets/collector_daichuli_6.png)

> 结案审核

~~~
组长审核组员催收完成的订单，检测发现实际没有催收完成的可以退回给组员继续让催收。组长要重点关注还款记录。
~~~
列表页
![](assets/collector_jieanshenghe_8.png)

点击『查看详情』进入订单详情审核订单
![](assets/collector_jieanshenghe_9.png)

勾选还款记录，选择审核结果
![](assets/collector_jieanshenghe_10.png)

> 组长查看订单

~~~
该功能主要为方便组长对于异常案件直接处理。
组长可以查看到组长所负责的所有案件，并且可以直接结束案件。
~~~

![](assets/collector_chakandingd_11.png)

> 组员管理

组员管理分为三部分：查看组员、目标设定、查看组员业绩
~~~
方便组长管理组员、查看组员业绩、设定组员绩效目标
~~~

* 查看组员
 
 ~~~
 组长可以触发给组员派单；
 撤回功能会把组员所有订单都撤回到系统中；
 停止派单是以后不在继续派单给这个催收员。
 ~~~
 
![](assets/collector_daichuli_7.png)


* 目标设定

~~~
组长可以设定不同产品、不同批次下的目标回收率、挑战回收率
~~~

![](assets/collector_12.png)


* 查看组员业绩

![](assets/collector_14.png)

> 报表

~~~
首页显示的是当前登录用户的个人业绩；
组长在组员管理》查看组员业绩 里面可以看到每个组员的业绩
~~~

* 首页个人业绩

![](assets/collector_13.png)


* 组员业绩

![](assets/collector_14.png)





