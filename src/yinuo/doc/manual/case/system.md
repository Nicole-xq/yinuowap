### 系统管理

系统管理包括两个模块：系统配置、策略配置

##### 系统配置 
> 系统配置目前支持字典类别配置;

![](assets/sys_config.png)

> 添加字典类别

字典添加成功后就会在当前列表中显示出来。在字典列表中可以点击编辑字典、禁用字典、添加字典的子集、查看字典子集功能。

```
字典 key 添加后不可修改
```

![](assets/sys_config_add_dict.png)
> 添加字典子集

字典值支持 String 和 Json 格式内容

![](assets/sys_config_add_dict_child.png)

> 子集字典管理

![](assets/sys_config_add_dict_child_mange.png)


#### 策略配置

~~~
派单中关键参数的配置；其他操作参考系统配置。
~~~

![](assets/sys_celve.png)




