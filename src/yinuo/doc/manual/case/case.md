### 案件管理

> 案件管理包括两个模块：机构管理、案件管理

案件导入流程
![](assets/anjiandaoruliucheng.png)



##### 机构管理 

> 管理案件所属机构，及机构负责人信息

![](assets/jigouguanliliebiao.png)

> 添加机构

![](assets/case_product_list.png)

> 查看机构

~~~
机构详情中可以查看到机构下面的所有案件催收进度
~~~

案件详情，点击查看可以直接进入案件详情
![](assets/case_product_anjianjinduchakan.png)

案件统计
![](assets/case_product_anjiantongji.png)

##### 案件管理

> 案件导入

~~~
案件需要研发先根据甲方数据格式导入系统，运营在操作
~~~

![](assets/case_anjiandaoru.png)

> 案件明细

~~~
甲方在这里查看催收进度
~~~

![](assets/case_anjian_detail.png)


> 案件分配

~~~
运营发布案件后，在这里分配案件给相应催收团队。
设定佣金、目标回收率、目标回款的值必须输入。
~~~

* 设定佣金(最大分配给催收团队的佣金)
* 目标回收率（设定催收团队的回收率）
* 目标回款（设定催收团队的回款率）

![](assets/case_anjian_fenpei.png)


> 案件撤回

~~~
甲方收回案件后需要在这里撤回案件
或者案件需要重新分配时。
~~~

![](assets/case_anjiancehui.png)



