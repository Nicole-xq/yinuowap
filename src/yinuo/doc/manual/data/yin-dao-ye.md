### 引导页

1.访问域名[**http://data.yicuicui.com/**](http://data.yicuicui.com/，可以看到引导页，此时没有登录限制)，可以看到引导页，此时没有登录限制

![](/assets/import.png)

2.点击以上的项目业绩查询或个人业绩查询（个人业绩查询功能暂不支持，只开放项目业绩查询），没有登录时会跳转到passport平台统一登录（SSO单点登录），登录后根据用户的身份显示对应的信息，如果没有权限显示以下提示：

![](/assets/noPermit.png)

3.用户角色相关权限配置在i平台进行配置，地址：[http://i.yicuicui.com/adminIndex\#/admin/role/index](http://i.yicuicui.com/adminIndex#/admin/role/index)

![](/assets/permit.png)![](/assets/role-permission.png)

先给用户分配角色，再给角色分配权限，如果需要看到项目业绩图表，点击对应的角色，在右边栏选择“易催催数据报表系统”，勾选“菜单-催收业绩”下所有的子权限，则相关角色即能看到项目业绩图表。

