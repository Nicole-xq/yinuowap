# Summary

* [介绍](/README.md)
* i系统
  * i平台手册-概述
    * [帐号申请，登录，忘记密码](i/register-login.md)
    * [基本模块简介](i/basemodule.md)
    * [用户中心](i/account-info.md)
* data系统

## i平台手册-概述

* [帐号申请，登录，忘记密码](i/register-login.md)
* [基本模块简介](i/basemodule.md)
* [用户中心](i/account-info.md)

## i平台手册-后台管理

* [用户管理](i/user-manage.md)
* [权限管理](i/permission.md)
* [角色管理](i/role-manage.md)
* [平台管理](i/platform-manage.md)

## i平台手册-组织管理

* [组管理](i/group-manage.md)
  * [管理组员](i/group-user-manage.md)

## i平台手册-应用中心\(Sass服务接入\)

* [我的应用](i/my_app.md)
  * [应用详情](i/app_info.md)

## i平台手册-三方接入管理\(Sass服务接入管理\)

* [应用列表](i/third-party-admin-list.md)
  * [查看应用](i/third-party-admin-show.md)
* [审核应用](i/third-party-admin-audit-list.md)
* [API列表](i/api_list.md)
  * [新建API](i/add_api.md)
* [API申请审核](i/api-audit.md)

## case平台手册

* [介绍](case/index.md)
* [系统管理](case/system.md)
* [案件管理](case/case.md)
* [催收管理](case/collector.md)

## data平台手册

* [data平台概述](data/index.md)
* [引导页](data/yin-dao-ye.md)
* [项目业绩查询](data/xiang-mu-ye-ji-cha-xun.md)

