## 三方接入管理-API列表（Sass服务API管理）

**三方接入管理**模块是用来管理Sass服务接入app、app申请、Sass服务api、api申请的模块，其中“**API列表**”用来管理Sass服务提供的API

在i平台的左侧边栏中的“三方接入管理”-“API列表”中，可以查看所有Sass服务提供的API。

1. API列表可以使用“Api名称”、“创建用户uid”、“平台id”、“api\_uri”进行筛选（全词匹配）。
2. 在API列表项 右侧可以对api进行“编辑“，“查看”，“发布”，“下线”操作。
3. 点击右上角"新建API"可以新建新的API。

![](/doc/manual/i/img/api_list.jpg)

### 编辑API

点击API列表项 右侧的“编辑“按钮可以对API进行编辑，编辑细节参考“[新建API](/i/add_api.md)”操作。

### 查看API

点击API列表项 右侧的“查看“按钮可以查看API详情。如果API未发布，下放会出现“发布Api”按钮，点击可以发布api。

![](/doc/manual/i/img/show_api_info.jpg)

