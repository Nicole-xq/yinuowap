## 帐号申请，登录

---

### 1.帐号申请

申请帐号请联系主管，主管使用“公司邮箱+员工手机”号码添加用户！

### 2.登录i后台

打开地址 [http://i.yicuicui.com](http://i.yicuicui.com) 自动进入登录页面，使用邮箱或手机号码登录平台！

![](/doc/manual/i/img/login.jpg)

注：

* 初始密码为主管设置密码。
* 判断是否需要验证码规则：
  ```
  30分钟内ip登录错误次数达到10次（相应ip登录需要验证码）
  30分钟内指定"用户名"登录错误次数达到2次（相应"用户"登录需要验证码）
  最近一次为异地登录 （相应"用户"登录需要验证码）
  ```

### **3.忘记密码**

打开地址 [http://i.yicuicui.com](http://i.yicuicui.com) 自动进入登录页面，点击登录按钮下方“忘记密码”连接，可重置密码，以下是重置密码流程。

1.进入"重置密码页面",输入邮箱地址，点击“发送邮件”按钮系统会发送一封重置密码邮件到指定邮箱；

![](/doc/manual/i/img/reset_password.jpg)

2.点击重置密码邮件的"reset password 按钮",如果无法点击请复制下方连接粘贴到浏览器中打开；

![](/doc/manual/i/img/reset_password1.jpg)

3.进入重置密码页面，输入邮箱地址、新的密码提交重置密码；

![](/doc/manual/i/img/reset_password2.jpg)

