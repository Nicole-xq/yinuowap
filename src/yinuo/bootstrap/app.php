<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/
$app = new App\Application(
    realpath(__DIR__.'/../')
);


/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/


$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

//设置日志 todo 这边必须和LogProvider->getInstance() 同步,否者会不一致
$app->configureMonologUsing(function ($monolog) {
    // create a log channel
    // the default date format is "Y-m-d H:i:s"
    if(defined('InShopNC')){
        $logConfig = YNApp()->log->getLogConfig();
    }else{
        //todo 这里没写成通用的log配置 没有写钉钉token配置
        $dateFormat = "Y-m-d H:i:s";
        $output = "[%datetime% %extra.url% %extra.ip%] %extra.uid% > %level_name% > %message% %context% {%extra.file%:%extra.line%}\n";
        $formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat);
        $logConfig = new \Yinuo\Providers\LogProvider\LogConfig(isProduction() ? \Yinuo\Providers\LogProvider\LogProvider::INFO : \Yinuo\Providers\LogProvider\LogProvider::DEBUG,
            __DIR__."/../../../data/log/yinuo_shopnc_system.log", $formatter);
    }


    //将记录记录到任何PHP流中，将其用于日志文件。
    $streamHandler = new \Monolog\Handler\StreamHandler($logConfig->getLogPath(), $logConfig->getLevel());
    //new RotatingFileHandler(); 每天一个文件，会自动删除比$maxFiles老的文件，这只是一个
    $streamHandler->setFormatter($logConfig->getFormatter());
    $monolog->pushHandler($streamHandler);

    //日志到钉钉
    $dingdingHandler = new \Yinuo\Providers\LogProvider\DingDingHandler(\Monolog\Logger::ERROR,
        isProduction() ? $logConfig->getDingDingProToken() : $logConfig->getDingDingTestToken(),
        true,
        $logConfig->getDingDingStartTime(),
        $logConfig->getDingDingEndTime()
    );
    $dingdingHandler->setFormatter($logConfig->getFormatter());
    $monolog->pushHandler($dingdingHandler);

    //为日志记录添加唯一标识符。
    $uidProcessor = new \Monolog\Processor\UidProcessor();
    //添加发起日志调用的行/文件/类/方法。
    $introspectionProcessor = new \Monolog\Processor\IntrospectionProcessor(\Monolog\Logger::ERROR);
    //将当前请求URI，请求方法和客户端IP添加到日志记录中。
    $webProcessor = new \Monolog\Processor\WebProcessor(null, ["url", "ip"]);
    $monolog->pushProcessor($uidProcessor);
    $monolog->pushProcessor($webProcessor);

    if(!isProduction()){
        /*            $chromePhpHandler = new ChromePHPHandler($config->getLevel());
                    $log->pushHandler($chromePhpHandler);*/
        $phpConsole = new \Yinuo\Providers\LogProvider\MyPHPConsoleHandler([], null, $logConfig->getLevel());
        $introspectionProcessor = new \Monolog\Processor\IntrospectionProcessor($logConfig->getLevel());
        $monolog->pushHandler($phpConsole);
        //$log->pushHandler(new BrowserConsoleHandler([], null, $config->getLevel()));
    }
    $monolog->pushProcessor($introspectionProcessor);
});


/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
