<?php
namespace App\Mq\Handler;

use App\Lib\HelperFactory;
use App\Mq\Message\LoginFinishMessage;
use App\Services\Notification\UserSafeNotification;
use App\User;
//use Galaxy\Framework\MQ\Lib\MessageHandler;
//use Galaxy\Framework\MQ\Lib\Message;


//class LoginIpPositionHandler extends MessageHandler{
class LoginIpPositionHandler{

     const QUEUE_NAME = 'login_ip_position_queue';
     public function __construct()
     {
         //parent::__construct(self::QUEUE_NAME);
         $this->message_ids = [
            LoginFinishMessage::MESSAGE_ID
         ];
     }

    /**
     * 消息处理结果
     * @param LoginFinishMessage|Message $message LoginFinishMessage 消息实例
     * @return bool
     */
     public function handle($message){
//     public function handle(Message $message){
         $data = $message->data();
         //记录的数据
         $loginIpInfoLogData = [
             'ip' => $data['ip'],
             'user_id' => $data['uid'],
             'login_time' => $data['loginTime'],
         ];
         $ipInfo = HelperFactory::instance()->Ip->getIpInfo($data['ip']);
         $isUnusual = false;
         if($ipInfo){
             $loginIpInfoLogData = array_merge($loginIpInfoLogData, $ipInfo);
             $isUnusual = !application()->userSafeApi->checkIpPosition($data['uid'], $ipInfo);
         }
         $loginIpInfoLogData['is_unusual'] = $isUnusual;
         application()->userSafeApi->addLoginIpInfoLog($loginIpInfoLogData);
         //后续事件 后期可以考虑事件
         if($isUnusual){
             if(config('my_safe.is_send_login_unusual_mail')){
                 //这里可以考虑是否要控制发送频率， 例如：如果2小时内已发送过 就不发
                 $email = User::where('id', $data['uid'])->value('email');
                 if($email){
                     UserSafeNotification::instance()->LoginIpUnusual($email, $loginIpInfoLogData);
                 }
             }
         }
         return true;
     }
}