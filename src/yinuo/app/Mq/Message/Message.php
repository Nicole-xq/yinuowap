<?php
namespace App\Mq\Message;
use App\Lib\Helpers\LogHelper;
//use Galaxy\Framework\Contracts\Service\Log;
//use Galaxy\Framework\MQ\Lib\Message as BaseMessage;

/**
 * 消息基类
 */
//class Message extends BaseMessage
class Message
{
    public static function sendMessage(...$arg){
        try{
            application()->messageCenter->send(new static(...$arg));
            return true;
        }catch (\Exception $e){
            //Log::error('sendMessage_error',[static::class, LogHelper::getEInfo($e)]);
            return false;
        }
    }
}
