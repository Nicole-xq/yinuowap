<?php
namespace App\Mq\Message;

/**
 * 登录完成消息
 */
class LoginFinishMessage extends Message
{
    const MESSAGE_ID = 'passport_login_finish';

    /**
     * LoginFinishMessage constructor.
     * @param string $uid
     * @param array $ip
     * @param array $attachData 附加信息
     */
    public function __construct($uid, $ip, $attachData = [])
    {
        $data = [
            'uid' => $uid,
            'ip' => $ip,
            'loginTime' => time(),
            'attachData' => $attachData,
        ];
        parent::__construct(self::MESSAGE_ID, $data);
    }
}
