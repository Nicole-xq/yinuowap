<?php
namespace App\Mq\Worker;


use App\Mq\Handler\LoginIpPositionHandler;
use Illuminate\Console\Command;

class LoginIpPositionWorker extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'MqWorkers:LoginIpPosition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '登录完成后 对ip做 位置策略';


    public function handle()
    {
        $this->comment('启动时间:'.date('Y-m-d H:i:s'));

        $handler = new LoginIpPositionHandler();
        application()->messageCenter->register($handler);
        $handler->start();
    }
} 