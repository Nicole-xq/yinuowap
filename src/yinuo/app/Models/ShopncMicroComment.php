<?php

namespace App\Models;



/**
 * Class ShopncMicroComment
 *
 * @property int $comment_id 评论编号
 * @property int $comment_type 评论类型编号
 * @property int $comment_object_id 推荐商品编号
 * @property string $comment_message 评论内容
 * @property int $comment_member_id 评论人编号
 * @property int $comment_time 评论时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroComment whereCommentType($value)
 * @mixin \Eloquent
 */
class ShopncMicroComment extends BaseModel
{
    protected $table = 'shopnc_micro_comment';

    protected $primaryKey = 'comment_id';

	public $timestamps = false;

    protected $fillable = [
        'comment_type',
        'comment_object_id',
        'comment_message',
        'comment_member_id',
        'comment_time'
    ];

    protected $guarded = [];

        
}