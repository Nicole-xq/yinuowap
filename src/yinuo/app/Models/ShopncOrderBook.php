<?php

namespace App\Models;



/**
 * Class ShopncOrderBook
 *
 * @property int $book_id 主键
 * @property int|null $book_order_id 订单ID
 * @property int|null $book_step 预定时段,值为1 or 2,0为不分时段，全款支付
 * @property float|null $book_amount 定金or尾款金额
 * @property float|null $book_pd_amount 预存款支付金额
 * @property float|null $book_rcb_amount 充值卡支付金额
 * @property string|null $book_pay_name 支付方式(文字)
 * @property string|null $book_trade_no 第三方平台交易号
 * @property int|null $book_pay_time 支付时间
 * @property int|null $book_end_time 时段1:订单自动取消时间,时段2:时段结束时间
 * @property int|null $book_buyer_phone 买家接收尾款交款通知的手机,只在第2时段有值即可
 * @property float|null $book_deposit_amount 定金金额,只在全款支付时有值即可
 * @property int|null $book_pay_notice 0未通知1已通知,该字段只对尾款时段有效
 * @property float|null $book_real_pay 订单被取消后最终支付金额（平台收款金额）
 * @property int|null $book_cancel_time 订单被取消时间,结算用,只有book_step是0或1时有值
 * @property int|null $book_store_id 商家 ID,只有book_step是0或1时有值即可
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookBuyerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookCancelTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookDepositAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookPayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookPayNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookPdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookRealPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBook whereBookTradeNo($value)
 * @mixin \Eloquent
 */
class ShopncOrderBook extends BaseModel
{
    protected $table = 'shopnc_order_book';

    protected $primaryKey = 'book_id';

	public $timestamps = false;

    protected $fillable = [
        'book_order_id',
        'book_step',
        'book_amount',
        'book_pd_amount',
        'book_rcb_amount',
        'book_pay_name',
        'book_trade_no',
        'book_pay_time',
        'book_end_time',
        'book_buyer_phone',
        'book_deposit_amount',
        'book_pay_notice',
        'book_real_pay',
        'book_cancel_time',
        'book_store_id'
    ];

    protected $guarded = [];

        
}