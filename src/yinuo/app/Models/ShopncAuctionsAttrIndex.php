<?php

namespace App\Models;



/**
 * Class ShopncAuctionsAttrIndex
 *
 * @property int $auction_id 拍品id
 * @property int $gc_id 拍品分类id
 * @property int $type_id 类型id
 * @property int $attr_id 属性id
 * @property int $attr_value_id 属性值id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsAttrIndex whereAttrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsAttrIndex whereAttrValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsAttrIndex whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsAttrIndex whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsAttrIndex whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsAttrIndex extends BaseModel
{
    protected $table = 'shopnc_auctions_attr_index';

    public $timestamps = false;

    protected $fillable = [
        'auction_id',
        'gc_id',
        'type_id',
        'attr_id',
        'attr_value_id'
    ];

    protected $guarded = [];

        
}