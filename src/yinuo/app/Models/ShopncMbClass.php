<?php

namespace App\Models;



/**
 * Class ShopncMbClass
 *
 * @property int $mb_class_id 手机拍卖分类ID
 * @property string $mb_class_name 手机拍卖分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbClass whereMbClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbClass whereMbClassName($value)
 * @mixin \Eloquent
 */
class ShopncMbClass extends BaseModel
{
    protected $table = 'shopnc_mb_class';

    protected $primaryKey = 'mb_class_id';

	public $timestamps = false;

    protected $fillable = [
        'mb_class_name'
    ];

    protected $guarded = [];

        
}