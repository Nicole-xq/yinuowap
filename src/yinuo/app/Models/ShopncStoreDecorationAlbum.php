<?php

namespace App\Models;



/**
 * Class ShopncStoreDecorationAlbum
 *
 * @property int $image_id 图片编号
 * @property string $image_name 图片名称
 * @property string $image_origin_name 图片原始名称
 * @property int $image_width 图片宽度
 * @property int $image_height 图片高度
 * @property int $image_size 图片大小
 * @property int $store_id 店铺编号
 * @property int $upload_time 上传时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageOriginName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereImageWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationAlbum whereUploadTime($value)
 * @mixin \Eloquent
 */
class ShopncStoreDecorationAlbum extends BaseModel
{
    protected $table = 'shopnc_store_decoration_album';

    protected $primaryKey = 'image_id';

	public $timestamps = false;

    protected $fillable = [
        'image_name',
        'image_origin_name',
        'image_width',
        'image_height',
        'image_size',
        'store_id',
        'upload_time'
    ];

    protected $guarded = [];

        
}