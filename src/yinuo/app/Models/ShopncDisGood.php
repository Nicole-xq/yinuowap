<?php

namespace App\Models;



/**
 * Class ShopncDisGood
 *
 * @property int $distri_id 分销编号
 * @property int|null $goods_commonid 商品编号（商品spu编号）
 * @property string|null $goods_name 商品名称
 * @property string|null $goods_image 商品图片
 * @property int|null $member_id 分销员编号
 * @property string|null $member_name 分销员名称
 * @property int|null $distri_time 分销时间
 * @property int|null $store_id 店铺编号
 * @property string|null $store_name 店铺名称
 * @property int $distri_goods_state 分销商品状态 1正常 2删除
 * @property int|null $quite_time 删除时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereDistriGoodsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereDistriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereDistriTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereQuiteTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGood whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncDisGood extends BaseModel
{
    protected $table = 'shopnc_dis_goods';

    protected $primaryKey = 'distri_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_commonid',
        'goods_name',
        'goods_image',
        'member_id',
        'member_name',
        'distri_time',
        'store_id',
        'store_name',
        'distri_goods_state',
        'quite_time'
    ];

    protected $guarded = [];

        
}