<?php

namespace App\Models;



/**
 * Class ShopncSnsAlbumpic
 *
 * @property int $ap_id 相册图片表id
 * @property string $ap_name 图片名称
 * @property int $ac_id 相册id
 * @property string $ap_cover 图片路径
 * @property int $ap_size 图片大小
 * @property string $ap_spec 图片规格
 * @property int $member_id 所属店铺id
 * @property int $upload_time 图片上传时间
 * @property int $ap_type 图片类型，0为无、1为买家秀
 * @property int $item_id 信息ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereAcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereApType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumpic whereUploadTime($value)
 * @mixin \Eloquent
 */
class ShopncSnsAlbumpic extends BaseModel
{
    protected $table = 'shopnc_sns_albumpic';

    protected $primaryKey = 'ap_id';

	public $timestamps = false;

    protected $fillable = [
        'ap_name',
        'ac_id',
        'ap_cover',
        'ap_size',
        'ap_spec',
        'member_id',
        'upload_time',
        'ap_type',
        'item_id'
    ];

    protected $guarded = [];

        
}