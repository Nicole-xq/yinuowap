<?php

namespace App\Models;



/**
 * Class ShopncAppBroadcast
 *
 * @property int $id 自增ID
 * @property string|null $title 标题
 * @property string|null $subtitle 副标题
 * @property string $content 描述
 * @property string $link 消息链接
 * @property int|null $state 发送状态 0未发送 1已发送
 * @property string $admin_name 操作的管理员
 * @property int $add_time 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAppBroadcast whereTitle($value)
 * @mixin \Eloquent
 */
class ShopncAppBroadcast extends BaseModel
{
    protected $table = 'shopnc_app_broadcast';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'subtitle',
        'content',
        'link',
        'state',
        'admin_name',
        'add_time'
    ];

    protected $guarded = [];

        
}