<?php

namespace App\Models;



/**
 * Class ShopncStoreAttention
 *
 * @property int $attention_id 关注ID
 * @property int $member_id 会员ID
 * @property int $store_id 商家ID
 * @property int $store_vendue_id 商家拍卖ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreAttention whereAttentionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreAttention whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreAttention whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreAttention whereStoreVendueId($value)
 * @mixin \Eloquent
 */
class ShopncStoreAttention extends BaseModel
{
    protected $table = 'shopnc_store_attention';

    protected $primaryKey = 'attention_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'store_id',
        'store_vendue_id'
    ];

    protected $guarded = [];

        
}