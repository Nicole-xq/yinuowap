<?php

namespace App\Models;



/**
 * Class ShopncLandingRelationGood
 *
 * @property int $id 自增ID
 * @property int $goods_id 商品ID
 * @property int $landing_page_id 落地页ID
 * @property string $introduce 介绍
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingRelationGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingRelationGood whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingRelationGood whereIntroduce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingRelationGood whereLandingPageId($value)
 * @mixin \Eloquent
 */
class ShopncLandingRelationGood extends BaseModel
{
    protected $table = 'shopnc_landing_relation_goods';

    public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'landing_page_id',
        'introduce'
    ];

    protected $guarded = [];

        
}