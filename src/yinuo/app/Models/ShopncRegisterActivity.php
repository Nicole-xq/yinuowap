<?php

namespace App\Models;



/**
 * Class ShopncRegisterActivity
 *
 * @property int $id
 * @property int|null $type 赠送类型,0:红包
 * @property int|null $code 奖励内容id(对应奖励类型的id)
 * @property int|null $start_time 活动开始时间
 * @property int|null $end_time 活动结束时间
 * @property string|null $status 活动状态 0:关闭 1:开启
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRegisterActivity whereType($value)
 * @mixin \Eloquent
 */
class ShopncRegisterActivity extends BaseModel
{
    protected $table = 'shopnc_register_activity';

    public $timestamps = false;

    protected $fillable = [
        'type',
        'code',
        'start_time',
        'end_time',
        'status'
    ];

    protected $guarded = [];

        
}