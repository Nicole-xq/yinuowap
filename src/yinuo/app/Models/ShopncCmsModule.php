<?php

namespace App\Models;



/**
 * Class ShopncCmsModule
 *
 * @property int $module_id 模板模块编号
 * @property string $module_title 模板模块标题
 * @property string $module_name 模板名称
 * @property string $module_type 模板模块类型，index-固定内容、article1-文章模块1、article2-文章模块2、micro-微商城、adv-通栏广告
 * @property int $module_class 模板模块种类1-系统自带 2-用户自定义
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModule whereModuleClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModule whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModule whereModuleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModule whereModuleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModule whereModuleType($value)
 * @mixin \Eloquent
 */
class ShopncCmsModule extends BaseModel
{
    protected $table = 'shopnc_cms_module';

    protected $primaryKey = 'module_id';

	public $timestamps = false;

    protected $fillable = [
        'module_title',
        'module_name',
        'module_type',
        'module_class'
    ];

    protected $guarded = [];

        
}