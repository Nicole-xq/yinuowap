<?php

namespace App\Models;



/**
 * Class ShopncOrderCommon
 *
 * @property int $order_id 订单索引id
 * @property int $store_id 店铺ID
 * @property int $shipping_time 配送时间
 * @property int $shipping_express_id 配送公司ID
 * @property int $evaluation_time 评价时间
 * @property string|null $order_message 订单留言
 * @property int $order_pointscount 订单赠送积分
 * @property int|null $voucher_price 代金券面额
 * @property string|null $voucher_code 代金券编码
 * @property string|null $deliver_explain 发货备注
 * @property int $daddress_id 发货地址ID
 * @property string $reciver_name 收货人姓名
 * @property string $reciver_info 收货人其它信息
 * @property int $reciver_province_id 收货人省级ID
 * @property int $reciver_city_id 收货人市级ID
 * @property string|null $invoice_info 发票信息
 * @property string|null $promotion_info 促销信息备注
 * @property string|null $dlyo_pickup_code 提货码
 * @property float|null $promotion_total 订单总优惠金额（代金券，满减，平台红包）
 * @property int|null $discount 会员折扣x%
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereDaddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereDeliverExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereDlyoPickupCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereEvaluationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereInvoiceInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereOrderMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereOrderPointscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon wherePromotionInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon wherePromotionTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereReciverCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereReciverInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereReciverName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereReciverProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereShippingExpressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereShippingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereVoucherCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereVoucherPrice($value)
 * @mixin \Eloquent
 * @property string|null $transit_shipment_express_company 发往中转仓 快递公司
 * @property string|null $transit_shipment_express_number 发往中转仓 快递单号
 * @property string|null $transit_shipment_delivery_time 发往中转仓 发货时间
 * @property string|null $transit_shipment_receiving_time 发往中转仓 收货时间
 * @property int|null $transit_shipment_status 发往中转仓 0.初始状态 1.已发出 2.已收到 3.异常
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereTransitShipmentDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereTransitShipmentExpressCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereTransitShipmentExpressNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereTransitShipmentReceivingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderCommon whereTransitShipmentStatus($value)
 */
class ShopncOrderCommon extends BaseModel
{
    protected $table = 'shopnc_order_common';

    protected $primaryKey = 'order_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'shipping_time',
        'shipping_express_id',
        'evaluation_time',
        'order_message',
        'order_pointscount',
        'voucher_price',
        'voucher_code',
        'deliver_explain',
        'daddress_id',
        'reciver_name',
        'reciver_info',
        'reciver_province_id',
        'reciver_city_id',
        'invoice_info',
        'promotion_info',
        'dlyo_pickup_code',
        'promotion_total',
        'discount'
    ];

    protected $guarded = [];
    //0.初始状态 1.已发出 2.已收到 3.异常
    const TRANSIT_SHIPMENT_STATUS_INIT = 0;
    const TRANSIT_SHIPMENT_STATUS_DELIVERED = 1;

}