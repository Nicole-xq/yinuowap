<?php

namespace App\Models;



/**
 * Class ShopncMemberDistribute20171208
 *
 * @property int $dis_id 分销编号
 * @property int $member_id 会员编号
 * @property int $add_time 添加时间
 * @property string|null $dis_code 邀请码
 * @property string|null $member_mobile 会员手机号
 * @property int|null $top_member 上级会员编号
 * @property int|null $top_member_2 上级的上级会员编号
 * @property int|null $member_type 会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家
 * @property string|null $member_name 会员名称
 * @property string|null $member_comment 用户备注(上级用户对下级用户的备注)
 * @property string|null $member_comment_2 用户备注(上上级用户对2级用户的备注)
 * @property string|null $top_member_name 上级会员名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereDisCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereDisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberComment2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereMemberType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereTopMember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereTopMember2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistribute20171208 whereTopMemberName($value)
 * @mixin \Eloquent
 */
class ShopncMemberDistribute20171208 extends BaseModel
{
    protected $table = 'shopnc_member_distribute_2017_12_08';

    protected $primaryKey = 'dis_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'add_time',
        'dis_code',
        'member_mobile',
        'top_member',
        'top_member_2',
        'member_type',
        'member_name',
        'member_comment',
        'member_comment_2',
        'top_member_name'
    ];

    protected $guarded = [];

        
}