<?php

namespace App\Models;



/**
 * Class ShopncConsultType
 *
 * @property int $ct_id 咨询类型id
 * @property string $ct_name 咨询类型名称
 * @property string $ct_introduce 咨询类型详细介绍
 * @property int $ct_sort 咨询类型排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsultType whereCtId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsultType whereCtIntroduce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsultType whereCtName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsultType whereCtSort($value)
 * @mixin \Eloquent
 */
class ShopncConsultType extends BaseModel
{
    protected $table = 'shopnc_consult_type';

    protected $primaryKey = 'ct_id';

	public $timestamps = false;

    protected $fillable = [
        'ct_name',
        'ct_introduce',
        'ct_sort'
    ];

    protected $guarded = [];

        
}