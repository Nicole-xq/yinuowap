<?php

namespace App\Models;



/**
 * Class ShopncStoreSnsTracelog
 *
 * @property int $strace_id 店铺动态id
 * @property int|null $strace_storeid 店铺id
 * @property string|null $strace_storename 店铺名称
 * @property string|null $strace_storelogo 店标
 * @property string|null $strace_title 动态标题
 * @property string|null $strace_content 发表内容
 * @property string|null $strace_time 发表时间
 * @property int|null $strace_cool 赞数量
 * @property int|null $strace_spread 转播数量
 * @property int|null $strace_comment 评论数量
 * @property int|null $strace_type 1=relay,2=normal,3=new,4=coupon,5=xianshi,6=mansong,7=bundling,8=groupbuy,9=recommend,10=hotsell
 * @property string|null $strace_goodsdata 商品信息
 * @property int $strace_state 动态状态 1正常，0屏蔽
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceCool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceGoodsdata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceSpread($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceStorelogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceStorename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsTracelog whereStraceType($value)
 * @mixin \Eloquent
 */
class ShopncStoreSnsTracelog extends BaseModel
{
    protected $table = 'shopnc_store_sns_tracelog';

    protected $primaryKey = 'strace_id';

	public $timestamps = false;

    protected $fillable = [
        'strace_storeid',
        'strace_storename',
        'strace_storelogo',
        'strace_title',
        'strace_content',
        'strace_time',
        'strace_cool',
        'strace_spread',
        'strace_comment',
        'strace_type',
        'strace_goodsdata',
        'strace_state'
    ];

    protected $guarded = [];

        
}