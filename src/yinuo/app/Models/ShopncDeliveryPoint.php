<?php

namespace App\Models;



/**
 * Class ShopncDeliveryPoint
 *
 * @property int $dlyp_id 提货站id
 * @property string $dlyp_name 提货站登录名
 * @property string $dlyp_passwd 提货站登录密码
 * @property string $dlyp_truename 真实姓名
 * @property string|null $dlyp_mobile 手机号码
 * @property string|null $dlyp_telephony 座机号码
 * @property string $dlyp_address_name 服务站名称
 * @property int $dlyp_area_1 一级地区id
 * @property int $dlyp_area_2 二级地区id
 * @property int $dlyp_area_3 三级地区id
 * @property int $dlyp_area_4 四级地区id
 * @property int $dlyp_area 地区id
 * @property string $dlyp_area_info 地区内容
 * @property string $dlyp_address 详细地址
 * @property string $dlyp_idcard 身份证号码
 * @property string $dlyp_idcard_image 身份证照片
 * @property int $dlyp_addtime 添加时间
 * @property int $dlyp_state 提货站状态 0关闭，1开启，10等待审核, 20审核失败
 * @property string|null $dlyp_fail_reason 失败原因
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypAddressName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypArea1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypArea2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypArea3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypArea4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypFailReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypIdcard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypIdcardImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypTelephony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryPoint whereDlypTruename($value)
 * @mixin \Eloquent
 */
class ShopncDeliveryPoint extends BaseModel
{
    protected $table = 'shopnc_delivery_point';

    protected $primaryKey = 'dlyp_id';

	public $timestamps = false;

    protected $fillable = [
        'dlyp_name',
        'dlyp_passwd',
        'dlyp_truename',
        'dlyp_mobile',
        'dlyp_telephony',
        'dlyp_address_name',
        'dlyp_area_1',
        'dlyp_area_2',
        'dlyp_area_3',
        'dlyp_area_4',
        'dlyp_area',
        'dlyp_area_info',
        'dlyp_address',
        'dlyp_idcard',
        'dlyp_idcard_image',
        'dlyp_addtime',
        'dlyp_state',
        'dlyp_fail_reason'
    ];

    protected $guarded = [];

        
}