<?php

namespace App\Models;



/**
 * Class ShopncCmsPictureImage
 *
 * @property int $image_id 图片编号
 * @property string $image_name 图片地址
 * @property string|null $image_abstract 图片摘要
 * @property string|null $image_goods 相关商品
 * @property string|null $image_store 相关店铺
 * @property int|null $image_width 图片宽度
 * @property int|null $image_height 图片高度
 * @property int $image_picture_id 画报编号
 * @property string|null $image_path 图片路径
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageAbstract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageGoods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImagePictureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageStore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureImage whereImageWidth($value)
 * @mixin \Eloquent
 */
class ShopncCmsPictureImage extends BaseModel
{
    protected $table = 'shopnc_cms_picture_image';

    protected $primaryKey = 'image_id';

	public $timestamps = false;

    protected $fillable = [
        'image_name',
        'image_abstract',
        'image_goods',
        'image_store',
        'image_width',
        'image_height',
        'image_picture_id',
        'image_path'
    ];

    protected $guarded = [];

        
}