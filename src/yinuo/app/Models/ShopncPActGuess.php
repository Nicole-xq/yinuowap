<?php

namespace App\Models;



/**
 * Class ShopncPActGuess
 *
 * @property int $id ID
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property string $name 活动名称
 * @property int $tstart 开始时间
 * @property int $tend 结束时间
 * @property int $state 趣猜状态 10-审核中 20-正常 30-审核失败 31-管理员关闭 32-已结束
 * @property int $recommended 推荐 1：推荐活动 0：普通活动
 * @property string $gs_act_img 活动图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereGsActImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereTend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPActGuess whereTstart($value)
 * @mixin \Eloquent
 */
class ShopncPActGuess extends BaseModel
{
    protected $table = 'shopnc_p_act_guess';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'name',
        'tstart',
        'tend',
        'state',
        'recommended',
        'gs_act_img'
    ];

    protected $guarded = [];

        
}