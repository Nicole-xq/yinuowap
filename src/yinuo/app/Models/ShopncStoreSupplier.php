<?php

namespace App\Models;



/**
 * Class ShopncStoreSupplier
 *
 * @property int $sup_id
 * @property int|null $sup_store_id 商家ID
 * @property string|null $sup_store_name 商家名称
 * @property string|null $sup_name 供货商名称
 * @property string|null $sup_desc 备注
 * @property string|null $sup_man 联系人
 * @property string|null $sup_phone 联系电话
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupMan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSupplier whereSupStoreName($value)
 * @mixin \Eloquent
 */
class ShopncStoreSupplier extends BaseModel
{
    protected $table = 'shopnc_store_supplier';

    protected $primaryKey = 'sup_id';

	public $timestamps = false;

    protected $fillable = [
        'sup_store_id',
        'sup_store_name',
        'sup_name',
        'sup_desc',
        'sup_man',
        'sup_phone'
    ];

    protected $guarded = [];

        
}