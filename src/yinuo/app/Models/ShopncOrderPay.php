<?php

namespace App\Models;



/**
 * Class ShopncOrderPay
 *
 * @property int $pay_id
 * @property int $pay_sn 支付单号
 * @property int $buyer_id 买家ID
 * @property string|null $api_pay_state 0默认未支付1已支付(只有第三方支付接口通知到时才会更改此状态)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPay whereApiPayState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPay whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPay wherePayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPay wherePaySn($value)
 * @mixin \Eloquent
 */
class ShopncOrderPay extends BaseModel
{
    protected $table = 'shopnc_order_pay';

    protected $primaryKey = 'pay_id';

	public $timestamps = false;

    protected $fillable = [
        'pay_sn',
        'buyer_id',
        'api_pay_state'
    ];

    protected $guarded = [];

        
}