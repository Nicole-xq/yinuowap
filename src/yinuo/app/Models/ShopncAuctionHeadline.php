<?php

namespace App\Models;



/**
 * Class ShopncAuctionHeadline
 *
 * @property int $au_head_id 拍卖头条ID
 * @property string $au_head_title 拍卖头条标题
 * @property string $au_head_url 拍卖头条链接
 * @property int $au_head_sort 排序
 * @property int $is_show 是否显示 0否 1是
 * @property int $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereAuHeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereAuHeadSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereAuHeadTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereAuHeadUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionHeadline whereIsShow($value)
 * @mixin \Eloquent
 */
class ShopncAuctionHeadline extends BaseModel
{
    protected $table = 'shopnc_auction_headline';

    protected $primaryKey = 'au_head_id';

	public $timestamps = false;

    protected $fillable = [
        'au_head_title',
        'au_head_url',
        'au_head_sort',
        'is_show',
        'add_time'
    ];

    protected $guarded = [];

        
}