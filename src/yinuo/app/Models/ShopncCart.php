<?php

namespace App\Models;



/**
 * Class ShopncCart
 *
 * @property int $cart_id 购物车id
 * @property int $buyer_id 买家id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property float $goods_price 商品价格
 * @property int $goods_num 购买商品数量
 * @property string $goods_image 商品图片
 * @property int $bl_id 组合套装ID
 * @property int $gs_id 趣猜id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereBlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereGsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCart whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncCart extends BaseModel
{
    protected $table = 'shopnc_cart';

    protected $primaryKey = 'cart_id';

	public $timestamps = false;

    protected $fillable = [
        'buyer_id',
        'store_id',
        'store_name',
        'goods_id',
        'goods_name',
        'goods_price',
        'goods_num',
        'goods_image',
        'bl_id',
        'gs_id'
    ];

    protected $guarded = [];

        
}