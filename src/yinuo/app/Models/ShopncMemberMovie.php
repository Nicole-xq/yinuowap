<?php

namespace App\Models;



/**
 * Class ShopncMemberMovie
 *
 * @property int $movie_id
 * @property string $true_name 真实姓名
 * @property string $card_number 身份证号码
 * @property string $card_before_image 身份证正面照
 * @property string $card_behind_image 身份证反面照
 * @property int $movie_verify 审核（0 待审核 1 通过 2 未通过）
 * @property string|null $verify_reason 未通过原因
 * @property int $add_time 申请时间
 * @property int $verify_time 审核时间
 * @property int $member_id 会员ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereCardBeforeImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereCardBehindImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereMovieId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereMovieVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereTrueName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereVerifyReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovie whereVerifyTime($value)
 * @mixin \Eloquent
 */
class ShopncMemberMovie extends BaseModel
{
    protected $table = 'shopnc_member_movie';

    protected $primaryKey = 'movie_id';

	public $timestamps = false;

    protected $fillable = [
        'true_name',
        'card_number',
        'card_before_image',
        'card_behind_image',
        'movie_verify',
        'verify_reason',
        'add_time',
        'verify_time',
        'member_id'
    ];

    protected $guarded = [];

        
}