<?php

namespace App\Models;



/**
 * Class ShopncStoreSnsComment
 *
 * @property int $scomm_id 店铺动态评论id
 * @property int $strace_id 店铺动态id
 * @property string|null $scomm_content 评论内容
 * @property int|null $scomm_memberid 会员id
 * @property string|null $scomm_membername 会员名称
 * @property string|null $scomm_memberavatar 会员头像
 * @property string|null $scomm_time 评论时间
 * @property int $scomm_state 评论状态 1正常，0屏蔽
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommMemberavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereScommTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsComment whereStraceId($value)
 * @mixin \Eloquent
 */
class ShopncStoreSnsComment extends BaseModel
{
    protected $table = 'shopnc_store_sns_comment';

    protected $primaryKey = 'scomm_id';

	public $timestamps = false;

    protected $fillable = [
        'strace_id',
        'scomm_content',
        'scomm_memberid',
        'scomm_membername',
        'scomm_memberavatar',
        'scomm_time',
        'scomm_state'
    ];

    protected $guarded = [];

        
}