<?php

namespace App\Models;



/**
 * Class ShopncAuctionsArtist
 *
 * @property int $auctions_artist_id ID
 * @property string|null $auctions_artist_name 名字
 * @property string|null $auctions_artist_img 头像
 * @property string|null $auction_artist_summary 简介
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsArtist whereAuctionArtistSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsArtist whereAuctionsArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsArtist whereAuctionsArtistImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsArtist whereAuctionsArtistName($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsArtist extends BaseModel
{
    protected $table = 'shopnc_auctions_artist';

    protected $primaryKey = 'auctions_artist_id';

	public $timestamps = false;

    protected $fillable = [
        'auctions_artist_name',
        'auctions_artist_img',
        'auction_artist_summary'
    ];

    protected $guarded = [];

        
}