<?php

namespace App\Models;



/**
 * Class ShopncStoreDecorationBlock
 *
 * @property int $block_id 装修块编号
 * @property int $decoration_id 装修编号
 * @property int $store_id 店铺编号
 * @property string $block_layout 块布局
 * @property string|null $block_content 块内容
 * @property string|null $block_module_type 装修块模块类型
 * @property int|null $block_full_width 是否100%宽度(0-否 1-是)
 * @property int $block_sort 块排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockFullWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockLayout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockModuleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereBlockSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereDecorationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecorationBlock whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreDecorationBlock extends BaseModel
{
    protected $table = 'shopnc_store_decoration_block';

    protected $primaryKey = 'block_id';

	public $timestamps = false;

    protected $fillable = [
        'decoration_id',
        'store_id',
        'block_layout',
        'block_content',
        'block_module_type',
        'block_full_width',
        'block_sort'
    ];

    protected $guarded = [];

        
}