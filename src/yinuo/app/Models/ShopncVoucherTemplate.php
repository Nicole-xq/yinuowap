<?php

namespace App\Models;



/**
 * Class ShopncVoucherTemplate
 *
 * @property int $voucher_t_id 代金券模版编号
 * @property string $voucher_t_title 代金券模版名称
 * @property string $voucher_t_desc 代金券模版描述
 * @property int $voucher_t_start_date 代金券模版有效期开始时间
 * @property int $voucher_t_end_date 代金券模版有效期结束时间
 * @property int $voucher_t_price 代金券模版面额
 * @property float $voucher_t_limit 代金券使用时的订单限额
 * @property int $voucher_t_store_id 代金券模版的店铺id
 * @property string|null $voucher_t_storename 店铺名称
 * @property int $voucher_t_sc_id 所属店铺分类ID
 * @property int $voucher_t_creator_id 代金券模版的创建者id
 * @property int $voucher_t_state 代金券模版状态(1-有效,2-失效)
 * @property int $voucher_t_total 模版可发放的代金券总数
 * @property int $voucher_t_giveout 模版已发放的代金券数量
 * @property int $voucher_t_used 模版已经使用过的代金券
 * @property int $voucher_t_add_date 模版的创建时间
 * @property int $voucher_t_quotaid 套餐编号
 * @property int $voucher_t_points 兑换所需积分
 * @property int $voucher_t_eachlimit 每人限领张数
 * @property string|null $voucher_t_styleimg 样式模版图片
 * @property string|null $voucher_t_customimg 自定义代金券模板图片
 * @property int $voucher_t_recommend 是否推荐 0不推荐 1推荐
 * @property int $voucher_t_gettype 领取方式 1积分兑换 2卡密兑换 3免费领取
 * @property int $voucher_t_isbuild 领取方式为卡密兑换是否已经生成下属代金券 0未生成 1已生成
 * @property int $voucher_t_mgradelimit 领取代金券限制的会员等级
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTAddDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTCustomimg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTEachlimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTGettype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTGiveout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTIsbuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTMgradelimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTQuotaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTStorename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTStyleimg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherTemplate whereVoucherTUsed($value)
 * @mixin \Eloquent
 */
class ShopncVoucherTemplate extends BaseModel
{
    protected $table = 'shopnc_voucher_template';

    protected $primaryKey = 'voucher_t_id';

	public $timestamps = false;

    protected $fillable = [
        'voucher_t_title',
        'voucher_t_desc',
        'voucher_t_start_date',
        'voucher_t_end_date',
        'voucher_t_price',
        'voucher_t_limit',
        'voucher_t_store_id',
        'voucher_t_storename',
        'voucher_t_sc_id',
        'voucher_t_creator_id',
        'voucher_t_state',
        'voucher_t_total',
        'voucher_t_giveout',
        'voucher_t_used',
        'voucher_t_add_date',
        'voucher_t_quotaid',
        'voucher_t_points',
        'voucher_t_eachlimit',
        'voucher_t_styleimg',
        'voucher_t_customimg',
        'voucher_t_recommend',
        'voucher_t_gettype',
        'voucher_t_isbuild',
        'voucher_t_mgradelimit'
    ];

    protected $guarded = [];

        
}