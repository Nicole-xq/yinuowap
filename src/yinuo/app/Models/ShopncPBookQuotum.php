<?php

namespace App\Models;



/**
 * Class ShopncPBookQuotum
 *
 * @property int $bkq_id 预定套餐id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $bkq_starttime 套餐开始时间
 * @property int $bkq_endtime 套餐结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBookQuotum whereBkqEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBookQuotum whereBkqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBookQuotum whereBkqStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBookQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBookQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPBookQuotum extends BaseModel
{
    protected $table = 'shopnc_p_book_quota';

    protected $primaryKey = 'bkq_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'bkq_starttime',
        'bkq_endtime'
    ];

    protected $guarded = [];

        
}