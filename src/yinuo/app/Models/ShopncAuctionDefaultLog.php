<?php

namespace App\Models;



/**
 * Class ShopncAuctionDefaultLog
 *
 * @property int $id
 * @property int|null $member_id 用户id
 * @property string|null $order_sn 保证金订单号
 * @property string|null $amount 保证金总金额
 * @property int|null $auction_id 拍卖id
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionDefaultLog whereOrderSn($value)
 * @mixin \Eloquent
 */
class ShopncAuctionDefaultLog extends BaseModel
{
    protected $table = 'shopnc_auction_default_log';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'order_sn',
        'amount',
        'auction_id',
        'cdate'
    ];

    protected $guarded = [];

        
}