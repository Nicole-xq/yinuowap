<?php

namespace App\Models;



/**
 * Class ShopncSnsMembertag
 *
 * @property int $mtag_id 会员标签id
 * @property string $mtag_name 会员标签名称
 * @property int $mtag_sort 会员标签排序
 * @property int $mtag_recommend 标签推荐 0未推荐（默认），1为已推荐
 * @property string|null $mtag_desc 标签描述
 * @property string|null $mtag_img 标签图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMembertag whereMtagSort($value)
 * @mixin \Eloquent
 */
class ShopncSnsMembertag extends BaseModel
{
    protected $table = 'shopnc_sns_membertag';

    protected $primaryKey = 'mtag_id';

	public $timestamps = false;

    protected $fillable = [
        'mtag_name',
        'mtag_sort',
        'mtag_recommend',
        'mtag_desc',
        'mtag_img'
    ];

    protected $guarded = [];

        
}