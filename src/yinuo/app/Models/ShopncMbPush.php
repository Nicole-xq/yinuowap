<?php

namespace App\Models;



/**
 * Class ShopncMbPush
 *
 * @property int $log_id 记录ID
 * @property string $log_msg 推送内容
 * @property int $log_type 推送类型:1为关键字,2为专题编号,3为商品编号,默认为1
 * @property string|null $log_type_v 推送类型值
 * @property string $msg_tag 标签组,默认为default
 * @property string|null $msg_id Android消息ID
 * @property string|null $msg_ios_id iOS消息ID
 * @property int $ios_status iOS应用状态:1为开发,2为生产
 * @property int $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereIosStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereLogMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereLogType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereLogTypeV($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereMsgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereMsgIosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPush whereMsgTag($value)
 * @mixin \Eloquent
 */
class ShopncMbPush extends BaseModel
{
    protected $table = 'shopnc_mb_push';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'log_msg',
        'log_type',
        'log_type_v',
        'msg_tag',
        'msg_id',
        'msg_ios_id',
        'ios_status',
        'add_time'
    ];

    protected $guarded = [];

        
}