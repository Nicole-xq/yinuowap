<?php

namespace App\Models;



/**
 * Class ShopncDisTradCash
 *
 * @property int $tradc_id 自增编号
 * @property int $tradc_sn 记录唯一标示
 * @property int $tradc_member_id 会员编号
 * @property string $tradc_member_name 会员名称
 * @property float $tradc_amount 金额
 * @property string $tradc_bank_name 收款银行
 * @property string|null $tradc_bank_no 收款账号
 * @property string|null $tradc_bank_user 开户人姓名
 * @property int $tradc_add_time 添加时间
 * @property int|null $tradc_payment_time 付款时间
 * @property string $tradc_payment_state 提现支付状态 0默认1支付完成
 * @property string|null $tradc_payment_admin 支付管理员
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcBankNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcBankUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcPaymentAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcPaymentState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcPaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradCash whereTradcSn($value)
 * @mixin \Eloquent
 */
class ShopncDisTradCash extends BaseModel
{
    protected $table = 'shopnc_dis_trad_cash';

    protected $primaryKey = 'tradc_id';

	public $timestamps = false;

    protected $fillable = [
        'tradc_sn',
        'tradc_member_id',
        'tradc_member_name',
        'tradc_amount',
        'tradc_bank_name',
        'tradc_bank_no',
        'tradc_bank_user',
        'tradc_add_time',
        'tradc_payment_time',
        'tradc_payment_state',
        'tradc_payment_admin'
    ];

    protected $guarded = [];

        
}