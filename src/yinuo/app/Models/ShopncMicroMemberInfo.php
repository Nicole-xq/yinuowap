<?php

namespace App\Models;



/**
 * Class ShopncMicroMemberInfo
 *
 * @property int $member_id 用户编号
 * @property int $visit_count 个人中心访问计数
 * @property int $personal_count 已发布个人秀数量
 * @property int $goods_count 已发布随心看数量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroMemberInfo whereGoodsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroMemberInfo whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroMemberInfo wherePersonalCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroMemberInfo whereVisitCount($value)
 * @mixin \Eloquent
 */
class ShopncMicroMemberInfo extends BaseModel
{
    protected $table = 'shopnc_micro_member_info';

    protected $primaryKey = 'member_id';

	public $timestamps = false;

    protected $fillable = [
        'visit_count',
        'personal_count',
        'goods_count'
    ];

    protected $guarded = [];

        
}