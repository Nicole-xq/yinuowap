<?php

namespace App\Models;



/**
 * Class ShopncConsult
 *
 * @property int $consult_id 咨询编号
 * @property int|null $goods_id 商品编号
 * @property string $goods_name 商品名称
 * @property int $member_id 咨询发布者会员编号(0：游客)
 * @property string|null $member_name 会员名称
 * @property int $store_id 店铺编号
 * @property string $store_name 店铺名称
 * @property int $ct_id 咨询类型
 * @property string|null $consult_content 咨询内容
 * @property int|null $consult_addtime 咨询发布时间
 * @property string|null $consult_reply 咨询回复内容
 * @property int|null $consult_reply_time 咨询回复时间
 * @property int $isanonymous 0表示不匿名 1表示匿名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereConsultAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereConsultContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereConsultId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereConsultReply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereConsultReplyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereCtId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereIsanonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsult whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncConsult extends BaseModel
{
    protected $table = 'shopnc_consult';

    protected $primaryKey = 'consult_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_name',
        'member_id',
        'member_name',
        'store_id',
        'store_name',
        'ct_id',
        'consult_content',
        'consult_addtime',
        'consult_reply',
        'consult_reply_time',
        'isanonymous'
    ];

    protected $guarded = [];

        
}