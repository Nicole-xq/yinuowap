<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * Class ShopncRankingMember
 *
 * @property int $id 排行榜用户数据
 * @property int|null $period_num 期数数字
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property string|null $member_avatar 会员头像
 * @property int $ranking_num 名次
 * @property int $status 1有效 0无效
 * @property int $r_type 榜单类型1.拉新榜
 * @property string|null $start_date 开始时间
 * @property string|null $end_date 结束时间
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereMemberAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember wherePeriodNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereRType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereRankingNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRankingMember whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ShopncRankingMember extends BaseModel
{
    // 1有效 0无效
    const START_OK = 1;

    //榜单类型 1.拉新榜
    const R_TYPE_NEWBIE = 1;

    protected $table = 'shopnc_ranking_member';

    public $timestamps = true;

    protected $fillable = [
        'period_num',
        'member_id',
        'member_name',
        'member_avatar',
        'ranking_num',
        'status',
        'r_type',
        'start_date',
        'end_date'
    ];

    protected $guarded = [];

        
}