<?php

namespace App\Models;



/**
 * Class ShopncPCouQuotum
 *
 * @property int $id ID
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $tstart 开始时间
 * @property int $tend 结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouQuotum whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouQuotum whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouQuotum whereTend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouQuotum whereTstart($value)
 * @mixin \Eloquent
 */
class ShopncPCouQuotum extends BaseModel
{
    protected $table = 'shopnc_p_cou_quota';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'tstart',
        'tend'
    ];

    protected $guarded = [];

        
}