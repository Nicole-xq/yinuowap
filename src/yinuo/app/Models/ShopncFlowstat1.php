<?php

namespace App\Models;



/**
 * Class ShopncFlowstat1
 *
 * @property int $stattime 访问日期
 * @property int $clicknum 访问量
 * @property int $store_id 店铺ID
 * @property string $type 类型
 * @property int $goods_id 商品ID
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 whereClicknum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 whereStattime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 whereType($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat1 where#alibabaRdsRowId#($value)
 */
class ShopncFlowstat1 extends BaseModel
{
    protected $table = 'shopnc_flowstat_1';

    public $timestamps = false;

    protected $fillable = [
        'stattime',
        'clicknum',
        'store_id',
        'type',
        'goods_id',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}