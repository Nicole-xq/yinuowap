<?php

namespace App\Models;



/**
 * Class ShopncDeliveryOrder
 *
 * @property int $order_id 订单ID
 * @property int|null $addtime 订单生成时间
 * @property int|null $order_sn 订单号
 * @property int|null $dlyp_id 自提点ID
 * @property string|null $shipping_code 物流单号
 * @property string|null $express_code 快递公司编码
 * @property string|null $express_name 快递公司名称
 * @property string|null $reciver_name 收货人
 * @property string|null $reciver_telphone 电话
 * @property string|null $reciver_mobphone 手机
 * @property int $dlyo_state 订单状态 10(默认)未到站，20已到站，30已提取
 * @property string|null $dlyo_pickup_code 提货码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereDlyoPickupCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereDlyoState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereDlypId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereExpressCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereExpressName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereReciverMobphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereReciverName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereReciverTelphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDeliveryOrder whereShippingCode($value)
 * @mixin \Eloquent
 */
class ShopncDeliveryOrder extends BaseModel
{
    protected $table = 'shopnc_delivery_order';

    protected $primaryKey = 'order_id';

	public $timestamps = false;

    protected $fillable = [
        'addtime',
        'order_sn',
        'dlyp_id',
        'shipping_code',
        'express_code',
        'express_name',
        'reciver_name',
        'reciver_telphone',
        'reciver_mobphone',
        'dlyo_state',
        'dlyo_pickup_code'
    ];

    protected $guarded = [];

        
}