<?php

namespace App\Models;



/**
 * Class ShopncStoreRecommendation
 *
 * @property int $id 推荐id
 * @property int|null $store_id 店铺ID
 * @property int|null $store_add_time 店铺添加时间
 * @property int $store_state 推荐状态 1开启 0关闭
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendation whereStoreAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendation whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendation whereStoreState($value)
 * @mixin \Eloquent
 */
class ShopncStoreRecommendation extends BaseModel
{
    protected $table = 'shopnc_store_recommendation';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_add_time',
        'store_state'
    ];

    protected $guarded = [];

        
}