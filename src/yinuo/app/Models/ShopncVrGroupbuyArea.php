<?php

namespace App\Models;



/**
 * Class ShopncVrGroupbuyArea
 *
 * @property int $area_id 区域id
 * @property string $area_name 域区名称
 * @property int $parent_area_id 域区id
 * @property int $add_time 添加时间
 * @property string $first_letter 首字母
 * @property string|null $area_number 区号
 * @property string|null $post 邮编
 * @property int $hot_city 0.否 1.是
 * @property int $area_num 数量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereAreaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereAreaNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereAreaNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereFirstLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereHotCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea whereParentAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyArea wherePost($value)
 * @mixin \Eloquent
 */
class ShopncVrGroupbuyArea extends BaseModel
{
    protected $table = 'shopnc_vr_groupbuy_area';

    protected $primaryKey = 'area_id';

	public $timestamps = false;

    protected $fillable = [
        'area_name',
        'parent_area_id',
        'add_time',
        'first_letter',
        'area_number',
        'post',
        'hot_city',
        'area_num'
    ];

    protected $guarded = [];

        
}