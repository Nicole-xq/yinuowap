<?php

namespace App\Models;



/**
 * Class ShopncVideoAlbumClass
 *
 * @property int $video_class_id 视频id
 * @property string $video_class_name 视频名称
 * @property int $store_id 所属店铺id
 * @property string $video_class_des 视频描述
 * @property int $video_class_sort 排序
 * @property int $upload_time 视频上传时间
 * @property int $is_default 是否为默认,1代表默认
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereUploadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereVideoClassDes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereVideoClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereVideoClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbumClass whereVideoClassSort($value)
 * @mixin \Eloquent
 */
class ShopncVideoAlbumClass extends BaseModel
{
    protected $table = 'shopnc_video_album_class';

    protected $primaryKey = 'video_class_id';

	public $timestamps = false;

    protected $fillable = [
        'video_class_name',
        'store_id',
        'video_class_des',
        'video_class_sort',
        'upload_time',
        'is_default'
    ];

    protected $guarded = [];

        
}