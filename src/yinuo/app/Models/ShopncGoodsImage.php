<?php

namespace App\Models;



/**
 * Class ShopncGoodsImage
 *
 * @property int $goods_image_id 商品图片id
 * @property int $goods_commonid 商品公共内容id
 * @property int $store_id 店铺id
 * @property int $color_id 颜色规格值id
 * @property string $goods_image 商品图片
 * @property int $goods_image_sort 排序
 * @property int $is_default 默认主题，1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereGoodsImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereGoodsImageSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsImage whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsImage extends BaseModel
{
    protected $table = 'shopnc_goods_images';

    protected $primaryKey = 'goods_image_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_commonid',
        'store_id',
        'color_id',
        'goods_image',
        'goods_image_sort',
        'is_default'
    ];

    protected $guarded = [];

        
}