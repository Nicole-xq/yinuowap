<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * App\Models\BaseModel
 *
 * @mixin \Eloquent
 */
class BaseModel extends EloquentModel {
    /**
     * Get the guarded attributes for the model.
     * @var array
     */
    protected $guarded = [];

    private static $instance;

    public static function instance()
    {
        if (null == self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * 以数组形式实现save
     * 为便捷的数据插入
     * @param array $attributes
     * @return BaseModel
     */
    public static function arrayToSave(array $attributes){
        $now = (new static());
        foreach ($attributes as $key => $value){
            $now ->setAttribute($key, $value);
        }
        if(!$now->save()){
            return false;
        }else{
            return $now;
        }
    }
    /**
     * 返回key全名带表名
     *
     * @param $keyNames
     * @return string|array
     */
    public function getKeyFullName($keyNames)
    {
        if(!is_array($keyNames)){
            $keyNames = explode(',', $keyNames);
        }
        if(count($keyNames) == 1){
            return $this->getTable().'.'.trim(reset($keyNames));
        }else{
            array_walk($keyNames, function (&$keyName){
                $keyName = $this->getTable().'.'.trim($keyName);
            });
            return $keyNames;
        }
    }
}