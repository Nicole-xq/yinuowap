<?php

namespace App\Models;



/**
 * Class ShopncMemberCommission
 *
 * @property int $log_id 记录编号
 * @property int|null $order_id 订单编号
 * @property int|null $order_goods_id 订单商品编号
 * @property int|null $goods_id 商品编号
 * @property string|null $goods_name 商品名称
 * @property string|null $goods_image 订单商品图片
 * @property float|null $goods_pay_amount 商品支付金额
 * @property int|null $from_member_id 佣金来源用户ID
 * @property float|null $commission_amount 返佣金额
 * @property float|null $dis_commis_rate 返佣比例
 * @property int|null $dis_member_id 返佣佣金收益人
 * @property int|null $add_time 添加时间
 * @property int|null $dis_commis_state 佣金结算状态 0未结 1已结
 * @property float|null $goods_commission 订单商品佣金
 * @property int|null $commission_type 返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息
 * @property float|null $pay_refund 退款金额
 * @property float|null $commission_refund 返佣退还佣金
 * @property float|null $goods_commission_refund 商品退还佣金
 * @property int|null $commission_time 返佣结算时间
 * @property string|null $from_member_name 返佣来源用户名
 * @property string|null $dis_member_name 返佣收益人用户名
 * @property string|null $order_sn 订单编号
 * @property int|null $top_lv 上级级别(1:上级,2:上级的上级..对应三级分销)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereCommissionAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereCommissionRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereCommissionTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereCommissionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereDisCommisState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereDisMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereDisMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereFromMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereFromMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsCommissionRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereGoodsPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereOrderGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission wherePayRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommission whereTopLv($value)
 * @mixin \Eloquent
 */
class ShopncMemberCommission extends BaseModel
{
    protected $table = 'shopnc_member_commission';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_goods_id',
        'goods_id',
        'goods_name',
        'goods_image',
        'goods_pay_amount',
        'from_member_id',
        'commission_amount',
        'dis_commis_rate',
        'dis_member_id',
        'add_time',
        'dis_commis_state',
        'goods_commission',
        'commission_type',
        'pay_refund',
        'commission_refund',
        'goods_commission_refund',
        'commission_time',
        'from_member_name',
        'dis_member_name',
        'order_sn',
        'top_lv'
    ];

    protected $guarded = [];

        
}