<?php

namespace App\Models;



/**
 * Class ShopncVrOrderCode
 *
 * @property int $rec_id 兑换码表索引id
 * @property int $order_id 虚拟订单id
 * @property int $store_id 店铺ID
 * @property int $buyer_id 买家ID
 * @property string $vr_code 兑换码
 * @property int $vr_state 使用状态 0:(默认)未使用1:已使用2:已过期
 * @property int|null $vr_usetime 使用时间
 * @property float $pay_price 实际支付金额(结算)
 * @property int|null $vr_indate 过期时间
 * @property int $commis_rate 佣金比例
 * @property int|null $refund_lock 退款锁定状态:0为正常,1为锁定,2为同意,默认为0
 * @property int $vr_invalid_refund 允许过期退款1是0否
 * @property string|null $qr_pic 二维码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode wherePayPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereQrPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereRefundLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereVrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereVrIndate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereVrInvalidRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereVrState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderCode whereVrUsetime($value)
 * @mixin \Eloquent
 */
class ShopncVrOrderCode extends BaseModel
{
    protected $table = 'shopnc_vr_order_code';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'store_id',
        'buyer_id',
        'vr_code',
        'vr_state',
        'vr_usetime',
        'pay_price',
        'vr_indate',
        'commis_rate',
        'refund_lock',
        'vr_invalid_refund',
        'qr_pic'
    ];

    protected $guarded = [];

        
}