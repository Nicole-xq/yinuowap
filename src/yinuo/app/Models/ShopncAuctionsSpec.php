<?php

namespace App\Models;



/**
 * Class ShopncAuctionsSpec
 *
 * @property int $auctions_spec_id 拍品规格ID
 * @property string|null $auctions_spec_name 拍品规格名称
 * @property int|null $auctions_class_id_lv_1 所属大分类ID
 * @property int|null $auctions_class_id_lv_2 所属二级分类
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpec whereAuctionsClassIdLv1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpec whereAuctionsClassIdLv2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpec whereAuctionsSpecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpec whereAuctionsSpecName($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsSpec extends BaseModel
{
    protected $table = 'shopnc_auctions_spec';

    protected $primaryKey = 'auctions_spec_id';

	public $timestamps = false;

    protected $fillable = [
        'auctions_spec_name',
        'auctions_class_id_lv_1',
        'auctions_class_id_lv_2'
    ];

    protected $guarded = [];

        
}