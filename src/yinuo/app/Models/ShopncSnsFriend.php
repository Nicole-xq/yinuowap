<?php

namespace App\Models;



/**
 * Class ShopncSnsFriend
 *
 * @property int $friend_id id值
 * @property int $friend_frommid 会员id
 * @property string|null $friend_frommname 会员名称
 * @property string|null $friend_frommavatar 会员头像
 * @property int $friend_tomid 朋友id
 * @property string $friend_tomname 好友会员名称
 * @property string|null $friend_tomavatar 朋友头像
 * @property int $friend_addtime 添加时间
 * @property int $friend_followstate 关注状态 1为单方关注 2为双方关注
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendFollowstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendFrommavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendFrommid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendFrommname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendTomavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendTomid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsFriend whereFriendTomname($value)
 * @mixin \Eloquent
 */
class ShopncSnsFriend extends BaseModel
{
    protected $table = 'shopnc_sns_friend';

    protected $primaryKey = 'friend_id';

	public $timestamps = false;

    protected $fillable = [
        'friend_frommid',
        'friend_frommname',
        'friend_frommavatar',
        'friend_tomid',
        'friend_tomname',
        'friend_tomavatar',
        'friend_addtime',
        'friend_followstate'
    ];

    protected $guarded = [];

        
}