<?php

namespace App\Models;



/**
 * Class ShopncGoodsAuction
 *
 * @property int $id
 * @property int|null $store_id 店铺id
 * @property string|null $store_name 店铺名称
 * @property int $goods_id sku商品ID
 * @property int $goods_common_id spu商品ID
 * @property string $auction_name 拍品名称
 * @property string $auction_image 拍卖首图
 * @property float $auction_start_price 起拍价
 * @property float $auction_increase_range 加价幅度
 * @property float $auction_reserve_price 保留价
 * @property float $auction_bond 保证金
 * @property int $cur_special_id 当前关联专场ID
 * @property int $last_special_id 最近关联专场ID
 * @property int $auction_sum 拍卖次数
 * @property int $auction_state 拍品状态（0：未审核；1：审核中；2：审核通过；3：审核失败）
 * @property int $del_flag 0:未删除；1：已删
 * @property int|null $artwork_id 作品id
 * @property int|null $artwork_category_id 作品分类
 * @property string|null $artist_name 艺术家名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereArtistName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereArtworkCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionBond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionIncreaseRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionReservePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionStartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereAuctionSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereCurSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereDelFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereGoodsCommonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereLastSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuction whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncGoodsAuction extends BaseModel
{
    protected $table = 'shopnc_goods_auctions';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'goods_id',
        'goods_common_id',
        'auction_name',
        'auction_image',
        'auction_start_price',
        'auction_increase_range',
        'auction_reserve_price',
        'auction_bond',
        'cur_special_id',
        'last_special_id',
        'auction_sum',
        'auction_state',
        'del_flag',
        'artwork_id',
        'artwork_category_id',
        'artist_name'
    ];

    protected $guarded = [];

        
}