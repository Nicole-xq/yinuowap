<?php

namespace App\Models;



/**
 * Class ShopncCmsArticleClass
 *
 * @property int $class_id 分类编号
 * @property string $class_name 分类名称
 * @property int $class_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleClass whereClassSort($value)
 * @mixin \Eloquent
 */
class ShopncCmsArticleClass extends BaseModel
{
    protected $table = 'shopnc_cms_article_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_sort'
    ];

    protected $guarded = [];

        
}