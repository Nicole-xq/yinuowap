<?php

namespace App\Models;



/**
 * Class ShopncSpecialNotice
 *
 * @property int $sn_id 通知ID
 * @property int $special_id 专场ID
 * @property string $special_name 专场名称
 * @property int $member_id 会员ID
 * @property int $sn_add_time 添加时间
 * @property int $sn_remind_time 提醒时间
 * @property string $sn_email 邮箱
 * @property string $sn_mobile 手机号
 * @property int $sn_type 状态 1开始 2结束
 * @property int|null $is_send 是否发送过 0未发送 1 已发送
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereIsSend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnRemindTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSnType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialNotice whereSpecialName($value)
 * @mixin \Eloquent
 */
class ShopncSpecialNotice extends BaseModel
{
    protected $table = 'shopnc_special_notice';

    protected $primaryKey = 'sn_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'special_name',
        'member_id',
        'sn_add_time',
        'sn_remind_time',
        'sn_email',
        'sn_mobile',
        'sn_type',
        'is_send'
    ];

    protected $guarded = [];

        
}