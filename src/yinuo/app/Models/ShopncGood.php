<?php

namespace App\Models;



/**
 * Class ShopncGood
 *
 * @property int $goods_id 商品id(SKU)
 * @property int $goods_commonid 商品公共表id
 * @property int|null $sales_model 销售类型：1普通商品、2可议价商品、3新手专区商品
 * @property string $goods_name 商品名称（+规格名称）
 * @property string|null $goods_jingle 商品广告词
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $gc_id 商品分类id
 * @property int $gc_id_1 一级分类id
 * @property int $gc_id_2 二级分类id
 * @property int $gc_id_3 三级分类id
 * @property int|null $brand_id 品牌id
 * @property float $goods_price 商品价格
 * @property float $goods_promotion_price 商品促销价格
 * @property int $goods_promotion_type 促销类型 0无促销，1团购，2限时折扣
 * @property float $goods_marketprice 市场价
 * @property string|null $goods_serial 商品货号
 * @property int $goods_storage_alarm 库存报警值
 * @property string|null $goods_barcode 商品条形码
 * @property int $goods_click 商品点击数量
 * @property int $goods_salenum 销售数量
 * @property int $goods_collect 收藏数量
 * @property string $spec_name 规格名称
 * @property string $goods_spec 商品规格序列化
 * @property int $goods_storage 商品库存
 * @property string $goods_image 商品主图
 * @property string $goods_body 商品描述
 * @property string $mobile_body 手机端商品描述
 * @property int $goods_state 商品状态 0下架，1正常，10违规（禁售）
 * @property int $goods_verify 商品审核 1通过，0未通过，10审核中
 * @property int $goods_addtime 商品添加时间
 * @property int $goods_edittime 商品编辑时间
 * @property int $areaid_1 一级地区id
 * @property int $areaid_2 二级地区id
 * @property int $color_id 颜色规格id
 * @property int $transport_id 运费模板id
 * @property float $goods_freight 运费 0为免运费
 * @property int $goods_vat 是否开具增值税发票 1是，0否
 * @property int $goods_commend 商品推荐 1是，0否 默认0
 * @property string|null $goods_stcids 店铺分类id 首尾用,隔开
 * @property int $evaluation_good_star 好评星级
 * @property int $evaluation_count 评价数
 * @property int $is_virtual 是否为虚拟商品 1是，0否
 * @property int $virtual_indate 虚拟商品有效期
 * @property int $virtual_limit 虚拟商品购买上限
 * @property int $virtual_invalid_refund 是否允许过期退款， 1是，0否
 * @property int $is_fcode 是否为F码商品 1是，0否
 * @property int $is_presell 是否是预售商品 1是，0否
 * @property int $presell_deliverdate 预售商品发货时间
 * @property int $is_book 是否为预定商品，1是，0否
 * @property float $book_down_payment 定金金额
 * @property float $book_final_payment 尾款金额
 * @property int $book_down_time 预定结束时间
 * @property int|null $book_buyers 预定人数
 * @property int $have_gift 是否拥有赠品
 * @property int $is_own_shop 是否为平台自营
 * @property int $contract_1 消费者保障服务状态 0关闭 1开启
 * @property int $contract_2 消费者保障服务状态 0关闭 1开启
 * @property int $contract_3 消费者保障服务状态 0关闭 1开启
 * @property int $contract_4 消费者保障服务状态 0关闭 1开启
 * @property int $contract_5 消费者保障服务状态 0关闭 1开启
 * @property int $contract_6 消费者保障服务状态 0关闭 1开启
 * @property int $contract_7 消费者保障服务状态 0关闭 1开启
 * @property int $contract_8 消费者保障服务状态 0关闭 1开启
 * @property int $contract_9 消费者保障服务状态 0关闭 1开启
 * @property int $contract_10 消费者保障服务状态 0关闭 1开启
 * @property int $is_chain 是否为门店商品 1是，0否
 * @property float $goods_trans_v 重量或体积
 * @property int $is_dis 是否分销
 * @property int $is_special 是否特殊商品(下单后不可退款退货) 0 不是 1 是
 * @property float|null $commis_level_1 分销一级返佣比例
 * @property float|null $commis_level_2 分销二级返佣比例
 * @property string|null $goods_jingle_pc_link 商品广告词PC链接
 * @property string|null $goods_jingle_wap_link 商品广告词WAP链接
 * @property int $is_add_auctions 是否添加到拍卖池0：未；1：已
 * @property int|null $sort 商品排序
 * @property int|null $artist_sort 艺术家首页排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereAreaid1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereAreaid2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereArtistSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereBookBuyers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereBookDownPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereBookDownTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereBookFinalPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereCommisLevel1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereCommisLevel2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereContract9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereEvaluationCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereEvaluationGoodStar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsCollect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsEdittime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsFreight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsJingle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsJinglePcLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsJingleWapLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsMarketprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsPromotionPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsPromotionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsSalenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsStcids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsStorageAlarm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsTransV($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsVat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereGoodsVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereHaveGift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsAddAuctions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsBook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsChain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsDis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsFcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsOwnShop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsPresell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsSpecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereIsVirtual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereMobileBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood wherePresellDeliverdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereSalesModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereSpecName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereTransportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereVirtualIndate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereVirtualInvalidRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereVirtualLimit($value)
 * @mixin \Eloquent
 * @property int $artist_id 艺术家售卖作品关联的艺术家ID
 * @property int $artwork_id 艺术家售卖作品,作品ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereArtworkId($value)
 * @property int|null $artwork_category_id 艺术家作品分类
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGood whereArtworkCategoryId($value)
 */
class ShopncGood extends BaseModel
{
    protected $table = 'shopnc_goods';

    protected $primaryKey = 'goods_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_commonid',
        'sales_model',
        'goods_name',
        'goods_jingle',
        'store_id',
        'store_name',
        'gc_id',
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        'brand_id',
        'goods_price',
        'goods_promotion_price',
        'goods_promotion_type',
        'goods_marketprice',
        'goods_serial',
        'goods_storage_alarm',
        'goods_barcode',
        'goods_click',
        'goods_salenum',
        'goods_collect',
        'spec_name',
        'goods_spec',
        'goods_storage',
        'goods_image',
        'goods_body',
        'mobile_body',
        'goods_state',
        'goods_verify',
        'goods_addtime',
        'goods_edittime',
        'areaid_1',
        'areaid_2',
        'color_id',
        'transport_id',
        'goods_freight',
        'goods_vat',
        'goods_commend',
        'goods_stcids',
        'evaluation_good_star',
        'evaluation_count',
        'is_virtual',
        'virtual_indate',
        'virtual_limit',
        'virtual_invalid_refund',
        'is_fcode',
        'is_presell',
        'presell_deliverdate',
        'is_book',
        'book_down_payment',
        'book_final_payment',
        'book_down_time',
        'book_buyers',
        'have_gift',
        'is_own_shop',
        'contract_1',
        'contract_2',
        'contract_3',
        'contract_4',
        'contract_5',
        'contract_6',
        'contract_7',
        'contract_8',
        'contract_9',
        'contract_10',
        'is_chain',
        'goods_trans_v',
        'is_dis',
        'is_special',
        'commis_level_1',
        'commis_level_2',
        'goods_jingle_pc_link',
        'goods_jingle_wap_link',
        'is_add_auctions',
        'sort',
        'artist_sort'
    ];

    protected $guarded = [];

        
}