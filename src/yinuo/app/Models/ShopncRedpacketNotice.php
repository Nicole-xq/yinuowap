<?php

namespace App\Models;



/**
 * Class ShopncRedpacketNotice
 *
 * @property int $id
 * @property int|null $member_id 用户id
 * @property int|null $redpacket_id 用户红包id
 * @property int|null $status 状态 0:未读,1:已读
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketNotice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketNotice whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketNotice whereRedpacketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketNotice whereStatus($value)
 * @mixin \Eloquent
 */
class ShopncRedpacketNotice extends BaseModel
{
    protected $table = 'shopnc_redpacket_notice';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'redpacket_id',
        'status'
    ];

    protected $guarded = [];

        
}