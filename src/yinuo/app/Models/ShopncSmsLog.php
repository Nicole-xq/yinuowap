<?php

namespace App\Models;



/**
 * Class ShopncSmsLog
 *
 * @property int $log_id 记录ID
 * @property string $log_phone 手机号
 * @property string $log_captcha 短信动态码
 * @property string $log_ip 请求IP
 * @property string $log_msg 短信内容
 * @property int $log_type 短信类型:1为注册,2为登录,3为找回密码,默认为1
 * @property int $add_time 添加时间
 * @property int|null $member_id 会员ID,注册为0
 * @property string|null $member_name 会员名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogCaptcha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereLogType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSmsLog whereMemberName($value)
 * @mixin \Eloquent
 */
class ShopncSmsLog extends BaseModel
{
    protected $table = 'shopnc_sms_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'log_phone',
        'log_captcha',
        'log_ip',
        'log_msg',
        'log_type',
        'add_time',
        'member_id',
        'member_name'
    ];

    protected $guarded = [];

        
}