<?php

namespace App\Models;



/**
 * Class ShopncOrderBill
 *
 * @property int $ob_id 主键ID作为新结算单编号
 * @property int|null $ob_no 结算单编号(年月店铺ID)
 * @property int $ob_start_date 开始日期
 * @property int $ob_end_date 结束日期
 * @property float $ob_order_totals 订单金额
 * @property float $ob_shipping_totals 运费
 * @property float $ob_order_return_totals 退单金额
 * @property float $ob_commis_totals 佣金金额
 * @property float $ob_commis_return_totals 退还佣金
 * @property float $ob_store_cost_totals 店铺促销活动费用
 * @property float $ob_result_totals 应结金额
 * @property int|null $ob_create_date 生成结算单日期
 * @property int|null $os_month 出账单应结时间,ob_end_date+1所在月(年月份)
 * @property string|null $ob_state 1默认2店家已确认3平台已审核4结算完成
 * @property int|null $ob_pay_date 付款日期
 * @property string|null $ob_pay_content 支付备注
 * @property int $ob_store_id 店铺ID
 * @property string|null $ob_store_name 店铺名
 * @property float $ob_order_book_totals 被关闭的预定订单的实收总金额
 * @property float $ob_rpt_amount 下单时使用的红包值
 * @property float $ob_rf_rpt_amount 全部退款时红包值
 * @property float|null $ob_dis_pay_amount 分销佣金
 * @property float|null $ob_points_amount 诺币抵现
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObCommisReturnTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObCommisTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObDisPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObOrderBookTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObOrderReturnTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObOrderTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObPayContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObPayDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObPointsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObResultTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObRfRptAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObRptAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObShippingTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObStoreCostTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereObStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderBill whereOsMonth($value)
 * @mixin \Eloquent
 */
class ShopncOrderBill extends BaseModel
{
    protected $table = 'shopnc_order_bill';

    protected $primaryKey = 'ob_id';

	public $timestamps = false;

    protected $fillable = [
        'ob_no',
        'ob_start_date',
        'ob_end_date',
        'ob_order_totals',
        'ob_shipping_totals',
        'ob_order_return_totals',
        'ob_commis_totals',
        'ob_commis_return_totals',
        'ob_store_cost_totals',
        'ob_result_totals',
        'ob_create_date',
        'os_month',
        'ob_state',
        'ob_pay_date',
        'ob_pay_content',
        'ob_store_id',
        'ob_store_name',
        'ob_order_book_totals',
        'ob_rpt_amount',
        'ob_rf_rpt_amount',
        'ob_dis_pay_amount',
        'ob_points_amount'
    ];

    protected $guarded = [];

        
}