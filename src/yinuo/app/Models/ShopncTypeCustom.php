<?php

namespace App\Models;



/**
 * Class ShopncTypeCustom
 *
 * @property int $custom_id 自定义属性id
 * @property string $custom_name 自定义属性名称
 * @property int $type_id 类型id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeCustom whereCustomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeCustom whereCustomName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeCustom whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncTypeCustom extends BaseModel
{
    protected $table = 'shopnc_type_custom';

    protected $primaryKey = 'custom_id';

	public $timestamps = false;

    protected $fillable = [
        'custom_name',
        'type_id'
    ];

    protected $guarded = [];

        
}