<?php

namespace App\Models;



/**
 * Class ShopncOrderPintuan
 *
 * @property int $log_id 记录ID
 * @property int $order_id 订单ID
 * @property string $order_sn 订单编号
 * @property int $buyer_type 参团类型:0为团长,其它为参团
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家用户名
 * @property int $goods_id 商品ID
 * @property int $goods_commonid 商品公共表ID
 * @property int $min_num 参团人数
 * @property int $add_time 添加时间
 * @property int|null $pay_time 支付时间
 * @property int|null $end_time 参团结束时间
 * @property int|null $cancel_time 订单被取消时间
 * @property int|null $store_id 商家ID
 * @property int|null $lock_state 锁定状态:0是正常,1是锁定,默认是1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereBuyerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereCancelTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereLockState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereMinNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderPintuan whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncOrderPintuan extends BaseModel
{
    protected $table = 'shopnc_order_pintuan';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_sn',
        'buyer_type',
        'buyer_id',
        'buyer_name',
        'goods_id',
        'goods_commonid',
        'min_num',
        'add_time',
        'pay_time',
        'end_time',
        'cancel_time',
        'store_id',
        'lock_state'
    ];

    protected $guarded = [];

        
}