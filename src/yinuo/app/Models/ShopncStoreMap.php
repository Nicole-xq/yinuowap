<?php

namespace App\Models;



/**
 * Class ShopncStoreMap
 *
 * @property int $map_id 地图ID
 * @property int $store_id 店铺ID
 * @property int $sc_id 店铺分类ID
 * @property string $store_name 店铺名称
 * @property string $name_info 详细名称
 * @property string $address_info 详细地址
 * @property string|null $phone_info 电话信息
 * @property string|null $bus_info 公交信息
 * @property int $update_time 更新时间
 * @property float $baidu_lng 百度经度
 * @property float $baidu_lat 百度纬度
 * @property string $baidu_province 百度省份
 * @property string $baidu_city 百度城市
 * @property string $baidu_district 百度区县
 * @property string|null $baidu_street 百度街道
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereAddressInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBaiduStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereBusInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereMapId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereNameInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap wherePhoneInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMap whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncStoreMap extends BaseModel
{
    protected $table = 'shopnc_store_map';

    protected $primaryKey = 'map_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'sc_id',
        'store_name',
        'name_info',
        'address_info',
        'phone_info',
        'bus_info',
        'update_time',
        'baidu_lng',
        'baidu_lat',
        'baidu_province',
        'baidu_city',
        'baidu_district',
        'baidu_street'
    ];

    protected $guarded = [];

        
}