<?php

namespace App\Models;



/**
 * Class ShopncRedpacket
 *
 * @property int $rpacket_id 红包编号
 * @property string $rpacket_code 红包编码
 * @property int $rpacket_t_id 红包模版编号
 * @property string $rpacket_title 红包标题
 * @property string $rpacket_desc 红包描述
 * @property int $rpacket_start_date 红包有效期开始时间
 * @property int $rpacket_end_date 红包有效期结束时间
 * @property int $rpacket_price 红包面额
 * @property float $rpacket_limit 红包使用时的订单限额
 * @property int $rpacket_state 红包状态(1-未用,2-已用,3-过期)
 * @property int $rpacket_active_date 红包发放日期
 * @property int $rpacket_owner_id 红包所有者id
 * @property string|null $rpacket_owner_name 红包所有者名称
 * @property int|null $rpacket_order_id 使用该红包的订单支付单号
 * @property string|null $rpacket_pwd 红包卡密
 * @property string|null $rpacket_pwd2 红包卡密2
 * @property string|null $rpacket_customimg 红包自定义图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketActiveDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketCustomimg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketPwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketPwd2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacket whereRpacketTitle($value)
 * @mixin \Eloquent
 */
class ShopncRedpacket extends BaseModel
{
    //红包状态  1-未用,2-已用,3-过期, 4锁定
    const RPACKET_STATE_UNUSED = 1;
    const RPACKET_STATE_USED = 2;
    const RPACKET_STATE_EXPIRE = 3;
    const RPACKET_STATE_LOCK = 4;

    protected $table = 'shopnc_redpacket';

    protected $primaryKey = 'rpacket_id';

	public $timestamps = false;
    
    protected $fillable = [
        'rpacket_code',
        'rpacket_t_id',
        'rpacket_title',
        'rpacket_desc',
        'rpacket_start_date',
        'rpacket_end_date',
        'rpacket_price',
        'rpacket_limit',
        'rpacket_state',
        'rpacket_active_date',
        'rpacket_owner_id',
        'rpacket_owner_name',
        'rpacket_order_id',
        'rpacket_pwd',
        'rpacket_pwd2',
        'rpacket_customimg'
    ];

    protected $guarded = [];

        
}