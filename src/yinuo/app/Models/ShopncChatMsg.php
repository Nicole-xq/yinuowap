<?php

namespace App\Models;



/**
 * Class ShopncChatMsg
 *
 * @property int $m_id 记录ID
 * @property int $f_id 会员ID
 * @property string $f_name 会员名
 * @property string $f_ip 发自IP
 * @property int $t_id 接收会员ID
 * @property string $t_name 接收会员名
 * @property string|null $t_msg 消息内容
 * @property int|null $r_state 状态:1为已读,2为未读,默认为2
 * @property int|null $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereFId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereFIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereFName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereRState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereTMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatMsg whereTName($value)
 * @mixin \Eloquent
 */
class ShopncChatMsg extends BaseModel
{
    protected $table = 'shopnc_chat_msg';

    protected $primaryKey = 'm_id';

	public $timestamps = false;

    protected $fillable = [
        'f_id',
        'f_name',
        'f_ip',
        't_id',
        't_name',
        't_msg',
        'r_state',
        'add_time'
    ];

    protected $guarded = [];

        
}