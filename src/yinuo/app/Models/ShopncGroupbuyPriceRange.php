<?php

namespace App\Models;



/**
 * Class ShopncGroupbuyPriceRange
 *
 * @property int $range_id 价格区间编号
 * @property string $range_name 区间名称
 * @property int $range_start 区间下限
 * @property int $range_end 区间上限
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyPriceRange whereRangeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyPriceRange whereRangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyPriceRange whereRangeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyPriceRange whereRangeStart($value)
 * @mixin \Eloquent
 */
class ShopncGroupbuyPriceRange extends BaseModel
{
    protected $table = 'shopnc_groupbuy_price_range';

    protected $primaryKey = 'range_id';

	public $timestamps = false;

    protected $fillable = [
        'range_name',
        'range_start',
        'range_end'
    ];

    protected $guarded = [];

        
}