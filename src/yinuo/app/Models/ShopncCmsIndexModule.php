<?php

namespace App\Models;



/**
 * Class ShopncCmsIndexModule
 *
 * @property int $module_id 模块编号
 * @property string|null $module_title 模块标题
 * @property string $module_name 模板名称
 * @property string|null $module_type 模块类型，index-固定内容、article1-文章模块1、article2-文章模块2、micro-微商城、adv-通栏广告
 * @property int|null $module_sort 排序
 * @property int|null $module_state 状态1-显示、0-不显示
 * @property string|null $module_content 模块内容
 * @property string $module_style 模块主题
 * @property int $module_view 后台列表显示样式 1-展开 2-折叠
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsIndexModule whereModuleView($value)
 * @mixin \Eloquent
 */
class ShopncCmsIndexModule extends BaseModel
{
    protected $table = 'shopnc_cms_index_module';

    protected $primaryKey = 'module_id';

	public $timestamps = false;

    protected $fillable = [
        'module_title',
        'module_name',
        'module_type',
        'module_sort',
        'module_state',
        'module_content',
        'module_style',
        'module_view'
    ];

    protected $guarded = [];

        
}