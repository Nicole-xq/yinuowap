<?php

namespace App\Models;



/**
 * Class ShopncPSoleQuotum
 *
 * @property int $sole_quota_id 套餐id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $sole_quota_starttime 开始时间
 * @property int $sole_quota_endtime 结束时间
 * @property int $sole_state 套餐状态 1开启 0关闭 默认1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereSoleQuotaEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereSoleQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereSoleQuotaStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereSoleState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPSoleQuotum extends BaseModel
{
    protected $table = 'shopnc_p_sole_quota';

    protected $primaryKey = 'sole_quota_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'sole_quota_starttime',
        'sole_quota_endtime',
        'sole_state'
    ];

    protected $guarded = [];

        
}