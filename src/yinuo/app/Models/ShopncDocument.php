<?php

namespace App\Models;



/**
 * Class ShopncDocument
 *
 * @property int $doc_id id
 * @property string $doc_code 调用标识码
 * @property string $doc_title 标题
 * @property string $doc_content 内容
 * @property int $doc_time 添加时间/修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDocument whereDocCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDocument whereDocContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDocument whereDocId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDocument whereDocTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDocument whereDocTitle($value)
 * @mixin \Eloquent
 */
class ShopncDocument extends BaseModel
{
    protected $table = 'shopnc_document';

    protected $primaryKey = 'doc_id';

	public $timestamps = false;

    protected $fillable = [
        'doc_code',
        'doc_title',
        'doc_content',
        'doc_time'
    ];

    protected $guarded = [];

        
}