<?php

namespace App\Models;



/**
 * Class ShopncHelp
 *
 * @property int $help_id 帮助ID
 * @property int|null $help_sort 排序
 * @property string $help_title 标题
 * @property string|null $help_info 帮助内容
 * @property string|null $help_url 跳转链接
 * @property int $update_time 更新时间
 * @property int $type_id 帮助类型
 * @property int|null $page_show 页面类型:1为店铺,2为会员,默认为1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereHelpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereHelpInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereHelpSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereHelpTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereHelpUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp wherePageShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelp whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncHelp extends BaseModel
{
    protected $table = 'shopnc_help';

    protected $primaryKey = 'help_id';

	public $timestamps = false;

    protected $fillable = [
        'help_sort',
        'help_title',
        'help_info',
        'help_url',
        'update_time',
        'type_id',
        'page_show'
    ];

    protected $guarded = [];

        
}