<?php

namespace App\Models;



/**
 * Class ShopncGoodsAuctions20171116
 *
 * @property int $id
 * @property int|null $store_id 店铺id
 * @property string|null $store_name 店铺名称
 * @property int $goods_id sku商品ID
 * @property int $goods_common_id spu商品ID
 * @property string $auction_name 拍品名称
 * @property string $auction_image 拍卖首图
 * @property float $auction_start_price 起拍价
 * @property float $auction_increase_range 加价幅度
 * @property float $auction_reserve_price 保留价
 * @property float $auction_bond 保证金
 * @property int $cur_special_id 当前关联专场ID
 * @property int $last_special_id 最近关联专场ID
 * @property int $auction_sum 拍卖次数
 * @property int $auction_state 拍品状态（0：未审核；1：审核中；2：审核通过；3：审核失败）
 * @property int $del_flag 0:未删除；1：已删
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionBond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionIncreaseRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionReservePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionStartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereAuctionSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereCurSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereDelFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereGoodsCommonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereLastSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAuctions20171116 whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncGoodsAuctions20171116 extends BaseModel
{
    protected $table = 'shopnc_goods_auctions_20171116';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'goods_id',
        'goods_common_id',
        'auction_name',
        'auction_image',
        'auction_start_price',
        'auction_increase_range',
        'auction_reserve_price',
        'auction_bond',
        'cur_special_id',
        'last_special_id',
        'auction_sum',
        'auction_state',
        'del_flag'
    ];

    protected $guarded = [];

        
}