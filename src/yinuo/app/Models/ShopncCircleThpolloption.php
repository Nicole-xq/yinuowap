<?php

namespace App\Models;



/**
 * Class ShopncCircleThpolloption
 *
 * @property int $pollop_id 投票选项id
 * @property int $theme_id 话题id
 * @property string $pollop_option 投票选项
 * @property int $pollop_votes 得票数
 * @property int $pollop_sort 排序
 * @property string|null $pollop_votername 投票者名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption wherePollopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption wherePollopOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption wherePollopSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption wherePollopVotername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption wherePollopVotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpolloption whereThemeId($value)
 * @mixin \Eloquent
 */
class ShopncCircleThpolloption extends BaseModel
{
    protected $table = 'shopnc_circle_thpolloption';

    protected $primaryKey = 'pollop_id';

	public $timestamps = false;

    protected $fillable = [
        'theme_id',
        'pollop_option',
        'pollop_votes',
        'pollop_sort',
        'pollop_votername'
    ];

    protected $guarded = [];

        
}