<?php

namespace App\Models;



/**
 * Class ShopncMicroGoodsRelation
 *
 * @property int $relation_id 关系编号
 * @property int $class_id 微商城商品分类编号
 * @property int $shop_class_id 商城商品分类编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsRelation whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsRelation whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsRelation whereShopClassId($value)
 * @mixin \Eloquent
 */
class ShopncMicroGoodsRelation extends BaseModel
{
    protected $table = 'shopnc_micro_goods_relation';

    protected $primaryKey = 'relation_id';

	public $timestamps = false;

    protected $fillable = [
        'class_id',
        'shop_class_id'
    ];

    protected $guarded = [];

        
}