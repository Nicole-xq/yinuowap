<?php

namespace App\Models;



/**
 * Class ShopncInform
 *
 * @property int $inform_id 举报id
 * @property int $inform_member_id 举报人id
 * @property string $inform_member_name 举报人会员名
 * @property int $inform_goods_id 被举报的商品id
 * @property string $inform_goods_name 被举报的商品名称
 * @property int $inform_subject_id 举报主题id
 * @property string $inform_subject_content 举报主题
 * @property string $inform_content 举报信息
 * @property string|null $inform_pic1 图片1
 * @property string|null $inform_pic2 图片2
 * @property string|null $inform_pic3 图片3
 * @property int $inform_datetime 举报时间
 * @property int $inform_store_id 被举报商品的店铺id
 * @property int $inform_state 举报状态(1未处理/2已处理)
 * @property int $inform_handle_type 举报处理结果(1无效举报/2恶意举报/3有效举报)
 * @property string|null $inform_handle_message 举报处理信息
 * @property int|null $inform_handle_datetime 举报处理时间
 * @property int|null $inform_handle_member_id 管理员id
 * @property string|null $inform_goods_image 商品图
 * @property string|null $inform_store_name 店铺名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformHandleDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformHandleMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformHandleMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformHandleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformPic1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformPic2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformPic3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformSubjectContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInform whereInformSubjectId($value)
 * @mixin \Eloquent
 */
class ShopncInform extends BaseModel
{
    protected $table = 'shopnc_inform';

    protected $primaryKey = 'inform_id';

	public $timestamps = false;

    protected $fillable = [
        'inform_member_id',
        'inform_member_name',
        'inform_goods_id',
        'inform_goods_name',
        'inform_subject_id',
        'inform_subject_content',
        'inform_content',
        'inform_pic1',
        'inform_pic2',
        'inform_pic3',
        'inform_datetime',
        'inform_store_id',
        'inform_state',
        'inform_handle_type',
        'inform_handle_message',
        'inform_handle_datetime',
        'inform_handle_member_id',
        'inform_goods_image',
        'inform_store_name'
    ];

    protected $guarded = [];

        
}