<?php

namespace App\Models;



/**
 * Class ShopncTypeSpec
 *
 * @property int $type_id 类型id
 * @property int $sp_id 规格id
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec whereSpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec whereTypeId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeSpec where#alibabaRdsRowId#($value)
 */
class ShopncTypeSpec extends BaseModel
{
    protected $table = 'shopnc_type_spec';

    public $timestamps = false;

    protected $fillable = [
        'type_id',
        'sp_id',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}