<?php

namespace App\Models;



/**
 * Class ShopncSpecialRec
 *
 * @property int $special_rec_id 专场推荐ID
 * @property int $special_id 专场ID
 * @property string|null $special_rec_pic 专场推荐图片
 * @property string $rec_auction 专场推荐商品
 * @property int $rec_sort 推荐排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRec whereRecAuction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRec whereRecSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRec whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRec whereSpecialRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRec whereSpecialRecPic($value)
 * @mixin \Eloquent
 */
class ShopncSpecialRec extends BaseModel
{
    protected $table = 'shopnc_special_rec';

    protected $primaryKey = 'special_rec_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'special_rec_pic',
        'rec_auction',
        'rec_sort'
    ];

    protected $guarded = [];

        
}