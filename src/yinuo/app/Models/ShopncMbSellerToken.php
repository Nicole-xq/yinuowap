<?php

namespace App\Models;



/**
 * Class ShopncMbSellerToken
 *
 * @property int $token_id 令牌编号
 * @property int $seller_id 用户编号
 * @property string $seller_name 用户名
 * @property string $token 登录令牌
 * @property int $login_time 登录时间
 * @property string $client_type 客户端类型 windows
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereClientType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSellerToken whereTokenId($value)
 * @mixin \Eloquent
 */
class ShopncMbSellerToken extends BaseModel
{
    protected $table = 'shopnc_mb_seller_token';

    protected $primaryKey = 'token_id';

	public $timestamps = false;

    protected $fillable = [
        'seller_id',
        'seller_name',
        'token',
        'login_time',
        'client_type'
    ];

    protected $guarded = [];

        
}