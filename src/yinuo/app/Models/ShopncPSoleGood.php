<?php

namespace App\Models;



/**
 * Class ShopncPSoleGood
 *
 * @property int $sole_goods_id 手机专享商品id
 * @property int $store_id 店铺id
 * @property int $goods_id 商品id
 * @property float $sole_price 专享价格
 * @property int $gc_id 商品分类id
 * @property int $sole_state 套餐状态 1开启 0关闭 默认1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereSoleGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereSolePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereSoleState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPSoleGood whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncPSoleGood extends BaseModel
{
    protected $table = 'shopnc_p_sole_goods';

    protected $primaryKey = 'sole_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'goods_id',
        'sole_price',
        'gc_id',
        'sole_state'
    ];

    protected $guarded = [];

        
}