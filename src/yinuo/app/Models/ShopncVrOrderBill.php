<?php

namespace App\Models;



/**
 * Class ShopncVrOrderBill
 *
 * @property int $ob_id 主键作为结算单号
 * @property int|null $ob_no 结算单编号(年月店铺ID)
 * @property int $ob_start_date 开始日期
 * @property int $ob_end_date 结束日期
 * @property float $ob_order_totals 订单金额
 * @property float $ob_commis_totals 佣金金额
 * @property float $ob_result_totals 应结金额
 * @property int|null $ob_create_date 生成结算单日期
 * @property int|null $os_month 出账单应结时间,ob_end_date+1所在月(年月份)
 * @property string|null $ob_state 1默认2店家已确认3平台已审核4结算完成
 * @property int|null $ob_pay_date 付款日期
 * @property string|null $ob_pay_content 支付备注
 * @property int $ob_store_id 店铺ID
 * @property string|null $ob_store_name 店铺名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObCommisTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObOrderTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObPayContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObPayDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObResultTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereObStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderBill whereOsMonth($value)
 * @mixin \Eloquent
 */
class ShopncVrOrderBill extends BaseModel
{
    protected $table = 'shopnc_vr_order_bill';

    protected $primaryKey = 'ob_id';

	public $timestamps = false;

    protected $fillable = [
        'ob_no',
        'ob_start_date',
        'ob_end_date',
        'ob_order_totals',
        'ob_commis_totals',
        'ob_result_totals',
        'ob_create_date',
        'os_month',
        'ob_state',
        'ob_pay_date',
        'ob_pay_content',
        'ob_store_id',
        'ob_store_name'
    ];

    protected $guarded = [];

        
}