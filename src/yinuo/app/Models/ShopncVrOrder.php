<?php

namespace App\Models;



/**
 * Class ShopncVrOrder
 *
 * @property int $order_id 虚拟订单索引id
 * @property int $order_sn 订单编号
 * @property int $store_id 卖家店铺id
 * @property string $store_name 卖家店铺名称
 * @property int $buyer_id 买家id
 * @property string $buyer_name 买家登录名
 * @property string $buyer_phone 买家手机
 * @property int $add_time 订单生成时间
 * @property string|null $payment_code 支付方式名称代码
 * @property int|null $payment_time 支付(付款)时间
 * @property string|null $trade_no 第三方平台交易号
 * @property int|null $close_time 关闭时间
 * @property string|null $close_reason 关闭原因
 * @property int|null $finnshed_time 完成时间
 * @property float $order_amount 订单总价格(支付金额)
 * @property float|null $refund_amount 退款金额
 * @property float $rcb_amount 充值卡支付金额
 * @property float $pd_amount 预存款支付金额
 * @property int $order_state 订单状态：0(已取消)10(默认):未付款;20:已付款;40:已完成;
 * @property int|null $refund_state 退款状态:0是无退款,1是部分退款,2是全部退款
 * @property string|null $buyer_msg 买家留言
 * @property int $delete_state 删除状态0未删除1放入回收站2彻底删除
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property float $goods_price 商品价格
 * @property int $goods_num 商品数量
 * @property string|null $goods_image 商品图片
 * @property int $commis_rate 佣金比例
 * @property int|null $gc_id 商品最底级分类ID
 * @property int|null $vr_indate 有效期
 * @property int $vr_send_times 兑换码发送次数
 * @property int $vr_invalid_refund 允许过期退款1是0否
 * @property int $order_promotion_type 订单参加的促销类型 0无促销1团购
 * @property int|null $promotions_id 促销ID，与order_promotion_type配合使用
 * @property int $order_from 1WEB2mobile
 * @property int $evaluation_state 评价状态0默认1已评价2禁止评价
 * @property int $evaluation_time 评价时间
 * @property int|null $use_state 使用状态0默认，未使用1已使用，有一个被使用即为1
 * @property int|null $api_pay_time 在线支付动作时间,只有站内+在线组合支付时记录
 * @property string|null $goods_contractid 商品开启的消费者保障服务id
 * @property string|null $goods_spec 规格
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereApiPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereBuyerMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereBuyerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereCloseReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereCloseTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereDeleteState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereEvaluationState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereEvaluationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereFinnshedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsContractid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereGoodsSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderPromotionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereOrderState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder wherePaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder wherePdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder wherePromotionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereUseState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereVrIndate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereVrInvalidRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrder whereVrSendTimes($value)
 * @mixin \Eloquent
 */
class ShopncVrOrder extends BaseModel
{
    protected $table = 'shopnc_vr_order';

    protected $primaryKey = 'order_id';

	public $timestamps = false;

    protected $fillable = [
        'order_sn',
        'store_id',
        'store_name',
        'buyer_id',
        'buyer_name',
        'buyer_phone',
        'add_time',
        'payment_code',
        'payment_time',
        'trade_no',
        'close_time',
        'close_reason',
        'finnshed_time',
        'order_amount',
        'refund_amount',
        'rcb_amount',
        'pd_amount',
        'order_state',
        'refund_state',
        'buyer_msg',
        'delete_state',
        'goods_id',
        'goods_name',
        'goods_price',
        'goods_num',
        'goods_image',
        'commis_rate',
        'gc_id',
        'vr_indate',
        'vr_send_times',
        'vr_invalid_refund',
        'order_promotion_type',
        'promotions_id',
        'order_from',
        'evaluation_state',
        'evaluation_time',
        'use_state',
        'api_pay_time',
        'goods_contractid',
        'goods_spec'
    ];

    protected $guarded = [];

        
}