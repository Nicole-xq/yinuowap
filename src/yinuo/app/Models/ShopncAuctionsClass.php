<?php

namespace App\Models;



/**
 * Class ShopncAuctionsClass
 *
 * @property int $auctions_class_id 拍品分类ID
 * @property string $auctions_class_name 分类名称
 * @property int $auctions_class_fid 上级分类ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsClass whereAuctionsClassFid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsClass whereAuctionsClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsClass whereAuctionsClassName($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsClass extends BaseModel
{
    protected $table = 'shopnc_auctions_class';

    protected $primaryKey = 'auctions_class_id';

	public $timestamps = false;

    protected $fillable = [
        'auctions_class_name',
        'auctions_class_fid'
    ];

    protected $guarded = [];

        
}