<?php

namespace App\Models;



/**
 * Class ShopncCircleThg
 *
 * @property int $themegoods_id 主题商品id
 * @property int $theme_id 主题id
 * @property int $reply_id 回复id
 * @property int $circle_id 圈子id
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property float $goods_price 商品价格
 * @property string $goods_image 商品图片
 * @property int $store_id 店铺id
 * @property int $thg_type 商品类型 0为本商城、1为淘宝 默认为0
 * @property string|null $thg_url 商品链接
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereReplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereThemegoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereThgType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThg whereThgUrl($value)
 * @mixin \Eloquent
 */
class ShopncCircleThg extends BaseModel
{
    protected $table = 'shopnc_circle_thg';

    protected $primaryKey = 'themegoods_id';

	public $timestamps = false;

    protected $fillable = [
        'theme_id',
        'reply_id',
        'circle_id',
        'goods_id',
        'goods_name',
        'goods_price',
        'goods_image',
        'store_id',
        'thg_type',
        'thg_url'
    ];

    protected $guarded = [];

        
}