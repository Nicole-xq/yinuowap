<?php

namespace App\Models;



/**
 * Class ShopncArticle
 *
 * @property int $article_id 索引id
 * @property int $ac_id 分类id
 * @property string|null $article_url 跳转链接
 * @property int $article_show 是否显示，0为否，1为是，默认为1
 * @property int $article_position 显示位置:1默认网站前台,2买家,3卖家,4全站
 * @property int $article_sort 排序
 * @property string|null $article_title 标题
 * @property string|null $article_content 内容
 * @property int $article_time 发布时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereAcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticlePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticle whereArticleUrl($value)
 * @mixin \Eloquent
 */
class ShopncArticle extends BaseModel
{
    protected $table = 'shopnc_article';

    protected $primaryKey = 'article_id';

	public $timestamps = false;

    protected $fillable = [
        'ac_id',
        'article_url',
        'article_show',
        'article_position',
        'article_sort',
        'article_title',
        'article_content',
        'article_time'
    ];

    protected $guarded = [];

        
}