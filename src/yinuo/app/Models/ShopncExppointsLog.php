<?php

namespace App\Models;



/**
 * Class ShopncExppointsLog
 *
 * @property int $exp_id 经验值日志编号
 * @property int $exp_memberid 会员编号
 * @property string $exp_membername 会员名称
 * @property int $exp_points 经验值负数表示扣除
 * @property int $exp_addtime 添加时间
 * @property string $exp_desc 操作描述
 * @property string $exp_stage 操作阶段
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExppointsLog whereExpStage($value)
 * @mixin \Eloquent
 */
class ShopncExppointsLog extends BaseModel
{
    protected $table = 'shopnc_exppoints_log';

    protected $primaryKey = 'exp_id';

	public $timestamps = false;

    protected $fillable = [
        'exp_memberid',
        'exp_membername',
        'exp_points',
        'exp_addtime',
        'exp_desc',
        'exp_stage'
    ];

    protected $guarded = [];

        
}