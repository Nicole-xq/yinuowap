<?php

namespace App\Models;



/**
 * Class ShopncTransportExtend
 *
 * @property int $id 运费模板扩展ID
 * @property string|null $area_id 市级地区ID组成的串，以，隔开，两端也有，
 * @property string|null $top_area_id 省级地区ID组成的串，以，隔开，两端也有，
 * @property string|null $area_name 地区name组成的串，以，隔开
 * @property float|null $sprice 首件运费
 * @property int $transport_id 运费模板ID
 * @property string|null $transport_title 运费模板
 * @property float $snum 首(件、重、体积)
 * @property float $xnum 续(件、重、体积)
 * @property float $xprice 续件运费
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereAreaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereSnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereSprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereTopAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereTransportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereTransportTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereXnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransportExtend whereXprice($value)
 * @mixin \Eloquent
 */
class ShopncTransportExtend extends BaseModel
{
    protected $table = 'shopnc_transport_extend';

    public $timestamps = false;

    protected $fillable = [
        'area_id',
        'top_area_id',
        'area_name',
        'sprice',
        'transport_id',
        'transport_title',
        'snum',
        'xnum',
        'xprice'
    ];

    protected $guarded = [];

        
}