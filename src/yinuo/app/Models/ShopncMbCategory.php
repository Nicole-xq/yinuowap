<?php

namespace App\Models;



/**
 * Class ShopncMbCategory
 *
 * @property int $gc_id 商城系统的分类ID
 * @property string|null $gc_thumb 缩略图
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbCategory whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbCategory whereGcThumb($value)
 * @mixin \Eloquent
 */
class ShopncMbCategory extends BaseModel
{
    protected $table = 'shopnc_mb_category';

    protected $primaryKey = 'gc_id';

	public $timestamps = false;

    protected $fillable = [
        'gc_thumb'
    ];

    protected $guarded = [];

        
}