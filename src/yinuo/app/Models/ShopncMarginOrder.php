<?php

namespace App\Models;



/**
 * Class ShopncMarginOrder
 *
 * @property int $margin_id
 * @property string $order_sn 订单编号
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $buyer_id 买家id
 * @property string $buyer_name 买家姓名
 * @property string $buyer_phone 买家手机
 * @property \Carbon\Carbon|null $created_at 创建时间
 * @property \Carbon\Carbon|null $updated_at 更新时间
 * @property string $payment_code 支付方式名称代码
 * @property string|null $payment_time 支付(付款)时间
 * @property int|null $pay_date_number 保证金计息天数
 * @property string|null $finnshed_time 订单完成时间
 * @property float|null $margin_amount 保证金金额
 * @property float|null $rcb_amount 充值卡支付金额
 * @property float|null $pd_amount 预存款支付金额
 * @property float|null $points_amount 诺币体系支付金额
 * @property float|null $api_pay_amount 线上支付方式支付金额
 * @property int|null $order_state 订单状态：0：未支付，1：已支付，2：提交线下支付凭证，3：线上支付部分支付，4：取消支付,5:支付失败
 * @property int|null $refund_state 退款状态：0：未退款，1：已退款,2:已扣除
 * @property int|null $lock_state 竞拍成功，锁定订单不可删除，0：未锁，1：锁定
 * @property string|null $api_pay_time 在线支付动作时间,只要向第三方支付平台提交就会更新
 * @property int $order_type 订单支付类型：0：线上支付：1：线下支付
 * @property string|null $trade_no 外部交易订单号
 * @property int $address_id 联系地址表ID
 * @property string|null $area_info 省，市，县
 * @property string|null $address 详细地址
 * @property int|null $is_points 是否使用积分 0否 1是
 * @property string|null $pay_voucher 订单的付款凭证
 * @property int $auction_id 拍品ID
 * @property string $auction_name 拍品名称
 * @property string $auction_image 拍品图片
 * @property float|null $account_margin_amount 会员账户保证金余额支付保证金金额
 * @property int|null $order_from 1PC，2移动
 * @property string|null $reciver_name 收货人姓名
 * @property int $auction_order_id 拍卖订单ID
 * @property string|null $api_pay_state 0默认未支付1已支付(只有第三方支付接口通知到时才会更改此状态)
 * @property int|null $lg_id 预存款变更id
 * @property int|null $notice_state 消息提醒：0未提醒，1已经提醒
 * @property int|null $financial_verification 财务校验：1已校验、0未校验
 * @property float|null $pay_rate 年利率
 * @property int|null $is_bid 是否出过价(0,未出价 1,已出价)
 * @property int $margin_list_id
 * @property int $api_refund_state 0未微信退款，1微信退款成功，2微信退款失败
 * @property string|null $api_refund_msg 微信退款备注
 * @property int|null $default_state 是否违约：0不违约，1违约，2释放退款
 * @property float|null $rufund_money 需要部分退还的金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAccountMarginAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereApiPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereApiPayState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereApiPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereApiRefundMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereApiRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereAuctionOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereBuyerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereDefaultState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereFinancialVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereFinnshedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereIsBid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereIsPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereLgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereLockState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereMarginAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereMarginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereMarginListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereNoticeState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereOrderFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereOrderState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereOrderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePayDateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePayRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePayVoucher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder wherePointsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereReciverName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereRufundMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginOrder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ShopncMarginOrder extends BaseModel
{
    //订单状态：0：未支付，1：已支付，2：提交线下支付凭证，3：线上支付部分支付，4：取消支付,5:支付失败
    const ORDER_STATE_PAID = 1;
    const ORDER_STATE_OFFLINE_PAY = 2;
    const ORDER_STATE_PARTIAL_PAY = 3;

    protected $table = 'shopnc_margin_orders';

    protected $primaryKey = 'margin_id';

	public $timestamps = true;

    protected $fillable = [
        'order_sn',
        'store_id',
        'store_name',
        'buyer_id',
        'buyer_name',
        'buyer_phone',
        'payment_code',
        'payment_time',
        'pay_date_number',
        'finnshed_time',
        'margin_amount',
        'rcb_amount',
        'pd_amount',
        'points_amount',
        'api_pay_amount',
        'order_state',
        'refund_state',
        'lock_state',
        'api_pay_time',
        'order_type',
        'trade_no',
        'address_id',
        'area_info',
        'address',
        'is_points',
        'pay_voucher',
        'auction_id',
        'auction_name',
        'auction_image',
        'account_margin_amount',
        'order_from',
        'reciver_name',
        'auction_order_id',
        'api_pay_state',
        'lg_id',
        'notice_state',
        'financial_verification',
        'pay_rate',
        'is_bid',
        'margin_list_id',
        'api_refund_state',
        'api_refund_msg',
        'default_state',
        'rufund_money'
    ];

    protected $guarded = [];

        
}