<?php

namespace App\Models;



/**
 * Class ShopncArtist
 *
 * @property int $id
 * @property int $member_id 用户id
 * @property string $artist_name 艺术家名称
 * @property string $artist_job_title 艺术家职称
 * @property string $artist_resume 艺术家简历
 * @property string|null $artist_represent 艺术家年代表
 * @property string|null $artist_awards 艺术家获奖情况
 * @property string|null $artist_works 艺术家获奖作品
 * @property int|null $artist_classify 艺术家分类
 * @property int|null $artist_grade 艺术家等级
 * @property int|null $is_rec 是否推荐
 * @property int|null $artist_sort 排序 值越小越靠前
 * @property string|null $artist_image 艺术家主图
 * @property int|null $artist_click 点击量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistAwards($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistClassify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistRepresent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistResume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereArtistWorks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereIsRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtist whereMemberId($value)
 * @mixin \Eloquent
 */
class ShopncArtist extends BaseModel
{
    protected $table = 'shopnc_artist';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'artist_name',
        'artist_job_title',
        'artist_resume',
        'artist_represent',
        'artist_awards',
        'artist_works',
        'artist_classify',
        'artist_grade',
        'is_rec',
        'artist_sort',
        'artist_image',
        'artist_click'
    ];

    protected $guarded = [];

        
}