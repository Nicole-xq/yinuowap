<?php

namespace App\Models;



/**
 * Class ShopncSpecialRateConfig
 *
 * @property int $id
 * @property string|null $title 配置标题
 * @property string|null $partner_lv_type 合伙人类型(wdl:微代理  zl:战略合伙人 sy:事业合伙人)
 * @property string|null $partner_lv 分销级别(1:直属上级 2:上级的上级)
 * @property float|null $rate 比率
 * @property float|null $max_rate 最大比率
 * @property string|null $udate
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereMaxRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig wherePartnerLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig wherePartnerLvType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialRateConfig whereUdate($value)
 * @mixin \Eloquent
 */
class ShopncSpecialRateConfig extends BaseModel
{
    protected $table = 'shopnc_special_rate_config';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'partner_lv_type',
        'partner_lv',
        'rate',
        'max_rate',
        'udate',
        'cdate'
    ];

    protected $guarded = [];

        
}