<?php

namespace App\Models;



/**
 * Class ShopncCircleF
 *
 * @property int $circle_id 圈子id
 * @property int $friendship_id 友情圈子id
 * @property string $friendship_name 友情圈子名称
 * @property int $friendship_sort 友情圈子排序
 * @property int $friendship_status 友情圈子名称 1显示 0隐藏
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleF whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleF whereFriendshipId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleF whereFriendshipName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleF whereFriendshipSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleF whereFriendshipStatus($value)
 * @mixin \Eloquent
 */
class ShopncCircleF extends BaseModel
{
    protected $table = 'shopnc_circle_fs';

    public $timestamps = false;

    protected $fillable = [
        'circle_id',
        'friendship_id',
        'friendship_name',
        'friendship_sort',
        'friendship_status'
    ];

    protected $guarded = [];

        
}