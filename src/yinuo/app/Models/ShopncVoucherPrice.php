<?php

namespace App\Models;



/**
 * Class ShopncVoucherPrice
 *
 * @property int $voucher_price_id 代金券面值编号
 * @property string $voucher_price_describe 代金券描述
 * @property int $voucher_price 代金券面值
 * @property int $voucher_defaultpoints 代金劵默认的兑换所需积分可以为0
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherPrice whereVoucherDefaultpoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherPrice whereVoucherPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherPrice whereVoucherPriceDescribe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherPrice whereVoucherPriceId($value)
 * @mixin \Eloquent
 */
class ShopncVoucherPrice extends BaseModel
{
    protected $table = 'shopnc_voucher_price';

    protected $primaryKey = 'voucher_price_id';

	public $timestamps = false;

    protected $fillable = [
        'voucher_price_describe',
        'voucher_price',
        'voucher_defaultpoints'
    ];

    protected $guarded = [];

        
}