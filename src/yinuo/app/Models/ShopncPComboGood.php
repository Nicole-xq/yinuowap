<?php

namespace App\Models;



/**
 * Class ShopncPComboGood
 *
 * @property int $cg_id 推荐组合id
 * @property string $cg_class 推荐组合名称
 * @property int $goods_id 主商品id
 * @property int $goods_commonid 主商品公共id
 * @property int $store_id 所属店铺id
 * @property int $combo_goodsid 推荐组合商品id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereCgClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereCgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereComboGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboGood whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncPComboGood extends BaseModel
{
    protected $table = 'shopnc_p_combo_goods';

    protected $primaryKey = 'cg_id';

	public $timestamps = false;

    protected $fillable = [
        'cg_class',
        'goods_id',
        'goods_commonid',
        'store_id',
        'combo_goodsid'
    ];

    protected $guarded = [];

        
}