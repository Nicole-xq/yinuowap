<?php

namespace App\Models;



/**
 * Class ShopncExpress
 *
 * @property int $id 索引ID
 * @property string $e_name 公司名称
 * @property string $e_state 状态
 * @property string $e_code 编号
 * @property string|null $e_code_kdniao 快递鸟快递公司代码
 * @property string $e_letter 首字母
 * @property string $e_order 1常用2不常用
 * @property string $e_url 公司网址
 * @property int|null $e_zt_state 是否支持服务站配送0否1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereECode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereECodeKdniao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereELetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereEName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereEOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereEState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereEUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereEZtState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExpress whereId($value)
 * @mixin \Eloquent
 */
class ShopncExpress extends BaseModel
{
    protected $table = 'shopnc_express';

    public $timestamps = false;

    protected $fillable = [
        'e_name',
        'e_state',
        'e_code',
        'e_code_kdniao',
        'e_letter',
        'e_order',
        'e_url',
        'e_zt_state'
    ];

    protected $guarded = [];

        
}