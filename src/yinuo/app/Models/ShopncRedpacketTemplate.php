<?php

namespace App\Models;



/**
 * Class ShopncRedpacketTemplate
 *
 * @property int $rpacket_t_id 红包模版编号
 * @property string $rpacket_t_title 红包模版名称
 * @property string $rpacket_t_desc 红包模版描述
 * @property int $rpacket_t_start_date 红包模版有效期开始时间
 * @property int $rpacket_t_end_date 红包模版有效期结束时间
 * @property float $rpacket_t_price 红包模版面额
 * @property float $rpacket_t_limit 红包使用时的订单限额
 * @property int $rpacket_t_adminid 修改管理员ID
 * @property int $rpacket_t_state 模版状态(1-有效,2-失效)
 * @property int $rpacket_t_total 模版可发放的红包总数
 * @property int $rpacket_t_giveout 模版已发放的红包数量
 * @property int $rpacket_t_used 模版已经使用过的红包数量
 * @property int $rpacket_t_updatetime 模版的创建时间
 * @property int $rpacket_t_points 兑换所需积分
 * @property int $rpacket_t_eachlimit 每人限领张数
 * @property string|null $rpacket_t_customimg 自定义模板图片
 * @property int $rpacket_t_recommend 是否推荐 0不推荐 1推荐
 * @property int $rpacket_t_gettype 领取方式 1积分兑换 2卡密兑换 3免费领取
 * @property int $rpacket_t_isbuild 领取方式为卡密兑换是否已经生成下属红包 0未生成 1已生成
 * @property int $rpacket_t_mgradelimit 领取限制的会员等级
 * @property int $rpacket_t_type 红包类型(1-全平台,2-指定店铺)
 * @property int $rpacket_t_store_id 店铺ID
 * @property string|null $rpacket_t_wap_link WAP链接
 * @property string|null $rpacket_t_pc_link PC链接
 * @property int|null $rpacket_t_special_type 红包类型, 0:普通红包,1:注册红包
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTAdminid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTCustomimg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTEachlimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTGettype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTGiveout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTIsbuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTMgradelimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTPcLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTSpecialType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTUpdatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRedpacketTemplate whereRpacketTWapLink($value)
 * @mixin \Eloquent
 */
class ShopncRedpacketTemplate extends BaseModel
{

    //红包类型, 0:普通红包,1:注册红包,2:保证金红包,3:结算红包
    const RPACKET_T_SPECIAL_TYPE_NORMAL = 0;
    const RPACKET_T_SPECIAL_TYPE_REGISTER = 1;
    const RPACKET_T_SPECIAL_TYPE_MARGIN = 2;
    const RPACKET_T_SPECIAL_TYPE_ORDER = 3;


    //红包模板状态 1-有效,2-失效
    const RPACKET_T_STATE_VALID = 1;
    const RPACKET_T_STATE_INVALID = 2;

    //红包使用范围  1-全平台,2-指定店铺,3-新手专区
    const RPACKET_T_TYPE_PLATFORM = 1;
    const RPACKET_T_TYPE_APPOINT = 2;
    const RPACKET_T_TYPE_NEWBIE = 3;

    //领取方式 1积分兑换 2卡密兑换 3免费领取
    const RPACKET_T_GETTYPE_POINT = 1;
    const RPACKET_T_GETTYPE_PASSWORD = 2;
    const RPACKET_T_GETTYPE_FREE = 3;

    protected $table = 'shopnc_redpacket_template';

    protected $primaryKey = 'rpacket_t_id';

	public $timestamps = false;

    protected $fillable = [
        'rpacket_t_title',
        'rpacket_t_desc',
        'rpacket_t_start_date',
        'rpacket_t_end_date',
        'rpacket_t_price',
        'rpacket_t_limit',
        'rpacket_t_adminid',
        'rpacket_t_state',
        'rpacket_t_total',
        'rpacket_t_giveout',
        'rpacket_t_used',
        'rpacket_t_updatetime',
        'rpacket_t_points',
        'rpacket_t_eachlimit',
        'rpacket_t_customimg',
        'rpacket_t_recommend',
        'rpacket_t_gettype',
        'rpacket_t_isbuild',
        'rpacket_t_mgradelimit',
        'rpacket_t_type',
        'rpacket_t_store_id',
        'rpacket_t_wap_link',
        'rpacket_t_pc_link',
        'rpacket_t_special_type'
    ];

    protected $guarded = [];

        
}