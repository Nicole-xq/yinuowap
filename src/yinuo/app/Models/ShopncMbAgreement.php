<?php

namespace App\Models;



/**
 * Class ShopncMbAgreement
 *
 * @property int $agreement_id 协议id
 * @property string $agreement_code 协议代码名称
 * @property string|null $agreement_title 协议标题
 * @property string|null $agreement_content 协议内容
 * @property int|null $last_modify_time 最后修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAgreement whereAgreementCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAgreement whereAgreementContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAgreement whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAgreement whereAgreementTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAgreement whereLastModifyTime($value)
 * @mixin \Eloquent
 */
class ShopncMbAgreement extends BaseModel
{
    protected $table = 'shopnc_mb_agreement';

    protected $primaryKey = 'agreement_id';

	public $timestamps = false;

    protected $fillable = [
        'agreement_code',
        'agreement_title',
        'agreement_content',
        'last_modify_time'
    ];

    protected $guarded = [];

        
}