<?php

namespace App\Models;



/**
 * Class ShopncMemberMsgTpl
 *
 * @property int $mmt_code 用户消息模板编号
 * @property string $mmt_name 模板名称
 * @property int $mmt_message_switch 站内信接收开关
 * @property string $mmt_message_subject 站内信标题
 * @property string $mmt_message_content 站内信消息内容
 * @property string|null $mmt_message_logo 站内信logo
 * @property int $mmt_short_switch 短信接收开关
 * @property string $mmt_short_content 短信接收内容
 * @property int $mmt_mail_switch 邮件接收开关
 * @property string $mmt_mail_subject 邮件标题
 * @property string $mmt_mail_content 邮件内容
 * @property int $mmt_wx_switch 微信消息模板开关
 * @property string|null $mmt_wx_subject 微信消息模板ID
 * @property int $mmt_app_switch app消息模板开关
 * @property string|null $mmt_app_subject app消息模板标题
 * @property string $mmt_app_link 消息链接
 * @property string $mmt_app_content app描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtAppContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtAppLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtAppSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtAppSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMailContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMailSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMailSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMessageContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMessageLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMessageSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtMessageSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtShortSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtWxSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgTpl whereMmtWxSwitch($value)
 * @mixin \Eloquent
 */
class ShopncMemberMsgTpl extends BaseModel
{
    protected $table = 'shopnc_member_msg_tpl';

    protected $primaryKey = 'mmt_code';

	public $timestamps = false;

    protected $fillable = [
        'mmt_name',
        'mmt_message_switch',
        'mmt_message_subject',
        'mmt_message_content',
        'mmt_message_logo',
        'mmt_short_switch',
        'mmt_short_content',
        'mmt_mail_switch',
        'mmt_mail_subject',
        'mmt_mail_content',
        'mmt_wx_switch',
        'mmt_wx_subject',
        'mmt_app_switch',
        'mmt_app_subject',
        'mmt_app_link',
        'mmt_app_content'
    ];

    protected $guarded = [];

        
}