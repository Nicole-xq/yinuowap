<?php

namespace App\Models;



/**
 * Class ShopncPMansongQuotum
 *
 * @property int $quota_id 满就送套餐编号
 * @property int $member_id 用户编号
 * @property int $store_id 店铺编号
 * @property string $member_name 用户名
 * @property string $store_name 店铺名称
 * @property int $start_time 开始时间
 * @property int $end_time 结束时间
 * @property int|null $state 配额状态(1-可用/2-取消/3-结束)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPMansongQuotum extends BaseModel
{
    protected $table = 'shopnc_p_mansong_quota';

    protected $primaryKey = 'quota_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'store_id',
        'member_name',
        'store_name',
        'start_time',
        'end_time',
        'state'
    ];

    protected $guarded = [];

        
}