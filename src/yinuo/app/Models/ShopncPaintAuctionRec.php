<?php

namespace App\Models;



/**
 * Class ShopncPaintAuctionRec
 *
 * @property int $paint_rec_id 书画推荐ID
 * @property string $tag_name 推荐标签名称
 * @property int $module_title_id 模块名称ID
 * @property int $auction_id 拍品ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintAuctionRec whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintAuctionRec whereModuleTitleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintAuctionRec wherePaintRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintAuctionRec whereTagName($value)
 * @mixin \Eloquent
 */
class ShopncPaintAuctionRec extends BaseModel
{
    protected $table = 'shopnc_paint_auction_rec';

    protected $primaryKey = 'paint_rec_id';

	public $timestamps = false;

    protected $fillable = [
        'tag_name',
        'module_title_id',
        'auction_id'
    ];

    protected $guarded = [];

        
}