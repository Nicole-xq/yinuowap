<?php

namespace App\Models;



/**
 * Class ShopncScLog
 *
 * @property int $sc_log_id 专场围观日志ID
 * @property int $special_id 专场ID
 * @property int $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScLog whereScLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScLog whereSpecialId($value)
 * @mixin \Eloquent
 */
class ShopncScLog extends BaseModel
{
    protected $table = 'shopnc_sc_log';

    protected $primaryKey = 'sc_log_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'add_time'
    ];

    protected $guarded = [];

        
}