<?php

namespace App\Models;



/**
 * Class ShopncGoodsFcode
 *
 * @property int $fc_id F码id
 * @property int $goods_id 商品sku
 * @property string $fc_code F码
 * @property int $fc_state 状态 0未使用，1已使用
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsFcode whereFcCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsFcode whereFcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsFcode whereFcState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsFcode whereGoodsId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsFcode extends BaseModel
{
    protected $table = 'shopnc_goods_fcode';

    protected $primaryKey = 'fc_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'fc_code',
        'fc_state'
    ];

    protected $guarded = [];

        
}