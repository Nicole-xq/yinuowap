<?php

namespace App\Models;



/**
 * Class ShopncStoreVendueCopy
 *
 * @property int $store_vendue_id 商家拍卖ID
 * @property int|null $store_vendue_contact 商家拍卖联系方式
 * @property int $city_id 市级ID
 * @property int $area_id 区级ID
 * @property string $oz_type 商家拍卖机构类型
 * @property string|null $store_vendue_intro 商家拍卖简介
 * @property int $store_id 商家ID
 * @property int $store_apply_state 商家拍卖申请状态 10 提交申请 20 审核成功 30 审核失败 11完成付款 40申请成功
 * @property int $add_time 商家拍卖申请时间
 * @property string $store_name 商家名称
 * @property string|null $check_message 审核意见
 * @property int $is_pay 是否支付保证金 0未支付 1已支付
 * @property int|null $attention_num 关注人数
 * @property string|null $paying_money_certificate 付款保证金凭证
 * @property string|null $paying_money_certif_exp 付款备注
 * @property string|null $store_class_auction_commis_rates 拍卖分类佣金比例
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereAttentionNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereCheckMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereIsPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereOzType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy wherePayingMoneyCertifExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy wherePayingMoneyCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreApplyState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreClassAuctionCommisRates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreVendueContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreVendueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreVendueCopy whereStoreVendueIntro($value)
 * @mixin \Eloquent
 */
class ShopncStoreVendueCopy extends BaseModel
{
    protected $table = 'shopnc_store_vendue_copy';

    protected $primaryKey = 'store_vendue_id';

	public $timestamps = false;

    protected $fillable = [
        'store_vendue_contact',
        'city_id',
        'area_id',
        'oz_type',
        'store_vendue_intro',
        'store_id',
        'store_apply_state',
        'add_time',
        'store_name',
        'check_message',
        'is_pay',
        'attention_num',
        'paying_money_certificate',
        'paying_money_certif_exp',
        'store_class_auction_commis_rates'
    ];

    protected $guarded = [];

        
}