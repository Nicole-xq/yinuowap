<?php

namespace App\Models;



/**
 * Class ShopncGoodsClassStaple
 *
 * @property int $staple_id 常用分类id
 * @property string $staple_name 常用分类名称
 * @property int $gc_id_1 一级分类id
 * @property int $gc_id_2 二级商品分类
 * @property int $gc_id_3 三级商品分类
 * @property int $type_id 类型id
 * @property int $member_id 会员id
 * @property int $counter 计数器
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereStapleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereStapleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassStaple whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsClassStaple extends BaseModel
{
    protected $table = 'shopnc_goods_class_staple';

    protected $primaryKey = 'staple_id';

	public $timestamps = false;

    protected $fillable = [
        'staple_name',
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        'type_id',
        'member_id',
        'counter'
    ];

    protected $guarded = [];

        
}