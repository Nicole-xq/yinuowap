<?php

namespace App\Models;



/**
 * Class ShopncPMansong
 *
 * @property int $mansong_id 满送活动编号
 * @property string $mansong_name 活动名称
 * @property int $quota_id 套餐编号
 * @property int $start_time 活动开始时间
 * @property int $end_time 活动结束时间
 * @property int $member_id 用户编号
 * @property int $store_id 店铺编号
 * @property string $member_name 用户名
 * @property string $store_name 店铺名称
 * @property int $state 活动状态(1-未发布/2-正常/3-取消/4-失效/5-结束)
 * @property string|null $remark 备注
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereMansongId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereMansongName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansong whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPMansong extends BaseModel
{
    protected $table = 'shopnc_p_mansong';

    protected $primaryKey = 'mansong_id';

	public $timestamps = false;

    protected $fillable = [
        'mansong_name',
        'quota_id',
        'start_time',
        'end_time',
        'member_id',
        'store_id',
        'member_name',
        'store_name',
        'state',
        'remark'
    ];

    protected $guarded = [];

        
}