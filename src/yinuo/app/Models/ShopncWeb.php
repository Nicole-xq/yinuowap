<?php

namespace App\Models;



/**
 * Class ShopncWeb
 *
 * @property int $web_id 模块ID
 * @property string|null $web_name 模块名称
 * @property string|null $style_name 风格名称
 * @property string|null $web_page 所在页面
 * @property int $update_time 更新时间
 * @property int|null $web_sort 排序
 * @property int|null $web_show 是否显示，0为否，1为是，默认为1
 * @property string|null $web_html 模块html代码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereStyleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebPage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWeb whereWebSort($value)
 * @mixin \Eloquent
 */
class ShopncWeb extends BaseModel
{
    protected $table = 'shopnc_web';

    protected $primaryKey = 'web_id';

	public $timestamps = false;

    protected $fillable = [
        'web_name',
        'style_name',
        'web_page',
        'update_time',
        'web_sort',
        'web_show',
        'web_html'
    ];

    protected $guarded = [];

        
}