<?php

namespace App\Models;



/**
 * Class ShopncAreaAgent
 *
 * @property int $agent_id 代理id
 * @property string $agent_name 代理名称
 * @property string $agent_address 代理街道地址
 * @property int $agent_type 代理类型：1自然人2法人
 * @property int $agent_state 代理状态：1为正常2禁用
 * @property string $linkman_name 联系人名称
 * @property string $linkman_mobile 联系人手机号
 * @property int $commission_lv 返佣等级
 * @property int $area_type 区域类型：1省2市3县
 * @property int $reg_time 注册时间
 * @property int $edit_time 更新时间
 * @property int|null $area_id 地区ID
 * @property int|null $county_id 县ID
 * @property int|null $city_id 市ID
 * @property int|null $province_id 省ID
 * @property string|null $area_info 地区内容
 * @property string|null $bank_householder 银行卡户主
 * @property string|null $bank_number 银行卡账号
 * @property string|null $bank_accounts_address 银行卡支行
 * @property string|null $identity_card_number 身份证号
 * @property string|null $business_license_image 营业执照图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAgentAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAgentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAgentState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAgentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereAreaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereBankAccountsAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereBankHouseholder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereBankNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereBusinessLicenseImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereCommissionLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereCountyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereIdentityCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereLinkmanMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereLinkmanName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgent whereRegTime($value)
 * @mixin \Eloquent
 */
class ShopncAreaAgent extends BaseModel
{
    protected $table = 'shopnc_area_agent';

    protected $primaryKey = 'agent_id';

	public $timestamps = false;

    protected $fillable = [
        'agent_name',
        'agent_address',
        'agent_type',
        'agent_state',
        'linkman_name',
        'linkman_mobile',
        'commission_lv',
        'area_type',
        'reg_time',
        'edit_time',
        'area_id',
        'county_id',
        'city_id',
        'province_id',
        'area_info',
        'bank_householder',
        'bank_number',
        'bank_accounts_address',
        'identity_card_number',
        'business_license_image'
    ];

    protected $guarded = [];

        
}