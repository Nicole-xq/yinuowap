<?php

namespace App\Models;



/**
 * Class ShopncMailMsgTemlate
 *
 * @property string $name 模板名称
 * @property string|null $title 模板标题
 * @property int $code 模板调用代码
 * @property string $content 模板内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailMsgTemlate whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailMsgTemlate whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailMsgTemlate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailMsgTemlate whereTitle($value)
 * @mixin \Eloquent
 */
class ShopncMailMsgTemlate extends BaseModel
{
    protected $table = 'shopnc_mail_msg_temlates';

    protected $primaryKey = 'code';

	public $timestamps = false;

    protected $fillable = [
        'name',
        'title',
        'content'
    ];

    protected $guarded = [];

        
}