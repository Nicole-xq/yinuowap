<?php

namespace App\Models;



/**
 * Class ShopncPXianshi
 *
 * @property int $xianshi_id 限时编号
 * @property string $xianshi_name 活动名称
 * @property string|null $xianshi_title 活动标题
 * @property string|null $xianshi_explain 活动说明
 * @property int $quota_id 套餐编号
 * @property int $start_time 活动开始时间
 * @property int $end_time 活动结束时间
 * @property int $member_id 用户编号
 * @property int $store_id 店铺编号
 * @property string $member_name 用户名
 * @property string $store_name 店铺名称
 * @property int $lower_limit 购买下限，1为不限制
 * @property int $state 状态，0-取消 1-正常
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereLowerLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereXianshiExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereXianshiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereXianshiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshi whereXianshiTitle($value)
 * @mixin \Eloquent
 */
class ShopncPXianshi extends BaseModel
{
    protected $table = 'shopnc_p_xianshi';

    protected $primaryKey = 'xianshi_id';

	public $timestamps = false;

    protected $fillable = [
        'xianshi_name',
        'xianshi_title',
        'xianshi_explain',
        'quota_id',
        'start_time',
        'end_time',
        'member_id',
        'store_id',
        'member_name',
        'store_name',
        'lower_limit',
        'state'
    ];

    protected $guarded = [];

        
}