<?php

namespace App\Models;



/**
 * Class ShopncChain
 *
 * @property int $chain_id 门店id
 * @property int $store_id 所属店铺id
 * @property string $chain_user 登录名
 * @property string $chain_pwd 登录密码
 * @property string $chain_name 门店名称
 * @property string $chain_img 门店图片
 * @property int $area_id_1 一级地区id
 * @property int $area_id_2 二级地区id
 * @property int $area_id_3 三级地区id
 * @property int $area_id_4 四级地区id
 * @property int $area_id 地区id
 * @property string $area_info 地区详情
 * @property string $chain_address 详细地址
 * @property string $chain_phone 联系方式
 * @property string $chain_opening_hours 营业时间
 * @property string $chain_traffic_line 交通线路
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaId4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainOpeningHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainPwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainTrafficLine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereChainUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChain whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncChain extends BaseModel
{
    protected $table = 'shopnc_chain';

    protected $primaryKey = 'chain_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'chain_user',
        'chain_pwd',
        'chain_name',
        'chain_img',
        'area_id_1',
        'area_id_2',
        'area_id_3',
        'area_id_4',
        'area_id',
        'area_info',
        'chain_address',
        'chain_phone',
        'chain_opening_hours',
        'chain_traffic_line'
    ];

    protected $guarded = [];

        
}