<?php

namespace App\Models;



/**
 * Class ShopncUpload
 *
 * @property int $upload_id 索引ID
 * @property string|null $file_name 文件名
 * @property string|null $file_thumb 缩微图片
 * @property int $file_size 文件大小
 * @property int $upload_type 文件类别，0为无，1为文章图片，默认为0，2为帮助内容图片，3为店铺幻灯片，4为系统文章图片，5为积分礼品切换图片，6为积分礼品内容图片
 * @property int $upload_time 添加时间
 * @property int $item_id 信息ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereFileThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereUploadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereUploadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUpload whereUploadType($value)
 * @mixin \Eloquent
 */
class ShopncUpload extends BaseModel
{
    protected $table = 'shopnc_upload';

    protected $primaryKey = 'upload_id';

	public $timestamps = false;

    protected $fillable = [
        'file_name',
        'file_thumb',
        'file_size',
        'upload_type',
        'upload_time',
        'item_id'
    ];

    protected $guarded = [];

        
}