<?php

namespace App\Models;



/**
 * Class ShopncDisLiveLog
 *
 * @property int $log_id 日志编号
 * @property int|null $live_id 直播编号
 * @property int|null $member_id 用户编号
 * @property string|null $member_name 用户名
 * @property int|null $add_time 添加时间
 * @property int|null $member_state 访客状态 1正常 2退出
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereLiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog whereMemberState($value)
 * @mixin \Eloquent
 */
class ShopncDisLiveLog extends BaseModel
{
    protected $table = 'shopnc_dis_live_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'live_id',
        'member_id',
        'member_name',
        'add_time',
        'member_state'
    ];

    protected $guarded = [];

        
}