<?php

namespace App\Models;



/**
 * Class ShopncCircleExplog
 *
 * @property int $el_id 经验日志id
 * @property int $circle_id 圈子id
 * @property int $member_id 成员id
 * @property string $member_name 成员名称
 * @property int $el_exp 获得经验
 * @property string $el_time 获得时间
 * @property int $el_type 类型 1管理员操作 2发表话题 3发表回复 4话题被回复 5话题被删除 6回复被删除
 * @property string $el_itemid 信息id
 * @property string $el_desc 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereElType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExplog whereMemberName($value)
 * @mixin \Eloquent
 */
class ShopncCircleExplog extends BaseModel
{
    protected $table = 'shopnc_circle_explog';

    protected $primaryKey = 'el_id';

	public $timestamps = false;

    protected $fillable = [
        'circle_id',
        'member_id',
        'member_name',
        'el_exp',
        'el_time',
        'el_type',
        'el_itemid',
        'el_desc'
    ];

    protected $guarded = [];

        
}