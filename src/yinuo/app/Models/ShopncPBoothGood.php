<?php

namespace App\Models;



/**
 * Class ShopncPBoothGood
 *
 * @property int $booth_goods_id 套餐商品id
 * @property int $store_id 店铺id
 * @property int $goods_id 商品id
 * @property int $gc_id 商品分类id
 * @property int $booth_state 套餐状态 1开启 0关闭 默认1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothGood whereBoothGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothGood whereBoothState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothGood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothGood whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncPBoothGood extends BaseModel
{
    protected $table = 'shopnc_p_booth_goods';

    protected $primaryKey = 'booth_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'goods_id',
        'gc_id',
        'booth_state'
    ];

    protected $guarded = [];

        
}