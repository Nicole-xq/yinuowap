<?php

namespace App\Models;



/**
 * Class ShopncAuctionVirtualDataConfig
 *
 * @property int $id
 * @property string|null $title 名称
 * @property int|null $type 类型(1,点击数  2,收藏数  3,参拍数)
 * @property float|null $min_value 下限
 * @property float|null $max_value 上限
 * @property int|null $rate_type 是否有收益 0,无收益 1,有收益
 * @property string|null $udate 修改时间
 * @property string|null $cdate 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereMaxValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereMinValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereRateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionVirtualDataConfig whereUdate($value)
 * @mixin \Eloquent
 */
class ShopncAuctionVirtualDataConfig extends BaseModel
{
    protected $table = 'shopnc_auction_virtual_data_config';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'type',
        'min_value',
        'max_value',
        'rate_type',
        'udate',
        'cdate'
    ];

    protected $guarded = [];

        
}