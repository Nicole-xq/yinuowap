<?php

namespace App\Models;



/**
 * Class ShopncStoreDecoration
 *
 * @property int $decoration_id 装修编号
 * @property string $decoration_name 装修名称
 * @property int $store_id 店铺编号
 * @property string|null $decoration_setting 装修整体设置(背景、边距等)
 * @property string|null $decoration_nav 装修导航
 * @property string|null $decoration_banner 装修头部banner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereDecorationBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereDecorationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereDecorationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereDecorationNav($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereDecorationSetting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreDecoration whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreDecoration extends BaseModel
{
    protected $table = 'shopnc_store_decoration';

    protected $primaryKey = 'decoration_id';

	public $timestamps = false;

    protected $fillable = [
        'decoration_name',
        'store_id',
        'decoration_setting',
        'decoration_nav',
        'decoration_banner'
    ];

    protected $guarded = [];

        
}