<?php

namespace App\Models;



/**
 * Class ShopncPBoothQuotum
 *
 * @property int $booth_quota_id 套餐id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $booth_quota_starttime 开始时间
 * @property int $booth_quota_endtime 结束时间
 * @property int $booth_state 套餐状态 1开启 0关闭 默认1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereBoothQuotaEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereBoothQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereBoothQuotaStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereBoothState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBoothQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPBoothQuotum extends BaseModel
{
    protected $table = 'shopnc_p_booth_quota';

    protected $primaryKey = 'booth_quota_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'booth_quota_starttime',
        'booth_quota_endtime',
        'booth_state'
    ];

    protected $guarded = [];

        
}