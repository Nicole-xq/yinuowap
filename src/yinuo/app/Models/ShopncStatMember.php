<?php

namespace App\Models;



/**
 * Class ShopncStatMember
 *
 * @property int $statm_id 自增ID
 * @property int $statm_memberid 会员ID
 * @property string $statm_membername 会员名称
 * @property int $statm_time 统计时间，当前按照最小时间单位为天
 * @property int $statm_ordernum 下单量
 * @property float $statm_orderamount 下单金额
 * @property int $statm_goodsnum 下单商品件数
 * @property float $statm_predincrease 预存款增加额
 * @property float $statm_predreduce 预存款减少额
 * @property int $statm_pointsincrease 积分增加额
 * @property int $statm_pointsreduce 积分减少额
 * @property int $statm_updatetime 记录更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmGoodsnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmOrderamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmOrdernum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmPointsincrease($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmPointsreduce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmPredincrease($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmPredreduce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatMember whereStatmUpdatetime($value)
 * @mixin \Eloquent
 */
class ShopncStatMember extends BaseModel
{
    protected $table = 'shopnc_stat_member';

    protected $primaryKey = 'statm_id';

	public $timestamps = false;

    protected $fillable = [
        'statm_memberid',
        'statm_membername',
        'statm_time',
        'statm_ordernum',
        'statm_orderamount',
        'statm_goodsnum',
        'statm_predincrease',
        'statm_predreduce',
        'statm_pointsincrease',
        'statm_pointsreduce',
        'statm_updatetime'
    ];

    protected $guarded = [];

        
}