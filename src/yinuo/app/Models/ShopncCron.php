<?php

namespace App\Models;



/**
 * Class ShopncCron
 *
 * @property int $id
 * @property int|null $type 任务类型 1商品上架 2根据商品id更新商品促销价格 3优惠套装过期 4推荐展位过期 5团购开始更新商品促销价格 6团购过期 7限时折扣过期 8加价购过期 9商品消费者保障服务开启状态更新 10手机专享过期 11：拍品预展，12：拍品开始拍卖，13：拍品结束拍卖，14：拍品消费者保障服务
 * @property int $exeid 关联任务的ID[如商品ID,会员ID]
 * @property int $exetime 执行时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCron whereExeid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCron whereExetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCron whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCron whereType($value)
 * @mixin \Eloquent
 */
class ShopncCron extends BaseModel
{
    protected $table = 'shopnc_cron';

    public $timestamps = false;

    protected $fillable = [
        'type',
        'exeid',
        'exetime'
    ];

    protected $guarded = [];

        
}