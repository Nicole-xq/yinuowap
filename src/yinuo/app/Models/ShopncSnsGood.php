<?php

namespace App\Models;



/**
 * Class ShopncSnsGood
 *
 * @property int $snsgoods_goodsid 商品ID
 * @property string $snsgoods_goodsname 商品名称
 * @property string|null $snsgoods_goodsimage 商品图片
 * @property float $snsgoods_goodsprice 商品价格
 * @property int $snsgoods_storeid 店铺ID
 * @property string $snsgoods_storename 店铺名称
 * @property int $snsgoods_addtime 添加时间
 * @property int $snsgoods_likenum 喜欢数
 * @property string|null $snsgoods_likemember 喜欢过的会员ID，用逗号分隔
 * @property int $snsgoods_sharenum 分享数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsGoodsimage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsGoodsname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsGoodsprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsLikemember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsLikenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsSharenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsGood whereSnsgoodsStorename($value)
 * @mixin \Eloquent
 */
class ShopncSnsGood extends BaseModel
{
    protected $table = 'shopnc_sns_goods';

    protected $primaryKey = 'snsgoods_goodsid';

	public $timestamps = false;

    protected $fillable = [
        'snsgoods_goodsname',
        'snsgoods_goodsimage',
        'snsgoods_goodsprice',
        'snsgoods_storeid',
        'snsgoods_storename',
        'snsgoods_addtime',
        'snsgoods_likenum',
        'snsgoods_likemember',
        'snsgoods_sharenum'
    ];

    protected $guarded = [];

        
}