<?php

namespace App\Models;



/**
 * Class ShopncStoreMsgRead
 *
 * @property int $sm_id 店铺消息id
 * @property int $seller_id 卖家id
 * @property int $read_time 阅读时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgRead whereReadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgRead whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgRead whereSmId($value)
 * @mixin \Eloquent
 */
class ShopncStoreMsgRead extends BaseModel
{
    protected $table = 'shopnc_store_msg_read';

    public $timestamps = false;

    protected $fillable = [
        'sm_id',
        'seller_id',
        'read_time'
    ];

    protected $guarded = [];

        
}