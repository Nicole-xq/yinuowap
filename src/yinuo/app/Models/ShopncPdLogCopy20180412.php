<?php

namespace App\Models;



/**
 * Class ShopncPdLogCopy20180412
 *
 * @property int $lg_id 自增编号
 * @property int $lg_member_id 会员编号
 * @property string $lg_member_name 会员名称
 * @property string|null $lg_admin_name 管理员名称
 * @property string $lg_type order_pay下单支付预存款,order_freeze下单冻结预存款,order_cancel取消订单解冻预存款,order_comb_pay下单支付被冻结的预存款,recharge充值,cash_apply申请提现冻结预存款,cash_pay提现成功,cash_del取消提现申请，解冻预存款,refund退款
 * @property float $lg_av_amount 可用金额变更0表示未变更
 * @property float $lg_freeze_amount 冻结金额变更0表示未变更
 * @property float $lg_freeze_predeposit 冻结预存款总额
 * @property float $lg_available_amount 剩余可用预存款
 * @property int $lg_add_time 添加时间
 * @property string|null $lg_desc 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgAvAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgAvailableAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgFreezeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgFreezePredeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdLogCopy20180412 whereLgType($value)
 * @mixin \Eloquent
 */
class ShopncPdLogCopy20180412 extends BaseModel
{
    protected $table = 'shopnc_pd_log_copy20180412';

    protected $primaryKey = 'lg_id';

	public $timestamps = false;

    protected $fillable = [
        'lg_member_id',
        'lg_member_name',
        'lg_admin_name',
        'lg_type',
        'lg_av_amount',
        'lg_freeze_amount',
        'lg_freeze_predeposit',
        'lg_available_amount',
        'lg_add_time',
        'lg_desc'
    ];

    protected $guarded = [];

        
}