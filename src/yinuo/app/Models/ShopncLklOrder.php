<?php

namespace App\Models;



/**
 * Class ShopncLklOrder
 *
 * @property int $id
 * @property string|null $pay_sn
 * @property string|null $lkl_order_sn
 * @property int|null $status 0,未付款 1,已付款
 * @property string|null $cdate 订单号创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLklOrder whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLklOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLklOrder whereLklOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLklOrder wherePaySn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLklOrder whereStatus($value)
 * @mixin \Eloquent
 */
class ShopncLklOrder extends BaseModel
{
    protected $table = 'shopnc_lkl_order';

    public $timestamps = false;

    protected $fillable = [
        'pay_sn',
        'lkl_order_sn',
        'status',
        'cdate'
    ];

    protected $guarded = [];

        
}