<?php

namespace App\Models;



/**
 * Class ShopncGuessMemberRelation
 *
 * @property int $relation_id
 * @property int $gs_id
 * @property int $member_id
 * @property int|null $is_remind 是否设置手机提醒 1：设置
 * @property int|null $remind_time 设置提醒的时间
 * @property string|null $member_mobile 会员设置提醒的手机号
 * @property int|null $is_remind_app 是否设置app提醒，1：设置
 * @property string|null $member_name 会员名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereGsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereIsRemind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereIsRemindApp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGuessMemberRelation whereRemindTime($value)
 * @mixin \Eloquent
 */
class ShopncGuessMemberRelation extends BaseModel
{
    protected $table = 'shopnc_guess_member_relation';

    protected $primaryKey = 'relation_id';

	public $timestamps = false;

    protected $fillable = [
        'gs_id',
        'member_id',
        'is_remind',
        'remind_time',
        'member_mobile',
        'is_remind_app',
        'member_name'
    ];

    protected $guarded = [];

        
}