<?php

namespace App\Models;



/**
 * Class ShopncMbFeedback
 *
 * @property int $id
 * @property string|null $content
 * @property string|null $type 1来自手机端2来自PC端
 * @property int $ftime 反馈时间
 * @property int $member_id 用户编号
 * @property string $member_name 用户名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereFtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFeedback whereType($value)
 * @mixin \Eloquent
 */
class ShopncMbFeedback extends BaseModel
{
    protected $table = 'shopnc_mb_feedback';

    public $timestamps = false;

    protected $fillable = [
        'content',
        'type',
        'ftime',
        'member_id',
        'member_name'
    ];

    protected $guarded = [];

        
}