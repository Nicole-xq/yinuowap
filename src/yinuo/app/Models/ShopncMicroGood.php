<?php

namespace App\Models;



/**
 * Class ShopncMicroGood
 *
 * @property int $commend_id 推荐编号
 * @property int $commend_member_id 推荐人用户编号
 * @property int $commend_goods_id 推荐商品编号
 * @property int $commend_goods_commonid 商品公共表id
 * @property int $commend_goods_store_id 推荐商品店铺编号
 * @property string $commend_goods_name 推荐商品名称
 * @property float $commend_goods_price 推荐商品价格
 * @property string $commend_goods_image 推荐商品图片
 * @property string $commend_message 推荐信息
 * @property int $commend_time 推荐时间
 * @property int $class_id
 * @property int $like_count 喜欢数
 * @property int $comment_count 评论数
 * @property int $click_count 点击数
 * @property int $microshop_commend 首页推荐 0-否 1-推荐
 * @property int $microshop_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereClickCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendGoodsStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommendTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereLikeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereMicroshopCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGood whereMicroshopSort($value)
 * @mixin \Eloquent
 */
class ShopncMicroGood extends BaseModel
{
    protected $table = 'shopnc_micro_goods';

    protected $primaryKey = 'commend_id';

	public $timestamps = false;

    protected $fillable = [
        'commend_member_id',
        'commend_goods_id',
        'commend_goods_commonid',
        'commend_goods_store_id',
        'commend_goods_name',
        'commend_goods_price',
        'commend_goods_image',
        'commend_message',
        'commend_time',
        'class_id',
        'like_count',
        'comment_count',
        'click_count',
        'microshop_commend',
        'microshop_sort'
    ];

    protected $guarded = [];

        
}