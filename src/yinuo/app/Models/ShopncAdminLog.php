<?php

namespace App\Models;



/**
 * Class ShopncAdminLog
 *
 * @property int $id
 * @property string $content 操作内容
 * @property int|null $createtime 发生时间
 * @property string $admin_name 管理员
 * @property int $admin_id 管理员ID
 * @property string $ip IP
 * @property string $url act&op
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereCreatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminLog whereUrl($value)
 * @mixin \Eloquent
 */
class ShopncAdminLog extends BaseModel
{
    protected $table = 'shopnc_admin_log';

    public $timestamps = false;

    protected $fillable = [
        'content',
        'createtime',
        'admin_name',
        'admin_id',
        'ip',
        'url'
    ];

    protected $guarded = [];

        
}