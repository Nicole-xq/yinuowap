<?php

namespace App\Models;



/**
 * Class ShopncSetting
 *
 * @property int $name 名称
 * @property string|null $value 值
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSetting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSetting whereValue($value)
 * @mixin \Eloquent
 */
class ShopncSetting extends BaseModel
{
    protected $table = 'shopnc_setting';

    protected $primaryKey = 'name';

	public $timestamps = false;

    protected $fillable = [
        'value'
    ];

    protected $guarded = [];

        
}