<?php

namespace App\Models;



/**
 * Class ShopncRefundDetail
 *
 * @property int $refund_id 记录ID
 * @property int $order_id 订单ID
 * @property string $batch_no 批次号
 * @property float|null $refund_amount 退款金额
 * @property float|null $pay_amount 在线退款金额
 * @property float|null $pd_amount 预存款金额
 * @property float|null $rcb_amount 充值卡金额
 * @property string $refund_code 退款支付代码
 * @property int|null $refund_state 退款状态:1为处理中,2为已完成,默认为1
 * @property int $add_time 添加时间
 * @property int|null $pay_time 在线退款完成时间,默认为0
 * @property float|null $points_amount 诺币金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereBatchNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail wherePayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail wherePdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail wherePointsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereRefundCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereRefundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundDetail whereRefundState($value)
 * @mixin \Eloquent
 */
class ShopncRefundDetail extends BaseModel
{
    protected $table = 'shopnc_refund_detail';

    protected $primaryKey = 'refund_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'batch_no',
        'refund_amount',
        'pay_amount',
        'pd_amount',
        'rcb_amount',
        'refund_code',
        'refund_state',
        'add_time',
        'pay_time',
        'points_amount'
    ];

    protected $guarded = [];

        
}