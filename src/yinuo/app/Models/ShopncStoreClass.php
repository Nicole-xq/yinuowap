<?php

namespace App\Models;



/**
 * Class ShopncStoreClass
 *
 * @property int $sc_id 索引ID
 * @property string $sc_name 分类名称
 * @property int $sc_bail 保证金数额
 * @property int $sc_sale 保证金优惠
 * @property int $sc_service_price 平台服务费
 * @property int $sc_service_price_sale 平台服务费优惠
 * @property int $sc_sort 排序
 * @property int $vendue_bail 拍卖保证金
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScBail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScServicePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScServicePriceSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereScSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreClass whereVendueBail($value)
 * @mixin \Eloquent
 */
class ShopncStoreClass extends BaseModel
{
    protected $table = 'shopnc_store_class';

    protected $primaryKey = 'sc_id';

	public $timestamps = false;

    protected $fillable = [
        'sc_name',
        'sc_bail',
        'sc_sale',
        'sc_service_price',
        'sc_service_price_sale',
        'sc_sort',
        'vendue_bail'
    ];

    protected $guarded = [];

        
}