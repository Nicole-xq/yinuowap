<?php

namespace App\Models;



/**
 * Class ShopncStatOrdergood
 *
 * @property int $rec_id 订单商品表索引id
 * @property int $stat_updatetime 缓存生成时间
 * @property int $order_id 订单id
 * @property int $order_sn 订单编号
 * @property int $order_add_time 订单生成时间
 * @property string|null $payment_code 支付方式
 * @property float $order_amount 订单总价格
 * @property float|null $shipping_fee 运费
 * @property string|null $evaluation_state 评价状态 0未评价，1已评价
 * @property string $order_state 订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;
 * @property int|null $refund_state 退款状态:0是无退款,1是部分退款,2是全部退款
 * @property float|null $refund_amount 退款金额
 * @property string $order_from 1WEB2mobile
 * @property int $order_isvalid 是否为计入统计的有效订单，0为无效 1为有效
 * @property int $reciver_province_id 收货人省级ID
 * @property int $store_id 店铺ID
 * @property string $store_name 卖家店铺名称
 * @property int|null $grade_id 店铺等级
 * @property int|null $sc_id 店铺分类
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家姓名
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称(+规格)
 * @property int $goods_commonid 商品公共表id
 * @property string|null $goods_commonname 商品公共表中商品名称
 * @property int $gc_id 商品最底级分类ID
 * @property int|null $gc_parentid_1 一级父类ID
 * @property int|null $gc_parentid_2 二级父类ID
 * @property int|null $gc_parentid_3 三级父类ID
 * @property int|null $brand_id 品牌id
 * @property string|null $brand_name 品牌名称
 * @property string|null $goods_serial 商家编号
 * @property float $goods_price 商品价格
 * @property int $goods_num 商品数量
 * @property string|null $goods_image 商品图片
 * @property float $goods_pay_price 商品实际成交价
 * @property string $goods_type 1默认2团购商品3限时折扣商品4优惠套装5赠品
 * @property int $promotions_id 促销活动ID（团购ID/限时折扣ID/优惠套装ID）与goods_type搭配使用
 * @property int $commis_rate 佣金比例
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereEvaluationState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGcParentid1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGcParentid2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGcParentid3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsCommonname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsPayPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGoodsType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderIsvalid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereOrderState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood wherePromotionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereReciverProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereShippingFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereStatUpdatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrdergood whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncStatOrdergood extends BaseModel
{
    protected $table = 'shopnc_stat_ordergoods';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'stat_updatetime',
        'order_id',
        'order_sn',
        'order_add_time',
        'payment_code',
        'order_amount',
        'shipping_fee',
        'evaluation_state',
        'order_state',
        'refund_state',
        'refund_amount',
        'order_from',
        'order_isvalid',
        'reciver_province_id',
        'store_id',
        'store_name',
        'grade_id',
        'sc_id',
        'buyer_id',
        'buyer_name',
        'goods_id',
        'goods_name',
        'goods_commonid',
        'goods_commonname',
        'gc_id',
        'gc_parentid_1',
        'gc_parentid_2',
        'gc_parentid_3',
        'brand_id',
        'brand_name',
        'goods_serial',
        'goods_price',
        'goods_num',
        'goods_image',
        'goods_pay_price',
        'goods_type',
        'promotions_id',
        'commis_rate'
    ];

    protected $guarded = [];

        
}