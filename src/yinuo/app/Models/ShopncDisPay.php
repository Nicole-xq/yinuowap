<?php

namespace App\Models;



/**
 * Class ShopncDisPay
 *
 * @property int $log_id 记录ID
 * @property int $order_goods_id 订单商品ID
 * @property int $order_id 订单ID
 * @property string $order_sn 订单编号
 * @property int $store_id 店铺ID
 * @property int $dis_member_id 分销会员ID
 * @property int $goods_id 商品ID
 * @property int $goods_commonid 商品公共表ID
 * @property string $goods_name 商品名称
 * @property string|null $goods_image 商品图片
 * @property int $add_time 添加时间
 * @property float $pay_goods_amount 已支付金额
 * @property float|null $refund_amount 退款金额
 * @property int $dis_commis_rate 分销佣金比例
 * @property float|null $dis_pay_amount 分销佣金
 * @property int|null $dis_pay_time 已结时间
 * @property int|null $log_state 结算状态:0为未结,1为已结
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereDisMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereDisPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereDisPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereLogState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereOrderGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay wherePayGoodsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisPay whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncDisPay extends BaseModel
{
    protected $table = 'shopnc_dis_pay';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'order_goods_id',
        'order_id',
        'order_sn',
        'store_id',
        'dis_member_id',
        'goods_id',
        'goods_commonid',
        'goods_name',
        'goods_image',
        'add_time',
        'pay_goods_amount',
        'refund_amount',
        'dis_commis_rate',
        'dis_pay_amount',
        'dis_pay_time',
        'log_state'
    ];

    protected $guarded = [];

        
}