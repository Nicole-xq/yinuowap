<?php

namespace App\Models;



/**
 * Class ShopncStoreWaybill
 *
 * @property int $store_waybill_id 店铺运单模板编号
 * @property int $store_id 店铺编号
 * @property int $express_id 物流公司编号
 * @property int $waybill_id 运单模板编号
 * @property string $waybill_name 运单模板名称
 * @property string|null $store_waybill_data 店铺自定义设置
 * @property int $is_default 是否默认模板
 * @property int $store_waybill_left 店铺运单左偏移
 * @property int $store_waybill_top 店铺运单上偏移
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereExpressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereStoreWaybillData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereStoreWaybillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereStoreWaybillLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereStoreWaybillTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereWaybillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWaybill whereWaybillName($value)
 * @mixin \Eloquent
 */
class ShopncStoreWaybill extends BaseModel
{
    protected $table = 'shopnc_store_waybill';

    protected $primaryKey = 'store_waybill_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'express_id',
        'waybill_id',
        'waybill_name',
        'store_waybill_data',
        'is_default',
        'store_waybill_left',
        'store_waybill_top'
    ];

    protected $guarded = [];

        
}