<?php

namespace App\Models;



/**
 * Class ShopncVoteActivityEntry
 *
 * @property int $id
 * @property int|null $entry_no 作品编号
 * @property int|null $member_id 会员id
 * @property int|null $activity_id 活动id
 * @property string|null $auther 作者
 * @property string|null $name 作品名称
 * @property string|null $description 描述
 * @property int|null $publish_id 管理员id
 * @property string|null $image
 * @property string|null $image_all
 * @property int|null $vote_num 票数
 * @property int|null $state 状态 0,待审核 1,审核通过 2,审核不通过 3,获奖 4,未获奖
 * @property string|null $modify_time
 * @property string|null $add_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereAuther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereEntryNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereImageAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry wherePublishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivityEntry whereVoteNum($value)
 * @mixin \Eloquent
 */
class ShopncVoteActivityEntry extends BaseModel
{
    protected $table = 'shopnc_vote_activity_entry';

    public $timestamps = false;

    protected $fillable = [
        'entry_no',
        'member_id',
        'activity_id',
        'auther',
        'name',
        'description',
        'publish_id',
        'image',
        'image_all',
        'vote_num',
        'state',
        'modify_time',
        'add_time'
    ];

    protected $guarded = [];

        
}