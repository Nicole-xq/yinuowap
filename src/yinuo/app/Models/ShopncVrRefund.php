<?php

namespace App\Models;



/**
 * Class ShopncVrRefund
 *
 * @property int $refund_id 记录ID
 * @property int $order_id 虚拟订单ID
 * @property string $order_sn 虚拟订单编号
 * @property string $refund_sn 申请编号
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家会员名
 * @property int $goods_id 商品ID
 * @property int|null $goods_num 退款商品数量
 * @property string $goods_name 商品名称
 * @property string|null $goods_image 商品图片
 * @property string $code_sn 兑换码编号
 * @property float|null $refund_amount 退款金额
 * @property int|null $admin_state 退款状态:1为待审核,2为同意,3为不同意,默认为1
 * @property int $add_time 添加时间
 * @property int|null $admin_time 管理员处理时间,默认为0
 * @property string|null $buyer_message 申请原因
 * @property string|null $admin_message 管理员备注
 * @property int|null $commis_rate 佣金比例
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereAdminMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereAdminState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereAdminTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereBuyerMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereCodeSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereRefundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereRefundSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefund whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncVrRefund extends BaseModel
{
    protected $table = 'shopnc_vr_refund';

    protected $primaryKey = 'refund_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_sn',
        'refund_sn',
        'store_id',
        'store_name',
        'buyer_id',
        'buyer_name',
        'goods_id',
        'goods_num',
        'goods_name',
        'goods_image',
        'code_sn',
        'refund_amount',
        'admin_state',
        'add_time',
        'admin_time',
        'buyer_message',
        'admin_message',
        'commis_rate'
    ];

    protected $guarded = [];

        
}