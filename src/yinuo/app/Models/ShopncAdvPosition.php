<?php

namespace App\Models;



/**
 * Class ShopncAdvPosition
 *
 * @property int $ap_id 广告位置id
 * @property string $ap_name 广告位置名
 * @property int $ap_class 广告类别：0图片1文字2幻灯3Flash
 * @property int $ap_display 广告展示方式：0幻灯片1多广告展示2单广告展示
 * @property int $is_use 广告位是否启用：0不启用1启用
 * @property int $ap_width 广告位宽度
 * @property int $ap_height 广告位高度
 * @property int $adv_num 拥有的广告数
 * @property int $click_num 广告位点击率
 * @property string $default_content 广告位默认内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereAdvNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApDisplay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereApWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereDefaultContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdvPosition whereIsUse($value)
 * @mixin \Eloquent
 */
class ShopncAdvPosition extends BaseModel
{
    protected $table = 'shopnc_adv_position';

    protected $primaryKey = 'ap_id';

	public $timestamps = false;

    protected $fillable = [
        'ap_name',
        'ap_class',
        'ap_display',
        'is_use',
        'ap_width',
        'ap_height',
        'adv_num',
        'click_num',
        'default_content'
    ];

    protected $guarded = [];

        
}