<?php

namespace App\Models;



/**
 * Class ShopncAgentStatement
 *
 * @property int $id
 * @property string|null $statement_no 对账单号
 * @property int $agent_id 代理id
 * @property string|null $title 标题
 * @property string|null $goods_amount 订单收益
 * @property string|null $goods_rate 订单返佣比例
 * @property string|null $margin_amount 保证金收益总额
 * @property string|null $margin_rate 考核比率
 * @property string|null $artist_amount 艺术家收益总额
 * @property string|null $artist_rate 艺术家考核比率
 * @property string|null $partner_amount 合伙人收益总额
 * @property string|null $partner_rate 合伙人考核比率
 * @property string|null $other_amount 额外奖惩
 * @property string|null $comment 备注
 * @property string|null $total_amount 总金额
 * @property int|null $state 状态(0:生成对账单,1:平台已审核,2:代理已确认,3:核对异常,4:已打款,5:代理确认收款,6:对账完成)
 * @property string|null $cdate
 * @property string|null $udate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereArtistAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereArtistRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereGoodsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereGoodsRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereMarginAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereMarginRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereOtherAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement wherePartnerAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement wherePartnerRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereStatementNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentStatement whereUdate($value)
 * @mixin \Eloquent
 */
class ShopncAgentStatement extends BaseModel
{
    protected $table = 'shopnc_agent_statement';

    public $timestamps = false;

    protected $fillable = [
        'statement_no',
        'agent_id',
        'title',
        'goods_amount',
        'goods_rate',
        'margin_amount',
        'margin_rate',
        'artist_amount',
        'artist_rate',
        'partner_amount',
        'partner_rate',
        'other_amount',
        'comment',
        'total_amount',
        'state',
        'cdate',
        'udate'
    ];

    protected $guarded = [];

        
}