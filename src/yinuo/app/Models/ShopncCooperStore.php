<?php

namespace App\Models;



/**
 * Class ShopncCooperStore
 *
 * @property int $cooper_store_id 合作商家ID
 * @property string|null $cooper_store_pic 合作商家图片
 * @property string $cooper_store_url 合作商家链接
 * @property int $cooper_store_sort 排序
 * @property int $is_show 是否显示 0否 1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCooperStore whereCooperStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCooperStore whereCooperStorePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCooperStore whereCooperStoreSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCooperStore whereCooperStoreUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCooperStore whereIsShow($value)
 * @mixin \Eloquent
 */
class ShopncCooperStore extends BaseModel
{
    protected $table = 'shopnc_cooper_store';

    protected $primaryKey = 'cooper_store_id';

	public $timestamps = false;

    protected $fillable = [
        'cooper_store_pic',
        'cooper_store_url',
        'cooper_store_sort',
        'is_show'
    ];

    protected $guarded = [];

        
}