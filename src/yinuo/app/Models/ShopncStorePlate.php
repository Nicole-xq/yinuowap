<?php

namespace App\Models;



/**
 * Class ShopncStorePlate
 *
 * @property int $plate_id 关联板式id
 * @property string $plate_name 关联板式名称
 * @property int $plate_position 关联板式位置 1顶部，0底部
 * @property string $plate_content 关联板式内容
 * @property int $store_id 所属店铺id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStorePlate wherePlateContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStorePlate wherePlateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStorePlate wherePlateName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStorePlate wherePlatePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStorePlate whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStorePlate extends BaseModel
{
    protected $table = 'shopnc_store_plate';

    protected $primaryKey = 'plate_id';

	public $timestamps = false;

    protected $fillable = [
        'plate_name',
        'plate_position',
        'plate_content',
        'store_id'
    ];

    protected $guarded = [];

        
}