<?php

namespace App\Models;



/**
 * Class ShopncMemberMovieLog
 *
 * @property int $log_id
 * @property string $content 内容
 * @property int $member_id 播主ID
 * @property string $member_name 播主名称
 * @property int $movie_state 直播状态 1是0否
 * @property string $movie_addtime 直播添加时间
 * @property string $movie_logout_time 直播退出时间
 * @property string|null $movie_cover_img 封面图
 * @property int|null $cate_id 分类ID
 * @property string|null $movie_title 标题
 * @property string $movie_off_time 关闭时间 1是 0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereCateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieCoverImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieLogoutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieOffTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMovieLog whereMovieTitle($value)
 * @mixin \Eloquent
 */
class ShopncMemberMovieLog extends BaseModel
{
    protected $table = 'shopnc_member_movie_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'content',
        'member_id',
        'member_name',
        'movie_state',
        'movie_addtime',
        'movie_logout_time',
        'movie_cover_img',
        'cate_id',
        'movie_title',
        'movie_off_time'
    ];

    protected $guarded = [];

        
}