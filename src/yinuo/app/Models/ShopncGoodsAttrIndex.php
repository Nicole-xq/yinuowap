<?php

namespace App\Models;



/**
 * Class ShopncGoodsAttrIndex
 *
 * @property int $goods_id 商品id
 * @property int $goods_commonid 商品公共表id
 * @property int $gc_id 商品分类id
 * @property int $type_id 类型id
 * @property int $attr_id 属性id
 * @property int $attr_value_id 属性值id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereAttrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereAttrValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsAttrIndex whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsAttrIndex extends BaseModel
{
    protected $table = 'shopnc_goods_attr_index';

    public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_commonid',
        'gc_id',
        'type_id',
        'attr_id',
        'attr_value_id'
    ];

    protected $guarded = [];

        
}