<?php

namespace App\Models;



/**
 * Class ShopncGoodsRecommend
 *
 * @property int $rec_id
 * @property int|null $rec_gc_id 最底级商品分类ID
 * @property int|null $rec_goods_id 商品goods_id
 * @property string|null $rec_gc_name 商品分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsRecommend whereRecGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsRecommend whereRecGcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsRecommend whereRecGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsRecommend whereRecId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsRecommend extends BaseModel
{
    protected $table = 'shopnc_goods_recommend';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'rec_gc_id',
        'rec_goods_id',
        'rec_gc_name'
    ];

    protected $guarded = [];

        
}