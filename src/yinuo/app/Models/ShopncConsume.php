<?php

namespace App\Models;



/**
 * Class ShopncConsume
 *
 * @property int $consume_id 消费表
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property float $consume_amount 金额
 * @property int $consume_time 时间
 * @property string $consume_remark 备注
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereConsumeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereConsumeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereConsumeRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereConsumeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncConsume whereMemberName($value)
 * @mixin \Eloquent
 */
class ShopncConsume extends BaseModel
{
    protected $table = 'shopnc_consume';

    protected $primaryKey = 'consume_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'consume_amount',
        'consume_time',
        'consume_remark'
    ];

    protected $guarded = [];

        
}