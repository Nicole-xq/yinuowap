<?php

namespace App\Models;



/**
 * Class ShopncMicroGoodsClass
 *
 * @property int $class_id 分类编号
 * @property string $class_name 分类名称
 * @property int|null $class_parent_id 父级分类编号
 * @property int $class_sort 排序
 * @property string|null $class_keyword 分类关键字
 * @property string|null $class_image 分类图片
 * @property int $class_commend 推荐标志0-不推荐 1-推荐到首页
 * @property int $class_default 默认标志，0-非默认 1-默认
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroGoodsClass whereClassSort($value)
 * @mixin \Eloquent
 */
class ShopncMicroGoodsClass extends BaseModel
{
    protected $table = 'shopnc_micro_goods_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_parent_id',
        'class_sort',
        'class_keyword',
        'class_image',
        'class_commend',
        'class_default'
    ];

    protected $guarded = [];

        
}