<?php

namespace App\Models;



/**
 * Class ShopncCircleTheme
 *
 * @property int $theme_id 主题id
 * @property string $theme_name 主题名称
 * @property string $theme_content 主题内容
 * @property int $circle_id 圈子id
 * @property string $circle_name 圈子名称
 * @property int $thclass_id 主题分类id
 * @property string|null $thclass_name 主题分类名称
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property int $is_identity 1圈主 2管理 3成员
 * @property string $theme_addtime 主题发表时间
 * @property string|null $theme_editname 编辑人名称
 * @property string|null $theme_edittime 主题编辑时间
 * @property int $theme_likecount 喜欢数量
 * @property int $theme_commentcount 评论数量
 * @property int $theme_browsecount 浏览数量
 * @property int $theme_sharecount 分享数量
 * @property int $is_stick 是否置顶 1是  0否
 * @property int $is_digest 是否加精 1是 0否
 * @property int|null $lastspeak_id 最后发言人id
 * @property string|null $lastspeak_name 最后发言人名称
 * @property string|null $lastspeak_time 最后发言时间
 * @property int $has_goods 商品标记 1是 0否
 * @property int $has_affix 附件标记 1是 0 否
 * @property int $is_closed 屏蔽 1是 0否
 * @property int $is_recommend 是否推荐 1是 0否
 * @property int $is_shut 主题是否关闭 1是 0否
 * @property int $theme_exp 获得经验
 * @property int $theme_readperm 阅读权限
 * @property int $theme_special 特殊话题 0普通 1投票
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereCircleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereHasAffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereHasGoods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsDigest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsShut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereIsStick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereLastspeakId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereLastspeakName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereLastspeakTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThclassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeBrowsecount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeCommentcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeEditname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeEdittime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeLikecount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeReadperm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeSharecount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleTheme whereThemeSpecial($value)
 * @mixin \Eloquent
 */
class ShopncCircleTheme extends BaseModel
{
    protected $table = 'shopnc_circle_theme';

    protected $primaryKey = 'theme_id';

	public $timestamps = false;

    protected $fillable = [
        'theme_name',
        'theme_content',
        'circle_id',
        'circle_name',
        'thclass_id',
        'thclass_name',
        'member_id',
        'member_name',
        'is_identity',
        'theme_addtime',
        'theme_editname',
        'theme_edittime',
        'theme_likecount',
        'theme_commentcount',
        'theme_browsecount',
        'theme_sharecount',
        'is_stick',
        'is_digest',
        'lastspeak_id',
        'lastspeak_name',
        'lastspeak_time',
        'has_goods',
        'has_affix',
        'is_closed',
        'is_recommend',
        'is_shut',
        'theme_exp',
        'theme_readperm',
        'theme_special'
    ];

    protected $guarded = [];

        
}