<?php

namespace App\Models;



/**
 * Class ShopncCircleMapply
 *
 * @property int $mapply_id 申请id
 * @property int $circle_id 圈子id
 * @property int $member_id 成员id
 * @property string $mapply_reason 申请理由
 * @property string $mapply_time 申请时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMapply whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMapply whereMapplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMapply whereMapplyReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMapply whereMapplyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMapply whereMemberId($value)
 * @mixin \Eloquent
 */
class ShopncCircleMapply extends BaseModel
{
    protected $table = 'shopnc_circle_mapply';

    protected $primaryKey = 'mapply_id';

	public $timestamps = false;

    protected $fillable = [
        'circle_id',
        'member_id',
        'mapply_reason',
        'mapply_time'
    ];

    protected $guarded = [];

        
}