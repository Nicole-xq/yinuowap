<?php

namespace App\Models;



/**
 * Class ShopncCircleExptheme
 *
 * @property int $theme_id 主题id
 * @property int $et_exp 获得经验
 * @property string $et_time 获得时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExptheme whereEtExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExptheme whereEtTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExptheme whereThemeId($value)
 * @mixin \Eloquent
 */
class ShopncCircleExptheme extends BaseModel
{
    protected $table = 'shopnc_circle_exptheme';

    protected $primaryKey = 'theme_id';

	public $timestamps = false;

    protected $fillable = [
        'et_exp',
        'et_time'
    ];

    protected $guarded = [];

        
}