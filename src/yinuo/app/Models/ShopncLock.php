<?php

namespace App\Models;



/**
 * Class ShopncLock
 *
 * @property int $pid IP+TYPE
 * @property int $pvalue 次数
 * @property int $expiretime 锁定截止时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLock whereExpiretime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLock wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLock wherePvalue($value)
 * @mixin \Eloquent
 */
class ShopncLock extends BaseModel
{
    protected $table = 'shopnc_locks';

    protected $primaryKey = 'pid';

	public $timestamps = false;

    protected $fillable = [
        'pvalue',
        'expiretime'
    ];

    protected $guarded = [];

        
}