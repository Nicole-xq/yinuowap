<?php

namespace App\Models;



/**
 * Class ShopncSlideshow
 *
 * @property int $id
 * @property string|null $title 标题
 * @property string|null $image 图片
 * @property string|null $url 链接
 * @property string|null $sort 排序
 * @property int|null $state 状态 0:不可用 1:可用
 * @property int|null $is_delete 是否删除 0:未删除 1:已删除
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSlideshow whereUrl($value)
 * @mixin \Eloquent
 */
class ShopncSlideshow extends BaseModel
{
    protected $table = 'shopnc_slideshow';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'image',
        'url',
        'sort',
        'state',
        'is_delete'
    ];

    protected $guarded = [];

        
}