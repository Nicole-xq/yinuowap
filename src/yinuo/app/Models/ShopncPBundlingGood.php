<?php

namespace App\Models;



/**
 * Class ShopncPBundlingGood
 *
 * @property int $bl_goods_id 组合商品id
 * @property int $bl_id 组合id
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property string $goods_image 商品图片
 * @property float $bl_goods_price 商品价格
 * @property int $bl_appoint 指定商品 1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereBlAppoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereBlGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereBlGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereBlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingGood whereGoodsName($value)
 * @mixin \Eloquent
 */
class ShopncPBundlingGood extends BaseModel
{
    protected $table = 'shopnc_p_bundling_goods';

    protected $primaryKey = 'bl_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'bl_id',
        'goods_id',
        'goods_name',
        'goods_image',
        'bl_goods_price',
        'bl_appoint'
    ];

    protected $guarded = [];

        
}