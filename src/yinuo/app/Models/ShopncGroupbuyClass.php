<?php

namespace App\Models;



/**
 * Class ShopncGroupbuyClass
 *
 * @property int $class_id 类别编号
 * @property string $class_name 类别名称
 * @property int $class_parent_id 父类别编号
 * @property int $sort 排序
 * @property int|null $deep 深度
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyClass whereClassParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyClass whereDeep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuyClass whereSort($value)
 * @mixin \Eloquent
 */
class ShopncGroupbuyClass extends BaseModel
{
    protected $table = 'shopnc_groupbuy_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_parent_id',
        'sort',
        'deep'
    ];

    protected $guarded = [];

        
}