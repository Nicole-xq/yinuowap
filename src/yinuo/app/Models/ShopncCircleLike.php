<?php

namespace App\Models;



/**
 * Class ShopncCircleLike
 *
 * @property int $theme_id 主题id
 * @property int $member_id 会员id
 * @property int $circle_id 圈子id
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike whereThemeId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleLike where#alibabaRdsRowId#($value)
 */
class ShopncCircleLike extends BaseModel
{
    protected $table = 'shopnc_circle_like';

    public $timestamps = false;

    protected $fillable = [
        'theme_id',
        'member_id',
        'circle_id',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}