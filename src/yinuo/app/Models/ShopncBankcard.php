<?php

namespace App\Models;



/**
 * Class ShopncBankcard
 *
 * @property int $bankcard_id 银行卡ID
 * @property int $member_id 会员ID
 * @property string $member_name 会员名称
 * @property string $true_name 真实姓名
 * @property string $member_phone 手机号
 * @property string $bank_card 银行卡号
 * @property string|null $card_type 卡种
 * @property string|null $bank_name 银行名称
 * @property string|null $bank_image 银行logo
 * @property string $id_number 身份证号
 * @property int|null $is_default 1默认优选
 * @property int|null $is_delete 是否删除：1是、0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereBankCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereBankImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereBankcardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereIdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereMemberPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBankcard whereTrueName($value)
 * @mixin \Eloquent
 */
class ShopncBankcard extends BaseModel
{
    protected $table = 'shopnc_bankcard';

    protected $primaryKey = 'bankcard_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'true_name',
        'member_phone',
        'bank_card',
        'card_type',
        'bank_name',
        'bank_image',
        'id_number',
        'is_default',
        'is_delete'
    ];

    protected $guarded = [];

        
}