<?php

namespace App\Models;



/**
 * Class ShopncMemberMsgSetting
 *
 * @property string $mmt_code 用户消息模板编号
 * @property int $member_id 会员id
 * @property int $is_receive 是否接收 1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgSetting whereIsReceive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgSetting whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberMsgSetting whereMmtCode($value)
 * @mixin \Eloquent
 */
class ShopncMemberMsgSetting extends BaseModel
{
    protected $table = 'shopnc_member_msg_setting';

    public $timestamps = false;

    protected $fillable = [
        'mmt_code',
        'member_id',
        'is_receive'
    ];

    protected $guarded = [];

        
}