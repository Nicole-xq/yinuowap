<?php

namespace App\Models;



/**
 * Class ShopncVideoCategory
 *
 * @property int $cate_id 索引ID
 * @property string $cate_name 分类名称
 * @property int|null $type_id 类型id
 * @property string|null $type_name 类型名称
 * @property int $cate_parent_id 父ID
 * @property int $cate_sort 排序
 * @property string $cate_image 分类图片
 * @property string $cate_description 描述
 * @property int $is_recommend 是否推荐，前台显示 0否1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereCateSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoCategory whereTypeName($value)
 * @mixin \Eloquent
 */
class ShopncVideoCategory extends BaseModel
{
    protected $table = 'shopnc_video_category';

    protected $primaryKey = 'cate_id';

	public $timestamps = false;

    protected $fillable = [
        'cate_name',
        'type_id',
        'type_name',
        'cate_parent_id',
        'cate_sort',
        'cate_image',
        'cate_description',
        'is_recommend'
    ];

    protected $guarded = [];

        
}