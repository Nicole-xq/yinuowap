<?php

namespace App\Models;



/**
 * Class ShopncVrOrderStati
 *
 * @property int $os_month 统计编号(年月)
 * @property int|null $os_year 年
 * @property int $os_start_date 开始日期
 * @property int $os_end_date 结束日期
 * @property float $os_order_totals 订单金额
 * @property float $os_commis_totals 佣金金额
 * @property float $os_result_totals 本期应结
 * @property int|null $os_create_date 创建记录日期
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsCommisTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsOrderTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsResultTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderStati whereOsYear($value)
 * @mixin \Eloquent
 */
class ShopncVrOrderStati extends BaseModel
{
    protected $table = 'shopnc_vr_order_statis';

    protected $primaryKey = 'os_month';

	public $timestamps = false;

    protected $fillable = [
        'os_year',
        'os_start_date',
        'os_end_date',
        'os_order_totals',
        'os_commis_totals',
        'os_result_totals',
        'os_create_date'
    ];

    protected $guarded = [];

        
}