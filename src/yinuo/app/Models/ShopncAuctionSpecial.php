<?php

namespace App\Models;



/**
 * Class ShopncAuctionSpecial
 *
 * @property int $special_id 专场ID
 * @property string $special_name 专场名称
 * @property int|null $special_type 拍品类型: 0,普通拍品,1,新手拍品
 * @property int|null $store_id 商家ID
 * @property string|null $store_name 商家名称
 * @property int|null $store_vendue_id 商家拍卖ID
 * @property int $special_add_time 专场添加时间
 * @property int $special_start_time 专场开始时间
 * @property int $special_end_time 专场结束时间
 * @property int $special_preview_start 预展开始时间
 * @property string $special_image 专场缩略图
 * @property string $adv_image 专场广告图
 * @property string|null $wap_image wap专场图
 * @property int $special_sponsor 发起者 0平台 1店铺
 * @property string|null $delivery_mechanism 送拍机构
 * @property int|null $auction_num 拍品数量
 * @property int|null $special_state 审核状态 10 未审核 20 审核成功 30审核失败 11 预展开始 12专场开始 13 专场结束
 * @property string|null $special_check_message 审核意见
 * @property int|null $is_open 是否开启 1开启 0关闭
 * @property int|null $special_click 专场围观数
 * @property int|null $participants 专场参与量
 * @property int|null $is_rec 是否推荐 0否 1是
 * @property float|null $special_rate 专场统一利率
 * @property int|null $special_rate_time 计息时间(超过此时间,将不再获得保证金收益)
 * @property float|null $partner_lv_1_wdl_rate 直属上级为微代理时,额外返佣比率
 * @property float|null $partner_lv_1_zl_rate 直属上级为战略合伙人时,额外返佣比率
 * @property float|null $partner_lv_1_sy_rate 直属上级为事业合伙人时,额外返佣比率
 * @property float|null $partner_lv_2_wdl_rate 直属上级的上级为微代理时,额外返佣比率
 * @property float|null $partner_lv_2_zl_rate 直属上级的上级为战略合伙人时,额外返佣比率
 * @property float|null $partner_lv_2_sy_rate 直属上级的上级为事业合伙人时,额外返佣比率
 * @property string|null $share_content 分享描述
 * @property int|null $is_pd_pay 是否诺币专场0不是1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereAdvImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereAuctionNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereDeliveryMechanism($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereIsOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereIsPdPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereIsRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereParticipants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv1SyRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv1WdlRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv1ZlRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv2SyRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv2WdlRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial wherePartnerLv2ZlRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereShareContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialCheckMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialPreviewStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialRateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialSponsor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereSpecialType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereStoreVendueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial whereWapImage($value)
 * @mixin \Eloquent
 */
class ShopncAuctionSpecial extends BaseModel
{

    //拍品类型: 0,普通拍品,1,新手拍品 2.新手专区
    const SPECIAL_TYPE_COMMON = 0;
    const SPECIAL_TYPE_NEW = 1;
    const SPECIAL_TYPE_NEWBIE = 2;

    //审核状态 10 未审核 20 审核成功 30审核失败 11 预展开始 12专场开始 13 专场结束
    const SPECIAL_STATE_NOT_AUDIT = 10;
    const SPECIAL_STATE_AUDIT_SUCCESS = 20;
    const SPECIAL_STATE_AUDIT_FAILURE = 30;
    const SPECIAL_STATE_PREVIEW_STARTED = 11;
    const SPECIAL_STATE_SPECIAL_STARTED = 12;
    const SPECIAL_STATE_SPECIAL_END = 13;

    public static $specialTypeText = [
        self::SPECIAL_TYPE_COMMON => "普通拍品",
        self::SPECIAL_TYPE_NEW => "新手拍品",
        self::SPECIAL_TYPE_NEWBIE => "新手专区",
    ];

    protected $table = 'shopnc_auction_special';

    protected $primaryKey = 'special_id';

	public $timestamps = false;

    protected $fillable = [
        'special_name',
        'special_type',
        'store_id',
        'store_name',
        'store_vendue_id',
        'special_add_time',
        'special_start_time',
        'special_end_time',
        'special_preview_start',
        'special_image',
        'adv_image',
        'wap_image',
        'special_sponsor',
        'delivery_mechanism',
        'auction_num',
        'special_state',
        'special_check_message',
        'is_open',
        'special_click',
        'participants',
        'is_rec',
        'special_rate',
        'special_rate_time',
        'partner_lv_1_wdl_rate',
        'partner_lv_1_zl_rate',
        'partner_lv_1_sy_rate',
        'partner_lv_2_wdl_rate',
        'partner_lv_2_zl_rate',
        'partner_lv_2_sy_rate',
        'share_content',
        'is_pd_pay'
    ];

    protected $guarded = [];

        
}