<?php

namespace App\Models;



/**
 * Class ShopncMessage
 *
 * @property int $message_id 短消息索引id
 * @property int $message_parent_id 回复短消息message_id
 * @property int $from_member_id 短消息发送人
 * @property string $to_member_id 短消息接收人
 * @property string|null $message_title 短消息标题
 * @property string $message_body 短消息内容
 * @property string $message_time 短消息发送时间
 * @property string|null $message_update_time 短消息回复更新时间
 * @property int $message_open 短消息打开状态
 * @property int $message_state 短消息状态，0为正常状态，1为发送人删除状态，2为接收人删除状态
 * @property int $message_type 0为私信、1为系统消息、2为留言
 * @property string|null $read_member_id 已经读过该消息的会员id
 * @property string|null $del_member_id 已经删除该消息的会员id
 * @property int $message_ismore 站内信是否为一条发给多个用户 0为否 1为多条
 * @property string|null $from_member_name 发信息人用户名
 * @property string|null $to_member_name 接收人用户名
 * @property string|null $mmt_code 用户消息模板编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereDelMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereFromMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereFromMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageIsmore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMessageUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereMmtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereReadMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereToMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMessage whereToMemberName($value)
 * @mixin \Eloquent
 */
class ShopncMessage extends BaseModel
{
    protected $table = 'shopnc_message';

    protected $primaryKey = 'message_id';

	public $timestamps = false;

    protected $fillable = [
        'message_parent_id',
        'from_member_id',
        'to_member_id',
        'message_title',
        'message_body',
        'message_time',
        'message_update_time',
        'message_open',
        'message_state',
        'message_type',
        'read_member_id',
        'del_member_id',
        'message_ismore',
        'from_member_name',
        'to_member_name',
        'mmt_code'
    ];

    protected $guarded = [];

        
}