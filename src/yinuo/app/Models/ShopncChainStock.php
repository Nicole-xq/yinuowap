<?php

namespace App\Models;



/**
 * Class ShopncChainStock
 *
 * @property int $chain_id 门店id
 * @property int $goods_id 商品id
 * @property int $goods_commonid 商品SPU
 * @property int $stock 库存
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChainStock whereChainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChainStock whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChainStock whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChainStock whereStock($value)
 * @mixin \Eloquent
 */
class ShopncChainStock extends BaseModel
{
    protected $table = 'shopnc_chain_stock';

    public $timestamps = false;

    protected $fillable = [
        'chain_id',
        'goods_id',
        'goods_commonid',
        'stock'
    ];

    protected $guarded = [];

        
}