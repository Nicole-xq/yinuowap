<?php

namespace App\Models;



/**
 * Class ShopncStoreReopen
 *
 * @property int $re_id
 * @property int $re_grade_id 店铺等级ID
 * @property string|null $re_grade_name 等级名称
 * @property float $re_grade_price 等级收费(元/年)
 * @property int $re_year 续签时长(年)
 * @property float $re_pay_amount 应付总金额
 * @property string|null $re_store_name 店铺名字
 * @property int $re_store_id 店铺ID
 * @property int $re_state 状态0默认，未上传凭证1审核中2审核通过
 * @property int|null $re_start_time 有效期开始时间
 * @property int|null $re_end_time 有效期结束时间
 * @property int $re_create_time 记录创建时间
 * @property string|null $re_pay_cert 付款凭证
 * @property string|null $re_pay_cert_explain 付款凭证说明
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReGradeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReGradePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereRePayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereRePayCert($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereRePayCertExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreReopen whereReYear($value)
 * @mixin \Eloquent
 */
class ShopncStoreReopen extends BaseModel
{
    protected $table = 'shopnc_store_reopen';

    protected $primaryKey = 're_id';

	public $timestamps = false;

    protected $fillable = [
        're_grade_id',
        're_grade_name',
        're_grade_price',
        're_year',
        're_pay_amount',
        're_store_name',
        're_store_id',
        're_state',
        're_start_time',
        're_end_time',
        're_create_time',
        're_pay_cert',
        're_pay_cert_explain'
    ];

    protected $guarded = [];

        
}