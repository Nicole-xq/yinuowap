<?php

namespace App\Models;



/**
 * Class ShopncPaintModule
 *
 * @property int $paint_module_id 书画模块名称ID
 * @property string $paint_module_name 书画模块名称
 * @property int $paint_module_sort 排序
 * @property int $paint_module_show 是否显示 1是 0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintModule wherePaintModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintModule wherePaintModuleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintModule wherePaintModuleShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintModule wherePaintModuleSort($value)
 * @mixin \Eloquent
 */
class ShopncPaintModule extends BaseModel
{
    protected $table = 'shopnc_paint_module';

    protected $primaryKey = 'paint_module_id';

	public $timestamps = false;

    protected $fillable = [
        'paint_module_name',
        'paint_module_sort',
        'paint_module_show'
    ];

    protected $guarded = [];

        
}