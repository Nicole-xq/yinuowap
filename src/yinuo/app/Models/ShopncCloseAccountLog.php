<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * Class ShopncCloseAccountLog
 *
 * @property int $id 商家结算账户变更记录表
 * @property int $store_id 用户ID
 * @property int $transaction_type 交易类型 1订单结算入账 2商家出账
 * @property float $before_wait_amount 变更前余额
 * @property float $change_wait_amount 变更金额
 * @property float $later_wait_amount 变更后金额
 * @property int $io_type 变更类型 1进 -1出
 * @property float $fee 手续费
 * @property string $note 备注
 * @property int $status 1有效 0无效
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereBeforeWaitAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereChangeWaitAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereIoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereLaterWaitAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereTransactionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $transaction_source 交易类型对应的源信息
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccountLog whereTransactionSource($value)
 */
class ShopncCloseAccountLog extends BaseModel
{
    //变更类型 1进 -1出
    const IO_TYPE_IN = 1;
    const IO_TYPE_OUT = -1;

    //交易类型 1订单结算入账 2商家出账
    const TRANSACTION_TYPE_FROM_ORDER = 1;
    const TRANSACTION_TYPE_TO_MEMBER_ACCOUNT = 2;

    const STATUS_VALID = 1;


    protected $table = 'shopnc_close_account_log';

    public $timestamps = true;

    protected $fillable = [
        'store_id',
        'transaction_type',
        'transaction_source',
        'before_wait_amount',
        'change_wait_amount',
        'later_wait_amount',
        'io_type',
        'fee',
        'note',
        'status'
    ];

    protected $guarded = [];

        
}