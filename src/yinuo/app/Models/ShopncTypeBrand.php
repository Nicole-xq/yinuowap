<?php

namespace App\Models;



/**
 * Class ShopncTypeBrand
 *
 * @property int $type_id 类型id
 * @property int $brand_id 品牌id
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand whereTypeId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTypeBrand where#alibabaRdsRowId#($value)
 */
class ShopncTypeBrand extends BaseModel
{
    protected $table = 'shopnc_type_brand';

    public $timestamps = false;

    protected $fillable = [
        'type_id',
        'brand_id',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}