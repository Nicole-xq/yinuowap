<?php

namespace App\Models;



/**
 * Class ShopncVoteEntryLog
 *
 * @property int $log_id
 * @property int $member_id 用户ID
 * @property int $activity_id 活动表ID
 * @property int $entry_id 投票ID
 * @property int|null $vote_num
 * @property int|null $add_time 添加时间
 * @property int|null $modify_time 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereEntryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteEntryLog whereVoteNum($value)
 * @mixin \Eloquent
 */
class ShopncVoteEntryLog extends BaseModel
{
    protected $table = 'shopnc_vote_entry_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'activity_id',
        'entry_id',
        'vote_num',
        'add_time',
        'modify_time'
    ];

    protected $guarded = [];

        
}