<?php

namespace App\Models;



/**
 * Class ShopncCmsArticleAttitude
 *
 * @property int $attitude_id 心情编号
 * @property int $attitude_article_id 文章编号
 * @property int $attitude_member_id 用户编号
 * @property int $attitude_time 发布心情时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleAttitude whereAttitudeArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleAttitude whereAttitudeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleAttitude whereAttitudeMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticleAttitude whereAttitudeTime($value)
 * @mixin \Eloquent
 */
class ShopncCmsArticleAttitude extends BaseModel
{
    protected $table = 'shopnc_cms_article_attitude';

    protected $primaryKey = 'attitude_id';

	public $timestamps = false;

    protected $fillable = [
        'attitude_article_id',
        'attitude_member_id',
        'attitude_time'
    ];

    protected $guarded = [];

        
}