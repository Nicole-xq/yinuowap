<?php

namespace App\Models;



/**
 * Class ShopncScOperatLog
 *
 * @property int $sc_operat_id 专场操作日志ID
 * @property int $special_id
 * @property int|null $save_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScOperatLog whereSaveTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScOperatLog whereScOperatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncScOperatLog whereSpecialId($value)
 * @mixin \Eloquent
 */
class ShopncScOperatLog extends BaseModel
{
    protected $table = 'shopnc_sc_operat_log';

    protected $primaryKey = 'sc_operat_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'save_time'
    ];

    protected $guarded = [];

        
}