<?php

namespace App\Models;



/**
 * Class ShopncAdv
 *
 * @property int $adv_id 广告自增标识编号
 * @property int $ap_id 广告位id
 * @property string $adv_title 广告内容描述
 * @property string $adv_content 广告内容
 * @property int|null $adv_start_date 广告开始时间
 * @property int|null $adv_end_date 广告结束时间
 * @property int $slide_sort 幻灯片排序
 * @property int $member_id 会员ID
 * @property string $member_name 会员用户名
 * @property int $click_num 广告点击率
 * @property int $is_allow 会员购买的广告是否通过审核0未审核1审核已通过2审核未通过
 * @property string $buy_style 购买方式
 * @property int $goldpay 购买所支付的金币
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereAdvContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereAdvEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereAdvId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereAdvStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereAdvTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereBuyStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereGoldpay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereIsAllow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdv whereSlideSort($value)
 * @mixin \Eloquent
 */
class ShopncAdv extends BaseModel
{
    protected $table = 'shopnc_adv';

    protected $primaryKey = 'adv_id';

	public $timestamps = false;

    protected $fillable = [
        'ap_id',
        'adv_title',
        'adv_content',
        'adv_start_date',
        'adv_end_date',
        'slide_sort',
        'member_id',
        'member_name',
        'click_num',
        'is_allow',
        'buy_style',
        'goldpay'
    ];

    protected $guarded = [];

        
}