<?php

namespace App\Models;



/**
 * Class ShopncMarginList
 *
 * @property int $margin_list_id 总结算ID
 * @property string|null $list_sn 结算编号
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家名字
 * @property float|null $all_money 总金额
 * @property int|null $pay_num 支付次数
 * @property string|null $last_payment_code 最后支付方式
 * @property int|null $last_pay_time 最后支付时间
 * @property int|null $auction_id 该拍品ID的保证金
 * @property int $statue 状态：1正常2抵扣3部分违约4违约
 * @property int|null $create_time 生产时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereAllMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereLastPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereLastPaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereListSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereMarginListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList wherePayNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginList whereStatue($value)
 * @mixin \Eloquent
 */
class ShopncMarginList extends BaseModel
{
    protected $table = 'shopnc_margin_list';

    protected $primaryKey = 'margin_list_id';

	public $timestamps = false;

    protected $fillable = [
        'list_sn',
        'buyer_id',
        'buyer_name',
        'all_money',
        'pay_num',
        'last_payment_code',
        'last_pay_time',
        'auction_id',
        'statue',
        'create_time'
    ];

    protected $guarded = [];

        
}