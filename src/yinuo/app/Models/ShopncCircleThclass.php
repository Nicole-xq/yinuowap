<?php

namespace App\Models;



/**
 * Class ShopncCircleThclass
 *
 * @property int $thclass_id 主题分类id
 * @property string $thclass_name 主题名称
 * @property int $thclass_status 主题状态 1开启，0关闭
 * @property int $is_moderator 管理专属 1是，0否
 * @property int $thclass_sort 分类排序
 * @property int $circle_id 所属圈子id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereIsModerator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereThclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereThclassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereThclassSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThclass whereThclassStatus($value)
 * @mixin \Eloquent
 */
class ShopncCircleThclass extends BaseModel
{
    protected $table = 'shopnc_circle_thclass';

    protected $primaryKey = 'thclass_id';

	public $timestamps = false;

    protected $fillable = [
        'thclass_name',
        'thclass_status',
        'is_moderator',
        'thclass_sort',
        'circle_id'
    ];

    protected $guarded = [];

        
}