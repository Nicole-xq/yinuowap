<?php

namespace App\Models;



/**
 * Class ShopncStoreNavigation
 *
 * @property int $sn_id 导航ID
 * @property string $sn_title 导航名称
 * @property int $sn_store_id 卖家店铺ID
 * @property string|null $sn_content 导航内容
 * @property int $sn_sort 导航排序
 * @property int $sn_if_show 导航是否显示
 * @property int $sn_add_time 导航
 * @property string|null $sn_url 店铺导航的外链URL
 * @property int $sn_new_open 店铺导航外链是否在新窗口打开：0不开新窗口1开新窗口，默认是0
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnIfShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnNewOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreNavigation whereSnUrl($value)
 * @mixin \Eloquent
 */
class ShopncStoreNavigation extends BaseModel
{
    protected $table = 'shopnc_store_navigation';

    protected $primaryKey = 'sn_id';

	public $timestamps = false;

    protected $fillable = [
        'sn_title',
        'sn_store_id',
        'sn_content',
        'sn_sort',
        'sn_if_show',
        'sn_add_time',
        'sn_url',
        'sn_new_open'
    ];

    protected $guarded = [];

        
}