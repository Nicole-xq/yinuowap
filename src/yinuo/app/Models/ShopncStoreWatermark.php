<?php

namespace App\Models;



/**
 * Class ShopncStoreWatermark
 *
 * @property int $wm_id 水印id
 * @property int $jpeg_quality jpeg图片质量
 * @property string|null $wm_image_name 水印图片的路径以及文件名
 * @property int $wm_image_pos 水印图片放置的位置
 * @property int $wm_image_transition 水印图片与原图片的融合度
 * @property string|null $wm_text 水印文字
 * @property int $wm_text_size 水印文字大小
 * @property int $wm_text_angle 水印文字角度
 * @property int $wm_text_pos 水印文字放置位置
 * @property string|null $wm_text_font 水印文字的字体
 * @property string $wm_text_color 水印字体的颜色值
 * @property int $wm_is_open 水印是否开启 0关闭 1开启
 * @property int|null $store_id 店铺id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereJpegQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmImageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmImagePos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmImageTransition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmIsOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmTextAngle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmTextColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmTextFont($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmTextPos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreWatermark whereWmTextSize($value)
 * @mixin \Eloquent
 */
class ShopncStoreWatermark extends BaseModel
{
    protected $table = 'shopnc_store_watermark';

    protected $primaryKey = 'wm_id';

	public $timestamps = false;

    protected $fillable = [
        'jpeg_quality',
        'wm_image_name',
        'wm_image_pos',
        'wm_image_transition',
        'wm_text',
        'wm_text_size',
        'wm_text_angle',
        'wm_text_pos',
        'wm_text_font',
        'wm_text_color',
        'wm_is_open',
        'store_id'
    ];

    protected $guarded = [];

        
}