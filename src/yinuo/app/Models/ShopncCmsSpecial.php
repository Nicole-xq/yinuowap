<?php

namespace App\Models;



/**
 * Class ShopncCmsSpecial
 *
 * @property int $special_id 专题编号
 * @property string $special_title 专题标题
 * @property string|null $special_description 描述(分享用)
 * @property int|null $special_margin_top 正文距顶部距离
 * @property string|null $special_background 专题背景
 * @property string|null $special_image 专题封面图
 * @property string|null $special_image_all 专题图片
 * @property string|null $special_content 专题内容
 * @property string|null $special_mobile_content 专题内容(手机排版)
 * @property string|null $special_share_logo 微信分享logo
 * @property int $special_modify_time 专题修改时间
 * @property int $special_publish_id 专题发布者编号
 * @property int $special_state 专题状态1-草稿、2-已发布
 * @property string $special_background_color 专题背景色
 * @property string $special_repeat 背景重复方式
 * @property int $special_type 专题类型(1-cms专题 2-商城专题)
 * @property int|null $special_add_time 专题添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialBackgroundColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialImageAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialMarginTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialMobileContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialPublishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialRepeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialShareLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsSpecial whereSpecialType($value)
 * @mixin \Eloquent
 */
class ShopncCmsSpecial extends BaseModel
{
    protected $table = 'shopnc_cms_special';

    protected $primaryKey = 'special_id';

	public $timestamps = false;

    protected $fillable = [
        'special_title',
        'special_description',
        'special_margin_top',
        'special_background',
        'special_image',
        'special_image_all',
        'special_content',
        'special_mobile_content',
        'special_share_logo',
        'special_modify_time',
        'special_publish_id',
        'special_state',
        'special_background_color',
        'special_repeat',
        'special_type',
        'special_add_time'
    ];

    protected $guarded = [];

        
}