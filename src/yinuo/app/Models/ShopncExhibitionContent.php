<?php

namespace App\Models;



/**
 * Class ShopncExhibitionContent
 *
 * @property int $id 会销编号
 * @property string $title 标题
 * @property int|null $apply_num 报名人数
 * @property string|null $description 描述(分享用)
 * @property int|null $margin_top 正文距顶部距离
 * @property string|null $background 背景
 * @property string|null $image 封面图
 * @property string|null $image_all 图片
 * @property string|null $content 会销内容
 * @property string|null $share_logo 微信分享logo
 * @property int $modify_time 会销修改时间
 * @property int $publish_id 会销发布者编号
 * @property int $state 会销状态1-草稿、2-已发布
 * @property string $background_color 会销背景色
 * @property string $repeat 背景重复方式
 * @property int $add_time 会销添加时间
 * @property int $entry_end_time 报名截止时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereApplyNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereBackgroundColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereEntryEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereImageAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereMarginTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent wherePublishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereRepeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereShareLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionContent whereTitle($value)
 * @mixin \Eloquent
 */
class ShopncExhibitionContent extends BaseModel
{
    protected $table = 'shopnc_exhibition_content';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'apply_num',
        'description',
        'margin_top',
        'background',
        'image',
        'image_all',
        'content',
        'share_logo',
        'modify_time',
        'publish_id',
        'state',
        'background_color',
        'repeat',
        'add_time',
        'entry_end_time'
    ];

    protected $guarded = [];

        
}