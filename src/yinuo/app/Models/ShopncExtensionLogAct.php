<?php

namespace App\Models;



/**
 * Class ShopncExtensionLogAct
 *
 * @property int $id 行为表id
 * @property string|null $open_id 微信open_id
 * @property string|null $extension_code 推广code
 * @property string|null $record 记录行为
 * @property int|null $add_time 操作时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLogAct whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLogAct whereExtensionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLogAct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLogAct whereOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLogAct whereRecord($value)
 * @mixin \Eloquent
 */
class ShopncExtensionLogAct extends BaseModel
{
    protected $table = 'shopnc_extension_log_act';

    public $timestamps = false;

    protected $fillable = [
        'open_id',
        'extension_code',
        'record',
        'add_time'
    ];

    protected $guarded = [];

        
}