<?php

namespace App\Models;



/**
 * Class ShopncRobotCopy
 *
 * @property int $robot_id 机器人ID
 * @property string|null $robot_name 机器人名称
 * @property int $update_time 更新时间
 * @property string|null $description 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobotCopy whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobotCopy whereRobotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobotCopy whereRobotName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobotCopy whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncRobotCopy extends BaseModel
{
    protected $table = 'shopnc_robot_copy';

    protected $primaryKey = 'robot_id';

	public $timestamps = false;

    protected $fillable = [
        'robot_name',
        'update_time',
        'description'
    ];

    protected $guarded = [];

        
}