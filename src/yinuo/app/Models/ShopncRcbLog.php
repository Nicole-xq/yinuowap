<?php

namespace App\Models;



/**
 * Class ShopncRcbLog
 *
 * @property int $id 自增编号
 * @property int $member_id 会员编号
 * @property string $member_name 会员名称
 * @property string $type order_pay下单使用 order_freeze下单冻结 order_cancel取消订单解冻 order_comb_pay下单扣除被冻结 recharge平台充值卡充值 refund确认退款 vr_refund虚拟兑码退款
 * @property int $add_time 添加时间
 * @property float $available_amount 可用充值卡余额变更 0表示未变更
 * @property float $freeze_amount 冻结充值卡余额变更 0表示未变更
 * @property string|null $description 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereAvailableAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereFreezeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRcbLog whereType($value)
 * @mixin \Eloquent
 */
class ShopncRcbLog extends BaseModel
{
    protected $table = 'shopnc_rcb_log';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'type',
        'add_time',
        'available_amount',
        'freeze_amount',
        'description'
    ];

    protected $guarded = [];

        
}