<?php

namespace App\Models;



/**
 * Class ShopncGoodsClassTag
 *
 * @property int $gc_tag_id TAGid
 * @property int $gc_id_1 一级分类id
 * @property int $gc_id_2 二级分类id
 * @property int $gc_id_3 三级分类id
 * @property string $gc_tag_name 分类TAG名称
 * @property string $gc_tag_value 分类TAG值
 * @property int $gc_id 商品分类id
 * @property int $type_id 类型id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcTagName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereGcTagValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassTag whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsClassTag extends BaseModel
{
    protected $table = 'shopnc_goods_class_tag';

    protected $primaryKey = 'gc_tag_id';

	public $timestamps = false;

    protected $fillable = [
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        'gc_tag_name',
        'gc_tag_value',
        'gc_id',
        'type_id'
    ];

    protected $guarded = [];

        
}