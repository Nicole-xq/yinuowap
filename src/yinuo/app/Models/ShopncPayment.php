<?php

namespace App\Models;



/**
 * Class ShopncPayment
 *
 * @property int $payment_id 支付索引id
 * @property string $payment_code 支付代码名称
 * @property string $payment_name 支付名称
 * @property string|null $payment_config 支付接口配置信息
 * @property string $payment_state 接口状态0禁用1启用
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPayment wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPayment wherePaymentConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPayment wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPayment wherePaymentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPayment wherePaymentState($value)
 * @mixin \Eloquent
 */
class ShopncPayment extends BaseModel
{
    protected $table = 'shopnc_payment';

    protected $primaryKey = 'payment_id';

	public $timestamps = false;

    protected $fillable = [
        'payment_code',
        'payment_name',
        'payment_config',
        'payment_state'
    ];

    protected $guarded = [];

        
}