<?php

namespace App\Models;



/**
 * Class ShopncCmsModuleFrame
 *
 * @property int $frame_id 框架编号
 * @property string $frame_title 框架标题
 * @property string $frame_name 框架名称
 * @property string $frame_explain 框架说明
 * @property string $frame_structure 框架结构
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleFrame whereFrameExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleFrame whereFrameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleFrame whereFrameName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleFrame whereFrameStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleFrame whereFrameTitle($value)
 * @mixin \Eloquent
 */
class ShopncCmsModuleFrame extends BaseModel
{
    protected $table = 'shopnc_cms_module_frame';

    protected $primaryKey = 'frame_id';

	public $timestamps = false;

    protected $fillable = [
        'frame_title',
        'frame_name',
        'frame_explain',
        'frame_structure'
    ];

    protected $guarded = [];

        
}