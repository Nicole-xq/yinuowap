<?php

namespace App\Models;



/**
 * Class ShopncPFcodeQuotum
 *
 * @property int $fcq_id F码套餐id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $fcq_starttime 套餐开始时间
 * @property int $fcq_endtime 套餐结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPFcodeQuotum whereFcqEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPFcodeQuotum whereFcqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPFcodeQuotum whereFcqStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPFcodeQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPFcodeQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPFcodeQuotum extends BaseModel
{
    protected $table = 'shopnc_p_fcode_quota';

    protected $primaryKey = 'fcq_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'fcq_starttime',
        'fcq_endtime'
    ];

    protected $guarded = [];

        
}