<?php

namespace App\Models;



/**
 * Class ShopncExtensionLog
 *
 * @property int $log_id
 * @property string $extension_code 渠道ID
 * @property int|null $subscribe_type 关注状态：0未关注，1关注
 * @property int|null $subscribe_time 关注时间
 * @property int|null $unsubscribe_time 取消关注时间
 * @property int|null $first_unsubscribe_time 第一次取消时间
 * @property int|null $is_register 是否注册：0未注册，1注册
 * @property string|null $open_id 微信openID
 * @property int|null $add_time
 * @property int|null $modify_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereExtensionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereFirstUnsubscribeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereIsRegister($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereSubscribeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereSubscribeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionLog whereUnsubscribeTime($value)
 * @mixin \Eloquent
 */
class ShopncExtensionLog extends BaseModel
{
    protected $table = 'shopnc_extension_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'extension_code',
        'subscribe_type',
        'subscribe_time',
        'unsubscribe_time',
        'first_unsubscribe_time',
        'is_register',
        'open_id',
        'add_time',
        'modify_time'
    ];

    protected $guarded = [];

        
}