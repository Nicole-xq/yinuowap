<?php

namespace App\Models;



/**
 * Class ShopncGoodsClassNav
 *
 * @property string $cn_adv2_link 广告2链接
 * @property int $gc_id 商品分类id
 * @property string|null $cn_alias 商品分类别名
 * @property string|null $cn_classids 推荐子级分类
 * @property string|null $cn_brandids 推荐的品牌
 * @property string|null $cn_pic 分类图片
 * @property string|null $cn_adv1 广告图1
 * @property string|null $cn_adv1_link 广告1链接
 * @property string|null $cn_adv2 广告图2
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnAdv1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnAdv1Link($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnAdv2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnAdv2Link($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnBrandids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnClassids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereCnPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClassNav whereGcId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsClassNav extends BaseModel
{
    protected $table = 'shopnc_goods_class_nav';

    protected $primaryKey = 'gc_id';

	public $timestamps = false;

    protected $fillable = [
        'cn_adv2_link',
        'cn_alias',
        'cn_classids',
        'cn_brandids',
        'cn_pic',
        'cn_adv1',
        'cn_adv1_link',
        'cn_adv2'
    ];

    protected $guarded = [];

        
}