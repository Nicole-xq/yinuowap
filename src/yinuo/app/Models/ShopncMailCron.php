<?php

namespace App\Models;



/**
 * Class ShopncMailCron
 *
 * @property int $mail_id 消息任务计划id
 * @property string $mail 邮箱地址
 * @property string $subject 邮件标题
 * @property string $contnet 邮件内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailCron whereContnet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailCron whereMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailCron whereMailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMailCron whereSubject($value)
 * @mixin \Eloquent
 */
class ShopncMailCron extends BaseModel
{
    protected $table = 'shopnc_mail_cron';

    protected $primaryKey = 'mail_id';

	public $timestamps = false;

    protected $fillable = [
        'mail',
        'subject',
        'contnet'
    ];

    protected $guarded = [];

        
}