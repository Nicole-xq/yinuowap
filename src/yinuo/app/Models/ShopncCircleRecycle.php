<?php

namespace App\Models;



/**
 * Class ShopncCircleRecycle
 *
 * @property int $recycle_id 回收站id
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property int $circle_id 圈子id
 * @property string $circle_name 圈子名称
 * @property string $theme_name 主题名称
 * @property string $recycle_content 内容
 * @property int $recycle_opid 操作人id
 * @property string $recycle_opname 操作人名称
 * @property int $recycle_type 类型 1话题，2回复
 * @property string $recycle_time 操作时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereCircleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleOpid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleOpname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereRecycleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleRecycle whereThemeName($value)
 * @mixin \Eloquent
 */
class ShopncCircleRecycle extends BaseModel
{
    protected $table = 'shopnc_circle_recycle';

    protected $primaryKey = 'recycle_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'circle_id',
        'circle_name',
        'theme_name',
        'recycle_content',
        'recycle_opid',
        'recycle_opname',
        'recycle_type',
        'recycle_time'
    ];

    protected $guarded = [];

        
}