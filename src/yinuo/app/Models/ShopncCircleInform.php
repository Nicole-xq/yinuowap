<?php

namespace App\Models;



/**
 * Class ShopncCircleInform
 *
 * @property int $inform_id 举报id
 * @property int $circle_id 圈子id
 * @property string $circle_name 圈子名称
 * @property int $theme_id 话题id
 * @property string $theme_name 主题名称
 * @property int $reply_id 回复id
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property string $inform_content 举报内容
 * @property string $inform_time 举报时间
 * @property int $inform_type 类型 0话题、1回复
 * @property int $inform_state 状态 0未处理、1已处理
 * @property int|null $inform_opid 操作人id
 * @property string|null $inform_opname 操作人名称
 * @property int|null $inform_opexp 操作经验
 * @property string|null $inform_opresult 处理结果
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereCircleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformOpexp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformOpid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformOpname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformOpresult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereInformType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereReplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleInform whereThemeName($value)
 * @mixin \Eloquent
 */
class ShopncCircleInform extends BaseModel
{
    protected $table = 'shopnc_circle_inform';

    protected $primaryKey = 'inform_id';

	public $timestamps = false;

    protected $fillable = [
        'circle_id',
        'circle_name',
        'theme_id',
        'theme_name',
        'reply_id',
        'member_id',
        'member_name',
        'inform_content',
        'inform_time',
        'inform_type',
        'inform_state',
        'inform_opid',
        'inform_opname',
        'inform_opexp',
        'inform_opresult'
    ];

    protected $guarded = [];

        
}