<?php

namespace App\Models;



/**
 * Class ShopncMbAuctionItem
 *
 * @property int $item_id 拍卖项目编号
 * @property int $special_id 专题ID
 * @property string $item_type 项目类型
 * @property string|null $item_data 项目内容
 * @property int $item_usable 项目是否可用 0-不可用 1-可用
 * @property int $item_sort 项目排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereItemData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereItemSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereItemType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereItemUsable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbAuctionItem whereSpecialId($value)
 * @mixin \Eloquent
 */
class ShopncMbAuctionItem extends BaseModel
{
    protected $table = 'shopnc_mb_auction_item';

    protected $primaryKey = 'item_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'item_type',
        'item_data',
        'item_usable',
        'item_sort'
    ];

    protected $guarded = [];

        
}