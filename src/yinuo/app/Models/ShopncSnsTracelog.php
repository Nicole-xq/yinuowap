<?php

namespace App\Models;



/**
 * Class ShopncSnsTracelog
 *
 * @property int $trace_id 自增ID
 * @property int $trace_originalid 原动态ID 默认为0
 * @property int $trace_originalmemberid 原帖会员编号
 * @property int $trace_originalstate 原帖的删除状态 0为正常 1为删除
 * @property int $trace_memberid 会员ID
 * @property string $trace_membername 会员名称
 * @property string|null $trace_memberavatar 会员头像
 * @property string|null $trace_title 动态标题
 * @property string $trace_content 动态内容
 * @property int $trace_addtime 添加时间
 * @property int $trace_state 状态  0正常 1为禁止显示 默认为0
 * @property int $trace_privacy 隐私可见度 0所有人可见 1好友可见 2仅自己可见
 * @property int $trace_commentcount 评论数
 * @property int $trace_copycount 转发数
 * @property int $trace_orgcommentcount 原帖评论次数
 * @property int $trace_orgcopycount 原帖转帖次数
 * @property int|null $trace_from 来源 1=shop 2=storetracelog 3=microshop 4=cms 5=circle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceCommentcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceCopycount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceMemberavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceOrgcommentcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceOrgcopycount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceOriginalid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceOriginalmemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceOriginalstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTracePrivacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsTracelog whereTraceTitle($value)
 * @mixin \Eloquent
 */
class ShopncSnsTracelog extends BaseModel
{
    protected $table = 'shopnc_sns_tracelog';

    protected $primaryKey = 'trace_id';

	public $timestamps = false;

    protected $fillable = [
        'trace_originalid',
        'trace_originalmemberid',
        'trace_originalstate',
        'trace_memberid',
        'trace_membername',
        'trace_memberavatar',
        'trace_title',
        'trace_content',
        'trace_addtime',
        'trace_state',
        'trace_privacy',
        'trace_commentcount',
        'trace_copycount',
        'trace_orgcommentcount',
        'trace_orgcopycount',
        'trace_from'
    ];

    protected $guarded = [];

        
}