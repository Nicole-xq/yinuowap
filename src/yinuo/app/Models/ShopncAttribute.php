<?php

namespace App\Models;



/**
 * Class ShopncAttribute
 *
 * @property int $attr_id 属性id
 * @property string $attr_name 属性名称
 * @property int $type_id 所属类型id
 * @property string $attr_value 属性值列
 * @property int $attr_show 是否显示。0为不显示、1为显示
 * @property int $attr_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereAttrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereAttrName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereAttrShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereAttrSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereAttrValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttribute whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncAttribute extends BaseModel
{
    protected $table = 'shopnc_attribute';

    protected $primaryKey = 'attr_id';

	public $timestamps = false;

    protected $fillable = [
        'attr_name',
        'type_id',
        'attr_value',
        'attr_show',
        'attr_sort'
    ];

    protected $guarded = [];

        
}