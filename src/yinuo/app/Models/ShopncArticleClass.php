<?php

namespace App\Models;



/**
 * Class ShopncArticleClass
 *
 * @property int $ac_id 索引ID
 * @property string|null $ac_code 分类标识码
 * @property string $ac_name 分类名称
 * @property int $ac_parent_id 父ID
 * @property int $ac_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticleClass whereAcCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticleClass whereAcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticleClass whereAcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticleClass whereAcParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArticleClass whereAcSort($value)
 * @mixin \Eloquent
 */
class ShopncArticleClass extends BaseModel
{
    protected $table = 'shopnc_article_class';

    protected $primaryKey = 'ac_id';

	public $timestamps = false;

    protected $fillable = [
        'ac_code',
        'ac_name',
        'ac_parent_id',
        'ac_sort'
    ];

    protected $guarded = [];

        
}