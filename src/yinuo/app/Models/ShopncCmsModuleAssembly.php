<?php

namespace App\Models;



/**
 * Class ShopncCmsModuleAssembly
 *
 * @property int $assembly_id 组件编号
 * @property string $assembly_title 组件标题
 * @property string $assembly_name 组件名称
 * @property string $assembly_explain 组件说明
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleAssembly whereAssemblyExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleAssembly whereAssemblyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleAssembly whereAssemblyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsModuleAssembly whereAssemblyTitle($value)
 * @mixin \Eloquent
 */
class ShopncCmsModuleAssembly extends BaseModel
{
    protected $table = 'shopnc_cms_module_assembly';

    protected $primaryKey = 'assembly_id';

	public $timestamps = false;

    protected $fillable = [
        'assembly_title',
        'assembly_name',
        'assembly_explain'
    ];

    protected $guarded = [];

        
}