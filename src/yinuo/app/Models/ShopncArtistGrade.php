<?php

namespace App\Models;



/**
 * Class ShopncArtistGrade
 *
 * @property int $artist_grade_id 艺术家等级ID
 * @property string $artist_grade_name 艺术家等级名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistGrade whereArtistGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistGrade whereArtistGradeName($value)
 * @mixin \Eloquent
 */
class ShopncArtistGrade extends BaseModel
{
    protected $table = 'shopnc_artist_grade';

    protected $primaryKey = 'artist_grade_id';

	public $timestamps = false;

    protected $fillable = [
        'artist_grade_name'
    ];

    protected $guarded = [];

        
}