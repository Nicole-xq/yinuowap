<?php

namespace App\Models;



/**
 * Class ShopncSignin
 *
 * @property int $sl_id 自增ID
 * @property int $sl_memberid 会员ID
 * @property string $sl_membername 会员名称
 * @property int $sl_addtime 签到时间
 * @property int $sl_points 获得积分数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSignin whereSlAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSignin whereSlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSignin whereSlMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSignin whereSlMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSignin whereSlPoints($value)
 * @mixin \Eloquent
 */
class ShopncSignin extends BaseModel
{
    protected $table = 'shopnc_signin';

    protected $primaryKey = 'sl_id';

	public $timestamps = false;

    protected $fillable = [
        'sl_memberid',
        'sl_membername',
        'sl_addtime',
        'sl_points'
    ];

    protected $guarded = [];

        
}