<?php

namespace App\Models;



/**
 * Class ShopncStoreMsg
 *
 * @property int $sm_id 店铺消息id
 * @property string $smt_code 模板编码
 * @property int $store_id 店铺id
 * @property string $sm_content 消息内容
 * @property int $sm_addtime 发送时间
 * @property string|null $sm_readids 已读卖家id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereSmAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereSmContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereSmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereSmReadids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereSmtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsg whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreMsg extends BaseModel
{
    protected $table = 'shopnc_store_msg';

    protected $primaryKey = 'sm_id';

	public $timestamps = false;

    protected $fillable = [
        'smt_code',
        'store_id',
        'sm_content',
        'sm_addtime',
        'sm_readids'
    ];

    protected $guarded = [];

        
}