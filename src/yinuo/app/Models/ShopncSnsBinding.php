<?php

namespace App\Models;



/**
 * Class ShopncSnsBinding
 *
 * @property int $snsbind_id 自增ID
 * @property int $snsbind_memberid 会员编号
 * @property string $snsbind_membername 会员名称
 * @property string $snsbind_appsign 应用标志
 * @property int $snsbind_updatetime 绑定更新时间
 * @property string $snsbind_openid 应用用户编号
 * @property string|null $snsbind_openinfo 应用用户信息
 * @property string $snsbind_accesstoken 访问第三方资源的凭证
 * @property int $snsbind_expiresin accesstoken过期时间，以返回的时间的准，单位为秒，注意过期时提醒用户重新授权
 * @property string|null $snsbind_refreshtoken 刷新token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindAccesstoken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindAppsign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindExpiresin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindOpeninfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindRefreshtoken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsBinding whereSnsbindUpdatetime($value)
 * @mixin \Eloquent
 */
class ShopncSnsBinding extends BaseModel
{
    protected $table = 'shopnc_sns_binding';

    protected $primaryKey = 'snsbind_id';

	public $timestamps = false;

    protected $fillable = [
        'snsbind_memberid',
        'snsbind_membername',
        'snsbind_appsign',
        'snsbind_updatetime',
        'snsbind_openid',
        'snsbind_openinfo',
        'snsbind_accesstoken',
        'snsbind_expiresin',
        'snsbind_refreshtoken'
    ];

    protected $guarded = [];

        
}