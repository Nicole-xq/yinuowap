<?php

namespace App\Models;



/**
 * Class ShopncPMansongRule
 *
 * @property int $rule_id 规则编号
 * @property int $mansong_id 活动编号
 * @property int $price 级别价格
 * @property int $discount 减现金优惠金额
 * @property string|null $mansong_goods_name 礼品名称
 * @property int $goods_id 商品编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule whereMansongGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule whereMansongId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPMansongRule whereRuleId($value)
 * @mixin \Eloquent
 */
class ShopncPMansongRule extends BaseModel
{
    protected $table = 'shopnc_p_mansong_rule';

    protected $primaryKey = 'rule_id';

	public $timestamps = false;

    protected $fillable = [
        'mansong_id',
        'price',
        'discount',
        'mansong_goods_name',
        'goods_id'
    ];

    protected $guarded = [];

        
}