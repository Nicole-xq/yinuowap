<?php

namespace App\Models;



/**
 * Class ShopncPdRecharge
 *
 * @property int $pdr_id 自增编号
 * @property int $pdr_sn 记录唯一标示
 * @property int $pdr_member_id 会员编号
 * @property string $pdr_member_name 会员名称
 * @property float $pdr_amount 充值金额
 * @property string|null $pdr_payment_code 支付方式
 * @property string|null $pdr_payment_name 支付方式
 * @property int|null $pdr_api_pay_time 在线支付动作时间,只要向第三方支付平台提交就会更新
 * @property string|null $pdr_trade_sn 第三方支付接口交易号
 * @property int $pdr_add_time 添加时间
 * @property string $pdr_payment_state 支付状态 0未支付1支付
 * @property int $pdr_payment_time 支付时间
 * @property string|null $pdr_admin 管理员名
 * @property int|null $financial_verification 财务校验：1已校验、0未校验
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge whereFinancialVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrApiPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrPaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrPaymentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrPaymentState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrPaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdRecharge wherePdrTradeSn($value)
 * @mixin \Eloquent
 */
class ShopncPdRecharge extends BaseModel
{
    protected $table = 'shopnc_pd_recharge';

    protected $primaryKey = 'pdr_id';

	public $timestamps = false;

    protected $fillable = [
        'pdr_sn',
        'pdr_member_id',
        'pdr_member_name',
        'pdr_amount',
        'pdr_payment_code',
        'pdr_payment_name',
        'pdr_api_pay_time',
        'pdr_trade_sn',
        'pdr_add_time',
        'pdr_payment_state',
        'pdr_payment_time',
        'pdr_admin',
        'financial_verification'
    ];

    protected $guarded = [];

        
}