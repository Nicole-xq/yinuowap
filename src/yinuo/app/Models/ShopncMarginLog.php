<?php

namespace App\Models;



/**
 * Class ShopncMarginLog
 *
 * @property int $log_id 日志ID
 * @property int $member_id 会员ID
 * @property string $member_name 会员名称
 * @property string $type 类型，order_pay支付拍品使用，order_freeze支付保证金冻结，order_refund平台到期返还
 * @property int|null $add_time 添加时间
 * @property float|null $available_amount 可用余额变更 0表示未变更
 * @property float|null $freeze_amount 冻结余额变更 0表示未变更
 * @property string|null $description 描述
 * @property float|null $api_pay_amount 在线支付变动，0未变动
 * @property int|null $auction_id 0、非拍品保证金，1、拍品的对应ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereApiPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereAvailableAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereFreezeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMarginLog whereType($value)
 * @mixin \Eloquent
 */
class ShopncMarginLog extends BaseModel
{
    protected $table = 'shopnc_margin_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'type',
        'add_time',
        'available_amount',
        'freeze_amount',
        'description',
        'api_pay_amount',
        'auction_id'
    ];

    protected $guarded = [];

        
}