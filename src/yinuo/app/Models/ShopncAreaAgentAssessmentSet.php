<?php

namespace App\Models;



/**
 * Class ShopncAreaAgentAssessmentSet
 *
 * @property int $id 主键id
 * @property string $assessment_set_name 考核配置名称
 * @property int $commission_lv 返佣等级
 * @property int $agent_type 区域类别：1省级2市级3县级
 * @property string $last_bzj_content 上月保证金考核
 * @property string $last_goods_content 上月订单考核
 * @property string $last_artist_content 上月艺术家考核
 * @property string $curr_bzj_content 当月保证金考核
 * @property string $curr_goods_content 当月订单考核
 * @property string $curr_artist_content 当月艺术家考核
 * @property string $next_bzj_content 下月保证金考核
 * @property string $next_goods_content 下月订单考核
 * @property string $next_artist_content 下月艺术家考核
 * @property int $edit_time 编辑时间
 * @property int $create_time 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereAgentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereAssessmentSetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereCommissionLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereCurrArtistContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereCurrBzjContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereCurrGoodsContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereLastArtistContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereLastBzjContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereLastGoodsContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereNextArtistContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereNextBzjContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAssessmentSet whereNextGoodsContent($value)
 * @mixin \Eloquent
 */
class ShopncAreaAgentAssessmentSet extends BaseModel
{
    protected $table = 'shopnc_area_agent_assessment_set';

    public $timestamps = false;

    protected $fillable = [
        'assessment_set_name',
        'commission_lv',
        'agent_type',
        'last_bzj_content',
        'last_goods_content',
        'last_artist_content',
        'curr_bzj_content',
        'curr_goods_content',
        'curr_artist_content',
        'next_bzj_content',
        'next_goods_content',
        'next_artist_content',
        'edit_time',
        'create_time'
    ];

    protected $guarded = [];

        
}