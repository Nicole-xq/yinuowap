<?php

namespace App\Models;



/**
 * Class ShopncGoodsBrowse
 *
 * @property int $goods_id 商品ID
 * @property int $member_id 会员ID
 * @property int $browsetime 浏览时间
 * @property int $gc_id 商品分类
 * @property int $gc_id_1 商品一级分类
 * @property int $gc_id_2 商品二级分类
 * @property int $gc_id_3 商品三级分类
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereBrowsetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse whereMemberId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsBrowse where#alibabaRdsRowId#($value)
 */
class ShopncGoodsBrowse extends BaseModel
{
    protected $table = 'shopnc_goods_browse';

    public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'member_id',
        'browsetime',
        'gc_id',
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}