<?php

namespace App\Models;



/**
 * Class ShopncSnsVisitor
 *
 * @property int $v_id 自增ID
 * @property int $v_mid 访客会员ID
 * @property string $v_mname 访客会员名称
 * @property string|null $v_mavatar 访客会员头像
 * @property int $v_ownermid 主人会员ID
 * @property string $v_ownermname 主人会员名称
 * @property string|null $v_ownermavatar 主人会员头像
 * @property int $v_addtime 访问时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVMavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVMid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVMname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVOwnermavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVOwnermid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsVisitor whereVOwnermname($value)
 * @mixin \Eloquent
 */
class ShopncSnsVisitor extends BaseModel
{
    protected $table = 'shopnc_sns_visitor';

    protected $primaryKey = 'v_id';

	public $timestamps = false;

    protected $fillable = [
        'v_mid',
        'v_mname',
        'v_mavatar',
        'v_ownermid',
        'v_ownermname',
        'v_ownermavatar',
        'v_addtime'
    ];

    protected $guarded = [];

        
}