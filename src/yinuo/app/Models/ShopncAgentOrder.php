<?php

namespace App\Models;



/**
 * Class ShopncAgentOrder
 *
 * @property int $id
 * @property int|null $agent_id 代理id
 * @property int|null $order_id
 * @property string|null $order_no 订单号
 * @property int|null $from_member_id 返佣来源用户id
 * @property float|null $order_price 订单金额
 * @property string|null $description 描述
 * @property int|null $commission_type 佣金类型(1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻)
 * @property int|null $agent_type 代理类型(1省2市3县/区)
 * @property int|null $agent_lv 代理级别
 * @property float|null $rate_num 返佣比例
 * @property float|null $commission_amount 返佣金额
 * @property int|null $commission_status 返佣状态(0,未结算 1,已结算 2,已打款)
 * @property int|null $province_id 省份id
 * @property int|null $city_id
 * @property int|null $area_id
 * @property string|null $finish_time 结算时间
 * @property string|null $udate 更新时间
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereAgentLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereAgentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereCommissionAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereCommissionStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereCommissionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereFinishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereFromMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereOrderNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereOrderPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereRateNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAgentOrder whereUdate($value)
 * @mixin \Eloquent
 */
class ShopncAgentOrder extends BaseModel
{
    protected $table = 'shopnc_agent_order';

    public $timestamps = false;

    protected $fillable = [
        'agent_id',
        'order_id',
        'order_no',
        'from_member_id',
        'order_price',
        'description',
        'commission_type',
        'agent_type',
        'agent_lv',
        'rate_num',
        'commission_amount',
        'commission_status',
        'province_id',
        'city_id',
        'area_id',
        'finish_time',
        'udate',
        'cdate'
    ];

    protected $guarded = [];

        
}