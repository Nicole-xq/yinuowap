<?php

namespace App\Models;



/**
 * Class ShopncArea
 *
 * @property int $area_id 索引ID
 * @property string $area_name 地区名称
 * @property int $area_parent_id 地区父ID
 * @property int $area_sort 排序
 * @property int $area_deep 地区深度，从1开始
 * @property string|null $area_region 大区名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaDeep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArea whereAreaSort($value)
 * @mixin \Eloquent
 */
class ShopncArea extends BaseModel
{
    protected $table = 'shopnc_area';

    protected $primaryKey = 'area_id';

	public $timestamps = false;

    protected $fillable = [
        'area_name',
        'area_parent_id',
        'area_sort',
        'area_deep',
        'area_region'
    ];

    protected $guarded = [];

        
}