<?php

namespace App\Models;



/**
 * Class ShopncDisCart
 *
 * @property int $goods_commonid 商品公共表ID
 * @property int $buyer_id 买家ID
 * @property int $dis_member_id 分销会员ID
 * @property int $end_time 到期时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisCart whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisCart whereDisMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisCart whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisCart whereGoodsCommonid($value)
 * @mixin \Eloquent
 */
class ShopncDisCart extends BaseModel
{
    protected $table = 'shopnc_dis_cart';

    public $timestamps = false;

    protected $fillable = [
        'goods_commonid',
        'buyer_id',
        'dis_member_id',
        'end_time'
    ];

    protected $guarded = [];

        
}