<?php

namespace App\Models;



/**
 * Class ShopncSnsSharegood
 *
 * @property int $share_id 自增ID
 * @property int $share_goodsid 商品ID
 * @property int $share_memberid 所属会员ID
 * @property string $share_membername 会员名称
 * @property string|null $share_content 描述内容
 * @property int $share_addtime 分享操作时间
 * @property int $share_likeaddtime 喜欢操作时间
 * @property int $share_privacy 隐私可见度 0所有人可见 1好友可见 2仅自己可见
 * @property int $share_commentcount 评论数
 * @property int $share_isshare 是否分享 0为未分享 1为分享
 * @property int $share_islike 是否喜欢 0为未喜欢 1为喜欢
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareCommentcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareIslike($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareIsshare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareLikeaddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereShareMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharegood whereSharePrivacy($value)
 * @mixin \Eloquent
 */
class ShopncSnsSharegood extends BaseModel
{
    protected $table = 'shopnc_sns_sharegoods';

    protected $primaryKey = 'share_id';

	public $timestamps = false;

    protected $fillable = [
        'share_goodsid',
        'share_memberid',
        'share_membername',
        'share_content',
        'share_addtime',
        'share_likeaddtime',
        'share_privacy',
        'share_commentcount',
        'share_isshare',
        'share_islike'
    ];

    protected $guarded = [];

        
}