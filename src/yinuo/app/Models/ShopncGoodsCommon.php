<?php

namespace App\Models;



/**
 * Class ShopncGoodsCommon
 *
 * @property int $goods_commonid 商品公共表id
 * @property int|null $sales_model 销售类型：1普通商品、2可议价商品、3新手专区商品
 * @property string $goods_name 商品名称
 * @property string|null $goods_jingle 商品广告词
 * @property int $gc_id 商品分类
 * @property int $gc_id_1 一级分类id
 * @property int $gc_id_2 二级分类id
 * @property int $gc_id_3 三级分类id
 * @property string $gc_name 商品分类
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property string $spec_name 规格名称
 * @property string $spec_value 规格值
 * @property int|null $brand_id 品牌id
 * @property string|null $brand_name 品牌名称
 * @property int $type_id 类型id
 * @property string $goods_image 商品主图
 * @property string $goods_attr 商品属性
 * @property string $goods_custom 商品自定义属性
 * @property string $goods_body 商品内容
 * @property string $mobile_body 手机端商品描述
 * @property int $goods_state 商品状态 0下架，1正常，10违规（禁售）
 * @property string|null $goods_stateremark 违规原因
 * @property int $goods_verify 商品审核 1通过，0未通过，10审核中
 * @property string|null $goods_verifyremark 审核失败原因
 * @property int $goods_lock 商品锁定 0未锁，1已锁
 * @property int $goods_addtime 商品添加时间
 * @property int $goods_selltime 上架时间
 * @property float $goods_price 商品价格
 * @property float $goods_marketprice 市场价
 * @property float $goods_costprice 成本价
 * @property float $goods_discount 折扣
 * @property string|null $goods_serial 商品货号
 * @property int $goods_storage_alarm 库存报警值
 * @property string|null $goods_barcode 商品条形码
 * @property int $transport_id 运费模板
 * @property string|null $transport_title 运费模板名称
 * @property int $goods_commend 商品推荐 1是，0否，默认为0
 * @property float $goods_freight 运费 0为免运费
 * @property int $goods_vat 是否开具增值税发票 1是，0否
 * @property int $areaid_1 一级地区id
 * @property int $areaid_2 二级地区id
 * @property string|null $goods_stcids 店铺分类id 首尾用,隔开
 * @property int|null $plateid_top 顶部关联板式
 * @property int|null $plateid_bottom 底部关联板式
 * @property int $is_virtual 是否为虚拟商品 1是，0否
 * @property int|null $virtual_indate 虚拟商品有效期
 * @property int|null $virtual_limit 虚拟商品购买上限
 * @property int $virtual_invalid_refund 是否允许过期退款， 1是，0否
 * @property int $sup_id 供应商id
 * @property int $is_own_shop 是否为平台自营
 * @property float $goods_trans_v 重量或体积
 * @property int $is_dis 是否分销
 * @property int $dis_add_time 分销添加时间
 * @property int $dis_commis_rate 分销佣金比例
 * @property string|null $goods_video 商品视频
 * @property int|null $sale_count 销量
 * @property int|null $click_count 点击量
 * @property int $is_special 是否特殊商品(下单后不可退款退货) 0 不是 1 是
 * @property string|null $goods_jingle_pc_link 商品广告词PC链接
 * @property string|null $goods_jingle_wap_link 商品广告词WAP链接
 * @property int|null $sort 商品排序
 * @property int|null $artist_sort 艺术家首页排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereAreaid1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereAreaid2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereArtistSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereClickCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereDisAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsAttr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsCostprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsFreight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsJingle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsJinglePcLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsJingleWapLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsMarketprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsSelltime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsStateremark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsStcids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsStorageAlarm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsTransV($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsVat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsVerifyremark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereGoodsVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereIsDis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereIsOwnShop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereIsSpecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereIsVirtual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereMobileBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon wherePlateidBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon wherePlateidTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSaleCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSalesModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSpecName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSpecValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereSupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereTransportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereTransportTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereVirtualIndate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereVirtualInvalidRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsCommon whereVirtualLimit($value)
 * @mixin \Eloquent
 */
class ShopncGoodsCommon extends BaseModel
{
    protected $table = 'shopnc_goods_common';

    protected $primaryKey = 'goods_commonid';

	public $timestamps = false;

    protected $fillable = [
        'sales_model',
        'goods_name',
        'goods_jingle',
        'gc_id',
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        'gc_name',
        'store_id',
        'store_name',
        'spec_name',
        'spec_value',
        'brand_id',
        'brand_name',
        'type_id',
        'goods_image',
        'goods_attr',
        'goods_custom',
        'goods_body',
        'mobile_body',
        'goods_state',
        'goods_stateremark',
        'goods_verify',
        'goods_verifyremark',
        'goods_lock',
        'goods_addtime',
        'goods_selltime',
        'goods_price',
        'goods_marketprice',
        'goods_costprice',
        'goods_discount',
        'goods_serial',
        'goods_storage_alarm',
        'goods_barcode',
        'transport_id',
        'transport_title',
        'goods_commend',
        'goods_freight',
        'goods_vat',
        'areaid_1',
        'areaid_2',
        'goods_stcids',
        'plateid_top',
        'plateid_bottom',
        'is_virtual',
        'virtual_indate',
        'virtual_limit',
        'virtual_invalid_refund',
        'sup_id',
        'is_own_shop',
        'goods_trans_v',
        'is_dis',
        'dis_add_time',
        'dis_commis_rate',
        'goods_video',
        'sale_count',
        'click_count',
        'is_special',
        'goods_jingle_pc_link',
        'goods_jingle_wap_link',
        'sort',
        'artist_sort'
    ];

    protected $guarded = [];

        
}