<?php

namespace App\Models;



/**
 * Class ShopncPXianshiQuotum
 *
 * @property int $quota_id 限时折扣套餐编号
 * @property int $member_id 用户编号
 * @property int $store_id 店铺编号
 * @property string $member_name 用户名
 * @property string $store_name 店铺名称
 * @property int $start_time 套餐开始时间
 * @property int $end_time 套餐结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPXianshiQuotum extends BaseModel
{
    protected $table = 'shopnc_p_xianshi_quota';

    protected $primaryKey = 'quota_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'store_id',
        'member_name',
        'store_name',
        'start_time',
        'end_time'
    ];

    protected $guarded = [];

        
}