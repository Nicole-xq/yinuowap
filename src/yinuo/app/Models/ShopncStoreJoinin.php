<?php

namespace App\Models;



/**
 * Class ShopncStoreJoinin
 *
 * @property int $member_id 用户编号
 * @property string|null $member_name 店主用户名
 * @property string|null $record_number 入驻标号
 * @property string|null $company_name 公司名称
 * @property int $company_province_id 所在地省ID
 * @property string|null $company_address 公司地址
 * @property string|null $company_address_detail 公司详细地址
 * @property string|null $company_phone 公司电话
 * @property int|null $company_employee_count 员工总数
 * @property int|null $company_registered_capital 注册资金
 * @property string|null $contacts_name 联系人姓名
 * @property string|null $contacts_phone 联系人电话
 * @property string|null $contacts_email 联系人邮箱
 * @property string|null $business_licence_number 营业执照号
 * @property string|null $business_licence_address 营业执所在地
 * @property string|null $business_licence_start 营业执照有效期开始
 * @property string|null $business_licence_end 营业执照有效期结束
 * @property string|null $business_sphere 法定经营范围
 * @property string|null $business_licence_number_elc 营业执照电子版
 * @property string|null $organization_code 组织机构代码
 * @property string|null $organization_code_electronic 组织机构代码电子版
 * @property string|null $general_taxpayer 一般纳税人证明
 * @property string|null $bank_account_name 银行开户名
 * @property string|null $bank_account_number 公司银行账号
 * @property string|null $bank_name 开户银行支行名称
 * @property string|null $bank_code 支行联行号
 * @property string|null $bank_address 开户银行所在地
 * @property string|null $bank_licence_electronic 开户银行许可证电子版
 * @property int|null $is_settlement_account 开户行账号是否为结算账号 1-开户行就是结算账号 2-独立的计算账号
 * @property string|null $settlement_bank_account_name 结算银行开户名
 * @property string|null $settlement_bank_account_number 结算公司银行账号
 * @property string|null $settlement_bank_name 结算开户银行支行名称
 * @property string|null $settlement_bank_code 结算支行联行号
 * @property string|null $settlement_bank_address 结算开户银行所在地
 * @property string|null $tax_registration_certificate 税务登记证号
 * @property string|null $taxpayer_id 纳税人识别号
 * @property string|null $tax_registration_certif_elc 税务登记证号电子版
 * @property string|null $seller_name 卖家账号
 * @property string|null $store_name 店铺名称
 * @property string|null $store_class_ids 店铺分类编号集合
 * @property string|null $store_class_names 店铺分类名称集合
 * @property string|null $joinin_state 申请状态 10-已提交申请 11-缴费完成  20-审核成功 30-审核失败 31-缴费审核失败 40-审核通过开店
 * @property string|null $joinin_message 管理员审核信息
 * @property int $joinin_year 开店时长(年)
 * @property string|null $sg_name 店铺等级名称
 * @property int|null $sg_id 店铺等级编号
 * @property string|null $sg_info 店铺等级下的收费等信息
 * @property string|null $sc_name 店铺分类名称
 * @property int|null $sc_id 店铺分类编号
 * @property int $sc_bail 店铺分类保证金
 * @property string|null $store_class_commis_rates 分类佣金比例
 * @property string|null $paying_money_certificate 付款凭证
 * @property string|null $paying_money_certif_exp 付款凭证说明
 * @property float $paying_amount 付款金额
 * @property string|null $paying_memo 付款金额备注
 * @property int $joinin_type 入驻类型 0商家 1艺术家
 * @property string|null $store_class_auction_commis_rates 店铺拍品佣金比例
 * @property string|null $all_cards_elc 三证合一电子版
 * @property int|null $card_type 三证类型 1普通 2三证合一
 * @property string|null $id_number 身份证号码
 * @property string|null $id_number_positive_elc 身份证正面照片
 * @property string|null $id_number_negative_elc 身份证反面照片
 * @property string|null $member_certificate_elc 会员证书
 * @property string|null $other_qualifications_elc 其他资质
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereAllCardsElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankLicenceElectronic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessLicenceAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessLicenceEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessLicenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessLicenceNumberElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessLicenceStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereBusinessSphere($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyAddressDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyEmployeeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereCompanyRegisteredCapital($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereContactsEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereContactsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereContactsPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereGeneralTaxpayer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereIdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereIdNumberNegativeElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereIdNumberPositiveElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereIsSettlementAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereJoininMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereJoininState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereJoininType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereJoininYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereMemberCertificateElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereOrganizationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereOrganizationCodeElectronic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereOtherQualificationsElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin wherePayingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin wherePayingMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin wherePayingMoneyCertifExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin wherePayingMoneyCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereRecordNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereScBail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereScName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSettlementBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSettlementBankAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSettlementBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSettlementBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSettlementBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSgInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereSgName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereStoreClassAuctionCommisRates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereStoreClassCommisRates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereStoreClassIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereStoreClassNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereTaxRegistrationCertifElc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereTaxRegistrationCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreJoinin whereTaxpayerId($value)
 * @mixin \Eloquent
 */
class ShopncStoreJoinin extends BaseModel
{
    protected $table = 'shopnc_store_joinin';

    protected $primaryKey = 'member_id';

	public $timestamps = false;

    protected $fillable = [
        'member_name',
        'record_number',
        'company_name',
        'company_province_id',
        'company_address',
        'company_address_detail',
        'company_phone',
        'company_employee_count',
        'company_registered_capital',
        'contacts_name',
        'contacts_phone',
        'contacts_email',
        'business_licence_number',
        'business_licence_address',
        'business_licence_start',
        'business_licence_end',
        'business_sphere',
        'business_licence_number_elc',
        'organization_code',
        'organization_code_electronic',
        'general_taxpayer',
        'bank_account_name',
        'bank_account_number',
        'bank_name',
        'bank_code',
        'bank_address',
        'bank_licence_electronic',
        'is_settlement_account',
        'settlement_bank_account_name',
        'settlement_bank_account_number',
        'settlement_bank_name',
        'settlement_bank_code',
        'settlement_bank_address',
        'tax_registration_certificate',
        'taxpayer_id',
        'tax_registration_certif_elc',
        'seller_name',
        'store_name',
        'store_class_ids',
        'store_class_names',
        'joinin_state',
        'joinin_message',
        'joinin_year',
        'sg_name',
        'sg_id',
        'sg_info',
        'sc_name',
        'sc_id',
        'sc_bail',
        'store_class_commis_rates',
        'paying_money_certificate',
        'paying_money_certif_exp',
        'paying_amount',
        'paying_memo',
        'joinin_type',
        'store_class_auction_commis_rates',
        'all_cards_elc',
        'card_type',
        'id_number',
        'id_number_positive_elc',
        'id_number_negative_elc',
        'member_certificate_elc',
        'other_qualifications_elc'
    ];

    protected $guarded = [];

        
}