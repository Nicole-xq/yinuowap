<?php

namespace App\Models;



/**
 * Class ShopncPointsLog
 *
 * @property int $pl_id 积分日志编号
 * @property int $pl_memberid 会员编号
 * @property string $pl_membername 会员名称
 * @property int|null $pl_adminid 管理员编号
 * @property string|null $pl_adminname 管理员名称
 * @property int $pl_points 积分数负数表示扣除
 * @property int $pl_addtime 添加时间
 * @property string $pl_desc 操作描述
 * @property string $pl_stage 操作阶段
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlAdminid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlAdminname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsLog wherePlStage($value)
 * @mixin \Eloquent
 */
class ShopncPointsLog extends BaseModel
{
    protected $table = 'shopnc_points_log';

    protected $primaryKey = 'pl_id';

	public $timestamps = false;

    protected $fillable = [
        'pl_memberid',
        'pl_membername',
        'pl_adminid',
        'pl_adminname',
        'pl_points',
        'pl_addtime',
        'pl_desc',
        'pl_stage'
    ];

    protected $guarded = [];

        
}