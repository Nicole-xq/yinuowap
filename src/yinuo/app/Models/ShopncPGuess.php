<?php

namespace App\Models;



/**
 * Class ShopncPGuess
 *
 * @property int $gs_id 组合ID
 * @property int $gs_act_id 活动id
 * @property string $gs_name 组合名称
 * @property int $store_id 店铺名称
 * @property string $store_name 店铺名称
 * @property float $gs_discount_price 购买折扣价格
 * @property float $gs_estimate_price 市场估值
 * @property float $gs_min_price 竞猜最低价
 * @property float $gs_max_price 竞猜最高价
 * @property float $gs_right_price 竞猜价
 * @property int $gs_freight_choose 运费承担方式
 * @property float|null $gs_freight 运费
 * @property int $gs_state 组合状态 0-关闭/1-开启
 * @property int $start_time 活动开始时间
 * @property int $end_time 活动结束时间
 * @property int $points 赠送诺币
 * @property int $state 趣猜状态 10-审核中 20-正常 30-审核失败 31-管理员关闭 32-已结束
 * @property int|null $set_reminders_num 设置提醒人数
 * @property string $gs_img 趣猜图片
 * @property float $gs_cost_price 商品原价总价
 * @property int $verify_state 审核状态 0 待审核 1 已通过 2 未通过
 * @property string $verify_desc 管理员备注
 * @property string|null $delivery_mechanism 送拍机构
 * @property int $views 围观次数
 * @property int $joined_times 参与竞猜的人数
 * @property int $gs_collect 趣猜收藏数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereDeliveryMechanism($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsActId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsCollect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsDiscountPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsEstimatePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsFreight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsFreightChoose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsMaxPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsRightPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereGsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereJoinedTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereSetRemindersNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereVerifyDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereVerifyState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuess whereViews($value)
 * @mixin \Eloquent
 */
class ShopncPGuess extends BaseModel
{
    protected $table = 'shopnc_p_guess';

    protected $primaryKey = 'gs_id';

	public $timestamps = false;

    protected $fillable = [
        'gs_act_id',
        'gs_name',
        'store_id',
        'store_name',
        'gs_discount_price',
        'gs_estimate_price',
        'gs_min_price',
        'gs_max_price',
        'gs_right_price',
        'gs_freight_choose',
        'gs_freight',
        'gs_state',
        'start_time',
        'end_time',
        'points',
        'state',
        'set_reminders_num',
        'gs_img',
        'gs_cost_price',
        'verify_state',
        'verify_desc',
        'delivery_mechanism',
        'views',
        'joined_times',
        'gs_collect'
    ];

    protected $guarded = [];

        
}