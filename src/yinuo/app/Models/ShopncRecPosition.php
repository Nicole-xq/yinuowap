<?php

namespace App\Models;



/**
 * Class ShopncRecPosition
 *
 * @property int $rec_id
 * @property string $pic_type 0文字1本地图片2远程
 * @property string|null $title 标题
 * @property string $content 序列化推荐位内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRecPosition whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRecPosition wherePicType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRecPosition whereRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRecPosition whereTitle($value)
 * @mixin \Eloquent
 */
class ShopncRecPosition extends BaseModel
{
    protected $table = 'shopnc_rec_position';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'pic_type',
        'title',
        'content'
    ];

    protected $guarded = [];

        
}