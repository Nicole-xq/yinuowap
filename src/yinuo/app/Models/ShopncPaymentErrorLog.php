<?php

namespace App\Models;



/**
 * Class ShopncPaymentErrorLog
 *
 * @property int $id
 * @property string|null $trade_no 支付订单号
 * @property string|null $order_sn 内部订单号
 * @property float|null $api_price 应付金额
 * @property float|null $api_price_t 实付金额
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereApiPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereApiPriceT($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaymentErrorLog whereTradeNo($value)
 * @mixin \Eloquent
 */
class ShopncPaymentErrorLog extends BaseModel
{
    protected $table = 'shopnc_payment_error_log';

    public $timestamps = false;

    protected $fillable = [
        'trade_no',
        'order_sn',
        'api_price',
        'api_price_t',
        'cdate'
    ];

    protected $guarded = [];

        
}