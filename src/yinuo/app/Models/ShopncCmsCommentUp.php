<?php

namespace App\Models;



/**
 * Class ShopncCmsCommentUp
 *
 * @property int $up_id 顶编号
 * @property int $comment_id 评论编号
 * @property int $up_member_id 用户编号
 * @property int $up_time 评论时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsCommentUp whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsCommentUp whereUpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsCommentUp whereUpMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsCommentUp whereUpTime($value)
 * @mixin \Eloquent
 */
class ShopncCmsCommentUp extends BaseModel
{
    protected $table = 'shopnc_cms_comment_up';

    protected $primaryKey = 'up_id';

	public $timestamps = false;

    protected $fillable = [
        'comment_id',
        'up_member_id',
        'up_time'
    ];

    protected $guarded = [];

        
}