<?php

namespace App\Models;



/**
 * Class ShopncSpecValue
 *
 * @property int $sp_value_id 规格值id
 * @property string $sp_value_name 规格值名称
 * @property int $sp_id 所属规格id
 * @property int $gc_id 分类id
 * @property int $store_id 店铺id
 * @property string|null $sp_value_color 规格颜色
 * @property int $sp_value_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereSpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereSpValueColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereSpValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereSpValueName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereSpValueSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecValue whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncSpecValue extends BaseModel
{
    protected $table = 'shopnc_spec_value';

    protected $primaryKey = 'sp_value_id';

	public $timestamps = false;

    protected $fillable = [
        'sp_value_name',
        'sp_id',
        'gc_id',
        'store_id',
        'sp_value_color',
        'sp_value_sort'
    ];

    protected $guarded = [];

        
}