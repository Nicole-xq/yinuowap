<?php

namespace App\Models;



/**
 * Class ShopncPXianshiGood
 *
 * @property int $xianshi_goods_id 限时折扣商品表
 * @property int $xianshi_id 限时活动编号
 * @property string $xianshi_name 活动名称
 * @property string|null $xianshi_title 活动标题
 * @property string|null $xianshi_explain 活动说明
 * @property int $goods_id 商品编号
 * @property int $store_id 店铺编号
 * @property string $goods_name 商品名称
 * @property float $goods_price 店铺价格
 * @property float $xianshi_price 限时折扣价格
 * @property string $goods_image 商品图片
 * @property int $start_time 开始时间
 * @property int $end_time 结束时间
 * @property int $lower_limit 购买下限，0为不限制
 * @property int $state 状态，0-取消 1-正常
 * @property int $xianshi_recommend 推荐标志 0-未推荐 1-已推荐
 * @property int|null $gc_id_1 商品分类一级ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereLowerLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPXianshiGood whereXianshiTitle($value)
 * @mixin \Eloquent
 */
class ShopncPXianshiGood extends BaseModel
{
    protected $table = 'shopnc_p_xianshi_goods';

    protected $primaryKey = 'xianshi_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'xianshi_id',
        'xianshi_name',
        'xianshi_title',
        'xianshi_explain',
        'goods_id',
        'store_id',
        'goods_name',
        'goods_price',
        'xianshi_price',
        'goods_image',
        'start_time',
        'end_time',
        'lower_limit',
        'state',
        'xianshi_recommend',
        'gc_id_1'
    ];

    protected $guarded = [];

        
}