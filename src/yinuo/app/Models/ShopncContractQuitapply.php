<?php

namespace App\Models;



/**
 * Class ShopncContractQuitapply
 *
 * @property int $ctq_id 退出申请ID
 * @property int $ctq_itemid 项目ID
 * @property string $ctq_itemname 项目名称
 * @property int $ctq_storeid 店铺ID
 * @property string $ctq_storename 店铺名称
 * @property int $ctq_addtime 添加时间
 * @property int $ctq_auditstate 审核状态0未审核1审核通过2审核失败
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqAuditstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqItemname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractQuitapply whereCtqStorename($value)
 * @mixin \Eloquent
 */
class ShopncContractQuitapply extends BaseModel
{
    protected $table = 'shopnc_contract_quitapply';

    protected $primaryKey = 'ctq_id';

	public $timestamps = false;

    protected $fillable = [
        'ctq_itemid',
        'ctq_itemname',
        'ctq_storeid',
        'ctq_storename',
        'ctq_addtime',
        'ctq_auditstate'
    ];

    protected $guarded = [];

        
}