<?php

namespace App\Models;



/**
 * Class ShopncAdmin
 *
 * @property int $admin_id 管理员ID
 * @property string $admin_name 管理员名称
 * @property string|null $admin_avatar 管理员头像
 * @property string $admin_password 管理员密码
 * @property int $admin_login_time 登录时间
 * @property int $admin_login_num 登录次数
 * @property int $admin_is_super 是否超级管理员
 * @property int|null $admin_gid 权限组ID
 * @property string|null $admin_quick_link 管理员常用操作
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminGid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminIsSuper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdmin whereAdminQuickLink($value)
 * @mixin \Eloquent
 */
class ShopncAdmin extends BaseModel
{
    protected $table = 'shopnc_admin';

    protected $primaryKey = 'admin_id';

	public $timestamps = false;

    protected $fillable = [
        'admin_name',
        'admin_avatar',
        'admin_password',
        'admin_login_time',
        'admin_login_num',
        'admin_is_super',
        'admin_gid',
        'admin_quick_link'
    ];

    protected $guarded = [];

        
}