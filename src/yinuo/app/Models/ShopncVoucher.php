<?php

namespace App\Models;



/**
 * Class ShopncVoucher
 *
 * @property int $voucher_id 代金券编号
 * @property string $voucher_code 代金券编码
 * @property int $voucher_t_id 代金券模版编号
 * @property string $voucher_title 代金券标题
 * @property string $voucher_desc 代金券描述
 * @property int $voucher_start_date 代金券有效期开始时间
 * @property int $voucher_end_date 代金券有效期结束时间
 * @property int $voucher_price 代金券面额
 * @property float $voucher_limit 代金券使用时的订单限额
 * @property int $voucher_store_id 代金券的店铺id
 * @property int $voucher_state 代金券状态(1-未用,2-已用,3-过期,4-收回)
 * @property int $voucher_active_date 代金券发放日期
 * @property int|null $voucher_type 代金券类别
 * @property int $voucher_owner_id 代金券所有者id
 * @property string $voucher_owner_name 代金券所有者名称
 * @property int|null $voucher_order_id 使用该代金券的订单编号
 * @property string|null $voucher_pwd 代金券卡密不可逆
 * @property string|null $voucher_pwd2 代金券卡密2可逆
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherActiveDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherPwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherPwd2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucher whereVoucherType($value)
 * @mixin \Eloquent
 */
class ShopncVoucher extends BaseModel
{
    protected $table = 'shopnc_voucher';

    protected $primaryKey = 'voucher_id';

	public $timestamps = false;

    protected $fillable = [
        'voucher_code',
        'voucher_t_id',
        'voucher_title',
        'voucher_desc',
        'voucher_start_date',
        'voucher_end_date',
        'voucher_price',
        'voucher_limit',
        'voucher_store_id',
        'voucher_state',
        'voucher_active_date',
        'voucher_type',
        'voucher_owner_id',
        'voucher_owner_name',
        'voucher_order_id',
        'voucher_pwd',
        'voucher_pwd2'
    ];

    protected $guarded = [];

        
}