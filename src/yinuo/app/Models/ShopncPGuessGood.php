<?php

namespace App\Models;



/**
 * Class ShopncPGuessGood
 *
 * @property int $gs_goods_id 趣猜商品id
 * @property int $gs_id 趣猜组合id
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property string $goods_image 商品图片
 * @property float $gs_goods_price 商品价格
 * @property int $gs_appoint 指定商品 1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGsAppoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGsGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGsGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPGuessGood whereGsId($value)
 * @mixin \Eloquent
 */
class ShopncPGuessGood extends BaseModel
{
    protected $table = 'shopnc_p_guess_goods';

    protected $primaryKey = 'gs_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'gs_id',
        'goods_id',
        'goods_name',
        'goods_image',
        'gs_goods_price',
        'gs_appoint'
    ];

    protected $guarded = [];

        
}