<?php

namespace App\Models;



/**
 * Class ShopncAlbumPic
 *
 * @property int $apic_id 相册图片表id
 * @property string $apic_name 图片名称
 * @property string|null $apic_tag 图片标签
 * @property int $aclass_id 相册id
 * @property string $apic_cover 图片路径
 * @property int $apic_size 图片大小
 * @property string $apic_spec 图片规格
 * @property int $store_id 所属店铺id
 * @property int $upload_time 图片上传时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereAclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereApicTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumPic whereUploadTime($value)
 * @mixin \Eloquent
 */
class ShopncAlbumPic extends BaseModel
{
    protected $table = 'shopnc_album_pic';

    protected $primaryKey = 'apic_id';

	public $timestamps = false;

    protected $fillable = [
        'apic_name',
        'apic_tag',
        'aclass_id',
        'apic_cover',
        'apic_size',
        'apic_spec',
        'store_id',
        'upload_time'
    ];

    protected $guarded = [];

        
}