<?php

namespace App\Models;



/**
 * Class ShopncInvoice
 *
 * @property int $inv_id 索引id
 * @property int $member_id 会员ID
 * @property string|null $inv_state 1普通发票2增值税发票
 * @property string|null $inv_title 发票抬头[普通发票]
 * @property string|null $inv_content 发票内容[普通发票]
 * @property string|null $inv_company 单位名称
 * @property string|null $inv_code 纳税人识别号
 * @property string|null $inv_reg_addr 注册地址
 * @property string|null $inv_reg_phone 注册电话
 * @property string|null $inv_reg_bname 开户银行
 * @property string|null $inv_reg_baccount 银行账户
 * @property string|null $inv_rec_name 收票人姓名
 * @property string|null $inv_rec_mobphone 收票人手机号
 * @property string|null $inv_rec_province 收票人省份
 * @property string|null $inv_goto_addr 送票地址
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvGotoAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRecMobphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRecName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRecProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRegAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRegBaccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRegBname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvRegPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereInvTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInvoice whereMemberId($value)
 * @mixin \Eloquent
 */
class ShopncInvoice extends BaseModel
{
    protected $table = 'shopnc_invoice';

    protected $primaryKey = 'inv_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'inv_state',
        'inv_title',
        'inv_content',
        'inv_company',
        'inv_code',
        'inv_reg_addr',
        'inv_reg_phone',
        'inv_reg_bname',
        'inv_reg_baccount',
        'inv_rec_name',
        'inv_rec_mobphone',
        'inv_rec_province',
        'inv_goto_addr'
    ];

    protected $guarded = [];

        
}