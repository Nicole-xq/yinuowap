<?php

namespace App\Models;



/**
 * Class ShopncSellerGroup
 *
 * @property int $group_id 卖家组编号
 * @property string $group_name 组名
 * @property string $limits 权限
 * @property string|null $smt_limits 消息权限范围
 * @property int|null $gc_limits 1拥有所有分类权限，0拥有部分分类权限
 * @property int $store_id 店铺编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereGcLimits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereGroupName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereLimits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereSmtLimits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroup whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncSellerGroup extends BaseModel
{
    protected $table = 'shopnc_seller_group';

    protected $primaryKey = 'group_id';

	public $timestamps = false;

    protected $fillable = [
        'group_name',
        'limits',
        'smt_limits',
        'gc_limits',
        'store_id'
    ];

    protected $guarded = [];

        
}