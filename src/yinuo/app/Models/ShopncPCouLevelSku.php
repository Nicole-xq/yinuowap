<?php

namespace App\Models;



/**
 * Class ShopncPCouLevelSku
 *
 * @property int $cou_id 加价购ID
 * @property int $xlevel 等级
 * @property int $sku_id 商品条目ID
 * @property float $price 价格
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevelSku whereCouId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevelSku wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevelSku whereSkuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevelSku whereXlevel($value)
 * @mixin \Eloquent
 */
class ShopncPCouLevelSku extends BaseModel
{
    protected $table = 'shopnc_p_cou_level_sku';

    public $timestamps = false;

    protected $fillable = [
        'cou_id',
        'xlevel',
        'sku_id',
        'price'
    ];

    protected $guarded = [];

        
}