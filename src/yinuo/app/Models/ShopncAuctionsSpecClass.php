<?php

namespace App\Models;



/**
 * Class ShopncAuctionsSpecClass
 *
 * @property int $auctions_spec_class_id 拍品分类ID
 * @property string $auctions_spec_class_name 分类名称
 * @property int $auctions_spec_id 上级分类ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpecClass whereAuctionsSpecClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpecClass whereAuctionsSpecClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsSpecClass whereAuctionsSpecId($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsSpecClass extends BaseModel
{
    protected $table = 'shopnc_auctions_spec_class';

    protected $primaryKey = 'auctions_spec_class_id';

	public $timestamps = false;

    protected $fillable = [
        'auctions_spec_class_name',
        'auctions_spec_id'
    ];

    protected $guarded = [];

        
}