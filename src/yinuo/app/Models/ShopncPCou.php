<?php

namespace App\Models;



/**
 * Class ShopncPCou
 *
 * @property int $id ID
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $quota_id 套餐ID
 * @property string $name 名称
 * @property int $tstart 开始时间
 * @property int $tend 结束时间
 * @property int $state 状态 1正常2结束3平台关闭
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereTend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCou whereTstart($value)
 * @mixin \Eloquent
 */
class ShopncPCou extends BaseModel
{
    protected $table = 'shopnc_p_cou';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'quota_id',
        'name',
        'tstart',
        'tend',
        'state'
    ];

    protected $guarded = [];

        
}