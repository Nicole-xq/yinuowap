<?php

namespace App\Models;



/**
 * Class ShopncOrderSnapshot
 *
 * @property int $rec_id 主键
 * @property int $goods_id 商品ID
 * @property int $create_time 生成时间
 * @property string|null $goods_attr 属性
 * @property string|null $file_dir 文件目录
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderSnapshot whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderSnapshot whereFileDir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderSnapshot whereGoodsAttr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderSnapshot whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderSnapshot whereRecId($value)
 * @mixin \Eloquent
 */
class ShopncOrderSnapshot extends BaseModel
{
    protected $table = 'shopnc_order_snapshot';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'create_time',
        'goods_attr',
        'file_dir'
    ];

    protected $guarded = [];

        
}