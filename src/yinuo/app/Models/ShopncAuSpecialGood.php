<?php

namespace App\Models;



/**
 * Class ShopncAuSpecialGood
 *
 * @property int $special_auction_id 专场拍品ID
 * @property int $special_id 专场ID
 * @property int $goods_auction_id 拍品池ID
 * @property string $auction_name 拍品名称
 * @property string $auction_image 拍品主图
 * @property float $auction_start_price 拍品开始价格
 * @property int $store_id 商家ID
 * @property string $store_name 商家名称
 * @property int|null $auction_sp_state 拍品审核状态 0待审核 1通过 2不通过 3再次申请
 * @property int|null $bid_number 临时解决报错
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereAuctionSpState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereAuctionStartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereBidNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereGoodsAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereSpecialAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuSpecialGood whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncAuSpecialGood extends BaseModel
{
    protected $table = 'shopnc_au_special_goods';

    protected $primaryKey = 'special_auction_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'goods_auction_id',
        'auction_name',
        'auction_image',
        'auction_start_price',
        'store_id',
        'store_name',
        'auction_sp_state',
        'bid_number'
    ];

    protected $guarded = [];

        
}