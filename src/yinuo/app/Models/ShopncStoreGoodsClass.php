<?php

namespace App\Models;



/**
 * Class ShopncStoreGoodsClass
 *
 * @property int $stc_id 索引ID
 * @property string $stc_name 店铺商品分类名称
 * @property int $stc_parent_id 父级id
 * @property int $stc_state 店铺商品分类状态
 * @property int $store_id 店铺id
 * @property int $stc_sort 商品分类排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStcParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStcSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStcState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGoodsClass whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreGoodsClass extends BaseModel
{
    protected $table = 'shopnc_store_goods_class';

    protected $primaryKey = 'stc_id';

	public $timestamps = false;

    protected $fillable = [
        'stc_name',
        'stc_parent_id',
        'stc_state',
        'store_id',
        'stc_sort'
    ];

    protected $guarded = [];

        
}