<?php

namespace App\Models;



/**
 * Class ShopncDisTradLog
 *
 * @property int $lg_id 自增编号
 * @property int $lg_member_id 会员编号
 * @property string $lg_member_name 会员名称
 * @property string|null $lg_admin_name 管理员名称
 * @property string $lg_type cash_apply申请提现冻结分销佣金,cash_pay提现成功,cash_del取消提现申请，解冻分销佣金,trad_bill订单结算获得佣金
 * @property float $lg_av_amount 可用金额变更0表示未变更
 * @property float $lg_freeze_amount 冻结金额变更0表示未变更
 * @property int $lg_add_time 添加时间
 * @property string|null $lg_desc 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgAvAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgFreezeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisTradLog whereLgType($value)
 * @mixin \Eloquent
 */
class ShopncDisTradLog extends BaseModel
{
    protected $table = 'shopnc_dis_trad_log';

    protected $primaryKey = 'lg_id';

	public $timestamps = false;

    protected $fillable = [
        'lg_member_id',
        'lg_member_name',
        'lg_admin_name',
        'lg_type',
        'lg_av_amount',
        'lg_freeze_amount',
        'lg_add_time',
        'lg_desc'
    ];

    protected $guarded = [];

        
}