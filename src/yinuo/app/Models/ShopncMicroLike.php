<?php

namespace App\Models;



/**
 * Class ShopncMicroLike
 *
 * @property int $like_id 喜欢编号
 * @property int $like_type 喜欢类型编号
 * @property int $like_object_id 喜欢对象编号
 * @property int $like_member_id 喜欢人编号
 * @property int $like_time 喜欢时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroLike whereLikeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroLike whereLikeMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroLike whereLikeObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroLike whereLikeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroLike whereLikeType($value)
 * @mixin \Eloquent
 */
class ShopncMicroLike extends BaseModel
{
    protected $table = 'shopnc_micro_like';

    protected $primaryKey = 'like_id';

	public $timestamps = false;

    protected $fillable = [
        'like_type',
        'like_object_id',
        'like_member_id',
        'like_time'
    ];

    protected $guarded = [];

        
}