<?php

namespace App\Models;



/**
 * Class ShopncAlbumClass
 *
 * @property int $aclass_id 相册id
 * @property string $aclass_name 相册名称
 * @property int $store_id 所属店铺id
 * @property string $aclass_des 相册描述
 * @property int $aclass_sort 排序
 * @property string $aclass_cover 相册封面
 * @property int $upload_time 图片上传时间
 * @property int $is_default 是否为默认相册,1代表默认
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereAclassCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereAclassDes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereAclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereAclassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereAclassSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAlbumClass whereUploadTime($value)
 * @mixin \Eloquent
 */
class ShopncAlbumClass extends BaseModel
{
    protected $table = 'shopnc_album_class';

    protected $primaryKey = 'aclass_id';

	public $timestamps = false;

    protected $fillable = [
        'aclass_name',
        'store_id',
        'aclass_des',
        'aclass_sort',
        'aclass_cover',
        'upload_time',
        'is_default'
    ];

    protected $guarded = [];

        
}