<?php

namespace App\Models;



/**
 * Class ShopncCmsPictureClass
 *
 * @property int $class_id 分类编号
 * @property string $class_name 分类名称
 * @property int $class_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPictureClass whereClassSort($value)
 * @mixin \Eloquent
 */
class ShopncCmsPictureClass extends BaseModel
{
    protected $table = 'shopnc_cms_picture_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_sort'
    ];

    protected $guarded = [];

        
}