<?php

namespace App\Models;



/**
 * Class ShopncGoodsClass
 *
 * @property int $gc_id 索引ID
 * @property string $gc_name 分类名称
 * @property int|null $type_id 类型id
 * @property string|null $type_name 类型名称
 * @property int $gc_parent_id 父ID
 * @property float $commis_rate 佣金比例
 * @property int $gc_sort 排序
 * @property int $gc_virtual 是否允许发布虚拟商品，1是，0否
 * @property string|null $gc_title 名称
 * @property string|null $gc_keywords 关键词
 * @property string|null $gc_description 描述
 * @property int $show_type 商品展示方式，1按颜色，2按SPU
 * @property int|null $auction_commis_rate 拍卖商品佣金
 * @property string|null $gc_logo 商品分类logo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereAuctionCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereGcVirtual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereShowType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsClass whereTypeName($value)
 * @mixin \Eloquent
 */
class ShopncGoodsClass extends BaseModel
{
    protected $table = 'shopnc_goods_class';

    protected $primaryKey = 'gc_id';

	public $timestamps = false;

    protected $fillable = [
        'gc_name',
        'type_id',
        'type_name',
        'gc_parent_id',
        'commis_rate',
        'gc_sort',
        'gc_virtual',
        'gc_title',
        'gc_keywords',
        'gc_description',
        'show_type',
        'auction_commis_rate',
        'gc_logo'
    ];

    protected $guarded = [];

        
}