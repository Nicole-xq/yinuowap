<?php

namespace App\Models;



/**
 * Class ShopncSnsAlbumclass
 *
 * @property int $ac_id 相册id
 * @property string $ac_name 相册名称
 * @property int $member_id 所属会员id
 * @property string|null $ac_des 相册描述
 * @property int $ac_sort 排序
 * @property string|null $ac_cover 相册封面
 * @property int $upload_time 图片上传时间
 * @property int $is_default 是否为买家秀相册  1为是,0为否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereAcCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereAcDes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereAcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereAcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereAcSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsAlbumclass whereUploadTime($value)
 * @mixin \Eloquent
 */
class ShopncSnsAlbumclass extends BaseModel
{
    protected $table = 'shopnc_sns_albumclass';

    protected $primaryKey = 'ac_id';

	public $timestamps = false;

    protected $fillable = [
        'ac_name',
        'member_id',
        'ac_des',
        'ac_sort',
        'ac_cover',
        'upload_time',
        'is_default'
    ];

    protected $guarded = [];

        
}