<?php

namespace App\Models;



/**
 * Class ShopncMicroPersonal
 *
 * @property int $personal_id 推荐编号
 * @property int $commend_member_id 推荐人用户编号
 * @property string $commend_image 推荐图片
 * @property string $commend_buy 购买信息
 * @property string $commend_message 推荐信息
 * @property int $commend_time 推荐时间
 * @property int $class_id
 * @property int $like_count 喜欢数
 * @property int $comment_count 评论数
 * @property int $click_count
 * @property int $microshop_commend 首页推荐 0-否 1-推荐
 * @property int $microshop_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereClickCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommendBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommendImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommendMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommendMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommendTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereLikeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereMicroshopCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal whereMicroshopSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonal wherePersonalId($value)
 * @mixin \Eloquent
 */
class ShopncMicroPersonal extends BaseModel
{
    protected $table = 'shopnc_micro_personal';

    protected $primaryKey = 'personal_id';

	public $timestamps = false;

    protected $fillable = [
        'commend_member_id',
        'commend_image',
        'commend_buy',
        'commend_message',
        'commend_time',
        'class_id',
        'like_count',
        'comment_count',
        'click_count',
        'microshop_commend',
        'microshop_sort'
    ];

    protected $guarded = [];

        
}