<?php

namespace App\Models;



/**
 * Class ShopncCmsArticle
 *
 * @property int $article_id 文章编号
 * @property string $article_title 文章标题
 * @property int $article_class_id 文章分类编号
 * @property string|null $article_origin 文章来源
 * @property string|null $article_origin_address 文章来源链接
 * @property string $article_author 文章作者
 * @property string|null $article_abstract 文章摘要
 * @property string|null $article_content 文章正文
 * @property string|null $article_image 文章图片
 * @property string|null $article_keyword 文章关键字
 * @property string|null $article_link 相关文章
 * @property string|null $article_goods 相关商品
 * @property int $article_start_time 文章有效期开始时间
 * @property int $article_end_time 文章有效期结束时间
 * @property int $article_publish_time 文章发布时间
 * @property int $article_click 文章点击量
 * @property int $article_sort 文章排序0-255
 * @property int $article_commend_flag 文章推荐标志0-未推荐，1-已推荐
 * @property int $article_comment_flag 文章是否允许评论1-允许，0-不允许
 * @property string|null $article_verify_admin 文章审核管理员
 * @property int $article_verify_time 文章审核时间
 * @property int $article_state 1-草稿、2-待审核、3-已发布、4-回收站
 * @property string $article_publisher_name 发布者用户名
 * @property int $article_publisher_id 发布者编号
 * @property int $article_type 文章类型1-管理员发布，2-用户投稿
 * @property string $article_attachment_path 文章附件路径
 * @property string|null $article_image_all 文章全部图片
 * @property int $article_modify_time 文章修改时间
 * @property string|null $article_tag 文章标签
 * @property int|null $article_comment_count 文章评论数
 * @property int $article_attitude_1 文章心情1
 * @property int $article_attitude_2 文章心情2
 * @property int $article_attitude_3 文章心情3
 * @property int $article_attitude_4 文章心情4
 * @property int $article_attitude_5 文章心情5
 * @property int $article_attitude_6 文章心情6
 * @property string $article_title_short 文章短标题
 * @property int $article_attitude_flag 文章态度开关1-允许，0-不允许
 * @property int $article_commend_image_flag 文章推荐标志(图文)
 * @property int $article_share_count 文章分享数
 * @property string|null $article_verify_reason 审核失败原因
 * @property string|null $article_video 视频链接地址
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAbstract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttachmentPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitude6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAttitudeFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleCommendFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleCommendImageFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleCommentFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleGoods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleImageAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleOriginAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticlePublishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticlePublisherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticlePublisherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleShareCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleTitleShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleVerifyAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleVerifyReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleVerifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsArticle whereArticleVideo($value)
 * @mixin \Eloquent
 */
class ShopncCmsArticle extends BaseModel
{
    protected $table = 'shopnc_cms_article';

    protected $primaryKey = 'article_id';

	public $timestamps = false;

    protected $fillable = [
        'article_title',
        'article_class_id',
        'article_origin',
        'article_origin_address',
        'article_author',
        'article_abstract',
        'article_content',
        'article_image',
        'article_keyword',
        'article_link',
        'article_goods',
        'article_start_time',
        'article_end_time',
        'article_publish_time',
        'article_click',
        'article_sort',
        'article_commend_flag',
        'article_comment_flag',
        'article_verify_admin',
        'article_verify_time',
        'article_state',
        'article_publisher_name',
        'article_publisher_id',
        'article_type',
        'article_attachment_path',
        'article_image_all',
        'article_modify_time',
        'article_tag',
        'article_comment_count',
        'article_attitude_1',
        'article_attitude_2',
        'article_attitude_3',
        'article_attitude_4',
        'article_attitude_5',
        'article_attitude_6',
        'article_title_short',
        'article_attitude_flag',
        'article_commend_image_flag',
        'article_share_count',
        'article_verify_reason',
        'article_video'
    ];

    protected $guarded = [];

        
}