<?php

namespace App\Models;



/**
 * Class ShopncArtistClassify
 *
 * @property int $artist_classify_id 艺术家分类ID
 * @property string $artist_classify_name 艺术家分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistClassify whereArtistClassifyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistClassify whereArtistClassifyName($value)
 * @mixin \Eloquent
 */
class ShopncArtistClassify extends BaseModel
{
    protected $table = 'shopnc_artist_classify';

    protected $primaryKey = 'artist_classify_id';

	public $timestamps = false;

    protected $fillable = [
        'artist_classify_name'
    ];

    protected $guarded = [];

        
}