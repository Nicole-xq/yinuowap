<?php

namespace App\Models;



/**
 * Class ShopncPointsOrderaddress
 *
 * @property int $point_oaid 自增id
 * @property int $point_orderid 订单id
 * @property string $point_truename 收货人姓名
 * @property int $point_areaid 地区id
 * @property string $point_areainfo 地区内容
 * @property string $point_address 详细地址
 * @property string|null $point_telphone 电话号码
 * @property string|null $point_mobphone 手机号码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointMobphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointOaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointOrderid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointTelphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrderaddress wherePointTruename($value)
 * @mixin \Eloquent
 */
class ShopncPointsOrderaddress extends BaseModel
{
    protected $table = 'shopnc_points_orderaddress';

    protected $primaryKey = 'point_oaid';

	public $timestamps = false;

    protected $fillable = [
        'point_orderid',
        'point_truename',
        'point_areaid',
        'point_areainfo',
        'point_address',
        'point_telphone',
        'point_mobphone'
    ];

    protected $guarded = [];

        
}