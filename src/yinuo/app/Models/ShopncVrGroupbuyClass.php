<?php

namespace App\Models;



/**
 * Class ShopncVrGroupbuyClass
 *
 * @property int $class_id 分类id
 * @property string $class_name 分类名称
 * @property int $parent_class_id 父类class_id
 * @property int|null $class_sort 分类排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyClass whereClassSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrGroupbuyClass whereParentClassId($value)
 * @mixin \Eloquent
 */
class ShopncVrGroupbuyClass extends BaseModel
{
    protected $table = 'shopnc_vr_groupbuy_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'parent_class_id',
        'class_sort'
    ];

    protected $guarded = [];

        
}