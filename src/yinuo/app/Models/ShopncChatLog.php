<?php

namespace App\Models;



/**
 * Class ShopncChatLog
 *
 * @property int $m_id 记录ID
 * @property int $f_id 会员ID
 * @property string $f_name 会员名
 * @property string $f_ip 发自IP
 * @property int $t_id 接收会员ID
 * @property string $t_name 接收会员名
 * @property string|null $t_msg 消息内容
 * @property int|null $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereFId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereFIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereFName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereTId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereTMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncChatLog whereTName($value)
 * @mixin \Eloquent
 */
class ShopncChatLog extends BaseModel
{
    protected $table = 'shopnc_chat_log';

    protected $primaryKey = 'm_id';

	public $timestamps = false;

    protected $fillable = [
        'f_id',
        'f_name',
        'f_ip',
        't_id',
        't_name',
        't_msg',
        'add_time'
    ];

    protected $guarded = [];

        
}