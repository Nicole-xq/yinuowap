<?php

namespace App\Models;



/**
 * Class ShopncMbSpecialItem
 *
 * @property int $item_id 专题项目编号
 * @property int $special_id 专题编号
 * @property string $item_type 项目类型
 * @property string|null $item_data
 * @property int $item_usable 项目是否可用 0-不可用 1-可用
 * @property int $item_sort 项目排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereItemData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereItemSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereItemType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereItemUsable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecialItem whereSpecialId($value)
 * @mixin \Eloquent
 */
class ShopncMbSpecialItem extends BaseModel
{
    protected $table = 'shopnc_mb_special_item';

    protected $primaryKey = 'item_id';

	public $timestamps = false;

    protected $fillable = [
        'special_id',
        'item_type',
        'item_data',
        'item_usable',
        'item_sort'
    ];

    protected $guarded = [];

        
}