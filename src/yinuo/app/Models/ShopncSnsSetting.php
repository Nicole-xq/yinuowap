<?php

namespace App\Models;



/**
 * Class ShopncSnsSetting
 *
 * @property int $member_id 会员id
 * @property string|null $setting_skin 皮肤
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSetting whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSetting whereSettingSkin($value)
 * @mixin \Eloquent
 */
class ShopncSnsSetting extends BaseModel
{
    protected $table = 'shopnc_sns_setting';

    protected $primaryKey = 'member_id';

	public $timestamps = false;

    protected $fillable = [
        'setting_skin'
    ];

    protected $guarded = [];

        
}