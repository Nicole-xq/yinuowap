<?php

namespace App\Models;



/**
 * Class ShopncCircleMlref
 *
 * @property int $mlref_id 参考头衔id
 * @property string $mlref_name 参考头衔名称
 * @property string $mlref_addtime 创建时间
 * @property int $mlref_status 状态
 * @property string $mlref_1 1级头衔名称
 * @property string $mlref_2 2级头衔名称
 * @property string $mlref_3 3级头衔名称
 * @property string $mlref_4 4级头衔名称
 * @property string $mlref_5 5级头衔名称
 * @property string $mlref_6 6级头衔名称
 * @property string $mlref_7 7级头衔名称
 * @property string $mlref_8 8级头衔名称
 * @property string $mlref_9 9级头衔名称
 * @property string $mlref_10 10级头衔名称
 * @property string $mlref_11 11级头衔名称
 * @property string $mlref_12 12级头衔名称
 * @property string $mlref_13 13级头衔名称
 * @property string $mlref_14 14级头衔名称
 * @property string $mlref_15 15级头衔名称
 * @property string $mlref_16 16级头衔名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref11($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref13($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref14($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref15($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref16($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlref9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlrefAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlrefId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlrefName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMlref whereMlrefStatus($value)
 * @mixin \Eloquent
 */
class ShopncCircleMlref extends BaseModel
{
    protected $table = 'shopnc_circle_mlref';

    protected $primaryKey = 'mlref_id';

	public $timestamps = false;

    protected $fillable = [
        'mlref_name',
        'mlref_addtime',
        'mlref_status',
        'mlref_1',
        'mlref_2',
        'mlref_3',
        'mlref_4',
        'mlref_5',
        'mlref_6',
        'mlref_7',
        'mlref_8',
        'mlref_9',
        'mlref_10',
        'mlref_11',
        'mlref_12',
        'mlref_13',
        'mlref_14',
        'mlref_15',
        'mlref_16'
    ];

    protected $guarded = [];

        
}