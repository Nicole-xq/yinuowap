<?php

namespace App\Models;



/**
 * Class ShopncCmsNavigation
 *
 * @property int $navigation_id 导航编号
 * @property string $navigation_title 导航标题
 * @property string $navigation_link 导航链接
 * @property int $navigation_sort 排序
 * @property int $navigation_open_type 导航打开方式1-本页打开，2-新页打开
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsNavigation whereNavigationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsNavigation whereNavigationLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsNavigation whereNavigationOpenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsNavigation whereNavigationSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsNavigation whereNavigationTitle($value)
 * @mixin \Eloquent
 */
class ShopncCmsNavigation extends BaseModel
{
    protected $table = 'shopnc_cms_navigation';

    protected $primaryKey = 'navigation_id';

	public $timestamps = false;

    protected $fillable = [
        'navigation_title',
        'navigation_link',
        'navigation_sort',
        'navigation_open_type'
    ];

    protected $guarded = [];

        
}