<?php

namespace App\Models;



/**
 * Class ShopncBidLog
 *
 * @property int $bid_id 出价日志ID
 * @property int $member_id 会员ID
 * @property int $auction_id 拍品ID
 * @property \Carbon\Carbon $created_at 创建时间
 * @property float $offer_num 出价价格
 * @property string|null $member_name 出价人会员名
 * @property int|null $is_anonymous 是否匿名，1：匿名，0：不匿名
 * @property float|null $commission_amount 用户竞价返佣金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereBidId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereCommissionAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereIsAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBidLog whereOfferNum($value)
 * @mixin \Eloquent
 */
class ShopncBidLog extends BaseModel
{
    const BID_TYPE_AUCTION = 1;        //拍卖竞价
    const BID_TYPE_NEWBIE = 2;         //新手专区竞价
    const BID_TYPE_EXP = 3;         //新手体验竞价

    //出价6次之后不做为新手
    const NEWBIE_COUNT = 6;

    protected $table = 'shopnc_bid_log';

    protected $primaryKey = 'bid_id';

	public $timestamps = true;

    protected $fillable = [
        'member_id',
        'auction_id',
        'offer_num',
        'member_name',
        'is_anonymous',
        'commission_amount'
    ];

    protected $guarded = [];

        
}