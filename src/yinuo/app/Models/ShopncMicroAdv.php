<?php

namespace App\Models;



/**
 * Class ShopncMicroAdv
 *
 * @property int $adv_id 广告编号
 * @property string|null $adv_type 广告类型
 * @property string|null $adv_name 广告名称
 * @property string $adv_image 广告图片
 * @property string|null $adv_url 广告链接
 * @property int $adv_sort 广告排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroAdv whereAdvUrl($value)
 * @mixin \Eloquent
 */
class ShopncMicroAdv extends BaseModel
{
    protected $table = 'shopnc_micro_adv';

    protected $primaryKey = 'adv_id';

	public $timestamps = false;

    protected $fillable = [
        'adv_type',
        'adv_name',
        'adv_image',
        'adv_url',
        'adv_sort'
    ];

    protected $guarded = [];

        
}