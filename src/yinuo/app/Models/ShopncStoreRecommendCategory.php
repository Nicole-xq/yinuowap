<?php

namespace App\Models;



/**
 * Class ShopncStoreRecommendCategory
 *
 * @property int $id
 * @property int|null $category_id 分类id
 * @property string|null $category_name 分类名称
 * @property string|null $img 图片
 * @property int|null $status 状态  0:有效 1:无效
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendCategory whereCategoryName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendCategory whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreRecommendCategory whereStatus($value)
 * @mixin \Eloquent
 */
class ShopncStoreRecommendCategory extends BaseModel
{
    protected $table = 'shopnc_store_recommend_category';

    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'category_name',
        'img',
        'status'
    ];

    protected $guarded = [];

        
}