<?php

namespace App\Models;



/**
 * Class ShopncVoteActivity
 *
 * @property int $id
 * @property string|null $title 活动名称
 * @property string|null $description 描述
 * @property string|null $content 内容
 * @property string|null $short_content 活动说明
 * @property string|null $vote_explain 说明
 * @property string|null $start_time
 * @property string|null $end_time
 * @property int|null $vote_type 投票类型:0,按天投票  1,按活动周期投票
 * @property int|null $vote_num 单人可投票数
 * @property int|null $vote_member_num 可投票人数上限
 * @property int|null $notice_num 中奖人数
 * @property int|null $entry_num 报名人数
 * @property string|null $share_logo 分享logo
 * @property string|null $image
 * @property string|null $image_all
 * @property int|null $publish_id 管理员id
 * @property string|null $modify_time
 * @property string|null $add_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereEntryNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereImageAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereNoticeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity wherePublishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereShareLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereVoteExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereVoteMemberNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereVoteNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoteActivity whereVoteType($value)
 * @mixin \Eloquent
 */
class ShopncVoteActivity extends BaseModel
{
    protected $table = 'shopnc_vote_activity';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'content',
        'short_content',
        'vote_explain',
        'start_time',
        'end_time',
        'vote_type',
        'vote_num',
        'vote_member_num',
        'notice_num',
        'entry_num',
        'share_logo',
        'image',
        'image_all',
        'publish_id',
        'modify_time',
        'add_time'
    ];

    protected $guarded = [];

        
}