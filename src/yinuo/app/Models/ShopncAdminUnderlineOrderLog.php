<?php

namespace App\Models;



/**
 * Class ShopncAdminUnderlineOrderLog
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string|null $admin_name
 * @property string|null $content
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminUnderlineOrderLog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminUnderlineOrderLog whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminUnderlineOrderLog whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminUnderlineOrderLog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAdminUnderlineOrderLog whereId($value)
 * @mixin \Eloquent
 */
class ShopncAdminUnderlineOrderLog extends BaseModel
{
    protected $table = 'shopnc_admin_underline_order_log';

    public $timestamps = false;

    protected $fillable = [
        'admin_id',
        'admin_name',
        'content',
        'cdate'
    ];

    protected $guarded = [];

        
}