<?php

namespace App\Models;



/**
 * Class ShopncAuctionSpecial20180301
 *
 * @property int $special_id 专场ID
 * @property string $special_name 专场名称
 * @property int|null $special_type 拍品类型: 0,普通拍品,1,新手拍品
 * @property int|null $store_id 商家ID
 * @property string|null $store_name 商家名称
 * @property int|null $store_vendue_id 商家拍卖ID
 * @property int $special_add_time 专场添加时间
 * @property int $special_start_time 专场开始时间
 * @property int $special_end_time 专场结束时间
 * @property int $special_preview_start 预展开始时间
 * @property string $special_image 专场缩略图
 * @property string $adv_image 专场广告图
 * @property string|null $wap_image wap专场图
 * @property int $special_sponsor 发起者 0平台 1店铺
 * @property string|null $delivery_mechanism 送拍机构
 * @property int|null $auction_num 拍品数量
 * @property int|null $special_state 审核状态 10 未审核 20 审核成功 30审核失败 11 预展开始 12专场开始 13 专场结束
 * @property string|null $special_check_message 审核意见
 * @property int|null $is_open 是否开启 1开启 0关闭
 * @property int|null $special_click 专场围观数
 * @property int|null $participants 专场参与量
 * @property int|null $is_rec 是否推荐 0否 1是
 * @property float|null $special_rate 专场统一利率
 * @property int|null $special_rate_time 计息时间(超过此时间,将不再获得保证金收益)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereAdvImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereAuctionNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereDeliveryMechanism($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereIsOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereIsRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereParticipants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialCheckMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialPreviewStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialRateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialSponsor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereSpecialType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereStoreVendueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionSpecial20180301 whereWapImage($value)
 * @mixin \Eloquent
 */
class ShopncAuctionSpecial20180301 extends BaseModel
{
    protected $table = 'shopnc_auction_special_20180301';

    protected $primaryKey = 'special_id';

	public $timestamps = false;

    protected $fillable = [
        'special_name',
        'special_type',
        'store_id',
        'store_name',
        'store_vendue_id',
        'special_add_time',
        'special_start_time',
        'special_end_time',
        'special_preview_start',
        'special_image',
        'adv_image',
        'wap_image',
        'special_sponsor',
        'delivery_mechanism',
        'auction_num',
        'special_state',
        'special_check_message',
        'is_open',
        'special_click',
        'participants',
        'is_rec',
        'special_rate',
        'special_rate_time'
    ];

    protected $guarded = [];

        
}