<?php

namespace App\Models;



/**
 * Class ShopncExhibitionEntry
 *
 * @property int $id 报名编号
 * @property int $ex_id 活动ID
 * @property int|null $member_id 报名用户ID
 * @property string $member_real_name 报名人姓名
 * @property string|null $member_using_mobile 报名人手机号
 * @property string|null $content 备注留言
 * @property int|null $member_top 邀请人ID
 * @property string|null $member_top_name 邀请人名字
 * @property string|null $member_top_mobile 邀请人手机号
 * @property int $add_time 添加时间
 * @property int $modify_time 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereExId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberTopMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberTopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereMemberUsingMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExhibitionEntry whereModifyTime($value)
 * @mixin \Eloquent
 */
class ShopncExhibitionEntry extends BaseModel
{
    protected $table = 'shopnc_exhibition_entry';

    public $timestamps = false;

    protected $fillable = [
        'ex_id',
        'member_id',
        'member_real_name',
        'member_using_mobile',
        'content',
        'member_top',
        'member_top_name',
        'member_top_mobile',
        'add_time',
        'modify_time'
    ];

    protected $guarded = [];

        
}