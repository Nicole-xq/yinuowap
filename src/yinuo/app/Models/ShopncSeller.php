<?php

namespace App\Models;



/**
 * Class ShopncSeller
 *
 * @property int $seller_id 卖家编号
 * @property string $seller_name 卖家用户名
 * @property int $member_id 用户编号
 * @property int $seller_group_id 卖家组编号
 * @property int $store_id 店铺编号
 * @property int $is_admin 是否管理员(0-不是 1-是)
 * @property string|null $seller_quicklink 卖家快捷操作
 * @property int|null $last_login_time 最后登录时间
 * @property int $is_client 是否客户端用户 0-否 1-是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereIsClient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereLastLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereSellerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereSellerQuicklink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeller whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncSeller extends BaseModel
{
    protected $table = 'shopnc_seller';

    protected $primaryKey = 'seller_id';

	public $timestamps = false;

    protected $fillable = [
        'seller_name',
        'member_id',
        'seller_group_id',
        'store_id',
        'is_admin',
        'seller_quicklink',
        'last_login_time',
        'is_client'
    ];

    protected $guarded = [];

        
}