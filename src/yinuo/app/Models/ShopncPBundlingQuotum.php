<?php

namespace App\Models;



/**
 * Class ShopncPBundlingQuotum
 *
 * @property int $bl_quota_id 套餐ID
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property int $bl_quota_month 购买数量（单位月）
 * @property string $bl_quota_starttime 套餐开始时间
 * @property string $bl_quota_endtime 套餐结束时间
 * @property int $bl_state 套餐状态：0关闭，1开启。默认为 1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereBlQuotaEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereBlQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereBlQuotaMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereBlQuotaStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereBlState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundlingQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPBundlingQuotum extends BaseModel
{
    protected $table = 'shopnc_p_bundling_quota';

    protected $primaryKey = 'bl_quota_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'member_id',
        'member_name',
        'bl_quota_month',
        'bl_quota_starttime',
        'bl_quota_endtime',
        'bl_state'
    ];

    protected $guarded = [];

        
}