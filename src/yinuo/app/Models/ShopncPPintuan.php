<?php

namespace App\Models;



/**
 * Class ShopncPPintuan
 *
 * @property int $pintuan_id 活动编号
 * @property string $pintuan_name 活动名称
 * @property string|null $pintuan_title 活动标题
 * @property string|null $pintuan_explain 活动说明
 * @property int $quota_id 套餐编号
 * @property int $start_time 活动开始时间
 * @property int $end_time 活动结束时间
 * @property int $member_id 用户编号
 * @property int $store_id 店铺编号
 * @property string $member_name 用户名
 * @property string $store_name 店铺名称
 * @property int $min_num 参团人数
 * @property int $state 状态:0取消,1正常
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereMinNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan wherePintuanExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan wherePintuanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan wherePintuanName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan wherePintuanTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuan whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPPintuan extends BaseModel
{
    protected $table = 'shopnc_p_pintuan';

    protected $primaryKey = 'pintuan_id';

	public $timestamps = false;

    protected $fillable = [
        'pintuan_name',
        'pintuan_title',
        'pintuan_explain',
        'quota_id',
        'start_time',
        'end_time',
        'member_id',
        'store_id',
        'member_name',
        'store_name',
        'min_num',
        'state'
    ];

    protected $guarded = [];

        
}