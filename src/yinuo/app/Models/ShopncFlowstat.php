<?php

namespace App\Models;



/**
 * Class ShopncFlowstat
 *
 * @property int $stattime 访问日期
 * @property int $clicknum 访问量
 * @property int $store_id 店铺ID
 * @property string $type 类型
 * @property int $goods_id 商品ID
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat whereClicknum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat whereStattime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat whereType($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFlowstat where#alibabaRdsRowId#($value)
 */
class ShopncFlowstat extends BaseModel
{
    protected $table = 'shopnc_flowstat';

    public $timestamps = false;

    protected $fillable = [
        'stattime',
        'clicknum',
        'store_id',
        'type',
        'goods_id',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}