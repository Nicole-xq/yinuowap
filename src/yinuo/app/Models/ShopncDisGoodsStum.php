<?php

namespace App\Models;



/**
 * Class ShopncDisGoodsStum
 *
 * @property int $goods_commonid 商品公共表ID
 * @property int $store_id 店铺ID
 * @property int $dis_num 领取次数
 * @property int $order_num 分销订单个数
 * @property float $order_goods_amount 订单商品金额
 * @property int $num_update_time 个数更新时间
 * @property float $pay_goods_amount 已支付金额
 * @property int $pay_update_time 支付更新时间
 * @property float $dis_commis_amount 已结分销佣金
 * @property float $order_commis_amount 未结分销佣金
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereDisCommisAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereDisNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereNumUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereOrderCommisAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereOrderGoodsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereOrderNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum wherePayGoodsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum wherePayUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisGoodsStum whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncDisGoodsStum extends BaseModel
{
    protected $table = 'shopnc_dis_goods_sta';

    protected $primaryKey = 'goods_commonid';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'dis_num',
        'order_num',
        'order_goods_amount',
        'num_update_time',
        'pay_goods_amount',
        'pay_update_time',
        'dis_commis_amount',
        'order_commis_amount'
    ];

    protected $guarded = [];

        
}