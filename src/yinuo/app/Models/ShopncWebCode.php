<?php

namespace App\Models;



/**
 * Class ShopncWebCode
 *
 * @property int $code_id 内容ID
 * @property int $web_id 模块ID
 * @property string $code_type 数据类型:array,html,json
 * @property string $var_name 变量名称
 * @property string|null $code_info 内容数据
 * @property string|null $show_name 页面名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereCodeInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereCodeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereShowName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereVarName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebCode whereWebId($value)
 * @mixin \Eloquent
 */
class ShopncWebCode extends BaseModel
{
    protected $table = 'shopnc_web_code';

    protected $primaryKey = 'code_id';

	public $timestamps = false;

    protected $fillable = [
        'web_id',
        'code_type',
        'var_name',
        'code_info',
        'show_name'
    ];

    protected $guarded = [];

        
}