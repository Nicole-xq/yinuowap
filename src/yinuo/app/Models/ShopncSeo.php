<?php

namespace App\Models;



/**
 * Class ShopncSeo
 *
 * @property int $id 主键
 * @property string $title 标题
 * @property string $keywords 关键词
 * @property string $description 描述
 * @property string $type 类型
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeo whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSeo whereType($value)
 * @mixin \Eloquent
 */
class ShopncSeo extends BaseModel
{
    protected $table = 'shopnc_seo';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'keywords',
        'description',
        'type'
    ];

    protected $guarded = [];

        
}