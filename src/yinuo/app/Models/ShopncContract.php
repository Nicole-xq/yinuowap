<?php

namespace App\Models;



/**
 * Class ShopncContract
 *
 * @property int $ct_id 自增ID
 * @property int $ct_storeid 店铺ID
 * @property string $ct_storename 店铺名称
 * @property int $ct_itemid 服务项目ID
 * @property int $ct_auditstate 申请审核状态0未审核1审核通过2审核失败3已支付保证金4保证金审核通过5保证金审核失败
 * @property int $ct_joinstate 加入状态 0未申请 1已申请 2已加入
 * @property float $ct_cost 保证金余额
 * @property int $ct_closestate 永久关闭 0永久关闭 1开启
 * @property int $ct_quitstate 退出申请状态0未申请 1已申请 2申请失败
 * @property int|null $is_auction 是否是拍品服务保障 1：是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtAuditstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtClosestate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtJoinstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtQuitstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereCtStorename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContract whereIsAuction($value)
 * @mixin \Eloquent
 */
class ShopncContract extends BaseModel
{
    protected $table = 'shopnc_contract';

    protected $primaryKey = 'ct_id';

	public $timestamps = false;

    protected $fillable = [
        'ct_storeid',
        'ct_storename',
        'ct_itemid',
        'ct_auditstate',
        'ct_joinstate',
        'ct_cost',
        'ct_closestate',
        'ct_quitstate',
        'is_auction'
    ];

    protected $guarded = [];

        
}