<?php

namespace App\Models;



/**
 * Class ShopncMbSpecial
 *
 * @property int $special_id 专题编号
 * @property string $special_desc 专题描述
 * @property string $type 专题导航类型title 标题 search搜索框
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecial whereSpecialDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecial whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbSpecial whereType($value)
 * @mixin \Eloquent
 */
class ShopncMbSpecial extends BaseModel
{
    protected $table = 'shopnc_mb_special';

    protected $primaryKey = 'special_id';

	public $timestamps = false;

    protected $fillable = [
        'special_desc',
        'type'
    ];

    protected $guarded = [];

        
}