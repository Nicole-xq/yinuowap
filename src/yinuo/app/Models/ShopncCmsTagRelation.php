<?php

namespace App\Models;



/**
 * Class ShopncCmsTagRelation
 *
 * @property int $relation_id 关系编号
 * @property int $relation_type 关系类型1-文章，2-画报
 * @property int $relation_tag_id 标签编号
 * @property int $relation_object_id 对象编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTagRelation whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTagRelation whereRelationObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTagRelation whereRelationTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTagRelation whereRelationType($value)
 * @mixin \Eloquent
 */
class ShopncCmsTagRelation extends BaseModel
{
    protected $table = 'shopnc_cms_tag_relation';

    protected $primaryKey = 'relation_id';

	public $timestamps = false;

    protected $fillable = [
        'relation_type',
        'relation_tag_id',
        'relation_object_id'
    ];

    protected $guarded = [];

        
}