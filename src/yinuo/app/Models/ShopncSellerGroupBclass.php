<?php

namespace App\Models;



/**
 * Class ShopncSellerGroupBclass
 *
 * @property int $bid
 * @property int|null $group_id 权限组ID
 * @property int|null $class_1 一级分类
 * @property int|null $class_2 二级分类
 * @property int|null $class_3 三级分类
 * @property int|null $gc_id 最底级分类
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereBid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereClass1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereClass2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereClass3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerGroupBclass whereGroupId($value)
 * @mixin \Eloquent
 */
class ShopncSellerGroupBclass extends BaseModel
{
    protected $table = 'shopnc_seller_group_bclass';

    protected $primaryKey = 'bid';

	public $timestamps = false;

    protected $fillable = [
        'group_id',
        'class_1',
        'class_2',
        'class_3',
        'gc_id'
    ];

    protected $guarded = [];

        
}