<?php

namespace App\Models;



/**
 * Class ShopncStoreTran
 *
 * @property int $store_trans_id 店铺成交ID
 * @property int $store_vendue_id 店铺拍卖ID
 * @property int $store_id 商家ID
 * @property float $turn_over 累计成交额
 * @property int $trade_goods_num 累计成交商品数
 * @property int $special_num 累计专场数
 * @property int $auction_num 累计拍品数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereAuctionNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereSpecialNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereStoreTransId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereStoreVendueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereTradeGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreTran whereTurnOver($value)
 * @mixin \Eloquent
 */
class ShopncStoreTran extends BaseModel
{
    protected $table = 'shopnc_store_trans';

    protected $primaryKey = 'store_trans_id';

	public $timestamps = false;

    protected $fillable = [
        'store_vendue_id',
        'store_id',
        'turn_over',
        'trade_goods_num',
        'special_num',
        'auction_num'
    ];

    protected $guarded = [];

        
}