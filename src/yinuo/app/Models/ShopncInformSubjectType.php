<?php

namespace App\Models;



/**
 * Class ShopncInformSubjectType
 *
 * @property int $inform_type_id 举报类型id
 * @property string $inform_type_name 举报类型名称
 * @property string $inform_type_desc 举报类型描述
 * @property int $inform_type_state 举报类型状态(1有效/2失效)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubjectType whereInformTypeDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubjectType whereInformTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubjectType whereInformTypeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubjectType whereInformTypeState($value)
 * @mixin \Eloquent
 */
class ShopncInformSubjectType extends BaseModel
{
    protected $table = 'shopnc_inform_subject_type';

    protected $primaryKey = 'inform_type_id';

	public $timestamps = false;

    protected $fillable = [
        'inform_type_name',
        'inform_type_desc',
        'inform_type_state'
    ];

    protected $guarded = [];

        
}