<?php

namespace App\Models;



/**
 * Class ShopncMbPayment
 *
 * @property int $payment_id 支付索引id
 * @property string $payment_code 支付代码名称
 * @property string $payment_name 支付名称
 * @property string|null $payment_config 支付接口配置信息
 * @property string $payment_state 接口状态0禁用1启用
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPayment wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPayment wherePaymentConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPayment wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPayment wherePaymentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbPayment wherePaymentState($value)
 * @mixin \Eloquent
 */
class ShopncMbPayment extends BaseModel
{
    protected $table = 'shopnc_mb_payment';

    protected $primaryKey = 'payment_id';

	public $timestamps = false;

    protected $fillable = [
        'payment_code',
        'payment_name',
        'payment_config',
        'payment_state'
    ];

    protected $guarded = [];

        
}