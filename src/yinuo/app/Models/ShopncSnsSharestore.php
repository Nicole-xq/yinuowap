<?php

namespace App\Models;



/**
 * Class ShopncSnsSharestore
 *
 * @property int $share_id 自增ID
 * @property int $share_storeid 店铺编号
 * @property string $share_storename 店铺名称
 * @property int $share_memberid 所属会员ID
 * @property string $share_membername 所属会员名称
 * @property string|null $share_content 描述内容
 * @property int $share_addtime 添加时间
 * @property int $share_privacy 隐私可见度 0所有人可见 1好友可见 2仅自己可见
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereSharePrivacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsSharestore whereShareStorename($value)
 * @mixin \Eloquent
 */
class ShopncSnsSharestore extends BaseModel
{
    protected $table = 'shopnc_sns_sharestore';

    protected $primaryKey = 'share_id';

	public $timestamps = false;

    protected $fillable = [
        'share_storeid',
        'share_storename',
        'share_memberid',
        'share_membername',
        'share_content',
        'share_addtime',
        'share_privacy'
    ];

    protected $guarded = [];

        
}