<?php

namespace App\Models;



/**
 * Class ShopncComplainSubject
 *
 * @property int $complain_subject_id 投诉主题id
 * @property string $complain_subject_content 投诉主题
 * @property string $complain_subject_desc 投诉主题描述
 * @property int $complain_subject_state 投诉主题状态(1-有效/2-失效)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainSubject whereComplainSubjectContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainSubject whereComplainSubjectDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainSubject whereComplainSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainSubject whereComplainSubjectState($value)
 * @mixin \Eloquent
 */
class ShopncComplainSubject extends BaseModel
{
    protected $table = 'shopnc_complain_subject';

    protected $primaryKey = 'complain_subject_id';

	public $timestamps = false;

    protected $fillable = [
        'complain_subject_content',
        'complain_subject_desc',
        'complain_subject_state'
    ];

    protected $guarded = [];

        
}