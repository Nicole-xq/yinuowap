<?php

namespace App\Models;



/**
 * Class ShopncRechargecard
 *
 * @property int $id 自增ID
 * @property string $sn 卡号
 * @property float $denomination 面额
 * @property string|null $batchflag 批次标识
 * @property string|null $admin_name 创建者名称
 * @property int $tscreated 创建时间
 * @property int $tsused 使用时间
 * @property int $state 状态 0可用 1已用 2已删
 * @property int $member_id 使用者会员ID
 * @property string|null $member_name 使用者会员名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereBatchflag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereDenomination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereTscreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRechargecard whereTsused($value)
 * @mixin \Eloquent
 */
class ShopncRechargecard extends BaseModel
{
    protected $table = 'shopnc_rechargecard';

    public $timestamps = false;

    protected $fillable = [
        'sn',
        'denomination',
        'batchflag',
        'admin_name',
        'tscreated',
        'tsused',
        'state',
        'member_id',
        'member_name'
    ];

    protected $guarded = [];

        
}