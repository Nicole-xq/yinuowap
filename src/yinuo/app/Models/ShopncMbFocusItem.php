<?php

namespace App\Models;



/**
 * Class ShopncMbFocusItem
 *
 * @property int $focus_id 项目编号
 * @property string|null $focus_type 项目类型
 * @property string|null $focus_url 图片链接
 * @property string $focus_image 项目内容
 * @property int $focus_sort 项目排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFocusItem whereFocusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFocusItem whereFocusImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFocusItem whereFocusSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFocusItem whereFocusType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbFocusItem whereFocusUrl($value)
 * @mixin \Eloquent
 */
class ShopncMbFocusItem extends BaseModel
{
    protected $table = 'shopnc_mb_focus_item';

    protected $primaryKey = 'focus_id';

	public $timestamps = false;

    protected $fillable = [
        'focus_type',
        'focus_url',
        'focus_image',
        'focus_sort'
    ];

    protected $guarded = [];

        
}