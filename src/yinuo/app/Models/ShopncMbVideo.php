<?php

namespace App\Models;



/**
 * Class ShopncMbVideo
 *
 * @property int $video_id
 * @property int $store_id 店铺索引id
 * @property int $cate_id 视频分类ID
 * @property string|null $recommend_goods 推荐商品
 * @property string|null $promote_video 推广位视频
 * @property string|null $promote_text 推广位文字
 * @property string|null $demand_video 点播视频
 * @property string|null $promote_image 推广位图片
 * @property string|null $news_name 资讯名称
 * @property string|null $news_image 资讯图片
 * @property string|null $mobile_body 手机端商品描述
 * @property int $add_time 添加时间
 * @property int|null $page_view 点击率
 * @property string $video_identity 标识 news资讯 demand点播 movie直播
 * @property int $member_id 直播播主id
 * @property string $member_name 直播播主名称
 * @property string|null $movie_rand 直播播流随机码
 * @property int $movie_state 直播状态 1是0否
 * @property string|null $movie_title 直播标题
 * @property string|null $movie_cover_img 直播封面图
 * @property int $video_identity_type 标识类型 1资讯 2点播 3直播
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereCateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereDemandVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMobileBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMovieCoverImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMovieRand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMovieState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereMovieTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereNewsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereNewsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo wherePageView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo wherePromoteImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo wherePromoteText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo wherePromoteVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereRecommendGoods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereVideoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereVideoIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbVideo whereVideoIdentityType($value)
 * @mixin \Eloquent
 */
class ShopncMbVideo extends BaseModel
{
    protected $table = 'shopnc_mb_video';

    protected $primaryKey = 'video_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'cate_id',
        'recommend_goods',
        'promote_video',
        'promote_text',
        'demand_video',
        'promote_image',
        'news_name',
        'news_image',
        'mobile_body',
        'add_time',
        'page_view',
        'video_identity',
        'member_id',
        'member_name',
        'movie_rand',
        'movie_state',
        'movie_title',
        'movie_cover_img',
        'video_identity_type'
    ];

    protected $guarded = [];

        
}