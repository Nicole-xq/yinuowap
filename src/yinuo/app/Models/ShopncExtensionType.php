<?php

namespace App\Models;



/**
 * Class ShopncExtensionType
 *
 * @property int $id 主键ID
 * @property string|null $type_name 渠道分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtensionType whereTypeName($value)
 * @mixin \Eloquent
 */
class ShopncExtensionType extends BaseModel
{
    protected $table = 'shopnc_extension_type';

    public $timestamps = false;

    protected $fillable = [
        'type_name'
    ];

    protected $guarded = [];

        
}