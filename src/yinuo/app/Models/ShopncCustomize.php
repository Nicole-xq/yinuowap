<?php

namespace App\Models;



/**
 * Class ShopncCustomize
 *
 * @property int $id
 * @property string $cus_name 姓名
 * @property string $cus_mobile 手机号
 * @property string|null $content 定制内容
 * @property int $member_id 会员ID
 * @property string $add_time 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereCusMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereCusName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCustomize whereMemberId($value)
 * @mixin \Eloquent
 */
class ShopncCustomize extends BaseModel
{
    protected $table = 'shopnc_customize';

    public $timestamps = false;

    protected $fillable = [
        'cus_name',
        'cus_mobile',
        'content',
        'member_id',
        'add_time'
    ];

    protected $guarded = [];

        
}