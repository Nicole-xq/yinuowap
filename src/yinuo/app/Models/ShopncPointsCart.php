<?php

namespace App\Models;



/**
 * Class ShopncPointsCart
 *
 * @property int $pcart_id 自增ID
 * @property int $pmember_id 会员编号
 * @property int $pgoods_id 积分礼品序号
 * @property string $pgoods_name 积分礼品名称
 * @property int $pgoods_points 积分礼品兑换积分
 * @property int $pgoods_choosenum 选择积分礼品数量
 * @property string|null $pgoods_image 积分礼品图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePcartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePgoodsChoosenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePgoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePgoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePgoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePgoodsPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsCart wherePmemberId($value)
 * @mixin \Eloquent
 */
class ShopncPointsCart extends BaseModel
{
    protected $table = 'shopnc_points_cart';

    protected $primaryKey = 'pcart_id';

	public $timestamps = false;

    protected $fillable = [
        'pmember_id',
        'pgoods_id',
        'pgoods_name',
        'pgoods_points',
        'pgoods_choosenum',
        'pgoods_image'
    ];

    protected $guarded = [];

        
}