<?php

namespace App\Models;



/**
 * Class ShopncExtension
 *
 * @property int $id
 * @property int|null $member_id
 * @property string|null $member_mobile 联系电话
 * @property string|null $extension_code 推广码
 * @property string|null $extension_name 渠道名称
 * @property int|null $extension_type 渠道分类：0校园 1机构 2个人
 * @property int|null $extension_class 渠道种类：0线上 1线下
 * @property string|null $area_info 地区
 * @property int|null $total_follow 总关注量
 * @property int|null $total_register 总注册量
 * @property int|null $month_follow 本月关注量
 * @property int|null $month_cancel 本月取关量
 * @property int|null $add_time 添加时间
 * @property int|null $modify_time 修改时间
 * @property string|null $qrcode_img
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereExtensionClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereExtensionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereExtensionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereExtensionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereMonthCancel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereMonthFollow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereQrcodeImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereTotalFollow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncExtension whereTotalRegister($value)
 * @mixin \Eloquent
 */
class ShopncExtension extends BaseModel
{
    protected $table = 'shopnc_extension';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_mobile',
        'extension_code',
        'extension_name',
        'extension_type',
        'extension_class',
        'area_info',
        'total_follow',
        'total_register',
        'month_follow',
        'month_cancel',
        'add_time',
        'modify_time',
        'qrcode_img'
    ];

    protected $guarded = [];

        
}