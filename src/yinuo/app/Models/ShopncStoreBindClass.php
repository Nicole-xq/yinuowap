<?php

namespace App\Models;



/**
 * Class ShopncStoreBindClass
 *
 * @property int $bid
 * @property int|null $store_id 店铺ID
 * @property int|null $commis_rate 佣金比例
 * @property int|null $class_1 一级分类
 * @property int|null $class_2 二级分类
 * @property int|null $class_3 三级分类
 * @property int $state 状态0审核中1已审核 2平台自营店铺
 * @property int|null $auction_commis_rate 拍品佣金比例
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereAuctionCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereBid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereClass1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereClass2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereClass3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreBindClass whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreBindClass extends BaseModel
{
    protected $table = 'shopnc_store_bind_class';

    protected $primaryKey = 'bid';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'commis_rate',
        'class_1',
        'class_2',
        'class_3',
        'state',
        'auction_commis_rate'
    ];

    protected $guarded = [];

        
}