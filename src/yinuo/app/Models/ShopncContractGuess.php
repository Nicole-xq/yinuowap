<?php

namespace App\Models;



/**
 * Class ShopncContractGuess
 *
 * @property int $id 自增ID
 * @property int $gs_id 趣猜ID
 * @property string $gs_name 趣猜名称
 * @property int $member_id 用户ID
 * @property string $member_name 用户名
 * @property int $gs_state 趣猜状态0未开奖 1未中奖 2已中奖
 * @property int $partake_time 参与时间
 * @property int $finished_time 结束时间
 * @property float $gs_offer_price 趣猜出价
 * @property string $gs_img 趣猜图片
 * @property float $gs_cost_price 商品原价总价
 * @property int $verify_state 审核状态 0 待审核 1 已通过 2 未通过
 * @property string $verify_desc 管理员备注
 * @property int $end_time 结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereFinishedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsOfferPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereGsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess wherePartakeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereVerifyDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractGuess whereVerifyState($value)
 * @mixin \Eloquent
 */
class ShopncContractGuess extends BaseModel
{
    protected $table = 'shopnc_contract_guess';

    public $timestamps = false;

    protected $fillable = [
        'gs_id',
        'gs_name',
        'member_id',
        'member_name',
        'gs_state',
        'partake_time',
        'finished_time',
        'gs_offer_price',
        'gs_img',
        'gs_cost_price',
        'verify_state',
        'verify_desc',
        'end_time'
    ];

    protected $guarded = [];

        
}