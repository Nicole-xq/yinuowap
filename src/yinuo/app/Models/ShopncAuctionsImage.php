<?php

namespace App\Models;



/**
 * Class ShopncAuctionsImage
 *
 * @property int $auction_image_id 拍品图片id
 * @property int $auction_id 拍品id
 * @property int $store_id 店铺id
 * @property string $auction_image 拍品图片
 * @property int|null $auction_image_sort 排序
 * @property int|null $is_default 默认主图，1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereAuctionImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereAuctionImageSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionsImage whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncAuctionsImage extends BaseModel
{
    protected $table = 'shopnc_auctions_images';

    protected $primaryKey = 'auction_image_id';

	public $timestamps = false;

    protected $fillable = [
        'auction_id',
        'store_id',
        'auction_image',
        'auction_image_sort',
        'is_default'
    ];

    protected $guarded = [];

        
}