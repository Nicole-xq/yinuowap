<?php

namespace App\Models;



/**
 * Class ShopncPaintingTag
 *
 * @property int $paint_tag_id 书画标签ID
 * @property string $paint_tag_name 书画标签名称
 * @property string $paint_tag_class 类别名称
 * @property int $class_id 所属分类ID
 * @property int $paint_tag_sort  排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintingTag whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintingTag wherePaintTagClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintingTag wherePaintTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintingTag wherePaintTagName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPaintingTag wherePaintTagSort($value)
 * @mixin \Eloquent
 */
class ShopncPaintingTag extends BaseModel
{
    protected $table = 'shopnc_painting_tag';

    protected $primaryKey = 'paint_tag_id';

	public $timestamps = false;

    protected $fillable = [
        'paint_tag_name',
        'paint_tag_class',
        'class_id',
        'paint_tag_sort'
    ];

    protected $guarded = [];

        
}