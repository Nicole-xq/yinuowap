<?php

namespace App\Models;



/**
 * Class ShopncDisMsg2
 *
 * @property int $log_id 日志编号
 * @property int|null $live_id 直播编号
 * @property int|null $member_id 用户编号
 * @property string|null $member_name 用户名
 * @property string|null $msg_txt 消息内容
 * @property int|null $add_time 添加时间
 * @property int|null $msg_state 消息状态 1 正常 2 删除
 * @property int|null $msg_type 消息类型 1访客 2播主
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereLiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereMsgState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereMsgTxt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisMsg2 whereMsgType($value)
 * @mixin \Eloquent
 */
class ShopncDisMsg2 extends BaseModel
{
    protected $table = 'shopnc_dis_msg_2';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'live_id',
        'member_id',
        'member_name',
        'msg_txt',
        'add_time',
        'msg_state',
        'msg_type'
    ];

    protected $guarded = [];

        
}