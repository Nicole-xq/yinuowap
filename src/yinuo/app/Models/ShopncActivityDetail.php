<?php

namespace App\Models;



/**
 * Class ShopncActivityDetail
 *
 * @property int $activity_detail_id id
 * @property int $activity_id 活动编号
 * @property int $item_id 商品或团购的编号
 * @property string $item_name 商品或团购名称
 * @property int $store_id 店铺编号
 * @property string $store_name 店铺名称
 * @property string $activity_detail_state 审核状态 0:(默认)待审核 1:通过 2:未通过 3:再次申请
 * @property int $activity_detail_sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereActivityDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereActivityDetailSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereActivityDetailState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereItemName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivityDetail whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncActivityDetail extends BaseModel
{
    protected $table = 'shopnc_activity_detail';

    protected $primaryKey = 'activity_detail_id';

	public $timestamps = false;

    protected $fillable = [
        'activity_id',
        'item_id',
        'item_name',
        'store_id',
        'store_name',
        'activity_detail_state',
        'activity_detail_sort'
    ];

    protected $guarded = [];

        
}