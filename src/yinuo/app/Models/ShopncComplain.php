<?php

namespace App\Models;



/**
 * Class ShopncComplain
 *
 * @property int $complain_id 投诉id
 * @property int $order_id 订单id
 * @property int|null $order_goods_id 订单商品ID
 * @property int $accuser_id 原告id
 * @property string $accuser_name 原告名称
 * @property int $accused_id 被告id
 * @property string $accused_name 被告名称
 * @property string $complain_subject_content 投诉主题
 * @property int $complain_subject_id 投诉主题id
 * @property string|null $complain_content 投诉内容
 * @property string|null $complain_pic1 投诉图片1
 * @property string|null $complain_pic2 投诉图片2
 * @property string|null $complain_pic3 投诉图片3
 * @property int $complain_datetime 投诉时间
 * @property int|null $complain_handle_datetime 投诉处理时间
 * @property int|null $complain_handle_member_id 投诉处理人id
 * @property string|null $appeal_message 申诉内容
 * @property int|null $appeal_datetime 申诉时间
 * @property string|null $appeal_pic1 申诉图片1
 * @property string|null $appeal_pic2 申诉图片2
 * @property string|null $appeal_pic3 申诉图片3
 * @property string|null $final_handle_message 最终处理意见
 * @property int|null $final_handle_datetime 最终处理时间
 * @property int|null $final_handle_member_id 最终处理人id
 * @property int $complain_state 投诉状态(10-新投诉/20-投诉通过转给被投诉人/30-被投诉人已申诉/40-提交仲裁/99-已关闭)
 * @property int $complain_active 投诉是否通过平台审批(1未通过/2通过)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAccusedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAccusedName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAccuserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAccuserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAppealDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAppealMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAppealPic1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAppealPic2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereAppealPic3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainHandleDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainHandleMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainPic1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainPic2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainPic3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainSubjectContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereComplainSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereFinalHandleDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereFinalHandleMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereFinalHandleMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereOrderGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplain whereOrderId($value)
 * @mixin \Eloquent
 */
class ShopncComplain extends BaseModel
{
    protected $table = 'shopnc_complain';

    protected $primaryKey = 'complain_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_goods_id',
        'accuser_id',
        'accuser_name',
        'accused_id',
        'accused_name',
        'complain_subject_content',
        'complain_subject_id',
        'complain_content',
        'complain_pic1',
        'complain_pic2',
        'complain_pic3',
        'complain_datetime',
        'complain_handle_datetime',
        'complain_handle_member_id',
        'appeal_message',
        'appeal_datetime',
        'appeal_pic1',
        'appeal_pic2',
        'appeal_pic3',
        'final_handle_message',
        'final_handle_datetime',
        'final_handle_member_id',
        'complain_state',
        'complain_active'
    ];

    protected $guarded = [];

        
}