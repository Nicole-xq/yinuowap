<?php

namespace App\Models;



/**
 * Class ShopncCircleClass
 *
 * @property int $class_id 圈子分类id
 * @property string $class_name 圈子分类名称
 * @property string $class_addtime 圈子分类创建时间
 * @property int $class_sort 圈子分类排序
 * @property int $class_status 圈子分类状态 0不显示，1显示
 * @property int $is_recommend 是否推荐 0未推荐，1已推荐
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereClassAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereClassSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereClassStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleClass whereIsRecommend($value)
 * @mixin \Eloquent
 */
class ShopncCircleClass extends BaseModel
{
    protected $table = 'shopnc_circle_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_addtime',
        'class_sort',
        'class_status',
        'is_recommend'
    ];

    protected $guarded = [];

        
}