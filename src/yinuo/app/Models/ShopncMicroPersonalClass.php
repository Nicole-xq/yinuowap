<?php

namespace App\Models;



/**
 * Class ShopncMicroPersonalClass
 *
 * @property int $class_id 分类编号
 * @property string $class_name 分类名称
 * @property int $class_sort 排序
 * @property string|null $class_image 分类图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonalClass whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonalClass whereClassImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonalClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroPersonalClass whereClassSort($value)
 * @mixin \Eloquent
 */
class ShopncMicroPersonalClass extends BaseModel
{
    protected $table = 'shopnc_micro_personal_class';

    protected $primaryKey = 'class_id';

	public $timestamps = false;

    protected $fillable = [
        'class_name',
        'class_sort',
        'class_image'
    ];

    protected $guarded = [];

        
}