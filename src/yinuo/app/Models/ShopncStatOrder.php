<?php

namespace App\Models;



/**
 * Class ShopncStatOrder
 *
 * @property int $order_id 订单id
 * @property int $order_sn 订单编号
 * @property int $order_add_time 订单生成时间
 * @property string|null $payment_code 支付方式
 * @property float $order_amount 订单总价格
 * @property float|null $shipping_fee 运费
 * @property string|null $evaluation_state 评价状态 0未评价，1已评价
 * @property string $order_state 订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;
 * @property int|null $refund_state 退款状态:0是无退款,1是部分退款,2是全部退款
 * @property float|null $refund_amount 退款金额
 * @property string $order_from 1WEB2mobile
 * @property int $order_isvalid 是否为计入统计的有效订单，0为无效 1为有效
 * @property int $reciver_province_id 收货人省级ID
 * @property int $store_id 店铺ID
 * @property string $store_name 卖家店铺名称
 * @property int|null $grade_id 店铺等级
 * @property int|null $sc_id 店铺分类
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家姓名
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereEvaluationState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderIsvalid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereOrderState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereReciverProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereShippingFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatOrder whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncStatOrder extends BaseModel
{
    protected $table = 'shopnc_stat_order';

    protected $primaryKey = 'order_id';

	public $timestamps = false;

    protected $fillable = [
        'order_sn',
        'order_add_time',
        'payment_code',
        'order_amount',
        'shipping_fee',
        'evaluation_state',
        'order_state',
        'refund_state',
        'refund_amount',
        'order_from',
        'order_isvalid',
        'reciver_province_id',
        'store_id',
        'store_name',
        'grade_id',
        'sc_id',
        'buyer_id',
        'buyer_name'
    ];

    protected $guarded = [];

        
}