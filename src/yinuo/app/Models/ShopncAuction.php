<?php

namespace App\Models;



/**
 * Class ShopncAuction
 *
 * @property int $auction_id 拍卖品ID
 * @property int|null $goods_id 商品(SKU)id
 * @property int|null $goods_common_id 商品id
 * @property string $auction_name 拍品名称
 * @property int|null $auction_type 拍品类型: 0,普通拍品,1,新手拍品
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int|null $special_id 专场id
 * @property int $auction_add_time 拍品添加时间
 * @property int|null $auction_edit_time 拍品编辑时间
 * @property int|null $auction_preview_start 预展开始时间
 * @property int $auction_start_time 开始拍卖时间
 * @property int $auction_end_time 结束拍卖时间
 * @property int|null $auction_end_true_t 拍品真实结束时间
 * @property string $auction_image 拍品主图
 * @property string|null $auction_video 拍品视频
 * @property float $auction_bond 拍品保证金
 * @property float $auction_start_price 起拍价
 * @property float $auction_reference_price 参考价
 * @property float $auction_increase_range 加价幅度
 * @property float|null $auction_reserve_price 保留价
 * @property string|null $delivery_mechanism 送拍机构
 * @property int $state 拍卖状态: 0未结束 1拍卖结束
 * @property int|null $gc_id 拍卖分类
 * @property int|null $auction_lock 拍品锁定，0：未锁定，1：锁定
 * @property int|null $auction_click 拍品点击数量
 * @property int $is_liupai 是否流拍，0：未流拍，1：流拍
 * @property float|null $current_price 拍品当前价
 * @property int|null $bid_number 出价次数
 * @property int|null $num_of_applicants 报名人数
 * @property int|null $real_participants 真实参与人数
 * @property int|null $set_reminders_num 设置提醒人数
 * @property int|null $auction_collect 收藏量
 * @property int $recommended 推荐 1：推荐活动 0：普通活动
 * @property int|null $current_price_time 当前价出价时间
 * @property int|null $store_vendue_id 店铺拍卖表ID
 * @property float $auction_bond_rate 保证金利息
 * @property string|null $interest_last_date 最后计息日期
 * @property int|null $auctions_artist_id 艺术家ID
 * @property string|null $auctions_artist_name 艺术家名字
 * @property int|null $auctions_class_lv1 一级分类
 * @property int|null $auctions_class_lv2 二级分类
 * @property string|null $auctions_spec 规格json
 * @property int $auctions_long 长
 * @property int $auctions_width 宽
 * @property int $auctions_height 高
 * @property string|null $create_age 创作年代
 * @property string|null $auctions_summary 作品简介
 * @property string|null $other_images 明细图片
 * @property string|null $share_content 分享描述
 * @property int|null $duration_time 持续时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionBond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionBondRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionCollect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionEndTrueT($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionIncreaseRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionPreviewStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionReferencePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionReservePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionStartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsArtistName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsClassLv1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsClassLv2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereAuctionsWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereBidNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereCreateAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereCurrentPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereCurrentPriceTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereDeliveryMechanism($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereDurationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereGoodsCommonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereInterestLastDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereIsLiupai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereNumOfApplicants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereOtherImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereRealParticipants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereSetRemindersNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereShareContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuction whereStoreVendueId($value)
 * @mixin \Eloquent
 */
class ShopncAuction extends BaseModel
{

    //是否流拍，0：未流拍，1：流拍
    const IS_LIUPAI_NOT = 0;
    const IS_LIUPAI_YES = 1;

    //拍卖状态: 0未结束 1拍卖结束
    const STATE_UNFINISHED = 0;
    const STATE_FINISHED = 1;

    //拍卖状态细化: 1.拍卖结束 2.预展中 3.拍卖中
    const STATUS_FINISHED = 1;
    const STATUS_IN_PREVIEW = 2;
    const STATUS_IN_AUCTION = 3;


    //拍品类型: 0,普通拍品,1,新手拍品 2.新手专区
    const AUCTION_TYPE_COMMON = 0;
    const AUCTION_TYPE_NEW = 1;
    const AUCTION_TYPE_NEWBIE = 2;

    public static $auctionTypeText = [
        self::AUCTION_TYPE_COMMON => "普通拍品",
        self::AUCTION_TYPE_NEW => "新手拍品",
        self::AUCTION_TYPE_NEWBIE => "新手专区",
    ];


    protected $table = 'shopnc_auctions';

    protected $primaryKey = 'auction_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_common_id',
        'auction_name',
        'auction_type',
        'store_id',
        'store_name',
        'special_id',
        'auction_add_time',
        'auction_edit_time',
        'auction_preview_start',
        'auction_start_time',
        'auction_end_time',
        'auction_end_true_t',
        'auction_image',
        'auction_video',
        'auction_bond',
        'auction_start_price',
        'auction_reference_price',
        'auction_increase_range',
        'auction_reserve_price',
        'delivery_mechanism',
        'state',
        'gc_id',
        'auction_lock',
        'auction_click',
        'is_liupai',
        'current_price',
        'bid_number',
        'num_of_applicants',
        'real_participants',
        'set_reminders_num',
        'auction_collect',
        'recommended',
        'current_price_time',
        'store_vendue_id',
        'auction_bond_rate',
        'interest_last_date',
        'auctions_artist_id',
        'auctions_artist_name',
        'auctions_class_lv1',
        'auctions_class_lv2',
        'auctions_spec',
        'auctions_long',
        'auctions_width',
        'auctions_height',
        'create_age',
        'auctions_summary',
        'other_images',
        'share_content',
        'duration_time'
    ];

    protected $guarded = [];

        
}