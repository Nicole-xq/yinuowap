<?php

namespace App\Models;



/**
 * Class ShopncMallConsultType
 *
 * @property int $mct_id 平台客服咨询类型id
 * @property string $mct_name 咨询类型名称
 * @property string $mct_introduce 平台客服咨询类型备注
 * @property int|null $mct_sort 咨询类型排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsultType whereMctId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsultType whereMctIntroduce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsultType whereMctName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsultType whereMctSort($value)
 * @mixin \Eloquent
 */
class ShopncMallConsultType extends BaseModel
{
    protected $table = 'shopnc_mall_consult_type';

    protected $primaryKey = 'mct_id';

	public $timestamps = false;

    protected $fillable = [
        'mct_name',
        'mct_introduce',
        'mct_sort'
    ];

    protected $guarded = [];

        
}