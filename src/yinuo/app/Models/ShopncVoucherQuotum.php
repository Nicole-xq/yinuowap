<?php

namespace App\Models;



/**
 * Class ShopncVoucherQuotum
 *
 * @property int $quota_id 套餐编号
 * @property int $quota_memberid 会员编号
 * @property string $quota_membername 会员名称
 * @property int $quota_storeid 店铺编号
 * @property string $quota_storename 店铺名称
 * @property int $quota_starttime 开始时间
 * @property int $quota_endtime 结束时间
 * @property int $quota_state 状态(1-可用/2-取消/3-结束)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVoucherQuotum whereQuotaStorename($value)
 * @mixin \Eloquent
 */
class ShopncVoucherQuotum extends BaseModel
{
    protected $table = 'shopnc_voucher_quota';

    protected $primaryKey = 'quota_id';

	public $timestamps = false;

    protected $fillable = [
        'quota_memberid',
        'quota_membername',
        'quota_storeid',
        'quota_storename',
        'quota_starttime',
        'quota_endtime',
        'quota_state'
    ];

    protected $guarded = [];

        
}