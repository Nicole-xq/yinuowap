<?php

namespace App\Models;



/**
 * Class ShopncPCouSku
 *
 * @property int $sku_id 商品条目ID
 * @property int $cou_id 加价购ID
 * @property int $tstart 开始时间
 * @property int $tend 结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouSku whereCouId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouSku whereSkuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouSku whereTend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouSku whereTstart($value)
 * @mixin \Eloquent
 */
class ShopncPCouSku extends BaseModel
{
    protected $table = 'shopnc_p_cou_sku';

    public $timestamps = false;

    protected $fillable = [
        'sku_id',
        'cou_id',
        'tstart',
        'tend'
    ];

    protected $guarded = [];

        
}