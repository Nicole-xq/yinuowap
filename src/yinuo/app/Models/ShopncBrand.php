<?php

namespace App\Models;



/**
 * Class ShopncBrand
 *
 * @property int $brand_id 索引ID
 * @property string|null $brand_name 品牌名称
 * @property string $brand_initial 品牌首字母
 * @property string|null $brand_class 类别名称
 * @property string|null $brand_pic 图片
 * @property int|null $brand_sort 排序
 * @property int|null $brand_recommend 推荐，0为否，1为是，默认为0
 * @property int $store_id 店铺ID
 * @property int $brand_apply 品牌申请，0为申请中，1为通过，默认为1，申请功能是会员使用，系统后台默认为1
 * @property int|null $class_id 所属分类id
 * @property int $show_type 品牌展示类型 0表示图片 1表示文字
 * @property int|null $brand_enjoy_zone_sort 尊享品牌排序
 * @property string|null $description 品牌描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandApply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandEnjoyZoneSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandInitial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereBrandSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereShowType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBrand whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncBrand extends BaseModel
{
    protected $table = 'shopnc_brand';

    protected $primaryKey = 'brand_id';

	public $timestamps = false;

    protected $fillable = [
        'brand_name',
        'brand_initial',
        'brand_class',
        'brand_pic',
        'brand_sort',
        'brand_recommend',
        'store_id',
        'brand_apply',
        'class_id',
        'show_type',
        'brand_enjoy_zone_sort',
        'description'
    ];

    protected $guarded = [];

        
}