<?php

namespace App\Models;



/**
 * Class ShopncAuctions20180301
 *
 * @property int $auction_id 拍卖品ID
 * @property int|null $goods_id 商品(SKU)id
 * @property int|null $goods_common_id 商品id
 * @property string $auction_name 拍品名称
 * @property int|null $auction_type 拍品类型: 0,普通拍品,1,新手拍品
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int|null $special_id 专场id
 * @property int $auction_add_time 拍品添加时间
 * @property int|null $auction_edit_time 拍品编辑时间
 * @property int|null $auction_preview_start 预展开始时间
 * @property int $auction_start_time 开始拍卖时间
 * @property int $auction_end_time 结束拍卖时间
 * @property int|null $auction_end_true_t 拍品真实结束时间
 * @property string $auction_image 拍品主图
 * @property string|null $auction_video 拍品视频
 * @property float $auction_bond 拍品保证金
 * @property float $auction_start_price 起拍价
 * @property float $auction_increase_range 加价幅度
 * @property float|null $auction_reserve_price 保留价
 * @property string|null $delivery_mechanism 送拍机构
 * @property int $state 拍卖状态: 0未结束 1拍卖结束
 * @property int|null $gc_id 拍卖分类
 * @property int|null $auction_lock 拍品锁定，0：未锁定，1：锁定
 * @property int|null $auction_click 拍品点击数量
 * @property int $is_liupai 是否流拍，0：未流拍，1：流拍
 * @property float|null $current_price 拍品当前价
 * @property int|null $bid_number 出价次数
 * @property int|null $num_of_applicants 报名人数
 * @property int|null $real_participants 真实参与人数
 * @property int|null $set_reminders_num 设置提醒人数
 * @property int|null $auction_collect 收藏量
 * @property int $recommended 推荐 1：推荐活动 0：普通活动
 * @property int|null $current_price_time 当前价出价时间
 * @property int|null $store_vendue_id 店铺拍卖表ID
 * @property float $auction_bond_rate 保证金利息
 * @property string|null $interest_last_date 最后计息日期
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionBond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionBondRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionCollect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionEndTrueT($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionIncreaseRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionPreviewStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionReservePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionStartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereAuctionVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereBidNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereCurrentPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereCurrentPriceTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereDeliveryMechanism($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereGoodsCommonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereInterestLastDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereIsLiupai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereNumOfApplicants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereRealParticipants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereSetRemindersNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctions20180301 whereStoreVendueId($value)
 * @mixin \Eloquent
 */
class ShopncAuctions20180301 extends BaseModel
{
    protected $table = 'shopnc_auctions_20180301';

    protected $primaryKey = 'auction_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_common_id',
        'auction_name',
        'auction_type',
        'store_id',
        'store_name',
        'special_id',
        'auction_add_time',
        'auction_edit_time',
        'auction_preview_start',
        'auction_start_time',
        'auction_end_time',
        'auction_end_true_t',
        'auction_image',
        'auction_video',
        'auction_bond',
        'auction_start_price',
        'auction_increase_range',
        'auction_reserve_price',
        'delivery_mechanism',
        'state',
        'gc_id',
        'auction_lock',
        'auction_click',
        'is_liupai',
        'current_price',
        'bid_number',
        'num_of_applicants',
        'real_participants',
        'set_reminders_num',
        'auction_collect',
        'recommended',
        'current_price_time',
        'store_vendue_id',
        'auction_bond_rate',
        'interest_last_date'
    ];

    protected $guarded = [];

        
}