<?php

namespace App\Models;



/**
 * Class ShopncSpecialPriceLog
 *
 * @property int $id
 * @property int|null $goods_id
 * @property int|null $goods_common_id
 * @property int|null $store_id 店铺id
 * @property int|null $member_id
 * @property string|null $member_name
 * @property float|null $special_price 协议价
 * @property int|null $status 0,待审核.1,同意.2,不同意
 * @property string|null $udate
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereGoodsCommonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereSpecialPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpecialPriceLog whereUdate($value)
 * @mixin \Eloquent
 */
class ShopncSpecialPriceLog extends BaseModel
{
    protected $table = 'shopnc_special_price_log';

    public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_common_id',
        'store_id',
        'member_id',
        'member_name',
        'special_price',
        'status',
        'udate',
        'cdate'
    ];

    protected $guarded = [];

        
}