<?php

namespace App\Models;



/**
 * Class ShopncPointsOrdergood
 *
 * @property int $point_recid 订单礼品表索引
 * @property int $point_orderid 订单id
 * @property int $point_goodsid 礼品id
 * @property string $point_goodsname 礼品名称
 * @property int $point_goodspoints 礼品兑换积分
 * @property int $point_goodsnum 礼品数量
 * @property string|null $point_goodsimage 礼品图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointGoodsimage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointGoodsname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointGoodsnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointGoodspoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointOrderid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrdergood wherePointRecid($value)
 * @mixin \Eloquent
 */
class ShopncPointsOrdergood extends BaseModel
{
    protected $table = 'shopnc_points_ordergoods';

    protected $primaryKey = 'point_recid';

	public $timestamps = false;

    protected $fillable = [
        'point_orderid',
        'point_goodsid',
        'point_goodsname',
        'point_goodspoints',
        'point_goodsnum',
        'point_goodsimage'
    ];

    protected $guarded = [];

        
}