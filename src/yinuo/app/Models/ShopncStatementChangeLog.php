<?php

namespace App\Models;



/**
 * Class ShopncStatementChangeLog
 *
 * @property int $id
 * @property int|null $statement_id 对账单id
 * @property int|null $admin_id 管理员id
 * @property string|null $content 内容
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatementChangeLog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatementChangeLog whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatementChangeLog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatementChangeLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStatementChangeLog whereStatementId($value)
 * @mixin \Eloquent
 */
class ShopncStatementChangeLog extends BaseModel
{
    protected $table = 'shopnc_statement_change_log';

    public $timestamps = false;

    protected $fillable = [
        'statement_id',
        'admin_id',
        'content',
        'cdate'
    ];

    protected $guarded = [];

        
}