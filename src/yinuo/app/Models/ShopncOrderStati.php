<?php

namespace App\Models;



/**
 * Class ShopncOrderStati
 *
 * @property int $os_month 统计编号(年月)
 * @property int|null $os_year 年
 * @property int $os_start_date 开始日期
 * @property int $os_end_date 结束日期
 * @property float $os_order_totals 订单金额
 * @property float $os_shipping_totals 运费
 * @property float $os_order_return_totals 退单金额
 * @property float $os_commis_totals 佣金金额
 * @property float $os_commis_return_totals 退还佣金
 * @property float $os_store_cost_totals 店铺促销活动费用
 * @property float $os_result_totals 本期应结
 * @property int|null $os_create_date 创建记录日期
 * @property float $os_order_book_totals 被关闭的预定订单的实收总金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsCommisReturnTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsCommisTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsOrderBookTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsOrderReturnTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsOrderTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsResultTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsShippingTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsStoreCostTotals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderStati whereOsYear($value)
 * @mixin \Eloquent
 */
class ShopncOrderStati extends BaseModel
{
    protected $table = 'shopnc_order_statis';

    protected $primaryKey = 'os_month';

	public $timestamps = false;

    protected $fillable = [
        'os_year',
        'os_start_date',
        'os_end_date',
        'os_order_totals',
        'os_shipping_totals',
        'os_order_return_totals',
        'os_commis_totals',
        'os_commis_return_totals',
        'os_store_cost_totals',
        'os_result_totals',
        'os_create_date',
        'os_order_book_totals'
    ];

    protected $guarded = [];

        
}