<?php

namespace App\Models;



/**
 * Class ShopncDisWeb
 *
 * @property int $web_id 模块ID
 * @property string|null $web_name 模块名称
 * @property string|null $style_name 风格名称
 * @property string|null $web_page 所在页面(暂时只有index)
 * @property int $update_time 更新时间
 * @property int|null $web_sort 排序
 * @property int|null $web_show 是否显示，0为否，1为是，默认为1
 * @property string|null $web_html 模块html代码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereStyleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebPage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWeb whereWebSort($value)
 * @mixin \Eloquent
 */
class ShopncDisWeb extends BaseModel
{
    protected $table = 'shopnc_dis_web';

    protected $primaryKey = 'web_id';

	public $timestamps = false;

    protected $fillable = [
        'web_name',
        'style_name',
        'web_page',
        'update_time',
        'web_sort',
        'web_show',
        'web_html'
    ];

    protected $guarded = [];

        
}