<?php

namespace App\Models;



/**
 * Class ShopncAreaAgentAccount
 *
 * @property int $id 账户id
 * @property string $agent_account 区域代理账号
 * @property string $agent_pwd 密码
 * @property int $agent_id 代理ID
 * @property int $last_login_time 最后登录时间
 * @property int $create_time 创建时间
 * @property int $edit_time 编辑时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereAgentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereAgentPwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentAccount whereLastLoginTime($value)
 * @mixin \Eloquent
 */
class ShopncAreaAgentAccount extends BaseModel
{
    protected $table = 'shopnc_area_agent_account';

    public $timestamps = false;

    protected $fillable = [
        'agent_account',
        'agent_pwd',
        'agent_id',
        'last_login_time',
        'create_time',
        'edit_time'
    ];

    protected $guarded = [];

        
}