<?php

namespace App\Models;



/**
 * Class ShopncPdCash
 *
 * @property int $pdc_id 自增编号
 * @property int $pdc_sn 记录唯一标示
 * @property int $pdc_member_id 会员编号
 * @property string $pdc_member_name 会员名称
 * @property float $pdc_amount 金额
 * @property string $pdc_bank_name 收款银行
 * @property string|null $pdc_bank_no 收款账号
 * @property string|null $pdc_bank_user 开户人姓名
 * @property int $pdc_add_time 添加时间
 * @property int|null $pdc_payment_time 付款时间
 * @property string $pdc_payment_state 提现支付状态 0默认1支付完成
 * @property string|null $pdc_payment_admin 支付管理员
 * @property int|null $is_commis 是否是佣金 0 否 1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash whereIsCommis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcBankNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcBankUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcPaymentAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcPaymentState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcPaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPdCash wherePdcSn($value)
 * @mixin \Eloquent
 */
class ShopncPdCash extends BaseModel
{
    protected $table = 'shopnc_pd_cash';

    protected $primaryKey = 'pdc_id';

	public $timestamps = false;

    protected $fillable = [
        'pdc_sn',
        'pdc_member_id',
        'pdc_member_name',
        'pdc_amount',
        'pdc_bank_name',
        'pdc_bank_no',
        'pdc_bank_user',
        'pdc_add_time',
        'pdc_payment_time',
        'pdc_payment_state',
        'pdc_payment_admin',
        'is_commis'
    ];

    protected $guarded = [];

        
}