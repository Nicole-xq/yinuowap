<?php

namespace App\Models;



/**
 * Class ShopncPPintuanGood
 *
 * @property int $pintuan_goods_id 拼团商品编号
 * @property int $pintuan_id 活动编号
 * @property string $pintuan_name 活动名称
 * @property string|null $pintuan_title 活动标题
 * @property string|null $pintuan_explain 活动说明
 * @property int $goods_id 商品编号
 * @property int $store_id 店铺编号
 * @property string $goods_name 商品名称
 * @property float $goods_price 店铺价格
 * @property float $pintuan_price 拼团价格
 * @property string $goods_image 商品图片
 * @property int $start_time 开始时间
 * @property int $end_time 结束时间
 * @property int $min_num 参团人数
 * @property int $state 状态:0取消,1正常
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereMinNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood wherePintuanTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPPintuanGood whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncPPintuanGood extends BaseModel
{
    protected $table = 'shopnc_p_pintuan_goods';

    protected $primaryKey = 'pintuan_goods_id';

	public $timestamps = false;

    protected $fillable = [
        'pintuan_id',
        'pintuan_name',
        'pintuan_title',
        'pintuan_explain',
        'goods_id',
        'store_id',
        'goods_name',
        'goods_price',
        'pintuan_price',
        'goods_image',
        'start_time',
        'end_time',
        'min_num',
        'state'
    ];

    protected $guarded = [];

        
}