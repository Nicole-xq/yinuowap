<?php

namespace App\Models;



/**
 * Class ShopncContractLog
 *
 * @property int $log_id 自增ID
 * @property int $log_storeid 店铺ID
 * @property string $log_storename 店铺名称
 * @property int $log_itemid 服务项目ID
 * @property string $log_itemname 服务项目名称
 * @property string $log_msg 操作描述
 * @property int $log_addtime 添加时间
 * @property string $log_role 操作者角色 管理员为admin 商家为seller
 * @property int $log_userid 操作者ID
 * @property string $log_username 操作者名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogItemname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogStorename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractLog whereLogUsername($value)
 * @mixin \Eloquent
 */
class ShopncContractLog extends BaseModel
{
    protected $table = 'shopnc_contract_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'log_storeid',
        'log_storename',
        'log_itemid',
        'log_itemname',
        'log_msg',
        'log_addtime',
        'log_role',
        'log_userid',
        'log_username'
    ];

    protected $guarded = [];

        
}