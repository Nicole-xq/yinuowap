<?php

namespace App\Models;



/**
 * Class ShopncEvaluateGood
 *
 * @property int $geval_id 评价ID
 * @property int $geval_orderid 订单表自增ID
 * @property int $geval_orderno 订单编号
 * @property int $geval_ordergoodsid 订单商品表编号
 * @property int $geval_goodsid 商品表编号
 * @property string $geval_goodsname 商品名称
 * @property float|null $geval_goodsprice 商品价格
 * @property string|null $geval_goodsimage 商品图片
 * @property int $geval_scores 1-5分
 * @property string|null $geval_content 信誉评价内容
 * @property int $geval_isanonymous 0表示不是 1表示是匿名评价
 * @property int $geval_addtime 评价时间
 * @property int $geval_storeid 店铺编号
 * @property string $geval_storename 店铺名称
 * @property int $geval_frommemberid 评价人编号
 * @property string $geval_frommembername 评价人名称
 * @property string|null $geval_explain 解释内容
 * @property string|null $geval_image 晒单图片
 * @property string $geval_content_again 追加评价内容
 * @property int $geval_addtime_again 追加评价时间
 * @property string $geval_image_again 追加评价图片
 * @property string $geval_explain_again 追加解释内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalAddtimeAgain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalContentAgain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalExplainAgain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalFrommemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalFrommembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalGoodsimage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalGoodsname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalGoodsprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalImageAgain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalIsanonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalOrdergoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalOrderid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalOrderno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalScores($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateGood whereGevalStorename($value)
 * @mixin \Eloquent
 */
class ShopncEvaluateGood extends BaseModel
{
    protected $table = 'shopnc_evaluate_goods';

    protected $primaryKey = 'geval_id';

	public $timestamps = false;

    protected $fillable = [
        'geval_orderid',
        'geval_orderno',
        'geval_ordergoodsid',
        'geval_goodsid',
        'geval_goodsname',
        'geval_goodsprice',
        'geval_goodsimage',
        'geval_scores',
        'geval_content',
        'geval_isanonymous',
        'geval_addtime',
        'geval_storeid',
        'geval_storename',
        'geval_frommemberid',
        'geval_frommembername',
        'geval_explain',
        'geval_image',
        'geval_content_again',
        'geval_addtime_again',
        'geval_image_again',
        'geval_explain_again'
    ];

    protected $guarded = [];

        
}