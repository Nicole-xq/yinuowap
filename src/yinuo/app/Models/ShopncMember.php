<?php

namespace App\Models;



/**
 * Class ShopncMember
 *
 * @property int $member_id 会员id
 * @property string|null $member_key 用户唯一标识key
 * @property string $member_name 会员名称
 * @property string|null $member_truename 会员名称
 * @property string|null $member_avatar 会员头像
 * @property int|null $member_sex 会员性别
 * @property string|null $member_real_name 真实姓名
 * @property string|null $member_using_mobile 正在使用的手机
 * @property string|null $member_profile 会员简介
 * @property string|null $member_birthday 生日
 * @property string $member_passwd 会员密码
 * @property string|null $member_paypwd 支付密码
 * @property string $member_email 会员邮箱
 * @property int $member_email_bind 0未绑定1已绑定
 * @property string|null $member_mobile 手机号
 * @property int $member_mobile_bind 0未绑定1已绑定
 * @property string|null $member_qq qq
 * @property string|null $member_ww 阿里旺旺
 * @property int $member_login_num 登录次数
 * @property string $member_time 会员注册时间
 * @property string $member_login_time 当前登录时间
 * @property string $member_old_login_time 上次登录时间
 * @property string|null $member_login_ip 当前登录ip
 * @property string|null $member_old_login_ip 上次登录ip
 * @property string|null $member_qqopenid qq互联id
 * @property string|null $member_qqinfo qq账号相关信息
 * @property string|null $member_sinaopenid 新浪微博登录id
 * @property string|null $member_sinainfo 新浪账号相关信息序列化值
 * @property string|null $weixin_unionid 微信用户统一标识
 * @property string|null $weixin_info 微信用户相关信息
 * @property string|null $weixin_open_id 微信openID
 * @property int $member_points 会员积分
 * @property float $available_predeposit 预存款可用金额
 * @property float $freeze_predeposit 预存款冻结金额
 * @property float $available_rc_balance 可用充值卡余额
 * @property float $freeze_rc_balance 冻结充值卡余额
 * @property int $inform_allow 是否允许举报(1可以/2不可以)
 * @property int $is_buy 会员是否有购买权限 1为开启 0为关闭
 * @property int $is_allowtalk 会员是否有咨询和发送站内信的权限 1为开启 0为关闭
 * @property int $member_state 会员的开启状态 1为开启 0为关闭
 * @property int $member_snsvisitnum sns空间访问次数
 * @property int|null $member_areaid 地区ID
 * @property int|null $member_cityid 城市ID
 * @property int|null $member_provinceid 省份ID
 * @property string|null $member_areainfo 地区内容
 * @property string|null $member_privacy 隐私设定
 * @property int $member_exppoints 会员经验值
 * @property float|null $trad_amount 佣金总额
 * @property string|null $auth_message 审核意见
 * @property int|null $distri_state 分销状态 0未申请 1待审核 2已通过 3未通过 4清退 5退出
 * @property string|null $bill_user_name 收款人姓名
 * @property string|null $bill_type_code 结算账户类型
 * @property string|null $bill_type_number 收款账号
 * @property string|null $bill_bank_name 开户行
 * @property float|null $freeze_trad 冻结佣金
 * @property string|null $distri_code 分销代码
 * @property int|null $distri_time 申请时间
 * @property int|null $distri_handle_time 处理时间
 * @property int $distri_show 分销中心是否显示 0不显示 1显示
 * @property int|null $quit_time 退出时间
 * @property int|null $distri_apply_times 申请次数
 * @property int|null $distri_quit_times 退出次数
 * @property string|null $alipay_account 支付宝账户
 * @property string|null $alipay_name 支付宝名称
 * @property int|null $member_alipay_bind 支付宝绑定 0否 1是
 * @property float|null $available_margin 保证金账户
 * @property float|null $default_amount 违约金金额
 * @property float|null $freeze_margin 冻结保证金金额
 * @property int|null $member_type 会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家
 * @property int|null $member_role 会员角色：0 注册会员，1 内部会员，2 导入会员
 * @property int|null $freeze_points 冻结积分
 * @property int|null $bind_member_id 绑定会员编号
 * @property string|null $bind_member_name 绑定会员名称
 * @property int|null $bind_time 绑定时间
 * @property float|null $available_commis 已返佣金
 * @property float $freeze_commis 待返佣金
 * @property int|null $is_artist 是否是艺术家 0:否   1:是
 * @property float|null $td_amount 购买特定专区商品金额
 * @property int|null $register_award 是否领取过注册奖励
 * @property int|null $source 用户来源  1,张雄艺术
 * @property int|null $distribute_lv_1 一级合伙人
 * @property int|null $distribute_lv_2 二级合伙人
 * @property int|null $recommended 是否已介绍拍乐赚专场0:未介绍,1:已介绍
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAlipayAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAlipayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAuthMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAvailableCommis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAvailableMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAvailablePredeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereAvailableRcBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBillBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBillTypeCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBillTypeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBillUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBindMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBindMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereBindTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDefaultAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriApplyTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriHandleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriQuitTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistriTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistributeLv1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereDistributeLv2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezeCommis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezeMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezePredeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezeRcBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereFreezeTrad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereInformAllow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereIsAllowtalk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereIsArtist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereIsBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberAlipayBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberCityid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberEmailBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberExppoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberMobileBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberOldLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberOldLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberPaypwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberPrivacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberProvinceid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberQq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberQqinfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberQqopenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberSinainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberSinaopenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberSnsvisitnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberTruename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberUsingMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereMemberWw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereQuitTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereRegisterAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereTdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereTradAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereWeixinInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereWeixinOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMember whereWeixinUnionid($value)
 * @mixin \Eloquent
 */
class ShopncMember extends BaseModel
{
    protected $table = 'shopnc_member';

    protected $primaryKey = 'member_id';

	public $timestamps = false;

    protected $fillable = [
        'member_key',
        'member_name',
        'member_truename',
        'member_avatar',
        'member_sex',
        'member_real_name',
        'member_using_mobile',
        'member_profile',
        'member_birthday',
        'member_passwd',
        'member_paypwd',
        'member_email',
        'member_email_bind',
        'member_mobile',
        'member_mobile_bind',
        'member_qq',
        'member_ww',
        'member_login_num',
        'member_time',
        'member_login_time',
        'member_old_login_time',
        'member_login_ip',
        'member_old_login_ip',
        'member_qqopenid',
        'member_qqinfo',
        'member_sinaopenid',
        'member_sinainfo',
        'weixin_unionid',
        'weixin_info',
        'weixin_open_id',
        'member_points',
        'available_predeposit',
        'freeze_predeposit',
        'available_rc_balance',
        'freeze_rc_balance',
        'inform_allow',
        'is_buy',
        'is_allowtalk',
        'member_state',
        'member_snsvisitnum',
        'member_areaid',
        'member_cityid',
        'member_provinceid',
        'member_areainfo',
        'member_privacy',
        'member_exppoints',
        'trad_amount',
        'auth_message',
        'distri_state',
        'bill_user_name',
        'bill_type_code',
        'bill_type_number',
        'bill_bank_name',
        'freeze_trad',
        'distri_code',
        'distri_time',
        'distri_handle_time',
        'distri_show',
        'quit_time',
        'distri_apply_times',
        'distri_quit_times',
        'alipay_account',
        'alipay_name',
        'member_alipay_bind',
        'available_margin',
        'default_amount',
        'freeze_margin',
        'member_type',
        'member_role',
        'freeze_points',
        'bind_member_id',
        'bind_member_name',
        'bind_time',
        'available_commis',
        'freeze_commis',
        'is_artist',
        'td_amount',
        'register_award',
        'source',
        'distribute_lv_1',
        'distribute_lv_2',
        'recommended'
    ];

    protected $guarded = [];

        
}