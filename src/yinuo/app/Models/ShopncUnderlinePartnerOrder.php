<?php

namespace App\Models;



/**
 * Class ShopncUnderlinePartnerOrder
 *
 * @property int $id
 * @property string|null $order_sn 订单号
 * @property int|null $member_id
 * @property string|null $member_name 用户名
 * @property string|null $type
 * @property float|null $amount
 * @property string|null $order_status 0:未完成,1:已完成,2:已取消
 * @property string|null $trade_no 外部单号
 * @property string|null $finish_time
 * @property string|null $msg 备注
 * @property string|null $cdate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereCdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereFinishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncUnderlinePartnerOrder whereType($value)
 * @mixin \Eloquent
 */
class ShopncUnderlinePartnerOrder extends BaseModel
{
    protected $table = 'shopnc_underline_partner_order';

    public $timestamps = false;

    protected $fillable = [
        'order_sn',
        'member_id',
        'member_name',
        'type',
        'amount',
        'order_status',
        'trade_no',
        'finish_time',
        'msg',
        'cdate'
    ];

    protected $guarded = [];

        
}