<?php

namespace App\Models;



/**
 * Class ShopncPCouLevel
 *
 * @property int $cou_id 加价购ID
 * @property int $xlevel 等级
 * @property float $mincost 最低消费金额
 * @property int $maxcou 最大可凑单数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevel whereCouId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevel whereMaxcou($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevel whereMincost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPCouLevel whereXlevel($value)
 * @mixin \Eloquent
 */
class ShopncPCouLevel extends BaseModel
{
    protected $table = 'shopnc_p_cou_level';

    public $timestamps = false;

    protected $fillable = [
        'cou_id',
        'xlevel',
        'mincost',
        'maxcou'
    ];

    protected $guarded = [];

        
}