<?php

namespace App\Models;



/**
 * Class ShopncSpec
 *
 * @property int $sp_id 规格id
 * @property string $sp_name 规格名称
 * @property int $sp_sort 排序
 * @property int|null $class_id 所属分类id
 * @property string|null $class_name 所属分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpec whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpec whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpec whereSpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpec whereSpName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSpec whereSpSort($value)
 * @mixin \Eloquent
 */
class ShopncSpec extends BaseModel
{
    protected $table = 'shopnc_spec';

    protected $primaryKey = 'sp_id';

	public $timestamps = false;

    protected $fillable = [
        'sp_name',
        'sp_sort',
        'class_id',
        'class_name'
    ];

    protected $guarded = [];

        
}