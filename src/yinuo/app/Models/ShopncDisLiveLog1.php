<?php

namespace App\Models;



/**
 * Class ShopncDisLiveLog1
 *
 * @property int $log_id 日志编号
 * @property int|null $live_id 直播编号
 * @property int|null $member_id 用户编号
 * @property string|null $member_name 用户名
 * @property int|null $add_time 添加时间
 * @property int|null $member_state 访客状态 1正常 2退出
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereLiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisLiveLog1 whereMemberState($value)
 * @mixin \Eloquent
 */
class ShopncDisLiveLog1 extends BaseModel
{
    protected $table = 'shopnc_dis_live_log_1';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'live_id',
        'member_id',
        'member_name',
        'add_time',
        'member_state'
    ];

    protected $guarded = [];

        
}