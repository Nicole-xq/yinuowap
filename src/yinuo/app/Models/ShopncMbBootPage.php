<?php

namespace App\Models;



/**
 * Class ShopncMbBootPage
 *
 * @property int $id 主键id
 * @property string $title 引导标题
 * @property string $boot_image 引导图片
 * @property string|null $wap_link 跳转链接
 * @property int|null $click_num 点击数
 * @property int|null $thumbs_up_num 点赞数
 * @property int|null $share_num 分享数
 * @property int $share_state 分享状态: 1关闭2开启
 * @property int $boot_state 引导状态: 1关闭2开启
 * @property int|null $add_time 添加时间
 * @property int|null $update_time 更新时间
 * @property string $share_desc 分享描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereBootImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereBootState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereShareDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereShareNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereShareState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereThumbsUpNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbBootPage whereWapLink($value)
 * @mixin \Eloquent
 */
class ShopncMbBootPage extends BaseModel
{
    protected $table = 'shopnc_mb_boot_page';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'boot_image',
        'wap_link',
        'click_num',
        'thumbs_up_num',
        'share_num',
        'share_state',
        'boot_state',
        'add_time',
        'update_time',
        'share_desc'
    ];

    protected $guarded = [];

        
}