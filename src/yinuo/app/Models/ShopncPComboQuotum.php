<?php

namespace App\Models;



/**
 * Class ShopncPComboQuotum
 *
 * @property int $cq_id 推荐组合套餐id
 * @property int $store_id 店铺id
 * @property string $store_name 店铺名称
 * @property int $cq_starttime 套餐开始时间
 * @property int $cq_endtime 套餐结束时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboQuotum whereCqEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboQuotum whereCqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboQuotum whereCqStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboQuotum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPComboQuotum whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPComboQuotum extends BaseModel
{
    protected $table = 'shopnc_p_combo_quota';

    protected $primaryKey = 'cq_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'store_name',
        'cq_starttime',
        'cq_endtime'
    ];

    protected $guarded = [];

        
}