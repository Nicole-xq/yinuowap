<?php

namespace App\Models;



/**
 * Class ShopncCircleThpollvoter
 *
 * @property int $theme_id 话题id
 * @property int $member_id 成员id
 * @property string $member_name 成员名称
 * @property string $pollvo_options 投票选项
 * @property string $pollvo_time 投票选项
 * @property int $__#alibaba_rds_row_id#__ Implicit Primary Key by RDS
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter wherePollvoOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter wherePollvoTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter whereThemeId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter where#alibabaRdsRowId#($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpollvoter where#alibabaRdsRowId#($value)
 */
class ShopncCircleThpollvoter extends BaseModel
{
    protected $table = 'shopnc_circle_thpollvoter';

    public $timestamps = false;

    protected $fillable = [
        'theme_id',
        'member_id',
        'member_name',
        'pollvo_options',
        'pollvo_time',
        '__#alibaba_rds_row_id#__'
    ];

    protected $guarded = [];

        
}