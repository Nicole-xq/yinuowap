<?php

namespace App\Models;



/**
 * Class ShopncPointsGood
 *
 * @property int $pgoods_id 积分礼品索引id
 * @property string $pgoods_name 积分礼品名称
 * @property float $pgoods_price 积分礼品原价
 * @property int $pgoods_points 积分礼品兑换所需积分
 * @property string|null $pgoods_image 积分礼品默认封面图片
 * @property string|null $pgoods_tag 积分礼品标签
 * @property string $pgoods_serial 积分礼品货号
 * @property int $pgoods_storage 积分礼品库存数
 * @property int $pgoods_show 积分礼品上架 0表示下架 1表示上架
 * @property int $pgoods_commend 积分礼品推荐
 * @property int $pgoods_add_time 积分礼品添加时间
 * @property string|null $pgoods_keywords 积分礼品关键字
 * @property string|null $pgoods_description 积分礼品描述
 * @property string $pgoods_body 积分礼品详细内容
 * @property int $pgoods_state 积分礼品状态，0开启，1禁售
 * @property string|null $pgoods_close_reason 积分礼品禁售原因
 * @property int $pgoods_salenum 积分礼品售出数量
 * @property int $pgoods_view 积分商品浏览次数
 * @property int $pgoods_islimit 是否限制每会员兑换数量
 * @property int|null $pgoods_limitnum 每会员限制兑换数量
 * @property int $pgoods_islimittime 是否限制兑换时间 0为不限制 1为限制
 * @property int $pgoods_limitmgrade 限制参与兑换的会员级别
 * @property int|null $pgoods_starttime 兑换开始时间
 * @property int|null $pgoods_endtime 兑换结束时间
 * @property int $pgoods_sort 礼品排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsCloseReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsIslimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsIslimittime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsLimitmgrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsLimitnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsSalenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsGood wherePgoodsView($value)
 * @mixin \Eloquent
 */
class ShopncPointsGood extends BaseModel
{
    protected $table = 'shopnc_points_goods';

    protected $primaryKey = 'pgoods_id';

	public $timestamps = false;

    protected $fillable = [
        'pgoods_name',
        'pgoods_price',
        'pgoods_points',
        'pgoods_image',
        'pgoods_tag',
        'pgoods_serial',
        'pgoods_storage',
        'pgoods_show',
        'pgoods_commend',
        'pgoods_add_time',
        'pgoods_keywords',
        'pgoods_description',
        'pgoods_body',
        'pgoods_state',
        'pgoods_close_reason',
        'pgoods_salenum',
        'pgoods_view',
        'pgoods_islimit',
        'pgoods_limitnum',
        'pgoods_islimittime',
        'pgoods_limitmgrade',
        'pgoods_starttime',
        'pgoods_endtime',
        'pgoods_sort'
    ];

    protected $guarded = [];

        
}