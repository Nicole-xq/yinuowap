<?php

namespace App\Models;



/**
 * Class ShopncCircleExpmember
 *
 * @property int $member_id 成员id
 * @property int $circle_id 圈子id
 * @property int $em_exp 获得经验
 * @property string $em_time 获得时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExpmember whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExpmember whereEmExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExpmember whereEmTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleExpmember whereMemberId($value)
 * @mixin \Eloquent
 */
class ShopncCircleExpmember extends BaseModel
{
    protected $table = 'shopnc_circle_expmember';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'circle_id',
        'em_exp',
        'em_time'
    ];

    protected $guarded = [];

        
}