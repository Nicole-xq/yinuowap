<?php

namespace App\Models;



/**
 * Class ShopncCircleThpoll
 *
 * @property int $theme_id 话题id
 * @property int $poll_multiple 单/多选 0单选、1多选
 * @property string $poll_startime 开始时间
 * @property string $poll_endtime 结束时间
 * @property int $poll_days 投票天数
 * @property int $poll_voters 投票参与人数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll wherePollDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll wherePollEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll wherePollMultiple($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll wherePollStartime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll wherePollVoters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThpoll whereThemeId($value)
 * @mixin \Eloquent
 */
class ShopncCircleThpoll extends BaseModel
{
    protected $table = 'shopnc_circle_thpoll';

    protected $primaryKey = 'theme_id';

	public $timestamps = false;

    protected $fillable = [
        'poll_multiple',
        'poll_startime',
        'poll_endtime',
        'poll_days',
        'poll_voters'
    ];

    protected $guarded = [];

        
}