<?php

namespace App\Models;



/**
 * Class ShopncCircleAffix
 *
 * @property int $affix_id 附件id
 * @property string $affix_filename 文件名称
 * @property string $affix_filethumb 缩略图名称
 * @property int $affix_filesize 文件大小，单位字节
 * @property string $affix_addtime 上传时间
 * @property int $affix_type 文件类型 0无 1主题 2评论
 * @property int $member_id 会员id
 * @property int $theme_id 主题id
 * @property int $reply_id 评论id
 * @property int $circle_id 圈子id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixFilesize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixFilethumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereAffixType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereReplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleAffix whereThemeId($value)
 * @mixin \Eloquent
 */
class ShopncCircleAffix extends BaseModel
{
    protected $table = 'shopnc_circle_affix';

    protected $primaryKey = 'affix_id';

	public $timestamps = false;

    protected $fillable = [
        'affix_filename',
        'affix_filethumb',
        'affix_filesize',
        'affix_addtime',
        'affix_type',
        'member_id',
        'theme_id',
        'reply_id',
        'circle_id'
    ];

    protected $guarded = [];

        
}