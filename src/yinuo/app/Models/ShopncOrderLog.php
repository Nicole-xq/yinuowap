<?php

namespace App\Models;



/**
 * Class ShopncOrderLog
 *
 * @property int $log_id 主键
 * @property int $order_id 订单id
 * @property string|null $log_msg 文字描述
 * @property int $log_time 处理时间
 * @property string $log_role 操作角色
 * @property string|null $log_user 操作人
 * @property string|null $log_orderstate 订单状态：0(已取消)10:未付款;20:已付款;30:已发货;40:已收货;
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogOrderstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereLogUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereOrderId($value)
 * @mixin \Eloquent
 * @property string $act_detail 改价明细
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderLog whereActDetail($value)
 */
class ShopncOrderLog extends BaseModel
{
    protected $table = 'shopnc_order_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'log_msg',
        'log_time',
        'log_role',
        'log_user',
        'log_orderstate'
    ];

    protected $guarded = [];

        
}