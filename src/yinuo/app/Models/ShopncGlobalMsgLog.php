<?php

namespace App\Models;



/**
 * Class ShopncGlobalMsgLog
 *
 * @property int $id 日志id
 * @property int $global_id 全局发送ID
 * @property string|null $send_member_id_str 发送会员ids
 * @property int|null $add_time 发送时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgLog whereGlobalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgLog whereSendMemberIdStr($value)
 * @mixin \Eloquent
 */
class ShopncGlobalMsgLog extends BaseModel
{
    protected $table = 'shopnc_global_msg_log';

    public $timestamps = false;

    protected $fillable = [
        'global_id',
        'send_member_id_str',
        'add_time'
    ];

    protected $guarded = [];

        
}