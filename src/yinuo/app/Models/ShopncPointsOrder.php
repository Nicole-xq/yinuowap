<?php

namespace App\Models;



/**
 * Class ShopncPointsOrder
 *
 * @property int $point_orderid 兑换订单编号
 * @property string $point_ordersn 兑换订单编号
 * @property int $point_buyerid 兑换会员id
 * @property string $point_buyername 兑换会员姓名
 * @property string $point_buyeremail 兑换会员email
 * @property int $point_addtime 兑换订单生成时间
 * @property int|null $point_shippingtime 配送时间
 * @property string|null $point_shippingcode 物流单号
 * @property string|null $point_shipping_ecode 物流公司编码
 * @property int|null $point_finnshedtime 订单完成时间
 * @property int $point_allpoint 兑换总积分
 * @property string|null $point_ordermessage 订单留言
 * @property int $point_orderstate 订单状态：20(默认):已兑换并扣除积分;30:已发货;40:已收货;50已完成;2已取消
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointAllpoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointBuyeremail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointBuyerid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointBuyername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointFinnshedtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointOrderid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointOrdermessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointOrdersn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointOrderstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointShippingEcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointShippingcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPointsOrder wherePointShippingtime($value)
 * @mixin \Eloquent
 */
class ShopncPointsOrder extends BaseModel
{
    protected $table = 'shopnc_points_order';

    protected $primaryKey = 'point_orderid';

	public $timestamps = false;

    protected $fillable = [
        'point_ordersn',
        'point_buyerid',
        'point_buyername',
        'point_buyeremail',
        'point_addtime',
        'point_shippingtime',
        'point_shippingcode',
        'point_shipping_ecode',
        'point_finnshedtime',
        'point_allpoint',
        'point_ordermessage',
        'point_orderstate'
    ];

    protected $guarded = [];

        
}