<?php

namespace App\Models;



/**
 * Class ShopncFavorite
 *
 * @property int $log_id 记录ID
 * @property int $member_id 会员ID
 * @property string $member_name 会员名
 * @property int $fav_id 商品或店铺ID
 * @property string $fav_type 类型:goods为商品,store为店铺,默认为商品，auction 为拍卖
 * @property int $fav_time 收藏时间
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int|null $sc_id 店铺分类ID
 * @property string|null $goods_name 商品名称
 * @property string|null $goods_image 商品图片
 * @property int|null $gc_id 商品分类ID
 * @property float|null $log_price 商品收藏时价格
 * @property string|null $log_msg 收藏备注
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereFavId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereFavTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereFavType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereLogMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereLogPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncFavorite whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncFavorite extends BaseModel
{
    protected $table = 'shopnc_favorites';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'fav_id',
        'fav_type',
        'fav_time',
        'store_id',
        'store_name',
        'sc_id',
        'goods_name',
        'goods_image',
        'gc_id',
        'log_price',
        'log_msg'
    ];

    protected $guarded = [];

        
}