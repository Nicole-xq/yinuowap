<?php

namespace App\Models;



/**
 * Class ShopncCircleMember
 *
 * @property int $member_id 会员id
 * @property int $circle_id 圈子id
 * @property string|null $circle_name 圈子名称
 * @property string $member_name 会员名称
 * @property string|null $cm_applycontent 申请内容
 * @property string|null $cm_applytime 申请时间
 * @property int $cm_state 状态 0申请中 1通过 2未通过
 * @property string|null $cm_intro 自我介绍
 * @property string $cm_jointime 加入时间
 * @property int $cm_level 成员级别
 * @property string $cm_levelname 成员头衔
 * @property int $cm_exp 会员经验
 * @property int $cm_nextexp 下一级所需经验
 * @property int|null $is_identity 1圈主 2管理 3成员
 * @property int $is_allowspeak 是否允许发言 1允许 0禁止
 * @property int $is_star 明星成员 1是 0否
 * @property int $cm_thcount 主题数
 * @property int $cm_comcount 回复数
 * @property string|null $cm_lastspeaktime 最后发言时间
 * @property int $is_recommend 是否推荐 1是 0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCircleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmApplycontent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmApplytime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmComcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmJointime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmLastspeaktime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmLevelname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmNextexp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereCmThcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereIsAllowspeak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereIsIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereIsStar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMember whereMemberName($value)
 * @mixin \Eloquent
 */
class ShopncCircleMember extends BaseModel
{
    protected $table = 'shopnc_circle_member';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'circle_id',
        'circle_name',
        'member_name',
        'cm_applycontent',
        'cm_applytime',
        'cm_state',
        'cm_intro',
        'cm_jointime',
        'cm_level',
        'cm_levelname',
        'cm_exp',
        'cm_nextexp',
        'is_identity',
        'is_allowspeak',
        'is_star',
        'cm_thcount',
        'cm_comcount',
        'cm_lastspeaktime',
        'is_recommend'
    ];

    protected $guarded = [];

        
}