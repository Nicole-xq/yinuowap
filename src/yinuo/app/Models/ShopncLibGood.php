<?php

namespace App\Models;



/**
 * Class ShopncLibGood
 *
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property string|null $goods_jingle 商品广告词
 * @property int $gc_id 商品分类id
 * @property int $gc_id_1 一级分类id
 * @property int $gc_id_2 二级分类id
 * @property int $gc_id_3 三级分类id
 * @property string $gc_name 商品分类
 * @property int|null $brand_id 品牌id
 * @property string|null $brand_name 品牌名称
 * @property string|null $goods_barcode 商品条形码
 * @property string $goods_image 商品主图
 * @property string|null $goods_attr 商品属性
 * @property string|null $goods_custom 商品自定义属性
 * @property string|null $goods_img 商品其它图片
 * @property string|null $goods_body 商品描述
 * @property string|null $mobile_body 手机端商品描述
 * @property int $goods_addtime 商品添加时间
 * @property int $goods_edittime 商品编辑时间
 * @property float $goods_trans_kg 重量
 * @property float $goods_trans_v 体积
 * @property string|null $goods_video 商品视频
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGcId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGcId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGcId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsAttr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsEdittime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsJingle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsTransKg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsTransV($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereGoodsVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLibGood whereMobileBody($value)
 * @mixin \Eloquent
 */
class ShopncLibGood extends BaseModel
{
    protected $table = 'shopnc_lib_goods';

    protected $primaryKey = 'goods_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_name',
        'goods_jingle',
        'gc_id',
        'gc_id_1',
        'gc_id_2',
        'gc_id_3',
        'gc_name',
        'brand_id',
        'brand_name',
        'goods_barcode',
        'goods_image',
        'goods_attr',
        'goods_custom',
        'goods_img',
        'goods_body',
        'mobile_body',
        'goods_addtime',
        'goods_edittime',
        'goods_trans_kg',
        'goods_trans_v',
        'goods_video'
    ];

    protected $guarded = [];

        
}