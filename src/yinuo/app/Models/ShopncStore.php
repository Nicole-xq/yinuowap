<?php

namespace App\Models;



/**
 * Class ShopncStore
 *
 * @property int $store_id 店铺索引id
 * @property string $store_name 店铺名称
 * @property string|null $record_number 入驻标号
 * @property int $grade_id 店铺等级
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property string|null $seller_name 店主卖家用户名
 * @property int $sc_id 店铺分类
 * @property string|null $store_company_name 店铺公司名称
 * @property int $area_id 地区ID
 * @property int $city_id 城市ID
 * @property int $province_id 省份ID
 * @property string|null $area_info 地区内容，冗余数据
 * @property string $store_address 详细地区
 * @property string $store_zip 邮政编码
 * @property int $store_state 店铺状态，0关闭，1开启，2审核中
 * @property string|null $store_close_info 店铺关闭原因
 * @property int $store_sort 店铺排序
 * @property string $store_time 店铺时间
 * @property string|null $store_end_time 店铺关闭时间
 * @property string|null $store_label 店铺logo
 * @property string|null $store_banner 店铺横幅
 * @property string|null $store_avatar 店铺头像
 * @property string $store_keywords 店铺seo关键字
 * @property string $store_description 店铺seo描述
 * @property string|null $store_qq QQ
 * @property string|null $store_ww 阿里旺旺
 * @property string|null $store_phone 商家电话
 * @property string|null $store_zy 主营商品
 * @property string|null $store_domain 店铺二级域名
 * @property int|null $store_domain_times 二级域名修改次数
 * @property int $store_recommend 推荐，0为否，1为是，默认为0
 * @property string $store_theme 店铺当前主题
 * @property int $store_credit 店铺信用
 * @property float $store_desccredit 描述相符度分数
 * @property float $store_servicecredit 服务态度分数
 * @property float $store_deliverycredit 发货速度分数
 * @property int $store_collect 店铺收藏数量
 * @property string|null $store_slide 店铺幻灯片
 * @property string|null $store_slide_url 店铺幻灯片链接
 * @property string|null $store_stamp 店铺印章
 * @property string|null $store_printdesc 打印订单页面下方说明文字
 * @property int $store_sales 店铺销量
 * @property string|null $store_presales 售前客服
 * @property string|null $store_aftersales 售后客服
 * @property string|null $store_workingtime 工作时间
 * @property float $store_free_price 超出该金额免运费，大于0才表示该值有效
 * @property int $store_decoration_switch 店铺装修开关(0-关闭 装修编号-开启)
 * @property int $store_decoration_only 开启店铺装修时，仅显示店铺装修(1-是 0-否
 * @property int $store_decoration_image_count 店铺装修相册图片数量
 * @property int $is_own_shop 是否自营店铺 1是 0否
 * @property int $bind_all_gc 自营店是否绑定全部分类 0否1是
 * @property string|null $store_vrcode_prefix 商家兑换码前缀
 * @property string|null $mb_title_img 手机店铺 页头背景图
 * @property string|null $mb_sliders 手机店铺 轮播图链接地址
 * @property int $left_bar_type 店铺商品页面左侧显示类型 1默认 2商城相关分类品牌商品推荐
 * @property string|null $deliver_region 店铺默认配送区域
 * @property int|null $store_type 店铺类型 0商家 1艺术家
 * @property int|null $delayed_time 店铺拍卖延时周期
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereBindAllGc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereDelayedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereDeliverRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereIsOwnShop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereLeftBarType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereMbSliders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereMbTitleImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereRecordNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereScId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreAftersales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreCloseInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreCollect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDecorationImageCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDecorationOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDecorationSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDeliverycredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDesccredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreDomainTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreFreePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStorePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStorePresales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStorePrintdesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreQq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreSales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreServicecredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreSlide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreSlideUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreStamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreVrcodePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreWorkingtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreWw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereStoreZy($value)
 * @mixin \Eloquent
 * @property int $artist_id 艺术家店铺艺术家ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStore whereArtistId($value)
 */
class ShopncStore extends BaseModel
{
    protected $table = 'shopnc_store';

    protected $primaryKey = 'store_id';

	public $timestamps = false;

    protected $fillable = [
        'store_name',
        'record_number',
        'grade_id',
        'member_id',
        'member_name',
        'seller_name',
        'sc_id',
        'store_company_name',
        'area_id',
        'city_id',
        'province_id',
        'area_info',
        'store_address',
        'store_zip',
        'store_state',
        'store_close_info',
        'store_sort',
        'store_time',
        'store_end_time',
        'store_label',
        'store_banner',
        'store_avatar',
        'store_keywords',
        'store_description',
        'store_qq',
        'store_ww',
        'store_phone',
        'store_zy',
        'store_domain',
        'store_domain_times',
        'store_recommend',
        'store_theme',
        'store_credit',
        'store_desccredit',
        'store_servicecredit',
        'store_deliverycredit',
        'store_collect',
        'store_slide',
        'store_slide_url',
        'store_stamp',
        'store_printdesc',
        'store_sales',
        'store_presales',
        'store_aftersales',
        'store_workingtime',
        'store_free_price',
        'store_decoration_switch',
        'store_decoration_only',
        'store_decoration_image_count',
        'is_own_shop',
        'bind_all_gc',
        'store_vrcode_prefix',
        'mb_title_img',
        'mb_sliders',
        'left_bar_type',
        'deliver_region',
        'store_type',
        'delayed_time'
    ];

    protected $guarded = [];

        
}