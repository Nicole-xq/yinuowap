<?php

namespace App\Models;



/**
 * Class ShopncStoreExtend
 *
 * @property int $store_id 店铺ID
 * @property string|null $express 快递公司ID的组合
 * @property string|null $pricerange 店铺统计设置的商品价格区间
 * @property string|null $orderpricerange 店铺统计设置的订单价格区间
 * @property int $bill_cycle 结算周期，单位天，默认0表示未设置，还是按月结算
 * @property int $dis_commis_rate 分销佣金比例
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend whereBillCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend whereExpress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend whereOrderpricerange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend wherePricerange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreExtend whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreExtend extends BaseModel
{
    protected $table = 'shopnc_store_extend';

    protected $primaryKey = 'store_id';

	public $timestamps = false;

    protected $fillable = [
        'express',
        'pricerange',
        'orderpricerange',
        'bill_cycle',
        'dis_commis_rate'
    ];

    protected $guarded = [];

        
}