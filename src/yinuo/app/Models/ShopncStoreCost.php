<?php

namespace App\Models;



/**
 * Class ShopncStoreCost
 *
 * @property int $cost_id 费用编号
 * @property int $cost_store_id 店铺编号
 * @property int $cost_seller_id 卖家编号
 * @property int $cost_price 价格
 * @property string $cost_remark 费用备注
 * @property int $cost_state 费用状态(0-未结算 1-已结算)
 * @property int $cost_time 费用发生时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCost whereCostTime($value)
 * @mixin \Eloquent
 */
class ShopncStoreCost extends BaseModel
{
    protected $table = 'shopnc_store_cost';

    protected $primaryKey = 'cost_id';

	public $timestamps = false;

    protected $fillable = [
        'cost_store_id',
        'cost_seller_id',
        'cost_price',
        'cost_remark',
        'cost_state',
        'cost_time'
    ];

    protected $guarded = [];

        
}