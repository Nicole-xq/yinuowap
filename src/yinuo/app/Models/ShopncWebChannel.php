<?php

namespace App\Models;



/**
 * Class ShopncWebChannel
 *
 * @property int $channel_id 频道ID
 * @property string|null $channel_name 频道名称
 * @property string|null $channel_style 颜色风格
 * @property int|null $gc_id 绑定分类ID
 * @property string|null $gc_name 分类名称
 * @property string|null $keywords 关键词
 * @property string|null $description 描述
 * @property int|null $top_id 顶部楼层编号
 * @property string|null $floor_ids 中部楼层编号
 * @property int $update_time 更新时间
 * @property int $channel_show 启用状态，0为否，1为是，默认为1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereChannelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereChannelShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereChannelStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereFloorIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereGcName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereTopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWebChannel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncWebChannel extends BaseModel
{
    protected $table = 'shopnc_web_channel';

    protected $primaryKey = 'channel_id';

	public $timestamps = false;

    protected $fillable = [
        'channel_name',
        'channel_style',
        'gc_id',
        'gc_name',
        'keywords',
        'description',
        'top_id',
        'floor_ids',
        'update_time',
        'channel_show'
    ];

    protected $guarded = [];

        
}