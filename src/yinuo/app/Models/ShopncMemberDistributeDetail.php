<?php

namespace App\Models;



/**
 * Class ShopncMemberDistributeDetail
 *
 * @property int $id 主键id
 * @property int $dis_id 表：distribute_type主键ID
 * @property int $level 层级1：一级；2：二级
 * @property int $type_id 分销类型id
 * @property float $dz_num 大众合伙人分销百分比
 * @property float $wdl_num 微代理分销百分比
 * @property float $zl_num 战略合伙人百分比
 * @property float $sy_num 事业合伙人百分比
 * @property float $artist_num 艺术家百分比
 * @property float $goods_num 购买艺术品百分比
 * @property float $bzj_num 拍卖保证金百分比
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereArtistNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereBzjNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereDisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereDzNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereSyNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereWdlNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeDetail whereZlNum($value)
 * @mixin \Eloquent
 */
class ShopncMemberDistributeDetail extends BaseModel
{
    protected $table = 'shopnc_member_distribute_detail';

    public $timestamps = false;

    protected $fillable = [
        'dis_id',
        'level',
        'type_id',
        'dz_num',
        'wdl_num',
        'zl_num',
        'sy_num',
        'artist_num',
        'goods_num',
        'bzj_num'
    ];

    protected $guarded = [];

        
}