<?php

namespace App\Models;



/**
 * Class ShopncContractApply
 *
 * @property int $cta_id 申请ID
 * @property int $cta_itemid 保障项目ID
 * @property int $cta_storeid 店铺ID
 * @property string $cta_storename 店铺名称
 * @property int $cta_addtime 申请时间
 * @property int $cta_auditstate 审核状态 0未审核 1审核通过 2审核失败 3保证金待审核 4保证金审核通过 5保证金审核失败
 * @property float|null $cta_cost 保证金金额
 * @property string|null $cta_costimg 保证金付款凭证图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaAuditstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaCostimg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractApply whereCtaStorename($value)
 * @mixin \Eloquent
 */
class ShopncContractApply extends BaseModel
{
    protected $table = 'shopnc_contract_apply';

    protected $primaryKey = 'cta_id';

	public $timestamps = false;

    protected $fillable = [
        'cta_itemid',
        'cta_storeid',
        'cta_storename',
        'cta_addtime',
        'cta_auditstate',
        'cta_cost',
        'cta_costimg'
    ];

    protected $guarded = [];

        
}