<?php

namespace App\Models;



/**
 * Class ShopncGoodsGift
 *
 * @property int $gift_id 赠品id
 * @property int $goods_id 主商品id
 * @property int $goods_commonid 主商品公共id
 * @property int $gift_goodsid 赠品商品id
 * @property string $gift_goodsname 主商品名称
 * @property string $gift_goodsimage 主商品图片
 * @property int $gift_amount 赠品数量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGiftAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGiftGoodsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGiftGoodsimage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGiftGoodsname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGiftId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGoodsGift whereGoodsId($value)
 * @mixin \Eloquent
 */
class ShopncGoodsGift extends BaseModel
{
    protected $table = 'shopnc_goods_gift';

    protected $primaryKey = 'gift_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_commonid',
        'gift_goodsid',
        'gift_goodsname',
        'gift_goodsimage',
        'gift_amount'
    ];

    protected $guarded = [];

        
}