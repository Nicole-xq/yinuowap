<?php

namespace App\Models;



/**
 * Class ShopncVrOrderSnapshot
 *
 * @property int $order_id 主键
 * @property int $goods_id 商品ID
 * @property int $create_time 生成时间
 * @property string|null $goods_attr 属性
 * @property string|null $file_dir 文件目录
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderSnapshot whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderSnapshot whereFileDir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderSnapshot whereGoodsAttr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderSnapshot whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrOrderSnapshot whereOrderId($value)
 * @mixin \Eloquent
 */
class ShopncVrOrderSnapshot extends BaseModel
{
    protected $table = 'shopnc_vr_order_snapshot';

    protected $primaryKey = 'order_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'create_time',
        'goods_attr',
        'file_dir'
    ];

    protected $guarded = [];

        
}