<?php

namespace App\Models;



/**
 * Class ShopncSellerLog
 *
 * @property int $log_id 日志编号
 * @property string $log_content 日志内容
 * @property int $log_time 日志时间
 * @property int $log_seller_id 卖家编号
 * @property string $log_seller_name 卖家账号
 * @property int $log_store_id 店铺编号
 * @property string $log_seller_ip 卖家ip
 * @property string $log_url 日志url
 * @property int $log_state 日志状态(0-失败 1-成功)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogSellerIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSellerLog whereLogUrl($value)
 * @mixin \Eloquent
 */
class ShopncSellerLog extends BaseModel
{
    protected $table = 'shopnc_seller_log';

    protected $primaryKey = 'log_id';

	public $timestamps = false;

    protected $fillable = [
        'log_content',
        'log_time',
        'log_seller_id',
        'log_seller_name',
        'log_store_id',
        'log_seller_ip',
        'log_url',
        'log_state'
    ];

    protected $guarded = [];

        
}