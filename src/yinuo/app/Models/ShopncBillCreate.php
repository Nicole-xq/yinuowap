<?php

namespace App\Models;



/**
 * Class ShopncBillCreate
 *
 * @property int $id
 * @property int|null $store_id
 * @property int|null $os_month
 * @property int|null $os_type 0实物1虚拟
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBillCreate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBillCreate whereOsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBillCreate whereOsType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncBillCreate whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncBillCreate extends BaseModel
{
    protected $table = 'shopnc_bill_create';

    public $timestamps = false;

    protected $fillable = [
        'store_id',
        'os_month',
        'os_type'
    ];

    protected $guarded = [];

        
}