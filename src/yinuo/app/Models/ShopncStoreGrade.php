<?php

namespace App\Models;



/**
 * Class ShopncStoreGrade
 *
 * @property int $sg_id 索引ID
 * @property string|null $sg_name 等级名称
 * @property int $sg_goods_limit 允许发布的商品数量
 * @property int $sg_album_limit 允许上传图片数量
 * @property int $sg_space_limit 上传空间大小，单位MB
 * @property int $sg_template_number 选择店铺模板套数
 * @property string|null $sg_template 模板内容
 * @property float $sg_price 开店费用(元/年)
 * @property string|null $sg_description 申请说明
 * @property string|null $sg_function 附加功能
 * @property int $sg_sort 级别，数目越大级别越高
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgAlbumLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgFunction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgGoodsLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgSpaceLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreGrade whereSgTemplateNumber($value)
 * @mixin \Eloquent
 */
class ShopncStoreGrade extends BaseModel
{
    protected $table = 'shopnc_store_grade';

    protected $primaryKey = 'sg_id';

	public $timestamps = false;

    protected $fillable = [
        'sg_name',
        'sg_goods_limit',
        'sg_album_limit',
        'sg_space_limit',
        'sg_template_number',
        'sg_template',
        'sg_price',
        'sg_description',
        'sg_function',
        'sg_sort'
    ];

    protected $guarded = [];

        
}