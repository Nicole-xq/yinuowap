<?php

namespace App\Models;



/**
 * Class ShopncOrder
 *
 * @property int $order_id 订单索引id
 * @property int $order_sn 订单编号
 * @property int $pay_sn 支付单号
 * @property int|null $pay_sn1 预定订单支付订金时的支付单号
 * @property int $store_id 卖家店铺id
 * @property string $store_name 卖家店铺名称
 * @property int $buyer_id 买家id
 * @property string $buyer_name 买家姓名
 * @property string|null $buyer_email 买家电子邮箱
 * @property int $buyer_phone 买家手机
 * @property int $add_time 订单生成时间
 * @property string $payment_code 支付方式名称代码
 * @property int|null $payment_time 支付(付款)时间
 * @property int $finnshed_time 订单完成时间
 * @property float $goods_amount 商品总价格
 * @property float $order_amount 订单总价格(不包括红包,包括诺币的实价)
 * @property float $rcb_amount 充值卡支付金额
 * @property float $pd_amount 预存款支付金额
 * @property float|null $shipping_fee 运费
 * @property int|null $evaluation_state 评价状态 0未评价，1已评价，2已过期未评价
 * @property int $evaluation_again_state 追加评价状态 0未评价，1已评价，2已过期未评价
 * @property int $order_state 订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;
 * @property int|null $refund_state 退款状态:0是无退款,1是部分退款,2是全部退款
 * @property int|null $lock_state 锁定状态:0是正常,大于0是锁定,默认是0
 * @property int $delete_state 删除状态0未删除1放入回收站2彻底删除
 * @property float|null $refund_amount 退款金额
 * @property int|null $delay_time 延迟时间,默认为0
 * @property int $order_from 1WEB2mobile
 * @property string|null $shipping_code 物流单号
 * @property int|null $order_type 订单类型1普通订单(默认),2预定订单,3门店自提订单 4拍卖订单
 * @property int|null $api_pay_time 在线支付动作时间,只要向第三方支付平台提交就会更新
 * @property float|null $api_pay_amount 第三方支付金额
 * @property int $chain_id 自提门店ID
 * @property int $chain_code 门店提货码
 * @property float|null $rpt_amount 红包值
 * @property string|null $trade_no 外部交易订单号
 * @property int $is_dis 是否分销订单
 * @property string|null $pay_voucher 订单的付款凭证
 * @property int|null $is_points 是否使用积分 0否 1是
 * @property float|null $points_amount 积分抵扣现金
 * @property float|null $margin_amount 保证金抵扣
 * @property int $gs_id 趣猜id
 * @property int|null $commission_state 返佣状态
 * @property int|null $auction_id 拍卖id
 * @property int|null $notice_state 消息提醒：0未提醒，1已经提醒
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereApiPayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereApiPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereBuyerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereBuyerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereChainCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereChainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereCommissionState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereDelayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereDeleteState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereEvaluationAgainState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereEvaluationState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereFinnshedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereGoodsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereGsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereIsDis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereIsPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereLockState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereMarginAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereNoticeState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePaySn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePaySn1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePayVoucher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder wherePointsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereRptAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereShippingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereShippingFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereTradeNo($value)
 * @mixin \Eloquent
 * @property int $artist_closing_status 艺术家结算状态 0.默认状态未结算  10.已结算
 * @property string|null $artist_closing_time 艺术家结算时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereArtistClosingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereArtistClosingTime($value)
 * @property float|null $order_amount_original 订单原始总价格
 * @property string|null $artist_closing_no 艺术家结算编号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereArtistClosingNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereOrderAmountOriginal($value)
 * @property float|null $artist_closing_amount 艺术家结算金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrder whereArtistClosingAmount($value)
 */
class ShopncOrder extends BaseModel
{
    //订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;
    const ORDER_STATE_CANCEL = 0;
    const ORDER_STATE_UN_PAY = 10;
    const ORDER_STATE_PAID = 20;
    const ORDER_STATE_DELIVERED = 30;
    const ORDER_STATE_RECEIVED = 40;

    //艺术家结算状态 0.默认状态未结算  10.已结算
    const ARTIST_CLOSING_STATUS_UN = 0;
    const ARTIST_CLOSING_STATUS_DONE = 10;

    //订单类型
    const ORDER_TYPE_AUCTION = 4;

    protected $table = 'shopnc_orders';

    protected $primaryKey = 'order_id';

    public $timestamps = false;

    /**
     * 应该被转换成原生类型的属性。
     *
     * @var array
     */
    protected $casts = [
        'pay_sn' => 'string',
        'order_sn' => 'string',
    ];

    protected $fillable = [
        'order_sn',
        'pay_sn',
        'pay_sn1',
        'store_id',
        'store_name',
        'buyer_id',
        'buyer_name',
        'buyer_email',
        'buyer_phone',
        'add_time',
        'payment_code',
        'payment_time',
        'finnshed_time',
        'goods_amount',
        'order_amount',
        'rcb_amount',
        'pd_amount',
        'shipping_fee',
        'evaluation_state',
        'evaluation_again_state',
        'order_state',
        'refund_state',
        'lock_state',
        'delete_state',
        'refund_amount',
        'delay_time',
        'order_from',
        'shipping_code',
        'order_type',
        'api_pay_time',
        'api_pay_amount',
        'chain_id',
        'chain_code',
        'rpt_amount',
        'trade_no',
        'is_dis',
        'pay_voucher',
        'is_points',
        'points_amount',
        'margin_amount',
        'gs_id',
        'commission_state',
        'auction_id',
        'notice_state'
    ];

    protected $guarded = [];


}