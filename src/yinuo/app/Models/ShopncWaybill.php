<?php

namespace App\Models;



/**
 * Class ShopncWaybill
 *
 * @property int $waybill_id 编号
 * @property string $waybill_name
 * @property string $waybill_image
 * @property int $waybill_width 宽度
 * @property int $waybill_height 高度
 * @property string|null $waybill_data 打印位置数据
 * @property int $waybill_usable 是否可用
 * @property int $waybill_top 上偏移量
 * @property int $waybill_left 左偏移量
 * @property int $express_id 快递公司编号
 * @property string $express_name 快递公司名称
 * @property int $store_id 店铺编号(0-代表系统模板)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereExpressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereExpressName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillUsable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncWaybill whereWaybillWidth($value)
 * @mixin \Eloquent
 */
class ShopncWaybill extends BaseModel
{
    protected $table = 'shopnc_waybill';

    protected $primaryKey = 'waybill_id';

	public $timestamps = false;

    protected $fillable = [
        'waybill_name',
        'waybill_image',
        'waybill_width',
        'waybill_height',
        'waybill_data',
        'waybill_usable',
        'waybill_top',
        'waybill_left',
        'express_id',
        'express_name',
        'store_id'
    ];

    protected $guarded = [];

        
}