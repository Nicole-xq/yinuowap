<?php

namespace App\Models;



/**
 * Class ShopncMicroStore
 *
 * @property int $microshop_store_id 店铺街店铺编号
 * @property int $shop_store_id 商城店铺编号
 * @property int|null $microshop_sort 排序
 * @property int|null $microshop_commend 推荐首页标志 1-正常 2-推荐
 * @property int $like_count 喜欢数
 * @property int $comment_count 评论数
 * @property int $click_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereClickCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereLikeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereMicroshopCommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereMicroshopSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereMicroshopStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMicroStore whereShopStoreId($value)
 * @mixin \Eloquent
 */
class ShopncMicroStore extends BaseModel
{
    protected $table = 'shopnc_micro_store';

    protected $primaryKey = 'microshop_store_id';

	public $timestamps = false;

    protected $fillable = [
        'shop_store_id',
        'microshop_sort',
        'microshop_commend',
        'like_count',
        'comment_count',
        'click_count'
    ];

    protected $guarded = [];

        
}