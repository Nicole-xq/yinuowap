<?php

namespace App\Models;



/**
 * Class ShopncRefundReturn
 *
 * @property int $refund_id 记录ID
 * @property int $order_id 订单ID
 * @property string $order_sn 订单编号
 * @property string $refund_sn 申请编号
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property int $buyer_id 买家ID
 * @property string $buyer_name 买家会员名
 * @property int $goods_id 商品ID,全部退款是0
 * @property int|null $order_goods_id 订单商品ID,全部退款是0
 * @property string $goods_name 商品名称
 * @property int|null $goods_num 商品数量
 * @property float|null $refund_amount 退款金额
 * @property string|null $goods_image 商品图片
 * @property int|null $order_goods_type 订单商品类型:1默认2团购商品3限时折扣商品4组合套装
 * @property int|null $refund_type 申请类型:1为退款,2为退货,默认为1
 * @property int|null $seller_state 卖家处理状态:1为待审核,2为同意,3为不同意,默认为1
 * @property int|null $refund_state 申请状态:1为处理中,2为待管理员处理,3为已完成,默认为1
 * @property int|null $return_type 退货类型:1为不用退货,2为需要退货,默认为1
 * @property int|null $order_lock 订单锁定类型:1为不用锁定,2为需要锁定,默认为1
 * @property int|null $goods_state 物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为1
 * @property int $add_time 添加时间
 * @property int|null $seller_time 卖家处理时间
 * @property int|null $admin_time 管理员处理时间,默认为0
 * @property int|null $reason_id 原因ID:0为其它
 * @property string|null $reason_info 原因内容
 * @property string|null $pic_info 图片
 * @property string|null $buyer_message 申请原因
 * @property string|null $seller_message 卖家备注
 * @property string|null $admin_message 管理员备注
 * @property int|null $express_id 物流公司编号
 * @property string|null $invoice_no 物流单号
 * @property int|null $ship_time 发货时间,默认为0
 * @property int|null $delay_time 收货延迟时间,默认为0
 * @property int|null $receive_time 收货时间,默认为0
 * @property string|null $receive_message 收货备注
 * @property int|null $commis_rate 佣金比例
 * @property float $rpt_amount 退款红包值，默认0，只有全部退款时才把该订单使用的红包金额写到此处
 * @property int|null $is_dis 是否分销商品
 * @property int|null $dis_commis_rate 分销佣金比例
 * @property int|null $goods_commonid 商品公共表ID
 * @property int|null $dis_pay_state 分销结算状态:0为未结,1为已结
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereAdminMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereAdminTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereBuyerMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereDelayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereDisPayState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereExpressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereGoodsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereInvoiceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereIsDis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereOrderGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereOrderGoodsType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereOrderLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereOrderSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn wherePicInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereReasonInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereReceiveMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereReceiveTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRefundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRefundSn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRefundState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRefundType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereReturnType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereRptAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereSellerMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereSellerState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereSellerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereShipTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReturn whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncRefundReturn extends BaseModel
{
    protected $table = 'shopnc_refund_return';

    protected $primaryKey = 'refund_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_sn',
        'refund_sn',
        'store_id',
        'store_name',
        'buyer_id',
        'buyer_name',
        'goods_id',
        'order_goods_id',
        'goods_name',
        'goods_num',
        'refund_amount',
        'goods_image',
        'order_goods_type',
        'refund_type',
        'seller_state',
        'refund_state',
        'return_type',
        'order_lock',
        'goods_state',
        'add_time',
        'seller_time',
        'admin_time',
        'reason_id',
        'reason_info',
        'pic_info',
        'buyer_message',
        'seller_message',
        'admin_message',
        'express_id',
        'invoice_no',
        'ship_time',
        'delay_time',
        'receive_time',
        'receive_message',
        'commis_rate',
        'rpt_amount',
        'is_dis',
        'dis_commis_rate',
        'goods_commonid',
        'dis_pay_state'
    ];

    protected $guarded = [];

        
}