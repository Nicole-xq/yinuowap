<?php

namespace App\Models;



/**
 * Class ShopncType
 *
 * @property int $type_id 类型id
 * @property string $type_name 类型名称
 * @property int $type_sort 排序
 * @property int|null $class_id 所属分类id
 * @property string|null $class_name 所属分类名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncType whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncType whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncType whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncType whereTypeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncType whereTypeSort($value)
 * @mixin \Eloquent
 */
class ShopncType extends BaseModel
{
    protected $table = 'shopnc_type';

    protected $primaryKey = 'type_id';

	public $timestamps = false;

    protected $fillable = [
        'type_name',
        'type_sort',
        'class_id',
        'class_name'
    ];

    protected $guarded = [];

        
}