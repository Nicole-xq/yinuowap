<?php

namespace App\Models;



/**
 * Class ShopncCircleMldefault
 *
 * @property int $mld_id 头衔等级
 * @property string $mld_name 头衔名称
 * @property int $mld_exp 所需经验值
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMldefault whereMldExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMldefault whereMldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMldefault whereMldName($value)
 * @mixin \Eloquent
 */
class ShopncCircleMldefault extends BaseModel
{
    protected $table = 'shopnc_circle_mldefault';

    protected $primaryKey = 'mld_id';

	public $timestamps = false;

    protected $fillable = [
        'mld_name',
        'mld_exp'
    ];

    protected $guarded = [];

        
}