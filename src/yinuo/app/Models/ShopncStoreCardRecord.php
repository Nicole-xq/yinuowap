<?php

namespace App\Models;



/**
 * Class ShopncStoreCardRecord
 *
 * @property int $id id
 * @property string $f_num 首字母类型
 * @property int $type 入驻类型 0商家 1艺术家
 * @property string $date 日期
 * @property int $l_num 最后序号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCardRecord whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCardRecord whereFNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCardRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCardRecord whereLNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreCardRecord whereType($value)
 * @mixin \Eloquent
 */
class ShopncStoreCardRecord extends BaseModel
{
    protected $table = 'shopnc_store_card_record';

    public $timestamps = false;

    protected $fillable = [
        'f_num',
        'type',
        'date',
        'l_num'
    ];

    protected $guarded = [];

        
}