<?php

namespace App\Models;



/**
 * Class ShopncAreaAgentCommissionSet
 *
 * @property int $id 主键id
 * @property string $commission_set_name 返佣配置名称
 * @property int $commission_lv 返佣等级
 * @property int $agent_type 区域类别：1省级2市级3县级
 * @property float $wdl_num 升级微代理返佣比例
 * @property float $zl_num 升级战略合伙人返佣比例
 * @property float $sy_num 升级事业合伙人返佣比例
 * @property float $artist_num 艺术家入驻返佣比例
 * @property float $gg_num 广告返佣比例
 * @property float $goods_num 销售返佣比例
 * @property float $bzj_num 保证金返佣比例
 * @property int $create_time 创建时间
 * @property int $edit_time 编辑时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereAgentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereArtistNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereBzjNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereCommissionLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereCommissionSetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereGgNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereSyNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereWdlNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAreaAgentCommissionSet whereZlNum($value)
 * @mixin \Eloquent
 */
class ShopncAreaAgentCommissionSet extends BaseModel
{
    protected $table = 'shopnc_area_agent_commission_set';

    public $timestamps = false;

    protected $fillable = [
        'commission_set_name',
        'commission_lv',
        'agent_type',
        'wdl_num',
        'zl_num',
        'sy_num',
        'artist_num',
        'gg_num',
        'goods_num',
        'bzj_num',
        'create_time',
        'edit_time'
    ];

    protected $guarded = [];

        
}