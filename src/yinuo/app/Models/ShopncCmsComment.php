<?php

namespace App\Models;



/**
 * Class ShopncCmsComment
 *
 * @property int $comment_id 评论编号
 * @property int $comment_type 评论类型编号
 * @property int $comment_object_id 推荐商品编号
 * @property string $comment_message 评论内容
 * @property int $comment_member_id 评论人编号
 * @property int $comment_time 评论时间
 * @property string|null $comment_quote 评论引用
 * @property int|null $comment_up 顶数量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentQuote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsComment whereCommentUp($value)
 * @mixin \Eloquent
 */
class ShopncCmsComment extends BaseModel
{
    protected $table = 'shopnc_cms_comment';

    protected $primaryKey = 'comment_id';

	public $timestamps = false;

    protected $fillable = [
        'comment_type',
        'comment_object_id',
        'comment_message',
        'comment_member_id',
        'comment_time',
        'comment_quote',
        'comment_up'
    ];

    protected $guarded = [];

        
}