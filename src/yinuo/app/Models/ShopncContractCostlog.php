<?php

namespace App\Models;



/**
 * Class ShopncContractCostlog
 *
 * @property int $clog_id 自增ID
 * @property int $clog_itemid 保障项目ID
 * @property string $clog_itemname 保障项目名称
 * @property int $clog_storeid 店铺ID
 * @property string $clog_storename 店铺名称
 * @property int|null $clog_adminid 操作管理员ID
 * @property string|null $clog_adminname 操作管理员名称
 * @property float $clog_price 金额
 * @property int $clog_addtime 添加时间
 * @property string $clog_desc 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogAdminid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogAdminname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogItemid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogItemname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractCostlog whereClogStorename($value)
 * @mixin \Eloquent
 */
class ShopncContractCostlog extends BaseModel
{
    protected $table = 'shopnc_contract_costlog';

    protected $primaryKey = 'clog_id';

	public $timestamps = false;

    protected $fillable = [
        'clog_itemid',
        'clog_itemname',
        'clog_storeid',
        'clog_storename',
        'clog_adminid',
        'clog_adminname',
        'clog_price',
        'clog_addtime',
        'clog_desc'
    ];

    protected $guarded = [];

        
}