<?php

namespace App\Models;



/**
 * Class ShopncStoreMsgSetting
 *
 * @property string $smt_code 模板编码
 * @property int $store_id 店铺id
 * @property int $sms_message_switch 站内信接收开关，0关闭，1开启
 * @property int $sms_short_switch 短消息接收开关，0关闭，1开启
 * @property int $sms_mail_switch 邮件接收开关，0关闭，1开启
 * @property string|null $sms_short_number 手机号码
 * @property string|null $sms_mail_number 邮箱号码
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmsMailNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmsMailSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmsMessageSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmsShortNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmsShortSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereSmtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgSetting whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncStoreMsgSetting extends BaseModel
{
    protected $table = 'shopnc_store_msg_setting';

    public $timestamps = false;

    protected $fillable = [
        'smt_code',
        'store_id',
        'sms_message_switch',
        'sms_short_switch',
        'sms_mail_switch',
        'sms_short_number',
        'sms_mail_number'
    ];

    protected $guarded = [];

        
}