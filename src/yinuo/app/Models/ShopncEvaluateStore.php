<?php

namespace App\Models;



/**
 * Class ShopncEvaluateStore
 *
 * @property int $seval_id 评价ID
 * @property int $seval_orderid 订单ID
 * @property string $seval_orderno 订单编号
 * @property int $seval_addtime 评价时间
 * @property int $seval_storeid 店铺编号
 * @property string $seval_storename 店铺名称
 * @property int $seval_memberid 买家编号
 * @property string $seval_membername 买家名称
 * @property int $seval_desccredit 描述相符评分
 * @property int $seval_servicecredit 服务态度评分
 * @property int $seval_deliverycredit 发货速度评分
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalDeliverycredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalDesccredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalOrderid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalOrderno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalServicecredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncEvaluateStore whereSevalStorename($value)
 * @mixin \Eloquent
 */
class ShopncEvaluateStore extends BaseModel
{
    protected $table = 'shopnc_evaluate_store';

    protected $primaryKey = 'seval_id';

	public $timestamps = false;

    protected $fillable = [
        'seval_orderid',
        'seval_orderno',
        'seval_addtime',
        'seval_storeid',
        'seval_storename',
        'seval_memberid',
        'seval_membername',
        'seval_desccredit',
        'seval_servicecredit',
        'seval_deliverycredit'
    ];

    protected $guarded = [];

        
}