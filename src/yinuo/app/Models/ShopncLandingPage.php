<?php

namespace App\Models;



/**
 * Class ShopncLandingPage
 *
 * @property int $id 自增ID
 * @property string $page_name 名称
 * @property string $title 标题
 * @property string|null $banner_image 顶部图片
 * @property string|null $footer_image 底部图片
 * @property int|null $show_goods_num 显示商品数
 * @property int|null $state 发布状态 1未发布 1已发布
 * @property int $modify_time 修改时间
 * @property int $add_time 新增时间
 * @property string $content 描述
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereBannerImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereFooterImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage wherePageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereShowGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncLandingPage whereTitle($value)
 * @mixin \Eloquent
 */
class ShopncLandingPage extends BaseModel
{
    protected $table = 'shopnc_landing_page';

    public $timestamps = false;

    protected $fillable = [
        'page_name',
        'title',
        'banner_image',
        'footer_image',
        'show_goods_num',
        'state',
        'modify_time',
        'add_time',
        'content'
    ];

    protected $guarded = [];

        
}