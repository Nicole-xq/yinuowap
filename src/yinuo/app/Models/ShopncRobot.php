<?php

namespace App\Models;



/**
 * Class ShopncRobot
 *
 * @property int $robot_id 机器人ID
 * @property string|null $robot_name 机器人名称
 * @property int $update_time 更新时间
 * @property string|null $description 描述
 * @property int|null $auction_id 当前绑定的拍品id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobot whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobot whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobot whereRobotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobot whereRobotName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRobot whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncRobot extends BaseModel
{
    protected $table = 'shopnc_robot';

    protected $primaryKey = 'robot_id';

	public $timestamps = false;

    protected $fillable = [
        'robot_name',
        'update_time',
        'description',
        'auction_id'
    ];

    protected $guarded = [];

        
}