<?php

namespace App\Models;



/**
 * Class ShopncOffpayArea
 *
 * @property int $store_id 商家ID
 * @property string|null $area_id 县ID组合
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOffpayArea whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOffpayArea whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncOffpayArea extends BaseModel
{
    protected $table = 'shopnc_offpay_area';

    protected $primaryKey = 'store_id';

	public $timestamps = false;

    protected $fillable = [
        'area_id'
    ];

    protected $guarded = [];

        
}