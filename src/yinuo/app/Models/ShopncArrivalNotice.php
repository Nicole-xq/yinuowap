<?php

namespace App\Models;



/**
 * Class ShopncArrivalNotice
 *
 * @property int $an_id 通知id
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property int $member_id 会员id
 * @property int $store_id 店铺id
 * @property int $an_addtime 添加时间
 * @property string $an_email 邮箱
 * @property string $an_mobile 手机号
 * @property int $an_type 状态 1到货通知，2预售
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereAnAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereAnEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereAnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereAnMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereAnType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArrivalNotice whereStoreId($value)
 * @mixin \Eloquent
 */
class ShopncArrivalNotice extends BaseModel
{
    protected $table = 'shopnc_arrival_notice';

    protected $primaryKey = 'an_id';

	public $timestamps = false;

    protected $fillable = [
        'goods_id',
        'goods_name',
        'member_id',
        'store_id',
        'an_addtime',
        'an_email',
        'an_mobile',
        'an_type'
    ];

    protected $guarded = [];

        
}