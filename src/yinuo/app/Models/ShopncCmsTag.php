<?php

namespace App\Models;



/**
 * Class ShopncCmsTag
 *
 * @property int $tag_id 标签编号
 * @property string $tag_name 标签名称
 * @property int $tag_sort 标签排序
 * @property int $tag_count 标签使用计数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTag whereTagCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTag whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTag whereTagName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsTag whereTagSort($value)
 * @mixin \Eloquent
 */
class ShopncCmsTag extends BaseModel
{
    protected $table = 'shopnc_cms_tag';

    protected $primaryKey = 'tag_id';

	public $timestamps = false;

    protected $fillable = [
        'tag_name',
        'tag_sort',
        'tag_count'
    ];

    protected $guarded = [];

        
}