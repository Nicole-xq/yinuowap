<?php

namespace App\Models;



/**
 * Class ShopncOrderGood
 *
 * @property int $rec_id 订单商品表索引id
 * @property int $order_id 订单id
 * @property int $goods_id 商品id
 * @property string $goods_name 商品名称
 * @property float $goods_price 商品价格
 * @property int $goods_num 商品数量
 * @property string|null $goods_image 商品图片
 * @property float $goods_pay_price 商品实际成交价
 * @property int $store_id 店铺ID
 * @property int $buyer_id 买家ID
 * @property int $goods_type 1默认2团购商品3限时折扣商品4组合套装5赠品8加价购活动商品9加价购换购商品10拼团
 * @property int $promotions_id 促销活动ID（团购ID/限时折扣ID/优惠套装ID）与goods_type搭配使用
 * @property int $commis_rate 佣金比例
 * @property int $gc_id 商品最底级分类ID
 * @property string|null $goods_spec 商品规格
 * @property string|null $goods_contractid 商品开启的消费者保障服务id
 * @property int $goods_commonid 商品公共表ID
 * @property int $add_time 订单添加时间
 * @property int $is_dis 是否分销商品
 * @property int $dis_commis_rate 分销佣金比例
 * @property int $dis_member_id 分销会员ID
 * @property int $is_special 是否特殊商品(下单后不可退款退货) 0 不是 1 是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereBuyerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereDisCommisRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereDisMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsContractid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsPayPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereGoodsType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereIsDis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereIsSpecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood wherePromotionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereRecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereStoreId($value)
 * @mixin \Eloquent
 * @property int|null $artwork_category_id 艺术家作品分类
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncOrderGood whereArtworkCategoryId($value)
 */
class ShopncOrderGood extends BaseModel
{
    protected $table = 'shopnc_order_goods';

    protected $primaryKey = 'rec_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'goods_id',
        'goods_name',
        'goods_price',
        'goods_num',
        'goods_image',
        'goods_pay_price',
        'store_id',
        'buyer_id',
        'goods_type',
        'promotions_id',
        'commis_rate',
        'gc_id',
        'goods_spec',
        'goods_contractid',
        'goods_commonid',
        'add_time',
        'is_dis',
        'dis_commis_rate',
        'dis_member_id',
        'is_special'
    ];

    protected $guarded = [];

        
}