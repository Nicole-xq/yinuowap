<?php

namespace App\Models;



/**
 * Class ShopncInformSubject
 *
 * @property int $inform_subject_id 举报主题id
 * @property string $inform_subject_content 举报主题内容
 * @property int $inform_subject_type_id 举报类型id
 * @property string $inform_subject_type_name 举报类型名称
 * @property int $inform_subject_state 举报主题状态(1可用/2失效)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubject whereInformSubjectContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubject whereInformSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubject whereInformSubjectState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubject whereInformSubjectTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncInformSubject whereInformSubjectTypeName($value)
 * @mixin \Eloquent
 */
class ShopncInformSubject extends BaseModel
{
    protected $table = 'shopnc_inform_subject';

    protected $primaryKey = 'inform_subject_id';

	public $timestamps = false;

    protected $fillable = [
        'inform_subject_content',
        'inform_subject_type_id',
        'inform_subject_type_name',
        'inform_subject_state'
    ];

    protected $guarded = [];

        
}