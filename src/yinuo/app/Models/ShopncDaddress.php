<?php

namespace App\Models;



/**
 * Class ShopncDaddress
 *
 * @property int $address_id 地址ID
 * @property int $store_id 店铺ID
 * @property string $seller_name 联系人
 * @property int $area_id 地区ID
 * @property int|null $city_id 市级ID
 * @property string|null $area_info 省市县
 * @property string $address 地址
 * @property string|null $telphone 电话
 * @property string|null $company 公司
 * @property string $is_default 是否默认1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDaddress whereTelphone($value)
 * @mixin \Eloquent
 */
class ShopncDaddress extends BaseModel
{
    protected $table = 'shopnc_daddress';

    protected $primaryKey = 'address_id';

	public $timestamps = false;

    protected $fillable = [
        'store_id',
        'seller_name',
        'area_id',
        'city_id',
        'area_info',
        'address',
        'telphone',
        'company',
        'is_default'
    ];

    protected $guarded = [];

        
}