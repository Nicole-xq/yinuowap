<?php

namespace App\Models;


/**
 * Class ShopncCloseAccount
 *
 * @property int $id 商家结算账户
 * @property int $store_id 用户id
 * @property float $wait_amount 待金额
 * @property float $clsd_amount 已结算总金额
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereClsdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCloseAccount whereWaitAmount($value)
 * @mixin \Eloquent
 */
class ShopncCloseAccount extends BaseModel
{
    protected $table = 'shopnc_close_account';

    public $timestamps = true;

    protected $fillable = [
        'store_id',
        'wait_amount',
        'clsd_amount'
    ];

    protected $guarded = [];

        
}