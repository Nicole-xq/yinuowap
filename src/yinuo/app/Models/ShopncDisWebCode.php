<?php

namespace App\Models;



/**
 * Class ShopncDisWebCode
 *
 * @property int $code_id 内容ID
 * @property int $web_id 模块ID
 * @property string $code_type 数据类型:array,html,json
 * @property string $var_name 变量名称
 * @property string|null $code_info 内容数据
 * @property string|null $show_name 页面名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereCodeInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereCodeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereShowName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereVarName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncDisWebCode whereWebId($value)
 * @mixin \Eloquent
 */
class ShopncDisWebCode extends BaseModel
{
    protected $table = 'shopnc_dis_web_code';

    protected $primaryKey = 'code_id';

	public $timestamps = false;

    protected $fillable = [
        'web_id',
        'code_type',
        'var_name',
        'code_info',
        'show_name'
    ];

    protected $guarded = [];

        
}