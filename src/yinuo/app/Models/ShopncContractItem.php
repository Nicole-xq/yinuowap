<?php

namespace App\Models;



/**
 * Class ShopncContractItem
 *
 * @property int $cti_id 自增ID
 * @property string $cti_name 保障项目名称
 * @property string $cti_describe 保障项目描述
 * @property float $cti_cost 保证金
 * @property string $cti_icon 图标
 * @property string|null $cti_descurl 内容介绍文章地址
 * @property int $cti_state 状态 0关闭 1开启
 * @property int|null $cti_sort 排序
 * @property int|null $is_auction 是否是拍品的服务保障
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiDescribe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiDescurl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereCtiState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncContractItem whereIsAuction($value)
 * @mixin \Eloquent
 */
class ShopncContractItem extends BaseModel
{
    protected $table = 'shopnc_contract_item';

    protected $primaryKey = 'cti_id';

	public $timestamps = false;

    protected $fillable = [
        'cti_name',
        'cti_describe',
        'cti_cost',
        'cti_icon',
        'cti_descurl',
        'cti_state',
        'cti_sort',
        'is_auction'
    ];

    protected $guarded = [];

        
}