<?php

namespace App\Models;



/**
 * Class ShopncMemberRateConfig
 *
 * @property int $id
 * @property string $title 标题
 * @property string $type 类型
 * @property float|null $bid_rate 竞价返佣
 * @property float|null $deal_rate 成交返佣
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberRateConfig whereBidRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberRateConfig whereDealRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberRateConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberRateConfig whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberRateConfig whereType($value)
 * @mixin \Eloquent
 */
class ShopncMemberRateConfig extends BaseModel
{
    protected $table = 'shopnc_member_rate_config';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'type',
        'bid_rate',
        'deal_rate'
    ];

    protected $guarded = [];

        
}