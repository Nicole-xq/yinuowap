<?php

namespace App\Models;



/**
 * Class ShopncTopMemberLog
 *
 * @property int $id 自增ID
 * @property int $member_id 用户ID
 * @property string $member_name 用户名
 * @property int|null $old_top_member_id 原上级用户ID
 * @property string|null $old_top_member_name 原上级用户名
 * @property int $change_top_member_id 更改的上级用户ID
 * @property string $change_top_member_name 更改的上级用户名
 * @property int $admin_id 操作的管理员ID
 * @property string $admin_name 操作的管理员名称
 * @property string|null $msg 变更备注信息
 * @property int $add_time 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereChangeTopMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereChangeTopMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereOldTopMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTopMemberLog whereOldTopMemberName($value)
 * @mixin \Eloquent
 */
class ShopncTopMemberLog extends BaseModel
{
    protected $table = 'shopnc_top_member_log';

    public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'old_top_member_id',
        'old_top_member_name',
        'change_top_member_id',
        'change_top_member_name',
        'admin_id',
        'admin_name',
        'msg',
        'add_time'
    ];

    protected $guarded = [];

        
}