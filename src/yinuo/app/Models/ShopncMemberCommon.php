<?php

namespace App\Models;



/**
 * Class ShopncMemberCommon
 *
 * @property int $member_id 会员ID
 * @property string|null $auth_code 短信/邮件验证码
 * @property int $send_acode_time 短信/邮件验证码发送时间
 * @property int|null $send_mb_time 发送短信验证码时间
 * @property int|null $send_email_time 发送邮件验证码时间
 * @property int $send_mb_times 发送手机验证码次数
 * @property int $send_acode_times 发送验证码次数
 * @property int $auth_code_check_times 验证码验证次数[目前wap使用]
 * @property int $auth_modify_pwd_time 修改密码授权时间[目前wap使用]
 * @property int $auth_code_check_date 提交错误验证码时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereAuthCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereAuthCodeCheckDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereAuthCodeCheckTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereAuthModifyPwdTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereSendAcodeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereSendAcodeTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereSendEmailTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereSendMbTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberCommon whereSendMbTimes($value)
 * @mixin \Eloquent
 */
class ShopncMemberCommon extends BaseModel
{
    protected $table = 'shopnc_member_common';

    protected $primaryKey = 'member_id';

	public $timestamps = false;

    protected $fillable = [
        'auth_code',
        'send_acode_time',
        'send_mb_time',
        'send_email_time',
        'send_mb_times',
        'send_acode_times',
        'auth_code_check_times',
        'auth_modify_pwd_time',
        'auth_code_check_date'
    ];

    protected $guarded = [];

        
}