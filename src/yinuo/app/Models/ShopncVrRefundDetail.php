<?php

namespace App\Models;



/**
 * Class ShopncVrRefundDetail
 *
 * @property int $refund_id 记录ID
 * @property int $order_id 订单ID
 * @property string $batch_no 批次号
 * @property float|null $refund_amount 退款金额
 * @property float|null $pay_amount 在线退款金额
 * @property float|null $pd_amount 预存款金额
 * @property float|null $rcb_amount 充值卡金额
 * @property string $refund_code 退款支付代码
 * @property int|null $refund_state 退款状态:1为处理中,2为已完成,默认为1
 * @property int $add_time 添加时间
 * @property int|null $pay_time 在线退款完成时间,默认为0
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereBatchNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail wherePayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail wherePdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereRcbAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereRefundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereRefundCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereRefundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVrRefundDetail whereRefundState($value)
 * @mixin \Eloquent
 */
class ShopncVrRefundDetail extends BaseModel
{
    protected $table = 'shopnc_vr_refund_detail';

    protected $primaryKey = 'refund_id';

	public $timestamps = false;

    protected $fillable = [
        'order_id',
        'batch_no',
        'refund_amount',
        'pay_amount',
        'pd_amount',
        'rcb_amount',
        'refund_code',
        'refund_state',
        'add_time',
        'pay_time'
    ];

    protected $guarded = [];

        
}