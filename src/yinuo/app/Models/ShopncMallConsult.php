<?php

namespace App\Models;



/**
 * Class ShopncMallConsult
 *
 * @property int $mc_id 平台客服咨询id
 * @property int $mct_id 咨询类型id
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property string $mc_content 咨询内容
 * @property int $mc_addtime 咨询时间
 * @property int $is_reply 是否回复，1是，0否，默认0
 * @property string|null $mc_reply 回复内容
 * @property int|null $mc_reply_time 回复时间
 * @property int|null $admin_id 管理员id
 * @property string|null $admin_name 管理员名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereIsReply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMcAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMcContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMcReply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMcReplyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMctId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMallConsult whereMemberName($value)
 * @mixin \Eloquent
 */
class ShopncMallConsult extends BaseModel
{
    protected $table = 'shopnc_mall_consult';

    protected $primaryKey = 'mc_id';

	public $timestamps = false;

    protected $fillable = [
        'mct_id',
        'member_id',
        'member_name',
        'mc_content',
        'mc_addtime',
        'is_reply',
        'mc_reply',
        'mc_reply_time',
        'admin_id',
        'admin_name'
    ];

    protected $guarded = [];

        
}