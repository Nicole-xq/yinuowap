<?php

namespace App\Models;



/**
 * Class ShopncAttributeValue
 *
 * @property int $attr_value_id 属性值id
 * @property string|null $attr_value_name 属性值名称
 * @property int $attr_id 所属属性id
 * @property int $type_id 类型id
 * @property int $attr_value_sort 属性值排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttributeValue whereAttrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttributeValue whereAttrValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttributeValue whereAttrValueName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttributeValue whereAttrValueSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAttributeValue whereTypeId($value)
 * @mixin \Eloquent
 */
class ShopncAttributeValue extends BaseModel
{
    protected $table = 'shopnc_attribute_value';

    protected $primaryKey = 'attr_value_id';

	public $timestamps = false;

    protected $fillable = [
        'attr_value_name',
        'attr_id',
        'type_id',
        'attr_value_sort'
    ];

    protected $guarded = [];

        
}