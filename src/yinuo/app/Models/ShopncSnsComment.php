<?php

namespace App\Models;



/**
 * Class ShopncSnsComment
 *
 * @property int $comment_id 自增ID
 * @property int $comment_memberid 会员ID
 * @property string $comment_membername 会员名称
 * @property string|null $comment_memberavatar 会员头像
 * @property int $comment_originalid 原帖ID
 * @property int $comment_originaltype 原帖类型 0表示动态信息 1表示分享商品 默认为0
 * @property string $comment_content 评论内容
 * @property int $comment_addtime 添加时间
 * @property string $comment_ip 来源IP
 * @property int $comment_state 状态 0正常 1屏蔽
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentMemberavatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentMemberid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentMembername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentOriginalid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentOriginaltype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsComment whereCommentState($value)
 * @mixin \Eloquent
 */
class ShopncSnsComment extends BaseModel
{
    protected $table = 'shopnc_sns_comment';

    protected $primaryKey = 'comment_id';

	public $timestamps = false;

    protected $fillable = [
        'comment_memberid',
        'comment_membername',
        'comment_memberavatar',
        'comment_originalid',
        'comment_originaltype',
        'comment_content',
        'comment_addtime',
        'comment_ip',
        'comment_state'
    ];

    protected $guarded = [];

        
}