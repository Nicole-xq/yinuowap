<?php

namespace App\Models;



/**
 * Class ShopncHelpType
 *
 * @property int $type_id 类型ID
 * @property string $type_name 类型名称
 * @property int|null $type_sort 排序
 * @property string|null $help_code 调用编号(auto的可删除)
 * @property int|null $help_show 是否显示,0为否,1为是,默认为1
 * @property int|null $page_show 页面类型:1为店铺,2为会员,默认为1
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType whereHelpCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType whereHelpShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType wherePageShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType whereTypeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncHelpType whereTypeSort($value)
 * @mixin \Eloquent
 */
class ShopncHelpType extends BaseModel
{
    protected $table = 'shopnc_help_type';

    protected $primaryKey = 'type_id';

	public $timestamps = false;

    protected $fillable = [
        'type_name',
        'type_sort',
        'help_code',
        'help_show',
        'page_show'
    ];

    protected $guarded = [];

        
}