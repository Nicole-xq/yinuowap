<?php

namespace App\Models;



/**
 * Class ShopncCircle
 *
 * @property int $circle_id 圈子id
 * @property string $circle_name 圈子名称
 * @property string|null $circle_desc 圈子描述
 * @property int $circle_masterid 圈主id
 * @property string $circle_mastername 圈主名称
 * @property string|null $circle_img 圈子图片
 * @property int $class_id 圈子分类
 * @property int $circle_mcount 圈子成员数
 * @property int $circle_thcount 圈子主题数
 * @property int $circle_gcount 圈子商品数
 * @property string|null $circle_pursuereason 圈子申请理由
 * @property string|null $circle_notice 圈子公告
 * @property int $circle_status 圈子状态，0关闭，1开启，2审核中，3审核失败
 * @property string|null $circle_statusinfo 关闭或审核失败原因
 * @property int $circle_joinaudit 加入圈子时候需要审核，0不需要，1需要
 * @property string $circle_addtime 圈子创建时间
 * @property string|null $circle_noticetime 圈子公告更新时间
 * @property int $is_recommend 是否推荐 0未推荐，1已推荐
 * @property int $is_hot 是否为热门圈子 1是 0否
 * @property string|null $circle_tag 圈子标签
 * @property int $new_verifycount 等待审核成员数
 * @property int $new_informcount 等待处理举报数
 * @property int $mapply_open 申请管理是否开启 0关闭，1开启
 * @property int $mapply_ml 成员级别
 * @property int $new_mapplycount 管理申请数量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleGcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleJoinaudit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleMasterid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleMastername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleMcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleNoticetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCirclePursuereason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleStatusinfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereCircleThcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereMapplyMl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereMapplyOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereNewInformcount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereNewMapplycount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircle whereNewVerifycount($value)
 * @mixin \Eloquent
 */
class ShopncCircle extends BaseModel
{
    protected $table = 'shopnc_circle';

    protected $primaryKey = 'circle_id';

	public $timestamps = false;

    protected $fillable = [
        'circle_name',
        'circle_desc',
        'circle_masterid',
        'circle_mastername',
        'circle_img',
        'class_id',
        'circle_mcount',
        'circle_thcount',
        'circle_gcount',
        'circle_pursuereason',
        'circle_notice',
        'circle_status',
        'circle_statusinfo',
        'circle_joinaudit',
        'circle_addtime',
        'circle_noticetime',
        'is_recommend',
        'is_hot',
        'circle_tag',
        'new_verifycount',
        'new_informcount',
        'mapply_open',
        'mapply_ml',
        'new_mapplycount'
    ];

    protected $guarded = [];

        
}