<?php

namespace App\Models;



/**
 * Class ShopncAuctionConfig
 *
 * @property int $id
 * @property int $duration_time 拍卖持续时间
 * @property int $add_time 拍卖最后添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionConfig whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionConfig whereDurationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionConfig whereId($value)
 * @mixin \Eloquent
 */
class ShopncAuctionConfig extends BaseModel
{
    protected $table = 'shopnc_auction_config';

    public $timestamps = false;

    protected $fillable = [
        'duration_time',
        'add_time'
    ];

    protected $guarded = [];

        
}