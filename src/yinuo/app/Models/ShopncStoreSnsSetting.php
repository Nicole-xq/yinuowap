<?php

namespace App\Models;



/**
 * Class ShopncStoreSnsSetting
 *
 * @property int $sauto_storeid 店铺id
 * @property int $sauto_new 新品,0为关闭/1为开启
 * @property string|null $sauto_newtitle 新品内容
 * @property int $sauto_coupon 优惠券,0为关闭/1为开启
 * @property string|null $sauto_coupontitle 优惠券内容
 * @property int $sauto_xianshi 限时折扣,0为关闭/1为开启
 * @property string|null $sauto_xianshititle 限时折扣内容
 * @property int $sauto_mansong 满即送,0为关闭/1为开启
 * @property string|null $sauto_mansongtitle 满即送内容
 * @property int $sauto_bundling 组合销售,0为关闭/1为开启
 * @property string|null $sauto_bundlingtitle 组合销售内容
 * @property int $sauto_groupbuy 团购,0为关闭/1为开启
 * @property string|null $sauto_groupbuytitle 团购内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoBundling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoBundlingtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoCoupon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoCoupontitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoGroupbuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoGroupbuytitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoMansong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoMansongtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoNewtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoStoreid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoXianshi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreSnsSetting whereSautoXianshititle($value)
 * @mixin \Eloquent
 */
class ShopncStoreSnsSetting extends BaseModel
{
    protected $table = 'shopnc_store_sns_setting';

    protected $primaryKey = 'sauto_storeid';

	public $timestamps = false;

    protected $fillable = [
        'sauto_new',
        'sauto_newtitle',
        'sauto_coupon',
        'sauto_coupontitle',
        'sauto_xianshi',
        'sauto_xianshititle',
        'sauto_mansong',
        'sauto_mansongtitle',
        'sauto_bundling',
        'sauto_bundlingtitle',
        'sauto_groupbuy',
        'sauto_groupbuytitle'
    ];

    protected $guarded = [];

        
}