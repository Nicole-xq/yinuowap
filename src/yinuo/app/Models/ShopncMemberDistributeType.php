<?php

namespace App\Models;



/**
 * Class ShopncMemberDistributeType
 *
 * @property int $id 主键id
 * @property int $type_id 分销类型id
 * @property string $name 名字
 * @property string|null $logo_img logo
 * @property float|null $price 特定专区升级金额
 * @property float $wdl_num 微代理分销百分比
 * @property float $zl_num 战略合伙人百分比
 * @property float $sy_num 事业合伙人百分比
 * @property float $artist_num 艺术家百分比
 * @property float $goods_num 购买艺术品百分比
 * @property float $bzj_num 拍卖保证金百分比
 * @property int|null $create_time 创建时间
 * @property int|null $last_modify_time 最后修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereArtistNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereBzjNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereGoodsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereLastModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereLogoImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereSyNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereWdlNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMemberDistributeType whereZlNum($value)
 * @mixin \Eloquent
 */
class ShopncMemberDistributeType extends BaseModel
{
    protected $table = 'shopnc_member_distribute_type';

    public $timestamps = false;

    protected $fillable = [
        'type_id',
        'name',
        'logo_img',
        'price',
        'wdl_num',
        'zl_num',
        'sy_num',
        'artist_num',
        'goods_num',
        'bzj_num',
        'create_time',
        'last_modify_time'
    ];

    protected $guarded = [];

        
}