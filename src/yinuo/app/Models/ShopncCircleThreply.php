<?php

namespace App\Models;



/**
 * Class ShopncCircleThreply
 *
 * @property int $theme_id 主题id
 * @property int $reply_id 评论id
 * @property int $circle_id 圈子id
 * @property int $member_id 会员id
 * @property string $member_name 会员名称
 * @property string $reply_content 评论内容
 * @property string $reply_addtime 发表时间
 * @property int|null $reply_replyid 回复楼层id
 * @property string|null $reply_replyname 回复楼层会员名称
 * @property int $is_closed 屏蔽 1是 0否
 * @property int $reply_exp 获得经验
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereIsClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyReplyid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereReplyReplyname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleThreply whereThemeId($value)
 * @mixin \Eloquent
 */
class ShopncCircleThreply extends BaseModel
{
    protected $table = 'shopnc_circle_threply';

    public $timestamps = false;

    protected $fillable = [
        'theme_id',
        'reply_id',
        'circle_id',
        'member_id',
        'member_name',
        'reply_content',
        'reply_addtime',
        'reply_replyid',
        'reply_replyname',
        'is_closed',
        'reply_exp'
    ];

    protected $guarded = [];

        
}