<?php

namespace App\Models;



/**
 * Class ShopncArtistVendueCopy20171122
 *
 * @property int $artist_vendue_id 艺术家拍卖ID
 * @property string $artist_name 艺术家名称
 * @property string $artist_job_title 艺术家职称
 * @property string $artist_resume 艺术家简历
 * @property string|null $artist_represent 艺术家年代表
 * @property string|null $artist_awards 艺术家获奖情况
 * @property string|null $artist_works 艺术家获奖作品
 * @property int|null $artist_classify 艺术家分类
 * @property int|null $artist_grade 艺术家等级
 * @property int|null $is_rec 是否推荐
 * @property int|null $artist_sort 排序 值越小越靠前
 * @property string|null $artist_image 艺术家主图
 * @property int $store_vendue_id 商家拍卖ID
 * @property int|null $member_id 卖家会员编号
 * @property int|null $artist_click 点击量
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistAwards($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistClassify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistRepresent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistResume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistVendueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereArtistWorks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereIsRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncArtistVendueCopy20171122 whereStoreVendueId($value)
 * @mixin \Eloquent
 */
class ShopncArtistVendueCopy20171122 extends BaseModel
{
    protected $table = 'shopnc_artist_vendue_copy_2017_11_22';

    protected $primaryKey = 'artist_vendue_id';

	public $timestamps = false;

    protected $fillable = [
        'artist_name',
        'artist_job_title',
        'artist_resume',
        'artist_represent',
        'artist_awards',
        'artist_works',
        'artist_classify',
        'artist_grade',
        'is_rec',
        'artist_sort',
        'artist_image',
        'store_vendue_id',
        'member_id',
        'artist_click'
    ];

    protected $guarded = [];

        
}