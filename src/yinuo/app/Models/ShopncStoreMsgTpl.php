<?php

namespace App\Models;



/**
 * Class ShopncStoreMsgTpl
 *
 * @property int $smt_code 模板编码
 * @property string $smt_name 模板名称
 * @property int $smt_message_switch 站内信默认开关，0关，1开
 * @property string $smt_message_content 站内信内容
 * @property int $smt_message_forced 站内信强制接收，0否，1是
 * @property int $smt_short_switch 短信默认开关，0关，1开
 * @property string $smt_short_content 短信内容
 * @property int $smt_short_forced 短信强制接收，0否，1是
 * @property int $smt_mail_switch 邮件默认开，0关，1开
 * @property string $smt_mail_subject 邮件标题
 * @property string $smt_mail_content 邮件内容
 * @property int $smt_mail_forced 邮件强制接收，0否，1是
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMailContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMailForced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMailSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMailSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMessageContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMessageForced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtMessageSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtShortForced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreMsgTpl whereSmtShortSwitch($value)
 * @mixin \Eloquent
 */
class ShopncStoreMsgTpl extends BaseModel
{
    protected $table = 'shopnc_store_msg_tpl';

    protected $primaryKey = 'smt_code';

	public $timestamps = false;

    protected $fillable = [
        'smt_name',
        'smt_message_switch',
        'smt_message_content',
        'smt_message_forced',
        'smt_short_switch',
        'smt_short_content',
        'smt_short_forced',
        'smt_mail_switch',
        'smt_mail_subject',
        'smt_mail_content',
        'smt_mail_forced'
    ];

    protected $guarded = [];

        
}