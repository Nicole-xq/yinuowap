<?php

namespace App\Models;



/**
 * Class ShopncAddress
 *
 * @property int $address_id 地址ID
 * @property int $member_id 会员ID
 * @property string $true_name 会员姓名
 * @property int $area_id 地区ID
 * @property int|null $city_id 市级ID
 * @property string $area_info 地区内容
 * @property string $address 地址
 * @property string|null $tel_phone 座机电话
 * @property string|null $mob_phone 手机电话
 * @property string $is_default 1默认收货地址
 * @property int|null $dlyp_id 自提点ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereAreaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereDlypId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereMobPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereTelPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAddress whereTrueName($value)
 * @mixin \Eloquent
 */
class ShopncAddress extends BaseModel
{
    protected $table = 'shopnc_address';

    protected $primaryKey = 'address_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'true_name',
        'area_id',
        'city_id',
        'area_info',
        'address',
        'tel_phone',
        'mob_phone',
        'is_default',
        'dlyp_id'
    ];

    protected $guarded = [];

        
}