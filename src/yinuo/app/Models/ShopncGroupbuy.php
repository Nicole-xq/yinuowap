<?php

namespace App\Models;



/**
 * Class ShopncGroupbuy
 *
 * @property int $groupbuy_id 团购ID
 * @property string $groupbuy_name 活动名称
 * @property int $start_time 开始时间
 * @property int $end_time 结束时间
 * @property int $goods_id 商品ID
 * @property int $goods_commonid 商品公共表ID
 * @property string $goods_name 商品名称
 * @property int $store_id 店铺ID
 * @property string $store_name 店铺名称
 * @property float $goods_price 商品原价
 * @property float $groupbuy_price 团购价格
 * @property float $groupbuy_rebate 折扣
 * @property int $virtual_quantity 虚拟购买数量
 * @property int $upper_limit 购买上限
 * @property int $buyer_count 已购买人数
 * @property int $buy_quantity 购买数量
 * @property string|null $groupbuy_intro 本团介绍
 * @property int $state 团购状态 10-审核中 20-正常 30-审核失败 31-管理员关闭 32-已结束
 * @property int $recommended 是否推荐 0.未推荐 1.已推荐
 * @property int $views 查看次数
 * @property int $class_id 团购类别编号
 * @property int $s_class_id 团购2级分类id
 * @property string $groupbuy_image 团购图片
 * @property string|null $groupbuy_image1 团购图片1
 * @property string|null $remark 备注
 * @property int $is_vr 是否虚拟团购 1是0否
 * @property int|null $vr_city_id 虚拟团购城市id
 * @property int|null $vr_area_id 虚拟团购区域id
 * @property int|null $vr_mall_id 虚拟团购商区id
 * @property int|null $vr_class_id 虚拟团购大分类id
 * @property int|null $vr_s_class_id 虚拟团购小分类id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereBuyQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereBuyerCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGoodsCommonid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGoodsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGoodsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGoodsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyImage1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereGroupbuyRebate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereIsVr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereSClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereUpperLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVirtualQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVrAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVrCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVrClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVrMallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGroupbuy whereVrSClassId($value)
 * @mixin \Eloquent
 */
class ShopncGroupbuy extends BaseModel
{
    protected $table = 'shopnc_groupbuy';

    protected $primaryKey = 'groupbuy_id';

	public $timestamps = false;

    protected $fillable = [
        'groupbuy_name',
        'start_time',
        'end_time',
        'goods_id',
        'goods_commonid',
        'goods_name',
        'store_id',
        'store_name',
        'goods_price',
        'groupbuy_price',
        'groupbuy_rebate',
        'virtual_quantity',
        'upper_limit',
        'buyer_count',
        'buy_quantity',
        'groupbuy_intro',
        'state',
        'recommended',
        'views',
        'class_id',
        's_class_id',
        'groupbuy_image',
        'groupbuy_image1',
        'remark',
        'is_vr',
        'vr_city_id',
        'vr_area_id',
        'vr_mall_id',
        'vr_class_id',
        'vr_s_class_id'
    ];

    protected $guarded = [];

        
}