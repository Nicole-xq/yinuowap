<?php

namespace App\Models;



/**
 * Class ShopncCircleMl
 *
 * @property int $circle_id 圈子id
 * @property int|null $mlref_id 参考头衔id 0为默认 null为自定义
 * @property string $ml_1 1级头衔名称
 * @property string $ml_2 2级头衔名称
 * @property string $ml_3 3级头衔名称
 * @property string $ml_4 4级头衔名称
 * @property string $ml_5 5级头衔名称
 * @property string $ml_6 6级头衔名称
 * @property string $ml_7 7级头衔名称
 * @property string $ml_8 8级头衔名称
 * @property string $ml_9 9级头衔名称
 * @property string $ml_10 10级头衔名称
 * @property string $ml_11 11级头衔名称
 * @property string $ml_12 12级头衔名称
 * @property string $ml_13 13级头衔名称
 * @property string $ml_14 14级头衔名称
 * @property string $ml_15 15级头衔名称
 * @property string $ml_16 16级头衔名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereCircleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl11($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl13($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl14($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl15($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl16($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMl9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCircleMl whereMlrefId($value)
 * @mixin \Eloquent
 */
class ShopncCircleMl extends BaseModel
{
    protected $table = 'shopnc_circle_ml';

    protected $primaryKey = 'circle_id';

	public $timestamps = false;

    protected $fillable = [
        'mlref_id',
        'ml_1',
        'ml_2',
        'ml_3',
        'ml_4',
        'ml_5',
        'ml_6',
        'ml_7',
        'ml_8',
        'ml_9',
        'ml_10',
        'ml_11',
        'ml_12',
        'ml_13',
        'ml_14',
        'ml_15',
        'ml_16'
    ];

    protected $guarded = [];

        
}