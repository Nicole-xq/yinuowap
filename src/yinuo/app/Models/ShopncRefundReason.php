<?php

namespace App\Models;



/**
 * Class ShopncRefundReason
 *
 * @property int $reason_id 原因ID
 * @property string $reason_info 原因内容
 * @property int|null $sort 排序
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReason whereReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReason whereReasonInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReason whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncRefundReason whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncRefundReason extends BaseModel
{
    protected $table = 'shopnc_refund_reason';

    protected $primaryKey = 'reason_id';

	public $timestamps = false;

    protected $fillable = [
        'reason_info',
        'sort',
        'update_time'
    ];

    protected $guarded = [];

        
}