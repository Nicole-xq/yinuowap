<?php

namespace App\Models;



/**
 * Class ShopncAuctionMemberRelation
 *
 * @property int $relation_id
 * @property int $auction_id
 * @property int $member_id
 * @property int|null $is_remind 是否设置手机提醒 1：设置
 * @property int|null $is_bond 是否提交保证金 0:未创建保证金订单,1：已支付,2:待支付
 * @property float|null $bond_num 保证金金额
 * @property int|null $is_agent 是否设置代理价 ，1：设置
 * @property float|null $agent_num 代理价
 * @property int|null $is_offer 是否出价 1：出价
 * @property float|null $offer_num 出价价格
 * @property int|null $remind_time 设置提醒的时间
 * @property string|null $member_mobile 会员设置提醒的手机号
 * @property int|null $is_remind_app 是否设置app提醒，1：设置
 * @property string|null $member_name 会员名称
 * @property int|null $is_anonymous 会员是否匿名出价，1：匿名，0：不匿名
 * @property string|null $auction_name 拍平名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereAgentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereAuctionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereBondNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsBond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsOffer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsRemind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereIsRemindApp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereOfferNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncAuctionMemberRelation whereRemindTime($value)
 * @mixin \Eloquent
 */
class ShopncAuctionMemberRelation extends BaseModel
{
    protected $table = 'shopnc_auction_member_relation';

    protected $primaryKey = 'relation_id';

	public $timestamps = false;

    protected $fillable = [
        'auction_id',
        'member_id',
        'is_remind',
        'is_bond',
        'bond_num',
        'is_agent',
        'agent_num',
        'is_offer',
        'offer_num',
        'remind_time',
        'member_mobile',
        'is_remind_app',
        'member_name',
        'is_anonymous',
        'auction_name'
    ];

    protected $guarded = [];

        
}