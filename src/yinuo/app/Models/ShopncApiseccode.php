<?php

namespace App\Models;



/**
 * Class ShopncApiseccode
 *
 * @property int $sec_id 自增ID
 * @property string $sec_key 验证码标识
 * @property string $sec_val 验证码值
 * @property int $sec_addtime 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncApiseccode whereSecAddtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncApiseccode whereSecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncApiseccode whereSecKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncApiseccode whereSecVal($value)
 * @mixin \Eloquent
 */
class ShopncApiseccode extends BaseModel
{
    protected $table = 'shopnc_apiseccode';

    protected $primaryKey = 'sec_id';

	public $timestamps = false;

    protected $fillable = [
        'sec_key',
        'sec_val',
        'sec_addtime'
    ];

    protected $guarded = [];

        
}