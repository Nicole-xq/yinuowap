<?php

namespace App\Models;



/**
 * Class ShopncMbUserToken
 *
 * @property int $token_id 令牌编号
 * @property int $member_id 用户编号
 * @property string $member_name 用户名
 * @property int|null $user_id 用户ID
 * @property string|null $user_name 用户名
 * @property string $token 登录令牌
 * @property int $login_time 登录时间
 * @property string $client_type 客户端类型 android wap
 * @property string|null $openid 微信支付jsapi的openid缓存
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereClientType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereTokenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncMbUserToken whereUserName($value)
 * @mixin \Eloquent
 */
class ShopncMbUserToken extends BaseModel
{
    protected $table = 'shopnc_mb_user_token';

    protected $primaryKey = 'token_id';

	public $timestamps = false;

    protected $fillable = [
        'member_id',
        'member_name',
        'user_id',
        'user_name',
        'token',
        'login_time',
        'client_type',
        'openid'
    ];

    protected $guarded = [];

        
}