<?php

namespace App\Models;



/**
 * Class ShopncActivity
 *
 * @property int $activity_id id
 * @property string $activity_title 标题
 * @property string|null $activity_type 活动类型 1:商品 2:团购
 * @property string $activity_banner 活动横幅大图片
 * @property string $activity_style 活动页面模板样式标识码
 * @property string|null $activity_desc 描述
 * @property int $activity_start_date 开始时间
 * @property int $activity_end_date 结束时间
 * @property int $activity_sort 排序
 * @property int $activity_state 活动状态 0为关闭 1为开启
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivitySort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncActivity whereActivityType($value)
 * @mixin \Eloquent
 */
class ShopncActivity extends BaseModel
{
    protected $table = 'shopnc_activity';

    protected $primaryKey = 'activity_id';

	public $timestamps = false;

    protected $fillable = [
        'activity_title',
        'activity_type',
        'activity_banner',
        'activity_style',
        'activity_desc',
        'activity_start_date',
        'activity_end_date',
        'activity_sort',
        'activity_state'
    ];

    protected $guarded = [];

        
}