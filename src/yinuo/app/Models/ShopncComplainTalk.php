<?php

namespace App\Models;



/**
 * Class ShopncComplainTalk
 *
 * @property int $talk_id 投诉对话id
 * @property int $complain_id 投诉id
 * @property int $talk_member_id 发言人id
 * @property string $talk_member_name 发言人名称
 * @property string $talk_member_type 发言人类型(1-投诉人/2-被投诉人/3-平台)
 * @property string $talk_content 发言内容
 * @property int $talk_state 发言状态(1-显示/2-不显示)
 * @property int $talk_admin 对话管理员，屏蔽对话人的id
 * @property int $talk_datetime 对话发表时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereComplainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkMemberType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncComplainTalk whereTalkState($value)
 * @mixin \Eloquent
 */
class ShopncComplainTalk extends BaseModel
{
    protected $table = 'shopnc_complain_talk';

    protected $primaryKey = 'talk_id';

	public $timestamps = false;

    protected $fillable = [
        'complain_id',
        'talk_member_id',
        'talk_member_name',
        'talk_member_type',
        'talk_content',
        'talk_state',
        'talk_admin',
        'talk_datetime'
    ];

    protected $guarded = [];

        
}