<?php

namespace App\Models;



/**
 * Class ShopncGadmin
 *
 * @property int $gid 自增id
 * @property string|null $gname 组名
 * @property string|null $limits 权限内容
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGadmin whereGid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGadmin whereGname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGadmin whereLimits($value)
 * @mixin \Eloquent
 */
class ShopncGadmin extends BaseModel
{
    protected $table = 'shopnc_gadmin';

    protected $primaryKey = 'gid';

	public $timestamps = false;

    protected $fillable = [
        'gname',
        'limits'
    ];

    protected $guarded = [];

        
}