<?php

namespace App\Models;



/**
 * Class ShopncGlobalMsgTpl
 *
 * @property int $id 主键id
 * @property string $tpl_title 模板标题
 * @property int $mmt_message_switch 站内信接收开关
 * @property string $mmt_message_subject 消息标题
 * @property string|null $mmt_message_content 消息内容
 * @property int $mmt_short_switch 短信接收开关
 * @property string|null $mmt_short_content 短信内容
 * @property int $send_type 发送类型: 1系统+短信2系统3短信
 * @property int $send_way 发送方式: 1立即2定时
 * @property int $send_state 发送状态: 1未发送2已发送
 * @property int|null $send_time 发送时间
 * @property int|null $add_time 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereAddTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereMmtMessageContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereMmtMessageSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereMmtMessageSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereMmtShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereMmtShortSwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereSendState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereSendTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereSendType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereSendWay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncGlobalMsgTpl whereTplTitle($value)
 * @mixin \Eloquent
 */
class ShopncGlobalMsgTpl extends BaseModel
{
    protected $table = 'shopnc_global_msg_tpl';

    public $timestamps = false;

    protected $fillable = [
        'tpl_title',
        'mmt_message_switch',
        'mmt_message_subject',
        'mmt_message_content',
        'mmt_short_switch',
        'mmt_short_content',
        'send_type',
        'send_way',
        'send_state',
        'send_time',
        'add_time'
    ];

    protected $guarded = [];

        
}