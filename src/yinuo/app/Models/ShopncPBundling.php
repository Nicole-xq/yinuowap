<?php

namespace App\Models;



/**
 * Class ShopncPBundling
 *
 * @property int $bl_id 组合ID
 * @property string $bl_name 组合名称
 * @property int $store_id 店铺名称
 * @property string $store_name 店铺名称
 * @property float $bl_discount_price 组合价格
 * @property int $bl_freight_choose 运费承担方式
 * @property float|null $bl_freight 运费
 * @property int $bl_state 组合状态 0-关闭/1-开启
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlDiscountPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlFreight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlFreightChoose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereBlState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncPBundling whereStoreName($value)
 * @mixin \Eloquent
 */
class ShopncPBundling extends BaseModel
{
    protected $table = 'shopnc_p_bundling';

    protected $primaryKey = 'bl_id';

	public $timestamps = false;

    protected $fillable = [
        'bl_name',
        'store_id',
        'store_name',
        'bl_discount_price',
        'bl_freight_choose',
        'bl_freight',
        'bl_state'
    ];

    protected $guarded = [];

        
}