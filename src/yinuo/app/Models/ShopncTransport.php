<?php

namespace App\Models;



/**
 * Class ShopncTransport
 *
 * @property int $id 运费模板ID
 * @property string $title 运费模板名称
 * @property int|null $send_tpl_id 发货地区模板ID
 * @property int $store_id 店铺ID
 * @property int|null $update_time 最后更新时间
 * @property int $goods_fee_type 运费承担:1买家承担2是商家承担
 * @property int $goods_trans_type 计费规则:1是按件数2是按重量3是按体积
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereGoodsFeeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereGoodsTransType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereSendTplId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncTransport whereUpdateTime($value)
 * @mixin \Eloquent
 */
class ShopncTransport extends BaseModel
{
    protected $table = 'shopnc_transport';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'send_tpl_id',
        'store_id',
        'update_time',
        'goods_fee_type',
        'goods_trans_type'
    ];

    protected $guarded = [];

        
}