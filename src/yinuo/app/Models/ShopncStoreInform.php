<?php

namespace App\Models;



/**
 * Class ShopncStoreInform
 *
 * @property int $inform_id 商家资讯ID
 * @property int $store_vendue_id 商家拍卖ID
 * @property int $store_id 商家ID
 * @property string|null $inform_url 商家资讯链接
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreInform whereInformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreInform whereInformUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreInform whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncStoreInform whereStoreVendueId($value)
 * @mixin \Eloquent
 */
class ShopncStoreInform extends BaseModel
{
    protected $table = 'shopnc_store_inform';

    protected $primaryKey = 'inform_id';

	public $timestamps = false;

    protected $fillable = [
        'store_vendue_id',
        'store_id',
        'inform_url'
    ];

    protected $guarded = [];

        
}