<?php

namespace App\Models;



/**
 * Class ShopncNavigation
 *
 * @property int $nav_id 索引ID
 * @property int $nav_type 类别，0自定义导航，1商品分类，2文章导航，3活动导航，默认为0
 * @property string|null $nav_title 导航标题
 * @property string|null $nav_url 导航链接
 * @property int $nav_location 导航位置，0头部，1中部，2底部，默认为0
 * @property int $nav_new_open 是否以新窗口打开，0为否，1为是，默认为0
 * @property int $nav_sort 排序
 * @property int $item_id 类别ID，对应着nav_type中的内容，默认为0
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavNewOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncNavigation whereNavUrl($value)
 * @mixin \Eloquent
 */
class ShopncNavigation extends BaseModel
{
    protected $table = 'shopnc_navigation';

    protected $primaryKey = 'nav_id';

	public $timestamps = false;

    protected $fillable = [
        'nav_type',
        'nav_title',
        'nav_url',
        'nav_location',
        'nav_new_open',
        'nav_sort',
        'item_id'
    ];

    protected $guarded = [];

        
}