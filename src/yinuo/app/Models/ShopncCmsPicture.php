<?php

namespace App\Models;



/**
 * Class ShopncCmsPicture
 *
 * @property int $picture_id 画报编号
 * @property string $picture_title 画报标题
 * @property int $picture_class_id 画报分类编号
 * @property string $picture_author 画报作者
 * @property string|null $picture_abstract 画报摘要
 * @property string|null $picture_image 画报图片
 * @property string|null $picture_keyword 画报关键字
 * @property int $picture_publish_time 画报发布时间
 * @property int $picture_click 画报点击量
 * @property int $picture_sort 画报排序0-255
 * @property int $picture_commend_flag 画报推荐标志1-未推荐，2-已推荐
 * @property int $picture_comment_flag 画报是否允许评论1-允许，2-不允许
 * @property string|null $picture_verify_admin 画报审核管理员
 * @property int $picture_verify_time 画报审核时间
 * @property int $picture_state 1-草稿、2-待审核、3-已发布、4-回收站、5-已关闭
 * @property string $picture_publisher_name 发布人用户名
 * @property int $picture_publisher_id 发布人编号
 * @property int $picture_type 画报类型1-管理员发布，2-用户投稿
 * @property string $picture_attachment_path
 * @property int $picture_modify_time 画报修改时间
 * @property string|null $picture_tag 画报标签
 * @property int|null $picture_comment_count 画报评论数
 * @property string|null $picture_title_short 画报短标题
 * @property int|null $picture_image_count 画报图片总数
 * @property int|null $picture_share_count 画报分享数
 * @property string|null $picture_verify_reason 审核失败原因
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureAbstract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureAttachmentPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureClick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureCommendFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureCommentFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureImageCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureModifyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePicturePublishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePicturePublisherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePicturePublisherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureShareCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureTitleShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureVerifyAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureVerifyReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncCmsPicture wherePictureVerifyTime($value)
 * @mixin \Eloquent
 */
class ShopncCmsPicture extends BaseModel
{
    protected $table = 'shopnc_cms_picture';

    protected $primaryKey = 'picture_id';

	public $timestamps = false;

    protected $fillable = [
        'picture_title',
        'picture_class_id',
        'picture_author',
        'picture_abstract',
        'picture_image',
        'picture_keyword',
        'picture_publish_time',
        'picture_click',
        'picture_sort',
        'picture_commend_flag',
        'picture_comment_flag',
        'picture_verify_admin',
        'picture_verify_time',
        'picture_state',
        'picture_publisher_name',
        'picture_publisher_id',
        'picture_type',
        'picture_attachment_path',
        'picture_modify_time',
        'picture_tag',
        'picture_comment_count',
        'picture_title_short',
        'picture_image_count',
        'picture_share_count',
        'picture_verify_reason'
    ];

    protected $guarded = [];

        
}