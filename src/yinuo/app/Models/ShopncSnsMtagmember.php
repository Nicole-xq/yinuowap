<?php

namespace App\Models;



/**
 * Class ShopncSnsMtagmember
 *
 * @property int $mtag_id 会员标签表id
 * @property int $member_id 会员id
 * @property int $recommend 推荐，默认为0
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMtagmember whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMtagmember whereMtagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncSnsMtagmember whereRecommend($value)
 * @mixin \Eloquent
 */
class ShopncSnsMtagmember extends BaseModel
{
    protected $table = 'shopnc_sns_mtagmember';

    public $timestamps = false;

    protected $fillable = [
        'mtag_id',
        'member_id',
        'recommend'
    ];

    protected $guarded = [];

        
}