<?php

namespace App\Models;



/**
 * Class ShopncVideoAlbum
 *
 * @property int $video_id 视频专辑表id
 * @property string $video_name 视频名称
 * @property string|null $video_tag 视频标签
 * @property int $video_class_id 专辑id
 * @property string $video_cover 视频路径
 * @property int $video_size 视频大小
 * @property int $store_id 所属店铺id
 * @property int $upload_time 视频上传时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereUploadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopncVideoAlbum whereVideoTag($value)
 * @mixin \Eloquent
 */
class ShopncVideoAlbum extends BaseModel
{
    protected $table = 'shopnc_video_album';

    protected $primaryKey = 'video_id';

	public $timestamps = false;

    protected $fillable = [
        'video_name',
        'video_tag',
        'video_class_id',
        'video_cover',
        'video_size',
        'store_id',
        'upload_time'
    ];

    protected $guarded = [];

        
}