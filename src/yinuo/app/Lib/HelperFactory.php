<?php 
namespace App\Lib;

use App\Lib\Helpers\HttpClientHelper;
use App\Lib\Helpers\IpHelper;
use App\Lib\Helpers\ModelHelper;
use App\Lib\Helpers\VerifyHelper;
use App\Lib\TraitCode\InstanceCode;

/**
 * Class HelperFactory
 * @package App\Lib
 * @property VerifyHelper Verify
 * @property ModelHelper Model
 * @property HttpClientHelper HttpClient
 * @property IpHelper Ip
 */
class HelperFactory
{
    use InstanceCode;

    const BASE_NAMESPACE = "App\\Lib\\Helpers\\";

    private $_service = [];

    //不是单例的配置
    static $notSingle = [
    ];
    public function __get($name)
    {
        $className = self::BASE_NAMESPACE . $name .'Helper';
        if(in_array($className, self::$notSingle)){
            return new $className;
        }
        if(!isset($this->_service[$className])){
            if (!class_exists($className)){
                throw new \ErrorException('class: "' . $className . '" not exist');
            }
            $this->_service[$className] = new $className;
        }
        return $this->_service[$className];
    }
}