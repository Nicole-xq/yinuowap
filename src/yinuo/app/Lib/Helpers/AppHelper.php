<?php 
namespace App\Lib\Helpers;

class AppHelper extends HelperBase{
    /**
     * 获得全地址
     * @param $uri
     * @param null $param
     * @return string
     */
    public static function getFullUrl($uri, $param = NULL){
        $BaseUrl = env('APP_URL').'/'.$uri;
        if($param){
            if(strpos($BaseUrl, '?')===false && ((is_string($BaseUrl) && strpos($BaseUrl, '?')!==0) || !is_string($param))){
                $BaseUrl .= '?';
            }else{
                $BaseUrl .= '&';
            }
            $BaseUrl .= (is_array($param) ? http_build_query($param) :  $param);
        }
        return $BaseUrl;
    }
    public static function getPermissionName(){
        $nowPermission = \Route::currentRouteName();
        if(!$nowPermission){
            $nowPermission = \Request::getPathInfo();
            $nowPermission = str_replace('/', '.', substr($nowPermission, 1));
        }
        return $nowPermission;
    }
}