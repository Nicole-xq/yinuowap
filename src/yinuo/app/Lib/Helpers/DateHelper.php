<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/4/14 0014
 * Time: 17:35
 */

namespace App\Lib\Helpers;


class DateHelper
{
    /**
     * 时间戳转换为数据库记录的 timestamp
     * @param $time
     * @return false|string
     */
    public static function timeToDbDate($time){
        return date('Y-m-d H:i:s', $time);
    }
}