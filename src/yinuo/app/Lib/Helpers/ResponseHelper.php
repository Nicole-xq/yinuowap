<?php
namespace App\Lib\Helpers;

use App\Services\Constant\ApiResponseCode;
use App\Services\Constant\ResponseCode;

class ResponseHelper extends HelperBase{
    /**
     * 把JsonResponse响应附上跨域支持
     * !先使用isCrossAllow判断是否允许跨域!
     * @param Response $response
     * @return Response
     */
    public static function appendCrossResponse(&$response){
        try{
            $response->header('Access-Control-Allow-Origin', "*");
            $response->header('Access-Control-Allow-Headers', config('access_control_allows.headers'));
            $response->header('Access-Control-Allow-Methods', config('access_control_allows.methods'));
            $response->header('Access-Control-Allow-Credentials', config('access_control_allows.credentials'));
            return $response;
        }catch (\Exception $e){
            \Log::error("appendCrossResponse_error", LogHelper::getEInfo($e));
        }
        return $response;
    }

    /**
     * 是否允许跨域
     * @param $referer
     * @return bool
     */
    public static function isCrossAllow($referer){
        if($referer){
            $origins = config('access_control_allows.origins');
            //当前站
            if(strpos($referer, config('app.url'))===0){
                return true;
            }
            $match_origin = false;
            foreach ($origins as $origin) {
                if(preg_match($origin, $referer) > 0){
//                    $match_origin = $origin;
                    $match_origin = true;
                    break;
                }
            }
            return $match_origin;
        }
        return true;
    }
    public static function json($code, $data = NULL, $status = ''){
        $responseData = ['code'=>$code, 'status'=>$status ? $status : ResponseCode::getMsg($code)];
        if($data !== NULL){
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
    public static function jsonApi($code, $data = NULL, $status = ''){
        $responseData = ['code'=>$code];
        //成功的不返回常用msg
        if($code != ApiResponseCode::SUCCESS || $status !== ''){
            $responseData['msg'] = $status ? $status : ApiResponseCode::getMsg($code);
        }
        if($data !== NULL){
            $responseData['data'] = $data;
        }
        return response()->json($responseData);
    }
}