<?php 
namespace App\Lib\Helpers;

use GuzzleHttp\Client;

class HttpClientHelper extends HelperBase{
    //get的默认请求超时时间
    const GET_DEFAULT_TIME_OUT = 120;
    /**
     * @param $url
     * @param array $param
     * @param null $timeout
     * @return array | NULL
     */
    public function getJson($url, $param = [], $timeout = NULL){
        try{
            $response = $this->_get($url, [
                'query' => $param,
                'timeout' => $timeout,
                //这里补充 json 请求头
                'headers' => [
                    'Accept'     => 'application/json',
                ]
            ]);
        }catch (\Exception $e){
            \Log::error('HttpClient_getJson_Exception', [func_get_args(), LogHelper::getEInfo($e)]);
            return NULL;
        }
        if($response->getStatusCode() !== 200){
            \Log::error('HttpClient_getJson_statusCode_not_200', [func_get_args(), $response->getStatusCode()]);
            return NULL;
        }
        $jsonCon = $response->getBody();
        $json = $jsonCon ? json_decode($jsonCon, true) : NULL;
        if(!$json){
            \Log::warning('HttpClient_getJson_JsonCon_Error', [func_get_args(), $jsonCon]);
            return NULL;
        }
        return $json;
    }

    /**
     * @param array ...$param
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function _get(...$param){
        $param[1] = $param[1] ? $param[1] : [];
        $param[1]['timeout'] = $this->_getGetTimeOut(array_get($param, 'timeout'));
        return $this->_getHttpClient()
            ->get(...$param);
    }
    protected function _getHttpClient(...$param){
        return new Client(...$param);
    }
    protected function _getGetTimeOut($time_out = NULL){
        if(is_int($time_out) && $time_out >= 0){
            return $time_out;
        }
        return self::GET_DEFAULT_TIME_OUT;
    }
}