<?php 
namespace App\Lib\Helpers;

class ToolsHelper extends HelperBase{
    /**
     * 获得唯一字符串
     * @param null $length 最长40
     * @return string
     */
    public static function generateUniqid($length=null){
         $uniqid = sha1(uniqid('', true).\Illuminate\Support\Str::random(25).microtime(true));
         if($length !== null){
             $maxLength = strlen($uniqid);
             if($length < $maxLength){
                 $start = mt_rand(0, $maxLength - $length);
                 $uniqid = substr($uniqid, $start, $length);
             }
         }
         return $uniqid;
    }

    /**
     * 随机数字
     * 首数字从一开始可能会变
     * @param int $length
     * @return int
     * @throws \Exception
     */
    public static function randomNumber($length){
        if(!is_int($length) || $length < 1){
            throw new \Exception('错误的$length参数', 0);
        };
        return mt_rand(pow(10, $length-1), (pow(10, $length)-1));
    }

    /**
     * 根据出生时间返回年龄
     * @param $birthTime
     * @return bool|int
     */
    public static function getAgeByBirthday($birthTime){
        if($birthTime < 1){
            return false;
        }
        list($y1,$m1,$d1) = explode("-",date("Y-m-d",$birthTime));
        $now = strtotime("now");
        list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now));
        $age = $y2 - $y1;
        if((int)($m2.$d2) < (int)($m1.$d1))
            $age -= 1;
        return $age;
    }

    /**
     * 判断是否休假
     * 20170118 判断2017年的除夕休假
     * @param $time 基于这个时间判断 如果没有 默认为当前时间
     * @return bool
     */
    public static function isHoliday($time = NULL){
        if($time === NULL){
            $time = time();
        }
        //2017/1/26 21:0:0
        $startHolidayTime = 1485435600;
        //2017/2/3 0:0:0
        $endHolidayTime = 1486051200;
        if($time >= $startHolidayTime && $time <= $endHolidayTime){
            return true;
        }
        return false;
    }

    //判断是否是正常字符组成的URL
    public static function isNormalUrl($str){
        $rule_str = '/^[0-9a-z:&?=._%\-\/#]+$/i';
        return empty($str) ? false : preg_match($rule_str, $str);
    }

    //是否绝对地址
    public static function isAbsoluteUrl($url){
        if(strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0){
            return true;
        }
        return false;
    }

    //时间差计算
    public static function time2Units ($date){
       $past = strtotime($date); // Some timestamp in the past
       $now  = time();     // Current timestamp
       $time = $now - $past;
       $year   = floor($time / 60 / 60 / 24 / 365);
       $time  -= $year * 60 * 60 * 24 * 365;
       $month  = floor($time / 60 / 60 / 24 / 30);
       $time  -= $month * 60 * 60 * 24 * 30;
       $week   = floor($time / 60 / 60 / 24 / 7);
       $time  -= $week * 60 * 60 * 24 * 7;
       $day    = floor($time / 60 / 60 / 24);
       $time  -= $day * 60 * 60 * 24;
       $hour   = floor($time / 60 / 60);
       $time  -= $hour * 60 * 60;
       $minute = floor($time / 60);
       $time  -= $minute * 60;
       $second = $time;
       $elapse = '';

       $unitArr = array('年'  =>'year', '个月'=>'month',  '周'=>'week', '天'=>'day',
                        '小时'=>'hour', '分钟'=>'minute', '秒'=>'second'
                        );

       foreach ( $unitArr as $cn => $u )
       {
           if ( $$u > 0 )
           {
               $elapse = $$u . $cn;
               break;
           }
        }

        return $elapse;
   }

    /*4、完整的时间：2018-08-28 10:29:16 247591*/
    public static function uDate($format = 'u', $uTimestamp = null)
    {
        if (is_null($uTimestamp)){
            $uTimestamp = microtime(true);
        }
        $timestamp = floor($uTimestamp);
        $milliseconds = round(($uTimestamp - $timestamp) * 1000000);//改这里的数值控制毫秒位数
        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }
}