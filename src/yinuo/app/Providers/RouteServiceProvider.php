<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::post('/rpc')->middleware('thrift:'.!isProduction());
        $this->mapApiRoutes();
        if(!isProduction()){
            $this->mapTestRoutes();
        }


        Route::options('/{all}', function(Request $request) {
            return response("", 200,
                [
                    "Access-Control-Allow-Origin"=>"*",
                    "Access-Control-Allow-Credentials"=>"true",
                    "Access-Control-Allow-Methods"=>"POST, GET, OPTIONS, PUT, DELETE",
                    "Access-Control-Allow-Headers" => "X-App-Ver, Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie",
                ]
            );
        })->where(['all' => '([a-zA-Z0-9-]|/)+']);
    }


    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('/api')
             ->middleware(['api'])
             ->namespace($this->namespace. '\Api')
             ->group(base_path('routes/api.php'));
    }

    /**
     * test 路由
     */
    protected function mapTestRoutes()
    {
        $routeRegistrar = Route::prefix('test')
            ->namespace($this->namespace.'\Test');
        // ！由于 resource 的路由绑定关系 admin_user/ 打头的get路由都必须放在 resource 的前面 ！
        $routeRegistrar->group(base_path('routes/test.php'));
    }
}
