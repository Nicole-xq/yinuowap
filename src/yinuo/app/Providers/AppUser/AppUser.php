<?php
namespace App\Providers\AppUser;

class AppUser{

    private $uid = null;
    private $userName = null;
    private $token = null;
    /**
     * @var array|null
     *      int $token_id 令牌编号
     *      int $member_id 用户编号
     *      string $member_name 用户名
     *      int|null $user_id 用户ID
     *      string|null $user_name 用户名
     *      string $token 登录令牌
     *      int $login_time 登录时间
     *      string $client_type 客户端类型 android wap
     *      string|null $openid 微信支付jsapi的openid缓存
     */
    private $tokenInfo = null;

    /**
     * @return null
     */
    public function getUserName()
    {
        return $this->userName;
    }

    public function getUid(){
        return $this->uid;
    }

    /**
     * @return null|string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *      @return array|null
     *      int $token_id 令牌编号
     *      int $member_id 用户编号
     *      string $member_name 用户名
     *      int|null $user_id 用户ID
     *      string|null $user_name 用户名
     *      string $token 登录令牌
     *      int $login_time 登录时间
     *      string $client_type 客户端类型 android wap
     *      string|null $openid 微信支付jsapi的openid缓存
     */
//    public function getTokenInfo()
//    {
//        return $this->tokenInfo;
//    }


    /**
     * @param $key
     * @throws \Exception
     */
    public function init($key){
        if($this->uid){
            throw new \Exception("重复初始化");
        }
        $this->token = $key;
        /**
         * 通过mb_user_token表获得基础用户信息
         * @param $token
         * @return array|null
         *      int $token_id 令牌编号
         *      int $member_id 用户编号
         *      string $member_name 用户名
         *      int|null $user_id 用户ID
         *      string|null $user_name 用户名
         *      string $token 登录令牌
         *      int $login_time 登录时间
         *      string $client_type 客户端类型 android wap
         *      string|null $openid 微信支付jsapi的openid缓存
         */
        $tokenInfo = application()->userApi->getUserTokenInfoByToken($key);
        if($tokenInfo){
            $this->uid = $tokenInfo['member_id'];
            $this->userName = $tokenInfo['member_name'];
//            $this->tokenInfo = $tokenInfo;
        }
    }

    /**
     * 检查是否登录
     * @return bool
     */
    public function checkLogin(){
        if(!$this->uid){
            return false;
        }
        return true;
    }
}