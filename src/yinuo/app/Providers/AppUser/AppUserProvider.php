<?php

namespace App\Providers\AppUser;

use Illuminate\Support\ServiceProvider;

class AppUserProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("appUser", function (){
            return new AppUser();
        });
    }
}
