<?php

namespace App\Providers;

//use Galaxy\Framework\MQ\Lib\MessageCenter;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        YNApp()->setErrorReporting(defined("APP_DEBUG")?APP_DEBUG:env("APP_DEBUG",false));
        // 兼容老数据库 https://segmentfault.com/a/1190000008416200
         Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->_registerDevProvider();
        $this->_registerGalaxyProvider();
        $this->_rpcRegister();
    }

    private function _registerDevProvider(){
        //\app("events")->listen();

        if (!isProduction()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Iber\Generator\ModelGeneratorProvider::class);
            $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
            // $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
           // $this->app->alias('Debugbar' , \Barryvdh\Debugbar\Facade::class);
        }
    }
    private function _registerGalaxyProvider(){
        //$this->app->singleton("galaxy-log", 'Galaxy\Framework\Service\LogImpl');

//        $this->app->singleton("messageCenter", function($app) {
//            $config = config("rabbitmq");
//            return new MessageCenter($config['host'], $config['port'], $config['user'],
//                $config['pass'],$config['vhost']);
//        });
    }
    private function _rpcRegister(){
        $this->app->register(\Galaxy\Framework\Service\ServiceProvider::class);
        $this->app->singleton("artistService", function (){
            $client = app("galaxy-client");
            $client = $client->with('Server.Rpc.Service.Artist.ArtistService');
            return $client;
        });
    }
}
