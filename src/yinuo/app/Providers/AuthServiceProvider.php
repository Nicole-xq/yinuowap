<?php

namespace App\Providers;

use App\Services\Auth\AdminTokenGuard;
use Illuminate\Http\Request;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param Request $request
     */
    public function boot(Request $request)
    {
        $this->registerPolicies();

        //
        \Auth::extend('adminToken', function ($app, $name, array $config) use ($request) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new AdminTokenGuard(\Auth::createUserProvider($config['provider']), $request);
        });
    }
}
