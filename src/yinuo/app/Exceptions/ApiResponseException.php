<?php
namespace App\Exceptions;

/**
 * 对外的api响应异常
 * !这个异常在shopNc下也使用了,请不要使用laravel特性!
 * Class ApiResponseException
 * code 使用 ApiResponseCode
 * @package App\Exceptions
 */
class ApiResponseException extends ResponseException {
    protected static $_codeClass = \App\Services\Constant\ApiResponseCode::class;
}