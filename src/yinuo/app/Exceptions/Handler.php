<?php

namespace App\Exceptions;

use App\Lib\Helpers\LogHelper;
use App\Lib\Helpers\ResponseHelper;
use App\Services\Constant\ApiResponseCode;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \App\Exceptions\ResponseException::class,
    ];
    /**
     * 非生成环境下需要报的异常 与 dontReport同时使用
     *
     * @var array
     */
    protected $devReport = [
        \Illuminate\Session\TokenMismatchException::class,
//        \Symfony\Component\HttpKernel\Exception\HttpException::class
    ];
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception) || $this->devShouldReport($exception)){
            \Log::error("exception_handler", ['env'=>env('APP_ENV'), LogHelper::getEInfo($exception)]);
            if(!isProduction()){
                \Log::error("exception_handler_showExceptionTrace", [showExceptionTrace($exception, false, true, false)]);
            }
        }
//        parent::report($exception);
    }
    protected function devShouldReport($exception){
        if(isProduction()){
            return false;
        }
        return !is_null(collect($this->devReport)->first(function ($type) use ($exception) {
            return $exception instanceof $type;
        }));
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $data = NULL;
        $code = $exception->getCode();
        $message = $exception->getMessage();

        //统一报错 避免用户看到不必要的报错
        if(!($exception instanceof ResponseException)) {
            //这里确保 日志里已经打了 error log
            if(isProduction()){
                $message = '网络繁忙，请稍后重试';
                $code = ApiResponseCode::OPERATE_FAIL;
            }else{
                $message = showExceptionTrace($exception, false, true, false);
            }

        }
        if ($request->wantsJson()) {
            $response = ResponseHelper::jsonApi($code, $data, $message);
            /*if(!($exception instanceof ResponseException)){
                $response->setStatusCode(500);
            }*/
        } else {
            //这里异常转换 (html 报错)
            $response = parent::render($request, $exception);
        }
        ResponseHelper::appendCrossResponse($response);
        return $response;
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
