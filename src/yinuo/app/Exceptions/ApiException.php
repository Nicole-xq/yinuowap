<?php

namespace App\Exceptions;

/**
 * Class ApiException
 * @deprecated 不要再使用这里了 请使用 ResponseException ApiResponseException
 * @package App\Exceptions
 */
class ApiException extends \Exception{
    const GOD_BLESS_YOU = 200;
    const FAIL = 9999;
    const INVALID_DATA = 1001;
    const ARGS_ERROR = 1002;
    const IM_SERVER_ERROR = 1003;

    public static $errorMessages = [
        self::FAIL => '操作失败',
        self::INVALID_DATA => '无效的数据格式',
        self::ARGS_ERROR => '参数错误',
        self::IM_SERVER_ERROR => '服务器异常',
    ];

    public static function throwException($code, $msg = '')
    {
        if (!empty($msg)) {
            $message = $msg;
        } elseif (!empty(self::$errorMessages[$code])) {
            $message = self::$errorMessages[$code];
        } else {
            $message = "未知错误[{$code}]";
        }

        if (!isProduction()) {
            $e = debug_backtrace()[0];
            $file = $e['file'];
            $line = $e['line'];
            throw new self($message . " " . $file . " " . $line, $code);
        }
        throw new self($message, $code);
    }
}