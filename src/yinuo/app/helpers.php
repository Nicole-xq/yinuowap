<?php

if (!function_exists("application")) {
    /**
     * @return \App\Application
     */
    function application() {
        return \App\Application::one();
    }
}

/**
 * @return \App\Services\Auth\AdminTokenGuard
 */
function adminGuard() {
    return \Auth::guard('admin');
}

/**
 * 是否为生产环境
 * @return bool
 */
function isProduction(){
    return env('APP_ENV') == 'production' || env('APP_ENV') == 'prod';
}

//开发环境使用 调试工具:
if(!isProduction() && !function_exists("myDump")){
    function myDump($valS, $isExit = true, $isPre = true){
        call_user_func_array(array('\App\Lib\Helpers\Debug', PHP_SAPI != 'cli' ? 'dump':'cliDump'), func_get_args());
    }
    function fileDump($val){
        call_user_func_array(array('\App\Lib\Helpers\Debug', 'fileDump'), func_get_args());
    }
    function showExceptionTrace(\Exception $e, $isExit = true, $is_return = false, $ispre = true){
        if($is_return){
            ob_start();
        }
        call_user_func_array(array('\App\Lib\Helpers\Debug', (PHP_SAPI != 'cli' && $ispre == true) ? 'showExceptionBacktrace':'cliShowExceptionBacktrace'), func_get_args());
        if($is_return){
            $outStr =  ob_get_contents();
            ob_end_clean();
            return $outStr;
        }
    }
}