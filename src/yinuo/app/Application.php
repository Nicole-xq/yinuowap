<?php namespace App;

use App\Logic\Api\AccountApi;
use App\Logic\Api\Admin\PermissionApi;
use App\Logic\Api\Admin\PlatformApi;
use App\Logic\Api\AuctionApi;
use App\Logic\Api\BidLogApi;
use App\Logic\Api\CloseAccountApi;
use App\Logic\Api\ExpressApi;
use App\Logic\Api\MemberApi;
use App\Logic\Api\OrderApi;
use App\Logic\Api\RankingApi;
use App\Logic\Api\RedPacketApi;
use App\Logic\Api\SellerApi;
use App\Logic\Api\StoreApi;
use App\Logic\Api\ThirdPartyApi;
use App\Logic\Api\ThirdPartyApiApi;
use App\Logic\Api\UserApi;
use App\Logic\Api\UserSafeApi;
use App\Providers\AppUser\AppUser;
use Server\Rpc\Service\Artist\ArtistServiceClient;
use Server\Rpc\Service\Artist\ArtistServiceIf;

//use Galaxy\Framework\MQ\Lib\MessageCenter;

/**
 *
 * Class Application
 * @package App
 * @property MessageCenter messageCenter
 *
 * @property UserApi userApi
 * @property OrderApi orderApi
 * @property SellerApi sellerApi
 * @property StoreApi storeApi
 * @property ExpressApi expressApi
 * @property AccountApi accountApi
 * @property CloseAccountApi closeAccountApi
 * @property AuctionApi auctionApi
 * @property RankingApi rankingApi
 * @property RedPacketApi redPacketApi
 * @property BidLogApi bidLogApi
 * @property MemberApi memberApi
 *
 *
 * @property ArtistServiceClient artistService
 *
 *
 *
 * @property AppUser appUser
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * //返回全局实例
     * @return Application
     */
    public static function one()
    {
        return self::getInstance();
    }
}
