<?php
namespace App\Http\Controllers\Api;

use App\Logic\Api\RankingApi;
use App\Models\ShopncRankingMember;
use Illuminate\Http\Request;

class RankingListController extends Controller
{
    /**
     * @SWG\Post(path="/api/RankingList/index",
     *   tags={"ranking"},
     *   operationId="/api/RankingList/index",
     *   summary="拉新排行榜",
     *   description="拉新排行榜",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="period_num",
     *     in="formData",
     *     description="期数 不传默认是当前期",
     *     required=false,
     *     type="integer"
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/RankingListIndexResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResponseException
     */
    public function index(Request $request)
    {
        $rules = [
            'period_num' => 'int|min:1',
        ];
        $this->nValidate($request, $rules);

        $period_num = $request->input("period_num", application()->rankingApi->getPeriodNum(time()));
        $topPackage = config("newbie.topPackage.".$period_num);
        $periodRankingDate = application()->rankingApi->getPeriodRankingData($period_num);
        $data['list'] = $periodRankingDate['list'];
        $topAdd = empty($periodRankingDate['topPackage']) ? 0 : count($periodRankingDate['topPackage']);
        $data['rankingHistory'] = application()->rankingApi->getRankingHistoryData();

        $nowPeriodWhere = RankingApi::$newBieRankingWhere;
        $nowPeriodWhere["period_num"] =  $period_num;
        //当期用户信息
        if(application()->appUser->getUid() > 0){
            $nowUserWhere = $nowPeriodWhere;
            $nowUserWhere[] = ["member_id", application()->appUser->getUid()];
            $nowUserInfo = application()->rankingApi->getInfoByWhere($nowUserWhere, ["member_name", "member_id", 'member_avatar', 'ranking_num', 'recommend_num']);
            if($nowUserInfo){
                $nowUserInfo = $nowUserInfo->toArray();
//                $nowUserInfo['ranking_num'] += $topAdd;
                if($topPackage){
                    foreach ($topPackage as $top){
                        if($top['recommend_num'] > $nowUserInfo['recommend_num']){
                            $nowUserInfo['ranking_num'] ++;
                        }
                    }
                }
            }else{
                $nowUserInfo = ["member_avatar" => "", "recommend_num" => 0];
                //取当期最后排名
                $maxUserWhere = $nowPeriodWhere;
                $maxUserInfo = application()->rankingApi->getInfoByWhere($maxUserWhere, ['ranking_num'], ["ranking_num"=>"desc"]);
                $nowUserInfo['ranking_num'] = $topAdd+1;
                if($maxUserInfo){
                    $nowUserInfo['ranking_num'] += $maxUserInfo['ranking_num'];
                }
                $memberInfo = application()->userApi->getInfoByWhere(["member_id"=>application()->appUser->getUid()], ["member_name", "member_mobile", "member_avatar"]);
                if($memberInfo){
                    $nowUserInfo["member_name"] = $memberInfo->member_name ? $memberInfo->member_name : ($memberInfo->member_mobile ? $memberInfo->member_mobile: "匿名用户");
                    $nowUserInfo["member_avatar"] = $memberInfo["member_avatar"];
                }
            }
            $nowUserInfo["member_avatar"] = getMemberAvatarForID(application()->appUser->getUid(), $nowUserInfo['member_avatar']);
            $data["nowUserInfo"] = $nowUserInfo;
        }
        //获得当期剩余时间
        $data['periodResidueTime'] = application()->rankingApi->getPeriodResidueTime(time());
        return $this->_json($data);
    }
}
