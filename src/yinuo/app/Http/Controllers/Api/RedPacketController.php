<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/4/11 0011
 * Time: 14:23
 */

namespace App\Http\Controllers\Api;

use App\Logic\Api\RedPacketApi;
use App\Models\ShopncRedpacket;
use App\Models\ShopncRedpacketTemplate;
use App\Services\Constant\ApiResponseCode;
use Illuminate\Http\Request;

class RedPacketController extends Controller
{
    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'getVoucherList',
        ]
        ]);
    }


    /**
     * @SWG\POST(
     *     path="/api/RedPacket/getRedPacketList",
     *     tags={"newBie"},
     *     operationId="/api/RedPacket/getRedPacketList",
     *     summary="我的代金券列表",
     *     description="我的代金券列表",
     *     produces={"application/json"},
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="代金券类型:margin-保证金代金券列表,order-订单代金券列表",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *     @SWG\Response(response="default", description=""),
     *     @SWG\Response(response="200", description="successful operation", @SWG\Schema(ref="#/definitions/RedPacketListResponse"))
     * )
     */
    public function getRedPacketList(Request $request)
    {
        application()->orderApi;
        $key = $request->key;
        $type = $request->type;
        $member_id = application()->memberApi->getMemberIdIfExists($key);
        if (empty($member_id)) {
            return $this->_json(ApiResponseCode::NOT_LOGIN);
        }
        //保证金代金券列表
        $condition = ['rpacket_owner_id' => $member_id];
        $condition['rpacket_t_type'] = ShopncRedpacketTemplate::RPACKET_T_TYPE_NEWBIE;
        if ($type == 'margin') {
            $condition['rpacket_t_special_type'] = ShopncRedpacketTemplate::RPACKET_T_SPECIAL_TYPE_MARGIN;
        }
        //订单代金券列表
        if ($type == 'order') {
            $condition['rpacket_t_special_type'] = ShopncRedpacketTemplate::RPACKET_T_SPECIAL_TYPE_ORDER;
        }
        $redPacketList = application()->redPacketApi->getRedPacketListType($condition);
        foreach ($redPacketList as $key => &$redPacket) {
            if ($redPacket['rpacket_end_date'] <= time()) {
                $condition = ['rpacket_id' => $redPacket['rpacket_id']];
                $update = ['rpacket_state' => ShopncRedpacket::RPACKET_STATE_EXPIRE];
                application()->redPacketApi->updateRedPacketState($condition, $update);
                unset($redPacketList[$key]);
            }
        }
        $redPacketList = array_values($redPacketList);
        return $this->_json($redPacketList);
    }


    /**
     * @SWG\POST(
     *     path="/api/RedPacket/getRedPacketTplList",
     *     tags={"newBie"},
     *     operationId="/api/RedPacket/getRedPacketTplList",
     *     summary="新手可领取的代金券",
     *     description="新手可领取的代金券",
     *     produces={"application/json"},
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="代金券类型:margin-保证金代金券列表,order-订单代金券列表",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *     @SWG\Response(response="default", description=""),
     *     @SWG\Response(response="200", description="successful operation", @SWG\Schema(ref="#/definitions/RedPacketListResponse"))
     * )
     */
    public function getRedPacketTplList()
    {
        //可领取红包列表
        application()->orderApi;
        $redPacketTplList = application()->redPacketApi->freeRedPacketList();
        foreach ($redPacketTplList as &$redPacketTpl) {
            $redPacketTpl['rpacket_t_price'] = intval($redPacketTpl['rpacket_t_price']);
            $redPacketTpl['rpacket_t_limit'] = intval($redPacketTpl['rpacket_t_limit']);
        }
        return $this->_json($redPacketTplList);
    }
}