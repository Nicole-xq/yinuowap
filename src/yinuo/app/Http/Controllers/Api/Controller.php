<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiResponseException;
use App\Lib\Helpers\ResponseHelper;
use App\Services\Constant\ApiResponseCode;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * @SWG\Swagger(
 *     host="",
 *     basePath="/src/index.php",
 *     swagger="2.0",
 *     @SWG\Info(
 *     version="2.0",
 *     title="艺诺艺术商城拍卖API",
 *     description="
 *     //App请求加入:header X-App-Ver=app版本
 *     基础路径: host + /src/index.php
 *     响应结构:{code:200, msg:'成功', data: {}}, 接口中如果没有特殊code,只说明data
 *     在api_key输入框可以输入token
 * <details><summary>code</summary>200:成功; 402:登录失效; 400:操作失败;</details>",
 * )
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="apiKey",
 *   type="apiKey",
 *   in="query",
 *   description = "认证key shopnc_mb_user_token表 token",
 *   name="key"
 * ),
 */
class Controller extends BaseController
{
    use ValidatesRequests;

    /**
     * @param Request|array $validateSource
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @example         $rules = [
                        'amount' => 'required|in:' . implode(',', $products['amount']),
                        'days' => 'required|in:' . implode(',', $products['deadline_day']),
                        'usage' => 'required|in:' . implode(',', array_keys($usages)),
                        'member_id' => 'required|integer|min:1',
                        ];
                      $this->nValidate($request, $rules,
                        [
                            'title.required' => 'A title is required',
                            'title.*' => 'A title is required',
                            'required' => 'The :attribute field is required.',
                            'same'    => 'The :attribute and :other must match.',
                            'size'    => 'The :attribute must be exactly :size.',
                            'between' => 'The :attribute must be between :min - :max.',
                            'in'      => 'The :attribute must be one of the following types: :values',
                        ],
                        [
                            'amount' => '金额',
                            'days' => '天数',
                            'usage' => '借款用途',
                        ]);
     * @throws \App\Exceptions\ResponseException
     */
    public function nValidate($validateSource, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make((is_array($validateSource)) ? $validateSource : $validateSource->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            //todo 后期增加字段 到data里
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID, $validator->errors()->first());
        }
    }

    protected function _json($data = [], $code = NULL, $status = ''){
        if($code === NULL){
            $code = ApiResponseCode::SUCCESS;
        }
        return ResponseHelper::jsonApi($code, $data, $status);
    }
    protected function _jsonError($code = ApiResponseCode::OPERATE_FAIL, $status = '', $data = NULL){
        return ResponseHelper::jsonApi($code, $data, $status);
    }
}
