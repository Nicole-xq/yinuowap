<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/4/18 0018
 * Time: 13:32
 */
namespace App\Http\Controllers\Api\Auction;

use App\Exceptions\ApiResponseException;
use App\Http\Controllers\Api\Controller;
use App\Models\ShopncAuction;
use App\Models\ShopncMarginOrder;
use App\Models\ShopncOrder;
use App\Models\ShopncRedpacket;
use App\Models\ShopncRedpacketTemplate;
use App\Services\Constant\ApiResponseCode;
use Illuminate\Http\Request;

class AuctionController extends Controller
{
    /**
     * @SWG\Post(path="/api/auction/createMarginOrder",
     *   tags={"newBie"},
     *   operationId="/api/auction/createMarginOrder",
     *   summary="创建保证金订单",
     *   description="创建保证金订单",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/NewBieIndexResponse"))
     * )
     *
     */
    public function createMarginOrder()
    {
        echo 'order';die;
    }

    /**
     * @SWG\Post(path="/api/auction/showAuctionOrder",
     *   tags={"auction"},
     *   operationId="/api/auction/showAuctionOrder",
     *   summary="显示拍卖订单支付详情",
     *   description="显示拍卖订单支付详情",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="auction_order_id",
     *     in="formData",
     *     description="拍卖订单id",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="auction_id",
     *     in="formData",
     *     description="拍品id",
     *     required=false,
     *     type="integer"
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/ShowAuctionOrderResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResponseException
     */
    public function showAuctionOrder(Request $request){

        $rules = [
            'auction_order_id' => 'int|min:0',
            'auction_id' => 'int|min:1',
        ];
        $this->nValidate($request, $rules);
        $auction_order_id = $request->input('auction_order_id');
        $auction_id = $request->input('auction_id');
        if (empty($auction_order_id) && empty($auction_id)) {
            return $this->_jsonError(ApiResponseCode::ARG_INVALID);
        }
        $member_id = application()->appUser->getUid();
        $logic_auction_order = Logic('auction_order');
        if (empty($auction_order_id)) {
            $auction_model = Model('auctions');
            $auction_info = $auction_model->getAuctionsInfoByID($auction_id);
            $map['auction_id']=$auction_id;
            $map['member_id']=array('neq',0);
            $auction_info['auction_reserve_price'] = $auction_info['auction_reserve_price'] ?: 0;
            $map['offer_num']=array('egt',$auction_info['auction_reserve_price']);
            $bid_log=Model("bid_log")->where($map)->order("offer_num DESC")->find();
            //查出谁最后出的最高价
            if ($bid_log['member_id'] != 0 && $bid_log['member_id']==$member_id) {
                $logic_auction_order->auctions_order($auction_info,$bid_log);
                $model_order = Model('order');
                $order_chekc_info=$model_order->table("orders")->where(array('buyer_id' => $member_id,'auction_id'=>$auction_id,'order_state'=>array("neq",0)))->find();
                if ($order_chekc_info){
                    $auction_order_id=$order_chekc_info['order_id'];
                }else{
                    return $this->_jsonError(ApiResponseCode::OPERATE_FAIL, '创建订单错误!请稍后再试!'.$member_id."+++".$bid_log['member_id']);
                }
            } else {
                return $this->_jsonError(ApiResponseCode::BAD_REQUEST);
            }
        }

        // 获取支付尾款的必要信息
        $result = $logic_auction_order->new_show_auction_order($auction_order_id, $member_id, $request->input('address_id', 0));
        if (!$result['state']) {
            return $this->_jsonError(ApiResponseCode::NOT_DATA, $result['msg']);
        }
        $data = array(
            'vat_deny' => $result['data']['vat_deny'],
            'vat_hash' => $result['data']['vat_hash'],
            'inv_info' => $result['data']['inv_info'],
            'store_info' => $result['data']['store_info'],
            'auction_order_info' => $result['data']['auction_order_info'],
            'margin_order_info' => $result['data']['margin_order_info'],
            'auction_info' => $result['data']['auction_info'],
            'underline' => true,
            'margin_amount' => $result['data']['margin_amount'],
            'refuse_amount' => $result['data']['refuse_amount'],
        );
        if(
            !empty($result['data']['auction_info']['auction_type']) &&
            $result['data']['auction_info']['auction_type'] == ShopncAuction::AUCTION_TYPE_NEWBIE &&
            isset($result['data']['auction_order_info']['order_amount'])
        ){
            //解锁当前订单的红包
            if($data["auction_order_info"]["rpt_amount"] > 0 && in_array($data["auction_order_info"]["order_state"], [ShopncOrder::ORDER_STATE_UN_PAY])){
                //解锁当前订单的红包
                \DB::transaction(function () use (&$data, &$member_id) {
                    $upCou = ShopncOrder::where(["order_id"=>$data["auction_order_info"]["order_id"], "order_state"=> ShopncOrder::ORDER_STATE_UN_PAY])
                        ->update(["order_amount"=>\DB::raw("order_amount+rpt_amount"), "rpt_amount"=>0]);
                    if($upCou < 1){
                        ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
                    }
                    $redPacketList = application()->redPacketApi->getNewBieRedPacketList(
                        $member_id,
                        [
                            "binding_id"=>$data["auction_order_info"]["order_id"],
                            "rpacket_state"=>ShopncRedpacket::RPACKET_STATE_USED
                        ],
                        null,
                        ["rpacket_id"]
                    );
                    if(empty($redPacketList)){
                        ApiResponseException::throwException(ApiResponseCode::OPERATE_FAIL, "没有绑定的红包,请联系客服处理");
                    }
                    foreach ($redPacketList as $item) {
                        $upRedCou = ShopncRedpacket::where(["rpacket_id"=>$item["rpacket_id"], "rpacket_state"=> ShopncRedpacket::RPACKET_STATE_USED, "binding_id"=>$data["auction_order_info"]["order_id"]])
                            ->update(["rpacket_state"=>ShopncRedpacket::RPACKET_STATE_UNUSED, "binding_id"=>null]);
                        if($upRedCou < 1){
                            ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
                        }
                    }
                    //更新数据
                    $data["auction_order_info"]["order_amount"] += $data["auction_order_info"]["rpt_amount"];
                    $data["auction_order_info"]["pay_amount"] += $data["auction_order_info"]["rpt_amount"];
                    $data["auction_order_info"]["rpt_amount"] = 0;
                });
            }
            //新手专区 ---------------
            //保证金未使用红包时, 取新手专区可用支付红包
            if(ShopncMarginOrder::where("red_packet_id", ">", 0)
                ->where("auction_id", $data["auction_order_info"]["auction_id"])
                ->where("buyer_id", $member_id)
                ->whereIn("order_state", [ShopncMarginOrder::ORDER_STATE_PAID,ShopncMarginOrder::ORDER_STATE_OFFLINE_PAY,ShopncMarginOrder::ORDER_STATE_PARTIAL_PAY])
                ->count() < 1
            ){
                $nowTime = time();
                $where = ["rpacket_t_special_type"=>ShopncRedpacketTemplate::RPACKET_T_SPECIAL_TYPE_ORDER];
                $where["rpacket_state"] = ShopncRedpacket::RPACKET_STATE_UNUSED;
                $where[] = ["rpacket_start_date", "<=" , $nowTime];
                $where[] = ["rpacket_end_date", ">=" , $nowTime];
                $where[] = ["rpacket_limit", "<=" , $result['data']['auction_order_info']['order_amount']];
                $redPacketList = application()->redPacketApi->getNewBieRedPacketList(application()->appUser->getUid(), $where, ["rpacket_price"=>"desc"],
                    ["rpacket_id", "rpacket_title", "rpacket_desc", "rpacket_start_date", "rpacket_end_date", "rpacket_price", "rpacket_limit"]);
                if($redPacketList){
                    $data['redPacketList'] = $redPacketList;
                }
            }
        }

        return $this->_json($data);
    }
}