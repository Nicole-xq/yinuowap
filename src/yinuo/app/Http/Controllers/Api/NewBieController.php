<?php
namespace App\Http\Controllers\Api;

use App\Logic\Api\BidLogApi;
use App\Models\ShopncAuction;
use App\Models\ShopncAuctionSpecial;
use App\Models\ShopncBidLog;
use App\Models\ShopncMember;
use App\Models\ShopncRedpacket;
use App\Services\Constant\ApiResponseCode;
use Illuminate\Http\Request;

class NewBieController extends Controller
{
    /**
     * @SWG\Post(path="/api/NewBie/index",
     *   tags={"newBie"},
     *   operationId="/api/NewBie/index",
     *   summary="新手专区 首页接口",
     *   description="新手专区 首页接口",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/NewBieIndexResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        /*$data = [
            //剩余体验时间
            'timeRemaining' => 100000,
            //代金券总金额
            'voucherMoney' => 180.1,
            //代金券剩余个数
            'voucherCount' => 3,
            //是否打开全列表,自动领取后弹出
            'isOpenVoucherList' => false,
            //专场矩阵
            'specials' => [
                [
                    "startTime"=>1554814292,
                    "startTimeStr" => "09:00",
                    //11 预展开始 12专场开始 13 专场结束
                    "status" => 1,
                    "specialId"=>225
                ],
                [
                    "startTime"=>1554814292,
                    "startTimeStr"=>"10:00",
                    "status" => 2,
                    "specialId"=>739
                ],
            ],
        ];*/
        $outData = [];
        //查询今天的专场
        $todayStart= strtotime(date('Y-m-d 00:00:00', time()));
        $todayEnd= strtotime(date('Y-m-d 23:59:59', time()));
        $where = [["special_start_time", ">", $todayStart]];
        $where[] = ["special_start_time", "<", $todayEnd];
        $where[] = ["is_open", 1];
        $where[] = ["special_state","<>",  ShopncAuctionSpecial::SPECIAL_STATE_NOT_AUDIT];
        $where[] = ["special_state","<>",  ShopncAuctionSpecial::SPECIAL_STATE_AUDIT_FAILURE];
        $where[] = ["special_type",  ShopncAuctionSpecial::SPECIAL_TYPE_NEWBIE];
        $data = application()->auctionApi->getAuctionSpecialList(
            0,
            100,
            $where,
            ['special_start_time'=>'asc'],
            ['special_start_time', 'special_id', 'special_state','special_preview_start','special_end_time']
        );
        $specials = [];
        $current_time = time();
        //专场状态 11 预展开始 12专场开始 13 专场结束
        if ($data["list"]) {
            foreach ($data["list"] as &$datum) {
                if ($datum['special_end_time'] != 0 && $datum['special_end_time'] <= $current_time) {
                    $status = 13;
                } else {
                    if ($datum['special_start_time'] <= $current_time) {
                        $status = 12;
                    } else {
                        $status = 11;
                    }
                }
                $specials[] = [
                    "startTime"=>$datum['special_start_time'],
                    "startTimeStr" => date("H:i", $datum['special_start_time']),
                    //11 预展开始 12专场开始 13 专场结束
                    "status" => $status,
                    "specialId"=>$datum['special_id'],
                ];
            }
        }
        $outData['specials'] = $specials;
        $outData['isNewBie'] = false;
        $outData["isOpenVoucherList"] = false;
        $outData['voucherCount'] = 0;
        $outData['voucherMoney'] = 0;
        $outData['isLogin'] = false;
        if(application()->appUser->checkLogin()){
            $outData['isLogin'] = true;
            //是否是新手
            $outData['isNewBie'] = application()->bidLogApi->isNewBie(application()->appUser->getUid());
            //领红包
            if($outData['isNewBie']){
                $isOk = application()->redPacketApi->autoGetRedPacket(application()->appUser->getUid());
                if($isOk){
                    $outData["isOpenVoucherList"] = true;
                }
            }
            $outData['voucherCount'] = application()->redPacketApi->getNewBieRedPacketCount(application()->appUser->getUid(), ["rpacket_state"=>ShopncRedpacket::RPACKET_STATE_UNUSED]);
            $outData['voucherMoney'] = application()->redPacketApi->freeRedPacketSum();
        }
        return $this->_json($outData);
    }

    /**
     * @SWG\Post(path="/api/NewBie/getAuctionList",
     *   tags={"newBie"},
     *   operationId="/api/NewBie/getAuctionList",
     *   summary="获得专场拍品列表",
     *   description="获得专场拍品列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   @SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认50",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="specialId",
     *     in="formData",
     *     description="专场id",
     *     required=true,
     *     type="integer",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/NewBieAuctionListResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResponseException
     */
    public function getAuctionList(Request $request)
    {
        $rules = [
            'page' => 'required|int|min:1',
            'pageSize' => 'int|min:1|max:80',
            'specialId' => 'required|int',
        ];
        $this->nValidate($request, $rules);

        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 50);
        $specialId = $request->input("specialId");
        $auctionList = application()->auctionApi->getAuctionList(
            $pageSize*($page-1),
            $pageSize,
            ["special_id"=>$specialId],
            ["auction_start_time"=>"asc"],
            [
                'auction_id', 'goods_id', 'auction_name', 'auction_preview_start',
                'auction_start_time', 'auction_end_time', 'auction_image', 'auction_start_price',
                'state', 'auction_click', 'current_price', 'real_participants', 'is_liupai'
            ]
        );
        foreach ($auctionList['list'] as &$action){
            $action["auction_image"] = cthumb($action["auction_image"], 360);
            //已经结束并且未流拍的 显示中拍价
//            if($action['is_liupai'] == ShopncAuction::IS_LIUPAI_NOT && $action["state"] == ShopncAuction::STATE_FINISHED){
            //拍卖状态细化: 1.拍卖结束 2.预展中 3.拍卖中
            if($action["state"] == ShopncAuction::STATE_UNFINISHED){
                //已经开始
                if( time() > $action["auction_start_time"]){
                    //已经结束
                    if(time() > $action["auction_end_time"]){
                        $action["status"] = ShopncAuction::STATUS_FINISHED;
                    }else{
                        $action["status"] = ShopncAuction::STATUS_IN_AUCTION;
                    }
                }
                //未开始
                else{
                    $action["status"] = ShopncAuction::STATUS_IN_PREVIEW;
                }
            }else{
                $action["status"] = $action["state"];
            }
            //有机器人不会流拍
            if($action["state"] == ShopncAuction::STATE_FINISHED){
                //中拍价
                $action["at_last_price"] = $action["current_price"];
            }
        }
        $auctionList['maxPage'] = ceil($auctionList['count']/$pageSize);
        $auctionList['nowPage'] = $page;
        return $this->_json($auctionList);
    }

    /**
     * @SWG\Post(path="/api/NewBie/auctionDetail",
     *   tags={"newBie"},
     *   operationId="/api/NewBie/auctionDetail",
     *   summary="新手拍卖演示的 拍卖详情",
     *   description="新手拍卖演示的 拍卖详情",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/NewBieAuctionResponse"))
     * )
     *
     */
    public function auctionDetail(Request $request)
    {
        application()->orderApi;
        $key = $request->key;
        // is_kaipai 是否开拍 1是（正在进行） 0否 2是（已结束）
        $member_id = (int)application()->memberApi->getMemberIdIfExists($key);
        //$member_id = (int)(new MemberApi())->getMemberIdIfExists($key);

        $auction_id = C('newBieAuctionId');
        if (intval($auction_id <= 0)){
            return $this->_jsonError(ApiResponseCode::ARG_INVALID);
        }

        /** @var auctionsModel $model_auction */
        /** @var auctionLogic $logic_auction */
        /** @var goodsModel $model_goods */
        /** @var goodsLogic $logic_goods */
        $model_auction = Model('auctions');
        $logic_auction = Logic('auction');

        // ---------new
        $fields = 'auction_id, special_id, goods_id, auction_name, auction_image, other_images, auction_start_price,
         current_price, auction_increase_range, auctions_artist_id, auctions_artist_name, create_age, auctions_long,
          gc_id, auctions_width, auctions_height, auctions_spec, share_content, auctions_class_lv1, auctions_class_lv2,
        auction_bond_rate, interest_last_date, auction_click, num_of_applicants, auctions_summary,
        auction_preview_start, auction_start_time, auction_end_time, auction_end_true_t, state, special_id,
        auction_reference_price, store_id';
        $auction_detail = $model_auction->getAuctionsInfo(['auction_id' => $auction_id], $fields);
        //output_data($auction_detail);
        if (empty($auction_detail)) {
            output_error('拍品不存在或未审核');
        }
        $memberInfo = application()->memberApi->getMemberInfo($member_id);
        if (substr($memberInfo['member_avatar'], 0, 4) == 'http') {
            $auction_detail['member_avatar'] = $memberInfo['member_avatar'];
        } else {
            $auction_detail['member_avatar'] = getMemberAvatarForID($memberInfo['member_id']) . '?' . TIMESTAMP;
        }
        $auction_detail['user_name'] = $memberInfo['member_name'];
        //判断是否诺币专场
        /** @var auction_specialModel $special */
        $special = Model('auction_special');
        $nb_condition = ['special_id' => $auction_detail['special_id']];
        $nb_list = $special->where($nb_condition)->find();
        $auction_detail['is_nb'] = isset($nb_list['is_pd_pay']) && $nb_list['is_pd_pay'] == 1 ? 1 : 0;
        $auction_detail['is_fetch'] = 0;
        $auction_detail['order_id'] = 0;
        $auction_detail['is_pay'] = 0;
        $auction_detail['is_mind'] = 0;
        $auction_detail['margin_money'] = 0;
        //介绍拍乐赚
        $auction_detail['recommended'] = 0;
        //最后一次出价时间
        $auction_detail['last_offer_time'] = 0;
        $auction_detail['commission_amount'] = 0;
        $auctions_class_lv1_info = Model('auctions_class')->getOneById($auction_detail['auctions_class_lv1']);
        $auctions_class_lv1_name = $auctions_class_lv1_info['auctions_class_name'];
        $auctions_class_lv2_info = Model('auctions_class')->getOneById($auction_detail['auctions_class_lv2']);
        $auctions_class_lv2_name = $auctions_class_lv2_info['auctions_class_name'];
        $auction_detail['auctions_class_lv1_name'] = $auctions_class_lv1_name;
        $auction_detail['auctions_class_lv2_name'] = $auctions_class_lv2_name;
        /** @var auction_configModel $auction_config_model */
        $auction_config_model = Model('auction_config');
        $auction_config = $auction_config_model->getInfo(1);
        $auction_detail['duration_time'] = isset($auction_detail['duration_time'])
            ? intval($auction_detail['duration_time']/60/24) : 1;
        $auction_detail['add_time'] = isset($auction_config['add_time'])
            ? $auction_config['add_time'] : 2;
        $auction_detail['auction_image'] = cthumb($auction_detail['auction_image'], 360);
        $auction_detail['auctions_summary'] = html_entity_decode(
            htmlspecialchars_decode($auction_detail['auctions_summary'])
        );
        $auction_body = '<div class="default">' . $auction_detail['auctions_summary'] . '</div>';
        $auction_info['auctions_summary'] = $auction_body;
        if (!empty($auction_detail['auctions_spec'])) {
            $auctions_spec_info = json_decode($auction_detail['auctions_spec'], true);
        }
        $new_spec = [];
        if (!empty($auctions_spec_info) && is_array($auctions_spec_info)) {
            /** @var $auctions_spec_model $auctions_spec_model */
            /** @var $auctions_spec_class_model $auctions_spec_model */
            $auctions_spec_model = Model('auctions_spec');
            $auctions_spec_class_model = Model('auctions_spec_class');
            foreach ($auctions_spec_info as $key => $value) {
                $spec_info = $auctions_spec_model->getOneById($key);
                if (empty($spec_info)) {
                    continue;
                }
                $class_info = $auctions_spec_class_model->getOneById($value);
                if (empty($class_info)) {
                    continue;
                }
                $key = $spec_info['auctions_spec_name'];
                $value = $class_info['auctions_spec_class_name'];
                $new_spec1['name'] = $key;
                $new_spec1['value'] = $value;
                $new_spec[] = $new_spec1;
            }
        } else {
            $new_spec = [];
        }
        $auction_detail['auctions_spec'] = $new_spec;
        //专场推荐
        /** @var auction_specialModel $model_special */
        $model_special = Model('auction_special');
        $special_info = $model_special
            ->getSpecialInfo(['special_id' => $auction_detail['special_id']], $field = "special_id,special_name");
        $auction_detail['special_name'] = $special_info['special_name'];
        //获取作品分类名
        /** @var auctions_spec_classModel $category_model */
        $category_model = Model('auctions_spec_class');
        $category_info = $category_model->getOneById($auction_detail['gc_id']);
        $auction_detail['gc_name'] = isset($category_info['auctions_spec_class_name'])
            ? $category_info['auctions_spec_class_name'] : '';
        $auction_detail['interest_last_date'] = strtotime($auction_detail['interest_last_date']);
        $other_images = [];
        if (isset($auction_detail['other_images']) && !empty(isset($auction_detail['other_images']))) {
            $auction_detail['other_images'] = json_decode($auction_detail['other_images'], true);
            foreach ($auction_detail['other_images'] as $key => $value) {
                $other_images[] = cthumb($value,360);
            }
        }
        $auction_detail['other_images'] = $other_images;
        array_unshift($auction_detail['other_images'], $auction_detail['auction_image']);
        $result = $logic_auction->bidListFetch($auction_id,'*',  5);
        $bidList = isset($result['bid_log_list']) ? $result['bid_log_list'] : [];
        if (!empty($bidList)) {
            $bidList = array_map(function ($value) {
                $value['created_at'] = date('Y-m-d H:i:s', $value['created_at']);
                return $value;
            }, $bidList);
        }
        $bidNum = isset($result['count_rows']) ? $result['count_rows'] : 0;
        $auction_detail['bid_log_list'] = $bidList;
        $auction_detail['bid_num'] = $bidNum;
        if (!empty($member_id)) {
            //是否提示介绍拍乐赚
            /** @var \memberModel $model_member */
            $model_member = Model('member');
            $member_info = $model_member->getMemberInfoByID($member_id);
            if ($member_info['recommended'] == 0) {
                $auction_detail['recommended'] = 1;
            }
            //当前用户是否拍中该拍品
            /** @var bid_logModel $bidder_member_model */
            $bidder_member_model = Model('bid_log');
            if ($auction_detail['auction_end_true_t'] < time()) {
                $order = Model('orders');
                $order_fetch = $order->where(['buyer_id' => $member_id, 'auction_id' => $auction_id])->find();
                if (empty($order_fetch)) {
                    //显示当前最高出价人, 方便片品结束生成订单显示
                    $bidder_member_info = $bidder_member_model->where(['auction_id' => $auction_id])
                        ->order('offer_num desc')->find();
                    $bidder_member_id = isset($bidder_member_info['member_id'])
                        ? $bidder_member_info['member_id'] : 0;
                    if ($bidder_member_id == $member_id) {
                        $auction_detail['is_fetch'] = 1;
                    }
                } else {
                    $auction_detail['is_fetch'] = 1;
                    $auction_detail['order_id'] = $order_fetch['order_id'];
                    if ($order_fetch['order_state'] == 10 || $order_fetch['order_state'] == 0) {
                        $auction_detail['is_pay'] = 0;
                    } else {
                        $auction_detail['is_pay'] = 1;
                    }
                }
            }
            $relation_model = Model('auction_member_relation');
            $where = [
                'auction_id' => $auction_id,
                'member_id' => $member_id,
            ];
            $remind = $relation_model->where($where)->find();
            if ($remind) {
                if ($remind['is_remind'] == 0) {
                    $auction_detail['is_mind'] = 0;
                } else {
                    $auction_detail['is_mind'] = 1;
                }
            }

            $margin_orders = Model('margin_orders');
            $condition = [];
            //$auction_start_time = $auction_detail['auction_start_time'];
            $condition['auction_id'] = $auction_id;
            $condition['buyer_id'] = $member_id;
            $condition['order_state'] = 1; //订单已支付状态
            //$condition['updated_at'] = array('lt', $auction_start_time);
            $margin_orders_info = $margin_orders->where($condition)->sum('margin_amount');
            $auction_detail['margin_money'] = $margin_orders_info ?: 0;
            $member_commission_model = Model('member_commission');
            $condition = [];
            $condition['goods_id'] = $auction_id;
            $condition['dis_member_id'] = $member_id;
            $condition['dis_commis_state'] = 1; //佣金已结算
            $condition['commission_type'] = 7; //竞拍返佣
            //TODO 竞拍返佣总金额计算
            $commission_amount = $member_commission_model->where($condition)->sum('commission_amount');
            $auction_detail['commission_amount'] = $commission_amount ?: 0;
            //用户最后出价时间
            $condition = [
                'member_id' => $member_id,
                'auction_id' => $auction_id
            ];
            $bid_info = $bidder_member_model->where($condition)->order('created_at desc')->find();
            if ($bid_info) {
                $auction_detail['last_offer_time'] = $bid_info['created_at'] ?: 0;
            }
        }
        $auctionInfo = [
            'auction_name' => '紫砂壶套装',          //拍品名称
            'current_price' => 0.00,                //当前价格
            'auction_reference_price' => 198.00,    //参考价格
            'special_name' => '紫砂壶专场',          //专场名称
            'auction_start_price' => 0.00,        //起拍价
            'auction_increase_range' => 10.00,     //加浮价
            'add_time' => 5,                   //竞拍延时时长
            'commission_amount' => 0.00,          //竞价奖励金
            'margin_money' => 0.00               //已交保证金
        ];
        $auction_detail['add_time'] = 5;
        return $this->_json($auction_detail);
    }

    /**
     * @SWG\Post(path="/api/NewBie/createNewBieAuctionOrder",
     *   tags={"newBie"},
     *   operationId="/api/NewBie/createNewBieAuctionOrder",
     *   summary="新手体验最后生成订单接口",
     *   description="新手体验最后生成订单接口",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="redPacketId",
     *     in="formData",
     *     description="红包-代金券id",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *  @SWG\Response(response="200", description="['order_id' => 1132]"))
     * )
     *
     */
    /*
     * 生成新手专区的拍卖订单
     */
    public function createNewBieAuctionOrder(Request $request)
    {
        application()->orderApi;
        $key = $request->key;
        $redPacketId = $request->redPacketId;
        $auctionId = C('newBieAuctionId');
        if (empty($redPacketId) || empty($auctionId)) {
            return $this->_jsonError(ApiResponseCode::ARG_INVALID);
        }
        $RedPacketApi = application()->redPacketApi;
        $memberId = application()->memberApi->getMemberIdIfExists($key);
        //$memberId = (new MemberApi())->getMemberIdIfExists($key);
        if (empty($memberId)) {
            return $this->_jsonError(ApiResponseCode::NOT_LOGIN);
        }
        //判断是否已进行新手体验 --  是否进行新手出价操作
        $bidLogApi = application()->bidLogApi;
        //$bidLogApi = new BidLogApi();
        $checkBidLog = $bidLogApi->isNewBie($memberId);
        if (!$checkBidLog) {
            return $this->_jsonError(ApiResponseCode::NEWBIE_BID_LOG);
        }
        $redPacketInfo = $RedPacketApi->getRedPacketInfo($redPacketId);
        if (empty($redPacketInfo)) {
            return $this->_jsonError(ApiResponseCode::NOT_DATA);
        }
        if ($redPacketInfo['rpacket_end_date'] < time()
            || $redPacketInfo['rpacket_state'] != ShopncRedpacket::RPACKET_STATE_UNUSED) {
            return $this->_jsonError(ApiResponseCode::RED_PACKET_NO_VALID);
        }
        $memberInfo = application()->memberApi->getMemberInfo($memberId);
        if (empty($auctionId)) {
            return $this->_jsonError(ApiResponseCode::ARG_INVALID);
        }
        $auctionInfo = application()->auctionApi->getAuctionInfo($auctionId);
        $auctionInfo['current_price'] = 20;
        if ($auctionInfo) {
            //去生成订单
            $orderInfo = application()->orderApi->_auctions_order($auctionInfo, $memberInfo);
        }
        if (empty($orderInfo['order_id'])) {
            return $this->_jsonError(ApiResponseCode::OPERATE_FAIL);
        }
        //生成保证金订单
        /** @var \auction_buyLogic  $logic_auction_buy*/
        $logic_auction_buy = Logic('auction_buy');
        $marginOrderCreate = $logic_auction_buy->createNewbieMargin($memberId, $redPacketId, $auctionId, $orderInfo['order_id']);
        if (!$marginOrderCreate) {
            yLog()->error($memberId . '生成新手保证金订单失败');
            return $this->_jsonError(ApiResponseCode::OPERATE_FAIL);
        }
        //代金券使用更改为已使用
        $redPacketWhere = [
            'rpacket_owner_id' => $memberId,
            'rpacket_id' => $redPacketId,
        ];
        $effectRow = $RedPacketApi->overdueRedPacket($redPacketWhere);
        if (!$effectRow) {
            yLog()->error($memberId.'用户的代金券红包,已使用状态未修改成功'.$redPacketId);
        }
        //用户竞拍返佣奖励添加
        /** @var \member_commissionModel $member_commission */
        $member_commission = Model('member_commission');
        //用户竞拍奖励金额
        $offerPrice = 20;
        $result = $member_commission->addWaitForCommission($memberId, $offerPrice, $auctionInfo);
        $person_num = isset($result['commission_num']) ? $result['commission_num'] : 0;
        if (empty($person_num)) {
            yLog()->error($memberId . '新手竞价返回未返现金额');
        }
        $person_num = $person_num ? $person_num : 0;
        //添加新手体验拍品出价记录
        $newbieInsert = [
            'member_id' => $memberId,
            'auction_id' => $auctionId,
            'created_at' => time(),
            'offer_num' => $offerPrice,
            'member_name' => $memberInfo['member_name'],
            'commission_amount' => $person_num,
            'bid_type' => ShopncBidLog::BID_TYPE_EXP,
        ];
        $bidLog = $bidLogApi->insertNewbieBidLog($newbieInsert);
        if (empty($bidLog)) {
            ylog()->error($memberId.'新手体验竞价日志添加失败');
        }
        return $this->_json(['order_id' => $orderInfo['order_id'], 'auction_id' => $auctionId]);
    }
}
