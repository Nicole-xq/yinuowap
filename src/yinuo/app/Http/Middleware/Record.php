<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
//use Galaxy\Framework\Contracts\Service\Log;

class Record {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //Log::info('api_request_original_data',['request_key'=>array_keys($_REQUEST), 'header'=>$request->headers->all()]);
        if(!isProduction()){
            //Log::info('api_request_decrypt_data_debug', ['_POST'=>$_POST, '_header'=>$request->headers->all(), '_GET'=>$_GET]);
        }
        return $next($request);
    }
}