<?php
namespace App\Http\Middleware\AppUser;

use App\Exceptions\ApiResponseException;
use App\Services\Constant\ApiResponseCode;
use Closure;

/**
 * 限制必须登录的中间件
 * Class AppUserLimitMiddleware
 * @package App\Http\Middleware\AppUser
 */
class AppUserLimitMiddleware{

    public function handle($request, Closure $next)
    {
        if(!application()->appUser->checkLogin()){
            ApiResponseException::throwException(ApiResponseCode::NOT_LOGIN);
        }
        return $next($request);
    }
}