<?php
namespace App\Http\Middleware\AppUser;

use Closure;

/**
 * 需要登录信息初始化的中间件
 * Class AppUserInitMiddleware
 * @package App\Http\Middleware\AppUser
 */
class AppUserInitMiddleware{

    public function handle($request, Closure $next)
    {
        $key = $request->input('key');
        if(!empty($key)){
            application()->appUser->init($key);
        }
        return $next($request);
    }
}