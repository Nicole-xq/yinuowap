<?php
namespace App\Http\Middleware;

use App\Lib\Helpers\ResponseHelper;
//use Galaxy\Framework\Contracts\Service\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * 开启跨域支持中间件
 * 后置中间件只有在正常响应时才会被追加响应头，而如果出现异常，这时响应是不会经过中间件的
 * Class EnableCrossRequestMiddleware
 * @package App\Http\Middleware
 */
class EnableCrossRequestMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        $referer = $request->header('origin');
        //是否跨域
        if(!ResponseHelper::isCrossAllow($referer)){
            throw new AccessDeniedHttpException("Not Allow Origin:".$referer);
        }
        $response = $next($request);
        if($referer){
            ResponseHelper::appendCrossResponse($response);
        }
        return $response;
    }
}