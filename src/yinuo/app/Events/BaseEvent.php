<?php
namespace App\Events;
use App\Lib\Helpers\LogHelper;
//use Galaxy\Framework\Contracts\Service\Log;

class BaseEvent
{
    public static function dispatching(...$args){
        try{
            event(new static(...$args));
            return true;
        }catch (\Exception $e){
            \Log::error('sendEvent_error',[static::class, LogHelper::getEInfo($e)]);
            return false;
        }

    }
}
