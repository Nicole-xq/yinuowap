<?php
namespace App\Events;

class LoginFailEvent extends BaseEvent
{
    public $ip;
    public $userName;

    /**
     * Create a new event instance.
     * @param string $ip
     * @param string $userName
     */
    public function __construct($ip = '', $userName = '')
    {
        $this->ip = $ip;
        $this->userName = $userName;
    }
}
