<?php

namespace App\Events;

use App\Lib\Helpers\AppHelper;
use Illuminate\Http\Request;

class AdminOperateEvent extends BaseEvent
{
    public $actionName;
    public $param = NULL;
    public $request;
    public $operateUserId;
    public $ip = '';

    /**
     * Create a new event instance.
     *
     * @param $operateUserId
     * @param $actionName
     * @param array $param
     * @param Request | null $request
     */
    public function __construct($operateUserId = NULL, $param = NULL, $request = NULL, $actionName = NULL)
    {
        $this->operateUserId = $operateUserId;
        if($this->operateUserId === NULL){
            $this->operateUserId = adminGuard()->user() ? adminGuard()->user()->id : 0;
        }
        $this->actionName = $actionName;
        if($this->actionName === NULL){
            $this->actionName = AppHelper::getPermissionName();
        }
        $this->param = $param;
        $this->request = $request;
        if(!$this->request){
            $this->request = app('request');
        }
        $this->ip = $this->request->ip();
    }
}
