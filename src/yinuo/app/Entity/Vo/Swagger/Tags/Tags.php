<?php namespace App\Entity\Vo\Swagger\Tags;

/**
 * @SWG\Tag(
 *   name="newBie",
 *   description="新手专区"
 * )
 * @SWG\Tag(
 *   name="ranking",
 *   description="排行榜"
 * )
 * @SWG\Tag(
 *   name="auction",
 *   description="拍卖相关"
 * )
 * @SWG\Tag(
 *   name="yinuo",
 *   description="个人中心"
 * )
 */