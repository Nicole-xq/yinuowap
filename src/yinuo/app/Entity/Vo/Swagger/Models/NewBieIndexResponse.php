<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="NewBieIndexResponse"))
 */
class NewBieIndexResponse
{

    /**
     * @SWG\Property(example="剩余体验时间|integer(100000)")
     * @var integer 剩余体验时间
     */
    public $timeRemaining;

    /**
     * @SWG\Property(example="代金券总金额|double(180.1)")
     * @var double 代金券总金额
     */
    public $voucherMoney;

    /**
     * @SWG\Property(example="代金券剩余个数|integer(3)")
     * @var integer 代金券剩余个数
     */
    public $voucherCount;

    /**
     * @SWG\Property(example="是否打开全列表,自动领取后弹出|boolean(false)")
     * @var boolean 是否打开全列表,自动领取后弹出
     */
    public $isOpenVoucherList;
    /**
     * @SWG\Property(example="是否是新手|boolean(false)")
     * @var boolean 是否是新手
     */
    public $isNewBie;
    /**
     * @SWG\Property(example="是否登录|boolean(false)")
     * @var boolean 是否登录
     */
    public $isLogin;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/NewBieIndexResponseSpecialsResponse"))
     * @var array 专场矩阵
     */
    public $specials;
}
/**
 * @SWG\Definition(@SWG\Xml(name="NewBieIndexResponseSpecialsResponse"))
 */
class NewBieIndexResponseSpecialsResponse
{

    /**
     * @SWG\Property(example="开始时间|integer(1554814292)")
     * @var integer 开始时间
     */
    public $startTime;

    /**
     * @SWG\Property(example="开始时间|string(09:00)")
     * @var string 开始时间
     */
    public $startTimeStr;

    /**
     * @SWG\Property(example="专场状态 11 预展开始 12专场开始 13 专场结束|integer(1)")
     * @var integer 专场状态 (20 审核成功 11 预展开始)即将开始 12专场开始 13 专场结束
     */
    public $status;

    /**
     * @SWG\Property(example="专场id|integer(1233)")
     * @var integer 专场id
     */
    public $specialId;

}