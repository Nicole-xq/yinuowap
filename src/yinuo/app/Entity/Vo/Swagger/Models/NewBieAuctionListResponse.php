<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="NewBieAuctionListResponse"))
 */
class NewBieAuctionListResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/NewBieAuctionListResponseListResponse"))
     * @var array
     */
    public $list;
    /**
     * @SWG\Property(example="count|integer(22)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="maxPage|integer(22)")
     * @var integer
     */
    public $maxPage;

    /**
     * @SWG\Property(example="nowPage|string(1)")
     * @var string
     */
    public $nowPage;

}
/**
 * @SWG\Definition(@SWG\Xml(name="NewBieAuctionListResponseListResponse"))
 */
class NewBieAuctionListResponseListResponse
{

    /**
     * @SWG\Property(example="拍品id|integer(2404)")
     * @var integer 拍品id
     */
    public $auction_id;

    /**
     * @SWG\Property(example="商品id|integer(107087)")
     * @var integer 商品id
     */
    public $goods_id;

    /**
     * @SWG\Property(example="拍品名称|string(《元曲风韵》之五)")
     * @var string 拍品名称
     */
    public $auction_name;

    /**
     * SWG\Property(example="auction_type|integer(0)")
     * @var integer
     */
    public $auction_type;

    /**
     * SWG\Property(example="store_id|integer(14)")
     * @var integer
     */
    public $store_id;

    /**
     * SWG\Property(example="store_name|string(赵长国艺术馆)")
     * @var string
     */
    public $store_name;

    /**
     * SWG\Property(example="special_id|integer(225)")
     * @var integer
     */
    public $special_id;

    /**
     * SWG\Property(example="auction_add_time|integer(1510318960)")
     * @var integer
     */
    public $auction_add_time;

    /**
     * SWG\Property(example="auction_edit_time|integer(1521704784)")
     * @var integer
     */
    public $auction_edit_time;

    /**
     * @SWG\Property(example="预展开始时间|integer(1510218000)")
     * @var integer 预展开始时间
     */
    public $auction_preview_start;

    /**
     * @SWG\Property(example="开始拍卖时间|integer(1510477200)")
     * @var integer 开始拍卖时间
     */
    public $auction_start_time;

    /**
     * @SWG\Property(example="结束拍卖时间|integer(1510650000)")
     * @var integer 结束拍卖时间
     */
    public $auction_end_time;

    /**
     * SWG\Property(example="auction_end_true_t|integer(1510650000)")
     * @var integer
     */
    public $auction_end_true_t;

    /**
     * @SWG\Property(example="拍品主图|string(14_05568249802951820.jpg)")
     * @var string 拍品主图
     */
    public $auction_image;

    /**
     * SWG\Property(example="auction_video|NULL()")
     * @var NULL
     */
    public $auction_video;

    /**
     * SWG\Property(example="auction_bond|string(1000.00)")
     * @var string
     */
    public $auction_bond;

    /**
     * @SWG\Property(example="起拍价|string(1.00)")
     * @var string 起拍价
     */
    public $auction_start_price;

    /**
     * SWG\Property(example="auction_reference_price|string(0.00)")
     * @var string
     */
    public $auction_reference_price;

    /**
     * SWG\Property(example="auction_increase_range|string(800.00)")
     * @var string
     */
    public $auction_increase_range;

    /**
     * SWG\Property(example="auction_reserve_price|string(10000.00)")
     * @var string
     */
    public $auction_reserve_price;

    /**
     * SWG\Property(example="delivery_mechanism|string(赵长国艺术馆)")
     * @var string
     */
    public $delivery_mechanism;

    /**
     * @SWG\Property(example="拍卖状态细化: 1.拍卖结束 2.预展中 3.拍卖中|integer(1)")
     * @var integer 拍卖状态细化: 1.拍卖结束 2.预展中 3.拍卖中
     */
    public $status;

    /**
     * SWG\Property(example="gc_id|NULL()")
     * @var NULL
     */
    public $gc_id;

    /**
     * SWG\Property(example="auction_lock|integer(1)")
     * @var integer
     */
    public $auction_lock;

    /**
     * @SWG\Property(example="热度 拍品点击数量|integer(345)")
     * @var integer 热度 拍品点击数量
     */
    public $auction_click;

    /**
     * SWG\Property(example="is_liupai|integer(0)")
     * @var integer
     */
    public $is_liupai;

    /**
     * @SWG\Property(example="当前价|string(9600.00)")
     * @var string 当前价
     */
    public $current_price;

    /**
     * @SWG\Property(example="中拍价 如果有就中拍了,没有就流拍了|string(9600.00)")
     * @var string 中拍价 如果有就中拍了,没有就流拍了
     */
    public $at_last_price;

    /**
     * SWG\Property(example="bid_number|integer(8)")
     * @var integer
     */
    public $bid_number;

    /**
     * SWG\Property(example="num_of_applicants|integer(4)")
     * @var integer
     */
    public $num_of_applicants;

    /**
     * @SWG\Property(example="真实参与人数|integer(0)")
     * @var integer 真实参与人数
     */
    public $real_participants;

    /**
     * SWG\Property(example="set_reminders_num|integer(0)")
     * @var integer
     */
    public $set_reminders_num;

    /**
     * SWG\Property(example="auction_collect|integer(0)")
     * @var integer
     */
    public $auction_collect;

    /**
     * SWG\Property(example="recommended|integer(0)")
     * @var integer
     */
    public $recommended;

    /**
     * SWG\Property(example="current_price_time|integer(1510542460)")
     * @var integer
     */
    public $current_price_time;

    /**
     * SWG\Property(example="store_vendue_id|NULL()")
     * @var NULL
     */
    public $store_vendue_id;

    /**
     * SWG\Property(example="auction_bond_rate|string(0.00)")
     * @var string
     */
    public $auction_bond_rate;

    /**
     * SWG\Property(example="interest_last_date|string(0000-00-00 00:00:00)")
     * @var string
     */
    public $interest_last_date;

    /**
     * SWG\Property(example="auctions_artist_id|integer(0)")
     * @var integer
     */
    public $auctions_artist_id;

    /**
     * SWG\Property(example="auctions_artist_name|NULL()")
     * @var NULL
     */
    public $auctions_artist_name;

    /**
     * SWG\Property(example="auctions_class_lv1|integer(0)")
     * @var integer
     */
    public $auctions_class_lv1;

    /**
     * SWG\Property(example="auctions_class_lv2|integer(0)")
     * @var integer
     */
    public $auctions_class_lv2;

    /**
     * SWG\Property(example="auctions_spec|NULL()")
     * @var NULL
     */
    public $auctions_spec;

    /**
     * SWG\Property(example="auctions_long|integer(0)")
     * @var integer
     */
    public $auctions_long;

    /**
     * SWG\Property(example="auctions_width|integer(0)")
     * @var integer
     */
    public $auctions_width;

    /**
     * SWG\Property(example="auctions_height|integer(0)")
     * @var integer
     */
    public $auctions_height;

    /**
     * SWG\Property(example="create_age|NULL()")
     * @var NULL
     */
    public $create_age;

    /**
     * SWG\Property(example="auctions_summary|NULL()")
     * @var NULL
     */
    public $auctions_summary;

    /**
     * SWG\Property(example="other_images|NULL()")
     * @var NULL
     */
    public $other_images;

    /**
     * SWG\Property(example="share_content|NULL()")
     * @var NULL
     */
    public $share_content;

    /**
     * SWG\Property(example="duration_time|integer(0)")
     * @var integer
     */
    public $duration_time;

}