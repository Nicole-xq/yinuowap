<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;




/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponse"))
 */
class ShowAuctionOrderResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/ShowAuctionOrderResponseRedPacketListResponse"))
     * @var array 可用红包信息
     */
    public $redPacketList;
    /**
     * @SWG\Property(example="vat_deny|boolean()")
     * @var boolean
     */
    public $vat_deny;

    /**
     * @SWG\Property(example="vat_hash|string(pxCyTtwBVmtV7VpBYDGWOQj2BrE2TANDScd)")
     * @var string
     */
    public $vat_hash;

    /**
     * @SWG\Property(ref="#/definitions/ShowAuctionOrderResponseInv_infoResponse")
     * @var object
     */
    public $inv_info;

    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array
     */
    public $store_info;
    /**
     * @SWG\Property(ref="#/definitions/ShowAuctionOrderResponseAuction_order_infoResponse")
     * @var object
     */
    public $auction_order_info;

    /**
     * @SWG\Property(example="margin_order_info|NULL()")
     * @var NULL
     */
    public $margin_order_info;

    /**
     * @SWG\Property(ref="#/definitions/ShowAuctionOrderResponseAuction_infoResponse")
     * @var object
     */
    public $auction_info;

    /**
     * @SWG\Property(example="underline|boolean(1)")
     * @var boolean
     */
    public $underline;

    /**
     * @SWG\Property(example="margin_amount|string(405.00)")
     * @var string
     */
    public $margin_amount;

    /**
     * @SWG\Property(example="refuse_amount|integer(0)")
     * @var integer
     */
    public $refuse_amount;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponseRedPacketListResponse"))
 */
class ShowAuctionOrderResponseRedPacketListResponse
{

    /**
     * @SWG\Property(example="红包id|integer(69513)")
     * @var integer 红包id
     */
    public $rpacket_id;

    /**
     * @SWG\Property(example="红包标题|string(新人专享)")
     * @var string 红包标题
     */
    public $rpacket_title;

    /**
     * @SWG\Property(example="红包描述|string(结算抵扣红包)")
     * @var string 红包描述
     */
    public $rpacket_desc;

    /**
     * @SWG\Property(example="开始时间戳|integer(1555516800)")
     * @var integer 开始时间戳
     */
    public $rpacket_start_date;

    /**
     * @SWG\Property(example="结束时间戳|integer(1559318399)")
     * @var integer 结束时间戳
     */
    public $rpacket_end_date;

    /**
     * @SWG\Property(example="红包金额|integer(100)")
     * @var integer 红包金额
     */
    public $rpacket_price;

    /**
     * @SWG\Property(example="满多少可用|string(100.00)")
     * @var string 满多少可用
     */
    public $rpacket_limit;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponseInv_infoResponse"))
 */
class ShowAuctionOrderResponseInv_infoResponse
{

    /**
     * @SWG\Property(example="content|string(不需要发票)")
     * @var string
     */
    public $content;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponseAuction_order_infoResponseAddress_infoResponse"))
 */
class ShowAuctionOrderResponseAuction_order_infoResponseAddress_infoResponse
{

    /**
     * @SWG\Property(example="address_id|string(576)")
     * @var string
     */
    public $address_id;

    /**
     * @SWG\Property(example="member_id|string(65332)")
     * @var string
     */
    public $member_id;

    /**
     * @SWG\Property(example="true_name|string(新增地址002)")
     * @var string
     */
    public $true_name;

    /**
     * @SWG\Property(example="area_id|string(56)")
     * @var string
     */
    public $area_id;

    /**
     * @SWG\Property(example="city_id|string(40)")
     * @var string
     */
    public $city_id;

    /**
     * @SWG\Property(example="area_info|string(天津 天津市 河东区)")
     * @var string
     */
    public $area_info;

    /**
     * @SWG\Property(example="address|string(巨蟹座)")
     * @var string
     */
    public $address;

    /**
     * @SWG\Property(example="tel_phone|NULL()")
     * @var NULL
     */
    public $tel_phone;

    /**
     * @SWG\Property(example="mob_phone|string(158865865558)")
     * @var string
     */
    public $mob_phone;

    /**
     * @SWG\Property(example="is_default|string(1)")
     * @var string
     */
    public $is_default;

    /**
     * @SWG\Property(example="dlyp_id|string(0)")
     * @var string
     */
    public $dlyp_id;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponseAuction_order_infoResponse"))
 */
class ShowAuctionOrderResponseAuction_order_infoResponse
{

    /**
     * @SWG\Property(example="order_id|string(1884)")
     * @var string
     */
    public $order_id;

    /**
     * @SWG\Property(example="order_sn|string(2000000004698101)")
     * @var string
     */
    public $order_sn;

    /**
     * @SWG\Property(example="pay_sn|string(660608751361672332)")
     * @var string
     */
    public $pay_sn;

    /**
     * @SWG\Property(example="pay_sn1|NULL()")
     * @var NULL
     */
    public $pay_sn1;

    /**
     * @SWG\Property(example="store_id|string(0)")
     * @var string
     */
    public $store_id;

    /**
     * @SWG\Property(example="store_name|string(a6)")
     * @var string
     */
    public $store_name;

    /**
     * @SWG\Property(example="buyer_id|string(65332)")
     * @var string
     */
    public $buyer_id;

    /**
     * @SWG\Property(example="buyer_name|string(18010000010)")
     * @var string
     */
    public $buyer_name;

    /**
     * @SWG\Property(example="buyer_email|NULL()")
     * @var NULL
     */
    public $buyer_email;

    /**
     * @SWG\Property(example="buyer_phone|string(0)")
     * @var string
     */
    public $buyer_phone;

    /**
     * @SWG\Property(example="add_time|string(1555407361)")
     * @var string
     */
    public $add_time;

    /**
     * @SWG\Property(example="payment_code|string(online)")
     * @var string
     */
    public $payment_code;

    /**
     * @SWG\Property(example="payment_time|string(0)")
     * @var string
     */
    public $payment_time;

    /**
     * @SWG\Property(example="finnshed_time|string(0)")
     * @var string
     */
    public $finnshed_time;

    /**
     * @SWG\Property(example="goods_amount|string(650.00)")
     * @var string
     */
    public $goods_amount;

    /**
     * @SWG\Property(example="order_amount|string(650.00)")
     * @var string
     */
    public $order_amount;

    /**
     * @SWG\Property(example="rcb_amount|string(0.00)")
     * @var string
     */
    public $rcb_amount;

    /**
     * @SWG\Property(example="pd_amount|string(0.00)")
     * @var string
     */
    public $pd_amount;

    /**
     * @SWG\Property(example="shipping_fee|string(0.00)")
     * @var string
     */
    public $shipping_fee;

    /**
     * @SWG\Property(example="evaluation_state|string(0)")
     * @var string
     */
    public $evaluation_state;

    /**
     * @SWG\Property(example="evaluation_again_state|string(0)")
     * @var string
     */
    public $evaluation_again_state;

    /**
     * @SWG\Property(example="order_state|string(10)")
     * @var string
     */
    public $order_state;

    /**
     * @SWG\Property(example="refund_state|string(0)")
     * @var string
     */
    public $refund_state;

    /**
     * @SWG\Property(example="lock_state|string(0)")
     * @var string
     */
    public $lock_state;

    /**
     * @SWG\Property(example="delete_state|string(0)")
     * @var string
     */
    public $delete_state;

    /**
     * @SWG\Property(example="refund_amount|string(0.00)")
     * @var string
     */
    public $refund_amount;

    /**
     * @SWG\Property(example="delay_time|string(0)")
     * @var string
     */
    public $delay_time;

    /**
     * @SWG\Property(example="order_from|string(2)")
     * @var string
     */
    public $order_from;

    /**
     * @SWG\Property(example="shipping_code|string()")
     * @var string
     */
    public $shipping_code;

    /**
     * @SWG\Property(example="order_type|string(4)")
     * @var string
     */
    public $order_type;

    /**
     * @SWG\Property(example="api_pay_time|string(0)")
     * @var string
     */
    public $api_pay_time;

    /**
     * @SWG\Property(example="api_pay_amount|string(0.00)")
     * @var string
     */
    public $api_pay_amount;

    /**
     * @SWG\Property(example="chain_id|string(0)")
     * @var string
     */
    public $chain_id;

    /**
     * @SWG\Property(example="chain_code|string(0)")
     * @var string
     */
    public $chain_code;

    /**
     * @SWG\Property(example="rpt_amount|string(0.00)")
     * @var string
     */
    public $rpt_amount;

    /**
     * @SWG\Property(example="trade_no|NULL()")
     * @var NULL
     */
    public $trade_no;

    /**
     * @SWG\Property(example="is_dis|string(0)")
     * @var string
     */
    public $is_dis;

    /**
     * @SWG\Property(example="pay_voucher|string()")
     * @var string
     */
    public $pay_voucher;

    /**
     * @SWG\Property(example="is_points|string(0)")
     * @var string
     */
    public $is_points;

    /**
     * @SWG\Property(example="points_amount|string(0.00)")
     * @var string
     */
    public $points_amount;

    /**
     * @SWG\Property(example="margin_amount|string(405.00)")
     * @var string
     */
    public $margin_amount;

    /**
     * @SWG\Property(example="gs_id|string(0)")
     * @var string
     */
    public $gs_id;

    /**
     * @SWG\Property(example="commission_state|string(0)")
     * @var string
     */
    public $commission_state;

    /**
     * @SWG\Property(example="auction_id|string(4452)")
     * @var string
     */
    public $auction_id;

    /**
     * @SWG\Property(example="notice_state|string(1)")
     * @var string
     */
    public $notice_state;

    /**
     * @SWG\Property(example="artist_closing_status|string(0)")
     * @var string
     */
    public $artist_closing_status;

    /**
     * @SWG\Property(example="artist_closing_time|NULL()")
     * @var NULL
     */
    public $artist_closing_time;

    /**
     * @SWG\Property(example="order_amount_original|NULL()")
     * @var NULL
     */
    public $order_amount_original;

    /**
     * @SWG\Property(example="artist_closing_no|NULL()")
     * @var NULL
     */
    public $artist_closing_no;

    /**
     * @SWG\Property(example="artist_closing_amount|NULL()")
     * @var NULL
     */
    public $artist_closing_amount;

    /**
     * @SWG\Property(example="artist_id|NULL()")
     * @var NULL
     */
    public $artist_id;

    /**
     * @SWG\Property(example="state_desc|string(待付款)")
     * @var string
     */
    public $state_desc;

    /**
     * @SWG\Property(example="payment_name|string(在线付款)")
     * @var string
     */
    public $payment_name;

    /**
     * @SWG\Property(example="pay_amount|integer(245)")
     * @var integer
     */
    public $pay_amount;

    /**
     * @SWG\Property(ref="#/definitions/ShowAuctionOrderResponseAuction_order_infoResponseAddress_infoResponse")
     * @var object
     */
    public $address_info;

    /**
     * @SWG\Property(example="wait_amount|integer(0)")
     * @var integer
     */
    public $wait_amount;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ShowAuctionOrderResponseAuction_infoResponse"))
 */
class ShowAuctionOrderResponseAuction_infoResponse
{

    /**
     * @SWG\Property(example="auction_id|string(4452)")
     * @var string
     */
    public $auction_id;

    /**
     * @SWG\Property(example="goods_id|string(0)")
     * @var string
     */
    public $goods_id;

    /**
     * @SWG\Property(example="goods_common_id|string(0)")
     * @var string
     */
    public $goods_common_id;

    /**
     * @SWG\Property(example="auction_name|string(20190416号006拍品)")
     * @var string
     */
    public $auction_name;

    /**
     * @SWG\Property(example="auction_type|string(2)")
     * @var string
     */
    public $auction_type;

    /**
     * @SWG\Property(example="store_id|string(0)")
     * @var string
     */
    public $store_id;

    /**
     * @SWG\Property(example="store_name|string(a6)")
     * @var string
     */
    public $store_name;

    /**
     * @SWG\Property(example="special_id|string(1052)")
     * @var string
     */
    public $special_id;

    /**
     * @SWG\Property(example="auction_add_time|string(1555404033)")
     * @var string
     */
    public $auction_add_time;

    /**
     * @SWG\Property(example="auction_edit_time|string(1555407207)")
     * @var string
     */
    public $auction_edit_time;

    /**
     * @SWG\Property(example="auction_preview_start|string(1555404332)")
     * @var string
     */
    public $auction_preview_start;

    /**
     * @SWG\Property(example="auction_start_time|string(1555404635)")
     * @var string
     */
    public $auction_start_time;

    /**
     * @SWG\Property(example="auction_end_time|string(1555407320)")
     * @var string
     */
    public $auction_end_time;

    /**
     * @SWG\Property(example="auction_end_true_t|string(1555407320)")
     * @var string
     */
    public $auction_end_true_t;

    /**
     * @SWG\Property(example="auction_image|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/0/0_06087480335367259.jpg?x-oss-process=style/240)")
     * @var string
     */
    public $auction_image;

    /**
     * @SWG\Property(example="auction_video|NULL()")
     * @var NULL
     */
    public $auction_video;

    /**
     * @SWG\Property(example="auction_bond|string(100.00)")
     * @var string
     */
    public $auction_bond;

    /**
     * @SWG\Property(example="auction_start_price|string(10.00)")
     * @var string
     */
    public $auction_start_price;

    /**
     * @SWG\Property(example="auction_reference_price|string(240.00)")
     * @var string
     */
    public $auction_reference_price;

    /**
     * @SWG\Property(example="auction_increase_range|string(20.00)")
     * @var string
     */
    public $auction_increase_range;

    /**
     * @SWG\Property(example="auction_reserve_price|string(300.00)")
     * @var string
     */
    public $auction_reserve_price;

    /**
     * @SWG\Property(example="delivery_mechanism|string(q6)")
     * @var string
     */
    public $delivery_mechanism;

    /**
     * @SWG\Property(example="state|string(1)")
     * @var string
     */
    public $state;

    /**
     * @SWG\Property(example="gc_id|NULL()")
     * @var NULL
     */
    public $gc_id;

    /**
     * @SWG\Property(example="auction_lock|string(0)")
     * @var string
     */
    public $auction_lock;

    /**
     * @SWG\Property(example="auction_click|string(612)")
     * @var string
     */
    public $auction_click;

    /**
     * @SWG\Property(example="is_liupai|string(0)")
     * @var string
     */
    public $is_liupai;

    /**
     * @SWG\Property(example="current_price|string(650.00)")
     * @var string
     */
    public $current_price;

    /**
     * @SWG\Property(example="bid_number|string(34)")
     * @var string
     */
    public $bid_number;

    /**
     * @SWG\Property(example="num_of_applicants|string(29)")
     * @var string
     */
    public $num_of_applicants;

    /**
     * @SWG\Property(example="real_participants|string(7)")
     * @var string
     */
    public $real_participants;

    /**
     * @SWG\Property(example="set_reminders_num|string(42)")
     * @var string
     */
    public $set_reminders_num;

    /**
     * @SWG\Property(example="auction_collect|string(46)")
     * @var string
     */
    public $auction_collect;

    /**
     * @SWG\Property(example="recommended|string(0)")
     * @var string
     */
    public $recommended;

    /**
     * @SWG\Property(example="current_price_time|string(1555406720)")
     * @var string
     */
    public $current_price_time;

    /**
     * @SWG\Property(example="store_vendue_id|NULL()")
     * @var NULL
     */
    public $store_vendue_id;

    /**
     * @SWG\Property(example="auction_bond_rate|string(0.00)")
     * @var string
     */
    public $auction_bond_rate;

    /**
     * @SWG\Property(example="interest_last_date|string(1970-01-01 08:00:00)")
     * @var string
     */
    public $interest_last_date;

    /**
     * @SWG\Property(example="auctions_artist_id|string(0)")
     * @var string
     */
    public $auctions_artist_id;

    /**
     * @SWG\Property(example="auctions_artist_name|NULL()")
     * @var NULL
     */
    public $auctions_artist_name;

    /**
     * @SWG\Property(example="auctions_class_lv1|string(26)")
     * @var string
     */
    public $auctions_class_lv1;

    /**
     * @SWG\Property(example="auctions_class_lv2|string(31)")
     * @var string
     */
    public $auctions_class_lv2;

    /**
     * @SWG\Property(example="auctions_spec|string({"16":"33","18":"41"})")
     * @var string
     */
    public $auctions_spec;

    /**
     * @SWG\Property(example="auctions_long|string(61)")
     * @var string
     */
    public $auctions_long;

    /**
     * @SWG\Property(example="auctions_width|string(62)")
     * @var string
     */
    public $auctions_width;

    /**
     * @SWG\Property(example="auctions_height|string(63)")
     * @var string
     */
    public $auctions_height;

    /**
     * @SWG\Property(example="create_age|string(2061)")
     * @var string
     */
    public $create_age;

    /**
     * @SWG\Property(example="auctions_summary|string(11)")
     * @var string
     */
    public $auctions_summary;

    /**
     * @SWG\Property(example="other_images|string(null)")
     * @var string
     */
    public $other_images;

    /**
     * @SWG\Property(example="share_content|string(22)")
     * @var string
     */
    public $share_content;

    /**
     * @SWG\Property(example="duration_time|string(10)")
     * @var string
     */
    public $duration_time;

    /**
     * @SWG\Property(example="image_60_url|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/0/0_06087480335367259.jpg?x-oss-process=style/240)")
     * @var string
     */
    public $image_60_url;

    /**
     * @SWG\Property(example="image_240_url|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/0/0_06087480335367259.jpg?x-oss-process=style/240)")
     * @var string
     */
    public $image_240_url;

    /**
     * @SWG\Property(example="auction_url|string(http://dev.huang.yinuovip.com/auction/index.php?act=auctions&op=index&id=4452)")
     * @var string
     */
    public $auction_url;

}