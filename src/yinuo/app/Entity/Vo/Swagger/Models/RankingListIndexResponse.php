<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;




/**
 * @SWG\Definition(@SWG\Xml(name="RankingListIndexResponse"))
 */
class RankingListIndexResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/RankingListIndexResponseListResponse"))
     * @var array 排行榜
     */
    public $list;
    /**
     * @SWG\Property(ref="#/definitions/RankingListIndexResponseNowUserInfoResponse")
     * @var object 当期用户排行信息
     */
    public $nowUserInfo;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/RankingListIndexResponseRankingHistoryResponse"))
     * @var array 历史排行榜
     */
    public $rankingHistory;


    /**
     * @SWG\Property(example="当期结束剩余时间(秒)|integer(551513)")
     * @var int 当期结束剩余时间(秒)
     */
    public $periodResidueTime;

}
/**
 * @SWG\Definition(@SWG\Xml(name="RankingListIndexResponseListResponse"))
 */
class RankingListIndexResponseListResponse
{

    /**
     * @SWG\Property(example="期数|integer(1)")
     * @var integer 期数
     */
    public $period_num;

    /**
     * @SWG\Property(example="用户名|string(第一名)")
     * @var string 用户名
     */
    public $member_name;

    /**
     * @SWG\Property(example="用户头像|string(https://ynysimg.oss-cn-beijing.aliyuncs.com/head_img/avatar_default.jpg)")
     * @var string 用户头像
     */
    public $member_avatar;

    /**
     * @SWG\Property(example="排名|integer(1)")
     * @var integer 排名
     */
    public $ranking_num;

    /**
     * @SWG\Property(example="邀请人数|integer(7)")
     * @var integer 邀请人数
     */
    public $recommend_num;

}
/**
 * @SWG\Definition(@SWG\Xml(name="RankingListIndexResponseNowUserInfoResponse"))
 */
class RankingListIndexResponseNowUserInfoResponse
{

    /**
     * @SWG\Property(example="用户头像|string(http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKlUrGOYdvIYoljUQicWLzCG8qELtcF7gXZFalmGTRYaLDFnhwnTYu0H9dzibuMDOrYXvDVmbs0PFog/132)")
     * @var string 用户头像
     */
    public $member_avatar;

    /**
     * @SWG\Property(example="邀请人数|integer(0)")
     * @var integer 邀请人数
     */
    public $recommend_num;

    /**
     * @SWG\Property(example="排名|integer(5)")
     * @var integer 排名
     */
    public $ranking_num;

    /**
     * @SWG\Property(example="用户名|string(13916994003)")
     * @var string 用户名
     */
    public $member_name;

}
/**
 * @SWG\Definition(@SWG\Xml(name="RankingListIndexResponseRankingHistoryResponse"))
 */
class RankingListIndexResponseRankingHistoryResponse
{

    /**
     * @SWG\Property(example="期数|integer(1)")
     * @var integer 期数
     */
    public $period_num;
    /**
     * @SWG\Property(example="期数字符串|integer(一)")
     * @var string 期数字符串
     */
    public $period_num_str;

    /**
     * @SWG\Property(example="开始时间|string(2019-04-08 00:00:00)")
     * @var string 开始时间
     */
    public $start_date;

    /**
     * @SWG\Property(example="结束时间|string(2019-04-14 18:43:06)")
     * @var string 结束时间
     */
    public $end_date;

}