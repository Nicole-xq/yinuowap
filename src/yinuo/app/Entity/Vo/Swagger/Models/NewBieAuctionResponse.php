<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="NewBieAuctionResponse"))
 */
class NewBieAuctionResponse
{

    /**
     * @SWG\Property(example="拍品名称|string(紫砂壶套装)")
     * @var string 拍品名称
     */
    public $auction_name;


    /**
     * @SWG\Property(example="当前价格|double(0.00)")
     * @var double 当前价格
     */
    public $current_price;



    /**
     * @SWG\Property(example="参考价格|double(198.00)")
     * @var string 参考价格
     */
    public $auction_reference_price;


    /**
     * @SWG\Property(example="专场名称|string(紫砂壶专场)")
     * @var string 专场名称
     */
    public $special_name;



    /**
     * @SWG\Property(example="起拍价|double(0.00)")
     * @var string 起拍价
     */
    public $auction_start_price;



    /**
     * @SWG\Property(example="加浮价|double(10.00)")
     * @var string 加浮价
     */
    public $auction_increase_range;



    /**
     * @SWG\Property(example="竞拍延时时长|integer(5)")
     * @var string 竞拍延时时长
     */
    public $add_time;

    /**
     * @SWG\Property(example="竞价奖励金|double(0.00)")
     * @var double 竞价奖励金
     */
    public $commission_amount;



    /**
     * @SWG\Property(example="已交保证金|double(0.00)")
     * @var double 已交保证金
     */
    public $margin_money;
}