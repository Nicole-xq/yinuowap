<?php
/**
 * User: Liu
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="RedPacketListResponse"))
 */
class RedPacketListResponse
{

    /**
     * @SWG\Property(example="红包编号|integer(9)")
     * @var string 红包编号
     */
    public $rpacket_id;


    /**
     * @SWG\Property(example="红包编码|string(757420551197708317)")
     * @var double 红包编码
     */
    public $rpacket_code;



    /**
     * @SWG\Property(example="红包标题|string(注册即送线上线下通用168红包)")
     * @var string 红包标题
     */
    public $rpacket_title;



    /**
     * @SWG\Property(example="红包描述|string(仅限尊享专区使用)")
     * @var string 红包描述
     */
    public $rpacket_desc;



    /**
     * @SWG\Property(example="红包有效期开始时间|string(1497801600)")
     * @var string 红包有效期开始时间
     */
    public $rpacket_start_date;



    /**
     * @SWG\Property(example="红包有效期结束时间|string(1506787199)")
     * @var string 红包有效期结束时间
     */
    public $rpacket_end_date;

    /**
     * @SWG\Property(example="红包面额|double(168)")
     * @var double 红包面额
     */
    public $rpacket_price;



    /**
     * @SWG\Property(example="红包使用时的订单限额|double(5000.00)")
     * @var double 红包使用时的订单限额
     */
    public $rpacket_limit;

    /**
     * @SWG\Property(example="红包状态(1-未用,2-已用,3-过期)|integer(1)")
     * @var double 红包状态(1-未用,2-已用,3-过期)
     */
    public $rpacket_state;

    /**
     * @SWG\Property(example="红包发放日期|string(1497853708)")
     * @var double 红包发放日期
     */
    public $rpacket_active_date;

    /**
     * @SWG\Property(example="红包所有者id|integer(317)")
     * @var double 红包所有者id
     */
    public $rpacket_owner_id;

    /**
     * @SWG\Property(example="红包所有者名称|string(johnny)")
     * @var double 红包所有者名称
     */
    public $rpacket_owner_name;

    /**
     * @SWG\Property(example="红包使用范围(1-全平台,2-指定店铺,3-新手专区)|integer(2)")
     * @var double 红包使用范围(1-全平台,2-指定店铺,3-新手专区)
     */
    public $rpacket_t_type;

    /**
     * @SWG\Property(example="红包类型, 0:普通红包,1:注册红包,2:保证金红包,3:结算红包|integer(2)")
     * @var double 红包类型, 0:普通红包,1:注册红包,2:保证金红包,3:结算红包
     */
    public $rpacket_t_special_type;
}