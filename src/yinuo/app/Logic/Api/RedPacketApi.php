<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/4/11 0011
 * Time: 14:41
 */

namespace App\Logic\Api;


use App\Lib\HelperFactory;
use App\Models\ShopncAuction;
use App\Models\ShopncMember;
use App\Models\ShopncRedpacket;
use App\Models\ShopncRedpacketTemplate;

class RedPacketApi extends BaseApi
{
    private $newbieRpacketTemplateWhere = [
        'rpacket_t_state' => ShopncRedpacketTemplate::RPACKET_T_STATE_VALID,       //红包模板有效
        'rpacket_t_type' => ShopncRedpacketTemplate::RPACKET_T_TYPE_NEWBIE,     //使用范围新手专区
        'rpacket_t_gettype' => ShopncRedpacketTemplate::RPACKET_T_GETTYPE_FREE,   //领取方式免费领取
    ];

    private $newbieRpacketWhere = [
        'rpacket_t_type' => ShopncRedpacketTemplate::RPACKET_T_TYPE_NEWBIE,     //使用范围新手专区
    ];

    protected $model;

    /**
     * @param $member_id
     * @return array
     */
    public function getRedPacketList($member_id)
    {
        $where = ['rpacket_owner_id' => $member_id];
        $redPacketList = ShopncRedpacket::where($where)->get();
        return $redPacketList ? $redPacketList->toArray() : [];
    }


    /**
     * 获得用户的新手红包
     * @param $member_id
     * @param array $addWhere
     * @param array $orderBy
     * @param array $columns
     * @return array
     */
    public function getNewBieRedPacketList($member_id, $addWhere = [], $orderBy = ["rpacket_price"=>"desc"], $columns = ['*'])
    {
        $redPacketList = $this->getNewBieRedPacketBuilder($member_id, $addWhere, $orderBy, $columns)->get($columns);
        return $redPacketList ? $redPacketList->toArray() : [];
    }

    /**
     * 获得用户的新手红包
     * @param $member_id
     * @param array $addWhere
     * @return int
     */
    public function getNewBieRedPacketCount($member_id, $addWhere = [])
    {
        return $this->getNewBieRedPacketBuilder($member_id, $addWhere)->count();
    }

    /**
     * 获得红包列表builder
     * @param $member_id
     * @param $addWhere
     * @param array $orderBy
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getNewBieRedPacketBuilder($member_id, $addWhere, $orderBy = ["rpacket_price"=>"desc"], $columns = ['*']){
        $where = $this->newbieRpacketWhere;
        $where['rpacket_owner_id'] = $member_id;
        $where = array_merge($where, $addWhere);
        $builder = ShopncRedpacket::query();
        HelperFactory::instance()->Model->whereBindQuery($builder, $where);
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $builder->orderBy($key, $item);
            }
        }
        return $builder;
    }

    public function getRedPacketListType($condition)
    {
        $redPacketList = ShopncRedpacket::where($condition)->get();
        return $redPacketList ? $redPacketList->toArray() : [];
    }

    /**
     * @param $member_id
     * @return array
     */
    public function getRedPacketExtend($where)
    {
        $redPacketList = ShopncRedpacketTemplate::where($where)->first();
        return $redPacketList ? $redPacketList->toArray() : [];
    }

    /**
     * 用户自动领取红包
     * @param $member_id
     * @return array|bool
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function autoGetRedPacket($member_id)
    {
        $checkBidLog = application()->bidLogApi->isNewBie($member_id);
        if (!$checkBidLog) {
            return false;
        }
        $memberInfo = ShopncMember::where(['member_id' => $member_id])->first();
        //获取可以领取的红包列表
        $redPacketList = $this->freeRedPacketList();
        if (empty($redPacketList)) {
            return $redPacketList;
        }
        $getRedPacketNum = 0;
        foreach ($redPacketList as $redPacket) {
            //效验红包是否可领取 已领取的红包个数
            $num = ShopncRedpacket::where(['rpacket_owner_id' => $member_id, 'rpacket_t_id' => $redPacket['rpacket_t_id']])->count();
            if (!empty($num) && $num >= $redPacket['rpacket_t_eachlimit']) {
                continue;
            }
            $getRedPacketNum += 1;
            $packetInsert = [];
            $packetInsert['rpacket_code'] = $this->get_rpt_code($member_id);
            $packetInsert['rpacket_t_id'] = $redPacket['rpacket_t_id'];
            $packetInsert['rpacket_title'] = $redPacket['rpacket_t_title'];
            $packetInsert['rpacket_desc'] = $redPacket['rpacket_t_desc'];
            $packetInsert['rpacket_start_date'] = $redPacket['rpacket_t_start_date'];
            $packetInsert['rpacket_end_date'] = $redPacket['rpacket_t_end_date'];
            $packetInsert['rpacket_price'] = $redPacket['rpacket_t_price'];
            $packetInsert['rpacket_limit'] = $redPacket['rpacket_t_limit'];
            $packetInsert['rpacket_state'] = ShopncRedpacket::RPACKET_STATE_UNUSED;      //红包未使用
            $packetInsert['rpacket_active_date'] = time();
            $packetInsert['rpacket_owner_id'] = $member_id;
            if ($redPacket['rpacket_t_type'] !== null) {
                $packetInsert['rpacket_t_type'] = $redPacket['rpacket_t_type'];       //红包类型(1-全平台,2-指定店铺,3-新手专区)
            }
            $packetInsert['rpacket_t_special_type'] = $redPacket['rpacket_t_special_type'];       //红包类型, 0:普通红包,1:注册红包,2:保证金红包,3:结算红包
            $packetInsert['rpacket_owner_name'] = $memberInfo['member_name'];
            $packetInsert['rpacket_customimg'] = $redPacket['rpacket_t_customimg'];
            $insertId = ShopncRedpacket::insertGetId($packetInsert);
            if (empty($insertId)) {
                yLog()->error('用户: ' . $member_id . '领取红包' . $redPacket['rpacket_t_id'] . '失败 !');
            }
        }
        if (empty($getRedPacketNum)) {
            return false;
        }
        return true;
    }


    /**
     * 获取新手专区可领取的红包列表
     * @return array
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function freeRedPacketList($fields = '*')
    {
        $freeRedPacket = ShopncRedpacketTemplate::where($this->newbieRpacketTemplateWhere)->select($fields)->get();
        return $freeRedPacket ? $freeRedPacket->toArray() : [];
    }

    /**
     * 获取新手专区可领取的红包总金额
     * @return mixed
     */
    public function freeRedPacketSum()
    {
        return ShopncRedpacketTemplate::where($this->newbieRpacketTemplateWhere)->sum('rpacket_t_price');
    }

    /*
     * 获取红包编码
     */
    public function get_rpt_code($member_id = 0)
    {
        static $num = 1;
        $sign_arr = array();
        $sign_arr[] = sprintf('%02d', mt_rand(10, 99));
        $sign_arr[] = sprintf('%03d', (float)microtime() * 1000);
        $sign_arr[] = sprintf('%010d', time() - 946656000);
        if ($member_id) {
            $sign_arr[] = sprintf('%03d', (int)$member_id % 1000);
        } else {
            //自增变量
            $tmpnum = 0;
            if ($num > 99) {
                $tmpnum = substr($num, -1, 2);
            } else {
                $tmpnum = $num;
            }
            $sign_arr[] = sprintf('%02d', $tmpnum);
            $sign_arr[] = mt_rand(1, 9);
        }
        $code = implode('', $sign_arr);
        $num += 1;
        return $code;
    }


    /**
     * 指定代金券过期操作
     * @param $condition
     * @return bool
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function overdueRedPacket($condition)
    {
        $overdueUpdate = [
            'rpacket_state' => ShopncRedpacket::RPACKET_STATE_USED
        ];
        return ShopncRedpacket::where($condition)->update($overdueUpdate);
    }
    
    /**
     * 获取红包代金券详情
     * @param $redPacketId
     * @return ShopncRedpacket|\Illuminate\Database\Eloquent\Model|null
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function getRedPacketInfo($redPacketId)
    {
        return ShopncRedpacket::where(['rpacket_id' => $redPacketId])->first();
    }

    /**
     * 获取红包代金券详情
     * @param $redPacketId
     * @return ShopncRedpacket|\Illuminate\Database\Eloquent\Model|null
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function updateRedPacketState($condition, $update)
    {
        return ShopncRedpacket::where($condition)->update($update);
    }

    /**
     * 确保支付金额满足红包限制金额
     * 判断该红包是否可用于当前拍品支付保证金
     * @param $auctionId
     * @param $redPacketId
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function checkMarginEnabled($auctionId, $redPacketId)
    {
        $return = [
            'state' => true,
            'data' => ''
        ];
        $auctionInfo = application()->auctionApi->getAuctionInfo($auctionId);
        if (empty($auctionInfo)) {
            $return['state'] = false;
            $return['data'] = '拍品不存在';
            return $return;
        }
        $redPacketInfo = $this->getRedPacketInfo($redPacketId);
        Model('auctions');
        Model('redpacket');
        if ($redPacketInfo['rpacket_t_type'] == \redpacketModel::REDPACKET_BEWBIE) {
            if ($auctionInfo['auction_type'] != \auctionsModel::AUCTION_TYPE_NEWBIE) {
                $return['state'] = false;
                $return['data'] = '红包仅限新手专区可用';
                return $return;
            }
        }
        //检测红包装填信息,通过返回红包模板信息
        $result = $this->checkRedPacket($redPacketInfo);
        if ($result['state'] === false) {
            return $result;
        }
        Model('margin_orders');
        $existWhere = [
            'auction_id' => $auctionId,
            'buyer_id' => $redPacketInfo['rpacket_owner_id'],
            'order_state' => \margin_ordersModel::ORDER_STATE_PREPAID
        ];
        $existWhere['red_packet_id'] = ['neq', 0];
        $packetExist = Model('margin_orders')->where($existWhere)->count();
        if ($packetExist > 1) {
            $return['state'] = false;
            $return['data'] = '同一个拍品只能使用一个红包';
            return $return;
        }
        $tplInfo = $result['data'];
        if ($auctionInfo['auction_type'] != ShopncAuction::AUCTION_TYPE_NEWBIE) {
            $return['state'] = false;
            $return['data'] = '非新手专区暂不支持红包';
            return $return;
        }
        if ($tplInfo['rpacket_t_special_type'] != ShopncRedpacketTemplate::RPACKET_T_SPECIAL_TYPE_MARGIN) {
            $return['state'] = false;
            $return['data'] = '该代金券只能用于保证金';
            return $return;
        }
        $return['data'] = $redPacketInfo;
        return $return;
    }

    /**
     * 检测红包装填信息,通过返回红包模板信息
     * @param $redPacketInfo
     * @return array
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function checkRedPacket($redPacketInfo)
    {
        $return = [
            'state' => true,
            'data' => ''
        ];
        if (empty($redPacketInfo)) {
            $return['state'] = false;
            $return['data'] = '代金券信息不存在';
            return $return;
        }
        if ($redPacketInfo['rpacket_state'] == ShopncRedpacket::RPACKET_STATE_UNUSED) {
            $return['state'] = false;
            $return['data'] = '代金券已使用';
            return $return;
        }
        if ($redPacketInfo['rpacket_end_date'] < time()) {
            $return['state'] = false;
            $return['data'] = '代金券已过期';
            return $return;
        }
        $tplWhere = ['rpacket_t_id' => $redPacketInfo['rpacket_t_id']];
        $tmlInfo = $this->getRedPacketExtend($tplWhere);
        if (empty($tmlInfo)) {
            $return['state'] = false;
            $return['data'] = '代金券扩展信息不存在';
            return $return;
        }
        $return['data'] = $tmlInfo;
        return $return;
    }

    /**
     * 红包代金券使用
     * @param $redPacketId
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function redPacketUsed($redPacketId)
    {
        if (empty($redPacketId)) {
            return false;
        }
        $update = ['rpacket_state' => ShopncRedpacket::RPACKET_STATE_USED];
        return ShopncRedpacket::where(['rpacket_id' => $redPacketId])->update($update);
    }

    /**
     * 红包代金券过期
     * @param $redPacketId
     * @Date: 2019/4/17 0017
     * @author: Mr.Liu
     */
    public function redPacketExpire($redPacketId)
    {
        if (empty($redPacketId)) {
            return false;
        }
        $update = ['rpacket_state' => ShopncRedpacket::RPACKET_STATE_EXPIRE];
        return ShopncRedpacket::where(['rpacket_id' => $redPacketId])->update($update);
    }
}