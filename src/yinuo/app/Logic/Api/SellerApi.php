<?php
namespace App\Logic\Api;

use App\Models\ShopncSeller;

/**
 * Class SellerApi
 * @package App\Logic\Api
 */
class SellerApi extends BaseApi {
    /**
     * 获得卖家信息
     * @param $memberId
     * @param array|string $column
     * @return ShopncSeller|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    function getSellerInfoByMemberId(
        $memberId,
        $column = ['*']
    ){
        return ShopncSeller::whereMemberId(intval($memberId))->first(is_array($column) ? $column : $column);
    }
}