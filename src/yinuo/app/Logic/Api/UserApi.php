<?php
namespace App\Logic\Api;

use App\Lib\HelperFactory;
use App\Models\ShopncBankcard;
use App\Models\ShopncMember;

/**
 * Class UserApi
 * @package App\Logic\Api
 */
class UserApi extends BaseApi {
    /**
     * 获取银行卡个数
     * @param $member_id
     * @return int
     */
    public function getBankcardCount($member_id){
        return ShopncBankcard::whereMemberId($member_id)->where("is_delete", 0)->count();
    }

    /**
     * 通过token获取用户信息
     * @param $token
     * @param bool $isSimple 是否只需要member表信息
     * @return null|array
     */
    public function getMemberInfoByToken($token, $isSimple = false){
        return Logic("member")->getMemberInfoByToken($token, $isSimple);
    }

    /**
     * 通过mb_user_token表获得基础用户信息
     * @param $token
     * @return array|null
     *      int $token_id 令牌编号
     *      int $member_id 用户编号
     *      string $member_name 用户名
     *      int|null $user_id 用户ID
     *      string|null $user_name 用户名
     *      string $token 登录令牌
     *      int $login_time 登录时间
     *      string $client_type 客户端类型 android wap
     *      string|null $openid 微信支付jsapi的openid缓存
     */
    public function getUserTokenInfoByToken($token){
        return Logic("member")->getUserTokenInfoByToken($token);
    }

    /**
     * 获得详情
     * @param $where
     * @param array $column
     * @param null $orderBy
     * @return ShopncMember|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getInfoByWhere(
        $where,
        $column = ['*'],
        $orderBy = null
    ){
        $query = ShopncMember::query();
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $query->orderBy($key, $item);
            }
        }
        HelperFactory::instance()->Model->whereBindQuery($query, $where);
        return $query->first(is_array($column) ? $column : $column);
    }
}