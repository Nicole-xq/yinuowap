<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/4/12 0012
 * Time: 20:14
 */

namespace App\Logic\Api;


use App\Models\ShopncBidLog;
use App\Models\ShopncOrder;

class BidLogApi
{
    /**
     * 根据用户ID判断用户是否为新手
     * @param $memberId
     * @return bool
     * @Date: 2019/4/16 0016
     * @author: Mr.Liu
     */
    public function isNewBie($memberId)
    {
        $checkBidLogCondition = [
            'member_id' => $memberId,
            //'bid_type' => ShopncBidLog::BID_TYPE_NEWBIE,
        ];
        $result = $this->checkBidLog($checkBidLogCondition);
        //第一次出价后（包含体验）7天内是新手
        if ($result && strtotime($result['created_at']) < time() - 7*24*3600) {
            return false;
        }

        //全场拍中1单即不为新手
        if (ShopncOrder::whereBuyerId($memberId)->where("auction_id", "<>", getMainConfig("newBieAuctionId"))->where("order_type", 4)->count() > 0){
            return false;
        }

        //出价6次之后不做为新手
        if (ShopncBidLog::whereMemberId($memberId)->where("auction_id", "<>", getMainConfig("newBieAuctionId"))->count() >= ShopncBidLog::NEWBIE_COUNT){
            return false;
        }

        return true;
    }


    public function checkBidLog($checkBidLog)
    {
        return ShopncBidLog::where($checkBidLog)->orderBy('created_at', 'asc')->first();
    }

    public function insertNewbieBidLog($newbieInsert)
    {
        return ShopncBidLog::insertGetId($newbieInsert);
    }
}