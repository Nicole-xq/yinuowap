<?php
namespace App\Logic\Api;

use App\Lib\HelperFactory;
use App\Models\ShopncExpress;

/**
 * 物流api
 * @package App\Logic\Api
 */
class ExpressApi extends BaseApi {
    public function getExpressList($start = 0, $size = 10, $where = false, $columns = ['*'], $isNeedCount = true){
        $builder = ShopncExpress::select($columns)->skip($start)->take($size);
        $count = false;
        if($where){
            $countBuilder = ShopncExpress::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);

            $isNeedCount && ($count = $countBuilder->count());
        }else{
            $isNeedCount && ($count = ShopncExpress::count());
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }
}