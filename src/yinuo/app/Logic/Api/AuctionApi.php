<?php
namespace App\Logic\Api;

use App\Lib\HelperFactory;
use App\Models\ShopncAuction;
use App\Models\ShopncAuctionSpecial;
use App\Models\ShopncBidLog;

class AuctionApi
{
    /**
     * 展示拍品列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @param null $orderBy
     * @return array
     */
    public function getAuctionList($start = 0, $size = 10, $where = false, $orderBy = null, $columns = ['*']){
        $builder = ShopncAuction::select($columns)->skip($start)->take($size);
        if($where){
            $countBuilder = ShopncAuction::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);
            $count = $countBuilder->count();
        }else{
            $count = ShopncAuction::count();
        }
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $builder->orderBy($key, $item);
            }
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }

    /**
     * 专场列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @param null $orderBy
     * @return array
     */
    public function getAuctionSpecialList($start = 0, $size = 10, $where = false, $orderBy = null, $columns = ['*']){
        $builder = ShopncAuctionSpecial::select($columns)->skip($start)->take($size);
        if($where){
            $countBuilder = ShopncAuctionSpecial::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);
            $count = $countBuilder->count();
        }else{
            $count = ShopncAuctionSpecial::count();
        }
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $builder->orderBy($key, $item);
            }
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }
    
    /*
     * 获取拍品详情
     */
    public function getAuctionInfo($auctionId)
    {
        return ShopncAuction::where(['auction_id' => $auctionId]) ->first() ;
    }

    /*
     * 获取出价记录
     */
    public function getBidLogInfo($condition)
    {
        $result = ShopncBidLog::where($condition)->select();
        return $result ? $result->toArray() : [];
    }



    /**
     * @param $where
     * @param array $column
     * @param null $orderBy
     * @return ShopncStore|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
//    public function getOrderInfoByWhere(
//        $where,
//        $column = ['*'],
//        $orderBy = null
//    ){
//        $query = ShopncOrder::query();
//        if($orderBy){
//            foreach ($orderBy as $key=>$item) {
//                $query->orderBy($key, $item);
//            }
//        }
//        HelperFactory::instance()->Model->whereBindQuery($query, $where);
//        return $query->first(is_array($column) ? $column : $column);
//    }
}