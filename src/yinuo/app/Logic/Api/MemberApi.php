<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/4/11 0011
 * Time: 14:30
 */

namespace App\Logic\Api;


use App\Models\ShopncMember;

class MemberApi extends BaseApi
{
    public function getMemberIdByKey($key)
    {
        $member_info = ShopncMember::where(['member_key' => $key])->first();
        if (!$member_info || !isset($member_info['member_id'])) {
            return 0;
        }
        return $member_info['member_id'];
    }

    public function getMemberIdIfExists($key)
    {
        $model_mb_user_token = Model('mb_user_token');
        $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($key);
        if (empty($mb_user_token_info)) {
            return 0;
        }
        return $mb_user_token_info['member_id'];
    }

    public function getMemberInfo($memberId)
    {
        return ShopncMember::where(['member_id' => $memberId])->first();
    }
}