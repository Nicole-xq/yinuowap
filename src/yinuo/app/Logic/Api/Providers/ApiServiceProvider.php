<?php

namespace App\Logic\Api\Providers;

use App\Logic\Api\AccountApi;
use App\Logic\Api\AuctionApi;
use App\Logic\Api\BidLogApi;
use App\Logic\Api\CloseAccountApi;
use App\Logic\Api\ExpressApi;
use App\Logic\Api\MemberApi;
use App\Logic\Api\OrderApi;
use App\Logic\Api\RankingApi;
use App\Logic\Api\RedPacketApi;
use App\Logic\Api\SellerApi;
use App\Logic\Api\StoreApi;
use App\Logic\Api\UserApi;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("userApi", function (){
                return new UserApi();
        });
        $this->app->singleton("orderApi", function (){
                return new OrderApi();
        });
        $this->app->singleton("sellerApi", function (){
                return new SellerApi();
        });
        $this->app->singleton("storeApi", function (){
                return new StoreApi();
        });
        $this->app->singleton("expressApi", function (){
                return new ExpressApi();
        });
        $this->app->singleton("accountApi", function (){
                return new AccountApi();
        });
        $this->app->singleton("closeAccountApi", function (){
                return new CloseAccountApi();
        });
        $this->app->singleton("auctionApi", function (){
                return new AuctionApi();
        });
        $this->app->singleton("rankingApi", function (){
                return new RankingApi();
        });
        $this->app->singleton("redPacketApi", function (){
            return new RedPacketApi();
        });
        $this->app->singleton("bidLogApi", function (){
            return new BidLogApi();
        });
        $this->app->singleton("memberApi", function (){
            return new MemberApi();
        });

    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'userApi', 'orderApi', 'sellerApi', 'storeApi', 'expressApi', 'accountApi', 'closeAccountApi',
            'auctionApi', 'rankingApi', 'redPacketApi', 'bidLogApi', 'memberApi',
            ];
    }
}
