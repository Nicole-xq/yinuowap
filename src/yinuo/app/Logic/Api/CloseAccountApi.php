<?php
namespace App\Logic\Api;

use App\Exceptions\ApiResponseException;
use App\Lib\HelperFactory;
use App\Models\ShopncCloseAccount;
use App\Models\ShopncCloseAccountLog;
use App\Services\Constant\ApiResponseCode;

class CloseAccountApi
{
    /**
     * 修改资金
     * @param integer $storeId
     * @param number $money
     * @param integer $transaction_type
     * @param null $transaction_source
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    protected  function modAccountMoney($storeId, $money, $transaction_type, $transaction_source = null, $note = '')
    {
        if(!is_numeric($money)){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        $memberAccount = ShopncCloseAccount::whereStoreId($storeId)->first();
        if (!$memberAccount) {
            ApiResponseException::throwException(ApiResponseCode::MEMBER_ACCOUNT_NOT_EXIST, '抱歉系统异常,请联系管理员');
        }
//        \Log::info('modAccountMoney'.$storeId, [func_get_args(), $memberAccount->toArray()]);
        
        $memberMoney = $memberAccount->wait_amount;

        if((bcadd($memberMoney, $money, 2)) < 0){
            ApiResponseException::throwException(ApiResponseCode::MEMBER_ACCOUNT_BALANCE_LACK);
        }
        
        if(ShopncCloseAccount::where('store_id', $storeId)->whereRaw("(`wait_amount` + $money) >= 0")->increment('wait_amount', $money)){
            if($transaction_type == ShopncCloseAccountLog::TRANSACTION_TYPE_TO_MEMBER_ACCOUNT){
                //出账加出账总金额
                if(!ShopncCloseAccount::where('store_id', $storeId)->increment('clsd_amount', abs($money))){
                    ApiResponseException::throwException(ApiResponseCode::OPERATE_FAIL);
                }
            }
            $balanceMoneyLogModel = new ShopncCloseAccountLog();
            $balanceMoneyLogModel->store_id = $storeId;
            $balanceMoneyLogModel->before_wait_amount = $memberMoney;
            $balanceMoneyLogModel->change_wait_amount = abs($money);
            $balanceMoneyLogModel->later_wait_amount = bcadd($memberMoney, $money, 2);
            $balanceMoneyLogModel->io_type = $money < 0 ? ShopncCloseAccountLog::IO_TYPE_OUT :  ShopncCloseAccountLog::IO_TYPE_IN;
            $balanceMoneyLogModel->note = $note;
            $balanceMoneyLogModel->transaction_type = $transaction_type;
            $balanceMoneyLogModel->transaction_source = $transaction_source;
            $balanceMoneyLogModel->save();
        }else{
            ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
        }

        return [
            'logId' => $balanceMoneyLogModel->id,
        ];
    }

    /**
     * 增加资金 外部事务
     * @param integer $storeId
     * @param int $money
     * @param integer $transaction_type
     * @param null $lg_source
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function addAccountMoney($storeId, $money, $transaction_type, $lg_source = null, $note = '收入'){
        if($money <= 0){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        return $this->modAccountMoney($storeId, $money, $transaction_type, $lg_source, $note);
    }

    /**
     * 减少资金 外部事务
     * @param integer $storeId
     * @param number $money
     * @param integer $transaction_type
     * @param null $lg_source
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function minusAccountMoney($storeId, $money, $transaction_type, $lg_source = null, $note = '支出'){
        if($money <= 0){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        return $this->modAccountMoney($storeId, -$money, $transaction_type, $lg_source, $note);
    }

    /**
     * 初始化账户
     * @param integer $storeId
     * @return bool
     */
    public function initAccount($storeId){
        if(ShopncCloseAccount::whereStoreId($storeId)->count() < 1){
            $account = new ShopncCloseAccount();
            $account->store_id = $storeId;
            $account->wait_amount = 0;
            $account->clsd_amount = 0;
            return $account->save();
        }
        return true;
    }

    /**
     * 获得订单信息
     * @param $storeId
     * @param array $columns
     * @return ShopncCloseAccount|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getCloseAccountInfo($storeId, $columns = ['*']){
        return ShopncCloseAccount::whereStoreId($storeId)->first($columns);
    }

    /**
     * 展示pdLog列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @return array
     */
    public function getLogList($start = 0, $size = 10, $where = false, $columns = ['*']){
        $builder = ShopncCloseAccountLog::select($columns)->skip($start)->take($size)->orderByDesc("id");
        if($where){
            $countBuilder = ShopncCloseAccountLog::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);

            $count = $countBuilder->count();
        }else{
            $count = ShopncCloseAccountLog::count();
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }
}