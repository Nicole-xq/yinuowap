<?php
namespace App\Logic\Api;

use App\Exceptions\ApiResponseException;
use App\Lib\HelperFactory;
use App\Models\ShopncMember;
use App\Models\ShopncPdLog;
use App\Services\Constant\ApiResponseCode;

class AccountApi
{
    /**
     * 修改资金
     * @param unknown $memberId
     * @param $member_name
     * @param number $money
     * @param unknown $transaction_type
     * @param null $lg_source
     * @param string $lg_admin_name
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    protected  function modAccountMoney($memberId, $member_name, $money, $transaction_type, $lg_source = null, $lg_admin_name = "user", $note = '')
    {
        if(!is_numeric($money)){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        $memberAccount = ShopncMember::whereMemberId($memberId)->first();
        if (!$memberAccount) {
            ApiResponseException::throwException(ApiResponseCode::MEMBER_ACCOUNT_NOT_EXIST, '抱歉系统异常,请联系管理员');
        }
//        \Log::info('modAccountMoney'.$memberId, [func_get_args(), $memberAccount->toArray()]);
        
        $memberMoney = $memberAccount->available_predeposit;

        if(($memberMoney + $money) < 0){
            ApiResponseException::throwException(ApiResponseCode::MEMBER_ACCOUNT_BALANCE_LACK);
        }
        
        if(ShopncMember::where('member_id', $memberId)->whereRaw("(`available_predeposit` + $money) >= 0")->increment('available_predeposit', $money)){
            $balanceMoneyLogModel = new ShopncPdLog();
            $balanceMoneyLogModel->lg_member_id = $memberId;
            $balanceMoneyLogModel->lg_member_name = $member_name;
            $balanceMoneyLogModel->lg_admin_name = $lg_admin_name;
            $balanceMoneyLogModel->lg_type = $transaction_type;
            $balanceMoneyLogModel->lg_av_amount = $money;
            $balanceMoneyLogModel->lg_freeze_amount = 0;
            $balanceMoneyLogModel->lg_freeze_predeposit = $memberAccount->freeze_predeposit;
            $balanceMoneyLogModel->lg_available_amount = bcadd($memberMoney, $money,2);
            $balanceMoneyLogModel->lg_add_time = time();
            $balanceMoneyLogModel->lg_desc = $note;
            $balanceMoneyLogModel->lg_source = $lg_source;
            $balanceMoneyLogModel->save();
        }else{
            ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
        }

        return [
            'money_log_id' => $balanceMoneyLogModel->lg_id,
            'member_id'=>$memberId,
        ];
    }

    /**
     * 增加资金 外部事务
     * @param unknown $memberId
     * @param $member_name
     * @param int $money
     * @param unknown $transaction_type
     * @param null $lg_source
     * @param string $lg_admin_name
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function addAccountMoney($memberId, $member_name, $money, $transaction_type, $lg_source = null, $lg_admin_name = "user", $note = '收入'){
        if($money <= 0){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        return $this->modAccountMoney($memberId, $member_name, $money, $transaction_type, $lg_source, $lg_admin_name, $note);
    }

    /**
     * 减少资金 外部事务
     * @param unknown $memberId
     * @param $member_name
     * @param number $money
     * @param unknown $transaction_type
     * @param null $lg_source
     * @param string $lg_admin_name
     * @param string $note
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function minusAccountMoney($memberId, $member_name, $money, $transaction_type, $lg_source = null, $lg_admin_name = "user", $note = '支出'){
        if($money <= 0){
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID);
        }
        return $this->modAccountMoney($memberId, $member_name, -$money, $transaction_type, $lg_source, $note);
    }

    /**
     * 展示pdLog列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @return array
     */
    public function getPdLogList($start = 0, $size = 10, $where = false, $columns = ['*']){
        $builder = ShopncPdLog::select($columns)->skip($start)->take($size)->orderByDesc("lg_add_time");
        if($where){
            $countBuilder = ShopncPdLog::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);

            $count = $countBuilder->count();
        }else{
            $count = ShopncPdLog::count();
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }
}