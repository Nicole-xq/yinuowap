<?php
namespace App\Logic\Api;

use App\Exceptions\ApiResponseException;
use App\Lib\HelperFactory;
use App\Models\ShopncOrder;
use App\Models\ShopncOrderCommon;
use App\Models\ShopncOrderGood;
use App\Services\Constant\ApiResponseCode;

/**
 * Class OrderApi 订单相关api
 * @package App\Logic\Api
 */
class OrderApi extends BaseApi {
    /**
     * 获取购买信息 下单页面
     * @param $memberId
     * @param $cartId string ifCart=1:购物车id|个数...  ifCart=2:商品id|个数(688|1,689|1)
     * @param $ifCart int 是否来自购物车 1.来自购物车 其他.直接购买
     * @param $addressId int 地址id
     * @param $storeId int 卖家店铺id  考虑是否为历史遗留错误字段?
     * @param $disId int 分销id
     * @return array
     */
    function getBuyInfo(
        $memberId,
        $cartId,
        $ifCart = 0,
        $storeId = 0,
        $addressId = 0,
        $disId = 0
    ){
        return Logic('buy')->getBuyInfo($memberId,
            $cartId,
            $ifCart,
            $storeId,
            $addressId,
            $disId
        );
    }
    /**
     * 下单第二步
     * 创建订单
     * @param $memberId int 当前用户id
     * @param $ifCart int 是否是来自购物车
     * @param $cartId int 购物车商品id
     * @param $addressId int 地址id
     * @param $vatHash string 发票hash
     * @param $offPayHash string 运费相关hash
     * @param $offPayHashBatch string 运费相关hash
     * @param $payName string 支付方式
     * @param int $invoiceId int 发票id
     * @param int $rpt string|array 红包id集合
     * @param int $pdPay int 是否预存款支付
     * @param int $rcbPay int 是否使用充值卡支付
     * @param int $pointsPay int 是否诺币支付
     * @param null $password string 支付密码
     * @param string $fcode string f码
     * @param string $voucherStr string 代金券
     * @param string $payMessage string 备注信息
     * @param int $orderFrom int 1WEB 2mobile 3原生
     * @return array
     * @throws \Exception \App\Exceptions\ApiException
     */
    public function createOrder(
        $memberId, $ifCart, $cartId, $addressId, $vatHash,
        $offPayHash, $offPayHashBatch, $payName, $invoiceId, $rpt,
        $pdPay, $rcbPay, $pointsPay, $password, $fcode, $voucherStr,
        $payMessage, $orderFrom
    )
    {
        return Logic("buy")->createOrder(
            $memberId,$ifCart, $cartId, $addressId, $vatHash,
            $offPayHash, $offPayHashBatch, $payName, $invoiceId, $rpt,
            $pdPay, $rcbPay, $pointsPay, $password, $fcode, $voucherStr,
            $payMessage, $orderFrom
        );
    }


    /**
     * 获取支付信息
     * @param $memberId
     * @param $paySn
     * @return array
     */
    public function getPayInfo($memberId, $paySn){
        return (Logic("buy")->getPayInfo(
            $memberId,
            $paySn
        ));
    }

    /**
     * 支付宝支付
     * @param $memberId
     * @param $paySn
     * @param $payDetail
     *    points_pay
     * rcb_pay
     * pd_pay
     * password
     * inapp 非必填
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function toAlipay($memberId, $paySn, $payDetail){
        $payInfo = Logic("payment")->toPayGetResultOrderPayInfo($memberId, $paySn, $payDetail, 1);
        if($payInfo['data']['api_pay_amount']  <= 0){
            return null;
        }
        $alipayInfo = Logic("payment")->alipay($payInfo['data']['subject'], $payInfo['data']['pay_sn'], $payInfo['data']['api_pay_amount']);
        return $alipayInfo;
    }

    /**
     * 微信支付
     * @param $memberId
     * @param $paySn
     * @param array $payDetail 保留参数
     *    points_pay
     * rcb_pay
     * pd_pay
     * password
     * inapp 非必填
     * @param bool $noCredit 禁用信用卡
     * @return array
     * @throws \App\Exceptions\ResponseException
     */
    public function toWxPay($memberId, $paySn, $payDetail, $noCredit = false){
        $payInfo = Logic("payment")->toPayGetResultOrderPayInfo($memberId, $paySn, $payDetail, 1);
        $param = array();
        $param['pay_sn'] = $paySn;
        $param['subject'] = $payInfo['data']['subject'];
        $param['amount'] = $payInfo['data']['api_pay_amount'] * 100;
        $param['no_credit'] = $noCredit;
        if($param['amount']  <= 0){
            return null;
        }
        $data = Logic("payment")->getWxPayInfo($param);
        return $data;
    }

    /**
     * 展示列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @return array
     */
    public function getList($start = 0, $size = 10, $where = false, $columns = ['*']){
        //TODO $size 暂时修改为1000
        $size = 1000;
        $builder = ShopncOrder::select($columns)->skip($start)->take($size)->orderByDesc("add_time");
        if($where){
            $countBuilder = ShopncOrder::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);

            $count = $countBuilder->count();
        }else{
            $count = ShopncOrder::count();
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }

    /**
     * 获得订单商品
     * @param $orderId
     * @param array $columns
     * @return array
     */
    public function getOrderGoodsList($orderId, $columns = ['*']){
        return ShopncOrderGood::whereOrderId($orderId)->select($columns)->get()->toArray();
    }

    /**
     * 获得订单扩展信息
     * @param $orderId
     * @param array $columns
     * @return ShopncOrderCommon|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getOrderCommonInfo($orderId, $columns = ['*']){
        return ShopncOrderCommon::whereOrderId($orderId)->first($columns);
    }
    /**
     * 获得订单信息
     * @param $orderId
     * @param array $columns
     * @return ShopncOrder|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getOrderInfo($orderId, $columns = ['*']){
        return ShopncOrder::whereOrderId($orderId)->first($columns);
    }

    /**
     * 获得订单商品的最终费率
     * 这里商品实际的金额 扣除比例要考虑到订单改价后的比例 order_amount_original !=null && order_amount_original != order_amount 改价了
     * @param $goodsPayPrice double 商品实际成交价
     * @param $artworkCategoryId int 艺术家作品分类
     * @param $orderAmount double 订单实价
     * @param $orderAmountOriginal double 订单原始价
     * @return array rate => 百分数*100, cName => 类名, feeAmount => 费用 小数位4位
     */
    public function getOrderGoodsClosingFeeInfo($goodsPayPrice, $artworkCategoryId, $orderAmount, $orderAmountOriginal){
        $feeRateInfo = $this->getArtworkCategoryFeeRateCName($artworkCategoryId);
        $feeRateInfo['feeAmount'] = bcmul($goodsPayPrice, bcdiv($feeRateInfo['rate'], 100, 6), 4);
        if(!is_null($orderAmountOriginal) && $orderAmountOriginal != $orderAmount){
            $feeRateInfo['feeAmount'] = bcmul($feeRateInfo['feeAmount'], bcdiv($orderAmount, $orderAmountOriginal, 6), 4);
        }
        $feeRateInfo["feeAmount"] = $feeRateInfo['feeAmount'];
        return $feeRateInfo;
    }

    /**
     * 获得费率和类名
     * @param int $artworkCategoryId
     * @return array rate=>百分数*100, cName=>类名
     */
    public function getArtworkCategoryFeeRateCName($artworkCategoryId){
        $artistOrderClosing = config("artist-order-closing");
        if($artworkCategoryId > 0){
            $artworkCategoryId = intval($artworkCategoryId);
            $artistArtworkCategory = config("artist-artwork-category");
            foreach ($artistArtworkCategory as $pId => $v){
                if(isset($v['child'][$artworkCategoryId])){
                    $rate = isset($artistOrderClosing['closingFeeRatio'][$pId]) ? $artistOrderClosing['closingFeeRatio'][$pId]['feeRatio'] : $artistOrderClosing['closingFeeUnknownRatio'];
                    return ['rate'=> $rate, "cName"=>$v['child'][$artworkCategoryId]['c_name']];
                }
            }
        }
        return ['rate'=>$artistOrderClosing['closingFeeUnknownRatio'], "cName"=>"未知"];
    }

    /**
     * 订单完成结算
     * @param $orderId
     * @param $closingNo
     * @param $time
     * @param $artistClosingAmount
     * @return bool
     * @throws \App\Exceptions\ResponseException
     */
    public function orderClosingDone($orderId, $closingNo, $time, $artistClosingAmount){
        $cou = ShopncOrder::whereOrderId($orderId)->where("artist_closing_status", ShopncOrder::ARTIST_CLOSING_STATUS_UN)->update([
            'artist_closing_status' => ShopncOrder::ARTIST_CLOSING_STATUS_DONE,
            'artist_closing_time' => date('Y-m-d H:i:s', $time),
            'artist_closing_no' => $closingNo,
            'artist_closing_amount' => $artistClosingAmount,
        ]);
        if($cou < 1){
            ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
        }
        return true;
    }

    /**
     * @param $where
     * @param array $column
     * @param null $orderBy
     * @return ShopncStore|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getOrderInfoByWhere(
        $where,
        $column = ['*'],
        $orderBy = null
    ){
        $query = ShopncOrder::query();
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $query->orderBy($key, $item);
            }
        }
        HelperFactory::instance()->Model->whereBindQuery($query, $where);
        return $query->first(is_array($column) ? $column : $column);
    }

    /**
     * 更具paySn获取汇总信息
     * 这里的 order_amount 为实际支付金额
     * @param $paySn
     * @return ShopncOrder|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null
     */
    public function getSumPayInfoByPaySn($paySn){
        return ShopncOrder::wherePaySn($paySn)->groupBy("pay_sn")->selectRaw("max(order_id) order_id, GROUP_CONCAT(order_sn) order_sn, (sum(order_amount)-sum(CASE WHEN is_points=1 THEN points_amount ELSE 0 END)) order_amount")->first();
    }

    /*
     * 新手专区专用
     * 生成拍卖订单
     * @$auction 拍品详情   --需要模拟
     * @$bid_log 出价记录   --需要模拟
     */
    public function _auctions_order($auction_info, $memberInfo)
    {
        $model_auctions = Model('auctions');
        /** @var \orderModel $model_order */
        $model_order = Model('order');
        $member = Model("member")->getMemberInfo(array('member_id' => $memberInfo['member_id']));
        try {
            $model_auctions->beginTransaction();
            $auction_id = $auction_info['auction_id'];
            $pay_sn = Logic('buy_1')->makePaySn($memberInfo['member_id']);
            $order_pay = array();
            $order_pay['pay_sn'] = $pay_sn;
            $order_pay['buyer_id'] = $memberInfo['member_id'];
            $orderPayExist = $model_order->getOrderPay($order_pay);
            if (!$orderPayExist) {
                $order_pay_id = $model_order->addOrderPay($order_pay);
            } else {
                $order_pay_id = isset($orderPayExist['pay_id']) ? $orderPayExist['pay_id'] : 0;
            }
            if (!$order_pay_id) {
                yLog()->error('订单保存失败[未生成支付单]');
                throw new \Exception('订单保存失败[未生成支付单]');
            }
            $order_info = Model("")->table("orders")->where(array('buyer_id' => $memberInfo['member_id'], 'auction_id' => $auction_id, 'order_type' => 4))->find();
            if (empty($order_info)) {
                $order['order_sn'] = Logic('buy_1')->makeOrderSn($order_pay_id);
                $order['pay_sn'] = $pay_sn;
                $order['store_id'] = $auction_info['store_id'];
                $order['store_name'] = $auction_info['store_name'];
                $order['buyer_id'] = $memberInfo['member_id'];
                $order['buyer_name'] = $memberInfo['member_name'];
                $order['add_time'] = TIMESTAMP;
                $order['payment_code'] = 'online';
                $order['order_state'] = 10;
                $order['auction_id'] = $auction_id;
                $order['order_amount'] = $auction_info['current_price'];        //订单金额需要修改
                $order['goods_amount'] = $auction_info['current_price'];
                $order['margin_amount'] = 10;
                $order['order_from'] = 1;   //1PC，2移动
                $order['order_type'] = 4;
                $order_id = ShopncOrder::insertGetId($order);
                $order['order_id'] = $order_id;
                if (!$order_id) {
                    $model_auctions->rollback();
                    throw new \Exception('订单创建失败');
                } else {
                    $url = AUCTION_NEW_URL . "/auctioncommoditydetails?auction_id=" . $auction_info['auction_id'];
                    $url = filterUrl($url);
                    $short = sinaShortenUrl($url);
                    $param = array();
                    $param['code'] = 'auction_auction';
                    $param['member_id'] = $member['member_id'];
                    $param['number']['mobile'] = $member['member_mobile'];
                    $param['param'] = array(
                        'auction_name' => $auction_info['auction_name'],
                        'money' => $auction_info['current_price'],
                        'url' => $short,
                        'wx_url' => $short
                    );
                    \QueueClient::push('sendMemberMsg', $param);
                    $model_auctions->commit();
                }
                $model_auctions->commit();
                return $order;
            } else {
                $model_auctions->rollback();
                return $order_info;
            }
        } catch (\Exception $e) {
            $model_auctions->rollback();
        }
    }
}