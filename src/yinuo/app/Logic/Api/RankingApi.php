<?php
namespace App\Logic\Api;

use App\Lib\HelperFactory;
use App\Lib\Helpers\MathHelper;
use App\Models\ShopncRankingMember;
use App\Services\Constant\RedisKeyConstant;

class RankingApi
{
    public static $newBieRankingWhere = [
        ["status", ShopncRankingMember::START_OK],
        ["r_type", ShopncRankingMember::R_TYPE_NEWBIE]
    ];
    /**
     * 列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @param null $orderBy
     * @return array
     */
    public function getRankingList($start = 0, $size = 10, $where = false, $orderBy = null, $columns = ['*']){
        $builder = ShopncRankingMember::select($columns)->skip($start)->take($size);
        if($where){
            $countBuilder = ShopncRankingMember::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);
            $count = $countBuilder->count();
        }else{
            $count = ShopncRankingMember::count();
        }
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $builder->orderBy($key, $item);
            }
        }
        $data = $builder->get()->toArray();
        return ['list' => $data, 'count' => $count];
    }

    /**
     * 根据时间获得期数
     * @param $nowTime
     * @return float
     */
    public function getPeriodNum($nowTime){
        $newbieConfig = config("newbie");
        $startTime = strtotime($newbieConfig["startDate"]);
        $periodSec = $newbieConfig["periodSec"];
        return ceil(($nowTime - $startTime) / $periodSec);
    }

    /**
     * 根据时间获得 当期剩余时间
     * @param $nowTime
     * @return int
     */
    public function getPeriodResidueTime($nowTime){
        $newbieConfig = config("newbie");
        $startTime = strtotime($newbieConfig["startDate"]);
        $periodSec = $newbieConfig["periodSec"];
        return $periodSec - (($nowTime - $startTime) % $periodSec);
    }
    /**
     * 获得详情
     * @param $where
     * @param array $column
     * @param null $orderBy
     * @return ShopncRankingMember|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getInfoByWhere(
        $where,
        $column = ['*'],
        $orderBy = null
    ){
        $query = ShopncRankingMember::query();
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $query->orderBy($key, $item);
            }
        }
        HelperFactory::instance()->Model->whereBindQuery($query, $where);
        return $query->first(is_array($column) ? $column : $column);
    }

    /**
     * 排行历史列表
     * @param int $start
     * @param int $size
     * @param bool $where
     * @param array $columns
     * @param null $orderBy
     * @return array
     */
    public function getRankingHistory($start = 0, $size = 10, $where = false, $orderBy = null, $columns = ['*']){
        $builder = ShopncRankingMember::select($columns)->skip($start)->take($size)->groupBy("period_num");
        if($where){
            $countBuilder = ShopncRankingMember::query();
            $allBuilder = [$builder, $countBuilder];
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);
        }
        if($orderBy){
            foreach ($orderBy as $key=>$item) {
                $builder->orderBy($key, $item);
            }
        }
        $data = $builder->get()->toArray();
        return $data;
    }
    public function getRankingHistoryData($isCache = true){
        $cacheTimeOut = config("newbie.cacheTimeOut");
        if($isCache){
            $redisCache = app("redis")->get(RedisKeyConstant::NEWBIE_RANKING_HISTORY_DATA);
            if($redisCache){
                return json_decode($redisCache, true);
            }
        }
        //历史期数等
        $rankingHistoryWhere = RankingApi::$newBieRankingWhere;
        $rankingHistory = application()->rankingApi->getRankingHistory(0, 1000, $rankingHistoryWhere, ["period_num"=>"desc"],
            ["period_num", \DB::raw("min(start_date) start_date"), \DB::raw("max(end_date) end_date")]);
        if($rankingHistory){
            foreach ($rankingHistory as &$item){
                $item['period_num_str'] = MathHelper::numToWord(intval($item['period_num']));
            }
        }
        app("redis")->setex(RedisKeyConstant::NEWBIE_RANKING_HISTORY_DATA, $cacheTimeOut, json_encode($rankingHistory));
        return $rankingHistory;
    }

    public function getPeriodRankingData($period_num, $isCache = true){
        if($isCache){
            $redisCache = app("redis")->get(RedisKeyConstant::PERIOD_NEWBIE_RANKING_LIST.$period_num);
            if($redisCache){
                return json_decode($redisCache, true);
            }
        }
        $out = [];
        $newBieRankingWhere = RankingApi::$newBieRankingWhere;
        $newBieRankingWhere["period_num"] =  $period_num;
        $out['topPackage'] = $topPackage = config("newbie.topPackage.".$period_num);
        $cacheTimeOut = config("newbie.cacheTimeOut");
//        $topAdd = empty($topPackage) ? 0 : count($topPackage);
        $size = 10;
        //当期排名
        $data = $this->getRankingList(0, $size, $newBieRankingWhere, ["ranking_num"=>'asc'],
            ['period_num', 'member_id', 'member_name', 'member_avatar', 'ranking_num', 'recommend_num']);
        unset($data["count"]);
        if(!empty($data['list'])){
            foreach ($data['list'] as &$item) {
                $item['member_avatar'] = getMemberAvatarForID($item['member_id'], $item['member_avatar']);
//                $item['ranking_num'] += $topAdd;
            }
        }
        if(!empty($topPackage)){
            $data['list'] = array_merge($topPackage, $data["list"]);
        }
        $outList = [];
        //重新排序整理排行榜
        if($data['list']){
            $data['list'] = collect($data['list'])->sortByDesc("recommend_num")->toArray();
            $nowRankingNum = 0;
            foreach ($data['list'] as &$datum) {
                $nowRankingNum++;
                if($nowRankingNum > $size){
                    break;
                }else{
                    $datum["ranking_num"] = $nowRankingNum;
                }
                $datum["recommend_num"] = strval($datum["recommend_num"]);
                if( $datum["recommend_num"] > 9){
                    $asterisks = "*******";
                    $datum["recommend_num"] = substr($datum["recommend_num"],0,1).substr($asterisks, 0, strlen($datum["recommend_num"])-1);
                }
                if(preg_match('/\d{11}/',$datum["member_name"])){
                    $datum["member_name"] = substr($datum["member_name"],0,4).'****'.substr($datum["member_name"], 8, 3);
                }

                $outList[] = $datum;
            }
        }
        $out['list'] = $outList;
        app("redis")->setex(RedisKeyConstant::PERIOD_NEWBIE_RANKING_LIST.$period_num, $cacheTimeOut, json_encode($out));
        return $out;
    }
}