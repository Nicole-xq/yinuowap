<?php
namespace App\Logic\Api;

use App\Lib\HelperFactory;
use App\Models\ShopncStore;

/**
 * Class SellerApi
 * @package App\Logic\Api
 */
class StoreApi extends BaseApi {
    /**
     * @param $artistId
     * @param array $column
     * @return ShopncStore|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    function getStoreInfoByArtistId(
        $artistId,
        $column = ['*']
    ){
        return ShopncStore::whereArtistId(intval($artistId))->first(is_array($column) ? $column : $column);
    }

    /**
     * @param $where
     * @param array $column
     * @return ShopncStore|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    function getStoreInfo(
        $where,
        $column = ['*']
    ){
        $query = ShopncStore::query();
        HelperFactory::instance()->Model->whereBindQuery($query, $where);
        return $query->first(is_array($column) ? $column : $column);
    }
}