<?php

namespace App\Listeners;

use App\Events\LoginFailEvent;
use App\Lib\Helpers\LogHelper;
use App\Logic\Api\UserSafeApi;
use App\Services\Constant\RedisKeyConstant;

class LoginFailCaptchaListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoginFailEvent $event
     */
    public function handle(LoginFailEvent $event)
    {
        try{
            $ip = $event->ip;
            $userName = $event->userName;
            if($ip){
                if(\Cache::has(RedisKeyConstant::PREFIX_IP_LOGIN_FAIL_COUNT.$ip)){
                    \Cache::increment(RedisKeyConstant::PREFIX_IP_LOGIN_FAIL_COUNT.$ip, 1);
                }else{
                    \Cache::put(RedisKeyConstant::PREFIX_IP_LOGIN_FAIL_COUNT.$ip, 1, UserSafeApi::IP_NEED_CAPTCHA_TIME_OUT);
                }
            }
            if($userName){
                if(\Cache::has(RedisKeyConstant::PREFIX_USER_NAME_LOGIN_FAIL_COUNT.$userName)){
                    \Cache::increment(RedisKeyConstant::PREFIX_USER_NAME_LOGIN_FAIL_COUNT.$userName, 1);
                }else{
                    \Cache::put(RedisKeyConstant::PREFIX_USER_NAME_LOGIN_FAIL_COUNT.$userName, 1, UserSafeApi::USER_NAME_NEED_CAPTCHA_TIME_OUT);
                }
            }
        }catch (\Exception $e){
            \Log::error('LoginFailCaptchaListener_error', LogHelper::getEInfo($e));
        }
    }
}
