<?php

namespace App\Listeners;

use App\Events\AdminOperateEvent;
use App\Lib\Helpers\LogHelper;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminOperateRecordLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminOperateEvent  $event
     * @return void
     */
    public function handle(AdminOperateEvent $event)
    {
        try{
            $adminOperateData = [
                'operate_user_id' => $event->operateUserId,
                'action_name' => $event->actionName,
                'ip' => $event->ip,
            ];
            if(!empty($event->param)){
                $adminOperateData['param'] = $event->param;
            }
            \Log::info('AdminOperateRecordLog', $adminOperateData);
/*            if(!empty($event->param)){
                $adminOperateData['param'] = json_encode($event->param);
            }
            application()->adminOperateApi->recordLog($adminOperateData);*/
        }catch (\Exception $e){
            \Log::error('AdminOperateRecordLogListener_error', LogHelper::getEInfo($e));
        }
    }
}
