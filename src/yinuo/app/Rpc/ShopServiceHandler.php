<?php
/**
 * Created by PhpStorm.
 * User: xuhui
 * Date: 17/5/11
 * Time: 17:14
 */
namespace App\Rpc;

use App\Exceptions\ResponseException;
use App\Lib\Helpers\ArrayHelper;
use App\Lib\Helpers\ToolsHelper;
use App\Models\ShopncCloseAccountLog;
use App\Models\ShopncOrder;
use App\Models\ShopncOrderCommon;
use App\Models\ShopncPdLog;
use Server\Rpc\Service\ShopNc\cartGoodsInfo;
use Server\Rpc\Service\ShopNc\OrderInfo;
use Server\Rpc\Service\ShopNc\ShopServiceIf;
use Server\Rpc\Common\Shared\ServiceExceptionCode;
use Server\Rpc\Common\Shared\ServiceException;
use App\Rpc\Exception\ServiceExceptionTools;
use App\Lib\Helpers\LogHelper;

class ShopServiceHandler extends BaseHandler implements ShopServiceIf {
    //艺术家一次计算结算的条数
    const ONCE_ARTIST_CLOSING_COUNT =500;

    /**
     * 获得订单数据
     * @param int order_id
     * @return OrderInfo 订单信息
     *
     *
     * @param int $order_id
     * @return \Server\Rpc\Service\ShopNc\OrderInfo
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getOrderInfo($order_id)
    {
        // TODO: Implement getOrderInfo() method.
        $logic_order = Logic('order');
        $result = $logic_order->getMemberOrderInfo($order_id, "105198");
        return new OrderInfo($result["data"]["order_info"]);
    }
    /**
     * 根据用户id数组获取用户购物车信息
     * @param list<i32> userId 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $userId
     * @return \Server\Rpc\Service\ShopNc\cartGoodsInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getCartList($userId)
    {
//        // TODO: Implement getCartList() method.
//        $logic_order = Logic('order');
//        $result = $logic_order->getMemberOrderInfo($userId, "105198");
//        return [new cartGoodsInfo($result["data"]["order_info"])];
        $model_cart = Model('cart');

        $condition = array('buyer_id' => $this->member_info['member_id']);
        $cart_list  = $model_cart->listCart('db', $condition);

        // 购物车列表 [得到最新商品属性及促销信息]
        $logic_buy_1 = logic('buy_1');
        $cart_list = $logic_buy_1->getGoodsCartList($cart_list);

        //购物车商品以店铺ID分组显示,并计算商品小计,店铺小计与总价由JS计算得出
        $store_cart_list = array();
        $sum = 0;
        foreach ($cart_list as $cart) {
            if (!empty($cart['gift_list'])) {
                foreach ($cart['gift_list'] as $key => $val) {
                    $cart['gift_list'][$key]['goods_image_url'] = cthumb($val['gift_goodsimage'], $cart['store_id']);
                }
                $cart['gift_list'] = array_values($cart['gift_list']);
            }
            $store_cart_list[$cart['store_id']]['store_id'] = $cart['store_id'];
            $store_cart_list[$cart['store_id']]['store_name'] = $cart['store_name'];
            $cart['goods_image_url'] = cthumb($cart['goods_image'], $cart['store_id']);
            $cart['goods_total'] = ncPriceFormat($cart['goods_price'] * $cart['goods_num']);
            $cart['xianshi_info'] = $cart['xianshi_info'] ? $cart['xianshi_info'] : array();
            $cart['groupbuy_info'] = $cart['groupbuy_info'] ? $cart['groupbuy_info'] : array();
            $store_cart_list[$cart['store_id']]['goods'][] = $cart;
            $sum += $cart['goods_total'];
        }

        // 店铺优惠券
        $condition = array();
        $condition['voucher_t_gettype'] = 3;
        $condition['voucher_t_state'] = 1;
        $condition['voucher_t_end_date'] = array('gt', time());
        $condition['voucher_t_mgradelimit'] = array('elt', $this->member_info['level']);
        $condition['voucher_t_store_id'] = array('in', array_keys($store_cart_list));
        $voucher_template = Model('voucher')->getVoucherTemplateList($condition);
        if (!empty($voucher_template)) {
            foreach ($voucher_template as $val) {
                $param = array();
                $param['voucher_t_id'] = $val['voucher_t_id'];
                $param['voucher_t_price'] = $val['voucher_t_price'];
                $param['voucher_t_limit'] = $val['voucher_t_limit'];
                $param['voucher_t_end_date'] = date('Y年m月d日', $val['voucher_t_end_date']);
                $store_cart_list[$val['voucher_t_store_id']]['voucher'][] = $param;
            }
        }

        //取得店铺级活动 - 可用的满即送活动
        $mansong_rule_list = $logic_buy_1->getMansongRuleList(array_keys($store_cart_list));
        if (!empty($mansong_rule_list)) {
            foreach ($mansong_rule_list as $key => $val) {
                $store_cart_list[$key]['mansong'] = $val;
            }
        }

        //取得哪些店铺有满免运费活动
        $free_freight_list = $logic_buy_1->getFreeFreightActiveList(array_keys($store_cart_list));
        if (!empty($free_freight_list)) {
            foreach ($free_freight_list as $key => $val) {
                $store_cart_list[$key]['free_freight'] = $val;
            }
        }

        return (array('cart_list' => array_values($store_cart_list), 'sum' => ncPriceFormat($sum), 'cart_count' => count($cart_list)));
    }

    /**
     * 根据用户id数组获取用户购物车信息
     * @param list<i32> userId 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $userId
     * @return \Server\Rpc\Service\ShopNc\cartGoodsInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function cartCount($userId)
    {
        return $param['cart_count'] = Model('cart')->countCartByMemberId($userId);
    }



    /**
     * 根据用户id数组获取用户购物车信息
     * @param list<i32> userId 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $userId
     * @return \Server\Rpc\Service\ShopNc\cartGoodsInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function cartAdd($userId, $goodsId, $quantity)
    {
        // TODO: Implement getCartList() method.
        $logic_order = Logic('order');
        $result = $logic_order->getMemberOrderInfo($userId, "105198");
        return [new cartGoodsInfo($result["data"]["order_info"])];
    }



    /**
     * 根据用户id数组获取用户购物车信息
     * @param list<i32> userId 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $userId
     * @return \Server\Rpc\Service\ShopNc\cartGoodsInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function cartDel($userId, $cartId)
    {
        // TODO: Implement getCartList() method.
        $logic_order = Logic('order');
        $result = $logic_order->getMemberOrderInfo($userId, "105198");
        return [new cartGoodsInfo($result["data"]["order_info"])];
    }



    /**
     * 根据用户id数组获取用户购物车信息
     * @param list<i32> userId 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $userId
     * @return \Server\Rpc\Service\ShopNc\cartGoodsInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function cartEdit($userId, $cartId, $quantity)
    {
        // TODO: Implement getCartList() method.
        $logic_order = Logic('order');
        $result = $logic_order->getMemberOrderInfo($userId, "105198");
        return [new cartGoodsInfo($result["data"]["order_info"])];
    }

    /**
     * 根据用户id数组获取用户的基本信息
     * @param list<i32> userIds 用户id数组
     * @return list<UserInfo> 用户基本信息列表
     *
     *
     * @param int $artistId
     * @return \Server\Rpc\Service\ShopNc\OrderInfo[]
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getOrderInfoList($artistId)
    {
        // TODO: Implement getOrderInfoList() method.
    }

    /**
     * 下单详情页 获取用户购买信息 购物车 或者 单个商品的
     * @param string $token 用户token
     * @param string $cartId
     * @param int $ifCart
     * @param int $addressId
     * @param int $disId
     * @return string json 字符串
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getBuyInfo($token, $cartId, $ifCart = 0, $addressId = 0, $disId = 0)
    {
        try{
            $memberInfo = application()->userApi->getMemberInfoByToken($token);
            if(!$memberInfo){
                ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
            }
            return json_encode(application()->orderApi->getBuyInfo(
                $memberInfo['member_id'],
                $cartId,
                $ifCart,
                $memberInfo['store_id'],
                $addressId,
                $disId
            ));
        }catch (ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch (\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }

    /**
     * 下单第二步 创建订单
     *
     * @param string $token
     * @param string $ifCart
     * @param string $cartId
     * @param string $addressId
     * @param string $vatHash
     * @param string $offPayHash
     * @param string $offPayHashBatch
     * @param string $payName
     * @param string $invoiceId
     * @param string $rpt
     * @param string $pdPay
     * @param string $rcbPay
     * @param string $pointsPay
     * @param string $password
     * @param string $fcode
     * @param string $voucherStr
     * @param string $payMessage
     * @param string $orderFrom
     * @return string
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function createOrder($token, $ifCart, $cartId, $addressId, $vatHash,
                                $offPayHash, $offPayHashBatch, $payName, $invoiceId, $rpt,
                                $pdPay, $rcbPay, $pointsPay, $password, $fcode, $voucherStr,
                                $payMessage, $orderFrom)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        try{
            $payInfo = application()->orderApi->createOrder(
                $memberInfo['member_id'],$ifCart, $cartId, $addressId, $vatHash,
                $offPayHash, $offPayHashBatch, $payName, $invoiceId, $rpt,
                $pdPay, $rcbPay, $pointsPay,$password, $fcode, $voucherStr,
                $payMessage, $orderFrom
            );
            return json_encode($payInfo);
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }

    /**
     * 获得支付信息 下单第三步
     *
     * @param string $token
     * @param string $paySn
     * @return string
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getPayInfo($token, $paySn)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        try{
            $payInfo = application()->orderApi->getPayInfo($memberInfo['member_id'], $paySn);
            return json_encode($payInfo);
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }

    /**
     * 支付宝支付 下单最后一(四)步
     *
     * @param string $token
     * @param string $paySn
     * @param $pdPay
     * @param $rcbPay
     * @param $pointsPay
     * @param $password
     * @return string
     * @throws ServiceException
     */
    public function alipayNativePay($token, $paySn, $pdPay, $rcbPay, $pointsPay, $password)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        try{
            $payInfo = application()->orderApi->toAlipay($memberInfo['member_id'], $paySn, [
                'points_pay'=>$pointsPay,
                'rcb_pay' => $rcbPay,
                'pd_pay' => $pdPay,
                'password' => $password
            ]);
            return json_encode($payInfo);
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }
    /**
     * 微信支付 下单最后一(四)步
     *
     * @param string $token
     * @param string $paySn
     * @param $pdPay
     * @param $rcbPay
     * @param $pointsPay
     * @param $password
     * @return string
     * @throws ServiceException
     */
    public function wxAppPay($token, $paySn, $pdPay, $rcbPay, $pointsPay, $password)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        try{
            $payInfo = application()->orderApi->toWxPay($memberInfo['member_id'], $paySn, [
                'points_pay'=>$pointsPay,
                'rcb_pay' => $rcbPay,
                'pd_pay' => $pdPay,
                'password' => $password
            ]);
            return json_encode($payInfo);
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }

    /**
     * 获得最新的支付完成订单
     * @param $token
     * @return null|string
     * @throws ServiceException
     */
    public function getNewestOrderPayedInfo($token)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $where = [
            ["buyer_id", $memberInfo["member_id"]],
            ["order_state", ShopncOrder::ORDER_STATE_PAID]
        ];
        $orderInfo = application()->orderApi->getOrderInfoByWhere($where, ["pay_sn", "order_id", "order_amount", "order_sn"],["payment_time"=>"desc"]);
        if(!$orderInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "没有最新支付的订单");
        }
        return json_encode(application()->orderApi->getSumPayInfoByPaySn($orderInfo["pay_sn"]));
    }

    /**
     * 获得用户订单列表
     *
     * @param string $token
     * @param $page
     * @param int $pageSize
     * @param int $status -1.所有 10.待付款 30.待收货(20.待发货 归到 待收货 ---- 加一个提示正在发货) 40.已完成 0.已取消
     * @return string
     * @throws ServiceException
     */
    public function getUserOrderList($token, $page, $pageSize=10, $status = -1)
    {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $where = ["buyer_id"=>$memberInfo['member_id']];
        $where["delete_state"] = 0;
        if($status != -1){
            if($status == ShopncOrder::ORDER_STATE_DELIVERED){
                //30.待收货(20.待发货,待发货 归到 待收货 ---- 加一个提示正在发货)
                $where['order_state'] = function ($query){
                    $query->whereIn("order_state", [ShopncOrder::ORDER_STATE_DELIVERED, ShopncOrder::ORDER_STATE_PAID]);
                };
            }else{
                $where['order_state'] = $status;
            }
        }
        //拍卖订单排除状态未付款的 产品确认 20190326
        //$where['order_state_auction_exclude'] = function ($query){
          //  $query->whereRaw("if (`order_type`=?, `order_state` <> ?,1=1)",[ShopncOrder::ORDER_TYPE_AUCTION,ShopncOrder::ORDER_STATE_UN_PAY]);
        //};
        $data = application()->orderApi->getList($pageSize*($page-1), $pageSize, $where, [
            "order_sn", "order_id", "pay_sn", "shipping_code", "order_type","auction_id",
            "store_id", "store_name", "buyer_id", "buyer_name", "add_time","artist_id",
            "add_time", "payment_time", "finnshed_time", "goods_amount","is_points","points_amount",
            "order_amount", "order_state"]);
        foreach ($data['list'] as &$orderInfo) {
            //允许取消的时间
            if($orderInfo['order_state'] == ShopncOrder::ORDER_STATE_UN_PAY){
                if($orderInfo['order_type'] != ShopncOrder::ORDER_TYPE_AUCTION) {
                    //非拍卖订单
                    $orderInfo['order_cancel_day'] = ($orderInfo['add_time'] + ORDER_AUTO_CANCEL_TIME * 3600) - time() - 3;
                }else{
                    //拍卖订单
                    $orderInfo['order_cancel_day'] = ($orderInfo['add_time'] + AUCTION_ORDER_AUTO_CANCEL_TIME * 3600) - time() - 3;
                }
            }
            //商品列表
            $orderInfo['goodsList'] = application()->orderApi->getOrderGoodsList($orderInfo['order_id'],
                ["goods_id", "goods_name", "goods_price", "goods_pay_price", "goods_num", "goods_image", "store_id","artwork_id"]);
            if($orderInfo['goodsList'] ){
                foreach ($orderInfo['goodsList'] as &$orderGoods){
                    $orderGoods['goods_image'] = cthumb($orderGoods['goods_image'], 360, $orderGoods['store_id']);
                }
            }
            //拍卖订单 从保证金里取商品信息
            else if(!empty($orderInfo['auction_id'])){
                $orderInfo['goodsList'] = [];
                $marginOrderInfo = Model("margin_orders")->getOrderInfo([
                    'auction_order_id'=>$orderInfo['order_id']
                ]);
                if($marginOrderInfo){
                    $orderInfo['goodsList'][] = [
                        'goods_image' => cthumb($marginOrderInfo['auction_image'], 360),
                        'goods_name' => $marginOrderInfo['auction_name'],
                        'goods_num' => 1,
                        'goods_price' => $orderInfo['goods_amount'],
                        'goods_pay_price' => $orderInfo['goods_amount']
                    ];
                    $orderInfo['store_name'] = getMainConfig('shop.auctionShopName', "拍卖");
                    $orderInfo['store_url']=AUCTION_URL;
                }
            }
        }
        $data['maxPage'] = ceil($data['count']/$pageSize);
        $data['nowPage'] = $page;
        return json_encode($data);
    }


    /**
     * 取消订单
     * @param $token
     * @param $orderId
     * @return bool
     * @throws ServiceException
     */
    public function orderCancelOp($token, $orderId) {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $model_order = Model('order');
        $logic_order = Logic('order');

        $condition = array();
        $condition['order_id'] = $orderId;
        $condition['buyer_id'] = $memberInfo['member_id'];
        $condition['order_type'] = array('in',array(1,3,4));
        $order_info = $model_order->getOrderInfo($condition, ['order_goods']);
        $if_allow = $model_order->getOrderOperateState('buyer_cancel',$order_info);
        if (!$if_allow) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL,'无权操作');
        }
        $nowTime = time();
        if ($nowTime - ORDER_CANCEL_TIMES < $order_info['api_pay_time'] && $order_info['payment_code'] != "underline") {
            $_hour = ceil(($order_info['api_pay_time']+ORDER_CANCEL_TIMES-$nowTime)/3600);
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL,'该订单曾尝试使用第三方支付平台支付，须在'.$_hour.'小时以后才可取消');
        }
        $result = $logic_order->changeOrderStateCancel($order_info, 'buyer', $memberInfo['member_name'], '其它原因');
        if(!$result['state']) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $result['msg']);
        }
        $orderGoodsList = $order_info['extend_order_goods'];
        if (is_array($orderGoodsList) && count($orderGoodsList) > 0) {
            foreach ($orderGoodsList as $goosInfo) {
                if (!empty($goosInfo['artwork_id'])) {
                    application()->artistService->syncArtworkStatus($goosInfo['artwork_id'], 1, "");
                }
                continue;
            }
        }
        return true;
    }

    /**
     * 订单确认收货
     * @param $token
     * @param $orderId
     * @return bool
     * @throws ServiceException
     */
    public function orderReceiveOp($token, $orderId) {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $model_order = Model('order');
        $logic_order = Logic('order');

        $condition = array();
        $condition['order_id'] = $orderId;
        $condition['buyer_id'] = $memberInfo['member_id'];
        $condition['order_type'] = ['in','1,4'];
        $order_info = $model_order->getOrderInfo($condition, ['order_goods']);
        $if_allow = $model_order->getOrderOperateState('receive',$order_info);
        if (!$if_allow) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL,'无权操作');
        }

        $result = $logic_order->changeOrderStateReceive($order_info,'buyer', $memberInfo['member_name'],'签收了货物');
        if(!$result['state']) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL,$result['msg']);
        } else {
            $orderGoodsList = $order_info['extend_order_goods'];
            if (is_array($orderGoodsList) && count($orderGoodsList) > 0) {
                foreach ($orderGoodsList as $goosInfo) {
                    if (!empty($goosInfo['artwork_id'])) {
                        application()->artistService->syncArtworkStatus($goosInfo['artwork_id'], 2, "");
                    }
                    continue;
                }
            }
            return true;
        }
    }

    /**
     * 移除订单
     * @param $token
     * @param $orderId
     * @return bool
     * @throws ServiceException
     */
    public function orderDeleteOp($token, $orderId) {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $model_order = Model('order');
        $logic_order = Logic('order');

        $condition = array();
        $condition['order_id'] = $orderId;
        $condition['buyer_id'] = $memberInfo['member_id'];
        $condition['order_type'] = array('in',array(1,3,4));
        $order_info = $model_order->getOrderInfo($condition);
        $if_allow = $model_order->getOrderOperateState('delete',$order_info);
        if (!$if_allow) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, '无权操作');
        }

        $result = $logic_order->changeOrderStateRecycle($order_info,'buyer','delete');
        if(!$result['state']) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $result['msg']);
        } else {
            return true;
        }
    }

    /**
     * 订单列表--我的拍卖
     * @param $token
     * @param $page
     * @param int $pageSize
     * @param $stateType string list_all, list_have, list_die
     * @return false|string
     * @throws ServiceException
     */
    public function auctionOrderListOp($token, $page, $pageSize = 10, $stateType = "list_all") {
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $member_id = $memberInfo['member_id'];
        $logic_auction_order = Logic('auction_order');
        $begin = ($page - 1) * $pageSize;
        $limit = $begin . ',' . $pageSize;
        if ($member_id == 0) {
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, '会员不存在');
        }
        $margin_state = array(0,1,2,3,5);
        // 获取订单列表
        switch ($stateType) {
            case 'list_all'://所有保证金订单
                $result = $logic_auction_order->auctionListFetch($member_id, '', '','', [], $limit);
                break;
            case 'list_ing'://参拍
                $result = $logic_auction_order->auctionListFetch($member_id, '', '','',array(1,3), $limit);
                break;
            case 'list_have'://已拍下
                $result = $logic_auction_order->auctionListFetch($member_id, 0, 1,null,$margin_state, $limit);
                break;
            case 'list_die'://未拍中
                $result = $logic_auction_order->auctionListFetch($member_id, 1, ['neq',1],null,$margin_state, $limit);
                break;
        }
        $page_count = $result['page_count'];
        $result['maxPage'] = ceil($page_count/$pageSize);
        $result['nowPage'] = $page;
        return json_encode($result);
    }




    /**
     * 获得艺术家订单列表
     *
     * @param integer $artistId
     * @param $page
     * @param int $pageSize
     * @param int $status -1.所有 10.待付款 20.待发货 30.待收货 40.已完成 0.已取消
     * @return string
     * @throws ServiceException
     */
    public function getOrderListByArtistId($artistId, $page, $pageSize=10, $status = -1)
    {
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo ||  $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $where = ["store_id"=>$storeInfo->store_id];
        if($status != -1){
            $where['order_state'] = $status;
        }
        $data = application()->orderApi->getList($pageSize*($page-1), $pageSize, $where, ["order_sn", "order_id", "pay_sn",
            "store_id", "store_name", "buyer_id", "buyer_name", "order_type","auction_id",
            "add_time", "payment_time", "finnshed_time", "goods_amount","artist_id",
            "order_amount", "order_state"]);
        foreach ($data['list'] as &$orderInfo) {
            //商品列表
            $orderInfo['goodsList'] = application()->orderApi->getOrderGoodsList($orderInfo['order_id'],
                ["goods_id", "goods_name", "goods_price", "goods_pay_price", "goods_num", "goods_image", "store_id", "artwork_id"]);
            if($orderInfo['goodsList'] ){
                foreach ($orderInfo['goodsList'] as &$orderGoods){
                    $orderGoods['goods_image'] = cthumb($orderGoods['goods_image'], 360, $orderGoods['store_id']);
                }
            }
            //拍卖订单 从保证金里取商品信息
            if(!empty($orderInfo['auction_id']) && empty($orderInfo['goodsList'])){
                $orderInfo['goodsList'] = [];
                $marginOrderInfo = Model("margin_orders")->getOrderInfo([
                    'auction_order_id'=>$orderInfo['order_id']
                ]);
                if($marginOrderInfo){
                    $orderInfo['goodsList'][] = [
                        'goods_image_url' => cthumb($marginOrderInfo['auction_image'], 360),
                        'refund' => $marginOrderInfo['refund_state']== \margin_ordersModel::REFUND_STATE_REFUNDED ? true : false,
                        'goods_name' => $marginOrderInfo['auction_name'],
                        'goods_num' => 1,
                        'goods_price' => $orderInfo['goods_amount']
                    ];
                    $orderInfo['store_name'] = getMainConfig('shop.auctionShopName', "拍卖");
                }
                $orderInfo['store_url']=AUCTION_URL;
            }else{
                if($orderInfo['artist_id']>0){
                    //如果有艺术家id 跳转到艺术家页面
                    $orderInfo['store_url'] = YSJ_URL . '/ynh/getUserIdByArtistId?artistId=' . $orderInfo["artist_id"] . '&api_token=' . $_REQUEST['key'];
                }
                foreach ($orderInfo['goodsList'] as $k => $goods_info) {
                    $orderInfo['goodsList'][$k]['goods_image_url'] = cthumb($goods_info['goods_image'], 240, $orderInfo['store_id']);
                    $orderInfo['goodsList'][$k]['refund'] = $orderInfo['goodsList'][$k]['refund'] ? true : false;
                    unset($orderInfo['goodsList'][$k]['rec_id']);
                    unset($orderInfo['goodsList'][$k]['order_id']);
                    unset($orderInfo['goodsList'][$k]['goods_pay_price']);
                    unset($orderInfo['goodsList'][$k]['store_id']);
                    unset($orderInfo['goodsList'][$k]['buyer_id']);
                    unset($orderInfo['goodsList'][$k]['promotions_id']);
                    unset($orderInfo['goodsList'][$k]['commis_rate']);
                    unset($orderInfo['goodsList'][$k]['gc_id']);
                    unset($orderInfo['goodsList'][$k]['goods_contractid']);
                    unset($orderInfo['goodsList'][$k]['goods_image']);
                    if ($orderInfo['goodsList'][$k]['goods_type'] == 5) {
                        $orderInfo['zengpin_list'][] = $orderInfo['goodsList'][$k];
                        unset($orderInfo['goodsList'][$k]);
                    }
                }
            }
        }
        $data['maxPage'] = ceil($data['count']/$pageSize);
        $data['nowPage'] = $page;
        return json_encode($data);
    }

    /**
     * 发往中转仓
     * 发往中转仓后订单状态变为待收货 世伟确认 20190328
     * @param integer $artistId
     * @param $orderId
     * @param $expressCompany
     * @param $expressNumber
     * @return string
     * @throws ServiceException
     */
    public function artistOrderToTransitShipment($artistId, $orderId, $expressCompany, $expressNumber)
    {
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo || $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $where = ["store_id" => $storeInfo['store_id']];
        $where["order_id"] = $orderId;
        $orderInfo = application()->orderApi->getOrderInfo($orderId);
        $orderCommonInfo = application()->orderApi->getOrderCommonInfo($orderId);
        //订单不存在或者不是当前店铺的
        if(!$orderInfo || !$orderCommonInfo || $orderCommonInfo->store_id != $storeInfo['store_id']){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "订单不存在");
        }
        //状态不允许发货
        if($orderInfo->order_state !=  ShopncOrder::ORDER_STATE_PAID || $orderCommonInfo->transit_shipment_status != ShopncOrderCommon::TRANSIT_SHIPMENT_STATUS_INIT){
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR, "当前状态不允许发货");
        }
        $orderCommonInfo->transit_shipment_express_company = $expressCompany;
        $orderCommonInfo->transit_shipment_express_number = $expressNumber;
        $orderCommonInfo->transit_shipment_delivery_time = date('Y-m-d H:i:s');
        $orderCommonInfo->transit_shipment_status = ShopncOrderCommon::TRANSIT_SHIPMENT_STATUS_DELIVERED;
        $orderInfo->order_state = ShopncOrder::ORDER_STATE_DELIVERED;
        $orderInfo->delay_time = time();
        $orderInfo->save();
        return $orderCommonInfo->save();
    }

/*    public function getOrderExtInfo($orderId){
        //扩展信息
        $orderExtInfo = application()->orderApi->getOrderCommonInfo($orderId, [
            "shipping_express_id", "shipping_time", "transit_shipment_express_company", "transit_shipment_status",
            "transit_shipment_express_number", "transit_shipment_delivery_time", "transit_shipment_receiving_time",
        ]);
        //物流信息
        if($orderExtInfo){
            if(!empty($orderExtInfo->shipping_express_id)){
                $orderExtInfo[""]
            }
        }
    }*/

    /**
     * 物流信息
     * @return static
     */
    public function expressAllList()
    {
        $storeInfo = application()->expressApi->getExpressList(0, 200, ["e_state"=>"1"], ["id", "e_name"], false);
        return json_encode($storeInfo["list"]);
    }

    /**
     * 获得艺术家结算列表订单列表
     * 这里商品实际的金额 扣除比例要考虑到订单改价后的比例 order_amount_original !=null && order_amount_original != order_amount 改价了
     * @param $artistId
     * @param $page
     * @param $pageSize
     * @param int $type -1.所有 1.待结算 2.已结算
     * @return false|string
     * @throws ServiceException
     */
    public function getArtistClosingOrderList($artistId, $page, $pageSize, $type){
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo ||  $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $now = time();
        $artistOrderClosingConfig = config("artist-order-closing");
        $where[] = ["store_id",$storeInfo->store_id];
        //订单状态：发完货以后算待结算 20:已付款 30:已发货;40:已收货;
        //zsw 已变更为已付款为待结算
        $where[] = function($query){
            $query->whereIn("order_state", [ShopncOrder::ORDER_STATE_PAID, ShopncOrder::ORDER_STATE_RECEIVED, ShopncOrder::ORDER_STATE_DELIVERED]);
        };

        if($type == 1){
            $where[] =["artist_closing_status", ShopncOrder::ARTIST_CLOSING_STATUS_UN];
        }
        if($type == 2){
            $where[] = ["artist_closing_status", ShopncOrder::ARTIST_CLOSING_STATUS_DONE];
        }
        $noSettlementAmount = 0;
        $data = application()->orderApi->getList($pageSize*($page-1), $pageSize, $where, ["order_sn", "order_id", "pay_sn",
            "store_id", "store_name", "buyer_id", "buyer_name","order_amount_original", "artist_closing_time", "auction_id",
            "add_time", "payment_time", "finnshed_time", "goods_amount","artist_closing_status",
            "order_amount", "order_state"]);
        foreach ($data['list'] as &$orderInfo) {
            //商品列表
            $orderInfo['goodsList'] = application()->orderApi->getOrderGoodsList($orderInfo['order_id'],
                ["goods_id", "goods_name", "goods_price", "goods_pay_price", "goods_num", "goods_image", "store_id", "artwork_category_id"]);
            if($orderInfo['goodsList']){
                foreach ($orderInfo['goodsList'] as &$orderGoods){
                    $orderGoods['goods_image'] = cthumb($orderGoods['goods_image'], 360, $orderGoods['store_id']);
                }
            }
            //拍卖订单 从保证金里取商品信息
            if(!empty($orderInfo['auction_id']) && empty($orderInfo['goodsList'])){
                $orderInfo['goodsList'] = [];
                $marginOrderInfo = Model("margin_orders")->getOrderInfo([
                    'auction_order_id'=>$orderInfo['order_id']
                ]);
                if($marginOrderInfo){
                    $orderInfo['goodsList'][] = [
                        'goods_image_url' => cthumb($marginOrderInfo['auction_image'], 360),
                        'goods_image' => cthumb($marginOrderInfo['auction_image'], 360),
                        'refund' => $marginOrderInfo['refund_state']== \margin_ordersModel::REFUND_STATE_REFUNDED ? true : false,
                        'goods_name' => $marginOrderInfo['auction_name'],
                        'goods_num' => 1,
                        'goods_price' => $orderInfo['goods_amount'],
                        'goods_pay_price' => $orderInfo['goods_amount'],
                        'artwork_category_id' => Model("auctions")->getArtworkCategoryByAuctionId($orderInfo['auction_id'])
                    ];
                    $orderInfo['store_name'] = getMainConfig('shop.auctionShopName', "拍卖");
                }

                $orderInfo['store_url']=AUCTION_URL;
            }else{
                if($orderInfo['artist_id']>0){
                    //如果有艺术家id 跳转到艺术家页面
                    $orderInfo['store_url'] = YSJ_URL . '/ynh/getUserIdByArtistId?artistId=' . $orderInfo["artist_id"] . '&api_token=' . $_REQUEST['key'];
                }
                foreach ($orderInfo['goodsList'] as $k => $goods_info) {
                    $orderInfo['goodsList'][$k]['goods_image_url'] = cthumb($goods_info['goods_image'], 240, $orderInfo['store_id']);
                    $orderInfo['goodsList'][$k]['refund'] = $orderInfo['goodsList'][$k]['refund'] ? true : false;
                    if ($orderInfo['goodsList'][$k]['goods_type'] == 5) {
                        $orderInfo['zengpin_list'][] = $orderInfo['goodsList'][$k];
                        unset($orderInfo['goodsList'][$k]);
                    }
                }
            }
            //订单费率总信息
            $orderFeeSumInfo = ["sum"=>"0.00", "categoryInfo"=>[]];
            foreach ($orderInfo['goodsList'] as &$orderGoods){
                //计算费率
                $nowOrderGoodsFeeInfo = application()->orderApi->getOrderGoodsClosingFeeInfo(
                    $orderGoods['goods_pay_price'],
                    $orderGoods['artwork_category_id'],
                    $orderInfo['order_amount'],
                    $orderInfo['order_amount_original']
                );
                if(empty($orderGoods['artwork_category_id'])){
                    $orderGoods['artwork_category_id'] = 9999999;
                }
                !isset($orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]) && $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']] = ["sum"=>"0.00", "cName"=>$nowOrderGoodsFeeInfo['cName']];
                $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]["sum"] = bcadd($nowOrderGoodsFeeInfo['feeAmount'], $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]["sum"], 4);
                $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]["rate"] = $nowOrderGoodsFeeInfo['rate'];
                $orderGoods['goods_image'] = cthumb($orderGoods['goods_image'], 360, $orderGoods['store_id']);
            }
            //遍历每个分类保留两位,四舍五入,计算总金额
            foreach ($orderFeeSumInfo["categoryInfo"] as &$item) {
                $item["sum"] = round($item["sum"], 2);
                $orderFeeSumInfo["sum"]=bcadd($item["sum"], $orderFeeSumInfo["sum"], 2);
            }
            //扁平化categoryInfo
            if($orderFeeSumInfo['categoryInfo']){
                $orderFeeSumInfo['categoryInfo'] = ArrayHelper::mapToList($orderFeeSumInfo['categoryInfo'], "artwork_category_id");
            }
            $orderInfo['feeSumInfo'] = $orderFeeSumInfo;
            //结算金额
            $orderInfo['order_amount_closing'] = bcsub($orderInfo['order_amount'], $orderFeeSumInfo['sum'], 2);
            $noSettlementAmount += $orderInfo['order_amount_closing'];
        }
        $data['noSettlementAmount'] = $noSettlementAmount;
        $data['maxPage'] = ceil($data['count']/$pageSize);
        $data['nowPage'] = $page;
        return json_encode($data);
    }
    /**
     * 获得艺术家待结算总金额
     * 这里商品实际的金额 扣除比例要考虑到订单改价后的比例 order_amount_original !=null && order_amount_original != order_amount 改价了
     * @param $artistId
     * @return double
     * @throws ServiceException
     */
    public function getArtistClosingWaitSum($artistId){
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo ||  $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $closeAccountInfo = application()->closeAccountApi->getCloseAccountInfo($storeInfo->store_id, ["wait_amount"]);
        return $closeAccountInfo ? $closeAccountInfo->wait_amount : 0;
    }

    /**
     * 艺术家去结算咯
     * 这里商品实际的金额 扣除比例要考虑到订单改价后的比例 order_amount_original !=null && order_amount_original != order_amount 改价了
     * @param $token string 这里调用层确保token是 $artistId 的token
     * @param $artistId
     * @return double
     * @throws ServiceException
     * @throws \Throwable
     */
    public function artistToClosing($token, $artistId){
        $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
        if(!$memberInfo){
            ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
        }
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo ||  $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $closeAccountInfo = application()->closeAccountApi->getCloseAccountInfo($storeInfo->store_id, ["wait_amount"]);
        if(!$closeAccountInfo || $closeAccountInfo->wait_amount <= 0){
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR, "没有需要结算的金额");
        }
        try{
            $closingNo = ToolsHelper::uDate("YmdHisu").ToolsHelper::randomNumber(2);
            //更新order表
            $ret = application()->closeAccountApi->minusAccountMoney($storeInfo->store_id, $closeAccountInfo->wait_amount, ShopncCloseAccountLog::TRANSACTION_TYPE_TO_MEMBER_ACCOUNT,  $closingNo, "艺术家结算到余额提现");
            //更新余额
            application()->accountApi->addAccountMoney($memberInfo['member_id'], $memberInfo['member_name'], $closeAccountInfo->wait_amount, ShopncPdLog::LG_TYPE_ARTIST_CLOSING, $ret["logId"], "user", "艺术家结算到余额提现");
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
        return $closeAccountInfo->wait_amount;
    }
    /**
     * 获得艺术家结算明细
     * @param $artistId
     * @param $page
     * @param $pageSize
     * @return false|string
     * @throws ServiceException
     */
    public function getArtistClosingLog($artistId, $page, $pageSize){
        $storeInfo = application()->storeApi->getStoreInfoByArtistId($artistId, ['store_id']);
        if(!$storeInfo ||  $storeInfo->store_id < 1){
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, "艺术家没有店铺");
        }
        $where = ["store_id"=>$storeInfo->store_id];
        $where["status"] = ShopncCloseAccountLog::STATUS_VALID;
        $data = application()->closeAccountApi->getLogList($pageSize*($page-1), $pageSize, $where,
            ["id","store_id","transaction_type","transaction_source","before_wait_amount","change_wait_amount","later_wait_amount", "io_type", "note","created_at"]);
        $data['maxPage'] = ceil($data['count']/$pageSize);
        $data['nowPage'] = $page;
        return json_encode($data);
    }
}
