<?php
namespace App\Rpc;

use App\Models\AppApiAllModel;
use App\Models\AppApiModel;
use App\Models\AppModel;
use App\Services\Constant\ApiResponseCode;
use Server\Rpc\Service\Oauth\OauthServiceIf;
use Server\Rpc\Service\Oauth\ResponseInfo;

//class OauthServiceHandler implements OauthServiceIf {
class OauthServiceHandler{

    /**
     * 通过平台appid，用户登录token获取用户信息
     * @param string $appid
     * @param string $uri
     * @param string $accessToken
     * @return \Server\Rpc\Service\Oauth\ResponseInfo
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function checkApiPermission($appid, $uri, $accessToken)
    {
        if(!$accessToken){
            return $this->responseInfo(ApiResponseCode::APP_ACCESS_TOKEN_ERROR);
        }
        $userAppId = application()->apiOauthApi->getAppIdByAccessToken($accessToken);
        if(!$userAppId){
            return $this->responseInfo(ApiResponseCode::APP_ACCESS_TOKEN_ERROR);
        }
        $appModel = new AppModel();
        $userAppInfo = application()->thirdPartyApi->getAppInfo($userAppId,
            [$appModel->getKeyFullName('id'), $appModel->getKeyFullName('status')]);
        // app 未申请完成
        if(empty($userAppInfo) || $userAppInfo['status'] != AppModel::STATUS_APPLY_SUCCESS){
            return $this->responseInfo(ApiResponseCode::APP_STATUS_NOT_OPEN);
        }
        $platformInfo = application()->adminPlatformApi->getPlatformInfoByAppid($appid);
        if(!$platformInfo){
            return $this->responseInfo(ApiResponseCode::PLATFORM_NOT_EXIST);
        }
        $appApiAllModel = new AppApiAllModel();
        $apiInfo = application()->thirdPartyApiApi->getApiInfoByPlatformIdUri($platformInfo['id'], $uri, [
            $appApiAllModel->getKeyFullName('id'),
            $appApiAllModel->getKeyFullName('api_type'),
            $appApiAllModel->getKeyFullName('status')
        ]);
        //api未开放
        if($apiInfo['status'] != AppApiAllModel::STATUS_RELEASED){
            return $this->responseInfo(ApiResponseCode::APP_API_NOT_OPEN);
        }
        // 不是公共api判断app是否申请api
        if($apiInfo['api_type'] != AppApiAllModel::API_TYPE_PUBLIC){
            $appApiInfo = application()->thirdPartyApi->getAppApiInfo($userAppInfo['id'], $apiInfo['id'], ['status']);
            if(!$appApiInfo || $appApiInfo['status'] != AppApiModel::STATUS_APPLY_SUCCESS){
                return $this->responseInfo(ApiResponseCode::APP_APP_API_NOT_PERMISSION);
            }
        }
        return $this->responseInfo(ApiResponseCode::GOD_BLESS_YOU);
    }
    protected function responseInfo($code, $msg = ''){
        $responseData = ['code'=>$code, 'msg'=>$msg ? $msg : ApiResponseCode::getMsg($code)];
        return new ResponseInfo($responseData);
    }
}
