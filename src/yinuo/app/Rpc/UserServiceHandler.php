<?php
/**
 * Created by PhpStorm.
 * User: xuhui
 * Date: 17/5/11
 * Time: 17:14
 */
namespace App\Rpc;

use Server\Rpc\Common\Shared\ServiceExceptionCode;
use Server\Rpc\Common\Shared\ServiceException;
use App\Rpc\Exception\ServiceExceptionTools;
use App\Lib\Helpers\LogHelper;

class UserServiceHandler extends BaseHandler implements \Server\Rpc\Service\ShopNc\UserServiceIf {

    /**
     * 银行卡个数
     * @return int 绑卡个数
     * @param int $token
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getBankcardCount($token)
    {
        try{
            $memberInfo = application()->userApi->getUserTokenInfoByToken($token);
            if(!$memberInfo){
                ServiceExceptionTools::throwError(ServiceExceptionCode::USER_NO_EXISTS);
            }
            return application()->userApi->getBankcardCount($memberInfo['member_id']);
        }catch(ServiceException $e){
            throw $e;
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }

}
