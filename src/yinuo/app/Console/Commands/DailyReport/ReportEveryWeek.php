<?php
namespace App\Console\Commands\DailyReport;

use Illuminate\Console\Command;

/**
 * Class ReportEveryday 日常报表
 * @package App\Console\Commands\DailyReport
 */
class ReportEveryWeek extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dailyReport:reportEveryday';

    protected $signature='dailyReport:reportEveryday {day=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '统计:每日数据报表';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //每次更新请更新版本
        $version = 1;
        $pdo = \DB::getPdo();
        //统计前几天
        $before_day=$this->argument('day');
        $nowTime = time();
        while($before_day){
            $yesterday = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime('-'.($before_day-1).' days', $nowTime))));
            $today=date('Y-m-d H:i:s',strtotime(date('Y-m-d',strtotime('+'.($before_day).' days', $nowTime))));
            $yesterdaytime=strtotime($yesterday);
            $todaytime=strtotime($today);

            $context = [];
            //今日 新增用户
            $sql = "SELECT count(*) FROM `shopnc_member` WHERE `member_time` >= $yesterdaytime AND `member_time` <= $todaytime";
            $context['新增用户'] = $pdo->query($sql)->fetchColumn() ?: 0;
            //今日 活跃用户
            $sql = "SELECT count(*) FROM `shopnc_member` WHERE `member_old_login_time` >= $yesterdaytime AND `member_old_login_time` <= $todaytime";
            $context['活跃总数'] = $pdo->query($sql)->fetchColumn() ?: 0;
            //今日 老用户
            $context['老用户'] = $context['活跃总数'] - $context['新增用户'];

            //保证金 ----
            $sql = "select sum(margin_amount) 总金额, sum(api_pay_amount) 微信, sum(case when refund_state = 1 then margin_amount else 0 end) 退回金额, sum(pd_amount) 余额  from shopnc_margin_orders where payment_time >= {$yesterdaytime}  AND `payment_time` <= $todaytime";
            $context['保证金'] = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);

            //竞价频率 ----
            $sql = "select count(*) 竞价次数, count(distinct member_id) 竞价人数, max(offer_num) 最高竞价, min(offer_num) 最低竞价 from shopnc_bid_log where member_id > 0 AND created_at >= {$yesterdaytime}  AND `created_at` <= $todaytime";
            $bidResult = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            $bidResult['平均用户竞价次数']  = 0;
            if($bidResult && $bidResult['竞价次数'] > 0){
                //平均出价次数
                $bidResult['平均用户竞价次数'] = bcdiv($bidResult['竞价次数'], $bidResult['竞价人数'], 2);
            }
            $context['竞价频率'] = $bidResult;

            //返佣 //返佣来源类型:1商品订单2拍卖3下级保证金返佣4合伙人升级5艺术家入驻6保证金收益利息7竞拍返佣8成交返佣9保证金利息返佣 ------
            $sql = <<<EOD
SELECT
	sum( commission_amount) 总金额,
	sum( case when from_member_id = dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 10 Then commission_amount else 0 end ) 自身竞价10,
	sum( case when from_member_id = dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 12 Then commission_amount else 0 end ) 自身竞价12,
	sum( case when from_member_id = dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 15 Then commission_amount else 0 end ) 自身竞价15,
	sum( case when from_member_id = dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 18 Then commission_amount else 0 end ) 自身竞价18,
	sum( case when from_member_id = dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 20 Then commission_amount else 0 end ) 自身竞价20,
	
	sum( case when from_member_id <> dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 12 Then commission_amount else 0 end ) 下级返佣12,
	sum( case when from_member_id <> dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 14 Then commission_amount else 0 end ) 下级返佣14,
	sum( case when from_member_id <> dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 18 Then commission_amount else 0 end ) 下级返佣18,
	sum( case when from_member_id <> dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 20 Then commission_amount else 0 end ) 下级返佣20,
	sum( case when from_member_id <> dis_member_id AND commission_type in (2,3,7) AND dis_commis_rate = 22 Then commission_amount else 0 end ) 下级返佣22,
	
	sum( case when commission_type=8 AND dis_commis_rate = 20 Then commission_amount else 0 end ) 成交返佣20
FROM
	shopnc_member_commission 
WHERE
	dis_commis_state = 1 
	AND commission_time >= $yesterdaytime
	AND commission_time <= $todaytime;
EOD;
;
            $commissionAmount = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            $context['返佣'] = $commissionAmount;

            //成交价 ----
            $sql = "select min(order_amount) 最高, min(order_amount) 最低 from shopnc_orders where order_type = 4 AND add_time >= {$yesterdaytime}  AND `add_time` <= $todaytime";
            $orderAuctionAmount = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            $context['成交价'] = $orderAuctionAmount;

            //总交易额  拍卖今日订单已付 拍卖今日订单待付 ----
            $sql = <<<EOD
            select sum(case when order_state in (10, 20, 30, 40) THEN order_amount ELSE 0 end ) 总金额,
            count(case when order_state in (10, 20, 30, 40) THEN 1 ELSE null end ) 笔数,
            sum(case when order_type<>4 and order_state in (20, 30, 40) THEN order_amount ELSE 0 end ) 商城,
            sum(case when order_type=4 THEN order_amount ELSE 0 end ) 拍卖,
            sum(case when order_type=4 and order_state in (20, 30, 40) THEN order_amount ELSE 0 end ) 拍卖今日订单已付总金额,
            count(case when order_type=4 and order_state in (20, 30, 40) THEN 1 ELSE null end ) 拍卖今日订单已付笔数,
            sum(case when order_type=4 and order_state in (10) THEN order_amount ELSE 0 end ) 拍卖今日订单待付总金额,
            count(case when order_type=4 and order_state in (10) THEN 1 ELSE null end ) 拍卖今日订单待付笔数
             from shopnc_orders where add_time >= {$yesterdaytime} AND `add_time` <= $todaytime;
EOD;
            $orderAmount = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            $orderAmount['客单价'] = 0;
            if($orderAmount && $orderAmount['总金额'] > 0){
                //客单价
                $orderAmount['客单价'] = bcdiv($orderAmount['总金额'], $orderAmount['笔数'], 2);
            }

            //黄山数据
            $huangSanMember = '17681290402, 18855995525, 13665595153, 18268192665, 15623791393, 15623869370, 18855946650, 15205595125, 18755968930, 18055950765, 15212320813, 18655927758, 18255988808';
            $sql = <<<EOD
            SELECT
	sum(case when order_state in (10, 20, 30, 40) THEN order_amount ELSE 0 end ) 黄山交易金额,
	count(case when order_state in (10, 20, 30, 40) THEN 1 ELSE null end )  黄山拍卖笔数
FROM
	shopnc_orders o 
WHERE
	(o.buyer_id IN (
SELECT
	member_id 
FROM
	shopnc_member_distribute 
WHERE
	top_member IN (
SELECT
	member_id 
FROM
	shopnc_member m 
WHERE
	m.member_mobile IN ( $huangSanMember ) 
	) 
	) 
	or
	o.buyer_id IN 
	(
SELECT
	member_id 
FROM
	shopnc_member m 
WHERE
	m.member_mobile IN ( $huangSanMember ) 
	)  
	)
	 and o.add_time >= {$yesterdaytime} AND o.add_time <= $todaytime and o.order_type = 4 and o.order_state in (10, 20, 30, 40);
EOD;

            $orderHuangSanAmount = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            if($orderHuangSanAmount){
                $orderAmount = array_merge($orderAmount, $orderHuangSanAmount);
            }
            $context['总交易额'] = $orderAmount;

            //3日 ----
            $threeDay = $todaytime - 259200;
            $sql = <<<EOD
select 
sum(case when order_state = 10 THEN order_amount ELSE 0 end) 3日违约订单总金额,
count(case when order_state = 10 THEN 1 ELSE NULL end)  3日违约笔数,
#付款时间是今天的
sum(case when order_state in (20, 30, 40) and payment_time >= {$yesterdaytime}  AND `payment_time` <= $todaytime THEN order_amount ELSE 0 end) 3日已付总金额,
count(case when order_state in (20, 30, 40) and payment_time >= {$yesterdaytime}  AND `payment_time` <= $todaytime THEN 1 ELSE NULL end) 3日已付笔数
  from shopnc_orders where order_type = 4 AND add_time >= {$threeDay}  AND `add_time` <= $todaytime;
EOD;

            $threeOrderAmount = $pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
            $threeOrderAmount['3日违约扣除总金额']  = 0;
            if($threeOrderAmount && $threeOrderAmount['3日违约订单总金额'] > 0){
                //扣除总金额
                $threeOrderAmount['3日违约扣除总金额'] = bcmul($threeOrderAmount['3日违约订单总金额'], 0.5, 2);
            }
            //3日已付数据需减去今日的
            if($threeOrderAmount['3日已付总金额'] > 0 && $context['总交易额']['拍卖今日订单已付总金额'] > 0){
                $threeOrderAmount['3日已付总金额'] -= $context['总交易额']['拍卖今日订单已付总金额'];
                $threeOrderAmount['3日已付笔数'] -= $context['总交易额']['拍卖今日订单已付笔数'];
            }

            $context['3日数据'] = $threeOrderAmount;
            // 今日统计数据项入库
//            $nowtime = date('Y-m-d H:i:s', time());
            $calcDate = date('Ymd', strtotime('-'.($before_day-1).' days', $nowTime));
            $deleteSql="delete from shopnc_daily_report where calc_date='{$calcDate}'";
            $pdo->query($deleteSql);
            $contextJson = json_encode($context, JSON_UNESCAPED_UNICODE);
            //插入数据
            $insertSql = "insert into shopnc_daily_report
            (`calc_date`, `context`, `version`, `updated_at`)
            values
            ('{$calcDate}', '$contextJson', $version, '$nowTime'
            )";

            $result = $pdo->query($insertSql);
            if (!$result) {
                \Log::error(0, 'daily_report_insert_data_error', [$insertSql, $result]);
                return;
            }
            $before_day--;
        }
        $testMail = 'huangguoliang@yinuovip.com';
        // 发送当月数据到邮件组
        $startDate = date('Ym01', $nowTime);
        $nowMonth = date('n', $nowTime);
        $nowYear = date('Y', $nowTime);
        $endDate = date('Ym01', strtotime('+1 month', $nowTime));
        $maxDay = date("j", strtotime(date('Ym01', strtotime('+1 month', $nowTime))-1));
        $monthDays = range(1, $maxDay);
        $weekStr = [
            '0'=>'日',
            '1'=>'一',
            '2'=>'二',
            '3'=>'三',
            '4'=>'四',
            '5'=>'五',
            '6'=>'六',
        ];
        $dateInfo = [
            'nowMonth' => $nowMonth,
            'monthDays' => $monthDays,
            'maxDay' => $maxDay,
            'nowYear' => $nowYear,
            'weekStr' => $weekStr,
        ];
        $sql = "select * from shopnc_daily_report where calc_date>= {$startDate} and calc_date<$endDate  order by calc_date ASC LIMIT 31";
        $data = $pdo->query($sql)->fetchAll();
        if ($data){
            $data = collect($data)->keyBy('calc_date')->toArray();
        }
        \Mail::send('DailyReport.ReportEveryday', ['data' => $data, 'dateInfo'=>$dateInfo], function ($msg) use ($testMail, $nowTime) {
            $mailto = isProduction() ? config('mail.dailyreporters') : $testMail;
            $msg->from(config("mail.from.address"), config("mail.from.name"));
            $msg->to($mailto)->subject("统计日报_".date('Y-m-d', $nowTime));
        });

//        \Mail::send('DailyReport.MemberLoanSummaryToFinance', ['data' => $data], function ($msg) use ($testMail) {
//            $mailto = isProduction() ? config('mail.dailytofinance') : $testMail;
//            $msg->from(config("mail.from.address"), 'power');
//            $msg->to($mailto)->subject("任我花财务统计日报_".date('Y-m-d', time()));
//        });
    }
} 