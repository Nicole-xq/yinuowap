<?php
namespace App\Console\Commands\Script;

use App\Lib\Helpers\DateHelper;
use App\Models\ShopncRankingMember;
use Illuminate\Console\Command;

class RankingNewBieScript extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'script:rankingNewBieScript';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '新手榜单脚本';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //防止并发
        exec("/bin/ps aux | /bin/grep 'php' | /bin/grep -v 'grep' | grep -v '/bin/sh -c' | /bin/grep '".$this->name."'", $do);
        $this->info('process status:'.json_encode($do));
        if(count($do) > 1) exit();
        $this->info('start run ---------------------------------------------');

        $newbieConfig = config("newbie");

        $nowTime = time();
        $startTime = strtotime($newbieConfig["startDate"]);
        $endTime = strtotime($newbieConfig["endDate"]);
        $periodSec = $newbieConfig["periodSec"];

        if($endTime < $nowTime || $startTime > $nowTime){
            //未开始或者已结束
            return;
        }
        //计算这一期（当前）的开始时间 结束时间
        $nowStartTime = $nowTime - ($nowTime - $startTime)%$periodSec;
        $nowStartDate = DateHelper::timeToDbDate($nowStartTime);
        $nowEndTime = $nowTime;
        $nowEndDate = DateHelper::timeToDbDate($nowEndTime);;
        $nowPeriod = application()->rankingApi->getPeriodNum($nowTime);

        $this->info("当期($nowPeriod)：" . $nowStartDate . " - " . $nowEndDate . " handle start --------");

        //删除当期老数据
        ShopncRankingMember::wherePeriodNum($nowPeriod)
            ->whereRType(ShopncRankingMember::R_TYPE_NEWBIE)
            ->whereStatus(ShopncRankingMember::START_OK)
            ->delete();

        $ranKingAddSql = <<<EOD
INSERT INTO shopnc_ranking_member ( period_num, STATUS, r_type, start_date, end_date, ranking_num, recommend_num, member_id, member_name, member_avatar ) (
SELECT
	$nowPeriod,
	1,
	1,
	'$nowStartDate',
	'$nowEndDate',
	@r := @r + 1 AS row_num,
	ranRet.* 
FROM
	(
SELECT
	count( md.top_member ) cou,
	md.top_member,
	(
CASE
	
	WHEN MAX(mtab.member_name) IS NULL 
	OR MAX(mtab.member_name) = '' THEN
	( CASE WHEN MAX(mtab.member_mobile) IS NULL OR MAX(mtab.member_mobile) = '' THEN '匿名用户' ELSE MAX(mtab.member_mobile) END ) ELSE MAX(mtab.member_name) 
END 
	) member_name,
	MAX(mtab.member_avatar)
FROM
	shopnc_member_distribute md
	LEFT JOIN shopnc_member mtab ON md.top_member = mtab.member_id 
WHERE
	md.top_member > 0  and
	md.add_time > $nowStartTime and 
	md.add_time < $nowEndTime
GROUP BY
	md.top_member 
ORDER BY
	cou DESC 
	) ranRet,
	( SELECT @r := 0 ) b 
);
EOD;
        $rowCou = \DB::affectingStatement($ranKingAddSql);
        if($rowCou < 1){
            $ranKingAddSql = <<<EOD
INSERT INTO shopnc_ranking_member ( period_num, STATUS, r_type, start_date, end_date, ranking_num, recommend_num, member_id, member_name, member_avatar ) values (
	$nowPeriod,
	1,
	1,
	'$nowStartDate',
	'$nowEndDate',
	1,
	0,
	65099,
	'13916994003',
	'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKlUrGOYdvIYoljUQicWLzCG8qELtcF7gXZFalmGTRYaLDFnhwnTYu0H9dzibuMDOrYXvDVmbs0PFog/132'
	)
EOD;
            $rowCou = \DB::affectingStatement($ranKingAddSql);
        }
        $this->info("当期($nowPeriod)：" . $nowStartDate . " - " . $nowEndDate . ";插入个数: $rowCou  handle end --------");
    }
}