<?php
namespace App\Console\Commands\Script;

use App\Lib\Helpers\DateHelper;
use App\Models\ShopncMbUserToken;
use App\Models\ShopncRankingMember;
use Illuminate\Console\Command;

class OptimizeScriptScript extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'script:optimizeScriptScript';

    //登录token过期时间 单位秒 (7天)
    const LOGIN_TOKEN_EXPIRES = 604800;


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '优化性能脚本 1.删除过期token';

    /**
     * Execute the console command.
     * 删除过期token
     * @return mixed
     */
    public function handle()
    {
        //防止并发
        exec("/bin/ps aux | /bin/grep 'php' | /bin/grep -v 'grep' | grep -v '/bin/sh -c' | /bin/grep '".$this->name."'", $do);
        $this->info('process status:'.json_encode($do));
        if(count($do) > 1) exit();



        $this->info('start run ---------------------------------------------');

        $now = time();
        //删除时间
        $lastLoginTime = $now - self::LOGIN_TOKEN_EXPIRES;

//        ShopncMbUserToken::where("login_time", "<", $lastLoginTime)->delete();

        $this->info("end run --------");
    }
}