<?php
namespace App\Console\Commands\Script;

use App\Lib\HelperFactory;
use App\Lib\Helpers\LogHelper;
use App\Lib\Helpers\ToolsHelper;
use App\Models\ShopncCloseAccountLog;
use App\Models\ShopncOrder;
use Illuminate\Console\Command;

class AutoOrderCloseScript extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'script:autoOrderCloseScript';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '自动订单结算脚本';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //防止并发
        exec("/bin/ps aux | /bin/grep 'php' | /bin/grep -v 'grep' | grep -v '/bin/sh -c' | /bin/grep '".$this->name."'", $do);
        $this->info('process status:'.json_encode($do));
        if(count($do) > 1) exit();
        $this->info('start run ---------------------------------------------');
        $onceNum = 100;
        $storeId = 0;
        //总个数 未成功个数
        $sumCou = $notDoneCou = 0;
        $nowTime = time();
        $artistOrderClosingConfig = config("artist-order-closing");
        $closeFinnshedTime = $nowTime - $artistOrderClosingConfig['closingDay']*86400;
        do{
            $where[] = ["artist_id", ">", 0];
            //订单状态 40:已收货;
            $where[] = function($query){
                $query->whereIn("order_state", [ShopncOrder::ORDER_STATE_RECEIVED]);
            };

            //天数
            $where[] = ['finnshed_time', '<', $closeFinnshedTime];
            $where[] = ["artist_closing_status", ShopncOrder::ARTIST_CLOSING_STATUS_UN];
            $where[] = ["store_id", ">", $storeId];

            $allBuilder = ShopncOrder::query();
            HelperFactory::instance()->Model->whereBindQuery($allBuilder, $where);
            $all = $allBuilder->take($onceNum)->groupBy("store_id")->orderBy('store_id', 'ASC')->get(["store_id"]);
            $nowCou = count($all);
            $sumCou += $nowCou;
            //遍历所有店铺
            foreach ($all as $v){
                $v = $v->toArray();
                $storeId = $v['store_id'];
                $this->info("storeId:'{$storeId}' ,handle start --------");
                try{
                    $storeOnceNum = 100;
                    $nowStoreSumCou = $nowStoreCou = 0;
                    $whereNowStore = $where;
                    $whereNowStore[] = ["store_id", $storeId];
                    $nowOrderId = 0;
                    do{
                        $whereNowStore[] = ["order_id", ">", $nowOrderId];
                        $storeBuilder = ShopncOrder::query();
                        HelperFactory::instance()->Model->whereBindQuery($storeBuilder, $whereNowStore);

                        $nowStoreAll = $storeBuilder->take($storeOnceNum)->orderBy('order_id', 'ASC')->get();
                        $nowStoreCou = count($all);
                        $nowStoreSumCou += $nowStoreCou;
                        foreach ($nowStoreAll as $orderInfo) {
                            $orderInfo = $orderInfo->toArray();
                            $nowOrderId  = $orderInfo['order_id'];
                            /**开始结算----------------------------------------------------------------------------------------------------------------------------------------------*/
                            try{
                                $sum = 0;
                                \DB::transaction(function () use (&$orderInfo, &$sum, &$nowTime){
                                    $closingNo = ToolsHelper::uDate("YmdHisu").ToolsHelper::randomNumber(2);
                                    $orderInfo['goodsList'] = application()->orderApi->getOrderGoodsList($orderInfo['order_id'], ["goods_pay_price", "artwork_category_id"]);
                                    //订单费率总信息
                                    $orderFeeSumInfo = ["sum"=>0, "categoryInfo"=>[]];
                                    foreach ($orderInfo['goodsList'] as $orderGoods){
                                        $nowOrderGoodsFeeInfo = application()->orderApi->getOrderGoodsClosingFeeInfo(
                                            $orderGoods['goods_pay_price'],
                                            $orderGoods['artwork_category_id'],
                                            $orderInfo['order_amount'],
                                            $orderInfo['order_amount_original']
                                        );
                                        if(empty($orderGoods['artwork_category_id'])){
                                            $orderGoods['artwork_category_id'] = 9999999;
                                        }
                                        !isset($orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]) && $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']] = ["sum"=>"0.00"];
                                        $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]["sum"] = bcadd($nowOrderGoodsFeeInfo['feeAmount'], $orderFeeSumInfo["categoryInfo"][$orderGoods['artwork_category_id']]["sum"], 4);
                                    }
                                    //遍历每个分类保留两位,四舍五入,计算总金额
                                    foreach ($orderFeeSumInfo["categoryInfo"] as &$item) {
                                        $item["sum"] = round($item["sum"], 2);
                                        $orderFeeSumInfo["sum"]=bcadd($item["sum"], $orderFeeSumInfo["sum"], 2);
                                    }
                                    $orderInfo['order_amount_closing'] = bcsub($orderInfo['order_amount'], $orderFeeSumInfo['sum'], 2);
                                    $sum = bcadd($sum, $orderInfo['order_amount_closing'], 2);
                                    //更新order表
                                    application()->orderApi->orderClosingDone($orderInfo['order_id'], $closingNo, $nowTime,  $orderInfo['order_amount_closing']);
                                    if($sum > 0){
                                        //初始化账户
                                        application()->closeAccountApi->initAccount($orderInfo['store_id']);
                                        //更新余额
                                        application()->closeAccountApi->addAccountMoney($orderInfo['store_id'], $sum, ShopncCloseAccountLog::TRANSACTION_TYPE_FROM_ORDER, $closingNo, "艺术家订单结算");
                                    }
                                });
                            }catch(\Exception $e){
                                \Log::error('AutoOrderCloseScript_order_error', [LogHelper::getEInfo($e), $orderInfo]);
                                $this->error("orderId:'{$orderInfo["order_id"]}' ,handle error:");
                                $this->error("File:".$e->getFile(). "(" . $e->getLine() . ");".$e->getMessage());
                                continue;
                            }
                        }
                    }while($nowStoreCou >= $onceNum);
                    $this->info("storeId:'{$storeId}' ,handle end --------");
                }catch (\Exception $e){
                     \Log::error('AutoOrderCloseScript_error', [LogHelper::getEInfo($e), $v]);
                     $this->error("storeId:'{$storeId}' ,handle error:");
                     $this->error("File:".$e->getFile(). "(" . $e->getLine() . ");".$e->getMessage());
                     $notDoneCou ++;
                     continue;
                }
            }
        }while($nowCou >= $onceNum);
        $this->info("end run SumCount:{$sumCou}, FailCount:{$notDoneCou}------------------------");
    }
}