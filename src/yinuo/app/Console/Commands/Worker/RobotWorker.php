<?php
namespace App\Console\Commands\Worker;

use App\Models\Shop\Auction;
use App\Models\ShopncAuction;
use Illuminate\Console\Command;

class RobotWorker extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'worker:robotWorker';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '机器人服务';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //防止并发
        exec("/bin/ps aux | /bin/grep 'php' | /bin/grep -v 'grep' | grep -v '/bin/sh -c' | /bin/grep '".$this->name."'", $do);
        $this->info('process status:'.json_encode($do));
        if(count($do) > 1) exit();
        $this->info('start run ---------------------------------------------');
        YNApp()->init();
        while (true){
            $nowTime = time();
            $auction_list = ShopncAuction::where("auction_start_time", "<", $nowTime+1)
                ->where("auction_end_time", ">", $nowTime)
                ->where("auction_type", "<>", Auction::AUCTION_TYPE_PICKER)
                ->whereRaw("auction_reserve_price > `current_price`" )
                ->select("auction_id")
                ->get();
            $auction_list = $auction_list ? $auction_list->toArray() : [];
            //print_r($auction_list);
            foreach($auction_list as $auction){
                //获得锁开进程
                $lockKey = \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_ROBOT_RUNTIME.$auction['auction_id'];
                $myRandomValue = \Yinuo\Lib\HelperFactory::instance()->Redis->getRedisLock()->lock($lockKey, \Yinuo\Entity\Constant\RedisKeyConstant::LOCK_ROBOT_RUNTIME_EXPIRES);
                if($myRandomValue !== false){
                    $this->info($auction['auction_id']." start");
                    exec(config("worker-script.robotScript") . " {$auction['auction_id']} {$myRandomValue} > /dev/null &");
                }
            }
            //一秒钟一次
            usleep(1000000);
        }
    }
}