<?php

namespace App\Services\Constant;
/**
 * redis key常量集合
 * Class RedisConstant
 * @package App\Services\Constant
 */
class RedisKeyConstant{
    const PERIOD_NEWBIE_RANKING_LIST = 'period_newbie_ranking_list:';//新手排行榜
    const NEWBIE_RANKING_HISTORY_DATA = 'newbie_ranking_history_data';//新手排行榜
}