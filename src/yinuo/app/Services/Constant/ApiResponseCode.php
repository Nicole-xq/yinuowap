<?php 
namespace App\Services\Constant;
/**
 * 对外的Api响应code 常量集合
 * 这里保留code > 1000 给实际api业务使用
 * @package App\Services\Constant
 */
class ApiResponseCode{
    const SUCCESS = 200;

    const OPERATE_FAIL = 400;
    const ARG_INVALID = 401;
    const NOT_LOGIN = 402;
    const BAD_REQUEST = 403;
    const NOT_DATA= 406;

    const LIMIT_ACTION = 4104;

    //资金

    const MEMBER_ACCOUNT_BALANCE_LACK = 9000;
    const MEMBER_ACCOUNT_NOT_EXIST = 9002;







    const RED_PACKET_NO_VALID = 10000;
    const NEWBIE_BID_LOG = 10001;

    /**
     * 每个code都必须有msg
     * @var array
     */
    public static $codeMsg = [
        self::SUCCESS => 'success',

        self::OPERATE_FAIL => '操作失败',
        self::ARG_INVALID => '参数无效',
        self::NOT_LOGIN => '登录失效',
        self::BAD_REQUEST => '非法请求',
        self::NOT_DATA => '查找不到数据',

        self::LIMIT_ACTION => '操作频繁',
        self::MEMBER_ACCOUNT_BALANCE_LACK=>'余额不足',
        self::MEMBER_ACCOUNT_NOT_EXIST => '用户账户错误',
        self::RED_PACKET_NO_VALID => '代金券已失效',
        self::NEWBIE_BID_LOG => '您已经进行新手体验',
    ];

    /**
     * @param $code
     * @return mixed|null
     */
    public static function getMsg($code){
        return isset(self::$codeMsg[$code]) ? self::$codeMsg[$code] : null;
    }

}