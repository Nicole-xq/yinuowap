<?php 
namespace App\Services\Constant;
/**
 * 响应code 常量集合
 * @package App\Services\Constant
 */
class ResponseCode{
    const GOD_BLESS_YOU = 200;

    const NOT_LOGIN = 400;
    const ARG_INVALID = 401;
    const NOT_PERMISSION= 403;
    const NOT_DATA= 406;

    const FAIL = 501;

    const GROUP_NOT_PERMISSION = 601;

    /**
     * 每个code都必须有msg
     * @var array
     */
    public static $codeMsg = [
        self::GOD_BLESS_YOU => 'success',
        self::NOT_LOGIN => '登录失效',
        self::NOT_PERMISSION => '没有权限',
        self::FAIL => 'fail',
        self::ARG_INVALID => '参数无效',
        self::NOT_DATA => '查找不到数据',
        self::GROUP_NOT_PERMISSION => '当前组无权操作',
    ];

    /**
     * @param $code
     * @return mixed|null
     */
    public static function getMsg($code){
        return isset(self::$codeMsg[$code]) ? self::$codeMsg[$code] : null;
    }

}