<?php
namespace App\Services\Notification;
use App\Lib\TraitCode\InstanceCode;

/**
 * 消息通知基类
 * Class Notification
 * @package App\Services\Notification
 */
class Notification{
    use InstanceCode;

/*    const MAIL_FROM_ADDRESS = 'admin@xiaochong.com';
    const MAIL_FROM_NAME = '易催催-用户系统';*/

    const MAIL_VIEW_BASE = 'notification.';


    /**
     * 发送邮件通知
     * @param $to
     * @param $title
     * @param $view
     * @param $data
     */
    protected function sendMail($to, $title, $view, $data){
        $view = self::MAIL_VIEW_BASE.$view;
        $title = '易催催-'.$title;
        return \Mail::send($view, $data, function($message) use($to, $title)
        {
            $message->to($to)
                ->subject($title);
        });

    }
}