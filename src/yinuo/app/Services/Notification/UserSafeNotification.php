<?php
namespace App\Services\Notification;

class UserSafeNotification extends Notification{
    /**
     * @param $email
     * @param $ipPosition
     * array (
     * 'country' => '中国',
     * 'region' => '上海',
     * 'city' => '上海',
     * )
     * @return bool
     */
    public function LoginIpUnusual($email, $ipPosition){
        $viewData = $ipPosition;
        $viewData['title'] = '异常登录提醒';
        $this->sendMail($email, $viewData['title'], 'user_safe.login_ip_unusual', $viewData);
        return true;
    }
}