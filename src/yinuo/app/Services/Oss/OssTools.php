<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/4/10 0010
 * Time: 下午 21:31
 */

namespace App\Services\Oss;


class OssTools
{
    public static function getImgUrl($path, $type = '240'){
        $img_path = getMainConfig('oss.img_url').$path;
        if($type != '1280'){
            //设置样式
            $img_path .= '?x-oss-process=style/'.$type;
        }
        return $img_path;
    }

}