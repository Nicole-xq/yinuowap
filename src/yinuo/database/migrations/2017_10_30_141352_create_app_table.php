<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app', function (Blueprint $table) {
            $table->increments('id')->comment('三方app接入 app基础信息表 ');
            $table->unsignedInteger('uid')->comment('申请人 用户id');
            $table->string('app_name', 20)->comment('应用名称 唯一');
            $table->string('app_key', 18)->comment('app_key');
            $table->string('app_secret', 32)->default('')->comment('app_secret 默认为空');
            $table->string('domain', 60)->comment('域名');
            $table->unsignedInteger('apply_time')->default(0)->comment('申请时间');
            $table->unsignedInteger('audit_end_time')->default(0)->comment('审核完成时间');
            $table->unsignedTinyInteger('status')->default('0')->comment('状态 0.创建完成 1.申请中 2.申请成功 20.申请失败 21.禁用');
            $table->timestamps();
            $table->unique('app_name');
            $table->index(['uid', 'status']);
        });
        \DB::select('ALTER TABLE `app` ADD INDEX `app_app_key_length_9_index` (`app_key`(9))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app');
    }
}
