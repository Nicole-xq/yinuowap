<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginIpInfoLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_ip_info_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip', 15)->comment('登录ip地址');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('country', 30)->default('')->comment('国家');
            $table->string('region', 30)->default('')->comment('州、省 等');
            $table->string('city', 30)->default('')->comment('城市 等');
            $table->unsignedInteger('login_time')->comment('登录时间');
            $table->unsignedTinyInteger('is_unusual')->default(0)->comment('是否 异常地址');
            $table->unsignedTinyInteger('confirm_status')->default(0)->comment('确认状态 0.未确认 1.确认自己登录 2.无法确认 3.确认非自己登录');
            $table->timestamps();
            //ALTER TABLE `login_ip_info_log` ADD INDEX `login_ip_info_log_user_id_country_region_city_index` (`user_id`, `country`(10), `region`(10), `city`(10));
            $table->index(['user_id', 'country', 'region', 'city']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_ip_info_log');
    }
}
