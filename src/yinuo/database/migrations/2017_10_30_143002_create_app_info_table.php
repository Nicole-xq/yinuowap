<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_info', function (Blueprint $table) {
            $table->increments('id')->comment('app 详细信息表');
            $table->unsignedInteger('app_id')->comment('app表id');
            $table->string('introduction', 160)->comment('应用简介');
            $table->string('organization_name', 42)->comment('机构名称 如果是个人为个人姓名');
            $table->unsignedTinyInteger('organization_type')->comment('机构类型 1.公司 2.个人');
            $table->string('industry', 30)->comment('所属行业');
            $table->string('phone', 13)->comment('联系电话');
            $table->string('qq', 11)->default('')->comment('qq 号码');
            $table->timestamps();
            $table->index('app_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_info');
    }
}
