<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppApiParamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_api_param', function (Blueprint $table) {
            $table->increments('id')->comment('api参数 表');
            $table->unsignedInteger('api_id')->comment('app_api_all表id');
            $table->string('name', 26)->comment('字段名');
            $table->unsignedTinyInteger('type')->comment('类型 1.number 2.string 3.bool 4.array 5.object');
            $table->unsignedTinyInteger('io_type')->default('1')->comment('1.入参 2.出参');
            $table->unsignedTinyInteger('is_required')->default(0)->comment('是否必填 1.必填 0.非必填');
            $table->string('detail', 50)->default('')->comment('说明');
            $table->timestamps();
            $table->index('api_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_api_param');
    }
}
