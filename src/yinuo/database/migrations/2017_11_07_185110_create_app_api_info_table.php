<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppApiInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_api_info', function (Blueprint $table) {
            $table->increments('id')->comment('app_api 详细信息表');
            $table->unsignedInteger('api_id')->comment('app_api_all表id');
            $table->string('introduce', 1000)->nullable()->comment('介绍');
            $table->timestamps();
            $table->unique('api_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_api_info');
    }
}
