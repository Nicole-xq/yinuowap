<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group', function (Blueprint $table) {
            $table->increments('id')->comment('群组表id');
            $table->string('name', 32)->default('')->comment('群组名称');
            $table->unsignedInteger('parent_id')->default(0)->comment('上级组id');
            $table->tinyInteger('type')->default(0)->comment('小组类型 1:管理员 2:公司 3:大区 4:组织 5:小组');
            $table->unsignedInteger('group_master')->default(0)->comment('管理人用户id');
            $table->string('desc', 255)->default('')->comment('小组描述');
            $table->tinyInteger('status')->default(0)->comment('状态 0:禁用 1:正常');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group');
    }
}
