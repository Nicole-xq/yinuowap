<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppApiAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_api_all', function (Blueprint $table) {
            $table->increments('id')->comment('api主表 所有api');
            $table->unsignedInteger('uid')->comment('api 创建人id');
            $table->string('api_name', 50)->comment('api 名称');
            $table->unsignedInteger('platform_id')->comment('平台id');
            $table->string('api_uri', 150)->comment('api的uri 指定了平台id uri应该是和平台关联的,通过平台的domain 应该是可以得到全地址');
            $table->unsignedTinyInteger('api_type')->default(1)->comment('api 类型 1.公共 2.需服务器授权api 3.需用户授权api ');
            $table->unsignedTinyInteger('request_type')->default(1)->comment('请求类型 1.get 2.post');
            $table->unsignedTinyInteger('status')->default(0)->comment('api 状态 0.待发布 1.已发布 2.已下线');
            $table->timestamps();
        });
        \DB::select('ALTER TABLE `app_api_all` ADD INDEX `app_api_all_platform_id_api_uri_length_55_status_index` (`platform_id`,`api_uri`(55),`status`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_api_all');
    }
}
