<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompanyTypeToGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group', function (Blueprint $table) {
            $table->unsignedTinyInteger('company_type')->default(1)->comment('公司类型 1.催收机构 2.委案机构');
            $table->index(['type', 'company_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group', function (Blueprint $table) {
            $table->dropIndex(['type', 'company_type']);
            $table->dropColumn('company_type');
        });
    }
}
