<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppApiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_api', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('app_id')->comment('app表id');
            $table->unsignedInteger('api_id')->comment('app_api_all表id');
            $table->unsignedInteger('apply_time')->default(0)->comment('申请时间');
            $table->unsignedInteger('audit_end_time')->default(0)->comment('审核完成时间');
            $table->unsignedTinyInteger('status')->default('0')->comment('状态 0.创建完成 1.申请中 2.申请成功 20.申请失败 21.禁用');
            $table->timestamps();
            $table->unique(['app_id', 'api_id']);
            $table->index(['app_id', 'api_id', 'status']);
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_api');
    }
}
