<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminOperateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_operate_log', function (Blueprint $table) {
            $table->increments('id')->comment('管理员操作日志表');
            $table->unsignedInteger('operate_user_id')->comment('管理员 user_id');
            $table->string('action_name', 80)->comment('操作名，在admin系统中对于 功能权限名');
            $table->string('ip', 15)->comment('记录时的ip地址');
            $table->string('param', 1000)->nullable()->comment('json 格式保存的参数');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_operate_log');
    }
}
