<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppApiApplyLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_api_apply_log', function (Blueprint $table) {
            $table->increments('id')->comment('app申请api日志');
            $table->unsignedInteger('app_id')->comment('app表id');
            $table->unsignedInteger('api_id')->comment('app表id');
            $table->unsignedInteger('operate_uid')->comment('操作人 用户id');
            $table->string('detail', 300)->default('')->comment('详细说明');
            $table->unsignedTinyInteger('last_status')->default('0')->comment('之前状态 app 表的状态');
            $table->unsignedTinyInteger('status')->comment('状态 app 表的状态');
            $table->timestamps();
            $table->index(['app_id','api_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_api_apply_log');
    }
}
