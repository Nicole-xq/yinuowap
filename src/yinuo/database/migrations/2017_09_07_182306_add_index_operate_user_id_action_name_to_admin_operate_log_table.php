<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOperateUserIdActionNameToAdminOperateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_operate_log', function (Blueprint $table) {
            //
            $table->index('operate_user_id');
            $table->index('action_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_operate_log', function (Blueprint $table) {
            //
        });
    }
}
