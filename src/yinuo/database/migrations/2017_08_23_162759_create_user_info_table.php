<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->increments('id')->comment('用户信息表id');
            $table->unsignedInteger('user_id')->default(0)->comment('用户id')->unique();
            $table->string('real_name', 32)->default('')->comment('真实姓名');
            $table->string('nick_name', 32)->default('')->comment('昵称');
            $table->tinyInteger('sex')->default(0)->comment('性别,0未知1男2女');
            $table->string('idcard_num', 20)->default('')->comment('身份证号');
            $table->string('head_img')->default('')->comment('用户头像');
            $table->string('nation', 20)->default('')->comment('民族');
            $table->string('homeplace', 20)->default('')->comment('籍贯');
            $table->string('position', 32)->default('')->comment('职位、用户身份');
            $table->tinyInteger('status')->default(0)->comment('状态 0:空闲 1:在职 2:离职');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
