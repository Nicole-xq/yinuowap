<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/4/14 0014
 * Time: 17:07
 * 新手配置文件
 */

return [
    'startDate' => '2019-04-29 00:00:00',
    'endDate' => '2020-12-28 23:59:59',
    //一个周期的秒数 //7*24*60*60 7天
    'periodSec' => 604800,
    //缓存秒数
    'cacheTimeOut' => 1800,
    //每一期顶包的
    'topPackage' => [
        '2'=>[
            [
                'period_num' => 2,
                'member_name' => '13564945670',
                'member_avatar' => 'http://thirdwx.qlogo.cn/mmopen/vi_32/6KibibNSichQekPufSEQOuUjFO2LdQ4NPy0wgstVOkv6VpB7kRxgfqMYwTYDZiciahdYHbhrTVP1dic3IxzPmWGnC3bw/132',
                'ranking_num' => 1,
                'recommend_num' => 30,
            ],
            [
                'period_num' => 2,
                'member_name' => '13916994003',
                'member_avatar' => 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKlUrGOYdvIYoljUQicWLzCG8qELtcF7gXZFalmGTRYaLDFnhwnTYu0H9dzibuMDOrYXvDVmbs0PFog/132',
                'ranking_num' => 2,
                'recommend_num' => 28,
            ],
            [
                'period_num' => 2,
                'member_name' => '17612157796',
                'member_avatar' => 'http://ynysimg.oss-cn-beijing.aliyuncs.com/head_img/avatar_81.jpg',
                'ranking_num' => 3,
                'recommend_num' => 25,
            ]
        ]
    ]
];