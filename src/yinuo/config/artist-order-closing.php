<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/3/24 0024
 * Time: 9:39
 */
return [
    //结算周期 订单完成后 单位天
    'closingDay' => 7,
    //结算费用 比例 百分比 主分类id => 佣金比例
    'closingFeeRatio' => [
        14=>['feeRatio'=>15, 'c_name'=>'珠宝玉翠'],
        1=>['feeRatio'=>50, 'c_name'=>'水墨篆刻'],
        4=>['feeRatio'=>50, 'c_name'=>'西画雕塑'],
        28=>['feeRatio'=>15, 'c_name'=>'紫砂陶瓷'],
        39=>['feeRatio'=>5, 'c_name'=>'文玩手串'],
    ],
    //未找到时 使用最大的费用比例
    'closingFeeUnknownRatio' => 50
];