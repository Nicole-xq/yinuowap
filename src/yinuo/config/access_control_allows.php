<?php
/**
 * 配置
 * Access-Control-Allow-Origin', config('app.allow'));
 * Access-Control-Allow-Headers', 'Origin,Content-Type,Cookie,Accept');
 * Access-Control-Allow-Methods', 'GET,POST,PATCH,PUT,OPTIONS');
 * Access-Control-Allow-Credentials
 */
return [
    //Access-Control-Allow-Origin 只有匹配了这里下面的头才会被设置
    'origins'=>[
        //微信
        '/^http:\/\/.*\.yinuovip\.com/',
        '/^https:\/\/.*\.yinuovip\.com/',
        '/^https:\/\/servicewechat\.com/',
        '/^http:\/\/127\.0\.0\.1\:\d+/',
        '/^http:\/\/192\.168\.97\.\d+\:\d+/',
    ],
    'headers'=>'Origin,Content-Type,Cookie,Accept,X-App-Ver',
    'methods'=>'POST,GET,OPTIONS',
    'credentials'=>'true',
];