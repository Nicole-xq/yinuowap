<?php


return [
    //服务监控
//    "zookeeper" => env('ZOOKEEPER', "zoo1.rwh.com:2181,zoo2.rwh.com:2181,zoo3.rwh.com:2181"),

    //服务使用的协议
    "protocol" => 'Thrift\Protocol\TBinaryProtocol',

    //客户端配置
    "clients" => [
        'Server.Rpc.Service.Artist.ArtistService' => [
            'endpoint' => env('ARTIST_RPC_URL', 'http://127.0.0.3/rpc'),
        ],
//        'Server.Rpc.Service.Oauth.OauthService' => [
//            'endpoint' => env('I_RPC_URL', 'http://192.168.100.77/rpc'),
//        ],
    ],

    //服务提供者配置
    "providers" => [
        'Server.Rpc.Service.ShopNc.ShopService' => [
            'handler' => 'App\Rpc\ShopServiceHandler',
        ],
        'Server.Rpc.Service.ShopNc.UserService' => [
            'handler' => 'App\Rpc\UserServiceHandler',
        ],
    ],
];