<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/4/14 0014
 * Time: 17:07
 * 工作脚本配置
 */

return [
    'robotScript' => env('WorkerRobotScript', '/opt/php/bin/php /srv/www/devDir/main_yinuo/crontab/index.php workerRobot')
];