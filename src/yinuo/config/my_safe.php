<?php

return [
    'is_send_login_unusual_mail' => env('SAFE_IS_SEND_LOGIN_UNUSUAL_MAIL', true),
    //ip 安全地址规则配置
    'ip_position_safe' => [
        // 10天内 同一城市 出现至少2次 为正常地址
        [
            'timeRange' => 864000, //时间区间 前 秒 10天
            'matchCount' => 2, //匹配次数
            'matchLevel' => 3, //级别 1.COUNTRY 2.REGION 3.CITY  越大越下层
        ]
    ],
    //ip 安全地址规则配置 最小的数据库记录 基数
    'ip_position_safe_min_count' => 20,
];
