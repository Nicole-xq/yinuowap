<?php

return [
    #safe_path安全域
    'safe_path'=>[
        'http://api.renwohua.com',
        'http://api.test.renwohua.com',

        'https://api.renwohua.com',
        'https://api.test.renwohua.com',

        'http://www.renwohua.com',
        'https://www.renwohua.com',
        //微信
        'https://wechat.renwohua.com',
        'http://wechat.renwohua.com',
        'http://wechat.test.renwohua.com',

        'https://s.renwohua.com',
        'http://s.renwohua.com',

        'https://shop.renwohua.com',
        'http://shop.renwohua.com',

        'https://open.renwohua.com',
        'http://open.renwohua.com',

        'http://passport.yicuicui.com',
        'http://i.yicuicui.com',
        'http://d.yicuicui.com',
        'http://op.yicuicui.com',
        'http://data.yicuicui.com',
        'http://case.yicuicui.com',
        'http://admin.yicuicui.com',
        'http://www.newyicuicui-core.com',
        'http://www.newyicuicui-web.com',
        'http://ycc.com'
    ]

];
