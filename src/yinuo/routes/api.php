<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//需要登录信息的路由
Route::group(['middleware' =>  ['login.info']], function () {
    Route::post('NewBie/index', 'NewBieController@index');
    Route::post('NewBie/getAuctionList', 'NewBieController@getAuctionList');
    Route::post('RankingList/index', 'RankingListController@index');
});


Route::post('NewBie/auctionDetail', 'NewBieController@auctionDetail');
//创建新手体验后的订单
Route::post('NewBie/createNewBieAuctionOrder', 'NewBieController@createNewBieAuctionOrder');

Route::post('RedPacket/getRedPacketList', 'RedPacketController@getRedPacketList');
Route::post('RedPacket/getRedPacketTplList', 'RedPacketController@getRedPacketTplList');


//必须登录的路由
Route::group(['middleware' =>  ['login.limit']], function () {
});

//需要登录的接口
//Route::group(['namespace' => 'Api', 'middleware' => 'verify.user.login', 'prefix'=> 'api'], function ($route) {
//    //订单
//    require __DIR__ . '/routes/order.php';
//    require __DIR__ . '/routes/express.php';
//    require __DIR__ . '/routes/artistOrder.php';
//});

require __DIR__ . '/api/auction.php';