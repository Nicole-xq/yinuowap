<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Auction', 'prefix'=> 'auction'], function ($route) {
    //需要登录信息的路由
    Route::group(['middleware' =>  ['login.info']], function () {

    });
    //必须登录的路由
    Route::group(['middleware' =>  ['login.limit']], function () {
        Route::post('createMarginOrder', 'AuctionController@createMarginOrder');

        //获取订单拍卖支付信息
        Route::post('showAuctionOrder', 'AuctionController@showAuctionOrder');
    });


});