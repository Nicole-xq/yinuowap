# 后台cms

* 生成环境 :`composer install or update --no-dev` "跳过 require-dev 字段中列出的包"
* 开发环境 :`composer install or update`
* `mv .env.example .env`
* 配置 .env
* 密钥生成 `php artisan key:generate`
* 数据库sql 迁移\(填充数据\) `php artisan db:seed`
* 数据库迁移 `php artisan migrate`
* Remove the compiled class file `php artisan clear-compiled`


## ide-helper使用 2.4
- php src/yinuo/artisan ide-helper:generate - phpDoc generation for Laravel Facades
- php src/yinuo/artisan ide-helper:models - phpDocs for models (*php artisan ide-helper:models Post*)
- php src/yinuo/artisan ide-helper:models "\App\Models\ShopNcOrder"
- php src/yinuo/artisan ide-helper:meta - PhpStorm Meta file


## 创建model[https://github.com/ignasbernotas/laravel-model-generator]
- php src/yinuo/artisan make:models --tables=shopnc_close_account --extends="App\Models\BaseModel"


    php src/yinuo/artisan help make:models
    
    Usage:
      make:models [options]
    
    Options:
          --tables[=TABLES]          Comma separated table names to generate
          --prefix[=PREFIX]          Table Prefix [default: DB::getTablePrefix()]
          --dir[=DIR]                Model directory [default: "Models/"]
          --extends[=EXTENDS]        Parent class [default: "Illuminate\Database\Eloquent\Model"]
          --fillable[=FILLABLE]      Rules for $fillable array columns [default: ""]
          --guarded[=GUARDED]        Rules for $guarded array columns [default: "ends:_guarded"]
          --timestamps[=TIMESTAMPS]  Rules for $timestamps columns [default: "ends:_at"]
      -i, --ignore[=IGNORE]          Ignores the tables you define, separated with ,
      -f, --force[=FORCE]            Force override [default: false]
      -s, --ignoresystem             If you want to ignore system tables.
                                                  Just type --ignoresystem or -s
      -m, --getset                   Defines if you want to generate set and get methods
      -h, --help                     Display this help message
      -q, --quiet                    Do not output any message
      -V, --version                  Display this application version
          --ansi                     Force ANSI output
          --no-ansi                  Disable ANSI output
      -n, --no-interaction           Do not ask any interactive question
          --env[=ENV]                The environment the command should run under.
      -v|vv|vvv, --verbose           Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
    
    Help:
      Build models from existing schema.