/**
 * copyBy https://github.com/shiguang0116/gulp3-project/blob/master/gulpfile.js
 * @type {Gulp}
 */
var gulp = require('gulp');
var uglify = require('gulp-uglify');        // js压缩
var pump = require('pump');
var rename = require('gulp-rename');        // 重命名
var clean = require('gulp-clean');          // 清空文件夹
var gulpif = require('gulp-if');            // 条件判断
var concat = require('gulp-concat');        // 合并文件
var babel = require('gulp-babel'); //编译es6
var runSequence = require('run-sequence');  // 按顺序执行task


var js_libs_path = 'wap/**/*.js';
var js_main_path = [js_libs_path,  '!wap/*.js'];
// js_main_path.push( '!wap/*.js');
//var js_libs_path = 'wap/js/**/*.js';


// 设置环境变量
var env = 'dev';    // 用于执行gulp任务时的判断
function set_env(type){
    env = type || 'dev';
}


// 打包js 直接copy
gulp.task('js_libs', function(){
    return gulp.src(js_libs_path)
        .pipe(rename({
            dirname: '' // 清空路径
        }))
        .pipe(gulp.dest('dist/js'));
});
gulp.task('js_main', ['uglify_check'], function(){
    return gulp.src(js_main_path)
        .pipe(babel({compact:false}))                  // 编译es6语法
       // .pipe(concat('main.min.js'))    // 合并文件并命名
        .pipe(uglify())  // 判断是否压缩js
        .pipe(gulp.dest('dist/js'));
});
/**
 * @description 检查压缩JS时的错误，作为'js_main'的依赖执行。
 *
 * 1、解决js压缩出错的问题
 * 2、解决修改的代码有语法错误时，服务会终止的问题
 */
gulp.task('uglify_check', function (cb) {
    pump([
        gulp.src(js_main_path),
        babel({compact:false}),
        uglify(),
    ], cb);
});
// 清空dist文件夹
gulp.task('clean', function(){
    return gulp.src(['dist/*'])
        .pipe(clean());
});

// 生产环境
gulp.task('build', function(cb) {
    set_env('build');
    runSequence(
        ['clean'],  // 首先清理文件，否则会把新打包的文件清掉
        ['js_libs'],
        ['js_main'], // 不分先后的任务最好并行执行，提高效率
        // ['rev'], // 所有文件打包完毕之后开始生成版本清单文件
        // ['set_version', 'version.txt'], // 根据清单文件替换html里的资源文件
        cb);
});